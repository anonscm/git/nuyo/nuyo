package de.tarent.groupware.resource;


/**
 * @author kleinw
 *
 *	Konstanten des RessourceWorkers.
 *
 */
public interface ResourceWorkerConstants {

    public final static String KEY_NAME = "name";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    
}
