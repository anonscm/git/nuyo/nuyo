package de.tarent.groupware.preview;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddresscategoryDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Limit;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.OptimizedInList;
import de.tarent.groupware.utils.ResultTransform;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcReflectedWorker;


/**
 * @author kleinw
 *	
 *	Funktionalit�ten, die sich mit Previews von Adressen
 *	besch�ftigen.
 *
 */
public class PreviewWorker extends TcReflectedWorker {

    final static public String[] INPUT_GETPREVIEWS = {"categories", "userid"};
    final static public boolean[] MANDATORY_GETPREVIEWS = {false, false};
    final static public String OUTPUT_GETPREVIEWS = "previews";
    
    final static public Object getPreviews(TcAll all, List categories, Integer userId) throws SQLException, TcContentProzessException {
        List response = new ArrayList();
        	Select preview = 
        	    SQL.Select(TcDBContext.getDefaultContext())
        	    	.from(TaddressextDB.getTableName())
        	    	.add(TaddressextDB.PK_PKADDRESSEXT, Integer.class)			//	[0]
        	    	.add(TaddressextDB.LABELFN, String.class);					//	[1]
        	
        	if(categories != null && categories.size() > 0) {
        	    response = 
        	        preview
        	        	.join(TaddresscategoryDB.getTableName(), TaddresscategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
        	        	.where(Expr.in(TaddresscategoryDB.FKCATEGORY, categories))
        	        	.getList(TcDBContext.getDefaultContext());
        	        
        	} else if(userId!= null) {
        	    return
        	    	preview
        	    		.join(TaddresscategoryDB.getTableName(), TaddresscategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
        	    		.join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TcategoryDB.PK_PKCATEGORY)
        	    		.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
        	    		.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
        	    		.where(Where.and(Expr.equal(TgroupUserDB.FKUSER, userId), Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1))))
        	    		.getList(TcDBContext.getDefaultContext());
        	    		
        	} else
        	    throw new TcContentProzessException("F�r die Methode 'getPreviews' muss entweder eine Kategorieliste oder eine Userid �bergeben werden.");
        	
        	return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
    }
    
    final static public String INPUT_GETAVAIBLEPREVIEWFIELDS[] = {};
    final static public boolean MANDATORY_GETAVAIBLEPREVIEWFIELDS[] = {};
    final static public String OUTPUT_GETAVAIBLEPREVIEWFIELDS = "fields";
    
    final static public Object getAvaiblePreviewFields(TcAll all) throws SQLException, TcContentProzessException {
        List fields = new ArrayList();
        fields = TaddressDB.getAvaiblePreviewFields();
        return ResultTransform.toSingleString(fields, all.getRequestObject().getRequestParameters());
              
    } 

    final static public String INPUT_GETCUSTOMPREVIEW[] = {"fields","sortField","order","offset","limit"};
    final static public boolean MANDATORY_GETCUSTOMPREVIEW[] = {true,true,true,true,true};
    final static public String OUTPUT_GETCUSTOMPREVIEW = "response";
    
    final static public Object getCustomPreview(TcAll all,List fields,String sortField,Boolean order, Integer offset, Integer limit) throws SQLException, TcContentProzessException {
        List response = new ArrayList();
        Select preview = 
    	    SQL.Select(TcDBContext.getDefaultContext())
    	    	.from(TaddressDB.getTableName());
        		for (int i = 0; i< fields.size(); i++){
        		    try {
                        preview.add("taddress." + fields.get(i), Class.forName(TaddressDB.getClassForPreviewField("taddress." + fields.get(i))));
                    } catch (ClassNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
        		}
        		// Sortierung nach Feld
        		if (order.booleanValue()){
        		    preview.orderBy(Order.asc("taddress." + sortField));
        		}else{
        		    preview.orderBy(Order.desc("taddress." + sortField));
        		}
        		// Offset/Limit
        		preview.Limit(new Limit(limit,offset));
        response = preview.getList(TcDBContext.getDefaultContext());		
        		
        return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
              
    } 
    
    final static public String INPUT_GETPREVIEWFIRSTLETTERS[] = {"sortField","order"};
    final static public boolean MANDATORY_GETPREVIEWFIRSTLETTERS[] = {true,true};
    final static public String OUTPUT_GETPREVIEWFIRSTLETTERS = "letters";
    
    final static public Object getPreviewFirstLetters(TcAll all, String sortField, Boolean order) throws SQLException, TcContentProzessException {
        Select preview = 
    	    SQL.Select(TcDBContext.getDefaultContext())
    	    	.from(TcDBContext.getSchemaName() + "vfirstkeys_" + sortField)
    	    	.add("firstletter",String.class)
    	    	.add("min",Integer.class);
		        if (order.booleanValue()){
		            preview.orderBy(Order.asc("firstletter"));
		        }else{
		            preview.orderBy(Order.desc("firstletter"));
		        }

		List response = preview.getList(TcDBContext.getDefaultContext());
		return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
    } 
    
    final static public String INPUT_GETSORTEDPREVIEWS[] = {"pks","sortField","order"};
    final static public boolean MANDATORY_GETSORTEDPREVIEWS[] = {true,true,true};
    final static public String OUTPUT_GETSORTEDPREVIEWS= "pks";
    
    final static public Object getSortedPreviews(TcAll all,List pks, String sortField, Boolean order) throws SQLException, TcContentProzessException {

        OptimizedInList inList = new OptimizedInList("pk_address",(Integer[])pks.toArray(new Integer[0]));
        Select preview = 
    	    SQL.Select(TcDBContext.getDefaultContext())
            .from( TcDBContext.getSchemaName() + "taddress")
            .add("pk_address",Integer.class)
            .where(new RawClause(inList.getOptimezedClause()));
            //.where(Expr.in("pk_address",pks));
        if (order.booleanValue()){
            preview.orderBy(Order.asc("coalesce (" + sortField + ",'')"));
        }else{
            preview.orderBy(Order.desc("coalesce (" + sortField + ",'')"));
        }
        
		List response = preview.getList(TcDBContext.getDefaultContext());
		return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
    } 
}
