package de.tarent.groupware.utils;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author kleinw
 *
 *	Funktionalitšt zum Konvertieren von Datentypen.
 *
 */
public class Conversion {

    final static private SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
    final static private SimpleDateFormat SDF_WRITE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    final static public SimpleDateFormat CONVERT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    
    /**
     * @param date
     * @return
     * @deprecated use Timestamp Object rather than String
     */
    static public String toTimestampString(Date date) {
        return
        	SDF_WRITE.format(date);
    }
    
    /**
     * @param date
     * @return
     * @deprecated use Timestamp Object rather than String
     */
    static public String toTimestampString(Long date) {
        return
        	SDF_WRITE.format(new Date(date.longValue()));
    }

    /**
     * @param date
     * @return
     * @deprecated use Timestamp Object rather than String
     */
    static public String toDateString(Date date) {
        return
        	SDF.format(date);
    }
    
    /**
     * @param date
     * @return
     * @deprecated use Timestamp Object rather than String
     */
    static public String toDateString(Long date) {
        return
        	SDF.format(new Date(date.longValue()));
    }

    /**
     * Returns the Integer Value of the ID.
     *
     * @param id This may be an Integer object or an not null object with an numeric toString()-result.
     * @return Integer value if id
     */
    static public Integer getInteger(Object id) {
        return (id instanceof Integer) ? (Integer)id : Integer.valueOf(id.toString());
    }
    
}
