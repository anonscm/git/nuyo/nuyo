package de.tarent.groupware.security;

import java.util.Map;

import de.tarent.groupware.queries.SecurityQueries;
import de.tarent.groupware.utils.EntityDescription;
import de.tarent.groupware.utils.EntityDescriptionAdapter;

/**
 * @author Sebastian Mancke
 *
 */
public interface SecurityWorkerConstants {

	public static Integer SCHEDULE_RIGHT_RELEASE = new Integer(SecurityQueries.SCHEDULERIGHT_RELEASE);
    public static Integer SCHEDULE_RIGHT_WRITE = new Integer(SecurityQueries.SCHEDULERIGHT_WRITE);
    public static Integer SCHEDULE_RIGHT_READ = new Integer(SecurityQueries.SCHEDULERIGHT_READ);

    public static EntityDescription userFolderRightEntityDescription = new EntityDescriptionAdapter() {
            public String getEntityName() {
                return "userFolderRight";
            }
            protected void addPropertyToDBKeyMappings(Map mapping) {
                mapping.put("category", "fk_folder");
                mapping.put("userid", "fk_user");
                mapping.put("auth_read", "auth_read");
                mapping.put("auth_edit", "auth_edit");
                mapping.put("auth_add", "auth_add");
                mapping.put("auth_remove", "auth_remove");
                mapping.put("auth_structure", "auth_structure");
                mapping.put("auth_addsub", "auth_addsub");
                mapping.put("auth_removesub", "auth_removesub");
                mapping.put("auth_grant", "auth_grant");
            }
        };
    
    public static EntityDescription userGlobalRightEntityDescription = new EntityDescriptionAdapter() {
            public String getEntityName() {
                return "userGlobalRight";
            }
            protected void addPropertyToDBKeyMappings(Map mapping) {
                mapping.put("userid", "fk_user");
                mapping.put("auth_edituser", "auth_edit");
                mapping.put("auth_removeuser", "auth_removeuser");
                mapping.put("auth_editfolder", "auth_editfolder");
                mapping.put("auth_removefolder", "auth_removefolder");
                mapping.put("auth_editschedule", "auth_editschedule");
                mapping.put("auth_removeschedule", "auth_removeschedule");
                mapping.put("auth_delete", "auth_delete");
            }
        };
        
}
