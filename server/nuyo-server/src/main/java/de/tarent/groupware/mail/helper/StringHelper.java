/*
 * Created on 26.10.2004
 */
package de.tarent.groupware.mail.helper;

public class StringHelper {
	private StringHelper() {
	}

	static public String trim(String string) {
		int a = 0;
		int b = string.length();
		while (a < b && (
				string.charAt(a) == '"' ||
				string.charAt(a) == '\'' ||
				string.charAt(a) == ' ' ))
			a++;
		while (b > a && (
				string.charAt(b - 1) == '"' ||
				string.charAt(b - 1) == '\'' ||
				string.charAt(b - 1) == ' ' ))
			b--;
		
		if (a != b)
			return string.substring(a, b);
		else
			return null;
	}}
