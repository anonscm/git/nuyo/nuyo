/* $Id: RoleWorkerConstants.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.role;

/**
 *
 * TODO Erkl�rung!? 
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public interface RoleWorkerConstants {
    public static final String KEY_EDITUSER = "auth_edituser";
    public static final String KEY_REMOVEUSER = "auth_removeuser";
    public static final String KEY_EDITFOLDER = "auth_editfolder";
    public static final String KEY_REMOVEFOLDER = "auth_removefolder";
    public static final String KEY_EDITSCHEDULE = "auth_editschedule";
    public static final String KEY_REMOVESCHEDULE = "auth_removeschedule";
    public static final String KEY_DELETE = "auth_delete";
	public static final String KEY_ROLENAME = "rolename";
	public static final String KEY_READ = "auth_read";
	public static final String KEY_EDIT = "auth_edit";
	public static final String KEY_ADD = "auth_add";
	public static final String KEY_REMOVE = "auth_remove";
	public static final String KEY_ADDSUB = "auth_addsub";
	public static final String KEY_REMOVESUB = "auth_removesub";
	public static final String KEY_STRUCTURE = "auth_structure";
	public static final String KEY_GRANT = "auth_grant";
}
