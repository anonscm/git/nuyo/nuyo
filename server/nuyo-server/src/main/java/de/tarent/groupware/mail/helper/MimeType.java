/*
 * Created on 26.10.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.tarent.groupware.mail.helper;

import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.Part;

/**
 * @author christoph
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MimeType {
	private MimeType() {
	}

	private static final Pattern pattern = Pattern.compile("\\t|\\n|\\r|\\f");

	public static String getContentType(Object object) throws MessagingException {
		if (object instanceof Part) {
			return getContentType(((Part)object).getContentType());
		} else if (object instanceof String) {
			return pattern.matcher((String)object).replaceAll("; ");
		} else {
			return null;
		}
	}

	public static String getMimeType(String contenttype) {
		if (contenttype == null) return null;
		if (contenttype.indexOf(';') != -1) {
			return StringHelper.trim(contenttype.substring(0, contenttype.indexOf(';'))).toLowerCase();
		} else {
			return StringHelper.trim(contenttype).toLowerCase();
		}
	}

	public static String getFilename(String contenttype) {
		if (contenttype == null) return null;
		int pos = contenttype.indexOf("name=") + 5;
		if (pos != 4) {
			if (contenttype.indexOf(';', pos) != -1) {
				return StringHelper.trim(contenttype.substring(pos, contenttype.indexOf(';', pos)));
			} else {
				return StringHelper.trim(contenttype.substring(pos));
			}
		} else {
			return null;
		}
	}
}
