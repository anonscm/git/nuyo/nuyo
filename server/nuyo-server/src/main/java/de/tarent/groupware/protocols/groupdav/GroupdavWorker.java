package de.tarent.groupware.protocols.groupdav;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.octopus.server.OctopusContext;

public class GroupdavWorker {
    
    public static final String[] INPUT_GETUSERSADDRESSETAGS = {"userid"};
    public static final boolean[] MANDATORY_GETUSERSADDRESSETAGS = {true};
    public static final String OUTPUT_GETUSERSADDRESSETAGS = "etags";
    
    /**
     * 
     * @param all
     * @param userId
     * @return
     * @throws SQLException
     */
    public static final Map getUsersAddressETags(OctopusContext all, Integer userId) 
                throws SQLException {     
        
        Result tr = null;
        Map mapResult = new HashMap();
        
        try {
            tr = de.tarent.dblayer.sql.SQL.Select(TcDBContext.getDefaultContext())
                        .from(TaddressDB.getTableName())
                        .join(TcDBContext.getSchemaName() + "v_user_address", "v_user_address.fk_address", TaddressDB.PK_PKADDRESS) 
                        .selectAs(TaddressDB.PK_PKADDRESS)
                        .selectAs(TaddressDB.CREATED)
                        .selectAs(TaddressDB.CHANGED)
                        .where(Expr.equal("v_user_address.userid", userId))
                        .executeSelect(TcDBContext.getDefaultContext());
    
            while (tr.resultSet().next()) {
                int pkAddress = tr.resultSet().getInt(TaddressDB.PK_PKADDRESS);
                
                long eTagGroupdav;
                if (tr.resultSet().getTimestamp(TaddressDB.CHANGED) != null)
                    eTagGroupdav = tr.resultSet().getTimestamp(TaddressDB.CHANGED).getTime();
                else 
                    eTagGroupdav = tr.resultSet().getTimestamp(TaddressDB.CREATED).getTime();
                
                mapResult.put(Integer.valueOf(pkAddress), Long.valueOf(eTagGroupdav));                
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }

        return mapResult;
    }    
}
