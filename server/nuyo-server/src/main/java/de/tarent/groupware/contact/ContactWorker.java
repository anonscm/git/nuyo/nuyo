package de.tarent.groupware.contact;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.bean.TcontactDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.Statement;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.address.AddressWorker;
import de.tarent.groupware.utils.AttributeSet;
import de.tarent.groupware.utils.ResultTransform;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcReflectedWorker;

/**
 * @author kleinw
 *	
 *	Hier wird die Kontakthistorie realisiert.
 *
 */
public class ContactWorker extends TcReflectedWorker implements ContactWorkerConstants {

    private static Logger logger = Logger.getLogger(TcReflectedWorker.class.getName());

    final static public String[] INPUT_GETCONTACTS = {"addressid"};
    final static public boolean[] MANDATORY_GETCONTACTS = {false};
    final static public String OUTPUT_GETCONTACTS = "contacts";
    
    final static public Object getContacts(TcAll all, Integer addressId) throws InputParameterException, SQLException {
        
        List response = null;
        
        Select select = SQL.Select(TcDBContext.getDefaultContext())
        	.from(TcontactDB.getTableName())
        	.add(TcontactDB.PK_PK, Integer.class)						//	[0]
        	.add(TcontactDB.SUBJECT, String.class)						//	[1]
        	.add(TcontactDB.NOTE, String.class)							//	[2]
        	.add(TcontactDB.STRUCTINFO, String.class)					//	[3]
        	.add(TcontactDB.FKCONTACTCATEGORY, Integer.class)			//	[4]
        	.add(TcontactDB.FKCONTACTCHANNEL, Integer.class)			//	[5]
        	.add(TcontactDB.FKCONTACTLINKTYPE, Integer.class)			//	[6]
        	.add(TcontactDB.CONTACTDATE, Timestamp.class)				//	[7]
        	.add(TcontactDB.CONTACTDIRECTION, Integer.class)			//	[8]
        	.add(TcontactDB.DURATION, Integer.class)					//	[9]
        	.add(TcontactDB.FKUSER, Integer.class);						//	[10]
    	
        if(addressId != null)
    	    response = 
    	        select.where(Expr.equal(TcontactDB.FKADDRESS, addressId))
    	    	.orderBy(Order.desc(TcontactDB.CONTACTDATE))
    	    	.orderBy(Order.desc(TcontactDB.PK_PK))
				.getList(TcDBContext.getDefaultContext());
    	else
    	    response =
    	    	select.getList(TcDBContext.getDefaultContext());
        
        return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
    }

    

    final static public String[] INPUT_CREATEORMODIFYCONTACT = {"contactid", "addressid", "category", "channel",
    													"duration", "linktype", "inbound", "attributes",
    													"startdate"};
	final static public boolean[] MANDATORY_CREATEORMODIFYCONTACT = {false, true, false, true, true,
															 true, true, true, true,
															 true};
	final static public String OUTPUT_CREATEORMODIFYCONTACT = null;															 
    
    final static public void createOrModifyContact(TcAll all, Integer contactId, Integer addressId, Integer category, Integer channel,
          									Integer duration, Integer linkType, Boolean inBound, Map attributes, Long startDate) throws SQLException {        
        Integer userID = all.personalConfig().getUserID();
        	
		Integer inbound = new Integer(0);
		if(inBound.booleanValue()) inbound = new Integer(1);
        
            InsertUpdate insertUpdate =
                SQL.InsertUpdate(TcDBContext.getDefaultContext())
                	.table(TcontactDB.getTableName())
                	.add(TcontactDB.FKADDRESS, addressId)
                	.add(TcontactDB.FKUSER, userID)
                	.add(TcontactDB.FKCONTACTCATEGORY, category)
                	.add(TcontactDB.FKCONTACTCHANNEL, channel)
                	.add(TcontactDB.FKCONTACTLINKTYPE,linkType)
					.add(TcontactDB.CONTACTDIRECTION, inbound)
					.add(TcontactDB.CONTACTDATE, new Date(startDate.longValue()))
					.add(TcontactDB.DURATION, duration);
            
            AttributeSet as = new AttributeSet(attributes, insertUpdate);
            as.add(TcontactDB.SUBJECT, KEY_SUBJECT);
            as.add(TcontactDB.NOTE, KEY_NOTE);
            as.add(TcontactDB.STRUCTINFO, KEY_STRUCT_INFO);
            
            if(contactId == null) {
                InsertKeys keys = insertUpdate.executeInsertKeys(TcDBContext.getDefaultContext());
                contactId = keys.getPkAsInteger();
            } else {
                insertUpdate
                	.executeUpdate(TcDBContext.getDefaultContext(), TcontactDB.PK_PK, contactId);
            }
    }


    
    final static public String[] INPUT_CREATECONTACTS = {"contactlist"};
	final static public boolean[] MANDATORY_CREATECONTACTS = {true, true};
	final static public String OUTPUT_CREATECONTACTS = null;
    /**
		 * Legt eine Liste von Contact Eintr�gen an. Erwartete Parameter: contactlist = Liste von Maps mit den Eintr�gen:
		 * 
		 * Map attributes; (keys "subject", "note", "structInfo") 
		 * Integer addressid; 
		 * Integer category; 
		 * Integer channel;
		 * Integer linktype; 
		 * Boolean inbound; 
		 * Long startdate; 
		 * Integer duration;
		 */
	final static public void createContacts(TcAll all, List contactlist) throws InputParameterException, SQLException
	{
		for (Iterator iter = contactlist.iterator(); iter.hasNext();)
		{
			Map attributes = null;
			Integer addressid = null;
			Integer category = null;
			Integer channel = null;
			Integer linktype = null;
			Long startdate = null;
			Integer duration = null;
			Boolean inbound = null;
			try
			{
				Map contact = (Map) iter.next();
				
				attributes = (Map) contact.get("attributes");
				addressid = (Integer) contact.get("addressid");
				category = (Integer) contact.get("category");
				channel = (Integer) contact.get("channel");
				linktype = (Integer) contact.get("linktype");
				
				inbound = (Boolean) contact.get("inbound");
				if (inbound == null)
					throw new InputParameterException("Der Parameter 'inbound' in ContactWorker.createContacts() wurde nicht angegeben");
				
				startdate = (Long) contact.get("startdate");
				if (startdate == null)
					throw new InputParameterException("Der Parameter 'startdate' in ContactWorker.createContacts() wurde nicht angegeben");
				
				duration = (Integer) contact.get("duration");
			} catch (NumberFormatException e)
			{//can't find any reason for an NFE
				logger.log(Level.WARNING, "Ein Parameter in ContactWorker.createContacts() ist keine g�ltige Nummer", e);
				throw new InputParameterException("Ein Parameter in ContactWorker.createContacts() ist keine g�ltige Nummer", e);
			} catch (ClassCastException e)
			{
				logger.log(Level.WARNING, "Ein Parameter in ContactWorker.createContacts() hat den falschen Typ", e);
				throw new InputParameterException("Ein Parameter in ContactWorker.createContacts() hat den falschen Typ", e);
			}
			if (attributes == null || addressid == null || category == null || channel == null || linktype == null || inbound == null
				|| duration == null)
				throw new InputParameterException("Ein Parameter in ContactWorker.createContacts() wurde nicht angegeben");

			logger.finest("execute insert for addressid: " + addressid);
			createOrModifyContact(all, null, addressid, category, channel, duration, linktype, inbound, attributes, startdate);
		}
		logger.fine("Habe " + contactlist.size() + " Datens�tze eingef�gt.");
	}

    protected static String getLocalName(String globalName) {
        return globalName.substring(globalName.indexOf(".") + 1);
    }

    
	/**
	 * Macht einen Eintrag in TContact f�r das gegebene Event und die gegebene Adresse <b>Es fehlt noch die Wahl des
	 * richtigen Kanals!</b>
	 * 
	 * @param eventId
	 *          EventPK
	 * @param addressId
	 *          PK der Adresse
	 * @param userID
	 *          PK des Users
	 * @param note
	 *          Notiz
	 * @param subject
	 *          Betreff
	 * @throws SQLException
	 * @deprecated will be removed soon to cleanup the source
	 */
	public static void insertContactEntry(TcAll all, Integer eventId, Integer addressId, Integer userID, String note, String subject) throws SQLException {
		boolean internal = AddressWorker.isAddressInternal(all, addressId).booleanValue();
		
	
		InsertUpdate tcontactinsert = SQL.InsertUpdate(TcDBContext.getDefaultContext())
			.table(TcontactDB.getTableName())
			.add(TcontactDB.CONTACTDATE, new Date())
			.add(TcontactDB.CONTACTDIRECTION, new Integer(0))
			.add(TcontactDB.DURATION, new Integer(0))
			.add(TcontactDB.FKADDRESS, addressId)
			.add(TcontactDB.FKCONTACTCATEGORY, ContactWorkerConstants.CATEGORY_REQUEST);
	    if(internal){
	    	tcontactinsert.add(TcontactDB.FKCONTACTCHANNEL, ContactWorkerConstants.CHANNEL_DIRECT);
	    }else{
	    	tcontactinsert.add(TcontactDB.FKCONTACTCHANNEL, ContactWorkerConstants.CHANNEL_EMAIL);
	    }
	    tcontactinsert.add(TcontactDB.FKCONTACTLINKTYPE, ContactWorkerConstants.LINKTYPE_APPOINTMENT);
	    tcontactinsert.add(TcontactDB.FKCONTACTLINK, eventId);
	    tcontactinsert.add(TcontactDB.FKUSER, userID);
	    tcontactinsert.add(TcontactDB.NOTE, note);
	    tcontactinsert.add(TcontactDB.SUBJECT, subject);
	    Statement tcontactstatement = tcontactinsert.insert();
	    DB.update(TcDBContext.getDefaultContext(), tcontactstatement);
	}
	
	/**
	 * Macht einen Eintrag in TContact f�r das gegebene Event und die gegebene Adresse
	 * <b>Es fehlt noch die Wahl des richtigen Kanals!</b>
	 * @param eventId	EventPK 
	 * @param addressId	PK der Adresse
	 * @param username	Login des Users
	 * @param note	Notiz
	 * @param subject	Betreff
	 * @throws SQLException
	 * @deprecated will be removed soon to cleanup the source
	 */
	public static void insertContactEntry(TcAll all, Integer eventId, Integer addressId, String username, String note, String subject) throws SQLException {
		boolean internal = AddressWorker.isAddressInternal(all, addressId).booleanValue();
		
	
		InsertUpdate tcontactinsert = SQL.InsertUpdate(TcDBContext.getDefaultContext())
			.table(TcontactDB.getTableName())
			.add(TcontactDB.CONTACTDATE, new Date())
			.add(TcontactDB.CONTACTDIRECTION, new Integer(0))
			.add(TcontactDB.DURATION, new Integer(0))
			.add(TcontactDB.FKADDRESS, addressId)
			.add(TcontactDB.FKCONTACTCATEGORY, ContactWorkerConstants.CATEGORY_REQUEST);
		if(internal){
	    	tcontactinsert.add(TcontactDB.FKCONTACTCHANNEL, ContactWorkerConstants.CHANNEL_DIRECT);
	    }else{
	    	tcontactinsert.add(TcontactDB.FKCONTACTCHANNEL, ContactWorkerConstants.CHANNEL_EMAIL);
	    }

	    Select userselect = SQL.Select(TcDBContext.getDefaultContext())
    	.from(TuserDB.getTableName())
    	.select(TuserDB.PK_PKUSER)
    	.where(Expr.equal(TuserDB.LOGINNAME, username));

		tcontactinsert
	    	.add(TcontactDB.FKCONTACTLINKTYPE, ContactWorkerConstants.LINKTYPE_APPOINTMENT)
	    	.add(TcontactDB.FKCONTACTLINK, eventId)
	    	.add(TcontactDB.FKUSER, userselect)
	    	.add(TcontactDB.NOTE, note)
	    	.add(TcontactDB.SUBJECT, subject);
	    Statement tcontactstatement = tcontactinsert.insert();
	    DB.update(TcDBContext.getDefaultContext(), tcontactstatement);
	}
    
    final static public String[] INPUT_CREATECONTACTFORADDRESSEVENTRELATION = {"eventid", "addresses"};
    final static public boolean[] MANDATORY_CREATECONTACTFORADDRESSEVENTRELATION = {true, true};
    final static public String OUTPUT_CREATECONTACTFORADDRESSEVENTRELATION = null;
    
    /**
     * 
     * @param all
     * @param eventid
     * @param addresses
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    final static public void createContactForAddressEventRelation(TcAll all, Integer eventid, List addresses) throws SQLException{
    	Iterator it = addresses.iterator();
    	while(it.hasNext()){
    		Integer addressId = (Integer) it.next();
    		String username = all.getRequestObject().get("username");
    		insertContactEntry(all, eventid, addressId, username, "zu Termin " + eventid + " eingeladen", "Einladung zu Termin");
    	}
    }
}