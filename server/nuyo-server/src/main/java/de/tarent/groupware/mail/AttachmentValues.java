package de.tarent.groupware.mail;

import java.sql.ResultSet;

import de.tarent.dblayer.engine.DBContext;
import de.tarent.groupware.mail.helper.Attachments.StorageType;

/**
 * An interface to access attachment values stored in a DB.
 *
 * @see de.tarent.groupware.mail.helper.Attachments#getValuesFrom(ResultSet)
 * @see de.tarent.groupware.mail.helper.Attachments#retrieveValuesOf(DBContext, String, Integer)
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public interface AttachmentValues {
    public String getName();
    public String getFilePath();
    public StorageType getStorageType();
}
