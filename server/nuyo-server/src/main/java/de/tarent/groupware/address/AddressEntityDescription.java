package de.tarent.groupware.address;
import java.util.Map;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.groupware.utils.EntityDescriptionAdapter;

public class AddressEntityDescription extends EntityDescriptionAdapter {

    public static final String ABTEILUNG = "abteilung";
	public static final String ADDRESS_ZUSATZ = "addressZusatz";
	public static final String ADR_NR = "adrNr";
	public static final String AKAD_TITLE = "akadTitle";
	public static final String ANREDE = "anrede";
	public static final String BANK = "bank";
	public static final String BANKLEITZAHL = "bankleitzahl";
	public static final String BRIEFANSCHRIFT = "briefanschrift";
	public static final String BUNDESLAND = "bundesland";
	public static final String CAPTURE_DATE = "captureDate";
	public static final String E_MAIL = "eMail";
	public static final String E_MAIL_PRIVAT = "eMailPrivat";
	public static final String FAX = "fax";
	public static final String FAX_PRIVAT = "faxPrivat";
	public static final String FK_USER_FOLLOWUP = "fk_user_followup";
	public static final String FOLLOWUP = "followup";
	public static final String FOLLOWUPDATE = "followupdate";
	public static final String GEBURTSDATUM = "geburtsdatum";
	public static final String GEBURTSJAHR = "geburtsjahr";
	public static final String GESCHLECHT = "geschlecht";
	public static final String HAUS_NR = "hausNr";
	public static final String HERRN_FRAU = "herrnFrau";
	public static final String HOMEPAGE = "homepage";
	public static final String INSTITUTION = "institution";
	public static final String INSTITUTION_ERWEITERT = "institutionErweitert";
	public static final String KONTONUMMER = "kontonummer";
	public static final String LAND = "land";
	public static final String LKZ = "lkz";
	public static final String MODIFICATION_DATE = "modificationDate";
	public static final String NACHNAME = "nachname";
	public static final String NAME_ERWEITERT = "nameErweitert";
	public static final String NAMENSZUSATZ = "namenszusatz";
	public static final String ORT = "ort";
	public static final String PLZ = "plz";
	public static final String POSITION = "position";
	public static final String POSTFACH_NR = "postfachNr";
	public static final String POSTFACH_NR2 = "postfachNr2";
	public static final String SPITZNAME = "spitzname";
	public static final String STRASSE = "strasse";
	public static final String TELEFON = "telefon";
	public static final String TELEFON_MOBIL = "telefonMobil";
	public static final String TELEFON_MOBIL_PRIVAT = "telefonMobilPrivat";
	public static final String TELEFON_PRIVAT = "telefonPrivat";
	public static final String TITEL = "titel";
	public static final String VORNAME = "vorname";

	protected void addPropertyToDBKeyMappings(Map propToDB) {
        propToDB.put(ADR_NR, TaddressDB.PK_PKADDRESS);
        propToDB.put(NACHNAME, TaddressDB.LASTNAME);
        propToDB.put(VORNAME, TaddressDB.FIRSTNAME);
        propToDB.put(NAME_ERWEITERT, TaddressDB.MIDDLENAME);
        propToDB.put(SPITZNAME, TaddressDB.NICKNAME);
        propToDB.put(NAMENSZUSATZ, TaddressDB.NAMESUFFIX);
        propToDB.put(AKAD_TITLE, TaddressDB.TITELACADEMIC);
        propToDB.put(TITEL, TaddressDB.TITELJOB);
        propToDB.put(GESCHLECHT, TaddressDB.SEX);
        propToDB.put(HERRN_FRAU, TaddressDB.SALUTATION);
        propToDB.put(ANREDE, TaddressDB.LETTERSALUTATION);
        propToDB.put(GEBURTSJAHR, TaddressDB.YEAROFBIRTH);
        propToDB.put(GEBURTSDATUM, TaddressDB.DATEOFBIRTH);
        propToDB.put(INSTITUTION, TaddressDB.ORGANISATION);
        propToDB.put(INSTITUTION_ERWEITERT, TaddressDB.ORGANISATION2);
        propToDB.put(ABTEILUNG, TaddressDB.DEPARTMENT);
        propToDB.put(POSITION, TaddressDB.POSITION);
        propToDB.put(STRASSE, TaddressDB.STREET);
        propToDB.put(HAUS_NR, TaddressDB.HOUSENO);
        propToDB.put(PLZ, TaddressDB.ZIPCODE);
        propToDB.put(ORT, TaddressDB.CITY);
        propToDB.put(BUNDESLAND, TaddressDB.REGION);
        propToDB.put(LKZ, TaddressDB.COUNTRYSHORT);
        propToDB.put(LAND, TaddressDB.COUNTRY);
        propToDB.put(POSTFACH_NR, TaddressDB.POBOX);
        propToDB.put(POSTFACH_NR2, TaddressDB.POBOXZIPCODE);
        propToDB.put(BANK, TaddressDB.BANKNAME);
        propToDB.put(BANKLEITZAHL, TaddressDB.BANKNO);
        propToDB.put(KONTONUMMER, TaddressDB.BANKACCOUNT);
        propToDB.put(ADDRESS_ZUSATZ, TaddressDB.NOTEONADDRESS);
        propToDB.put(FOLLOWUP, TaddressDB.FOLLOWUP);
        propToDB.put(FOLLOWUPDATE, TaddressDB.FOLLOWUPDATE);
        propToDB.put(FK_USER_FOLLOWUP, TaddressDB.FKUSERFOLLOWUP);
        propToDB.put(E_MAIL, TaddressextDB.EMAIL);
        propToDB.put(E_MAIL_PRIVAT, TaddressextDB.EMAILHOME);
        propToDB.put(TELEFON_PRIVAT, TaddressextDB.FONHOME);
        propToDB.put(TELEFON, TaddressextDB.FON);
        propToDB.put(TELEFON_MOBIL_PRIVAT, TaddressextDB.MOBILEHOME);
        propToDB.put(TELEFON_MOBIL, TaddressextDB.MOBILE);
        propToDB.put(FAX_PRIVAT, TaddressextDB.FAXHOME);
        propToDB.put(FAX, TaddressextDB.FAX);
        propToDB.put(HOMEPAGE, TaddressextDB.URL);
        propToDB.put(CAPTURE_DATE, TaddressDB.CREATED);
        propToDB.put(MODIFICATION_DATE, TaddressDB.CHANGED);
        propToDB.put(BRIEFANSCHRIFT, TaddressDB.LETTERSALUTATION);

        // Fields gibt es im Client nicht
        //propToDB.put("", TaddressDB.SUBSCRIPTOR);
        //propToDB.put("", TaddressDB.SUBSCRIPTION);
        //propToDB.put("", TaddressDB.SUBSCRIPTIONDATE);

        // Todo:
        //             propToDB.put("", TaddressDB.MANAGED);
        //             propToDB.put("", TaddressDB.CREATEDBY);
        //             propToDB.put("", TaddressDB.CHANGEDBY);
        //             propToDB.put("", TaddressDB.CREATED);
        //             propToDB.put("", TaddressDB.CHANGED);
        //             propToDB.put("", TaddressDB.EXTERNALID);
        //             propToDB.put("", TaddressDB.EXTERNALDATE);
        //             propToDB.put("", TaddressDB.ADDRESSSUFFIX);
        //             propToDB.put("", "street2");
        //             propToDB.put("", "houseno2");
        //             propToDB.put("", "zipcode2");
        //             propToDB.put("", "city2");
        //             propToDB.put("", "region2");
        //             propToDB.put("", "countryshort2");
        //             propToDB.put("", "country2");
        //             propToDB.put("", "pobox2");
        //             propToDB.put("", "poboxzipcode2");
        //             propToDB.put("", "addresssuffix2");
        //             propToDB.put("", TaddressDB.FKUSER);
    }

    public String getEntityName() {
        return "address";
    }

}