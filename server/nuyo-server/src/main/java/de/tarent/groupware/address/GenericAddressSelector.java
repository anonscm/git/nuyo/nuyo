package de.tarent.groupware.address;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.EntityDescription;
import de.tarent.groupware.utils.GenericSelector;
import de.tarent.octopus.server.InOutParam;
import de.tarent.octopus.server.OctopusContext;

/**
 * Liefern von Adressen auf eine generische Weise.
 * 
 * @author Sebastian Mancke
 */
public class GenericAddressSelector {

    static EntityDescription entityDescription = new AddressEntityDescription();

    public String[] INPUT_loadEntityDescription = new String[] {};
    public String OUTPUT_loadEntityDescription = "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD;
    public EntityDescription loadEntityDescription(OctopusContext cntx) {
        return entityDescription;
    } 


    
    public String[] INPUT_createBaseSelect = new String[] { "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD,
                                                            "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD,
                                                            "CONTENT:"+GenericSelector.CURRENT_WHERE_LIST_FIELD
    };
    public boolean[] MANDATORY_createBaseSelect = new boolean[] { true, false, false };

    public void createBaseSelect(OctopusContext cntx,
                                 EntityDescription entityDescription,
                                 InOutParam selectP,
                                 InOutParam wherelistP) {
        
        Select select = SQL.Select(TcDBContext.getDefaultContext())
            .from(TcDBContext.getSchemaName() + "v_user_address")
            .from(TaddressDB.getTableName())
            .from(TaddressextDB.getTableName());
        
        select.selectAs(TaddressDB.PK_PKADDRESS, entityDescription.getPropertyNameByDB(TaddressDB.PK_PKADDRESS));
        selectP.set(select);
        
         WhereList wherelist = new WhereList();
         wherelist.addAnd(Expr.equal("v_user_address.userid", cntx.personalConfig().getUserID()) );
         wherelist.addAnd(new RawClause("v_user_address.fk_address = "+ TaddressextDB.PK_PKADDRESSEXT));
         wherelist.addAnd(new RawClause(TaddressextDB.PK_PKADDRESSEXT +" = "+ TaddressDB.PK_PKADDRESS));

         wherelistP.set(wherelist);
    }    

    public String[] INPUT_orderBaseSelect = new String[] { "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD,
            "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD,
            "CONTENT:"+GenericSelector.CURRENT_WHERE_LIST_FIELD
    		};
    public boolean[] MANDATORY_orderBaseSelect = new boolean[] { true, false, false };

    public void orderBaseSelect(OctopusContext cntx, EntityDescription entityDescription, InOutParam selectP, InOutParam wherelistP) {
		
		Select select = (Select) selectP.get();
		
		select.selectAs(TaddressextDB.SORTORDER, TaddressextDB.SORTORDER);
		select.orderBy(Order.asc(TaddressextDB.SORTORDER));
		selectP.set(select);
    }    
    
    
    /**
     * This small task iterates through the filter parameters and searches for die Date-String value of followupdate 
     * to replace it with a real date. So it can be handles from different db-sources 
     */
    
    public String[] INPUT_parseFilterExpressions = new String[] {"filter"};
    public boolean[] MANDATORY_parseFilterExpressions = new boolean[] { false };
    public String OUTPUT_parseFilterExpressions = "filter";

    public List parseFilterExpressions(OctopusContext cntx, List filter){
    	
    	
    	if (filter == null || filter.size() == 0)
            return null;
    	else {
    		// hier wird das als String �bergebene Filterobject followupdate gesucht und zu einem Date umgewandelt
    	    
    		for (int i = 0 ; i < filter.size(); i++){
	    		Object tmp = filter.get(i);
	    		
	    		if ("followupdate".equals(tmp)) {
		    		Object tmp2 = filter.get(i + 1);
		    		if (tmp2 instanceof String){
		    			Date followupdate = null;
						try {
							//Fri Nov 03 17:26:31 CET 2006
							SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.US);
							followupdate = format.parse((String)tmp2);
						} catch (Exception e) {
							e.printStackTrace();
						}					
		    			if (followupdate != null)
		    				filter.set(i+1, followupdate);
		    		}
		    		break;
	    		}
	    	}
    	}
    	return filter;
    }
    

    
    

//     /**
//      * Suchanfrage f�r die Wiederfolage
//      */
//     public String[] INPUT_addReviewFilter = new String[] { "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD,
//                                                            "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD,
//                                                            "CONTENT:"+GenericSelector.CURRENT_WHERE_LIST_FIELD
//     };
//     public boolean[] MANDATORY_addReviewFilter = new boolean[] { true, false, false };
    
//     public void addReviewFilter(OctopusContext cntx,
//                                 EntityDescription entityDescription,
//                                 InOutParam selectP,
//                                 InOutParam wherelistP) {
        
//         Select select = SQL.Select()
//             .from(TcDBContext.getSchemaName() + "v_user_address")
//             .from(TaddressDB.getTableName())
//             .from(TaddressextDB.getTableName());
        
//         select.selectAs(TaddressDB.PK_PKADDRESS, entityDescription.getPropertyNameByDB(TaddressDB.PK_PKADDRESS));
//         selectP.set(select);
        
//         WhereList wherelist = new WhereList();
//         wherelist.addAnd(Expr.equal("v_user_address.userid", cntx.personalConfig().getUserID()) );
//         wherelist.addAnd(new RawClause("v_user_address.fk_address = "+ TaddressextDB.PK_PKADDRESSEXT));
//         wherelist.addAnd(new RawClause(TaddressextDB.PK_PKADDRESSEXT +" = "+ TaddressDB.PK_PKADDRESS));
        
//         wherelistP.set(wherelist);
//     }    

//     public String[] INPUT_createReviewBaseSelect = new String[] {};
//     public String OUTPUT_createReviewBaseSelect = "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD;
//     public Select createReviewBaseSelect(OctopusContext cntx) throws SQLException {        
//         return SQL.SelectDistinct()
//             .from(TaddressDB.getTableName())
//             .join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS)
//             .join(TcDBContext.getSchemaName() + "v_user_address", "v_user_address.fk_address", TaddressDB.PK_PKADDRESS)
//             .where(Expr.equal("v_user_folder.userid", cntx.personalConfig().getUserID()));
//     }

}
