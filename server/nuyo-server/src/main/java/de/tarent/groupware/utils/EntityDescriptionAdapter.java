package de.tarent.groupware.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Template Method Pattern
 */
public abstract class EntityDescriptionAdapter implements EntityDescription {
    HashMap propToDB = new HashMap();
    HashMap dbToProp = new HashMap();
    String [] propertyNames;
    String [] dbKeys;

    public EntityDescriptionAdapter() {
        addPropertyToDBKeyMappings(propToDB);


        propertyNames = new String[propToDB.size()];
        dbKeys = new String[propToDB.size()];
        
        int i = 0;
        for (Iterator iter = propToDB.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry)iter.next();
            dbToProp.put(entry.getValue(), entry.getKey() );
            propertyNames[i] = (String)entry.getKey();
            dbKeys[i] = (String)entry.getValue();
            i++;
        }
    }

    /**
     * Template Method.
     * Hier m�ssen die Mappings von der konkreten Realisierung hinzugef�gt werden.
     * 
     * Die DB Keys werden mit tabellenprefix erwartet. z.B: 
     *   propToDB.put("nachname", "taddress.lastname");
     */
    protected abstract void addPropertyToDBKeyMappings(Map mapping);


    /**
     * Liefert den Namen der Entity.
     */
    public abstract String getEntityName();
        


    public String getPropertyNameByDB(String dbKey) {
        return (String)dbToProp.get(dbKey);
    }
    public String getDBKeyByProperty(String propertyName) {
        return (String)propToDB.get(propertyName);
    }

    public String[] getPropertyNames() {
        return propertyNames;
    }
    
    public String[] getDBKeys() {
        return dbKeys;
    }

}