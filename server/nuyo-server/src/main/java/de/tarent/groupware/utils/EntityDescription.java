package de.tarent.groupware.utils;


/**
 * liefert Informationen zu den Bestandteilen einer
 * Entity des Cients. Darin ist vor allem das Mapping
 * der Entityfelder auf die der DB abgelegt.
 */
public interface EntityDescription {

    /**
     * Liefert den Namen einer EntityProperty zu einem Spaltenbezeichner der DB.
     * @param dbKey voll qualifiziertzer Spaltenbezeichner. z.B. taddress.lastname
     */
    public String getPropertyNameByDB(String dbKey);

    /**
     * Liefert den qualifiziertzer Spaltenbezeichner der DB zu einem Namen einer EntityProperty.
     * @param propertyName name, den die property auf dem Client hat.
     */
    public String getDBKeyByProperty(String propertyName);

    
    /**
     * Liefert den Namen der Entity.
     */
    public String getEntityName();



    /**
     * Liefert alle Property Namen
     */
    public String[] getPropertyNames();


    /**
     * Liefert alle DB Keys
     */
    public String[] getDBKeys();
}