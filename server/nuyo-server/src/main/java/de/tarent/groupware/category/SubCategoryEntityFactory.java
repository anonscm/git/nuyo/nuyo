package de.tarent.groupware.category;

import de.tarent.commons.datahandling.entity.DefaultEntityFactory;
import de.tarent.commons.datahandling.entity.EntityFactory;
import de.tarent.groupware.impl.SubCategoryImpl;

/**
 * 
 * @author Nils Neumaier
 *
 */
 
public class SubCategoryEntityFactory extends DefaultEntityFactory {
	
	private static SubCategoryEntityFactory instance = null;
	
	protected SubCategoryEntityFactory(){
		super(SubCategoryImpl.class);
	}
	
	public static synchronized SubCategoryEntityFactory getInstance() {
		if(instance == null){			
			instance = new SubCategoryEntityFactory();
		}
		return instance;
	}

    protected EntityFactory getFactoryFor(String attributeName) {
        return null;
    }

}