/* $Id: MailSendThread.java,v 1.21 2008/09/03 15:43:25 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;

import org.apache.commons.logging.Log;

import de.tarent.commons.logging.LogFactory;
import de.tarent.groupware.mail.MailException.Database;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.request.TcRequest;

/**
 * A thread sending scheduled mails every 30 seconds.
 *   
 * @author Simon Bühler <simon@aktionspotential.de>
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.21 $
 */
public class MailSendThread extends Thread {
    
    private final static Log logger = LogFactory.getLog(MailSendThread.class);
    
	private int waitMiliSeconds = 1000 * 30; // 30 Sekunden
	private boolean keeprunning = true;
	private boolean isworking = false;
	private String module;
	private Map serverData;
	private TcAll all;
	
	String summarySubjectText = "Summary of email job #";

	public MailSendThread(String moduleName, Map sd) {
		super();
		module = moduleName;
		serverData = sd;
		
		HashMap request = new HashMap();
		all = new TcAll(request, TcRequest.REQUEST_TYPE_WEB );
		all.getRequestObject().setModule(module);
	}

	public void run() {
		while (keeprunning && !isworking) {
			isworking = true;
			logger.debug("=======================================================");
			logger.debug("[Mail-Sending-Thread] started.");
            logger.debug("=======================================================");
			    try {
			        sendMessage();
			    } catch (MailException.Database e) {
			        logger.error("failed sending Message: " + e.getMessage(), e);
			    } catch (Throwable e) {
			        logger.fatal("uncatched exception while sending a message: " + e.getMessage(), e);
			    }
            logger.debug("=======================================================");
			logger.debug("[Mail-Sending-Thread] stopped.");
            logger.debug("=======================================================");
			    try {
			        // thread parken
			        sleep(waitMiliSeconds);
			    } catch (InterruptedException e) {
			    }
			isworking = false;
		}
	}
	
	private void sendMessage() throws Database {
		// Liste von offenen Jobs holen
		List jobs = MailSendDB.getJobsToDo(module);
		for (int i = 0; i < jobs.size(); i++) {
			
			String senderEmailAddress = null;
            String subjectOfJob = null;
			int numberOfMessages = 0;
			int errors = 0;
			Integer jobid = (Integer)jobs.get(i);
			logger.debug("==============================");
			logger.info("[job]: " + jobid);
			logger.debug("==============================");
			
			MailSendDB.updateJobstatus(module, jobid, MailConstants.JOBSTATUS_WORKING);
			Session session = null;
			
            try {
                
                // obtain mail session
                Properties properties = System.getProperties();
                if (serverData.get("username") != null && serverData.get("password") != null) {
                    properties.put("mail.smtp.auth", "true");
                }
                session = Session.getDefaultInstance(properties);


                // get jobs
                List messageList = MailSendDB.getMessagesToJob(module, jobid);
                //Store email address of sender to send summary of this job later
                if (messageList != null && messageList.size() > 0){
                    senderEmailAddress = ((MailSendMessage)messageList.get(0)).getMailFrom();
                    subjectOfJob = ((MailSendMessage)messageList.get(0)).getMailSubject();
                    numberOfMessages = messageList.size();
                }
                        
                Iterator messages = messageList.iterator();
                
                while (messages.hasNext()) {
                    MailSendMessage entry = (MailSendMessage)messages.next();
                    logger.debug("==============================");
                    logger.info("[message]: " + entry.getId());
                    logger.debug("==============================");
				
                    try {
                        if (entry.getMailFrom() == null || entry.getMailFrom().equals("")) {
                            logger.error("Mail field 'From' must not be empty (message id ="+entry.getId()+")");
                            continue;
                        }
                        
                        if(entry.getMailTo() == null || entry.getMailTo().equals("")) {
                        	logger.error("Mail field 'To' must not be empty (message id ="+entry.getId()+")");
                            continue;
                        }
                        
                        
                        //
                        // eMail versenden
                        //
                        long start = System.currentTimeMillis();
                        Message message =
                            MailSendSMTP.buildMessage(session,
                                                      entry.getMailFrom(),
                                                      entry.getMailTo(),
                                                      entry.getMailCc(),
                                                      entry.getMailBcc(),
                                                      entry.getMailSubject(),
                                                      entry.getMailBody(),
                                                      entry.getMailAttachment(),
                                                      entry.getPriority(),
                                                      entry.getRequestAnswer());
                        logger.debug("==============================");
                        logger.debug("content loaded.");
                        logger.debug("sending...");
                        logger.debug("==============================");                        
                        MailSendSMTP.mail(session,
                                          (String)serverData.get("server"),
                                          (String)serverData.get("username"),
                                          (String)serverData.get("password"),
                                          message);
                        logger.debug("==============================");
                        logger.info("successfully sent. ("+ (System.currentTimeMillis()-start) +"ms)");
                        logger.debug("==============================");
                        MailSendDB.updateMessageStatus(module, entry.getId(), null, MailSendMessage.MAILSTATUS_SENT);
                        logger.debug("==============================");
                        logger.debug("message status updated.");
                        //
                        // Empf�nger suchen und ggf. Kontakthistoren Eintrag einf�gen
                        //

                        // TODO: Reimplement contact entries
                        //                         logger.info("checking recipients to add sent-history-entries:");
                        //                         logger.debug("==============================");
                        //                         Result tcResult = null;
                        //                         try {
                        //                             Iterator address = Arrays.asList(message.getRecipients(Message.RecipientType.TO)).iterator();
                        //                             if (address.hasNext()) {
                        //                                 WhereList where = Where.list().add(
                        //                                                                    Expr.equal(TcommDB.VALUE, MailHelper.getAddressMail((Address)address.next())));
                        //                                 while (address.hasNext()) {
                        //                                     where.addOr(Expr.equal(TcommDB.VALUE, MailHelper.getAddressMail((Address)address.next())));
                        //                                 }
                        //                                 tcResult = SQL.Select(MailSendDB.getDBContext(module))
                        //                                     .from(TcommDB.getTableName())
                        //                                     .select(TcommDB.FKADDRESS)
                        //                                     .where(where)
                        //                                     .executeSelect(MailSendDB.getDBContext(module));
                        //                                 ResultSet resultSet = tcResult.resultSet();
                        //                                 while (resultSet.next()) {
                        //                                     Integer addressid = new Integer(resultSet.getInt(1));
                        //                                     logger.debug("[contact]: addressid=" + addressid + ", type=email, bound=out, subject=" + entry.getMailSubject() + ", note=eMail versendet");
                        //                                     HashMap attributes = new HashMap();
                        //                                     attributes.put(ContactWorker.KEY_SUBJECT, entry.getMailSubject());
                        //                                     attributes.put(ContactWorker.KEY_NOTE, "eMail versendet");
                        //                                     logger.error("[!] entry for contact (id="+addressid+") not saved: not implemented!");
                        //	TODO fixen, hier sind ein paar Parameter zuviel
                        //								ContactWorker.createContact(
                        //										all,
                        //										addressid, // addressid
                        //										new Integer(entry.getUserId()), // userid
                        //										ContactWorker.CATEGORY_MAILING, // category
                        //										ContactWorker.CHANNEL_EMAIL, // channel
                        //										new Integer(0), // daration
                        //										new Integer(0), // linktype
                        //										Boolean.FALSE, // inbound
                        //										attributes, // attributes
                        //										new Long(System.currentTimeMillis())); // startdate
                        //}
                        //    }
                        //                         } catch (SQLException e) {
                        //                             logger.error("failed checking new recipients: " + e.getMessage(), e);
                        //                         } finally {
                        //                             if (tcResult != null)
                        //                                 tcResult.close();
                        //                         }
                        
                        //
                        // eMail im Sent-Folder des eMail-Stores eintragen
                        //

                        logger.debug("==============================");
                        logger.debug("updating sent-folder:");
                        logger.debug("==============================");
                        if (entry.getStoreId() > 0) {
                            Map storeInfo = MailDB.getStoreInfo(
                                                                new Integer(entry.getUserId()),
                                                                new Integer(entry.getStoreId()));
                            MailStore mailStore = new MailStore(storeInfo);
                            mailStore.connect();
                            logger.debug("==============================");
                            logger.debug("[message]: store=" + mailStore + ", folder=" + storeInfo.get(MailConstants.SERVER_FOLDERSENT));
                            logger.debug("==============================");
                            mailStore.addMessage(
                                                 (String)storeInfo.get(MailConstants.SERVER_FOLDERSENT),
                                                 message);
                            mailStore.disconnect();
                            MailSendDB.updateMessageStatus(module, entry.getId(), null, MailSendMessage.MAILSTATUS_MOVED);
                        }
					
                    } catch (AddressException e) {
                        errors++;
                        MailSendDB.updateMessageStatus(module, entry.getId(), e.getLocalizedMessage(), MailSendMessage.MAILSTATUS_ERROR);
                        logger.error("shouldn't occur (addresses had been preliminary filtered), cause:" + e.getLocalizedMessage(), e);
                    } catch (MessagingException e) {
                        errors++;
                        MailSendDB.updateMessageStatus(module, entry.getId(), e.getLocalizedMessage(), MailSendMessage.MAILSTATUS_ERROR);
                        logger.error("general error, cause: " + e.getLocalizedMessage(), e);
                    } catch (MailException e) {
                        errors++;
                        MailSendDB.updateMessageStatus(module, entry.getId(), e.getLocalizedMessage(), MailSendMessage.MAILSTATUS_ERROR);
                        logger.error("failed moving message to the IMAP folder, cause: " + e.getLocalizedMessage(), e);
                    } catch (Exception e) {
                        errors++;
                        MailSendDB.updateMessageStatus(module, entry.getId(), e.getLocalizedMessage(), MailSendMessage.MAILSTATUS_ERROR);
                        logger.error("uncaught exception: " + e.getLocalizedMessage(), e);
                    }
				       
                } //ENDE NACHRICHT
            } catch(Throwable e) {
                logger.error("failed loading messages for current job: " + e.getClass().getSimpleName() + ": " + e.getMessage(), e);
                errors++;
            }
            
            // send summary to sender email address
			String summaryMessageText = ""; 
			// Job Status in DB updaten
			if (errors != 0) {
				summaryMessageText = "Some problems occured during processing of job number " + jobid + ".\n";
				summaryMessageText += errors + " of " + numberOfMessages + " emails could not be sent.\n\n";
				
			    logger.debug("==============================");
			    logger.warn("#job-errors: " + errors);
			    logger.debug("==============================");
				MailSendDB.updateJobstatus(module, jobid, MailConstants.JOBSTATUS_ERROR);
			} else {
				summaryMessageText = "All " + numberOfMessages + " have been successfully sent.\n\n";		
				logger.debug("==============================");
			    logger.info("job successfully done.");
			    logger.debug("==============================");
				MailSendDB.updateJobstatus(module, jobid, MailConstants.JOBSTATUS_DONE);
			}
			
			String summaryEmailFrom = serverData.get("summaryEmailFrom") == null ? "report@emailserver"
					: serverData.get("summaryEmailFrom").toString();
			
			summaryMessageText += "This email has been generated automatically. Please do not reply on this email.";
			try {
			Message summaryMessage = MailSendSMTP.buildMessage(session,
                               summaryEmailFrom,
                               senderEmailAddress,
                               null,
                               null,
                               summarySubjectText + jobid +" "+subjectOfJob,
                               summaryMessageText,
                               null,
                               3,
                               false);

			logger.debug("==============================");
			logger.debug("sending summary to sender...");
			logger.debug("==============================");                        
	        
			MailSendSMTP.mail(session,
                   (String)serverData.get("server"),
                   (String)serverData.get("username"),
                   (String)serverData.get("password"),
                   summaryMessage);
			} catch (MessagingException e) {
				logger.error("failed sending summary message for current job " + jobid + ": " +  e.getClass().getSimpleName() + ": " + e.getMessage(), e);
				return;
			}
			
			logger.debug("==============================");
			logger.debug("successfully sent.");
			logger.debug("==============================");
			
		} //ENDE JOB
	}
	
	/**
	 * Setter f�r das Intervall in Sekunden, in denen der SMTPThread nachschaut, ob neues vorliegt.
	 * @return waitSeconds
	 */
	public int getWaitMiliSeconds() {
		return waitMiliSeconds;
	}

	/**
	 * Setter f�r das Intervall in Sekunden, in denen der SMTPThread nachschaut, ob neues vorliegt.
	 * @param i
	 */
	public void setWaitMiliSeconds(int i) {
		waitMiliSeconds = i;
	}

	/**
	 * @param b
	 */
	public void setKeepRunning(boolean b) {
		keeprunning = b;
	}

	/**
	 * Diese Funktion h�lt den SMTPThread an
	 */
	public void halt() {
		keeprunning = false;
		this.interrupt();
	}
}
