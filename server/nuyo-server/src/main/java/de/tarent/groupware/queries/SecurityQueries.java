/* $Id: SecurityQueries.java,v 1.14 2007/06/15 15:58:30 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.queries;

import java.util.Date;
import java.util.List;

import com.sun.jmx.snmp.Timestamp;

import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TscheduleDB;
import de.tarent.contact.bean.TuserscheduleDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.OptimizedInList;

/**
 * Diese Klasse generiert Queries f�r die Securityschicht
 *  
 * @author Philipp Kirchner, tarent GmbH
 */
public class SecurityQueries {
    //Object(Folder) Berechtigungen
    public static final int FOLDERRIGHT_READ = 1;
    public static final int FOLDERRIGHT_EDIT = 2;
    public static final int FOLDERRIGHT_ADD = 3;
    public static final int FOLDERRIGHT_REMOVE = 4;
    public static final int FOLDERRIGHT_STRUCTURE = 5;
    public static final int FOLDERRIGHT_ADDSUB = 6;
    public static final int FOLDERRIGHT_REMOVESUB = 7;
    public static final int FOLDERRIGHT_GRANT = 8;

	public static final String[] folderright_columns = {"auth_read", "auth_edit", "auth_add", "auth_remove", "auth_structure", "auth_addsub","auth_removesub", "auth_grant" }; 
    
    //Globale Berechtigungen    
    public static final int GLOBALRIGHT_EDITUSER 		= 8;
    public static final int GLOBALRIGHT_REMOVEUSER 		= 9;
    public static final int GLOBALRIGHT_EDITFOLDER 		= 10;
    public static final int GLOBALRIGHT_REMOVEFOLDER 	= 11;
    public static final int GLOBALRIGHT_EDITSCHEDULE 	= 12;
    public static final int GLOBALRIGHT_REMOVESCHEDULE 	= 13;
    public static final int GLOBALRIGHT_DELETE 			= 14;
	public static final String[] globalright_columns = { "auth_edituser", "auth_removeuser", "auth_editfolder", "auth_removefolder", "auth_editschedule", "auth_removeschedule", "auth_delete"};
	
	//Kalenderbrechtigungen
	public static final int SCHEDULERIGHT_RELEASE 		= 15;
	public static final int SCHEDULERIGHT_READ			= 16;
	public static final int SCHEDULERIGHT_WRITE			= 17;
	/**
	 * Generiert eine Query zur Abfrage, ob ein User ein bestimmtes Recht hat oder nicht.
	 * 
	 * @param userlogin Loginname des Users
	 * @param folderID ID des Folders
	 * @param folderRight ID des Folder-Rechts @see SecurityQueries#folderright_columns
	 * @return Query, der im Result nur die Spalte "right" f�hrt, 1 wenn Recht besteht, 0 sonst
	 * @deprecated Bitte aus Performance-Gr�nden �ber die ID des Users gehen
	 */
	public static String getUserRightOnFolderQuery(String userlogin, Integer folderID, Integer folderRight){
		String sql = 
			  "SELECT COALESCE(MAX(tfolderrole." + getFolderRight(folderRight) + "), 0) as \"right\" "
			+ "FROM " + TcDBContext.getSchemaName() + "tfolderrole, " + TcDBContext.getSchemaName() + "tgroup_folder, " + TcDBContext.getSchemaName() + "tgroup_user, " + TcDBContext.getSchemaName() + "tuser "
			+ "WHERE tgroup_folder.fk_folderrole = tfolderrole.pk "
			+ "AND tgroup_folder.fk_folder = '" + folderID + "' "
			+ "AND tgroup_user.fk_group = tgroup_folder.fk_group "
			+ "AND tgroup_user.fk_user = tuser.pk_user "
			+ "AND tuser.loginname = '" + userlogin + "'";
		return sql;
	}

	/**
	 * Generiert eine Query zur Abfrage, ob ein User ein bestimmtes Recht auf einer Menge von Adressen hat oder nicht.
	 * 
	 * @param userid ID des Benutzers
	 * @param addressIDs ID des Folders
	 * @return Query, der im Result nur die Spalte "right" f�hrt, 1 wenn Recht besteht, 0 sonst
	 * @deprecated Bitte aus Performance-Gr�nden �ber die ID des Users gehen
	 */
	public static String hasUserRightToReadAddressesQuery(Integer userid, List<Integer[]> addressIDs){
		
        OptimizedInList inList = new OptimizedInList("v_user_address.fk_address",addressIDs.toArray(new Integer[0]));
        return (new StringBuffer())
			.append("SELECT DISTINCT taddress.pk_address AS \"taddress.pk_address\" FROM " + TcDBContext.getSchemaName() + "taddress") 
			.append(" JOIN " + TcDBContext.getSchemaName() + "v_user_address ON (v_user_address.fk_address=taddress.pk_address)")
			.append(" WHERE (")
            .append(inList.getOptimezedClause())
            .append(") AND v_user_address.userid = " + userid)
            .toString();
	}
	
	
	/**
	 * Generiert eine Query zur Abfrage, ob ein User ein bestimmtes Recht hat oder nicht.
	 * 
	 * @param userlogin Loginname des Users
	 * @param addressID ID des Folders
	 * @param folderRight ID des Folder-Rechts @see SecurityQueries#folderright_columns
	 * @return Query, der im Result nur die Spalte "right" f�hrt, 1 wenn Recht besteht, 0 sonst
	 * @deprecated Bitte aus Performance-Gr�nden �ber die ID des Users gehen
	 */
	public static String getUserRightOnAddressQuery(String userlogin, Integer addressID, Integer folderRight){
		String sql = 
			  "SELECT COALESCE(MAX(tfolderrole." + getFolderRight(folderRight) + "), 0) as \"right\" "
			+ "FROM " + TcDBContext.getSchemaName() + "tfolderrole, " + TcDBContext.getSchemaName() + "tgroup_folder, " + TcDBContext.getSchemaName() + "tgroup_user, " + TcDBContext.getSchemaName() + "tuser, " + TcDBContext.getSchemaName() + "taddressfolder "
			+ "WHERE tgroup_folder.fk_folderrole = tfolderrole.pk "
			+ "AND taddressfolder.fk_address = '" + addressID + "' "
			+ "AND tgroup_folder.fk_folder = taddressfolder.fk_folder "
			+ "AND tgroup_user.fk_group = tgroup_folder.fk_group "
			+ "AND tgroup_user.fk_user = tuser.pk_user "
			+ "AND tuser.loginname = '" + userlogin + "'";
		return sql;
	}

	/**
	 * Generiert eine Query zur Abfrage, ob ein User ein bestimmtes Recht hat oder nicht.
	 * 
	 * @param userID ID des Users
	 * @param folderID ID des Folders
	 * @param folderRight ID des Folder-Rechts @see SecurityQueries#folderright_columns
	 * @return Query, der im Result nur die Spalte "right" f�hrt, 1 wenn Recht besteht, 0 sonst
	 */
	public static String getUserRightOnFolderQuery(Integer userID, Integer folderID, Integer folderRight){
		String sql = 
			  "SELECT COALESCE(MAX(tfolderrole." + getFolderRight(folderRight) + "), 0) as \"right\" "
			+ "FROM " + TcDBContext.getSchemaName() + "tfolderrole, " + TcDBContext.getSchemaName() + "tgroup_folder, " + TcDBContext.getSchemaName() + "tgroup_user, " + TcDBContext.getSchemaName() + "tuser "
			+ "WHERE tgroup_folder.fk_folderrole = tfolderrole.pk "
			+ "AND tgroup_folder.fk_folder = '" + folderID + "' "
			+ "AND tgroup_user.fk_group = tgroup_folder.fk_group "
			+ "AND tgroup_user.fk_user = " + userID;
		return sql;
	}

	private static String getFolderRight(Integer folderRight) {
		return folderright_columns[folderRight.intValue()-1];
	}

	/**
	 * Gibt zu einer globalen Rechte-ID den Namen der korrespondierenden Spalte zur�ck.
	 * @param globalRight ID des globalen Rechts
	 * @return String mit Spaltenname
	 */
	private static String getGlobalRight(Integer globalRight) {
		return globalright_columns[globalRight.intValue()-8];
	}

	/**
	 * Generiert eine Query zur Abfrage, ob ein User ein bestimmtes Recht hat oder nicht.
	 * 
	 * @param userLogin Loginname des Users
	 * @param globalRight ID des globalen Rechts @see SecurityQueries#globalright_columns
	 * @return Query, der im Result nur die Spalte "right" f�hrt, 1 wenn Recht besteht, 0 sonst
	 * @deprecated Bitte aus Performance-Gr�nden �ber die ID des Users gehen
	 */
	public static String getUserGlobalRightQuery(String userLogin, Integer globalRight) {
		
		
		/*
		String sql =
			"SELECT tglobalrole."  + getGlobalRight(globalRight) + " AS \"right\"" + " FROM " + TglobalroleDB.getTableName() + " " +  
			"JOIN " + TgroupDB.getTableName() + " ON (" + TgroupDB.FKGLOBALROLE + "=" + TglobalroleDB.PK_PK + ") " +
			"JOIN " + TgroupUserDB.getTableName() + " ON (" + TgroupDB.PK_PK + "=" + TgroupUserDB.FKGROUP + ") " +
			"JOIN " + TuserDB.getTableName() + " ON (" + TgroupDB.FKUSERPRIVATE + "=" + TuserDB.PK_PKUSER + ") " +			
			"WHERE " + TuserDB.LOGINNAME + " = '" + userLogin + "'";
			*/
		
		String sql = 
			  "SELECT COALESCE(MAX(tglobalrole." + getGlobalRight(globalRight) + "), 0) as \"right\" "
			+ "FROM " + TcDBContext.getSchemaName() + "tglobalrole, " + TcDBContext.getSchemaName() + "tgroup, " + TcDBContext.getSchemaName() + "tgroup_user, " + TcDBContext.getSchemaName() + "tuser "
			+ "WHERE tgroup.fk_globalrole = tglobalrole.pk "
			+ "AND tgroup.pk = tgroup_user.fk_group "
			+ "AND tgroup_user.fk_user = tuser.pk_user "
			+ "AND tuser.loginname = '" + userLogin + "'";			
			
		
		return sql;
	}
	
	/**
	 * Generiert eine Query zur Abfrage, ob ein User ein bestimmtes Recht hat oder nicht.
	 * 
	 * @param userID ID des Users
	 * @param globalRight ID des globalen Rechts @see SecurityQueries#globalright_columns
	 * @return Query, der im Result nur die Spalte "right" f�hrt, 1 wenn Recht besteht, 0 sonst
	 */
	public static String getUserGlobalRightQuery(Integer userID, Integer globalRight) {
		
		/*
		String sql =
			"SELECT tglobalrole."  + getGlobalRight(globalRight) + " AS right" + " FROM " + TglobalroleDB.getTableName() + " " +  
			"JOIN " + TgroupDB.getTableName() + " ON (" + TgroupDB.FKGLOBALROLE + "=" + TglobalroleDB.PK_PK + ") " +
			"JOIN " + TgroupUserDB.getTableName() + " ON (" + TgroupDB.PK_PK + "=" + TgroupUserDB.FKGROUP + ") " +
			"WHERE " + TgroupUserDB.FKUSER + "=" + userID;
		*/		
		 
		String sql = 
			  "SELECT COALESCE(MAX(tglobalrole." + getGlobalRight(globalRight) + "), 0) as \"right\" "
			+ "FROM " + TcDBContext.getSchemaName() + "tglobalrole, " + TcDBContext.getSchemaName() + "tgroup, " + TcDBContext.getSchemaName() + "tgroup_user, " + TcDBContext.getSchemaName() + "tuser "
			+ "WHERE tgroup.fk_globalrole = tglobalrole.pk "
			+ "AND tgroup.pk = tgroup_user.fk_group "
			+ "AND tgroup_user.fk_user = " + userID;
		
		return sql;	
			
	}
	
	/**
	 * Generiert eine Anfrage, die ermittelt, ob ein User ein bestimmtes Recht
	 * auf einem Kalender hat. Dabei wird zun�chst gepr�ft, ob der user der Besitzer des Kalenders ist
	 * 
	 * @param userID ID des anfragenden Users
	 * @param scheduleid ID des Kalenders
	 * @param scheduleright Abgefragtes Recht - nur Freigabe oder Freigabe + Lese- bzw. Schreibrecht
	 * @param now Date mit jetziger zeit
	 */
	
	public static String getUserHasRightOnScheduleQuery(Integer userID, Integer scheduleid, Integer scheduleright, Long start, Long end) {
		String nowString = SQL.format(TcDBContext.getDefaultContext(),new Date());
		String startString = SQL.format(TcDBContext.getDefaultContext(),new Timestamp(start));
		String endString = SQL.format(TcDBContext.getDefaultContext(),new Timestamp(end));
        
		String sql = 
			"SELECT " + TuserscheduleDB.PK_PK +" FROM " + TuserscheduleDB.getTableName()
            + " WHERE " + TuserscheduleDB.FKSCHEDULE + "=" + scheduleid 
            + " AND "+ TuserscheduleDB.FKUSER +"="+ userID
            + " AND ( "+ TuserscheduleDB.BEGINSCHEDULE +" IS NULL OR "+ TuserscheduleDB.BEGINSCHEDULE + "<=" + nowString +")"
            + " AND ( "+ TuserscheduleDB.ENDSCHEDULE +" IS NULL OR "+ TuserscheduleDB.ENDSCHEDULE +">="+ nowString + ")";
        
        if (scheduleright.intValue() == SCHEDULERIGHT_RELEASE){
            // DO NOTHING .. wenn nur auf Freigabe getestet wird, muss kein weiteres Zeitinterval eingehalten werden
        }
        else if (scheduleright.intValue() == SCHEDULERIGHT_READ){
            sql += 	
                " AND (" + TuserscheduleDB.BEGINREAD +" IS NULL OR "+ TuserscheduleDB.BEGINREAD +"<=" + startString +")"
                +" AND (" + TuserscheduleDB.ENDREAD +" IS NULL OR "+ TuserscheduleDB.ENDREAD + ">=" + endString +")";
        }
        else if (scheduleright.intValue() == SCHEDULERIGHT_WRITE){
            sql += 	
                " AND (" + TuserscheduleDB.BEGINWRITE +" IS NULL OR "+ TuserscheduleDB.BEGINWRITE +"<=" + startString +")"
                +" AND (" + TuserscheduleDB.ENDWRITE +" IS NULL OR "+ TuserscheduleDB.ENDWRITE + ">=" + endString +")";
        }

		return sql;
	}

	public static Select getCategoryRights() {
		return SQL.Select(TcDBContext.getDefaultContext()).from(TgroupCategoryDB.getTableName()).select(TgroupCategoryDB.ALL);
	}

	public static Delete deleteCategoryRight(Integer id) {
		return SQL.Delete(TcDBContext.getDefaultContext()).from(TgroupCategoryDB.getTableName()).where(Expr.equal(TgroupCategoryDB.PK_PK, id));
	}


	/**
	 * @param userid
	 * @param scheduleid
	 * @return String
	 */
	public static String getIsUserOwnerOfScheduleQuery(Integer userid, Integer scheduleid) {
		
		String sql =
			"SELECT " + TscheduleDB.PK_PK + " AS \"" + TscheduleDB.PK_PK + "\" FROM " + TscheduleDB.getTableName() + " " +  
			"JOIN " + TuserscheduleDB.getTableName() + " ON (" + TuserscheduleDB.FKSCHEDULE + "=" + TscheduleDB.PK_PK + ") " +
			"WHERE " + TscheduleDB.PK_PK + "=" + scheduleid + " AND (" + TuserscheduleDB.FKUSEROWNER + "=" + userid + " OR " + 
			 TscheduleDB.FKUSER + "=" + userid + " )";
		return sql;
	}

}