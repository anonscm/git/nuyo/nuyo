package de.tarent.groupware.category;

import de.tarent.commons.datahandling.entity.AttributeSource;
import de.tarent.commons.datahandling.entity.DefaultEntityFactory;
import de.tarent.commons.datahandling.entity.EntityFactory;
import de.tarent.commons.datahandling.entity.LookupContext;
import de.tarent.groupware.Category;
import de.tarent.groupware.SubCategory;
import de.tarent.groupware.impl.CategoryImpl;

/**
 *
 * @author Sebastian Mancke, tarent GmbH
 */

public class CategoryEntityFactory extends DefaultEntityFactory {


	private static final String entityTypeCategory = Category.class.getName();
	private static CategoryEntityFactory instance = null;
	
	protected CategoryEntityFactory(){
		super(CategoryImpl.class);
	}
	
	public static synchronized CategoryEntityFactory getInstance() {
		if(instance == null){			
			instance = new CategoryEntityFactory();
		}
		return instance;
	}
	
	protected EntityFactory getFactoryFor(String attributeName) {
        if (attributeName.equals(SubCategory.entityName))
        		return SubCategoryEntityFactory.getInstance();
        return null;
    }
    
    /**
     * Template method for returning an Entity from the LookupContext. Default implementation allways returns null.
     */
    public Object getEntityFromLookupContext(AttributeSource as, LookupContext lc) {
       return lc.getEntity(as.getAttribute(Category.PROPERTY_ID.getKey()),  entityTypeCategory);
    }

    /**
     * Default implementation for template method. This implementaoin does not store the entity.
     */
    public void storeEntityInLookupContext(Object entity, LookupContext lc) {
    		lc.registerEntity(new Integer(((Category)entity).getId()), entityTypeCategory, entity);
    }

}