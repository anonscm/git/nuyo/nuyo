package de.tarent.groupware.serverStatus;

import java.io.File;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.jar.Manifest;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.tarent.commons.utils.StringTools;
import de.tarent.commons.utils.VersionTool;
import de.tarent.contact.bean.TparamDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.octopus.server.OctopusContext;

public class ServerStatusWorker {
	
	private static final Log logger = LogFactory.getLog(ServerStatusWorker.class);   
	
	 
    /**
      * reads the build-versions from the jarfiles and returns it as list
      */
    
    public static final String[] INPUT_GETALLVERSIONS = {};
    public static final boolean[] MANDATORY_GETALLVERSIONS = {};
    public static final String OUTPUT_GETALLVERSIONS = "versions";
    
    public Map getAllVersions(OctopusContext oc){
    	VersionTool vt = new VersionTool();
    	
    	vt.search(new String[]{
    			//oc.moduleRootPath().getParentFile().getAbsolutePath(),
                new File(new File(oc.moduleRootPath().getParentFile(), "WEB-INF"),"lib").getAbsolutePath()
            });
    	
    	List list = vt.getVersionInfos();
    	Map versionMap = new HashMap();
    	
    	// to submit the version list via xml, we have to tranlate everything to strings
    	// during this, we extract the valid information from the entries and put it into a map
    	
    	for (Iterator iter = list.iterator(); iter.hasNext();){
    		String entry = iter.next().toString();
    		int begin = entry.indexOf(".jar:") + 6;
    		int end = entry.indexOf(',');
    		String key = entry.substring(begin, end);
    		String value = entry.substring(end + 2);
    		
    		if (!value.equals("null"))
    			versionMap.put(key, value);
    	}
    	
    	return versionMap;
    }
    
    
    public static final String[] INPUT_getServerVersion = {};
    public static final boolean[] MANDATORY_getServerVersion = {};
    public static final String OUTPUT_getServerVersion = "version";
    
    public String getServerVersion(OctopusContext oc){
    	Map allversions = getAllVersions(oc);
    	
    	if (allversions.containsKey("contact-server"))
    		return allversions.get("contact-server").toString();
    	
    	return "No version for contact-server found";
    }
    	
  
    	
    public static final String[] INPUT_testUser = {"un", "pw"};
    public static final boolean[] MANDATORY_testUser = {false, false};
   
    		
    public void testUser(OctopusContext oc, String username, String password){
    	
    	Select select1 = SQL.Select(TcDBContext.getDefaultContext())
        .from(TuserDB.getTableName())
        .add(TuserDB.PK_PKUSER, Integer.class)
        .add(TuserDB.ISPWDENCRYPTED, Integer.class)
        .where(Expr.equal(TuserDB.LOGINNAME, username));
    	
    	try {
			ResultSet result1 = select1.executeSelect(TcDBContext.getDefaultContext()).resultSet();
			
			int pwEncrypted;
			
			if (result1.next()){
				pwEncrypted = result1.getInt(2);
			}
			else return;
			
			Select select2 = SQL.Select(TcDBContext.getDefaultContext())
			.from(TuserDB.getTableName())
			.add(TuserDB.PK_PKUSER, Integer.class)
			.where(
				Where.and(
					Expr.equal(TuserDB.LOGINNAME, username),
					Expr.equal(TuserDB.PWD, pwEncrypted == 1 ? StringTools.md5(password): password)
				)
			);
			
			ResultSet result2 = select2.getResultSet();
			
			if (result2.next())
				oc.setContent("testUserAnswer", "User " + username + " authentifiziert.");
		} catch (NoSuchAlgorithmException e) {
			logger.warn(e);
		} catch (SQLException e) {
			logger.warn(e);
		}
    }
   
	public static final String[] INPUT_initStatusPage = {};
    public static final boolean[] MANDATORY_initStatusPage = {};
    
    public void initStatusPage(OctopusContext oc){
    	
    	boolean connectedToDatabase = false;
    	
    	try {
			Connection con = DB.getConnection(TcDBContext.getDefaultContext());
			if (con != null && !con.isClosed())
				connectedToDatabase = true;
			
		} catch (SQLException e) {
			logger.warn(e);
			connectedToDatabase = false;
		}
		
        
		//databasename: 
    	String databaseName = ((de.tarent.dblayer.engine.DBPool)TcDBContext.getDefaultContext().getPool()).getProperty("databaseName");
		String databaseIP = ((de.tarent.dblayer.engine.DBPool)TcDBContext.getDefaultContext().getPool()).getProperty("serverName");
		String schemaVersion = "no schema found";
		try {
			String schemaname = TcDBContext.getSchemaNameWithoutDot();
			
			List resultList = SQL.Select(TcDBContext.getDefaultContext())
			.add(TparamDB.PARAMVALUE, String.class)
			.from(TparamDB.getTableName())
			.where(Expr.equal(TparamDB.PARAMNAME, "COLIBRI_SCHEMA_VERSION"))
			.getList(TcDBContext.getDefaultContext());
			
			if (resultList.size() > 0 && resultList.get(0) instanceof String ) {
                schemaVersion = schemaname + "(" + (String) resultList.get(0) + ")";                
            }
			
		} catch (SQLException e) {
			logger.warn(e);
		}
		
		Map serverData = new HashMap();
		serverData.put("server", oc.getConfigObject().getModuleConfig().getParam("mail_SMTPServer"));
		serverData.put("username",  oc.getConfigObject().getModuleConfig().getParam("mail_SMTPUsername") );
		serverData.put("password",  oc.getConfigObject().getModuleConfig().getParam("mail_SMTPPassword") );
		
		boolean smtpReachable = false;
		
		Properties properties = System.getProperties();
        if ( serverData.get("username") != null && serverData.get("password") != null) {
            properties.put("mail.smtp.auth", "true");
        }
        Session session = Session.getDefaultInstance(properties);
        
        try {
    		Transport transport = session.getTransport("smtp");
			transport.connect((String)serverData.get("server"), (String)serverData.get("username"), (String)serverData.get("password"));
		
			if (transport.isConnected()){
				smtpReachable = true;
				transport.close();
			}
		} catch (Exception e) {
			logger.warn(e);
		}
		
		serverData.put("connected", smtpReachable ? "connceted" : "not connected");
		
		// set content variables
		oc.setContent("emailserver", serverData);
		oc.setContent("dbconnected", connectedToDatabase? "connected" : "not connected");
		oc.setContent("dbname", databaseName);
    	oc.setContent("dbip", databaseIP);
    	oc.setContent("dbschemaversion", schemaVersion);
    	
    }

}
