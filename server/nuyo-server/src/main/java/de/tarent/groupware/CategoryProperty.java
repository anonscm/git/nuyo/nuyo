package de.tarent.groupware;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.tarent.commons.datahandling.entity.EntityProperty;

/**
 * <code>CategoryProperty</code> provides type-safe keys for the category data
 * fields and allows indexed access to the property names as well as i18n.
 * 
 * <p>
 * Additionally <code>CategoryProperty</code> can be in zero to many lists of
 * properties. This allows one to receive only a set of properties that are
 * interesting for a certain purpose (e.g. all properties which can be searched
 * for).
 * </p>
 * 
 * @author Nils Neumaier, tarent GmbH Bonn
 */
public class CategoryProperty extends EntityProperty {
	
	static
	{
		// HACK: This hack provokes the Category class to be initialized
		// whenever the CategoryProperty class is touched. This behavior
		// is needed because Category' initialization causes static lists
		// in CategoryProperty to be filled. Without Category being initialized
		// those lists would be empty. 
		Category.class.getName();
	}


	private static ResourceBundle currentResourceBundle = PropertyResourceBundle
			.getBundle("de.tarent.groupware.categoryproperty");

	/**
	 * Java class denoting the type of the category property.
	 */
	private Class dataType;

	/**
	 * Creates an <code>CategoryProperty</code> which belongs only to the given
	 * lists.
	 * 
	 * @param name
	 * @param fields
	 *            bitfield denoting the list affiliation
	 * @return
	 */
	static CategoryProperty make(String name) {
		return new CategoryProperty(name);
	}

	
	private Class retrieveDataType() {
	
		try {
			PropertyDescriptor descs[] = Introspector
					.getBeanInfo(Category.class).getPropertyDescriptors();

			for (int i = 0; i < descs.length; i++) {
				if (descs[i].getName().equals(key))
					return descs[i].getPropertyType();
			}

			throw new IllegalStateException(
					"Category property is not a bean property of Category class");
		} catch (IntrospectionException ie) {
			throw new IllegalStateException(
					"Unable to introspect Category class");
		}

	}

	private CategoryProperty(String name) {
		super(name);
		dataType = retrieveDataType();
	}

	
	/**
	 * Returns a human-readable localized label of the property.
	 * 
	 * <p>
	 * In case a property cannot be found the method returns the property's key
	 * quoted in exclamation marks ("!").
	 * </p>
	 * 
	 * <p>
	 * In case a property's label is empty the method returns the property's key
	 * quoted in question marks ("?").
	 * </p>
	 * 
	 */
	public String getLabel() {
		try {
			String res = currentResourceBundle.getString(Category.propertyNamePrefix + key);

			if (res.trim().length() == 0)
				return "?" + key + "?";

			return res;

		} catch (MissingResourceException e) {
			String bundleSuffix = "";
			if (!"".equals(currentResourceBundle.getLocale().getLanguage())) {
				bundleSuffix += "_"
						+ currentResourceBundle.getLocale().getLanguage();
			}

			if (!"".equals(currentResourceBundle.getLocale().getCountry())) {
				bundleSuffix += "_"
						+ currentResourceBundle.getLocale().getCountry();
			}
			return "!" + key + "!";
		}
	}

	/**
	 * Returns the property'y datatype.
	 * 
	 * @return
	 */
	public Class getDataType() {
		return dataType;
	}

}
