package de.tarent.groupware.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author kleinw
 *
 *	Hiermit k�nnen Templates gelesen und
 *	mit dynamischen Inhalten versehen werden.
 *
 */
public class Template {

    private String _template;
    
    public Template(File textFile) throws TemplateException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(textFile));
        } catch (FileNotFoundException e) {
            throw new TemplateException("Die angegebene Datei " + textFile + " konnte nicht gefunden werden.");
        }
        StringBuffer sb = new StringBuffer();
        try {
            String line = reader.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = reader.readLine();
            }
            reader.close();
            
            _template = sb.toString();
            
        } catch (IOException e1) {
            throw new TemplateException("Fehler beim Einlesen der Datei : " + textFile);
        }

        String content = sb.toString();
        
    }

    /**
     * @param replacements A collection of String[] {key, value}
     */
    public String evaluateTemplate(Collection replacements) {
        String result = _template;
        for (Iterator it = replacements.iterator();it.hasNext();) {
            String[] replacement = (String[])it.next();
            if (replacement[1] != null)
                result = result.replaceAll(replacement[0], replacement[1]);
        }
        return result;
    }
    
    static public class TemplateException extends Exception {
        public TemplateException(String msg) {
            super(msg);
        }
    }
    
}

    
