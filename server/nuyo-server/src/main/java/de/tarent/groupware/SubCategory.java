/* $Id: SubCategory.java,v 1.3 2007/06/15 15:58:29 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.groupware.mail.MailException.Database;

/**
 * This is the basic interface for an Category.
 *
 * For each property of the Category a constant should be defined.
 *
 * @author Nils Neumaier, tarent GmbH
 */
public interface SubCategory extends Entity {

	public static final String entityName = "SubCategory";
	
	public static final String propertyNamePrefix = "SUBCAT_PROP_";  
	
    /**
     * Fills this Category with all fields of the supplied source Category. The id-field will not be modified.
     */
    public void fill(SubCategory sourceSubCategory);
	
	
	

    // generic methods for setting and retrieving properties

	public Object getStandardData(String id);

    public Object getStandardData(SubCategoryProperty prop);
	
	/**
	 * Diese Methode liefert Standarddaten zur bergebenen Identifikation.
	 * 
	 * @param id Identifikator, vergleich hier zu auch
	 *  {@link Database#getExtendedFields() Database.getExtendedFields()}.
	 * @param newValue die neuen Standarddaten.
	 */
	public void setStandardData(String id, Object newValue);
	
	public void setStandardData(SubCategoryProperty prop, Object newValue);
        

	/**
	 * this method is necessary for the implemented interface Entity
	 */
	public Object getAttribute(String id);
	

	/**
	 * this method is necessary for the implemented interface Entity
	 * @param id
	 * @param newValue
	 * @throws ContactDBException
	 */
	public void setAttribute(String id, Object newValue) throws EntityException;



	public final static SubCategoryProperty PROPERTY_ID = SubCategoryProperty.make("id");
	/**
	 * This method gets the id of this subcategory 
	 */
    public int getId();	

    /**
     * Sets the id of the object
     */
    public void setId(int newid);
    
    
	
	public final static SubCategoryProperty PROPERTY_SUBCATEGORYNAME = SubCategoryProperty.make("subCategoryName");
	/**
	 * Returns the name of the subcategory.
	 * @return String
	 */
	public String getSubCategoryName();
	
	/**
	 * Sets the name of the subcategory.
	 * @param vorname The name to set
	 */
	public void setSubCategoryName(String subcategoryname);




	public final static SubCategoryProperty PROPERTY_DESCRIPTION = SubCategoryProperty.make("description");
	/**
	 * @return the description
	 */
	public String getDescription();
	
	/**
	 * @param description the description of the subcategory
	 */
	public void setDescription(String description);	
	
	
	
	public final static SubCategoryProperty PROPERTY_PARENTCATEGORY = SubCategoryProperty.make("parentCategory");
	/**
	 * @return the parent category
	 */
	public Integer getParentCategory();
	
	/**
	 * @param parent the parent category of the subcategory
	 */
	public void setParentCategory(Integer parent);	
	
	
}  
