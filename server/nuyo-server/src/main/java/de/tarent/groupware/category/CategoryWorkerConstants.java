package de.tarent.groupware.category;

import java.util.Map;

import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.groupware.utils.EntityDescription;
import de.tarent.groupware.utils.EntityDescriptionAdapter;

/**
 * @author kleinw
 *
 */
public interface CategoryWorkerConstants {

//    /** Name der Kategorie */
//    static public final String KEY_NAME = "name";
//    /**	Beschreibung der Kategorie */
//    static public final String KEY_DESCRIPTION = "description";


    public static EntityDescription categoryEntityDescription = new EntityDescriptionAdapter() {
            public String getEntityName() {
                return "category";
            }
            protected void addPropertyToDBKeyMappings(Map mapping) {
                mapping.put("id", TcategoryDB.PK_PKCATEGORY);
                mapping.put("name", TcategoryDB.CATEGORYNAME);
                mapping.put("description", TcategoryDB.DESCRIPTION);
                mapping.put("type", TcategoryDB.FKCATEGORYTYPE);
                mapping.put("virtual", "tfolder.isvirtual");               
            }
        };
    public static EntityDescription subCategoryEntityDescription = new EntityDescriptionAdapter() {
            public String getEntityName() {
                return "subcategory";
            }
            protected void addPropertyToDBKeyMappings(Map mapping) {
                mapping.put("id", TsubcategoryDB.PK_PKSUBCATEGORY);
                mapping.put("parentcategory", TsubcategoryDB.FKCATEGORY);
                mapping.put("name", TsubcategoryDB.CATEGORYNAME);
                mapping.put("description", TsubcategoryDB.DESCRIPTION);
            }
        };
        
}
