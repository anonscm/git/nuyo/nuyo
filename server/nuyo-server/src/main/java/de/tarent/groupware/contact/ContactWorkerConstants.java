package de.tarent.groupware.contact;


/**
 * @author kleinw
 *
 *	Sammlung von Konstanten.
 *
 */
public interface ContactWorkerConstants {

    public final static String KEY_SUBJECT = "subject";
    public final static String KEY_NOTE = "note";
    public final static String KEY_STRUCT_INFO = "structInfo";
    
    /** Kontaktkategorien: Gespr�chsnotiz, Terminanfrage, Postversand */
    public final static Integer CATEGORY_MEMO = new Integer(0);
    public final static Integer CATEGORY_REQUEST = new Integer(1);
    public final static Integer CATEGORY_MAILING = new Integer(2);
    
    /** Kontaktkan�le: E-Mail, Telefon, Post, Fax, pers�nlich */
    public final static Integer CHANNEL_NOT_ASSIGNED = new Integer(0);
    public final static Integer CHANNEL_EMAIL = new Integer(1);
    public final static Integer CHANNEL_PHONE = new Integer(2);
    public final static Integer CHANNEL_MAIL = new Integer(3);
    public final static Integer CHANNEL_FAX = new Integer(4);
    public final static Integer CHANNEL_DIRECT = new Integer(5);
    
    /** Linktypen: E-Mail, Termin, Aufgabe, Dokument */
    public final static Integer LINKTYPE_NULL = new Integer(0);
    public final static Integer LINKTYPE_EMAIL = new Integer(1);
    public final static Integer LINKTYPE_APPOINTMENT = new Integer(2);
    public final static Integer LINKTYPE_JOB = new Integer(3);
    public final static Integer LINKTYPE_DOCUMENT = new Integer(4);
    
    /** Direction */
    public final static Integer DIRECTION_OUTBOUND = new Integer(0);
    public final static Integer DIRECTION_INBOUND = new Integer(1);
    
}
