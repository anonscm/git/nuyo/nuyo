/* $Id: LocaleMessage.java,v 1.3 2006/09/11 08:43:07 kirchner Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.webclient.utils;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author christoph
 */
public final class LocaleMessage {
	private static final String BUNDLE_NAME = "de.tarent.groupware.webclient.messages";
	
	private final Locale resourceLocale;
	private final ResourceBundle resourceBundle;
	
	public LocaleMessage(Locale locale) {
		resourceLocale = locale;
		resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
	}

	public final Object get(String key) {
		try {
			return MessageFormat.format(resourceBundle.getString(key), (Object[])null);
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public final Object get(String key, Object arg0) {
		try {
			return MessageFormat.format(resourceBundle.getString(key), new Object[] { arg0 });
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public final Object get(String key, Object arg0, Object arg1) {
		try {
			return MessageFormat.format(resourceBundle.getString(key), new Object[] { arg0, arg1 });
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public final Object get(String key, Object arg0, Object arg1, Object arg2) {
		try {
			return MessageFormat.format(resourceBundle.getString(key), new Object[] { arg0, arg1, arg2 });
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public final Object get(String key, Object arg0, Object arg1, Object arg2, Object arg3) {
		try {
			return MessageFormat.format(resourceBundle.getString(key), new Object[] { arg0, arg1, arg2, arg3});
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public final Object get(String key, Object arg0, Object arg1, Object arg2, Object arg3, Object arg4) {
		try {
			return MessageFormat.format(resourceBundle.getString(key), new Object[] { arg0, arg1, arg2, arg3, arg4});
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public final Object date(String key, Object arg0) {
		try {
			Calendar calendar = Calendar.getInstance(resourceLocale);
			if (arg0 instanceof Long) {
				calendar.setTimeInMillis(((Long)arg0).longValue());
			} else {
				calendar.setTimeInMillis(new Long(arg0.toString()).longValue());
			}
			return new SimpleDateFormat(resourceBundle.getString(key), resourceLocale).format(calendar.getTime());
		} catch (MissingResourceException e) {
			return null;
		} catch (NumberFormatException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}
	}
}
