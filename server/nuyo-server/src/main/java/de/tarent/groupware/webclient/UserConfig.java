/* $Id: UserConfig.java,v 1.3 2006/09/15 13:00:03 kirchner Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 * Created on 30.05.2003
 */
package de.tarent.groupware.webclient;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.octopus.DBDataAccessPostgres;
import de.tarent.contact.octopus.DataAccess;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * Dieser Worker verwaltet Session-Daten f�r die contact-Webapplikation.
 * 
 * @author mikel
 */
public class UserConfig implements TcContentWorker {
	public final static Logger logger = Logger.getLogger(UserConfig.class.getName());

	public UserConfig() {
		super();
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
	}

	/**
	 * @see TcContentWorker#doAction(de.tarent.octopus.config.TcConfig, java.lang.String, de.tarent.octopus.request.TcRequest, de.tarent.octopus.content.TcContent)
	 */
	public String doAction(TcConfig tcConfig, String actionName, TcRequest tcRequest, TcContent tcContent) throws TcContentProzessException {
		String result = RESULT_error;

			DataAccess dataAccess = new DBDataAccessPostgres();

			if (ACTION_USER_MAIL_PROPERTIES.equals(actionName)) {
				// UserID besorgen
				Integer userId = (Integer) ((Map)tcConfig.getSessionValueAsObject(Controller.SESSION_USERDATA_NAME)).get("id");
				if (userId == null) {
					throw new TcContentProzessException("can not find user id");
				}

				/**
				 * serverList - Map mit allen Server Listen, z.B. In, Out, etc. ("In" => "1,2")
				 */
				Map serverList = dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_LIST);
				
				/**
				 * serverIn - String[] mit einer Liste ("1", "2") von Servern
				 */
				String[] serverIn = null;
				String[] serverOut = null;
				if (serverList != null) {
					if (serverList.containsKey(Controller.USERPARAM_MAIL_LIST_IN)) {
						serverIn = ((String)serverList.get(Controller.USERPARAM_MAIL_LIST_IN)).split(",");
					}
					if (serverList.containsKey(Controller.USERPARAM_MAIL_LIST_OUT)) {
						serverOut = ((String)serverList.get(Controller.USERPARAM_MAIL_LIST_OUT)).split(",");
					}
				}

				/**
				 * serverInMap - Map mit Server-Informationen ("1" => (Map)serverinfo)
				 */ 
				Map serverInMap = new HashMap();
				if (serverIn != null) {
					for (int i = 0; i < serverIn.length; i++) {
						serverInMap.put(serverIn[i], dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + serverIn[i]));
					}
				}

				/**
				 * serverOutMap - Map mit Server-Informationen ("1" => (Map)serverinfo)
				 */ 
				Map serverOutMap = new HashMap();
				if (serverOut != null) {
					for (int i = 0; i < serverOut.length; i++) {
						serverOutMap.put(serverOut[i], dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + serverOut[i]));
					}
				}

				String action = tcRequest.get(PARAM_USER_MAIL_ACTION);
				if (PARAM_USER_MAIL_ACTION_NEWSERVER.equals(action)) {
					/**
					 * PARAM_USER_MAIL_NEWSERVER - Neuen Server in den User-Properties speichern.
					 * 
					 * Gibt ID des neuen Server's als "mail.id" zur�ck,
					 * null wenn nicht gespeichert werden konnte.
					 * Bei einem Fehler wird zus�tzlich "mail.error" zur�ckgegeben.
					 */
					String type = tcRequest.get("mail.type");

					/**
					 * Erste freien "Platz" in der Server-Liste finden...
					 */
					int freeNode = 0;
					if (serverInMap != null) {
						while (serverInMap.containsKey(Integer.toString(freeNode))) {
							freeNode++;
						}
					}
					if (serverOutMap != null) {
						while (serverOutMap.containsKey(Integer.toString(freeNode))) {
							freeNode++;
						}
					}
					
					/**
					 * ...und dort den Server abspeichern. 
					 */
					logger.log(Level.FINE, "UserConfig: Add new mail-server ('" + freeNode + "')");

					String prefix = Controller.USERPARAM_MAIL_PREFIX + "." + Integer.toString(freeNode);
					dataAccess.setUserProperty(userId, prefix + ".name", tcRequest.get("mail.name"));
					dataAccess.setUserProperty(userId, prefix + ".protocol", tcRequest.get("mail.protocol"));
					dataAccess.setUserProperty(userId, prefix + ".server", tcRequest.get("mail.server"));
					dataAccess.setUserProperty(userId, prefix + ".port", tcRequest.get("mail.port"));
					dataAccess.setUserProperty(userId, prefix + ".username", tcRequest.get("mail.username"));
					dataAccess.setUserProperty(userId, prefix + ".password", tcRequest.get("mail.password"));

					dataAccess.setUserProperty(userId, prefix + ".fromname", tcRequest.get("mail.fromname"));
					dataAccess.setUserProperty(userId, prefix + ".fromaddress", tcRequest.get("mail.fromaddress"));
					dataAccess.setUserProperty(userId, prefix + ".fromorga", tcRequest.get("mail.fromorga"));
					
					/**
					 * Server in entsprechender Liste abspeichern und im Content nachtragen.
					 */
					if (type == null) {
					} else if (type.equals("store")) {
						String tmp = new String();
						if (serverIn != null) {
							for (int i = 0; i < serverIn.length; i++) {
								tmp += serverIn[i] + ",";
							}
						}
						tmp += Integer.toString(freeNode);
						dataAccess.setUserProperty(userId, Controller.USERPARAM_MAIL_LIST_IN, tmp);
						serverInMap.put(Integer.toString(freeNode), dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + Integer.toString(freeNode)));
						tcContent.setField("mail.id", Integer.toString(freeNode));
					} else if (type.equals("transfer")) {
						String tmp = new String();
						if (serverOut != null) {
							for (int i = 0; i < serverOut.length; i++) {
								tmp += serverOut[i] + ",";
							}
						}
						tmp += Integer.toString(freeNode);
						dataAccess.setUserProperty(userId, Controller.USERPARAM_MAIL_LIST_OUT, tmp);
						serverOutMap.put(Integer.toString(freeNode), dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + Integer.toString(freeNode)));
						tcContent.setField("mail.id", Integer.toString(freeNode));
					}

				} else if (PARAM_USER_MAIL_ACTION_EDITSERVER.equals(action)) {
					/**
					 * PARAM_USER_MAIL_EDITSERVER - Server-�nderungen in den User-Properties speichern.
					 * 
					 * Gibt ID des Server's als "mail.id" zur�ck,
					 * null wenn nicht gespeichert werden konnte.
					 * Bei einem Fehler wird zus�tzlich "mail.error" zur�ckgegeben.
					 */
					String id = tcRequest.get("mail.id");
					String type = tcRequest.get("mail.type");

					if (id != null && type != null) {
						/**
						 * Server abspeichern
						 */
						logger.log(Level.FINE, "UserConfig: Edit mail-server ('" + id + "')");
						
						String prefix = Controller.USERPARAM_MAIL_PREFIX + "." + id;
						dataAccess.setUserProperty(userId, prefix + ".protocol", tcRequest.get("mail.protocol"));
						dataAccess.setUserProperty(userId, prefix + ".protocol", tcRequest.get("mail.protocol"));
						dataAccess.setUserProperty(userId, prefix + ".server", tcRequest.get("mail.server"));
						dataAccess.setUserProperty(userId, prefix + ".port", tcRequest.get("mail.port"));
						dataAccess.setUserProperty(userId, prefix + ".username", tcRequest.get("mail.username"));
						dataAccess.setUserProperty(userId, prefix + ".password", tcRequest.get("mail.password"));

						dataAccess.setUserProperty(userId, prefix + ".fromname", tcRequest.get("mail.fromname"));
						dataAccess.setUserProperty(userId, prefix + ".fromaddress", tcRequest.get("mail.fromaddress"));
						dataAccess.setUserProperty(userId, prefix + ".fromorga", tcRequest.get("mail.fromorga"));

						/**
						 * F�r aktuellen Request den Server im Content nachtragen.
						 */
						try {
							if (type.equals("store")) {
								serverInMap.put(serverIn[Integer.parseInt(id)], dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + serverIn[Integer.parseInt(id)]));
								tcContent.setField("mail.id", id);
							} else if (type.equals("transfer")) {
								serverOutMap.put(serverOut[Integer.parseInt(id)], dataAccess.getUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + serverOut[Integer.parseInt(id)]));
								tcContent.setField("mail.id", id);
							}
						} catch (NumberFormatException ex) {
							logger.log(Level.WARNING, "UserConfig: can not parse '" + id + "' to int", ex);
						}

					} else {
						tcContent.setField("mail.error", "can not find 'mail.id'");
					}

				} else if (PARAM_USER_MAIL_ACTION_REMOVESERVER.equals(action)) {
					/**
					 * PARAM_USER_MAIL_ACTION_REMOVESERVER - L�scht einen Mail-Server.
					 * 
					 * "mail.id" gibt null zur�ck.
					 * Bei einem Fehler wird zus�tzlich "mail.error" zur�ckgegeben.
					 */
					String id = tcRequest.get("mail.id");
					if (id != null) {
						String tmp;
						
						if (serverIn != null) {
							tmp = new String();
							for (int i = 0; i < serverIn.length; i++) {
								if (!serverIn[i].equals(id)) {
									if (tmp.length() != 0)
									tmp += ",";
									tmp += serverIn[i];
								}
							}
							if (tmp.length() == 0) {
								tmp = null;
							}
							logger.log(Level.FINE, "UserConfig: Remove mail-server ('" + id + "') from in-list");
							dataAccess.setUserProperty(userId, Controller.USERPARAM_MAIL_LIST_IN, tmp);
						}

						if (serverOut != null) {
							tmp = new String();
							for (int i = 0; i < serverOut.length; i++) {
								if (!serverOut[i].equals(id)) {
									if (tmp.length() != 0)
									tmp += ",";
									tmp += serverOut[i];
								}
							}
							if (tmp.length() == 0) {
								tmp = null;
							}
							logger.log(Level.FINE, "UserConfig: Remove mail-server ('" + id + "') from out-list");
							dataAccess.setUserProperty(userId, Controller.USERPARAM_MAIL_LIST_OUT, tmp);
						}

						logger.log(Level.FINE, "UserConfig: Remove mail-server ('" + id + "')");
						dataAccess.removeUserProperties(userId, Controller.USERPARAM_MAIL_PREFIX + "." + id);

						/**
						 * F�r aktuellen Request den Server im Content nachtragen.
						 */
						serverInMap.remove(id);
						tcContent.setField("mail.id", (String)null);
						
					} else {
						tcContent.setField("mail.error", "can not find 'mail.id'");
					}
				}
				
				/**
				 * Daten in Content stellen	
				 */
				tcContent.setField("mail.serverIn", serverInMap);
				tcContent.setField("mail.serverOut", serverOutMap);
				
				result = RESULT_ok;
			}

		return result;
	}

	public final static String ACTION_USER_MAIL_PROPERTIES = "mailProperties";
	
	public final static String PARAM_USER_MAIL_ACTION = "mail.action";
	public final static String PARAM_USER_MAIL_ACTION_NEWSERVER = "newServer";
	public final static String PARAM_USER_MAIL_ACTION_EDITSERVER = "editServer";
	public final static String PARAM_USER_MAIL_ACTION_REMOVESERVER = "removeServer";

	public final static String RESULT_ok = "ok";
	public final static String RESULT_error = "error";

	/**
	 * @see TcContentWorker#getWorkerDefinition()
	 */
	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port = new TcPortDefinition("de.tarent.octopus.webclient.UserController", "");

		TcOperationDefinition operation = port.addOperation(ACTION_USER_MAIL_PROPERTIES, "Speichert Userdaten in der Datenbank.");
		operation.setInputMessage();
		operation.setOutputMessage();
		return port;
	}

	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.3 $") + " (" + CVS.getContent("$Date: 2006/09/15 13:00:03 $") + ')';
	}
}
