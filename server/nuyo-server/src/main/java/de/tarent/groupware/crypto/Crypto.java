/* $Id: Crypto.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.security.GeneralSecurityException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPUtil;

/**
 * @see #init()
 *
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public class Crypto {
	private Crypto() {
	}

	private static boolean init = false;
	public static final String PROVIDER = "BC";

	public static void init() {
		if (!init) {
			Security.addProvider(new BouncyCastleProvider());
			PGPUtil.setDefaultProvider(PROVIDER);
		}
	}

	public static PGPPublicKey assertKey(PGPPublicKey key) throws GeneralSecurityException {
		if (key == null)
			throw new GeneralSecurityException("Public-Key is null");
		return key;
	}

	public static PGPSecretKey assertKey(PGPSecretKey key) throws GeneralSecurityException {
		if (key == null)
			throw new GeneralSecurityException("Secret-Key is null");
		return key;
	}

	public static PGPPrivateKey assertKey(PGPPrivateKey key) throws GeneralSecurityException {
		if (key == null)
			throw new GeneralSecurityException("Private-Key is null");
		return key;
	}

	public static char[] assertPass(char[] pass) throws GeneralSecurityException {
		if (pass == null)
			throw new GeneralSecurityException("Passphrase is null");
		return pass;
	}

	public static void clearPass(char[] pass) {
		if (pass != null)
			for (int i = 0; i < pass.length; i++)
				pass[i] = 0;
	}
}
