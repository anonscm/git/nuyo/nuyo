/*
 * Created on 26.10.2004
 */
package de.tarent.groupware.mail.helper;

import java.util.Arrays;
import java.util.Collection;

import javax.mail.Message;
import javax.mail.MessagingException;

public class Header {
	private Header() {
	}

	public static String getHeader(Message message, String name) throws MessagingException {
		String header[] = message.getHeader(name);
		if (header != null && header.length != 0)
			return header[0];
		else
			return null;
	}

	public static String getMessageId(Message message) throws MessagingException {
		return getHeader(message, "Message-ID");
	}

	public static String getReplyToId(Message message) throws MessagingException {
		return getHeader(message, "In-Reply-To");
	}

	public static Collection getReferences(Message message) throws MessagingException {
		String ref = getHeader(message, "References");
		if (ref == null)
			return null;
		else
			return Arrays.asList(ref.split(" "));
	}
}
