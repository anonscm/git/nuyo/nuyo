package de.tarent.groupware.format;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tarent.groupware.format.vcard.VCardWorker;
import de.tarent.groupware.utils.GenericSelector;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.pdc.format.VCard;

public class FormatWorker {
    
    public static final String[] INPUT_GETADDRESSASVCARD = { GenericSelector.RESULT_ENTITY_MAPS, "vcard_version" };
    public static final boolean[] MANDATORY_GETADDRESSASVCARD = { true, false };
    public static final String OUTPUT_GETADDRESSASVCARD = "vcard";
        
    /**
     * 
     * @param all
     * @param listAddress
     * @return
     * @throws SQLException
     */
    public static final String getAddressAsVCard(OctopusContext all, List entityMaps, String vCardVersion) 
                throws SQLException {
        
        // vCard Version can be 2.1 or 3.0 as default
        if (!vCardVersion.equals(VCard.VERSION_2_1))
            vCardVersion = VCard.VERSION_3_0;
        
        StringBuffer output = new StringBuffer();
        
        for (Iterator iter = entityMaps.iterator(); iter.hasNext();) {
            Map entity = (Map) iter.next();
            output.append(VCardWorker.convertAddressMapToVCard(entity, vCardVersion));
        }
        
        return output.toString();
    }
}
