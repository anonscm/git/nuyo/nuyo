package de.tarent.groupware.usergroup;


/**
 * @author kleinw
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public interface UserGroupWorkerConstants {

    public final static String KEY_NAME = "name";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_GLOBALROLE = "globalrole";
    public final static String KEY_SHORTNAME = "shortname";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    
}
