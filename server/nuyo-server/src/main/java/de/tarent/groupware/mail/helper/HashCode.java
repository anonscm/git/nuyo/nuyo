/*
 * Created on 26.10.2004
 */
package de.tarent.groupware.mail.helper;

import javax.mail.Message;
import javax.mail.MessagingException;

public class HashCode {
	private HashCode() {
	}
	
	/**
	 * Generiert aus den Informationen subject und size
	 * einer eMail einen eindeutigen Hash-Code.
	 * 
	 * @throws MessagingException
	 */
	public static int getHashCode(Message message) throws MessagingException {
		return (message.getSubject() != null ? message.getSubject().hashCode() : 0xAAAAAAAA) ^ message.getSize();
	}
	
	/**
	 * Vergleicht eine eMail-Nachricht mit dem übergebenen
	 * hashcode und sentdate.
	 * 
	 * @throws MessagingException
	 */
	public static boolean compare(Message message, int hashcode, long sentdate) throws MessagingException {
		return (((message.getSubject() != null ? message.getSubject().hashCode() : 0xAAAAAAAA) ^ message.getSize()) == hashcode)
				&& (message.getSentDate() != null ? message.getSentDate().getTime() == sentdate : 0L == sentdate);
	}
}
