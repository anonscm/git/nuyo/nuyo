/* $Id: RoleWorker.java,v 1.12 2007/06/15 15:58:28 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.role;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TglobalroleDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.AttributeSet;
import de.tarent.octopus.server.OctopusContext;

/**
 *
 * TODO Erkl�rung!? 
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class RoleWorker implements RoleWorkerConstants {
	
	public String[] INPUT_getGlobalRoles = {};
	public boolean[] MANDATORY_getGlobalRoles = {};
	public String OUTPUT_getGlobalRoles = "GlobalRoles";
	
	public Collection getGlobalRoles(OctopusContext oc) throws SQLException{
		Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TglobalroleDB.getTableName());
		select.select(TglobalroleDB.ALL);
		Result result = DB.result(TcDBContext.getDefaultContext(), select);
		//result.resultSet().next();
		Collection resultc = TglobalroleDB.getBeanCollection(result.resultSet());
		return resultc;
	}
	
	public String[] INPUT_createOrModifyGlobalRole = { "id", "attributes" };
	public boolean[] MANDATORY_createOrModifyGlobalRole = { false, true };
	public String OUTPUT_createOrModifyGlobalRole = "id";
	
	public Integer createOrModifyGlobalRole(OctopusContext oc, Integer id, Map attributes) throws SQLException{
		InsertUpdate statement = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TglobalroleDB.getTableName());
		//Konvertiere die dreckigen String aus der Attribute-Map nach Boolean... Das muss eigentlich im Client im AbstractEntity gefixt werden!!!!
		Iterator it = attributes.keySet().iterator();
		while(it.hasNext()){
			Object key = it.next();
			Object value = attributes.get(key);
			if(value instanceof String){
				//Teste, ob das ein Boolean ist...
				Boolean test = Boolean.valueOf((String)value);
				if(test.toString().equals(value)){
					attributes.put(key, test);
				}
			}
		}
		AttributeSet as = new AttributeSet(attributes, statement);
		as.add(TglobalroleDB.ROLENAME, KEY_ROLENAME);
		as.add(TglobalroleDB.AUTHDELETE, KEY_DELETE);
		as.add(TglobalroleDB.AUTHEDITCATEGORY, KEY_EDITFOLDER);
		as.add(TglobalroleDB.AUTHEDITSCHEDULE, KEY_EDITSCHEDULE);
		as.add(TglobalroleDB.AUTHEDITUSER, KEY_EDITUSER);
		as.add(TglobalroleDB.AUTHREMOVECATEGORY, KEY_REMOVEFOLDER);
		as.add(TglobalroleDB.AUTHREMOVESCHEDULE, KEY_REMOVESCHEDULE);
		as.add(TglobalroleDB.AUTHREMOVEUSER, KEY_REMOVEUSER);
        if (id == null) {
			InsertKeys keys = statement.executeInsertKeys(TcDBContext.getDefaultContext());
			id = keys.getPkAsInteger();
        }else{
			statement.executeUpdate(TcDBContext.getDefaultContext(), TglobalroleDB.PK_PK, id);
        }
		return id;
	}
	

	public String[] INPUT_deleteGlobalRole = { "id" };
	public boolean[] MANDATORY_deleteGlobalRole = { true };
	public String OUPUT_deleteGlobalRole = null;
	
	public void deleteGlobalRole(OctopusContext oc, Integer id) throws SQLException, Exception {
		List referringGroups = SQL.Select(TcDBContext.getDefaultContext())
            .from(TgroupDB.getTableName())
            .add(TgroupDB.GROUPNAME, String.class)
            .where(Expr.equal(TgroupDB.FKGLOBALROLE, id))
            .getList(TcDBContext.getDefaultContext());
        checkGroupDependences( referringGroups );
        
		SQL.Delete(TcDBContext.getDefaultContext()).from(TglobalroleDB.getTableName()).where(Expr.equal(TglobalroleDB.PK_PK, id)).executeDelete(TcDBContext.getDefaultContext());
	}
    
    private void checkGroupDependences( List referringGroups ) throws Exception {
        int count = referringGroups.size();
        if (count > 0){
            StringBuilder errorMessage = new StringBuilder(40 * count); 
            for (int i = 0; i < count; i++ ){
                errorMessage.append("- ").append(referringGroups.get(i)).append("\n");
            }
        	throw new Exception(errorMessage.toString());
        }
    }
	
	public String[] INPUT_getObjectRoles = { "objectroleid" };
	public boolean[] MANDATORY_getObjectRoles = { false};
	public String OUTPUT_getObjectRoles = "ObjectRoles";
	
	public Collection getObjectRoles(OctopusContext oc, Integer id) throws SQLException{
		Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TcategoryroleDB.getTableName());
		select.select(TcategoryroleDB.ALL);
		WhereList wl = new WhereList();
		if(id!=null){wl.addAnd(Expr.equal(TcategoryroleDB.PK_PK, id));}
		select.where(wl);
		Result result = DB.result(TcDBContext.getDefaultContext(), select);
		//result.resultSet().next();
		Collection resultc = TcategoryroleDB.getBeanCollection(result.resultSet());
		return resultc;
	}
	
	public String[] INPUT_createOrModifyObjectRole = { "id", "attributes" };
	public boolean[] MANDATORY_createOrModifyObjectRole = { false, true };
	public String OUTPUT_createOrModifyObjectRole = "id";
	
	public Integer createOrModifyObjectRole(OctopusContext oc, Integer id, Map attributes) throws SQLException{
		InsertUpdate statement = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TcategoryroleDB.getTableName());
/*		//Konvertiere die dreckigen String aus der Attribute-Map nach Boolean... Das muss eigentlich im Client im AbstractEntity gefixt werden!!!!
		Iterator it = attributes.keySet().iterator();
		while(it.hasNext()){
			Object key = it.next();
			Object value = attributes.get(key);
			if(value instanceof String){
				//Teste, ob das ein Boolean ist...
				Boolean test = Boolean.valueOf((String)value);
				if(test.toString().equals((String)value)){
					attributes.put(key, test);
				}
			}
		}
*/		AttributeSet as = new AttributeSet(attributes, statement);
		as.add(TcategoryroleDB.ROLENAME, KEY_ROLENAME);
		as.add(TcategoryroleDB.AUTHADD, KEY_ADD);
		as.add(TcategoryroleDB.AUTHADDSUB, KEY_ADDSUB);
		as.add(TcategoryroleDB.AUTHEDIT, KEY_EDIT);
		as.add(TcategoryroleDB.AUTHGRANT, KEY_GRANT);
		as.add(TcategoryroleDB.AUTHREAD, KEY_READ);
		as.add(TcategoryroleDB.AUTHREMOVE, KEY_REMOVE);
		as.add(TcategoryroleDB.AUTHSTRUCTURE, KEY_STRUCTURE);
		as.add(TcategoryroleDB.AUTHREMOVESUB, KEY_REMOVESUB);
        if (id == null) {
			InsertKeys keys = statement.executeInsertKeys(TcDBContext.getDefaultContext());
			id = keys.getPkAsInteger();
        }else{
			//System.out.println(statement.update().statementToString());
			statement.executeUpdate(TcDBContext.getDefaultContext(), TcategoryroleDB.PK_PK, id);
        }
		return id;
	}
	

	public String[] INPUT_deleteObjectRole = { "id" };
	public boolean[] MANDATORY_deleteObjectRole = { true };
	public String OUPUT_deleteObjectRole = null;
	
	public void deleteObjectRole(OctopusContext oc, Integer id) throws SQLException, Exception{
		List referringGroups = SQL.Select(TcDBContext.getDefaultContext())
            .from(TgroupCategoryDB.getTableName())
            .add(TgroupCategoryDB.PK_PK, Integer.class)
            .where(Expr.equal(TgroupCategoryDB.FKCATEGORYROLE, id))
            .getList(TcDBContext.getDefaultContext());
        checkGroupDependences(referringGroups);

		SQL.Delete(TcDBContext.getDefaultContext()).from(TcategoryroleDB.getTableName()).where(Expr.equal(TcategoryroleDB.PK_PK, id)).executeDelete(TcDBContext.getDefaultContext());
	}



    public static String[] INPUT_getOwnerRoleID = {};
    public static String OUTPUT_getOwnerRoleID = "ownerRoleID";
    /**
     * Liefert die ID der Kategoriebezoge owner-Rolle zur�ck
     */
    public Integer getOwnerRoleID(OctopusContext oc) throws SQLException {
        List ownerRolles = SQL.Select(TcDBContext.getDefaultContext())
            .from(TcategoryroleDB.getTableName())
            .add(TcategoryroleDB.PK_PK, Integer.class)
            .byId(TcategoryroleDB.ROLETYPE, new Integer(1))							
            .getList(TcDBContext.getDefaultContext());
        if (ownerRolles.size() > 0)
            return (Integer)ownerRolles.get(0);
        return null;
    }
}
