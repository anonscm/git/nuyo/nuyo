/* $Id: Session.java,v 1.7 2006/09/15 10:52:11 schmitz Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 * Created on 30.05.2003
 */
package de.tarent.groupware.webclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import de.tarent.groupware.address.AddressEntityDescription;
import de.tarent.groupware.utils.GenericSelector;
import de.tarent.octopus.server.OctopusContext;

/**
 * Dieser Worker verwaltet Session-Daten f�r die contact-Webapplikation.
 * 
 * @author mikel
 */
public class Session {
	// TODO Feld f�r zwischengespeicherte Kategorien und Unterkategorien
	// hier bekannt machen.
	
	public static final Logger logger = Logger.getLogger(Session.class.getName());
	
	public static final String STATUS_LIST_CRITERIA_CHANGED = "updateList";
	
	public static final String SESSION_ADRESS_ID = "adrId";
	public static final String SESSION_BENUTZER = "user";
	public static final String SESSION_ID = "id";
	
	public static final String SESSION_PAGE = "page";
	public static final String SESSION_CATEGORY = "category";
	public static final String SESSION_SUBCATEGORY = "subcategory";
	
	public static final String SESSION_SEARCH_HERRNFRAU = "herrnFrau";
	public static final String SESSION_SEARCH_TITEL = "titel";
	public static final String SESSION_SEARCH_AKAD_TITEL = "akad_titel";
	public static final String SESSION_SEARCH_VORNAME = "vorname";
	public static final String SESSION_SEARCH_NACHNAME = "nachname";
	public static final String SESSION_SEARCH_NAMENSZUSATZ = "namenszusatz";
	public static final String SESSION_SEARCH_INSTITUTION = "institution";
	public static final String SESSION_SEARCH_STRASSE = "strasse";
	public static final String SESSION_SEARCH_PLZ = "plz";
	public static final String SESSION_SEARCH_ORT = "ort";
	public static final String SESSION_SEARCH_BUNDESLAND = "bundesland";
	public static final String SESSION_SEARCH_LKZ = "lkz";
	public static final String SESSION_SEARCH_LAND = "land";
	public static final String SESSION_SEARCH_TELP = "tel_p";
	public static final String SESSION_SEARCH_TELD = "tel_d";
	public static final String SESSION_SEARCH_FAXP = "fax_p";
	public static final String SESSION_SEARCH_FAXD = "fax_d";
	public static final String SESSION_SEARCH_EMAIL = "email";
	public static final String SESSION_SEARCH_MATCHCODESTR = "matchcodeStr";
	public static final String SESSION_SEARCH_VERTGRP = "vertgrp";
	
	public static final String[] PARAMS_SESSION_SEARCH = 
	{ 	SESSION_SEARCH_HERRNFRAU, SESSION_SEARCH_TITEL, SESSION_SEARCH_AKAD_TITEL,
		SESSION_SEARCH_VORNAME, SESSION_SEARCH_NACHNAME, SESSION_SEARCH_NAMENSZUSATZ,
		SESSION_SEARCH_INSTITUTION, SESSION_SEARCH_STRASSE,
		SESSION_SEARCH_PLZ, SESSION_SEARCH_ORT, SESSION_SEARCH_BUNDESLAND,
		SESSION_SEARCH_LKZ, SESSION_SEARCH_LAND, SESSION_SEARCH_TELP,
		SESSION_SEARCH_TELD, SESSION_SEARCH_FAXP, SESSION_SEARCH_FAXD,
		SESSION_SEARCH_EMAIL };
	
	public static final Map<String, String> SEARCH_INPUT_ENTITY_MAPPING;
	static { 
		SEARCH_INPUT_ENTITY_MAPPING = new HashMap<String, String>(17);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_HERRNFRAU, AddressEntityDescription.HERRN_FRAU);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_TITEL, AddressEntityDescription.TITEL);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_AKAD_TITEL, AddressEntityDescription.AKAD_TITLE);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_VORNAME, AddressEntityDescription.VORNAME);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_NACHNAME, AddressEntityDescription.NACHNAME);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_NAMENSZUSATZ, AddressEntityDescription.NAMENSZUSATZ);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_INSTITUTION, AddressEntityDescription.INSTITUTION);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_STRASSE, AddressEntityDescription.STRASSE);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_PLZ, AddressEntityDescription.PLZ);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_ORT, AddressEntityDescription.ORT);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_BUNDESLAND, AddressEntityDescription.BUNDESLAND);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_LKZ, AddressEntityDescription.LKZ);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_LAND, AddressEntityDescription.LAND);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_TELP, AddressEntityDescription.TELEFON_PRIVAT);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_FAXP, AddressEntityDescription.FAX_PRIVAT);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_FAXD, AddressEntityDescription.FAX);
		SEARCH_INPUT_ENTITY_MAPPING.put(SESSION_SEARCH_EMAIL, AddressEntityDescription.E_MAIL);
	}
	
	public static final String SESSION_SEARCH_ENABLED = "searchEnabled";
	public static final String SESSION_LIST_ORDER = "order";
	public static final String SESSION_LIST_ORDER_ASC = "orderAsc";
	public static final String SESSION_ENTRIES_PER_PAGE = "entriesPerPage";
	public static final String SESSION_KEY_LIST = "addressKeyList";
	
	public static final String FIELD_SESSION_ADDRESS = "address";

	public static final String RESULT_ok = "ok";
	public static final String RESULT_error = "error";
	
	public static final String[] INPUT_update = {};
	public static final boolean[] MANDATORY_update = {};
	public static final String OUTPUT_update = null;
	public static void update(OctopusContext oc) {
		oc.setContent(GenericSelector.ORDER_BY, oc.sessionAsObject(GenericSelector.ORDER_BY));
		if (oc.getStatus().equals(STATUS_LIST_CRITERIA_CHANGED))
			return;
		
		//Session-Daten
		// Update list criteria if session is not initialized yet
		if (oc.sessionAsObject(SESSION_ID) == null)
			oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
		updateSessionParam(oc, SESSION_ID, String.class, "");
		
		if (updateSessionParam(oc, SESSION_ENTRIES_PER_PAGE, Integer.class, new Integer(10)))
			oc.setContent(SESSION_PAGE, new Integer(0));
		
		//Session-Daten f�r Suche
		updateSessionParam(oc, SESSION_PAGE, Integer.class, 0);
		
		for (String param: PARAMS_SESSION_SEARCH) {
			if (param.equals(SESSION_SEARCH_NACHNAME)) 
				System.out.println();
			boolean paramChanged = updateSessionParam(oc, param, String.class, "");
			if (paramChanged) 
				oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
		}
									
		if (updateSessionParam(oc, SESSION_SEARCH_MATCHCODESTR, Boolean.class, false))
			oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
		
		if (updateSessionParam(oc, SESSION_SEARCH_ENABLED, Boolean.class, false))
			oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
		
		if (updateSessionParam(oc, SESSION_CATEGORY, Integer.class, -1)) {
			oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
			oc.setContent(SESSION_SUBCATEGORY, new Integer(0));
		}
			
		if (updateSessionParam(oc, SESSION_SUBCATEGORY, Integer.class, 0))
			oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
		
		if (oc.getStatus().equals(STATUS_LIST_CRITERIA_CHANGED))
			oc.setContent(SESSION_PAGE, new Integer(0));
		
		if (updateSessionParam(oc, SESSION_LIST_ORDER, String.class, "nachname")
				|| oc.requestContains(SESSION_LIST_ORDER)) {
			// switching Boolean SESSION_LIST_ORDER_ASC
			if (!updateSessionParam(oc, SESSION_LIST_ORDER_ASC, Boolean.class, new Boolean(true)))
				oc.setSession(SESSION_LIST_ORDER_ASC, new Boolean(!((Boolean)oc.sessionAsObject(SESSION_LIST_ORDER_ASC)).booleanValue()));
			oc.setStatus(STATUS_LIST_CRITERIA_CHANGED);
		}
		
		// Benutzer �bernehmen
		oc.setContent(SESSION_BENUTZER, oc.getConfigObject().getLoginname());
		
		oc.setContent("viewAddressList", "viewAddressList");

		// Adressnummer �bernehmen und �bertragen
		if (oc.getStatus().equals(STATUS_LIST_CRITERIA_CHANGED))
			oc.setContent(SESSION_ADRESS_ID, new Integer(0));

		updateSessionParam(oc, SESSION_ADRESS_ID, Integer.class, 0);		
	}
	
	public static final String[] INPUT_updateKeyList = { GenericSelector.RESULT_ENTITY_TABLE };
	public static final boolean[] MANDATORY_updateKeyList = { true };
	public static final String OUTPUT_updateKeyList = "SESSION:"+SESSION_KEY_LIST;
	public static List updateKeyList(OctopusContext oc, List addressKeyList) {
		// empty projection fields to allow automatic map projection from the entity description
		oc.setContent(GenericSelector.PROJECTION_FIELDS, new ArrayList(0));
		return addressKeyList;		
	}
	
	private static boolean updateSessionParam(OctopusContext oc, String sessionKey, Class objectType, Object defaultValue) {
		Object value = null;
		Object sessionValue = null;
		if (objectType.equals(Boolean.class)) {
			value = oc.getContextField(sessionKey) == null ? null : Boolean.valueOf(String.valueOf(oc.getContextField(sessionKey)));
			sessionValue = oc.sessionAsObject(sessionKey) == null ? null : (Boolean)oc.sessionAsObject(sessionKey);
		} else if (objectType.equals(Integer.class)) {
			value = oc.getContextField(sessionKey) == null ? null : Integer.valueOf(String.valueOf(oc.getContextField(sessionKey)));
			sessionValue = oc.sessionAsObject(sessionKey) == null ? null : (Integer)oc.sessionAsObject(sessionKey);
		} else { 
			value = oc.getContextField(sessionKey) == null ? null : (String)oc.getContextField(sessionKey);
			sessionValue = oc.sessionAsObject(sessionKey) == null ? null : (String)oc.sessionAsObject(sessionKey);
		}
		Object newValue = null;
		if (value != null && sessionValue != null && !value.toString().equals(sessionValue.toString()))
			newValue = value;
		else if (sessionValue == null)
			newValue = defaultValue;
		
		if (newValue != null) {
			oc.setSession(sessionKey, newValue);
			oc.setContent(sessionKey, newValue.toString());
			return true;
		} else {
			oc.setContent(sessionKey, sessionValue.toString());
			return false;
		}
	}
}
