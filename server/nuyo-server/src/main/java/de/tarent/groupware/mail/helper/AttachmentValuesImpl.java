package de.tarent.groupware.mail.helper;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.tarent.contact.bean.TattachmentDB;
import de.tarent.groupware.mail.AttachmentValues;
import de.tarent.groupware.mail.helper.Attachments.StorageType;


/**
 * Is a wrapper for the robust handling of a result set.
 * 
 * Alternative use {@link de.tarent.contact.bean.Tattachment} as a Bean with:<p>  
 * <code>
 * Tattachment bean = new Tattachment(); 
 * TattachmentDB.fetchDefaultColumns(resultSet, bean);
 * </code>  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
class AttachmentValuesImpl implements AttachmentValues {
    // column names (without table prefix) for robust access of values
    private static final String TEXTCONTENT = TattachmentDB.TEXTCONTENT.substring(TattachmentDB.TEXTCONTENT.indexOf('.') + 1);
    private static final String FILENAME = TattachmentDB.FILENAME.substring(TattachmentDB.FILENAME.indexOf('.') + 1);
    private static final String FKATTACHMENTTYPE = TattachmentDB.FKATTACHMENTTYPE.substring(TattachmentDB.FKATTACHMENTTYPE.indexOf('.') + 1);
    // values
    private String name;
    private String filePath;
    private StorageType storageType;
    
    
    /**
     * Retrieves values of a given attachment entry.
     * Expects only one row with an attachment entry of {@link de.tarent.contact.bean.TattachmentDB} schema instance.
     * That is the first row will be handled if several rows exist.
     * 
     * @param oneRowResultSet represents a row to be handled (a cursor points to a required row if there are several).  
     * @throws SQLException if failed retrieving values
     * @exception IllegalArgumentException if empty result set
     * @exception NullPointerException if invalid value for {@link de.tarent.groupware.mail.helper.Attachments.StorageType}
     */
    public AttachmentValuesImpl(ResultSet oneRowResultSet) throws SQLException {
            if ( !oneRowResultSet.wasNull() ) {
                name = oneRowResultSet.getString(TEXTCONTENT); 
                filePath = oneRowResultSet.getString(FILENAME);
                int storageValue = oneRowResultSet.getInt(FKATTACHMENTTYPE);
                if(StorageType.BLOB.ordinal() == storageValue) storageType = StorageType.BLOB;
                else if (StorageType.FILE.ordinal() == storageValue 
                    || new File(filePath).exists()) storageType = StorageType.FILE;
                else throw new IllegalStateException("unknown storage type of attachment");
            }
    }
    
    public String getName(){ return name; }
    public String getFilePath(){ return filePath; }
    public StorageType getStorageType(){ return storageType;}
}
