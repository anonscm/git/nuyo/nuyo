/* $Id: ListProperties.java,v 1.2 2006/03/16 13:49:30 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * ListProperties ist eine Liste mit zus�tzlichen Eigenschaften (Map).
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public class ListProperties {
	private List mList;
	private Map mMap;
	
	/**
	 * Erstellt neue ListProperties mit leerer Liste und Map.
	 */
	public ListProperties() {
		mList = new ArrayList();
		mMap = new Properties();
	}

	/**
	 * �bernimmt Liste 'list' und erstellt lediglich eine neue Map.
	 * 
	 * @param list
	 */
	public ListProperties(List list) {
		mList = list;
		mMap = new Properties();
	}
	
	/**
	 * Leert Liste und Map.
	 */
	public void clear() {
		mList.clear();
		mMap.clear();
	}
	
	/**
	 * Leert Liste.
	 */
	public void clearList() {
		mList.clear();
	}
	
	/**
	 * Leert Map.
	 */
	public void clearProperties() {
		mMap.clear();
	}
	
	/**
	 * F�gt 'object' der Liste hinzu.
	 * 
	 * @param object
	 */
	public void add(Object object) {
		mList.add(object);
	}

	/**
	 * F�gt 'object' der Liste an Position 'offset' hinzu.
	 * 
	 * @param offset
	 * @param object
	 */
	public void add(int offset, Object object) {
		mList.add(offset, object);
	}

	/**
	 * L�scht Objekt an Position 'offset' aus Liste.
	 * 
	 * @param offset
	 */
	public void remove(int offset) {
		mList.remove(offset);
	}

	/**
	 * L�scht Objekt 'object' aus Liste.
	 * 
	 * @param object
	 */
	public void remove(Object object) {
		mList.remove(object);
	}
	
	/**
	 * Gibt Gr��e der Liste zur�ck.
	 * 
	 * @return size
	 */
	public int size() {
		return mList.size();
	}
	
	/**
	 * Speichert Eigenschaft.
	 * 
	 * @param key
	 * @param value
	 */
	public void setProperty(String key, String value) {
		mMap.put(key, value);
	}

	/**
	 * Speichert Eigenschaft.
	 * 
	 * @param key
	 * @param value
	 */
	public void setProperty(String key, int value) {
		mMap.put(key, new Integer(value));
	}

	/**
	 * Speichert Eigenschaft.
	 * 
	 * @param key
	 * @param value
	 */
	public void setProperty(String key, Integer value) {
		mMap.put(key, value);
	}

	/**
	 * Gibt Eigenschaft zur�ck.
	 * 
	 * @param key
	 */
	public Object getProperty(Object key) {
		return mMap.get(key);
	}
	
	/**
	 * Gibt die Liste zur�ck.
	 * 
	 * @return 'list'
	 */
	public List getList() {
		return mList;
	}
	
	/**
	 * Gibt die Map zur�ck.
	 * 
	 * @return 'map'
	 */
	public Map getMap() {
		return mMap;
	}
}
