package de.tarent.groupware.category;


import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.groupware.SubCategory;

/**
 * 
 * @author Nils Neumaier
 *
 */

public class SubCategoryDBMapping extends AbstractDBMapping {

	public static final String STMT_SELECT_ALL_OF_ONE_CATEGORY = "selectAllSubCategoriesOfOneCategory";
	public static final String PARAM_SELECTING_USER_ID = "selectingUserId";
	public static final String PARAM_SELECTING_CATEGORY_ID = "selectingCategoryId";
	
 	DBContext initialDBC;
    public SubCategoryDBMapping(DBContext dbc) {
        super(dbc);
        initialDBC = dbc;
    }

	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDBMapping#getTargetTable()
	 */
	public String getTargetTable() {
		return TsubcategoryDB.getTableName();
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDBMapping#configureMapping()
	 */
	public void configureMapping() {		

		addField(TsubcategoryDB.PK_PKSUBCATEGORY, SubCategory.PROPERTY_ID, PRIMARY_KEY_FIELDS | COMMON_FIELDS);
		addField(TsubcategoryDB.CATEGORYNAME, SubCategory.PROPERTY_SUBCATEGORYNAME, COMMON_FIELDS);
		addField(TsubcategoryDB.DESCRIPTION, SubCategory.PROPERTY_DESCRIPTION, COMMON_FIELDS);
		addField(TsubcategoryDB.FKCATEGORY, SubCategory.PROPERTY_PARENTCATEGORY, COMMON_FIELDS);
	      
        addQuery(STMT_SELECT_ONE, 
                 createBasicSelectOne(),     
                 COMMON_FIELDS);
        
        // Selects all subcategories of one category
        addQuery(STMT_SELECT_ALL_OF_ONE_CATEGORY, 
                 createBasicSelectAll()
                 .join(TsubcategoryDB.getTableName(),"v_user_folder.fk_folder", TsubcategoryDB.FKCATEGORY)
                 .whereAndEq("v_user_folder.userid", new ParamValue(PARAM_SELECTING_USER_ID))
                 .whereAndEq("v_user_folder.fk_folder", new ParamValue(PARAM_SELECTING_CATEGORY_ID)),
                 COMMON_FIELDS);
	}
}