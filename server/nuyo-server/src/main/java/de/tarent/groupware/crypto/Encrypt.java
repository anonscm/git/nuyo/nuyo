/* $Id: Encrypt.java,v 1.3 2007/06/15 15:58:30 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Date;

import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;

import de.tarent.groupware.utils.LengthCounter;

/**
 * Verschl�sselt Daten mit einem Public-Key oder einer Passphrase.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.3 $
 */
public class Encrypt extends AbstractStream {
	protected String name;
	protected long length;
	protected Date modification;
	protected PGPPublicKey publicKey;
	protected char[] passphrase;
	protected boolean armor = true;
	protected boolean compress = true;
	protected boolean integrityCheck = true;

	public void setInput(File file) throws IOException {
		this.dataSource = new FileDataSource(file);
		this.name = file.getName();
		this.length = file.length();
		this.modification = new Date(file.lastModified());
	}

	public void setInput(DataSource dataSource) throws IOException {
		this.dataSource = dataSource;
		this.name = dataSource.getName();
		InputStream inputStream = dataSource.getInputStream();
		LengthCounter counter = new LengthCounter();
		pipe(inputStream, counter);
		inputStream.close();
		this.length = counter.length();
		this.modification = PGPLiteralDataGenerator.NOW;
	}

	public void setInput(DataSource dataSource, String name, long length) throws IOException {
		this.dataSource = dataSource;
		this.name = name;
		this.length = length;
		this.modification = PGPLiteralDataGenerator.NOW;
	}

	public void setInput(DataSource dataSource, String name, long length, Date modification) throws IOException {
		this.dataSource = dataSource;
		this.name = name;
		this.length = length;
		this.modification = modification;
	}

	public void setPublicKey(PGPPublicKey publicKey) throws GeneralSecurityException {
		this.publicKey = Crypto.assertKey(publicKey);
	}

	public void setPassphrase(char[] passphrase) throws GeneralSecurityException {
		this.passphrase = Crypto.assertPass(passphrase);
	}

	public void setArmor(boolean armor) {
		this.armor = armor;
	}

	public void setCompress(boolean compress) {
		this.compress = compress;
	}

	public void setIntegrityCheck(boolean integrityCheck) {
		this.integrityCheck = integrityCheck;
	}

	protected void crypt() throws GeneralSecurityException, IOException {
		try {
			// Daten Base64 kodieren.
			OutputStream outputStream = this.outputStream;
			if (armor) {
				outputStream = new ArmoredOutputStream(outputStream);
			}
			
			InputStream inputStream;
			OutputStream literalStream;
			OutputStream encryptStream;
			PGPCompressedDataGenerator compressGenerator =
				new PGPCompressedDataGenerator(PGPCompressedDataGenerator.ZIP);
			PGPLiteralDataGenerator literalGenerator =
				new PGPLiteralDataGenerator();
			
			// Gr��e der gepackten Daten inkl. Header ermitteln.
			LengthCounter counter = new LengthCounter();
			literalStream = literalGenerator.open(
					compress ? compressGenerator.open(counter) : counter,
					PGPLiteralDataGenerator.BINARY, name, length, modification);
			inputStream = dataSource.getInputStream();
			pipe(inputStream, literalStream);
			inputStream.close();
			literalGenerator.close();
			if (compress) {
				compressGenerator.close();
			}
			long size = counter.length();
			
			// Daten mit Public-Key oder Passphrase verschl�sseln.
			PGPEncryptedDataGenerator encryptGenerator =
				new PGPEncryptedDataGenerator(PGPEncryptedDataGenerator.CAST5,
						integrityCheck, new SecureRandom(), Crypto.PROVIDER);
			if (publicKey != null)
				encryptGenerator.addMethod(publicKey);
			else if (passphrase != null)
				encryptGenerator.addMethod(passphrase);
			
			encryptStream = encryptGenerator.open(outputStream, size);
			literalStream = literalGenerator.open(
					compress ? compressGenerator.open(encryptStream) : encryptStream,
					PGPLiteralDataGenerator.BINARY, name, length, modification);
			inputStream = dataSource.getInputStream();
			pipe(inputStream, literalStream);
			inputStream.close();
			literalGenerator.close();
			if (compress) {
				compressGenerator.close();
			}
			encryptGenerator.close();
			
			if (armor) {
				outputStream.close();
			}
		} catch (PGPException e) {
			GeneralSecurityException gse = new GeneralSecurityException(e.getLocalizedMessage());
			gse.setStackTrace(e.getStackTrace());
			throw gse;
		}
	}

	protected void clear() {
		Crypto.clearPass(passphrase);
	}
}