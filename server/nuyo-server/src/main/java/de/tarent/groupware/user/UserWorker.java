package de.tarent.groupware.user;

import java.sql.SQLException;
import java.util.List;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.bean.TuserparamDB;
import de.tarent.contact.bean.TuserscheduleDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.ResultTransform;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcReflectedWorker;
/**
 * @author kleinw
 *
 *	createOrModfiyUser
 *	getUser
 *
 */
public class UserWorker extends TcReflectedWorker implements UserWorkerConstants {

    public UserWorker() throws TcActionDeclarationException {
    }
    
    final static public String[] INPUT_GETUSERS = {"userid" ,"userlogin", "categoryid", "usergroupid", "scheduleid", "ismanager", "with_password"};
    final static public boolean[] MANDATORY_GETUSERS = {false, false, false, false, false, false, false, false};
    final static public String OUTPUT_GETUSERS = "users";
    
    final static public Object getUsers(TcAll all, Integer userId, String userLogin, Integer categoryId, Integer groupId, Integer scheduleId, Boolean isManager, Boolean with_password) throws SQLException {
        
    	List response = null;
        
    	/*Abfragen, ob allowFetchPassword gesetzt ist, nur wenn gesetzt, darf Password gelesen werden*/
    	Boolean allowFetchPassword = Boolean.valueOf(all.moduleConfig().getParam("allowFetchPassword"));
    	if(allowFetchPassword==null||!allowFetchPassword.booleanValue()){
    		with_password=Boolean.FALSE;
    	}
    	
        Select select =
        	SQL.Select(TcDBContext.getDefaultContext())
        		.from(TuserDB.getTableName())
        		.add(TuserDB.PK_PKUSER, Integer.class)
        		.add(TuserDB.LOGINNAME, String.class);
                // Passwort wurde aus Sicherheitsgründen heraus genommen. 
        		if(with_password!=null&&with_password.booleanValue()){
        			select.add(new RawClause("encode(decode("+TuserDB.PWD+",'hex'),'base64')").toString(), String.class);
        		}else{
        			select.add(new RawClause("lower('password_is_hidden')").toString(), String.class);
        		}
                select.add(TuserDB.LASTNAME, String.class)
        		.add(TuserDB.FIRSTNAME, String.class)
        		.add(TuserDB.USERTYPE, Integer.class)
        		.orderBy(Order.asc(TuserDB.LOGINNAME));
        //System.out.println(select);
        if (userId != null)
            response =
	            select
	            	.where(Expr.equal(TuserDB.PK_PKUSER, userId))
	            	.getList(TcDBContext.getDefaultContext());
        
        else if (userLogin != null)
            response = 
            	select
            		.where(Expr.equal(TuserDB.LOGINNAME, userLogin))
            		.getList(TcDBContext.getDefaultContext());
        
        else if(categoryId != null) 
            response =
                select
                .join(TgroupUserDB.getTableName(), TgroupUserDB.FKUSER, TuserDB.PK_PKUSER)
                .join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKGROUP, TgroupUserDB.FKGROUP)
                .where(Expr.equal(TgroupCategoryDB.FKCATEGORY, categoryId))
                	.getList(TcDBContext.getDefaultContext());
        
        else if (groupId != null) {
	            response =
		        	select
		        		.join(TgroupUserDB.getTableName(), TgroupUserDB.FKUSER, TuserDB.PK_PKUSER)
		        		.where(Expr.equal(TgroupUserDB.FKGROUP, groupId))
		        		.getList(TcDBContext.getDefaultContext());
        } 
        
        else if (scheduleId != null) {
            response = 
	        	select
	        		.join(TuserscheduleDB.getTableName(), TuserscheduleDB.FKUSER, TuserDB.PK_PKUSER)
	        		.where(Expr.equal(TuserscheduleDB.FKSCHEDULE, scheduleId))
	        		.getList(TcDBContext.getDefaultContext());
        }
        
        else 
            response = 
                select
                	.getList(TcDBContext.getDefaultContext());
        
        return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
        
    }

    




    public static void testParamExists(String value, String paramName) 
        throws TcContentProzessException{
        if (value == null)
            throw new TcContentProzessException("Der Parameter "+paramName+" muss vorhanden sein.");
        if ("".equals(value))
            throw new TcContentProzessException("Der Parameter "+paramName+" darf nicht leer sein.");
    }

	
	static final public String[] INPUT_GETADDRESSESFROMUSERS = {};
	static final public boolean[] MANDATORY_GETADDRESSESFROMUSERS = {};
	static final public String OUTPUT_GETADDRESSESFROMUSERS = "addresses";
	
	static final public List getAddressesFromUsers(TcAll all) throws SQLException {
		try {
			return SQL.Select(TcDBContext.getDefaultContext())
				.from(TaddressDB.getTableName())
				.add(TaddressDB.PK_PKADDRESS, Integer.class)
				.add(TaddressDB.FKUSER, Integer.class)
				.join(TcDBContext.getSchemaName() + "v_user_address", "v_user_address.fk_address" , TaddressDB.PK_PKADDRESS )
				.where(Where.and(
						Expr.equal("v_user_address.userid", all.personalConfig().getUserID()),
						Where.and(						
								Expr.isNotNull(TaddressDB.FKUSER),
								Expr.notEqual(TaddressDB.FKUSER, new Integer(0)))
								))				
				.getList(all.getModuleName());
    	} catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			throw e;
    	}
    }

	static final public String[] INPUT_GETUSERFORLOGINNAME = { "loginname" };
	static final public boolean[] MANDATORY_GETUSERFORLOGINNAME = { true };
	static final public String OUTPUT_GETUSERFORLOGINNAME = "userid";
	
	static final public Integer getUserForLoginname(TcAll all, String loginname) throws SQLException {
		try {
			return (Integer)SQL.Select(TcDBContext.getDefaultContext())
				.from(TuserDB.getTableName())
				.add(TuserDB.PK_PKUSER, Integer.class)
				.where(Expr.equal(TuserDB.LOGINNAME, loginname))
				.getList(all.getModuleName())
				.get(0);
		} catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			throw e;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	static final public String[] INPUT_GETCURRENTUSERID = {  };
	static final public boolean[] MANDATORY_GETCURRENTUSERID = {  };
	static final public String OUTPUT_GETCURRENTUSERID = "userid";
	
	static final public Integer getCurrentUserID(TcAll all) throws SQLException {
        if (all.personalConfig().getUserID() == null || all.personalConfig().getUserID().intValue() == 0)
            return getUserForLoginname(all, all.personalConfig().getLoginname());
        return all.personalConfig().getUserID();
	}


	static final public String[] INPUT_SETPERSONALCONFIGCONTENS = { "username" };
	static final public boolean[] MANDATORY_SETPERSONALCONFIGCONTENS = { true };
	static final public String OUTPUT_SETPERSONALCONFIGCONTENS = null;
	
	static final public void setPersonalConfigContens(TcAll all, String username) throws SQLException {
        all.personalConfig().setUserID(getUserForLoginname(all, username));
	}

	static final public String[] INPUT_PUTUSERDATA = { };
	static final public boolean[] MANDATORY_PUTUSERDATA = { };
	static final public String OUTPUT_PUTUSERDATA = null;
	
	static final public void putUserData(TcAll all) throws SQLException {
		all.setContent("userLogin", all.personalConfig().getLoginname());
		all.setContent("username", all.personalConfig().getLoginname());
        all.setContent("userid", all.personalConfig().getUserID());
	}
	
	//FIXME: Der Code wird nicht mehr benutzt?
	/*
	static final public String[] INPUT_GETUSERPROXY = {"userid"};
	static final public boolean[] MANDATORY_GETUSERPROXY = {true};
	static final public String OUTPUT_GETUSERPROXY = "getuserproxy";

	static final public List getUserProxy(TcAll all, Integer userID) throws SQLException {
	    return  SQL.Select()
	    		.from(TuserproxyDB.getTableName())
	    		.add(TuserproxyDB.FKUSERPROXY, Integer.class)
	    		.where(Expr.equal(TuserproxyDB.FKUSER, userID))
	    		.getList(all.getModuleName());
	}
	*/
	
	static final public String[] INPUT_GETPARAMETER = {"userid", "parameter"};
	static final public boolean[] MANDATORY_GETPARAMETER = {true, false};
	static final public String OUTPUT_GETPARAMETER = "parameter";
	
	final static public Object getParameter(TcAll all, Integer userId, String parameter) throws SQLException {

	    List response = null;
	    
	    Select select = 
	    	SQL.Select(TcDBContext.getDefaultContext())
	    		.from(TuserparamDB.getTableName())
	    		.add(TuserparamDB.PARAMVALUE, String.class);
	    
	    if(parameter != null && userId != null)
	        response =
	        	select.where(
        	        Where.and(
    	                Expr.equal(TuserparamDB.FKUSER, userId),
    	                Expr.equal(TuserparamDB.PARAMNAME, parameter)
	                )
                )
	        	.getList(all.getModuleName());
	    else
	        response =
	        	select
	        		.where(Expr.equal(TuserparamDB.FKUSER, userId))
	        		.getList(all.getModuleName());
	    
	    return
	    	ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
	    
	}
	
}
