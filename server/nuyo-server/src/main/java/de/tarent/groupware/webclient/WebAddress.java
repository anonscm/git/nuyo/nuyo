/* $Id: WebAddress.java,v 1.2 2006/03/16 13:49:30 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.webclient;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tarent.contact.octopus.db.TcAddressDB;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.groupware.address.AddressWorker;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcReflectedWorker;

/**
 * Verpackt die Datenbank-Klasse TcAddressDB und den AddressWorker
 * fuer eine einfache Handhabung aus dem Webclient herraus.
 * 
 * @author christoph
 */
public class WebAddress extends TcReflectedWorker {
	//
	// externe Konstanten
	//
	
	// see de.tarent.contact.octopus.db.TcAddressSelect.FILTER_MAP
	private static final String FILTER_MAP = "filterMap";
	
	
	//
	// Implementierung
	//
	
	public static final String INPUT_GETADDRESSESBYFILTER[] = { "filtername", "filtervalue" };
	public static final boolean MANDATORY_GETADDRESSESBYFILTER[] = { false, false };
	public static final String OUTPUT_GETADDRESSESBYFILTER = null;
	
	public static final void getAddressesByFilter(TcAll all, String filtername, String filtervalue) throws InputParameterException, SQLException {
		try {
			Map filter = new HashMap();
			
			if (filtername != null && filtervalue != null && filtername.length() != 0 && filtervalue.length() != 0) {
				if (filtervalue.indexOf('*') == -1)
					filter.put(filtername, new StringBuffer(filtervalue.length() + 2)
							.append('*')
							.append(filtervalue)
							.append('*')
							.toString());
				else
					filter.put(filtername, filtervalue);
			}
			
			if (filter.size() == 0)
				return;
			
			Map parameter = new HashMap();
			parameter.put("username", all.personalConfig().getLoginname());
			parameter.put("module", all.getModuleName());
			parameter.put(FILTER_MAP, filter);
			
			List addressId = TcAddressDB.getAddressesPreviewByFilter(parameter);
			if (addressId.size() == 0)
				return;
			
			all.setContent("addresspreview",
					AddressWorker.getAddressPreviewById(all, addressId));
			all.setContent("addressid",
					addressId);
		} catch (InputParameterException e) {
			logError(e);
			throw e;
		} catch (SQLException e) {
			logError(e);
			throw e;
		}
	}
	
	private static final void logError(Exception e) {
		e.printStackTrace();
	}
}
