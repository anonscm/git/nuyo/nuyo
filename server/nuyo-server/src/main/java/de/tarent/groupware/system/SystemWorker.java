/*
 * $Id: SystemWorker.java,v 1.8 2007/06/15 15:58:29 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Wolfgang Klein.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.system;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Category;
import org.apache.log4j.FileAppender;
import org.apache.log4j.xml.DOMConfigurator;

import de.tarent.contact.bean.TparamDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.tcobjects.BeanSettings;

/**
 * Hier werden initiale Methoden verwaltet. (Logging, DB-Pooling, etc.)
 * 
 * @author Wolfgang Klein
 */
public class SystemWorker {

    public static String KEY_CHECK_DB_RESULT = "checkDBResult";
    public static String KEY_DB_CONFIG = "dbConfig";

    public static String TCOBJECTSFILE = "tcobjectsfile";

    protected Throwable initDBException;

	public String INPUT_initLogging[] = {};
	public void initLogging(OctopusContext cntx) {
		DOMConfigurator.configure(cntx.moduleConfig().getOtherNode("log4j:configuration"));
		
		String path = cntx.moduleConfig().getRealPath() + "/log/";
		new File(path).mkdirs();
		changeLogDir(Category.getInstance("SQL"), path);
		changeLogDir(Category.getInstance("WORKER"), path);
		changeLogDir(Category.getInstance("ERROR"), path);
		changeLogDir(Category.getInstance("de.tarent"), path);
		changeLogDir(Category.getInstance("de.tarent.dblayer"), path);
	}

	protected void changeLogDir(Category category, String path) {
		if (category == null) return;
		for (Enumeration e = category.getAllAppenders(); e.hasMoreElements(); ) {
			Object o = e.nextElement();
			if (o instanceof FileAppender) {
				FileAppender a = (FileAppender)o;
				if(a!=null&&a.getFile()!=null){//FIXME: feststellen, warum das hier null sein k�nnte, macht n�mlich eigentlich keinen Sinn
					if (a.getFile().indexOf('/') == -1 && a.getFile().indexOf('\\') == -1) {
						a.setFile(path + a.getFile());
						a.activateOptions();
					}
				}
			}
		}
	}

	
	public String INPUT_initSchema[] = {};
	/**
	 * Initialises the db-Schema
	 *
	 */
	public void initSchema(OctopusContext cntx){
		String schemaname = cntx.moduleConfig().getParam("dbschema");
		if(schemaname!=null){
			if(!schemaname.endsWith(".")&&schemaname.length()>0){
				schemaname+=".";
			}
			BeanSettings.setSchemaName(schemaname);
			TcDBContext.setSchemaName(schemaname);
		}
	}
	
	public static final void init(TcModuleConfig config){
		String schemaname = config.getParam("dbschema");
		if(schemaname!=null){
			if(!schemaname.endsWith(".")&&schemaname.length()>0){
				schemaname+=".";
			}
			BeanSettings.setSchemaName(schemaname);
			TcDBContext.setSchemaName(schemaname);
		}
	}
	
	public String INPUT_checkDB[] = {};
	public String OUTPUT_checkDB = KEY_CHECK_DB_RESULT;
	public String checkDB(OctopusContext cntx) {
        String checkDBResult = "";
        
        if (initDBException != null) {
            StringWriter stringWriter1 = new StringWriter();
            initDBException.printStackTrace(new PrintWriter(stringWriter1));
            checkDBResult += "Beim Der Initialisierung der Datenbank ist ein Fehler auf getreten: \n"+initDBException +"\n\n";
            checkDBResult += stringWriter1.toString();
            cntx.setContent("error", "Fehler bei der Datenbankanbindung");
        }
        
        try {            
            List resultList = SQL.Select(TcDBContext.getDefaultContext())
	    		.from(TparamDB.getTableName())
	    		.add(TparamDB.PARAMVALUE, String.class)
	    		.where(Expr.equal(TparamDB.PARAMNAME, "COLIBRI_SCHEMA_VERSION"))
	    		.getList(cntx.getModuleName());
            
            if (resultList.size() > 0 && resultList.get(0) instanceof String ) {
                checkDBResult += "Datenbank ist erreichbar. Version: "+resultList.get(0);                
            } else {
                cntx.setContent("error", "Fehler bei der Datenbankanbindung");
                checkDBResult += "Version Datenbank konnte nicht ermittelt.";
            }
        } catch (Throwable tw) {
            checkDBResult += "Beim Ermitteln der Datenbankversion trat ein Fehler auf: \n"+tw +"\n\n";
            StringWriter stringWriter = new StringWriter();
            tw.printStackTrace(new PrintWriter(stringWriter));
            checkDBResult += stringWriter.toString();
            cntx.setContent("error", "Fehler bei der Datenbankanbindung");
        }
        return checkDBResult;
	}
	
	public String INPUT_fixDateStrings[] = { "standardAddress" };
	public String OUTPUT_fixDateStrings = "standardAddress";
	
	public List fixDateStrings(OctopusContext cntx, List standardAddress){
		DateFormat df = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy", Locale.ENGLISH);
		List<Object> fixed = new ArrayList<Object>(standardAddress.size());
		
		for(int i=0; i<standardAddress.size(); i++){
			Object tmp = standardAddress.get(i);
			if(tmp!=null){
				Date test = null;
				try {
					test = df.parse(tmp.toString());
				} catch (ParseException e) {
					//Don't worry, there aren't too much dates...
				}
				if(test!=null){
					fixed.add(test);
				}else{
					fixed.add(tmp);
				}
			}else{
				fixed.add(tmp);
			}
		}
		return fixed;
	}
}