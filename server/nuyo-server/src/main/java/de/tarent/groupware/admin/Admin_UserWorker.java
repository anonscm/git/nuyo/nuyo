package de.tarent.groupware.admin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcReflectedWorker;


/**
 * @author kleinw
 *
 *	Dieser Worker realisiert administrative Vorg�nge.
 *
 */
public class Admin_UserWorker extends TcReflectedWorker {
    final static public String[] INPUT_GETUSER = {"userid", "userlogin"};
    final static public boolean[] MANDATORY_GETUSER = {false, false};
    final static public String OUTPUT_GETUSER = "user";

    /**
     * 
     * @param all
     * @param userId
     * @param userLogin
     * @return
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    final static public List getUser(TcAll all, Integer userId, String userLogin) throws SQLException {
        List user = new ArrayList();

        Result tr = null;
        
        Select select =
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TuserDB.getTableName())
            	.selectAs(TuserDB.PK_PKUSER)
            	.selectAs(TuserDB.LOGINNAME)
            	.selectAs(TuserDB.LASTNAME)
            	.selectAs(TuserDB.FIRSTNAME)
            	.selectAs(TuserDB.ISMANAGER);

        	if (userId != null)
        	    select.where(Expr.equal(TuserDB.PK_PKUSER, userId));
        	else if (userLogin != null)
        	    select.where(Expr.equal(TuserDB.LOGINNAME, userLogin));
        
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            if (tr.resultSet().next()) {
                fillResult(tr, user);
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null)
                tr.closeAll();
        }
        
        return user;
    }
    
    final static public String[] INPUT_GETUSERS = {};
    final static public boolean[] MANDATORY_GETUSERS = {};
    final static public String OUTPUT_GETUSERS = "users";
    
    /**
     * 
     * @param all
     * @return
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    final static public List getUsers(TcAll all) throws SQLException {
        List users = new ArrayList();
        
        Result tr = null;
        
        Select select = 
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TuserDB.getTableName())
            	.selectAs(TuserDB.PK_PKUSER)
            	.selectAs(TuserDB.LOGINNAME)
            	.selectAs(TuserDB.LASTNAME)
            	.selectAs(TuserDB.FIRSTNAME)
            	.selectAs(TuserDB.ISMANAGER);
            	
       
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            while (tr.resultSet().next()) {
                List entry = new ArrayList();
                fillResult(tr, entry);
                users.add(entry);
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null)
                tr.closeAll();
        }
        
        return users;
    }

    final static public String[] INPUT_GETUSERSADDRESS = {"userid"};
    final static public boolean[] MANDATORY_GETUSERSADDRESS = {true};
    final static public String OUTPUT_GETUSERSADDRESS = "address";
    
    /**
     * 
     * @param all
     * @param userId
     * @return
     * @throws SQLException
     */
    final static public String getUsersAddress(TcAll all, Integer userId) 
    	throws SQLException {
        String preview = null;
        
        Result tr = null;
        
        try {
            tr = 
                de.tarent.dblayer.sql.SQL.Select(TcDBContext.getDefaultContext())
                	.from(TaddressDB.getTableName())
                	.join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS)
					.join(TcDBContext.getSchemaName() + "v_user_address", "v_user_address.fk_address", TaddressDB.PK_PKADDRESS)
                	.selectAs(TaddressDB.PK_PKADDRESS)
                	.selectAs(TaddressextDB.LABELFN)
					.where(Where.and(
							Expr.equal(TaddressDB.FKUSER, userId),
							Expr.equal("v_user_address.userid", all.personalConfig().getUserID())))
                	.executeSelect(TcDBContext.getDefaultContext());
            	       
            if (tr.resultSet().next()) {
                StringBuffer sb = new StringBuffer();
                sb.append(tr.resultSet().getInt(TaddressDB.PK_PKADDRESS));
                String tmp = tr.resultSet().getString(TaddressextDB.LABELFN);
                tmp = tmp.replaceAll("#", "/;");
                sb.append(" #");
                sb.append(tmp);
                sb.append("#");
                Result r = null;
                try{
                	r = SQL.Select(TcDBContext.getDefaultContext())
                			.from(TaddresssubcategoryDB.getTableName())
                			.selectAs(TaddresssubcategoryDB.FKSUBCATEGORY)
                			.where(Expr.equal(TaddresssubcategoryDB.FKADDRESS, tr.resultSet().getInt(TaddressDB.PK_PKADDRESS)))
                			.executeSelect(TcDBContext.getDefaultContext());
                	StringBuffer sb2 = new StringBuffer();
                	while(r.resultSet().next()){
                		sb2.append(r.resultSet().getString(TaddresssubcategoryDB.FKSUBCATEGORY));
                		sb2.append(" ");
                	}
                	sb.append(sb2);
                }catch(SQLException e){
                	throw (e);
                }finally{
                	if(r != null)
                		r.close();
                }
                
                preview = sb.toString();
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null)
                tr.close();
        }
            	
        return preview;
    }
    
    private static void fillResult(Result tr, List entry) throws SQLException {
        entry.add(new Integer(tr.resultSet().getInt(TuserDB.PK_PKUSER)));
        entry.add(tr.resultSet().getString(TuserDB.LOGINNAME));
        entry.add(tr.resultSet().getString(TuserDB.LASTNAME));
        entry.add(tr.resultSet().getString(TuserDB.FIRSTNAME));
        entry.add(new Boolean(tr.resultSet().getBoolean(TuserDB.ISMANAGER)));
    }
    
}
