/**
 * 
 */
package de.tarent.groupware.category;

import java.sql.SQLException;
import java.util.List;

import de.tarent.commons.datahandling.ListFilter;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.sql.ParamValueList;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.Category;

/**
 *
 * @author Nils Neumaier, tarent GmbH
 */
public class CategoryDAO extends AbstractDAO {

	private static CategoryDAO instance = null;
	
	CategoryDBMapping mapping = new CategoryDBMapping(TcDBContext.getDefaultContext());
	CategoryEntityFactory entityFactory =  CategoryEntityFactory.getInstance();

    protected CategoryDAO() {
        super();
        setDbMapping(mapping);
        setEntityFactory(entityFactory);
    }
	
    /**
     * Returns the instance
     */
    public static synchronized CategoryDAO getInstance() {
		if(instance == null){
			instance = new CategoryDAO();
		}
		return instance;
	}
    
    
     /**
      * Returns one CatCategoryegory by
      * selected by the given primary key pk.
      * @param dbc The database context.
      * @param pk Primary key. 
      * @return One Categroy
      */
    public Category getCategoryByPk(DBContext dbc, Integer pk) throws SQLException {
        return (Category)getEntityByIdFilter(dbc, mapping.STMT_SELECT_ONE, Category.PROPERTY_ID.getKey(), pk);
    }
    

    /**
     * List all categories visible for the given user, matching the supplied search criteria.
     */
    public List getCategoriesByFilter(DBContext dbc, Integer userId, ListFilter listFilterParams) throws SQLException {
        ParamValueList paramList = new ParamValueList();
        Select selectClone = (Select)mapping.getQuery(mapping.STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES_AND_RIGHTS).clone();
        selectClone.getParams(paramList);
        paramList.setAttribute(mapping.PARAM_SELECTING_USER_ID, userId);
        return getEntityList(dbc, selectClone, listFilterParams);
    }

    public List getCategoriesForAddresses(DBContext dbc, Integer userId, List addressPks, ListFilter filter) throws SQLException{
    		ParamValueList paramList = new ParamValueList();
    		Select selectClone = (Select)mapping.getQuery(mapping.STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES_AND_RIGHTS).clone();
    		selectClone.getParams(paramList);
    		paramList.setAttribute(mapping.PARAM_SELECTING_USER_ID, userId);
    		selectClone.whereAnd(Expr.exists(getAddressPksSubSelect(dbc, addressPks)));
    		return getEntityList(dbc, selectClone, filter);
    }
    
	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDAO#setEntityKeys(de.tarent.dblayer.engine.InsertKeys, java.lang.Object)
	 */
	public void setEntityKeys(InsertKeys keys, Object entity) {
        ((Category)entity).setId(keys.getPk());
	}
    
	private SubSelect getAddressPksSubSelect(DBContext dbc, List addressPks){
		
		SubSelect sel = new SubSelect(SQL.Select(dbc)
        .from(TaddresssubcategoryDB.getTableName())
        .whereAndEq(TsubcategoryDB.PK_PKSUBCATEGORY, new RawClause(TaddresssubcategoryDB.FKSUBCATEGORY))
        .whereAnd(Expr.optimizedIn(dbc, TaddresssubcategoryDB.FKADDRESS, addressPks))
        .select(TaddresssubcategoryDB.FKSUBCATEGORY));     
		return sel;
	}
}
