/* $Id: MailSendMessage.java,v 1.7 2007/06/15 15:58:28 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.util.Map;

import javax.activation.DataSource;

/**
 * @author Simon B�hler <simon@aktionspotential.de>
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.7 $
 *
 * Repr�sentiert eine Emailnachricht
 */
public class MailSendMessage {
	/**
	 * eMail ist im Bearbeitungszustand - Anzeigen in "Entw�rfe"
	 */
	public final static Integer MAILSTATUS_BUILDING = new Integer(0);
	/**
	 * eMail ist zum versenden Freigegeben - Anzeigen im "Postausgang"
	 */
	public final static Integer MAILSTATUS_NEW 		= new Integer(1);
	/**
	 * eMail wird gerade versendet - Anzeigen im "Postausgang"
	 */
	public final static Integer MAILSTATUS_WORKING	= new Integer(2);
	/**
	 * eMail wurde versendet - Anzeigen in "Gesendet"
	 */
	public final static Integer MAILSTATUS_SENT  	= new Integer(3);
	/**
	 * eMail wurde versendet und soll nun in einen IMAP-Folder verschoben werden - Anzeigen in "Gesendet"
	 */
	public final static Integer MAILSTATUS_MOVED  	= new Integer(4);
	/**
	 * eMail wurde versendet und ggf. in einem IMAP-Folder verschoben - Anzeigen in "Gesendet"
	 */
	public final static Integer MAILSTATUS_DONE  	= new Integer(5);
	/**
	 * Beim versenden ist ein Fehler aufgetreten - Anzeigen in "Entw�rde" / "Postausgang"
	 * M�glichkeit zum bearbeiten geben.
	 */
	public final static Integer MAILSTATUS_ERROR  	= new Integer(100);
	
	private int 	mID;
	private String  mMailSubject;
	private String  mMailBody;
	private String  mMailTo;
	private String  mMailCc;
	private String  mMailBcc;
	private String  mMailFrom;
     
	private Map<String, DataSource> mAttachements;
	private boolean mRequestAnswer;
	private boolean mCopySelf;
	private int     mPriority;
	private int     mStatus;
	private String  mErrordetails;
	private int     mUserID;
	private int     mStoreID;
	
	public MailSendMessage(int id,
			String MailSubject,
			String MailBody,
			String MailTo,
			String MailCc,
			String MailBcc,
			String MailFrom,
			Map<String, DataSource> Attachements,
			boolean RequestAnswer,
			boolean CopySelf,
			int Priority,
			int userid,
			int storeid) {
		this.mID			= id;
		this.mMailSubject 	= MailSubject;
		this.mMailBody 		= MailBody;
		this.mMailTo		= MailTo;
		this.mMailCc		= MailCc;
		this.mMailBcc		= MailBcc;
		this.mMailFrom		= MailFrom;
		this.mAttachements	= Attachements;
		this.mRequestAnswer = RequestAnswer;
		this.mCopySelf 		= CopySelf;
		this.mPriority		= Priority;
		this.mUserID        = userid;
		this.mStoreID       = storeid;
	}	
	
	public MailSendMessage(String MailSubject,
			String MailBody,
			String MailTo,
			String MailCc,
			String MailBcc,
			String MailFrom,
			Map<String, DataSource> Attachements,
			boolean RequestAnswer,
			boolean CopySelf,
			int Priority,
			int userid,
			int storeid) {
		this.mMailSubject 	= MailSubject;
		this.mMailBody 		= MailBody;
		this.mMailTo		= MailTo;
		this.mMailCc		= MailCc;
		this.mMailBcc		= MailBcc;
		this.mMailFrom		= MailFrom;
		this.mAttachements	= Attachements;
		this.mRequestAnswer = RequestAnswer;
		this.mCopySelf 		= CopySelf;
		this.mPriority		= Priority;	
		this.mUserID        = userid;
		this.mStoreID       = storeid;
	}
	
	public String getMailSubject(){
		return mMailSubject;	
	}
	public String getMailBody(){
		return mMailBody;	
	}
	public String getMailTo(){
		return mMailTo;	
	}
	public String getMailCc(){
		return mMailCc;	
	}
	public String getMailBcc(){
		return mMailBcc;	
	}
	public String getMailFrom(){
		return mMailFrom;	
	}
	public Map<String, DataSource> getMailAttachment(){
		return mAttachements;	
	}
	public boolean getRequestAnswer(){
		return mRequestAnswer;	
	}
	public boolean getCopySelf(){
		return mCopySelf;	
	}
	public int getPriority(){
		return mPriority;
	}
	public int getStatus(){
		return mStatus;
	}
	public String getErrordetails(){
		return mErrordetails;
	}
	
	public void setStatus(int Status){
		mStatus = Status;
	}
	public void setErrordetails(String Errordetails){
		mErrordetails = Errordetails;
	}
	
	public int getId(){
		return mID;
	}

	public int setId(Integer newMID){
		return this.mID = newMID.intValue();
	}

	public int setId(int newMID){
		return this.mID = newMID;
	}
	
	public int getUserId() {
		return mUserID;
	}
	
	public int getStoreId() {
		return mStoreID;
	}
	
	public String toString() {
		return new StringBuffer()
			.append(super.toString())
			.append(" [ID=").append(mID)
			.append(",subject=").append(mMailSubject)
			.append(",body=").append(mMailBody)
			.append(",to=").append(mMailTo)
			.append(",cc=").append(mMailCc)
			.append(",bcc=").append(mMailBcc)
			.append(",from=").append(mMailFrom)
			.append(",attachment=").append(mAttachements)
			.append(",requestanswer=").append(mRequestAnswer)
			.append(",copyself=").append(mCopySelf)
			.append(",priority=").append(mPriority)
			.append(",status=").append(mStatus)
			.append(",error=").append(mErrordetails)
			.append(",userid=").append(mUserID)
			.append(",storeid=").append(mStoreID)
			.toString();
	}
}
