/* $Id: MailSendWorker.java,v 1.8 2007/03/22 19:29:11 aleksej Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.commons.logging.LogFactory;
import de.tarent.groupware.mail.MailException.Database;
import de.tarent.groupware.mail.MailException.JobAlreadyEnqueuedException;
import de.tarent.groupware.utils.ListProperties;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * @author Simon B�hler <simon@aktionspotential.de>
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.8 $
 */
public class MailSendWorker implements TcContentWorker {
    
    private final static Log logger = LogFactory.getLog(MailSendWorker.class);

    public final static String ACTION_CREATEJOB = "createNewJob";
	public final static String ACTION_ADDMESSAGETOJOB = "addMessage";
	public final static String ACTION_ENQUEUEJOB = "enqueueJob";
	public final static String ACTION_WRITEMAIL = "mailWrite";
	public final static String ACTION_MAILIST = "mailList";
	public final static String ACTION_REMOVEMESSAGE = "removeMessage";

	//	public final static String CONTENT_JOBID = "jobid";
	//	public final static String CONTENT_MAILID = "mailid";
	public final static String CONTENT_MAILLIST = "mailTransferList";
	public final static String CONTENT_MAILLISTINFO = "mailTransferListInfo";
	public final static String CONTENT_MAILDETAILS = "mailTransferDetails";
	public final static String CONTENT_MAILADDRESS = "mailAddress";

	public final static String PARAM_JOBID = "jobid";
	public final static String PARAM_MAILID = "mailid";

	public final static String PARAM_MAILACTION_SAVE = "MailSave"; // key, value wird ignoriert
	public final static String PARAM_MAILACTION_SEND = "MailSend"; // key, value wird ignoriert
	public final static String PARAM_MAILACTION_ADDATTACHMENT = "AttachmentAdd"; // key, value mit Daten als Map
	public final static String PARAM_MAILACTION_REMOVEATTACHMENT = "AttachmentRemove"; // key, value mit ID des Attachments

	public final static String PARAM_MAILSUBJECT = "MailSubject";
	public final static String PARAM_MAILBODY = "MailBody";
	public final static String PARAM_MAILFROM = "MailFrom";
	public final static String PARAM_MAILTO = "MailTo";
	public final static String PARAM_MAILCC = "MailCc";
	public final static String PARAM_MAILBCC = "MailBcc";
	public final static String PARAM_REQUESTANSWER = "RequestAnswer";
	public final static String PARAM_ATTACHMENTS = "Attachements";
	public final static String PARAM_COPYSELF = "CopySelf";
	public final static String PARAM_PRIORITY = "Priority";

	final static String PARAM_ATTACHMENT_TEMP_PATH = "attachmentTempDir";
	final static String PARAM_STORE_ATTACHMENTS_IN_FILESYSTEM = "storeAttachmentsInFilesystem";
	
	
	public MailSendWorker() {
		super();
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#doAction(de.tarent.octopus.config.TcConfig, java.lang.String, de.tarent.octopus.request.TcRequest, de.tarent.octopus.content.TcContent)
	 */
	public String doAction(TcConfig tcConfig, String actionName, TcRequest tcRequest, TcContent tcContent) throws TcContentProzessException {
		String result;
		logger.debug("=================================");
		logger.debug("[action]: " + actionName);
		logger.debug("=================================");
		String module = tcRequest.getModule();
		Map userData = (Map)tcConfig.getSessionValueAsObject("userData");
		Integer userId = null;
		if (userData != null && userData.containsKey("id")) {
			// webclient aus session
			userId = (Integer)userData.get("id");
		} else {
			try {
				// soap aus datenbank
				userId = MailDB.getUserId(module, tcRequest.get("username"));
			} catch (Database e) {
				e.printStackTrace();
			}
		}
		
		int storeId = tcRequest.getParamAsInt("store");

		try {
		if (actionName.equals(ACTION_CREATEJOB)) {
			tcContent.setField(PARAM_JOBID, MailSendDB.createNewJob(module, new Integer(tcRequest.get("userid"))));
			result = RESULT_ok;

		} else if (actionName.equals(ACTION_ADDMESSAGETOJOB)) {
			try {
				jobEditable(module, new Integer(tcRequest.getParamAsInt(PARAM_JOBID)));
				MailSendDB.addMessagetoJob(
					module,
					null,
					new Integer(tcRequest.getParamAsInt(PARAM_JOBID)),
					new MailSendMessage(
						tcRequest.get(PARAM_MAILSUBJECT),
						tcRequest.get(PARAM_MAILBODY),
						tcRequest.get(PARAM_MAILTO),
						tcRequest.get(PARAM_MAILCC),
						tcRequest.get(PARAM_MAILBCC),
						tcRequest.get(PARAM_MAILFROM),
						null,
						tcRequest.getParameterAsBoolean(PARAM_REQUESTANSWER),
						tcRequest.getParameterAsBoolean(PARAM_COPYSELF),
						tcRequest.getParamAsInt(PARAM_PRIORITY),
						userId.intValue(),
						storeId));
				result = RESULT_ok;
			} catch (MailException.JobAlreadyEnqueuedException e) {
				result = RESULT_error;
			}

		} else if (actionName.equals(ACTION_ENQUEUEJOB)) {
			try {
				jobEditable(module, new Integer(tcRequest.getParamAsInt(PARAM_JOBID)));
				MailSendDB.enqueueJob(
						module,
						new Integer(tcRequest.getParamAsInt(PARAM_JOBID)),
						(Timestamp)tcRequest.getParam("runDate"));
				result = RESULT_ok;
			} catch (MailException.JobAlreadyEnqueuedException e) {
				result = RESULT_error;
			}

		} else if (actionName.equals(ACTION_REMOVEMESSAGE)) {
			MailSendDB.removeMessage(
					module,
					userId,
					new Integer(tcRequest.getParamAsInt(PARAM_JOBID)),
					new Integer(tcRequest.getParamAsInt(PARAM_MAILID)));
			result = RESULT_ok;

		} else if (actionName.equals(ACTION_WRITEMAIL)) {
			try {
				Map request = tcRequest.getRequestParameters();
				Integer jobid = new Integer(tcRequest.getParamAsInt(PARAM_JOBID));
				Integer mailid = new Integer(tcRequest.getParamAsInt(PARAM_MAILID));

				String name = tcConfig.getPersonalConfig().getUserName();
				String userMailAddress = tcConfig.getPersonalConfig().getUserEmail();
				if (userMailAddress == null || userMailAddress.length() == 0) {
					return RESULT_error;
				} else if (name != null && name.length() != 0) {
					userMailAddress = name + " <" + userMailAddress + ">";
				}
				tcContent.setField(CONTENT_MAILADDRESS, userMailAddress);

				if (request.containsKey(PARAM_MAILACTION_SAVE)) {
					if (jobid.intValue() == 0) {
						// Job erzeugen falls noch keiner existiert
						jobid = MailSendDB.createNewJob(module, userId);
					} else {
						jobEditable(module, jobid);
					}

					if (mailid.intValue() == 0) {
						// eMail hinzuf�gen
						mailid = MailSendDB.addMessagetoJob(
							module,
							userId,
							jobid,
							new MailSendMessage(
								tcRequest.get(PARAM_MAILSUBJECT),
								tcRequest.get(PARAM_MAILBODY),
								tcRequest.get(PARAM_MAILTO),
								tcRequest.get(PARAM_MAILCC),
								tcRequest.get(PARAM_MAILBCC),
								userMailAddress,
								null,
								tcRequest.getParameterAsBoolean(PARAM_REQUESTANSWER),
								tcRequest.getParameterAsBoolean(PARAM_COPYSELF),
								tcRequest.getParamAsInt(PARAM_PRIORITY),
								userId.intValue(),
								storeId));
					} else {
						// eMail bearbeiten
						MailSendDB.updateMessage(
							module,
							mailid,
							new MailSendMessage(
								tcRequest.get(PARAM_MAILSUBJECT),
								tcRequest.get(PARAM_MAILBODY),
								tcRequest.get(PARAM_MAILTO),
								tcRequest.get(PARAM_MAILCC),
								tcRequest.get(PARAM_MAILBCC),
								userMailAddress,
								null,
								tcRequest.getParameterAsBoolean(PARAM_REQUESTANSWER),
								tcRequest.getParameterAsBoolean(PARAM_COPYSELF),
								tcRequest.getParamAsInt(PARAM_PRIORITY),
								userId.intValue(),
								storeId));
					}
				}
				
				if (request.containsKey(PARAM_MAILACTION_ADDATTACHMENT)) {
					// eMail-Anhang hinzuf�gen
					try {
						String filename;
						InputStream filestream;
						Long filesize;
						if (TcRequest.isWebType(tcRequest.getRequestType())) {
							Map attachment = (Map) request.get(PARAM_MAILACTION_ADDATTACHMENT);
							filename = (String)attachment.get(TcRequest.PARAM_FILE_CONTENT_NAME);
							filestream = (InputStream)attachment.get(TcRequest.PARAM_FILE_CONTENT_STREAM);
							filesize = ((Long)attachment.get(TcRequest.PARAM_FILE_CONTENT_SIZE));
						} else {
							filename = (String)tcRequest.getParam("filename");
							filestream = new ArrayInputStream(Arrays.asList((Object[])tcRequest.getParam("filestream")));
							filesize = (Long)tcRequest.getParam("filesize");
						}
                        boolean storeAttachmentsInFilesSystem = Boolean.valueOf(tcConfig.getModuleConfig().getParam(PARAM_STORE_ATTACHMENTS_IN_FILESYSTEM));
                        if ( storeAttachmentsInFilesSystem ) {//attachments => file system
                                MailSendDB.addAttachmentStreamAsFile( tcConfig.getModuleConfig().getParam( PARAM_ATTACHMENT_TEMP_PATH ), 
                                                                      module, userId, jobid, mailid,
                                                                      filename, filestream, filesize.intValue() );
                        } else {//attachments => DB
                                MailSendDB.addAttachmentStreamAsBLOB( module, userId, jobid, mailid, filename,
                                                                      filestream, filesize.intValue() );
                        }

                        
					} catch (IOException e) {
						logger.error("failed adding attachment");
					}
				}
				if (request.containsKey(PARAM_MAILACTION_REMOVEATTACHMENT)) {
					// eMail-Anhang l�schen
					try {
						Integer attachmentid = new Integer(request.get(PARAM_MAILACTION_REMOVEATTACHMENT).toString());
						MailSendDB.removeAttachment(module, userId, jobid, mailid, attachmentid);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (request.containsKey(PARAM_MAILACTION_SEND)) {
					// eMail zum senden freigeben
					MailSendDB.enqueueJob(module, jobid, new Timestamp(System.currentTimeMillis()));
				}

				tcContent.setField(CONTENT_MAILDETAILS, MailSendDB.getMessageDetails(module, userId, jobid, mailid));
				result = RESULT_ok;
				
			} catch (MailException.JobAlreadyEnqueuedException e) {
				e.printStackTrace();
				result = RESULT_error;
			}

		} else if (actionName.equals(ACTION_MAILIST)) {
			String filter = tcRequest.getParamAsString("filter");
			int from = 0;
			int size = 0;
			
			TcAll all = new TcAll(tcRequest, tcContent, tcConfig);
			MailWorker.pre(all, actionName);
			if (all.contentContains(MailConstants.PARAM_INDEXSIZE)) {
				from = tcRequest.getParamAsInt(MailConstants.PARAM_INDEXFROM);
				size = ((Integer)all.contentAsObject(MailConstants.PARAM_INDEXSIZE)).intValue();
			}
			ListProperties lp = MailSendDB.getMessageList(module, userId, from, size, filter);
			tcContent.setField(CONTENT_MAILLIST, lp.getList());
			tcContent.setField(CONTENT_MAILLISTINFO, lp.getMap());
			result = RESULT_ok;

		} else {
			throw new TcContentProzessException("Nicht unterst�tzte Aktion im Worker 'SMTPWorker': " + actionName);
		}
		return result;
		} catch (MailException.Database e) {
			e.printStackTrace();
			return RESULT_error;
		}
	}

	private void jobEditable(String module, Integer job) throws JobAlreadyEnqueuedException, Database {
		Integer status = MailSendDB.getJobStatus(module, job);
		if (status.equals(MailConstants.JOBSTATUS_WORKING) || status.equals(MailConstants.JOBSTATUS_DONE)) {
			throw new MailException.JobAlreadyEnqueuedException();
		}
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#getWorkerDefinition()
	 */
	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port = new TcPortDefinition("de.tarent.contact.octopus.SMTPWorker", "Worker zum Emailversand.");

		TcOperationDefinition operation1 = port.addOperation(ACTION_CREATEJOB, "Versandauftrag erstellen.");
		operation1.setInputMessage();
		operation1.setOutputMessage();

		TcOperationDefinition operation2 = port.addOperation(ACTION_ADDMESSAGETOJOB, "Nachricht zum Versandauftrag hinzuf�gen.");
		operation2.setInputMessage();
		operation2.setOutputMessage();

		TcOperationDefinition operation3 = port.addOperation(ACTION_ENQUEUEJOB, "Versandauftrag zum Versand freigeben.");
		operation3.setInputMessage();
		operation3.setOutputMessage();
		return port;
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.8 $") + " (" + CVS.getContent("$Date: 2007/03/22 19:29:11 $") + ')';
	}
	
	
	
	// Hilfsklassen
	/**
	 * Wandelt eine Liste mit Byte-Arrays in einen InputStream um:
	 * 
	 * List _list = [ byte[100], byte[100], byte[23] ]
	 * 
	 * kann dann als InputStream mit read gelesen werden.
	 */
	private static class ArrayInputStream extends InputStream {
		private List _list;
		private int _list_len;
		private int _list_pos;
		private byte _block[];
		private int _block_len;
		private int _block_pos;
		
		public ArrayInputStream(List list) throws IOException {
			super();
			_list = list;
			_list_len = list.size() - 1;
			_list_pos = -1;
			nextBlock();
		}

		public int read() throws IOException {
			return -1;
		}
		
	    public int read(byte b[], int off, int len) throws IOException {
	    	if (off != 0)
	    		skip(off);
	    	
	    	int i = 0;
	    	while (i < len) {
	    		if (_block_pos == _block_len) {
		    		if (_list_pos == _list_len)
		    			break;
		    		else
		    			nextBlock();
	    		}
	    		b[i] = _block[_block_pos];
	    		_block_pos++;
	    		i++;
	    	}
	    	return i;
	    }
	    
	    private void nextBlock() throws IOException {
	    	try {
	    		_block = (byte[])_list.get(++_list_pos);
	    		_block_len = _block.length;
	    		_block_pos = 0;
	    	} catch (IndexOutOfBoundsException e) {
	    		throw new IOException(e.toString());
	    	}
	    }
	}
}
