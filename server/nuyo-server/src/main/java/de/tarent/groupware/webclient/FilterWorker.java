/**
 * 
 */
package de.tarent.groupware.webclient;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.address.AddressEntityDescription;
import de.tarent.groupware.utils.EntityDescription;
import de.tarent.groupware.utils.GenericSelector;
import de.tarent.octopus.server.OctopusContext;

/**
 * @author jens
 *
 */
public class FilterWorker {
	
	public static final String[] INPUT_copyCategoryWorkerData = { "adrId" };
    public static final boolean[] MANDATORY_copyCategoryWorkerData = { true };
    public static final String OUTPUT_copyCategoryWorkerData = "userid";
    
    public Integer copyCategoryWorkerData(OctopusContext oc, Integer adrId) throws SQLException {
    	Result result;
    	Map subcategoryMap = new HashMap();
    	try {
            result = DB.result(TcDBContext.getDefaultContext(), "SELECT tsubfolder.pk_subfolder AS id, tsubfolder.foldername AS foldername FROM " + TcDBContext.getSchemaName() + "tsubfolder");
            try {
            ResultSet resultSet = result.resultSet();
            
            while (resultSet.next()) {
            	subcategoryMap.put(resultSet.getObject("id"), resultSet.getObject("foldername"));
            }
            } finally {
				if (result != null)
					result.close();
			}
        } catch (SQLException e) {
			Lg.SQL(oc.getModuleName()).error(e.getMessage(), e);
			throw e;
        }
			
    	oc.setContent("addressID", adrId);
    	oc.setContent("subcategoryMap", subcategoryMap);
    	return oc.getConfigObject().getPersonalConfig().getUserID();        
    }
	
	public static final String[] INPUT_filterSingleAddress = { "CONTENT:filter", "adrId" };
    public static final boolean[] MANDATORY_filterSingleAddress = { false, true };
    public static final String OUTPUT_filterSingleAddress = "filter";
    
    public List filterSingleAddress(OctopusContext oc, List<Object> filter, int adrId) {
    	filter = new ArrayList<Object>();
    	filter.add("adrNr");
        filter.add(new Integer(adrId));
        filter.add("=");
        return filter;        
    }
    
    public static final String[] INPUT_updateListFilter = { "CONTENT:"+GenericSelector.FILTER_LIST, "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD };
    public static final boolean[] MANDATORY_updateListFilter = { false, true };
    public static final String OUTPUT_updateListFilter = "SESSION:"+GenericSelector.FILTER_LIST;
    
    public List updateListFilter(OctopusContext oc, List<Object> filter, EntityDescription entityDescription) {
    	// select only addresses primary key
    	List<String> columnNames = new ArrayList<String>(1);
    	columnNames.add(AddressEntityDescription.ADR_NR);
    	oc.setContent(GenericSelector.PROJECTION_FIELDS, columnNames);
    	
    	Object asdasd = oc.sessionAsObject(Session.SESSION_LIST_ORDER_ASC);
    	Boolean kaka = (Boolean) oc.sessionAsObject(Session.SESSION_LIST_ORDER_ASC);
    	
    	Order orderBy;
    	if ((Boolean)oc.sessionAsObject(Session.SESSION_LIST_ORDER_ASC))
    		orderBy = Order.asc(entityDescription.getDBKeyByProperty(oc.sessionAsString(Session.SESSION_LIST_ORDER)));
    	else
    		orderBy = Order.desc(entityDescription.getDBKeyByProperty(oc.sessionAsString(Session.SESSION_LIST_ORDER)));
    	if (oc.sessionAsString(Session.SESSION_LIST_ORDER).equals("strasse"))
    		if ((Boolean)oc.sessionAsObject(Session.SESSION_LIST_ORDER_ASC))
        		orderBy = orderBy.andAsc(entityDescription.getDBKeyByProperty("hausNr"));
        	else
        		orderBy = orderBy.andDesc(entityDescription.getDBKeyByProperty("hausNr"));
    	oc.setSession(GenericSelector.ORDER_BY, orderBy);
    	oc.setContent(GenericSelector.ORDER_BY, orderBy);
    	
    	if ((Boolean)oc.sessionAsObject(Session.SESSION_SEARCH_ENABLED)) {
	    	filter = new ArrayList<Object>();
	    	
	    	// select comparator (only one needed now)
	    	String comparator = "ILIKE";
	    	//if (oc.sessionAsObject(Session.SESSION_SEARCH_MATCHCODESTR) != null && ((Boolean)oc.sessionAsObject(Session.SESSION_SEARCH_MATCHCODESTR)).booleanValue())
	    			    	
	    	int conditionCounter = 0;
	        for (Map.Entry<String, String> mappingInputToEntityDescription: Session.SEARCH_INPUT_ENTITY_MAPPING.entrySet()) {
	        	String input = oc.sessionAsString(mappingInputToEntityDescription.getKey());
	        	if (input != null && input != "") {
	        		conditionCounter++;
	        		filter.add(mappingInputToEntityDescription.getValue());
	        		filter.add(input.replace('*', '%'));
	        		filter.add(comparator);
	        		if (conditionCounter > 1)
	        			filter.add("AND");
	        	}
	        }
    	}
        return filter;        
    }
    
    public static final String[] INPUT_updateNavigationList = { "SESSION:"+Session.SESSION_PAGE, "SESSION:"+Session.SESSION_KEY_LIST,
    															"SESSION:"+Session.SESSION_ENTRIES_PER_PAGE};
    public static final boolean[] MANDATORY_updateNavigationList = { false, true, true };
    public static final String OUTPUT_updateNavigationList = "navigation";
    
    public Map updateNavigationList(OctopusContext oc, Integer page, List addressKeyList, Integer entriesPerPage) {
    	oc.setContent(GenericSelector.SELECT_PARAM_LIMIT, entriesPerPage);
    	if (page == null)
    		oc.setContent(GenericSelector.SELECT_PARAM_OFFSET, 0);
    	else
    		oc.setContent(GenericSelector.SELECT_PARAM_OFFSET, entriesPerPage * page);
    	
    	Map<String, Object> navigation = new HashMap<String, Object>(3);
    	navigation.put("currentEntry", page+1);
    	navigation.put("numberEntries", addressKeyList.size() / entriesPerPage + 1);
    	int previousEntry = page - 1 < 0 ? -1 : page - 1;  
    	navigation.put("previousEntry", previousEntry);
    	int nextEntry = (page + 1) * entriesPerPage > addressKeyList.size() ? -1 : page + 1;  
    	navigation.put("nextEntry", nextEntry);
    	int lastEntry = nextEntry == -1 ? -1 : addressKeyList.size() / entriesPerPage;  
    	navigation.put("lastEntry", lastEntry);    	
    	    	
    	return navigation;
    }
    
    public static final String[] INPUT_updateNavigationSingle = { "SESSION:"+Session.SESSION_ADRESS_ID, "SESSION:"+Session.SESSION_KEY_LIST };
    public static final boolean[] MANDATORY_updateNavigationSingle = { true, true };
    public static final String OUTPUT_updateNavigationSingle = "navigation";
    
    public Map updateNavigationSingle(OctopusContext oc, Integer adrId, List addressKeyList) {
    	int i = 0;
    	for (i=0; i<addressKeyList.size(); i++) {
    		if (((Integer)((List)addressKeyList.get(i)).get(0)).equals(adrId))
    			break;
    	}
    	
    	Map<String, Object> navigation = new HashMap<String, Object>(3);
    	navigation.put("currentEntry", i+1);
    	navigation.put("numberEntries", addressKeyList.size());
    	int firstEntry = i < 1 ? -1 : (Integer)((List)addressKeyList.get(0)).get(0);  
    	navigation.put("firstEntry", firstEntry);
    	int previousEntry = i < 1 ? -1 : (Integer)((List)addressKeyList.get(i-1)).get(0);  
    	navigation.put("previousEntry", previousEntry);
    	int nextEntry = i+1 >= addressKeyList.size() ? -1 : (Integer)((List)addressKeyList.get(i+1)).get(0);  
    	navigation.put("nextEntry", nextEntry);
    	int lastEntry = nextEntry == -1 ? -1 : (Integer)((List)addressKeyList.get(addressKeyList.size()-1)).get(0);
    	navigation.put("lastEntry", lastEntry);
    	
    	return navigation;
    }
    
    /*public static final String[] INPUT_addEmptyFilter = {};
    public static final boolean[] MANDATORY_addEmptyFilter = {};
    public static final String OUTPUT_addEmptyFilter = "filter";
    
    public List addEmptyFilter() {
    	return new ArrayList(0);
    }*/
    
    public static final String[] INPUT_filterCategories = { 	"CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD,
    															"SESSION:"+Session.SESSION_CATEGORY, 
    															"SESSION:"+Session.SESSION_SUBCATEGORY };
    public static final boolean[] MANDATORY_filterCategories = { true, true, false };
    public static final String OUTPUT_filterCategories = "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD;
    
    public Select filterCategories(OctopusContext oc, Select select, Integer category, Integer subcategory) {
    	if (subcategory.intValue() != 0 && category != -1) {
    		select.from(TcDBContext.getSchemaName() + "taddresssubfolder")
    				.whereAnd(Where.and(	new RawClause("taddresssubfolder.fk_address="+TaddressDB.PK_PKADDRESS),
											Expr.equal("taddresssubfolder.fk_subfolder", subcategory)));
    	} else if (category != -1) {
    		select.from(TcDBContext.getSchemaName() + "taddressfolder")
					.whereAnd(Where.and(	new RawClause("taddressfolder.fk_address="+TaddressDB.PK_PKADDRESS),
											Expr.equal("taddressfolder.fk_folder", category)));
    	}
    	 	
    	return select;
    }
    
    public static final String[] INPUT_filterSubCategoryList = { 	"CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD,
																	"SESSION:"+Session.SESSION_CATEGORY	}; 
    public static final boolean[] MANDATORY_filterSubCategoryList = { true, true };
    public static final String OUTPUT_filterSubCategoryList = "CONTENT"+GenericSelector.CURRENT_SELECT_FIELD;

    public Select filterSubCategoryList(OctopusContext oc, Select select, Integer category) {
    	select.whereAnd(Expr.equal("tsubfolder.fk_folder", category));
    	return select;
    }
}
