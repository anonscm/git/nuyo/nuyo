package de.tarent.groupware.impl;

import java.util.HashMap;

import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.groupware.SubCategory;
import de.tarent.groupware.SubCategoryProperty;

/**
 * 
 * @author Nils Neumaier, tarent GmbH
 *
 */

public class SubCategoryImpl implements SubCategory {
	
	
	protected HashMap data = new HashMap();
	
		
	
	/**
	 * Diese Methode wendet auf den Parameter {@link Object#toString()} an,
	 * sofern er nicht <code>null</code> ist.
	 * 
	 * @param o das in einen String zu ueberfuehrende Objekt.
	 * @return die String-Darstellung des Objekts. 
	 */    
	public static String saveToString(Object o) {
		return (o == null) ? null : o.toString();
	}	

    /**
     * Returns the int value of the object or 0 as default
     */
	public static int saveToInt(Object o) {
		return (o instanceof Integer) ? ((Integer)o).intValue() : 0;
	}	

	public boolean equals(Object subcategory){
		return (subcategory instanceof SubCategory && equals((SubCategory)subcategory));
	}
	
	public boolean equals(SubCategory subcategory){
		return (this.getId() == subcategory.getId());
	}

	public void fill(SubCategory sourceSubCategory) {
		// TODO Auto-generated method stub
		
	}

	public Object getAttribute(String id) {
		return getStandardData(id);
	}

	public String getDescription() {
		return saveToString(getStandardData(PROPERTY_DESCRIPTION));
	}

	public int getId() {
		return saveToInt(getStandardData(PROPERTY_ID));
	}

	public Integer getParentCategory() {
		return saveToInt(getStandardData(PROPERTY_PARENTCATEGORY));
	}

	public Object getStandardData(String id) {
		return data.get(id);
	}

	public Object getStandardData(SubCategoryProperty prop) {
		return getStandardData(prop.getKey()); 
	}

	public String getSubCategoryName() {
		return saveToString(getStandardData(PROPERTY_SUBCATEGORYNAME));
	}

	public void setAttribute(String id, Object newValue) throws EntityException {
		setStandardData(id, newValue);
	}

	public void setDescription(String description) {
		setStandardData(PROPERTY_DESCRIPTION, description);
	}

	public void setId(int newid) {
		setStandardData(PROPERTY_ID, new Integer(newid));
	}

	public void setParentCategory(Integer parent) {
		setStandardData(PROPERTY_PARENTCATEGORY, parent);
	}

	public void setStandardData(String id, Object newValue) {
		data.put(id, newValue);
	}

	public void setStandardData(SubCategoryProperty prop, Object newValue) {
		data.put(prop.getKey(), newValue);
	}

	public void setSubCategoryName(String subcategoryname) {
		setStandardData(PROPERTY_SUBCATEGORYNAME, subcategoryname);		
	}
}
