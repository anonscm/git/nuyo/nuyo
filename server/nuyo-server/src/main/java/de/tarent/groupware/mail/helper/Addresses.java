/*
 * Created on 26.10.2004
 */
package de.tarent.groupware.mail.helper;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class Addresses {
	private static final String ISO_START = "<";
	private static final String ISO_END = ">";
	private static final String ISO_QUOT = "\"";

	private Addresses() {
	}

	/**
	 * Gibt den Namen der Adresse zur�ck.
	 * 
	 * @param address
	 * @return name
	 */
	public static String getName(Address address) {
		if (address != null)  {
			String result;
			try {
				result = new InternetAddress(address.toString()).toUnicodeString();
			} catch (AddressException e) {
				result = address.toString();
			}
			int lt = result.lastIndexOf(ISO_START);
			int gt = result.lastIndexOf(ISO_END);
			if (lt > 1 && gt > 1 && lt < gt) {
				result = result.substring(0, lt - 1);
				if (result.startsWith(ISO_QUOT))
					result = result.substring(1);
				if (result.endsWith(ISO_QUOT))
					result = result.substring(0, result.length() - 1);
			}
			return StringHelper.trim(result);
		}
		return null;
	}

	/**
	 * Gibt die eMail der Adresse zur�ck.
	 * 
	 * @param address
	 * @return email
	 */
	public static String getMail(Address address) {
		if (address != null)  {
			String result;
			try {
				result = new InternetAddress(address.toString()).toUnicodeString();
			} catch (AddressException e) {
				result = address.toString();
			}
			int lt = result.lastIndexOf(ISO_START);
			int gt = result.lastIndexOf(ISO_END);
			if (lt > 1 && gt > 1 && lt < gt) {
				result = result.substring(lt + 1, gt);
			}
			return StringHelper.trim(result);
		}
		return null;
	}
}
