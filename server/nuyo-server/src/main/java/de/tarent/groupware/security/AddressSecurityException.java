/*
 * Created on 02.08.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.tarent.groupware.security;

import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import de.tarent.octopus.security.TcSecurityException;

/**
 * @author Nils Neumaier, tarent GmbH
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AddressSecurityException extends TcSecurityException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 292858292366341927L;

	// Kategoriebezogene Rechte
	
	public static final int		ERROR_NO_AUTHORISATION_TO_READ_ADDRESS					= 100;

	public static final int		ERROR_NO_AUTHORISATION_TO_EDIT_ADDRESS					= 101;

	public static final int		ERROR_NO_AUTHORISATION_TO_ADD_ADDRESS					= 102;

	public static final int		ERROR_NO_AUTHORISATION_TO_REMOVE_ADDRESS				= 103;

	public static final int		ERROR_NO_AUTHORISATION_TO_ADD_SUBCATEGORY				= 104;
	
	public static final int		ERROR_NO_AUTHORISATION_TO_DELETE_SUBCATEGORY			= 105;
	
	public static final int 	ERROR_NO_AUTHORISATION_TO_EDIT_AUTHORISATIONS			= 106;
	
	
	// Globale Rechte
	
	public static final int		ERROR_NO_AUTHORISATION_TO_EDIT_USER						= 107;
	
	public static final int		ERROR_NO_AUTHORISATION_TO_DELETE_USER 					= 108;
	
	public static final int		ERROR_NO_AUTHORISATION_TO_EDIT_CATEGORY					= 109;

	public static final int		ERROR_NO_AUTHORISATION_TO_DELETE_CATEGORY				= 110;

	public static final int		ERROR_NO_AUTHORISATION_TO_EDIT_SCHEDULE					= 111;
	
	public static final int		ERROR_NO_AUTHORISATION_TO_DELETE_SCHEDULE				= 112;
	
	public static final int		ERROR_NO_AUTHORISATION_TO_DELETE_ADDRESS				= 113; // Hier wird die Adresse wirklich gel�scht und nicht nur aus einer Kategorie entfernt


	// Kalender Rechte
	
	public static final int 	ERROR_NO_AUTHORISATION_TO_SEE_SCHEDULE					= 114;

	public static final int 	ERROR_NO_AUTHORISATION_TO_READ_SCHEDULE					= 115;

	public static final int 	ERROR_NO_AUTHORISATION_TO_WRITE_SCHEDULE				= 116;

	//	 Sonderf�lle	
	
	public static final int		ERROR_ADMIN_AUTHORISIATION_REQUIRED						= 117; // Effektiv erh�lt man f�r verschiedene Aktionen diese Berchtigung, wenn man User editieren darf
	
	public static final int		ERROR_NO_AUTHORISATION_TO_READ_ALL_REQUESTED_ADDRESSES	= 118;

	public static final int 	ERROR_NO_AUTHORISATION_TO_CHANGE_ASSIGNMENT			    = 119;

	public static final int 	ERROR_INVALID_ID			                            = 120;

	
	
	// id der Adress/Kategorie/Kalender bei dem der Fehler aufgetreten ist
	protected int object_Id;
	protected List object_Ids;
	protected int user_Id;
	protected int errorCode;
	protected String userName;
	
	public QName getSoapFaultCode() {
        if (errorCode >= 100)
            return new QName("http://schemas.tarent.de/contact", "Client.access.violation."+errorCode);
        else
            return super.getSoapFaultCode();
    }

	protected String getMessageByErrorCode(int errorCode) {
		String user = getUserName() == null ? getUser_Id()+"" : getUserName();
		String id = getObject_Id()==-1 ? "" : ""+ getObject_Id(); 
		
		if (errorCode == ERROR_NO_AUTHORISATION_TO_READ_ADDRESS)
			return "User " + user + " hat versucht die Adresse " + id + " ohne Berechtigung zu lesen"; 

		if (errorCode == ERROR_NO_AUTHORISATION_TO_EDIT_ADDRESS)
			
			return "User " + user + " hat versucht die Adresse " + id + " ohne Berechtigung zu ver�ndern";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_ADD_ADDRESS)
			return "User " + user + " hat versucht ohne Berechtigung eine Adresse in Kategorie " + id + " anzulegen";
		
		if (errorCode == ERROR_NO_AUTHORISATION_TO_REMOVE_ADDRESS)
			return "User " + user + " hat versucht die Adresse " + id + " ohne Berechtigung zu l�schen"; 

		if (errorCode == ERROR_NO_AUTHORISATION_TO_READ_ALL_REQUESTED_ADDRESSES){
			StringBuffer buff = new StringBuffer();
			buff.append("User " + user + " hat versucht eine Menge von Adressen zu lesen \nAuf mindestens eine der Adressen besteht kein Leserecht - Aktion abgebrochen\n");
			if (getObject_Ids() != null){
				buff.append("Auf folgenden Addressen existiert keine Leseberechtigung: ");
				for (Iterator iter = getObject_Ids().iterator(); iter.hasNext(); ){
					buff.append(iter.next().toString());
					if (iter.hasNext())
						buff.append(",");
					else
						buff.append(".");
				}
			}
			return buff.toString();
		}

		if (errorCode == ERROR_NO_AUTHORISATION_TO_EDIT_CATEGORY)
			return "User " + user + " hat versucht eine Kategorie ohne Berechtigung zu ver�ndern";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_DELETE_CATEGORY)
			return "User " + user + " hat versucht die Kategorie ohne Berechtigung zu l�schen";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_ADD_SUBCATEGORY)
			return "User " + user + " hat versucht die Unterkategorie " + id + " ohne Berechtigung anzulegen";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_DELETE_SUBCATEGORY)
			return "User " + user + " hat versucht die Unterkategorie " + id + " ohne Berechtigung zu l�schen";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_EDIT_USER)
			return "User " + user + " hat versucht einen Benutzeraccount ohne Berechtigung zu ver�ndern/anzulegen bzw. eine andere Aktion auszuf�hren, die dieses Recht voraussetzt";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_DELETE_USER)
			return "User " + user + " hat versucht einen Benutzeraccount ohne Berechtigung zu l�schen";

		if (errorCode == ERROR_ADMIN_AUTHORISIATION_REQUIRED)
			return "User " + user + " hat versucht ohne Administratorrechte eine Aktion durchzuf�hren, die Administratorrechte verlangt";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_DELETE_ADDRESS)
			return "User " + user + " hat versucht die Adresse " + id + " ohne entsprechende Berechtigung zu l�schen";
		
		if (errorCode == ERROR_NO_AUTHORISATION_TO_EDIT_SCHEDULE)
			return "User " + user + " hat versucht einen Kalender ohne Berechtigung zu ver�ndern/anzulegen";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_DELETE_SCHEDULE)
			return "User " + user + " hat versucht einen Kalender ohne Berechtigung zu l�schen";
		
		if (errorCode == ERROR_NO_AUTHORISATION_TO_SEE_SCHEDULE)
			return "User " + user + " hat versucht den Kalender " + id + " ohne Berechtigung anzuzeigen";
		
		if (errorCode == ERROR_NO_AUTHORISATION_TO_READ_SCHEDULE)
			return "User " + user + " hat versucht den Kalender "+ id + " ohne Berechtigung zu lesen";
		
		if (errorCode == ERROR_NO_AUTHORISATION_TO_WRITE_SCHEDULE)
			return "User " + user + " hat versucht ohne Berechtigung in dem Kalender " + id + " zu schreiben";

		if (errorCode == ERROR_NO_AUTHORISATION_TO_CHANGE_ASSIGNMENT)
			return "User " + user + " hat versucht ohne Berechtigung die Kategoriezuordnung von Adresse " + id + " zu �ndern";

		if (errorCode == ERROR_INVALID_ID)
			return "User " + user + " hat versucht auf die nicht g�ltige ID " + id + " zu zu greifen";

		return "Unknown Errorcode";
	}
	/**
	 * @param arg0
	 */
	public AddressSecurityException(int arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	/** 
	 * @param errorCode
	 * @param id ID der Adresse/Kalender/Kategorie
	 * @param userid ID des users
	 */
	public AddressSecurityException(int errorCode, int id, int userid) {
		super(errorCode);
        this.errorCode = errorCode;
		setObject_Id(id);
		setUser_Id(userid);
	}
	
	/** 
	 * @param errorCode
	 * @param ids IDs der Adressen/Kalender/Kategorien
	 * @param userid ID des users
	 */
	public AddressSecurityException(int errorCode, List ids, int userid) {
		super(errorCode);
        this.errorCode = errorCode;
		setObject_Ids(ids);
		setUser_Id(userid);
	}

	/** 
	 * @param errorCode
	 * @param id ID der Adresse/Kalender/Kategorie
	 * @param username ID des users
	 */
	public AddressSecurityException(int errorCode, int id, String username) {
		super(errorCode);		
        this.errorCode = errorCode;
		setObject_Id(id);
		setUserName(username);
		setMessage(getMessageByErrorCode(errorCode));
	}

	
	/**
	 * @param arg0
	 */
	public AddressSecurityException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public AddressSecurityException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param errorCode
	 * @param arg1
	 */
	public AddressSecurityException(int errorCode, Throwable arg1) {
		super(errorCode, arg1);
        this.errorCode = errorCode;
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param errorCode
	 * @param arg1
	 */
	public AddressSecurityException(int errorCode, String arg1) {
		super(errorCode, arg1);
        this.errorCode = errorCode;
		// TODO Auto-generated constructor stub
	}

	
	protected int getObject_Id() {
		return this.object_Id;
	}
	protected void setObject_Id(int object_Id) {
		this.object_Id = object_Id;
	}
	protected int getUser_Id() {
		return this.user_Id;
	}
	protected void setUser_Id(int user_Id) {
		this.user_Id = user_Id;
	}
	protected String getUserName() {
		return this.userName;
	}
	protected void setUserName(String userName) {
		this.userName = userName;
	}

	public List getObject_Ids() {
		return object_Ids;
	}

	public void setObject_Ids(List object_Ids) {
		this.object_Ids = object_Ids;
	}
}
