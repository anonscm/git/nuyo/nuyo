package de.tarent.groupware.resource;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.tarent.contact.bean.TressourceDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.AttributeSet;
import de.tarent.groupware.utils.ResultTransform;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcReflectedWorker;

/**
 * @author kleinw
 *
 *	Funktionalit�t f�r Resource:
 *	
 *	- getResources
 *	- createOrModifyResource	
 *	- deleteResource
 */
public class ResourceWorker extends TcReflectedWorker implements ResourceWorkerConstants {
    
    final static public String[] INPUT_GETRESOURCES= {"resourceid"};
    final static public boolean[] MANDATORY_GETRESOURCES = {false};
    final static public String OUTPUT_GETRESOURCES = "resources";
    
    final static public Object getResources(TcAll all, Integer id) throws SQLException {

        List response = null;
        
        Select select =
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TressourceDB.getTableName())
            	.add(TressourceDB.PK_PK, Integer.class)					//	[0]
            	.add(TressourceDB.RESSOURCENAME, String.class)			//	[1]
            	.add(TressourceDB.DESCRIPTION, String.class)			//	[2]
            	.add(TressourceDB.AUTOPUBLISHING, Integer.class)		//	[3]	
            	.add(TressourceDB.CREATED, Timestamp.class)				//	[4]
            	.add(TressourceDB.CHANGED, Timestamp.class);			//	[5]
            	
        if(id != null)
            response =
                select.where(Expr.equal(TressourceDB.PK_PK, id))
                .getList(TcDBContext.getDefaultContext());

        else
            response =
                select.getList(TcDBContext.getDefaultContext());

        return ResultTransform.toSingleString(response, all.getRequestObject().getRequestParameters());
    }

    final static public String[] INPUT_CREATEORMODIFYRESOURCE = {"resourceid", "autopublishing", "attributes"};
    final static public boolean[] MANDATORY_CREATEORMODIFYRESOURCE = {false, true, false};
    final static public String OUTPUT_CREATEORMODIFYRESOURCE = "resourceid";
    
    final static public Integer createOrModifyResource(TcAll all, Integer resourceId, Boolean autoPublishing, Map attributes) 
    	throws SQLException {

        InsertUpdate insertUpdate =
            SQL.InsertUpdate(TcDBContext.getDefaultContext())
            	.table(TressourceDB.getTableName())
            	.add(TressourceDB.AUTOPUBLISHING, autoPublishing);
        
        if (attributes != null) {
            AttributeSet as = new AttributeSet(attributes, insertUpdate);
            as.add(TressourceDB.RESSOURCENAME, KEY_NAME);
            as.add(TressourceDB.DESCRIPTION, KEY_DESCRIPTION);
            as.add(TressourceDB.CREATED, KEY_CREATOR);
            as.add(TressourceDB.CHANGED, KEY_CHANGER);
        }
        
        insertUpdate.add(TressourceDB.CREATED, new Date());
        if (resourceId == null) {
            InsertKeys keys = insertUpdate.executeInsertKeys(TcDBContext.getDefaultContext());
            resourceId = keys.getPkAsInteger();
        } else {
            insertUpdate.executeUpdate(TcDBContext.getDefaultContext(), TressourceDB.PK_PK, resourceId);
        }
        
    	return resourceId;
    }

    final static public String[] INPUT_DELETERESOURCE = {"resourceid"};
    final static public boolean[] MANDATORY_DELETERESOURCE = {true};
    final static public String OUTPUT_DELETERESOURCE = null;
    
    final static public void deleteResource(TcAll all, Integer resourceId) throws SQLException {
        SQL.Delete(TcDBContext.getDefaultContext())
            	.from(TressourceDB.getTableName())
            	.byId(TressourceDB.PK_PK, resourceId)
            	.executeDelete(TcDBContext.getDefaultContext());
    }

}
