/* $Id: MailSendSMTP.java,v 1.11 2008/09/03 15:43:25 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MailDateFormat;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


/**
 * This class will be used by {@link de.tarent.groupware.mail.MailSendThread}
 * in order to build and send emails via smtp.
 * 
 * @author Simon Bühler <simon@aktionspotential.de>
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.11 $
 */
public class MailSendSMTP {
	private static MailDateFormat dateFormat = new MailDateFormat();
	
	/**
     * Returns a message filled with content and sending information.
     *    
	 * @param session
	 * @param from
	 * @param to
	 * @param cc
	 * @param bcc
	 * @param Subject
	 * @param text
	 * @param attachments
	 * @param Priority
	 * @param RequestAnswer
	 * @return the initialized message
	 * @throws MessagingException if failed 
	 */
	public static Message buildMessage(
			Session session,
			String from,
			String to,
			String cc,
			String bcc,
			String Subject,
			String text,
			Map<String, DataSource> attachments,
			int Priority,
			boolean RequestAnswer)
	throws MessagingException {

		// define Message
		Message message = new MimeMessage(session);

		if (attachments != null && attachments.size() > 0) {
			Multipart multipart = new MimeMultipart();

			// set Text
			BodyPart bodypart = new MimeBodyPart();
			bodypart.setText(text);
			multipart.addBodyPart(bodypart);

			// add Attachments
			Iterator<Map.Entry <String, DataSource>> it = attachments.entrySet().iterator();
			while (it.hasNext()) {
                Map.Entry<String, DataSource> entry = it.next();
				DataSource source = entry.getValue();
				bodypart = new MimeBodyPart();
				bodypart.setDataHandler(new DataHandler(source));
				bodypart.setFileName(source.getName());
				multipart.addBodyPart(bodypart);
			}
			message.setContent(multipart);
		} else {
			message.setText(text);
		}

		// set From
		try {
			
			message.setFrom(new InternetAddress(from));
			
		} catch(AddressException excp) {
			throw new MessagingException("Sender E-Mail-address is not valid!", excp);
		}

		// set To/Cc/Bcc
		addRecipient(message, Message.RecipientType.TO, to);
		addRecipient(message, Message.RecipientType.CC, cc);
		addRecipient(message, Message.RecipientType.BCC, bcc);
		
		// set Subject
		message.setSubject(Subject);

		// set Priority
		message.setHeader("Date", dateFormat.format(new Date(System.currentTimeMillis())));
		if (Priority != 0 && Priority != 3)
		message.setHeader("X-Priority", new Integer(Priority).toString());
		//message.setHeader("X-Mailer", TcDBContext.getSchemaName() + "org");
		//message.setHeader("User-Agent", TcDBContext.getSchemaName() + "org");

		// set Disposition-Notification-To
		if (RequestAnswer)
			message.addHeader("Disposition-Notification-To", from);

		message.saveChanges();
		return message;
	}
	
	/**
     * Sends a given message to recipients using smtp.
     *  
	 * @param session
	 * @param host
	 * @param username
	 * @param password
	 * @param message
	 * @throws MessagingException
	 */
	public static void mail(Session session, String host, String username, String password, Message message) throws MessagingException {
		Transport transport = session.getTransport("smtp");
		transport.connect(host, username, password);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}
	
	static public void addRecipient(Message message, Message.RecipientType type, String recipient) throws AddressException, MessagingException {
		if (recipient != null && recipient.length() != 0) {
			if (recipient.indexOf(',') != -1 || recipient.indexOf(';') != -1) {
				String[] s = recipient.split("[,;]");
				for (int i = 0; i < s.length; i++) {
					message.addRecipient(type, new InternetAddress(s[i]));
				}
			} else {
				message.addRecipient(type, new InternetAddress(recipient));
			}
		}
	}

}
