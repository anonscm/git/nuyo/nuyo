/* $Id: Syncer.java,v 1.2 2006/03/16 13:49:30 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstrakte Syncer-Klasse, die dass automatische wiederholen und beenden
 * von Syncern, die im Hintergrund arbeiten, unterstuetzt. Alle Syncer werden
 * waehrend ihrer Laufzeit in einem Syncer-Pool gespeichert und koennen dort
 * jederzeit statisch unter ihrem eindeutigem Namen geladen werden.
 * 
 * @see #start()
 * @see #stop()
 * @see #getName()
 * @see Syncer#get(String)
 * 
 * Die Syncer unterstuetzen zwei autoamtische Abbruchmechanismen. Zum einem das
 * Beenden nach einer bestimmten Zeit <code>syncUntil(long until)</code>
 * (in Milisekunden) und zum anderen das Beenden nach einer bestimmten Anzahl
 * <code>syncTimes(long times)</code> von Durchlaufen. Dabei wird der jeweils
 * aktuelle Sync-Durchlauf zu ende gefuehrt. Zwischen zwei Syncvorgaengen kann eine
 * Pause in Milisekunden definiert <code>syncInterval(long interval)</code> werden. 
 * 
 * Alle Optionen koennen mit <code>Syncer.DISABLED</code> (-1) deaktiviert werden.
 * 
 * @see #syncUntil(long)
 * @see #syncTimes(long)
 * @see #syncInterval(long)
 * 
 * @author Christoph Jerolimov
 */
abstract public class Syncer implements Runnable {
	
	//
	// Konstanten
	//
	
	public final static long DISABLED = -1;
	
	
	//
	// Syncer Class
	//
	
	private boolean _run = false;
	private long _until;
	private long _times;
	private long _interval;
	
	private final Thread _thread;
	
	/**
	 * Syncer wird als Endlosschleife initalisiert und in einem Interval von
	 * einer Minute immer wieder erneut gestartet.
	 */
	public Syncer() {
		_until = DISABLED;
		_times = DISABLED;
		_interval = 60000;
		
		_thread = new Thread(this);
		_thread.setPriority(Thread.MIN_PRIORITY);
	}
	
	/**
	 * Syncer wird mit Parametern initalisiert.
	 * Siehe Allgemeine Syncer Beschreibung.
	 * 
	 * @param until
	 * @param times
	 * @param interval
	 */
	public Syncer(long until, long times, long interval) {
		if (until == DISABLED)
			_until = DISABLED;
		else
			_until = System.currentTimeMillis() + until;
		_times = times;
		_interval = interval;
		
		_thread = new Thread(this);
		_thread.setPriority(Thread.MIN_PRIORITY);
	}
	
	/**
	 * Zeit nach der der Syncer nicht erneut gestartet werden soll.
	 */
	public final void syncUntil(long until) {
		if (until == DISABLED)
			_until = DISABLED;
		else
			_until = System.currentTimeMillis() + until;
	}
	
	/**
	 * Angabe wie oft der Syncer maximal noch neu gestartet werden soll.
	 */
	public final void syncTimes(long times) {
		_times = times;
	}
	
	/**
	 * Zeitspanne zwischen zwei Sync-Vorgaengen.
	 */
	public final void syncInterval(long interval) {
		_interval = interval;
	}
	
	public final void syncDaemon(boolean daemon) throws IllegalThreadStateException {
		_thread.setDaemon(daemon);
	}
	
	/**
	 * Startet den Syncvorgang
	 */
	public final void start() {
		if (_syncer.containsKey(getName())) {
			log("syncer is running and will not start again");
			return;
		}
		
		_run = true;
		if (!_thread.isAlive()) {
			log("syncer will be start");
			_thread.start();
			_syncer.put(getName(), this);
		}
	}
	
	/**
	 * Stopped den Syncvorgang
	 */
	public final void stop() {
		log("syncer will be stopped next run");
		_run = false;
		_syncer.remove(getName());
	}
	
	/**
	 * Gibt an ob ein Syncvorgang gerade lauft.
	 */
	public final boolean isSyncronising() {
		return _thread.isAlive();
	}
	
	/**
	 * Gibt an ob der Thread ein Daemon ist.
	 * 
	 * @see Thread#isDaemon()
	 */
	public final boolean isDaemon() {
		return _thread.isDaemon();
	}
	
	/**
	 * Der Syncer sollte ueber die Methode <code>start()</code> gestartet werden.
	 * 
	 * @see #start()
	 * @see #stop()
	 */
	public final void run() {
		while (_run &&
				(_until == DISABLED || _until > System.currentTimeMillis()) &&
				(_times == DISABLED || _times-- > 0)) {
			try {
				log("syncer will sync now");
				sync();
			} catch (RuntimeException e) {
				log(e);
			}
			
			if (_run && _interval != DISABLED &&
					(_until == DISABLED || _until > System.currentTimeMillis()) &&
					(_times == DISABLED || _times > 0))
			try {
				log("syncer will sleep now");
				Thread.sleep(_interval);
			} catch (InterruptedException e) {
				// nothing
			}
		}
		stop();
	}
	
	/**
	 * Diese Methode wird bei jedem Syncvorgang aufgerufen.
	 */
	abstract public void sync();
	
	/**
	 * Diese Methode gibt einen eindeutigen Namen f�r diesen Syncer zur�ck.
	 * Unter diesem Namen kann der Syncer dann ggf. aus dem Mappool geladen
	 * werden. Beispielsweise "syncername-" + ID
	 */
	abstract public String getName();
	
	
	//
	// Logger
	//
	
	protected abstract void log(String msg);
	protected abstract void log(Exception e);
	
	
	/**
	 * �berl�dt Object.equals() zum einfachen vergleichen.
	 */
	public boolean equals(Object o) {
		return o instanceof Syncer && ((Syncer)o).getName().equals(getName());
	}
	
	/**
	 * �berl�dt Object.toString() zum einfacheren debuggen.
	 */
	public String toString() {
		return getName() + "@" + Integer.toHexString(hashCode()) + " ["
				+ "until=" + ((_until == DISABLED) ? "disabled" : ((_until - System.currentTimeMillis()) / 60000) + "m")
				+ ",times=" + ((_times == DISABLED) ? "disabled" : (_times) + "x")
				+ ",interval=" + ((_interval == DISABLED) ? "disabled" : (_interval / 60000) + "m")
				+ "]";
	}

	protected void finalize() throws Throwable {
		stop();
		super.finalize();
	}
	
	
	//
	// Syncer Controller
	//
	
	private final static Map _syncer = new HashMap();
	
	public final static Syncer get(String name) {
		return (Syncer)_syncer.get(name);
	}
}
