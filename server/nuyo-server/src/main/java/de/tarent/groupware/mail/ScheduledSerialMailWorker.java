package de.tarent.groupware.mail;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;

import de.tarent.commons.logging.LogFactory;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.groupware.Address;
import de.tarent.groupware.address.AddressDAO;
import de.tarent.groupware.mail.helper.Attachments;
import de.tarent.octopus.server.OctopusContext;

/**
 * 
 * @author Robert Schuster (robert.schuster@tarent.de)
 *
 */
public class ScheduledSerialMailWorker {

    private static final Log logger = LogFactory.getLog(ScheduledSerialMailWorker.class);
    
	public final static String PARAM_ACTIONID = "actionId";
	public final static String PARAM_MAILSUBJECT = "MailSubject";
	public final static String PARAM_MAILBODY = "MailBody";
	public final static String PARAM_MAILFROM = "MailFrom";
	public final static String PARAM_ATTACHMENT_NAMES = "AttachmentNames";
	public final static String PARAM_ATTACHMENT_DATAS = "AttachmentDatas";
	public final static String PARAM_SEND_DELAYED = "SendDelayed";
	public final static String PARAM_COPY_TO_SENDER = "CopyToSender";


	public final String[] INPUT_SCHEDULEMAIL = { PARAM_ACTIONID,
			PARAM_MAILSUBJECT, PARAM_MAILBODY, PARAM_MAILFROM,
			PARAM_ATTACHMENT_NAMES, PARAM_ATTACHMENT_DATAS, PARAM_SEND_DELAYED, PARAM_COPY_TO_SENDER };

	public final boolean[] MANDATORY_SCHEDULEMAIL = { true, true, true, true,
			false, false, false, false };

	public final String OUTPUT_SCHEDULEMAIL = null;

	/**
	 * This method allows sending serial mails to a set of recipients specified
	 * through the action id (denotes a shipping order).
	 * 
	 * <p>The method is to be called solely from the contact-client and allows
	 * specifying the return address as well as the shipping date.</p>
	 * 
	 * <p>The mails subject and body are treated as templates which get personalized
	 * for each recipient.</p>
	 * 
	 * <p>If copyToSender is set to 'true' an email with template-subject and
	 * template-body is sent to the address given by mailFrom</p>
	 * 
	 * @param octx
	 * @param actionId
	 * @param storeId
	 * @param subjectTemplate
	 * @param bodyTemplate
	 * @param attachmentNames
	 * @param attachmentDatas
	 * @param sendDelayed
	 * @param copyToSender
	 */
	public final static void scheduleMail(OctopusContext octx,
			Integer actionId, String subjectTemplate, String bodyTemplate,
			String mailFrom, List<String> attachmentNames, List<byte[]> attachmentDatas,
			boolean sendDelayed, boolean copyToSender) {

	    long sendTime = getSendTime(octx, sendDelayed);
	    logger.info("scheduling a serial email: action-id: " + actionId);
		logger.info("send-time: " + new Date(sendTime));
		// Retrieve all addresses associated with the given action id.
		List<Address> addresses = null;
		try {
			addresses = (List<Address>) AddressDAO.getInstance()
                .getAddressesForAddressAction(TcDBContext.getDefaultContext(), actionId, "Mail");
		} catch (SQLException sqle) {
            logger.error("failed retrieving addresses: " + sqle.getMessage(),sqle);
			throw new IllegalStateException(
					"Unable to retrieve the addresses for action " + actionId, sqle);
		}
		logger.info("# of emails-to-send: " + addresses.size());

		// Retrieve the user's mail store.
		String module = octx.getModuleName();
		Integer userId = octx.personalConfig().getUserID();
        
        // In case the return address was not explicitly given try to get it
        // from the personal config.
        if (mailFrom == null || mailFrom.length() == 0)
          mailFrom = octx.personalConfig().getUserEmail();
        
        if (mailFrom == null || mailFrom.length() == 0)
          mailFrom = octx.personalConfig().getEmail();

        if (mailFrom == null || mailFrom.length() == 0)
          {
            logger.error("While trying to send scheduled serial mails no valid return address could be found.");
            logger.error("Neither the 'mailFrom' argument, nor the properties 'userEmail' and 'email' from the user config provided one.");
            logger.error("Possible solution: Check configuration and respective DB entries");
            throw new IllegalStateException("No valid return address provided explicitly and no email address in the user config.");
          }

        Integer mailJobId = null;
        try {
            mailJobId = MailSendDB.createNewJob(module, userId, sendTime);
        } catch (MailException.Database med) {
            logger.error("failed creating a SEND-JOB: " + med.getMessage());
            throw new IllegalStateException("Unable to create mail job: " + med.getMessage(), med);
        }

        List<Integer> attachmentsIdList = null;
        try {
          String attchTempPath = octx.getConfigObject().getModuleConfig().getParam(MailSendWorker.PARAM_ATTACHMENT_TEMP_PATH);
          attachmentsIdList = Attachments.toFilesystem(module, attchTempPath, actionId, mailJobId, attachmentNames, attachmentDatas );
          logger.info("#attachments: " + attachmentsIdList.size());
        } catch (IOException e)
        {
            logger.error("failed storing attachments: " + e.getMessage(), e);
            throw new IllegalStateException("Unable to store the attachments for action " + actionId, e);
        } catch (MailException.Database e ) {
            logger.error("failed storing attachments: " + e.getMessage(), e);
            throw new IllegalStateException("Unable to store the attachments for action " + actionId, e);
        } finally {
            //FIXME: remove invalid job 
        }
        
		// Generate personalized subject and message text for the given template
		// and schedule a mail for every address.
		for (Address a : addresses) {
			String subject = MailHelper.doTemplateReplacement(subjectTemplate, a);
			String mail = MailHelper.doTemplateReplacement(bodyTemplate, a);

			String mailTo = a.getEmailAdresseDienstlich();
			if (mailTo == null)
				mailTo = a.getEmailAdressePrivat();
			if (mailTo == null)
				mailTo = a.getEmailAdresseWeitere();

			scheduleMail(mailTo, mailFrom, subject, mail, module, userId, mailJobId, attachmentsIdList);
		}
		
		// if a copy of this email should also be sent to the sender, just send the templates
		if(copyToSender)
			scheduleMail(mailFrom, mailFrom, subjectTemplate, bodyTemplate, module, userId, mailJobId, attachmentsIdList);
        
        try {
            logger.debug("===== updating JOB_STATUS to NEW...");
            MailSendDB.updateJobstatus(module, mailJobId, MailConstants.JOBSTATUS_NEW);
        } catch (MailException.Database med) {
            logger.error("failed status update for SEND-JOB: " + med.getMessage());
            throw new IllegalStateException("Unable to update mail job: " + med.getMessage(), med);
        }
        
        logger.debug("=========================================================");
        logger.debug("scheduling finished:");
        logger.debug("serial email: " + actionId );
        logger.debug("job: " + mailJobId );
        logger.debug("=========================================================");
	}
	
	/**
	 * Schedule a mail 
	 * 
	 * @param mailTo
	 * @param mailFrom
	 * @param subject
	 * @param mail
	 * @param module
	 * @param userId
	 * @param mailJobId
	 * @param attachmentsIdList
	 */
	
	protected static void scheduleMail(String mailTo, String mailFrom, String subject, String mail, String module, Integer userId, Integer mailJobId, List<Integer> attachmentsIdList)
	{
		try {
            logger.debug("===== adding message to job...");
            Integer messageId = MailSendDB.addMessagetoJob(module, userId, mailJobId,
					new MailSendMessage(subject, mail, mailTo, "", "",
							mailFrom, null, false, false, 0, userId
									.intValue(), 0));
			
			logger.debug("===== adding attachments to message...");
            for(Integer currentAttachmentID : attachmentsIdList){
                MailSendDB.assignAttchIDToMessageEntry( module, userId, mailJobId, messageId, currentAttachmentID );
            }
            
		} catch (MailException.Database med) {
            logger.error("failed to add delayed message job", med);
			throw new IllegalStateException("Unable to add message to mail job: " + med.getMessage(), med);
        } finally {
            //FIXME: remove invalid job, it's message entries and attachments 
        }
	}
	
	/**
	 * Returns the time in milliseconds (from 1970-1-1) when to
	 * send the mails.
	 * 
	 * <p>Depending on the value of <code>sendDelayed</code> this
	 * will be immediately or at the next occurance of the time of
	 * day mentioned in the server configuration.</p>
	 * 
	 * @param octx
	 * @param sendDelayed
	 * @return
	 */
	private static long getSendTime(OctopusContext octx, boolean sendDelayed)
	{
		if (sendDelayed)
		{
			// Reads configured time from configuration and
			// calculates the next occurance of that time.
			DateFormat df = new SimpleDateFormat("hh:mm");
			try
			{
			  String time = octx.getConfigObject().getModuleConfig().getParam("scheduleMailTime");
			  Calendar scheduleTime = Calendar.getInstance();
			  scheduleTime.setTime(df.parse(time));
				
			  // mailTime gets initialized to the current
			  // date & time and then the time is set to
			  // the parsed one
			  Calendar mailTime = Calendar.getInstance();
			  mailTime.set(Calendar.HOUR_OF_DAY, scheduleTime.get(Calendar.HOUR_OF_DAY));
			  mailTime.set(Calendar.MINUTE, scheduleTime.get(Calendar.MINUTE));
				
			  Calendar current = Calendar.getInstance();
			  // If the mail time is already over just do
			  // it the next day.
			  if (mailTime.before(current));
			    mailTime.add(Calendar.DAY_OF_YEAR, 1);
			    
			  return mailTime.getTimeInMillis();
			}
			catch (ParseException _)
			{
				throw new IllegalStateException(
				"Unable to parse time for delayed mails. Check \"scheduleMailTime\" setting in server configuration.");
			}
		}
		
		// If send delay is not requested return the current time
		// which directs the mail scheduler to run the mail batch
		// immediately.
		return new Date().getTime();
	}
}
