/* $Id: MailSendLoopWorker.java,v 1.4 2007/06/15 15:58:28 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.util.HashMap;
import java.util.Map;

import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;

/**
 * @author Simon Bühler <simon@aktionspotential.de>
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.4 $
 */
public class MailSendLoopWorker implements TcContentWorker {
	private static MailSendThread thread;
	private Map serverData;

	/* (non-Javadoc)
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
		serverData = new HashMap();
		serverData.put("server", config.getParam("mail_SMTPServer"));
		serverData.put("username", config.getParam("mail_SMTPUsername"));
		serverData.put("password", config.getParam("mail_SMTPPassword"));
		serverData.put("summaryEmailFrom", config.getParam("mail_summaryEmailFrom"));
	}

	/* (non-Javadoc)
	 * @see de.tarent.octopus.content.TcContentWorker#doAction(de.tarent.octopus.config.TcConfig, java.lang.String, de.tarent.octopus.request.TcRequest, de.tarent.octopus.content.TcContent)
	 */
	public String doAction(TcConfig tcConfig, String actionName, TcRequest tcRequest, TcContent tcContent) throws TcContentProzessException {
		if (actionName.equals(ACTION_CreateLoop)) {
			if(thread==null){
				thread = new MailSendThread(
					tcConfig.getModuleConfig().getName(),
					serverData);
				thread.start();
			}
			return RESULT_ok;
		} else if (actionName.equals(ACTION_DestroyLoop)) {
			if(thread!=null){
				thread.halt();
				thread.interrupt();
				thread = null;
			}
			return RESULT_ok;
		} else {
			throw new TcContentProzessException(
				"Nicht unterst�tzte Aktion im Worker 'SMTPLOOPWorker': "
					+ actionName);
		}
	}

	/* (non-Javadoc)
	 * @see de.tarent.octopus.content.TcContentWorker#getWorkerDefinition()
	 */
	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port =
			new TcPortDefinition(getClass().getName(),
				"Worker der im Loop die SMTPQueue nach zu verschickenden Nachrichten durchsucht.");
		TcOperationDefinition operation =
			port.addOperation(ACTION_CreateLoop, "Erstellt den Loop");
		port.addOperation(ACTION_DestroyLoop, "Stoppt den Loop");
		operation.setInputMessage();
		operation.setOutputMessage();
		return port;
	}

	public final static String ACTION_CreateLoop = "LoopStart";
	public final static String ACTION_DestroyLoop = "LoopStop";

	public final static String RESULT_ok = "ok";
	public final static String RESULT_error = "error";
	/* (non-Javadoc)
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		// TODO Auto-generated method stub
		return null;
	}

}
