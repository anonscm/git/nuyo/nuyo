/* $Id: SecurityWorker.java,v 1.16 2007/06/15 15:58:29 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-oc
 * topus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.security;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddresscategoryDB;
import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TeventDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupDB;
import de.tarent.contact.bean.TscheduleDB;
import de.tarent.contact.bean.TscheduleeventDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.bean.TuserscheduleDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.queries.SecurityQueries;
import de.tarent.groupware.utils.AttributeSet;
import de.tarent.groupware.utils.Conversion;
import de.tarent.groupware.utils.EntityDescription;
import de.tarent.groupware.utils.GenericSelector;
import de.tarent.groupware.utils.OptimizedInList;
import de.tarent.octopus.server.OctopusContext;

/**
 * Worker f�r die Sicherheitsfunktionen, die der SecurityManager aus dem Client antriggern/abfragen kann
 * 
 * @author Nils Neumaier, tarent GmbH
 */
public class SecurityWorker
    implements SecurityWorkerConstants {


    // Globale Berechtigungen

    public String[] INPUT_loadUserGlobalRightEntityDescription = new String[] {};
    public String OUTPUT_loadUserGlobalRightEntityDescription = "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD;
    public EntityDescription loadUserGlobalRightEntityDescription() {
        return userGlobalRightEntityDescription;
    }


    public String[] INPUT_createGlobalRightBaseSelect = new String[] {"userid"};
    public boolean[] MANDATORY_createGlobalRightBaseSelect = new boolean[] {false};
    public String OUTPUT_createGlobalRightBaseSelect = "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD;
    public Select createGlobalRightBaseSelect(OctopusContext cntx, Integer userid) throws SQLException {
        if (userid == null)
            userid = cntx.personalConfig().getUserID();
        
        return SQL.SelectDistinct(TcDBContext.getDefaultContext())
            .from(TcDBContext.getSchemaName() + "v_user_globalright")
            .where(Expr.equal("v_user_globalright.fk_user", userid));
    }


    // Kategory Berechtigungen

    public String[] INPUT_loadUserFolderRightEntityDescription = new String[] {};
    public String OUTPUT_loadUserFolderRightEntityDescription = "CONTENT:"+GenericSelector.CURRENT_ENTITY_DESCRIPTION_FIELD;
    public EntityDescription loadUserFolderRightEntityDescription() {
        return userFolderRightEntityDescription;
    }

    public String[] INPUT_createFolderRightBaseSelect = new String[] {"userid"};
    public boolean[] MANDATORY_createFolderRightBaseSelect = new boolean[] {false};
    public String OUTPUT_createFolderRightBaseSelect = "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD;
    public Select createFolderRightBaseSelect(OctopusContext cntx, Integer userid) throws SQLException {
        if (userid == null)
            userid = cntx.personalConfig().getUserID();
        
        return SQL.SelectDistinct(TcDBContext.getDefaultContext())
            .from(TcDBContext.getSchemaName() + "v_user_folderright")
            .where(Expr.equal("v_user_folderright.fk_user", userid));
    }

    /* Definition der Action checkUserRightOnFolder*/
    public String[] INPUT_checkUserRightOnFolder = { "username", "folderRight", "folderID", "subfolderID", "subCategories"};
	public boolean[] MANDATORY_checkUserRightOnFolder = { true, true, true, false, false };
	public String OUTPUT_checkUserRightOnFolder = null;
	
	/**
	 * Diese Action ruft die Methode hasUserRightOnFolder auf und wirft eine Exception, falls die Methode FALSE zur�ckgibt
	 * um so den Ablauf des Tasks, aus dem sie aufgerufen wurde, zu unterbrechen
	 * @param oc der Octopuscontext
	 * @param userLogin
	 * @param folderRight
	 * @param folderID
	 * @throws SQLException
	 * @throws AddressSecurityException
	 */
	public void checkUserRightOnFolder(OctopusContext oc, String userLogin, Integer folderRight, Integer folderID, Integer subfolderID, List subCategories) throws SQLException, AddressSecurityException{
		
		if (subCategories != null && subCategories.size() > 0){
			   
	        de.tarent.dblayer.sql.statement.Select selectCategories = de.tarent.dblayer.sql.SQL.Select(TcDBContext.getDefaultContext())
	    	.from(TsubcategoryDB.getTableName())
	        .selectAs(TsubcategoryDB.FKCATEGORY)
			.selectAs(TsubcategoryDB.PK_PKSUBCATEGORY)
			.where(Expr.in(TsubcategoryDB.PK_PKSUBCATEGORY, subCategories));

	        Result result = DB.result(TcDBContext.getDefaultContext(), selectCategories.toString());
	        ResultSet rs = result.resultSet();
	        
	        while (rs.next()){
	        	if (!(hasUserRightOnFolder(oc, userLogin, folderRight, new Integer(rs.getInt(TsubcategoryDB.FKCATEGORY)), null).booleanValue())){
	    			throw new AddressSecurityException(folderRight.intValue() + 99, folderID.intValue(), userLogin);
	    		}
	        	
	        }        
		}
		
		
		if (!(hasUserRightOnFolder(oc, userLogin, folderRight, folderID, subfolderID).booleanValue())){
			throw new AddressSecurityException(folderRight.intValue() + 99, folderID.intValue(), userLogin);
		}
	}
	

	/*Definition der Action "hasUserRightOnFolder"*/
	public static final String GROUPID = "groupID";
	public static final String ROLEID = "roleID";
	public static final String CATEGORYID = "categoryID";
	
	public String[] INPUT_hasUserRightOnFolder = { "userLogin", "folderRight", "folderID", "subfolderID"};
	public boolean[] MANDATORY_hasUserRightOnFolder = { true, true, true, false };
	public String OUTPUT_hasUserRightOnFolder = "right";
	/**
	 * Stellt fest, ob ein User ein bestimmtes Recht auf einer Kategorie hat
	 * @param oc der OctopusContext
	 * @param userLogin Login des Users
	 * @param folderRight Recht das abgefragt wird TODO @see
	 * @param folderID ID der Kategorie auf der das Recht gefragt wird
	 * @return Boolean.TRUE, falls Recht besteht, Boolean.FALSE sonst
	 * @throws SQLException 
	 */
	public Boolean hasUserRightOnFolder(OctopusContext oc, String userLogin, Integer folderRight, Integer folderID, Integer subFolderID) throws SQLException{
		
		
		//System.out.print("Pr�fe Recht " + folderRight + " f�r User " + userLogin + " auf Kategorie " + folderID + " ... ");
		
		if (subFolderID != null){
			String select = "SELECT fk_folder FROM " + TcDBContext.getSchemaName() + "tsubfolder " +
			"WHERE tsubfolder.pk_subfolder=" + subFolderID + " AND tsubfolder.fk_folder="+ folderID;
			
			Result r = DB.result(TcDBContext.getDefaultContext(), select);
			ResultSet rs = r.resultSet();			
			boolean vorhanden = rs.next();
			
			if (!vorhanden) {
				Lg.WORKER(oc.getModuleName()).warn("Ung�ltige Kombination von Kategorie und Unterkategorie in HasUserRightOnFolder");
				return Boolean.FALSE;
			}
		}
		
		//System.out.print("Checke Recht " + folderRight + " f�r User " + userLogin + " auf Kategorie " + folderID + " ... ");
		String sql = SecurityQueries.getUserRightOnFolderQuery(userLogin, folderID, folderRight);
		Result r = DB.result(TcDBContext.getDefaultContext(), sql);
		ResultSet rs = r.resultSet();
		boolean vorhanden = rs.next();
		if(vorhanden){
			int right = rs.getInt("right");
			if(right>0){
				//System.out.println("Zugriff gew�hrt...");
				return Boolean.TRUE;
			}else
				//System.out.println("Zugriff verweigert...");
				return Boolean.FALSE;
		}else{
			//System.out.println("Zugriff verweigert...");
			return Boolean.FALSE;
		}
	}


    public String[] INPUT_checkUserRightToReleaseSchedule = { "scheduleid", "userscheduleid", "userid" };
	public boolean[] MANDATORY_checkUserRightToReleaseSchedule = { false, false, true };
	public String OUTPUT_checkUserRightToReleaseSchedule = null;
	
	/**
	 * Diese Action testet, ob der aktuelle Benutzer den angegebenen Kalendar editieren darf.
	 * @param oc der Octopuscontext
	 * @param scheduleid : ID des Kalendars
	 * @param userscheduleid : ID des eintrags in tuserschedule, der bei deletereleaseforschedule gel�scht werden soll
	 * @throws SQLException
	 * @throws AddressSecurityException
	 */
	public void checkUserRightToReleaseSchedule(OctopusContext oc, Integer scheduleid, Integer userscheduleid, Integer userid) throws SQLException, AddressSecurityException{
        
		if (scheduleid == null && userscheduleid != null){
			String sql = "SELECT " + TuserscheduleDB.FKSCHEDULE + " AS \"" + TuserscheduleDB.FKSCHEDULE + "\" FROM " + TuserscheduleDB.getTableName() + " " +  
			"WHERE " + TuserscheduleDB.PK_PK + "=" + userscheduleid;
			Result r = DB.result(TcDBContext.getDefaultContext(), sql);
			ResultSet rs = r.resultSet();
			if(rs.next())
				scheduleid = new Integer(rs.getInt(1));			
		}
		
		if (!
             // erlaubt, wenn der user der besitzer des Kalendars ist
             (checkUserIsOwnerOfSchedule(oc, scheduleid, null)
            
             ||            		
            		
             // , wenn er global Kalendar editieren darf
             hasUserGlobalRight(oc, oc.personalConfig().getLoginname(), new Integer(SecurityQueries.GLOBALRIGHT_EDITSCHEDULE)).booleanValue())
           ){			
			throw new AddressSecurityException(SecurityQueries.GLOBALRIGHT_EDITSCHEDULE + 99, -1, oc.personalConfig().getUserLogin());
		}
            
	}

	
    public String[] INPUT_checkUserRightToEditUser = { "username" };
	public boolean[] MANDATORY_checkUserRightToEditUser = { true };
	public String OUTPUT_checkUserRightToEditUser = null;
	
	/**
	 * Diese Action testet, ob der aktuelle Benutzer den angegebenen User editieren darf.
	 * @param oc der Octopuscontext
	 * @param userLogin
	 * @throws SQLException
	 * @throws AddressSecurityException
	 */
	public void checkUserRightToEditUser(OctopusContext oc, String userLogin) throws SQLException, AddressSecurityException{
        
		if (!
            (
            // erlaubt, wenn er global User editieren darf
             hasUserGlobalRight(oc, oc.personalConfig().getUserLogin(), new Integer(SecurityQueries.GLOBALRIGHT_EDITUSER)).booleanValue()
             
             ||
             
             // oder selbst der user ist
             userLogin.equals(oc.personalConfig().getUserLogin())
             )
            )   
            throw new AddressSecurityException(SecurityQueries.GLOBALRIGHT_EDITUSER + 99, -1, userLogin);
	}


	
	/* Definition der Action checkUserRightOnAddress*/
    public String[] INPUT_checkUserRightOnAddress = { "username", "folderRight", "addressID" };
	public boolean[] MANDATORY_checkUserRightOnAddress = { true, true, true };
	public String OUTPUT_checkUserRightOnAddress = null;
	
	/**
	 * Diese Action ruft die Methode hasUserRightOnAddress auf und wirft eine Exception, falls die Methode FALSE zur�ckgibt
	 * um so den Ablauf des Tasks, aus dem sie aufgerufen wurde, zu unterbrechen
	 * @param oc der Octopuscontext
	 * @param userLogin
	 * @param folderRight
	 * @param addressID
	 * @throws SQLException
	 * @throws AddressSecurityException
	 */
	public void checkUserRightOnAddress(OctopusContext oc, String userLogin, Integer folderRight, Integer addressID) throws SQLException, AddressSecurityException{
		if (!(hasUserRightOnAddress(oc, userLogin, folderRight, addressID).booleanValue())){
			throw new AddressSecurityException(folderRight.intValue() + 99, addressID.intValue(), userLogin);
		}
	} 
	
	
	
	
	
	/*Definition der Action "hasUserRightOnAddress"*/
	public String[] INPUT_hasUserRightOnAddress = { "userLogin", "folderRight", "addressID" };
	public boolean[] MANDATORY_hasUserRightOnAddress = { true, true, true };
	public String OUTPUT_hasUserRightOnAddress = "right";
	/**
	 * Stellt fest, ob ein User ein bestimmtes Recht auf einer Addresse hat
	 * @param oc der OctopusContext
	 * @param userLogin Login des Users
	 * @param folderRight Recht das abgefragt wird TODO @see

	 * @return Boolean.TRUE, falls Recht besteht, Boolean.FALSE sonst
	 * @throws SQLException 
	 */
	public static Boolean hasUserRightOnAddress(OctopusContext oc, String userLogin, Integer folderRight, Integer addressID) throws SQLException{
		
		//System.out.print("Checke Recht f�r User " + userLogin + " auf Adresse " + addressID + " ... ");
		
		
		String sql = SecurityQueries.getUserRightOnAddressQuery(userLogin, addressID, folderRight);
		Result r = DB.result(TcDBContext.getDefaultContext(), sql);
		ResultSet rs = r.resultSet();
		boolean vorhanden = rs.next();
		if(vorhanden){
			int right = rs.getInt("right");
			if(right>0){
				//System.out.println("Zugriff gew�hrt...");
				return Boolean.TRUE;
			}else
				//System.out.println("Zugriff verweigert...");
				return Boolean.FALSE;
		}else{
			//System.out.println("Zugriff verweigert...");
			return Boolean.FALSE;
		}
	}

	

	/* Definition der Action checkUserGlobalRight*/
    public String[] INPUT_checkUserGlobalRight = { "username", "globalRight" };
	public boolean[] MANDATORY_checkUserGlobalRight = { true, true };
	public String OUTPUT_checkUserGlobalRight = null;
	
	/**
	 * Diese Action ruft die Methode hasUserGlobalRight auf und wirft eine Exception, falls die Method FALSE zur�ckgibt
	 * um so den Ablauf des Tasks, aus dem sie aufgerufen wurde, zu unterbrechen
	 * @param oc der Octopuscontext
	 * @param userLogin
	 * @param globalRight
	 * @throws SQLException
	 * @throws AddressSecurityException
	 */
	public void checkUserGlobalRight(OctopusContext oc, String userLogin, Integer globalRight) throws SQLException, AddressSecurityException{
		if (!(hasUserGlobalRight(oc, userLogin, globalRight).booleanValue())){
			throw new AddressSecurityException(globalRight.intValue() + 99, -1, userLogin);
		}
	} 
	
	/*Definition der Action "hasUserGlobalRight"*/
	public String[] INPUT_hasUserGlobalRight = { "userLogin", "globalRight"};
	public boolean[] MANDATORY_hasUserGlobalRight = { true, true};
	public String OUTPUT_hasUserGlobalRight = "right";
	/**
	 * Stellt fest, ob ein User ein bestimmtes globales Recht hat
	 * @param oc der OctopusContext
	 * @param userLogin Login des Users
	 * @param globalRight Recht das abgefragt wird TODO @see
	 * @return Boolean.TRUE, falls Recht besteht, Boolean.FALSE sonst
	 * @throws SQLException 
	 */
	public Boolean hasUserGlobalRight(OctopusContext oc, String userLogin, Integer globalRight) throws SQLException{
		
		//System.out.print("Checke globales Recht " + globalRight + " f�r User " + userLogin + " ... ");
		
		String sql = SecurityQueries.getUserGlobalRightQuery(userLogin, globalRight);
		Result r = DB.result(TcDBContext.getDefaultContext(), sql);
		ResultSet rs = r.resultSet();
		boolean vorhanden = rs.next();
		
		if(vorhanden){
			int right = rs.getInt("right");
			if(right>0)
				//System.out.println("Zugriff gew�hrt...");
				return Boolean.TRUE;
		}
			
		return Boolean.FALSE;
		
	}
	
	/*Definition der Action "getCategoryRights"*/
	public String[] INPUT_getCategoryRights = {};
	public boolean[] MANDATORY_getCategoryRights = {};
	public String OUTPUT_getCategoryRights = "arights";
	/**
	 * Liefert alle vorhanden Rechte-Tupel (Kategorie, Gruppe, Rolle).
	 * f�r die der Benutzer die Berechtigung zur Rechtevergabe besitzt (siehe DB-view "v_user_folderright")
	 * @param oc
	 * @return Collection von Rechte-Tupeln
	 * @throws SQLException
	 */
	public Collection getCategoryRights(OctopusContext oc) throws SQLException{
		Select select = SecurityQueries.getCategoryRights().select(TgroupDB.ALL).select(TcategoryroleDB.ALL).select(TcategoryDB.ALL);
		select.join(TgroupDB.getTableName(), TgroupCategoryDB.FKGROUP, TgroupDB.PK_PK);
		select.join(TcategoryroleDB.getTableName(), TgroupCategoryDB.FKCATEGORYROLE, TcategoryroleDB.PK_PK);
		select.join(TcategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TcategoryDB.PK_PKCATEGORY);
		select.join(TcDBContext.getSchemaName() + "v_user_folderright", TcategoryDB.PK_PKCATEGORY, "v_user_folderright.fk_folder" );
		select.where( Where.and(Expr.equal("v_user_folderright.fk_user", oc.personalConfig().getUserID()),
				                Expr.equal("v_user_folderright.auth_grant", "1")));
		
		Result result = DB.result(TcDBContext.getDefaultContext(), select);
		oc.setContent("groups", TgroupDB.getBeanCollection(result.resultSet()));
		result.resultSet().beforeFirst();
		oc.setContent("objects", TcategoryDB.getBeanCollection(result.resultSet()));
		result.resultSet().beforeFirst();
		oc.setContent("roles", TcategoryroleDB.getBeanCollection(result.resultSet()));
		result.resultSet().beforeFirst();
		return TgroupCategoryDB.getBeanCollection(result.resultSet());
	}
	
	
	/*Definition der Action "checkUserRightOnCategoryRight"*/
	public String[] INPUT_checkUserRightOnCategoryRight = { "id", "categoryid", "attributes" };
	public boolean[] MANDATORY_checkUserRightOnCategoryRight = { false, false, false };
	public String OUTPUT_checkUserRightOnCategoryRight = null;
	/**
	 * �berpr�ft, ob der User das Recht (auth_grant) auf der Kategorie hat,
	 * auf der das zu l�schende/ver�ndernde Rechtetripel definiert ist
	 * @param oc
	 * @param id Die ID des Rechtetripels in tgroup_folder
	 * @throws SQLException
	 */
	public void checkUserRightOnCategoryRight(OctopusContext oc, Integer id, Integer categoryid, Map attributes) throws SQLException, AddressSecurityException{
		
		Integer categoryId = new Integer(-1);
		
		if (categoryid != null){
			categoryId = categoryid;
		}
		
		else if (attributes != null){
			if (attributes.containsKey("categoryID"))
				categoryId =(Integer) attributes.get("categoryID");
				
		}
		
		else {
			
			//Zugeh�rige Kategorie ermitteln
			String sql = 
			"SELECT tfolder.pk_folder  AS \"tfolder.pk_folder\" FROM " + TcDBContext.getSchemaName() + "tfolder " +  
			"JOIN " + TcDBContext.getSchemaName() + "tgroup_folder ON (tfolder.pk_folder=tgroup_folder.fk_folder) " +
			"WHERE tgroup_folder.pk="+ id;
			
			Result result = DB.result(TcDBContext.getDefaultContext(), sql);
			ResultSet rs = result.resultSet();
			rs.first();		
			categoryId = new Integer(rs.getInt("tfolder.pk_folder"));
			
		}
				
		// Recht auf Kategorie pr�fen (falls kein Recht besteht wird eine Exceptiongeorfen und der Task abgebrochen		
		checkUserRightOnFolder(oc, oc.personalConfig().getLoginname(), new Integer(SecurityQueries.FOLDERRIGHT_GRANT), categoryId, null, null);
	}
	
	/*Definition der Action "deleteCategoryRight"*/
	public String[] INPUT_deleteCategoryRight = { "id" };
	public boolean[] MANDATORY_deleteCategoryRight = { true };
	public String OUTPUT_deleteCategoryRight = null;
	/**
	 * L�scht ein Rechte-Tupel (Kategorie, Gruppe, Rolle).
	 * @param oc
	 * @throws SQLException
	 */
	public void deleteCategoryRight(OctopusContext oc, Integer id) throws SQLException, AddressSecurityException{
	
		Delete delete = SecurityQueries.deleteCategoryRight(id);
		delete.executeDelete(TcDBContext.getDefaultContext());
	}
	
	/*Definition der Action "createOrModifyCategoryRight"*/
	public String[] INPUT_createOrModifyCategoryRight = { "categoryRightID", "attributes" };
	public boolean[] MANDATORY_createOrModifyCategoryRight = { false, true };
	public String OUTPUT_createOrModifyCategoryRight = "categoryRightID";
	
	public Integer createOrModifyCategoryRight(OctopusContext oc, Integer id, Map attributes) throws SQLException{
		InsertUpdate stmnt = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TgroupCategoryDB.getTableName());
		AttributeSet as = new AttributeSet(attributes, stmnt);
		as.add(TgroupCategoryDB.FKCATEGORY, CATEGORYID);
		as.add(TgroupCategoryDB.FKCATEGORYROLE, ROLEID);
		as.add(TgroupCategoryDB.FKGROUP, GROUPID);
		if(id==null){
			InsertKeys keys = stmnt.executeInsertKeys(TcDBContext.getDefaultContext());
			id = keys.getPkAsInteger();
		}else{
			stmnt.executeUpdate(TcDBContext.getDefaultContext(), TgroupCategoryDB.PK_PK, id);
		}
		return id;
	}

	/*Definition der Action "hasUserRightToReadAddresses"*/
	public String[] INPUT_hasUserRightToReadAddresses = { "userid", "addressPks" };
	public boolean[] MANDATORY_hasUserRightToReadAddresses = { true, true };
	public String OUTPUT_hasUserRightToReadAddresses = null;
	
	
	/**
	 * Stellt fest, ob ein User Leserecht auf einer Menge von Addressen hat
	 * @param oc der OctopusContext
	 * @param userLogin Login des Users
	 * @param folderRight Recht das abgefragt wird 
	 * @return Exception falls Menge der Adressen, die gelesen werden d�rfen ungleich der Menge der Angefragten Addressen
	 * @throws SQLException
	 * @throws AddressSecurityException
	 */
	public void hasUserRightToReadAddresses(OctopusContext oc, Integer userID, List addressIDs) throws SQLException, AddressSecurityException{

		if (addressIDs.size() == 0)
            return;

		Integer[] array = (Integer[]) addressIDs.toArray(new Integer[0]);
		
		
		OptimizedInList inList = new OptimizedInList(TaddressDB.PK_PKADDRESS, array);
		
		Select subselect = SQL.Select(TcDBContext.getDefaultContext())
			.from(TcDBContext.getSchemaName() + "v_user_address")
			.add("v_user_address.fk_address", Integer.class)
			.where(Expr.equal("v_user_address.userid", oc.personalConfig().getUserID()))
			.whereAnd(Expr.equalColumns("v_user_address.fk_address", TaddressDB.PK_PKADDRESS));
			
 
		Select sel =
				SQL.Select(TcDBContext.getDefaultContext())
			.from(TaddressDB.getTableName())
			.add(TaddressDB.PK_PKADDRESS, Integer.class)
			.where(new RawClause(inList.getOptimezedClause()))
			.whereAnd(Expr.notExists(new SubSelect(subselect)));
		
		List addressesWithNoPermissionToRead = sel.getList(TcDBContext.getDefaultContext());
						
		if (addressesWithNoPermissionToRead != null && addressesWithNoPermissionToRead.size() == 0){
			return;
		}
	
		throw new AddressSecurityException(AddressSecurityException.ERROR_NO_AUTHORISATION_TO_READ_ALL_REQUESTED_ADDRESSES , addressIDs, oc.personalConfig().getUserID() );
		
		
		
//		System.out.println("alte sicherheitsabfrage");
//		String sql = SecurityQueries.hasUserRightToReadAddressesQuery(oc.personalConfig().getUserID(), addressIDs);
//		
//		int resultsize = 0;
//		ResultSet rs;
//		
//		try {
//			Result r = DB.result(TcDBContext.getDefaultContext(), sql);
//			rs = r.resultSet();
//			rs.last();
//			resultsize = rs.getRow();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		if(resultsize == addressIDs.size()){
//			System.out.println("OK");
//			return;
//		}
//		else
//			//Hier muss noch ein Logger hin
//			throw new AddressSecurityException(AddressSecurityException.ERROR_NO_AUTHORISATION_TO_READ_ALL_REQUESTED_ADDRESSES , -1, oc.personalConfig().getUserID() );
//		
	}

    public static String[] INPUT_checkUserRightToAssignAddresses = new String[]{ "addressPks", "addSubCategories", "removeSubCategories" };
    public static boolean[] MANDATORY_checkUserRightToAssignAddresses = new boolean[]{ true, false, false };
    /** 
     * Test, if it is allowed to assign/deassign a set of addresses to subcategories. If not, this method throws an AddressSecurityException.
     * This is allowed, if one of the following conditions match:
     * 1. The user has the right to add the address and the parent-category of the target subcategory.
     * 2. The user has the structure-right in the parent-category of the target subcategory and the address already belongs to this parent-category.
     *
     * This tests have to be performed for each address separately, because some addresses may be only assigneable by one of the.
     *
     * TODO: One special case is not cauth by this tests: A user who has only the structure-right in one category will
     *     be able to remove the address from this category by removing all sub-category-assignements.
     * 
     * @param addressPks List of address ids (the values must providing a meaningfull .toString() Method)
     * @param addSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     * @param removeSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     */
    public void checkUserRightToAssignAddresses(OctopusContext cntx, List addressPks, List addSubCategories, List removeSubCategories) throws SQLException, AddressSecurityException {
        final Integer EDIT = new Integer(SecurityQueries.FOLDERRIGHT_EDIT);
        final Integer STRUCTURE = new Integer(SecurityQueries.FOLDERRIGHT_STRUCTURE);
        final String userLogin = cntx.personalConfig().getUserLogin();

        // Combine subcategories in one list
        int size = 0;
        size += (addSubCategories != null) ? addSubCategories.size() : 0;
        size += (removeSubCategories != null) ? removeSubCategories.size() : 0;
        List allSubCategories = new ArrayList(size);        
        if (addSubCategories != null)
            allSubCategories.addAll(addSubCategories);
        if (removeSubCategories != null)
            allSubCategories.addAll(removeSubCategories);

        if (size == 0)
            return;
        
        // Fetch all parentCategories for the requested categories
        Map parentCategories = getParentCategories(cntx, allSubCategories);

        // Now iterate over the addresses and test for each subcategory
        for (Iterator addressIter = addressPks.iterator(); addressIter.hasNext();) {
            Integer addressID = Conversion.getInteger(addressIter.next());
            for (Iterator subCatIter = allSubCategories.iterator(); subCatIter.hasNext();) {
                Integer subCatId = Conversion.getInteger(subCatIter.next());
                Integer parentCategory = (Integer)parentCategories.get(subCatId);
                if (parentCategory == null)
                    throw new AddressSecurityException(AddressSecurityException.ERROR_INVALID_ID, subCatId.intValue(), userLogin);
                    

                // case 1:
                final boolean rightEditOnFolder = hasUserRightOnFolder(cntx, userLogin, EDIT, parentCategory, subCatId).booleanValue();
                final boolean rightEditOnAddress = hasUserRightOnAddress(cntx, userLogin, EDIT, addressID).booleanValue();
                if (rightEditOnFolder && rightEditOnAddress)
                    continue;
                
                // case 2:
                final boolean rightStructureOnFolder = hasUserRightOnFolder(cntx, userLogin, STRUCTURE, parentCategory, subCatId).booleanValue();                
                final boolean addressBelongsToFolderB = addressBelongsToFolder(cntx, addressID, parentCategory);                
                if (rightStructureOnFolder && addressBelongsToFolderB)
                    continue;
                
                throw new AddressSecurityException(AddressSecurityException.ERROR_NO_AUTHORISATION_TO_CHANGE_ASSIGNMENT,
                                                   addressID.intValue(),
                                                   userLogin);
            }
        }
    }

    /**
     * Tests, if an address belongs to an folder
     *
     */
    protected boolean addressBelongsToFolder(OctopusContext cntx, Integer addressID, Integer folderID) 
        throws SQLException {

        return SQL.Select(TcDBContext.getDefaultContext()).from(TaddresscategoryDB.getTableName())
            .add(TaddresscategoryDB.PK_PK, Integer.class)
			.where(Where.and(Expr.equal(TaddresscategoryDB.FKADDRESS, addressID),
                             Expr.equal(TaddresscategoryDB.FKCATEGORY, folderID)))            
            .getList(TcDBContext.getDefaultContext())
            .size() > 0;
    }


    /**
     * Returns a Map with the parent catecorys for each subcategory in the argument
     *
     * @param allSubCategories List with Integer IDs
     * @return Map with Integer keys and values: pk_subcategory => fkcategory
     */
    protected Map getParentCategories(OctopusContext cntx, List allSubCategories) 
        throws SQLException {

        Map parentCategories = new HashMap(); // pk_subcategory => fkcategory
        Result res = SQL.Select(TcDBContext.getDefaultContext()).from(TsubcategoryDB.getTableName())
            .selectAs(TsubcategoryDB.PK_PKSUBCATEGORY)
            .selectAs(TsubcategoryDB.FKCATEGORY)
			.where(Expr.in(TsubcategoryDB.PK_PKSUBCATEGORY, allSubCategories))
            .executeSelect(TcDBContext.getDefaultContext());
        ResultSet rs = res.resultSet();
        while (rs.next()) {
            parentCategories.put(new Integer(rs.getInt(TsubcategoryDB.PK_PKSUBCATEGORY)),
                                 new Integer(rs.getInt(TsubcategoryDB.FKCATEGORY)));
        }
        res.close();
        return parentCategories;
    }
    
	 public String[] INPUT_MayUserWriteEvent = {"eventid"};
	 public boolean[] MANDATORY_MayUserWriteEvent = { true };
	 public String OUTPUT_MayUserWriteEvent = "right";
	
	 /**
	  * Stellt fest, ob ein User Schreibrecht auf einem Event hat
	  * @param oc
	  * @param eventid: ID des Events
	  * @return
	  */
	 public Boolean MayUserWriteEvent(OctopusContext oc, Integer eventid){
		
		try {
				checkUserMayWriteEvent(oc, eventid, null, null, null, null, null);
			} catch (AddressSecurityException e) {
				return Boolean.FALSE;
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return Boolean.TRUE;		
	}
	
	
	/* Definition der Action checkUserRightOnEvent*/
    public String[] INPUT_checkUserMayWriteEvent = { "eventid", 
                                                     "start", 
                                                     "end", 
                                                     "userid",
                                                     "addressrelationscreateormodify", 
                                                     "schedulerelationscreateormodify"};
    
	public boolean[] MANDATORY_checkUserMayWriteEvent = { false, 
                                                          false, 
                                                          false, 
                                                          false,
                                                          false, 
                                                          false };
	public String OUTPUT_checkUserMayWriteEvent = null;


	
	public static void checkUserMayWriteEvent(OctopusContext oc, 
                                       Integer eventid, 
                                       Long startDate, 
                                       Long endDate,
                                       Integer owner,
                                       List addressRelationsCreateOrModify,
                                       List scheduleRelationsCreateOrModify
                                      ) throws SQLException, AddressSecurityException {
		
		Integer userid = oc.personalConfig().getUserID();

        // Neu Anlegen oder �ndern
        if (owner != null && startDate != null && endDate != null) {

            // Als userid wurde der owner �bergeben oder der owner kommt aus dem Termin selbst.
            // Check, ob ich der owner bin, oder ich Schreibrechte auf den Kalender des Owners 
            // im entsprechenden Zeitraum habe.
            
            if (!userid.equals(owner)) {
                // User != owner, also Test auf Schreibrecht im Ownerkalender
                Integer scheduleID = getScheduleIDByOwner(oc.getModuleName(), owner);
                if (! hasUserRightOnSchedule(oc, userid, scheduleID, SCHEDULE_RIGHT_WRITE, startDate, endDate).booleanValue()) {
                    throw new AddressSecurityException(SCHEDULE_RIGHT_WRITE.intValue() + 99, -1, userid.intValue());
                }
            }            
        }

        // �ndern oder l�schen
		if (eventid != null) {
            checkRightOnEvent(oc, eventid, SCHEDULE_RIGHT_WRITE);
		}
		
		if (eventid == null && (scheduleRelationsCreateOrModify == null || scheduleRelationsCreateOrModify.size() == 0)){
			
			/*	Wenn keine Eventid angegeben ist, soll ein neues Event angelegt werden
			 	Wenn jedoch keine Kalender angegeben sind, mit denen das Event verkn�pft 
			 	werden soll, wird dies verhindert	*/

			throw new AddressSecurityException(SCHEDULE_RIGHT_WRITE.intValue() + 99, -1, userid.intValue());	
		}



        // Anf�gen von Adressen.
        // Es darf nicht m�glich sein, einen Termin mit einer Adresse zu verkn�pfen,
        // die ich selbst nicht sehen darf.
		if (addressRelationsCreateOrModify != null && addressRelationsCreateOrModify.size() > 0) {

			/* 		 
			 Wenn Kalender mit Adressen verkn�pft werden sollen, werden
			 diese Adressen aus der Map geholt und auf Berechtigung gecheckt		  
			*/			
		    for (Iterator it = addressRelationsCreateOrModify.iterator();it.hasNext();) {
		        List entry = (List)it.next();
		    	Integer addressid = (Integer)entry.get(6);
                
				if (!(hasUserRightOnAddress(oc, oc.personalConfig().getLoginname(), new Integer(1), addressid).booleanValue())){
					throw new AddressSecurityException(SCHEDULE_RIGHT_WRITE.intValue() + 99, addressid.intValue(), oc.personalConfig().getLoginname());
				}
		    }
		}		

				
        // ScheduleRelations m�ssen nicht getestet werden, 
        // da ich durch das Einladen zu jedem beliebiegen Kalender Relations definieren kann,
        // sobald ich Schreibrechte auf den Termin habe.


        // TODO: Die �nderungen zum best�tigen bzw. Ablehnen m�ssen durch gelassen werden.
        
	} 

    public String[] INPUT_checkRightOnEvent = { "eventid", 
                                                "scheduleright"};
    
	public boolean[] MANDATORY_checkRightOnEvent = { true, 
                                                     true };
	public String OUTPUT_checkRightOnEvent = null;
    public static void checkRightOnEvent(OctopusContext oc, Integer eventid, Integer right) throws AddressSecurityException, SQLException {

		Integer userid = oc.personalConfig().getUserID();

        // Check, ob ich der Owner des Termins bin, 
        // oder ob ich Schreibrechte auf den Kalender des Owners im entsprechenden Zeitraum habe.

        // Holen der Eventdaten
        Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TeventDB.getTableName())
            .selectAs(TeventDB.EVENTSTART)
            .selectAs(TeventDB.EVENTEND)
            .selectAs(TeventDB.FKUSERMANAGER)
            .where(Expr.equal(TeventDB.PK_PK, eventid));
        
        Integer eventOwner = null;
        Long eventStart = null;
        Long eventEnd = null;
        Result tr = null;
    	try {
            tr = DB.result(TcDBContext.getDefaultContext(), select);
            if (tr.resultSet().next()) {
                eventOwner = new Integer(tr.resultSet().getInt(TeventDB.FKUSERMANAGER));
                eventStart = new Long(tr.resultSet().getTimestamp(TeventDB.EVENTSTART).getTime());
                eventEnd = new Long(tr.resultSet().getTimestamp(TeventDB.EVENTEND).getTime());
            } else
                throw new SQLException("Event mit der ID "+ eventid +" ist nicht vorhanden");            
        } finally {
        	if (tr != null)
        		tr.close();
        }

        if (right.equals(SCHEDULE_RIGHT_WRITE)) {
            // Beim Schreibrecht muss der User Schreibrechte auf den Kalender des event-owners haben
            Integer scheduleId = getScheduleIDByOwner(oc.getModuleName(), eventOwner);            
            if (hasUserRightOnSchedule(oc, userid, scheduleId, right, eventStart, eventEnd).booleanValue())
                return;
        } else {
            // Sonst muss er nur Leserechte auf irgend einen Kalender haben, in dem das Event enthalten ist
            List schedules = SQL.Select(TcDBContext.getDefaultContext())
	        	.from(TscheduleeventDB.getTableName())
	        	.add(TscheduleeventDB.FKSCHEDULE, Integer.class)			// [0]
	        	.byId(TscheduleeventDB.FKEVENT, eventid)							
	        	.getList(oc.getModuleName());
            
            for (Iterator schedule = schedules.iterator(); schedule.hasNext();) {
                Integer scheduleId = (Integer)schedule.next();
                
                if (hasUserRightOnSchedule(oc, userid, scheduleId, right, eventStart, eventEnd).booleanValue())
                    return;
            }            
        }
        throw new AddressSecurityException(right.intValue() + 99, -1, userid.intValue());
    }    


	/*Definition der Action "hasUserRightOnSchedule"*/
	public String[] INPUT_hasUserRightOnSchedule = { "userid", "scheduleid", "scheduleRight", "start", "end"};
	public boolean[] MANDATORY_hasUserRightOnSchedule = { true, true, true, true, true };
	public String OUTPUT_hasUserRightOnSchedule = "right";
	/**
	 * Stellt fest, ob ein User ein bestimmtes Recht auf einem Kalender hat
	 * @param oc der OctopusContext
	 * @param userid ID des Users
	 * @param scheduleid Die ID des Kalenders
	 * @param scheduleRight Das zu pr�fende Recht
	 * @return Boolean.TRUE, falls Recht besteht, Boolean.FALSE sonst
	 * @throws SQLException 
	 */
	public static Boolean hasUserRightOnSchedule(OctopusContext oc, Integer userid, Integer scheduleid, Integer scheduleRight, Long start, Long end) throws SQLException {
		String sql = SecurityQueries.getUserHasRightOnScheduleQuery(userid, scheduleid, scheduleRight, start, end);        
        Result r = DB.result(TcDBContext.getDefaultContext(), sql);
		return new Boolean( r.resultSet().next() );
	}
	

	/*Definition der Action "isUserOwnerOfSchedule"*/
	public String[] INPUT_isUserOwnerOfSchedule = { "scheduleid", "userscheduleid"};
	public boolean[] MANDATORY_isUserOwnerOfSchedule = { false, false };
	public String OUTPUT_isUserOwnerOfSchedule = null;
	/**
	 * Stellt fest, ob ein User Besitzer eines Kalenders ist
	 * @param oc der OctopusContext
	 * @param userid ID des Users
	 * @param scheduleid Die ID des Kalenders
	 * @throws SQLException 
	 */
	/* ******************************************************************************************* */
	/* 
	/* ******************************************************************************************* */
	
	public void isUserOwnerOfSchedule(OctopusContext oc, Integer scheduleid, Integer userscheduleid) throws SQLException, AddressSecurityException{
		
		if(!checkUserIsOwnerOfSchedule(oc, scheduleid, userscheduleid))
			throw new AddressSecurityException(AddressSecurityException.ERROR_NO_AUTHORISATION_TO_EDIT_SCHEDULE, scheduleid.intValue(), oc.personalConfig().getLoginname());
		
	}
	
	private boolean checkUserIsOwnerOfSchedule(OctopusContext oc, Integer scheduleid, Integer userscheduleid) throws SQLException, AddressSecurityException{
		
		Integer userid = oc.personalConfig().getUserID();
		
		//System.out.println("Checke isUserOwnerOfSchedule auf schedule " + scheduleid);
		if (scheduleid != null){
				
			String sql = SecurityQueries.getIsUserOwnerOfScheduleQuery(userid, scheduleid);
			Result r = DB.result(TcDBContext.getDefaultContext(), sql);
			ResultSet rs = r.resultSet();
			boolean vorhanden = rs.next();
			
			//System.out.println("Is User Owner of Schedule " + scheduleid + ": " + vorhanden);
			
			if(!vorhanden)
				return false;
		}
		
		else {
							
			if (userscheduleid != null){
				// Manchmal wird die ID des Eintrags in tuserschedule angegeben (um z.B. den Eitrag zu l�schen)
				// Dann muss der entsprechend verkn�pfte Kalender wiederum durch isUserOwnerOfSchedule gepr�ft werden
			
				String sql = 	"SELECT fk_schedule AS \"fk_schedule\" FROM " + TcDBContext.getSchemaName() + "tuserschedule " + 
								"WHERE tuserschedule.pk = " + userscheduleid;
				Result r = DB.result(TcDBContext.getDefaultContext(), sql);
				ResultSet rs = r.resultSet();
				
				while (rs.next()){
					checkUserIsOwnerOfSchedule(oc, new Integer(rs.getInt("fk_schedule")), null);
				}
			}
			else

				// Falls keine ID f�r Kalender oder tuserschedule angegeben wurde, wird nicht versucht einen bestehenden Kalender zu manipulieren, sondern
				// ein neuer Kalender angelegt. Daf�r muss das globale Recht gepr�ft werden
				checkUserGlobalRight(oc, oc.personalConfig().getLoginname(), new Integer(12));
		}
		
		return true;
	}
	
	
	/*Definition der Action "convertStringPkListtoList"*/
	public String[] INPUT_convertStringPkListtoList = { "addressPks", "pklistSeperator" };
	public boolean[] MANDATORY_convertStringPkListtoList = { true, true };
	public String OUTPUT_convertStringPkListtoList = "addressPks";
	/**
	 * Wenn eine PK-Liste durch einen String angegeben ist, kann diese Methode die String-Liste
	 * in eine echte Liste umwandeln
	 * Diese Liste kann dann in 'hasUserRightToReadAddresses' einfach �berpr�ft werden
	 * @param oc der OctopusContext
	 * @param stringPkList Liste der Pks als konkatenierter String
	 * @param seperator Trennzeichen zur Trennung der Pks in der String-PK-Liste
	 * @return List die Pks in Listenform
	 */

	public List convertStringPkListtoList(OctopusContext oc, String stringPkList, String seperator){
		
		List pkList = new ArrayList();
		
		for (StringTokenizer st = new StringTokenizer(stringPkList, seperator);st.hasMoreTokens();) {
			pkList.add(new Integer (Integer.parseInt(st.nextToken())));            
        }
		
		return pkList;
	}
	
	
	
	final static public String[] INPUT_convertContactListIntoAddressList = {"contactlist"};
	//final static public boolean[] MANDATORY_convertContactListIntoAddressList = {true};
	final static public String OUTPUT_convertContactListIntoAddressList = "addressPks";
	
	/**
	 * Diese Methode holt die AdressPks aus der Liste "Contaclist",
	 * so dass die Berechtigung f�r diese in 'hasUserRightToReadAddresses' gepr�ft werden kann
	 * @param oc der OctopusContext
	 * @param  Liste der Contacteintr�ge inkl AddressPks
	 * @return List die extrahierten Pks in Listenform
	 */

	public List convertContactListIntoAddressList(OctopusContext oc,  List contactlist){
		
		Integer addressid;
		Map contact; 
		List pkList = new ArrayList();
        
		
		for (Iterator iter = contactlist.iterator(); iter.hasNext();) {
			contact = (Map)iter.next();
			addressid = (Integer)contact.get("addressid");
			pkList.add(addressid);
		}
		return pkList;
	}
    
    /**
     * Liefert die ID des Schedules, der dem angegebenen User geh�rt
     */
    private static Integer getScheduleIDByOwner(String dbPoolName, Integer ownerId) throws SQLException {
        Select select =
            SQL.Select(TcDBContext.getDefaultContext())
            .from(TscheduleDB.getTableName())
            .selectAs(TscheduleDB.PK_PK)
            .where(Expr.equal(TscheduleDB.FKUSER, ownerId));
        

        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select);            
            if (tr.resultSet().next())
                return new Integer(tr.resultSet().getInt(TscheduleDB.PK_PK));

            return null;
        } finally {
            if (tr != null)
                tr.close();
        }               
    }

}