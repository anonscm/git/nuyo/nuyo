/* $Id: UserManagerDB.java,v 1.11 2007/06/15 15:58:28 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Sebastian Mancke and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.user;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.commons.utils.StringTools;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.octopus.security.TcSecurityException;
import de.tarent.octopus.server.UserManager;

/** 
 * Implementierung eines LoginManagers, �ber die Tarent-Contact DB direkt.
 * 
 * @author Sebastian Mancke
 */
public class UserManagerDB implements UserManager {

    String moduleName;
    Integer lastInsertedId;
    
	static Logger logger = Logger.getLogger(UserManagerDB.class.getName());
    
    protected InsertUpdate createInsertUpdate(String userID, String firstName, String lastName, String password) {
        InsertUpdate insertUpdate = SQL.InsertUpdate(TcDBContext.getDefaultContext())
        .table(TuserDB.getTableName());
        insertUpdate.add(TuserDB.LOGINNAME, userID);
        insertUpdate.add(TuserDB.FIRSTNAME, firstName);
        insertUpdate.add(TuserDB.LASTNAME, lastName);
        if (password != null && ! "".equals(password))
			try {
				insertUpdate.add(TuserDB.PWD, StringTools.md5(password));
				insertUpdate.add(TuserDB.ISPWDENCRYPTED, new Integer(1));
			} catch (NoSuchAlgorithmException e) {
				insertUpdate.add(TuserDB.PWD, password);
				insertUpdate.add(TuserDB.ISPWDENCRYPTED, new Integer(0));
				if(logger.isLoggable(Level.WARNING)){
					logger.log(Level.WARNING, e.getLocalizedMessage());
				}
			}		
			
			
        return insertUpdate;
    }
    
    public void addUser(String userID, String firstName, String lastName, String password) 
        throws TcSecurityException {

        if (getModuleName() == null)
            throw new TcSecurityException("Das Modul muss vor Aktionen mit dem UserManagerDB gesetzt sein.");

        try {
            InsertKeys keys = createInsertUpdate(userID, firstName, lastName, password)
                .executeInsertKeys(TcDBContext.getDefaultContext());
            lastInsertedId = keys.getPkAsInteger();
        } catch (SQLException sqle) {
            logger.log(Level.SEVERE, "Fehler beim Anlegen des Benutzers in der DB", sqle);
            throw new TcSecurityException("Fehler beim Anlegen des Benutzers in der DB", sqle);
        }
    }

	public void modifyUser(String userID, String firstName, String lastName, String password) 
        throws TcSecurityException {

        if (getModuleName() == null)
            throw new TcSecurityException("Das Modul muss vor Aktionen mit dem UserManagerDB gesetzt sein.");
        
        try {
			Update update = createInsertUpdate(userID, firstName, lastName, password).update();
            update.where(Expr.equal(TuserDB.LOGINNAME, userID));
			DB.update(TcDBContext.getDefaultContext(), update);
        } catch (SQLException sqle) {
            logger.log(Level.SEVERE, "Fehler beim �ndern des Benutzers in der DB", sqle);
            throw new TcSecurityException("Fehler beim �ndern des Benutzers in der DB", sqle);
        }
    }

	public void setUserParam(String userID, String paramname, Object paramvalue) throws TcSecurityException {
        logger.log(Level.WARNING, "Not supported call to UserManagerDB.setUserParam()");
    }

	public Object getUserParam(String userID, String paramname) throws TcSecurityException {
        logger.log(Level.WARNING, "Not supported call to UserManagerDB.getUserParam()");
        return null;
    }

	public void deleteUser(String userID) throws TcSecurityException {
        if (getModuleName() == null)
            throw new TcSecurityException("Das Modul muss vor Aktionen mit dem UserManagerDB gesetzt sein.");

        try {
            SQL.Delete(TcDBContext.getDefaultContext())
                .from(TuserDB.getTableName())
                .where(Expr.equal(TuserDB.LOGINNAME, userID))
                .executeDelete(TcDBContext.getDefaultContext());
        } catch (SQLException sqle) {
            logger.log(Level.SEVERE, "Fehler beim L�schen des Benutzers in der DB", sqle);
            throw new TcSecurityException("Fehler beim L�schen des Benutzers in der DB", sqle);
        }
    }

    public String getModuleName() {
        return moduleName;
    }
    
    public void setModuleName(String newModuleName) {
        this.moduleName = newModuleName;
    }

    public Integer getLastInsertedId() {
        return lastInsertedId;
    }
    
    public void passwdfix(String username) throws SQLException, TcSecurityException{
    	Select select = SQL.Select(TcDBContext.getDefaultContext())
    							.from(TuserDB.getTableName())
    							.selectAs(TuserDB.ISPWDENCRYPTED)
    							.selectAs(TuserDB.PWD)
    							.selectAs(TuserDB.FIRSTNAME)
    							.selectAs(TuserDB.LASTNAME)
    							.where(Expr.equal(TuserDB.LOGINNAME, username));
    	Result r = select.executeSelect(TcDBContext.getDefaultContext());
    	if(!r.resultSet().next()){
    		//for now: ignore
    	}else{
	    	int ispwdencrypted = r.resultSet().getInt(TuserDB.ISPWDENCRYPTED);
	    	if(ispwdencrypted!=1){
	    		String password=r.resultSet().getString(TuserDB.PWD);
	    		String firstName = r.resultSet().getString(TuserDB.FIRSTNAME);
	    		String lastName = r.resultSet().getString(TuserDB.LASTNAME);
	    		modifyUser(username, firstName, lastName, password);
	    	}
    	}
    }
}