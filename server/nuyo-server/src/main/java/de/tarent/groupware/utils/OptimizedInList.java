/* $Id: OptimizedInList.java,v 1.4 2007/03/12 15:10:46 nils Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Sebastian Mancke
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


/**
 * Hilfsklasse um eine Where Clause der Form 'key IN (int1, int2, int3)' optimiert dar zu stellen.
 */
public class OptimizedInList {

    String key;
    Integer[] values;


    public OptimizedInList(String key, Integer[] values) {
        this.key = key;
        this.values = values;
    }

    public String getOptimezedClause() {
        Arrays.sort(values);
        List parts = getParts();
        if (parts.size() == 0)
            return "FALSE";
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        for (Iterator iter = parts.iterator(); iter.hasNext();) {
            Object part = iter.next();
            if (part instanceof Integer) {
                sb.append(key)
                    .append(" = ")
                    .append(((Integer)part).intValue());
            } 
            else if (part instanceof Between) {
                sb.append(key)
                    .append(" BETWEEN ")
                    .append(((Between)part).lower.intValue())
                    .append(" AND ")
                    .append(((Between)part).upper.intValue());
            }
            if (iter.hasNext())
                sb.append(" OR ");            
        }  
        sb.append(")");
        return sb.toString();
    }
    
    protected List getParts() {
        List parts = new LinkedList();
        Integer start = null;
        Integer last = null;
        for (int i=0; i<values.length; i++) {
            if (start == null) {
                start = last = values[i];                
            }
            else if (values[i].intValue() == last.intValue()+1)
                last = values[i];
            else {
                if (start.intValue() == last.intValue())
                    parts.add(start);
                else
                    parts.add(new Between(start, last));                
                start = last = values[i];
            }
        }
        if (start != null) {
            // last == null kann eigentlich nicht passieren, aber voricht ist besser als ...
            if (last == null || start.intValue() == last.intValue())
                parts.add(start);
            else
                parts.add(new Between(start, last));
        }
        return parts;
    }

    /**
     * Converts a List with Numbers to an Integer Array
     * @param list containing Integer objects or objects with a number repräsentation in the toString() method
     */
    public static Integer[] getIntegerArray(List list) {
        Integer[] keys = new Integer[list.size()];
        for (int i=0; i<keys.length; i++) {
            Object o = list.get(i);
            if (o instanceof Integer)
                keys[i] = (Integer)o;
            else
                keys[i] = new Integer(o.toString());
        }
        return keys;
    }

    class Between {
        Integer lower;
        Integer upper;

        public Between(Integer lower, Integer upper) {
            this.lower = lower;
            this.upper = upper;
        }
    }

}
