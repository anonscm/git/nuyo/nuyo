package de.tarent.groupware.utils;

import java.util.LinkedList;
import java.util.List;

import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Clause;
import de.tarent.dblayer.sql.clause.RawClause;

/**
 * Helferklasse zum Aufbau einer Where Clause aus einer Liste
 * von Filterausdr�cken in umgekehrt polnischer Notation.
 */
public class FilterCreater {

    EntityDescription entityDescription;

    public FilterCreater(EntityDescription entityDescription) {
        this.entityDescription = entityDescription;
    }


    
    public Clause getWhereList(List<Object> filter) 
        throws FilterCreaterParsingException {

        try {
            filter = new LinkedList<Object>(filter);
            LinkedList<Object> stack = new LinkedList<Object>();
            while (filter.size() > 0) {
                Object tokenObject = filter.remove(0);
                // evtl. m�ssen unterschiedliche Datentypen 
                // (z.B. Date) hier anders behandelt werden, wenn man das ben�tigt.
                String token = tokenObject == null ? "" : tokenObject.toString();
                
                switch (getConsuminArgs(token)) 
                    {
                    case 0 :
                        String dbKey = entityDescription.getDBKeyByProperty(token);
                        if (dbKey != null)
                            stack.add(dbKey);
                        else
                        	stack.add(SQL.format(TcDBContext.getDefaultContext(), tokenObject));
                            //stack.add("'"+ token.replaceAll("'", "\\\\'") +"'");                    
                        break;
                    case 1 :
                        stack.add(apply(token, stack.removeLast()));
                        break;
                    case 2 :
                        stack.add(apply(token, stack.removeLast(), stack.removeLast()));
                        break;
                    }
            
                //                 System.out.println("stack: "+stack);
                //                 System.out.println("filter: "+filter);            
                //                 System.out.println();
                //                 System.out.println();
            
            }
            if (stack.size() > 0)
                return (Clause)stack.removeLast();
            return null;
        } catch (Exception e) {
            throw new FilterCreaterParsingException("Fehler beim �bersetzten der Filterliste in einen Filterbaum", e);
        }
    }

    public int getConsuminArgs(String token)
        throws FilterCreaterParsingException {

        if ("=".equals(token))
            return 2;
        else if ("<".equals(token))
            return 2;
        else if (">".equals(token))
            return 2;
        else if ("OR".equals(token))
            return 2;
        else if ("AND".equals(token))
            return 2;
        else if ("NOT".equals(token))
            return 1;
        else if ("LIKE".equals(token))
            return 2;
        else if ("ILIKE".equals(token))
            return 2;

        return 0;
    }

    /**
     * Der Dreher in der Signatur ist absicht! 
     */
    protected Clause apply(String operator, Object right, Object left) 
        throws FilterCreaterParsingException {

        if (left instanceof Clause)
            left = ((Clause)left).clauseToString(TcDBContext.getDefaultContext());
        if (right instanceof Clause)
            right = ((Clause)right).clauseToString(TcDBContext.getDefaultContext());

        return new RawClause( "("+left +" "+ operator +" "+ right+")");
    }
    
    protected Clause apply(String operator, Object right) 
        throws FilterCreaterParsingException {

        if (right instanceof Clause)
            right = ((Clause)right).clauseToString(TcDBContext.getDefaultContext());

        return new RawClause("("+operator +" "+ right+")");
    }

    public class FilterCreaterParsingException extends Exception {
        /**
		 * 
		 */
		private static final long serialVersionUID = 9038152539240722172L;

		public FilterCreaterParsingException(String msg, Exception e) {
            super( msg, e );
        }

        public FilterCreaterParsingException(String msg ) {
            super( msg );
        }        
    }
}