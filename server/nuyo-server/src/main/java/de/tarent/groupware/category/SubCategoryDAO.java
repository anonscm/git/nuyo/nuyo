/**
 * 
 */
package de.tarent.groupware.category;

import java.sql.SQLException;
import java.util.List;

import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.sql.ParamValueList;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.SubCategory;

/**
 *
 * @author Nils Neumaier, tarent GmbH
 */
public class SubCategoryDAO extends AbstractDAO {

	private static SubCategoryDAO instance = null;
	
	SubCategoryDBMapping mapping = new SubCategoryDBMapping(TcDBContext.getDefaultContext());
	SubCategoryEntityFactory entityFactory =  SubCategoryEntityFactory.getInstance();

    protected SubCategoryDAO() {
        super();
        setDbMapping(mapping);
        setEntityFactory(entityFactory);
    }
	
    /**
     * Returns the instance
     */
    public static synchronized SubCategoryDAO getInstance() {
		if(instance == null){
			instance = new SubCategoryDAO();
		}
		return instance;
	}
    
    
     /**
      * Returns one CatCategoryegory by
      * selected by the given primary key pk.
      * @param dbc The database context.
      * @param pk Primary key. 
      * @return One Categroy
      */
    public SubCategory getSubCategoryByPk(DBContext dbc, Integer pk) throws SQLException {
        return (SubCategory)getEntityByIdFilter(dbc, mapping.STMT_SELECT_ONE, SubCategory.PROPERTY_ID.getKey(), pk);
    }
    

    /**
     * List all subcategories of one category
     */
    public List getAllSubCategoriesForOneCategory(DBContext dbc, Integer userId, Integer categoryPk) throws SQLException {
        ParamValueList paramList = new ParamValueList();
        Select selectClone = (Select)mapping.getQuery(mapping.STMT_SELECT_ALL_OF_ONE_CATEGORY).clone();
        selectClone.getParams(paramList);
        paramList.setAttribute(mapping.PARAM_SELECTING_USER_ID, userId);
        paramList.setAttribute(mapping.PARAM_SELECTING_CATEGORY_ID, categoryPk);
        return getEntityList(dbc, selectClone, null);
    }

    
	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDAO#setEntityKeys(de.tarent.dblayer.engine.InsertKeys, java.lang.Object)
	 */
	public void setEntityKeys(InsertKeys keys, Object entity) {
        ((SubCategory)entity).setId(keys.getPk());
	}
    
}
