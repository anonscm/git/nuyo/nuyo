/* $Id: Decode.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Date;

import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPOnePassSignature;
import org.bouncycastle.openpgp.PGPPBEEncryptedData;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSignature;

/**
 * Klasse zum dekodieren von InputStreams.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public class Decode extends AbstractDecode {
	protected String filename;
	protected Date modification;
	protected boolean integrityCheck = false;
	protected boolean integrityVerify = false;
	protected boolean signatureVerify = false;

	protected PGPPublicKey publicKey;
	protected PGPPrivateKey privateKey;
	protected char[] passphrase;

	public void setInput(File file) throws IOException {
		this.dataSource = new FileDataSource(file);
	}

	public void setInput(DataSource dataSource) throws IOException {
		this.dataSource = dataSource;
	}

	public void setPublicKey(PGPPublicKey publicKey) throws GeneralSecurityException {
		this.publicKey = Crypto.assertKey(publicKey);
	}

	public void setPrivateKey(PGPPrivateKey privateKey) throws GeneralSecurityException {
		this.privateKey = Crypto.assertKey(privateKey);
	}

	public void setPassphrase(char[] passphrase) throws GeneralSecurityException {
		this.passphrase = Crypto.assertPass(passphrase);
	}

	public boolean isIntegrityCheck() throws GeneralSecurityException {
		if (!isDecrypted()) throw new GeneralSecurityException("data not decrypted");
		return integrityCheck;
	}

	public boolean isIntegrityVerify() throws GeneralSecurityException {
		if (!isDecrypted()) throw new GeneralSecurityException("data not decrypted");
		return integrityVerify;
	}

	public boolean isSignatureVerify() throws GeneralSecurityException {
		if (!isVerified()) throw new GeneralSecurityException("data not verified");
		return signatureVerify;
	}

	protected void decode(PGPPBEEncryptedData data) throws GeneralSecurityException, PGPException, IOException {
		decode(data.getDataStream(Crypto.assertPass(passphrase), Crypto.PROVIDER));
		integrityCheck = data.isIntegrityProtected();
		integrityVerify = data.isIntegrityProtected() && data.verify();
	}

	protected void decode(PGPPublicKeyEncryptedData data) throws GeneralSecurityException, PGPException, IOException {
		decode(data.getDataStream(Crypto.assertKey(privateKey), Crypto.PROVIDER));
		integrityCheck = data.isIntegrityProtected();
		integrityVerify = data.isIntegrityProtected() && data.verify();
	}

	protected void decode(PGPLiteralData data) throws GeneralSecurityException, PGPException, IOException {
		filename = data.getFileName();
		modification = data.getModificationTime();
		if (modification.getTime() == 0L)
			modification = null;
		pipe(data.getDataStream(), outputStream);
	}

	protected void verify(InputStream inputStream, PGPSignature signature) throws GeneralSecurityException, PGPException, IOException {
		signature.initVerify(Crypto.assertKey(publicKey), Crypto.PROVIDER);
		
		int i;
		byte buffer[] = new byte[BUFFERSIZE];
		while ((i = inputStream.read(buffer)) >= 0) {
			signature.update(buffer, 0, i);
		}
		
		signatureVerify = signature.verify();
	}

	protected void verify(InputStream inputStream, PGPOnePassSignature onePassSignature, PGPSignature signature) throws GeneralSecurityException, PGPException, IOException {
		onePassSignature.initVerify(Crypto.assertKey(publicKey), Crypto.PROVIDER);
		
		int i;
		byte buffer[] = new byte[BUFFERSIZE];
		while ((i = inputStream.read(buffer)) >= 0) {
			onePassSignature.update(buffer, 0, i);
		}
		
		signatureVerify = onePassSignature.verify(signature);
	}

	protected void clear() {
		Crypto.clearPass(passphrase);
	}
}
