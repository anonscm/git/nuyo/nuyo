/**
 * 
 */
package de.tarent.groupware.category;

import java.util.Map;

import de.tarent.contact.bean.TcategoryDB;
import de.tarent.groupware.Category;
import de.tarent.groupware.utils.EntityDescriptionAdapter;

/**
 * Description of a Category
 * 
 * @author kirchner
 */
public class CategoryEntityDescription extends EntityDescriptionAdapter {

	public final String CATEGORY_ID = Category.PROPERTY_ID.getKey();
	public final String CATEGORY_NAME = Category.PROPERTY_CATEGORYNAME.getKey();
	
	
	/**
	 * @see de.tarent.groupware.utils.EntityDescriptionAdapter#addPropertyToDBKeyMappings(java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void addPropertyToDBKeyMappings(Map mapping) {
		mapping.put(CATEGORY_ID, TcategoryDB.PK_PKCATEGORY); 
		mapping.put(CATEGORY_NAME, TcategoryDB.CATEGORYNAME);
	}

	/**
	 * @see de.tarent.groupware.utils.EntityDescriptionAdapter#getEntityName()
	 */
	@Override
	public String getEntityName() {
		return "category";
	}

}
