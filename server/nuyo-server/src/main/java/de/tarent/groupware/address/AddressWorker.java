package de.tarent.groupware.address;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.tarent.commons.datahandling.ListFilterImpl;
import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.commons.logging.TimeMeasureTool;
import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddresscategoryDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.octopus.db.TcCommDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Clause;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.GroupBy;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.Address;
import de.tarent.groupware.Constants;
import de.tarent.groupware.category.CategoryDAO;
import de.tarent.groupware.category.CategoryList;
import de.tarent.octopus.server.OctopusContext;

/**
 * @author Sebastian Mancke, s.mancke@tarent.de
 * 
 * AddressService - Basic functionality for addresses.
 */
public class AddressWorker {

    private static final Log logger = LogFactory.getLog(AddressWorker.class);    

    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_ADDRESS_ID = "addressid";
    public static final String PARAM_ADDRESSES_FILTER = "addressesFilter";
    public static final String PARAM_CATEGORIES_FILTER = "categoriesFilter";
    public static final String PARAM_DISJUNCTION_SUBCATEGORIES = "disjunctionSubcategories";
    public static final String PARAM_CONJUNCTION_SUBCATEGORIES = "conjunctionSubcategories";
    public static final String PARAM_NEGATED_SUBCATEGORIES = "negatedSubcategories";
    public static final String PARAM_ADDRESS_PKS = "addressPks";
    public static final String PARAM_ADD_SUB_CATEGORIES = "addSubCategories";

    public static final String PARAM_COUNT = "addressesCount";

    private static final int TCOMM_TYPE_TEL_WEITERES = 111;
    private static final int TCOMM_TYPE_FAX_WEITERES = 112;
    private static final int TCOMM_TYPE_HANDY_WEIERES= 113;
    private static final int TCOMM_TYPE_EMAIL_WEITERE = 114;
    private static final int TCOMM_TYPE_HOMEPAGE_WEITERE = 115;
    private static final int TCOMM_TYPE_HOMEPAGE_DI = 110;
    private static final int TCOMM_TYPE_HOMEPAGE_PRI = 109;
    private static final int TCOMM_TYPE_EMAIL2 = 107;
    private static final int TCOMM_TYPE_EMAIL1 = 108;
    private static final int TCOMM_TYPE_FAX_PRI = 103;
    private static final int TCOMM_TYPE_FAX_DI = 104;
    private static final int TCOMM_TYPE_HANDY_PRI = 105;
    private static final int TCOMM_TYPE_HANDY_DI = 106;
    private static final int TCOMM_TYPE_TEL_PRI = 101;
    private static final int TCOMM_TYPE_TEL_DI = 102;

    
    public String[] INPUT_getAddresses = {PARAM_DISJUNCTION_SUBCATEGORIES, PARAM_CONJUNCTION_SUBCATEGORIES, PARAM_NEGATED_SUBCATEGORIES};
    public boolean[] MANDATORY_getAddresses = { false, false, false };
	public String OUTPUT_getAddresses = "addresses";
    
	public AddressList getAddresses(OctopusContext cntx,
                                    List disjunctionSubcategories,
                                    List conjunctionSubcategories,
                                    List negatedSubcategories) throws SQLException {
        
        ListFilterImpl filter = new ListFilterImpl();
        filter.init(cntx, PARAM_ADDRESSES_FILTER);
        TimeMeasureTool tm = TimeMeasureTool.getMeasureTool(logger);
        List list = AddressDAO.getInstance().getAddressesByFilter(TcDBContext.getDefaultContext(), 
                                                                  cntx.personalConfig().getUserID(), 
                                                                  filter,
                                                                  ensureIntegerList(disjunctionSubcategories),
                                                                  ensureIntegerList(conjunctionSubcategories),
                                                                  ensureIntegerList(negatedSubcategories));
        

        tm.step("getAddresses with #"+list.size());
        
        cntx.setContent(PARAM_COUNT, new Integer(filter.getCount()));        
        return new AddressList(list);
    }



    public String[] INPUT_getAddressPks = INPUT_getAddresses;
    public boolean[] MANDATORY_getAddressPks = MANDATORY_getAddresses;
	public String OUTPUT_getAddressPks = PARAM_ADDRESS_PKS;
    
	public PrimaryKeyList getAddressPks(OctopusContext cntx,
                                        List disjunctionSubcategories,
                                        List conjunctionSubcategories,
                                        List negatedSubcategories) throws SQLException {
        
        ListFilterImpl filter = new ListFilterImpl();
        filter.init(cntx, PARAM_ADDRESSES_FILTER);
        PrimaryKeyList list = AddressDAO.getInstance().getAddressPks(TcDBContext.getDefaultContext(), 
                                                                     cntx.personalConfig().getUserID(), 
                                                                     filter,
                                                                     ensureIntegerList(disjunctionSubcategories),
                                                                     ensureIntegerList(conjunctionSubcategories),
                                                                     ensureIntegerList(negatedSubcategories));
        return list;
    }


    /**
     * Ensures, that only integer values are in the list, the supllied list will be modified my this method
     * @param List ob objects, may be null
     */
    private List ensureIntegerList(List idList) {
        if (idList == null)
            return null;

        boolean modify = false;
        for (ListIterator iter = idList.listIterator(); iter.hasNext();) {
            if (! (iter.next() instanceof Integer)) {
                modify = true;
                break;
            }
        }
        if (!modify)
            return idList;

        List newList = new ArrayList(idList.size());
        for (ListIterator iter = idList.listIterator(); iter.hasNext();) {
            Object element = iter.next();
            if (! (element instanceof Integer))
                element = new Integer(""+element);
            newList.add(element);
        }
        return newList;
    }

    public String[] INPUT_getAddressByPk = { PARAM_ADDRESS_ID };
	public String OUTPUT_getAddressByPk = "address";
	
	public Address getAddressByPk(OctopusContext all, Integer addressId) throws SQLException {
        return AddressDAO.getInstance().getAddressByPk(TcDBContext.getDefaultContext(), addressId);
    }

    public String[] INPUT_deleteAddress = { PARAM_ADDRESS_ID };
	
	public void deleteAddress(OctopusContext all, Integer addressId) throws SQLException {
        Address address = AddressDAO.getInstance().getAddressByPk(TcDBContext.getDefaultContext(), addressId);
        if (address == null)
            throw new IllegalArgumentException("no address with the supplied id");
        AddressDAO.getInstance().delete(TcDBContext.getDefaultContext(), address);
    }

    public String[] INPUT_createAddress = { PARAM_ADDRESS };
	public String OUTPUT_createAddress = "address";
	
	public Address createAddress(OctopusContext all, Address address) throws SQLException {
		AddressDAO.getInstance().insert(TcDBContext.getDefaultContext(), address);
        updateCommunicationFields(address);
        
        // reload the address from db an return if e.g. for the modification date
        Address createdAddress = AddressDAO.getInstance().getAddressByPk(TcDBContext.getDefaultContext(), address.getId());

        // store a list with this address in the context, to use the mass assignment afterwards
        all.setContent(PARAM_ADDRESS_PKS, Collections.singletonList(address.getId()));
        return createdAddress;
    }

    public String[] INPUT_saveAddress = { PARAM_ADDRESS };
	public String OUTPUT_saveAddress = "address";
	
	public Address saveAddress(OctopusContext all, Address address) throws SQLException {
		AddressDAO.getInstance().update(TcDBContext.getDefaultContext(), address);
        
        updateCommunicationFields(address);
        
        // reload the address from db an return if e.g. for the modification date
        return AddressDAO.getInstance().getAddressByPk(TcDBContext.getDefaultContext(), address.getId());
    }

    /**
     * The communication entries are stored in the table TCOMM.
     * The entries in taddressext are only triggered aggregations.
     */
    private void updateCommunicationFields(Address address) throws SQLException {
        Map commChanges = new HashMap();
        commChanges.put(new Integer(TCOMM_TYPE_TEL_DI), address.getTelefonDienstlich());
        commChanges.put(new Integer(TCOMM_TYPE_TEL_PRI), address.getTelefonPrivat());
        commChanges.put(new Integer(TCOMM_TYPE_TEL_WEITERES), address.getTelefonWeiteres());
        commChanges.put(new Integer(TCOMM_TYPE_HANDY_DI), address.getHandyDienstlich());
        commChanges.put(new Integer(TCOMM_TYPE_HANDY_PRI), address.getHandyPrivat());
        commChanges.put(new Integer(TCOMM_TYPE_HANDY_WEIERES), address.getHandyWeiteres());
        commChanges.put(new Integer(TCOMM_TYPE_FAX_DI), address.getFaxDienstlich());
        commChanges.put(new Integer(TCOMM_TYPE_FAX_PRI), address.getFaxPrivat());
        commChanges.put(new Integer(TCOMM_TYPE_FAX_WEITERES), address.getFaxWeiteres());
        commChanges.put(new Integer(TCOMM_TYPE_EMAIL1), address.getEmailAdresseDienstlich());
        commChanges.put(new Integer(TCOMM_TYPE_EMAIL2), address.getEmailAdressePrivat());
        commChanges.put(new Integer(TCOMM_TYPE_EMAIL_WEITERE), address.getEmailAdresseWeitere());
        commChanges.put(new Integer(TCOMM_TYPE_HOMEPAGE_DI), address.getHomepageDienstlich());
        commChanges.put(new Integer(TCOMM_TYPE_HOMEPAGE_PRI), address.getHomepagePrivat());
        commChanges.put(new Integer(TCOMM_TYPE_HOMEPAGE_WEITERE), address.getHomepageWeitere());
        TcCommDB.saveCommData(new Integer(address.getId()), commChanges);
    }

    
    static final public String[] INPUT_ISADDRESSINTERNAL = { PARAM_ADDRESS_ID };
	static final public boolean[] MANDATORY_ISADDRESSINTERNAL = { true };
	static final public String OUTPUT_ISADDRESSINTERNAL = "isuser";
	
	static public Boolean isAddressInternal(OctopusContext all, Integer addressId) {
		try {
			return ((Integer)SQL.Select(TcDBContext.getDefaultContext())
				.from(TaddressDB.getTableName())
				.add(TaddressDB.FKUSER, Integer.class)
				.where(Expr.equal(TaddressDB.PK_PKADDRESS, addressId))
				.getList(TcDBContext.getDefaultContext())
				.get(0)).intValue() != 0 ?
						Boolean.TRUE :
						Boolean.FALSE;
		} catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			return Boolean.FALSE;
		} catch (IndexOutOfBoundsException e) {
			return Boolean.FALSE;
		}
	}
	
	public static String[] INPUT_detectNotAssignableAddresses = new String[]{ PARAM_ADDRESS_PKS, PARAM_ADD_SUB_CATEGORIES, "removeSubCategories", "force"};
	public static boolean[] MANDATORY_detectNotAssignableAddresses = new boolean[]{ true, false, false, false};
    /** 
     * Detects all combinations of categories and addresses that violate the security rulez for address assingment or deassignment
     * @param addressPks List of address ids (the values must provide a meaningfull .toString() Method)
     * @param addSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     * @param removeSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     * @param force Flag that decides if procedure will abort when a not assignable address is found
     */
    public void detectNotAssignableAddresses(OctopusContext cntx, List addressPks, List addSubCategories, List removeSubCategories, Boolean force) throws SQLException {
       
	    List addressesWithNoAddRight = new LinkedList();
	    List addressesWithNoRemoveRight = new LinkedList();
	    
	    Where addWhere = null;	  
	    
		Select protoTypeSelect = SQL
			.Select(TcDBContext.getDefaultContext())
			.from(TaddresssubcategoryDB.getTableName(), "addresslist")
			.join(TcDBContext.getSchemaName() + "v_user_folderright", "addresslist.fk_folder", "v_user_folderright.fk_folder")
			.selectAs("addresslist.fk_address", "address_pk")
			.where(	Expr.in("addresslist.fk_address", addressPks));

		if (addSubCategories != null && addSubCategories.size() != 0) {

			SubSelect existsSubselect = new SubSelect(TcDBContext.getDefaultContext(), SQL.Select(TcDBContext.getDefaultContext())
					.from(TaddresscategoryDB.getTableName(), "addresslist2")
					.join(TcDBContext.getSchemaName() + "v_user_folderright", "addresslist2.fk_folder", "v_user_folderright.fk_folder")
					.add("addresslist2.fk_folder", Integer.class)
					.where(Expr.equal("v_user_folderright.fk_user", cntx.personalConfig().getUserID()))
					.whereAnd(Expr.equal("v_user_folderright.auth_edit", 1))
					.whereAnd(Expr.equalColumns("addresslist.fk_address",	"addresslist2.fk_address")));

			addWhere = 
				Where.or(
					Where.and(Expr.equal("v_user_folderright.fk_user", cntx.personalConfig().getUserID()),
							Where.and(Expr.in( TaddresssubcategoryDB.FKSUBCATEGORY, addSubCategories), Expr.notEqual("v_user_folderright.auth_add", 1))
					), 
				Expr.notExists(existsSubselect));
		}
		
		if (addWhere != null){
			Select completeAddSelect =((Select)protoTypeSelect.clone()).whereAnd(addWhere);
			//System.out.println("\nSQL:\n" + completeAddSelect.statementToString() + "\n");
			
			ResultSet result = completeAddSelect.getResultSet();
			while (result.next()){
				addressesWithNoAddRight.add(result.getInt("address_pk"));
			}
			result.close();
		}

		if (removeSubCategories != null && removeSubCategories.size() != 0) {
			Where removeWhere = Where.and(
					Expr.equal("v_user_folderright.fk_user", 	cntx.personalConfig().getUserID()), 
					 Where.and(Expr.in(TaddresssubcategoryDB.FKSUBCATEGORY, removeSubCategories), 
							 Expr.notEqual("v_user_folderright.auth_remove", 1)));

			Select completeRemoveSelect = ((Select)protoTypeSelect.clone()).whereAnd(removeWhere);
			//System.out.println("\nSQL:\n" + completeRemoveSelect.statementToString() +"\n");
			ResultSet result = completeRemoveSelect.getResultSet();
			while (result.next()){
				addressesWithNoRemoveRight.add(result.getInt("address_pk"));
			}
			result.close();
		}
		
		// After checking which addresses can't be moved with normal add or delete rights on categories
		// we have to check these blocked addresses if they can be structured so that they must not be blocked
		
		List blockedAddresses = new LinkedList();
		List addressesWithStructureRight = new LinkedList();
		
		if (addressesWithNoAddRight.size() != 0){
			blockedAddresses.addAll(addressesWithNoAddRight);
			if ( addressesWithNoRemoveRight.size() != 0){
				// union both lists of blocked addressPKs
				blockedAddresses.removeAll(addressesWithNoRemoveRight);
				blockedAddresses.addAll(addressesWithNoRemoveRight);
			}
		}
		else if (addressesWithNoRemoveRight.size() != 0)
			blockedAddresses.addAll(addressesWithNoRemoveRight);
		
		if (blockedAddresses.size() > 0){
			// check blocked addresses for structure right
			addressesWithStructureRight = checkForStructureRight(blockedAddresses, addSubCategories, removeSubCategories, cntx.personalConfig().getUserID());
			// remove all addresses with structure right from blocking lists
			addressesWithNoAddRight.removeAll(addressesWithStructureRight);
			addressesWithNoRemoveRight.removeAll(addressesWithStructureRight);
		}
		if ((force != null && force.booleanValue() == false) && (addressesWithNoAddRight.size() != 0 || addressesWithNoRemoveRight.size() != 0)){
				cntx.setStatus(Constants.STATUS_UNAUTHORIZED_ADDRESSASSIGN);
				logger.info("Unauthorized addressAssignment detected. Setting status to \""+ Constants.STATUS_UNAUTHORIZED_ADDRESSASSIGN +"\".");
		}
		
    	cntx.setContent("notAssingableAddresses", addressesWithNoAddRight);
    	cntx.setContent("notDeassignableAddresses", addressesWithNoRemoveRight);
	}

    /**
     * Method that checks a list of addresses on the structure right for two lists of subcategories 
     * @param addressPks, List of addresses that must be checked
     * @param addSubCategories List of subcategories to which the addresses should be assigned 
     * @param removeSubCategories List of subcategories from which the addresses should be deassigned 
     * @return List of addresses that have a structure right
     * @throws SQLException 
     */
    private List checkForStructureRight(List addressPks, List addSubCategories, List removeSubCategories, Integer userId) throws SQLException {
		
		List addressesWithStructureRight = new LinkedList();
		
		// This subselect counts how often all related categories are involved in an add-operation via the given subcategories 
		SubSelect addSubSelect = new SubSelect(TcDBContext.getDefaultContext(), SQL.Select(TcDBContext.getDefaultContext())
				.select(TsubcategoryDB.FKCATEGORY)
				.selectAs("COUNT(*)", "sum")
				.from(TsubcategoryDB.getTableName())
				.where(Expr.in(TsubcategoryDB.PK_PKSUBCATEGORY, addSubCategories))
				.groupBy(new GroupBy(TsubcategoryDB.FKCATEGORY)));

		// This subselect counts how often all related categories are involved in an remove-operation via the given subcategories 
		SubSelect removeSubSelect = new SubSelect(TcDBContext.getDefaultContext(), SQL.Select(TcDBContext.getDefaultContext())
				.select(TsubcategoryDB.FKCATEGORY)
				.selectAs("COUNT(*)", "sum")
				.from(TsubcategoryDB.getTableName())
				.where(Expr.in(TsubcategoryDB.PK_PKSUBCATEGORY, removeSubCategories))
				.groupBy(new GroupBy(TsubcategoryDB.FKCATEGORY)));
		
		GroupBy twoColumnsGroupBy = new GroupBy();
		twoColumnsGroupBy.add(TaddresssubcategoryDB.FKCATEGORY);
		twoColumnsGroupBy.add(TaddresssubcategoryDB.FKADDRESS);
		
		// This subselect counts how many assignments of the given addresses are currently registered for the related categories 
		SubSelect currentSubSelect = new SubSelect(TcDBContext.getDefaultContext(), SQL.Select(TcDBContext.getDefaultContext())
				.select(TaddresssubcategoryDB.FKCATEGORY)
				.select(TaddresssubcategoryDB.FKADDRESS)
				.selectAs("COUNT(*)", "sum")
				.from(TaddresssubcategoryDB.getTableName())
				.where(Expr.in(TaddresssubcategoryDB.FKADDRESS, addressPks))
				.groupBy(twoColumnsGroupBy));
				
		SubSelect allSubSelect = new SubSelect (TcDBContext.getDefaultContext(), SQL.Select(TcDBContext.getDefaultContext())
			// addressPK
			.selectAs("currentsubselect.fk_address", "addressPK")										
			// categoriePK
			.selectAs("COALESCE (addsubselect.fk_folder, removesubselect.fk_folder )" , "kategorie" )	
			// add-operations on this category for this address
			.selectAs("COALESCE (addsubselect.sum, 0 )" , "add")										
			// remove-operations on this category for this address
			.selectAs("COALESCE (removesubselect.sum, 0 )" , "remove")									
			// number of current relations between category and address
    		.selectAs("currentsubselect.sum" , "currents")												
    		// number of relations between category and address after adding and removing (currents + add - remove)
    		.selectAs("COALESCE(currentsubselect.sum, 0) + COALESCE(addsubselect.sum, 0) - COALESCE(removesubselect.sum, 0)", "differenz")  
    		.from(addSubSelect.clauseToString(TcDBContext.getDefaultContext()), "addsubselect")
    		.joinOuter(removeSubSelect.clauseToString(TcDBContext.getDefaultContext()) + " AS removesubselect ",
    				"addsubselect.fk_folder" , "removesubselect.fk_folder")
			.joinLeftOuter(currentSubSelect.clauseToString(TcDBContext.getDefaultContext()) + " AS currentsubselect ",
					"COALESCE(addsubselect.fk_folder, removesubselect.fk_folder)", "currentsubselect.fk_folder"));
			
		Select structureSelect = SQL.Select(TcDBContext.getDefaultContext())
		.from(allSubSelect.clauseToString(TcDBContext.getDefaultContext()), "structure")
		.select("structure.*")
		.select("v_user_folderright.fk_user")
		.select("v_user_folderright.auth_structure")
		.join(TcDBContext.getSchemaName() + "v_user_folderright", "structure.kategorie", "v_user_folderright.fk_folder")
		.where(Expr.greater("differenz", 0)) // addresses that won't get deleted (currents + add - remove > 0) 
		.whereAnd(Expr.greater("currents", 0))	// and addresses that are not added (currents > 0)
		.whereAnd(Expr.equal("v_user_folderright.fk_user", userId))
		.whereAnd(Expr.equal("v_user_folderright.auth_structure", new Integer(1)));
		
		ResultSet rs = structureSelect.getResultSet(TcDBContext.getDefaultContext());
		while (rs.next()){
			addressesWithStructureRight.add(rs.getInt("addressPK"));
			}
		rs.close();
		
		return addressesWithStructureRight;
		
	}

    public static String[] INPUT_checkForAccidentlyDeletedAddresses = new String[]{ PARAM_ADDRESS_PKS, "removeSubCategories", PARAM_ADD_SUB_CATEGORIES, "notDeassingableAddresses", "forceWithDeletion"};
    public static boolean[] MANDATORY_checkForAccidentlyDeletedAddresses = new boolean[]{ true, false, false, false, false};
    
    
    /** 
     * Checks if any addresses are deleted and moved to the trash-folder during a assignAddresses operation
     * @param addressPks List of address ids (the values must provide a meaningfull .toString() Method)
     * @param removeSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     * @param addSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null 
     * @param notDeassingableAddresses List of blocked addresses that should be ignored
     * @param forceWithDeletion If any addresses are implictily deleted during this task this flag can force the progress to delete them or to ignore them. If forceWithDeletion isn't set the process will abort if any deleted addresses are detected.
     */
    
    public void checkForAccidentlyDeletedAddresses(OctopusContext cntx, List addressPks, List removeSubCategories, List addSubCategories, List notDeassingableAddresses, Boolean forceWithDeletion) throws SQLException {
    	
    	// Abort this check and proceed normally if forceWithDeletion is set, no subcategories to remove from are set or at least one subcategory to add is set
    	if (forceWithDeletion != null && forceWithDeletion.booleanValue()){
    		logger.info("Addresses threatened by deletion found. Proceeding with deletion." );
		return;
    	}
    	if ((addSubCategories != null && addSubCategories.size() > 0) || removeSubCategories == null || removeSubCategories.size() == 0)
    		return;
    
   	// Get all relations of addresses and subcategories that are connected with a subfolder 
    	// that is not referred in the remove list. The Distinct results in only one result-line per addresspk
    	// If all addresses have at least one relation to a subcategory that is not in the remove list 
    	// (and so no address is completely deleted) the result should have exactly the same number of lines 
    	// as the addressPks list.
    	
    	Select checkSelect = SQL.SelectDistinct(TcDBContext.getDefaultContext())
    		.from(TaddresssubcategoryDB.getTableName())
    		.join(TcDBContext.getSchemaName() + "v_user_folder", TaddresssubcategoryDB.FKCATEGORY, "v_user_folder.fk_folder")
    		.select(TaddresssubcategoryDB.FKADDRESS)
    		.where(Expr.optimizedIn(TcDBContext.getDefaultContext(), TaddresssubcategoryDB.FKADDRESS, addressPks))
    		//.whereAnd(Expr.equal("v_user_folder.userid", cntx.personalConfig().getUserID()))
    		.whereAnd(Expr.notIn(TaddresssubcategoryDB.FKSUBCATEGORY, removeSubCategories));
    	
    	ResultSet res = checkSelect.getResultSet(TcDBContext.getDefaultContext());
    	res.last();
    	int numberOfDeletedAddresses = addressPks.size() - res.getRow();
    	
    	if (numberOfDeletedAddresses != 0){
    		
    		List allRemainingAddresses = new ArrayList();
    		
    		res.beforeFirst();
    		
    		while (res.next()){
    			allRemainingAddresses.add(new Integer(res.getInt(1)));
    		}
     	
    		// overwrite addressPks List if deletion is not allowed
    		cntx.setContent(PARAM_ADDRESS_PKS, allRemainingAddresses);
    		if (forceWithDeletion != null && forceWithDeletion.equals(Boolean.FALSE)){
    			logger.info("Addresses theatened by deletion found. Ignoring threatened addresses and proceed assignment for the remaining " + allRemainingAddresses.size() + " addresses." );
    		}
    		// Set Error status to abort if no force flag is set
    		if (forceWithDeletion == null){
    			cntx.setStatus(Constants.STATUS_IMPLICIT_DELETION_DETECTED);
	    		List deletedAddressPks = new ArrayList();
	    		
	    		
	    		deletedAddressPks.addAll(addressPks);
	    		deletedAddressPks.removeAll(allRemainingAddresses);
	    		
	    		cntx.setContent("implicitlyDeletedAddresses", deletedAddressPks);
    			logger.info("Addresses theatened by deletion found. Aborting assignment to ask user what to do." );
    		} 		
    	}
    	res.close();
    }
	    
    public static String[] INPUT_deassignAddresses = new String[]{ PARAM_ADDRESS_PKS, "removeSubCategories", "notDeassignableAddresses"};
    public static boolean[] MANDATORY_deassignAddresses = new boolean[]{ true, false, false };
    /** 
     * Remove the  a set of addresses to subcategories in one action
     * @param addressPks List of address ids (the values must provide a meaningfull .toString() Method)
     * @param removeSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     */
    public void deassignAddresses(OctopusContext cntx, List addressPks, List removeSubCategories, List notDeassingableAddresses) throws SQLException {
    		
        if (notDeassingableAddresses != null && notDeassingableAddresses.size() != 0)
        		addressPks .removeAll(notDeassingableAddresses);
        

		if (removeSubCategories == null || removeSubCategories.size() == 0 || addressPks.size() == 0)
            return;
		
        //OptimizedInList oAddress = new OptimizedInList("fk_address", OptimizedInList.getIntegerArray(addressPks));
        //OptimizedInList oCat = new OptimizedInList("fk_subfolder", OptimizedInList.getIntegerArray(removeSubCategories));
        String cmd = "DELETE FROM " + TcDBContext.getSchemaName() + "taddresssubfolder WHERE "
            +"("+ Expr.optimizedIn(TcDBContext.getDefaultContext(), "fk_address", addressPks).clauseToString(TcDBContext.getDefaultContext())+")"
            + " AND "
            +"("+ Expr.optimizedIn(TcDBContext.getDefaultContext(), "fk_subfolder", removeSubCategories).clauseToString(TcDBContext.getDefaultContext())+")";
            //+ oAddress.getOptimezedClause() 
            //+ oCat.getOptimezedClause();
        
        DB.update(TcDBContext.getDefaultContext(), cmd);
    }
    

    
    public static String[] INPUT_assignAddresses = new String[]{ PARAM_ADDRESS_PKS, PARAM_ADD_SUB_CATEGORIES, "notAssingableAddresses"};
    public static boolean[] MANDATORY_assignAddresses = new boolean[]{ true, false, false};
    /** 
     * Assign a set of addresses to subcategories in one action
     * @param addressPks List of address ids (the values must provide a meaningfull .toString() Method)
     * @param addSubCategories List of subcategory ids (the values must provide a meaningfull .toString() Method). Nothing will be done if the param is null
     */
    public void assignAddresses(OctopusContext cntx, List addressPks, List addSubCategories, List notAssingableAddresses) throws SQLException {
        
    	 if (addSubCategories == null || addSubCategories.size() == 0 || addressPks.size() == 0)
             return;
    	 
        if (notAssingableAddresses != null && notAssingableAddresses.size() != 0)
    			addressPks.removeAll(notAssingableAddresses);
        
        Connection con = DB.getConnection(TcDBContext.getDefaultContext());
        con.setAutoCommit(false);
        
        PreparedStatement insertStmt = null;
        PreparedStatement testStmt = null;
        PreparedStatement catStmt = null;
        ResultSet rs = null;

        try {
        	catStmt = con.prepareStatement("SELECT fk_folder as fk_folder FROM " + TcDBContext.getSchemaName() + "tsubfolder WHERE pk_subfolder = ?");
        	
            String cmd = "INSERT INTO " + TcDBContext.getSchemaName() + "taddresssubfolder (createdby, fk_address, fk_subfolder, fk_folder)"
                +" VALUES ( "
                + "'" + cntx.personalConfig().getUserLogin() +"',"
                + " ?, ?, ?)";

            insertStmt = con.prepareStatement(cmd);
            testStmt = con.prepareStatement("SELECT 'TRUE' AS already_there FROM " + TcDBContext.getSchemaName() + "taddresssubfolder"
                                            +" WHERE fk_address = ? AND fk_subfolder = ?");
            
            for (Iterator addressIter = addressPks.iterator(); addressIter.hasNext();) {
                Integer addressPK = (Integer)addressIter.next();
                
                for (Iterator katIter = addSubCategories.iterator(); katIter.hasNext();) {
                    Integer subKatPK = (Integer) katIter.next();

                    // Test, if this entry already exists
                    testStmt.setInt(1, addressPK);
                    testStmt.setInt(2, subKatPK);
                    rs = testStmt.executeQuery();
                    if (! rs.next()) {
                    	catStmt.setInt(1, subKatPK);
                    	ResultSet rs2 = catStmt.executeQuery();
                    	rs2.next();
                    	Integer katpk = rs2.getInt("fk_folder");
                    	rs2.close();
                    	
                        insertStmt.setInt(1, addressPK);
                        insertStmt.setInt(2, subKatPK);
                        insertStmt.setInt(3, katpk);
                        insertStmt.execute();
                    }
                    rs.close();
                    rs = null;
                }
            }
            con.commit();
        } catch (SQLException e) {
            con.rollback();
            throw e;
        } finally {
            if (insertStmt != null)
                insertStmt.close();
            if (rs != null)
                rs.close();
        }
    }
    
    
    
    
	
    public static String[] INPUT_getAssociatedSubCategoriesByAddressPk = new String[]{"addressPk"};
    public static String OUTPUT_getAssociatedSubCategoriesByAddressPk = "associatedSubCategories";
    /**
     * Returns a list with the Primary Keys of all assoziated SubCategorys for the address.
     */
    public List getAssociatedSubCategoriesByAddressPk(OctopusContext cntx, Integer addressPk) 
        throws SQLException {

        return SQL.Select(TcDBContext.getDefaultContext()).from(TaddresssubcategoryDB.getTableName())
            .add(TaddresssubcategoryDB.FKSUBCATEGORY, Integer.class)
			.where(Expr.equal(TaddresssubcategoryDB.FKADDRESS, addressPk))
            .getList(TcDBContext.getDefaultContext());        
    }
    
    public static String[] INPUT_getAssociatedCategoriesByAddressPks = new String[]{PARAM_ADDRESS_PKS};
    static final public boolean[] MANDATORY_getAssociatedCategoriesByAddressPks = { true };
	public static String OUTPUT_getAssociatedCategoriesByAddressPks = "associatedCategories";
    
    
    public CategoryList getAssociatedCategoriesByAddressPks(OctopusContext cntx, List addressPks) 
        throws SQLException {

    		if (addressPks == null || addressPks.isEmpty())
			return new CategoryList(new ArrayList());
		else{
			 ListFilterImpl filter = new ListFilterImpl();
			 filter.init(cntx, PARAM_CATEGORIES_FILTER);
			 filter.setUseLimit(false);
			 
			 List list = CategoryDAO.getInstance().getCategoriesForAddresses(TcDBContext.getDefaultContext(), cntx.personalConfig().getUserID(), addressPks, filter);
		     
//			 System.out.println(list.size() + " Kategorien gefunden");
//			 
//			 for (Iterator iter = list.iterator(); iter.hasNext(); ){
//				 Category cat =(Category) iter.next();
//				 System.out.println("Kategorie: " + cat.getId());
//				 List subs = cat.getSubCategories();
//				 for (Iterator iter2 = subs.iterator(); iter2.hasNext(); ){
//					 SubCategory sub = (SubCategory) iter2.next();
//					 System.out.println(" U.kat: " + sub.getId());
//				 }
//				 System.out.println("");
//			 }
				 
		     //cntx.setContent(PARAM_COUNT, new Integer(filter.getCount()));        
		     return new CategoryList(list);
		}
    }

	static final public String[] INPUT_GETADDRESSPREVIEWBYID = { "addressid" };
	static final public boolean[] MANDATORY_GETADDRESSPREVIEWBYID = { true };
	static final public String OUTPUT_GETADDRESSPREVIEWBYID = "addresses";

	static public List getAddressPreviewById(OctopusContext all, List addressId) throws SQLException {
		try {
			if (addressId != null && addressId.isEmpty())
				return Collections.EMPTY_LIST;
			else if (addressId == null)
				return SQL.Select(TcDBContext.getDefaultContext())
						.from(TaddressextDB.getTableName())
						.add(TaddressextDB.LABELFN, String.class)
						.getList(TcDBContext.getDefaultContext());
			else
				return SQL.Select(TcDBContext.getDefaultContext())
						.from(TaddressextDB.getTableName())
						.add(TaddressextDB.LABELFN, String.class)
						.where(Expr.in(TaddressextDB.PK_PKADDRESSEXT, addressId))
						.getList(TcDBContext.getDefaultContext());
		} catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			throw e;
		}
	}
	
	
	static final public String[] INPUT_GETUSERFORADDRESSID = { "addressid" };
	static final public boolean[] MANDATORY_GETUSERFORADDRESSID = { true };
	static final public String OUTPUT_GETUSERFORADDRESSID = "userid";
	
	static public Integer getUserForAddressId(OctopusContext all, Integer addressId) throws SQLException {
		try {
			return (Integer)SQL.Select(TcDBContext.getDefaultContext())
				.from(TaddressDB.getTableName())
				.add(TaddressDB.FKUSER, Integer.class)
				.where(Expr.equal(TaddressDB.PK_PKADDRESS, addressId))
				.getList(TcDBContext.getDefaultContext())
				.get(0);
		} catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			throw e;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	static final public String[] INPUT_getAddressIdForUser = { "userid" };
	static final public boolean[] MANDATORY_getAddressIdForUser = { true };
	static final public String OUTPUT_getAddressIdForUser = "addressid";
	
    static public Integer getAddressIdForUser(OctopusContext all, Integer userId) throws SQLException {
        try {
			return (Integer)SQL.Select(TcDBContext.getDefaultContext())
				.from(TaddressDB.getTableName())
				.add(TaddressDB.PK_PKADDRESS, Integer.class)
				.where(Expr.equal(TaddressDB.FKUSER, userId))
				.getList(TcDBContext.getDefaultContext())
				.get(0);
		} catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			throw e;
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	
	
	
	static final public String[] INPUT_GETADDRESSESBYFILTER = { "addressid", "userid", "label", "lastname", "firstname", "email" };
	static final public boolean[] MANDATORY_GETADDRESSESBYFILTER = { false, false, false, false, false, false };
	static final public String OUTPUT_GETADDRESSESBYFILTER = "addresses";

	static public List getAddressesByFilter(OctopusContext all, Object addressId, Object userId, Object label, Object lastname, Object firstname, Object email) throws SQLException {
	    try {
	    	Select select = SQL.Select(TcDBContext.getDefaultContext())
				.from(TaddressextDB.getTableName())
				.add(TaddressextDB.PK_PKADDRESSEXT, Integer.class);
	    	
			List whereList = new ArrayList();
			
			addWhere(whereList, TaddressextDB.PK_PKADDRESSEXT, addressId);
			addWhere(whereList, new String[] { TaddressextDB.LABELFN, TaddressextDB.LABELFNM }, label);
			addWhere(whereList, TaddressextDB.LASTNAMEM, lastname);
			addWhere(whereList, TaddressextDB.FIRSTNAMEM, firstname);
			addWhere(whereList, new String[] { TaddressextDB.EMAIL, TaddressextDB.EMAILHOME, TaddressextDB.EMAIL3 }, email);
			addWhere(whereList, TaddressDB.FKUSER, userId);
			
			if (userId != null)
				select.join(TaddressDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS);
			
			if (whereList.size() > 0) {
				Iterator it = whereList.iterator();
				WhereList where = null;
				if (it.hasNext()) {
					where = Where.list().add((Clause)it.next());
					while (it.hasNext()) {
						where.addOr((Clause)it.next());
					}
				}
				return select
					.where(where)
					.getList(TcDBContext.getDefaultContext());
			} else {
				return null;
			}
	    } catch (SQLException e) {
			Lg.SQL(all.getModuleName()).error(e.getMessage(), e);
			throw e;
	    }
	}
	
	static final public String[] INPUT_GETTOTALADDRESSCOUNT = { };
	static final public boolean[] MANDATORY_GETTOTALADDRESSCOUNT = { };
	static final public String OUTPUT_GETTOTALADDRESSCOUNT = "addressCount";
	
	/** Retrieves the user total count of addresses. */
	static public Integer getTotalAddressCount(OctopusContext all) throws SQLException
	{
		return AddressDAO.getInstance().getTotalCount(TcDBContext.getDefaultContext());
	}

    static private void addWhere(List list, Object column, Object value) {
    	if (value != null) {
    		Clause clause = getWhere(column, value);
    		if (clause != null)
    			list.add(clause);
    	}
    }
    
    static private Clause getWhere(Object column, Object value) {
    	if (column instanceof String) {
    		if (value instanceof Collection)  {
    			return getWhere((String)column, (Collection)value);
    		} else if (value instanceof Object[]) {
    			return getWhere((String)column, Arrays.asList((Object[])value));
    		} else {
    			return getExpr((String)column, value);
    		}
    	} else if (column instanceof String[]) {
    		Iterator it = Arrays.asList((String[])column).iterator();
    		if (it.hasNext()) {
    			WhereList where = Where.list()
					.add(getWhere(it.next(), value));
    			while (it.hasNext()) {
    				where.addOr(getWhere((String)it.next(), value));
    			}
    			return where;
    		} else {
    			return null;
    		}
    	} else {
    		return null;
    	}
    }

	static private Clause getWhere(String column, Collection coll) {
		boolean like = false;
		
		Iterator it = coll.iterator();
		while (it.hasNext()) {
			Object entry = it.next();
			if (entry instanceof String && ((String) entry).indexOf('*') != -1) {
				like = true;
				break;
			}
		}
		
		if (like) {
			it = coll.iterator();
			WhereList where = null;
			if (it.hasNext()) {
				where = Where.list().add(getExpr(column, it.next()));
				while (it.hasNext()) {
					where.addOr(getExpr(column, it.next()));
				}
			}
			return where;
		} else {
			return Expr.in(column, coll);
		}
	}

	static private Clause getExpr(String column, Object value) {
		if (value instanceof String && (((String)value).indexOf('*')) != -1)
			return Expr.like(column, ((String)value).replaceAll("\\*", "%"));
		else
			return Expr.equal(column, value);
	}
}