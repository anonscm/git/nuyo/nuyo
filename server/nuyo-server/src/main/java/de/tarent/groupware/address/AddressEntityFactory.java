package de.tarent.groupware.address;

import java.util.Iterator;

import de.tarent.commons.datahandling.entity.DefaultEntityFactory;
import de.tarent.commons.datahandling.entity.EntityFactory;
import de.tarent.commons.datahandling.entity.ParamSet;
import de.tarent.commons.utils.Pojo;
import de.tarent.groupware.impl.AddressImpl;

/**
 *
 * @author Sebastian Mancke, tarent GmbH
 */
public class AddressEntityFactory extends DefaultEntityFactory {
	
	private static AddressEntityFactory instance = null;
	
	protected AddressEntityFactory(){
		super(AddressImpl.class);
	}
	
	public static synchronized AddressEntityFactory getInstance() {
		if(instance == null){			
			instance = new AddressEntityFactory();
		}
		return instance;
	}

    protected EntityFactory getFactoryFor(String attributeName) {
        return null;
    }


    /**
     * Returns an AttributeSource wrapper over the supplied entity
     */
    public void writeTo(ParamSet target, Object entity) {
        // HACK: here we overide the default implementation to set an int 
        //       for the boolean fields of the address.
        //       But this should be declared as type-conversion in the 
        //       AddressDBMapping and done by the setAttribute of the prepared statement 
        for (Iterator iter = target.getAttributeNames().iterator(); iter.hasNext();) {
            String attributeName = (String)iter.next();
            Object attributeValue = Pojo.get(entity, attributeName);
            if (attributeValue instanceof Boolean) {
                attributeValue = ((Boolean)attributeValue).booleanValue() ? new Integer(1) : new Integer(0);
            }
            target.setAttribute(attributeName, attributeValue);
        }
    }
}