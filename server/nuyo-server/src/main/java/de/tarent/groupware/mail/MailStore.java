/* $Id: MailStore.java,v 1.8 2007/06/16 14:55:48 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import gnu.mail.providers.imap.IMAPStore;
import gnu.mail.providers.pop3.POP3Store;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.event.ConnectionEvent;
import javax.mail.event.ConnectionListener;

import de.tarent.contact.bean.TmailmessageDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.octopus.content.TcAll;

/**
 * Klasse resp�sentiert einen Mail-Store,
 * dies entspricht aus Benutzersicht einem Posteingang.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.8 $
 */
public class MailStore implements Runnable {
	/**
	 * Dieser Logger soll f�r Ausgaben im Octopus-Kontext benutzt werden.
	 */
	public final static Logger logger = Logger.getLogger(MailWorker.class.getName());
	
	/**
	 * Server-Daten
	 */
	private Map mSettings = null;
	private Integer mID;
	private String mProtocol;
	private String mServer;
	private int mPort = -1;
	private String mUsername;
	private String mPassword;
	private String mDisplayName;

	/**
	 * Connections
	 */
	private Session mSession;
	private Store mStore;

	/**
	 * Thread Parameter
	 * 
	 * threadTime
	 *   Speichert die Startzeit des letzten Thread-Durchlaufes.
	 * threadWaitTime
	 *   Speichert die Zeit wielange zwischen zwei
	 *   Thread-Durchlaufen vergehen sollm in Mili-Sekunden.
	 */
	private long threadTime;
	private long threadWaitTime = 3 * 60 * 1000; // 3 Minuten
	private boolean threadIsRunning = false;

	/**
	 * Cachen von Folder-Infos und eMails
	 * 
	 * mFolderCache
	 *   Speichert Infos zu einem Folder.
	 *   key: (Integer)ID
	 *   value: (Map)mit den Keys FOLDER_*
	 * 
	 * mFolderCacheRecursiv
	 *   Speichert eine rekursive Map, dient zur Ausgabe beim Client.
	 * 
	 * mFolderCacheComplete
	 *   Speichert eine Liste mit allen Foldern die der Store hat.
	 * 
	 * mFolderCacheFilter
	 *   Der Filter mit dem die Folder vorgeladen werden.
	 */
	private Map mFolderCache = new HashMap();
	private Map mFolderCacheRecursiv;
	private List mFolderCacheComplete;
	private List mFolderCacheFilter = MailHelper.filter_cache;

	/**
	 * Parameter
	 */
	private String folderDelimiterRegEx;
	private boolean showDeleted = false;
	private Integer status = MailConstants.STORESTATUS_NONE;

	/**
	 * @see #setServer(String, String, Integer)
	 * @see #setUser(String, String)
	 */
	public MailStore() {
	}

	/**
	 * @param properties mit 'protocol', 'server', 'port', 'username' und 'password'.
	 * @throws MailException
	 * 
	 * @see #setServer(String, String, Integer)
	 * @see #setUser(String, String)
	 */
	public MailStore(Properties properties) throws MailException {
		setServer(properties.getProperty(MailConstants.SERVER_PROTOCOL), properties.getProperty(MailConstants.SERVER_NAME), new Integer(properties.getProperty(MailConstants.SERVER_PROTOCOL)));
		setUser(properties.getProperty(MailConstants.SERVER_USERNAME), properties.getProperty(MailConstants.SERVER_PASSWORD));
		mID = (Integer)properties.get(MailConstants.SERVER_ID);
		mDisplayName = properties.getProperty(MailConstants.SERVER_DISPLAYNAME);
	}

	/**
	 * @param map Map mit 'protocol', 'server', 'port', 'username' und 'password'.
	 * @throws MailException
	 * 
	 * @see #setServer(String, String, Integer)
	 * @see #setUser(String, String)
	 */
	public MailStore(Map map) throws MailException {
		setServer((String)map.get(MailConstants.SERVER_PROTOCOL), (String)map.get(MailConstants.SERVER_NAME), (Integer)map.get(MailConstants.SERVER_PORT));
		setUser((String)map.get(MailConstants.SERVER_USERNAME), (String)map.get(MailConstants.SERVER_PASSWORD));
		mID = (Integer)map.get(MailConstants.SERVER_ID);
		mDisplayName = (String)map.get(MailConstants.SERVER_DISPLAYNAME);
		mSettings = map;
	}

	/**
	 * @param protocol
	 * @param server
	 * @param port
	 * @param username
	 * @param password
	 * 
	 * @see #setServer(String, String, Integer)
	 * @see #setUser(String, String)
	 */
	public MailStore(String protocol, String server, Integer port, String username, String password) throws MailException {
		setServer(protocol, server, port);
		setUser(username, password);
	}

	/**
	 * @see Object#toString()
	 */
	public String toString() {
		if (mDisplayName != null) {
			return mDisplayName;
		} else if (mStore != null) {
			return mStore.getURLName().toString();
		} else {
			return super.toString();
		}
	}
	
	/**
	 * Speichert 'protocol', 'server' und 'port'.
	 * 
	 * @param protocol (PROTOCOL_*)
	 * @param server
	 * @param port (null = default oder 0-65535)
	 * 
	 * @see MailConstants#PROTOCOL_IMAP
	 * @see MailConstants#PROTOCOL_POP3
	 * @throws MailException (ERROR_INVALID_*)
	 */
	public void setServer(String protocol, String server, Integer port) throws MailException {
		if (MailConstants.PROTOCOL_IMAP.equalsIgnoreCase(protocol)) {
			mProtocol = MailConstants.PROTOCOL_IMAP;
		} else if (MailConstants.PROTOCOL_POP3.equalsIgnoreCase(protocol)) {
			mProtocol = MailConstants.PROTOCOL_POP3;
		} else {
			logger.log(Level.WARNING, MailConstants.ERROR_INVALID_PROTOCOL + ": " + protocol);
			throw new MailException(MailConstants.ERROR_INVALID_PROTOCOL);
		}

		if (server != null && server.length() != 0) {
			mServer = server;
		} else {
			logger.log(Level.WARNING, MailConstants.ERROR_INVALID_SERVER + ": " + server);
			throw new MailException(MailConstants.ERROR_INVALID_SERVER);
		}

		if (port != null) {
			if (port.intValue() < 0 || port.intValue() > 65535) {
				logger.log(Level.WARNING, MailConstants.ERROR_INVALID_PORT + ": " + port);
				throw new MailException(MailConstants.ERROR_INVALID_PORT);
			}
			mPort = port.intValue();
		} else {
			mPort = -1; // default port
		}
	}

	/**
	 * Speichert 'username' und 'password'.
	 * 
	 * @param username
	 * @param password
	 */
	public void setUser(String username, String password) {
		mUsername = username;
		mPassword = password;
	}
	
	/**
	 * Filter Methoden addFilter, removeFilter, setFilter
	 * bearbeiten den Filter unter dem die Mitteilungen
	 * vorgeladen werden.
	 */
	public void addFilter(String filter) {
		if (!mFolderCacheFilter.contains(filter))
			mFolderCacheFilter.add(filter);
	}

	public void removeFilter(String filter) {
		while (mFolderCacheFilter.remove(filter)) {
		}
	}

	public void setFilter(List filter) {
		if (filter != null)
			mFolderCacheFilter = filter;
	}

	/**
	 * F�gt den Folder hinzu.
	 * 
	 * @see #loadFolderInfo(Folder, Map)
	 * 
	 * @param foldername
	 */
	public void addFolder(Integer folderid, String foldername) throws MailException {
		Map folder = new HashMap();
		folder.put(MailConstants.FOLDER_FULLNAME, foldername);
//		folder.put(MailConstants.FOLDER_SORT_KEY, MailConstants.MESSAGE_SENTDATE);
//		folder.put(MailConstants.FOLDER_SORT_REVERSE, Boolean.TRUE);
		mFolderCache.put(folderid, folder);
	}

	/**
	 * F�gt eine Liste von Foldern hinzu. Erwartet Map mit foldern
	 * in einer (Integer)id zu (String)foldername -Beziehung.
	 * Ruft f�r jeden �bergebenen Folder addFolder(Integer, String) auf.
	 * 
	 * @see #addFolder(Integer id, String foldername)
	 * @param folders
	 * @throws MailException
	 */
	public void addFolder(Map folders) throws MailException {
		Iterator it = folders.keySet().iterator();
		while (it.hasNext()) {
			Integer id = (Integer)it.next();
			addFolder(id, (String)folders.get(id));
		}
	}

	/**
	 * Setzt die Sortierreihenfolge f�r einen bestimmten Folder.
	 * 
	 * @param id
	 * @param key
	 * @param reserve
	 */
//	public void setFolderSort(Integer id, String key, Boolean reserve) {
//		Map folder = (Map)mFolderCache.get(id);
//		folder.put(MailConstants.FOLDER_SORT_KEY, key);
//		folder.put(MailConstants.FOLDER_SORT_REVERSE, reserve);
//		sortMessageList((List)folder.get(MailConstants.FOLDER_MAIL_LIST), key, reserve.booleanValue());
//	}

	/**
	 * Entfernt einen Folder.
	 * 
	 * @param id
	 */
	public void removeFolder(Integer id) {
		mFolderCache.remove(id);
	}

	/**
	 * Entfernt alle Eintr�ge aus dem Folder-Cache.
	 */
	public void removeAllFolder() {
		mFolderCache.clear();
	}

	/**
	 * connect to server
	 * 
	 * @throws MailException
	 */
	public void connect() throws MailException {
		status = MailConstants.STORESTATUS_INIT;
		try {
			if (mSession == null || mStore == null) {
				if (mProtocol.equals(MailConstants.PROTOCOL_IMAP)) {
					Properties sessionProperties = new Properties();
					sessionProperties.setProperty("protocol", MailConstants.PROTOCOL_IMAP);
					sessionProperties.setProperty("type", "store");
					sessionProperties.setProperty("class", "com.sun.mail.imap.IMAPStore");
					sessionProperties.setProperty("vendor", "Sun Microsystems, Inc.");
					mSession = Session.getInstance(sessionProperties);
					URLName urlname = new URLName(MailConstants.PROTOCOL_IMAP, mServer, mPort, "", mUsername, mPassword);
					mStore = new IMAPStore(mSession, urlname);
				} else if (mProtocol.equals(MailConstants.PROTOCOL_POP3)) {
					Properties sessionProperties = new Properties();
					sessionProperties.setProperty("protocol", MailConstants.PROTOCOL_POP3);
					sessionProperties.setProperty("type", "store");
					sessionProperties.setProperty("class", "com.sun.mail.pop3.POP3Store");
					sessionProperties.setProperty("vendor", "Sun Microsystems, Inc.");
					mSession = Session.getInstance(sessionProperties);
					URLName urlname = new URLName(MailConstants.PROTOCOL_POP3, mServer, mPort, "", mUsername, mPassword);
					mStore = new POP3Store(mSession, urlname);
				} else {
					throw new MailException(MailConstants.ERROR_INVALID_PROTOCOL);
				}
			}

			if (!mStore.isConnected()) {
				mStore.connect();
			}
			status = MailConstants.STORESTATUS_CONNECT;
			Folder defaultFolder = mStore.getDefaultFolder();
			folderDelimiterRegEx = "[" + defaultFolder.getSeparator() + "]";
			mFolderCacheRecursiv = MailHelper.createRecursiveMap(mFolderCache, folderDelimiterRegEx);
			mFolderCacheComplete = loadFolderList(new ArrayList(), defaultFolder, true);
			status = MailConstants.STORESTATUS_READY;

			mStore.addConnectionListener(new ConnectionListener() {
				public void opened(ConnectionEvent event) {
					System.out.println("opened: " + event);
				}
				public void disconnected(ConnectionEvent event) {
					System.out.println("discon: " + event);
				}
				public void closed(ConnectionEvent event) {
					System.out.println("closed: " + event);
				}
			});
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}

	/**
	 * disconnect from Server
	 * 
	 * @throws MailException
	 */
	public void disconnect() throws MailException {
		try {
			status = MailConstants.STORESTATUS_NONE;
			Iterator it = mFolderCache.keySet().iterator();
			while (it.hasNext()) {
				try {
					Integer id = (Integer)it.next();
					Folder folder = (Folder) ((Map)mFolderCache.get(id)).get(MailConstants.FOLDER_OBJECT);
					closeFolder(folder);
				} catch (Throwable t) {
					logger.log(Level.SEVERE, t.getLocalizedMessage(), t);
				}
			}
			mStore.close();
			mSession = null;
			mStore = null;
			mFolderCache = null;
		} catch (MessagingException ex) {
			throw new MailException(ex);
		}
	}

	/**
	 * @return Status ob MailStore eine Verbindung zum Mail-Server hat.
	 */
	public boolean isConnected() {
		return (mStore != null && mStore.isConnected());
	}

	/**
	 * Thread-Implementierung
	 * 
	 * Arbeitet im Hintergrund und sollte �ber die start()
	 * Methode aufgerufen werden. Baut u. a. eine Connection
	 * im Hintergrund auf und ruft eMails im Hintergrund ab.
	 * 
	 * Der Thread startet keine Endlos-Schleife und muss
	 * daher immer wieder vom Worker angetriggert werden.
	 */
	public void run() {
		System.out.println("run MailStore: " + toString());
		
		if (threadIsRunning) return;
		
		threadIsRunning = true;
		try {
			if (!isConnected()) {
				connect();
			}
			long currentTime = System.currentTimeMillis();
			if (currentTime - threadTime > threadWaitTime) {
				logger.log(Level.FINER, "MailStore.Thread: start...");
				Iterator it = mFolderCache.keySet().iterator();
				while (it.hasNext()) {
					Integer id = (Integer)it.next();
					Map folderInfo = (Map)mFolderCache.get(id);
					Folder folder = (Folder)folderInfo.get(MailConstants.FOLDER_OBJECT);
					if (folder == null) {
						folder = mStore.getFolder((String)folderInfo.get(MailConstants.FOLDER_FULLNAME));
					}
					loadFolderInfo(folder, folderInfo);
//					folderInfo.put(MailConstants.FOLDER_STATUS, MailConstants.FOLDERSTATUS_CONNECT);
//					List list = loadMessageList(folder, mFolderCacheFilter);
//					sortMessageList(list, (String)folderInfo.get(MailConstants.FOLDER_SORT_KEY), ((Boolean)folderInfo.get(MailConstants.FOLDER_SORT_REVERSE)).booleanValue());
//					folderInfo.put(MailConstants.FOLDER_MAIL_LIST, list);
//					folderInfo.put(MailConstants.FOLDER_STATUS, MailConstants.FOLDERSTATUS_READY);
				}
				logger.log(Level.FINER, "MailStore.Thread: stopped.");
				threadTime = currentTime;
			}
		} catch (MailException e) {
			logger.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} catch (MessagingException e) {
			logger.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
		threadIsRunning = false;
	}

	/**
	 * L�dt die komplette Folder-Liste.
	 * 
	 */
	public List getCompleteFolderList() {
		return mFolderCacheComplete;
	}

//	/**
//	 * Gibt eMails zwischen fromIndex und toIndex zur�ck,
//	 * inklusive der angegeben Werte, 1-basierend.
//	 * 
//	 * @param folderid
//	 * @param fromIndex
//	 * @param toIndex
//	 * @return
//	 */
//	public List getMessageCache(Integer folderid, int fromIndex, int toIndex) {
//		return ((List) ((Map)mFolderCache.get(folderid)).get(MailConstants.FOLDER_MAIL_LIST)).subList(fromIndex - 1, toIndex);
//	}

	public Map getFolder(Integer folderid) {
		return (Map)mFolderCache.get(folderid);
	}
	
	public Map getFolderDetail(Integer folderid, List filter) {
		Map folderinfo = (Map)mFolderCache.get(folderid);
		if (filter == null)
			return folderinfo;
		
		HashMap result = new HashMap();
		int i = 0;
		int size = filter.size();
		for (i = 0; i < size; i++) {
			Object key = filter.get(i);
			if (folderinfo.containsKey(key)) {
				result.put(key, folderinfo.get(key));
			}
		}
		return result;
	}

	/**
	 * Gibt die Gr��e der eMail-Liste zur�ck.
	 * 
	 * @param id
	 * @return
	 */
//	public int getMessageCacheSize(Integer folderid) {
//		return ((List) ((Map)mFolderCache.get(folderid)).get(MailConstants.FOLDER_MAIL_LIST)).size();
//	}

	/**
	 * @param folderid
	 * @return mailDetail
	 */
	public Map getMessageDetail(TcAll all, Integer folderid, int hashcode, long sentdate, List filter) throws MailException {
		try {
			Folder folder = (Folder) ((Map)mFolderCache.get(folderid)).get(MailConstants.FOLDER_OBJECT);
			
			Message msg = null;
			
			// eMail-Nachricht suchen
			Message message[] = folder.getMessages();
			for (int m = 0; m < message.length; m++) {
				msg = message[m];
				if (MailHelper.compareMail(msg, hashcode, sentdate))
					break;
			}
			
			if (MailHelper.compareMail(msg, hashcode, sentdate)) {
				/*
				 * Nachricht ale "gelesen" und "nicht neu" markieren.
				 */
				try {
					List messagelist = Collections.singletonList(hashcode + "-" + sentdate);
					setMessageFlag(all, folderid, messagelist, Flags.Flag.RECENT, false);
					setMessageFlag(all, folderid, messagelist, Flags.Flag.SEEN, true);
				} catch (MailException e) {
					logger.log(Level.WARNING, e.getLocalizedMessage(), e);
				} catch (SQLException e) {
					logger.log(Level.WARNING, e.getLocalizedMessage(), e);
				}
				return MailHelper.getMailInfo(
						msg,
						filter,
						filter.contains(MailConstants.FILTER_SIMPLEADDRESSES),
						filter.contains(MailConstants.FILTER_USEINPUTSTREAMS));
			}
			return null;
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}

	/**
	 * Setzt bei den angegebenen eMails (List messageids) aus dem
	 * Ordner (String folder) das Flag (javax.mail.Flags.Flag)
	 * auf den Wert (boolean bool).
	 * 
	 * @param folderid
	 * @param messageids
	 * @param flag
	 * @param bool
	 * @throws MailException
	 * @throws SQLException
	 */
	public void setMessageFlag(TcAll all, Integer folderid, List messageids, Flags.Flag flag, boolean bool) throws MailException, SQLException {
		try {
			Map folderInfo = (Map)mFolderCache.get(folderid);
			Folder folder = (Folder)folderInfo.get(MailConstants.FOLDER_OBJECT);
			openFolder(folder);
			
			int hashcode;
			long sentdate;
			String messageid;
			Message msg;
			
			Message message[] = folder.getMessages();
			for (int i = 0; i < messageids.size(); i++) {
				messageid = (String)messageids.get(i);
				hashcode = Integer.parseInt(messageid.substring(0, messageid.lastIndexOf('-')));
				sentdate = Long.parseLong(messageid.substring(messageid.lastIndexOf('-') + 1));
				
				for (int m = 0; m < message.length; m++) {
					msg = message[m];
					if (MailHelper.compareMail(msg, hashcode, sentdate)) {
						msg.setFlag(flag, bool);
						Update update = SQL.Update(TcDBContext.getDefaultContext())
							.table(TmailmessageDB.getTableName())
							.where(Where.list()
									.add(Expr.equal(TmailmessageDB.FKMAILSTORECATEGORY, folderid))
									.addAnd(Expr.equal(TmailmessageDB.MAILKEY, new Integer(hashcode)))
									.addAnd(sentdate == 0 ? Expr.isNull(TmailmessageDB.SENDDATE) : Expr.equal(TmailmessageDB.SENDDATE, new Date(sentdate))));
						
						if (bool) {
							update.update(TmailmessageDB.FLAG, new MailWorker.StmntFromString(
									TmailmessageDB.FLAG + " | " + new Flags(flag).hashCode()));
						} else {
							update.update(TmailmessageDB.FLAG, new MailWorker.StmntFromString(
									TmailmessageDB.FLAG + " & (-2147483647 - " + new Flags(flag).hashCode() + ")"));
						}
						
						logger.log(Level.FINE, update.toString());
						
						update.executeUpdate(all.getModuleName());
					}
				}
			}
			
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}
	
	public void moveMessage(Integer folderid, List messageids) throws MailException {
		try {
			Map folderInfo = (Map)mFolderCache.get(folderid);
			Folder folder = (Folder)folderInfo.get(MailConstants.FOLDER_OBJECT);
			openFolder(folder);
			for (int i = 0; i < messageids.size(); i++) {
				Message m = folder.getMessage(((Integer)messageids.get(i)).intValue());
			}
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}
	
	public void addMessage(Integer folderid, Message message) throws MailException {
		try {
			Message msgs[] = { message };
			Map folderInfo = (Map)mFolderCache.get(folderid);
			Folder f = (Folder)folderInfo.get(MailConstants.FOLDER_OBJECT);
			openFolder(f);
			f.appendMessages(msgs);
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}

	public void addMessage(String folder, Message message) throws MailException {
		try {
			Message msgs[] = { message };
			Folder f = openFolder(folder);
			f.appendMessages(msgs);
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}

	/**
	 * l�scht die eMails, die als "zu l�schen" (Flag.DELETED) markiert sind.
	 * 
	 * @throws MailException
	 */
	public void removeMails(Integer folderid) throws MailException {
		try {
			Map folderInfo = (Map)mFolderCache.get(folderid);
			Folder folder = (Folder) folderInfo.get(MailConstants.FOLDER_OBJECT);
			folder.expunge();

//			List list = loadMessageList(folder, mFolderCacheFilter);
//			sortMessageList(list, (String)folderInfo.get(MailConstants.FOLDER_SORT_KEY), ((Boolean)folderInfo.get(MailConstants.FOLDER_SORT_REVERSE)).booleanValue());
//			folderInfo.put(MailConstants.FOLDER_MAIL_LIST, list);
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}
	
	public boolean renameFolder(Integer folderid, String newName) throws MessagingException {
		Map folderInfo = (Map)mFolderCache.get(folderid);
		Folder folder = (Folder)folderInfo.get(MailConstants.FOLDER_OBJECT);
		Folder newFolder = folder.getParent().getFolder(newName);
		return folder.renameTo(newFolder);
	}

	private Folder openFolder(Folder f) throws MessagingException {
		if (f != null && !f.isOpen()) {
			f.open(Folder.READ_WRITE);
		}
		return f;
	}

	private Folder closeFolder(Folder f) throws MessagingException {
		if (f != null && f.isOpen()) {
			f.close(false);
		}
		return f;
	}

	private Folder openFolder(String folder) throws MessagingException {
		return openFolder(mStore.getFolder(folder));
	}

	private Folder closeFolder(String folder) throws MessagingException {
		return closeFolder(mStore.getFolder(folder));
	}

	public boolean isProtocol(String name) {
		return MailConstants.PROTOCOL_IMAP.equalsIgnoreCase(name) || MailConstants.PROTOCOL_POP3.equalsIgnoreCase(name);
	}

	public boolean isThreadRunning() {
		return threadIsRunning;
	}

	private List loadFolderList(List list, Folder folder, boolean recuriv) {
		try {
			Folder folders[] = folder.list();
			for (int i = 0; i < folders.length; i++) {
				list.add(folders[i].getFullName());
				if (recuriv) {
					loadFolderList(list, folders[i], true);
				}
			}
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}
		return list;
	}

	/**
	 * L�dt die Nachrichten eines Folders.
	 * 
	 * MailHelper.MESSAGE_ID
	 * MailHelper.MESSAGE_SUBJECT
	 * MailHelper.MESSAGE_SENTDATE
	 * MailHelper.MESSAGE_FROM
	 * MailHelper.MESSAGE_FLAGS
	 * 
	 * @param id
	 * @return
	 * @throws MailException
	 */
//	private List loadMessageList(Folder folder, List filter) throws MailException {
//		boolean simpleAddresses = filter != null && filter.contains(MailConstants.FILTER_SIMPLEADDRESSES);
//		boolean useInputStreams = filter != null && filter.contains(MailConstants.FILTER_USEINPUTSTREAMS);
//		
//		try {
//			List result = new ArrayList();
//			if (folder != null) {
//				openFolder(folder);
//				Message[] message = folder.getMessages();
//				for (int i = 0; i < message.length; i++) {
//					try {
//						if (!message[i].isExpunged() && (showDeleted || !message[i].isSet(Flags.Flag.DELETED))) {
//							result.add(MailHelper.getMailInfo(
//									message[i],
//									filter,
//									simpleAddresses,
//									useInputStreams));
//						}
//					} catch (MessagingException ex) {
//						throw new MailException(ex);
//					}
//				}
//			}
//			return result;
//		} catch (MessagingException ex) {
//			throw new MailException(ex);
//		}
//	}

//	public void sortMessageList(List list, String key, boolean reverse) {
//		Collections.sort(list, new MailComparator(key, reverse));
//	}

	/**
	 * L�dt die Infos zu einen bestimmten Folder, siehe FOLDER_*
	 * 
	 * @param folder
	 * @return Map mit Infos �ber den Folder.
	 * @throws MailException
	 */
	private Map loadFolderInfo(Folder folder, Map info) throws MailException {
		try {
			if (info == null) {
				info = new HashMap();
//				info.put(MailConstants.FOLDER_STATUS, MailConstants.FOLDERSTATUS_INIT);
			}
			openFolder(folder);
			info.put(MailConstants.FOLDER_OBJECT, folder);
			info.put(MailConstants.FOLDER_NAME, folder.getName());
			info.put(MailConstants.FOLDER_FULLNAME, folder.getFullName());
			info.put(MailConstants.FOLDER_COUNT_ALL, new Integer(folder.getMessageCount()));
			info.put(MailConstants.FOLDER_COUNT_UNREAD, new Integer(folder.getUnreadMessageCount()));
			info.put(MailConstants.FOLDER_COUNT_NEW, new Integer(folder.getNewMessageCount()));
			return info;
		} catch (MessagingException ex) {
			return null;
		}
	}

	public Map getFolderCache() {
		return mFolderCache;
	}

	public Map getFolderCacheRecursiv() {
		return mFolderCacheRecursiv;
	}

	public String getFolderDelimiterRegEx() {
		return folderDelimiterRegEx;
	}

	public Integer getServerID() {
		return mID;
	}

	public String getServerName() {
		return mServer;
	}

	public int getServerPort() {
		return mPort;
	}

	public String getServerProtocol() {
		return mProtocol;
	}

	public String getServerUsername() {
		return mUsername;
	}

	public String getServerPassword() {
		return mPassword;
	}

	public String getDisplayName() {
		return mDisplayName;
	}

	public Integer getStatus() {
		return status;
	}
	
	public String getAttributes(String key) {
		if (mSettings.get(key) == null)
			return null;
		else
			return mSettings.get(key).toString();
	}
}
