/* $Id: MailException.java,v 1.2 2006/03/16 13:49:29 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

/**
 * Abstrakte Klasse um Exceptions nicht �ber die
 * Mail-API verwalten zu m�ssen.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public class MailException extends Exception {
	public MailException() {
		super();
	}
	
	public MailException(Throwable t) {
		super(t);
	}
	
	public MailException(String msg) {
		super(msg);
	}

	public MailException(String msg, Throwable t) {
		super(msg, t);
	}

	public static class Database extends MailException {
		public Database(Throwable t) {
			super(t);
		}
	}

	public static class Security extends MailException {
		public Security(String msg) {
			super(msg);
		}
	}
	
	public static class JobAlreadyEnqueuedException extends MailException {
	}
}
