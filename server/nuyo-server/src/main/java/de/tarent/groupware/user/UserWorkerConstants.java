package de.tarent.groupware.user;


/**
 * @author kleinw
 *
 *	Funktionalit�t f�r den User.
 *
 */
public interface UserWorkerConstants {

    public final static String KEY_LOGIN_NAME = "login";
    public final static String KEY_FIRST_NAME = "firstname";
    public final static String KEY_LAST_NAME = "lastname";
    public final static String KEY_PASSWORD = "password";
    public final static String KEY_ADMIN = "adminflag";

}
