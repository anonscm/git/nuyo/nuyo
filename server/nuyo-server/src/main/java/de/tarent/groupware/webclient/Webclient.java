/* $Id: Webclient.java,v 1.5 2006/09/15 13:00:03 kirchner Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.webclient;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import de.tarent.contact.octopus.DBDataAccessPostgres;
import de.tarent.contact.octopus.DataAccess;
import de.tarent.groupware.webclient.utils.LocaleMessage;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * Dieser Worker bearbeitet den Content fuer die korrekte
 * Anzeige im WebClient. Umlaute nach HTML-Standart konvertieren, etc.
 * 
 * @author christoph
 */
public class Webclient implements TcContentWorker {
	/**
	 * Dieser Logger soll f�r Ausgaben im Octopus-Kontext benutzt werden.
	 */
	public final static Logger logger = Logger.getLogger(Webclient.class.getName());
	private static Map localeMessages = new HashMap();

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
		Locale locale = Locale.getDefault();
		localeMessages.put(locale, new LocaleMessage(locale));
	}

	public String doAction(TcConfig tcConfig, String actionName, TcRequest tcRequest, TcContent tcContent) throws TcContentProzessException {

			String result = RESULT_error;
			DataAccess dataAccess = new DBDataAccessPostgres();

			if (actionName.equals(ACTION_INIT)) {
				/**
				 * Alle Daten laden und in Content und Session speichern.
				 */
				Map userData = loadUserdata(tcRequest, tcContent, tcConfig, dataAccess);
				Integer userid = (Integer)userData.get("id");

				Map userInfo = loadUserinfo(userid, dataAccess);
				Map configServer = loadConfigServer(userid, dataAccess);
				Map configWebclient = loadConfigWebclient(userid, dataAccess);
				
				tcContent.setField(Controller.SESSION_USERDATA_NAME, userData);
				tcContent.setField(Controller.SESSION_USERINFO_NAME, userInfo);
				tcContent.setField(Controller.SESSION_CONFIGSERVER_NAME, configServer);
				tcContent.setField(Controller.SESSION_CONFIGWEBCLIENT_NAME, configWebclient);
				tcContent.setField(CONTENT_LOCALEMESSAGES, loadLocaleMessage(tcContent, tcRequest));

				tcConfig.setSessionValue(Controller.SESSION_USERDATA_NAME, userData);
				tcConfig.setSessionValue(Controller.SESSION_USERINFO_NAME, userInfo);
				tcConfig.setSessionValue(Controller.SESSION_CONFIGSERVER_NAME, configServer);
				tcConfig.setSessionValue(Controller.SESSION_CONFIGWEBCLIENT_NAME, configWebclient);

				result = RESULT_ok;
				
			} else if (actionName.equals(ACTION_LOAD)) {
				/**
				 * Alle Daten aus Session laden und im Content speichern.
				 */
				Map userData = (Map)tcConfig.getSessionValueAsObject(Controller.SESSION_USERDATA_NAME);
				Map userInfo = (Map)tcConfig.getSessionValueAsObject(Controller.SESSION_USERINFO_NAME);
				Map configServer = (Map)tcConfig.getSessionValueAsObject(Controller.SESSION_CONFIGSERVER_NAME);
				Map configWebclient = (Map)tcConfig.getSessionValueAsObject(Controller.SESSION_CONFIGWEBCLIENT_NAME);

				tcContent.setField(Controller.SESSION_USERDATA_NAME, userData);
				tcContent.setField(Controller.SESSION_USERINFO_NAME, userInfo);
				tcContent.setField(Controller.SESSION_CONFIGSERVER_NAME, configServer);
				tcContent.setField(Controller.SESSION_CONFIGWEBCLIENT_NAME, configWebclient);
				tcContent.setField(CONTENT_LOCALEMESSAGES, loadLocaleMessage(tcContent, tcRequest));
				
			} else if (actionName.equals(ACTION_RELOAD)) {
				/**
				 * L�dt userData aus Session und l�dt alle anderen Daten neu.
				 * Speichert alle Daten dann im Content und in der Session.
				 */
				Map userData = (Map)tcConfig.getSessionValueAsObject(Controller.SESSION_USERDATA_NAME);
				Integer userid = (Integer)userData.get("id");

				Map userInfo = loadUserinfo(userid, dataAccess);
				Map configServer = loadConfigServer(userid, dataAccess);
				Map configWebclient = loadConfigWebclient(userid, dataAccess);

				tcContent.setField(Controller.SESSION_USERDATA_NAME, userData);
				tcContent.setField(Controller.SESSION_USERINFO_NAME, userInfo);
				tcContent.setField(Controller.SESSION_CONFIGSERVER_NAME, configServer);
				tcContent.setField(Controller.SESSION_CONFIGWEBCLIENT_NAME, configWebclient);
				tcContent.setField(CONTENT_LOCALEMESSAGES, loadLocaleMessage(tcContent, tcRequest));

				tcConfig.setSessionValue(Controller.SESSION_USERDATA_NAME, userData);
				tcConfig.setSessionValue(Controller.SESSION_USERINFO_NAME, userInfo);
				tcConfig.setSessionValue(Controller.SESSION_CONFIGSERVER_NAME, configServer);
				tcConfig.setSessionValue(Controller.SESSION_CONFIGWEBCLIENT_NAME, configWebclient);
			
			} else if (actionName.equals(ACTION_LOCALEMESSAGE)) {
				/**
				 * Stellt die lokalen Benutzer Nachrichten in den Content.
				 * @see loadLocaleMessage
				 * @see LocaleMessage
				 */
				tcContent.setField(CONTENT_LOCALEMESSAGES, loadLocaleMessage(tcContent, tcRequest));
				
			} else if (actionName.equals(ACTION_FORMATHTML)) {
				/**
				 * Formatiert den Content nach HTML, arbeitet rekursiv.
				 * @see FormatController
				 */
//				Iterator it = tcContent.getKeys();
//				while (it.hasNext()) {
//					String key = it.next().toString();
//					Object entry = tcContent.getAsObject(key);
//					if (entry == null) {
//					} else if (entry instanceof String) {
//						tcContent.setField(key, FormatController.formatTextToHtmlString((String)entry));
//					} else if (entry instanceof Map) {
//						tcContent.setField(key, FormatController.formatTextToHtmlMap((Map)entry));
//					} else if (entry instanceof List) {
//						tcContent.setField(key, FormatController.formatTextToHtmlList((List)entry));
//					}
//				}
				result = RESULT_ok;
			}

			return result;
	}

	/**
	 * L�dt Benutzerdaten (ID und Benutzername)
	 * 
	 * @param request
	 * @param content
	 * @param config
	 * @param dataAccess
	 * @return userdata
	 */
	private Map loadUserdata(TcRequest request, TcContent content, TcConfig config, DataAccess dataAccess) {
		Map result = new HashMap();
		String name = config.getLoginname();
		Integer id = dataAccess.getUserId(name);
		result.put("id", id);
		result.put("name", name);
	
		return result;
	}

	/**
	 * L�dt Benutzer-Informationen (z.B. "name", "fullname", ...)
	 * 
	 * @param userid
	 * @param dataAccess
	 * @return userinfos
	 */
	private Map loadUserinfo(Integer userid, DataAccess dataAccess) {
		return dataAccess.getUserProperties(userid, Controller.USERPARAM_INFO);
	}

	/**
	 * L�dt Benutzer-Server-Einstellungen (z.B. eMail-Server, ...)
	 * 
	 * @param userid
	 * @param dataAccess
	 */
	private Map loadConfigServer(Integer userid, DataAccess dataAccess) {
		return dataAccess.getUserProperties(userid, Controller.USERPARAM_CONFIG_SERVER);
	}

	/**
	 * L�dt Benutzer-Einstellungen f�r den Webclient (z.B. Anzahl-Elemente in der Liste, ...)
	 * 
	 * @param userid
	 * @param dataAccess
	 */
	private Map loadConfigWebclient(Integer userid, DataAccess dataAccess) {
		return dataAccess.getUserProperties(userid, Controller.UESRPARAM_CONFIG_WEBCLIENT);
	}
	
	/**
	 * L�dt die LocaleMessages zu einem Request.
	 * 
	 * @param tcContent
	 * @param tcRequest
	 */
	private LocaleMessage loadLocaleMessage(TcContent tcContent, TcRequest tcRequest) {
		Locale locale = (Locale)tcRequest.getParam(TcRequest.PARAM_LOCALE);
		LocaleMessage result = (LocaleMessage)localeMessages.get(locale);
		if (result == null) {
			result = new LocaleMessage(locale);
			localeMessages.put(locale, result);
		}
		return result;
	}
	
	/**
	 * "Globale" - Worker f�r den WebClient:
	 * 
	 * ACTION_WEBCLIENT_INIT
	 *  o Initalisiert den WebClient.
	 *  o L�dt Benutzeinstellungen und speichert diese
	 *    in einer Session.
	 *  o Stellt Benutzereinstellungen in den Content
	 * 
	 * ACTION_WEBCLIENT_LOAD
	 *  o L�dt die Benutzereinstellungen aus der Session
	 *    in den Content. Aktuallisiert die Benutzereinstellungen
	 *    in der Session nach x Minuten.
	 * 
	 * ACTION_WEBCLIENT_RELOAD
	 *  o Erzwingt das neu laden der Benutzereinstellungen,
	 *    speichert diese dann in der Session und im Content.
	 */
	public final static String ACTION_INIT = "init";
	public final static String ACTION_LOAD = "load";
	public final static String ACTION_RELOAD = "reload";
	public final static String ACTION_LOCALEMESSAGE = "localeMessage";
	public final static String ACTION_FORMATHTML = "formatHTML";
	
	public final static String CONTENT_LOCALEMESSAGES = "LOCALE";

	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port = new TcPortDefinition("de.tarent.octopus.webclient.Webclient", "Worker f�r den WebClient: Formatierung von Ein- und Ausgaben.");

		TcOperationDefinition operation;

		operation = port.addOperation(ACTION_INIT, "Initalisiert den WebClient und speichert Benutzerdaten in Session und Content.");
		operation.setInputMessage();
		operation.setOutputMessage();

		operation = port.addOperation(ACTION_LOAD, "Holt Benutzerdaten aus der Session und speichert diese im Content.");
		operation.setInputMessage();
		operation.setOutputMessage();

		operation = port.addOperation(ACTION_RELOAD, "L�dt Benutzerdaten erneut und speichert diese in der Session und im Content.");
		operation.setInputMessage();
		operation.setOutputMessage();

		operation = port.addOperation(ACTION_LOCALEMESSAGE, "Stellt die lokalen Benutzer Nachrichten in den Content.");
		operation.setInputMessage();
		operation.setOutputMessage();

		operation = port.addOperation(ACTION_FORMATHTML, "Formatiert den Content nach HTML-Standard.");
		operation.setInputMessage();
		operation.setOutputMessage();

		return port;
	}

	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.5 $") + " (" + CVS.getContent("$Date: 2006/09/15 13:00:03 $") + ')';
	}
}
