package de.tarent.groupware.admin;


/**
 * @author kleinw
 *
 *	Konstanten f�r den UserGroupWorker.
 *
 */
public interface UserGroupWorkerConstants {

    public final static String KEY_NAME = "name";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    
}
