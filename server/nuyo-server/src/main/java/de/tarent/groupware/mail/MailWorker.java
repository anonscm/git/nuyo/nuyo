/* $Id: MailWorker.java,v 1.8 2007/06/15 15:58:28 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.mail.Flags;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import de.tarent.contact.bean.TmailmessageDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.Statement;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.ExtPreparedStatement;
import de.tarent.groupware.mail.MailException.Database;
import de.tarent.groupware.user.UserWorker;
import de.tarent.groupware.utils.Syncer;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcReflectedWorker;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.response.TcBinaryResponseEngine;

/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.8 $
 * TODO deprecated-Markierungen dringend �berpr�fen!
 */
public class MailWorker extends TcReflectedWorker {
	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
	}
	
	
	
	public static void pre(TcAll all, String action) {
		if (TcRequest.isWebType(all.requestType())) {
			Integer indexSize = null;
			try {
				if (all.requestContains(MailConstants.PARAM_INDEXSIZE))
					indexSize = Integer.valueOf((String)all.requestAsObject(MailConstants.PARAM_INDEXSIZE));
				if (indexSize == null && all.contentContains(MailConstants.PARAM_INDEXSIZE))
					indexSize = Integer.valueOf((String)all.contentAsObject(MailConstants.PARAM_INDEXSIZE));
			} catch (NumberFormatException e) {
			} catch (ClassCastException e) {
			}
			if (indexSize == null) {
				indexSize = (Integer)all.sessionAsObject("MailWorker.indexSize");
				if (indexSize == null) {
					indexSize = new Integer(10);
				}
			}
			all.setContent(MailConstants.PARAM_INDEXSIZE, indexSize);
			all.setSession("MailWorker.indexSize", indexSize);
		}
	}
	
	
	
	public final static String[] INPUT_INIT = { MailConstants.PARAM_FILTER };
	public final static boolean[] MANDATORY_INIT = { false };
	public final static String OUTPUT_INIT = null;
	
	public final static void init(TcAll all, List filter) throws SQLException {
		try {
			getStores(all, true, getUserID(all), filter);
		} catch (Database e) {
			setException(all, e);
		}
	}
	
	
	
	public final static String[] INPUT_LOAD = { MailConstants.PARAM_FILTER };
	public final static boolean[] MANDATORY_LOAD = { false };
	public final static String OUTPUT_LOAD = null;
	
	/**
	 * 
	 * @param all
	 * @param filter
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static void load(TcAll all, List filter) throws SQLException {
		try {
			getStores(all, false, getUserID(all), filter);
		} catch (Database e) {
			setException(all, e);
		}
	}
	
	
	
	public final static String[] INPUT_GETSIMPLESTORELIST = {};
	public final static boolean[] MANDATORY_GETSIMPLESTORELIST = {};
	public final static String OUTPUT_GETSIMPLESTORELIST = MailConstants.CONTENT_STORELIST;
	
	/**
	 * 
	 * @param all
	 * @return
	 * @throws SQLException
	 */
	public final static List getSimpleStoreList(TcAll all) throws SQLException {
		try {
			return MailDB.getStores(getUserID(all));
		} catch (Database e) {
			e.printStackTrace();
			setException(all, e);
			return null;
		}
	}
	
	
	public final static String[] INPUT_GETSTOREDETAIL = { MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_GETSTOREDETAIL = { true };
	public final static String OUTPUT_GETSTOREDETAIL = MailConstants.CONTENT_STOREDETAIL;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 */
	public final static Map getStoreDetail(TcAll all, Integer storeid) {
		try {
			return MailDB.getStoreInfo(getUserID(all), storeid);
		} catch (Database e) {
			setException(all, e);
			return null;
		}
	}
	
	
	
	public final static String[] INPUT_GETSTORELIST = {};
	public final static boolean[] MANDATORY_GETSTORELIST = {};
	public final static String OUTPUT_GETSTORELIST = MailConstants.CONTENT_STORELIST;
	
	/**
	 * 
	 * @param all
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getStoreList(TcAll all) throws SQLException {
		try {
			return getStores(all, false, getUserID(all), null);
		} catch (Database e) {
			setException(all, e);
			return null;
		}
	}
	
	
	
	public final static String[] INPUT_GETMAILLIST = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_SORTKEY,
			MailConstants.PARAM_SORTREVERSE,
			MailConstants.PARAM_INDEXFROM,
			MailConstants.PARAM_INDEXSIZE };
	public final static boolean[] MANDATORY_GETMAILLIST = { true, true, false, false, false, false };
	public final static String OUTPUT_GETMAILLIST = MailConstants.CONTENT_MAILLIST;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param sortkey
	 * @param sortreverse
	 * @param indexFrom
	 * @param indexSize
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static List getMailList(
			TcAll all,
			Integer storeid,
			Integer folderid,
			String sortkey,
			Boolean sortreverse,
			Integer indexFrom,
			Integer indexSize) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());
		all.setContent(MailConstants.CONTENT_FOLDERID, folderid);
		
		Result tcResult = null;
		try {
			tcResult = SQL.Select(TcDBContext.getDefaultContext())
					.from(TmailmessageDB.getTableName())
					.select(TmailmessageDB.MAILKEY)
					.select(TmailmessageDB.SENDDATE)
					.select(TmailmessageDB.SENDERNAME)
					.select(TmailmessageDB.SUBJECT)
					.select(TmailmessageDB.FLAG)
					.select(TmailmessageDB.FKADDRESS)
					.where(Where.list()
							.add(Expr.equal(TmailmessageDB.FKUSER, getUserID(all)))
							.addAnd(Expr.equal(TmailmessageDB.FKMAILSTORECATEGORY, folderid))
							// alle nicht als gel�scht markierten
							.addAnd(Expr.equal("(" + TmailmessageDB.FLAG + " & 2)", new Integer(0))))
					.orderBy(Order.desc(TmailmessageDB.SENDDATE))
					.executeSelect(TcDBContext.getDefaultContext());
			
			ArrayList result = new ArrayList();
			ResultSet resultSet = tcResult.resultSet();
			
			// Einschr�nkung der R�ckgabe
			int i = 0;
			int from = 0;
			int size = 0;
			if (indexSize != null) {
				if (indexFrom != null && indexFrom.intValue() != 0)
					from = indexFrom.intValue();
				else
					from = 1;
				size = indexSize.intValue();
				if (from != 1)
					resultSet.absolute(from - 1);
			}
			
			while (resultSet.next() && (size == 0 || i < size)) {
				HashMap entry = new HashMap();
				entry.put(MailConstants.MESSAGE_ID, resultSet.getObject(1));
				Date sentdate = resultSet.getTimestamp(2);
				if (sentdate != null)
					entry.put(MailConstants.MESSAGE_SENTDATE, new Long(sentdate.getTime()));
				// TODO remove hack
				entry.put("fromstring", resultSet.getString(3));
				entry.put(MailConstants.MESSAGE_SUBJECT, resultSet.getString(4));
				entry.put(MailConstants.MESSAGE_FLAGS, new Integer(resultSet.getInt(5)));
				// TODO remove hack
				entry.put("fromaddress", resultSet.getObject(6));
				result.add(entry);
				i++;
			}
			
			if (TcRequest.isWebType(all.requestType()) && result.size() > 0) {
				resultSet.last();
				HashMap info = new HashMap();
				info.put("from", new Integer(from));
				info.put("size", new Integer(resultSet.getRow()));
				info.put("show", indexSize);
				all.setContent("mailListInfo", info);
			}
			
			return result;
		} catch (SQLException e) {
			setException(all, e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
		return null;
	}
	
	
	
	public final static String[] INPUT_GETMAILDETAIL = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_MESSAGEID,
			MailConstants.PARAM_MESSAGEDATE,
			MailConstants.PARAM_FILTER };
	public final static boolean[] MANDATORY_GETMAILDETAIL = { true, true, true, true, false };
	public final static String OUTPUT_GETMAILDETAIL = MailConstants.CONTENT_MAILDETAIL;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param messageid
	 * @param messagedate
	 * @param filter
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getMailDetail(
			TcAll all,
			Integer storeid,
			Integer folderid,
			Integer messageid,
			Long messagedate,
			List filter) throws SQLException {
		
		if (filter == null)
			filter = MailHelper.filter_all;
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());
		all.setContent(MailConstants.CONTENT_FOLDERID, folderid);
		all.setContent(MailConstants.CONTENT_MESSAGEID, messageid);
		
		try {
			return store.getMessageDetail(all, folderid, messageid.intValue(), messagedate.longValue(), filter);
		} catch (MailException e) {
			setException(all, e);
			return null;
		}
	}
	
	
	
	public final static String[] INPUT_SETMAILFLAG = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_MESSAGEID,
			MailConstants.PARAM_FLAG ,
			MailConstants.PARAM_FLAGVALUE };
	public final static boolean[] MANDATORY_SETMAILFLAG = {
			true, true, true,
			true, true };
	public final static String OUTPUT_SETMAILFLAG = null;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param messageids
	 * @param flag
	 * @param flagvalue
	 */
	public final static void setMailFlag(
			TcAll all,
			Integer storeid,
			Integer folderid,
			List messageids,
			String flag,
			Boolean flagvalue) {
		
		try {
			Flags.Flag mailflag = getMailFlag(flag);
			
			Map stores = getStoreList(all);
			MailStore store = (MailStore)stores.get(storeid);
			store.setMessageFlag(all, folderid, messageids, mailflag, flagvalue.booleanValue());
		} catch (MailException e) {
			setException(all, e);
		} catch (SQLException e) {
			setException(all, e);
		}
	}
	
	
	
	public final static String[] INPUT_GETMAILREPLY = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_MESSAGEID,
			MailConstants.PARAM_MESSAGEDATE,
			MailConstants.PARAM_ACTION };
	public final static boolean[] MANDATORY_GETMAILREPLY = { true, true, true, true, true };
	public final static String OUTPUT_GETMAILREPLY = "mailTransferDetails";
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param messageid
	 * @param messagedate
	 * @param action
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getMailReply(
			TcAll all,
			Integer storeid,
			Integer folderid,
			Integer messageid,
			Long messagedate,
			String action) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());
		all.setContent(MailConstants.CONTENT_FOLDERID, folderid);
		all.setContent(MailConstants.CONTENT_MESSAGEID, messageid);
		
		try {
			// nur Daten holen die auch ben�tigt werden
			ArrayList filter = new ArrayList();
			filter.add(MailConstants.MESSAGE_SUBJECT);
			filter.add(MailConstants.MESSAGE_CONTENT);
			filter.add(MailConstants.MESSAGE_FROM);
			filter.add(MailConstants.MESSAGE_CC);
			filter.add(MailConstants.MESSAGE_BCC);
			
			Map messageDetails = store.getMessageDetail(all, folderid, messageid.intValue(), messagedate.longValue(), filter);
			Map messageTransport = new HashMap();
			
			if (action.equals(MailConstants.VALUE_REPLY_RE)) {
				StringBuffer to = new StringBuffer(50);
				List list = (List)messageDetails.get(MailConstants.MESSAGE_FROM);
				if (list != null) {
					for (int i = 0; i < list.size(); i++) {
						if (to.length() > 0) to.append("; ");
						to.append(((Map)list.get(i)).get(MailConstants.MAILADDRESS_STRING));
					}
				}
				messageTransport.put("to", to.toString());
				messageTransport.put("subject", "Re: " + messageDetails.get(MailConstants.MESSAGE_SUBJECT));
			} else if (action.equals(MailConstants.VALUE_REPLY_REALL)) {
				StringBuffer to = new StringBuffer(50);
				List list = (List)messageDetails.get(MailConstants.MESSAGE_FROM);
				if (list != null) {
					for (int i = 0; i < list.size(); i++) {
						if (to.length() > 0) to.append("; ");
						to.append(((Map)list.get(i)).get(MailConstants.MAILADDRESS_STRING));
					}
				}
				list = (List) messageDetails.get(MailConstants.MESSAGE_TO);
				if (list != null) {
					for (int i = 0; i < list.size(); i++) {
						if (to.length() > 0) to.append("; ");
						to.append(((Map)list.get(i)).get(MailConstants.MAILADDRESS_STRING));
					}
				}
				messageTransport.put("to", to.toString());
				to = new StringBuffer(50);
				list = (List) messageDetails.get(MailConstants.MESSAGE_CC);
				if (list != null) {
					for (int i = 0; i < list.size(); i++) {
						if (to.length() > 0) to.append("; ");
						to.append(((Map)list.get(i)).get(MailConstants.MAILADDRESS_STRING));
					}
				}
				messageTransport.put("cc", to.toString());
				messageTransport.put("subject", "Re: " + messageDetails.get(MailConstants.MESSAGE_SUBJECT));
			} else if (action.equals(MailConstants.VALUE_REPLY_FWD)) {
				messageTransport.put("subject", "Fwd: " + messageDetails.get(MailConstants.MESSAGE_SUBJECT));
			}
			
			StringBuffer firstline = new StringBuffer(30);
			List list = (List)messageDetails.get(MailConstants.MESSAGE_FROM);
			if (list != null && list.size() > 0) {
				Map map = (Map)list.get(0);
				if (map.containsKey(MailConstants.MAILADDRESS_NAME)) {
					firstline
						.append(map.get(MailConstants.MAILADDRESS_NAME))
						.append(" schrieb:");
				} else {
					firstline
						.append(map.get(MailConstants.MAILADDRESS_STRING))
						.append(" schrieb:");
				}
			}
			Object mailContent = messageDetails.get(MailConstants.MESSAGE_CONTENT);
			if (mailContent instanceof String) {
				messageTransport.put("body", MailHelper.quotation(firstline.toString(),
						(String)mailContent));
			} else if (mailContent instanceof List) {
				messageTransport.put("body", MailHelper.quotation(firstline.toString(),
						((Map)((List)mailContent).get(0)).get(MailConstants.MESSAGE_CONTENT).toString()));
			}
			return messageTransport;
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		}
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return null;
	}
	
	
	
	public final static String[] INPUT_GETMAILATTACHMENT = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_MESSAGEID,
			MailConstants.PARAM_MESSAGEDATE,
			MailConstants.PARAM_ATTACHMENTID };
	public final static boolean[] MANDATORY_GETMAILATTACHMENT = { true, true, true, true, true };
	public final static String OUTPUT_GETMAILATTACHMENT = MailConstants.CONTENT_ATTACHMENTSTREAM;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param messageid
	 * @param messagedate
	 * @param attachmentid
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Object getMailAttachment(
			TcAll all,
			Integer storeid,
			Integer folderid,
			Integer messageid,
			Long messagedate,
			Integer attachmentid) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_FOLDERID, folderid);
		all.setContent(MailConstants.CONTENT_MESSAGEID, messageid);
		
		try {
			Map mailDetails = store.getMessageDetail(all, folderid, messageid.intValue(), messagedate.longValue(), MailHelper.filter_content);
			List mailContent = (List)mailDetails.get(MailConstants.MESSAGE_CONTENT);
			
			Map map = (Map)((List)mailContent).get(attachmentid.intValue());
			
			if (TcRequest.isWebType(all.requestType())) {
				HashMap filestream = new HashMap();
				filestream.put("type", TcBinaryResponseEngine.BINARY_RESPONSE_TYPE_STREAM);
				filestream.put("stream", (InputStream)map.get("content"));
				filestream.put("filename", map.get("name"));
				return filestream;
			} else {
				return inputStreamAsList((InputStream)map.get("content"));
			}
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		} catch (IndexOutOfBoundsException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		} catch (IOException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		}
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return null;
	}
	
	

	public final static String[] INPUT_DOMAILACTION = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_ACTION };
	public final static boolean[] MANDATORY_DOMAILACTION = { true, true, true };
	public final static String OUTPUT_DOMAILACTION = null;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param action
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static void doMailAction(
			TcAll all,
			Integer storeid,
			Integer folderid,
			String action) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);

		if (action.equals("remove")) {
			removeMail(all, storeid, folderid, getSelectedMails(all), Boolean.TRUE);
		} else if (action.equals("removeUndo")) {
			removeMail(all, storeid, folderid, getSelectedMails(all), Boolean.FALSE);
		} else if (action.equals("removeNow")) {
			expungeFolder(all, storeid, folderid);
		}
	}

	
	
	public final static String[] INPUT_REMOVEMAIL = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_MESSAGEIDS,
			MailConstants.PARAM_BOOLEAN };
	public final static boolean[] MANDATORY_REMOVEMAIL = { true, true, true, true };
	public final static String OUTPUT_REMOVEMAIL = null;
	
	/**
	 * @deprecated
	 */
	public final static void removeMail(
			TcAll all,
			Integer storeid,
			Integer folderid,
			List messageids,
			Boolean flag) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_FOLDERID, folderid);

		try {
			store.setMessageFlag(all, folderid, messageids, Flags.Flag.DELETED, flag.booleanValue());
			return;
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		} catch (SQLException e) {
			setException(all, e);
		}			
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return;
	}

	
		
	public final static String[] INPUT_EXPUNGEFOLDER = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID };
	public final static boolean[] MANDATORY_EXPUNGEFOLDER = { true, true };
	public final static String OUTPUT_EXPUNGEFOLDER = null;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @throws SQLException
	 * @deprecated will soon be removed do cleanup source
	 */
	public final static void expungeFolder(
			TcAll all,
			Integer storeid,
			Integer folderid) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_FOLDERID, folderid);

		try {
			store.removeMails(folderid);
			return;
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		}			
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return;
	}

	
		
	public final static String[] INPUT_GETALLFOLDER = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_GETALLFOLDER = { true };
	public final static String OUTPUT_GETALLFOLDER = MailConstants.CONTENT_FOLDERLIST_ALL;

	/**
	 * 
	 * @param all
	 * @param storeid
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static List getAllFolder(
			TcAll all,
			Integer storeid) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());
		all.setContent(MailConstants.CONTENT_STOREID, storeid);

		return store.getCompleteFolderList();
	}
	
	
	
	public final static String[] INPUT_GETALLFOLDERRECURSIV = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_GETALLFOLDERRECURSIV = { true };
	public final static String OUTPUT_GETALLFOLDERRECURSIV = MailConstants.CONTENT_FOLDERLIST_ALL;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getAllFolderRecursiv(
			TcAll all,
			Integer storeid) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());
		
		List list = store.getCompleteFolderList();
		Map folder;
		Map result = new TreeMap();
		int i = 0;
		for (Iterator it = list.iterator(); it.hasNext(); i++) {
			folder = new TreeMap();
			folder.put(MailConstants.FOLDER_FULLNAME, it.next());
			result.put(new Integer(i), folder);
		}
		return MailHelper.createRecursiveMap(result, store.getFolderDelimiterRegEx());
	}
	
	
	
	public final static String[] INPUT_GETFOLDER = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_GETFOLDER = { true };
	public final static String OUTPUT_GETFOLDER = MailConstants.CONTENT_FOLDERLIST;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getFolder(
			TcAll all,
			Integer storeid) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());

		return store.getFolderCache();
	}
	
	
	
	public final static String[] INPUT_GETSIMPLEFOLDERLIST = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_GETSIMPLEFOLDERLIST = { true };
	public final static String OUTPUT_GETSIMPLEFOLDERLIST = MailConstants.CONTENT_FOLDERLIST;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @return
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getSimpleFolderList(
			TcAll all,
			Integer storeid) {
		
		try {
			return MailDB.getStoreFolders(all.getModuleName(), storeid);
		} catch (Database e) {
			e.printStackTrace();
			setException(all, e);
			return null;
		}
	}
	
	
	
	public final static String[] INPUT_GETFOLDERDETAIL = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_FOLDERID,
			MailConstants.PARAM_FILTER };
	public final static boolean[] MANDATORY_GETFOLDERDETAIL = { true, true, true };
	public final static String OUTPUT_GETFOLDERDETAIL = MailConstants.CONTENT_FOLDERDETAIL;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @param folderid
	 * @param filter
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getFolderDetail(
			TcAll all,
			Integer storeid,
			Integer folderid,
			List filter) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		return store.getFolderDetail(folderid, filter);
	}
	
	
	
	public final static String[] INPUT_GETRECURSIVFOLDER = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_GETRECURSIVFOLDER = { true };
	public final static String OUTPUT_GETRECURSIVFOLDER = MailConstants.CONTENT_FOLDERLIST_RECURSIV;
	
	/**
	 * 
	 * @param all
	 * @param storeid
	 * @return
	 * @throws SQLException
	 * @deprecated will soon be removed to cleanup source
	 */
	public final static Map getRecursivFolder(
			TcAll all,
			Integer storeid) throws SQLException {
		
		Map stores = getStoreList(all);
		MailStore store = (MailStore)stores.get(storeid);
		all.setContent(MailConstants.CONTENT_STOREID, storeid);
		all.setContent(MailConstants.CONTENT_STORESTATUS, store.getStatus());

		return store.getFolderCacheRecursiv();
	}
	
	
	
	public final static String[] INPUT_INSERTSTORE = {
			MailConstants.PARAM_DISPLAYNAME,
			MailConstants.PARAM_SERVERPROTOCOL,
			MailConstants.PARAM_SERVERNAME,
			MailConstants.PARAM_SERVERPORT,
			MailConstants.PARAM_USERNAME,
			MailConstants.PARAM_PASSWORD,
			MailConstants.PARAM_INBOXFOLDER,
			MailConstants.PARAM_SENTFOLDER,
			MailConstants.PARAM_TRASHFOLDER };
	public final static boolean[] MANDATORY_INSERTSTORE = { true, true, true, false, false, false, false, false, false };
	public final static String OUTPUT_INSERTSTORE = MailConstants.CONTENT_STOREID;
	
	/**
	 * 
	 * @param all
	 * @param displayname
	 * @param protocol
	 * @param servername
	 * @param port
	 * @param username
	 * @param password
	 * @param inboxFolder
	 * @param sentFolder
	 * @param trashFolder
	 * @return
	 */
	public final static Integer insertStore(
			TcAll all,
			String displayname,
			String protocol,
			String servername,
			Integer port,
			String username,
			String password,
			String inboxFolder,
			String sentFolder,
			String trashFolder) {
		
		try {
			new MailStore(protocol, servername, port, username, password);
			return MailDB.createStore(
					all.getModuleName(),
					getUserID(all),
					displayname,
					protocol,
					servername,
					port,
					username,
					password,
					inboxFolder,
					sentFolder,
					trashFolder);
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		}			
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return null;
	}

	
	
	public final static String[] INPUT_UPDATESTORE = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_DISPLAYNAME,
			MailConstants.PARAM_SERVERPROTOCOL,
			MailConstants.PARAM_SERVERNAME,
			MailConstants.PARAM_SERVERPORT,
			MailConstants.PARAM_USERNAME,
			MailConstants.PARAM_PASSWORD,
			MailConstants.PARAM_INBOXFOLDER,
			MailConstants.PARAM_SENTFOLDER,
			MailConstants.PARAM_TRASHFOLDER };
	public final static boolean[] MANDATORY_UPDATESTORE = { true, true, true, true, false, false, false, false, false, false };
	public final static String OUTPUT_UPDATESTORE = MailConstants.CONTENT_STOREID;
	
	public final static Integer updateStore(
			TcAll all,
			Integer storeid,
			String displayname,
			String protocol,
			String servername,
			Integer port,
			String username,
			String password,
			String inboxFolder,
			String sentFolder,
			String trashFolder) {
		
		try {
			new MailStore(protocol, servername, port, username, password);
			return MailDB.updateStore(
					all.getModuleName(),
					getUserID(all),
					storeid,
					displayname,
					protocol,
					servername,
					port,
					username,
					password,
					inboxFolder,
					sentFolder,
					trashFolder);
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		}			
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return null;
	}

	
	
	public final static String[] INPUT_REMOVESTORE = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_REMOVESTORE = { true };
	public final static String OUTPUT_REMOVESTORE = null;
	
	public final static void removeStore(
			TcAll all,
			Integer storeid) {
		
		try {
			MailDB.removeStore(
					all.getModuleName(),
					getUserID(all),
					storeid);
		} catch (MailException.Database e) {
			
		}
		return;
	}

	
	
	public final static String[] INPUT_CONFIGURESTORE = {
			MailConstants.PARAM_STOREID,
			MailConstants.PARAM_DISPLAYNAME,
			MailConstants.PARAM_SERVERPROTOCOL,
			MailConstants.PARAM_SERVERNAME,
			MailConstants.PARAM_SERVERPORT,
			MailConstants.PARAM_USERNAME,
			MailConstants.PARAM_PASSWORD,
			MailConstants.PARAM_ACTION,
			MailConstants.PARAM_INBOXFOLDER,
			MailConstants.PARAM_SENTFOLDER,
			MailConstants.PARAM_TRASHFOLDER };
	public final static boolean[] MANDATORY_CONFIGURESTORE = {
			false, false, false, false, false, false, false,
			true, false, false, false };
	public final static String OUTPUT_CONFIGURESTORE = MailConstants.CONTENT_STOREID;
	
	public final static Integer configureStore(
			TcAll all,
			Integer storeid,
			String displayname,
			String protocol,
			String servername,
			Integer port,
			String username,
			String password,
			String action,
			String inboxFolder,
			String sentFolder,
			String trashFolder) {
		
		if (action.equals(MailConstants.VALUE_CONF_INSERT)) {
			if (
					displayname != null &&
					protocol != null &&
					servername != null)
			storeid = insertStore(
					all,
					displayname,
					protocol,
					servername,
					port,
					username,
					password,
					inboxFolder,
					sentFolder,
					trashFolder);
		} else if (action.equals(MailConstants.VALUE_CONF_UPDATE)) {
			if (
					displayname != null &&
					protocol != null &&
					servername != null &&
					storeid != null)
			storeid = updateStore(
					all,
					storeid,
					displayname,
					protocol,
					servername,
					port,
					username,
					password,
					inboxFolder,
					sentFolder,
					trashFolder);
		} else if (action.equals(MailConstants.VALUE_CONF_REMOVE)) {
			if (storeid != null)
				removeStore(all, storeid);
		} else if (action.equals(MailConstants.VALUE_CONF_SETFOLDER)) {
			if (storeid != null)
				setStoreFolder(all, storeid);
		}
		return storeid;
	}
	
	
	
	public final static String[] INPUT_SETSTOREFOLDER = {
			MailConstants.PARAM_STOREID };
	public final static boolean[] MANDATORY_SETSTOREFOLDER = { true };
	public final static String OUTPUT_SETSTOREFOLDER = null;
	
	public final static void setStoreFolder(
			TcAll all,
			Integer storeid) {
		
		try {
			MailDB.setStoreFolders(
					all.getModuleName(),
					getUserID(all),
					storeid,
					getSelectedFolder(all));
			return;
		} catch (MailException e) {
			all.setContent(MailConstants.CONTENT_EXCEPTION, e);
		}			
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
		return;
	}
	
	
	
	/**
	 * L�dt die eMail-Stores und gibt Map mit der ID als Key zur�ck.
	 * 
	 * Versucht die Stores zuerst aus der Session zu laden,
	 * sollten dort keine Daten hinterlegt oder reloadForce
	 * wahr sein werden diese neu angelegt.
	 * 
	 * @param all
	 * @param reloadForce
	 * @param userId
	 * @return Map mit ID => MailStore
	 */
	private static Map getStores(TcAll all, boolean reloadForce, Integer userId, List filter) throws Database, SQLException {
		Map stores = (Map)all.sessionAsObject(MailConstants.SESSION_STORES);
		if (stores == null) {
			stores = new HashMap();
			reloadForce = true;
		}
		
		if (reloadForce) {
			stores.clear();
			Iterator it = MailDB.getStores(userId).iterator();
			while (it.hasNext()) {
				try {
					Integer id = (Integer)it.next();
					
					// MailStore - Daten
					Map storeInfo = MailDB.getStoreInfo(userId, id);
					Map storeFolder = MailDB.getStoreFolders(all.getModuleName(), id);
					
					// Init MailStore
					MailStore mailStore = new MailStore(storeInfo);
					mailStore.setFilter(filter);
					mailStore.addFolder(storeFolder);
					new Thread(mailStore).start();
					stores.put(id, mailStore);
					
					// Init MailSyncer
					MailSyncer mailSyncer = (MailSyncer)Syncer.get("mailsyncer-" + id);
					if (mailSyncer == null) {
						mailSyncer = new MailSyncer(all, userId, id, all.getModuleName());
						mailSyncer.start();
					}
				} catch (MailException e) {
					e.printStackTrace();
				}
			}
			all.setSession(MailConstants.SESSION_STORES, stores);
		}
		
		// laufende MailSyncer verl�ngern
		Iterator it = stores.keySet().iterator();
		while (it.hasNext()) {
			Integer id = (Integer)it.next();
			MailSyncer mailSyncer = (MailSyncer)Syncer.get("mailsyncer-" + id);
			if (mailSyncer == null) {
				try {
					mailSyncer = new MailSyncer(all, userId, id, all.getModuleName());
					mailSyncer.start();
				} catch (MailException e) {
					e.printStackTrace();
				}
			} else {
				mailSyncer.syncExtend();
			}
		}
		return stores;
	}
	
	/**
	 * @param flag	MailFlag als String
	 * @return Flags.Flag
	 */
	private static Flags.Flag getMailFlag(String flag) {
		if (MailConstants.FLAGS_ANSWERED.equals(flag))
			return Flags.Flag.ANSWERED;
		else if (MailConstants.FLAGS_DELETED.equals(flag))
			return Flags.Flag.DELETED;	
		else if (MailConstants.FLAGS_DRAFT.equals(flag))
			return Flags.Flag.DRAFT;
		else if (MailConstants.FLAGS_FLAGGED.equals(flag))
			return Flags.Flag.FLAGGED;
		else if (MailConstants.FLAGS_RECENT.equals(flag))
			return Flags.Flag.RECENT;
		else if (MailConstants.FLAGS_SEEN.equals(flag))
			return Flags.Flag.SEEN;
		else if (MailConstants.FLAGS_USER.equals(flag))
			return Flags.Flag.USER;
		return null;
	}
	
	private static Integer getUserID(TcAll all) {
		try {
			Map userData = (Map)all.contentAsObject("userData");
			if (userData != null && userData.containsKey("id")) {
				return (Integer)userData.get("id");
			} else if (all.requestContains("username")) {
				return UserWorker.getUserForLoginname(
						all,
						all.requestAsString("username"));
			}
		} catch (SQLException e) {
		}
		return null;
	}
	
	private static void setException(TcAll all, Throwable t) {
		t.printStackTrace();
		all.setContent(MailConstants.CONTENT_EXCEPTION, t);
		all.setContent(MailConstants.CONTENT_ERROR, "fehler");
	}
	
	private static List getSelectedMails(TcAll all) {
		Iterator it = all.getRequestObject().getRequestParameters().keySet().iterator();
		List messageid = new ArrayList();
		while (it.hasNext()) {
			String key = it.next().toString();
			if (key.startsWith("selectMail-")) {
				messageid.add(key.substring(11));
			}
		}
		return messageid;
	}
	
	private static List getSelectedFolder(TcAll all) {
		Iterator it = all.getRequestObject().getRequestParameters().keySet().iterator();
		List folder = new ArrayList();
		while (it.hasNext()) {
			String key = (String)it.next();
			if (key.startsWith("selectFolder-")) {
				folder.add(all.requestAsString(key));
			}
		}
		return folder;
	}
	
	private static List inputStreamAsList(InputStream inputStream) throws IOException {
		ArrayList list = new ArrayList(100); 
		int i;
		byte buffer[] = new byte[1024];
		while ((i = inputStream.read(buffer)) > 0) {
			if (i != 1024) {
				byte buffer2[] = new byte[i];
				for (int i2 = 0; i2 < i; i2++)
					buffer2[i2] = buffer[i2];
				buffer = buffer2;
			}
			list.add(buffer);
			buffer = new byte[1024];
		}
		return list;
	}
	
	public static class StmntFromString implements Statement {
		private String _string;
		
		public StmntFromString(String stmnt) {
			_string = stmnt;
		}
		
		public String toString() {
			return _string;
		}
		
		public String statementToString() {
			return _string;
		}

		public Object execute() throws SQLException {
			throw new SQLException("Not supported!");
		}

		public void setDBContext(DBContext arg0) {
			//Only for compatibility
		}

		public ExtPreparedStatement prepare(DBContext dbContext) throws SQLException
		{
			// TODO Auto-generated method stub
			throw new NotImplementedException();
		}

		public ExtPreparedStatement prepare() throws SQLException
		{
			// TODO Auto-generated method stub
			throw new NotImplementedException();
		}

		public void getParams(List list)
		{
			// TODO Auto-generated method stub
			throw new NotImplementedException();
		}
	}
}
