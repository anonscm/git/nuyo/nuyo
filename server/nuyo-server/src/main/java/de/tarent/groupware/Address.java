/* $Id: Address.java,v 1.16 2007/07/03 12:15:36 nils Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware;

import java.util.Date;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.groupware.mail.MailException.Database;

/**
 * This is the basic interface for an address.
 *
 * For each property of the address a constant should be defined.
 *
 * @author Sebastian Mancke, tarent GmbH
 */
public interface Address extends Entity {

	public final static AddressProperty PROPERTY_COMMONTEXT01 = AddressProperty.make("commontext01");
	public final static AddressProperty PROPERTY_COMMONTEXT02 = AddressProperty.make("commontext02");
	public final static AddressProperty PROPERTY_COMMONTEXT03 = AddressProperty.make("commontext03");
	public final static AddressProperty PROPERTY_COMMONTEXT04 = AddressProperty.make("commontext04");
	public final static AddressProperty PROPERTY_COMMONTEXT05 = AddressProperty.make("commontext05");
	public final static AddressProperty PROPERTY_COMMONTEXT06 = AddressProperty.make("commontext06");
	public final static AddressProperty PROPERTY_COMMONTEXT07 = AddressProperty.make("commontext07");
	public final static AddressProperty PROPERTY_COMMONTEXT08 = AddressProperty.make("commontext08");
	public final static AddressProperty PROPERTY_COMMONTEXT09 = AddressProperty.make("commontext09");
	public final static AddressProperty PROPERTY_COMMONTEXT10 = AddressProperty.make("commontext10");
	public final static AddressProperty PROPERTY_COMMONTEXT11 = AddressProperty.make("commontext11");
	public final static AddressProperty PROPERTY_COMMONTEXT12 = AddressProperty.make("commontext12");
	public final static AddressProperty PROPERTY_COMMONTEXT13 = AddressProperty.make("commontext13");
	public final static AddressProperty PROPERTY_COMMONTEXT14 = AddressProperty.make("commontext14");
	public final static AddressProperty PROPERTY_COMMONTEXT15 = AddressProperty.make("commontext15");
	public final static AddressProperty PROPERTY_COMMONTEXT16 = AddressProperty.make("commontext16");
	public final static AddressProperty PROPERTY_COMMONTEXT17 = AddressProperty.make("commontext17");
	public final static AddressProperty PROPERTY_COMMONTEXT18 = AddressProperty.make("commontext18");
	public final static AddressProperty PROPERTY_COMMONTEXT19 = AddressProperty.make("commontext19");
	public final static AddressProperty PROPERTY_COMMONTEXT20 = AddressProperty.make("commontext20");
	
	public final static AddressProperty PROPERTY_COMMONINT01 = AddressProperty.make("commonint01");
	public final static AddressProperty PROPERTY_COMMONINT02 = AddressProperty.make("commonint02");
	public final static AddressProperty PROPERTY_COMMONINT03 = AddressProperty.make("commonint03");
	public final static AddressProperty PROPERTY_COMMONINT04 = AddressProperty.make("commonint04");
	public final static AddressProperty PROPERTY_COMMONINT05 = AddressProperty.make("commonint05");
	public final static AddressProperty PROPERTY_COMMONINT06 = AddressProperty.make("commonint06");
	public final static AddressProperty PROPERTY_COMMONINT07 = AddressProperty.make("commonint07");
	public final static AddressProperty PROPERTY_COMMONINT08 = AddressProperty.make("commonint08");
	public final static AddressProperty PROPERTY_COMMONINT09 = AddressProperty.make("commonint09");
	public final static AddressProperty PROPERTY_COMMONINT10 = AddressProperty.make("commonint10");
	
	public final static AddressProperty PROPERTY_COMMONBOOL01 = AddressProperty.make("commonbool01");
	public final static AddressProperty PROPERTY_COMMONBOOL02 = AddressProperty.make("commonbool02");
	public final static AddressProperty PROPERTY_COMMONBOOL03 = AddressProperty.make("commonbool03");
	public final static AddressProperty PROPERTY_COMMONBOOL04 = AddressProperty.make("commonbool04");
	public final static AddressProperty PROPERTY_COMMONBOOL05 = AddressProperty.make("commonbool05");
	public final static AddressProperty PROPERTY_COMMONBOOL06 = AddressProperty.make("commonbool06");
	public final static AddressProperty PROPERTY_COMMONBOOL07 = AddressProperty.make("commonbool07");
	public final static AddressProperty PROPERTY_COMMONBOOL08 = AddressProperty.make("commonbool08");
	public final static AddressProperty PROPERTY_COMMONBOOL09 = AddressProperty.make("commonbool09");
	public final static AddressProperty PROPERTY_COMMONBOOL10 = AddressProperty.make("commonbool10");
	
	public final static AddressProperty PROPERTY_COMMONDATE01 = AddressProperty.make("commondate01");
	public final static AddressProperty PROPERTY_COMMONDATE02 = AddressProperty.make("commondate02");
	public final static AddressProperty PROPERTY_COMMONDATE03 = AddressProperty.make("commondate03");
	public final static AddressProperty PROPERTY_COMMONDATE04 = AddressProperty.make("commondate04");
	public final static AddressProperty PROPERTY_COMMONDATE05 = AddressProperty.make("commondate05");
	public final static AddressProperty PROPERTY_COMMONDATE06 = AddressProperty.make("commondate06");
	public final static AddressProperty PROPERTY_COMMONDATE07 = AddressProperty.make("commondate07");
	public final static AddressProperty PROPERTY_COMMONDATE08 = AddressProperty.make("commondate08");
	public final static AddressProperty PROPERTY_COMMONDATE09 = AddressProperty.make("commondate09");
	public final static AddressProperty PROPERTY_COMMONDATE10 = AddressProperty.make("commondate10");
	
	public final static AddressProperty PROPERTY_COMMONMONEY01 = AddressProperty.make("commonmoney01");
	public final static AddressProperty PROPERTY_COMMONMONEY02 = AddressProperty.make("commonmoney02");
	public final static AddressProperty PROPERTY_COMMONMONEY03 = AddressProperty.make("commonmoney03");
	public final static AddressProperty PROPERTY_COMMONMONEY04 = AddressProperty.make("commonmoney04");
	public final static AddressProperty PROPERTY_COMMONMONEY05 = AddressProperty.make("commonmoney05");
	public final static AddressProperty PROPERTY_COMMONMONEY06 = AddressProperty.make("commonmoney06");
	public final static AddressProperty PROPERTY_COMMONMONEY07 = AddressProperty.make("commonmoney07");
	public final static AddressProperty PROPERTY_COMMONMONEY08 = AddressProperty.make("commonmoney08");
	public final static AddressProperty PROPERTY_COMMONMONEY09 = AddressProperty.make("commonmoney09");
	public final static AddressProperty PROPERTY_COMMONMONEY10 = AddressProperty.make("commonmoney10");
	
	
	public final static String GESCHLECHT_M = "m";
	public final static String GESCHLECHT_W = "w";
	public final static String GESCHLECHT_FIRMA = "f";
	public final static String GESCHLECHT_UNDEFINED = "-";

// 	//Constructor
// 	public Address(){
		
    // 		aliases= new HashMap();
    // 		//put the aliases, that the Address-Object supports into the hashmap, one after one
    // 		aliases.put("m",this.PROPERTY_E_MAIL_DIENST);
    // 		aliases.put("l",this.PROPERTY_LABEL);
    // 		aliases.put("u",this.PROPERTY_HOMEPAGE_DIENST);
    // 		aliases.put("t",this.PROPERTY_TELEFON_DIENST);
    
    // 	}

    // check methods

	
	/**
	 * Diese Methode testet, ob die Daten des Adressobjekts eine vollst�ndige
	 * Adresse darstellen.
	 * 
	 * @return true, gdw. die Adressangabe hinreichend vollst�ndig ist.
	 */
	public boolean isComplete();


    /**
     * Fills this address with all fields of the supplied source address. The id-field will not be modified.
     */
    public void fill(Address sourceAddress);
	
	
	/*
	 * Diese Methode �berprft Email-Eintr�ge auf Syntaktische Korrektheit
	 * bevor sie abgespeichert werden.
	 */	
	public boolean checkEmailDienstlich();
	
	public boolean checkEmailPrivat();
	
	/*
	 * Diese Methode entfernt alle f�hrenden und abschliessenden Leerzeichen 
	 * aus Adressfeldern vom Typ String
	 */
	public void trimAddressData();
	
	
	/*
	 * Diese Methode �berprft Telefonnumern-Eintr�ge auf Syntaktische Korrektheit
	 * bevor sie abgespeichert werden.
	 */
	
	public boolean checkPhonenumbers();
	
	

    // generic methods for setting and retrieving properties

	public Object getStandardData(String id);

    public Object getStandardData(AddressProperty prop);
	
	/**
	 * Diese Methode liefert Standarddaten zur bergebenen Identifikation.
	 * 
	 * @param id Identifikator, vergleich hier zu auch
	 *  {@link Database#getExtendedFields() Database.getExtendedFields()}.
	 * @param newValue die neuen Standarddaten.
	 */
	public void setStandardData(String id, Object newValue);
	
	public void setStandardData(AddressProperty prop, Object newValue);
        

	/**
	 * this method is necessary for the implemented interface Entity
	 */
	public Object getAttribute(String id);
	

	/**
	 * this method is necessary for the implemented interface Entity
	 * @param id
	 * @param newValue
	 * @throws ContactDBException
	 */
	public void setAttribute(String id, Object newValue) throws EntityException;



    // getXXX, setXXX methods
	
	public final static AddressProperty PROPERTY_ID = AddressProperty.make("id", AddressProperty.SEARCH);
	/**
	 * Diese Methode liefert die ID der Adresse. Diese entspricht der AdrNr
	 */
    public int getId();	

    /**
     * Sets the id of the object. The Id is the same as the adrNr
     */
    public void setId(int newid);
    

	/**
	 * 
	 * Returns the adrNr  (this is the same as the id).
	 * 
	 * @return int
	 */
	public int getAdrNr();


	/**
	 * Sets the adrNr (this is the same as the id).
	 * @param adrNr The adrNr to set
	 */
	public void setAdrNr(int adrNr);



// TODO: dependency to the User class
//
// 	public static final AddressProperty PROPERTY_ASSOCIATED_USER = AddressProperty.make("associatedUser");
	
//     /**
// 	 * Der Benutzer, der diesem Addressobjekt zugeordnet ist.
// 	 * Wenn keine Zuordnung besteht: <code>null</code>
// 	 *
// 	 */
// 	public User getAssociatedUser();
	
// 	/**
// 	 * Der Benutzer, der diesem Addressobjekt zugeordnet ist.
// 	 * Wenn keine Zuordnung besteht: <code>null</code>
// 	 */
// 	public void setAssociatedUser(User user);

	

	public static final AddressProperty PROPERTY_ASSOCIATED_USER_ID = AddressProperty.make("associatedUserID", AddressProperty.INTERNAL);

	/**
	 * Die ID des Benutzers, der diesem Addressobjekt zugeordnet ist.
	 * Wenn keine Zuordnung besteht: <code>null</code>
	 *
	 */
	public Integer getAssociatedUserID();
	
	/**
	 * Die ID des Benutzers, der diesem Addressobjekt zugeordnet ist.
	 * Wenn keine Zuordnung besteht: <code>0</code>
	 *
	 */
	public void setAssociatedUserID(Integer userID);   
	


	public final static AddressProperty PROPERTY_ABTEILUNG = AddressProperty.make("abteilung");
	
	/**
	 * @return Abteilung
	 */
	public String getAbteilung();

	/**
	 * @param abteilung die neue Abteilung
	 */
	public void setAbteilung(String abteilung);




	public final static AddressProperty PROPERTY_LETTERADDRESS = AddressProperty.make("letterAddress", AddressProperty.REPLACE);
	/**
	 * returns the addresslabel for a letter
	 * @return String
	 * 
	 */
    public String getLetterAddress();

	public void setLetterAddress(String text);
		


	public final static AddressProperty PROPERTY_AKAD_TITEL = AddressProperty.make("akadTitel");
	
	/**
	 * Returns the akadTitel.
	 * 
	 * @return String
	 */
	public String getAkadTitel();

	/**
	 * Sets the akadTitel.
	 * @param akadTitel The akadTitel to set
	 */
	public void setAkadTitel(String akadTitel);
	


	public final static AddressProperty PROPERTY_AENDERUNGSDATUM = AddressProperty.make("aenderungsdatum", AddressProperty.NONE);
	
	/**
	 * Returns the update date.
	 * 
	 * @return Date
	 */
	public Date getAenderungsdatum();

	/**
	 * Sets the update date.
	 * @param �nderungsdatum The update date to set
	 */
	public void setAenderungsdatum(Date aenderungsdatum);



	public final static AddressProperty PROPERTY_ANREDE = AddressProperty.make("anrede", AddressProperty.REPLACE);	
	/**
	 * Returns the anrede.
	 * 
	 * @return String
	 */
	public String getAnrede();

	/**
	 * Sets the anrede.
	 * @param anrede The anrede to set
	 */
	public void setAnrede(String anrede);


	
	public final static AddressProperty PROPERTY_BANK_CODE = AddressProperty.make("bankCode", AddressProperty.NONE);
	
	/**
	 * @return BLZ
	 */
	public String getBankCode();

	/**
	 * @param bankCode die neue BLZ
	 */
	public void setBankCode(String bankCode);




	public final static AddressProperty PROPERTY_BANK_KONTO = AddressProperty.make("bankKonto", AddressProperty.NONE);	
	/**
	 * @return Kontonummer
	 */
	public String getBankKonto();
	
	/**
	 * @param bankKonto das neue Konto
	 */
	public void setBankKonto(String bankKonto);




	public final static AddressProperty PROPERTY_BANK_NAME = AddressProperty.make("bankName", AddressProperty.NONE);
	
	/**
	 * @return Bankname
	 */
	public String getBankName();

	/**
	 * @param bankName der neue Bankname
	 */
	public void setBankName(String bankName);




	public final static AddressProperty PROPERTY_BUNDESLAND = AddressProperty.make("bundesland");

	/**
	 * Returns the bundesland.
	 * @return String
	 */
	public String getBundesland();

	/**
	 * Sets the bundesland.
	 * @param bundesland The bundesland to set
	 */
	public void setBundesland(String bundesland);



	public final static AddressProperty PROPERTY_BUNDESLAND2 = AddressProperty.make("bundesland2", AddressProperty.NONE);
	
	/**
	 * Returns the bundesland2.
	 * @return String
	 */
	public String getBundesland2();
	
	/**
	 * Sets the bundesland2.
	 * @param bundesland The bundesland to set
	 */
	public void setBundesland2(String bundesland);

	

	public final static AddressProperty PROPERTY_BUNDESLAND3 = AddressProperty.make("bundesland3", AddressProperty.NONE);
	/**
	 * Returns the bundesland2.
	 * @return String
	 */
	public String getBundesland3();

	/**
	 * Sets the bundesland3.
	 * @param bundesland The bundesland to set
	 */
	public void setBundesland3(String bundesland);

	


	public final static AddressProperty PROPERTY_E_MAIL_DIENSTLICH  = AddressProperty.make("emailAdresseDienstlich");    

	/**
	 * Returns the emailAdresse.
	 * @return String
	 */
	public String getEmailAdresseDienstlich();
	
	/**
	 * Sets the emailAdresse.
	 * @param emailAdresse The emailAdresse to set
	 */
	public void setEmailAdresseDienstlich(String emailAdresse);
	



    // this was emailAdresse2
	public final static AddressProperty PROPERTY_E_MAIL_PRIVAT = AddressProperty.make("emailAdressePrivat", AddressProperty.NONE);

	/**
	 * Returns the emailAdresse.
	 * @return String
	 */
	public String getEmailAdressePrivat();

	/**
	 * Sets the private emailAdresse.
	 * @param emailAdresse The emailAdresse to set
	 */
	public void setEmailAdressePrivat(String emailAdresse);




	public final static AddressProperty PROPERTY_E_MAIL_WEITERE = AddressProperty.make("emailAdresseWeitere", AddressProperty.NONE);	

	/**
	 * Returns the email3.
	 * @return String
	 */
	public String getEmailAdresseWeitere();
	
	/**
	 * Sets the emailAdresse3.
	 * @param emailAdresse The emailAdresse to set
	 */
	public void setEmailAdresseWeitere(String emailAdresse);



    
	public final static AddressProperty PROPERTY_ERFASSUNGSDATUM = AddressProperty.make("erfassungsdatum", AddressProperty.NONE);		
	/**
	 * Returns the erfassungsdatum.
	 * @return Date
	 */
	public Date getErfassungsdatum();

	/**
	 * Sets the erfassungsdatum.
	 * @param erfassungsdatum The erfassungsdatum to set
	 */
	public void setErfassungsdatum(Date erfassungsdatum);


	public final static AddressProperty PROPERTY_FAX_DIENSTLICH = AddressProperty.make("faxDienstlich");	
	/**
	 * Returns the faxDienstlich.
	 * @return String
	 */
	public String getFaxDienstlich();

	/**
	 * Sets the faxDienstlich.
	 * @param faxDienstlich The faxDienstlich to set
	 */
	public void setFaxDienstlich(String faxDienstlich);



	public final static AddressProperty PROPERTY_FAX_PRIVAT = AddressProperty.make("faxPrivat", AddressProperty.REPLACE);
	
	/**
	 * Returns the faxPrivat.
	 * @return String
	 */
	public String getFaxPrivat();

	/**
	 * Sets the faxPrivat.
	 * @param faxPrivat The faxPrivat to set
	 */
	public void setFaxPrivat(String faxPrivat);


	
	public final static AddressProperty PROPERTY_FAX_WEITERES = AddressProperty.make("faxWeiteres", AddressProperty.NONE);
	/**
	 * Returns the fax3.
	 * @return String
	 */
	public String getFaxWeiteres();

	/**
	 * Sets the fax3.
	 * @param fax3 The fax3 to set
	 */
	public void setFaxWeiteres(String fax3);


	
	public final static AddressProperty PROPERTY_FOLLOWUP = AddressProperty.make("followup", AddressProperty.NONE);
	/**
	 * returns wether the address is in the follow-up (wiedervorlage)
	 * @return followup flag
	 * 
	 */
	public Boolean getFollowup();

	/**
	 * Sets the Followup flag
	 * @param Boolean
	 */	
	public void setFollowup(Boolean b);
	

	public final static AddressProperty PROPERTY_FOLLOWUPDATE = AddressProperty.make("followupDate", AddressProperty.NONE);	
	/**
	 * Returns the date for the 
	 * @return followupdate
	 * 
	 */	
	public Date getFollowupDate();

	/**
	 * sets the followup date
	 * @param date
	 */	
	public void setFollowupDate(Date date);



	public final static AddressProperty PROPERTY_FK_USER_FOLLOWUP = AddressProperty.make("fkUserFollowup", AddressProperty.INTERNAL);	
	/**
	 * Returns the pk of the user, who has the address in his follow-up
	 * @return fk_user_followup
	 * 
	 */	
	public int getFkUserFollowup();
	
	/**
	 * @param id
	 */	
	public void setFkUserFollowup(int id);


	
	public final static AddressProperty PROPERTY_GEBURTSDATUM = AddressProperty.make("geburtsdatum", AddressProperty.NONE);	
	/**
	 * @return Geburtsdatum
	 */
	public Date getGeburtsdatum();
	/**
	 * @param geburtsdatum das neue Geburtsdatum
	 */
	public void setGeburtsdatum(Date geburtsdatum);



	public final static AddressProperty PROPERTY_GEBURTSJAHR = AddressProperty.make("geburtsjahr", AddressProperty.NONE);	
	/**
	 * @return Geburtsjahr
	 */
	public int getGeburtsjahr();

	/**
	 * @param geburtsjahr das neue Geburtsjahr
	 */
	public void setGeburtsjahr(int geburtsjahr);

	


	public final static AddressProperty PROPERTY_GESCHLECHT = AddressProperty.make("geschlecht", AddressProperty.NONE);
	/**
	 * @return Geschlecht
	 */
	public String getGeschlecht();

	/**
	 * @param sex das Geschlechtskennzeichen
	 */
	public void setGeschlecht(String sex);




	
	public final static AddressProperty PROPERTY_HANDY_DIENSTLICH = AddressProperty.make("handyDienstlich");
	/**
	 * Returns the handyDienstlich.
	 * @return String
	 */
	public String getHandyDienstlich();

	/**
	 * Sets the handyDienstlich.
	 * @param handyDienstlich The handyDienstlich to set
	 */
	public void setHandyDienstlich(String handyDienstlich);



	
	public final static AddressProperty PROPERTY_HANDY_PRIVAT = AddressProperty.make("handyPrivat", AddressProperty.REPLACE);
	/**
	 * Returns the handyPrivat.
	 * @return String
	 */
	public String getHandyPrivat();

	/**
	 * Sets the handyPrivat.
	 * @param handyPrivat The handyPrivat to set
	 */
	public void setHandyPrivat(String handyPrivat);

	

	public final static AddressProperty PROPERTY_HANDY_WEITERES = AddressProperty.make("handyWeiteres", AddressProperty.NONE);
	/**
	 * Returns the handy3.
	 * @return String
	 */
	public String getHandyWeiteres();
	
	/**
	 * Sets the handy3.
	 * @param handyPrivat The handyPrivat to set
	 */
	public void setHandyWeiteres(String handyPrivat);



	public final static AddressProperty PROPERTY_HAUS_NR = AddressProperty.make("hausNr");	
	/**
	 * Returns the hausNr2.
	 * @return String
	 */
	public String getHausNr();

	/**
	 * Sets the hausNr.
	 * @param hausNr The hausNr to set
	 */
	public void setHausNr(String hausNr);


	
	public final static AddressProperty PROPERTY_HAUS_NR2 = AddressProperty.make("hausNr2", AddressProperty.NONE);

	/**
	 * Returns the hausNr.
	 * @return String
	 */
	public String getHausNr2();

	/**
	 * Sets the hausNr2.
	 * @param hausNr The hausNr to set
	 */
	public void setHausNr2(String hausNr);
	
	


	public final static AddressProperty PROPERTY_HAUS_NR3 = AddressProperty.make("hausNr3", AddressProperty.NONE);	
	/**
	 * Returns the hausNr.
	 * @return String
	 */
	public String getHausNr3();

	/**
	 * Sets the hausNr3.
	 * @param hausNr The hausNr to set
	 */
	public void setHausNr3(String hausNr);



	public final static AddressProperty PROPERTY_HERRN_FRAU = AddressProperty.make("herrnFrau", AddressProperty.NONE);	
	/**
	 * Returns the herrnFrau.
	 * @return String
	 */
	public String getHerrnFrau();

	/**
	 * Sets the herrnFrau.
	 * @param herrnFrau The herrnFrau to set
	 */
	public void setHerrnFrau(String herrnFrau);


	
	public final static AddressProperty PROPERTY_HOMEPAGE_DIENSTLICH = AddressProperty.make("homepageDienstlich");
	/**
	 * Returns the homepageURL.
	 * @return String
	 */
	public String getHomepageDienstlich();

    /**
	 * Sets the homepageURL.
	 * @param homepageURL The homepageURL to set
	 */
	public void setHomepageDienstlich(String homepageURL);


	
	public final static AddressProperty PROPERTY_HOMEPAGE_PRIVAT = AddressProperty.make("homepagePrivat", AddressProperty.NONE);
	/**
	 * Returns the homepageURL.
	 * @return String
	 */
	public String getHomepagePrivat();

	/**
	 * Sets the homepageURL.
	 * @param homepageURL The homepageURL to set
	 */
	public void setHomepagePrivat(String homepageURL);


	public final static AddressProperty PROPERTY_HOMEPAGE_WEITERE = AddressProperty.make("homepageWeitere", AddressProperty.NONE);
    /**
	 * Returns the homepageURL.
	 * @return String
	 */
	public String getHomepageWeitere();

	/**
	 * Sets the homepageURL.
	 * @param homepageURL The homepageURL to set
	 */
	public void setHomepageWeitere(String homepageURL);



	
	public final static AddressProperty PROPERTY_INSTITUTION = AddressProperty.make("institution");
	/**
	 * Returns the institution.
	 * @return String
	 */
	public String getInstitution();

	/**
	 * Sets the institution.
	 * @param institution The institution to set
	 */
	public void setInstitution(String institution);


	public final static AddressProperty PROPERTY_INSTITUTION_WEITERE = AddressProperty.make("institutionWeitere", AddressProperty.NONE);	
	/**
	 * @return Institution2
	 */
	public String getInstitutionWeitere();

	/**
	 * @param institution2 ds zusaenzliche Institutionsfeld
	 */
	public void setInstitutionWeitere(String institution2);



	public final static AddressProperty PROPERTY_LAND = AddressProperty.make("land");	
	/**
	 * Returns the country.
	 * @return String
	 */
	public String getLand();

	/**
	 * Sets the country.
	 * @param land The land to set
	 */
	public void setLand(String land);

	
	public final static AddressProperty PROPERTY_LAND2 = AddressProperty.make("land2", AddressProperty.NONE);
	/**
	 * Returns the land2.
	 * @return String
	 */
	public String getLand2();

	/**
	 * Sets the land2.
	 * @param land The land to set
	 */
	public void setLand2(String land);
	


	
	public final static AddressProperty PROPERTY_LAND3 = AddressProperty.make("land3", AddressProperty.NONE);
	/**
	 * Returns the land3.
	 * @return String
	 */
	public String getLand3();

	/**
	 * Sets the land3.
	 * @param land The land to set
	 */
	public void setLand3(String land);
	

	public final static AddressProperty PROPERTY_LKZ = AddressProperty.make("lkz", AddressProperty.REPLACE);	
	/**
	 * Returns the lkz.
	 * @return String
	 */
	public String getLkz();

	/**
	 * Sets the lkz.
	 * @param lkz The lkz to set
	 */
	public void setLkz(String lkz);



	public final static AddressProperty PROPERTY_LKZ2 = AddressProperty.make("lkz2", AddressProperty.NONE);
	/**
	 * Returns the lkz2.
	 * @return String
	 */
	public String getLkz2();

	/**
	 * Sets the lkz2.
	 * @param lkz The lkz to set
	 */
	public void setLkz2(String lkz);


	
	public final static AddressProperty PROPERTY_LKZ3 = AddressProperty.make("lkz3", AddressProperty.NONE);
	/**
	 * Returns the lkz3.
	 * @return String
	 */
	public String getLkz3();

	/**
	 * Sets the lkz3.
	 * @param lkz The lkz to set
	 */
	public void setLkz3(String lkz);



	public final static AddressProperty PROPERTY_LETTER_ADDRESS_AUTO = AddressProperty.make("letterAddressAuto", AddressProperty.NONE);
	/**
	 * returns the flag, that indicates, if the addresslabel should be generated
	 * @return Boolean
	 * 
	 */
	public Boolean getLetterAddressAuto();

	/**
	 * sets the letter_auto flag. is set TRUE, if the letteraddress should be generated by default
	 * @param bool
	 * 
	 */
	public void setLetterAddressAuto(Boolean bool);



	public final static AddressProperty PROPERTY_LETTERSALUTATION = AddressProperty.make("letterSalutation", AddressProperty.REPLACE);
	/**
	 * returns the salutation for a letter
	 * @return String
	 * 
	 */	
	public String getLetterSalutation();

	/**
	 * sets the salutation for a letter. This has only affect, if the letterSalutationAuto flag is false
	 * @return String
	 * 
	 */	
	public void setLetterSalutation(String letterSalutation);



	public final static AddressProperty PROPERTY_NACHNAME = AddressProperty.make("nachname");	
	/**
	 * Returns the nachname.
	 * @return String
	 */
	public String getNachname();

	/**
	 * Sets the nachname.
	 * @param nachname The nachname to set
	 */
	public void setNachname(String nachname);


	


	public final static AddressProperty PROPERTY_NAMENSZUSATZ = AddressProperty.make("namenszusatz");
	/**
	 * Returns the namenszusatz.
	 * @return String
	 */
	public String getNamenszusatz();

	/**
	 * Sets the namenszusatz.
	 * @param namenszusatz The namenszusatz to set
	 */
	public void setNamenszusatz(String namenszusatz);
	


	public final static AddressProperty PROPERTY_NOTIZ = AddressProperty.make("notiz", AddressProperty.NONE);	
	/**
	 * @return Notiz
	 */
	public String getNotiz();

	/**
	 * @param notiz die neue Notiz
	 */
	public void setNotiz(String notiz);


	
	public final static AddressProperty PROPERTY_ORT = AddressProperty.make("ort");
	/**
	 * Returns the ort.
	 * @return String
	 */
	public String getOrt();

	/**
	 * Sets the ort.
	 * @param ort The ort to set
	 */
	public void setOrt(String ort);


	
	public final static AddressProperty PROPERTY_ORT2 = AddressProperty.make("ort2", AddressProperty.NONE);
	/**
	 * Returns the ort2.
	 * @return String
	 */
	public String getOrt2();

	/**
	 * Sets the ort2.
	 * @param ort The ort to set
	 */
	public void setOrt2(String ort);

	
	public final static AddressProperty PROPERTY_ORT3 = AddressProperty.make("ort3", AddressProperty.NONE);
	/**
	 * Returns the ort3.
	 * @return String
	 */
	public String getOrt3();

	/**
	 * Sets the ort3.
	 * @param ort The ort to set
	 */
	public void setOrt3(String ort);

	
	public final static AddressProperty PROPERTY_PICTURE = AddressProperty.make("picture", AddressProperty.NONE);
	/**
	 * 
	 * returns the picture as byte[]
	 * @return byte[]    
	 */
	
	public byte[] getPicture();
	/**
	 * 
	 * sets the picture as byte[]
	 */
	
	public void setPicture(byte[] picture);

	
	public final static AddressProperty PROPERTY_PICTUREEXPRESSION = AddressProperty.make("pictureExpression", AddressProperty.NONE);
	/**
	 * gets the pics name as String
	 * useful to check mime-type
	 * @return String
	 * 
	 */
	public String getPictureExpression();

	/**
	 * sets the filename of the picture
	 * useful to check mime-type
	 * @param name
	 * 
	 */
	
	public void setPictureExpression(String name);


	public final static AddressProperty PROPERTY_PLZ = AddressProperty.make("plz");	
	/**
	 * Returns the plz.
	 * @return int
	 */
	public String getPlz();
	
	/**
	 * Sets the plz.
	 * @param plz The plz to set
	 */
	public void setPlz(String plz);

	
	public final static AddressProperty PROPERTY_PLZ2 = AddressProperty.make("plz2", AddressProperty.NONE);
	/**
	 * Returns the plz2.
	 * @return int
	 */
	public String getPlz2();

	/**
	 * Sets the plz.
	 * @param plz The plz to set
	 */
	public void setPlz2(String plz);


	
	
	public final static AddressProperty PROPERTY_PLZ3 = AddressProperty.make("plz3", AddressProperty.NONE);
	/**
	 * Sets the plz.
	 * @param plz The plz to set
	 */
	public void setPlz3(String plz);

	/**
	 * Returns the plz3.
	 * @return int
	 */
	public String getPlz3();



	
	public final static AddressProperty PROPERTY_PLZ_POSTFACH= AddressProperty.make("plzPostfach");
	/**
	 * Returns the plzPostfach.
	 * @return int
	 */
	public String getPlzPostfach();
	
	/**
	 * Sets the plzPostfach.
	 * @param plzPostfach The plzPostfach to set
	 */
	public void setPlzPostfach(String plzPostfach);


	
	public final static AddressProperty PROPERTY_PLZ_POSTFACH2= AddressProperty.make("plzPostfach2", AddressProperty.NONE);
	/**
	 * Returns the plzPostfach2.
	 * @return int
	 */
	public String getPlzPostfach2();

	/**
	 * Sets the plzPostfach2.
	 * @param plzPostfach The plzPostfach to set
	 */
	public void setPlzPostfach2(String plzPostfach);



	
	public final static AddressProperty PROPERTY_PLZ_POSTFACH3= AddressProperty.make("plzPostfach3", AddressProperty.NONE);
	/**
	 * Returns the plzPostfach3.
	 * @return int
	 */
	public String getPlzPostfach3();

 	/**
	 * Sets the plzPostfach3.
	 * @param plzPostfach The plzPostfach to set
	 */
	public void setPlzPostfach3(String plzPostfach);
	



	public final static AddressProperty PROPERTY_POSITION = AddressProperty.make("position");
	/**
	 * @return Position
	 */
	public String getPosition();

	/**
	 * @param position die neue Position
	 */
	public void setPosition(String position);


	
	public final static AddressProperty PROPERTY_POSTFACH_NR = AddressProperty.make("postfachNr");
	/**
	 * Returns the postfachNr.
	 * @return String
	 */
	public String getPostfachNr();

	/**
	 * Sets the postfachNr.
	 * @param postfachNr The postfachNr to set
	 */
	public void setPostfachNr(String postfachNr);


	
	public final static AddressProperty PROPERTY_POSTFACH_NR2 = AddressProperty.make("postfachNr2", AddressProperty.NONE);
	/**
	 * Returns the postfachNr2.
	 * @return String
	 */
	public String getPostfachNr2();

	/**
	 * Sets the postfachNr2.
	 * @param postfachNr The postfachNr to set
	 */
	public void setPostfachNr2(String postfachNr);


	
	public final static AddressProperty PROPERTY_POSTFACH_NR3 = AddressProperty.make("postfachNr3", AddressProperty.NONE);
	/**
	 * Returns the postfachNr2.
	 * @return String
	 */
	public String getPostfachNr3();

	/**
	 * Sets the postfachNr3.
	 * @param postfachNr The postfachNr to set
	 */
	public void setPostfachNr3(String postfachNr);



	public final static AddressProperty PROPERTY_LETTER_SALUTATION_AUTO = AddressProperty.make("letterSalutationAuto", AddressProperty.NONE);
	/**Returns the flag LetterSalutation_auto
	 * @return Boolean
	 * 
	 */	
	public Boolean getLetterSalutationAuto();
	/**
	 * Sets the flag salutation_auto 
	 * @param b
	 * 
	 */
	public void setLetterSalutationAuto(Boolean bool);



	public final static AddressProperty PROPERTY_SHORT_ADDRESS_LABEL = AddressProperty.make("shortAddressLabel", AddressProperty.NONE);
    /**
     * returns the shortAddressLabel
     */
	public String getShortAddressLabel();

    /**
     * sets the shortAddressLabel
     */
	public void setShortAddressLabel(String label);

    /**
     * returns the shortAddressLabel
     */
	public String getLabel();	


	public final static AddressProperty PROPERTY_BLOCKEDFORCONSIGNMENT = AddressProperty.make("blockedForConsignment", AddressProperty.NONE);
	/**
	 * @return blockedForConsignment
	 */
	public Boolean getBlockedForConsignment();

	/**
	 * @param spitzname der neue Spitzname
	 */
	public void setBlockedForConsignment(Boolean blockedForConsignment);
	
	
	public final static AddressProperty PROPERTY_SPITZNAME = AddressProperty.make("spitzname", AddressProperty.NONE);
	/**
	 * @return Spitzname
	 */
	public String getSpitzname();

	/**
	 * @param spitzname der neue Spitzname
	 */
	public void setSpitzname(String spitzname);
	
	

	
	public final static AddressProperty PROPERTY_STRASSE = AddressProperty.make("strasse");
	/**
	 * Returns the stra�e.
	 * @return String
	 */
	public String getStrasse();

	/**
	 * Sets the straaen.
	 * @param straaen The straaen to set
	 */
	public void setStrasse(String strasse);


	
	public final static AddressProperty PROPERTY_STRASSE2 = AddressProperty.make("strasse2", AddressProperty.NONE);
	/**
	 * Returns the stra�e2.
	 * @return String
	 */
	public String getStrasse2();

	/**
	 * Sets the straaen2.
	 * @param straaen The straaen to set
	 */
	public void setStrasse2(String strasse);


	
	public final static AddressProperty PROPERTY_STRASSE3 = AddressProperty.make("strasse3", AddressProperty.NONE);
	/**
	 * Returns the stra�e2.
	 * @return String
	 */
	public String getStrasse3();

	/**
	 * Sets the straaen3.
	 * @param straaen The straaen to set
	 */
	public void setStrasse3(String strasse);
	

	public final static AddressProperty PROPERTY_TELEFON_DIENSTLICH = AddressProperty.make("telefonDienstlich");	
	/**
	 * Returns the telefonDienstlich.
	 * @return String
	 */
	public String getTelefonDienstlich();

	/**
	 * Sets the telefonDienstlich.
	 * @param telefonDienstlich The telefonDienstlich to set
	 */
	public void setTelefonDienstlich(String telefonDienstlich);


	
	public final static AddressProperty PROPERTY_TELEFON_PRIVAT = AddressProperty.make("telefonPrivat", AddressProperty.REPLACE);
	/**
	 * Returns the telefonPrivat.
	 * @return String
	 */
	public String getTelefonPrivat();

	/**
	 * Sets the telefonPrivat.
	 * @param telefonPrivat The telefonPrivat to set
	 */
	public void setTelefonPrivat(String telefonPrivat);

	
	public final static AddressProperty PROPERTY_TELEFON_WEITERES = AddressProperty.make("telefonWeiteres", AddressProperty.NONE);
	/**
	 * Returns the telefon3.
	 * @return String
	 */
	public String getTelefonWeiteres();

	/**
	 * Sets the telefon3.
	 * @param telefon3 The telefon3 to set
	 */
	public void setTelefonWeiteres(String telefon3);


	public final static AddressProperty PROPERTY_TITEL = AddressProperty.make("titel");	
	/**
	 * Returns the titel.
	 * @return String
	 */
	public String getTitel();

	/**
	 * Sets the titel.
	 * @param titel The titel to set
	 */
	public void setTitel(String titel);



	
	public final static AddressProperty PROPERTY_ADDRESSZUSATZ= AddressProperty.make("addressZusatz", AddressProperty.NONE);
	/**
	 * Returns suffix to the address
	 * @return String
	 * 
	 */
	public String getAddressZusatz();

	/**
	 * Sets the suffix2.
	 * @param suffix The suffix to set
	 * 
	 */
	public void setAddressZusatz(String suffix);



	
	public final static AddressProperty PROPERTY_ADDRESSZUSATZ2= AddressProperty.make("addressZusatz2", AddressProperty.NONE);
	/**
	 * Returns suffix2 to the address
	 * @return String
	 * 
	 */
	public String getAddressZusatz2();

	/**
	 * Sets the suffix2.
	 * @param suffix The suffix to set
	 * 
	 */
	public void setAddressZusatz2(String suffix);



	
	public final static AddressProperty PROPERTY_ADDRESSZUSATZ3= AddressProperty.make("addressZusatz3", AddressProperty.NONE);
    /** 
	 */
	public String getAddressZusatz3();

	/**
	 * Sets the suffix3.
	 * @param suffix The suffix to set
	 * 
	 */
	public void setAddressZusatz3(String suffix);
	

	
	public final static AddressProperty PROPERTY_VORNAME = AddressProperty.make("vorname");
	/**
	 * Returns the vorname.
	 * @return String
	 */
	public String getVorname();
	
	/**
	 * Sets the vorname.
	 * @param vorname The vorname to set
	 */
	public void setVorname(String vorname);




	public final static AddressProperty PROPERTY_WEITERE_VORNAMEN = AddressProperty.make("weitereVornamen", AddressProperty.NONE);
	/**
	 * @return weitere Vornamen
	 */
	public String getWeitereVornamen();
	
	/**
	 * @param weitereVornamen die neuen weiteren Vornamen
	 */
	public void setWeitereVornamen(String weitereVornamen);		
    
}
