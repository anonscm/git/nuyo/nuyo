package de.tarent.groupware;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.tarent.commons.datahandling.entity.EntityProperty;

/**
 * <code>SubCategoryProperty</code> provides type-safe keys for the subcategory data
 * fields and allows indexed access to the property names as well as i18n.
 * 
 * <p>
 * Additionally <code>SubCategoryProperty</code> can be in zero to many lists of
 * properties. This allows one to receive only a set of properties that are
 * interesting for a certain purpose (e.g. all properties which can be searched
 * for).
 * </p>
 * 
 * @author Nils Neumaier, tarent GmbH Bonn
 */
public class SubCategoryProperty extends EntityProperty {
	
	static
	{
		// HACK: This hack provokes the SubCategory class to be initialized
		// whenever the SubCategoryProperty class is touched. This behavior
		// is needed because SubCategory' initialization causes static lists
		// in SubCategoryProperty to be filled. Without SubCategory being initialized
		// those lists would be empty. 
		SubCategory.class.getName();
	}


	private static ResourceBundle currentResourceBundle = PropertyResourceBundle
			.getBundle("de.tarent.groupware.categoryproperty");

	/**
	 * Java class denoting the type of the subcategory property.
	 */
	private Class dataType;

	/**
	 * Creates an <code>SubCategoryProperty</code> which belongs only to the given
	 * lists.
	 * 
	 * @param name
	 * @param fields bitfield denoting the list affiliation
	 * @return
	 */
	static SubCategoryProperty make(String name) {
		return new SubCategoryProperty(name);
	}

	
	private Class retrieveDataType() {
		
		if (key.startsWith("common"))
		{
			// Strips "common" and the two trailing digits from the key.
			String type = key.substring(6, key.length() - 2).intern();
			
			if (type == "text")
				return String.class;
			else if (type == "date")
				return Date.class;
			else if (type == "bool")
				return Boolean.class;
		    else if (type == "int")
		    	return Integer.class;
		    else if (type == "money")
		    	return Float.class;
		    else
		    	throw new IllegalStateException("Unknown datatype for common address property: " + key);
		}
		try {
			PropertyDescriptor descs[] = Introspector
					.getBeanInfo(SubCategory.class).getPropertyDescriptors();

			for (int i = 0; i < descs.length; i++) {
				if (descs[i].getName().equals(key))
					return descs[i].getPropertyType();
			}

			throw new IllegalStateException(
					"SubCategory property is not a bean property of SubCategory class");
		} catch (IntrospectionException ie) {
			throw new IllegalStateException(
					"Unable to introspect SubCategory class");
		}

	}

	private SubCategoryProperty(String name) {
		super(name);
		dataType = retrieveDataType();
	}

	
	/**
	 * Returns a human-readable localized label of the property.
	 * 
	 * <p>
	 * In case a property cannot be found the method returns the property's key
	 * quoted in exclamation marks ("!").
	 * </p>
	 * 
	 * <p>
	 * In case a property's label is empty the method returns the property's key
	 * quoted in question marks ("?").
	 * </p>
	 * 
	 */
	public String getLabel() {
		try {
			String res = currentResourceBundle.getString(SubCategory.propertyNamePrefix + key);

			if (res.trim().length() == 0)
				return "?" + key + "?";

			return res;

		} catch (MissingResourceException e) {
			String bundleSuffix = "";
			if (!"".equals(currentResourceBundle.getLocale().getLanguage())) {
				bundleSuffix += "_"
						+ currentResourceBundle.getLocale().getLanguage();
			}

			if (!"".equals(currentResourceBundle.getLocale().getCountry())) {
				bundleSuffix += "_"
						+ currentResourceBundle.getLocale().getCountry();
			}
			return "!" + key + "!";
		}
	}

	/**
	 * Returns the property'y datatype.
	 * 
	 * @return
	 */
	public Class getDataType() {
		return dataType;
	}

}
