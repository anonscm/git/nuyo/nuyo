package de.tarent.groupware.utils;

import java.util.Map;

import de.tarent.dblayer.sql.statement.AbstractStatement;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Update;


/**
 * @author kleinw
 *
 *	@deprecated
 *
 *	User InsertUpdate from package de.tarent.dblayer.
 *
 */
public class InsertUpdate {

    private AbstractStatement _statement;
    private Map _attributes;
    
    public InsertUpdate(AbstractStatement statement) {
        _statement = statement;
    }

    public InsertUpdate(Map attributes, AbstractStatement statement) {
        _attributes = attributes;
        _statement = statement;
    }
    
    public void add(String column, Object value) {
        if (_statement instanceof Insert)
            ((Insert)_statement).insert(column, value);
        else if (_statement instanceof Update)
            ((Update)_statement).update(column, value);
    }

    public void addAttribute(String column, Object key) {
        if (_attributes != null) {
            if (_attributes.containsKey(key)) {
                this.add(column, _attributes.get(key));
            }
        }
    }
    
}
