package de.tarent.groupware.category;


import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.Category;
import de.tarent.groupware.SubCategory;

/**
 * 
 * @author Nils Neumaier, tarent GmbH
 *
 */
 
 
public class CategoryDBMapping extends AbstractDBMapping {

    public static final String STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES_AND_RIGHTS = "stmtSelectAllForUserWithSubcategoriesAndRights";
   
    public static final String PARAM_SELECTING_USER_ID = "selectingUserId";
    public static final String PARAM_SELECTING_ADDRESS_PKS = "addressPks";
    
    protected static final int COMMON_CATEGORY_FIELDS = 128;
    protected static final int SUBCATEGORY_FIELDS = 256;
    protected static final int RIGHT_FIELDS = 512;
    
 	DBContext initialDBC;
    public CategoryDBMapping(DBContext dbc) {
        super(dbc);
        initialDBC = dbc;
    }

	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDBMapping#getTargetTable()
	 */
	public String getTargetTable() {
		return TcategoryDB.getTableName();
	}
	
	
	
	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDBMapping#configureMapping()
	 */
	public void configureMapping() {		
		

        addField(TcategoryDB.PK_PKCATEGORY, 	Category.PROPERTY_ID, PRIMARY_KEY_FIELDS | COMMON_CATEGORY_FIELDS);
        addField(TcategoryDB.CATEGORYNAME, Category.PROPERTY_CATEGORYNAME, COMMON_CATEGORY_FIELDS);
        addField(TcategoryDB.DESCRIPTION, Category.PROPERTY_DESCRIPTION, COMMON_CATEGORY_FIELDS);
        addField(TcategoryDB.FKCATEGORYTYPE, Category.PROPERTY_CATEGORYTYPE, COMMON_CATEGORY_FIELDS);
        addField(TcategoryDB.ISVIRTUAL, Category.PROPERTY_ISVIRTUAL, COMMON_CATEGORY_FIELDS);
        addField(TcategoryDB.ISPRIVATE, Category.PROPERTY_ISPRIVATE, COMMON_CATEGORY_FIELDS);
        addField(TcategoryDB.FKUSERPRIVATE, Category.PROPERTY_USERPRIVATE, COMMON_CATEGORY_FIELDS);
        addFields(new SubCategoryDBMapping(initialDBC), SubCategory.entityName, COMMON_FIELDS, COMMON_CATEGORY_FIELDS);
        addField("v_user_folderright.auth_read", Category.PROPERTY_RIGHTREAD, RIGHT_FIELDS);
        addField("v_user_folderright.auth_edit", Category.PROPERTY_RIGHTEDIT, RIGHT_FIELDS);
        addField("v_user_folderright.auth_add", Category.PROPERTY_RIGHTADD, RIGHT_FIELDS);
        addField("v_user_folderright.auth_remove", Category.PROPERTY_RIGHTREMOVE, RIGHT_FIELDS);
        addField("v_user_folderright.auth_addsub", Category.PROPERTY_RIGHTADDSUBCAT, RIGHT_FIELDS);
        addField("v_user_folderright.auth_removesub", Category.PROPERTY_RIGHTREMOVESUBCAT, RIGHT_FIELDS);
        addField("v_user_folderright.auth_structure", Category.PROPERTY_RIGHTSTRUCTURE, RIGHT_FIELDS);
        addField("v_user_folderright.auth_grant", Category.PROPERTY_RIGHTGRANT, RIGHT_FIELDS);
        
        addQuery(STMT_SELECT_ONE, 
                 createBasicSelectOne()
                 .join(TcDBContext.getSchemaName()+"v_user_folder", TcategoryDB.PK_PKCATEGORY, "v_user_folder.fk_folder")
                 .whereAndEq("v_user_folder.userid", new ParamValue(PARAM_SELECTING_USER_ID)),
                 COMMON_CATEGORY_FIELDS);
        
        
        addQuery(STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES_AND_RIGHTS, 
                 getStandardSelect(),
                 COMMON_CATEGORY_FIELDS | SUBCATEGORY_FIELDS | RIGHT_FIELDS);
        	}

	private Select getStandardSelect() {
		return createBasicSelectAll()
		 .join(TcDBContext.getSchemaName()+"v_user_folderright", TcategoryDB.PK_PKCATEGORY, "v_user_folderright.fk_folder")
		 .joinLeftOuter(TsubcategoryDB.getTableName(),"v_user_folderright.fk_folder", TsubcategoryDB.FKCATEGORY)
		 .whereAndEq("v_user_folderright.fk_user", new ParamValue(PARAM_SELECTING_USER_ID));
	}
}