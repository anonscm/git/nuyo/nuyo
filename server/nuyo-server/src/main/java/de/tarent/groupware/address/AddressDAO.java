/**
 * 
 */
package de.tarent.groupware.address;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.ListFilterImpl;
import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.persistence.AbstractDAO;
import de.tarent.dblayer.sql.ParamValueList;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.Address;

/**
 *
 * @author Sebastian Mancke, tarent GmbH
 */
public class AddressDAO extends AbstractDAO {

	private static AddressDAO instance = null;
	
	AddressDBMapping mapping = new AddressDBMapping(TcDBContext.getDefaultContext());
	AddressEntityFactory entityFactory =  AddressEntityFactory.getInstance();

    protected AddressDAO() {
        super();
        setDbMapping(mapping);
        setEntityFactory(entityFactory);
    }
	
    /**
     * Returns the instance
     */
    public static synchronized AddressDAO getInstance() {
		if(instance == null){
			instance = new AddressDAO();
		}
		return instance;
	}


    /**
     * overridden implementation to switch between different strategies
     */
    protected List getEntityList(DBContext dbc, Select select, ListFilter listFilterParams) throws SQLException {
        boolean useCursorImplementation = false;
        if (SQL.isMSSQL(dbc)) {
            for (Iterator iter = listFilterParams.getFilterList().iterator(); iter.hasNext();) {
                Object o = iter.next();
                if (o instanceof PrimaryKeyList && ((PrimaryKeyList)o).size() > 1000) {
                    useCursorImplementation = true;
                    break;
                }
            }
        }
        return  useCursorImplementation ?
            getEntityList_CursorImplementation(dbc, select, listFilterParams)
            : getEntityList_LimitImplementation(dbc, select, listFilterParams);
    }
    
     /**
      * Returns one Address by
      * selected by the given primary key pk.
      * @param dbc The database context.
      * @param pk Primary key. 
      * @return One Address
      */
    public Address getAddressByPk(DBContext dbc, Integer pk) throws SQLException {
        return (Address)getEntityByIdFilter(dbc, mapping.STMT_SELECT_ONE, Address.PROPERTY_ID.getKey(), pk);
    }
    
    
    /**
     * List all addresses visible for the given user, matching the supplied search criteria.
     * @param dbc The database context
     * @param userId the integer user id of the calling user
     * @param listFilterParams a filter tree for search, order and paging parameters
     * @param disjunctionSubcategories the search subfolder is as integer objects, conncatenated by OR. A returned address has to be in ANY of those subcategories.
     * @param conjunctionSubcategories the search subfolder is as integer objects, conncatenated by AND. A returned address has to be in ALL of those subcategories.
     * @param negatedSubcategories the search subfolder is as integer objects, conncatenated by AND. A returned address may not be in ANY of those subcategories.
     */
    public List getAddressesByFilter(DBContext dbc, Integer userId, ListFilter listFilterParams, 
                                     List disjunctionSubcategories, 
                                     List conjunctionSubcategories, 
                                     List negatedSubcategories) throws SQLException {

        Select select = createAddressCategorySelect(dbc, userId, disjunctionSubcategories, conjunctionSubcategories, negatedSubcategories);
        return getEntityList(dbc, select, listFilterParams);
    }
    
    /**
     * Returns a list of the Address Primary Keys, following the same sheme as {@see getAddressesByFilter}     
     */
    public PrimaryKeyList getAddressPks(DBContext dbc, Integer userId, ListFilter listFilterParams, 
                                        List disjunctionSubcategories, 
                                        List conjunctionSubcategories, 
                                        List negatedSubcategories) throws SQLException {
        
        Select select = createAddressCategorySelect(dbc, userId, disjunctionSubcategories, conjunctionSubcategories, negatedSubcategories);
        return getPrimaryKeyList(dbc, select, listFilterParams);
    }

    private Select createAddressCategorySelect(DBContext dbc, Integer userId, List disjunctionSubcategories, List conjunctionSubcategories, List negatedSubcategories) throws SQLException {
        ParamValueList paramList = new ParamValueList();
        Select selectClone = (Select)mapping.getQuery(mapping.STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES).clone();
        selectClone.getParams(paramList);
        paramList.setAttribute(mapping.PARAM_SELECTING_USER_ID, userId);

        // build the intersection of (the disjunction categories UNION all the categories, the user is allowed to see) and append them
        // user subcats UNION requested subcats
        Select subcatSelect = SQL.SelectDistinct(dbc)
            .add(TsubcategoryDB.PK_PKSUBCATEGORY, Integer.class)
            .from(TsubcategoryDB.getTableName())
            .join(TcDBContext.getSchemaName()+"v_user_folder", "v_user_folder.fk_folder", TsubcategoryDB.FKCATEGORY)
            .whereAndEq("v_user_folder.userid", userId);
        
        if (disjunctionSubcategories != null && disjunctionSubcategories.size() > 0) {
            subcatSelect.whereAnd(Expr.in(TsubcategoryDB.PK_PKSUBCATEGORY, disjunctionSubcategories));
        }
        List disjunctionUserSubcategories = subcatSelect.getList(dbc);
        
        selectClone.whereAnd(Expr.exists(new SubSelect(getSubcategorySubSelect(dbc,disjunctionUserSubcategories).select(TaddressDB.PK_PKADDRESS))));

        // append conjunctoin categories
        if (conjunctionSubcategories != null && conjunctionSubcategories.size() > 0) {
            // count of selectd subcategories for address == count of subcategories requested
            // this relys on duplicate-free TaddresssubcategoryDB
            selectClone.whereAndEq(new SubSelect(getSubcategorySubSelect(dbc,conjunctionSubcategories).select("COUNT(*)")).clauseToString(dbc), new Integer(conjunctionSubcategories.size()));
        }
        
        // append negated categories
        if (negatedSubcategories != null && negatedSubcategories.size() > 0) {
            selectClone.whereAnd(Expr.notExists(new SubSelect(getSubcategorySubSelect(dbc,negatedSubcategories).select(TaddressDB.PK_PKADDRESS))));
        }
        return(selectClone);
    }



    /**
     * Returns a sub select of subcategories for the adddress.
     */
    private Select getSubcategorySubSelect(DBContext dbc, List subcategoryPKs) {
        return SQL.Select(dbc)
            .from(TaddresssubcategoryDB.getTableName())
            .whereAndEq(TaddressDB.PK_PKADDRESS, new RawClause(TaddresssubcategoryDB.FKADDRESS))
            .whereAnd(Expr.in(TaddresssubcategoryDB.FKSUBCATEGORY, subcategoryPKs));        
    }

    /**
     * List all addresses for a address action (aka "Versandauftrag".
     * 
     * @param actionId the pk of the address action
     * @param actionType a String constant for an address action-channel (column "note") (aka "Versandkanal")
     * @return The list of Address objects in this channel
     */
    public List getAddressesForAddressAction(DBContext dbc, Integer actionId, String actionType) throws SQLException {

//BUG 'Parameter #1 not set' with this code:
//        
//      ParamList paramList = new ParamList()
//      .add(TaddressactionDB.FKACTION, actionId)
//      .add(TaddressactionDB.NOTE, actionType);
//      
//      return getEntityList(dbc, mapping.STMT_SELECT_ALL_FROM_TADDRESSACTION, paramList);

        ParamValueList paramValueList = new ParamValueList();
        Select selectClone = (Select)mapping.getQuery( mapping.STMT_SELECT_ALL_FROM_TADDRESSACTION).clone();
        selectClone.getParams(paramValueList);
        
        paramValueList.setAttribute(mapping.PARAM_SELECTING_ACTION_ID, actionId);
        paramValueList.setAttribute(mapping.PARAM_SELECTING_ACTION_NOTE, actionType);

        return getEntityList(dbc, selectClone, new ListFilterImpl());
    }

    /**
     * Retrieves the total count of addresses the user can access.
     * 
     * @param dbc
     * @return
     * @throws SQLException
     */
    public Integer getTotalCount(DBContext dbc) throws SQLException {
    	// As the statement instance may be used by multiple threads setting its DBContext to
    	// retrieve the result values as string is not a good idea. The workaround is to directly
    	// call via the DB class
    	return new Integer(DB.fetchFirstCellAsString(dbc, mapping.getQuery(mapping.STMT_SELECT_COUNT).clauseToString(dbc)));    
    }
    
	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDAO#setEntityKeys(de.tarent.dblayer.engine.InsertKeys, java.lang.Object)
	 */
	public void setEntityKeys(InsertKeys keys, Object entity) {
        ((Address)entity).setAdrNr(keys.getPk());
	}
    
}
