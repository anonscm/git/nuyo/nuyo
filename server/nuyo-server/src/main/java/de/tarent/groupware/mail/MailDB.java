/* $Id: MailDB.java,v 1.8 2007/06/15 15:58:28 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tarent.contact.bean.TmailstoreDB;
import de.tarent.contact.bean.TmailstorecategoryDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.groupware.mail.MailException.Database;
import de.tarent.groupware.mail.MailException.Security;
import de.tarent.octopus.server.Context;

/**
 * <strong>Logging:</strong><br>
 * <code>de.tarent.contact.octopus.logging.Lg</code><br>
 * SQL-Statements werden als <code>debug</code> und
 * SQLExceptions als <code>error</code> geloggt.<br><br>
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.8 $
 */
public class MailDB {
	/**
	 * Gibt eine List mit den Keys aller eMail-Stores eines
	 * Benutzers als Integer zur�ck.
	 * @param userId
	 */
    
    public final static List getStores(Integer userId) throws Database, SQLException {
        List response = null;
        
        if(userId != null)
            response =
	        	de.tarent.dblayer.sql.SQL.Select(TcDBContext.getDefaultContext())
	        		.from(TmailstoreDB.getTableName())
	        		.add(TmailstoreDB.PK_PK, Integer.class)							//	[0]
	        		.where(Expr.equal(TmailstoreDB.FKUSER, userId))
	        		.getList(TcDBContext.getDefaultContext());
        
        else
            response = Collections.EMPTY_LIST;
        
        return response;
    }
    
//	public final static List getStores(String module, Integer userid) throws Database {
//		Result tcResult = null;
//		try {
//			Select select = SQL.Select()
//				.from(TmailstoreDB.getTableName())
//				.select(TmailstoreDB.PK_PK)
//				.and(TmailstoreDB.FKUSER, Where.EXP_EQUAL, userid);
//			Lg.SQL(module).debug(select);
//			tcResult = DB.result(module, select.toString());
//			ResultSet resultSet = tcResult.resultSet();
//			List result = new ArrayList();
//			while (resultSet.next()) {
//				result.add(new Integer(resultSet.getInt(1)));
//			}
//			return result;
//		} catch (SQLException e) {
//			Lg.SQL(module).error("getStores", e);
//			throw new Database(e);
//		} finally {
//			if (tcResult != null)
//				tcResult.close();
//		}
//	}

	/**
	 * Holt alle n�tigen Informationen zu einem eMail-Store.
	 * Gibt Map mit den Keys 'MailStore.SERVER_*' zur�ck.
	 * @param userid
	 * @param storeid
	 */
	public final static Map getStoreInfo(Integer userid, Integer storeid) throws Database {
		Result tcResult = null;
		try {
			de.tarent.dblayer.sql.statement.Select select = de.tarent.dblayer.sql.SQL.Select(TcDBContext.getDefaultContext())
				.from(TmailstoreDB.getTableName())
				.select(TmailstoreDB.PK_PK)
				.select(TmailstoreDB.NAME)
				.select(TmailstoreDB.PROTOCOL)
				.select(TmailstoreDB.SERVER)
				.select(TmailstoreDB.PORT)
				.select(TmailstoreDB.USERNAME)
				.select(TmailstoreDB.PASSWORD)
				.select(TmailstoreDB.CATEGORYINBOX)
				.select(TmailstoreDB.CATEGORYSENT)
				.select(TmailstoreDB.CATEGORYTRASH)
				.where(de.tarent.dblayer.sql.clause.Where.and(
			        Expr.equal(TmailstoreDB.FKUSER, userid),
			        Expr.equal(TmailstoreDB.PK_PK, storeid)
			        )
		        );
			
			tcResult = DB.result(TcDBContext.getDefaultContext(), select);
			ResultSet resultSet = tcResult.resultSet();
			Map<String, Object> result = null;
			if (resultSet.next()) {
				result = new HashMap<String, Object>();
				result.put(MailConstants.SERVER_ID, new Integer(resultSet.getInt(1)));
				result.put(MailConstants.SERVER_DISPLAYNAME, resultSet.getObject(2));
				result.put(MailConstants.SERVER_PROTOCOL, resultSet.getString(3));
				result.put(MailConstants.SERVER_NAME, resultSet.getString(4));
				result.put(MailConstants.SERVER_PORT, resultSet.getObject(5));
				result.put(MailConstants.SERVER_USERNAME, resultSet.getString(6));
				result.put(MailConstants.SERVER_PASSWORD, resultSet.getObject(7));
				result.put(MailConstants.SERVER_FOLDERINBOX, resultSet.getObject(8));
				result.put(MailConstants.SERVER_FOLDERSENT, resultSet.getObject(9));
				result.put(MailConstants.SERVER_FOLDERTRASH, resultSet.getObject(10));
			}
			return result;
		} catch (SQLException e) {
			Lg.SQL(Context.getActive().getModuleName()).error("getStoreInfo", e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}

	/**
	 * Holt eine Liste von Foldern zu bei einem Bestimmten
	 * eMail-Store angezeigt werden sollen.
	 * 
	 * @param storeid des eMail-Stores
	 */
	public final static Map getStoreFolders(String module, Integer storeid) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(TcDBContext.getDefaultContext())
				.from(TmailstorecategoryDB.getTableName())
				.select(TmailstorecategoryDB.PK_PK)
				.select(TmailstorecategoryDB.NAME)
				.where(Expr.equal(TmailstorecategoryDB.FKMAILSTORE, storeid));
			tcResult = DB.result(TcDBContext.getDefaultContext(), select);
			ResultSet resultSet = tcResult.resultSet();
			Map result = new HashMap();
			while (resultSet.next()) {
				result.put(new Integer(resultSet.getInt(1)), resultSet.getString(2));
			}
			return result;
		} catch (SQLException e) {
			Lg.SQL(module).error("getStoreFolders", e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}

	/**
	 * Speichert eine Liste von Foldern die zu einem bestimmten
	 * eMailStore. ID der Map muss ein Integer sein.
	 * 
	 * @param storeid des eMail-Stores
	 * @param folders
	 */
	public final static void setStoreFolders(String module, Integer userid, Integer storeid, List folders) throws Security, Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(TcDBContext.getDefaultContext())
				.from(TmailstoreDB.getTableName())
				.select(TmailstoreDB.PK_PK)
				.where(Where.and(
			        Expr.equal(TmailstoreDB.PK_PK, storeid),
			        Expr.equal(TmailstoreDB.FKUSER, userid))
		        );
			tcResult = DB.result(TcDBContext.getDefaultContext(), select);
			ResultSet resultSet = tcResult.resultSet();
			if (!resultSet.next()) {
				throw new Security("no permission");
			}
		} catch (SQLException e) {
			Lg.SQL(module).error("setStoreFolders", e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
		
		try {
			Delete delete = SQL.Delete(TcDBContext.getDefaultContext())
				.from(TmailstorecategoryDB.getTableName())
				.where(Expr.equal(TmailstorecategoryDB.FKMAILSTORE, storeid));
			DB.update(TcDBContext.getDefaultContext(), delete.toString());
		} catch (SQLException e) {
			Lg.SQL(module).error("setStoreFolders", e);
			throw new Database(e);
		} finally {
			// nothing
		}
		
		try {
			for (int i = 0; i < folders.size(); i++) {
				Insert insert = SQL.Insert(TcDBContext.getDefaultContext())
					.table(TmailstorecategoryDB.getTableName())
					.insert(TmailstorecategoryDB.FKMAILSTORE, storeid)
					.insert(TmailstorecategoryDB.NAME, folders.get(i));
				DB.update(TcDBContext.getDefaultContext(), insert);
			}
		} catch (SQLException e) {
			Lg.SQL(module).error("setStoreFolders", e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}

	/**
	 * Erstellt einen neuen eMail-Store.
	 * 
	 * @param userid
	 * @param name
	 * @param protocol
	 * @param server
	 * @param port
	 * @param username
	 * @param password
	 * @return neue Store ID
	 */
	public final static Integer createStore(
			String module,
			Integer userid,
			String name,
			String protocol,
			String server,
			Integer port,
			String username,
			String password,
			String inboxFolder,
			String sentFolder,
			String trashFolder) throws Database {
		
		try {
			Insert insert = SQL.Insert(TcDBContext.getDefaultContext())
				.table(TmailstoreDB.getTableName())
				.insert(TmailstoreDB.NAME, name)
				.insert(TmailstoreDB.PROTOCOL, protocol)
				.insert(TmailstoreDB.SERVER, server)
				.insert(TmailstoreDB.PORT, port)
				.insert(TmailstoreDB.FKUSER, userid)
				.insert(TmailstoreDB.USERNAME, username)
				.insert(TmailstoreDB.PASSWORD, password);
			if (inboxFolder != null)
				insert.insert(TmailstoreDB.CATEGORYINBOX, inboxFolder);
			if (sentFolder != null)
				insert.insert(TmailstoreDB.CATEGORYSENT, sentFolder);
			if (trashFolder != null)
				insert.insert(TmailstoreDB.CATEGORYTRASH, trashFolder);
			DB.update(TcDBContext.getDefaultContext(), insert.toString());
		} catch (SQLException e) {
			Lg.SQL(module).error("createStore", e);
			throw new Database(e);
		} finally {
			// nothing
		}
		
		Result tcResult = null;
		try {
			Select select = SQL.Select(TcDBContext.getDefaultContext())
				.from(TmailstoreDB.getTableName())
				.select("MAX(" + TmailstoreDB.PK_PK + ")")
				.where(Expr.equal(TmailstoreDB.FKUSER, userid));
			tcResult = DB.result(TcDBContext.getDefaultContext(), select.toString());
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				return new Integer(resultSet.getInt(1));
			}
			return null;
		} catch (SQLException e) {
			Lg.SQL(module).error("createStore", e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}

	public final static Integer updateStore(
			String module,
			Integer userid,
			Integer storeid,
			String name,
			String protocol,
			String server,
			Integer port,
			String username,
			String password,
			String inboxFolder,
			String sentFolder,
			String trashFolder) throws Database {
		
		try {
			Update update = SQL.Update(TcDBContext.getDefaultContext())
				.table(TmailstoreDB.getTableName())
				.update(TmailstoreDB.NAME, name)
				.update(TmailstoreDB.PROTOCOL, protocol)
				.update(TmailstoreDB.SERVER, server)
				.update(TmailstoreDB.PORT, port)
				.update(TmailstoreDB.USERNAME, username)
				.update(TmailstoreDB.PASSWORD, password)
				.where(Where.and(
				     Expr.equal(TmailstoreDB.PK_PK, storeid),
				     Expr.equal(TmailstoreDB.FKUSER, userid)
			    ));
			if (inboxFolder != null)
				update.update(TmailstoreDB.CATEGORYINBOX, inboxFolder);
			if (sentFolder != null)
				update.update(TmailstoreDB.CATEGORYSENT, sentFolder);
			if (trashFolder != null)
				update.update(TmailstoreDB.CATEGORYTRASH, trashFolder);
			DB.update(TcDBContext.getDefaultContext(), update);
			return storeid;
		} catch (SQLException e) {
			Lg.SQL(module).error("updateStore", e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}
	
	public final static void removeStore(String module, Integer userid, Integer storeid) throws Database {
		try {
			Delete delete = SQL.Delete(TcDBContext.getDefaultContext())
				.from(TmailstoreDB.getTableName())
				.where(Where.and(
				        Expr.equal(TmailstoreDB.PK_PK, storeid),
				        Expr.equal(TmailstoreDB.FKUSER, userid)
				        )
				);
			DB.update(TcDBContext.getDefaultContext(), delete);
		} catch (SQLException e) {
			Lg.SQL(module).error("removeStore", e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}
	
	public final static Integer getUserId(String module, String loginname) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(TcDBContext.getDefaultContext())
				.from(TuserDB.getTableName())
				.select(TuserDB.PK_PKUSER)
				.where(Expr.equal(TuserDB.LOGINNAME, loginname));
			tcResult = DB.result(TcDBContext.getDefaultContext(), select);
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				return new Integer(resultSet.getInt(1));
			}
			return null;
		} catch (SQLException e) {
			Lg.SQL(module).error("getUserId", e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}
}
