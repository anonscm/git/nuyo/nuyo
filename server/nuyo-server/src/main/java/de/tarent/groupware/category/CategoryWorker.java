package de.tarent.groupware.category;

import java.sql.SQLException;
import java.util.List;

import de.tarent.commons.datahandling.ListFilterImpl;
import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.server.OctopusContext;


/**
 * @author kleinw
 *
 *	Worker f�r Kategorien
 *
 */
public class CategoryWorker implements CategoryWorkerConstants {

	public static final String PARAM_CATEGORIES_FILTER = "categoriesFilter";
	    
    final static public String[] INPUT_GETCATEGORIES = {"categoryids"};
    final static public boolean[] MANDATORY_GETCATEGORIES = {false};
    final static public String OUTPUT_GETCATEGORIES = "categories";
    
    /**
     * 
     * @param all
     * @param categoryId
   * @return
     * @throws SQLException
     */
    final static public CategoryList getCategories(OctopusContext cntx, List categoryIds) throws SQLException, TcContentProzessException  {


		 ListFilterImpl filter = new ListFilterImpl();
		 filter.setSortField(TcategoryDB.CATEGORYNAME + ", " + TsubcategoryDB.CATEGORYNAME);
		 filter.setSortDirection(Order.ASC);
		 
		 filter.init(cntx, PARAM_CATEGORIES_FILTER);
		 filter.setUseLimit(false);
		 List list = CategoryDAO.getInstance().getCategoriesByFilter(TcDBContext.getDefaultContext(),  cntx.personalConfig().getUserID(), filter);
	     
////		 for (Iterator iter = list.iterator(); iter.hasNext();)
////			 System.out.println("Kategorie: " + ((Category) iter.next()).getCategoryName());
////		 
//		 
//		 //cntx.setContent(PARAM_COUNT, new Integer(filter.getCount()));
		 
	     return new CategoryList(list);
    }    

}
        
        
    
