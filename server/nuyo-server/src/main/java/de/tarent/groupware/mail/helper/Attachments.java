package de.tarent.groupware.mail.helper;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;
import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.logging.Log;

import de.tarent.commons.logging.LogFactory;
import de.tarent.contact.bean.TattachmentDB;
import de.tarent.contact.bean.TmessageattachmentDB;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.mail.AttachmentValues;
import de.tarent.groupware.mail.MailSendDB;
import de.tarent.groupware.mail.MailException.Database;

/**
 * This class is a collection of usefull methods to handle attachments.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public abstract class Attachments {
    
    private final static Log logger = LogFactory.getLog(Attachments.class);
    
    /** A list of valid storage types for attachments.*/
    public enum StorageType{ BLOB, FILE }

    /** A Map of mime types. It's used to identify a mime type of an attachment.*/
    private static final MimetypesFileTypeMap MIMETYPES_FILE_TYPE_MAP = new MimetypesFileTypeMap();

    /**
     * Every attachment will be saved as a file.
     * It's path will be stored within a DB.
     * This method returns a list of attachments ids have been stored.
     * These IDs can be used in order to retrieve the path information from a DB to each attachment.
     * 
     * <p>In case <code>attachmentNames</code> or <code>attachmentDatas</code> is
     * <code>null</code> or contains zero objects an empty list will be returned.</p>
     * 
     * @param module
     * @param tempPath
     * @param actionId is serial mail order id
     * @param scheduledJobID is a scheduled job id
     * @param attachmentNames
     * @param attachmentDatas 
     * @return a list of attachment IDs 
     * @throws IOException if failed to save any attachment to a file
     * @throws Database if failed to store any file path within a DB
     */
    public static List<Integer> toFilesystem(String module, String tempPath, Integer actionId, Integer scheduledJobID, List<String> attachmentNames, List<byte[]> attachmentDatas ) throws IOException, Database {
        if (attachmentNames == null || attachmentDatas == null)
        	return (List<Integer>) Collections.EMPTY_LIST;
    	
    	int size = attachmentNames.size();
        if(size == 0) {
            logger.debug("no attachments to save as a file");
        	return (List<Integer>) Collections.EMPTY_LIST;
        }
        
        List<Integer> attachmentsIdList = new ArrayList<Integer>();
        logger.debug("===== attachments -> filesystem...");
        for (int i = 0; i < size; i++)
        {
          String name = attachmentNames.get(i);
          byte[] data = attachmentDatas.get(i);
          String filePath = saveBytesToFile(tempPath, actionId, scheduledJobID, i + 1, name, data);
          logger.debug("------------");
          logger.debug("[attachment]: " + name + " (" + (data.length / 1000) + "kB)");
          logger.debug("[path]: " + filePath);
          Integer currentAttachmentID = MailSendDB.storeAttachmentAsFilePath( module, name, filePath );
          logger.debug("[ID in DB]: " + currentAttachmentID.toString());
          attachmentsIdList.add(currentAttachmentID);
        }
        return attachmentsIdList;
    }

    /** Prepare input streams to read attachments content.*/
    public static Map<String, DataSource> retrieveAttachments(DBContext context, String module, int messageid ) {
        Map<String, DataSource>  streams = new HashMap<String, DataSource>();
        Result tcResultAttach = null;
        try {
            Select selectAttach = getColumnsSelect( context )
            .join(TmessageattachmentDB.getTableName(), TmessageattachmentDB.FKATTACHMENT, TattachmentDB.PK_PK)
            .where(Expr.equal(TmessageattachmentDB.FKMESSAGE, new Integer(messageid)));
            tcResultAttach = DB.result(context, selectAttach);
            ResultSet resultSetAttach = tcResultAttach.resultSet();
            while (resultSetAttach.next()) {
                    AttachmentValues attchValues = new AttachmentValuesImpl(resultSetAttach);
                    // the blob is at index 3.
                    DataSource ds = ((StorageType.FILE == attchValues.getStorageType())
                            ? new FileDataSource(attchValues.getName(), attchValues.getFilePath())
                    		: new BlobDataSource(attchValues.getName(), resultSetAttach.getBlob(3)));
                    //Note: map.key = filePath (not 'name' in order to handle attachments with equal names)  
                    streams.put(attchValues.getFilePath(), ds); 
            }
        } catch (SQLException e) {
            logger.error("SQL error", e);
            throw new IllegalStateException("failed preloading attachments: " + e.getMessage(), e);
        } finally {
            if (tcResultAttach != null)
                tcResultAttach.close();
        }
        
        return (streams.isEmpty()) ? null : streams;
    }

    /**
     * Saves attachment's content (bytes) to a file in the specified temp folder.
     * 
     * @param tempPath to save an attachment
     * @param actionId is a post order ID of a current attachment
     * @param jobID is used to store the same attachments for all messages inside a given job
     * @param attachmentIndex is used for handling attachments with the same name but different content
     * @param name of an attachment file, i.e. 'foto.jpg'
     * @param data of a given attachment represented as an array of bytes 
     * @return absolute path to a file: 'tempPath/actionId_jobID_attachmentIndex_name'
     * @throws IOException if failed creating a file or writing bytes to a file
     */
    public static String saveBytesToFile( String tempPath, Integer actionId, Integer jobID, int attachmentIndex, String name, byte[] data ) throws IOException {
        if (tempPath == null) tempPath = System.getProperty("java.io.tmpdir");
        File tempDir = new File(tempPath);
        if(!tempDir.exists() && !tempDir.mkdirs()) throw new IOException("can't create temp dir: " + tempDir.getCanonicalPath());
        File file = new File(tempDir, 
                             (actionId == null ? "" : actionId.toString() + "_") 
                             + jobID.toString() + "_" +String.valueOf(attachmentIndex) + "_" + name);
        file.createNewFile();
        FileOutputStream fos = null;
        try {            
            fos = new FileOutputStream(file);
            fos.write( data );            
        } catch ( IOException e ) {
            logger.error("failed pushing bytes to a file: " + e.getMessage(), e);
            if(file.exists()) file.delete();
            throw e;
        } finally {
            if(fos != null){
                try {            
                    fos.close();
                }
                catch ( IOException e ) {}
            }
        }
        return file.getCanonicalPath();
    }

    /**
     * Retrieves values of a given attachment entry.
     * Expects only one row with an attachment entry of {@link de.tarent.contact.bean.TattachmentDB} schema instance.
     * That is the first row will be handled if several rows exist.
     * 
     * @param oneRowResultSet is a cursor with only one row.  
     * @return retrieved values
     * @throws SQLException if failed
     * @exception IllegalArgumentException if empty result set
     * @exception NullPointerException if invalid value for {@link StorageType}
     * 
     * @see AttachmentValues
     * @see AttachmentValuesImpl
     */
    public static AttachmentValues getValuesFrom( ResultSet oneRowResultSet ) throws SQLException {
        return new AttachmentValuesImpl(oneRowResultSet);
    }

    
    /** Returns {@link AttachmentValues} retrieved from the row with a given ID or NULL if no row found.
     * @throws Database exception if failed */
    public static AttachmentValues retrieveValuesOf(DBContext context, String module, Integer attachmentID ) throws Database {
        Result tcResult = null;
        try {
            Select select = getColumnsSelect( context )
                .where(Expr.equal(TattachmentDB.PK_PK, attachmentID));
            tcResult = DB.result(context, select.toString());
            ResultSet resultSet = tcResult.resultSet();
            return Attachments.getValuesFrom(resultSet);
        } catch (SQLException e) {
            logger.error("[retrieveAttachmentValues]: SQLException: " + e.getMessage(), e);
            throw new Database(e);
        } catch (IllegalArgumentException e){
            logger.error("[retrieveAttachmentValues]: result set is empty: attachment not found: id=" + attachmentID, e);
            return null;
        } catch (NullPointerException e){
            logger.error("[retrieveAttachmentValues]: unknown storage type: " + attachmentID, e);
            throw new Database(e);
        } finally {
            if (tcResult != null)
                tcResult.close();
        }
    }

    private static Select getColumnsSelect( DBContext context ) {
        //note: select at least the four columns (required for AttachmentValuesImpl)
        return SQL.Select(context)
            .from(TattachmentDB.getTableName())
            .select(TattachmentDB.FILENAME)
            .select(TattachmentDB.TEXTCONTENT)
            .select(TattachmentDB.FKATTACHMENTTYPE)
            .select(TattachmentDB.BINARYCONTENT);
    }
    
    /**
     * Implementation of DataSource to retrieve an attachment from a file system.
     * 
     * @see de.tarent.groupware.mail.MailSendSMTP#buildMessage()  
     */
    static class FileDataSource implements DataSource
    {
    	String name;
    	String filepath;
    	
    	FileDataSource(String name, String filepath)
    	{
    		this.name = name;
    		this.filepath = filepath;
    	}

		public String getContentType() {
			return MIMETYPES_FILE_TYPE_MAP.getContentType(name);
		}

		public InputStream getInputStream() throws IOException {
			// Java Mail seems to prefer fresh InputStreams ...
			return new BufferedInputStream(new FileInputStream(filepath));
		}

		public String getName() {
			return name;
		}

		public OutputStream getOutputStream() throws IOException {
			throw new UnsupportedOperationException("Not supported");
		}
    }
    
    /**
     * Implementation of DataSource to retrieve an attachment from a DB.
     * 
     * @see de.tarent.groupware.mail.MailSendSMTP#buildMessage()  
     */
    static class BlobDataSource implements DataSource
    {
    	String name;
    	byte buffer[];
    	int size;
    	
    	BlobDataSource(String name, Blob blob)
    	{
    		try
    		{
    		  size = (int) blob.length();
    		  buffer = new byte[size];
    		
    		  blob.getBinaryStream().read(buffer, 0, size);
    		}
    		catch (SQLException e)
    		{
    			throw new IllegalStateException("error accessing blob length", e);
    		}
    		catch (IOException e)
    		{
    			throw new IllegalStateException("error reading blob data", e);
    		}
    	}

		public String getContentType() {
			return MIMETYPES_FILE_TYPE_MAP.getContentType(name);
		}

		public InputStream getInputStream() throws IOException {
			// Java Mail seems to prefer fresh InputStreams ...
			return new BufferedInputStream(new ByteArrayInputStream(buffer));
		}

		public String getName() {
			return name;
		}

		public OutputStream getOutputStream() throws IOException {
			throw new UnsupportedOperationException("Not supported");
		}
    	
    }
}
