package de.tarent.groupware.utils;

import java.util.Map;

import de.tarent.dblayer.sql.statement.AbstractStatement;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Update;


/**
 * @author kleinw
 *
 *	Das Speichern / Updaten von Attributesmap
 *	wird hier vollzogen.
 *
 */
public class Attributes {

    private Map _attributes;
    private AbstractStatement _statement;
    
    public Attributes(Map attributes, AbstractStatement statement) {
        _attributes = attributes;
        _statement = statement;
    }
    
    public void add(String column, String key) {
        if (_attributes.containsKey(key)) {
            if (_statement instanceof Insert) {
                ((Insert)_statement).insert(column, _attributes.get(key));
            } else if (_statement instanceof Update) {
                ((Update)_statement).update(column, _attributes.get(key));
            }
        }
    } 
    
}
