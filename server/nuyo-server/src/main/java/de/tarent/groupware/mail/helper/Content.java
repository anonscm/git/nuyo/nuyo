/*
 * Created on 28.10.2004
 */
package de.tarent.groupware.mail.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;

public abstract class Content {
	public static void main(String[] args) {
		try {
			new Content("mail-folder/e4355f7b2daade8f") {
				protected void handleMessagePart(String string, String mimetype, String filename) throws IOException, MessagingException {
					System.out.println("handleMessagePart " + mimetype + "; " + filename);
				}
				
				protected void handleContentPart(String string, String mimetype, String filename) throws IOException, MessagingException {
					System.out.println("handleContentPart " + mimetype + "; " + filename);
				}
				
				protected void handleMessagePart(InputStream inputStream, String mimetype, String filename, Integer size) throws IOException, MessagingException {
					System.out.println("handleMessagePart " + mimetype + "; " + filename);
				}
				
				protected void handleContentPart(InputStream inputStream, String mimetype, String filename, Integer size) throws IOException, MessagingException {
					System.out.println("handleContentPart " + mimetype + "; " + filename);
				}
			}.parseMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected Message message;

	public Content(File file) throws IOException, MessagingException {
		this(new FileInputStream(file));
	}

	public Content(String file) throws IOException, MessagingException {
		this(new FileInputStream(file));
	}

	public Content(InputStream inputStream) throws IOException, MessagingException {
		this.message = new MimeMessage(null, inputStream);
	}

	public Content(Message message) throws IOException, MessagingException {
		this.message = message;
	}

	public Message getMessage() {
		return message;
	}

	public void parseMessage() throws Exception {
		System.err.println(message.getSubject());
		parsePart(message, null, null, null, -1);
	}

	protected void parseObject(Object object, String mimetype, String filename, Integer size, int level) throws Exception {
		for (int i = 0; i <= level; i++)
		System.out.print('\t');
		System.out.println(object.getClass().getName());
		
		if (object == null) {
			// nothing
		} else if (object instanceof Multipart) {
			parsePart((Multipart)object, mimetype, filename, size, level);
		} else if (object instanceof Part) {
			parsePart((Part)object, mimetype, filename, size, level);
		} else if (object instanceof String) {
			if (level <= 1)
				handleMessagePart((String)object, mimetype, filename);
			else
				handleContentPart((String)object, mimetype, filename);
		} else if (object instanceof InputStream) {
			if (level <= 1)
				handleMessagePart((InputStream)object, mimetype, filename, size);
			else
				handleContentPart((InputStream)object, mimetype, filename, size);
		}
	}

	protected void parsePart(Part part, String mimetype, String filename, Integer size, int level) throws Exception {
		String contenttype = MimeType.getContentType(part);
		mimetype = MimeType.getMimeType(contenttype);
		filename = MimeType.getFilename(contenttype);
		size = getSize(part);
		
		Object content = part.getContent();
		parseObject(content, mimetype, filename, size, level + 1);
		
//		if (content instanceof Part) {
//			InputStream inputStream = null;
//			try {
//				inputStream = ((Part)content).getInputStream();
//				parseObject(inputStream, mimetype, filename, size, level + 1);
//			} finally {
//				if (inputStream != null)
//					inputStream.close();
//			}
//		}
//		parseObject(content, mimetype, filename, size, level + 1);
	}

	protected void parsePart(Multipart part, String mimetype, String filename, Integer size, int level) throws Exception {
		for (int i = 0; i < part.getCount(); i++) {
			parsePart(part.getBodyPart(i), mimetype, filename, size, level);
		}
	}

	protected void handleMessagePart(String string, String mimetype, String filename) throws Exception {
	}

	protected void handleContentPart(String string, String mimetype, String filename) throws Exception {
	}

	protected void handleMessagePart(InputStream inputStream, String mimetype, String filename, Integer size) throws Exception {
	}

	protected void handleContentPart(InputStream inputStream, String mimetype, String filename, Integer size) throws Exception {
	}

	public static Integer getSize(Part part) throws IOException, MessagingException {
		InputStream inputStream = null;
		try {
			inputStream = part.getInputStream();
			return getSize(inputStream);
		} finally {
			if (inputStream != null)
				inputStream.close();
		}
	}
	

	public static Integer getSize(InputStream inputStream) throws IOException {
		int size = 0;
		int i;
		byte buffer[] = new byte[1024];
		while ((i = inputStream.read(buffer)) >= 0) {
			size += i;
		}
		return new Integer(size);
	}
}
