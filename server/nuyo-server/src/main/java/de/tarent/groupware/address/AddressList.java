package de.tarent.groupware.address;

import java.util.ArrayList;
import java.util.Collection;


/**
 * List wrapper for Addresses. This class is used to get a typemapping for axis working.
 */
public class AddressList extends ArrayList {
    
    public AddressList() {
    }
    
    public AddressList(Collection c) {
        super(c);
    }
}