package de.tarent.groupware.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.clause.Clause;
import de.tarent.dblayer.sql.clause.Limit;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.octopus.server.OctopusContext;

/**
 * Liefern von Daten auf eine generische Weise.
 * 
 * @author Sebastian Mancke
 */
public class GenericSelector {
    // Content Parameter:
    public static final String CURRENT_WHERE_LIST_FIELD = "genericSelector_whereList";
    public static final String CURRENT_SELECT_FIELD = "genericSelector_select";
    public static final String CURRENT_ENTITY_DESCRIPTION_FIELD = "genericSelector_entityDescription";
    public static final String SELECT_STRING = "genericSelector_selectString";
    public static final String RESULT = "genericSelector_result";

    // Request oder Content Parameter:
    public static final String ORDER_BY = "orderBy";
    /** Felder im Namespace des Client, die zur�ck geliefert werden sollen*/
    public static final String PROJECTION_FIELDS = "columnNames";
    // Input fields for action addLimitAndOffset
    public static final String SELECT_PARAM_LIMIT = "limit";
    public static final String SELECT_PARAM_OFFSET = "offset";

    /** Liste mit Filtern in umgekeht polnischer Notation
     *  z.B. name, "Mancke", =, vorname, "Sebastian", =, NOT, AND
     *  bedeutet:  (name = "Mancke") AND (NOT (vorname = "Sebastian"))
     * Vorsicht: wird nur st�ckweise nach Bedarf implementiert!
     */
    public static final String FILTER_LIST = "filter";
    public static final String RESULT_ENTITY_MAPS = "entityMaps";
    public static final String RESULT_ENTITY_TABLE = "entityTable";
    public static final String RESULT_ENTITY = "entity";
    public static final String RESULT_COUNT_FUNCTION = "entityCount";

    public String[] INPUT_fireSelectString = new String[] { "CONTENT:"+SELECT_STRING };
    public String OUTPUT_fireSelectString = "CONTENT:"+RESULT;
    public Result fireSelectString(OctopusContext cntx, String selectString) 
        throws SQLException {
        try {
            return DB.result(TcDBContext.getDefaultContext(), selectString);
        } catch (SQLException e) {
			Lg.SQL(cntx.getModuleName()).error(e.getMessage(), e);
			throw e;
	    }
    }

    public String[] INPUT_fireSelect = new String[] { "CONTENT:"+CURRENT_SELECT_FIELD,
                                                      "CONTENT:"+CURRENT_WHERE_LIST_FIELD,
                                                      "CONTENT:"+ORDER_BY};
    public boolean[] MANDATORY_fireSelect = new boolean[] { true, false, false };
    public String OUTPUT_fireSelect = RESULT;

    public Result fireSelect(OctopusContext cntx, Select select, WhereList where, Order orderBy)
        throws SQLException {
        if (where != null && where.size() > 0)
            select.whereAnd(where);
        
        if (orderBy != null)
            select.orderBy(orderBy);
        
        try {
            return select.executeSelect(TcDBContext.getDefaultContext());
        } catch (SQLException e) {
			Lg.SQL(cntx.getModuleName()).error(e.getMessage(), e);
			throw e;
	    }
    }
    
    public String[] INPUT_fireCountFunction = { "CONTENT:"+CURRENT_SELECT_FIELD,
            "CONTENT:"+CURRENT_WHERE_LIST_FIELD,
            "CONTENT:"+ORDER_BY};
    public boolean[] MANDATORY_fireCountFunction = { true, false, false };
    public String OUTPUT_fireCountFunction = "CONTENT:"+GenericSelector.RESULT_COUNT_FUNCTION;

    public int fireCountFunction(OctopusContext cntx, Select select, WhereList where, Order orderBy) throws SQLException {
    	select.clearColumnSelection();
		select.selectAs("count(*)", "count");
		
	        if (where != null && where.size() > 0)
	            select.where(where);
	        
	        Result result;
	        try {
	             result = select.executeSelect(TcDBContext.getDefaultContext());
	        } catch (SQLException e) {
				Lg.SQL(cntx.getModuleName()).error(e.getMessage(), e);
				throw e;
		    }
	        
	        ResultSet resultSet = result.resultSet();
	        if (resultSet.next())
	        	return resultSet.getInt("count");	        
	        else
	        	return 0;
    }

    public String[] INPUT_addFilterExpressions = new String[] {"CONTENT:"+CURRENT_WHERE_LIST_FIELD,
                                                               FILTER_LIST,
                                                               "CONTENT:"+CURRENT_ENTITY_DESCRIPTION_FIELD};
    public boolean[] MANDATORY_addFilterExpressions = new boolean[] { false, false, true };
    public String OUTPUT_addFilterExpressions = CURRENT_WHERE_LIST_FIELD;

    public WhereList addFilterExpressions(OctopusContext cntx, WhereList where, List filter, EntityDescription entityDescription)
        throws SQLException, FilterCreater.FilterCreaterParsingException {
        
        if (filter == null || filter.size() == 0)
            return where;

        if (where == null)
            where = new WhereList();
        
        FilterCreater f  = new FilterCreater(entityDescription);
        Clause c = f.getWhereList(filter);
        where.addAnd(c);

        return where;

    }



    public String[] INPUT_addMappedProjectionFields = new String[] { "CONTENT:"+CURRENT_SELECT_FIELD,
                                                               PROJECTION_FIELDS,
                                                               "CONTENT:"+CURRENT_ENTITY_DESCRIPTION_FIELD };
    public boolean[] MANDATORY_addMappedProjectionFields = new boolean[] { true, false, true };
    public String OUTPUT_addMappedProjectionFields = PROJECTION_FIELDS;
    public List addMappedProjectionFields(OctopusContext cntx, Select select, List projectionFields, EntityDescription entityDescription)
        throws SQLException {

        if (projectionFields == null || projectionFields.size() == 0)
            projectionFields = Arrays.asList(entityDescription.getPropertyNames());
        
        for (Iterator iter = projectionFields.iterator(); iter.hasNext();) {
            String propertyName = (String)iter.next();
            select.selectAs(entityDescription.getDBKeyByProperty(propertyName), propertyName);
        }
        return projectionFields;
    }
    
    /**
     * Clears mapping, filter, order and where list conditions from CONTENT
     * If more fields have to be cleared to reset the GenericSelector for your
     * purposes feel free to add these fields.
     */    
    public String[] INPUT_reset = new String[] { "CONTENT:"+PROJECTION_FIELDS, "CONTENT:"+FILTER_LIST, "CONTENT:"+CURRENT_WHERE_LIST_FIELD };
	public boolean[] MANDATORY_reset = new boolean[] { false, false, false };
	public String OUTPUT_reset = null;
	public void reset(OctopusContext oc, List projectionFields, List filter, WhereList whereList) {
		Object nullObject = null;
		if (projectionFields != null && projectionFields.size() > 0)
			oc.setContent(PROJECTION_FIELDS, nullObject);
		if (filter != null && filter.size() > 0)
			oc.setContent(FILTER_LIST, nullObject);
		if (whereList != null && whereList.size() > 0)
			oc.setContent(CURRENT_WHERE_LIST_FIELD, nullObject);		
		oc.setContent(ORDER_BY, nullObject);		
	}
    
    public String[] INPUT_addLimitAndOffset = { "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD,
    											GenericSelector.SELECT_PARAM_LIMIT,
    											GenericSelector.SELECT_PARAM_OFFSET };
    public boolean[] MANDATORY_addLimitAndOffset = { true, false, false };
    public String OUTPUT_addLimitAndOffset = "CONTENT:"+GenericSelector.CURRENT_SELECT_FIELD;

    public Select addLimitAndOffset(Select select, Integer limit, Integer offset) {
		if (limit != null && offset != null)
			select.Limit(new Limit(limit, offset));
		return select;
    }
    
    public String[] INPUT_getEntityMaps = new String[] { "CONTENT:"+RESULT,
                                                         PROJECTION_FIELDS};
    public boolean[] MANDATORY_getEntityMaps = new boolean[] { true, true};
    public String OUTPUT_getEntityMaps = RESULT_ENTITY_MAPS;
    public List getEntityMaps(OctopusContext cntx, Result result, List<String> projectionFields)
        throws SQLException {
        
        String[] projectionFieldArray = projectionFields.toArray(new String[projectionFields.size()]);

        ResultSet resultSet = result.resultSet();
        try {
            List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>(resultSet.getFetchSize());
            while (resultSet.next()) {
                Map<String, Object> row = new HashMap<String, Object>(projectionFieldArray.length);       
                for (int i=0; i<projectionFieldArray.length; i++) {
                    row.put(projectionFieldArray[i], resultSet.getObject(projectionFieldArray[i]));
                }
                resultList.add(row);
            }
            return resultList;
		} finally {
			if (result != null)
				result.closeAll();
		}
    }

    
    public String[] INPUT_getEntityTable = new String[] { "CONTENT:"+RESULT,
                                                         PROJECTION_FIELDS};
    public boolean[] MANDATORY_getEntityTable = new boolean[] { true, true};
    public String OUTPUT_getEntityTable = RESULT_ENTITY_TABLE;
    public List getEntityTable(OctopusContext cntx, Result result, List projectionFields)
        throws SQLException {
        
        ResultSet resultSet = result.resultSet();
        int cols = projectionFields.size();
        try {
            List<List<Object>> resultList = new ArrayList<List<Object>>(resultSet.getFetchSize());
            while (resultSet.next()) {
                List<Object> row = new ArrayList<Object>(cols);
                for (int i=0; i<cols; i++) {
                    row.add(resultSet.getObject((String)projectionFields.get(i)));
                }
                resultList.add(row);
            }
            return resultList;
		} finally {
			if (result != null)
				result.closeAll();
		}
    }


    public String[] INPUT_getFirstEntity = new String[] { "CONTENT:"+RESULT,
                                                         PROJECTION_FIELDS};
    public boolean[] MANDATORY_getFirstEntity = new boolean[] { true, true};
    public String OUTPUT_getFirstEntity = RESULT_ENTITY;
    public Map getFirstEntity(OctopusContext cntx, Result result, List projectionFields)
        throws SQLException {

        ResultSet resultSet = result.resultSet();
        try {
            if (resultSet.next()) {
                Map<Object, Object> row = new HashMap<Object, Object>(projectionFields.size()); 
                for (int i=0; i<projectionFields.size(); i++) {
                    row.put(projectionFields.get(i), resultSet.getObject((String)projectionFields.get(i)));
                }
                return row;
            }
		} finally {
			if (result != null)
				result.closeAll();
		}
        return null;
    }
}