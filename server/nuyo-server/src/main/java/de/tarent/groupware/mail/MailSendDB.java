/* $Id: MailSendDB.java,v 1.20 2007/06/15 15:58:28 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;

import org.apache.commons.logging.Log;

import de.tarent.commons.logging.LogFactory;
import de.tarent.contact.bean.TattachmentDB;
import de.tarent.contact.bean.TjobDB;
import de.tarent.contact.bean.TmessageDB;
import de.tarent.contact.bean.TmessageattachmentDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.groupware.mail.MailException.Database;
import de.tarent.groupware.mail.helper.Attachments;
import de.tarent.groupware.mail.helper.Attachments.StorageType;
import de.tarent.groupware.utils.ListProperties;
import de.tarent.octopus.server.Context;
import de.tarent.octopus.server.OctopusContext;

/**
 * <strong>Logging:</strong><br>
 * <code>de.tarent.contact.octopus.logging.Lg</code><br>
 * SQL-Statements werden als <code>debug</code> und
 * SQLExceptions als <code>error</code> geloggt.<br><br>
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.20 $
 */
abstract public class MailSendDB {
	
    
    private final static Log logger = LogFactory.getLog(MailSendDB.class);

    /**
	 * legt einen neuen leeren Job an
	 * setzt Benutzer und Auftragsdatum
	 * Jobstatus auf BUILDING
	 * 
	 * @param userid ID
	 * @return neue Job ID
	 */
	public final static Integer createNewJob(String module, Integer userid) throws Database {
        return createNewJob(module, userid, System.currentTimeMillis());
    }

	/**
	 * legt einen neuen leeren Job an
	 * setzt Benutzer und Auftragsdatum
	 * Jobstatus auf BUILDING
	 * 
	 * @param userid ID
	 * @return neue Job ID
	 */
	public final static Integer createNewJob(String module, Integer userid, long workdate) throws Database {
		try {
			Insert insert = SQL.Insert(getDBContext(module))
				.table(TjobDB.getTableName())
				.insert(TjobDB.FKUSER, userid)
				.insert(TjobDB.CREATEDATE, new Timestamp(System.currentTimeMillis()))
				.insert(TjobDB.WORKDATE, new Timestamp(workdate))
				.insert(TjobDB.STATUS, MailConstants.JOBSTATUS_BUILDING);
			DB.update(getDBContext(module), insert);
		} catch (SQLException e) {
			logger.error("failed while createNewJob: " + e.getMessage(), e);
			throw new Database(e);
		}
		
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TjobDB.getTableName())
				.select("MAX(" + TjobDB.PK_PK + ")");
			tcResult = DB.result(getDBContext(module), select.toString());
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				return new Integer(resultSet.getInt(1));
			}
			return null;
		} catch (SQLException e) {
			logger.error("failed createNewJob: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.closeAll();
		}
	}

    /**
     * @return the status of the mail-job or null, if there is no such jobid
     */
	public final static Integer getJobStatus(String module, Integer jobid) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TjobDB.getTableName())
				.select(TjobDB.STATUS)
				.where(Expr.equal(TjobDB.PK_PK, jobid));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				return new Integer(resultSet.getInt(1));
			}
			return null;
		} catch (SQLException e) {
			logger.error("failed getJobStatus: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.closeAll();
		}
	}
	
	public final static void removeMessage(
			String module,
			Integer userid,
			Integer jobid,
			Integer messageid) throws Database {
		
		Result tcResult = null;
		try {
			// remove attachments
			// TODO Anh�nge l�schen um DB-Leichen zu verhinden
			
			// remove message
			Delete delete = SQL.Delete(getDBContext(module))
				.from(TmessageDB.getTableName())
				.where(Where.list()
					.add(Expr.equal(TmessageDB.PK_PK, messageid))
					.addAnd(Expr.equal(TmessageDB.FKJOB, jobid))
					.addAnd(Expr.equal(TmessageDB.FKUSER, userid))
		        );
			// TODO eMail nur l�schen wenn Status dies zul��t
			// Building || New? || Sent? || Move? || Done? || Error?
			DB.update(getDBContext(module), delete);
			
			// remove empty job
			Select select = SQL.Select(getDBContext(module))
				.from(TmessageDB.getTableName())
				.select("COUNT(*)")
				.where(Where.and(
			        Expr.equal(TmessageDB.FKJOB, jobid),
			        Expr.equal(TmessageDB.FKUSER, userid)
		        ));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				int count = resultSet.getInt(1); // message-count
				if (count <= 0) {
					// remove job
					delete = SQL.Delete(getDBContext(module))
						.from(TjobDB.getTableName())
						.where(Where.and(
					        Expr.equal(TjobDB.PK_PK, jobid),
					        Expr.equal(TjobDB.FKUSER, userid)
				        ));
					DB.update(getDBContext(module), delete);
				}
			}
		} catch (SQLException e) {
            logger.error("failed removing message: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.closeAll();
		}
	}
	
	public final static Integer addMessagetoJob(
			String module,
			Integer userid,
			Integer jobid,
			MailSendMessage message) throws Database {
		
		Integer req_ans = new Integer(message.getRequestAnswer() ? 1 : 0);
		Integer copyself = new Integer(message.getCopySelf() ? 1 : 0);
		try {
			Insert insert = SQL.Insert(getDBContext(module))
				.table(TmessageDB.getTableName())
				.insert(TmessageDB.FKJOB, jobid)
				.insert(TmessageDB.MAILSUBJECT, message.getMailSubject())
				.insert(TmessageDB.MAILBODY, message.getMailBody())
				.insert(TmessageDB.MAILTO, message.getMailTo())
				.insert(TmessageDB.MAILCC, message.getMailCc())
				.insert(TmessageDB.MAILBCC, message.getMailBcc())
				.insert(TmessageDB.MAILFROM, message.getMailFrom())
				.insert(TmessageDB.REQUESTANSWER, req_ans)
				.insert(TmessageDB.COPYSELF, copyself)
				.insert(TmessageDB.PRIORITY, new Integer(message.getPriority()))
				.insert(TmessageDB.STATUS, MailSendMessage.MAILSTATUS_BUILDING)
				.insert(TmessageDB.FKUSER, userid)
				.insert(TmessageDB.FKSTORE, new Integer(message.getStoreId()));
			InsertKeys keys = DB.insertKeys(getDBContext(module), insert);
            return keys.getPkAsInteger();
		} catch (SQLException e) {
            logger.error("failed addMessagetoJob: " + e.getMessage(), e);
			throw new Database(e);
		}        
	}
	
	/**
	 * update a message
	 * 
	 * @param messageid
	 */
	public final static void updateMessage(String module, Integer messageid, MailSendMessage message) throws Database {
		try {
			Integer req_ans = new Integer(message.getRequestAnswer() ? 1 : 0);
			Integer copyself = new Integer(message.getCopySelf() ? 1 : 0);

			Update update = SQL.Update(getDBContext(module))
				.table(TmessageDB.getTableName())
				.update(TmessageDB.MAILSUBJECT, message.getMailSubject())
				.update(TmessageDB.MAILBODY, message.getMailBody())
				.update(TmessageDB.MAILTO, message.getMailTo())
				.update(TmessageDB.MAILCC, message.getMailCc())
				.update(TmessageDB.MAILBCC, message.getMailBcc())
				.update(TmessageDB.MAILFROM, message.getMailFrom())
				.update(TmessageDB.REQUESTANSWER, req_ans)
				.update(TmessageDB.COPYSELF, copyself)
				.update(TmessageDB.PRIORITY, new Integer(message.getPriority()))
				.update(TmessageDB.FKSTORE, new Integer(message.getStoreId()))
				.update(TmessageDB.STATUS, MailSendMessage.MAILSTATUS_BUILDING)
				.where(Expr.equal(TmessageDB.PK_PK, messageid));
			DB.update(getDBContext(module), update);
		} catch (SQLException e) {
            logger.error("failed updateMessage: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}
	
	public final static Integer addAttachmentStreamAsBLOB(
			String module,
			Integer userId,
			Integer jobId,
			Integer messageId,
			String filename,
			InputStream inputStream,
			int streamSize) throws Database {
		
		if (!isValidAttachmentStream( filename, inputStream, streamSize ))
			return null;

		logger.debug("===========================================");
        logger.debug("[DB -> BLOB]: " + (streamSize / 1000) + "kB");
        logger.debug("===========================================");
		storeAttachmentAsBLOB( module, filename, inputStream, streamSize );
		Integer attachId = retrieveNewestAttachmentID( module );		
		assignAttchIDToMessageEntry( module, userId, jobId, messageId, attachId ); 
		return attachId;
	}

    /** 
     * Saves an attachment as a file (not in DB).
     * The file path will be stored as {@link TattachmentDB} in a DB.
     * Then the id of this attachment will be assigned to a message entry 
     * and stored as {@link TmessageattachmentDB} in a DB.
     * 
     * @param module
     * @param userId
     * @param jobId  
     * @param messageId
     * @param fileName
     * @param filePath
     * @return id of the current attachment have been stored in a DB 
     * @throws Database
     */
    static Integer addAttachmentStreamAsFile(String tempPath, String module, Integer userId, Integer jobId, Integer messageId,
                                                    String fileName, InputStream inputStream, int streamSize) throws Database {

        if (!isValidAttachmentStream( fileName, inputStream, streamSize )) {
            logger.error("invalid attachment '"+fileName+"' in the message: " + messageId);
            return null;
        }

        String filePath = pushInputStreamToFile(tempPath, module, jobId, fileName, inputStream, streamSize );
        logger.debug("attachment saved to file: " + filePath);
        return addAttachmentAsFilePath( module, userId, jobId, messageId, fileName, filePath );
    }

    private static Integer addAttachmentAsFilePath( String module, Integer userId, Integer jobId, Integer messageId, String fileName, String filePath ) throws Database {
        if ( filePath == null || filePath.equals( "" )){
            logger.error("couldn't store empty path for message: " + messageId);
            return null;
        }
        Integer attachmentID = storeAttachmentAsFilePath( module, fileName, filePath );
        assignAttchIDToMessageEntry( module, userId, jobId, messageId, attachmentID );
        return attachmentID;
    }

    public static Integer storeAttachmentAsFilePath( String module, String fileName, String filePath ) throws Database {
        logger.debug("==================================================");
        logger.debug("[DB-attachment-path]: " + filePath);
        logger.debug("==================================================");
        // storing attachment as link in DB
        // 1. TattachmentDB.FILENAME = filePath
        // 2. TattachmentDB.FKATTACHMENTTYPE = File / BLOB
        // 3. TattachmentDB.TEXTCONTENT = fileName
        // 4. TattachmentDB.FKATTACHMENTDOCTYPE = mime-type of file (not really required)
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            Insert insert = SQL.Insert(getDBContext(module))
                .table(TattachmentDB.getTableName())
                .insert(TattachmentDB.FILENAME, filePath)// link 
                .insert(TattachmentDB.FKATTACHMENTTYPE, new Integer(StorageType.FILE.ordinal()))
                .insert(TattachmentDB.TEXTCONTENT, fileName);// doc name
            connection = DB.getConnection(getDBContext(module));
            connection.setAutoCommit(true);
            statement = connection.prepareStatement(insert.toString());
            statement.executeUpdate();
            
            return retrieveNewestAttachmentID( module );

        } catch (SQLException e) {
            logger.error("failed to store file path of attachment", e);
            throw new Database(e);
        } finally {
            if (connection != null && statement != null)
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            else if (connection != null)
                try {
                    connection.close();
                } catch (SQLException e) {
                }
        }
    }

    private static String pushInputStreamToFile(String tempPath, String module, Integer jobId, String fileName, InputStream inputStream, int streamSize ) throws Database {
        byte[] data = new byte[streamSize]; 
        try {
            inputStream.read(data);
        }
        catch ( IOException e1 ) {
            logger.error("failed reading bytes", e1);
            throw new Database(e1);
        }
        
        String filePath;
        try {
            filePath = Attachments.saveBytesToFile(tempPath, null, jobId, 0, fileName, data);
        }
        catch ( IOException e1 ) {
            logger.error("failed saving a file: " + e1.getMessage(), e1);
            throw new Database(e1);
        }
        return filePath;
    }

    private static boolean isValidAttachmentStream( String filename, InputStream inputStream, int streamSize ) {
        return filename != null && filename.length() != 0 && inputStream != null && streamSize != 0;
    }

    private static void storeAttachmentAsBLOB( String module, String filename, InputStream inputStream, int streamSize ) throws Database {
        Connection connection = null;
		PreparedStatement statement = null;
		try {
			Insert insert = SQL.Insert(getDBContext(module))
				.table(TattachmentDB.getTableName())
				.insert(TattachmentDB.FILENAME, filename)
				.insert(TattachmentDB.TEXTCONTENT, filename)//compatible to file storing  
                .insert(TattachmentDB.FKATTACHMENTTYPE, StorageType.BLOB.toString())//compatible to file storing
				.insert(TattachmentDB.BINARYCONTENT, SQL.Function(getDBContext(module), "?"));
			connection = DB.getConnection(getDBContext(module));
			connection.setAutoCommit(true);
			statement = connection.prepareStatement(insert.toString());
			statement.setBinaryStream(1, inputStream, streamSize);
			statement.executeUpdate();
		} catch (SQLException e) {
            logger.error("failed while storeAttachmentAsBLOB: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (connection != null && statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
				}
			else if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
				}
		}
    }
    
    public static void assignAttchIDToMessageEntry( String module, Integer userId, Integer jobId, Integer messageId, Integer attachId ) throws Database {
        try {
			Insert insert = SQL.Insert(getDBContext(module))
				.table(TmessageattachmentDB.getTableName())
				.insert(TmessageattachmentDB.FKMESSAGE, messageId)
				.insert(TmessageattachmentDB.FKJOB, jobId)
				.insert(TmessageattachmentDB.FKUSER, userId)
				.insert(TmessageattachmentDB.FKATTACHMENT, attachId);
			DB.update(getDBContext(module), insert.toString());
			logger.debug("==================================================");
            logger.debug("[DB-attachment -> DB-message] : [" + attachId + " -> " + messageId +"]");
            logger.debug("==================================================");
		} catch (SQLException e) {
            logger.error("failed while assignAttchIDToMessageEntry: " + e.getMessage(), e);
			throw new Database(e);
		}
    }

    private static Integer retrieveNewestAttachmentID( String module ) throws Database {
        Integer attachId = null;
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TattachmentDB.getTableName())
				.select("max(" + TattachmentDB.PK_PK + ")");
			tcResult = DB.result(getDBContext(module), select.toString());
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				attachId = new Integer(resultSet.getInt(1));
			}
		} catch (SQLException e) {
            logger.error("failed while retrieveNewestAttachmentID: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
        return attachId;
    }


	public final static void removeAttachment(
			String module,
			Integer userId,
			Integer jobId,
			Integer messageId,
			Integer attachmentId) throws Database {
		
		removeAttachmentDependences( module, userId, jobId, messageId, attachmentId );

        removeAttachmentOutsideDB(module, attachmentId);
        
        removeAttachmentInsideDB( module, attachmentId );
	}

    private static void removeAttachmentDependences( String module, Integer userId, Integer jobId, Integer messageId, Integer attachmentId ) throws Database {
		try {
		    //an attachment can be assigned to multiple messages
			Delete delete = SQL.Delete(getDBContext(module))
				.from(TmessageattachmentDB.getTableName())
				.where(Where.list()
			        .add(Expr.equal(TmessageattachmentDB.FKATTACHMENT, attachmentId))
			        .addAnd(Expr.equal(TmessageattachmentDB.FKJOB, jobId))
				    .addAnd(Expr.equal(TmessageattachmentDB.FKUSER, userId))    
				    .addAnd(Expr.equal(TmessageattachmentDB.FKMESSAGE, messageId))
			    );
			DB.update(getDBContext(module), delete);
		} catch (SQLException e) {
			logger.error("[removeAttachmentDependences]: failed removing assignments to: " + attachmentId, e);
			throw new Database(e);
		}
    }

    private static void removeAttachmentInsideDB( String module, Integer attachmentId ) throws Database {
        try {
            Delete delete = SQL.Delete(getDBContext(module))
                .from(TattachmentDB.getTableName())
                .where(Expr.equal(TattachmentDB.PK_PK, attachmentId));
            DB.update(getDBContext(module), delete);
        } catch (SQLException e) {
            logger.error("[removeAttachmentInsideDB]: failed removing an attachment entry", e);
            throw new Database(e);
        }
    }

    // an attachment could have been stored as a BLOB (inside DB) or as a file (outside DB)
	private static void removeAttachmentOutsideDB( String module, Integer attachmentId ) throws Database {
        AttachmentValues attachmentValues = Attachments.retrieveValuesOf(getDBContext(module), module, attachmentId);
        if(attachmentValues == null) {
            logger.error("[removeAttachmentOutsideDB]: failed removing attachment: id not found: "+attachmentId);
            return;
        }
        
        if (StorageType.FILE == attachmentValues.getStorageType()){
            try{
                new File(attachmentValues.getFilePath()).delete();
            } catch (Exception e) {
                logger.error("[removeAttachmentOutsideDB]: failed removing a file: " + attachmentValues.getFilePath() + "\n" + e.getMessage(),e);
            }
        } else if (StorageType.BLOB == attachmentValues.getStorageType()) {
            //do nothing: BLOB is inside DB
        } else { 
            //shouldn't happen because of Enumeration
            logger.error("[removeAttachmentOutsideDB]: can't handle unknown storage type");
        }
    }

    public final static void enqueueJob(String module, Integer jobid, Timestamp timestamp) throws Database {
		if (timestamp == null) {
			timestamp = new Timestamp(System.currentTimeMillis());
		}
		
		try {
			Update update = SQL.Update(getDBContext(module))
				.table(TjobDB.getTableName())
				.update(TjobDB.STATUS, MailConstants.JOBSTATUS_NEW)
				.update(TjobDB.WORKDATE, timestamp)
				.where(Expr.equal(TjobDB.PK_PK, jobid));
			DB.update(getDBContext(module), update);
		} catch (SQLException e) {
			logger.error("failed while enqueueJob: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			// nothing
		}

		try {
			Update update = SQL.Update(getDBContext(module))
				.table(TmessageDB.getTableName())
				.update(TmessageDB.STATUS, MailSendMessage.MAILSTATUS_NEW)
				.where(Expr.equal(TmessageDB.FKJOB, jobid));
			DB.update(getDBContext(module), update);
		} catch (SQLException e) {
			logger.error("failed while enqueueJob: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}
	
	public final static Integer getMessageCount(String module, int JobID) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TmessageDB.getTableName())
				.select("COUNT(*)")
				.where(Expr.equal(TmessageDB.FKJOB, new Integer(JobID)));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			if (resultSet.next()) {
				return new Integer(resultSet.getInt(1));
			}
			return null;
		} catch (SQLException e) {
			logger.error("failed while getMessageCount", e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}

	/**
	 * @return Map mit jobid => status
	 */
	public final static Map getJobStatus(String module) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TjobDB.getTableName())
				.select(TjobDB.PK_PK)
				.select(TjobDB.STATUS);
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			HashMap result = new HashMap();
			while (resultSet.next()) {
				result.put(
					new Integer(resultSet.getInt(1)),
					new Integer(resultSet.getInt(2)));
			}
			return result;
		} catch (SQLException e) {
			logger.error("failed while getJobStatus: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.closeAll();
		}
	}

	/**
	 * @return Liste mit allen MessageIDs eines Jobs, die abgearbeitet werden sollen.
	 */
	public final static List getJobsToDo(String module) throws Database {
		// TODO Workdate mit ins SQL einbeziehen
		// beachte: datenbank speichert hh:mm nicht!? umstellen auf datetime?
		Timestamp now = new Timestamp(System.currentTimeMillis());
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TjobDB.getTableName())
				.select(TjobDB.PK_PK)
//				.select(TjobDB.WORKDATE)
				.where(Where.and(Expr.equal(TjobDB.STATUS , MailConstants.JOBSTATUS_NEW),
                                 Expr.lessOrEqual(TjobDB.WORKDATE, now)));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			ArrayList result = new ArrayList();
			while (resultSet.next()) {
//				if (resultSet.getTimestamp(2).before(now)) {
					result.add(new Integer(resultSet.getInt(1)));
//				}
			}
			logger.debug(result);
			return result;
		} catch (SQLException e) {
			logger.error("failed while getJobsToDo: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null){
				tcResult.closeAll();
			}
		}
	}

	/**
	 * @return List mit n MailSendMessage-Objecten pro Message
	 */
	public final static List getMessagesToJob(String module, Integer jobid) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TmessageDB.getTableName())
				.select(TmessageDB.PK_PK) // 1
				.select(TmessageDB.MAILSUBJECT) // 2
				.select(TmessageDB.MAILBODY) // 3
				.select(TmessageDB.MAILTO) // 4
				.select(TmessageDB.MAILCC) // 5
				.select(TmessageDB.MAILBCC) // 6
				.select(TmessageDB.MAILFROM) // 7
				.select(TmessageDB.REQUESTANSWER) // 8
				.select(TmessageDB.COPYSELF) // 9
				.select(TmessageDB.PRIORITY) // 10
				.select(TmessageDB.FKUSER) // 11
				.select(TmessageDB.FKSTORE) // 12
				.where(Expr.equal(TmessageDB.FKJOB, jobid));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			ArrayList<MailSendMessage> list = new ArrayList<MailSendMessage>();
			while (resultSet.next()) { // start message
				int messageid = resultSet.getInt(1); // id
				
				// message attachment
                
                //TODO: [!] aleksej: handling attachments: the same attachments for bundle of messages 
                Map<String, DataSource> attachments = Attachments.retrieveAttachments(getDBContext(module), module, messageid );
                
				MailSendMessage message = new MailSendMessage(
						messageid, // id
						resultSet.getString(2), // subject
						resultSet.getString(3), // body
						resultSet.getString(4), // to
						resultSet.getString(5), // cc
						resultSet.getString(6), // bcc
						resultSet.getString(7), // from
						attachments, // attachments
						resultSet.getInt(8) == 1, // request answer
						resultSet.getInt(9) == 1, // copyself
						resultSet.getInt(10), // priority
						resultSet.getInt(11), // userid
						resultSet.getInt(12)); // storeid
				list.add(message);
			} // end message
			return list;
		} catch (SQLException e) {
			logger.error("failed while getMessagesToJob: "+e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}


	/**
	 * @return List mit n Integer IDs der MailSendMessage-Objecte
	 */
	public final static List getMessagesIDsToJob(String module, Integer jobid) throws Database {
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TmessageDB.getTableName())
				.select(TmessageDB.PK_PK) // 1
				.where(Expr.equal(TmessageDB.FKJOB, jobid));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			ArrayList list = new ArrayList();
			while (resultSet.next()) { // start message
				list.add( new Integer(resultSet.getInt(1) )); // id
				
			}
			return list;
		} catch (SQLException e) {
			logger.error("failed while getMessagesIDsToJob:"+e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}        
	}

	public final static void updateMessageStatus(
			String module,
			int messageid,
			String errorcode,
			Integer status) throws Database {
		
		try {
			Update update = SQL.Update(getDBContext(module))
				.table(TmessageDB.getTableName())
				.update(TmessageDB.STATUS, status);
			if (errorcode != null)
				update.update(TmessageDB.ERRORCODE, errorcode)
				.where(Expr.equal(TmessageDB.PK_PK, new Integer(messageid)));
			DB.update(getDBContext(module), update.toString());
		} catch (SQLException e) {
			logger.error("failed while updateMessageStatus: "+ e.getMessage(), e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}

	public final static void updateJobstatus(
			String module,
			Integer jobid,
			Integer status) throws Database {
		
		// TODO: datum und runcount noch setzen
		try {
			Update update = SQL.Update(getDBContext(module))
				.table(TjobDB.getTableName())
				.update(TjobDB.STATUS, status)
				.where(Expr.equal(TjobDB.PK_PK, jobid));
			DB.update(getDBContext(module), update);
		} catch (SQLException e) {
			logger.error("failed while updateJobstatus: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}

	public final static void updateJobDate(String module, Integer jobid, Timestamp newDate) throws Database {
		if (newDate == null) return;
		try {
			Update update = SQL.Update(getDBContext(module))
				.table(TjobDB.getTableName())
				.update(TjobDB.WORKDATE, newDate)
				.where(Expr.equal(TjobDB.PK_PK, jobid));
			DB.update(getDBContext(module), update);
		} catch (SQLException e) {
			logger.error("failed while updateJobDate: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			// nothing
		}
	}

	public final static Map getMessageDetails(
			String module,
			Integer userid,
			Integer jobid,
			Integer mailid) throws Database {
		
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TmessageDB.getTableName())
				.select(TmessageDB.PK_PK)
				.select(TmessageDB.MAILSUBJECT)
				.select(TmessageDB.MAILBODY)
				.select(TmessageDB.MAILTO)
				.select(TmessageDB.MAILCC)
				.select(TmessageDB.MAILBCC)
				.select(TmessageDB.MAILFROM)
				.select(TmessageDB.REQUESTANSWER)
				.select(TmessageDB.COPYSELF)
				.select(TmessageDB.PRIORITY)
				.select(TmessageDB.STATUS)
				.select(TmessageDB.ERRORCODE)
				.select(TmessageDB.FKJOB)
				.where(Where.list()
			       .add(Expr.equal(TmessageDB.PK_PK, mailid))
			       .addAnd(Expr.equal(TmessageDB.FKJOB, jobid))
			       .addAnd(Expr.equal(TmessageDB.FKUSER, userid)));
			       
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			Map result = null;
			if (resultSet.next()) {
				result = new HashMap();
				result.put("mailid", new Integer(resultSet.getInt(1)));
				result.put("subject", resultSet.getString(2));
				result.put("body", resultSet.getString(3));
				result.put("to", resultSet.getString(4));
				result.put("cc", resultSet.getString(5));
				result.put("bcc", resultSet.getString(6));
				result.put("from", resultSet.getString(7));
				result.put("requestanswer", new Integer(resultSet.getInt(8)));
				result.put("copyself", new Integer(resultSet.getInt(9)));
				result.put("priority", new Integer(resultSet.getInt(10)));
				result.put("status", new Integer(resultSet.getInt(11)));
				result.put("errorcode", resultSet.getString(12));
				result.put("jobid", new Integer(resultSet.getInt(13)));

				Map attachment = null;
				Result tcResultAttach = null;
				try {
					Select selectAttach = SQL.Select(getDBContext(module))
						.from(TattachmentDB.getTableName())
						.select(TattachmentDB.PK_PK)
						.select(TattachmentDB.FILENAME)
						.join(TmessageattachmentDB.getTableName(), TmessageattachmentDB.FKATTACHMENT, TattachmentDB.PK_PK)
						.where(Expr.equal(TmessageattachmentDB.FKMESSAGE, mailid));
					tcResultAttach = DB.result(module, selectAttach);
					ResultSet resultSetAttach = tcResultAttach.resultSet();
					attachment = new HashMap();
					while (resultSetAttach.next()) {
						attachment.put(new Integer(resultSetAttach.getInt(1)), resultSetAttach.getString(2));
					}
				} catch (SQLException e) {
					logger.error("failed while getMessageDetails: " + e.getMessage(), e);
					// do not throw again
				} finally {
					if (tcResultAttach != null)
						tcResultAttach.close();
				}
				result.put("attachment", attachment);
			}
			return result;
		} catch (SQLException e) {
			logger.error("failed while getMessageDetails: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}

	/**
	 * Holt eine Liste von eMail's ab.
	 * 
	 * @param userid
	 * @param from
	 *   Null-basierent
	 * @param size
	 */
	public final static ListProperties getMessageList(
			String module,
			Integer userid,
			int from,
			int size,
			String filter) throws Database {
		
		// TODO: sollte evtl. auch einen zusammenhang mit den Jobs zur�ck liefern.
		Result tcResult = null;
		try {
			Select select = SQL.Select(getDBContext(module))
				.from(TmessageDB.getTableName())
				.select(TmessageDB.PK_PK)
				.select(TmessageDB.MAILSUBJECT)
				.select(TmessageDB.MAILTO)
				.select(TmessageDB.MAILFROM)
				.select(TmessageDB.STATUS)
				.select(TmessageDB.ERRORCODE)
				.select(TmessageDB.FKJOB)
				.where(Expr.equal(TmessageDB.FKUSER, userid));
			
			// Where-Klauseln zusamenbauen
			if (filter != null){
				List list = new ArrayList();
				if (filter.equals("drafts")) {
					list.add(MailSendMessage.MAILSTATUS_ERROR);
					list.add(MailSendMessage.MAILSTATUS_BUILDING);
				}
				
				if (filter.equals("send")) {
					list.add(MailSendMessage.MAILSTATUS_SENT);
					list.add(MailSendMessage.MAILSTATUS_DONE);
				}
				
				if (filter.equals("unsend")) {
					list.add(MailSendMessage.MAILSTATUS_NEW);
					list.add(MailSendMessage.MAILSTATUS_WORKING);
				}
				select.where(Where.and(
				        Expr.equal(TmessageDB.FKUSER, userid),
				        Expr.in(TmessageDB.STATUS, list)));
			}
			
			select.orderBy(Order.desc(TmessageDB.PK_PK));
			tcResult = DB.result(getDBContext(module), select);
			ResultSet resultSet = tcResult.resultSet();
			
			// Einschr�nkung der R�ckgabe
			int i = 0;
			if (size > 0) {
				if (from == 0)
					from = 1;
				else if (from != 1)
					resultSet.absolute(from - 1);
			}
			
			ListProperties result = new ListProperties();
			result.setProperty("from", from);
			
			while (resultSet.next() && (size == 0 || i < size)) {
				Map entry = new HashMap();
				entry.put("mailid", new Integer(resultSet.getInt(1)));
				entry.put("subject", resultSet.getString(2));
				entry.put("to", resultSet.getString(3));
				entry.put("from", resultSet.getString(4));
				entry.put("status", new Integer(resultSet.getInt(5)));
				entry.put("errorcode", resultSet.getString(6));
				entry.put("jobid", new Integer(resultSet.getInt(7)));
				result.add(entry);
				i++;
			}
			if (size > 0) {
				resultSet.last();
				result.setProperty("size", resultSet.getRow());
				result.setProperty("show", size);
			}
			return result;
		} catch (SQLException e) {
			logger.error("failed while getMessageList: " + e.getMessage(), e);
			throw new Database(e);
		} finally {
			if (tcResult != null)
				tcResult.close();
		}
	}
	
    // TODO: introduce a clean connection handling 
    // where the db context is supplied to all using methods, and not shared between the threads.
	public final static DBContext getDBContext(String module) {

        // if we are in an Octopus dispatch thread, we use the managed connection
        OctopusContext oc = Context.getActive();
        if (oc != null) {
            return TcDBContext.getDefaultContext();
        }

        try {
            // otherwise we create and cache one connection for all other threads!
            if(dbx==null || dbx.getDefaultConnection().isClosed())
                dbx = DB.getDefaultContext(module);
            return dbx;
        } catch (SQLException e) {
            logger.error("error testing connection", e);
            throw new RuntimeException(e);
        }

	}
	
	private static DBContext dbx = null;
}
