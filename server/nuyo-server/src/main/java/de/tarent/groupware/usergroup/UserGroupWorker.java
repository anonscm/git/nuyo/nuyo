package de.tarent.groupware.usergroup;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Logger;

import de.tarent.contact.bean.TgroupDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.utils.ResultTransform;
import de.tarent.octopus.server.OctopusContext;

/**
 * @author kleinw
 *  
 * 	- createOrModifyUserGroup
 * 	- getUserGroups
 * 
 */
public class UserGroupWorker implements UserGroupWorkerConstants {
		private static final Logger logger = Logger.getLogger(UserGroupWorker.class.getName());

        public String[]	INPUT_GETUSERGROUPS		= {"userid", "groupid", "onlyPrivateGroups"};
        public boolean[]	MANDATORY_GETUSERGROUPS	= {false, false, false};
		public String		OUTPUT_GETUSERGROUPS	= "usergroups";

        public Object getUserGroups(OctopusContext oc, Integer userId, Integer groupId, Boolean onlyPrivateGroups) throws SQLException {		   		    
		    
		    Select select =
		        SQL.Select(TcDBContext.getDefaultContext())
		        	.from(TgroupDB.getTableName())
		        	.add(TgroupDB.PK_PK, Integer.class)					//	[0]
		        	.add(TgroupDB.GROUPNAME, String.class)				//	[1]
		        	.add(TgroupDB.DESCRIPTION, String.class)			//	[2]
		        	.add(TgroupDB.CREATED, Timestamp.class)				//	[3]
		        	.add(TgroupDB.CHANGED, Timestamp.class)				//	[4]
					.add(TgroupDB.FKGLOBALROLE, Integer.class)			//	[5]
					.add(TgroupDB.ISPRIVATE, Boolean.class)			    //	[6]
					.add(TgroupDB.GROUPTYPE, Integer.class);			//	[7]
		    
            WhereList whereList = null;

		    if(groupId != null) {
                whereList = new WhereList();
                whereList.addAnd(Expr.equal(TgroupDB.PK_PK, groupId));
            }            
		    else if(userId != null) {
                if (whereList == null)
                    whereList = new WhereList();
                whereList.addAnd(Expr.equal(TgroupUserDB.FKUSER, userId));
                select.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupDB.PK_PK);
            }

            if (onlyPrivateGroups!=null) {
                if (whereList == null)
                    whereList = new WhereList();
                whereList.addAnd(Expr.equal(TgroupDB.ISPRIVATE, onlyPrivateGroups));
            }             
            select.where(whereList);

		    return ResultTransform.toSingleString(select.getList(TcDBContext.getDefaultContext()), oc.getRequestObject().getRequestParameters());
		    
	}
		
	
	
}