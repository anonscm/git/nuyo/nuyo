/* $Id: MailSyncer.java,v 1.10 2007/06/16 14:55:48 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import gnu.mail.providers.imap.IMAPStore;
import gnu.mail.providers.pop3.POP3Store;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.event.ConnectionEvent;
import javax.mail.event.ConnectionListener;

import de.tarent.contact.bean.TcommDB;
import de.tarent.contact.bean.TmailmessageDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Clause;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.groupware.contact.ContactWorkerConstants;
import de.tarent.groupware.utils.Syncer;
import de.tarent.octopus.content.TcAll;

/**
 * Diese Klasse repr�sentiert einen eMail-Syncer der
 * IMAP-Ordner in die Datenbank syncronisiert.
 * 
 * Je eine Instanz dieser Klasse ist f�r ein eMail-Postfach
 * zust�ndig und verarbeitet n Ordner.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.10 $
 */
public class MailSyncer extends Syncer implements ConnectionListener {
	
	// KONSTANTEN - DETAULT WERTE
	
	public final static long SYNCER_RUNUNTIL = 10 * 60 * 1000; // 10 Minuten
	public final static long SYNCER_INTERVAL = 1 * 60 * 1000; // 1 Minute
	
	
	// MEMBERVARIABLEN
	
	private TcAll _all; // TcAll Objekt um andere Worker aufzurufen
	private Integer _userid;
	
	private Integer _syncid; // Sync-ID
	private Date _syncstart; // Start des Syncvorganges
	
	private String _username; // Zur Authentifizierung
	private String _password;
	private String _serverprotocol; // see MailConstants.PROTOCOL_*
	private String _servername;
	private int _serverport = -1; // -1 = Default f�r Protokol
	
	private Map _folderlist = new HashMap(); // folderid => foldername
	
	private Session _session;
	private Store _store;
	private Integer _storeid;
	
	private String _dbPoolName;
	
	private long _syncUntil = SYNCER_RUNUNTIL;
	
	
	// Kontruktor
	
	public MailSyncer(TcAll all, Integer userid, Integer storeid, String modulename) throws MailException {
		super(SYNCER_RUNUNTIL, Syncer.DISABLED, SYNCER_INTERVAL);
		
		_all = all;
		_userid = userid;
		_storeid = storeid;
		_dbPoolName = modulename;
		
		init(); // �berschreibt Sync-Parameter
		load(); // L�dt MailStore-Parameter
	}
	
	
	// Setter
	
	private void init() {
		try {
			_syncUntil = Long.parseLong(_all.moduleConfig().getParam("mailSyncer_RunUntil")) * 1000;
			syncExtend();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		try {
			syncInterval(Long.parseLong(_all.moduleConfig().getParam("mailSyncer_Interval")) * 1000);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}
	
	public void syncExtend() {
		syncUntil(_syncUntil);
	}
	
	private void load() throws MailException {
		Map storeinfo = MailDB.getStoreInfo(_userid, _storeid);
		setServer((String)storeinfo.get(MailConstants.SERVER_PROTOCOL),
				(String)storeinfo.get(MailConstants.SERVER_NAME),
				(Integer)storeinfo.get(MailConstants.SERVER_PORT));
		setUser((String)storeinfo.get(MailConstants.SERVER_USERNAME),
				(String)storeinfo.get(MailConstants.SERVER_PASSWORD));
		addFolder(MailDB.getStoreFolders(_dbPoolName, _storeid));
	}
	
	
	/**
	 * Speichert 'username' und 'password'.
	 * 
	 * @param username
	 * @param password
	 */
	public void setUser(String username, String password) {
		_username = username;
		_password = password;
	}
	
	
	/**
	 * Speichert 'protocol', 'server' und 'port'.
	 * 
	 * @param protocol (PROTOCOL_*)
	 * @param server
	 * @param port (null = default oder 0-65535)
	 * 
	 * @see MailConstants#PROTOCOL_IMAP
	 * @see MailConstants#PROTOCOL_POP3
	 * @throws MailException (ERROR_INVALID_*)
	 */
	public void setServer(String protocol, String server, Integer port) throws MailException {
		if (MailConstants.PROTOCOL_IMAP.equalsIgnoreCase(protocol)) {
			_serverprotocol = MailConstants.PROTOCOL_IMAP;
		} else if (MailConstants.PROTOCOL_POP3.equalsIgnoreCase(protocol)) {
			_serverprotocol = MailConstants.PROTOCOL_POP3;
		} else {
			logWarning(MailConstants.ERROR_INVALID_PROTOCOL + ": " + protocol);
			throw new MailException(MailConstants.ERROR_INVALID_PROTOCOL);
		}
		
		if (server != null && server.length() != 0) {
			_servername = server;
		} else {
			logWarning(MailConstants.ERROR_INVALID_SERVER + ": " + server);
			throw new MailException(MailConstants.ERROR_INVALID_SERVER);
		}
		
		if (port != null) {
			if (port.intValue() < 0 || port.intValue() > 65535) {
				logWarning( MailConstants.ERROR_INVALID_PORT + ": " + port);
				throw new MailException(MailConstants.ERROR_INVALID_PORT);
			}
			_serverport = port.intValue();
		} else {
			_serverport = -1; // default port
		}
	}
	
	/**
	 * F�gt einen Ordner der Liste hinzu, der in die
	 * Datenbank gesynct werden soll.
	 * 
	 * @param folderid
	 * @param foldername
	 */
	public void addFolder(Integer folderid, String foldername) {
		_folderlist.put(folderid, foldername);
	}
	
	public void addFolder(Map folder) {
		_folderlist.putAll(folder);
	}
	
	/**
	 * L�scht einen Ordner, der nicht mehr in die
	 * Datenbank gesynct werden soll.
	 * 
	 * @param folderid
	 */
	public void removeFolder(String folderid) {
		_folderlist.remove(folderid);
	}
	
	/**
	 * Leert die Liste mit Ordnern, die in die
	 * Datenbank gesynct werden sollen.
	 */
	public void removeAllFolder() {
		_folderlist.clear();
	}
	
	
	// eMail-Sync-Methoden
	
	public String getName() {
		return "mailsyncer-" + _storeid;
	}
	
	public void sync() {
		try {
			// Sync-ID und Sync-Start setzten
			_syncid = getNextMailSyncer();
			_syncstart = new Date(System.currentTimeMillis());
			
			// eMail-Server connecten
			connect();
			
			// Alle zu syncenden Folder durchgehen
			Iterator it = _folderlist.keySet().iterator();
			while (it.hasNext()) {
				Integer folderid = (Integer)it.next();
				String foldername = (String)_folderlist.get(folderid);
				// und diese syncen.
				sync(folderid, foldername);
			}
		} catch (Exception e) {
			logError(e.getMessage(), e);
		} finally {
			try {
				disconnect();
			} catch (MailException e) {
				logError(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Syncronisiert einen Folder in die Datenbank.
	 * 
	 * @param folderid
	 * @param foldername
	 */
	private void sync(Integer folderid, String foldername) {
		log("sync folder '" + foldername + "'");
		
		try {
			// Prepare
			Result tcResult;
			ResultSet resultSet;
			
			String subject; // Betreff
			String sendername; // Absender
			String sendermail;
			Integer senderid; // fk_address
			Integer hashcode; // Eindeutiger Key der die Nachricht auf dem eMail-Server identifiziert.
			Date sentdate; // Absender Datum
			Integer flag; // Flags wie Gel�scht, Neu, etc.
			
			boolean exists;
			
			// Folder der gesynct werden sollen
			Folder folder = _store.getFolder(foldername);
			if (!folder.isOpen()) {
				folder.open(Folder.READ_WRITE);
			}
			
			// Holt alle Nachrichten und geht diese einzeln durch
			Message message[] = folder.getMessages();
			FetchProfile fp = new FetchProfile();
			fp.add(FetchProfile.Item.ENVELOPE);
			folder.fetch(message, fp);
			
			for (int m = 0; m < message.length; m++) {
				Message msg = message[m];
				
				tcResult = null;
				
				try {
					// Datenbank-Felder generieren
					subject = msg.getSubject();
					sentdate = msg.getSentDate();
					Address address[] = msg.getFrom();
					sendername = MailHelper.getAddressName(address);
					sendermail = MailHelper.getAddressMail(address);
					flag = new Integer((msg.getFlags().hashCode()));
					
					// Eindeutigen eMail-Key erzeugen
					hashcode = new Integer(MailHelper.getMailHashCode(msg));
					
					// TODO Datenbank �ber Stored Procedure f�llen.
					
					// Eintrag pr�fen ob vorhanden
					Clause clause = Where.list()
							.add(Expr.equal(TmailmessageDB.FKMAILSTORECATEGORY, folderid))
							.addAnd(Expr.equal(TmailmessageDB.FKUSER, _userid))
							.addAnd(Expr.equal(TmailmessageDB.MAILKEY, hashcode))
							.addAnd(sentdate == null ? Expr.isNull(TmailmessageDB.SENDDATE) : Expr.equal(TmailmessageDB.SENDDATE, sentdate));
					
					tcResult = SQL.Select(TcDBContext.getDefaultContext())
							.from(TmailmessageDB.getTableName())
							.select(TmailmessageDB.FKMAILSYNC)
							.where(clause)
							.executeSelect(TcDBContext.getDefaultContext());
					resultSet = tcResult.resultSet();
					exists = resultSet.next();
					tcResult.close();
					
					// Eintrag aktualisieren oder einf�gen
					if (!exists) {
						// senderid / fk_address holen
						tcResult = SQL.Select(TcDBContext.getDefaultContext())
								.from(TcommDB.getTableName())
								.select(TcommDB.FKADDRESS)
								.where(Expr.equal(TcommDB.VALUE, sendermail))
								.executeSelect(TcDBContext.getDefaultContext());
						resultSet = tcResult.resultSet();
						if (resultSet.next()) {
							senderid = new Integer(resultSet.getInt(1));
						} else {
							senderid = null;
						}
						tcResult.close();
						// Eintrag ins Postfach schreiben f�r Anzeige
						SQL.Insert(TcDBContext.getDefaultContext())
								.table(TmailmessageDB.getTableName())
								.insert(TmailmessageDB.FKUSER, _userid)
								.insert(TmailmessageDB.FKMAILSTORECATEGORY, folderid)
								.insert(TmailmessageDB.FKMAILSYNC, _syncid)
								.insert(TmailmessageDB.FIRSTMAILSYNC, _syncstart)
								.insert(TmailmessageDB.MAILKEY, hashcode)
								.insert(TmailmessageDB.SENDERNAME, sendername)
								.insert(TmailmessageDB.SENDERMAIL, sendermail)
								.insert(TmailmessageDB.SUBJECT, (subject != null && subject.length() > 200) ? subject.substring(0, 195) + "..." : subject)
								.insert(TmailmessageDB.SENDDATE, sentdate)
								.insert(TmailmessageDB.FLAG, flag)
								.insert(TmailmessageDB.FKADDRESS, senderid)
								.executeInsert(TcDBContext.getDefaultContext());
						
						// Eintrag in Kontakthistorie Eintragen
						if (senderid != null) {
							logInfo("new contact: addressid=" + senderid + ", type=email, bound=in, subject=" + subject + ", note=eMail eingegangen");
							HashMap attributes = new HashMap();
							attributes.put(ContactWorkerConstants.KEY_SUBJECT, subject);
							attributes.put(ContactWorkerConstants.KEY_NOTE, "eMail eingegangen");
							//	TODO fixen, hier sind ein paar Parameter zuviel
							//							ContactWorker.createContact(
//									_all,
//									senderid, // addressid
//									_userid, // userid
//									ContactWorkerConstants.CATEGORY_MAILING, // category
//									ContactWorkerConstants.CHANNEL_EMAIL, // channel
//									new Integer(0), // duration
//									new Integer(0), // linktype
//									Boolean.TRUE, // inbound
//									attributes, // attributes
//									sentdate == null ? null : new Long(sentdate.getTime())); // startdate
						}
						
					} else {
						// Eintrag nur aktuallisieren
						SQL.Update(TcDBContext.getDefaultContext())
								.table(TmailmessageDB.getTableName())
								.update(TmailmessageDB.FKMAILSYNC, _syncid)
								.update(TmailmessageDB.FLAG, flag)
								.where(clause)
								.executeUpdate(TcDBContext.getDefaultContext());
						
					}
				} catch (MessagingException e) {
					logError(e.getMessage(), e);
				} catch (SQLException e) {
					logError(e.getMessage(), e);
				} finally {
					if (tcResult != null)
						tcResult.close();
				}
			} // ende eMail-Nachrichten durchlauf
			
			// Alte eMails aus Datenbank l�schen
			try {
				SQL.Delete(TcDBContext.getDefaultContext())
						.from(TmailmessageDB.getTableName())
						.where(Where.list()
								.add(Expr.equal(TmailmessageDB.FKUSER, _userid))
								.addAnd(Expr.notEqual(TmailmessageDB.FKMAILSYNC, _syncid))
								.addAnd(Expr.equal(TmailmessageDB.FKMAILSTORECATEGORY, folderid)))
						.executeDelete(TcDBContext.getDefaultContext());
			} catch (SQLException e) {
				logError(e.getMessage(), e);
			}
		} catch (MessagingException e) {
			logError(e.getMessage(), e);
		} catch (Exception e) {
			logError(e.getMessage(), e);
		}
	}
	
	private Integer getNextMailSyncer() {
	    Result result = null;
	    try {
			result = SQL.Select(TcDBContext.getDefaultContext())
					.from(TmailmessageDB.getTableName())
					.select("MAX(" + TmailmessageDB.FKMAILSYNC + ")")
					.executeSelect(TcDBContext.getDefaultContext());
			ResultSet resultSet = result.resultSet();
			if (resultSet.next()) {
				return new Integer(resultSet.getInt(1) + 1);
			}
		} catch (SQLException e) {
			logError(e.getMessage(), e);
		} finally {
		    if(result != null) result.close();
		}
		return null;
	}
	
	
	// eMail-Server-Methoden
	
	/**
	 * connect to server
	 * 
	 * @throws MailException
	 */
	public void connect() throws MailException {
		try {
			if (_session == null || _store == null) {
				if (_serverprotocol.equals(MailConstants.PROTOCOL_IMAP)) {
					Properties sessionProperties = new Properties();
					sessionProperties.setProperty("protocol", MailConstants.PROTOCOL_IMAP);
					sessionProperties.setProperty("type", "store");
					sessionProperties.setProperty("class", "com.sun.mail.imap.IMAPStore");
					sessionProperties.setProperty("vendor", "Sun Microsystems, Inc.");
					_session = Session.getInstance(sessionProperties);
					URLName urlname = new URLName(MailConstants.PROTOCOL_IMAP, _servername, _serverport, "", _username, _password);
					_store = new IMAPStore(_session, urlname);
				} else if (_serverprotocol.equals(MailConstants.PROTOCOL_POP3)) {
					Properties sessionProperties = new Properties();
					sessionProperties.setProperty("protocol", MailConstants.PROTOCOL_POP3);
					sessionProperties.setProperty("type", "store");
					sessionProperties.setProperty("class", "com.sun.mail.pop3.POP3Store");
					sessionProperties.setProperty("vendor", "Sun Microsystems, Inc.");
					_session = Session.getInstance(sessionProperties);
					URLName urlname = new URLName(MailConstants.PROTOCOL_POP3, _servername, _serverport, "", _username, _password);
					_store = new POP3Store(_session, urlname);
				} else {
					throw new MailException(MailConstants.ERROR_INVALID_PROTOCOL);
				}
			}

			if (!_store.isConnected()) {
				_store.connect();
			}
			_store.addConnectionListener(this);
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}	
	
	public void disconnect() throws MailException {
		try {
			if (_store != null) {
				_store.close();
				_store = null;
				_session = null;
			}
		} catch (MessagingException e) {
			throw new MailException(e);
		}
	}
	
	
	// impl. ConnectionListner
	
	/**
	 * @see javax.mail.event.ConnectionListener#opened(javax.mail.event.ConnectionEvent)
	 */
	public void opened(ConnectionEvent event) {
		logDebug(event);
	}
	
	/**
	 * @see javax.mail.event.ConnectionListener#disconnected(javax.mail.event.ConnectionEvent)
	 */
	public void disconnected(ConnectionEvent event) {
		logDebug(event);
	}
	
	/**
	 * @see javax.mail.event.ConnectionListener#closed(javax.mail.event.ConnectionEvent)
	 */
	public void closed(ConnectionEvent event) {
		logDebug(event);
	}
	
	
	// impl. toString
	
	/**
	 * @see Object#toString()
	 */
	public String toString() {
		return super.toString() + " ["
				+ "id=" + _storeid
				+ ",store=" + ((_store == null) ? null : _store.getURLName().toString())
				+ ",folder=" + _folderlist.size()
				+ "]";
	}
	
	
	// logging
	
	protected void log(Exception e) {
		logError(toString() + " - " + e.toString(), e);
	}
	
	protected void log(String msg) {
		logError(toString() + " - " + msg);
	}
	
	private final void logDebug(Object msg) {
		Lg.WORKER(_dbPoolName).debug(msg);
	}

	private final void logInfo(Object msg) {
		Lg.WORKER(_dbPoolName).info(msg);
	}
	
	private final void logWarning(Object msg) {
		Lg.WORKER(_dbPoolName).warn(msg);
	}

	private final void logWarning(Object msg, Exception e) {
		Lg.WORKER(_dbPoolName).warn(msg, e);
	}

	private final void logError(Object msg) {
		Lg.WORKER(_dbPoolName).error(msg);
	}
	
	private final void logError(Object msg, Exception e) {
		Lg.WORKER(_dbPoolName).error(msg, e);
	}
	
	
	// finalize
	
	protected void finalize() throws MailException {
		disconnect();
	}
}
