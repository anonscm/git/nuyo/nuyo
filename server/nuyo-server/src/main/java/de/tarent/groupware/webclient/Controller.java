/* $Id: Controller.java,v 1.3 2006/06/21 14:02:33 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.webclient;

/**
 * Dieser Worker definiert ein paar Globale Strings.
 * 
 * @author christoph
 */
public class Controller {

	/** Namen unter dennen Info's in der Session gespeichert werden. */
	public final static String SESSION_USERDATA_NAME = "userData";
	public final static String SESSION_USERINFO_NAME = "userInfo";
	public final static String SESSION_CONFIGSERVER_NAME = "configServer";
	public final static String SESSION_CONFIGWEBCLIENT_NAME = "configWebclient";
	public final static String SESSION_MAIL_CACHE_INBOX = "mailCacheInbox";
	public final static String SESSION_MAIL_CACHE_OUTBOX = "mailCacheInbox";
	public final static String SESSION_MAIL_CACHE_CURRENT = "mailCacheCurrent";

	/** Namen unter dennen Info's in der Datenbank gespeichert werden. */
	public final static String USERPARAM_INFO = "info";
	public final static String USERPARAM_CONFIG_SERVER = "config.server";
	public final static String UESRPARAM_CONFIG_WEBCLIENT = "config.webclient";

	/** Prefix'es unter dennen Info's in der Datenbank gespeichert werden. */
	public final static String USERPARAM_MAIL_PREFIX = "config.server.mail";
	public final static String USERPARAM_MAIL_LIST = "config.server.mail.list";
	public final static String USERPARAM_MAIL_LIST_IN = "config.server.mail.list.store";
	public final static String USERPARAM_MAIL_LIST_OUT = "config.server.mail.list.transport";

}
