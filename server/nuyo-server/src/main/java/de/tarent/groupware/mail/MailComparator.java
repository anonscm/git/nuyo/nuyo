/* $Id: MailComparator.java,v 1.2 2006/03/16 13:49:29 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.mail;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Hilfsklasse die das Sortieren von eMails �bernimmt.
 * 
 * Zu vergleichene Eintr�ge der Liste sind jeweils zwei Maps,
 * die Anhand eines Keys vergleicht werden sollen.
 * 
 * Arbeitet mit String und Date - Werten zusammen.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public class MailComparator implements Comparator {
	private String k;
	private boolean r;
	
	/**
	 * Key entspricht dem Key in der jeweiligen Map.
	 * Reverse gibt ab ob das Ergebnis umgedreht werden soll.
	 * 
	 * @param key
	 * @param reverse
	 */
	public MailComparator(String key, boolean reverse) {
		k = key;
		r = reverse;
	}

	public int compare(Object arg0, Object arg1) {
		int result = 0;
		Object o0 = ((Map)arg0).get(k);
		Object o1 = ((Map)arg1).get(k);
		if (o0 == null) return -1;
		if (o1 == null) return 1;
		if (o0 instanceof String) {
			result = ((String)o0).compareTo((String)o1);
		} else if (o0 instanceof Date) {
			result = ((Date)o0).compareTo((Date)o1);
		} else if (o0 instanceof List) {
			/**
			 * Sortiert nach einer eMail-Adress-Liste.
			 * 
			 * @see MailHelper.getAddressList(Address[] addresses)
			 */
			try {
				Map m0 = (Map)((List)o0).get(0);
				Map m1 = (Map)((List)o1).get(0);
				if (m0.containsKey(MailConstants.MAILADDRESS_NAME) && m1.containsKey(MailConstants.MAILADDRESS_NAME)) {
					result = ((String)m0.get(MailConstants.MAILADDRESS_NAME)).compareTo((String)m1.get(MailConstants.MAILADDRESS_NAME));
				} else {
					result = ((String)m0.get(MailConstants.MAILADDRESS_STRING)).compareTo((String)m1.get(MailConstants.MAILADDRESS_STRING));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("can not compare " + o0.getClass());
		}
		if (r) {
			return -result;
		} else {
			return result;
		}
	}
}
