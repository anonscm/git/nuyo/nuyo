package de.tarent.groupware.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tarent.contact.octopus.worker.constants.AddressWorkerConstants;


/**
 * @author kleinw
 *
 *	Hier k�nnen Listen in "^" - getrennte Strings verwandelt werden,
 *	um bei Massendaten den Soapcontent klein zu halten.
 *
 */
public class ResultTransform {

    final static public Object toSingleString(Object source, Map parameter) {
    if (parameter.containsKey(AddressWorkerConstants.PARAM_SINGLESTRINGRESULT)) {
            Boolean toString = (Boolean) parameter
                .get(AddressWorkerConstants.PARAM_SINGLESTRINGRESULT);
            StringBuffer result = new StringBuffer();
            if (source instanceof List) {
                for (Iterator it = ((List) source).iterator(); it.hasNext();) {
                    Object o = it.next();
                    if (o instanceof String && ((String)o).length() == 0)
                        result.append(" ");
                    else if (o == null)
                        result.append(" ");
                    else if (o instanceof List) {
                        for(Iterator it2 = ((List)o).iterator();it2.hasNext();) {
                            Object o2 = it2.next();
                            if(o2 == null)
                                result.append(" ");
                            else if (o2 instanceof String && ((String)o2).length() == 0)
                                result.append(" ");
                            else
                                result.append(o2);
                            if(it2.hasNext())
                                result.append("#");
                        }
                    }
                    else 
                        result.append(o.toString());
                    if (it.hasNext()) {
                        result.append("^");
                    }
                }
                return result.toString();
            } else return source; 
    	} else return source;
    } 
}
