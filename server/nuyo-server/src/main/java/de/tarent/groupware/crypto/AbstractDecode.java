/* $Id: AbstractDecode.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Iterator;

import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignature;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPBEEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;

/**
 * Abstrakte Klasse zum dekodieren von InputStreams.
 * 
 * @see #check()
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public abstract class AbstractDecode extends AbstractStream {
	private final int STATUS_NONE = 0;
	private final int STATUS_CHECK = 1;
	private final int STATUS_DECRYPT = 2;
	private final int STATUS_VERIFY = 3;

	private int status = STATUS_NONE;

	// Testet welche Daten der Stream enth�lt
	private boolean isChecked = false;
	private boolean hasPBEEncryptedData = false;
	private boolean hasPublicKeyEncryptedData = false;
	private boolean hasLiteralData = false;
	private boolean hasSignature = false;
	private boolean hasOnePassSignature = false;
	private boolean isDecrypted = false;
	private boolean isVerified = false;

	private InputStream verifyInputStream;

	private PGPSignature signature;
	private PGPOnePassSignature onePassSignature;

	/** Speichert die Key-ID des Public-Keys, der zum verifizieren notwenig, ist. */
	private Long publicKeyId;
	/** Speichert die Key-ID des Private-Keys, der zum entschl�sseln notwenig, ist. */
	private Long privateKeyId;

	public boolean isChecked() {
		return isChecked;
	}

	public boolean isDecrypted() {
		return isDecrypted;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public boolean hasData() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return hasPBEEncryptedData || hasPublicKeyEncryptedData || hasLiteralData;
	}

	public boolean hasSignature() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return hasSignature || hasOnePassSignature;
	}

	public boolean needPublicKey() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return hasSignature || hasOnePassSignature;
	}

	public boolean needPrivateKey() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return hasPublicKeyEncryptedData;
	}

	public boolean needPassphrase() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return hasPBEEncryptedData;
	}

	/** @return Gibt die Key-ID des Public-Keys, der zum verifizieren notwendig, zur�ck. */
	public Long getPublicKeyId() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return privateKeyId;
	}

	/** @return Gibt die Key-ID des Private-Keys, der zum entschl�ssln notwendig, zur�ck. */
	public Long getPrivateKeyId() throws GeneralSecurityException {
		if (!isChecked) throw new GeneralSecurityException("data not checked");
		return privateKeyId;
	}

	public void check() throws GeneralSecurityException, IOException {
		try {
			status = STATUS_CHECK;
			InputStream inputStream = dataSource.getInputStream();
			decode(inputStream);
			inputStream.close();
			isChecked = true;
		} catch (PGPException e) {
			GeneralSecurityException gse = new GeneralSecurityException(e.getLocalizedMessage());
			gse.setStackTrace(e.getStackTrace());
			throw gse;
		} finally {
			status = STATUS_NONE;
		}
	}

	protected void crypt() throws GeneralSecurityException, IOException {
		if (!hasData())
			throw new GeneralSecurityException("no encrypted data found");
		
		try {
			status = STATUS_DECRYPT;
			InputStream inputStream = dataSource.getInputStream();
			decode(inputStream);
			inputStream.close();
			isDecrypted = true;
		} catch (PGPException e) {
			GeneralSecurityException gse = new GeneralSecurityException(e.getLocalizedMessage());
			gse.setStackTrace(e.getStackTrace());
			throw gse;
		} finally {
			status = STATUS_NONE;
		}
	}

	public void verify(InputStream inputStream) throws GeneralSecurityException, IOException {
		if (!hasSignature())
			throw new GeneralSecurityException("no signature found");
		
		try {
			status = STATUS_VERIFY;
			if (hasSignature && hasOnePassSignature) {
				verify(inputStream, onePassSignature, signature);
			} else if (hasSignature && !hasOnePassSignature) {
				verify(inputStream, signature);
			} else {
				throw new GeneralSecurityException("no signature found");
			}
			isVerified = true;
		} catch (PGPException e) {
			GeneralSecurityException gse = new GeneralSecurityException(e.getLocalizedMessage());
			gse.setStackTrace(e.getStackTrace());
			throw gse;
		} finally {
			status = STATUS_NONE;
		}
	}

	protected final void decode(Object in) throws GeneralSecurityException, PGPException, IOException {
		if (in == null) {
			// nothing
		} else if (in instanceof InputStream) {
			InputStream decoderStream = PGPUtil.getDecoderStream((InputStream)in);
			decode(new PGPObjectFactory(decoderStream));
			decoderStream.close();
		} else if (in instanceof PGPObjectFactory) {
			PGPObjectFactory factory = (PGPObjectFactory)in;
			Object object = null;
			try {
				object = factory.nextObject();
			} catch (IOException e) {
			}
			while (object != null) {
				decode(object);
				try {
					object = factory.nextObject();
				} catch (IOException e) {
					object = null;
				}
			}
		} else if (in instanceof PGPCompressedData) {
			PGPCompressedData data = (PGPCompressedData)in;
			decode(data.getDataStream());
		} else if (in instanceof PGPEncryptedDataList) {
			Iterator it = ((PGPEncryptedDataList)in).getEncyptedDataObjects();
			while (it.hasNext()) {
				decode(it.next());
			}
		} else if (in instanceof PGPSignatureList) {
			PGPSignatureList list = (PGPSignatureList)in;
			for (int i = 0; true; i++) {
				try {
					decode((Object)list.get(i));
				} catch (IndexOutOfBoundsException e) {
					break;
				}
			}
		} else if (in instanceof PGPOnePassSignatureList) {
			PGPOnePassSignatureList list = (PGPOnePassSignatureList)in;
			for (int i = 0; true; i++) {
				try {
					decode((Object)list.get(i));
				} catch (IndexOutOfBoundsException e) {
					break;
				}
			}
		} else if (in instanceof PGPPBEEncryptedData) {
			if (status == STATUS_CHECK) {
				hasPBEEncryptedData = true;
			} else if (status == STATUS_DECRYPT) {
				decode((PGPPBEEncryptedData)in);
			}
		} else if (in instanceof PGPPublicKeyEncryptedData) {
			if (status == STATUS_CHECK) {
				privateKeyId = new Long(((PGPPublicKeyEncryptedData)in).getKeyID());
				hasPublicKeyEncryptedData = true;
			} else if (status == STATUS_DECRYPT) {
				decode((PGPPublicKeyEncryptedData)in);
			}
		} else if (in instanceof PGPLiteralData) {
			if (status == STATUS_CHECK) {
				hasLiteralData = true;
			} else if (status == STATUS_DECRYPT) {
				decode((PGPLiteralData)in);
			}
		} else if (in instanceof PGPSignature) {
			publicKeyId = new Long(((PGPSignature)in).getKeyID());
			hasSignature = true;
			signature = (PGPSignature)in;
		} else if (in instanceof PGPOnePassSignature) {
			publicKeyId = new Long(((PGPOnePassSignature)in).getKeyID());
			hasOnePassSignature = true;
			onePassSignature = (PGPOnePassSignature)in;
		} else {
			throw new IOException("found unknown type " + in.getClass().getName());
		}
	}

	protected abstract void decode(PGPPBEEncryptedData data) throws GeneralSecurityException, PGPException, IOException;
	protected abstract void decode(PGPPublicKeyEncryptedData data) throws GeneralSecurityException, PGPException, IOException;
	protected abstract void decode(PGPLiteralData data) throws GeneralSecurityException, PGPException, IOException;

	protected abstract void verify(InputStream inputStream, PGPSignature signature) throws GeneralSecurityException, PGPException, IOException;
	protected abstract void verify(InputStream inputStream, PGPOnePassSignature onePassSignature, PGPSignature signature) throws GeneralSecurityException, PGPException, IOException;
}
