/* $Id: Sign.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;

import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.PublicKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;

/**
 * Erstellt eine Signiertur mit einem Privaten-Key. 
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public class Sign extends AbstractStream {
	protected PGPPrivateKey privateKey;
	protected boolean armor = true;

	public void setInput(File file) throws IOException {
		this.dataSource = new FileDataSource(file);
	}

	public void setInput(DataSource dataSource) throws IOException {
		this.dataSource = dataSource;
	}

	public void setPrivateKey(PGPPrivateKey privateKey) throws GeneralSecurityException {
		this.privateKey = Crypto.assertKey(privateKey);
	}

	public void setArmor(boolean armor) {
		this.armor = armor;
	}

	protected void crypt() throws GeneralSecurityException, IOException {
		try {
			// Daten Base64 kodieren.
			OutputStream outputStream = this.outputStream;
			if (armor) {
				outputStream = new ArmoredOutputStream(outputStream);
			}
			
			// Signieren mit Private-Key
			PGPSignatureGenerator signatureGenerator =
				new PGPSignatureGenerator(PublicKeyAlgorithmTags.DSA, HashAlgorithmTags.SHA1, Crypto.PROVIDER);
			signatureGenerator.initSign(PGPSignature.BINARY_DOCUMENT, Crypto.assertKey(privateKey));
			
			int i;
			byte buffer[] = new byte[BUFFERSIZE];
			InputStream inputStream = dataSource.getInputStream();
			while ((i = inputStream.read(buffer)) >= 0) {
				signatureGenerator.update(buffer, 0, i);
			}
			inputStream.close();
			
			signatureGenerator.generate().encode(outputStream);
			
			if (armor) {
				outputStream.close();
			}
		} catch (PGPException e) {
			GeneralSecurityException gse = new GeneralSecurityException(e.getLocalizedMessage());
			gse.setStackTrace(e.getStackTrace());
			throw gse;
		}
	}

	protected void clear() {
	}
}