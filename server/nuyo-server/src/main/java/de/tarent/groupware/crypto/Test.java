/* $Id: Test.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Iterator;

import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;

public class Test {
	private static String PUBLICSTORE_FILE = "c:/gnupg/pubring.gpg";
	private static String PUBLICSTORE_PASS = null;
	private static String PRIVATESTORE_FILE = "c:/gnupg/secring.gpg";
	private static String PRIVATESTORE_PASS = null;
	private static String PRIVATE_PASS = "tarent";

	private static String USER = "Christoph (tarent) <christoph@tarent.de>";

	private static File SAMPLEPATH = new File("c:/gnupg/");
	private static File SAMPLEFILE = new File("c:/gnupg/message.txt");

	private static PGPPublicKey PUBLICKEY;
	private static PGPPrivateKey PRIVATEKEY;

	public static void main(String[] args) {
		try {
			Crypto.init();
			
			PUBLICKEY = getPublicKey(null, true);
			PRIVATEKEY = getPrivateKey(null);
			
			System.out.println("   public key  " + Long.toHexString(PUBLICKEY.getKeyID()).toUpperCase());
			System.out.println("  private key  " + Long.toHexString(PRIVATEKEY.getKeyID()).toUpperCase());
			System.out.println();
			
			if (false) {
				System.out.println("encrypt with public key");
				Encrypt encrypt = new Encrypt();
				encrypt.setInput(SAMPLEFILE);
				encrypt.setPublicKey(PUBLICKEY);
				encrypt.writeOutput(new FileOutputStream(new File(SAMPLEPATH, "message.txt.crypto.key")));
			}
			
			if (false) {
				System.out.println("encrypt with passphrase");
				Encrypt encrypt = new Encrypt();
				encrypt.setInput(SAMPLEFILE);
				encrypt.setPassphrase(PRIVATE_PASS.toCharArray());
				encrypt.writeOutput(new FileOutputStream(new File(SAMPLEPATH, "message.txt.crypto.pass")));
			}
			
			if (false) {
				System.out.println("sign with private key");
				Sign sign = new Sign();
				sign.setInput(SAMPLEFILE);
				sign.setPrivateKey(PRIVATEKEY);
				sign.writeOutput(new FileOutputStream(new File(SAMPLEPATH, "message.txt.crypto.sign")));
			}
			
			decode("message.txt.crypto.key");
			decode("message.txt.crypto.pass");
			decode("message.txt.crypto.sign");
			decode("message.txt.gnupg.sign");
			decode("message.txt.gnupg.clearsign");
			decode("message.txt.gnupg.detachsign");
			decode("message.txt.gnupg.pass");
			decode("message.txt.gnupg.key");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void decode(String file) {
		System.out.println("decode file " + file.toUpperCase());
		DataSource dataSource = new FileDataSource(new File(SAMPLEPATH, file));
		
		try {
			Decode decode = new Decode();
			decode.setInput(dataSource);
			decode.check();
			
			if (decode.needPublicKey())
				decode.setPublicKey(getPublicKey(decode.getPublicKeyId(), false));
			if (decode.needPrivateKey())
				decode.setPrivateKey(getPrivateKey(decode.getPrivateKeyId()));
			if (decode.needPassphrase())
				decode.setPassphrase(PRIVATE_PASS.toCharArray());
			
			if (decode.hasData()) {
				CompareOutputStream compare = new CompareOutputStream(SAMPLEFILE);
				decode.writeOutput(compare);
				if (compare.equal()) {
					System.out.println("  compare data: OK");
				} else {
					System.err.println("  compare data: ERROR");
				}
				if (decode.isIntegrityCheck()) {
					if (decode.isIntegrityVerify()) {
						System.out.println("  decrypt data: OK");
					} else {
						System.err.println("  decrypt data: ERROR");
					}
				} else {
					System.out.println("  decrypt data: UNKNOWN");
				}
			}
			if (decode.hasSignature()) {
				if (decode.hasData()) {
					System.out.println("  signature verify: self data");
					decode.verify(decode.readInput());
				} else {
					System.out.println("  signature verify: inputstream");
					decode.verify(new FileInputStream(SAMPLEFILE));
				}
				
				if (decode.isSignatureVerify()) {
					System.out.println("  signature verify: OK");
				} else {
					System.err.println("  signature verify: ERROR");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	public static PGPPublicKey getPublicKey(Long keyid, boolean encryptionKey) throws GeneralSecurityException, PGPException, IOException {
		if (keyid != null)
		System.out.println(Long.toHexString(keyid.longValue()));
		
		InputStream inputStream = new FileInputStream(PUBLICSTORE_FILE);
		PGPPublicKeyRingCollection store = new PGPPublicKeyRingCollection(inputStream);
		
		if (keyid != null) return store.getPublicKey(keyid.longValue());
		
		for (Iterator keyRings = store.getKeyRings(); keyRings.hasNext();) {
			PGPPublicKeyRing keyRing = (PGPPublicKeyRing) keyRings.next();
			
			for (Iterator it = keyRing.getPublicKeys(); it.hasNext();) {
				PGPPublicKey key = (PGPPublicKey)it.next();
				if (!encryptionKey || key.isEncryptionKey())
					return key;
			}
		}
		return null;
	}
	
	public static PGPPrivateKey getPrivateKey(Long keyid) throws GeneralSecurityException, PGPException, IOException {
		return getSecretKey(keyid).extractPrivateKey(PRIVATE_PASS.toCharArray(), Crypto.PROVIDER);
	}
	
	public static PGPSecretKey getSecretKey(Long keyid) throws GeneralSecurityException, PGPException, IOException {
		InputStream inputStream = new FileInputStream(PRIVATESTORE_FILE);
		PGPSecretKeyRingCollection store = new PGPSecretKeyRingCollection(inputStream);
		
		if (keyid != null) return store.getSecretKey(keyid.longValue());
		
		for (Iterator keyRings = store.getKeyRings(); keyRings.hasNext();) {
			PGPSecretKeyRing keyRing = (PGPSecretKeyRing) keyRings.next();
			
			for (Iterator it = keyRing.getSecretKeys(); it.hasNext(); ) {
				PGPSecretKey key = (PGPSecretKey)it.next();
				return key;
			}
		}
		return null;
	}

	private static class CompareOutputStream extends OutputStream {
		private InputStream inputStream;
		private boolean equal = true;

		public CompareOutputStream(File file) throws FileNotFoundException {
			this.inputStream = new FileInputStream(file);
		}

		public CompareOutputStream(InputStream inputStream) {
			this.inputStream = inputStream;
		}

		public void write(int b) throws IOException {
			equal &= ((byte)b) == ((byte)inputStream.read());
		}

		public boolean equal() throws IOException {
			return equal && (inputStream.available() <= 0);
		}
	}
}