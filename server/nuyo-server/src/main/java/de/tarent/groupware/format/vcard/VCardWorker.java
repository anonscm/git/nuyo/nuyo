package de.tarent.groupware.format.vcard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.tarent.pdc.contact.Address;
import de.tarent.pdc.contact.Email;
import de.tarent.pdc.contact.ExplanatoryInformation;
import de.tarent.pdc.contact.Identification;
import de.tarent.pdc.contact.InternetResource;
import de.tarent.pdc.contact.Organization;
import de.tarent.pdc.contact.Telephone;
import de.tarent.pdc.format.PropertyCollection;
import de.tarent.pdc.format.VCard;
import de.tarent.pdc.formatter.VCardFormatter;

public class VCardWorker {
    
    public static String convertAddressMapToVCard (Map mapAddress, String vCardVersion) {

        // Deklaration und Initialisierung der Objekte ---------------------------------        
        
        VCard vCard = new VCard();        
        Identification identification = new Identification();
        Address address1 = new Address();
        Address address2 = new Address();
        Address address3 = new Address();
        Telephone telephone1 = new Telephone();
        Telephone telephone2 = new Telephone();
        Telephone telephone3 = new Telephone();
        Telephone telephone4 = new Telephone();
        Telephone telephone5 = new Telephone();
        Telephone telephone6 = new Telephone();
        Email email1 = new Email();
        Email email2 = new Email();
        InternetResource url = new InternetResource();
        Organization organization = new Organization();
        ExplanatoryInformation explInformation = new ExplanatoryInformation();
        
        PropertyCollection propertyCollection = new PropertyCollection();        

        // Formatierter Name -----------------------------------------------------------
        
        /*String[] arrayFormattedName = { (String) mapAddress.get(TaddressDB.TITELACADEMIC),
                                        (String) mapAddress.get(TaddressDB.FIRSTNAME),
                                        (String) mapAddress.get(TaddressDB.MIDDLENAME),
                                        (String) mapAddress.get(TaddressDB.NAMESUFFIX),
                                        (String) mapAddress.get(TaddressDB.LASTNAME) };*/
        
        String[] arrayFormattedName = new String[] {    (String) mapAddress.get("akadTitle"),
                                                        (String) mapAddress.get("vorname"),
                                                        (String) mapAddress.get("nameErweitert"),
                                                        (String) mapAddress.get("namenszusatz"),
                                                        (String) mapAddress.get("nachname") };
        
        identification.setFormattedName(concat(arrayFormattedName, " "));
        
        // Name + Titel ----------------------------------------------------------------
        
        identification.setHonorificPrefixes((String) mapAddress.get("akadTitle"))
                        .setGivenName((String) mapAddress.get("vorname"))
                        .setAdditionalNames((String) mapAddress.get("nameErweitert"))
                        .setFamilyName((String) mapAddress.get("nachname"))
                        .setHonorificSuffixes((String) mapAddress.get("namenszusatz"))
                        .setNickname((String) mapAddress.get("spitzname"));

        // Adresse 1 -------------------------------------------------------------------
                
        String strasseHausnummer = concat((String) mapAddress.get("stra�e"), (String) mapAddress.get("hausNr"));
        
        String postfach = concat((String) mapAddress.get("postfachNr2"), (String) mapAddress.get("postfachNr"));
        
        address1.setStreetAddress(strasseHausnummer)
                .setPostalCode((String) mapAddress.get("plz"))
                .setLocality((String) mapAddress.get("ort"))
                .setRegion((String) mapAddress.get("bundesland"))
                .setCountry((String) mapAddress.get("land"))
                .setPostOfficeBox(postfach)
                //.setLabel("Label Beispiel\r\nStra�e 123\r\nPLZ Ort")
                .setTypePreferred(true)
                .setTypeWork(true);
        
        // Adresse 2 -------------------------------------------------------------------

        strasseHausnummer = concat((String) mapAddress.get("stra�e2"), (String) mapAddress.get("hausNr2"));
        
        postfach = concat((String) mapAddress.get("postfachNr2_2"), (String) mapAddress.get("postfachNr_2"));
                
        address2.setStreetAddress(strasseHausnummer)
                .setPostalCode((String) mapAddress.get("plz2"))
                .setLocality((String) mapAddress.get("ort2"))
                .setRegion((String) mapAddress.get("bundesland2"))
                .setCountry((String) mapAddress.get("land2"))
                .setPostOfficeBox(postfach);

        // Adresse 3 -------------------------------------------------------------------
        
        strasseHausnummer = concat((String) mapAddress.get("stra�e3"), (String) mapAddress.get("hausNr3"));
        
        postfach = concat((String) mapAddress.get("postfachNr2_3"), (String) mapAddress.get("postfachNr_3"));
                
        address3.setStreetAddress(strasseHausnummer)
                .setPostalCode((String) mapAddress.get("plz3"))
                .setLocality((String) mapAddress.get("ort3"))
                .setRegion((String) mapAddress.get("bundesland3"))
                .setCountry((String) mapAddress.get("land3"))
                .setPostOfficeBox(postfach);
        
        // Telefon ---------------------------------------------------------------------
        
        telephone1.setTelephone((String) mapAddress.get("telefon"))
                    .setTypeWork(true);
        telephone2.setTelephone((String) mapAddress.get("fax"))
                    .setTypeFax(true)
                    .setTypeWork(true);
        telephone3.setTelephone((String) mapAddress.get("mobiltelefon"))
                    .setTypeCell(true);
        telephone4.setTelephone((String) mapAddress.get("telefonHeim"))
                    .setTypeVoice(true)
                    .setTypeHome(true);
        telephone5.setTelephone((String) mapAddress.get("faxHeim"))
                    .setTypeFax(true)
                    .setTypeHome(true);                    
        telephone6.setTelephone((String) mapAddress.get("mobiltelefonHeim"))
                    .setTypeCell(true)
                    .setTypeHome(true);
        
        // Email -----------------------------------------------------------------------
        
        email1.setEmail((String) mapAddress.get("eMailAdresse"))
                .setTypePreferred(true)
                .setTypeWork(true);
        email2.setEmail((String) mapAddress.get("eMailAdresseHeim"))
                .setTypeHome(true);
        
        // Internet Resource (URL) -----------------------------------------------------
        
        url.setURL((String) mapAddress.get("homepage"));

        // Organisation ----------------------------------------------------------------
                
        organization.setOrganization((String) mapAddress.get("institution"));
        
        List departments = new ArrayList();
        departments.add((String) mapAddress.get("institutionErweitert"));
        
        organization.setUnits(departments);

        // Weitere Eigenschaften -------------------------------------------------------
        
        explInformation.setVersion(vCardVersion);
        
        // -----------------------------------------------------------------------------
        
        propertyCollection.addProperty(identification)
                            .addProperty(address1)
                            .addProperty(address2)
                            .addProperty(address3)
                            .addProperty(telephone1)
                            .addProperty(telephone2)
                            .addProperty(telephone3)
                            .addProperty(telephone4)
                            .addProperty(telephone5)
                            .addProperty(telephone6)
                            .addProperty(email1)
                            .addProperty(email2)
                            .addProperty(url)
                            .addProperty(organization)
                            .addProperty(explInformation);
        
        // -----------------------------------------------------------------------------
        
        // -----------------------------------------------------------------------------
        
        // -----------------------------------------------------------------------------
        
        vCard.setPropertyCollection(propertyCollection);
        
        VCardFormatter formatter = new VCardFormatter();
        
        String vCardString = "";
        
        try {
            vCardString = formatter.format(vCard);
        } catch (Exception e) {
            System.out.println("verdammt");
            e.printStackTrace();
            // TODO
        }
        
        // TODO CATEGORIES
        
        // -----------------------------------------------------------------------------
        
        return vCardString;
    }
    
    private static String concat(String[] arrayConcat, String sSeparator) {
        StringBuffer concat = new StringBuffer();
        boolean bIsFirstItem = true;
        
        for (int i=0; i<arrayConcat.length; i++) {
            if (arrayConcat[i] != null && arrayConcat[i].length() > 0) {
                if (bIsFirstItem) {
                    bIsFirstItem = false;
                    concat.append(arrayConcat[i]);
                }                
                else
                    concat.append(sSeparator).append(arrayConcat[i]);
            }                  
        }
        
        return concat.toString();
    }
    
    private static String concat(String firstString, String secondString) {
        if (firstString != null && firstString.length() > 0)
            if (secondString != null && secondString.length() > 0)
                return firstString + " " + secondString;
            else
                return firstString;
        else if (secondString != null && secondString.length() > 0)
            return secondString;
        else
            return "";
    }
}
