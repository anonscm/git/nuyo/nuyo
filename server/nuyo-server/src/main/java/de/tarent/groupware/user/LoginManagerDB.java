/* $Id: LoginManagerDB.java,v 1.7 2006/10/31 12:13:05 kirchner Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Sebastian Mancke and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.groupware.user;

import java.net.PasswordAuthentication;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.commons.utils.StringTools;
import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Clause;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.octopus.config.TcCommonConfig;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.security.AbstractLoginManager;
import de.tarent.octopus.security.TcSecurityException;
import de.tarent.octopus.server.PersonalConfig;
import de.tarent.octopus.server.UserManager;

/** 
 * Implementierung eines LoginManagers, �ber die Tarent-Contact DB direkt.
 * 
 * @author Sebastian Mancke
 */
public class LoginManagerDB extends AbstractLoginManager {
	
    static Logger logger = Logger.getLogger(LoginManagerDB.class.getName());

    public static String PARAM_CHECK_PASSWORD = "checkPassword";

    protected void doLogin(TcCommonConfig commonConfig, PersonalConfig pConfig, TcRequest tcRequest) 
        throws TcSecurityException {
       
        PasswordAuthentication pwdAuth = tcRequest.getPasswordAuthentication();
        if (pwdAuth == null)
            throw new TcSecurityException(TcSecurityException.ERROR_AUTH_ERROR);

       
        Clause where = null;
        String paramCheckPassword = getConfigurationString(PARAM_CHECK_PASSWORD);
        boolean checkPassword = paramCheckPassword == null || (new Boolean(paramCheckPassword)).booleanValue();
        if (checkPassword)
			try {
				where = Where.and(Expr.equal(TuserDB.LOGINNAME, pwdAuth.getUserName()),
				                  Expr.equal(TuserDB.PWD, StringTools.md5(new String(pwdAuth.getPassword())) ));
			} catch (NoSuchAlgorithmException e) {
				throw new TcSecurityException(e.getLocalizedMessage());
			}
		else 
            where = Expr.equal(TuserDB.LOGINNAME, pwdAuth.getUserName());
                
        Select select2 = SQL.Select(TcDBContext.getDefaultContext())
            .from(TuserDB.getTableName())
            .join(TaddressDB.getTableName(), TaddressDB.FKUSER, TuserDB.PK_PKUSER)
            .join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS)
            .add(TuserDB.PK_PKUSER, Integer.class)
            .add(TuserDB.LASTNAME, String.class)
            .add(TuserDB.FIRSTNAME, String.class)
            .add(TaddressextDB.EMAIL, String.class);
            
        Select select = ((Select)select2.clone())
            .where(where);

        try {
            ResultSet result = select.executeSelect(TcDBContext.getDefaultContext()).resultSet();
            if (!result.next()){
            	result = select2.add(TuserDB.ISPWDENCRYPTED, Integer.class).where(Expr.equal(TuserDB.LOGINNAME, pwdAuth.getUserName())).executeSelect(TcDBContext.getDefaultContext()).resultSet();
            	if(result.next()){
            		//testen, ob passwort nicht encrypted
            		int ispwdencrypted = result.getInt(5);
            		if(ispwdencrypted!=1){
            			UserManagerDB um = new UserManagerDB();
            			um.setModuleName(tcRequest.getModule());
            			um.passwdfix(pwdAuth.getUserName());
            			result = select.executeSelect(TcDBContext.getDefaultContext()).resultSet();
            			if(!result.next()){
            				throw new TcSecurityException(TcSecurityException.ERROR_AUTH_ERROR);
            			}
            		}else{
            			throw new TcSecurityException(TcSecurityException.ERROR_AUTH_ERROR);
            		}
            	}
            }
            
            pConfig.setUserID(new Integer(result.getInt(1)));
            pConfig.setUserLastName(result.getString(2));
            pConfig.setUserGivenName(result.getString(3));
            pConfig.setUserEmail(result.getString(4));
            
            pConfig.setUserGroups(new String[]{PersonalConfig.GROUP_USER});
            pConfig.userLoggedIn(pwdAuth.getUserName());
            DB.close(result);
        } catch (SQLException sqle) {
            logger.log(Level.SEVERE, "Fehler beim Testen eines Benutzers in der DB", sqle);
            throw new TcSecurityException(TcSecurityException.ERROR_SERVER_AUTH_ERROR);
        }            
    }
    
    protected void doLogout(TcCommonConfig commonConfig, PersonalConfig pConfig, TcRequest tcRequest)
        throws TcSecurityException {
        pConfig.setUserGroups(new String[]{PersonalConfig.GROUP_LOGGED_OUT});
        pConfig.userLoggedOut();
    }


	public boolean isUserManagementSupported() {
		return true;
	}

	public UserManager getUserManager() {
		return new UserManagerDB();
	}

}