package de.tarent.groupware.address;


import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressactionDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.persistence.AbstractDBMapping;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.groupware.Address;

/**
 *
 * TODO: Addresszugriff: include DETAILED_ADDRESS_FIELDS and the commonXXX fields
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class AddressDBMapping extends AbstractDBMapping {

    public static final String STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES = "stmtSelectAllForUserWithSubcategories";
    public static final String STMT_SELECT_ALL_FROM_TADDRESSACTION = "stmtSelectAllFromTaddressaction";
    public static final String STMT_SELECT_COUNT = "stmtSelectCount";
    
    public static final String PARAM_SELECTING_USER_ID = "selectingUserId";
    public static final String PARAM_SELECTING_ACTION_ID = "selectingActionId";
    public static final String PARAM_SELECTING_ACTION_NOTE = "selectingActionNote";
    
    
    protected static final int COMMON_ADDRESS_FIELDS = 128;
    protected static final int DETAILED_ADDRESS_FIELDS = 256;

	DBContext initialDBC;
    public AddressDBMapping(DBContext dbc) {
        super(dbc);
        initialDBC = dbc;
    }

	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDBMapping#getTargetTable()
	 */
	public String getTargetTable() {
		return TaddressDB.getTableName();
	}
	
	/* (non-Javadoc)
	 * @see de.tarent.dblayer.persistence.AbstractDBMapping#configureMapping()
	 */
	public void configureMapping() {		

        // pks and references
        addField(TaddressDB.PK_PKADDRESS, Address.PROPERTY_ID, COMMON_FIELDS | MINIMAL_FIELDS | PRIMARY_KEY_FIELDS);
        addField(TaddressDB.FKUSER, Address.PROPERTY_ASSOCIATED_USER_ID);

        // name fields
        addField(TaddressDB.LASTNAME, Address.PROPERTY_NACHNAME, COMMON_FIELDS | WRITEABLE_FIELDS | MINIMAL_FIELDS);
        addField(TaddressDB.FIRSTNAME, Address.PROPERTY_VORNAME, COMMON_FIELDS | WRITEABLE_FIELDS | MINIMAL_FIELDS);
        addField(TaddressDB.MIDDLENAME, Address.PROPERTY_WEITERE_VORNAMEN);
        addField(TaddressDB.NICKNAME, Address.PROPERTY_SPITZNAME);
        addField(TaddressDB.NAMESUFFIX, Address.PROPERTY_NAMENSZUSATZ);
        addField(TaddressDB.SALUTATION, Address.PROPERTY_HERRN_FRAU); // e.g. Herr, Frau, ..
        addField(TaddressDB.LETTERSALUTATIONSHORT, Address.PROPERTY_ANREDE); // e.g. Sehr geehrter, lieber, ..
        addField(TaddressDB.TITELACADEMIC, Address.PROPERTY_AKAD_TITEL);

        // institution fields
        addField(TaddressDB.DEPARTMENT, Address.PROPERTY_ABTEILUNG);
        addField(TaddressDB.POSITION, Address.PROPERTY_POSITION);
        addField(TaddressDB.TITELJOB, Address.PROPERTY_TITEL);

        // other address fields
        addField(TaddressDB.BANKNO, Address.PROPERTY_BANK_CODE);
        addField(TaddressDB.BANKACCOUNT, Address.PROPERTY_BANK_KONTO);
        addField(TaddressDB.BANKNAME, Address.PROPERTY_BANK_NAME);
        addField(TaddressDB.DATEOFBIRTH, Address.PROPERTY_GEBURTSDATUM);
        addField(TaddressDB.YEAROFBIRTH, Address.PROPERTY_GEBURTSJAHR);
        addField(TaddressDB.SEX, Address.PROPERTY_GESCHLECHT);
        addField(TaddressDB.NOTEONADDRESS, Address.PROPERTY_NOTIZ);//, DETAILED_ADDRESS_FIELDS);
        addField(TaddressDB.PICTURE, Address.PROPERTY_PICTURE, DETAILED_ADDRESS_FIELDS);//, DETAILED_ADDRESS_FIELDS);
        addField(TaddressDB.PICTUREEXPRESSION, Address.PROPERTY_PICTUREEXPRESSION);
        addField(TaddressextDB.LABELFN, Address.PROPERTY_SHORT_ADDRESS_LABEL, COMMON_FIELDS);

        addField(TaddressDB.BLOCKEDFORCONSIGNMENT, Address.PROPERTY_BLOCKEDFORCONSIGNMENT);
        
        // change and creation date
        addField(TaddressDB.CHANGED, Address.PROPERTY_AENDERUNGSDATUM, COMMON_FIELDS);
        addField(TaddressDB.CREATED, Address.PROPERTY_ERFASSUNGSDATUM, COMMON_FIELDS);
        
        // auto generateable fields
        addField(TaddressDB.LETTERADDRESS, Address.PROPERTY_LETTERADDRESS);//, DETAILED_ADDRESS_FIELDS);
        addField(TaddressDB.LETTERADDRESSAUTO, Address.PROPERTY_LETTER_ADDRESS_AUTO);
            
        addField(TaddressDB.LETTERSALUTATION, Address.PROPERTY_LETTERSALUTATION);
        addField(TaddressDB.LETTERSALUTATIONAUTO, Address.PROPERTY_LETTER_SALUTATION_AUTO);

        // followup (wiedervorlage)
        addField(TaddressDB.FOLLOWUP, Address.PROPERTY_FOLLOWUP);
        addField(TaddressDB.FOLLOWUPDATE, Address.PROPERTY_FOLLOWUPDATE);
        addField(TaddressDB.FKUSERFOLLOWUP, Address.PROPERTY_FK_USER_FOLLOWUP);

        // 1. communication "dienstlich"
        addField(TaddressDB.REGION, Address.PROPERTY_BUNDESLAND);
        addField(TaddressDB.HOUSENO, Address.PROPERTY_HAUS_NR);
        addField(TaddressDB.ORGANISATION, Address.PROPERTY_INSTITUTION);
        addField(TaddressDB.COUNTRY, Address.PROPERTY_LAND);
        addField(TaddressDB.COUNTRYSHORT, Address.PROPERTY_LKZ);
        addField(TaddressDB.CITY, Address.PROPERTY_ORT);
        addField(TaddressDB.ZIPCODE, Address.PROPERTY_PLZ);
        addField(TaddressDB.POBOX, Address.PROPERTY_POSTFACH_NR);
        addField(TaddressDB.POBOXZIPCODE, Address.PROPERTY_PLZ_POSTFACH);
        addField(TaddressDB.STREET, Address.PROPERTY_STRASSE);
        addField(TaddressDB.ADDRESSSUFFIX, Address.PROPERTY_ADDRESSZUSATZ);
        addField(TaddressextDB.FAX, Address.PROPERTY_FAX_DIENSTLICH, COMMON_FIELDS);
        addField(TaddressextDB.MOBILE, Address.PROPERTY_HANDY_DIENSTLICH, COMMON_FIELDS);
        addField(TaddressextDB.EMAIL, Address.PROPERTY_E_MAIL_DIENSTLICH, COMMON_FIELDS);
        addField(TaddressextDB.URL, Address.PROPERTY_HOMEPAGE_DIENSTLICH, COMMON_FIELDS);
        addField(TaddressextDB.FON, Address.PROPERTY_TELEFON_DIENSTLICH, COMMON_FIELDS);

        // 2. communication "privat"/"home"
        addField(TaddressDB.REGION2, Address.PROPERTY_BUNDESLAND2);
        addField(TaddressDB.HOUSENO2, Address.PROPERTY_HAUS_NR2);
        // no "organisation for this private address
        addField(TaddressDB.COUNTRY2, Address.PROPERTY_LAND2);
        addField(TaddressDB.COUNTRYSHORT2, Address.PROPERTY_LKZ2);
        addField(TaddressDB.CITY2, Address.PROPERTY_ORT2);
        addField(TaddressDB.ZIPCODE2, Address.PROPERTY_PLZ2);
        addField(TaddressDB.POBOX2, Address.PROPERTY_PLZ_POSTFACH2);
        addField(TaddressDB.POBOXZIPCODE2, Address.PROPERTY_POSTFACH_NR2);
        addField(TaddressDB.STREET2, Address.PROPERTY_STRASSE2);
        addField(TaddressDB.ADDRESSSUFFIX2, Address.PROPERTY_ADDRESSZUSATZ2);
        addField(TaddressextDB.EMAILHOME, Address.PROPERTY_E_MAIL_PRIVAT, COMMON_FIELDS);
        addField(TaddressextDB.FAXHOME, Address.PROPERTY_FAX_PRIVAT, COMMON_FIELDS);
        addField(TaddressextDB.MOBILEHOME, Address.PROPERTY_HANDY_PRIVAT, COMMON_FIELDS);
        addField(TaddressextDB.FONHOME, Address.PROPERTY_TELEFON_PRIVAT, COMMON_FIELDS);
        addField(TaddressextDB.URLHOME, Address.PROPERTY_HOMEPAGE_PRIVAT, COMMON_FIELDS);


        // 3. communication
        addField(TaddressDB.REGION3, Address.PROPERTY_BUNDESLAND3);
        addField(TaddressDB.HOUSENO3, Address.PROPERTY_HAUS_NR3);
        addField(TaddressDB.ORGANISATION2, Address.PROPERTY_INSTITUTION_WEITERE);
        addField(TaddressDB.COUNTRY3, Address.PROPERTY_LAND3);
        addField(TaddressDB.COUNTRYSHORT3, Address.PROPERTY_LKZ3);
        addField(TaddressDB.CITY3, Address.PROPERTY_ORT3);
        addField(TaddressDB.ZIPCODE3, Address.PROPERTY_PLZ3);
        addField(TaddressDB.POBOX3, Address.PROPERTY_PLZ_POSTFACH3);
        addField(TaddressDB.POBOXZIPCODE3, Address.PROPERTY_POSTFACH_NR3);
        addField(TaddressDB.STREET3, Address.PROPERTY_STRASSE3);
        addField(TaddressDB.ADDRESSSUFFIX3, Address.PROPERTY_ADDRESSZUSATZ3);
        addField(TaddressextDB.EMAIL3, Address.PROPERTY_E_MAIL_WEITERE, COMMON_FIELDS);
        addField(TaddressextDB.FAX3, Address.PROPERTY_FAX_WEITERES, COMMON_FIELDS);
        addField(TaddressextDB.MOBILE3, Address.PROPERTY_HANDY_WEITERES, COMMON_FIELDS);
        addField(TaddressextDB.FON3, Address.PROPERTY_TELEFON_WEITERES, COMMON_FIELDS);
        addField(TaddressextDB.URL3, Address.PROPERTY_HOMEPAGE_WEITERE, COMMON_FIELDS);


        // common fields
        addField(TaddressDB.COMMONTEXT01, Address.PROPERTY_COMMONTEXT01, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT02, Address.PROPERTY_COMMONTEXT02, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT03, Address.PROPERTY_COMMONTEXT03, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT04, Address.PROPERTY_COMMONTEXT04, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT05, Address.PROPERTY_COMMONTEXT05, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT06, Address.PROPERTY_COMMONTEXT06, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT07, Address.PROPERTY_COMMONTEXT07, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT08, Address.PROPERTY_COMMONTEXT08, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT09, Address.PROPERTY_COMMONTEXT09, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT10, Address.PROPERTY_COMMONTEXT10, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT11, Address.PROPERTY_COMMONTEXT11, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT12, Address.PROPERTY_COMMONTEXT12, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT13, Address.PROPERTY_COMMONTEXT13, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT14, Address.PROPERTY_COMMONTEXT14, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT15, Address.PROPERTY_COMMONTEXT15, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT16, Address.PROPERTY_COMMONTEXT16, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT17, Address.PROPERTY_COMMONTEXT17, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT18, Address.PROPERTY_COMMONTEXT18, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT19, Address.PROPERTY_COMMONTEXT19, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONTEXT20, Address.PROPERTY_COMMONTEXT20, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT01, Address.PROPERTY_COMMONINT01, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT02, Address.PROPERTY_COMMONINT02, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT03, Address.PROPERTY_COMMONINT03, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT04, Address.PROPERTY_COMMONINT04, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT05, Address.PROPERTY_COMMONINT05, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT06, Address.PROPERTY_COMMONINT06, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT07, Address.PROPERTY_COMMONINT07, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT08, Address.PROPERTY_COMMONINT08, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT09, Address.PROPERTY_COMMONINT09, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONINT10, Address.PROPERTY_COMMONINT10, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL01, Address.PROPERTY_COMMONBOOL01, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL02, Address.PROPERTY_COMMONBOOL02, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL03, Address.PROPERTY_COMMONBOOL03, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL04, Address.PROPERTY_COMMONBOOL04, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL05, Address.PROPERTY_COMMONBOOL05, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL06, Address.PROPERTY_COMMONBOOL06, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL07, Address.PROPERTY_COMMONBOOL07, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL08, Address.PROPERTY_COMMONBOOL08, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL09, Address.PROPERTY_COMMONBOOL09, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONBOOL10, Address.PROPERTY_COMMONBOOL10, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE01, Address.PROPERTY_COMMONDATE01, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE02, Address.PROPERTY_COMMONDATE02, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE03, Address.PROPERTY_COMMONDATE03, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE04, Address.PROPERTY_COMMONDATE04, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE05, Address.PROPERTY_COMMONDATE05, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE06, Address.PROPERTY_COMMONDATE06, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE07, Address.PROPERTY_COMMONDATE07, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE08, Address.PROPERTY_COMMONDATE08, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE09, Address.PROPERTY_COMMONDATE09, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONDATE10, Address.PROPERTY_COMMONDATE10, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY01, Address.PROPERTY_COMMONMONEY01, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY02, Address.PROPERTY_COMMONMONEY02, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY03, Address.PROPERTY_COMMONMONEY03, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY04, Address.PROPERTY_COMMONMONEY04, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY05, Address.PROPERTY_COMMONMONEY05, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY06, Address.PROPERTY_COMMONMONEY06, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY07, Address.PROPERTY_COMMONMONEY07, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY08, Address.PROPERTY_COMMONMONEY08, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY09, Address.PROPERTY_COMMONMONEY09, COMMON_ADDRESS_FIELDS);
        addField(TaddressDB.COMMONMONEY10, Address.PROPERTY_COMMONMONEY10, COMMON_ADDRESS_FIELDS);

        addQuery(STMT_SELECT_ONE, 
                 createBasicSelectOne()
                 .join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS),
                 COMMON_FIELDS);
        
        addQuery(STMT_SELECT_ALL_FOR_USER_WITH_SUBCATEGORIES, 
                 createBasicSelectAll()
                 .join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS),
                 COMMON_FIELDS);

        
        Select actionSubSelect = 
            SQL.Select(initialDBC).selectAs(TaddressactionDB.FKADDRESS)       
            .from(TaddressactionDB.getTableName())
            .whereAndEq(TaddressactionDB.FKACTION, new ParamValue(PARAM_SELECTING_ACTION_ID))
            .whereAndEq(TaddressactionDB.NOTE, new ParamValue(PARAM_SELECTING_ACTION_NOTE));
  
        addQuery(STMT_SELECT_ALL_FROM_TADDRESSACTION, 
                 createBasicSelectAll()
                 .join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS)                 
                 .whereAnd( Expr.in(TaddressDB.PK_PKADDRESS, new SubSelect(initialDBC, actionSubSelect)) ),
                 COMMON_FIELDS);

        addQuery(STMT_SELECT_COUNT, 
                createBasicSelectAll().selectAs("count(*)"),
                0x00);
	}
}