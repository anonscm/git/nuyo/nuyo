package de.tarent.groupware;

public interface Constants {

    public static final String P_POOL = "CONFIG:pool";

    public static final String STATUS_INSERT_DONE = "INSERT_DONE";
    public static final String STATUS_UPDATE_DONE = "UPDATE_DONE";
    public static final String STATUS_UNAUTHORIZED_ADDRESSASSIGN = "UNAUTHORIZED_ADDRESSASSIGN";
    public static final String STATUS_IMPLICIT_DELETION_DETECTED = "IMPLICIT_DELETION_DETECTED";

}