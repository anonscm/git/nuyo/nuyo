/* $Id: Mail.java,v 1.3 2006/09/15 13:00:03 kirchner Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.webclient;

//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import de.tarent.contact.octopus.DBDataAccessPostgres;
import de.tarent.contact.octopus.DataAccess;
import de.tarent.contact.octopus.MailService;
import de.tarent.contact.util.FieldMaker;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * Dieser Worker bearbeitet den Content fuer die korrekte
 * Anzeige im WebClient. Umlaute nach HTML-Standart konvertieren, etc.
 * 
 * @author christoph
 */
public class Mail implements TcContentWorker {
	/**
	 * Dieser Logger soll f�r Ausgaben im Octopus-Kontext benutzt werden.
	 */
	public final static Logger logger = Logger.getLogger(Mail.class.getName());

	private static boolean isEmpty(TcRequest tcRequest, String id) {
		String s = tcRequest.get(id);
		return s == null || s.equals("");
	}

	private static boolean isNumber(TcRequest tcRequest, String id) {
		String s = tcRequest.get(id);
		if (s == null)
			return false;
		if (s.equals(""))
			return true;
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
	}

	public String doAction(TcConfig tcConfig, String actionName, TcRequest tcRequest, TcContent tcContent) throws TcContentProzessException {

			String result = RESULT_error;
			DataAccess dataAccess = new DBDataAccessPostgres();
			MailService mailService = new MailService();

			if (actionName.equals(ACTION_MAIL_SEND)) {
				// Absenderdaten holen
				String userLogin = tcContent.getAsString("user");
				// getParamAsString(tcRequest, tcContent, "user");
				Map userProperties = dataAccess.getUserProperties(dataAccess.getUserId(userLogin), "info");
				Map params = new TreeMap();

				String mailSend = tcRequest.get("mailSend");
				String mimeType = tcRequest.get("mimetype");
				String textInput = tcRequest.get("text");
				String textOutput = null;

				params.put("subject", tcRequest.get("subject"));
				if (mimeType == null) {
					params.put("mimetype", "text/plain");
				} else {
					params.put("mimetype", mimeType);
				}
				if (userProperties != null && userProperties.containsKey("info.email") && userProperties.containsKey("info.fullname")) {
					params.put("frommail", userProperties.get("info.email"));
					params.put("fromname", userProperties.get("info.fullname"));
				} else {
					params.put("frommail", userLogin + "@localhost");
					params.put("fromname", null);
				}

				if (mailSend == null) {
				} else if (mailSend.equalsIgnoreCase("ByUser")) {
					// parameter eine eMail-Adresse
					String to = tcRequest.get("to");
					textOutput = textInput;

					// eMail versenden.
					params.put("to", to);
					params.put("text", textOutput);
					tcContent.setField("mailsend", params);
					mailService.doAction(tcConfig, "mailSend", tcRequest, tcContent);
				} else if (mailSend.equalsIgnoreCase("ByAddress")) {
					// parameter eine ID einer Adresse
				} else if (mailSend.equalsIgnoreCase("ByMailbatch")) {
					// parameter eine AddressListe
					String to = null;
					Map addresses = (Map)tcContent.getAsObject("addresses");
					if (addresses != null && addresses.get("address") != null) {
						Map address;
						List addressList = (List)addresses.get("address");

						// Adressliste durchgehen.
						for (int i = 0; i < addressList.size(); i++) {
							address = (Map) ((List)addressList.get(i)).get(1);
							if (address != null) {
								if (address.containsKey("a22") && ((Map)address.get("a22")).containsKey("108")) {
									to = (String) ((Map)address.get("a22")).get("108");
									textOutput = formatMail(textInput, address);

									// eMail versenden.
									params.put("to", to);
									params.put("text", textOutput);
									tcContent.setField("mailsend", params);
									mailService.doAction(tcConfig, "mailSend", tcRequest, tcContent);
								}
							}
						}
					}
				}
				result = RESULT_ok;

//			} else if (actionName.equals(ACTION_MAIL_TREE)) {
//				Map inboxes = loadSessionInbox(tcConfig, false);
//				saveSession(tcConfig, inboxes, null);
//				result = RESULT_ok;

//			} else if (actionName.equals(ACTION_MAIL_ACTION)) {
//				try {
//					Map inboxes = loadSessionInbox(tcConfig, true);
//					Map outboxes = loadSessionOutbox(tcConfig, true);
//					Map current = loadSessionCurrent(tcConfig);
//					Map request = tcRequest.getParamMap();
//					String action = (String)request.get("mail.action");
//					
//					if (request.containsKey("mail.inbox")) {
//						String inboxName = (String)request.get("mail.inbox");
//						if (!inboxName.equals(current.get("inbox"))) {
//							current.put("inbox", inboxName);
//							if (inboxes.containsKey(inboxName)) {
//								Map inboxMap = (Map)inboxes.get(inboxName);
//								current.put("in", (In)inboxMap.get("in"));
//							}
//						}
//					}
//
//					if (request.containsKey("mail.folder")) {
//						current.put("folder", request.get("mail.folder"));
//					}
//
//					if (request.containsKey("mail.start")) {
//						current.put("start", request.get("mail.start"));
//					}
//
//					if (request.containsKey("mail.size")) {
//						current.put("size", request.get("mail.size"));
//					}
//
//					In in = (In)current.get("in");
//					if (in != null) {
//						in.changeFolder((String)current.get("folder"));
////						current.put("info", in.getFolderInfos());
//					}
//
//					tcContent.setField(PARAM_MAIL_INBOXES, inboxes);
//					tcContent.setField(PARAM_MAIL_OUTBOXES, outboxes);
//
//					if (action == null) {
//					} else if (action.equals("list")) {
//						if (in != null) {
//							int start = 0;
//							try {
//								start = Integer.parseInt((String)current.get("start"));
//							} catch (NumberFormatException ex) {
//							}
//							int size = 10;
//							try {
//								size = Integer.parseInt((String)current.get("size"));
//							} catch (NumberFormatException ex) {
//							}
//							tcContent.setField(PARAM_MAIL_LIST, in.getMails(start, size));
//							tcContent.setField(PARAM_MAIL_LIST_INFO, in.getFolderInfos());
//							tcContent.setField(PARAM_MAIL_CURRENT, current);
//						}
//					} else if (action.equals("detail")) {
//						if (in != null) {
//							tcContent.setField(PARAM_MAIL_DETAIL, in.getMail(Integer.parseInt((String)tcRequest.get("mail.id"))));
//						}
//					} else if (action.equals("send")) {
//						/**
//						 * eMail versenden
//						 */
//						String outbox = (String)request.get("mail.outbox");
//						String addressTo = (String)request.get("mail.to");
//						String addressCc = (String)request.get("mail.cc");
//						String addressBcc = (String)request.get("mail.bcc");
//						String subject = (String)request.get("mail.subject");
//						String text = (String)request.get("mail.text");
//						
//						if (outboxes.containsKey(outbox) && ((Map)outboxes.get(outbox)).containsKey("out")) {
//							Out out = (Out)((Map)outboxes.get(outbox)).get("out");
//							
//							/**
//							 * Empf�nger-Liste, jeder Eintrag eine Map mit
//							 * den Keys "type", "mail" und evtl. "name".
//							 */
//							List to = new ArrayList();
//							String sTemp[];
//							
//							/**
//							 * Das Absender Feld TO parsen
//							 */
//							if (addressTo != null) {
//								sTemp = addressTo.split(";");
//								for (int i = 0; i < sTemp.length; i++) {
//									if (sTemp[i].trim().length() > 0) {
//										Map mTemp = new HashMap();
//										mTemp.put("type", "to");
//										mTemp.put("mail", sTemp[i].trim());
//										// TODO mTemp.put("name", "");
//										to.add(mTemp);
//									}
//								}
//							}
//							
//							/**
//							 * Das Absender Feld CC parsen
//							 */
//							if (addressCc != null) {
//								sTemp = addressCc.split(";");
//								for (int i = 0; i < sTemp.length; i++) {
//									if (sTemp[i].trim().length() > 0) {
//										Map mTemp = new HashMap();
//										mTemp.put("type", "cc");
//										mTemp.put("mail", sTemp[i].trim());
//										// TODO mTemp.put("name", "");
//										to.add(mTemp);
//									}
//								}
//							}
//							
//							/**
//							 * Das Absender Feld BCC parsen
//							 */
//							if (addressBcc != null) {
//								sTemp = addressBcc.split(";");
//								for (int i = 0; i < sTemp.length; i++) {
//									if (sTemp[i].trim().length() > 0) {
//										Map mTemp = new HashMap();
//										mTemp.put("type", "bcc");
//										mTemp.put("mail", sTemp[i].trim());
//										// TODO mTemp.put("name", "");
//										to.add(mTemp);
//									}
//								}
//							}
//							
//							Map info = (Map)((Map)((Map)tcContent.get("mail")).get("serverOut")).get(outbox);
//							Map from = new HashMap();
//							from.put(info.get("config.server.mail." + outbox + ".fromaddress"), info.get("config.server.mail." + outbox + ".fromname"));
//							
//							out.sendMail(from, to, subject, text);
//						}
//
//						tcContent.setField("out", outboxes);
//					}
//					
//					saveSession(tcConfig, inboxes, outboxes, current);
//				} catch (MessagingException ex) {
//					logger.log(Level.SEVERE, "Fehler beim laden einer Mail.", ex);
//				}
//
//			} else if (actionName.equals(ACTION_MAIL_ATTACHMENT)) {
//				Map inboxes = loadSessionInbox(tcConfig, false);
//				Map current = loadSessionCurrent(tcConfig);
//
//				In in = (In)current.get("in");
//				if (in != null) {
//					try {
//						Map mail = in.getMail(Integer.parseInt(tcRequest.get("mail.id")));
//						Object contentObject = mail.get("content");
//						
//						/**
//						 * F�r Bin�ren-Respone n�tige angaben in den Content stellen.
//						 */
//						if (contentObject instanceof List) {
//							List contentList = (List)contentObject;
//							Map contentMap = (Map)contentList.get(Integer.parseInt(tcRequest.get("mail.attachment")));
//							Map contentType = (Map)contentList.get(Integer.parseInt(tcRequest.get("mail.attachment")));
//
//							Map filestream = new HashMap();
//							filestream.put("type", TcBinaryResponseEngine.BINARY_RESPONSE_TYPE_STREAM);
//							filestream.put("stream", contentMap.get("content"));
//							filestream.put("filename", contentType.get("filename"));
//							tcContent.setField("mailstream", filestream);
//						}
//	
//						result = RESULT_ok;
//					} catch (MessagingException ex) {
//						logger.log(Level.SEVERE, "Fehler beim laden eines Mail-Attachments.", ex);
//					}
//				}
			}

			return result;
	}

//	/**
//	 * L�dt die InboxMap.
//	 * 
//	 * @param config
//	 * @param reload - Erzwingt bei true das neu Laden der InboxMap.
//	 * @return inboxMap
//	 */
//	private Map loadSessionInbox(TcConfig config, boolean reload) {
//		Map result = (Map)config.getSessionValueAsObject(Controller.SESSION_MAIL_CACHE_INBOX);
//		if (result == null || reload == true)
//			result = loadInboxMap((Map)config.getSessionValueAsObject(Controller.SESSION_CONFIGSERVER_NAME));
//		return result;
//	}
	
//	/** L�dt die OutboxMap
//	 * 
//	 * @param config
//	 * @param reload - Erzwingt bei true das neu Laden der OutboxMap.
//	 * @return outboxMap
//	 */
//	private Map loadSessionOutbox(TcConfig config, boolean reload) {
//		Map result = (Map)config.getSessionValueAsObject(Controller.SESSION_MAIL_CACHE_OUTBOX);
//		if (result == null || reload == true)
//			result = loadOutboxMap((Map)config.getSessionValueAsObject(Controller.SESSION_CONFIGSERVER_NAME));
//		return result;
//	}
	
//	/**
//	 * L�dt die aktuellen "Positionen".
//	 * 
//	 * @param config
//	 * @return currentMap
//	 */
//	private Map loadSessionCurrent(TcConfig config) {
//		Map result = (Map)config.getSessionValueAsObject(Controller.SESSION_MAIL_CACHE_CURRENT);
//		if (result == null) {
//			result = new HashMap();
//		}
//		return result;
//	}
	
//	/**
//	 * Speichert die Mail-Daten in der Session.
//	 * 
//	 * @param config
//	 * @param inboxMap
//	 * @param outboxMap
//	 * @param currentMap
//	 */
//	private void saveSession(TcConfig config, Map inboxMap, Map outboxMap, Map currentMap) {
//		if (inboxMap != null)
//			config.setSessionValue(Controller.SESSION_MAIL_CACHE_INBOX, inboxMap);
//		if (outboxMap != null)
//			config.setSessionValue(Controller.SESSION_MAIL_CACHE_OUTBOX, outboxMap);
//		if (currentMap != null)
//			config.setSessionValue(Controller.SESSION_MAIL_CACHE_CURRENT, currentMap);
//		return;
//	}

//	/**
//	 * L�dt alle Inboxes eines Users in einer Map,
//	 * Key entspricht der ID der Inbox.
//	 * Value ist eine InboxMap mit den Keys "in" und "tree".
//	 */
//	private static Map loadInboxMap(Map configServer) {
//		Map result = new HashMap();
//
//		if (configServer != null && configServer.containsKey(Controller.USERPARAM_MAIL_LIST_IN)) {
//			String[] inboxString = ((String)configServer.get(Controller.USERPARAM_MAIL_LIST_IN)).split(",");
//			for (int i = 0; i < inboxString.length; i++) {
//				Map inboxMap = new HashMap();
//				In in = new In();
//				try {
//					String prefix = Controller.USERPARAM_MAIL_PREFIX + ".";
//					in.setServer((String)configServer.get(prefix + inboxString[i] + ".protocol"), (String)configServer.get(prefix + inboxString[i] + ".server"), (String)configServer.get(prefix + inboxString[i] + ".port"));
//					in.setUser((String)configServer.get(prefix + inboxString[i] + ".username"), (String)configServer.get(prefix + inboxString[i] + ".password"));
//					in.connect();
//					inboxMap.put("in", in);
//					inboxMap.put("tree", in.getFolderMap());
//					result.put(inboxString[i], inboxMap);
//				} catch (MessagingException ex) {
////					inboxMap.put("error", ex.toString());
////					result.put(inboxString[i], inboxMap);
//				}
//			}
//			
//		}
//		return result;
//	}

//	/**
//	 * L�dt alle Outboxes eines Users in einer Map,
//	 * Key entspricht der ID der Outbox.
//	 */
//	private static Map loadOutboxMap(Map configServer) {
//		Map result = new HashMap();
//
//		if (configServer != null && configServer.containsKey(Controller.USERPARAM_MAIL_LIST_OUT)) {
//			String[] outboxString = ((String)configServer.get(Controller.USERPARAM_MAIL_LIST_OUT)).split(",");
//			for (int i = 0; i < outboxString.length; i++) {
//				Map outboxMap = new HashMap();
//				Out out = new Out();
//				try {
//					String prefix = Controller.USERPARAM_MAIL_PREFIX + ".";
//					out.setServer((String)configServer.get(prefix + outboxString[i] + ".protocol"), (String)configServer.get(prefix + outboxString[i] + ".server"), (String)configServer.get(prefix + outboxString[i] + ".port"));
//					out.setUser((String)configServer.get(prefix + outboxString[i] + ".username"), (String)configServer.get(prefix + outboxString[i] + ".password"));
//					out.connect();
//					outboxMap.put("out", out);
//					result.put(outboxString[i], outboxMap);
//				} catch (MessagingException ex) {
////					inboxMap.put("error", ex.toString());
////					result.put(inboxString[i], inboxMap);
//				}
//			}
//			
//		}
//		return result;
//	}

	/**
	 * Formatiert eine eMail bzw. f�llt Platzhalter mit den Adressinformationen.
	 */
	public static String formatMail(String text, Map address) {
		if (text == null || address == null)
			return null;

		// Adress-Felder ersetzten
		String result = text
			.replaceAll("<!HERRFRAU!>", (String)address.get("a1"))
			.replaceAll("<!ANREDE!>", (String)address.get("a18"))
			.replaceAll("<!TITEL!>", (String)address.get("a2"))
			.replaceAll("<!AKAD!>", (String)address.get("a3"))
			.replaceAll("<!VORNAME!>", (String)address.get("a4"))
			.replaceAll("<!NACHNAME!>", (String)address.get("a5"))
			.replaceAll("<!NAMENSZUSATZ!>", (String)address.get("a6"))
			.replaceAll("<!INSTITUTION!>", (String)address.get("a7"))
			.replaceAll("<!EMAIL!>", (String)address.get("a108"))
			.replaceAll("<!INTERNET!>", (String)address.get("a109"))
			.replaceAll("<!STRASSE!>", (String)address.get("a8"))
			.replaceAll("<!HAUSNUMMER!>", (String)address.get("a9"))
			.replaceAll("<!PLZ_ORT!>", (String)address.get("a10"))
			.replaceAll("<!ORT!>", (String)address.get("a11"))
			.replaceAll("<!PLZ_POSTFACH!>", (String)address.get("a12"))
			.replaceAll("<!POSTFACH!>", (String)address.get("a13"))
			.replaceAll("<!BUNDESLAND!>", (String)address.get("a21"))
			.replaceAll("<!LAND!>", (String)address.get("a19"))
			.replaceAll("<!BRIEFANREDE!>", FieldMaker.createLetterSalutation(
				(String)address.get("a18"), // Anrede
				(String)address.get("a1"), // Herr / Frau
				(String)address.get("a3"), // Akad. Titel
				(String)address.get("a5"))) // Nachname
			.replaceAll("<!BRIEFANSCHRIFT!>", FieldMaker.createLabel(
				(String)address.get("a1"), // Herr / Frau
				(String)address.get("a2"), // Beruf. Titel
				(String)address.get("a3"), // Akad. Titel
				(String)address.get("a4"), // Vorname
				(String)address.get("a5"), // Nachname
				(String)address.get("a6"), // Namenszusatz
				(String)address.get("a7"), // Institution
				(String)address.get("a8"), // Stra�e
				(String)address.get("a9"), // Haus.-Nr.
				(String)address.get("a10"), // PLZ
				(String)address.get("a11"), // Stadt
				(String)address.get("a12"), // PLZ Postfach
				(String)address.get("a13"), // Postfach
				(String)address.get("a19")) // Land
			.replaceAll("<br>", "\n"));

		return result;
	}

	/** Worker-Aktionen */
	public final static String ACTION_MAIL_SEND = "mailSend";

//	public final static String ACTION_MAIL_ACTION = "mailAction";
//	public final static String ACTION_MAIL_ATTACHMENT = "mailAttachment";
	
	/** Mail-Parameter */
//	public final static String PARAM_MAIL_INBOXES = "mailInboxes";
//	public final static String PARAM_MAIL_OUTBOXES = "mailOutboxes";
//	public final static String PARAM_MAIL_LIST = "mailList";
//	public final static String PARAM_MAIL_LIST_INFO = "mailListInfo";
//	public final static String PARAM_MAIL_DETAIL = "mailDetail";
//	public final static String PARAM_MAIL_ATTACHMENT = "mailAttachment";
//	public final static String PARAM_MAIL_CURRENT = "mailCurrent";

	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port = new TcPortDefinition("de.tarent.contact.octopus.WebClientControl", "Worker zur Verarbeitung von eMail f�r den WebClient.");

		TcOperationDefinition operation;
		operation = port.addOperation(ACTION_MAIL_SEND, "Versendet eMail's (Einfache und Postversand)");
		operation.setInputMessage();
		operation.setOutputMessage();

		return port;
	}

	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.3 $") + " (" + CVS.getContent("$Date: 2006/09/15 13:00:03 $") + ')';
	}
}
