/* $Id: AbstractStream.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.groupware.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.security.GeneralSecurityException;

import javax.activation.DataSource;

/**
 * @see #readInput()
 * @see #closeInput()
 * @see #writeOutput(OutputStream)
 *
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public abstract class AbstractStream implements Runnable {
	protected final static int BUFFERSIZE = 1024;

	private Exception exception;

	protected DataSource dataSource;
	protected OutputStream outputStream;

	static {
		Crypto.init();
	}

	protected abstract void crypt() throws IOException, GeneralSecurityException;
	protected abstract void clear();

	/**
	 * Schreibt die ver-/entschlüsselten Daten direkt
	 * in den übergebenen OutputStream.
	 * 
	 * @param outputStream
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public void writeOutput(OutputStream outputStream) throws IOException, GeneralSecurityException {
		try {
			this.outputStream = outputStream;
			crypt();
		} finally {
			clear();
		}
	}

	/**
	 * Gibt einen InputStream zurück aus dem man anschließend
	 * die ver-/entschlüsselten Daten lesen kann.
	 * Danach muss die <code>close()</code> Methode aufgerufen werden.
	 * 
	 * @see #closeInput()
	 * 
	 * @return
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public InputStream readInput() throws IOException, GeneralSecurityException {
		PipedInputStream inputStream = new PipedInputStream();
		this.outputStream = new PipedOutputStream(inputStream);
		new Thread(this).start();
		return inputStream;
	}

	public void closeInput() throws IOException, GeneralSecurityException {
		if (exception != null) {
			if (exception instanceof IOException)
				throw (IOException)exception;
			else if (exception instanceof GeneralSecurityException)
				throw (GeneralSecurityException)exception;
			else if (exception instanceof RuntimeException)
				throw (RuntimeException)exception;
		}
	}

	public void run() {
		try {
			crypt();
		} catch (IOException e) {
			exception = e;
		} catch (GeneralSecurityException e) {
			exception = e;
		} catch (RuntimeException e) {
			exception = e;
		} finally {
			clear();
		}
	}

	protected void pipe(InputStream inputStream, OutputStream outputStream) throws IOException {
		int i;
		byte buffer[] = new byte[BUFFERSIZE];
		while ((i = inputStream.read(buffer)) >= 0) {
			outputStream.write(buffer, 0, i);
		}
		outputStream.flush();
	}

	protected void finalize() {
		clear();
	}
}