package de.tarent.contact.octopus.logging;

import org.apache.log4j.Logger;

/**
 * Hier wird der Loggingmechanismus realisiert.
 * 
 * @author Wolfgang Klein
 * @deprecated <strong>BITTE</strong> das Logging mal richtig machen.
 */
public class Lg {
	private static final Logger SQL = Logger.getLogger("SQL");

	private static final Logger DBASE = Logger.getLogger("DBASE");

	private static final Logger WORKER = Logger.getLogger("WORKER");

	static public Logger SQL(String module) {
		return SQL;
	}

	static public Logger DBASE(String module) {
		return DBASE;
	}

	static public Logger WORKER(String module) {
		return WORKER;
	}
}
