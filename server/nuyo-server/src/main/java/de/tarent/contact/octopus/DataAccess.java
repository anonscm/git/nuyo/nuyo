/* $Id: DataAccess.java,v 1.11 2007/06/15 15:58:30 fkoester Exp $
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Thomas Fuchs and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus;

import java.util.Date;
import java.util.List;
import java.util.Map;

import de.tarent.contact.octopus.ldap.LDAPManager;

/**
 * Diese Schnittstelle abstrahiert die Datenzugriffsmethoden des
 * contact-Moduls.
 * 
 * @author mikel
 */
public interface DataAccess {
	/**
	 * Diese Methode liefert eine Map der Gruppen des 
	 * derzeitigen Users
	 */
	public Map getGruppen(String userId);

	/**
	 * Diese Methode liefert eine Map der Gruppen des 
	 * derzeitigen Users
	 */
	public Map getGruppenExt(String userId);

	/**
	 * Diese Methode liefert eine Map der User der 
	 * gegebenen Verteilergruppe
	 * 
	 * @param verteilergruppe der Verteilergruppenschl�ssel
	 */
	public List getUserGroups(String verteilergruppe);

	/**
	 * Holt zu einer gegebenen Address ID die Vereilergruppen, zu der die Addresse geh�rt
	 * @param AdrId Address ID zu der die Verteilergruppen
	 * @return Map mit VerteilergruppenIDs als Key und Name als Value
	 * 
	 * @deprecated Wird von niemandem mehr benutzt...
	 */
	public Map getVertGrp(int AdrId);
		
	/**
	 * Liefert eine Map mit allen m�glichen Verteilergruppenschl�sseln als Key und den dazugeh�rigen Usern als Liste
	 * 
	 * @deprecated Wird von niemandem mehr benutzt...
	 */
	public Map getAllUserGroups();
	
	
	/**
	 * Diese Methode liefert eine Map zu einer 
	 * bestimmten Addresse
	 */
	public Map getAddress(int AdrId, String verteilergrp);
	public List getAddressList(int AdrId, String verteilergrp);	
	/**
	 * Diese Methode liefert eine Map aller 
	 * vorhandenen Bundesl�nder
	 */
	public Map getBundeslaender();

	/**
	 * Diese Methode liefert eine Map aller 
	 * vorhandenen Anreden
	 */
	public Map getAnreden();

	/**
	 * Diese Methode liefert eine Map aller vorhandenen
	 * Landeskennzahlen
	 */
	public Map getLKZ();

	/**
	 * Diese Methode liefert eine Map aller vorhandenen
	 * L�nder
	 */
	public Map getLaender();

	/**
	 * Diese Methode pr�ft ob die �bergebene Addresse
	 * in den L�schverteiler geh�rt
	 */
	public void collectOrphan(int adrId);

	/**
	 * Diese Methode liefert die StandardVerteilergruppe
	 * zum eingeloggten Benutzer
	 */
	public Map getStandardVerteiler(String UserID);

	/** 
	 * Diese Methode liefert alle zu Zeit vorhandenen
	 * Verteilergruppen
	 */
	public Map getAllVerteilergruppen();

	/**
	 * Die Methode liefert alle zur Zeit vorhandenen User
	 */
	public Map getUsers();

	/**
	 * Diese Methode setzt die StandardVerteilergruppe
	 * zum eingeloggten Benutzer
	 */
	public Map setStandardVerteilergruppe(String gruppe, String UserID);

	/**
	 * Diese Methode liefert Verteiler 
	 */
	public Map getVerteiler(String userId, String grp);

	/**
	 * Diese Methode inserted einen Addressdatensatz 
	 */
	public Map insertAddress(Map map);

	/**
	 * Diese Methode liefert alle Adressen
	 * @param verteilergruppe
	 */
	public Map getAddressesfromGroup(int verteilergruppe);

	/**
	 * Diese Methode updated einen Addressdatensatz 
	 */
	public void updateAddress(Map map);

	/**
	 * Diese Methode updated ein Abodatum (Subscriptor) 
	 */
	public void updateSubscriptor(int adrId);

	/**
	 * Diese Methode setzt eine Bemerkung 
	 */
	public void setBemerkung(
		int adrId,
		String grp,
		String bem,
		String stichwort);

	/**
	 * Diese Methode aktualisiert einen Verteiler 
	 */
	public void setVerteiler(Map t);

	/**
	 * Diese Methode l�scht einen Adressdatensatz 
	 */
	public Map deleteAddress(int adrId, String grp);

	/**
	 * Diese Methode gibt die LKZ zu einem �bergebenen Land zur�ck 
	 */
	public Map getLKZforLand(String land);

	/**
	 * Diese Methode gibt das Land zu einer �bergebenen LKZ zur�ck 
	 */
	public Map getLandforLKZ(String lkz);

	/**
	 * Diese Methode erzeugt eine Verteilergruppe 
	 */
	public Map createVerteilergruppe(String key, String name, String description);

	public Integer getUserId(String userLogin);

	/**
	 * Holt alle Einstellungen eines Benutzers die mit dem String anfangen.
	 * 
	 * @param userId
	 * @param userProperties
	 */
	public Map getUserProperties(Integer userId, String userProperties);

	/**
	 * Speichert Einstellungen eines Benutzers.
	 * 
	 * @param userId
	 * @param key
	 * @param value
	 * 
	 * @deprecated Wird von niemandem mehr benutzt...
	 */
	public boolean setUserProperty(Integer userId, String key, String value);

	/**
	 * L�scht die Einstellungen eines Benutzers die mit dem �bergebenem String anfangen.
	 * 
	 * @param userId
	 * @param userProperties
	 * 
	 * @deprecated Wird von niemandem mehr benutzt...
	 */
	public boolean removeUserProperties(Integer userId, String userProperties);

	/**
	 * Diese Methode erzeugt einen neuen Versandauftrag.
	 * 
	 * @param values 
	 */
	public Map createVersandAuftrag(Map values);

	/**
	 * Diese Methode holt vorhandene Versandauftr�ge zum
	 * eingelogten User
	 * 
	 */
	public Map getVersandAuftraege(String user);

	/**
	 * 
	 */
	public Map getBundeslandForPLZ(String plz);

	/**
	 * Diese Methode sucht nach Addressen zu �bergebener Map
	 */
	public Map searchAddress(Map map);

	/**
	 * Diese Methode sucht nach Addressen zu �bergebener Map und liefert sie
	 * als Liste zur�ck.
	 */
	public Map searchAddressesVector(
		String kategorie,
		boolean matchcode,
		int addressCount,
		int packageIndex,
		String nachname,
		String vorname,
		String strasse,
		String ort,
		String herrnFrau,
		String titel,
		String akadTitel,
		String namenszusatz,
		String institution,
		String plz,
		String bundesland,
		String lkz,
		String land,
		String telefonP,
		String telefonD,
		String faxP,
		String faxD,
		String email);

	/**
	 * Diese Methode stellt zu Methoden aus OctopusMailBatch entsprechende
	 * SQL-Anweisungen zur Verf�gung
	 */
	public Map mailBatch(Map map);

	/**
	 * Diese Methode liefert die aktuelle Octopus-Version
	 */
	public Map getVersion();

	/**
	 * Diese Methode liefert den Ort zu gegebener PLZ
	 */
	public Map getCityForPLZ(String plz);

	/**
	 * 	Diese Methode liefert die erweiterten Felder
	 */
	public Map getExtendedFields();

	/**
	 * 	Diese Methode liefert einen Parameter zu einem Key
	 */
	public Map getParameter(String paramKey);

	/**
	 *  Diese Methode setzt einen Parameter
	 */
	public Map setParameter(String paramKey, String paramValue);

	/**
	 * Diese Methode erzeugt aus einer XML-Suchanfrage eine Ergebnismenge als
	 * XML-Tabelle.
	 * 
	 * @param XML die XML-Suchanfrage
	 * @return die Map, die ??? auf ??? abbildet.
	 */
	public String getAdresses(String XML,String userID);

	/**
	 * Diese Methode holt alle initialen Datens�tze.
	 * Es werden lediglich alle Aufrufe der Startprozedur gesammelt und
	 * geb�ndelt �bergeben, da bei einer SSL-Verbindung Einzelaufrufe
	 * aufgrund der Authentifizierung zu lange dauern.
	 * 
	 */
	public Map getInitDatas(Map params);

	/**
	 * Setter f�r ldm
	 * @param ldm
	 * 
	 * @deprecated Wird von niemandem mehr benutzt...
	 */
	public void setLdm(LDAPManager ldm);

	/**
	 * Diese Methode liefert zu einer Adress-Map, in der id&lt;n> auf eine
	 * n-te Adress-Id abgebildet wird, eine Liste, in der zu dem in den
	 * Parametern beschriebenen Indexfenster die konkreten Addressdaten
	 * stehen.
	 *  
	 * @param indices eine Map "id&lt;n>" -> n-te Adress-ID als Integer
	 * @param start der erste Index f�r die Liste
	 * @param count die gew�nschte Listenl�nge
	 * @param category Kategorie, in der liegend die Adresse verstanden werden soll
	 * @return eine Liste von Listen (id, Adressdaten) des definierten Fensters.
	 */
	public List getAddressSectionList(
		Map indices,
		int start,
		int count,
		String category);

	/**
	 * Diese Methode erzeugt eine Adresslisteninfoliste.
	 * 
	 * @param total Anzahl Adressen insgesamt
	 * @param start Startindex (0-basiert) des Anzeigefensters
	 * @param count Gr��e des Anzeigefensters
	 * @return Liste (aktuelle Seite, Anzahl Seiten, Startindex erste Seite,
	 *  Startindex vorige Seite, Startindex n�chste Seite, Startindex
	 *  letzte Seite, Index erste angezeigte Adresse, Index letzte
	 *  angezeigte Adresse, Index letzte Adresse)
	 */
	public List createAddressListInfo(int total, int start, int count);

	
	public List getColumnsFromTable(String tableName);
	
	/**
	 * Diese Methode liefert die Unterkategorien einer Kategorie mit Beschreibung
	 */
	public Map getVerteilerExt(String userId, String grp);

    /**
     * @param i
     */
    public List getVersandAuftrag(int i);    
    /**
     * Holt Historie zu einer Adresse
     * @param adrid
     * @param type
     * @param start
     * @param end
     */
    public List getHistory_tuned(int adrid, List type, Date start, Date end);
}
