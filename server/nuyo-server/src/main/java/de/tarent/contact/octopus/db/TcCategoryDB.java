package de.tarent.contact.octopus.db;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;

/**
 * @author kleinw
 * 
 * Hier befinden sich alle Methoden, die sich mit Kategorien besch�ftigen.
 *  
 */
public class TcCategoryDB {

    static public List getCategoriesFromUser(Map parameter)
            throws SQLException, InputParameterException {
        List categories = new Vector();
        if (!parameter.containsKey("userid")) { throw new InputParameterException(
                "F�r die Methode 'getCategoriesFromUser' muss ein String Feld 'userid' �bergeben sein."); }
        if (!(parameter.get("userid") instanceof String)) { throw new InputParameterException(
                "Der Parameter 'userid' muss als String �bergeben werden."); }

        Select subSelect = SQL.Select(TcDBContext.getDefaultContext()).from(TuserDB.getTableName()).select(
                TuserDB.PK_PKUSER).where(Expr.equal(TuserDB.LOGINNAME, (String) parameter.get("userid")));

        Select main = SQL.Select(TcDBContext.getDefaultContext())
        	.from(TcategoryDB.getTableName())
            .selectAs(TcategoryDB.PK_PKCATEGORY)
            .selectAs(TcategoryDB.CATEGORYNAME)
            .selectAs(TcategoryDB.DESCRIPTION)
            .selectAs(TsubcategoryDB.PK_PKSUBCATEGORY)
            .selectAs(TsubcategoryDB.CATEGORYNAME)
            .selectAs(TsubcategoryDB.DESCRIPTION)
        	.join(TsubcategoryDB.getTableName(),TcategoryDB.PK_PKCATEGORY, TsubcategoryDB.FKCATEGORY)
        	.join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TcategoryDB.PK_PKCATEGORY)
        	.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
        	.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
            .where(Expr.in(TgroupUserDB.FKUSER, new SubSelect(TcDBContext.getDefaultContext(), subSelect)))
            .whereAnd(Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1)))
            .orderBy(Order.asc(TcategoryDB.PK_PKCATEGORY));

        Integer categoryPk = null;
        String categoryName = null;
        String categoryDescription = null;
        StringBuffer line = new StringBuffer();
        StringBuffer subCategories = new StringBuffer();

        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), main.toString());
            while (tr.resultSet().next()) {
                categoryPk = new Integer(tr.resultSet().getInt(TcategoryDB.PK_PKCATEGORY));
                categoryName = "#"+ tr.resultSet().getString(TcategoryDB.CATEGORYNAME)+ "#";
                categoryDescription = "#" + tr.resultSet().getString(TcategoryDB.DESCRIPTION) + "#";
                subCategories
                	.append(tr.resultSet().getInt(TsubcategoryDB.PK_PKSUBCATEGORY))
                	.append(" #")
                	.append(tr.resultSet().getString(TsubcategoryDB.CATEGORYNAME))
                	.append("##");
                
                String des = tr.resultSet().getString(
                        TsubcategoryDB.DESCRIPTION);
                if (des == null)
                    subCategories.append(" ");
                else if (des.length() == 0)
                    subCategories.append(" ");
                else
                    subCategories.append(des);
                subCategories.append("# ");
                if (tr.resultSet().next()) {
                    if (tr.resultSet().getInt(TcategoryDB.PK_PKCATEGORY) != categoryPk
                            .intValue()) {
                        line.append(categoryPk);
                        line.append(" ");
                        line.append(categoryName);
                        line.append(categoryDescription);
                        categories.add(line.append("[").append(
                                subCategories.toString().substring(0,
                                        subCategories.toString().length() - 1))
                                .append("]"));
                        line = new StringBuffer();
                        subCategories = new StringBuffer();
                    }
                    tr.resultSet().previous();
                }
            }
            line.append(categoryPk);
            line.append(" ");
            line.append(categoryName);
            line.append(categoryDescription);
            categories
                    .add(line.append("[").append(
                            subCategories.toString().substring(0,
                                    subCategories.toString().length() - 1))
                            .append("]"));
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) {
                tr.close();
            }
        }
        System.out.println("cats : " + categories);
        return categories;
    }

    static public List getSubCategories(Map parameter)
            throws InputParameterException, SQLException {
        List subCategories = new Vector();
        if (!parameter.containsKey("categoryPk"))
                throw new InputParameterException(
                        "F�r die Methode getSubCategories muss der Parameter 'categoryPk' �bergeben werden.");
        if (!(parameter.get("categoryPk") instanceof Integer))
                throw new InputParameterException(
                        "Der Parameter 'categoryPk' muss als Integer �bergeben werden.");

        Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TsubcategoryDB.getTableName())
                .selectAs(TsubcategoryDB.PK_PKSUBCATEGORY)
                .selectAs(TsubcategoryDB.CATEGORYNAME)
                .selectAs(TsubcategoryDB.DESCRIPTION)
                .where(Expr.equal(TsubcategoryDB.FKCATEGORY, (Integer) parameter.get("categoryPk")));


        Result tr = null;
        StringBuffer subCategory = new StringBuffer();

        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            while (tr.resultSet().next()) {
                subCategory.append(tr.resultSet().getString(
                        TsubcategoryDB.PK_PKSUBCATEGORY));
                subCategory.append("#");
                subCategory.append(tr.resultSet().getString(
                        TsubcategoryDB.CATEGORYNAME));
                subCategory.append("#");
                String desc = tr.resultSet().getString(
                        TsubcategoryDB.DESCRIPTION);
                subCategory.append(tr.resultSet().getString(
                        TsubcategoryDB.DESCRIPTION));
                subCategories.add(subCategory.toString());
                subCategory = new StringBuffer();
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }
        return subCategories;
    }

    static public String modifyCategory(Map parameter)
            throws InputParameterException, SQLException {
        String result = "";
        if (!parameter.containsKey("categoryPk"))
                throw new InputParameterException(
                        "F�r die Methode modifyCategory muss der Parameter 'categoryPk' �bergeben werden.");
        if (!parameter.containsKey("categoryName"))
                throw new InputParameterException(
                        "F�r die Methode modifyCategory muss der Parameter 'categoryName' �bergeben werden.");
        if (!(parameter.get("categoryPk") instanceof Integer))
                throw new InputParameterException(
                        "Der Parameter 'categoryPk' muss als Integer �bergeben werden.");
        if (!(parameter.get("categoryName") instanceof String))
                throw new InputParameterException(
                        "Der Parameter 'categoryName' muss als String �bergeben werden.");

        Select select =
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TcategoryDB.getTableName())
            	.selectAs("count(*)", "count")
            	.where(Expr.equal(TcategoryDB.CATEGORYNAME, (String)parameter.get("categoryName")))
            	.whereAnd(Expr.notEqual(TcategoryDB.PK_PKCATEGORY, (Integer)parameter.get("categoryPk")));
        
        Result tr = null;

        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            
            if (tr.resultSet().next()) {
                if (tr.resultSet().getInt("count") > 0) {
                    result = "Eine Kategorie mit dem Namen " + parameter.get("categoryName") + " ist im System bereits vorhanden.";
                } else {
                    Update update = SQL.Update(TcDBContext.getDefaultContext())
                    	.table(TcategoryDB.getTableName())
                    	.update(TcategoryDB.CATEGORYNAME, (String) parameter.get("categoryName"))
                    	.where(Expr.equal(TcategoryDB.PK_PKCATEGORY, (Integer) parameter.get("categoryPk")));
                    
                    if (parameter.containsKey("categoryDescription")) 
                        update.update(TcategoryDB.DESCRIPTION, (String)parameter.get("categoryDescription"));

                    DB.update(TcDBContext.getDefaultContext(), update.toString());
                }
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null)
                tr.close();
        }
        return result;
    }
}
