package de.tarent.contact.octopus.worker.constants;


/**
 * @author kleinw
 *
 *	Konstanten, die von allen Workern geteilt werden.
 *
 */
public interface CommonConstants {

    static public final String MSG_ERROR ="Fehler.";
    static public final String MSG_OK="Ok.";

}
