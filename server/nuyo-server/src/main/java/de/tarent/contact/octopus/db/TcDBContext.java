/**
 * 
 */
package de.tarent.contact.octopus.db;

import java.sql.Connection;
import java.sql.SQLException;

import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.DBContextImpl;
import de.tarent.octopus.server.Context;
import de.tarent.octopus.server.OctopusContext;

/**
 * Tc-Specific implementation of a DB-Context.
 * 
 * @author kirchner
  */
public class TcDBContext extends DBContextImpl{
	
	public final static String DEFAULTCONTEXT_NAME = "defaultDBContext";
	
	public static String schemaname = "colibri.";

	/**
	 * Returns the DefaultContext of this Tc-Instance.
	 * @return
	 */
	public static TcDBContext getDefaultContext(){
		OctopusContext oc = Context.getActive();
		if(oc.contentContains(DEFAULTCONTEXT_NAME)){
			TcDBContext defaultcontext = (TcDBContext) Context.getActive().contentAsObject(DEFAULTCONTEXT_NAME);
			return defaultcontext;
		}else{
			TcDBContext defaultcontext = new TcDBContext(DB.getDefaultContext(oc.getModuleName()));
			oc.setContent(DEFAULTCONTEXT_NAME, defaultcontext);
			return defaultcontext;
		}
	}

	private Connection defaultcon = null;
	
	private TcDBContext(DBContext ctx){
		setPoolName(ctx.getPoolName());
	}
	
	/**
	 * Returns Name of Used DB-Schema. Usefull for
	 * hand-optimized Statements. Default is "colibri."
	 * @return Name of DB-Schema with following dot.
	 */
	static public String getSchemaName(){
		return schemaname;
	}
	
	/**
	 * There are circumstances where you need the name of
	 * the DB-Schema without the "dot".
	 * @see #getSchemaName()
	 * @return Name of Schema without dot at the end.
	 */
	static public String getSchemaNameWithoutDot(){
		return schemaname.substring(0, schemaname.length()-1);
	}
	
	/**
	 * Setter for the name of the DB-Schema. 
	 * @param name
	 */
	static public void setSchemaName(String name){
		if(name!=null){
			if(!name.endsWith(".")){
				name+=".";
			}
			schemaname = name;
		}
	}

	
	/* Overridden methods */
	
	/**
	 * Returns the default-Connection of this DBContext.
	 * @see DBContext#getDefaultConnection()
	 */
	@Override
	public Connection getDefaultConnection() throws SQLException {
		if(defaultcon==null||defaultcon.isClosed()){
			defaultcon = getPool().getConnection();
			Context.getActive().addCleanupCode(new DBConnectionCloser(defaultcon));
		}
		return defaultcon;
	}
	
	
	
	
	
	
	private TcDBContext(){
		
	}
}
