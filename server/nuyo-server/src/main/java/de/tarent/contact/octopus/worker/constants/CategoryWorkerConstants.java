package de.tarent.contact.octopus.worker.constants;


/**
 * @author kleinw
 *	
 *	Hier werden Funktionalitšten rundum 
 *	Kategorienmanagement bereitgestellt.
 *
 */
public interface CategoryWorkerConstants {

    final static public String FIELD_SUBCATEGORIES = "subCategories";
    
    final static public String FIELD_CATEGORYNAME = "categoryName";
    final static public String FIELD_CATEGORYPK = "categoryPk";
    final static public String FIELD_RESULT = "result";
    
}
