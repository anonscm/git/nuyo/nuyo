/* $Id: Queries.java,v 1.5 2006/10/06 18:46:52 peet Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.db.queries;

import de.tarent.contact.octopus.db.TcDBContext;

/**
 *
 * TODO Erkl�rung!? 
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class Queries {
	

    /**
     * Liefert die -nicht virtuellen- Verteiler eines Users
     */
	public static String getGruppenQuery(String loginname){
		String sql =
				"SELECT distinct(TFOLDER.PK_FOLDER), TFOLDER.FOLDERNAME "
			+	"FROM " + TcDBContext.getSchemaName() + "TFOLDER " 
			+ 	"JOIN " + TcDBContext.getSchemaName() + "tgroup_folder on tgroup_folder.fk_folder = tfolder.pk_folder "
			+	"join " + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.fk_group = tgroup_folder.fk_group "
			+	"join " + TcDBContext.getSchemaName() + "tuser on tuser.pk_user = tgroup_user.fk_user "
			+	"join " + TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
			+	"where tuser.loginname = '" + loginname + "' "
			+	"and tfolder.isvirtual = '0' "
			+	"and tfolderrole.auth_read = 1";
		return sql;
	}
	
	public static String getGruppenExtQuery(String loginname){
		String sql =
				"SELECT distinct(TFOLDER.PK_FOLDER), TFOLDER.FOLDERNAME, TFOLDER.DESCRIPTION "
			+	"FROM " + TcDBContext.getSchemaName() + "TFOLDER " 
			+ 	"JOIN " + TcDBContext.getSchemaName() + "tgroup_folder on tgroup_folder.fk_folder = tfolder.pk_folder "
			+	"join " + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.fk_group = tgroup_folder.fk_group "
			+	"join " + TcDBContext.getSchemaName() + "tuser on tuser.pk_user = tgroup_user.fk_user "
			+	"join " + TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
			+	"where tuser.loginname = '" + loginname + "' "
			+	"and tfolderrole.auth_read = 1";
		return sql;
	}
	
	public static String getUserGroupsQuery(String verteilergruppenpk){
		String sql =
			"select distinct(tuser.loginname) "
		+	"from " + TcDBContext.getSchemaName() + "tuser "
		+ 	"join " + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.fk_user = tuser.pk_user "
		+	"join " + TcDBContext.getSchemaName() + "tgroup_folder on tgroup_folder.fk_group = tgroup_user.fk_group "
		+	"join " + TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
		+ 	"where tgroup_folder.fk_folder = " + verteilergruppenpk + " "
		+	"and tfolderrole.auth_read = 1";
		return sql;
	}
	
	public static String getVerteilerQuery(String loginname, String folderID){
		String sql =
			"SELECT distinct(TSUBFOLDER.PK_SUBFOLDER), TSUBFOLDER.FOLDERNAME "
		+	"FROM " + TcDBContext.getSchemaName() + "tsubfolder "
		+	"join " + TcDBContext.getSchemaName() + "tgroup_folder on tgroup_folder.fk_folder = tsubfolder.fk_folder "
		+	"join " + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.fk_group = tgroup_folder.fk_group "
		+	"join " + TcDBContext.getSchemaName() + "tuser on tuser.pk_user = tgroup_user.fk_user "
		+	"join " + TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
		+	"where tuser.loginname = '" + loginname + "' "
		+	"and tgroup_folder.fk_folder = " + folderID + " "
		+	"and tfolderrole.auth_read = 1";
		return sql;
	}
	
	public static String getVerteilerExtQuery(String loginname, String folderID){
		String sql =
			"SELECT distinct(TSUBFOLDER.PK_SUBFOLDER), TSUBFOLDER.FOLDERNAME, TSUBFOLDER.DESCRIPTION "
		+	"FROM " + TcDBContext.getSchemaName() + "tsubfolder "
		+	"join " + TcDBContext.getSchemaName() + "tgroup_folder on tgroup_folder.fk_folder = tsubfolder.fk_folder "
		+	"join " + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.pk = tgroup_folder.fk_group "
		+	"join " + TcDBContext.getSchemaName() + "tuser on tuser.pk_user = tgroup_user.fk_user "
		+	"join " + TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
		+	"where tuser.loginname = '" + loginname + "' "
		+	"and tgroup_folder.fk_folder = " + folderID + " "
		+	"and tfolderrole.auth_read = 1";
		return sql;
	}
	
	public static String mayAccessCategory(String loginname, String folderID){
		String sql = 
			"select distinct(tgroup_folder.fk_folder) FROM " + TcDBContext.getSchemaName() + "tgroup_folder "
		+	"join" +TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
		+	"join" + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.pk = tgroup_folder.fk_group "
		+	"join" + TcDBContext.getSchemaName() + "tuser on tuser.pk_user = tgroup_user.fk_user "
		+	"where tgroup_folder.fk_folder = " + folderID + " "
		+	"and tuser.loginname = '" + loginname + "' "
		+	"and tfolderrole.auth_read = 1";
		return sql;
	}
	
	public static String getStandardCategoryQuery(String loginname){
		String sql =
			"select tgroup_folder.fk_folder FROM " + TcDBContext.getSchemaName() + "tgroup_folder "
		+	"join" + TcDBContext.getSchemaName() + "tgroup_user on tgroup_user.fk_group = tgroup_folder.fk_group "
		+	"join" + TcDBContext.getSchemaName() + "tuser on tuser.pk_user = tgroup_user.fk_user "
		+	"join" + TcDBContext.getSchemaName() + "tfolderrole on tfolderrole.pk = tgroup_folder.fk_folderrole "
		+	"where tuser.loginname = '" + loginname + "' "
		+	"and tfolderrole.auth_read = 1 "
		+	"limit 1 ";
		return sql;
	}

	/**
	 * @param dupid
	 * @param vorname
	 * @param nachname
	 * @param organisation
	 * @param plz
	 * @param postfach
	 * @param postfachplz
	 * @param comm1
	 * @param comm2
	 * @param comm3
	 * @param comm4
	 * @param comm5
	 * @param comm6
	 * @param excl
	 * @return
	 * 
	 * @deprecated use SQL.Procedure
	 */
	public static String doDupcheck(Integer dupid, String vorname, String nachname, String organisation, String plz, String postfach, String postfachplz, String comm1, String comm2, String comm3, String comm4, String comm5, String comm6, Integer excl) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ").append(TcDBContext.getSchemaName()).append("dupcheck_generic(");
		sql.append(dupid).append(",");
		appendString(sql, nachname);
		appendString(sql, vorname);
		appendString(sql, organisation);
		appendString(sql, plz);
		appendString(sql, postfach);
		appendString(sql, postfachplz);
		appendString(sql, comm1);
		appendString(sql, comm2);
		appendString(sql, comm3);
		appendString(sql, comm4);
		appendString(sql, comm5);
		appendString(sql, comm6);
        String exclStr = (excl!=null) ? ""+excl : "0";
        appendString(sql, exclStr);
        sql.append("0");
		sql.append(")");
		return sql.toString();
	}

	/**
	 * TODO: Was macht diese Methode?
	 * @param sql
	 * @param string
	 */
	private static void appendString(StringBuffer sql, String string) {
		if(string!=null)
            sql.append("'").append(string).append("'");
		else 
            sql.append("NULL");
		sql.append(",");
	}
	
	
	public static String getDupcheckResult(Integer dupid, Integer excl, boolean onlyUserViewableAdresses, Integer userid){
		String sql = 
			"SELECT TADDRESS.PK_ADDRESS "
            +	"FROM " + TcDBContext.getSchemaName() + "TADDRESS "
            +	"JOIN " + TcDBContext.getSchemaName() + "TADDRESSDUP ON TADDRESSDUP.FK_ADDRESS = TADDRESS.PK_ADDRESS ";

        if (onlyUserViewableAdresses)
            sql += "JOIN " + TcDBContext.getSchemaName() + "V_USER_ADDRESS ON TADDRESSDUP.FK_ADDRESS = V_USER_ADDRESS.FK_ADDRESS ";
                
        sql += "WHERE TADDRESSDUP.DUPCHECKID = " + dupid + " ";
        if (onlyUserViewableAdresses)
            sql += "AND V_USER_ADDRESS.USERID = "+ userid  + " ";

        sql += "AND NOT TADDRESS.PK_ADDRESS = " + excl + " "
            + 	"ORDER BY TADDRESSDUP.DUPCHECKRESULT DESC";
		return sql;
	}

	public static String deleteDupcheck(Integer dupid) {
		String sql = "DELETE FROM " + TcDBContext.getSchemaName() + "TADDRESSDUP WHERE DUPCHECKID = " + dupid;
		return sql;
	}


}
