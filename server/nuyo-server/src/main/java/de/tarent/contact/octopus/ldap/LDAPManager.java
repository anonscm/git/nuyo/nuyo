/* $Id: LDAPManager.java,v 1.3 2006/09/11 08:43:07 kirchner Exp $
 * 
 * Created on 02.05.2003 by philipp
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.ldap;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.tarent.octopus.config.TcConfig;

/**
 * f�r Zugriff auf ein LDAP-Verzeichnis
 * 
 * @author philipp
 */
public class LDAPManager {
	//Hashtable mit Zugriffsdaten
	private Hashtable<String, String> env;
	// Der n�tige LDAPContext
	private InitialLdapContext lctx;
	// Base DN f�r's LDAP
	private String baseDN;
	// ein relativer DN, falls alles, was behandelt wird relativ zum Base DN gesehen werden soll
	private String relative;
	
	private TcConfig tcConfig;

	
	/**
		 * Erzeugt einen LDAPManager
		 * @param config
		 */
	public LDAPManager(TcConfig config)
	{
		this.baseDN = config.getModuleConfig().getParam("ldapbasedn"); //$NON-NLS-1$
		env = new Hashtable<String, String>();
		env.put(
			Context.INITIAL_CONTEXT_FACTORY,
			"com.sun.jndi.ldap.LdapCtxFactory"); //$NON-NLS-1$
		env.put(Context.PROVIDER_URL, config.getModuleConfig().getParam("ldapurl")); //$NON-NLS-1$
		this.relative = cleanup_relative(config.getModuleConfig().getParam("ldaprelative")); //$NON-NLS-1$
		this.tcConfig = config;
	}
	

	/**
	 * Methode, die einen gegebenen Benutzer einloggt.
	 * @param username Benutzername entweder nur als Benutzername, wenn appendBaseDN
	 * gesetzt ist, sonst als komplette DN des Users
	 * @param appendBaseDN	Soll der Base-DN angeh�ngt werden?
	 * @param passwort	Passwort
	 * @param authType	Authentifizierungstyp gegen�ber dem LDAP (im Moment nur "simple" ausprobiert & supported)
	 * @throws LDAPException wenn etwas schiefl�uft
	 */
	public void login(
		String username,
		boolean appendBaseDN,
		String passwort,
		String authType)
		throws LDAPException
	{
		env.put(Context.SECURITY_AUTHENTICATION, authType);
		String security_principal = username;
		if (appendBaseDN)
		{
			security_principal = "uid=" + security_principal + "," + baseDN; //$NON-NLS-1$ //$NON-NLS-2$
		}
		env.put(Context.SECURITY_PRINCIPAL, security_principal);
		env.put(Context.SECURITY_CREDENTIALS, passwort);
		try
		{
			//Versuche, LDAP-Verbindung herzustellen
			lctx = new InitialLdapContext(env, null);
		} catch (AuthenticationException e)
		{
			//Falls die Authentification nicht stimmt
			throw new LDAPException(Messages.getString("LDAPManager.user_or_password_incorrect_01")); //$NON-NLS-1$
		} catch (CommunicationException e)
		{
			//Falls LDAP-Server nicht erreichbar
			throw new LDAPException(Messages.getString("LDAPManager.ldap_not_reachable_01")); //$NON-NLS-1$
		} catch (NamingException e)
		{
			//sonst
			throw new LDAPException(Messages.getString("LDAPManager.commication_error_01")); //$NON-NLS-1$
		}
	}

	/**
	 * Anonymer Login
	 * @throws LDAPException
	 */
	public void login() throws LDAPException
	{
		env.put(Context.SECURITY_AUTHENTICATION, "none"); //$NON-NLS-1$
		try
		{
			//Versuche, LDAP-Verbindung herzustellen
			lctx = new InitialLdapContext(env, null);
		} catch (CommunicationException e)
		{
			//Falls LDAP-Server nicht erreichbar
			throw new LDAPException(Messages.getString("LDAPManager.ldap_not_reachable_01")); //$NON-NLS-1$
		} catch (NamingException e)
		{
			//sonst
			throw new LDAPException(Messages.getString("LDAPManager.commication_error_01")); //$NON-NLS-1$
		}
	}


	/**
	 * F�gt einen Contact zum LDAP hinzu
	 * @param ldc  der Kontakt, der hinzu gef�gt werden soll
	 * @throws LDAPException  wenn etwas schief l�uft
	 */
	public void addContact(LDAPContact ldc) throws LDAPException{
		if(lctx == null)
		{ //Falls noch nicht eingeloggt, tue es
			this.login();
		}
		//Object anlegen
		try {
			lctx.createSubcontext("uid="+ldc.getUserid()+relative+baseDN, generate_Attributes(ldc)); //NON-NLS-1$ //$NON-NLS-1$
		}catch (NameAlreadyBoundException nabe){
			 throw new LDAPException(Messages.getString("LDAPManager.entry_already_there_01")); //$NON-NLS-1$
		}catch (NamingException e) {
			throw new LDAPException(Messages.getString("LDAPManager.fehler_anlegen_01") + e.getMessage()); //$NON-NLS-1$
		}
		
	}
	/**
	 * F�gt einen Contact zum LDAP hinzu
	 * @param ldc  der Kontakt, der hinzu gef�gt werden soll
	 * @throws LDAPException  wenn etwas schief l�uft
	 */
	public void addContact_restricted(LDAPContact ldc) throws LDAPException{
		if(lctx == null)
		{ //Falls noch nicht eingeloggt, tue es
			this.login();
		}
		//Object anlegen
		try {
			BasicAttributes attrs = generate_Attributes_restricted(ldc);
			
			lctx.createSubcontext("uid="+ldc.getUserid()+",ou="+ldc.getVerteilergruppe()+relative+baseDN, attrs); //NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-2$
		}catch (NameAlreadyBoundException nabe){
			 throw new LDAPException(Messages.getString("LDAPManager.entry_already_there_01")); //$NON-NLS-1$
		}catch (NamingException e) {
			throw new LDAPException(Messages.getString("LDAPManager.fehler_anlegen_01") + e.getMessage()); //$NON-NLS-1$
		}
		
	}
	
	/**
	 * Generiert aus einen LDAPContact ein BasicAttributes
	 * @param ldc  Kontakt, der generiert werden soll
	 * @return BasicAttributes, die man dann im LDAP verwenden kann
	 * @throws LDAPException Wenn was daneben geht
	 * @see LDAPContact
	 * @see BasicAttributes
	 **/
	private BasicAttributes generate_Attributes(LDAPContact ldc) throws LDAPException{
		BasicAttributes attr = new BasicAttributes();
		Element mapping = null;
		//Hole XML-Mapping
		try {
			mapping = XMLUtil.getParsedDocument(tcConfig.getCommonConfig().getConfigData("paths.root") + tcConfig.getCommonConfig().getConfigData("paths.configRoot") + "mapping.xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} catch (Exception e) {
			throw new LDAPException(e.getMessage());
		}
		Node objectclassnode = XMLUtil.getObjectClass(mapping);
		NodeList objectClassList = objectclassnode.getChildNodes();
		//Hole objectclass aus dem XML
		BasicAttribute objectclass = new BasicAttribute("objectclass", objectClassList.item(0).getAttributes().item(0).getNodeValue()); //$NON-NLS-1$ //$NON-NLS-2$
		for(int i=1; i<objectClassList.getLength(); i++){
			objectclass.add(objectClassList.item(i).getAttributes().item(0).getNodeValue());
		}
		attr.put(objectclass);
		// Hole alles andere aus dem XML
		NodeList childs = XMLUtil.getrelevantChilds(mapping);
		for(int i=0; i<childs.getLength();i++){
			String attribut = childs.item(i).getAttributes().item(0).getNodeValue();
			String value = getValue(ldc, childs.item(i).getAttributes());
			if((value!=null)&(value!="")){ //$NON-NLS-1$
				attr.put(attribut, value);
			}
		}
		return attr;
	}

	/**
	 * Generiert aus einen LDAPContact ein BasicAttributes
	 * @param ldc  Kontakt, der generiert werden soll
	 * @return BasicAttributes, die man dann im LDAP verwenden kann
	 * @throws LDAPException Wenn was daneben geht
	 * @see LDAPContact
	 * @see BasicAttributes
	 **/
	private BasicAttributes generate_Attributes_restricted(LDAPContact ldc) throws LDAPException{
		BasicAttributes attr = new BasicAttributes();
		Element mapping = null;
		//Hole XML-Mapping
		try {
			mapping = XMLUtil.getParsedDocument(tcConfig.getCommonConfig().getConfigData("paths.root") + tcConfig.getCommonConfig().getConfigData("paths.configRoot") + "mapping.xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			
		} catch (Exception e) {
			throw new LDAPException(e.getMessage());
		}
		Node objectclassnode = XMLUtil.getObjectClass(mapping);
		NodeList objectClassList = objectclassnode.getChildNodes();
		//Hole objectclass aus dem XML
		BasicAttribute objectclass = new BasicAttribute("objectclass"); //$NON-NLS-1$ //$NON-NLS-2$
		//System.out.println(nl);
		for(int i=0; i<objectClassList.getLength(); i++){
			objectclass.add(objectClassList.item(i).getAttributes().item(0).getNodeValue());
		}
		attr.put(objectclass);
		// Hole alles andere aus dem XML
		NodeList childs = XMLUtil.getrelevantChilds(mapping);
		for(int i=0; i<childs.getLength();i++){
			String attribut = childs.item(i).getAttributes().item(0).getNodeValue();
			String value = getValue(ldc, childs.item(i).getAttributes());
			if((value!=null)&(!value.equals(""))){ //$NON-NLS-1$
				attr.put(attribut, value);
			}
		}
		BasicAttribute users = new BasicAttribute("member"); //$NON-NLS-1$
		Iterator it = ldc.getUsers().iterator();
		while(it.hasNext()){
			String adduser = (String) it.next();
			String adduser2 = "uid="+adduser+relative+baseDN; //$NON-NLS-1$
			//System.out.println(adduser2);
			users.add(adduser2);
		}
		attr.put(users);

		return attr;
	}

	/**
	 * Methode, die einen Kontakt l�scht
	 * @param uid UserID des Kontakts
	 * @throws LDAPException wenn etwas schief l�uft
	 */
	public void delContact(String uid) throws LDAPException{
		//Wenn noch nicht eingeloggt, versuche es anonym
		if(lctx==null){this.login();}
		//Object l�schen
		try {
			lctx.destroySubcontext(uid + relative + baseDN);
		} catch (NamingException e) {
			new LDAPException(Messages.getString("LDAPManager.fehler_l�schen_01")); //$NON-NLS-1$
		}
	}
	
	/**
   * @param ou ou in der Form "ou=blahbla"
   * @throws LDAPException
   */
  public void createOU(String ou) throws LDAPException{
		//Wenn noch nicht eingeloggt, versuche es anonym
		if(lctx==null){this.login();}
		//angegebene ou anlegen
		try {
			BasicAttributes attr = new BasicAttributes();
			BasicAttribute objectclass = new BasicAttribute("objectclass", "top"); //$NON-NLS-1$ //$NON-NLS-2$
			objectclass.add("organizationalUnit"); //$NON-NLS-1$
			attr.put(objectclass);
			lctx.createSubcontext(ou + relative + baseDN, attr);			
		}
		catch (Exception e) {
			throw new LDAPException(Messages.getString("LDAPManager.Konnte_OU_nicht_anlegen_01") + e.getMessage()); //$NON-NLS-1$
    }
	}
	
	/**
	 * @param ou ou 
	 * @param user uid's der User die Zugriff auf die OU erhalten sollen
	 * @throws LDAPException
	 */
	public void createOU(String ou, List user) throws LDAPException{
		//Wenn noch nicht eingeloggt, versuche es anonym
		if(lctx==null){this.login();}
		//angegebene ou anlegen
		try {
			BasicAttributes attr = new BasicAttributes();
			BasicAttribute objectclass = new BasicAttribute("objectclass", "top"); //$NON-NLS-1$ //$NON-NLS-2$
			//objectclass.add("organizationalUnit");
			objectclass.add("groupOfNames"); //$NON-NLS-1$
			attr.put(objectclass);
			attr.put("cn", ou); //$NON-NLS-1$
			//attr.put("ou", ou);
			BasicAttribute users = new BasicAttribute("member"); //$NON-NLS-1$
			Iterator it = user.iterator();
			while(it.hasNext()){
				String adduser = (String) it.next();
				String adduser2 = "uid="+adduser+relative+baseDN; //$NON-NLS-1$
				//System.out.println(adduser2);
				users.add(adduser2);
			}
			attr.put(users);
			lctx.createSubcontext("ou=" + ou + relative + baseDN, attr);			 //$NON-NLS-1$
		}
		catch (Exception e) {
			throw new LDAPException(Messages.getString("LDAPManager.Konnte_OU_nicht_anlegen_01")+e.getMessage()); //$NON-NLS-1$
		}
	}

	/**
	 * Methode die testet, ob User name bestimmtes Attribut hat
	 * @param name UserID, des zu testenden User
	 * @param attribut Attribut, auf welches getestet werden soll
	 * @return true, wenn vorhanden, false sonst
	 */
	private boolean checkAttribute(String name, String attribut){
		String relative = this.relative.substring(1);
		boolean vorhanden = false;
		//Wenn noch nicht eingeloggt, hole es nach
		if(lctx==null){
			try{
				this.login();
			}
			catch (LDAPException e1){
				vorhanden = false;
			}
		}
		try{
			//Baue zu suchendes Attribut
			BasicAttributes zusuchendeAttribute = new BasicAttributes(true);
			//Suche nach uid
			zusuchendeAttribute.put( new BasicAttribute("uid", name)); //$NON-NLS-1$
			//nach folgendem Attribut
			zusuchendeAttribute.put( new BasicAttribute(attribut) );
			//ausf�hren!
			NamingEnumeration ergebnis = lctx.search(relative + baseDN, zusuchendeAttribute);
			//Wenn was kommt, gibt es das Attribut
			if(ergebnis.hasMore()){
				vorhanden = true;
			}
		}
		catch (NamingException e){
			//Wenn was schief l�uft, ist Attribut nicht vorhanden
			vorhanden = false;
		}
		return vorhanden;
	}
	
	/**
	 * Methode, die testet, ob User name im LDAP das bestimmte Attribut
	 * mit dem bestimmten Wert hat.
	 * @param name	uid des Objekts
	 * @param attribute	zu suchendes Attribut
	 * @param wert	Wert des Attributes
	 * @return	true, wenn Wert vorhanden, false sonst, auch bei anderen Fehlern
	 */
	private boolean checkAttribute(String name, String attribute, String wert){
		boolean vorhanden = false;
		//Einloggen, wenn nicht schon eh geschehen
		if(lctx == null){
			try {
				this.login();
			} catch (LDAPException e) {
			vorhanden = false;
			}
		}
		try {
			//Zu suchende Attribute zusammenbauen
			Attributes attributes = lctx.getAttributes("uid="+name+relative+baseDN); //$NON-NLS-1$
			//hole Attrbute
			Attribute tester = attributes.get(attribute);
			NamingEnumeration liste = null;
			if(tester!=null){
				//hole alle
				liste = tester.getAll();
				while(liste.hasMore()){
					String testwert = (String) liste.next();
					//Teste durch, ob wert vorhanden
					if(testwert.equals(wert)){
						vorhanden = true; 
					}
				}
			}
		} catch (NamingException e) {
			//Im Fehlerfall ist der Wert nat�rlich nicht vorhanden
			vorhanden = false;
		}
		return vorhanden;
	}
	
	/**
		 * Methode, die testet, ob OU name im LDAP das bestimmte Attribut
		 * mit dem bestimmten Wert hat.
		 * @param name	ou des Objekts
		 * @param attribute	zu suchendes Attribut
		 * @param wert	Wert des Attributes
		 * @return	true, wenn Wert vorhanden, false sonst, auch bei anderen Fehlern
		 */
		private boolean checkAttributeOU(String name, String attribute, String wert){
			boolean vorhanden = false;
			//Einloggen, wenn nicht schon eh geschehen
			if(lctx == null){
				try {
					this.login();
				} catch (LDAPException e) {
				vorhanden = false;
				}
			}
			try {
				//Zu suchende Attribute zusammenbauen
				Attributes attributes = lctx.getAttributes("ou="+name+relative+baseDN); //$NON-NLS-1$
				//hole Attrbute
				Attribute tester = attributes.get(attribute);
				NamingEnumeration liste = null;
				if(tester!=null){
					//hole alle
					liste = tester.getAll();
					while(liste.hasMore()){
						String testwert = (String) liste.next();
						//Teste durch, ob wert vorhanden
						if(testwert.equals(wert)){
							vorhanden = true; 
						}
					}
				}
			} catch (NamingException e) {
				//Im Fehlerfall ist der Wert nat�rlich nicht vorhanden
				vorhanden = false;
			}
			return vorhanden;
		}

	/**
	 * Testet, ob userid im LDAP vorhanden
	 * @param userid	UserID, die getestet werden soll
	 * @return	true, wenn vorhanden, false sonst
	 * @throws LDAPException, wenn Fehler auftritt
	 */
	public boolean checkUid(String userid) throws LDAPException{
		boolean vorhanden = false;
		if(lctx==null){
			this.login();
		}
		try
		{
			BasicAttributes zusuchendeAttribute = new BasicAttributes(true);
			zusuchendeAttribute.put("uid",userid); //$NON-NLS-1$
			NamingEnumeration ergebnis = lctx.search(relative.substring(1) + baseDN, zusuchendeAttribute);
			if(ergebnis.hasMore()){
				vorhanden = true;
			}
		}
		catch (NamingException e)
		{
			vorhanden = false;
		}
		return vorhanden;
	}

	/**
		 * Testet, ob ou im LDAP vorhanden
		 * @param ou	ou, die getestet werden soll
		 * @return	true, wenn vorhanden, false sonst
		 * @throws LDAPException, wenn Fehler auftritt
		 */
		public boolean checkOu(String ou) throws LDAPException{
			boolean vorhanden = false;
			if(lctx==null){
				this.login();
			}
			try
			{
				BasicAttributes zusuchendeAttribute = new BasicAttributes(true);
				zusuchendeAttribute.put("ou",ou); //$NON-NLS-1$
				NamingEnumeration ergebnis = lctx.search(relative.substring(1) + baseDN, zusuchendeAttribute);
				//System.out.print("Test ou=" + ou + relative + baseDN);
				if(ergebnis.hasMore()){
					vorhanden = true;
					//System.out.print(" - gefunden!");
				}
			}
			catch (NamingException e)
			{
				//System.out.print("Test ou=" + ou + relative + baseDN);
				vorhanden = false;
			}
			//System.out.println();
			return vorhanden;
		}
		
	/**
	 * Testet, ob Kontakt im LDAP vorhanden
	 * @param ldc	Kontakt, die getestet werden soll
	 * @return	true, wenn vorhanden, false sonst
	 * @throws LDAPException, wenn Fehler auftritt
	 */
	public boolean checkContact(LDAPContact ldc) throws LDAPException{
		boolean vorhanden = false;
		if(lctx==null){
			this.login();
		}
		try
		{
			BasicAttributes zusuchendeAttribute = new BasicAttributes(true);
			zusuchendeAttribute.put("uid",ldc.getUserid()); //$NON-NLS-1$
			NamingEnumeration ergebnis = lctx.search("ou=" + ldc.getVerteilergruppe()  + relative + baseDN, zusuchendeAttribute); //$NON-NLS-1$
			if(ergebnis.hasMore()){
				vorhanden = true;
			}
		}
		catch (NamingException e)
		{
			vorhanden = false;
		}
		return vorhanden;
	}
	
	/**
	 * Methode, die Attribute modifiziert
	 * @param mods Vector mit schon bestehenden Modifikationen
	 * @param userid UserID des zu modifizierenden Objekts im LDAP
	 * @param attribute Attribut, das modifiziert werden soll
	 * @param value Wert des Attributes
	 * @return Vector mit neuen Mofifikationen
	 */
	private List<ModificationItem> modifyAttributes(List<ModificationItem> mods, String userid, String attribute, String value){
		if((value!=null)&(!value.equals(""))){ //$NON-NLS-1$
			if(!this.checkAttribute(userid, attribute)){
				mods.add(new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute(attribute, value)));
			}else if(!this.checkAttribute(userid, attribute, value)){
				mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(attribute, value)));
			}
		}
		return mods;
	}
	
	/**
	 * Methode, die einen gegebenen Kontakt im LDAP modifiziert
	 * @param ldc  der zu modifizierende Kontakt
	 * @throws LDAPException wenn etwas schief l�uft
	 * @see de.tarent.ldap.LDAPContact
	 */
	public void modifyContact(LDAPContact ldc) throws LDAPException{
		List<ModificationItem> modifications = new ArrayList<ModificationItem>();
		Element mapping = null;
		try {
			mapping = XMLUtil.getParsedDocument(tcConfig.getCommonConfig().getConfigData("paths.root") + tcConfig.getCommonConfig().getConfigData("paths.configRoot") + "mapping.xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} catch (Exception e) {
			throw new LDAPException(e.getMessage());
		}
		Node objectclassnode = XMLUtil.getObjectClass(mapping);
		NodeList objectClassList = objectclassnode.getChildNodes();
		//relative filtern
		relative = cleanup_relative(relative);
		//alte objectclass aus LDAP holen
		Attribute objectClass;
		try {
			String[] objectClassA = {"objectClass"}; //$NON-NLS-1$
			objectClass = lctx.getAttributes("uid=" + ldc.getUserid() + relative +baseDN, objectClassA).get("objectClass"); //$NON-NLS-1$ //$NON-NLS-2$
		} catch (NamingException e1) {
			throw new LDAPException(Messages.getString("LDAPManager.Konnte_ObjectClass_nicht_sichern_01")); //$NON-NLS-1$
		}
		//checke objectclass
		boolean modified = false;
		for(int i=0; i< objectClassList.getLength(); i++){
			String value = objectClassList.item(i).getAttributes().item(0).getNodeValue();
			if(!checkAttribute(ldc.getUserid(), "objectclass", value)){objectClass.add(value);modified=true;} //$NON-NLS-1$
		}
		//Wenn ge�ndert, dann modifizieren
		if(modified){
			modifications.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, objectClass));
		}
		NodeList kinder = XMLUtil.getrelevantChilds(mapping);
		for(int i=0; i < kinder.getLength(); i++){
			modifications = modifyAttributes(modifications, ldc.getUserid(), kinder.item(i).getAttributes().item(0).getNodeValue(), getValue(ldc, kinder.item(i).getAttributes())); 
		}
		//F�hre �nderungen durch
		try {
			lctx.modifyAttributes("uid="+ldc.getUserid()+relative+baseDN, (ModificationItem[]) modifications.toArray(new ModificationItem[1])); //$NON-NLS-1$
		} catch (NamingException e2) {
			throw new LDAPException(Messages.getString("LDAPManager.User_konnte_nicht_modifiziert_werden_01") + e2.toString()); //$NON-NLS-1$
		}

		//System.out.println(modifications);
	}
	/**
	 * Methode, die einen gegebenen Kontakt im LDAP modifiziert
	 * @param ldc  der zu modifizierende Kontakt
	 * @throws LDAPException wenn etwas schief l�uft
	 * @see de.tarent.ldap.LDAPContact
	 */
	public void modifyContact_restricted(LDAPContact ldc) throws LDAPException{
		List<ModificationItem> modifications = new ArrayList<ModificationItem>();
		Element mapping = null;
		try {
			mapping = XMLUtil.getParsedDocument(tcConfig.getCommonConfig().getConfigData("paths.root") + tcConfig.getCommonConfig().getConfigData("paths.configRoot") + "mapping.xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} catch (Exception e) {
			throw new LDAPException(e.getMessage());
		}
		Node objectclassnode = XMLUtil.getObjectClass(mapping);
		NodeList objectClassList = objectclassnode.getChildNodes();
		//relative filtern
		relative = cleanup_relative(relative);
		//alten Contact aus dem LDAP holen
		Attributes contact = null;
		try {
      		contact = lctx.getAttributes("uid="+ldc.getUserid()+",ou="+ldc.getVerteilergruppe()+relative+baseDN); //$NON-NLS-1$ //$NON-NLS-2$
    	} catch (NamingException e1) {
    		throw new LDAPException(Messages.getString("LDAPManager.Contact_01")+ldc.getUserid()+Messages.getString("LDAPManager._existiert_gar_nicht_01") + e1.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
	    }
	    //hole ObjectClass
	    Attribute objectClass = contact.get("objectClass"); //$NON-NLS-1$
		//checke objectclass
		boolean modified = false;
		for(int i=0; i< objectClassList.getLength(); i++){
			String value = objectClassList.item(i).getAttributes().item(0).getNodeValue();
			if(!checkAttribute(contact, "objectClass", value)){objectClass.add(value);modified=true;} //$NON-NLS-1$
		}
		//Wenn ge�ndert, dann modifizieren
		if(modified){
			modifications.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, objectClass));
		}
		//hole member
		Attribute member = contact.get("member"); //$NON-NLS-1$
		//checke objectclass
		modified = false;
		for(int i=0; i< ldc.getUsers().size(); i++){
			String value = "uid="+ldc.getUsers().get(i).toString()+relative+baseDN; //$NON-NLS-1$
			if(!checkAttribute(contact, "member", value)){member.add(value);modified=true;} //$NON-NLS-1$
		}
		//Wenn ge�ndert, dann modifizieren
		if(modified){
			modifications.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, member));
		}
		//Checke Rest vom Kontakt
		NodeList kinder = XMLUtil.getrelevantChilds(mapping);
		for(int i=0; i < kinder.getLength(); i++){
			modifications = modifyAttributes_restricted(modifications, contact, kinder.item(i).getAttributes().item(0).getNodeValue(), getValue(ldc, kinder.item(i).getAttributes())); 
		}
		//F�hre �nderungen durch
		if(!modifications.isEmpty()){
			//System.out.println("Modifikation: " + modifications);
			try {
				lctx.modifyAttributes("uid="+ldc.getUserid()+",ou="+ldc.getVerteilergruppe()+relative+baseDN, (ModificationItem[]) modifications.toArray(new ModificationItem[1])); //$NON-NLS-1$ //$NON-NLS-2$
			} catch (NamingException e2) {
				throw new LDAPException(Messages.getString("LDAPManager.User_konnte_nicht_modifiziert_werden_01") + e2.toString()); //$NON-NLS-1$
			}
		}
		

			
	}

	
	/**
   * @param modifications
   * @param contact
   */
  private List<ModificationItem> modifyAttributes_restricted(List<ModificationItem> modifications, Attributes contact, String attribut, String wert) {
	if((wert!=null)&(!wert.equals(""))){ //$NON-NLS-1$
		if(!this.checkAttribute(contact, attribut)){
			modifications.add(new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute(attribut, wert)));
		}else if(!this.checkAttribute(contact, attribut, wert)){
			modifications.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(attribut, wert)));
		}
	}
    return modifications;
  }


  /**
   * @param contact
   * @param attribut
   */
  private boolean checkAttribute(Attributes contact, String attribut) {
    Attribute tester = contact.get(attribut);
    if(tester==null){return false;}
    else{return true;}
    }
  


  /**
   * @param contact
   * @param value
   */
  private boolean checkAttribute(Attributes contact, String attribute, String value) {
		boolean vorhanden = false;
		//hole Attribute
		Attribute tester = contact.get(attribute);
		NamingEnumeration liste = null;
		if(tester!=null){
			//hole alle
      try {
        liste = tester.getAll();
        while(liste.hasMore()){
        	String testwert = (String) liste.next();
        	//Teste durch, ob wert vorhanden
        	if(testwert.equals(value)){
        		vorhanden = true; 
        	}
        }
      } catch (NamingException e) {
		vorhanden = false;        	
      }
		}
    return vorhanden;
  }



  /**
	 * Kleiner Helper, der aufpasst, das bei relative am Anfang und am Ende ein Komma ist
	 * @param relative
	 * @return relative
	 */
	private String cleanup_relative(String relative) {
		//testen, ob am Anfang kein Komma, sonst hinzuf�gen
		if(relative.equals(" ")){relative = "";} //$NON-NLS-1$ //$NON-NLS-2$
		
		if(!relative.startsWith(",")){relative=","+relative;} //$NON-NLS-1$ //$NON-NLS-2$
		//das ganze nochmal am Ende
		if(!relative.endsWith(",")){relative=relative+",";} //$NON-NLS-1$ //$NON-NLS-2$
		return relative;
	}
	
	/**
	 * Methode, die zu einem gegebenen Attribut die passende Variable aus einem @see LDAPContact ausliest..
	 * 
	 * @param ldc - LDAPContact , aus dem gelesen werden soll
	 * 					@see de.tarent.ldap.LDAPContact
	 * @param attribute - Attribut, welches den Wert beschreibt
	 * @return String mit dem gew�nschen Wert
	 * @throws LDAPException - wenn etwas schief l�uft
	 */
	private String getValue(LDAPContact ldc, NamedNodeMap attribute) throws LDAPException{
		Method getter;
		String value;
		String attribut = attribute.item(0).getNodeValue();
		try {
			getter = ldc.getClass().getDeclaredMethod("get" + attribute.item(1).getNodeValue(),	(Class[]) null); //$NON-NLS-1$
			value = (String) getter.invoke(ldc, (Object[]) null);
			if(attribute.getLength()>2){
				getter = ldc.getClass().getDeclaredMethod("get" + attribute.item(2).getNodeValue(), (Class[]) null); //$NON-NLS-1$
				value += Messages.getString("LDAPManager.whitespace_01") + (String) getter.invoke(ldc, (Object[]) null); //$NON-NLS-1$
			}
		} catch (Exception e) {
			throw new LDAPException(Messages.getString("LDAPManager.getter_not_found_01")+ attribut +Messages.getString("LDAPManager.getter_not_found_02") + e.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
		} //$NON-NLS-1$

		return value;
	}
	/**
	 * getter f�r Relative
	 * @return relativen DN
	 */
	public String getRelative() {
		return relative;
	}

	/**
	 * Setze Relative
	 * @param string neuer relativer DN
	 */
	public void setRelative(String string) {
		relative = cleanup_relative(string);
	}

  /**
   * @param value 
   * @param user
   */
  public void modifyOU(String value, List user) throws LDAPException {
    if(lctx == null){
    	this.login();
    }
    List<ModificationItem> modifications = new ArrayList<ModificationItem>();
    //Hole objectclass aus dem LDAP
    Attribute objectclass = null;
    String[] objectClassList = {"top",  "groupOfNames"  }; //$NON-NLS-1$ //$NON-NLS-2$
    
    try{
    	String[] objectClassA = {"objectclass"}; //$NON-NLS-1$
    	objectclass = lctx.getAttributes("ou=" + value + relative + baseDN, objectClassA).get("objectClass"); //$NON-NLS-1$ //$NON-NLS-2$
    }catch(NamingException e){
    	throw new LDAPException(Messages.getString("LDAPManager.Konnte_objectClass_nicht_sichern_01") + e.getMessage());  //$NON-NLS-1$
    }
    boolean modified = false;
    //Checke objectclass
    for (int i = 0; i < objectClassList.length; i++) {
      if (!checkAttributeOU(value, "objectClass", objectClassList[i])) { //$NON-NLS-1$
        modified = true;
        objectclass.add(objectClassList[i]);
      }
    }
    if(modified){
    	//Ersetze Objectclass
    	modifications.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, objectclass));
    }
    Attribute member = new BasicAttribute("member"); //$NON-NLS-1$
    //member neu bauen
    for (int i = 0; i < user.size(); i++){
    		member.add("uid=" + user.get(i).toString() + relative + baseDN); //$NON-NLS-1$
    }
    modifications.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, member));    
		//System.out.println(modifications);
	//F�hre �nderungen durch
	try {
		lctx.modifyAttributes("ou="+value+relative+baseDN, (ModificationItem[]) modifications.toArray(new ModificationItem[1])); //$NON-NLS-1$
	} catch (NamingException e2) {
		throw new LDAPException(Messages.getString("LDAPManager.OU_konnte_nicht_modifiziert_werden_01") + e2.toString()); //$NON-NLS-1$
	}
  }

	public List getOUs(String base) throws LDAPException{
		if(lctx == null){
			this.login();
		}
    	List<String> ou = new ArrayList<String>();
		try{
			SearchControls cons = new SearchControls();
			cons.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			NamingEnumeration enumeration = lctx.search(base, "(&(objectClass=groupOfNames))", cons); //$NON-NLS-1$
			while(enumeration.hasMoreElements()){
				Attributes result = ((SearchResult) enumeration.nextElement()).getAttributes();
				ou.add(result.get("ou").toString().substring(4)); //$NON-NLS-1$
			}
		}catch(Exception e){
			
		}
		return ou;
	}

	public List getUIDs(String base) throws LDAPException{
		if(lctx == null){
			this.login();
		}
			List<String> uid = new ArrayList<String>();
		try{
			SearchControls cons = new SearchControls();
			cons.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			NamingEnumeration enumeration = lctx.search(base, "(&(objectClass=mozillaOrgPerson))", cons); //$NON-NLS-1$
			while(enumeration.hasMoreElements()){
				Attributes result = ((SearchResult) enumeration.nextElement()).getAttributes();
				uid.add(result.get("uid").toString().substring(4)); //$NON-NLS-1$
			}
		}catch(Exception e){
			
		}
		return uid;
	}
	
  public String getBaseDN() {
    return baseDN;
  }


  /**
   * @param testou
   */
  public void delOU(String testou) throws LDAPException{
		//Wenn noch nicht eingeloggt, versuche es anonym
		if(lctx==null){this.login();}
		//checke, ob Objekt kinder hat
		try{
			List uids = this.getUIDs("ou="+testou+relative+baseDN);
			for(int i=0; i<uids.size(); i++){
				delContact("uid="+uids.get(i)+",ou="+testou);
			}
		}catch (LDAPException e){
			new LDAPException(Messages.getString("LDAPManager.fehler_l�schen_01")); //$NON-NLS-1$
		}
		//Object l�schen
		try {
			lctx.destroySubcontext("ou=" + testou + relative + baseDN); //$NON-NLS-1$
		} catch (NamingException e) {
			new LDAPException(Messages.getString("LDAPManager.fehler_l�schen_01")); //$NON-NLS-1$
		}
	}
}
