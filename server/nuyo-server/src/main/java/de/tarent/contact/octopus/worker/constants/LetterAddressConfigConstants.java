package de.tarent.contact.octopus.worker.constants;

public class LetterAddressConfigConstants {

	public static final String INSTISTUTION = "institution";
	public static final String HERRFRAU = "herrfrau";
	public static final String AKADTITEL = "akadtitel";
	public static final String VORNAME = "vorname";
	public static final String TITEL = "titel";
	public static final String NACHNAME = "nachname";
	public static final String NAMENSZUSATZ = "namenszusatz";
	public static final String POSITION = "position";
	public static final String ABTEILUNG = "abteilung";
	public static final String STRASSE = "strasse";
	public static final String HAUSNUMMER = "hausnummer";
	public static final String PFPLZ = "pfplz";
	public static final String POSTFACH = "postfach";
	public static final String PLZ = "plz";
	public static final String ORT ="ort";
	public static final String LAND = "land";
}
