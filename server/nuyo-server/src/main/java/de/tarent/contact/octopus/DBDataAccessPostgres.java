
/* $Id: DBDataAccessPostgres.java,v 1.33 2007/06/13 17:01:54 fkoester Exp $
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Thomas Fuchs and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.contact.octopus;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.bean.TactionDB;
import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TuserparamDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.db.queries.Queries;
import de.tarent.contact.octopus.finder.XMLSearchHandlerPostgres;
import de.tarent.contact.octopus.ldap.LDAPManager;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.sql.ParamValue;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.SQLStatementException;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.ExtPreparedStatement;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.octopus.config.TcConfig;

/**
 * Dieser Octopus-Datenbank-Wrapper benutzt die f�r den Contact-Client
 * entwickelten Datenbankzugriffsm�glichkeiten.
 * 
 * Diese Version ist speziell f�r die Postgres vorgesehen.
 * 
 * 2005-10-08 Die Klasse wurde jetzt auf DBLayer umgestellt.
 * recht bald sollten die verbleibenden benutzten Methoden in 
 * die groupware-Packages verschoben werden.
 * 
 * @author mikel
 */
public class DBDataAccessPostgres implements DataAccess {

    private final static Logger logger = Logger.getLogger(DBDataAccessPostgres.class.getName());
	
	//Konstanten f�r TACTION TODO: auslagern?
    public final static String KEY_SUBJECT = "subject";
    public final static String KEY_NOTE = "note";
    public final static String KEY_STRUCT_INFO = "structInfo";
    
    /** Kontaktkategorien: Gespraechsnotiz, Terminanfrage, Postversand */
    public final static int CATEGORY_MEMO = 0;
    public final static int CATEGORY_REQUEST = 1;
    public final static int CATEGORY_MAILING = 2;
    
    /** Kontaktkanaele: E-Mail, Telefon, Post, Fax, pers�nlich */
    public final static int CHANNEL_NOT_ASSIGNED = 0;
    public final static int CHANNEL_EMAIL = 1;
    public final static int CHANNEL_PHONE = 2;
    public final static int CHANNEL_MAIL = 3;
    public final static int CHANNEL_FAX = 4;
    public final static int CHANNEL_DIRECT = 5;
    
    
    /** Linktypen: E-Mail, Termin, Aufgabe, Dokument */
    public final static int LINKTYPE_EMAIL = 1;
    public final static int LINKTYPE_APPOINTMENT = 2;
    public final static int LINKTYPE_JOB = 3;
    public final static int LINKTYPE_DOCUMENT = 4;
    
    /** Kontakt-Richtungen    */
    public final static int DIRECTION_OUTBOUND = 0;
    public final static int DIRECTION_INBOUND = 1;

	//LDAP-Manager
	LDAPManager ldm;

	protected static final SimpleDateFormat SQL_DATE_FORMAT =
		new SimpleDateFormat("");

    public final static Level LOG_LEVEL_SQL_STMTS = Level.INFO;

	/**
	 * Constructor for DBDataAccess.
	 */
	public DBDataAccessPostgres() {
	}

    /**
     *  F�hrt ein SQL Kommando aus.
     * @return Anzahl der betroffenen Zeilen
     */
    public int doSql(String sql)
        throws SQLException {

		Connection connection = null;
        Statement statement = null;
		try {
			connection = DB.getConnection(TcDBContext.getDefaultContext());
			statement = connection.createStatement();
			logger.fine(sql);
			return statement.executeUpdate(sql);             
		} catch (SQLException e) {
			throw new SQLStatementException(e, sql);
		} finally {
			DB.close(statement);
		}
    }


    /**
     * Liefert ein fertiges ResultSet zu dem �bergebenen sql String
     */
    public ResultSet getResultSet(String sql) 
        throws SQLException {
		Connection connection = null;       
		try {
			connection = DB.getConnection(TcDBContext.getDefaultContext());
			Statement statement = connection.createStatement();
			logger.fine(sql);
			return statement.executeQuery(sql);             
		} catch (SQLException e) {
			throw new SQLStatementException(e, sql);
		}
    }

    /**
     * Liefert eine neue JDBC Connection aus dem Pool
     */
    public Connection createJdbcConnection()
        throws SQLException {
        return DB.getConnection(TcDBContext.getDefaultContext());
    }

	// Key-Values f�r den Contact-Client!
	private String[] index =
		{
			"a1",
			"a2",
			"a3",
			"a4",
			"a5",
			"a6",
			"a7",
			"a8",
			"a9",
			"a10",
			"a11",
			"a12",
			"a13",
			"a14",
			"a15",
			"a16",
			"a17",
			"a18",
			"a19",
			"a20",
			"a21",
			"a22",
			"a23",
			"a24",
			"a25" };

	// Key-Values f�r den Reiter "erweitert" werden extra gef�hrt,
	//	da einige Methoden leider von einer festen Gr�sse des Arrays index ausgehen.
	private String[] index_erweitert =
		{
			"e1",
			"e2",
			"e3",
			"e4",
			"e5",
			"e6",
			"e7",
			"e8",
			"e9",
			"e10",
			"e11",
			"e12" };

	/**
	 * Diese Methode liefert eine Map der Gruppen des 
	 * derzeitigen Users
	 */
	public Map getGruppen(String userId) {
		Map gruppen = new TreeMap();
		try {

			String sql = Queries.getGruppenQuery(userId);
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				gruppen.put(rs.getString(1), rs.getObject(2));
				i++;
			}
			rs.close();
			return gruppen;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getGruppen: Verbindung konnte nicht erstellt werden.",
				e);
			gruppen.put("fehler", e.toString());
			return gruppen;
		}
	}

	/**
	* Diese Methode liefert eine Map der Gruppen des 
	* derzeitigen Users
	*/
	public Map getGruppenExt(String userId) {
		String PARAM_ID = "id";
		String PARAM_NAME = "name";
		String PARAM_DESCRIPTION = "description";
		Map<String, Object> gruppen = new TreeMap<String, Object>();
		Map<String, String> folder = null;
		try {

			String sql = Queries.getGruppenExtQuery(userId);

			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				folder = new TreeMap<String, String>();
				folder.put(PARAM_ID, rs.getString(1));
				folder.put(PARAM_NAME, rs.getString(2));
				folder.put(PARAM_DESCRIPTION, rs.getString(3));
				gruppen.put(new Integer(i).toString(), folder);
				i++;
			}
			rs.close();
			return gruppen;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getGruppen: Verbindung konnte nicht erstellt werden.",
				e);
			gruppen.put("error", e.toString());
			return gruppen;
		}
	}
	
	/**
	 * Liefert eine Map mit allen m�glichen Verteilergruppenschl�sseln als Key und den dazugeh�rigen Usern als Liste
	 */
	public Map getAllUserGroups(){
		Map gruppen = new TreeMap();
		Map grp = getAllVerteilergruppen();
		Iterator it = grp.keySet().iterator();
		String gruppe;
		while(it.hasNext()){
			gruppe = (String) it.next();
			gruppen.put(gruppe, getUserGroups(gruppe));
		}
		return gruppen;
	}

	/**
	 * Diese Methode liefert eine Map der User der 
	 * gegebenen Verteilergruppe
	 * 
	 * @param verteilergruppe der Verteilergruppenschl�ssel
	 */
	public List getUserGroups(String verteilergruppe) {
		try {

			List user = new ArrayList();
			String sql = Queries.getUserGroupsQuery(verteilergruppe);
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				user.add(rs.getString(1));
				i++;
			}
			rs.close();
			return user;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getGruppen: Verbindung konnte nicht erstellt werden.",
				e);
			return null;
		}
	}

	/**
	 * Gibt die User-ID eines Benutzer zur�ck oder
	 * null wenn Benutzer nicht gefunden wurde.
	 *
	 * @param user
	 */
	public Integer getUserId(String user) {
		try {
			String sql = null;
			ResultSet rs = null;
			Integer userId = null;

			if (user != null && user.length() != 0) {

				sql =
					"SELECT "
						+ TcDBContext.getSchemaName()
						+ "TUSER.PK_USER "
						+ "FROM "
						+ TcDBContext.getSchemaName()
						+ "TUSER "
						+ "WHERE "
						+ TcDBContext.getSchemaName()
						+ "TUSER.LOGINNAME='"
						+ user
						+ "'";

				rs = getResultSet(sql);
				while (rs.next()) {
					userId = new Integer(rs.getObject(1).toString());
					break;
				}
				rs.close();
			}
			
			return userId;
		} catch (SQLException e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getUserId: Verbindung konnte nicht erstellt werden.",
				e);
			return null;
		}
	}

	/**
	 * Holt alle Einstellungen eines Benutzers
	 * die mit dem String anfangen.
	 * Ohne userProperties werden alle Eintraege zur�ckgegeben.
	 * 
	 * @param userId
	 * @param userProperties
	 */
	public Map getUserProperties(Integer userId, String userProperties) {
		try {
			if (userId != null) {
				Map result = new HashMap();
				WhereList wl = new WhereList();
				Select select = SQL.Select(TcDBContext.getDefaultContext())
					.from(TuserparamDB.getTableName())
					.select(TuserparamDB.PARAMNAME)
					.select(TuserparamDB.PARAMVALUE);
					wl.addAnd(Expr.equal(TuserparamDB.FKUSER, userId));
				if (userProperties != null && userProperties.length() != 0) {
						wl.addAnd(Expr.equal(TuserparamDB.PARAMNAME, userProperties + "%"));
				}
				select.where(wl);
				ResultSet rs = getResultSet(select.toString());
				while (rs.next()) {
					result.put(rs.getString(1), rs.getString(2));
				}
				rs.close();
				return result;
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "DataAccess::getUserProperties: Konnte Benutzer-Einstellungen nicht laden.", e);
		}
		return null;
	}

	/**
	 * Holt die Einstellungen eines Benutzers mit dem entsprechendem String.
	 * 
	 * @param userId
	 * @param userProperty
	 */
	public String getUserProperty(Integer userId, String userProperty) {
		try {
			if (userId != null && userProperty != null) {
				String result = null;
				WhereList wl = new WhereList();
				wl.addAnd(Expr.equal(TuserparamDB.FKUSER, userId));
				wl.addAnd(Expr.equal(TuserparamDB.PARAMNAME, userProperty));
				Select select = SQL.Select(TcDBContext.getDefaultContext())
					.from(TuserparamDB.getTableName())
					.select(TuserparamDB.PARAMNAME)
					.select(TuserparamDB.PARAMVALUE)
					.where(wl);
				ResultSet rs = getResultSet(select.toString());
				if (rs.next()) {
					result = rs.getString(2);
				}
				rs.close();
				return result;
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "DataAccess::getUserProperties: Konnte Benutzer-Einstellungen nicht laden.", e);
		}
		return null;
	}

	/**
	* Speichert Einstellungen eines Benutzers.
	* L�scht diesen wenn das value = null ist. 
	* 
	* @param userId
	* @param key
	* @param value
	*/
	public boolean setUserProperty(Integer userId, String key, String value) {
		try {
			String sql = null;
			ResultSet rs = null;
			boolean exists = false;

			// UserProperties holen
			if (userId != null && key != null && key.length() != 0) {
				sql = "SELECT paramname, paramvalue"
					+ " FROM " + TcDBContext.getSchemaName()	+ "tuserparam"
					+ " WHERE fk_user = " + userId
					+ " AND paramname = '" + key + "'";
				rs = getResultSet(sql);
				if (rs.next())
					exists = true;
				rs.close();

				if (exists) {
					if (value == null || value.length() == 0) {
						sql = "DELETE FROM " + TcDBContext.getSchemaName() + "tuserparam"
							+ " WHERE fk_user = " + userId
							+ " AND paramname = '" + key + "'";
						doSql(sql);
					} else {
						sql = "UPDATE " + TcDBContext.getSchemaName() + "tuserparam"
							+ " SET paramvalue = '" + value + "'"
							+ " WHERE fk_user = " + userId
							+ " AND paramname = '" + key + "'";
						doSql(sql);
					}
				} else if (value != null && value.length() != 0) {
					sql = "INSERT INTO " + TcDBContext.getSchemaName() + "tuserparam "
						+ "(fk_user, paramname, paramvalue)"
						+ " VALUES (" + userId + ", '" + key + "', '" + value + "')";
					doSql(sql);
				}
				return true;
			}
			return false;
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "DataAccess::setUserProperty: Konnte Benutzer-Einstellungen nicht speichern.", e);
			return false;
		}
	}

	/**
	 * L�scht die Einstellungen eines Benutzers die mit dem �bergebenem String anfangen.
	 * 
	 * @param userId
	 * @param userProperties
	 */
	public boolean removeUserProperties(Integer userId, String userProperties) {
		try {
			String sql = null;

			// UserProperties l�schen
			if (userId != null && userProperties != null && userProperties.length() != 0) {
				sql = "DELETE FROM " + TcDBContext.getSchemaName() + "tuserparam "
					+ "WHERE fk_user = " + userId
					+ " AND paramname LIKE '" + userProperties + "%'";
				doSql(sql);
				return true;
			}
			return false;
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "DataAccess::getUserProperties: Konnte Benutzer-Einstellungen nicht l?en.", e);
			return false;
		}
	}

	/**
	 * Diese Methode inserted einen �bergebenen Adressdatensatz
	 * und gibt die neue ID der Aktion zur�ck
	 */
	public Map insertAddress(Map map) {
		map = (Map) escapeSQL(map);
		Map result = new TreeMap();
		try {
			String query =
				SQLGenerator.createInsertAddressStatement(
					map,
					"TADDRESS",
					false);

			InsertKeys keys = DB.insertKeys(TcDBContext.getDefaultContext(), query);

			int i = 0;

			int id = keys.getPk();

			if (id > 0) {
				String vertgrp = (String) map.get("vertgrp");

				String sql2 =
					"INSERT INTO "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSFOLDER (fk_address, fk_folder) VALUES "
						+ "("
						+ id
						+ ", "
						+ vertgrp
						+ ")";

				doSql(sql2);

				result.put("adrId", new Integer(id));

				for (i = 1; i <= 9; i++) {
					if (map.containsKey("a10" + i)
						&& ((String) map.get("a10" + i)) != null
						&& !((String) map.get("a10" + i)).equals("")) {
						//		 				sql = "INSERT INTO "+schema+"TCOMM ("+
						//		 					  "PK_COMM, FK_ADDRESS, FK_COMMTYPE, VALUE) "+
						//		 			  		  "VALUES (0, "+id+", 10"+i+", '"+(String) map.get("a10"+i)+"')";

						String sql =
							"INSERT INTO "
								+ TcDBContext.getSchemaName()
								+ "TCOMM ("
								+ "FK_ADDRESS, FK_COMMTYPE, VALUE) "
								+ "VALUES ("
								+ id
								+ ", 10"
								+ i
								+ ", '"
								+ (String) map.get("a10" + i)
								+ "')";

						doSql(sql);
					}
				}
				if (map.containsKey("a110")
					&& ((String) map.get("a110")) != null
					&& !((String) map.get("a110")).equals("")) {
					String sql =
						"INSERT INTO "
							+ TcDBContext.getSchemaName()
							+ "TCOMM ("
							+ "FK_ADDRESS, FK_COMMTYPE, VALUE) "
							+ "VALUES ("
							+ id
							+ ", 110, '"
							+ (String) map.get("a110")
							+ "')";
					doSql(sql);
				}
			}

			return result;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::insertAddress: Verbindung konnte nicht erstellt werden.",
				e);
			result.put("fehler", e.toString());
			return result;
		}
	}

	
	/**
	 * Holt zu einer gegebenen Address ID die Vereilergruppen, zu der die Addresse geh�rt
	 * @param AdrId Address ID zu der die Verteilergruppen
	 * @return Map mit VerteilergruppenIDs als Key und Name als Value
	 */
	public Map getVertGrp(int AdrId){
		Map vertgrp = new HashMap();
		
		String sql = "SELECT TADDRESSFOLDER.fk_folder, TFOLDER.foldername from "+TcDBContext.getSchemaName()+"TADDRESSFOLDER, "+TcDBContext.getSchemaName()+"TFOLDER WHERE TADDRESSFOLDER.fk_address = '"+AdrId + "' AND TFOLDER.pk_folder = TADDRESSFOLDER.fk_folder";
		ResultSet rs = null;
		try {
			rs = getResultSet(sql);
			while(rs.next()){
				vertgrp.put(rs.getString(1), rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		
		
		return vertgrp;
	}
	
	public List getAddressList(int AdrId, String vertgrp) {
		List addresse = new ArrayList();
		try {
			int verteilergruppe = -1;
			if (vertgrp != null && vertgrp.length() != 0)
				verteilergruppe = Integer.parseInt(vertgrp);

			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SALUTATION,"
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.TITELJOB, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.TITELACADEMIC, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.FIRSTNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.LASTNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.NAMESUFFIX, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.ORGANISATION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.STREET, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.HOUSENO, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.ZIPCODE, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.CITY, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.POBOXZIPCODE, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.POBOX, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.CREATED, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.CHANGED, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SUBSCRIPTOR, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SUBSCRIPTION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.LETTERSALUTATION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.COUNTRY, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.COUNTRYSHORT, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.REGION "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.PK_ADDRESS = '"
					+ AdrId
					+ "' ";

			String sql1b =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.MIDDLENAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.NICKNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.ORGANISATION2, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.DEPARTMENT, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.NOTEONADDRESS, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.POSITION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.BANKNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.BANKNO, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.BANKACCOUNT, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SEX, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.YEAROFBIRTH, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.DATEOFBIRTH "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.PK_ADDRESS = '"
					+ AdrId
					+ "' ";

			String sql2 =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.NOTE, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.NOTE2 "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.FK_ADDRESS = '"
					+ AdrId
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.FK_FOLDER = '"
					+ verteilergruppe
					+ "' ";

			String sql3 =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.PK_SUBFOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.FOLDERNAME "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_ADDRESS = '"
					+ AdrId
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.FK_FOLDER = '"
					+ verteilergruppe
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.PK_SUBFOLDER = "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_SUBFOLDER ";

			String sql4 =
				"SELECT "
					+ "FK_COMMTYPE, VALUE "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOMM "
					+ "WHERE FK_ADDRESS = '"
					+ AdrId
					+ "' ";

			ResultSet rs = getResultSet(sql);
			if (verteilergruppe != -1) {
				ResultSet rs2 = getResultSet(sql2);
				ResultSet rs3 = getResultSet(sql3);
				String bemerkung = null;
				String stichwort = null;

				int i = 0;
				while (rs2.next()) {
					bemerkung = rs2.getString(1);
					stichwort = rs2.getString(2);
					i++;
				}
				rs2.close();

				List verteiler = new ArrayList();

				i = 0;
				while (rs3.next()) {
//					verteiler.add(rs3.getString(1), rs3.getObject(2));
					verteiler.add(rs3.getString(1));
					i++;
				}
				rs3.close();

				addresse.add(stichwort);
				addresse.add(bemerkung);
				addresse.add(verteiler);
			} else {
				addresse.add("");
				addresse.add("");
				addresse.add("");
			}

			ResultSet rs4 = getResultSet(sql4);
			ResultSetMetaData rsmd = rs.getMetaData();
			List commtypes = new ArrayList();

			int i = 0;
			while (rs4.next()) {
				commtypes.add(rs4.getString(2));
				i++;
			}
			rs4.close();

			int spalten = rsmd.getColumnCount();
			i = 0;
			while (rs.next()) {
				for (int j = 0; j < spalten; j++) {
					// key = Spaltenname, val = Object
					addresse.add(rs.getString(j + 1));
				}
				i++;
			}
			addresse.add(commtypes);
			rs.close();

			// nicht optimal f�r den Reitererweitert eine weitere Abfrage zu taetigen,
			// ansonsten m�sste das obere auseinandergerupft und mit festen Indizes
			// benutzt werden, was auch nicht wesentlich besser ist.
			ResultSet rs1b = getResultSet(sql1b);
			rsmd = rs1b.getMetaData();
			if (rs1b.next()) {
				for (int j = 1; j <= rsmd.getColumnCount() - 3; j++) {
					addresse.add(rs1b.getString(j));
				}
				addresse.add(rs1b.getString("SEX"));
				addresse.add(new Integer(rs1b.getInt("YEAROFBIRTH")));
				addresse.add(
					rs1b.getString("DATEOFBIRTH"));
			}
			rs1b.close();

		} catch (NumberFormatException e) {
			logger.log(Level.SEVERE, e.toString(), e);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, e.toString(), e);
		}

		return addresse;
	}	/**
	 * Diese Methode liefert eine Map zu einer 
	 * bestimmten Addresse
	 */
	public Map getAddress(int AdrId, String vertgrp) {
		Map addresse = new TreeMap();
		try {
			int verteilergruppe = -1;
			if (vertgrp != null && vertgrp.length() != 0)
				verteilergruppe = Integer.parseInt(vertgrp);

			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SALUTATION,"
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.TITELJOB, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.TITELACADEMIC, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.FIRSTNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.LASTNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.NAMESUFFIX, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.ORGANISATION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.STREET, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.HOUSENO, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.ZIPCODE, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.CITY, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.POBOXZIPCODE, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.POBOX, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.CREATED, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.CHANGED, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SUBSCRIPTOR, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SUBSCRIPTION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.LETTERSALUTATION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.COUNTRY, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.COUNTRYSHORT, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.REGION "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.PK_ADDRESS = '"
					+ AdrId
					+ "' ";

			String sql1b =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.MIDDLENAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.NICKNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.ORGANISATION2, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.DEPARTMENT, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.NOTEONADDRESS, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.POSITION, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.BANKNAME, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.BANKNO, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.BANKACCOUNT, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.SEX, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.YEAROFBIRTH, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.DATEOFBIRTH "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS.PK_ADDRESS = '"
					+ AdrId
					+ "' ";

			String sql2 =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.NOTE, "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.NOTE2 "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.FK_ADDRESS = '"
					+ AdrId
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER.FK_FOLDER = '"
					+ verteilergruppe
					+ "' ";

			String sql3 =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.PK_SUBFOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.FOLDERNAME "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_ADDRESS = '"
					+ AdrId
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.FK_FOLDER = '"
					+ verteilergruppe
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TSUBFOLDER.PK_SUBFOLDER = "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_SUBFOLDER ";

			String sql4 =
				"SELECT "
					+ "FK_COMMTYPE, VALUE "
					+ "FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOMM "
					+ "WHERE FK_ADDRESS = '"
					+ AdrId
					+ "' ";

			ResultSet rs = getResultSet(sql);
			if (verteilergruppe != -1) {
				ResultSet rs2 = getResultSet(sql2);
				ResultSet rs3 = getResultSet(sql3);
				String bemerkung = null;
				String stichwort = null;

				int i = 0;
				while (rs2.next()) {
					bemerkung = rs2.getString(1);
					stichwort = rs2.getString(2);
					i++;
				}
				rs2.close();

				Map verteiler = new TreeMap();

				i = 0;
				while (rs3.next()) {
					verteiler.put(rs3.getString(1), rs3.getObject(2));
					i++;
				}
				rs3.close();

				addresse.put(index[index.length - 3], stichwort);
				addresse.put(index[index.length - 2], bemerkung);
				addresse.put(index[index.length - 1], verteiler);
			} else {
				addresse.put(index[index.length - 3], "");
				addresse.put(index[index.length - 2], "");
				addresse.put(index[index.length - 1], new TreeMap());
			}

			ResultSet rs4 = getResultSet(sql4);
			ResultSetMetaData rsmd = rs.getMetaData();
			Map commtypes = new TreeMap();

			int i = 0;
			while (rs4.next()) {
				commtypes.put(rs4.getString(1), rs4.getString(2));
				i++;
			}
			rs4.close();

			int spalten = rsmd.getColumnCount();
			i = 0;
			while (rs.next()) {
				for (int j = 0; j < spalten; j++) {
					// key = Spaltenname, val = Object
					addresse.put(index[j], rs.getString(j + 1));
				}
				i++;
			}
			addresse.put(index[index.length - 4], commtypes);
			rs.close();

			// nicht optimal f�r den Reitererweitert eine weitere Abfrage zu taetigen,
			// ansonsten m�sste das obere auseinandergerupft und mit festen Indizes
			// benutzt werden, was auch nicht wesentlich besser ist.
			ResultSet rs1b = getResultSet(sql1b);
			rsmd = rs1b.getMetaData();
			if (rs1b.next()) {
				for (int j = 1; j <= rsmd.getColumnCount() - 3; j++) {
					addresse.put(index_erweitert[j - 1], rs1b.getString(j));
				}
				addresse.put(index_erweitert[9], rs1b.getString("SEX"));
				addresse.put(
					index_erweitert[10],
					new Integer(rs1b.getInt("YEAROFBIRTH")));
				addresse.put(
					index_erweitert[11],
					rs1b.getString("DATEOFBIRTH"));
			}
			rs1b.close();

		} catch (NumberFormatException e) {
			logger.log(Level.SEVERE, e.toString(), e);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, e.toString(), e);
		}

		return addresse;
	}

	private static boolean isNumber(Object s) {
		if (s == null)
			return false;
		if (s instanceof Integer || s.equals(""))
			return true;
		try {
			Integer.parseInt(s.toString());
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	/**
	 * Diese Methode updated einen �bergebenen Adressdatensatz
	 */
	public void updateAddress(Map map) {

		map = (Map) escapeSQL(map);
		String sql = null;
		try {
			int adrId = Integer.parseInt(map.get("adrId").toString());

			sql =
				SQLGenerator.createUpdateAddressStatement(
					map,
					TcDBContext.getSchemaName(),
					"TADDRESS",
					TcDBContext.getSchemaName() + "TADDRESS.PK_ADDRESS = '" + adrId + "'");

			doSql(sql);

			sql =
				"SELECT FK_COMMTYPE FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOMM WHERE FK_ADDRESS = "
					+ adrId;

			ResultSet rs = getResultSet(sql);

			Map val = new TreeMap();
			while (rs.next())
				val.put(rs.getString(1), null);
			rs.close();

			for (int i = 1; i <= 9; i++) {
				if (map.containsKey("a10" + i)
					&& ((String) map.get("a10" + i)) != null) {
					// Es gibt bereits einen Eintrag mit der pk und dem Typ vorhanden
					// dann wird ein update ausgef\x{00FC}hrt.
					if (val.containsKey("10" + i)) {

						if (((String) map.get("a10" + i)).length() > 0) {
							if (!(((String) map.get("a10" + i))
								.equals(val.get("10" + i)))) {
								sql =
									"UPDATE "
										+ TcDBContext.getSchemaName()
										+ "TCOMM SET VALUE = '"
										+ (String) map.get("a10" + i)
										+ "' "
										+ "WHERE FK_ADDRESS = "
										+ adrId
										+ " AND FK_COMMTYPE = 10"
										+ i;

								doSql(sql);
							}
						} else {
							sql =
								"DELETE FROM "
									+ TcDBContext.getSchemaName()
									+ "TCOMM WHERE FK_ADDRESS = "
									+ adrId
									+ " AND FK_COMMTYPE = 10"
									+ i;
							doSql(sql);
						}

					}
					// Unter der pk gibt es zu diesem Typ kein Eintrag
					//	dann wird ein insert ausgef\x{00FC}hrt.
					else {
						if (((String) map.get("a10" + i)).length() > 0) {
							sql =
								"INSERT INTO "
									+ TcDBContext.getSchemaName()
									+ "TCOMM ("
									+ "FK_ADDRESS, FK_COMMTYPE, VALUE) "
									+ "VALUES ("
									+ adrId
									+ ", 10"
									+ i
									+ ", '"
									+ (String) map.get("a10" + i)
									+ "')";

							doSql(sql);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::updateAddress: Verbindung konnte nicht erstellt werden.",
				e);
		};

	}

	/**
	 * Diese Methode aktualisiert das Abo Datum
	 * Anhand der �bergebenen Adress-ID
	 */
	public void updateSubscriptor(int adrId) {
		try {

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date currentDate = new Date();
			String datum = format.format(currentDate);
			
			String sql =
				"UPDATE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESS SET SUBSCRIPTIONDATE = '"
					+ datum
					+ "' "
					+ "WHERE PK_ADDRESS = '"
					+ adrId
					+ "'";

			logger.fine("SQL[0]: " + sql);
			doSql(sql);

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::updateSubscriptor: Verbindung konnte nicht erstellt werden.",
				e);
		};
	}

	/**
	 * Diese Methode setzt eine Bemerkung 
	 */
	public void setBemerkung(
		int adrId,
		String grp,
		String bem,
		String stichwort) {
		try {

			String sql =
				"UPDATE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER SET NOTE = '"
					+ bem
					+ "', "
					+ "NOTE2 = '"
					+ stichwort
					+ "' "
					+ "WHERE FK_ADDRESS = '"
					+ adrId
					+ "' AND "
					+ "FK_FOLDER = '"
					+ grp
					+ "'";
			doSql(sql);

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::setBemerkung: Verbindung konnte nicht erstellt werden.",
				e);
		};
	}

	/**
	 * Diese Methode aktualisiert einen Verteiler 
	 */
	public void setVerteiler(Map map) {
		try {
			int adrId = Integer.parseInt(map.get("adrId").toString());
			int size = Integer.parseInt(map.get("size").toString());
			String grp = (String) map.get("verteilergruppe");

			String sql = null;

			for (int i = 0; i < size; i++) {
				if ((String) map.get("delete" + i) != null) {

					sql =
						"DELETE FROM "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSSUBFOLDER WHERE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSSUBFOLDER.FK_ADDRESS = '"
							+ adrId
							+ "' AND "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
							+ grp
							+ "' AND "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSSUBFOLDER.FK_SUBFOLDER = '"
							+ Integer.parseInt((String) map.get("delete" + i))
							+ "'";

					doSql(sql);
				}

				int index = 0;
				if (map.containsKey("insert" + i)) {
					sql =
						"INSERT INTO "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSSUBFOLDER (fk_address, fk_subfolder, fk_folder) VALUES  "
							+ "("
							+ adrId
							+ ", "
							+ Integer.parseInt((String) map.get("insert" + i))
							+ ", '"
							+ grp
							+ "')";
					doSql(sql);
					//LDAP: insert

					if (index == 0) {
						sql =
							"DELETE FROM "
								+ TcDBContext.getSchemaName()
								+ "TADDRESSSUBFOLDER WHERE "
								+ TcDBContext.getSchemaName()
								+ "TADDRESSSUBFOLDER.FK_SUBFOLDER = 2000 AND "
								+ TcDBContext.getSchemaName()
								+ "TADDRESSSUBFOLDER.FK_ADDRESS = '"
								+ adrId
								+ "'";

						doSql(sql);

						sql =
							"DELETE FROM "
								+ TcDBContext.getSchemaName()
								+ "TADDRESSFOLDER WHERE "
								+ TcDBContext.getSchemaName()
								+ "TADDRESSFOLDER.FK_FOLDER = 1 AND "
								+ TcDBContext.getSchemaName()
								+ "TADDRESSFOLDER.FK_ADDRESS = '"
								+ adrId
								+ "'";

						doSql(sql);

					}
					index++;
				}
			}

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::setVerteiler: Verbindung konnte nicht erstellt werden.",
				e);
		}

	}

	/**
	 * Diese Methode l�scht einen Adressdatensatz 
	 */
	public Map deleteAddress(int adrId, String grp) {
		Map map = new TreeMap();
		try {
			String sql =
				"DELETE FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSFOLDER "
					+ "WHERE FK_ADDRESS = '"
					+ adrId
					+ "' AND "
					+ "FK_FOLDER = '"
					+ grp
					+ "'";
			doSql(sql);

			sql =
				"DELETE FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER "
					+ "WHERE FK_FOLDER = '"
					+ grp
					+ "' AND "
					+ "FK_ADDRESS = '"
					+ adrId
					+ "'";
			doSql(sql);

			map.put("ok", "ok");
			return map;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::deleteAddress: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}
	}

	/**
	 * Die Methode liefert alle zur Zeit vorhandenen Verteilergruppen
	 */
	public Map getAllVerteilergruppen() {
		Map gruppen = new TreeMap();
		try {

			String sql =
				"SELECT "	
					+ TcDBContext.getSchemaName()
					+ "TFOLDER.PK_FOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TFOLDER.DESCRIPTION, "
					+ TcDBContext.getSchemaName()
					+ "TFOLDER.FOLDERNAME FROM "
					+ TcDBContext.getSchemaName()
					+ "TFOLDER ";
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				// key = PK_FOLDER, val = NAME    
				gruppen.put(rs.getString(1), rs.getObject(3) + "#" + rs.getString(2));
				i++;
			}
			rs.close();
			return gruppen;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getAllVerteilergruppen: Verbindung konnte nicht erstellt werden.",
				e);
			gruppen.put("fehler", e.toString());
			return gruppen;
		}
	}

	/**
	 * Die Methode liefert alle zur Zeit vorhandenen User
	 */
	public Map getUsers() {
		Map user = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TUSER.LOGINNAME, "
					+ TcDBContext.getSchemaName()
					+ "TUSER.FIRSTNAME, "
					+ TcDBContext.getSchemaName()
					+ "TUSER.LASTNAME FROM "
					+ TcDBContext.getSchemaName()
					+ "TUSER ";
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				// key = LOGINNAME, val = NAME    
				user.put(
					rs.getString(1),
					rs.getObject(2) + " " + rs.getObject(3));
				i++;
			}
			rs.close();

			return user;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getUser: Verbindung konnte nicht erstellt werden.",
				e);
			user.put("fehler", e.toString());
			return user;
		}
	}

	/**
	* Die Methode pr�ft zu �bergebener AddressId, ob sie
	* in den L�schverteiler verschoben werden muss
	* 
	*/
	public void collectOrphan(int adrId) {

		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_FOLDER FROM "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_ADDRESS = '"
					+ adrId
					+ "' "
					+ "ORDER BY "
					+ TcDBContext.getSchemaName()
					+ "TADDRESSSUBFOLDER.FK_FOLDER ASC ";
			ResultSet rs = getResultSet(sql);

			int folder = 0;
			if (rs.next())
				folder = rs.getInt(1);

			if (!rs.next() && folder == 0) {
				sql =
					"DELETE FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER WHERE "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS = '"
						+ adrId
						+ "'";
				doSql(sql);

				sql =
					"INSERT INTO "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER (fk_address, fk_subfolder, fk_folder) VALUES "
						+ "("
						+ adrId
						+ ", 2000, 1)";
				doSql(sql);

				sql =
					"DELETE FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSFOLDER WHERE "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSFOLDER.FK_ADDRESS = '"
						+ adrId
						+ "'";
				doSql(sql);

				sql =
					"INSERT INTO "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSFOLDER (fk_address, fk_folder) VALUES "
						+ "("
						+ adrId
						+ ", 1)";
				doSql(sql);

			}
			rs.close();
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::collectOrphan: Verbindung konnte nicht erstellt werden.",
				e);
		}
	}

	/**
	 * 	Diese Methode liefert die erweiterten Felder
	 */
	public Map getExtendedFields() {
		Map result = new TreeMap();
		return result;
	}

	
	/**
	 * Diese Methode gibt die LKZ zu einem �bergebenen Land zur�ck 
	 */
	public Map getLKZforLand(String land) {
		TreeMap map = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRYSHORT FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRY = '"
					+ land
					+ "'";
			ResultSet rs = getResultSet(sql);
			int i = 0;
			while (rs.next()) {
				map.put("lkz", rs.getString(1));
				i++;
			}
			rs.close();

			return map;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getLKZforLand: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}
	}

	/**
	 * Diese Methode gibt das Land zu einer �bergebenen LKZ zur�ck 
	 */
	public Map getLandforLKZ(String lkz) {
		TreeMap map = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRY FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRYSHORT = '"
					+ lkz
					+ "'";
			ResultSet rs = getResultSet(sql);
			int i = 0;
			while (rs.next()) {
				map.put("land", rs.getString(1));
				i++;
			}
			rs.close();

			return map;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getLandforLKZ: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}
	}

	/**
	 * Diese Methode liefert eine Map aller 
	 * vorhandenen Bundeslaender
	 */
	public Map getBundeslaender() {
		SortedMap country = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TREGION.PK_REGION, "
					+ TcDBContext.getSchemaName()
					+ "TREGION.REGION FROM "
					+ TcDBContext.getSchemaName()
					+ "TREGION ";
			ResultSet rs = getResultSet(sql);

			int i = 0;
			String val = "";
			while (rs.next()) {
				// key = REGION, val = REGIONSHORT
				val = rs.getString(2);
				if (val == null)
					val = "";
				country.put(rs.getString(1), val);
				i++;
			}
			rs.close();
			return country;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getBundeslaender: Verbindung konnte nicht erstellt werden.",
				e);
			country.put("fehler", e.toString());
			return country;
		}
	}

	/**
	 * Diese Methode liefert eine Map aller 
	 * vorhandenen Anreden
	 */
	public Map getAnreden() {
		Map anreden = new TreeMap();
		try {
			String sql =
				"SELECT * FROM "
					+ TcDBContext.getSchemaName()
					+ "TSALUTATION "
					+ "ORDER BY "
					+ TcDBContext.getSchemaName()
					+ "TSALUTATION.SALUTATION ASC";

			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				// key = PK_SALUTATION, val = SALUTATION    
				anreden.put(rs.getString(1), rs.getObject(2));
				i++;
			}
			rs.close();

			return anreden;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getAnreden: Verbindung konnte nicht erstellt werden.",
				e);
			anreden.put("fehler", e.toString());
			return anreden;
		}
	}

	/**
	 * Diese Methode liefert eine Map aller vorhandenen
	 * Landeskennzahlen
	 */
	public Map getLKZ() {
		Map lkz = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.PK_COUNTRY, "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRYSHORT FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY "
					+ "ORDER BY "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRYSHORT ASC";

			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				// key = PK_COUNTRY, val = COUNTRYLKZ    
				lkz.put(rs.getString(1), rs.getObject(2));
				i++;
			}
			rs.close();

			return lkz;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getLKZ: Verbindung konnte nicht erstellt werden.",
				e);
			lkz.put("fehler", e.toString());
			return lkz;
		}
	}

	/**
	 * Diese Methode liefert eine Map aller vorhandenen
	 * Laender
	 */
	public Map getLaender() {
		Map laender = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.PK_COUNTRY, "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRY FROM "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY "
					+ "ORDER BY "
					+ TcDBContext.getSchemaName()
					+ "TCOUNTRY.COUNTRY ASC";

			ResultSet rs = getResultSet(sql);

			int i = 0;
			String val = "";
			while (rs.next()) {
				val = rs.getString(2);
				if (val == null)
					val = "";
				// key = PK_COUNTRY, val = COUNTRY  
				laender.put(rs.getString(1), val);
				i++;
			}
			rs.close();

			return laender;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getLaender: Verbindung konnte nicht erstellt werden.",
				e);
			laender.put("fehler", e.toString());
			return laender;
		}
	}

	/**
	 * Diese Methode liefert die Standardverteilergruppe 
	 * des derzeitigen Users
	 */
	public Map getStandardVerteiler(String userId) {
		Map verteiler = new TreeMap();
		try {

			String sql = Queries.getStandardCategoryQuery(userId);
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				verteiler.put("verteiler", rs.getString(1));
				i++;
			}
			rs.close();

			return verteiler;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getStandardVerteiler: Verbindung konnte nicht erstellt werden.",
				e);
			verteiler.put("fehler", e.toString());
			return verteiler;
		}
	}

	/**
	 * Diese Methode setzt die StandardVerteilergruppe
	 * zum eingeloggten Benutzer
	 * 
	 * @deprecated Da Mimik nicht mehr drin, nicht mehr benutzen!
	 */
	public Map setStandardVerteilergruppe(String gruppe, String UserID) {
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TUSERFOLDER.PK_USERFOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TUSER.PK_USER FROM "
					+ TcDBContext.getSchemaName()
					+ "TUSER, "
					+ TcDBContext.getSchemaName()
					+ "TUSERFOLDER, "
					+ TcDBContext.getSchemaName()
					+ "TFOLDER "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TUSER.LOGINNAME = '"
					+ UserID
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TUSERFOLDER.FK_USER = "
					+ TcDBContext.getSchemaName()
					+ "TUSER.PK_USER "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TUSERFOLDER.FK_FOLDER = '"
					+ Integer.parseInt(gruppe)
					+ "' ";
			ResultSet rs = getResultSet(sql);
			int id = 0, userid = 0;
			if (rs.next()) {
				id = rs.getInt(1);
				userid = rs.getInt(2);
			}
			rs.close();

			sql =
				"UPDATE "
					+ TcDBContext.getSchemaName()
					+ "TUSERFOLDER SET ISDEFAULT = 1 WHERE PK_USERFOLDER = '"
					+ id
					+ "' AND FK_USER = '"
					+ userid
					+ "'";
			doSql(sql);
			sql =
				"UPDATE "
					+ TcDBContext.getSchemaName()
					+ "TUSERFOLDER SET ISDEFAULT = 0 WHERE NOT PK_USERFOLDER = '"
					+ id
					+ "' AND FK_USER = '"
					+ userid
					+ "'";
		
			doSql(sql);
			return new TreeMap();
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::setStandardVerteilergruppe: Verbindung konnte nicht erstellt werden.",
				e);
			Map sm = new TreeMap();
			sm.put("fehler", e.toString());
			return sm;
		}
	}

	/**
	 * Diese Methode liefert die Unterkategorien einer Kategorie
	 */
	public Map getVerteiler(String userId, String grp) {
		Map verteiler = new TreeMap();
		try {
			String sql = "";

			sql = Queries.getVerteilerQuery(userId, grp);
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				// key = PK_SUBFOLDER, val = FOLDERNAME
				verteiler.put(rs.getString(1), rs.getObject(2));
				i++;
			}
			rs.close();
			return verteiler;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getVerteiler: Verbindung konnte nicht erstellt werden.",
				e);
			verteiler.put("fehler", e.toString());
			return verteiler;
		}
	}

	/**
	 * Diese Methode liefert die Unterkategorien einer Kategorie mit Beschreibung
	 */
	public Map getVerteilerExt(String userId, String grp) {
		Map verteiler = new TreeMap();
		try {
			String sql = "";

			sql = Queries.getVerteilerExtQuery(userId, grp);
			ResultSet rs = getResultSet(sql);

			int i = 0;
			while (rs.next()) {
				List payload = new Vector();
				// 1. Element FOLDERNAME, 2. Element DESCRIPTION
				payload.add(rs.getObject(2));
				payload.add(rs.getObject(3));
				// key = PK_SUBFOLDER, val = payload
				verteiler.put(rs.getString(1), payload);
				i++;
			}
			rs.close();
			return verteiler;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getVerteilerExt: Verbindung konnte nicht erstellt werden.",
				e);
			verteiler.put("fehler", e.toString());
			return verteiler;
		}
	}

	/**
	 * Diese Methode erzeugt eine Verteilergruppe 
	 */
	public Map createVerteilergruppe(String key, String name, String description) {

		Map map = new TreeMap();
		try {
			if ("null".equals(key) || key == null || "".equals(key))
				key = "0";

			String exists =
			    "SELECT count(*) as count FROM " 
			    + TcDBContext.getSchemaName()
			    + "TFOLDER WHERE TFOLDER.FOLDERNAME = '"
			    + name
			    + "'";
			
			logger.log(Level.FINER, exists);
			
			ResultSet rs = getResultSet(exists);
			
			if (rs.next()) {
			    if (rs.getInt("count") > 0) {
			        map.put("fehler", "Eine Kategorie mit dem Namen " + name + " ist im System bereits vorhanden.");
			        rs.close();
			        return map;
			    }
			}
			
			rs.close();
			
			String sql =
				"INSERT INTO "
					+ TcDBContext.getSchemaName()
					+ "TFOLDER (FOLDERNAME, DESCRIPTION) "
					+ "VALUES ('"	
					+ name
					+ "','"
					+ description
					+ "')";
			InsertKeys keys = DB.insertKeys(TcDBContext.getDefaultContext(), sql);

			int id = keys.getPk();		

			map.put("cathegoryId", new Integer(id));

			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::createVerteilergruppe: Verbindung konnte nicht erstellt werden.",
				e);
			map.put(
				"fehler",
				"Bitte andere Kurzbezeichnung vergeben, da diese in der Datenbank bereits vorhanden ist.");
			return map;
		}
	}

	/**
	 * L�scht Benutzer.
	 * 
	 * @param userId die Benutzerkennung.
	 */
	public Map deleteUser(TcConfig tcConfig, String userId) {
		Map map = new TreeMap();
		try {
			String sql =
				"SELECT PK_USER FROM "
					+ TcDBContext.getSchemaName()
					+ "TUSER WHERE LOGINNAME = '"
					+ userId
					+ "'";

			ResultSet rs = getResultSet(sql);

			if (rs.next())
				rs.getInt(1);
			rs.close();

			sql =
				"DELETE FROM "
					+ TcDBContext.getSchemaName()
					+ "TUSER WHERE LOGINNAME = '"
					+ userId
					+ "'";
			doSql(sql);

			/* Jetzt in der DB...
			sql = "DELETE FROM " + TcDBContext.getSchemaName() + "tgroup_user where fk_user = " + id;
			doSql(sql);
			*/
			//LDAP
			
			//TODO: Auf neues LDAP umstellen
			return map;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::deleteUser: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}
	}

	/**
	 * Gibt das Passwort zu einem Benutzer zur�ck.
	 * 
	 * @param userId die Benutzerkennung.
	 * 
	 */
	public Map getUserPwd(String userId) {
		Map map = new TreeMap();
		try {
			String sql =
				"SELECT PWD FROM "
					+ TcDBContext.getSchemaName()
					+ "TUSER WHERE LOGINNAME = '"
					+ userId
					+ "'";
			ResultSet rs = getResultSet(sql);
			if (rs.next()) {
				map.put("passwort", rs.getString(1));
				rs.close();
				return map;
			}
			map.put("false", null);
			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getUserPwd: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}
	}


	
	/**
	  * 
	  */
	public Map getBundeslandForPLZ(String plz) {
		Map result = new TreeMap();
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TREGION.REGION FROM "
					+ TcDBContext.getSchemaName()
					+ "TREGION, "
					+ TcDBContext.getSchemaName()
					+ "TREGIONZIPCODE "
					+ "WHERE "
					+ TcDBContext.getSchemaName()
					+ "TREGIONZIPCODE.ZIPCODEFROM <= '"
					+ plz
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TREGIONZIPCODE.ZIPCODETO >= '"
					+ plz
					+ "' "
					+ "AND "
					+ TcDBContext.getSchemaName()
					+ "TREGION.PK_REGION = "
					+ TcDBContext.getSchemaName()
					+ "TREGIONZIPCODE.FK_REGION";

			ResultSet rs = getResultSet(sql);

			if (rs.next())
				result.put("bundesland", rs.getString(1));
			rs.close();

			return result;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getBundeslandForPLZ: Verbindung konnte nicht erstellt werden.",
				e);
			result.put("fehler", e.toString());
			return result;
		}
	}

	/**
	 * Diese Methode sucht nach Addressen zu �bergebener Map
	 * 
	 * WICHTIG: Bei AEnderungen in dieser Methode,
	 *          diese bitte auch an "searchAddress" / "searchAddressVector" vornehmen!!
	 */
	public Map searchAddress(Map map) {
		//TODO: Wenn Postgres wieder lower() oder upper() mit UNICODE kann, sollte hier wieder searchAddress_old(map) aufgerufen werden
		//return searchAddress_old(map);
		return searchAddress_fixed(map);
	}
	
	/**
	 * @param map
	 * @return
	 * 
	 * WICHTIG: Bei AEnderungen in dieser Methode,
	 *          diese bitte auch an "searchAddress" / "searchAddressVector" vornehmen!!
	 */
	private Map searchAddress_old(Map map) {
		Map result = new HashMap();
		try {
			boolean matchcode = false;
			if (map.containsKey("matchcodeStr")) {
				if ((String) map.get("matchcodeStr") == "true") {
					matchcode = true;
				}
			} else {
				matchcode = ((Boolean) map.get("matchcode")).booleanValue();
			}

			String last,
				first,
				city,
				street,
				ext = "",
				field = TcDBContext.getSchemaName() + "TADDRESS.PK_ADDRESS",
				nachname,
				vorname,
				strasse,
				ort,
				ext_and = "";

			last = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.LASTNAME)";
			first = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.FIRSTNAME)";
			street = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.STREET)";
			city = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.CITY)";

			nachname = ((String) map.get("nachname")).toLowerCase();
			vorname = ((String) map.get("vorname")).toLowerCase();
			strasse = ((String) map.get("strasse")).toLowerCase();
			ort = ((String) map.get("ort")).toLowerCase();

			if (matchcode
				&& (nachname.length() > 0
					|| vorname.length() > 0
					|| strasse.length() > 0
					|| ort.length() > 0)) {
				ext = ", " + TcDBContext.getSchemaName() + "TADDRESSEXT";
				field = TcDBContext.getSchemaName() + "TADDRESSEXT.PK_ADDRESSEXT";
				last = TcDBContext.getSchemaName() + "TADDRESSEXT.LASTNAME_M";
				first = TcDBContext.getSchemaName() + "TADDRESSEXT.FIRSTNAME_M";
				street = TcDBContext.getSchemaName() + "TADDRESSEXT.STREET_M";
				city = TcDBContext.getSchemaName() + "TADDRESSEXT.CITY_M";
				ext_and =
					TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT AND ";
			}

			StringBuffer buffer = new StringBuffer();
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.SALUTATION)",
				((String) map.get("herrnFrau")).toLowerCase(),
				false);
			appendCondition(buffer, last, nachname, matchcode);
			appendCondition(buffer, first, vorname, matchcode);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.TITELJOB)",
				((String) map.get("titel")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.TITELACADEMIC)",
				((String) map.get("akad_titel")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.NAMESUFFIX)",
				((String) map.get("namenszusatz")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.ORGANISATION)",
				((String) map.get("institution")).toLowerCase(),
				false);
			appendCondition(buffer, street, strasse, matchcode);
			appendPLZCondition(buffer, (String) map.get("plz"));
			appendCondition(buffer, city, ort.toLowerCase(), matchcode);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.REGION)",
				((String) map.get("bundesland")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.COUNTRYSHORT)",
				((String) map.get("lkz")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				"lower(" + TcDBContext.getSchemaName() + "TADDRESS.COUNTRY)",
				((String) map.get("land")).toLowerCase(),
				false);

			boolean comflag = false;
			String telefonP = (String) map.get("tel_p");
			String telefonD = (String) map.get("tel_d");
			String faxP = (String) map.get("fax_p");
			String faxD = (String) map.get("fax_d");
			String email = ((String) map.get("email")).toLowerCase();

			if (telefonP != null && telefonP.length() > 0) {
				appendCondition(buffer, "101", telefonP, false);
				comflag = true;
			}
			if (telefonD != null && telefonD.length() > 0) {
				appendCondition(buffer, "102", telefonD, false);
				comflag = true;
			}
			if (faxP != null && faxP.length() > 0) {
				appendCondition(buffer, "103", faxP, false);
				comflag = true;
			}
			if (faxD != null && faxD.length() > 0) {
				appendCondition(buffer, "104", faxD, false);
				comflag = true;
			}
			if (email != null && email.length() > 0) {
				appendCondition(buffer, "108", email.toLowerCase(), false);
				comflag = true;
			}

			String sql = "";
			String kategorie = (String) map.get("vertgrp");
			if (comflag) {
				sql =
					"SELECT DISTINCT "
						+ field
						+ ", "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS,  "
						+ TcDBContext.getSchemaName()
						+ "TCOMM"
						+ ext
						+ " WHERE "
						+ buffer.toString()
						+ " AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
						+ kategorie
						+ "' AND "
						+ ext_and
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TCOMM.FK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M ASC";
			} else {
				sql =
					"SELECT DISTINCT "
						+ field
						+ ", "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS"
						+ ext
						+ " WHERE "
						+ buffer.toString()
						+ " AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
						+ kategorie
						+ "' AND "
						+ ext_and
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M ASC";
			}

			/*
			 SQL �ber alle dem User zugeordneten Kategorien
			 --> wird ev. spaeter mal ben�tigt
			String sql="SELECT DISTINCT "+field+" FROM TUSER, TUSERFOLDER, TADDRESSFOLDER, TADDRESS "+ext+" "+
					   "WHERE "+buffer.toString()+" AND "+
					   "TUSER.LOGINNAME = '"+(String) map.get("user")+"' AND "+
					   "TUSERFOLDER.FK_USER = TUSER.PK_USER AND "+
					   "TADDRESSFOLDER.FK_FOLDER = TUSERFOLDER.FK_FOLDER AND "+ext_and+
					   "TADDRESS.PK_ADDRESS = TADDRESSFOLDER.FK_ADDRESS";
					   
			*/
			ResultSet rs = getResultSet(sql);

			int index = 0;
			while (rs.next()) {
				result.put("id" + index, new Integer(rs.getInt(1)));
				index++;
			}
			rs.close();

			return result;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::searchAddress: Verbindung konnte nicht erstellt werden.",
				e);
			result.put("fehler", e.toString());
			return result;
		}
	}

	/**
	 * Nur dazu da, um Postgres-Bug zu umgehen...
	 * @param map
	 * @return map
	 * 
	 * WICHTIG: Bei AEnderungen in dieser Methode,
	 *          diese bitte auch an "searchAddress" / "searchAddressVector" vornehmen!!
	 */
	private Map searchAddress_fixed(Map map) {
		Map result = new HashMap();
		try {
			boolean matchcode = false;
			if (map.containsKey("matchcodeStr")) {
				if ((String) map.get("matchcodeStr") == "true") {
					matchcode = true;
				}
			} else {
				matchcode = ((Boolean) map.get("matchcode")).booleanValue();
			}

			String last,
				first,
				city,
				street,
				ext = "",
				field = TcDBContext.getSchemaName() + "TADDRESS.PK_ADDRESS",
				nachname,
				vorname,
				strasse,
				ort,
				ext_and = "";

			last = TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.LASTNAME)";
			first = TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.FIRSTNAME)";
			street = TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.STREET)";
			city = TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.CITY)";

			nachname = ((String) map.get("nachname")).toLowerCase();
			vorname = ((String) map.get("vorname")).toLowerCase();
			strasse = ((String) map.get("strasse")).toLowerCase();
			ort = ((String) map.get("ort")).toLowerCase();

			if (matchcode
				&& (nachname.length() > 0
					|| vorname.length() > 0
					|| strasse.length() > 0
					|| ort.length() > 0)) {
				ext = ", " + TcDBContext.getSchemaName() + "TADDRESSEXT";
				field = TcDBContext.getSchemaName() + "TADDRESSEXT.PK_ADDRESSEXT";
				last = TcDBContext.getSchemaName() + "TADDRESSEXT.LASTNAME_M";
				first = TcDBContext.getSchemaName() + "TADDRESSEXT.FIRSTNAME_M";
				street = TcDBContext.getSchemaName() + "TADDRESSEXT.STREET_M";
				city = TcDBContext.getSchemaName() + "TADDRESSEXT.CITY_M";
				ext_and =
					TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT AND ";
			}

			StringBuffer buffer = new StringBuffer();
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.SALUTATION)",
				((String) map.get("herrnFrau")).toLowerCase(),
				false);
			appendCondition(buffer, last, nachname, matchcode);
			appendCondition(buffer, first, vorname, matchcode);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.TITELJOB)",
				((String) map.get("titel")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.TITELACADEMIC)",
				((String) map.get("akad_titel")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.NAMESUFFIX)",
				((String) map.get("namenszusatz")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.ORGANISATION)",
				((String) map.get("institution")).toLowerCase(),
				false);
			appendCondition(buffer, street, strasse, matchcode);
			appendPLZCondition(buffer, (String) map.get("plz"));
			appendCondition(buffer, city, ort.toLowerCase(), matchcode);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.REGION)",
				((String) map.get("bundesland")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.COUNTRYSHORT)",
				((String) map.get("lkz")).toLowerCase(),
				false);
			appendCondition(
				buffer,
				TcDBContext.getSchemaName()+"lower_fix(" + TcDBContext.getSchemaName() + "TADDRESS.COUNTRY)",
				((String) map.get("land")).toLowerCase(),
				false);

			boolean comflag = false;
			String telefonP = (String) map.get("tel_p");
			String telefonD = (String) map.get("tel_d");
			String faxP = (String) map.get("fax_p");
			String faxD = (String) map.get("fax_d");
			String email = ((String) map.get("email")).toLowerCase();

			if (telefonP != null && telefonP.length() > 0) {
//				appendCondition(buffer, "101", telefonP, false);
				comflag = true;
			}
			if (telefonD != null && telefonD.length() > 0) {
//				appendCondition(buffer, "102", telefonD, false);
				comflag = true;
			}
			if (faxP != null && faxP.length() > 0) {
//				appendCondition(buffer, "103", faxP, false);
				comflag = true;
			}
			if (faxD != null && faxD.length() > 0) {
//				appendCondition(buffer, "104", faxD, false);
				comflag = true;
			}
			if (email != null && email.length() > 0) {
//				appendCondition(buffer, "108", email.toLowerCase(), false);
				comflag = true;
			}

			String sql = "";
			String kategorie = (String) map.get("vertgrp");
			if (comflag) {
				sql =
					"SELECT DISTINCT "
						+ field
						+ ", "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS,  "
						+ TcDBContext.getSchemaName()
						+ "TCOMM"
						+ ext
						+ " WHERE "
						+ buffer.toString()
						+ " AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
						+ kategorie
						+ "' AND "
						+ ext_and
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TCOMM.FK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M ASC";
			} else {
				sql =
					"SELECT DISTINCT "
						+ field
						+ ", "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS"
						+ ext
						+ " WHERE "
						+ buffer.toString()
						+ " AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
						+ kategorie
						+ "' AND "
						+ ext_and
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M ASC";
			}

			/*
			 SQL �ber alle dem User zugeordneten Kategorien
			 --> wird ev. spaeter mal ben�tigt
			String sql="SELECT DISTINCT "+field+" FROM TUSER, TUSERFOLDER, TADDRESSFOLDER, TADDRESS "+ext+" "+
					   "WHERE "+buffer.toString()+" AND "+
					   "TUSER.LOGINNAME = '"+(String) map.get("user")+"' AND "+
					   "TUSERFOLDER.FK_USER = TUSER.PK_USER AND "+
					   "TADDRESSFOLDER.FK_FOLDER = TUSERFOLDER.FK_FOLDER AND "+ext_and+
					   "TADDRESS.PK_ADDRESS = TADDRESSFOLDER.FK_ADDRESS";
					   
			*/

			AddressSelect as = new AddressSelect(map);
			Select newSQL = as.getSelect(comflag);
					
			if (buffer.toString().length() > 0)
			    newSQL.where(new RawClause(buffer));
			
			ResultSet rs = getResultSet(newSQL.toString());

			int index = 0;
			while (rs.next()) {
				result.put("id" + index, new Integer(rs.getInt(1)));
				index++;
			}
			rs.close();

			
			return result;

		} catch (Exception e) {
		    Lg.DBASE((String)map.get("module")).error("Fehler bei der Suche.", e);
		    logger.log(
				Level.SEVERE,
				"DataAccess::searchAddress: Verbindung konnte nicht erstellt werden.",
				e);
			result.put("fehler", e.toString());
			return result;
		}
	}

	/**
	 * Diese Methode sucht nach Addressen und liefert sie als Liste zur�ck.
	 * 
	 * WICHTIG: Bei AEnderungen in dieser Methode,
	 *          diese bitte auch an "searchAddress" / "searchAddressVector" vornehmen!!
	 */
	public Map searchAddressesVector(
		String kategorie,
		boolean matchcode,
		int addressCount,
		int packageIndex,
		String nachname,
		String vorname,
		String strasse,
		String ort,
		String herrnFrau,
		String titel,
		String akadTitel,
		String namenszusatz,
		String institution,
		String plz,
		String bundesland,
		String lkz,
		String land,
		String telefonP,
		String telefonD,
		String faxP,
		String faxD,
		String email) {
		Map result = new TreeMap();
		try {

			String last,
				first,
				city,
				street,
				ext = "",
				field = TcDBContext.getSchemaName() + "TADDRESS.PK_ADDRESS",
				ext_and = "";

			last = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.LASTNAME)";
			first = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.FIRSTNAME)";
			street = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.STREET)";
			city = "lower(" + TcDBContext.getSchemaName() + "TADDRESS.CITY)";

			nachname = (nachname != null) ? nachname.toLowerCase() : "";
			vorname = (vorname != null) ? vorname.toLowerCase() : "";
			strasse = (strasse != null) ? strasse.toLowerCase() : "";
			ort = (ort != null) ? ort.toLowerCase() : "";
			herrnFrau = (herrnFrau != null) ? herrnFrau.toLowerCase() : "";
			titel = (titel != null) ? titel.toLowerCase() : "";
			akadTitel = (akadTitel != null) ? akadTitel.toLowerCase() : "";
			namenszusatz =
				(namenszusatz != null) ? namenszusatz.toLowerCase() : "";
			institution =
				(institution != null) ? institution.toLowerCase() : "";
			plz = (plz != null) ? plz : "";
			bundesland = (bundesland != null) ? bundesland.toLowerCase() : "";
			lkz = (lkz != null) ? lkz.toLowerCase() : "";
			land = (land != null) ? land.toLowerCase() : "";
			telefonP = (telefonP != null) ? telefonP : "";
			telefonD = (telefonD != null) ? telefonD : "";
			faxP = (faxP != null) ? faxP : "";
			faxD = (faxD != null) ? faxD : "";
			email = (email != null) ? email.toLowerCase() : "";

			if (matchcode
				&& (nachname.length() > 0
					|| vorname.length() > 0
					|| strasse.length() > 0
					|| ort.length() > 0)) {
				ext = ", " + TcDBContext.getSchemaName() + "TADDRESSEXT";
				field = TcDBContext.getSchemaName() + "TADDRESSEXT.PK_ADDRESSEXT";
				last = TcDBContext.getSchemaName() + "TADDRESSEXT.LASTNAME_M";
				first = TcDBContext.getSchemaName() + "TADDRESSEXT.FIRSTNAME_M";
				street = TcDBContext.getSchemaName() + "TADDRESSEXT.STREET_M";
				city = TcDBContext.getSchemaName() + "TADDRESSEXT.CITY_M";
				ext_and =
					TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT AND ";
			}

			StringBuffer buffer = new StringBuffer();
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.SALUTATION)", herrnFrau.toUpperCase(), false);
			appendCondition(buffer, last, nachname.toLowerCase(), matchcode);
			appendCondition(buffer, first, vorname.toLowerCase(), matchcode);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.TITELJOB)", titel.toLowerCase(), false);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.TITELACADEMIC)", akadTitel.toLowerCase(), false);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.NAMESUFFIX)", namenszusatz.toLowerCase(), false);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.ORGANISATION)", institution.toLowerCase(), false);
			appendCondition(buffer, street, strasse.toLowerCase(), matchcode);
			appendPLZCondition(buffer, plz);
			appendCondition(buffer, city, ort.toLowerCase(), matchcode);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.REGION)", bundesland.toLowerCase(), false);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.COUNTRYSHORT)", lkz.toLowerCase(), false);
			appendCondition(buffer, "lower(" + TcDBContext.getSchemaName() + "TADDRESS.COUNTRY)", land.toLowerCase(), false);
			boolean comflag = false;
			if (telefonP != null && telefonP.length() > 0) {
				appendCondition(buffer, "101", telefonP, false);
				comflag = true;
			}
			if (telefonD != null && telefonD.length() > 0) {
				appendCondition(buffer, "102", telefonD, false);
				comflag = true;
			}
			if (faxP != null && faxP.length() > 0) {
				appendCondition(buffer, "103", faxP, false);
				comflag = true;
			}
			if (faxD != null && faxD.length() > 0) {
				appendCondition(buffer, "104", faxD, false);
				comflag = true;
			}
			if (email != null && email.length() > 0) {
				appendCondition(buffer, "108", email.toLowerCase(), false);
				comflag = true;
			}

			String sql = "";
			if (comflag) {
				sql =
					"SELECT DISTINCT "
						+ field
						+ ", "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS, "
						+ TcDBContext.getSchemaName()
						+ "TCOMM "
						+ ext
						+ " WHERE "
						+ buffer.toString()
						+ " AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
						+ kategorie
						+ "' AND "
						+ ext_and
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TCOMM.FK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M ASC";
				/*
				sql="SELECT DISTINCT "+field+" FROM "+TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER, "+TcDBContext.getSchemaName()+"TADDRESS,  "+TcDBContext.getSchemaName()+"TCOMM "+ext+" "+
				    "WHERE "+buffer.toString()+" AND "+
				    TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER.FK_FOLDER = '"+kategorie+"' AND "+ext_and+
				    TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER.FK_ADDRESS AND "+
				    TcDBContext.getSchemaName()+"TCOMM.FK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS";
				    */
			} else {
				sql =
					"SELECT DISTINCT "
						+ field
						+ ", "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER, "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS"
						+ ext
						+ " WHERE "
						+ buffer.toString()
						+ " AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
						+ kategorie
						+ "' AND "
						+ ext_and
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSSUBFOLDER.FK_ADDRESS AND "
						+ TcDBContext.getSchemaName()
						+ "TADDRESS.PK_ADDRESS = "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSEXT.LASTNAME_M ASC";
				/*
				sql="SELECT DISTINCT "+field+" FROM "+TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER, "+TcDBContext.getSchemaName()+"TADDRESS "+ext+" "+
					"WHERE "+buffer.toString()+" AND "+
					TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER.FK_FOLDER = '"+kategorie+"' AND "+ext_and+
					TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER.FK_ADDRESS";
				   */
			}
			/*
			 SQL �ber alle dem User zugeordneten Kategorien
			 --> wird ev. spaeter mal ben�tigt
			String sql="SELECT DISTINCT "+field+" FROM TUSER, TUSERFOLDER, TADDRESSFOLDER, TADDRESS "+ext+" "+
			           "WHERE "+buffer.toString()+" AND "+
			           "TUSER.LOGINNAME = '"+(String) map.get("user")+"' AND "+
			           "TUSERFOLDER.FK_USER = TUSER.PK_USER AND "+
			           "TADDRESSFOLDER.FK_FOLDER = TUSERFOLDER.FK_FOLDER AND "+ext_and+
			           "TADDRESS.PK_ADDRESS = TADDRESSFOLDER.FK_ADDRESS";
			           
			*/
			logger.log(Level.FINE, "Such-SQL: " + sql);
			ResultSet rs = getResultSet(sql);

			int index = 0;
			int counter = 0;
			Vector addressMap = new Vector();
			while (rs.next()) {
				result.put("id" + index, new Integer(rs.getInt(1)));
				if (index >= packageIndex && counter <= addressCount) {
					Vector uebergebener = new Vector();
					uebergebener.add(new Integer(rs.getInt(1)));
					uebergebener.add(getAddress(rs.getInt(1), kategorie));
					addressMap.add(uebergebener);
					counter++;
				}
				index++;
			}
			rs.close();

			result.put("address", addressMap);
			result.put("addressInfo", createAddressListInfo(index, packageIndex, addressCount));

			return result;

		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::searchAddressVector: Verbindung konnte nicht erstellt werden.",
				e);
			result.put("fehler", e.toString());
			return result;
		}
	}

	/**
	 * Diese Methode erstellt einen neuen Versandauftrag
	 */
	public Map createVersandAuftrag(Map values) {
	    SortedMap map = new TreeMap();
		try {
			int batchID = ((Integer) values.get("batchID")).intValue();
			String grp = (String) values.get("Gruppe");
			int size = ((Integer) values.get("size")).intValue();
			String user = (String) values.get("user");
			String name = new String();
			if (values.containsKey("name")) {
				name = (String) values.get("name");
			}
			int search = 0;
			if (values.containsKey("search"))
				search = ((Integer) values.get("search")).intValue();

			int total = 0;

			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TUSER.PK_USER FROM "
					+ TcDBContext.getSchemaName()
					+ "TUSER WHERE "
					+ TcDBContext.getSchemaName()
					+ "TUSER.LOGINNAME = '"
					+ user
					+ "'";

			ResultSet rs = getResultSet(sql);
			if (rs.next()) {
				
				Insert insert = SQL.Insert(TcDBContext.getDefaultContext())
					.table(TactionDB.getTableName())
					.insert(TactionDB.FKACTIONTYPE, 1)
					.insert(TactionDB.FKACTIONSTATUS, 1)
					.insert(TactionDB.FKUSERINIT, rs.getInt(1))
					.insert(TactionDB.ACTIONTITLE, name)
					.insert(TactionDB.ACTIONDATE, new ParamValue("date"));
		
				// we convert the date with a SimpledateFormat to get a well formed date without second values
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				Date currentDate = new Date();
				String datum = format.format(currentDate);
				Date wellformedDate = format.parse(datum);
				
				ExtPreparedStatement preparedInsert = new ExtPreparedStatement(insert);
				preparedInsert.prepare(TcDBContext.getDefaultContext(), true);
				preparedInsert.setAttribute("date", wellformedDate);
				PreparedStatement ps = preparedInsert.getPreparedStatement();
	            ps.executeUpdate();
				InsertKeys keys = preparedInsert.returnGeneratedKeys(TcDBContext.getDefaultContext());
				
				batchID= keys.getPk();
				
				if (batchID > 0) {
					
					map.put("id", new Integer(batchID));
					if (search > 0) {
					    if (values.containsKey("addressPks")) {
                            Connection connection = null;
                            connection = createJdbcConnection();
                            boolean originalAutoCommit = connection.getAutoCommit();
                            connection.setAutoCommit(false);
                            sql =
                                "INSERT INTO "
                                + TcDBContext.getSchemaName()
                                + "TADDRESSACTION (fk_address, fk_action, fk_addressactionstatus, note) VALUES (?, ?, 0, 'NotAssigned')";                                					            
                            PreparedStatement insertAddressAction = connection.prepareStatement(sql);
				        
                            List addressPks = (List)values.get("addressPks");
                            for (Iterator iter = addressPks.iterator(); iter.hasNext();) {
                                insertAddressAction.setInt(1, ((Integer)iter.next()).intValue());
                                insertAddressAction.setInt(2, batchID);
                                insertAddressAction.execute();
                            }
                        
                            // 				            for (StringTokenizer st = new StringTokenizer((String)values.get("addressPks"), " ");st.hasMoreTokens();) {
                            // 				                insertAddressAction.setInt(1, Integer.parseInt(st.nextToken()));
                            // 				                insertAddressAction.setInt(2, batchID);
                            // 				                insertAddressAction.execute();
                            // 					        }
                            connection.commit();
                            connection.setAutoCommit(originalAutoCommit);
					    }
					    else {
						    for (int k = 0; k < search; k++) {
								sql =
								"INSERT INTO "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSACTION (fk_address, fk_action, fk_addressactionstatus, note) VALUES ('"
									+ ((Integer) values.get("s" + k)).intValue()
									+ "', '"
									+ batchID
									+ "',  0, 'NotAssigned')";
							doSql(sql);
						}
					    }
					} else {
						String in = "";
						if (size > 0) {
							for (int i = 0; i < size; i++) {
								if (i == 0)
									in =
										"('"
											+ (String) values.get("vert" + i)
											+ "'";
								else
									in += ", '"
										+ (String) values.get("vert" + i)
										+ "'";
							}
							in += ")";
						}
						if (!in.equals("")) {
							sql =
								"SELECT DISTINCT "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER.FK_ADDRESS FROM "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER "
									+ "WHERE "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER.FK_SUBFOLDER IN "
									+ in
									+ " AND "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
									+ grp
									+ "'";
						} else {
							sql =
								"SELECT DISTINCT "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER.FK_ADDRESS FROM "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER "
									+ "WHERE "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
									+ grp
									+ "'";
						}
						rs = getResultSet(sql);
						int i = 0;
						while (rs.next()) {
							if (rs.getInt(1) != 0) {
							sql =
								"INSERT INTO "
									+ TcDBContext.getSchemaName()
									+ "TADDRESSACTION (fk_address, fk_action, fk_addressactionstatus, note) VALUES ('"
									+ rs.getInt(1)
									+ "', '"
									+ batchID
									+ "',  0, 'NotAssigned')";
							doSql(sql);
							i++;
							}
						}
						rs.close();
					}

					sql =
						"SELECT DISTINCT count(FK_ADDRESS) AS myCount FROM "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION WHERE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.FK_ACTION = "
							+ batchID;
					rs = getResultSet(sql);
					if (rs.next())
						total = rs.getInt(1);
					rs.close();

					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TACTION SET ADDRESSCOUNT = '"
							+ total
							+ "', ADDRESSCOUNT04 = '"
							+ total
							+ "', ADDRESSCOUNT01 = '0', ADDRESSCOUNT02 = '0', ADDRESSCOUNT03 = '0' WHERE "
							+ "PK_ACTION = '"
							+ batchID
							+ "' ";
					doSql(sql);
				}
				createContactFromMailBatchChannel(user, batchID, "NotAssigned", "Versandauftrag " + batchID + " erstellt", "Diese Adresse ist noch keinem Kanal zugeordnet", CATEGORY_MAILING, CHANNEL_NOT_ASSIGNED);
				sql =
					"SELECT "
						+ TcDBContext.getSchemaName()
						+ "TACTION.PK_ACTION, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ACTIONTITLE, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.FK_USERINIT, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ACTIONDATE, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT02, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT01, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT03, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT04, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT06, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT05,"
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT07, "
						+ TcDBContext.getSchemaName()
						+ "TACTION.ADDRESSCOUNT FROM "
						+ TcDBContext.getSchemaName()
						+ "TACTION "
						+ "WHERE "
						+ TcDBContext.getSchemaName()
						+ "TACTION.PK_ACTION = "
						+ batchID;
				rs = getResultSet(sql);
				ResultSetMetaData rsmd = rs.getMetaData();
				if (rs.next()) {
					String[] val = new String[rsmd.getColumnCount()];
					for (int j = 1; j <= rsmd.getColumnCount(); j++)
						val[j - 1] = rs.getString(j);
					map.put("batch", val);
				}
				DB.close(rs);
				return map;

			}
			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::createVersandAuftrag: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}

	}
	
	/**
	 * Diese Methode holten einen Versandauftrag mit einer bestimmten ID
	 */
	public List getVersandAuftrag(int id) {
	    List resultlist = new Vector();
	    ResultSet rs = null;
        try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TACTION.PK_ACTION, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONTITLE, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.FK_USERINIT, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONDATE, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT02, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT01, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT03, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT04, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT06, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT05,"
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT07, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT FROM "
					+ TcDBContext.getSchemaName()
					+ "TACTION WHERE "
					+ TcDBContext.getSchemaName()
					+ "TACTION.pk_action = "
					+ id
					+ " ORDER BY "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONDATE desc";

			rs = getResultSet(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			for (int j = 1; j <= rsmd.getColumnCount(); j++)
				resultlist.add(rs.getString(j));

        } catch (SQLException e) {
			logger.log(
					Level.SEVERE,
					"DataAccess::getVersandAuftrag: Verbindung konnte nicht erstellt werden.",
					e);
        }finally{
        	DB.close(rs);
        }
	    return resultlist;
	}

	/**
	* Diese Methode holt vorhandene Versandauftraege
	* zum eingeloggten User
	*/
	public Map getVersandAuftraege(String user) {
		SortedMap map = new TreeMap();
		ResultSet rs = null;
		try {
			String sql =
				"SELECT "
					+ TcDBContext.getSchemaName()
					+ "TACTION.PK_ACTION, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONTITLE, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.FK_USERINIT, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONDATE, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT02, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT01, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT03, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT04, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT06, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT05,"
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT07, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT FROM "
					+ TcDBContext.getSchemaName()
					+ "TACTION, "
					+ TcDBContext.getSchemaName()
					+ "TUSER WHERE "
					+ TcDBContext.getSchemaName()
					+ "TUSER.LOGINNAME = '"
					+ user
					+ "' AND "
					+ TcDBContext.getSchemaName()
					+ "TACTION.FK_USERINIT = "
					+ TcDBContext.getSchemaName()
					+ "TUSER.PK_USER ORDER BY "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONDATE desc";

			rs = getResultSet(sql);
			ResultSetMetaData rsmd = rs.getMetaData();

			int i = 0;
			while (rs.next()) {
				List values = new ArrayList();
				for (int j = 1; j <= rsmd.getColumnCount(); j++)
					values.add(rs.getString(j));

				map.put("id" + i, values);
				i++;
			}

			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getVersandAuftraege: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}finally{
			DB.close(rs);
		}
	}

	/**
	  * Diese Methode liefert die aktuelle Octopus-Version
	  */
	public Map getVersion(){
		Map result = new HashMap();
		Map value = getParameter("COLIBRI_SCHEMA_VERSION");
		
		result.put("version", TcDBContext.getSchemaName() + " (" + value.get("value") + ")" 
				+ "\ndatabasename: " + ((de.tarent.dblayer.engine.DBPool)TcDBContext.getDefaultContext().getPool()).getProperty("databaseName")
				+ "\ndatabase IP: " + ((de.tarent.dblayer.engine.DBPool)TcDBContext.getDefaultContext().getPool()).getProperty("serverName"));
		
		
		return result;
	}

	/**
	 * Diese Methode liefert den Ort zu gegebener PLZ
	 */
	public Map getCityForPLZ(String plz) {
		SortedMap map = new TreeMap();
		ResultSet rs = null;
		try {
			String sql =
				"SELECT CITY FROM "
					+ TcDBContext.getSchemaName()
					+ "TZIPCODE WHERE "
					+ "ZIPCODE = '"
					+ plz
					+ "' ";
			rs = getResultSet(sql);
			if (rs.next())
				map.put("city", rs.getString(1));
			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getCityForPLZ: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		} finally{
			DB.close(rs);
		}
	}

	/**
	 * Diese Methode liefert den Parameter zu gegebenem Key
	 */
	public Map getParameter(String paramKey) {
		SortedMap map = new TreeMap();
		ResultSet rs = null;
		try {
			String sql =
				"SELECT PARAMVALUE FROM "
					+ TcDBContext.getSchemaName()
					+ "TPARAM WHERE "
					+ "PARAMNAME = '"
					+ paramKey
					+ "' ";
			rs = getResultSet(sql);
			if (rs.next())
				map.put("value", rs.getString(1));
			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getParameter: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}finally{
			DB.close(rs);
		}
	}

	/**
	 * Diese Methode setzt Parameter
	 */
	public Map setParameter(String paramKey, String paramValue) {
		//FIXME: Hiermit wird immer die FinderService-URL �berschrieben, und
		//sobald mehr als ein Wert in der Tabelle TPARAM vorhanden, so wird immer ein update gemacht
		SortedMap map = new TreeMap();
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM " + TcDBContext.getSchemaName() + "TPARAM";
			rs = getResultSet(sql);

			if (rs.next()) {
				sql =
					"UPDATE "
						+ TcDBContext.getSchemaName()
						+ "TPARAM  SET "
						+ "PARAMVALUE =  '"
						+ paramValue
						+ "'  WHERE "
						+ "PARAMNAME = 'FinderService'";
			} else {
				sql =
					"INSERT INTO "
						+ TcDBContext.getSchemaName()
						+ "TPARAM (0, '"
						+ paramKey
						+ "',  '"
						+ paramValue
						+ "')";
			}
			doSql(sql);
			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::setParameter: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}finally{
			DB.close(rs);
		}
	}

	/**
	 * Diese Methode stellt alle Funktionen der OctopusMailBatch-Klasse
	 * zur Verf�gung
	 */
	public Map mailBatch(Map values) {
		String user = null;
		if (values.containsKey("user")) {
			user = values.get("user").toString();
		}
		SortedMap map = new TreeMap();
		ResultSet rs = null;
		try {

			String function = (String) values.get("function");
			String sql = "";

			if (function == null) {
				function = "";
			}

			if (function.equalsIgnoreCase("getMailbatch")) {
				String id = values.get("id").toString();

				if (id != null && user != null) {
					sql =
						"SELECT "
							+ TcDBContext.getSchemaName()
							+ "TACTION.PK_ACTION, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ACTIONTITLE, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.FK_USERINIT, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ACTIONDATE, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT02, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT01, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT03, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT04, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT06, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT05,"
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT07, "
							+ TcDBContext.getSchemaName()
							+ "TACTION.ADDRESSCOUNT FROM "
							+ TcDBContext.getSchemaName()
							+ "TACTION, "
							+ TcDBContext.getSchemaName()
							+ "TUSER WHERE "
							+ TcDBContext.getSchemaName()
							+ "TUSER.LOGINNAME = '"
							+ user
							+ "' AND "
							+ TcDBContext.getSchemaName()
							+ "TACTION.PK_ACTION = "
							+ id;
					
					
					rs = getResultSet(sql);
					ResultSetMetaData rsmd = rs.getMetaData();
					int i = 0;
					List mailbatch = new ArrayList();
					while (rs.next()) {
						mailbatch.clear();
						for (int j = 1; j <= rsmd.getColumnCount(); j++)
							mailbatch.add(rs.getString(j));
						map.put("id" + new Integer(i).toString(), mailbatch);
						i++;
					}

				}
				return map;
			}
			if (function.equalsIgnoreCase("delete")) {
				logger.fine("delete " + values.get("id"));
				int id = Integer.parseInt(values.get("id").toString());
				createContactFromMailBatchChannel(user, id, "%", "Versandauftrag " + id + " gel?t", null, CATEGORY_MAILING, CHANNEL_NOT_ASSIGNED);
				sql =
					"DELETE FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSACTION WHERE FK_ACTION = "
						+ id;
				doSql(sql);
				sql =
					"DELETE FROM " + TcDBContext.getSchemaName() + "TACTION WHERE PK_ACTION = " + id;
				doSql(sql);
				
			} else if (function.equalsIgnoreCase("getChannelCount")) {
				logger.fine(
					"getChannelCount "
						+ values.get("channel")
						+ " "
						+ values.get("id"));
				String channel = (String) values.get("channel");
				int id = Integer.parseInt(values.get("id").toString());
				sql =
					"SELECT DISTINCT COUNT(FK_ADDRESS) FROM "
						+ TcDBContext.getSchemaName()
						+ "TADDRESSACTION WHERE FK_ACTION = "
						+ id;

				if (!channel.equals(""))
					sql += " AND NOTE = '" + channel + "' ";

				rs = getResultSet(sql);
				if (rs.next()) {
					map.put("id", new Integer(rs.getInt(1)));
				} else
					map.put("id", new Integer(0));
				rs.close();
				return map;
			} else if (function.equalsIgnoreCase("setChannelCount")) {
				logger.fine(
					"setChannelCount "
						+ values.get("channel")
						+ " "
						+ values.get("id"));
				String channel = (String) values.get("channel");
				int id = Integer.parseInt(values.get("id").toString());
				if (channel.equalsIgnoreCase("c_all")) {
					int total = ((Integer) values.get("total")).intValue();
					int mail = ((Integer) values.get("mail")).intValue();
					int fax = ((Integer) values.get("fax")).intValue();
					int post = ((Integer) values.get("post")).intValue();
					int assigned =
						((Integer) values.get("assigned")).intValue();
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TACTION SET ADDRESSCOUNT = "
							+ total
							+ ", ADDRESSCOUNT04 = "
							+ assigned
							+ ", ADDRESSCOUNT01 = "
							+ mail
							+ ", ADDRESSCOUNT02 = "
							+ fax
							+ ", ADDRESSCOUNT03 = "
							+ post
							+ " WHERE PK_ACTION = "
							+ id;
					doSql(sql);
				} else if (channel.equalsIgnoreCase("c_mail")) {
					int mail = ((Integer) values.get("mail")).intValue();
					int assigned =
						((Integer) values.get("assigned")).intValue();
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TACTION SET ADDRESSCOUNT04 = "
							+ assigned
							+ ", ADDRESSCOUNT01 = "
							+ mail
							+ " WHERE PK_ACTION = "
							+ id;
					doSql(sql);
				} else if (channel.equalsIgnoreCase("c_fax")) {
					int fax = ((Integer) values.get("fax")).intValue();
					int assigned =
						((Integer) values.get("assigned")).intValue();
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TACTION SET ADDRESSCOUNT04 = "
							+ assigned
							+ ", ADDRESSCOUNT02 = "
							+ fax
							+ " WHERE PK_ACTION = "
							+ id;
					doSql(sql);
				} else if (channel.equalsIgnoreCase("c_post")) {
					int post = ((Integer) values.get("post")).intValue();
					int assigned =
						((Integer) values.get("assigned")).intValue();
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TACTION SET ADDRESSCOUNT04 = "
							+ assigned
							+ ", ADDRESSCOUNT03 = "
							+ post
							+ " WHERE PK_ACTION = "
							+ id;
					doSql(sql);
				}
			} else if (function.equalsIgnoreCase("setChannel")) {
				logger.fine(
					"setChannel "
						+ values.get("channel")
						+ " "
						+ values.get("id"));
				String channel = (String) values.get("channel");
				int id = Integer.parseInt(values.get("id").toString());
				if (channel.equalsIgnoreCase("mail")) {
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION SET NOTE = 'Mail' WHERE FK_ACTION = "
							+ id
							+ " AND NOTE = 'NotAssigned' AND FK_ADDRESS IN (" +
									//subselect
									"SELECT PK_ADDRESSEXT FROM "
									+ TcDBContext.getSchemaName()+ "TADDRESSEXT " 
									+ " JOIN " + TcDBContext.getSchemaName()+ "TADDRESS  ON (" + TaddressextDB.PK_PKADDRESSEXT  + " = " + TaddressDB.PK_PKADDRESS + " )" 
									+ " WHERE "
									+ TcDBContext.getSchemaName()
									+ "isemailrfc2822compliant(EMAIL)="+(SQL.isMSSQL(TcDBContext.getDefaultContext())?"1":"TRUE")
									+ " AND (" + TaddressDB.BLOCKEDFORCONSIGNMENT + " IS NULL OR " + TaddressDB.BLOCKEDFORCONSIGNMENT + " = 0 )" 
									+ ")";
					logger.log(Level.FINEST, sql);
					doSql(sql);
					createContactFromMailBatchChannel(user, id, "Mail", "Versandauftrag " + id + ": EMailversand", "Dieser Kontakt wurde dem Kanal \"EMailversand\" des Versandauftrages " + id + " hinzugefügt.", CATEGORY_MAILING, CHANNEL_EMAIL);
					logger.log(Level.FINER, values.toString());
				} else if (channel.equalsIgnoreCase("fax")) {
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION SET NOTE = 'Fax' WHERE FK_ACTION = "
							+ id
							+ " AND NOTE = 'NotAssigned' AND FK_ADDRESS IN " +
									"(SELECT PK_ADDRESSEXT FROM "
									+ TcDBContext.getSchemaName()+ "TADDRESSEXT " 
									+ "JOIN " + TcDBContext.getSchemaName() + "TADDRESS ON (" + TaddressextDB.PK_PKADDRESSEXT + "=" + TaddressDB.PK_PKADDRESS + ") "
									+ "WHERE (FAX !='' OR FAX_HOME != '') AND (" + TaddressDB.BLOCKEDFORCONSIGNMENT + " IS NULL OR " + TaddressDB.BLOCKEDFORCONSIGNMENT + " = 0 )" 
									+ ")";
					logger.log(Level.FINER, sql);
					doSql(sql);
					createContactFromMailBatchChannel(user, id, "Fax", "Versandauftrag " + id + ": EMailversand", "Dieser Kontakt wurde dem Kanal \"Faxversand\" des Versandauftrages " + id + " hinzugefügt.", CATEGORY_MAILING, CHANNEL_FAX);
				} else if (channel.equalsIgnoreCase("post")) {
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION SET NOTE = 'Post' WHERE FK_ACTION = "
							+ id
							+ " AND NOTE = 'NotAssigned' AND FK_ADDRESS IN "
								+ "(SELECT PK_ADDRESS FROM "
								+ TcDBContext.getSchemaName() + "TADDRESS " 
								+ "WHERE ((STREET != '' AND ZIPCODE != '' AND CITY != '') OR (POBOX != '' AND POBOXZIPCODE != '' AND CITY != ''))" 
								+ "AND (" + TaddressDB.BLOCKEDFORCONSIGNMENT +" IS NULL OR " + TaddressDB.BLOCKEDFORCONSIGNMENT + " = 0 )"
								+ ")";
					logger.log(Level.FINER, sql);
					doSql(sql);
					createContactFromMailBatchChannel(user, id, "Post", "Versandauftrag " + id + ": EMailversand", "Dieser Kontakt wurde dem Kanal \"Postversand\" des Versandauftrages " + id + " hinzugefügt.", CATEGORY_MAILING, CHANNEL_MAIL);
				}
			} else if (function.equalsIgnoreCase("resetChannel")) {
				logger.fine(
					"resetChannel "
						+ values.get("channel")
						+ " "
						+ values.get("id"));
				String channel = (String) values.get("channel");
				int id = Integer.parseInt(values.get("id").toString());
				if (channel.equalsIgnoreCase("mail")) {
					createContactFromMailBatchChannel(user, id, "Mail", "Versandauftrag " + id + ": EMailversand", "Diese Adresse wurde aus dem Kanal EMailversand des Versandauftrages " + id + " gelöscht.", CATEGORY_MAILING, CHANNEL_EMAIL);
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION SET NOTE = 'NotAssigned' WHERE FK_ACTION = "
							+ id
							+ " AND NOTE = 'Mail'";
					doSql(sql);
				} else if (channel.equalsIgnoreCase("fax")) {
					createContactFromMailBatchChannel(user, id, "Fax", "Versandauftrag " + id + ": Faxversand", "Diese Adresse wurde aus dem Kanal Faxversand des Versandauftrages " + id + " gelöscht.", CATEGORY_MAILING, CHANNEL_FAX);
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION SET NOTE = 'NotAssigned' WHERE FK_ACTION = "
							+ id
							+ " AND NOTE = 'Fax'";
					doSql(sql);
				} else if (channel.equalsIgnoreCase("post")) {
					createContactFromMailBatchChannel(user, id, "Post", "Versandauftrag " + id + ": Postversand", "Diese Adresse wurde aus dem Kanal Postversand des Versandauftrages " + id + " gelöscht.", CATEGORY_MAILING, CHANNEL_MAIL);
					sql =
						"UPDATE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION SET NOTE = 'NotAssigned' WHERE FK_ACTION = "
							+ id
							+ " AND NOTE = 'Post'";
					doSql(sql);
				}
			} else if (function.equals("getChannelAddresses")) {
				logger.fine(
					"getChannelAddresses "
						+ values.get("channel")
						+ " "
						+ values.get("id"));
				int id = Integer.parseInt(values.get("id").toString());
				String channel = (String) values.get("channel");

				if (channel.equalsIgnoreCase("")) {
					sql =
						"SELECT DISTINCT "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.FK_ADDRESS, "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT.LASTNAME_M FROM "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION, "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT "
							+ "WHERE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.FK_ACTION = "
							+ id
							+ " "
							+ "AND "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT.PK_ADDRESSEXT = TADDRESSACTION.FK_ADDRESS ORDER BY "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT.LASTNAME_M ASC";
				} else {
					sql =
						"SELECT DISTINCT "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.FK_ADDRESS, "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT.LASTNAME_M FROM "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION, "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT "
							+ "WHERE "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.FK_ACTION = "
							+ id
							+ " AND "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.NOTE = '"
							+ channel
							+ "' "
							+ "AND "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT.PK_ADDRESSEXT = "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSACTION.FK_ADDRESS ORDER BY "
							+ TcDBContext.getSchemaName()
							+ "TADDRESSEXT.LASTNAME_M ASC";
				}

				rs = getResultSet(sql);
				int i = 0;
				while (rs.next()) {
					map.put("id" + i, new Integer(rs.getInt(1)));
					i++;
				}
				return map;
			} else if (function.equals("setIntValue")) {
				logger.fine(
					"setIntValue "
						+ values.get("name")
						+ " "
						+ values.get("id"));
				int id = Integer.parseInt(values.get("id").toString());
				int value = ((Integer) values.get("value")).intValue();
				String name = (String) values.get("name");
				sql =
					"UPDATE "
						+ TcDBContext.getSchemaName()
						+ "TACTION SET "
						+ name
						+ " = "
						+ value
						+ " WHERE PK_ACTION = "
						+ id;
				doSql(sql);
			} else if (function.equals("setStringValue")) {
				logger.fine(
					"setStringValue "
						+ values.get("name")
						+ " "
						+ values.get("id"));
				int id = Integer.parseInt(values.get("id").toString());
				String value = (String) values.get("value");
				String name = (String) values.get("name");
				sql =
					"UPDATE "
						+ TcDBContext.getSchemaName()
						+ "TACTION SET "
						+ name
						+ " = '"
						+ value
						+ "' WHERE PK_ACTION = "
						+ id;
				doSql(sql);
			}

			return map;
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::mailBatch: Verbindung konnte nicht erstellt werden.",
				e);
			map.put("fehler", e.toString());
			return map;
		}finally{
			DB.close(rs);
		}
	}

	/**
	 * @param user
	 * @param id
	 * @param channel
	 * @throws SQLException
	 */
	private void createContactFromMailBatchChannel(String user, int id, String mb_channel, String subject, String note, int category, int channel) throws SQLException {
		String sql;
		String mb_channelstring = "= '"+mb_channel+"'";
		if(mb_channel.equals("%")){
			mb_channelstring = "LIKE '%'";
		}
		//Jetzt das ganze f�r die Contact-Tabelle
		//Hole alle PK's die zugeordnet wurden
		Integer userid = getUserId(user);
		//Und f�ge die entsprechenden Adressen in TCONTACT ein
		sql = "INSERT INTO " + TcDBContext.getSchemaName() + "TCONTACT (FK_ADDRESS, FK_USER, FK_CONTACTCATEGORY, FK_CONTACTCHANNEL, FK_CONTACTLINK, FK_CONTACTLINKTYPE, CONTACTDIRECTION, SUBJECT, NOTE, CONTACTDATE, DURATION) "
			+ "SELECT FK_ADDRESS , " + userid + ", " + category + ", " + channel + ", 0, 0, " + DIRECTION_OUTBOUND + ", '" + subject + "', '" + note + "', " + SQL.format(TcDBContext.getDefaultContext(), new Date()) +", 0 FROM " + TcDBContext.getSchemaName() + "TADDRESSACTION WHERE FK_ACTION = " + id + " AND NOTE " + mb_channelstring;
		doSql(sql);
	}

	/**
	 * Diese Methode erweitert einen Stringbuffer um eine Bedingung, in der eine
	 * Spalte mit einem Inhalt verglichen wird. Enthaelt der Inhalt ein '*', wird
	 * mit 'LIKE' gepr�ft. Gegebenenfalls wird mit Matchcodes getestet.
	 * 
	 * @param buffer die SQL-WHERE-Klausel in diesem Buffer wird erweitert.
	 * @param column der Spaltenname, zu dem eine Bedingung angehaengt wird.
	 * @param content der Inhalt, auf den getestet wird; enthaelt dieser ein
	 *  '*', wird mit 'LIKE' gepr�ft.
	 * @param useMatchcode legt fest, ob unscharfe Suche benutzt werden soll.
	 */
	protected void appendCondition(
		StringBuffer buffer,
		String column,
		String content,
		boolean useMatchcode) {
		if (column == null || content == null)
			return;
		content = content.trim();
		if (content.length() == 0)
			return;

		if (buffer.length() > 0)
			buffer.append(" AND ");
		buffer.append('(');

		if (useMatchcode) {
			if ("strasse".equalsIgnoreCase(column))
				column = "STREET";
			content = getMatchcode(content);
			//buffer.append("MATCHCODE"); // Praefix zum Spaltennamen
		}

		String operator = " = '";
		if (content.indexOf('*') > -1) {
			operator = " LIKE '";
			content = content.replace('*', '%');
		}

		if (column.equals("101")
			|| column.equals("102")
			|| column.equals("103")
			|| column.equals("104"))
			buffer
				.append(TcDBContext.getSchemaName() + "TCOMM.FK_COMMTYPE = '" + column + "' AND ")
				.append(TcDBContext.getSchemaName() + "TCOMM.VALUE")
				.append(operator)
				.append(content)
				.append('\'');
		else if (column.equals("108"))
			buffer
				.append(TcDBContext.getSchemaName() + "TCOMM.FK_COMMTYPE = '" + column + "' AND ")
				.append("lower(" + TcDBContext.getSchemaName() + "TCOMM.VALUE)")
				.append(operator)
				.append(content)
				.append('\'');
		else
			buffer.append(column).append(operator).append(content).append('\'');

		buffer.append(')');
	}

	/**
	 * Diese Methode erweitert einen Stringbuffer um eine Bedingung, in der der
	 * Inhalt der PLZ-Spalte getestet wird. Hierbei sind auch Bereichsabfragen
	 * erlaubt: [n]-[m] testet den Bereich von n bis m, jeweils auf 5 Stellen
	 * erweitert, &gt;[=]n testet den Bereich ab n, &lt;[=]m den bis m.
	 * @param buffer die SQL-WHERE-Klausel in diesem Buffer wird erweitert.
	 */
	protected void appendPLZCondition(StringBuffer buffer, String plz) {
		if (plz == null)
			return;
		plz = plz.trim();
		if (plz.length() == 0 || plz.equals("-"))
			return;

		if (buffer.length() > 0)
			buffer.append(" AND ");
		buffer.append('(');

		int dashIndex = plz.indexOf("-");
		if (dashIndex > -1) { // range
			String start = plz.substring(0, dashIndex).trim() + "00000";
			String end = plz.substring(dashIndex + 1).trim() + "00000";
			boolean hasStart = (start.length() > 5);
			boolean hasEnd = (end.length() > 5);

			if (hasStart) {
				buffer.append(TcDBContext.getSchemaName() + "TADDRESS.ZIPCODE >= ").append(
					"'" + start.substring(0, 5) + "'");
				if (hasEnd)
					buffer.append(") AND (");
			}

			if (hasEnd)
				buffer.append(TcDBContext.getSchemaName() + "TADDRESS.ZIPCODE <= ").append(
					"'" + end.substring(0, 5) + "'");

		} else if (plz.charAt(0) == '<' || plz.charAt(0) == '>') {
			buffer.append(TcDBContext.getSchemaName() + "TADDRESS.ZIPCODE ").append(plz.charAt(0));
			if (plz.charAt(1) == '=') {
				buffer.append('=');
				plz = plz.substring(2).trim();
			} else {
				plz = plz.substring(1).trim();
			}
			plz += "00000";
			buffer.append("'" + plz.substring(0, 5) + "'");
		} else if ((dashIndex = plz.indexOf("*")) > -1) {
			String start = plz.substring(0, dashIndex).trim() + "00000";
			String end = plz.substring(0, dashIndex).trim() + "99999";
			buffer.append(TcDBContext.getSchemaName() + "TADDRESS.ZIPCODE >= ").append(
				"'" + start.substring(0, 5));
			buffer.append("') AND (" + TcDBContext.getSchemaName() + "TADDRESS.ZIPCODE <= ").append(
				"'" + end.substring(0, 5) + "'");
		} else
			buffer.append(TcDBContext.getSchemaName() + "TADDRESS.ZIPCODE = ").append(
				"'" + plz + "'");

		buffer.append(')');
	}

	/**
	 * Diese Methode erzeugt zu einem Wert einen Matchcode passend zur �bergebenen
	 * Spalte. Erlaubtes Jokerzeichen ist '*'.<br>
	 * Hierbei sollte eigentlich die Stored Procedure buildMatchcode genutzt werden,
	 * deren Funktionalitaet hier nachgebildet wird.
	 * 
	 * @param value der Wert, zu dem ein Matchcode erzeugt werden soll.
	 * @return der erzeugte Matchcode.
	 */
	protected String getMatchcode(String value) {
		if (value == null || value.length() == 0)
			return "";

		value =
			value
            .toUpperCase()
            .replaceAll("\u00C4", "AE")
            .replaceAll("\u00D6", "OE")
            .replaceAll("\u00DC", "UE")
            .replaceAll("SS", "S")
            .replaceAll("CK", "K")
            .replaceAll("DT", "D")
            .replaceAll("-", "")
            .replace('\u00DF', 'S');

		StringBuffer buffer = new StringBuffer(value);
		char lastChar = 0;
		int index = 0;
		while (index < buffer.length()) {
			char thisChar = buffer.charAt(index);
			if ((thisChar != lastChar)
				&& ((thisChar == '*')
					|| ((thisChar >= 'A') && (thisChar <= 'Z')))) {
				lastChar = thisChar;
				index++;
			} else
				buffer.deleteCharAt(index);
		}

		return buffer.toString();
	}

	/**
	 * Diese Methode liefert zu einer Adress-Map, in der id&lt;n> auf eine
	 * n-te Adress-Id abgebildet wird, eine Liste, in der zu dem in den
	 * Parametern beschriebenen Indexfenster die konkreten Addressdaten
	 * stehen.
	 *  
	 * @param indices eine Map "id&lt;n>" -> n-te Adress-ID als Integer
	 * @param start der erste Index f�r die Liste
	 * @param count die gew�nschte Listenlaenge
	 * @param category Kategorie, in der liegend die Adresse verstanden werden soll
	 * @return eine Liste von Listen (id, Adressdaten) des definierten Fensters.
	 */
	public List getAddressSectionList(
		Map indices,
		int start,
		int count,
		String category) {
		List result = new ArrayList(count);

		if (indices != null) {
			for (int i = start; count > 0; count--, i++) {
				Integer id = (Integer) indices.get("id" + i);
				if (id == null)
					break;
				List entry = new ArrayList(2);
				entry.add(id);
				entry.add(getAddress(id.intValue(), category));
				result.add(entry);
			}
		}

		return result;
	}

	/**
	 * Diese Methode erzeugt eine Adresslisteninfoliste.
	 * 
	 * @param total Anzahl Adressen insgesamt
	 * @param start Startindex (0-basiert) des Anzeigefensters
	 * @param count Gr�sse des Anzeigefensters
	 * @return Liste (aktuelle Seite, Anzahl Seiten, Startindex erste Seite,
	 *  Startindex vorige Seite, Startindex naechste Seite, Startindex
	 *  letzte Seite, Index erste angezeigte Adresse, Index letzte
	 *  angezeigte Adresse, Index letzte Adresse)
	 */
	public List createAddressListInfo(int total, int start, int count) {
		List result = new ArrayList(9);

		int currentPage = 0;
		int maxPage = 0;

		if (count > 0) {
			currentPage = 1 + start / count;
			maxPage = (total + (count - 1)) / count;
		}

		int firstPageStart = 0;
		int lastPageStart = (maxPage - 1) * count;
		int previousPageStart =
			(currentPage > 1) ? (currentPage - 2) * count : firstPageStart;

		int nextPageStart =
			(currentPage < maxPage)
				? currentPage * count
				: (maxPage - 1) * count;

		int maxAddress = total;
		int viewFirstAddress = start + 1;
		int viewLastAddress = start + count;
		if (viewLastAddress > maxAddress)
			viewLastAddress = maxAddress;

		result.add(new Integer(currentPage));
		result.add(new Integer(maxPage));
		result.add(new Integer(firstPageStart));
		result.add(new Integer(previousPageStart));
		result.add(new Integer(nextPageStart));
		result.add(new Integer(lastPageStart));
		result.add(new Integer(viewFirstAddress));
		result.add(new Integer(viewLastAddress));
		result.add(new Integer(maxAddress));

		return result;
	}

	/**
	* Diese Methode holt alle initialen Datensaetze.
	* Es werden lediglich alle Aufrufe der Startprozedur gesammelt und
	* geb�ndelt �bergeben, da bei einer SSL-Verbindung Einzelaufrufe
	* aufgrund der Authentifizierung zu lange dauern.
	* 
	*/
	public Map getInitDatas(Map params) {
		Map result = new TreeMap();
		String user = (String) params.get("user");
		String kategorie = (String) getStandardVerteiler(user).get("verteiler");
		boolean allgemein = ((Boolean) params.get("allgemein")).booleanValue();
		int count = ((Integer) params.get("count")).intValue();
		int allg = 0;
		if (allgemein)
			allg = 1;

		Map map = new TreeMap();
		map.put("verteilergruppe", kategorie);
		map.put("allgemein", new Integer(allg));
		map.put("schnitt", params.get("selection"));
		map.put("count", new Integer(count));
		map.put("package", params.get("index"));
		map.put("user", user);
		int size = 0;
		Map verteilerList = getVerteiler(user, kategorie);
		if (verteilerList != null) {
			Iterator it = verteilerList.keySet().iterator();
			for (int i = 0; it.hasNext(); i++) {
				map.put("selVert" + i, it.next().toString());
				size++;
			}
		}
		map.put("size", new Integer(size));

		result.put("getVersion", getVersion());
		result.put("getAnreden", getAnreden());
		result.put("getL�nder", getLaender());
		result.put("getLKZen", getLKZ());
		result.put("getBundesl�nder", getBundeslaender());
//		result.put("getVerteilergruppen", getGruppen(user));
		result.put("getStandardVerteilergruppe", kategorie);
//		result.put("getVerteiler", verteilerList);
//		result.put("getChosenAddresses", getChosenAddresses(map));
		return result;
	}

	/**
	 * Diese Methode erzeugt aus einer XML-Suchanfrage eine Ergebnismenge als
	 * XML-Tabelle.
	 * 
	 * @param XML die XML-Suchanfrage
	 * @return die Map, die ??? auf ??? abbildet.
	 */
	public String getAdresses(String XML, String userID) {
		ResultSet rs = null;
		try {
			boolean createLabel = true;
			//Anfrage parsen
			XMLSearchHandlerPostgres handler = new XMLSearchHandlerPostgres(XML,userID);
			List fields = handler.getSQLFieldList();
			//SQL-Anfrage erstellen
			StringBuffer sb = new StringBuffer();
			sb.append(handler.getSQLSelect());
			sb.append(handler.getSQLFrom());
            sb.append(handler.getSQLWhere());

			//Anfrage ausf�hren
			rs = getResultSet(sb.toString());
			//XML-Tabelle erstellen
			sb =
				new StringBuffer("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n");
			//sb.append("<dataset dataformat=\"tarent.xml.orders\" header=\"no\">\n");
			sb.append("<dataset>");
			//in die erste Tabellenzeile kommen die Feldnamen
			Iterator it = fields.iterator();
			sb.append("<R>");
			while (it.hasNext()) {
                XMLSearchHandlerPostgres.FieldDescriptor fd = (XMLSearchHandlerPostgres.FieldDescriptor) it.next();

                if (! fd.getTitle().equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERADDRESS)
                    && ! fd.getTitle().equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERADDRESSAUTO)
                    && ! fd.getTitle().equals(XMLSearchHandlerPostgres.REQUEST_FIELD_ANREDE)
                    && ! fd.getTitle().equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERSALUTATIONAUTO))
                    
                    {                
                        sb.append("<F>");
                        sb.append(escape(fd.getTitle()));
                        sb.append("</F>");
                    }
            }
			if (createLabel) {
				sb.append("<F>Anschrift</F>");
				sb.append("<F>Briefanrede</F>");
			}
			sb.append("</R>");
			//der Tabelleninhalt
			int i;
			while (rs.next()) {
				sb.append("<R>");
				if (createLabel)
					getXMLResultWithLabel(rs, sb, fields);
				else
					for (i = 1; i <= fields.size(); i++)
					    appendResultField(sb, rs, i);
				sb.append("</R>");
			}
			sb.append("</dataset>");
			rs.close();

			return sb.toString();
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"DataAccess::getAdresses: Verbindung konnte nicht erstellt werden.",
				e);
			return e.toString();
		}finally{
			DB.close(rs);
		}
	}

	private static String appendResultField(StringBuffer buffer, ResultSet rs, int column) throws SQLException {
	    String str = rs.getString(column);
	    if (rs.wasNull() || str == null)
	        buffer.append("<F isnull=\"true\"/>");
	    else
	        buffer.append("<F>").append(escape(str)).append("</F>");
	    return str;
	}

	/**
	 * haengt an den XML-String die zwei Felder Label und Salutation an, diese werden aus der naechsten
	 * Zeile des ResultSets erzeugt.
	 * @param result 
	 * @param buffer
	 * @param fields
	 * @throws SQLException
	 */
	private void getXMLResultWithLabel(
		ResultSet result,
		StringBuffer buffer,
		List fields)
		throws SQLException {
		String anrede = "", anredekurz = "", herrFrau = "", titel = "", akadTitle = "";
		String vorname = "", nachname = "", namenszusatz = "", institution = "";
		String strasse = "", hausNr = "", plz = "", stadt = "";
		String plzPostfach = "", postfach = "", land = "", position = "", abteilung = "";
        String lettersalutation_auto = "", letteraddress = "", letteraddress_auto = "";
		int i;
		String str;
		Iterator it = fields.iterator();
		for (i = 1; i <= fields.size(); i++) {

			String fieldTitle = ((XMLSearchHandlerPostgres.FieldDescriptor) it.next()).getTitle();

			if (! fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERADDRESS)
                && ! fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERADDRESSAUTO)
                    
                && ! fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_ANREDE)
                && ! fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERSALUTATIONAUTO))
                {
                
                    str = appendResultField(buffer, result, i);
                    
                } 
                else 

                    str = result.getString(i);


			if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_ANREDE))
				anrede = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERSALUTATIONAUTO))
				lettersalutation_auto = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERADDRESS))
				letteraddress = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LETTERADDRESSAUTO))
				letteraddress_auto = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_HERRNFRAU))
				herrFrau = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_TITEL))
				titel = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_AKAD_TITEL))
				akadTitle = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_VORNAME))
				vorname = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_NACHNAME))
				nachname = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_NAMENSZUSATZ))
				namenszusatz = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_INSTITUTION))
				institution = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_STRASSE))
				strasse = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_HAUSNR))
				hausNr = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_PLZ))
				plz = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_ORT))
				stadt = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_PLZ_POSTFACH))
				plzPostfach = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_POSTFACHNR))
				postfach = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_LAND))
				land = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_POSITION))
				position = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_ABTEILUNG))
				abteilung = str;
			else if (fieldTitle.equals(XMLSearchHandlerPostgres.REQUEST_FIELD_KURZANREDE))
				anredekurz = str;
		}

		buffer
			.append("<F>")
			.append(
				escape(
                       nl2br(
                             //Einbau der neuen, einheitlichen Funktion von Nils
                             (letteraddress_auto == null 
                              || letteraddress_auto.equals("")
                              || letteraddress_auto.equals("1")) 
                             ?                       
                             LetterAddressGenerator.generateLetterAddress(
                                                               institution,
                                                               herrFrau,
                                                               akadTitle,
                                                               vorname,
                                                               titel,
                                                               nachname,
                                                               namenszusatz,
                                                               position,
                                                               abteilung,
                                                               strasse,
                                                               hausNr,
                                                               plzPostfach,
                                                               postfach,
                                                               plz,
                                                               stadt,
                                                               land)
                             :
                             letteraddress
                             ))
                )  
			.append("</F><F>")
			.append(
				escape(
                       //Einbau der neuen, einheitlichen Funktion von Nils
                       nl2br(

                             (lettersalutation_auto == null
                              || lettersalutation_auto.equals("")
                              || lettersalutation_auto.equals("1"))
                             ?
                             LetterSalutationGenerator.generateLetterSalutation(
                                                                  anredekurz,
                                                                  vorname,
                                                                  herrFrau,
                                                                  akadTitle,
                                                                  titel,
                                                                  nachname
                                                                  )
                             :
                             anrede))
                )
			.append("</F>");
	}

	/*
	 * private Hilfsmethoden
	 */
	/** Diese Klassenmethode ersetzt die Zeichen in <code>source</code>, die in
	 *  XML-Dateien speziellen Bedeutung haben, durch entsprechende Entitaeten.
	 * 
	 *  @param source der String, dessen Zeichen umgesetzt werden sollen.
	 *  @return der String, in dem die speziellen Zeichen durch Entitaeten ersetzt
	 *    wurden.
	 */
	private static String escape(String source) {
		StringBuffer buffer = new StringBuffer();
		if (source != null) {
			for (int index = 0; index < source.length(); index++) {
				switch (source.charAt(index)) {
					case '&' :
						buffer.append("&amp;");
						break;
					case '<' :
						buffer.append("&lt;");
						break;
					case '>' :
						buffer.append("&gt;");
						break;
					case '"' :
						buffer.append("&quot;");
						break;
					default :
						buffer.append(source.charAt(index));
				}
			}
		}
		return buffer.toString();
	}

	private static String nl2br(String source) {
		StringBuffer buffer = new StringBuffer();
		if (source != null) {
			for (int index = 0; index < source.length(); index++) {
				switch (source.charAt(index)) {
                case '\n' :
                    buffer.append("<br>");
                    break;
                default :
						buffer.append(source.charAt(index));
				}
			}
		}
		return buffer.toString();
	}

	/**
	 * Diese Methode maskiert ein Object f�r einen SQL String.
	 * Arbeitet bei Maps und Listen rekursiv.
	 * 
	 * @param object
	 */
	private static Object escapeSQL(Object object) {
		if (object instanceof String)
			object = escapeSQLstring((String) object);
		else if (object instanceof Map)
			object = escapeSQLmap((Map) object);
		return object;
	}

	private static Object escapeSQLstring(String string) {
		string = string.replace('\'', '`').replaceAll("\0", "");
		return string;
	}

	private static Map escapeSQLmap(Map map) {
		Object key = null;
		Object value = null;
		Iterator it = map.keySet().iterator();
		while (it.hasNext()) {
			key = it.next();
			value = map.get(key);
			value = escapeSQL(value);
			map.put(key, value);
		}
		return map;
	}

	private static List escapeSQLlist(List list) {
		int key = 0;
		Object value = null;
		for (key = 0; key < list.size(); ++key) {
			value = list.get(key);
			value = escapeSQL(value);
			list.set(key, value);
		}
		return list;
	}

	/**
	 * Setter f�r ldm
	 * @param ldm
	 */
	public void setLdm(LDAPManager ldm) {
		this.ldm = ldm;
	}

	public String getVerteilerGruppenName(int key) {
		String sql =
			"SELECT FOLDERNAME FROM "
				+ TcDBContext.getSchemaName()
				+ "TFOLDER WHERE FK_FOLDER='"
				+ String.valueOf(key)
				+ "'";
		try {
			ResultSet rs = getResultSet(sql);
			rs.next();
			return rs.getString(0);
		} catch (Exception e) {
			logger.log(
				Level.SEVERE,
				"Konnte Verteilergruppenname nicht ermitteln! "
					+ e.getMessage());
			return null;
		}

	}

	/* (non-Javadoc)
	 * @see de.tarent.contact.octopus.DataAccess#getAddressesfromGroup(int)
	 */
	public Map getAddressesfromGroup(int verteilergruppe) {
		SortedMap sm = new TreeMap();
		try {
			String sql = "";
			int index = 0;
			sql =
				"SELECT DISTINCT "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSSUBFOLDER.FK_ADDRESS, "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSEXT.LASTNAME_M FROM "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSSUBFOLDER, "
				+ TcDBContext.getSchemaName()
				+ "TADDRESS, "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSEXT "
				+ "WHERE "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSSUBFOLDER.FK_FOLDER = '"
				+ verteilergruppe
				+ "' "
				+ "AND "
				+ TcDBContext.getSchemaName()
				+ "TADDRESS.PK_ADDRESS = "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSSUBFOLDER.FK_ADDRESS "
				+ "AND "
				+ TcDBContext.getSchemaName()
				+ "TADDRESS.PK_ADDRESS = "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSEXT.PK_ADDRESSEXT ORDER BY "
				+ TcDBContext.getSchemaName()
				+ "TADDRESSEXT.LASTNAME_M ASC";
			//logger.log(Level.FINE, "SQL: "+sql);

			ResultSet rs = getResultSet(sql);
			Map addressMap = new TreeMap();
			while (rs.next()) {
				sm.put("id" + index, new Integer(rs.getInt(1)));
				addressMap.put(
						"id" + rs.getInt(1),
						getAddressRev(
								rs.getInt(1),
								String.valueOf(verteilergruppe)));
				index++;
			}
			rs.close();
			sm.put("address", addressMap);
			if (index > 0)
				return sm;
			return new TreeMap();
		} catch (Exception e) {
			logger.log(
					Level.SEVERE,
					"DataAccess::getChosenAddresses: Verbindung konnte nicht erstellt werden.",
					e);
			sm.put("fehler", e.toString());
			return sm;
		}
	}

 	synchronized public Map getAddressRev(int AdrId, String category) {
 		Map addressData = new TreeMap();
 		ResultSet rs = null;

        PreparedStatement stmtGetAddressMainData = null;
        PreparedStatement stmtGetAddressExtData = null;
        PreparedStatement stmtGetAddressCategoryData = null;
        PreparedStatement stmtGetAddressSubCategories = null;		

		String sql = null;
        Connection connection = null;
 		try {
            connection = createJdbcConnection();
            if (stmtGetAddressMainData == null) {
                sql = "SELECT * FROM " + TcDBContext.getSchemaName() + "TADDRESS WHERE PK_ADDRESS = ?";
                logger.fine(
                            "SQL-Statement prepare [stmtGetAddressMainData]: " + sql);
                stmtGetAddressMainData = connection.prepareStatement(sql);
            }
            if (stmtGetAddressExtData == null) {
                sql =
                    "SELECT * FROM "
                    + TcDBContext.getSchemaName()
                    + "TADDRESSEXT WHERE PK_ADDRESSEXT = ?";
                logger.fine(
                            "SQL-Statement prepare [stmtGetAddressExtData]: " + sql);
                stmtGetAddressExtData = connection.prepareStatement(sql);
            }
            if (stmtGetAddressCategoryData == null) {
                sql =
                    "SELECT * FROM "
                    + TcDBContext.getSchemaName()
                    + "TADDRESSFOLDER "
                    + "WHERE FK_ADDRESS = ? AND FK_FOLDER = ?";
                logger.fine(
                            "SQL-Statement prepare [stmtGetAddressCategoryData]: " + sql);
                stmtGetAddressCategoryData =
                    connection.prepareStatement(sql);
            }
            if (stmtGetAddressSubCategories == null) {
                sql =
                    "SELECT SUB.PK_SUBFOLDER AS PK_SUBFOLDER, SUB.FOLDERNAME AS FOLDERNAME "
                    + "FROM "
                    + TcDBContext.getSchemaName()
                    + "TADDRESSSUBFOLDER AS ASUB, "
                    + TcDBContext.getSchemaName()
                    + "TSUBFOLDER AS SUB "
                    + "WHERE ASUB.FK_ADDRESS = ? AND SUB.FK_FOLDER = ? "
                    + "AND SUB.PK_SUBFOLDER = ASUB.FK_SUBFOLDER";
                logger.fine(
                            "SQL-Statement prepare [stmtGetAddressSubCategories]: " + sql);
                stmtGetAddressSubCategories =
                    connection.prepareStatement(sql);
            }

 			stmtGetAddressMainData.setInt(1, AdrId);
 			rs = stmtGetAddressMainData.executeQuery();
 			if (rs.next()) {
 				addressData.put("a1", rs.getString("SALUTATION"));
 				addressData.put("a2", rs.getString("TITELJOB"));
 				addressData.put("a3", rs.getString("TITELACADEMIC"));
 				addressData.put("a4", rs.getString("FIRSTNAME"));
 				addressData.put("a5", rs.getString("LASTNAME"));
 				addressData.put("a6", rs.getString("NAMESUFFIX"));
 				addressData.put("a7", rs.getString("ORGANISATION"));
 				addressData.put("a8", rs.getString("STREET"));
 				addressData.put("a9", rs.getString("HOUSENO"));
 				addressData.put("a10", rs.getString("ZIPCODE"));
 				addressData.put("a11", rs.getString("CITY"));
 				addressData.put("a12", rs.getString("POBOXZIPCODE"));
 				addressData.put("a13", rs.getString("POBOX"));
 				addressData.put("a14", rs.getString("CREATED"));
 				addressData.put("a15", rs.getString("CHANGED"));
 				addressData.put("a16", rs.getString("SUBSCRIPTOR"));
 				addressData.put("a17", rs.getString("SUBSCRIPTION"));
 				addressData.put("a18", rs.getString("LETTERSALUTATION"));
 				addressData.put("a19", rs.getString("COUNTRY"));
 				addressData.put("a20", rs.getString("COUNTRYSHORT"));
 				addressData.put("a21", rs.getString("REGION"));
 				addressData.put("e1", rs.getString("MIDDLENAME"));
 				addressData.put("e2", rs.getString("NICKNAME"));
 				addressData.put("e3", rs.getString("ORGANISATION2"));
 				addressData.put("e4", rs.getString("DEPARTMENT"));
 				addressData.put("e5", rs.getString("NOTEONADDRESS"));
 				addressData.put("e6", rs.getString("POSITION"));
 				addressData.put("e7", rs.getString("BANKNAME"));
 				addressData.put("e8", rs.getString("BANKNO"));
 				addressData.put("e9", rs.getString("BANKACCOUNT"));
 				addressData.put("e10", rs.getString("SEX"));
 				addressData.put("e11", new Integer(rs.getInt("YEAROFBIRTH")));
 				addressData.put("e12", rs.getString("DATEOFBIRTH"));
 			}
 			rs.close();

 			stmtGetAddressExtData.setInt(1, AdrId);
 			rs = stmtGetAddressExtData.executeQuery();
 			Map commData = new TreeMap();
 			if (rs.next()) {
 				commData.put("101", rs.getString("FON_HOME"));
 				commData.put("102", rs.getString("FON"));
 				commData.put("103", rs.getString("FAX_HOME"));
 				commData.put("104", rs.getString("FAX"));
 				commData.put("105", rs.getString("MOBILE_HOME"));
 				commData.put("106", rs.getString("MOBILE"));
 				String office = rs.getString("EMAIL");
 				String home = rs.getString("EMAIL_HOME");
 				commData.put(
                             "108",
                             (office == null || office.length() == 0) ? home : office);
 				office = rs.getString("URL");
 				home = rs.getString("URL_HOME");
 				commData.put(
                             "109",
                             (office == null || office.length() == 0) ? home : office);
 			}
 			addressData.put("a22", commData);
 			rs.close();

 			Map subCategories = new TreeMap();
 			if (category != null && category.length() > 0) {
 				stmtGetAddressCategoryData.setInt(1, AdrId);
 				stmtGetAddressCategoryData.setObject(2, category);
 				rs = stmtGetAddressCategoryData.executeQuery();
 				if (rs.next()) {
 					addressData.put("a23", rs.getString("NOTE2"));
 					addressData.put("a24", rs.getString("NOTE"));
 				}
 				rs.close();

 				stmtGetAddressSubCategories.setInt(1, AdrId);
 				stmtGetAddressSubCategories.setObject(2, category);
 				rs = stmtGetAddressSubCategories.executeQuery();
 				while (rs.next()) {
 					String id = rs.getString("PK_SUBFOLDER");
 					String name = rs.getString("FOLDERNAME");
 					subCategories.put(id, name);
 				}
 				rs.close();
 			}
 			addressData.put("a25", subCategories);
 		} catch (SQLException sex) {
 			logger.log(
                       Level.WARNING,
                       "Fehler beim Abholen von Adressdaten zu Adresse "
                       + AdrId
                       + " in "
                       + category,
                       sex);
        } finally {
        }


 		return addressData;
 	}
	
	
	public List getColumnsFromTable (String tableName) {
		List names = new Vector();
        Connection con = null;
		try {
			con = createJdbcConnection();
			DatabaseMetaData metaData = con.getMetaData();
			ResultSet columnNames = metaData.getColumns(null, TcDBContext.getSchemaNameWithoutDot(), tableName, "%"); 
			while (columnNames.next()) {
				names.add(columnNames.getString("COLUMN_NAME"));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
        finally {
            if (con != null)
                try {
                    con.close();
                } catch (SQLException closeE) {}

        }
		return names;
	}
	


	private boolean containsVerteiler (Map map) {
		for (Iterator it = map.keySet().iterator();it.hasNext();) {
		    String key = (String)it.next();
		    if (key.startsWith("selVert")) {
		        return true;
		    }
		}
		return false;
	}
	
	/* (non-Javadoc)
     * @see de.tarent.contact.octopus.DataAccess#getHistory(java.lang.String, java.util.List, java.util.Date, java.util.Date)
     */
    public List getHistory(int adrid, List type, Date start, Date end) {

        //
        // Konstanten f�r HistoryElemente
        //
        /** Typ: Termin */
        final String TYPE_APPOINTMENT = "appointment";
        /** Typ: Versandauftrag */
        final String TYPE_MAILBATCH = "mailbatch";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	    
        Vector returnlist = new Vector();
        if(type==null || type.contains(TYPE_MAILBATCH)) {
		    //Type: MailBatch
		    String sql = "" +
		    		"SELECT pk_action" +
		    		" FROM " +
		    		TcDBContext.getSchemaName() + "taction, " +
		    		TcDBContext.getSchemaName() + "taddressaction" +
		    		" WHERE taddressaction.fk_address = " + adrid
		    		+" AND taddressaction.fk_action = taction.pk_action" +
		    				" AND ((taction.actiondate <= date '" + sdf.format(end) + "')" +
		    						"AND (taction.actiondate >= date '" + sdf.format(start) + "'))";
		    ResultSet rs = null;
		    try {
                rs = getResultSet(sql);
                while(rs.next()) {
                    String id = rs.getString("pk_action");
                    logger.log(Level.FINE, "Adding MailBatch PK: " + id);
                    returnlist.add(id);
                    returnlist.add(TYPE_MAILBATCH);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Fehler bei der Datenbankabfrage!", e);
            }finally{
            	DB.close(rs);
            }
        }
        if(type==null || type.contains(TYPE_APPOINTMENT)) {
            //Type: Appointment
        	String sql = 
        		"SELECT TEVENT.PK "
        		+ "FROM "
				+ TcDBContext.getSchemaName() + "TEVENT, "
				+ TcDBContext.getSchemaName() + "TADDRESSEVENT "
				+ "WHERE TADDRESSEVENT.FK_ADDRESS = '" + adrid + "' "
				+ "AND TEVENT.PK = TADDRESSEVENT.FK_EVENT "
				+ "AND( "
				+ "(TEVENT.EVENTSTART <= DATE('"+sdf.format(start)+"') AND TEVENT.EVENTEND >=DATE('"+sdf.format(end)+"'))"
				+ " OR "
				+ "(TEVENT.EVENTSTART >= DATE('"+sdf.format(start)+"') AND TEVENT.EVENTSTART <= DATE('"+sdf.format(end)+"'))"
				+ " OR "
				+ "(TEVENT.EVENTEND >= DATE('"+sdf.format(start)+"') AND TEVENT.EVENTEND <= DATE('"+sdf.format(end)+"'))"
        		+ ")";
			logger.log(Level.FINE, sql);
			ResultSet rs = null;
		    try {
                rs = getResultSet(sql);
                while(rs.next()) {
                    String id = rs.getString("PK");
                    logger.log(Level.FINE, "Adding Event PK: " + id);
                    returnlist.add(id);
                    returnlist.add(TYPE_APPOINTMENT);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Fehler bei der Datenbankabfrage!", e);
            }finally{
            	DB.close(rs);
            }
			
        }
        logger.log(Level.FINE, returnlist.toString());
        return returnlist;
    }

	/* (non-Javadoc)
	 * @see de.tarent.contact.octopus.DataAccess#getHistory_tuned(int, java.util.List, java.util.Date, java.util.Date)
	 */
	public List getHistory_tuned(int adrid, List type, Date start, Date end) {
        //
        // Konstanten f�r HistoryElemente
        //
        /** Typ: Termin */
        final String TYPE_APPOINTMENT = "appointment";
        /** Typ: Versandauftrag */
        final String TYPE_MAILBATCH = "mailbatch";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	    
        Vector returnlist = new Vector();
        if(type==null || type.contains(TYPE_MAILBATCH)) {
		    //Type: MailBatch
		    String sql = "" +
		    		"SELECT pk_action, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONTITLE, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.FK_USERINIT, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ACTIONDATE, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT02, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT01, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT03, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT04, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT06, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT05,"
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT07, "
					+ TcDBContext.getSchemaName()
					+ "TACTION.ADDRESSCOUNT FROM " + 
		    		TcDBContext.getSchemaName() + "taction, " +
		    		TcDBContext.getSchemaName() + "taddressaction" +
		    		" WHERE taddressaction.fk_address = " + adrid
		    		+" AND taddressaction.fk_action = taction.pk_action" +
		    				" AND ((taction.actiondate <= date '" + sdf.format(end) + "')" +
		    						"AND (taction.actiondate >= date '" + sdf.format(start) + "'))";
		    ResultSet rs = null;
		    try {
                rs = getResultSet(sql);
                while(rs.next()) {
                    String id = rs.getString("pk_action");
//                    logger.log(Level.FINE, "Adding MailBatch PK: " + id);
                    returnlist.add(id);
                    returnlist.add(TYPE_MAILBATCH);
                    Vector payload = new Vector();
                    ResultSetMetaData rsmd = rs.getMetaData();
                    for(int j=1; j<=rsmd.getColumnCount(); j++){
                    	payload.add(rs.getString(j));
                    }
                    returnlist.add(payload);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Fehler bei der Datenbankabfrage!", e);
            } finally{
            	DB.close(rs);
            }
        }
        if(type==null || type.contains(TYPE_APPOINTMENT)) {
            //Type: Appointment
        	String sql = 
        		"SELECT TEVENT.PK, TEVENT.EVENTSTART, TEVENT.EVENTEND, TEVENT.SUBJECT, TUSER.LOGINNAME "
        		+ "FROM "
				+ TcDBContext.getSchemaName() + "TEVENT, "
				+ TcDBContext.getSchemaName() + "TADDRESSEVENT, "
				+ TcDBContext.getSchemaName() + "TUSER "
				+ "WHERE TADDRESSEVENT.FK_ADDRESS = '" + adrid + "' "
				+ "AND TEVENT.PK = TADDRESSEVENT.FK_EVENT "
				+ "AND TEVENT.FK_USERMANAGER=TUSER.PK_USER "
				+ "AND( "
				+ "(TEVENT.EVENTSTART <= DATE('"+sdf.format(start)+"') AND TEVENT.EVENTEND >=DATE('"+sdf.format(end)+"'))"
				+ " OR "
				+ "(TEVENT.EVENTSTART >= DATE('"+sdf.format(start)+"') AND TEVENT.EVENTSTART <= DATE('"+sdf.format(end)+"'))"
				+ " OR "
				+ "(TEVENT.EVENTEND >= DATE('"+sdf.format(start)+"') AND TEVENT.EVENTEND <= DATE('"+sdf.format(end)+"'))"
        		+ ")";
			logger.log(Level.FINE, sql);
			ResultSet rs = null;
		    try {
                rs = getResultSet(sql);
                while(rs.next()) {
                    String id = rs.getString("PK");
  //                  logger.log(Level.FINE, "Adding Event PK: " + id);
                    returnlist.add(id);
                    returnlist.add(TYPE_APPOINTMENT);
                    Vector payload = new Vector();
                    payload.add(rs.getDate("EVENTSTART"));
                    payload.add(rs.getDate("EVENTEND"));
                    payload.add(rs.getString("SUBJECT"));
                    payload.add(rs.getString("LOGINNAME"));
                    returnlist.add(payload);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Fehler bei der Datenbankabfrage!", e);
            } finally{
            	DB.close(rs);
            }
			
        }
        logger.log(Level.FINE, returnlist.toString());
        return returnlist;
	}
}