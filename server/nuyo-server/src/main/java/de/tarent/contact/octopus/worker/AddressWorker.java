package de.tarent.contact.octopus.worker;

import java.sql.SQLException;

import de.tarent.contact.octopus.db.TcAddressDB;
import de.tarent.contact.octopus.db.TcCategoryDB;
import de.tarent.contact.octopus.db.TcDispatchDB;
import de.tarent.contact.octopus.db.TcStaticDB;
import de.tarent.contact.octopus.worker.constants.AddressWorkerConstants;
import de.tarent.contact.util.ResultTransform;
import de.tarent.groupware.security.AddressSecurityException;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.request.TcRequest;

/**
 * @author kleinw
 * 
 * Dieser Worker liefert Methoden zum Adressenmanagement.
 */
public class AddressWorker extends BaseAddressWorker implements 
AddressWorkerConstants {

    public void getPkPreviewList(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
        Object test = ResultTransform.toSingleString(TcAddressDB
            .getAddressesPreview(tcRequest.getRequestParameters()), tcRequest.getRequestParameters());
        tcContent.setField(FIELD_PKPREVIEWLIST, test);
    }

    /**
     * 
     * @param tcConfig
     * @param tcRequest
     * @param tcContent
     * @throws InputParameterException
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    public void getAddressesPreviewByPkList(TcConfig tcConfig, 
    TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
        tcContent.setField(FIELD_ADDRESSES, ResultTransform.toSingleString(
        TcAddressDB
        .getAddressesPreviewByPkList(tcRequest.getRequestParameters()), tcRequest
        .getRequestParameters()));
    }

    public void getAddressStandardDataByPk(TcConfig tcConfig, 
    TcRequest tcRequest, TcContent tcContent)
    throws InputParameterException, SQLException {
        tcContent.setField(FIELD_ADDRESS, ResultTransform.toSingleString(
        TcAddressDB.getAddressStandardDataByPk(tcRequest.getRequestParameters(), tcConfig.getModuleConfig().getParam("letterAddressParams")), 
        tcRequest.getRequestParameters()));
    }

    /**
     * 
     * @param tcConfig
     * @param tcRequest
     * @param tcContent
     * @throws InputParameterException
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    public void getAddressProtectedDataByPk(TcConfig tcConfig, 
    TcRequest tcRequest, TcContent tcContent)
    throws InputParameterException, SQLException {
        tcContent.setField(FIELD_ADDRESS, TcAddressDB
        .getAddressProtectedData(tcRequest.getRequestParameters()));
    }

    public void getAddressesPreviewByFilter(TcConfig tcConfig, 
    TcRequest tcRequest, TcContent tcContent)
    throws InputParameterException, SQLException {
    	tcRequest.setParam("username", tcConfig.getPersonalConfig().getUserLogin());
        tcContent.setField(FIELD_PREVIEW, ResultTransform.toSingleString(
        TcAddressDB.getAddressesPreviewByFilter(tcRequest.getRequestParameters()), 
        tcRequest.getRequestParameters()));
    }

    /**
     * 
     * @param tcConfig
     * @param tcRequest
     * @param tcContent
     * @throws InputParameterException
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    public void saveAddress(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
    	if(tcContent.get("standardAddress")!=null){
    		tcRequest.setParam("standardAddress", tcContent.get("standardAddress"));
    	}
        tcContent.setField(FIELD_ADDRESS, TcAddressDB.saveAddress(tcConfig.getPersonalConfig().getUserID(), tcRequest
        .getRequestParameters()));
    }

    /**
     * 
     * @param tcConfig
     * @param tcRequest
     * @param tcContent
     * @throws InputParameterException
     * @throws SQLException
     */
    public void updateAddress(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException, AddressSecurityException {
    	if(tcContent.get("standardAddress")!=null){
    		tcRequest.setParam("standardAddress", tcContent.get("standardAddress"));
    	}
        tcContent.setField(FIELD_ADDRESS, TcAddressDB.updateAddress(tcConfig.getPersonalConfig().getUserID(), tcRequest.getRequestParameters(), new TcAll(tcRequest, tcContent, tcConfig)));
    }

    /**
     * 
     * @param tcConfig
     * @param tcRequest
     * @param tcContent
     * @throws InputParameterException
     * @throws SQLException
     * @deprecated will be removed soon to cleanup the source
     */
    
    public void deleteAddress(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
        TcAddressDB.deleteAddress(tcRequest.getRequestParameters());
    }

    
    public void getCategoriesFromUser(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
        tcContent.setField(FIELD_CATEGORIES, ResultTransform.toSingleString(
        TcCategoryDB
        .getCategoriesFromUser(tcRequest.getRequestParameters()), tcRequest
        .getRequestParameters()));
    }

    public void getEmailsByPkList(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
        tcContent.setField(FIELD_EMAIL, ResultTransform.toSingleString(
        TcDispatchDB
        .getMailsFromPkList(tcRequest.getRequestParameters()), tcRequest
        .getRequestParameters()));
    }

    public void getStaticData(TcConfig tcConfig, TcRequest tcRequest, 
    TcContent tcContent) throws InputParameterException, SQLException {
        tcContent.setField(FIELD_STATICDATA, ResultTransform.toSingleString(TcStaticDB
            .getStaticData(tcRequest.getRequestParameters()), tcRequest.getRequestParameters()));
    }
    
    
    public void getAssociatedCategoriesByAddressPk(TcConfig tcConfig, TcRequest tcRequest, 
    	    TcContent tcContent) throws InputParameterException, SQLException{
    	tcContent.setField(FIELD_ASSOCIATEDCATEGORIES, TcAddressDB.getAssociatedCategoriesByAddressPk(tcRequest.getRequestParameters(), tcContent.getContent()));
    }
}
