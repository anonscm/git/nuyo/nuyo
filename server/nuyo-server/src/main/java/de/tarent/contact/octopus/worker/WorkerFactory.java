/*
 * $Id: WorkerFactory.java,v 1.3 2007/06/15 15:58:29 fkoester Exp $
 * 
 * Created on 17.05.2005
 */
package de.tarent.contact.octopus.worker;

import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.groupware.security.SecurityWorker;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcContentWorkerFactory;
import de.tarent.octopus.content.TcReflectedWorkerWrapper;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.server.WorkerCreationException;

/**
 * Diese Klasse gibt Instanzen anderer Worker zum direktem Arbeiten mit
 * diesen zur�ck. Dadurch verlaggert sich die Verwendung von statischen
 * Methoden ausschlie�lich in diese Klasse.
 * 
 * @author Christoph
 */
public class WorkerFactory {
    
    private static Logger logger = Logger.getLogger(WorkerFactory.class.getName());
    
	private WorkerFactory() {
	}

    /**
     * Diese Methode liefert den Worker namens "SecurityWorker".
     * 
     * @param cntx Octopus-Kontext
     * @return {@link SecurityWorker}-Instanz des Moduls
     */
	static public SecurityWorker getSecurityWorker(OctopusContext cntx) {
		return (SecurityWorker)getWorker(cntx, "SecurityWorker");
	}

    /**
     * Diese statische Methode holt mittels des Octopus-Kontexts einen Worker. 
     * 
     * @param cntx Octopus-Kontext
     * @param name Worker-Name
     * @return Worker-Instanz
     */
	static private Object getWorker(OctopusContext cntx, String name) {
        TcContentWorker worker;
        try {
            worker = TcContentWorkerFactory.getContentWorker(
                    cntx.moduleConfig(), name,
                    cntx.getRequestObject().getRequestID());
            return (worker instanceof TcReflectedWorkerWrapper) ?
                    ((TcReflectedWorkerWrapper)worker).getWorkerDelegate() : worker;
        } catch (WorkerCreationException e) {
            logger.log(Level.SEVERE, "Fehler beim Erstellen der Worker-Instanz", e);
        }
        return null;
	}
}
