/*
 * $Id: BaseAddressWorker.java,v 1.2 2006/03/16 13:49:30 jens Exp $
 * 
 * Created on 27.10.2003
 */
package de.tarent.contact.octopus.worker;

import java.util.logging.Logger;

import de.tarent.contact.octopus.logging.Lg;
import de.tarent.contact.octopus.worker.constants.AddressWorkerConstants;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcAbstractContentWorker;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcMessageDefinition;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * Dieser Worker liefert Daten zu Adressen.
 * 
 * @author mikel
 */
public class BaseAddressWorker extends TcAbstractContentWorker implements
    AddressWorkerConstants {

    public final static Logger logger = Logger
        .getLogger(BaseAddressWorker.class.getName());

    public BaseAddressWorker() {
        super();
    }

    public void init(TcModuleConfig config) {
    }

    public TcPortDefinition getWorkerDefinition() {
        TcPortDefinition port = new TcPortDefinition(
            "de.tarent.contact.octopus.AddressFetcher",
            "Worker zum Beziehen von Addressdaten.");

        TcOperationDefinition operation = port.addOperation(ACTION_GET_ADDRESS,
            "Liefert die Daten einer Adresse.");
        operation.setInputMessage().addPart(PARAM_ADDRESS_ID,
            TcMessageDefinition.TYPE_SCALAR, "ID der Adresse").addPart(
            PARAM_DETAIL, TcMessageDefinition.TYPE_SCALAR,
            "Bit-Maske der gewŁnschten Detaildaten der Adresse");
        operation.setOutputMessage().addPart(FIELD_ADDRESS,
            TcMessageDefinition.TYPE_STRUCT,
            "Map der zu der Id gefundenen Daten");
        operation.addFaultMessage(MSG_ERROR,
            "Fehler beim Zugriff auf die Adressdaten.");

        operation = port.addOperation(ACTION_GET_ADDRESSES,
            "Liefert die Daten mehrerer Adressen.");
        operation.setInputMessage().addPart(PARAM_ADDRESS_IDS,
            TcMessageDefinition.TYPE_ARRAY, "IDs der Adressen").addPart(
            PARAM_DETAIL, TcMessageDefinition.TYPE_SCALAR,
            "Bit-Maske der gewŁnschten Detaildaten der Adresse");
        operation.setOutputMessage().addPart(FIELD_ADDRESSES,
            TcMessageDefinition.TYPE_STRUCT,
            "Map von Maps zu jeder Id mit den gefundenen Daten");
        operation.addFaultMessage(MSG_ERROR,
            "Fehler beim Zugriff auf die Adressdaten.");

        return port;
    }

    public String getVersion() {
        return CVS.getContent("$Revision: 1.2 $") + " ("
            + CVS.getContent("$Date: 2006/03/16 13:49:30 $") + ')';
    }

    static public class InputParameterException extends Exception {

        public InputParameterException(String msg) {
            super(msg);
        }

        public InputParameterException(String msg, Exception e) {
            super(msg, e);
        }
    }

    public String doAction(TcConfig arg0, String arg1, TcRequest arg2,
        TcContent arg3) {
        StringBuffer sb = new StringBuffer().append("Action :"  + arg1);
        if (arg2 != null) {
            if (arg2.getRequestParameters() != null)
                sb.append("\nParameter : ").append(arg2.getRequestParameters());
        }
        Lg.WORKER(arg0.getModuleConfig().getName()).debug(sb.toString());
        try {
            return super.doAction(arg0, arg1, arg2, arg3);
        }
        catch (TcContentProzessException e) {
            Lg.WORKER(arg0.getModuleConfig().getName()).debug(e.getMessage(), e);
        }
        return "";
    }
}
