package de.tarent.contact.octopus.worker.constants;


/**
 * @author kleinw
 *
 *	Konstanten f�r den Mailworker.
 *
 */
public interface MailWorkerConstants {

	public final static String ACTION_MAILSEND = "mailSend";

	public final static String FIELD_MAILSEND = "mailsend";

}
