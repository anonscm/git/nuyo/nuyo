/*
 * Created on 29.08.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.tarent.contact.octopus;

import de.tarent.groupware.Address;

/**
 * @author nils
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LetterSalutationGenerator {

	public static String generateLetterSalutation(Address address){
		return generateLetterSalutation(address.getAnrede(), address.getVorname(),  address.getNachname(), address.getHerrnFrau(), address.getAkadTitel(), address.getTitel());
	}

    public static String generateLetterSalutation(String anredekurz, String vorname, String nachname, String herrfrau, String akad_titel, String titel){

    	StringBuffer anrede = new StringBuffer("");
	  	
	  	if (anredekurz != null && !("").equals(anredekurz)){
	  		anrede.append(anredekurz);
	  		if ("Sehr geehrte Damen und Herren".equals(anredekurz)){
	  			return anrede.toString();
	  		}
	  	}
	  		
	  	// Falls die Kurzanrede "Lieber" oder "Liebe" ist, wird nur noch der Nachname genommen (kein Titel etc.) 
	  	if (vorname != null && !("").equals(vorname) && anredekurz != null && (anredekurz.equals("Lieber") ||  anredekurz.equals("Liebe"))){
	  		anrede.append(" " + vorname);
	  		
	  	}
	  	
	  	else {
		  		  	
		  	if (herrfrau != null && !("").equals(herrfrau))
		  		anrede.append(" " + herrfrau);
		  	
	  		if (akad_titel != null && !("").equals(akad_titel))
	  			anrede.append(" " + akad_titel);  		
	  		
	  		if (titel != null && !("").equals(titel))
	  			anrede.append(" " + titel);
	  		
	  		if (nachname != null && !("").equals(nachname))
	  			anrede.append(" " + nachname);
	  	
	  	}	 		
	  	
	  	//anrede.append(",");  	
	  	return anrede.toString();   
    }
}
