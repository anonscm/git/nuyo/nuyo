package de.tarent.contact.octopus.worker.constants;


/**
 * @author kleinw
 *
 *	Hier stehen alle f�r den AddressWorker ben�tigten
 *	Konstanten.
 *
 */
public interface AddressWorkerConstants extends CommonConstants {
    
    public final static String ACTION_GET_ADDRESS = "getAddress";
	public final static String ACTION_GET_ADDRESSES = "getAddresses";
	public final static String ACTION_GETADDRESSSTANDARDDATA = "getAddressStandardDataByPk";
	public final static String ACTION_GETADDRESSEXTENDEDDATA = "getAddressExtendedDataByPk";
	public final static String ACTION_GETADDRESSPROTECTEDDATA = "getAddressProtectedDataByPk";
	public final static String ACTION_GETADDRESSEMAILDATA = "getAddressEmailDataByPk";
	public final static String ACTION_GETADDRESSESPREVIEW_BY_FILTER = "getAddressesPreviewByFilter";
	public final static String ACTION_SAVE_ADDRESS = "saveAddress";
	public final static String ACTION_UPDATE_ADDRESS = "updateAddress";
	public final static String ACTION_GETADDRESSESBYPKLIST = "getPreviewByPkList";
	public final static String ACTION_DELETE_ADDRESS = "deleteAddress";
	public final static String ACTION_GETEXTENDEDDATA = "getExtendedDataByPk";
	public final static String ACTION_GETPKLISTPREVIEW = "getPkPreviewList";
	public final static String ACTION_CLOSECONNECTIONBUFFER = "closeConnectionBuffer";
	public final static String ACTION_DELETEADDRESS = "deleteAddress";
	public final static String ACTION_GETSTATICDATA = "getStaticData";
	
	public final static String PARAM_ADDRESS_ID = "addressId";
	public final static String PARAM_DETAIL = "detail";
	public final static String PARAM_ADDRESS_IDS = "addressIds";
	public final static String PARAM_CATEGORY_ID = "categoryId";
	public final static String PARAM_SUBCATEGORIES_IDS = "subcategoryId";
	public final static String PARAM_USERID = "userId";
	public final static String PARAM_UNION = "union";
	public final static String PARAM_ADDRESSPK = "addressPk";
	public final static String PARAM_ADDRESS = "address";
	public final static String PARAM_CATEGORY = "category";
	public final static String PARAM_SUBCATEGORIES = "subCategories";
	public final static String PARAM_GENERAL_CATEGORY = "generalCategory";
	public final static String PARAM_INTERSECTION = "intersection";
	public final static String PARAM_COMMVALUES = "commValues";
	public final static String PARAM_PKLIST = "pklist";
	public final static String PARAM_SINGLESTRINGRESULT = "singleStringResult";
	
	public final static String FIELD_ADDRESS = "address";
	public final static String FIELD_PREVIEW = "preview";
	public final static String FIELD_ADDRESS_PKS = "pks";
	public final static String FIELD_DETAILS = "details";
	public final static String FIELD_EXTENDED = "extended";
	public final static String FIELD_CATEGORIES = "categories";
	public final static String FIELD_SUBCATEGORIES = "subCategories";
	public final static String FIELD_ADDRESSES = "addresses";
	public final static String FIELD_PKPREVIEWLIST = "pkPreviewList";
	public final static String FIELD_EMAIL = "email";
	public final static String FIELD_STATICDATA = "staticdata";
	public final static String FIELD_ASSOCIATEDCATEGORIES = "associatedCategories";
    
}
