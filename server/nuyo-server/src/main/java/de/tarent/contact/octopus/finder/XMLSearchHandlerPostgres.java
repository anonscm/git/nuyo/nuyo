/* $Id: XMLSearchHandlerPostgres.java,v 1.6 2007/06/15 15:58:30 fkoester Exp $
 * 
 * Created on 30.06.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Hendrik Helwich and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.finder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import de.tarent.contact.octopus.db.TcDBContext;

/**
 * @author mikel
 */
public class XMLSearchHandlerPostgres extends DefaultHandler {
    /**
     * Dieser Logger soll f�r Ausgaben im Octopus-Kontext benutzt werden.
     */
    public final static Logger logger = Logger.getLogger(XMLSearchHandlerPostgres.class.getName());

    private LinkedList SQLFieldList = new LinkedList();

    private AtomStack stack;
    //spezielle Felder
    private SearchField VGroup;
    private SearchField Verteiler;
    private SearchField PostOrderId;
    private SearchField PostOrderChannel;
    private SearchField AdressNr;

    private SAXParser parser = null;
    private String logonname;


    String verifiedUserId;
    public boolean valid = false;

    public XMLSearchHandlerPostgres(String query, String verifiedUserId) {
        this.verifiedUserId = verifiedUserId;

        if (!query.startsWith("<?xml "))
            query = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" + query;
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            factory.setFeature("http://xml.org/sax/features/namespaces", true);
            parser = factory.newSAXParser();
        } catch (SAXException e) {
            logger.log(Level.WARNING, "Fehler bei der SAX-Parser konfiguration", e);
        } catch (ParserConfigurationException e) {
            logger.log(Level.WARNING, "Fehler bei der SAX-Parser konfiguration", e);
        }

        stack = new AtomStack();
        VGroup = null;
        //parser.setContentHandler(this);
        InputSource insrc = new InputSource(new ByteArrayInputStream(query.getBytes()));
        try {
            parser.parse(insrc, this);
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO-Fehler beim Parsen", e);
        } catch (SAXException se) {
            logger.log(Level.WARNING, "SAX-Fehler beim Parsen", se);
        }
        //System.out.println(stack.top());
        //falls nur ein Suchfeld eingelesen wurde, wird es zur Wurzel
        if (!stack.isEmpty())
            if (stack.top().getRight() == null)
                stack.push(stack.top().getLeft());
        //

        // Diverse Tests
        if (PostOrderId == null & AdressNr == null) {
            // Kategorienzugriff testen
            if (VGroup != null) {
                //falls die Beschreibung der Verteilergruppe angegegeben wurde, wird diese entfernt (besser im Client machen)
                String str = VGroup.getSearchString();
                if (str.indexOf(' ') != -1)
                    VGroup.setSearchString(str.substring(0, str.indexOf(' ')));
                valid = true;
            } else {
                // Keine kategorie angegeben => Suche in allen Adressen
                valid = true;
            }
            // Kategorienauswahl-Suchatom erstellen
            SearchAtom atom = null;
            if (Verteiler != null) {
                Verteiler.setName("TADDRESSSUBFOLDER.FK_SUBFOLDER");
                Verteiler.setOperation(SearchField.IN);
                atom = new SearchOperator(SearchOperator.AND);
                atom.addChild(VGroup);
                atom.addChild(Verteiler);
            } else
                atom = VGroup;

            if (atom != null) {
                // Suchatom auf den Stack legen.
                if (stack.isEmpty())
                    stack.push(atom);
                else {
                    SearchOperator op = new SearchOperator(SearchOperator.AND);
                    op.addChild(stack.top());
                    op.addChild(atom);
                    stack.push(op);
                }
            }
        } else
            valid = true;

        if (PostOrderId != null)
        	SQLFieldList.add(fields.get(REQUEST_FIELD_VERSANDART));
        
        SQLFieldList.add(fields.get(REQUEST_FIELD_ANREDE));
        SQLFieldList.add(fields.get(REQUEST_FIELD_LETTERSALUTATIONAUTO));
        SQLFieldList.add(fields.get(REQUEST_FIELD_LETTERADDRESSAUTO));
        SQLFieldList.add(fields.get(REQUEST_FIELD_LETTERADDRESS));
        SQLFieldList.add(fields.get(REQUEST_FIELD_KURZANREDE));
        SQLFieldList.add(fields.get(REQUEST_FIELD_ABTEILUNG));
        SQLFieldList.add(fields.get(REQUEST_FIELD_POSITION));
        SQLFieldList.add(fields.get(REQUEST_FIELD_NACHNAME));
        SQLFieldList.add(fields.get(REQUEST_FIELD_VORNAME));
        SQLFieldList.add(fields.get(REQUEST_FIELD_INSTITUTION));
        SQLFieldList.add(fields.get(REQUEST_FIELD_EMAIL));
        SQLFieldList.add(fields.get(REQUEST_FIELD_TEL_DIENST));
        SQLFieldList.add(fields.get(REQUEST_FIELD_FAX_DIENST));
        SQLFieldList.add(fields.get(REQUEST_FIELD_HANDY_DIENST));
        SQLFieldList.add(fields.get(REQUEST_FIELD_TEL_PRIVAT));
        SQLFieldList.add(fields.get(REQUEST_FIELD_FAX_PRIVAT));
        SQLFieldList.add(fields.get(REQUEST_FIELD_HANDY_PRIVAT));
        SQLFieldList.add(fields.get(REQUEST_FIELD_PLZ));
        SQLFieldList.add(fields.get(REQUEST_FIELD_ORT));
        SQLFieldList.add(fields.get(REQUEST_FIELD_STRASSE));
        SQLFieldList.add(fields.get(REQUEST_FIELD_HAUSNR));
        SQLFieldList.add(fields.get(REQUEST_FIELD_NAMENSZUSATZ));
        SQLFieldList.add(fields.get(REQUEST_FIELD_TITEL));
        SQLFieldList.add(fields.get(REQUEST_FIELD_AKAD_TITEL));
        SQLFieldList.add(fields.get(REQUEST_FIELD_POSTFACHNR));
        SQLFieldList.add(fields.get(REQUEST_FIELD_PLZ_POSTFACH));
        SQLFieldList.add(fields.get(REQUEST_FIELD_ADRNR));
        SQLFieldList.add(fields.get(REQUEST_FIELD_LAND));
        SQLFieldList.add(fields.get(REQUEST_FIELD_LKZ));
        SQLFieldList.add(fields.get(REQUEST_FIELD_HERRNFRAU));
    }

    public final static String REQUEST_FIELD_ANREDE = "Anrede";
    public final static String REQUEST_FIELD_LETTERSALUTATIONAUTO = "Breifanrede_auto";
    public final static String REQUEST_FIELD_LETTERADDRESSAUTO = "Briefadresse_auto";
    public final static String REQUEST_FIELD_LETTERADDRESS = "Briefadresse";
    public final static String REQUEST_FIELD_KURZANREDE = "Kurzanrede";
    public final static String REQUEST_FIELD_ABTEILUNG = "Abteilung";
    public final static String REQUEST_FIELD_POSITION = "Position";
    public final static String REQUEST_FIELD_VERSANDART = "Versandart";
    public final static String REQUEST_FIELD_NACHNAME = "Nachname";
    public final static String REQUEST_FIELD_VORNAME = "Vorname";
    public final static String REQUEST_FIELD_INSTITUTION = "Institution";
    public final static String REQUEST_FIELD_EMAIL = "E_Mail_Adresse";
    public final static String REQUEST_FIELD_TEL_DIENST = "Telefon_Dienstlich";
    public final static String REQUEST_FIELD_FAX_DIENST = "Fax_Dienstlich";
    public final static String REQUEST_FIELD_HANDY_DIENST = "Handy_Dienstlich";
    public final static String REQUEST_FIELD_TEL_PRIVAT = "Telefon_Privat";
    public final static String REQUEST_FIELD_FAX_PRIVAT = "Fax_Privat";
    public final static String REQUEST_FIELD_HANDY_PRIVAT = "Handy_Privat";
    public final static String REQUEST_FIELD_PLZ = "PLZ";
    public final static String REQUEST_FIELD_ORT = "Ort";
    public final static String REQUEST_FIELD_STRASSE = "Strasse";
    public final static String REQUEST_FIELD_HAUSNR = "Haus_Nr";
    public final static String REQUEST_FIELD_NAMENSZUSATZ = "Namenszusatz";
    public final static String REQUEST_FIELD_TITEL = "Titel";
    public final static String REQUEST_FIELD_AKAD_TITEL = "Akad_Titel";
    public final static String REQUEST_FIELD_POSTFACHNR = "Postfach_Nr";
    public final static String REQUEST_FIELD_PLZ_POSTFACH = "PLZ_Postfach";
    public final static String REQUEST_FIELD_ADRNR = "Adr_nr";
    public final static String REQUEST_FIELD_LAND = "Land";
    public final static String REQUEST_FIELD_LKZ = "LKZ";
    public final static String REQUEST_FIELD_HERRNFRAU = "Herrn_Frau";
    public final static String REQUEST_FIELD_MATCHCODENACHNAME = "matchcodenachname";
    public final static String REQUEST_FIELD_MATCHCODEVORNAME = "matchcodevorname";
    public final static String REQUEST_FIELD_MATCHCODEINSTITUTION = "matchcodeinstitution";
    public final static String REQUEST_FIELD_MATCHCODESTRASSE = "matchcodestrasse";
    public final static String REQUEST_FIELD_MATCHCODEORT = "matchcodeort";

    public final static String TABLE_ADDRESSES = "TADDRESS";
    public final static String TABLE_ADDRESSES_EXT = "TADDRESSEXT";
    public final static String TABLE_VERSAND = "TADDRESSACTION";

    /**
     * statische Tabelle von Informationen zu verf�gbaren Feldern
     */
    static Map fields = new HashMap();
    static {
        fields.put(REQUEST_FIELD_LETTERADDRESS, new FieldDescriptor(REQUEST_FIELD_LETTERADDRESS, TABLE_ADDRESSES, "LETTERADDRESS"));
        fields.put(REQUEST_FIELD_LETTERADDRESSAUTO, new FieldDescriptor(REQUEST_FIELD_LETTERADDRESSAUTO, TABLE_ADDRESSES, "LETTERADDRESS_AUTO"));

        fields.put(REQUEST_FIELD_ANREDE,new FieldDescriptor(REQUEST_FIELD_ANREDE, TABLE_ADDRESSES, "LETTERSALUTATION"));
        fields.put(REQUEST_FIELD_LETTERSALUTATIONAUTO, new FieldDescriptor(REQUEST_FIELD_LETTERSALUTATIONAUTO, TABLE_ADDRESSES, "LETTERSALUTATION_AUTO"));

        fields.put(REQUEST_FIELD_KURZANREDE, new FieldDescriptor(REQUEST_FIELD_KURZANREDE, TABLE_ADDRESSES, "LETTERSALUTATIONSHORT"));
        fields.put(REQUEST_FIELD_POSITION, new FieldDescriptor(REQUEST_FIELD_POSITION, TABLE_ADDRESSES, "POSITION"));
        fields.put(REQUEST_FIELD_ABTEILUNG, new FieldDescriptor(REQUEST_FIELD_ABTEILUNG, TABLE_ADDRESSES, "DEPARTMENT"));

        fields.put(REQUEST_FIELD_VERSANDART, new FieldDescriptor(REQUEST_FIELD_VERSANDART, TABLE_VERSAND, "NOTE"));
        fields.put(REQUEST_FIELD_NACHNAME, new FieldDescriptor(REQUEST_FIELD_NACHNAME, TABLE_ADDRESSES, "LASTNAME"));
        fields.put(REQUEST_FIELD_VORNAME, new FieldDescriptor(REQUEST_FIELD_VORNAME, TABLE_ADDRESSES, "FIRSTNAME"));
        fields.put(REQUEST_FIELD_INSTITUTION, new FieldDescriptor(REQUEST_FIELD_INSTITUTION, TABLE_ADDRESSES, "ORGANISATION"));
        //          Kommunikationsdaten aus TCOMM (1) oder aus TADRESSEXT (2) 
        //         fields.put(REQUEST_FIELD_EMAIL, new FieldDescriptor(REQUEST_FIELD_EMAIL, "EMAIL", "VALUE"));
        //         fields.put(REQUEST_FIELD_TEL_DIENST, new FieldDescriptor(REQUEST_FIELD_TEL_DIENST, "TELD", "VALUE"));
        //         fields.put(REQUEST_FIELD_FAX_DIENST, new FieldDescriptor(REQUEST_FIELD_FAX_DIENST, "FAXD", "VALUE"));
        //         fields.put(REQUEST_FIELD_HANDY_DIENST, new FieldDescriptor(REQUEST_FIELD_HANDY_DIENST, "MOBD", "VALUE"));
        //         fields.put(REQUEST_FIELD_TEL_PRIVAT, new FieldDescriptor(REQUEST_FIELD_TEL_PRIVAT, "TELP", "VALUE"));
        //         fields.put(REQUEST_FIELD_FAX_PRIVAT, new FieldDescriptor(REQUEST_FIELD_FAX_PRIVAT, "FAXP", "VALUE"));
        //         fields.put(REQUEST_FIELD_HANDY_PRIVAT, new FieldDescriptor(REQUEST_FIELD_HANDY_PRIVAT, "MOBP", "VALUE"));
        
        fields.put(REQUEST_FIELD_EMAIL, new FieldDescriptor(REQUEST_FIELD_EMAIL, TABLE_ADDRESSES_EXT, "EMAIL"));
        fields.put(REQUEST_FIELD_TEL_DIENST, new FieldDescriptor(REQUEST_FIELD_TEL_DIENST, TABLE_ADDRESSES_EXT, "FON"));
        fields.put(REQUEST_FIELD_FAX_DIENST, new FieldDescriptor(REQUEST_FIELD_FAX_DIENST, TABLE_ADDRESSES_EXT, "FAX"));
        fields.put(REQUEST_FIELD_HANDY_DIENST, new FieldDescriptor(REQUEST_FIELD_HANDY_DIENST, TABLE_ADDRESSES_EXT, "MOBILE"));
        fields.put(REQUEST_FIELD_TEL_PRIVAT, new FieldDescriptor(REQUEST_FIELD_TEL_PRIVAT, TABLE_ADDRESSES_EXT, "FON_HOME"));
        fields.put(REQUEST_FIELD_FAX_PRIVAT, new FieldDescriptor(REQUEST_FIELD_FAX_PRIVAT, TABLE_ADDRESSES_EXT, "FAX_HOME"));
        fields.put(REQUEST_FIELD_HANDY_PRIVAT, new FieldDescriptor(REQUEST_FIELD_HANDY_PRIVAT, TABLE_ADDRESSES_EXT, "MOBILE_HOME"));

        fields.put(REQUEST_FIELD_PLZ, new FieldDescriptor(REQUEST_FIELD_PLZ, TABLE_ADDRESSES, "ZIPCODE"));
        fields.put(REQUEST_FIELD_ORT, new FieldDescriptor(REQUEST_FIELD_ORT, TABLE_ADDRESSES, "CITY"));
        fields.put(REQUEST_FIELD_STRASSE, new FieldDescriptor(REQUEST_FIELD_STRASSE, TABLE_ADDRESSES, "STREET"));
        fields.put(REQUEST_FIELD_HAUSNR, new FieldDescriptor(REQUEST_FIELD_HAUSNR, TABLE_ADDRESSES, "HOUSENO"));
        fields.put(REQUEST_FIELD_NAMENSZUSATZ, new FieldDescriptor(REQUEST_FIELD_NAMENSZUSATZ, TABLE_ADDRESSES, "NAMESUFFIX"));
        fields.put(REQUEST_FIELD_TITEL, new FieldDescriptor(REQUEST_FIELD_TITEL, TABLE_ADDRESSES, "TITELJOB"));
        fields.put(REQUEST_FIELD_AKAD_TITEL, new FieldDescriptor(REQUEST_FIELD_AKAD_TITEL, TABLE_ADDRESSES, "TITELACADEMIC"));
        fields.put(REQUEST_FIELD_POSTFACHNR, new FieldDescriptor(REQUEST_FIELD_POSTFACHNR, TABLE_ADDRESSES, "POBOX"));
        fields.put(REQUEST_FIELD_PLZ_POSTFACH, new FieldDescriptor(REQUEST_FIELD_PLZ_POSTFACH, TABLE_ADDRESSES, "POBOXZIPCODE"));
        fields.put(REQUEST_FIELD_ADRNR, new FieldDescriptor(REQUEST_FIELD_ADRNR, TABLE_ADDRESSES, "PK_ADDRESS"));
        fields.put(REQUEST_FIELD_LAND, new FieldDescriptor(REQUEST_FIELD_LAND, TABLE_ADDRESSES, "COUNTRY"));
        fields.put(REQUEST_FIELD_LKZ, new FieldDescriptor(REQUEST_FIELD_LKZ, TABLE_ADDRESSES, "COUNTRYSHORT"));
        fields.put(REQUEST_FIELD_HERRNFRAU, new FieldDescriptor(REQUEST_FIELD_HERRNFRAU, TABLE_ADDRESSES, "SALUTATION"));
        //Matchcode Fields
        fields.put(REQUEST_FIELD_MATCHCODENACHNAME, new FieldDescriptor(REQUEST_FIELD_MATCHCODENACHNAME, TABLE_ADDRESSES_EXT, "LASTNAME_M"));
        fields.put(REQUEST_FIELD_MATCHCODEVORNAME, new FieldDescriptor(REQUEST_FIELD_MATCHCODEVORNAME, TABLE_ADDRESSES_EXT, "FIRSTNAME_M"));
        fields.put(REQUEST_FIELD_MATCHCODEINSTITUTION, new FieldDescriptor(REQUEST_FIELD_MATCHCODEINSTITUTION, TABLE_ADDRESSES_EXT, "ORGANISATION_M"));
        fields.put(REQUEST_FIELD_MATCHCODESTRASSE, new FieldDescriptor(REQUEST_FIELD_MATCHCODESTRASSE, TABLE_ADDRESSES_EXT, "STREET_M"));
        fields.put(REQUEST_FIELD_MATCHCODEORT, new FieldDescriptor(REQUEST_FIELD_MATCHCODEORT, TABLE_ADDRESSES_EXT, "CITY_M"));  
    }

    /**
     * Diese Klasse beschreibt ein Feld mit seinem Titel, �ber den es
     * angefordert wird, und seine Tabelle und seinen Feldnamen dort.
     */
    public static class FieldDescriptor {
        public FieldDescriptor(String theTitle, String theTable, String theName) {
            title = theTitle;
            table = TcDBContext.getSchemaName() + theTable;
            name = theName;
        }

        public String getName() {
            return name;
        }
        public String getTable() {
            return table;
        }
        public String getTitle() {
            return title;
        }

        public String getSelect() {
            return (table != null) ? table + '.' + name : '\'' + name + '\'';
        }

        private String title;
        private String table;
        private String name;
    }

    //Stack wird beim Parsen und Erstellen des Suchbaums ben�tigt.
    //oberstes Element ist immer der aktuelle Knoten.
    //Nach dem Parsen steht der Root-Node oben im Stack
    //MAXITEMS muss die Anzahl der maximal abfragbaren Suchfelder enthalten
    private class AtomStack {
        private int MAXITEMS = 256;
        private SearchAtom stack[] = new SearchAtom[MAXITEMS];
        private int top = 0;

        public void push(SearchAtom atom) {
            if (top == MAXITEMS)
                throw new RuntimeException("Stack ist voll");
            stack[top++] = atom;
        }

        public SearchAtom pop() {
            if (isEmpty())
                throw new RuntimeException("Stack ist leer");
            return stack[--top];
        }

        public SearchAtom top() {
            if (isEmpty())
                throw new RuntimeException("Stack ist leer");
            return stack[top - 1];
        }

        public boolean isEmpty() {
            return top == 0;
        }
    }


    public List getSQLFieldList() {
        return SQLFieldList;
    }

    public String getSQLSelect() {
        StringBuffer sb = new StringBuffer("SELECT ");
        Iterator it = SQLFieldList.iterator();
        while (it.hasNext()) {
            FieldDescriptor field = (FieldDescriptor) it.next();
            sb.append(field.getSelect());
            sb.append(it.hasNext() ? ", " : " ");
        }
        return sb.toString();
    }

    private static final String SQL_BASE =
        "FROM "+TcDBContext.getSchemaName()+"V_USER_ADDRESS INNER JOIN "+TcDBContext.getSchemaName()+"TADDRESS ON "+TcDBContext.getSchemaName()+"V_USER_ADDRESS.FK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS  LEFT JOIN "+TcDBContext.getSchemaName()+"TADDRESSEXT ON "+TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESSEXT.PK_ADDRESSEXT ";
    private static final String SQL_BATCH =
        SQL_BASE + "INNER JOIN "+TcDBContext.getSchemaName()+"TADDRESSACTION ON "+TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESSACTION.FK_ADDRESS ";
    private static final String SQL_CATEGORY =
        SQL_BASE + "INNER JOIN "+TcDBContext.getSchemaName()+"TADDRESSFOLDER ON "+TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESSFOLDER.FK_ADDRESS ";
    private static final String SQL_SUBCATEGORY =
        SQL_CATEGORY + "INNER JOIN "+TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER ON "+TcDBContext.getSchemaName()+"TADDRESS.PK_ADDRESS = "+TcDBContext.getSchemaName()+"TADDRESSSUBFOLDER.FK_ADDRESS ";

    public String getSQLFrom() {
        if (PostOrderId != null)
            return SQL_BATCH;
        else if (AdressNr != null)
            return SQL_BASE;
        else if (Verteiler != null)
            return SQL_SUBCATEGORY;
        else
            return SQL_CATEGORY;
    }

    public String getSQLWhere() {
        String out = "WHERE V_USER_ADDRESS.userid = '"+verifiedUserId+"'";
        if (!valid)
            out += " AND 1=0";
        if (PostOrderId != null)
            out += " AND TADDRESSACTION.FK_ACTION = "
                + PostOrderId.getSearchString()
                + " AND UPPER(TADDRESSACTION.NOTE) = '"
                + PostOrderChannel.getSearchString().toUpperCase()
                + '\'';
        else if (AdressNr != null)
            out += " AND TADDRESS.PK_ADDRESS = " + AdressNr.getSearchString();
        else {
            if (!stack.isEmpty()) {
                out += " AND "+ stack.top();
            }
        }
        return out;
    }

    /*
     * Schnittstelle ContentHandler
     */
    /**
     * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
    public void startElement(String uri, String localpart, String rawname, Attributes attributes) throws SAXException {
    	if (rawname.equals("sf")) {
            String fieldName = attributes.getValue("n");
            SearchField sf = new SearchField(fieldName);
            if (attributes.getValue("o") != null)
                sf.setOperation(attributes.getValue("o"));
            if (sf.getName().equals("VGRUPPE")) {
                VGroup = sf;
                VGroup.setName("TADDRESSFOLDER.FK_FOLDER");
            } else if (sf.getName().equals("VERTEILER")) {
                Verteiler = sf;
            } else if (
                sf.getName().equals("ORDERID")
                    || sf.getName().equals("ORDER_ID")
                    || sf.getName().equals("ORDERNR")
                    || sf.getName().equals("ORDER_NR")) {
                PostOrderId = sf;
            } else if (sf.getName().equals("ORDERCHANNEL") || sf.getName().equals("ORDER_CHANNEL")) {
                PostOrderChannel = sf;
            } else if (sf.getName().equals("ADR_NR") || sf.getName().equals("ADRNR")) {
                AdressNr = sf;
            } else {
                if (fieldName != null && fields.containsKey(fieldName))
                    sf.setName(((FieldDescriptor) fields.get(fieldName)).getSelect());
                if (stack.isEmpty()) {
                    String operand = attributes.getValue("t");
                    if (operand == null || "0".equals(operand))
                        stack.push(new SearchOperator(SearchOperator.AND));
                    else
                        stack.push(new SearchOperator(SearchOperator.OR));
                }
                stack.top().addChild(sf);
            }
            stack.push(sf);
        }
    }

    /**
     * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    public void endElement(String uri, String localpart, String rawname) throws SAXException {
        if (rawname.equals("sf"))
            stack.pop();
    }

    /**
     * @see org.xml.sax.ContentHandler#characters(char[], int, int)
     */
    public void characters(char[] ch, int offset, int length) throws SAXException {
        if (!stack.top().isOperator())
             ((SearchField) stack.top()).setSearchString(new String(ch, offset, length));
    }
}
