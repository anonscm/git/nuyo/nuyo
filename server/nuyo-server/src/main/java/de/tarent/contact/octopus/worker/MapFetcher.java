/* $Id: MapFetcher.java,v 1.14 2007/06/15 15:58:29 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.worker;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tarent.contact.octopus.DBDataAccessPostgres;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.db.queries.Queries;
import de.tarent.contact.octopus.worker.constants.MapFetcherConstants;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.security.TcSecurityException;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.util.CVS;

/**
 *
 * Dieser Worker soll die Funktionalit�t des alten (fast unwartbaren) Mapfetchers ersetzen. 
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class MapFetcher implements MapFetcherConstants{
	/* Alte DataAccess-Klasse, benutzt keine Pools!*/
	private static DBDataAccessPostgres dataAccess = null;
	
	/* (non-Javadoc)
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 * FIXME: dataAccess Generierung richtig machen...
	 */
	public void init(TcModuleConfig config) {
		if(dataAccess==null){
			dataAccess = new DBDataAccessPostgres();
		}
	}
	
	
	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.14 $")
			+ " ("
			+ CVS.getContent("$Date: 2007/06/15 15:58:29 $")
			+ ')';
	}
	
	/*Definition der Action "createVersandAuftrag"*/
	public String[] INPUT_createVersandAuftrag = {};
	public boolean[] MANDATORY_createVersandAuftrag = {};
	public String OUTPUT_createVersandAuftrag = FIELD_CreateVersandAuftrag;
	/**
	 * Erstellt einen Versandauftrag.
	 * 
	 * @param oc der Octopus-Context
	 * @return Map, die den neu angelegten Versandauftrag beschreibt
	 */
	public Map createVersandAuftrag(OctopusContext oc){
		Map params = new HashMap(oc.getRequestObject().getRequestParameters());
		params.putAll(oc.getContentObject().getContent());
		return dataAccess.createVersandAuftrag(params);
	}
	
	/* Definition der Action "createVerteilergruppe" */
	public String[] INPUT_createVerteilergruppe = { "name", "key", "description"};
	public boolean[] MANDATORY_createVerteilergruppe = { true, true, true};
	public String OUTPUT_createVerteilergruppe = FIELD_CreateVerteilergruppe;
	/**
	 * Erzeugt eine neue Kategorie
	 * @param oc der Octopus-Context
	 * @param name Name der Kategorie
	 * @param key Schl�ssel der Kategorie
	 * @param description Beschreibung der Kategorie
	 * @return Map mit Daten der neuen Kategorie
	 */
	public Map createVerteilergruppe(OctopusContext oc, String name, String key, String description){
		return dataAccess.createVerteilergruppe(key, name, description);
	}
	
	/*Definition der Action "deleteUser" */
	public String[] INPUT_deleteUser = { "userId" };
	public boolean[] MANDATORY_deleteUser = { true };
	public String OUTPUT_deleteUser = FIELD_DeleteUser;
	/**
	 * L�scht einen Benutzer
	 * @param oc der Octopus-Context
	 * @param userId UserId des Benutzers
	 * @return Map TODO: Was f�r eine Map?
	 * @throws TcSecurityException
	 */
	public Map deleteUser(OctopusContext oc, String userId) throws TcSecurityException{
		if(oc.commonConfig().getLoginManager(oc.getModuleName()).isUserManagementSupported()){
			oc.commonConfig().getLoginManager(oc.getModuleName()).getUserManager().deleteUser(userId);
		}

		return dataAccess.deleteUser(oc.getConfigObject(), userId);
	}
	
	
	/*Definition der Action "getBundeslaender" */
	public String[] INPUT_getBundeslaender = {};
	public boolean[] MANDATORY_getBundeslaender = {};
	public String OUTPUT_getBundeslaender = FIELD_Bundeslaender;
	/**
	 * Holt Map mit allen Bundesl�ndern
	 * @param oc der Octopus-Context
	 * @return Map mit den Bundesl�ndern
	 */
	public Map getBundeslaender(OctopusContext oc){
		return dataAccess.getBundeslaender();
	}
	
	/*Definition der Action "getBundeslaender" */
	public String[] INPUT_getBundeslandForPLZ = { "plz"};
	public boolean[] MANDATORY_getBundeslandForPLZ = { true };
	public String OUTPUT_getBundeslandForPLZ = FIELD_BundeslandForPLZ;
	/**
	 * Holt das Bundesland zu einer PLZ
	 * @param oc der Octopus-Context
	 * @param plz Postleitzahl, zu der das Bundesland geholt werden soll
	 * @return Map
	 */
	public Map getBundeslandForPLZ(OctopusContext oc, String plz){
		return dataAccess.getBundeslandForPLZ(plz);
	}
	
	/*Definition der Action "getCityForPLZ"*/
	public String[] INPUT_getCityForPLZ = { "plz" };
	public boolean[] MANDATORY_getCityForPLZ = { true };
	public String OUTPUT_getCityForPLZ = FIELD_GetCityForPLZ;
	/**
	 * Holt die Stadt zu einer PLZ.
	 * @param oc der OctopusContext
	 * @param plz Postleitzahl, zu der die Stadt geholt werden soll
	 * @return Map
	 */
	public Map getCityForPLZ(OctopusContext oc, String plz){
		return dataAccess.getCityForPLZ(plz);
	}
	
	/*Definition der Action "getDublikate"*/
	public String[] INPUT_getDublikate = {"duplicateFields", "exclude"};
	public boolean[] MANDATORY_getDublikate = {true, false};
	public String OUTPUT_getDublikate = FIELD_GetDublikate;
	/**
	 * Holt Duplikate. TODO: Doku!
	 * @param oc der OctopusContext
	 * @return Map
	 * @throws SQLException 
	 */
	public Map getDublikate(OctopusContext oc, Map duplicateFields, Integer excl) throws SQLException{
        
        // TODO: Bei Bedarf konfigurierbar machen. 
        // Der Client muss dann aber auch mit Dubletten IDs, die er nicht sehen darf umgehen k�nnen.
        boolean onlyUserViewableAdresses = true;

		Map resultmap = new HashMap();
		if(excl==null)excl = new Integer(0);
		//Parameter aus duplicateFields extrahieren
		String vorname = getDuplicateData(duplicateFields, "vorname");
		String nachname = getDuplicateData(duplicateFields, "name");
		String organisation = getDuplicateData(duplicateFields, "organisation");
		String plz = getDuplicateData(duplicateFields, "plz");
		String postfach = getDuplicateData(duplicateFields, "postfach");
		String postfachplz = getDuplicateData(duplicateFields, "plzpost");
		String[] commdata = pickfirst6comm(duplicateFields);
		//Eine ID f�r den Duplikatscheck generieren...
		Random random = new Random();
		Integer dupid = new Integer(random.nextInt(10000));
		//Query f�r den Aufruf:
		SQL.Procedure(TcDBContext.getDefaultContext(), TcDBContext.getSchemaName() + "dupcheck_generic") //FIXME TCObjects fuer StoredProcedures??
		.addParam(dupid)
		.addParam(nachname)
		.addParam(vorname)
		.addParam(organisation)
		.addParam(plz)
		.addParam(postfach)
		.addParam(postfachplz)
		.addParam(commdata[0])
		.addParam(commdata[1])
		.addParam(commdata[2])
		.addParam(commdata[3])
		.addParam(commdata[4])
		.addParam(commdata[5])
		.addParam((excl!=null) ? ""+excl : "0")
		.addParam(new Integer(0))
		.executeVoidProcedure(TcDBContext.getDefaultContext());
		
		/* old
		String doDupcheck = Queries.doDupcheck(dupid, vorname, nachname, organisation, plz, postfach, postfachplz, commdata[0], commdata[1], commdata[2], commdata[3], commdata[4], commdata[5], excl);
		DB.result(TcDBContext.getDefaultContext(), doDupcheck);
		old */
		
		//Ergebnisse holen
		Result result = DB.result(TcDBContext.getDefaultContext(), Queries.getDupcheckResult(dupid, excl, onlyUserViewableAdresses, oc.personalConfig().getUserID()));
		for(int i=0; result.resultSet().next(); i++){
			resultmap.put("id" + i, new Integer(result.resultSet().getInt(1)));
		}
		//Dupcheck l�schen
		DB.update(TcDBContext.getDefaultContext(), Queries.deleteDupcheck(dupid));
//		System.out.println("Gefundene Dublikate f�r:");
//		
//		System.out.println(" vorname = " + vorname);
//		System.out.println(" nachname = " + nachname);
//		System.out.println(" organisation = " + organisation);
//		System.out.println(" plz = " + plz);
//		System.out.println(" postfach = " + postfach );
//		System.out.println(" postfachplz = " + postfachplz );
//		System.out.println("[] commdata = " );
//		for (int r = 0; r < commdata.length; r++)
//			System.out.println(commdata[r]);
//		System.out.println("\n"+ resultmap);
//		
		return resultmap;
	}
	
	private String getDuplicateData(Map duplicateFields, String name){
		String result = null;
		if(duplicateFields.get(name)!=null&&((String)duplicateFields.get(name)).length()>0){
			result = (String) duplicateFields.get(name);
		}
		return result;
	}
	
	/**
	 * Holt die ersten 6 Kommdaten aus der duplicateFields und schreibt sie in einen Array
	 * @param duplicateFields
	 * @return
	 */
	private String[] pickfirst6comm(Map duplicateFields) {
		String commdata[] = new String[6];
		List comm = new ArrayList();
		addCommField(duplicateFields, comm, "telefonP");
		addCommField(duplicateFields, comm, "handyP");
		addCommField(duplicateFields, comm, "telefonD");
		addCommField(duplicateFields, comm, "faxP");
		addCommField(duplicateFields, comm, "faxD");
		addCommField(duplicateFields, comm, "handyD");
		addCommField(duplicateFields, comm, "email");
		addCommField(duplicateFields, comm, "emailp");
		for(int i=0; i<comm.size()&&i<6; i++){
			commdata[i] = (String) comm.get(i);
		}
		for(int i=comm.size();i<6; i++){
			commdata[i] = null;
		}
		return commdata;
	}


	/**
	 * F�gt ein Feld zur comm-Liste hinzu, falls der Eintrag in duplicateFields nicht null ist
	 * @param duplicateFields
	 * @param comm
	 * @param commname
	 */
	private void addCommField(Map duplicateFields, List comm, String commname) {
		if(duplicateFields.get(commname)!=null&&((String)duplicateFields.get(commname)).length()>0)comm.add(duplicateFields.get(commname));
	}

	/*Definition der Action "getGruppenForUser"*/
	public String[] INPUT_getGruppenForUser = {"user"};
	public boolean[] MANDATORY_getGruppenForUser = { true};
	public String OUTPUT_getGruppenForUser = FIELD_Gruppen;
	/**
	 * Holt alle Kategorien zu einem User.
	 * @param oc OctopusContext
	 * @param userId ID des Users
	 * @return Map mit KategorieDaten
	 */
	public Map getGruppenForUser(OctopusContext oc, String userId){
		return dataAccess.getGruppen(userId);
	}
	
	/*Definition der Action "getUserPwd"*/
	public String[] INPUT_getUserPwd = {"userId"};
	public boolean[] MANDATORY_getUserPwd = {true};
	public String OUTPUT_getUserPwd = FIELD_GetUserPwd;
	/**
	 * Holt das Passwort eines Users
	 * @deprecated Darf nicht mehr benutzt werden
	 * @param oc Octopus-Context
	 * @param userId ID des Users
	 * @return Map
	 */
	public Map getUserPwd(OctopusContext oc, String userId){
		return dataAccess.getUserPwd(userId);
	}
	
	/*Definition der Action "getVersion"*/
	public String[] INPUT_getVersionSOAP = {};
	public boolean[] MANDATORY_getVersionSOAP = {};
	public String OUTPUT_getVersionSOAP = FIELD_GetVersion;
	/**
	 * Holt die Version des ContactClients.
	 * @param oc der OctopusContext
	 * @return Map mit Versionsinformationen
	 */
	public Map getVersionSOAP(OctopusContext oc){
		return dataAccess.getVersion();
	}
	
	/*Definition der Action "mailBatch"*/
	public String[] INPUT_mailBatch = {};
	public boolean[] MANDATORY_mailBatch = {};
	public String OUTPUT_mailBatch = FIELD_MailBatch;
	/**
	 * Handling von Versandauftr�gen
	 * TODO: Dokumentation!!!!
	 * @param oc der OcotpusContext
	 * @return Map
	 */
	public Map mailBatch(OctopusContext oc){
		Map params = new HashMap(oc.getRequestObject().getRequestParameters());
		params.putAll(oc.getContentObject().getContent());
		String user = oc.getConfigObject().getLoginname();
		params.put("user", user);
		return dataAccess.mailBatch(params);
	}
	
	/*Definition der Action "setParameter"*/
	public String[] INPUT_setParameter = {"key", "value"};
	public boolean[] MANDATORY_setParameter = { true, true };
	public String OUTPUT_setParameter = FIELD_SetParameter;
	/**
	 * Setzt einen Parameter
	 * @param oc OctopusContext
	 * @param key Schl�ssel des Parameters
	 * @param value Wert des Parameters
	 * @return Map
	 */
	public Map setParameter(OctopusContext oc, String key, String value){
		return dataAccess.setParameter(key, value);
	}
	
	/*Definition der Action "setStandardVerteilergruppe"*/
	public String[] INPUT_setStandardVerteilergruppe = { "userId", "verteilergruppe" };
	public boolean[] MANDATORY_setStandardVerteilergruppe = { true, true };
	public String OUTPUT_setStandardVerteilergruppe = FIELD_SetStandardVerteilergruppe;
	/**
	 * Setzt die StandartVerteilergruppe eines Users.
	 * @param oc OctopusContext
	 * @param loginname Loginname des Users
	 * @param gruppe ID der Gruppe
	 * @return Map
	 */
	public static Map setStandardVerteilergruppe(OctopusContext oc, String loginname, String gruppe){
		return dataAccess.setStandardVerteilergruppe(gruppe, loginname);
	}
	
	/*Definition der Action "deleteAddress"*/
	public String[] INPUT_deleteAddress = { "adrId" , "verteilergruppe"};
	public boolean[] MANDATORY_deleteAddress = { true, true};
	public String OUTPUT_deleteAddress = FIELD_DeleteAddress;
	/**
	 * L�sche eine Adresse aus einer Kategorie
	 * @param oc der OctopusContext
	 * @param adrId AdressId der Adresse
	 * @param grp Kategorie
	 * @return Map
	 */
	public Map deleteAddress(OctopusContext oc, Integer adrId, String grp){
		return dataAccess.deleteAddress(adrId.intValue(), grp);
	}
	
}
