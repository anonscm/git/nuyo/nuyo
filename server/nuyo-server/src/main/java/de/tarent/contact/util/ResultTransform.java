package de.tarent.contact.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.contact.octopus.worker.constants.AddressWorkerConstants;

/**
 * @author kleinw
 * 
 * In dieser Klasse k�nnen Ergebnisse aus den DB-Klassen in Stringobjekte
 * umgewandelt werden, um eine kleinere Soapantwort zu generieren.
 *  
 */
public class ResultTransform {

    static public Object toSingleString(Object source, Map parameter)
        throws InputParameterException {
        if (parameter.containsKey(AddressWorkerConstants.PARAM_SINGLESTRINGRESULT)) {
            try {
                StringBuffer result = new StringBuffer();
                if (source instanceof List) {
                    for (Iterator it = ((List) source).iterator(); it.hasNext();) {
                        Object o = it.next();
                        if (o instanceof String && ((String)o).length() == 0)
                            result.append(" ");
                        else if (o == null)
                            result.append(" ");
                        else 
                            result.append(o.toString());
                        if (it.hasNext()) {
                            result.append("^");
                        }
                    }
                }
                return result.toString();
            }
            catch (ClassCastException e) {
                throw new InputParameterException(
                    "Der Parameter 'toSingleString' muss als Boolean �bergeben werden.");
            }
        }
        else {
            return source;
        }
    }
}
