/* $Id: MailWorker.java,v 1.4 2006/09/15 13:00:03 kirchner Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 * Created on 30.05.2003
 */
package de.tarent.contact.octopus.worker;

import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import de.tarent.contact.octopus.DBDataAccessPostgres;
import de.tarent.contact.octopus.DataAccess;
import de.tarent.contact.octopus.worker.constants.MailWorkerConstants;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * Dieser Worker verwaltet Session-Daten f�r die contact-Webapplikation.
 * 
 * @author mikel
 */
public class MailWorker implements TcContentWorker, MailWorkerConstants {
	public final static Logger logger =
		Logger.getLogger(MapFetcher.class.getName());

	public MailWorker() {
		super();
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
	}

	/* (non-Javadoc)
	 * @see tc.tcContent.TcContentWorker#doAction(tc.tcConfig.TcConfig, java.lang.String, tc.tcRequest.TcRequest, tc.tcContent.TcContent)
	 */
	public String doAction(
		TcConfig tcConfig,
		String actionName,
		TcRequest tcRequest,
		TcContent tcContent)
		throws TcContentProzessException {
		String result = RESULT_error;
		try {
			DataAccess dataAccess = new DBDataAccessPostgres();

			if (actionName.equals(ACTION_MAILSEND)) {
				// Infos f�r Mailverschicken
				String host = null;
				String to = null;
				String frommail = null;
				String fromname = null;
				String subject = null;
				String mimetype = null;
				String text = null;

				Map mailinfos = (Map) tcContent.get(FIELD_MAILSEND);
				if (mailinfos != null) {
					// Server-Einstellungen
					host =
						tcConfig.getCommonConfig().getConfigData(
							"extern.server.mail.smtp");

					// Grunddaten
					to = (String) mailinfos.get("to");
					frommail = (String) mailinfos.get("frommail");
					fromname = (String) mailinfos.get("fromname");
					subject = (String) mailinfos.get("subject");
					mimetype = (String) mailinfos.get("mimetype");
					text = (String) mailinfos.get("text");

					if (host != null
						&& to != null
						&& frommail != null
						&& to != null
						&& subject != null
						&& text != null) {
						Properties props = System.getProperties();
						props.put("mail.smtp.host", host);

						Session session =
							Session.getDefaultInstance(props, null);
						MimeMessage message = new MimeMessage(session);
						//						message.setFrom(new InternetAddress(frommail));
						if (fromname != null && fromname.length() > 0)
							message.addHeader(
								"From",
								"\"" + fromname + "\" <" + frommail + ">");
						else
							message.addHeader("From", frommail);
						message.setText(text);
						message.addRecipient(
							Message.RecipientType.TO,
							new InternetAddress(to));
						message.setSubject(subject);
						Transport.send(message);
						logger.fine("Sende eMail per SMTP an " + host);
						result = RESULT_ok;
					}
				}
			}

		} catch (AddressException e) {
			tcContent.setError(e);
			tcContent.setField(
				"status.message",
				"Es ist ein Fehler beim Zugriff auf die Daten entstanden.");
			result = RESULT_error;
		} catch (MessagingException e) {
			tcContent.setError(e);
			tcContent.setField(
				"status.message",
				"Es ist ein Fehler beim Zugriff auf die Daten entstanden.");
			result = RESULT_error;
		}
		return result;
	}


	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port =
			new TcPortDefinition(
				"de.tarent.contact.octopus.MapFetcher",
				"Worker zum Beziehen von Java Map's.");

		TcOperationDefinition operation =
			port.addOperation(ACTION_MAILSEND, "Verschickt eine eMail.");
		operation.setInputMessage();
		operation.setOutputMessage();
		return port;
	}

	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.4 $")
			+ " ("
			+ CVS.getContent("$Date: 2006/09/15 13:00:03 $")
			+ ')';
	}
}
