package de.tarent.contact.octopus.db;

import java.sql.SQLException;
import java.util.Map;

import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.Update;


/**
 * @author kleinw
 * 
 * Funktionalit�ten f�r Unterkategorien.
 *  
 */
public class TcSubCategoryDB {

    static public String modifySubCategory (Map parameter) throws InputParameterException, SQLException {
        String result = "";
        if (!parameter.containsKey("categoryPk"))
            throw new InputParameterException("F�r die Methode modifySubCategory muss der Parameter 'categoryPk' �bergeben sein. ");
        if (!parameter.containsKey("subCategoryPk")) 
            throw new InputParameterException("F�r die Methode modifySubCategory muss der Parameter 'subCategoryPk' �bergeben sein. ");
        if (!parameter.containsKey("subCategoryName") && !parameter.containsKey("subCategoryDescription"))
            throw new InputParameterException("F�r die Method modifySubCategory muss mindestens der Parameter 'subCategoryName' oder 'subCategoryDescription �bergeben werden. ");

        if (!(parameter.get("categoryPk") instanceof Integer))
            throw new InputParameterException("Der Parameter 'categoryPk' muss als Integer �bergeben werden.");
        if (!(parameter.get("subCategoryPk") instanceof Integer))
            throw new InputParameterException("Der Parameter 'subCategoryPk' muss als Integer �bergeben werden.");

        if (parameter.containsKey("subCategoryName") && !(parameter.get("subCategoryName") instanceof String)) 
            throw new InputParameterException("Der Parameter 'subCategoryName' muss als String �bergeben werden.");
        if (parameter.containsKey("subCategoryDescription") && !(parameter.get("subCategoryDescription") instanceof String)) 
            throw new InputParameterException("Der Parameter 'subCategoryDescription' muss als String �bergeben werden.");
        
        Update update =
            SQL.Update(TcDBContext.getDefaultContext())
            	.table(TsubcategoryDB.getTableName());
        if (parameter.containsKey("subCategoryName"))
            update.update(TsubcategoryDB.CATEGORYNAME, (String)parameter.get("subCategoryName"));
        if (parameter.containsKey("subCategoryDescription"))
            update.update(TsubcategoryDB.DESCRIPTION, (String)parameter.get("subCategoryDescription"));

        update.where(Expr.equal(TsubcategoryDB.PK_PKSUBCATEGORY, (Integer)parameter.get("subCategoryPk")));
        update.whereAnd(Expr.equal(TsubcategoryDB.FKCATEGORY, (Integer)parameter.get("categoryPk")));
        
        DB.update(TcDBContext.getDefaultContext(), update.toString());

        return result;
    }
    
}
