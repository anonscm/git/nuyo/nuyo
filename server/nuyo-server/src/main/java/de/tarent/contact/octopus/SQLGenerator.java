package de.tarent.contact.octopus;

import java.util.Map;

import de.tarent.contact.octopus.db.TcDBContext;

/**
 * @author kleinw
 *
 *	In dieser Klassen werden Methoden zur Produktion von SQL-Statements
 *	bereitgestellt. Weiterhin soll die Klasse DBDataAccess bereinigt und einige
 *	Funktionalit�t in diese Klasse �bertragen werden. Das Insert-, Select- und
 *	Update f�r die Tabelle TADDRESS wird auch hier �bernommen.
 *
 */
public class SQLGenerator {

	//	Diese Methode erstellt ein Select-Statement f�r TADDRESS
	static public String createSelectAddressStatement(
		Map inputData,
		String tableName) {
		StringBuffer statement = new StringBuffer();
		statement.append("SELECT ");
		for (int i = 1; i <= DB2Map.DATABASE_FIELDS_TADDRESS.length; i++) {
			statement.append(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][0]);
			statement.append(",");
		}
		statement.replace(statement.length() - 1, statement.length(), "");
		statement.append("FROM ");
		statement.append(TcDBContext.getSchemaName());
		statement.append(tableName);
		statement.append(" WHERE ");
		statement.append(TcDBContext.getSchemaName());
		statement.append(tableName);
		statement.append(".PK_ADRESS = ");
		statement.append("'");
		statement.append(inputData.get("adrId"));
		statement.append("'");
		return statement.toString();
	}

	//	Diese Methode erstellt ein Insert-Statement f�r TADDRESS.
	// 	Hier bei werden nur die Felder eingef�gt, die tats�chlich in
	//	der Map �bergeben wurden.
	static public String createInsertAddressStatement(
		Map inputData,
		String tableName,
		boolean insertPK) {
		StringBuffer statement = new StringBuffer();
		statement.append("INSERT INTO ");
		statement.append(TcDBContext.getSchemaName());
		statement.append(tableName);
		statement.append(" (");
		if (insertPK) {
			statement.append("PK_ADDRESS,");
		}
		for (int i = 1; i <= DB2Map.DATABASE_FIELDS_TADDRESS.length; i++) {
			if (inputData.containsKey(DB2Map.DATABASE_FIELDS_TADDRESS[i-1][1])) {
				statement.append(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][0]);
				statement.append(",");
			}
		}
		statement.replace(statement.length() - 1, statement.length(), "");
		statement.append(") VALUES (");
		if (insertPK) {
			statement.append("0,");
		}
		for (int i = 1; i <= DB2Map.DATABASE_FIELDS_TADDRESS.length; i++) {
			if (inputData
				.containsKey(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][1])) {
				statement.append("'");
				statement.append(
					inputData.get(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][1]));
				statement.append("'");
				statement.append(",");
			} 
		}
		statement.replace(statement.length() - 1, statement.length(), "");
		statement.append(")");
		return statement.toString();
	}

	// 	Diese Methode erstellt ein Update-Statement f�r TADDRESS. 
	// 	Hierbei werden nur die Felder erneuert, die in der Map tats�chlich
	//	�bergeben wurden.
	static public String createUpdateAddressStatement(
		Map inputData,
		String schema,
		String tableName,
		String whereClause) {
		StringBuffer statement = new StringBuffer();
		statement.append("UPDATE ");
		statement.append(TcDBContext.getSchemaName());
		statement.append(tableName);
		statement.append(" SET ");
		boolean entryFound = false;
		for (int i = 1; i <= DB2Map.DATABASE_FIELDS_TADDRESS.length; i++) {
			if (inputData.get(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][1])
				!= null) {
				entryFound = true;
				if (inputData.get(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][1])
					instanceof String) {
					statement.append(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][0]);
					statement.append("='");
					statement.append(
						inputData.get(
							DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][1]));
					statement.append("'");
				} else {
					statement.append(DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][0]);
					statement.append("=");
					statement.append(
						inputData.get(
							DB2Map.DATABASE_FIELDS_TADDRESS[i - 1][1]));
				}
				statement.append(",");
			}
		}
		statement.replace(statement.length() - 1, statement.length(), "");
		if (whereClause != null) {
			statement.append(" WHERE ");
			statement.append(whereClause);
		}

		if (entryFound)
			return statement.toString();
		else
			return "";
	}
}