package de.tarent.contact.octopus.worker.constants;

/**
 * @author kleinw
 *		
 *	Konstanten f�r den LDAPWorker.
 *
 */
public interface LDAPWorkerConstants {

	public final static String ACTION_EXPORT_ALL = "exportall";

}
