package de.tarent.contact.octopus.db;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.bean.TactionDB;
import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TcountryDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.bean.TregionDB;
import de.tarent.contact.bean.TsalutationDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.statement.Select;

/**
 * @author kleinw
 * 
 * Hier werden allgemeine Funktionen gesammelt.
 *  
 */
public class TcStaticDB {

	private static Logger logger = Logger.getLogger(TcStaticDB.class.getName());

    static public List getStaticData(Map parameter) throws SQLException,
            InputParameterException {
        if (!parameter.containsKey("userid"))
                throw new InputParameterException(
                        "F�r das laden der statischen Daten muss der Parameter 'userid' �bergeben werden.");
        if (!(parameter.get("userid") instanceof String))
                throw new InputParameterException(
                        "Der Parameter 'userid' muss als String �bergeben werden.");
        String userid = (String) parameter.get("userid");
        List<Object> staticData = new Vector<Object>();
        Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TcountryDB.getTableName())
                .selectAs(TcountryDB.COUNTRY)
                .selectAs(TcountryDB.COUNTRYSHORT)
                .where(Expr.notEqual(TcountryDB.COUNTRY, new RawClause("''")))
                .whereAnd(Expr.notEqual(TcountryDB.COUNTRYSHORT, new RawClause("''")));
        Result tr = null;
        StringBuffer sb = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            while (tr.resultSet().next()) {
                sb.append(tr.resultSet().getString(TcountryDB.COUNTRYSHORT));
                sb2.append(tr.resultSet().getString(TcountryDB.COUNTRY));
                if (tr.resultSet().next()) {
                    sb.append("#");
                    sb2.append("#");
                    tr.resultSet().previous();
                }
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e);
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }
        staticData.add(sb2.toString());
        staticData.add(sb.toString());

        select = SQL.Select(TcDBContext.getDefaultContext()).from(TregionDB.getTableName()).selectAs(
                TregionDB.REGION);


        tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
        sb = new StringBuffer();

        try {
            while (tr.resultSet().next()) {
                sb.append(tr.resultSet().getString(TregionDB.REGION));
                if (tr.resultSet().next()) {
                    sb.append("#");
                    tr.resultSet().previous();
                }
            }
        } catch (SQLException e7) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e7);
            throw(e7);
        } finally {
            if (tr != null) tr.close();
        }

        staticData.add(sb.toString());

        select = SQL.Select(TcDBContext.getDefaultContext()).from(TsalutationDB.getTableName()).selectAs(
                TsalutationDB.SALUTATION);
        sb = new StringBuffer();
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            while (tr.resultSet().next()) {
                sb.append(tr.resultSet().getString(TsalutationDB.SALUTATION));
                if (tr.resultSet().next()) {
                    sb.append("#");
                    tr.resultSet().previous();
                }
            }
        } catch (SQLException e1) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e1);
            throw (e1);
        } finally {
            if (tr != null) tr.close();
        }
        staticData.add(sb.toString());

        //	Standardkategorie des Users.
        Select subSelect = SQL.Select(TcDBContext.getDefaultContext()).from(TuserDB.getTableName()).select(
                TuserDB.PK_PKUSER).where(Expr.equal(TuserDB.LOGINNAME, userid));
		//TODO: Solange uns hier nichts besseres einf�llt, liefern wir hier einfach die erste Kategorie der zugeordneten Kategorien
		select = SQL.Select(TcDBContext.getDefaultContext()).from(TgroupCategoryDB.getTableName())
				.selectAs(TgroupCategoryDB.FKCATEGORY)
				.join(TsubcategoryDB.getTableName(), TsubcategoryDB.FKCATEGORY, TgroupCategoryDB.FKCATEGORY)
				.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
				.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
				.where(Expr.in(TgroupUserDB.FKUSER, new SubSelect(TcDBContext.getDefaultContext(), subSelect)))
                .whereAnd(Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1)));

        tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
        try {
            if (tr.resultSet().next()) {
                staticData.add(new Integer(tr.resultSet().getInt(
                        TgroupCategoryDB.FKCATEGORY)));
            } else {
                staticData.add("n.a.");
                logger.log(Level.INFO, "Der User hat keien Standardkategorie zugeordnet");
            }
        } catch (SQLException e2) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e2);
            throw (e2);
        } finally {
            if (tr != null) tr.close();
        }

        //	Allgemeine Kategorie holen
        select = SQL.Select(TcDBContext.getDefaultContext()).from(TcategoryDB.getTableName()).join(
                TsubcategoryDB.getTableName(), TsubcategoryDB.FKCATEGORY,
                TcategoryDB.PK_PKCATEGORY).selectAs(
                TcategoryDB.PK_PKCATEGORY).selectAs(
                TsubcategoryDB.PK_PKSUBCATEGORY).where(Expr.equal(TcategoryDB.FKCATEGORYTYPE, new Integer(10)));
        tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
        try {	
            if (tr.resultSet().next()) {
                System.out.println(tr.resultSet().getInt(TcategoryDB.PK_PKCATEGORY));
                staticData
                        .add(new StringBuffer().append(tr.resultSet().getInt(
                                TcategoryDB.PK_PKCATEGORY)).append("#").append(
                                tr.resultSet().getInt(
                                        TsubcategoryDB.PK_PKSUBCATEGORY)).toString());
            } else {
                logger.log(Level.WARNING, "Keine allgemeine Kategorie gefunden.");
                staticData.add(new StringBuffer().append(new Integer(0)).append("#").append(new Integer(0)));
            }
        } catch (SQLException e3) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e3);
            throw (e3);
        } finally {
            if (tr != null) tr.close();
        }

        //	Adminstatus holen
        select = SQL.Select(TcDBContext.getDefaultContext()).from(TuserDB.getTableName()).selectAs(
                TuserDB.ISMANAGER).where(Expr.equal(TuserDB.LOGINNAME, userid));
        tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
        try {
            if (tr.resultSet().next()) {
                staticData.add(new Integer(tr.resultSet().getInt(
                        TuserDB.ISMANAGER)));
            } else {
                logger.log(Level.WARNING, "Adminstatus nicht vorhanden");
            }
        } catch (SQLException e4) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e4);
            throw (e4);
        } finally {
            if (tr != null) tr.close();
        }

        //	Kategorie "Meine Kategorie" holen
        select = SQL.Select(TcDBContext.getDefaultContext())
        	.from(TcategoryDB.getTableName())
        	.selectAs(TcategoryDB.PK_PKCATEGORY)
        	.where(Expr.equal(TcategoryDB.FKCATEGORYTYPE, new Integer(12)));
        
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            if(tr.resultSet().next()) {
                staticData.add(new Integer(tr.resultSet().getInt(TcategoryDB.PK_PKCATEGORY)));
            } else {
                staticData.add(new Integer(0));
                logger.log(Level.INFO, "Kategorie 'Meine Kategorie' nicht vorhanden");
            }
        } catch (SQLException e8) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e8);
            throw(e8);
        } finally {
            if (tr != null) tr.close();
        }
        
        //	Versandauftr�ge des Users
        sb = new StringBuffer();
        select = SQL.Select(TcDBContext.getDefaultContext()).from(TactionDB.getTableName()).join(
                TuserDB.getTableName(), TuserDB.PK_PKUSER, TactionDB.FKUSERINIT)
                			.selectAs(TactionDB.PK_PKACTION)
                			.selectAs(TactionDB.ACTIONTITLE)
                        .selectAs(TactionDB.FKUSERINIT)
                        .selectAs(TactionDB.ACTIONDATE)
                        .selectAs(TactionDB.ADDRESSCOUNT02)
                        .selectAs(TactionDB.ADDRESSCOUNT01)
                        .selectAs(TactionDB.ADDRESSCOUNT03)
                        .selectAs(TactionDB.ADDRESSCOUNT04)
                        .selectAs(TactionDB.ADDRESSCOUNT06)
                        .selectAs(TactionDB.ADDRESSCOUNT05)
                        .selectAs(TactionDB.ADDRESSCOUNT07)
                        .selectAs(TactionDB.ADDRESSCOUNT)
                        .where(Expr.equal(TuserDB.LOGINNAME, userid));
        tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
        try {
            while (tr.resultSet().next()) {
                sb.append(tr.resultSet().getInt(TactionDB.PK_PKACTION));
                sb.append("#");
                if (tr.resultSet().getString(TactionDB.ACTIONTITLE).length() == 0)
                    sb.append(" ");
                else
                    sb.append(tr.resultSet().getString(TactionDB.ACTIONTITLE));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.FKUSERINIT));
                sb.append("#");
                sb.append(tr.resultSet().getString(TactionDB.ACTIONDATE));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT02));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT01));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT03));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT04));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT06));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT05));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT07));
                sb.append("#");
                sb.append(tr.resultSet().getInt(TactionDB.ADDRESSCOUNT));
                staticData.add(sb.toString());
                sb = new StringBuffer();
            }
        } catch (SQLException e5) {
            logger.log(Level.SEVERE, "SQLException Error in TcStaticDB", e5);
            throw (e5);
        } finally {
            if (tr != null) tr.close();
        }

        return staticData;
    }
}
