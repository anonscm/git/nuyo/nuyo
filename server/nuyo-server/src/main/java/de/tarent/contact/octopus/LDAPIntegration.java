/* $Id: LDAPIntegration.java,v 1.4 2006/09/15 13:00:02 kirchner Exp $
 * 
 * Created on 13.06.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.octopus.ldap.LDAPContact;
import de.tarent.contact.octopus.ldap.LDAPException;
import de.tarent.contact.octopus.ldap.LDAPManager;
import de.tarent.contact.octopus.worker.MapFetcher;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * @author philipp
 *
 * Dieser Worker sorgt f�r die LDAP Integration in des Octopus-Framework
 */
public class LDAPIntegration implements TcContentWorker {
	public final static Logger logger =
		Logger.getLogger(MapFetcher.class.getName());

	public LDAPIntegration() {
		super();
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig config) {
	}

	/* (non-Javadoc)
	 * @see tc.tcContent.TcContentWorker#doAction(tc.tcConfig.TcConfig, java.lang.String, tc.tcRequest.TcRequest, tc.tcContent.TcContent)
	 */
	public String doAction(
		TcConfig tcConfig,
		String actionName,
		TcRequest tcRequest,
		TcContent tcContent)
		throws TcContentProzessException {
		String result = RESULT_error;
		if (ACTION_EXPORT_ALL.equalsIgnoreCase(actionName)) {
			try {
				LDAPManager ldm = LDAPConnect(tcConfig);
				DataAccess DataAccess = null;
                DataAccess = new DBDataAccessPostgres();
				logger.log(Level.FINE, "Starte Export!");
				Map verteilergruppen = createStructure(ldm, DataAccess);
				createContacts(ldm, DataAccess, verteilergruppen);
			} catch (LDAPException e) {
				logger.log(Level.SEVERE, "Konnte Aktion nicht durchf�hren", e);
			}
		}
		return result;
	}

	/**
	* @param ldm der LDAP-Manager
	* @param DataAccess
	* @param verteilergruppen
	*/
	private void createContacts(
		LDAPManager ldm,
		DataAccess DataAccess,
		Map verteilergruppen) {
		//durchlaufe alle Verteilergruppen 
		Iterator it = verteilergruppen.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = entry.getKey().toString();
			String value = entry.getValue().toString();
			List user = DataAccess.getUserGroups(key);
			logger.log(
				Level.FINE,
				"Hole Kontakte aus Datenbank! " + key + ":" + value);
			Map contacts =
				DataAccess.getAddressesfromGroup(Integer.parseInt(key));
			logger.log(Level.FINE, "Fertig!");
			Map address = (Map) contacts.get("address");
			List users = DataAccess.getUserGroups(key);
			if (address != null) {
				Iterator it2 = address.entrySet().iterator();
				List uids = new ArrayList();
				while (it2.hasNext()) {
					Map.Entry contact = (Map.Entry) it2.next();
					Map contactMap = (Map) contact.getValue();
					String userid = contact.getKey().toString();
					uids.add(userid);
					createContact(ldm, value, users, contactMap, userid);
				}

				//hole Liste der Addressen aus dem LDAP
				try {
					List uidsldap =
						ldm.getUIDs(
							"ou="
								+ value
								+ ldm.getRelative()
								+ ldm.getBaseDN());
					String ldap = new String();
					String db = new String();
					for (int i = 0; i < uidsldap.size(); i++) {
						boolean found = false;
						for (int j = 0; j < uids.size(); j++) {
							db = uids.get(j).toString().trim();
							ldap = uidsldap.get(i).toString().trim();
							if (db.equals(ldap)) {
								found = true;
							}
						}
						if (!found) {
							ldm.delContact("uid=" + ldap + ",ou=" + value);
						}
					}

				} catch (LDAPException e) {
					logger.log(
						Level.SEVERE,
						"Konnte Liste der Kontakte nicht abgleichen!");
				}
			} else {
				System.out.println(contacts.get("fehler"));
				System.out.println("Konnte keine Adressen finden!");
			}

		}

	}
	public static void createContact(
		LDAPManager ldm,
		String verteilergruppe,
		List users,
		Map contactMap,
		String userid) {
		LDAPContact ldc = new LDAPContact();

		try {
			if (contactMap.get("a4") != null)
				ldc.setVorname(contactMap.get("a4").toString());
			if (contactMap.get("a5") != null)
				ldc.setNachname(contactMap.get("a5").toString());
			//ldc.setMittelname("");
			ldc.setUserid(userid);
			if (contactMap.get("a4") != null)
				ldc.setSpitzname(contactMap.get("a4").toString());
			if (contactMap.get("a7") != null)
				ldc.setArbeitFirma(contactMap.get("a7").toString());
			//ldc.setArbeitAbteilung("EDV-Abteilung");
			if (contactMap.get("a8") != null) {
				if (contactMap.get("a9") != null) {
					ldc.setArbeitStrasse(
						contactMap.get("a8").toString()
							+ " "
							+ contactMap.get("a9").toString());
				}
			}
			if (contactMap.get("a10") != null)
				ldc.setArbeitPLZ(contactMap.get("a10").toString());
			if (contactMap.get("a11") != null)
				ldc.setArbeitOrt(contactMap.get("a11").toString());
			if (contactMap.get("a21") != null)
				ldc.setArbeitBundesstaat(contactMap.get("a21").toString());
			if (contactMap.get("a19") != null)
				ldc.setArbeitLand(contactMap.get("a19").toString());
			if (contactMap.get("a2") != null)
				ldc.setArbeitJob(contactMap.get("a2").toString());
			if (contactMap.get("a22") != null) {
				if (((Map) contactMap.get("a22")).get("102") != null)
					ldc.setArbeitTelefon(
						((Map) contactMap.get("a22")).get("102").toString());
				if (((Map) contactMap.get("a22")).get("104") != null)
					ldc.setArbeitFax(
						((Map) contactMap.get("a22")).get("104").toString());
				if (((Map) contactMap.get("a22")).get("106") != null)
					ldc.setHandy(
						((Map) contactMap.get("a22")).get("106").toString());
				if (((Map) contactMap.get("a22")).get("108") != null)
					ldc.setEmail(
						((Map) contactMap.get("a22")).get("108").toString());
				if (((Map) contactMap.get("a22")).get("101") != null)
					ldc.setHomeTelefon(
						((Map) contactMap.get("a22")).get("101").toString());
			}
			//System.out.println("EMail: " + ldc.getEmail() + " Initials: " + ldc.getMittelname() + "|");
			/*if(ldc.getEmail().equals("")){
				ldc.setEmail("unknown@unkno.wn");
			}*/
			//ldc.setPager("");
			if (contactMap.get("a8") != null)
				if (contactMap.get("a8") != null)
					ldc.setHomeStrasse(contactMap.get("a8").toString());

			//ldc.setBeschreibung("Mein toller Testeintrag!");
			ldc.setVerteilergruppe(verteilergruppe);
			ldc.setUsers(users);
			//System.out.println(contactMap.get("a4")+" "+contactMap.get("a5"));
			//System.out.println(ldc);
			if (ldm.checkContact(ldc)) {
				//System.out.println("Modifiziere Contact " + ldc.getNachname());
				ldm.modifyContact_restricted(ldc);
				//Das untere ist schneller ?!
				//ldm.delContact("uid="+ldc.getUserid()+",ou="+ldc.getVerteilergruppe());
				//ldm.addContact_restricted(ldc);	
			} else {
				//System.out.println("Lege Contact " + ldc.getNachname()+ " an");
				ldm.addContact_restricted(ldc);
			}
		} catch (LDAPException e) {
			logger.log(
				Level.SEVERE,
				"Konnte Kontakt "
					+ ldc.getVorname()
					+ " "
					+ ldc.getNachname()
					+ " nicht anlegen! "
					+ e.getMessage());
		}
	}

	private LDAPManager LDAPConnect(TcConfig tcConfig) throws LDAPException {
		LDAPManager ldm = new LDAPManager(tcConfig);
		ldm.login(
			tcConfig.getModuleConfig().getParam("ldapuser"),
			false,
			tcConfig.getModuleConfig().getParam("ldappwd"),
			tcConfig.getModuleConfig().getParam("ldapauth"));
		return ldm;
	}

	private Map createStructure(LDAPManager ldm, DataAccess DataAccess) {
		Map verteilergruppen = DataAccess.getAllVerteilergruppen();
		//durchlaufe alle Verteilergruppen 
		Iterator it = verteilergruppen.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = entry.getKey().toString();
			String value = entry.getValue().toString();
			List user = DataAccess.getUserGroups(key);
			boolean test = false;
			try {
				test = ldm.checkOu(value);
				if (!test) {
					ldm.createOU(value, user);
				} else {
					ldm.modifyOU(value, user);
				}
			} catch (LDAPException e1) {
				logger.log(
					Level.SEVERE,
					"Konnte Verteilergruppe "
						+ value
						+ " nicht anlegen!"
						+ e1.getMessage());
			}
		}
		//TODO: Testen, ob Verteilergruppen aus dem LDAP nicht mehr in Datenbank vorkommen
		List verteilergruppenldap = getVerteilerGruppen(ldm);
		for (int i = 0; i < verteilergruppenldap.size(); i++) {
			String testou = verteilergruppenldap.get(i).toString();
			boolean found = false;
			Iterator it2 = verteilergruppen.entrySet().iterator();
			while (it2.hasNext()) {
				Map.Entry entry = (Map.Entry) it2.next();
				String value = entry.getValue().toString();
				if (value.equals(testou)) {
					found = true;
				}
			}
			if (!found) {
				//System.out.println(testou + " nicht gefunden!");
				try {
					ldm.delOU(testou);
				} catch (LDAPException e) {
					logger.log(
						Level.SEVERE,
						"Konnte gel�schte Verteilergruppe "
							+ testou
							+ "im LDAP nicht l�schen!");
				}
			}
		}
		return verteilergruppen;
	}

	private List getVerteilerGruppen(LDAPManager ldm) {
		List vertgrps = new ArrayList();
		try {
			vertgrps =
				ldm.getOUs(ldm.getRelative().substring(1) + ldm.getBaseDN());
		} catch (LDAPException e) {
			logger.log(
				Level.SEVERE,
				"Konnte Verteilergruppen nicht aus dem LDAP auslesen!");
		}
		return vertgrps;
	}

	/* (non-Javadoc)
	 * @see tc.tcContent.TcContentWorker#getWorkerDefinition()
	 */
	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port =
			new TcPortDefinition(
				"de.tarent.contact.octopus.LDAPIntegration",
				"Worker zum Integrieren von LDAP");

		TcOperationDefinition operation =
			port.addOperation(
				ACTION_EXPORT_ALL,
				"Exportiert den kompletten Addressbestand ins LDAP");
		operation.setInputMessage();
		operation.setOutputMessage();
		return port;
	}

	public final static String ACTION_EXPORT_ALL = "exportall";

	public final static String RESULT_ok = "ok";
	public final static String RESULT_error = "error";

	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.4 $")
			+ " ("
			+ CVS.getContent("$Date: 2006/09/15 13:00:02 $")
			+ ')';
	}
}
