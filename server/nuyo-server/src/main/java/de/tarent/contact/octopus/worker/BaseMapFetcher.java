package de.tarent.contact.octopus.worker;

import de.tarent.contact.octopus.worker.constants.MapFetcherConstants;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcAbstractContentWorker;
import de.tarent.octopus.content.TcMessageDefinition;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.util.CVS;


/**
 * @author kleinw
 *
 *	Dies ist die alte Version des Workers.
 *	
 *
*/
public class BaseMapFetcher extends TcAbstractContentWorker implements MapFetcherConstants {

    public BaseMapFetcher () {
        super ();
    }
    
	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port =
			new TcPortDefinition(
				"de.tarent.contact.octopus.MapFetcher",
				"Worker zum Beziehen von Java Map's.");

		TcOperationDefinition operation =
			port.addOperation(ACTION_GetAnreden, "Liefert Anreden.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Anreden,
			TcMessageDefinition.TYPE_STRUCT,
			"Auflistung aller vorhandenen Anreden.");

		operation =
			port.addOperation(
				ACTION_GetGruppenForUser,
				"Liefert die Verteilergruppen, auf die der Benutzer Zugriff hat.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Gruppen,
			TcMessageDefinition.TYPE_STRUCT,
			"Abbildung der Schl�ssel der Verteilergruppen des Benutzers auf ihre Beschreibungen.");

		operation =
			port.addOperation(ACTION_GetBundeslaender, "Liefert Bundesl�nder.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Bundeslaender,
			TcMessageDefinition.TYPE_STRUCT,
			"Auflistung aller vorhandener Bundesl�nder.");

		operation =
			port.addOperation(ACTION_GetLKZ, "Liefert Landeskennzahlen.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_LKZ,
			TcMessageDefinition.TYPE_STRUCT,
			"Auflistung aller vorhandener Landeskennzahlen.");

		operation = port.addOperation(ACTION_GetLaender, "Liefert L�nder.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Laender,
			TcMessageDefinition.TYPE_STRUCT,
			"Auflistung aller vorhandener L�nder.");

		operation =
			port.addOperation(
				ACTION_GetStandardVerteiler,
				"Liefert Standardverteiler.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_StandardVerteiler,
			TcMessageDefinition.TYPE_STRUCT,
			"Standardverteilergruppe zu aktuellem Benutzer.");

		operation =
			port.addOperation(
				ACTION_SetStandardVerteilergruppe,
				"Setzt die Standardverteilergruppe zu einem Benutzer.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_CollectOrphan,
				"Checkt eine Addresse ob sie in den L�schverteiler geh�rt.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_ShowGruppeToUser,
				"Ordnet einem Benutzer eine Verteilergruppe zu.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_DeleteGruppeFromUser,
				"Nimmt einem Benutzer eine Verteilergruppe weg.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_GetAllgemeinenVerteiler,
				"Liefert Allgemeinen Verteiler.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_AllgemeinerVerteiler,
			TcMessageDefinition.TYPE_STRUCT,
			"Allgemeine Verteilergruppe.");

		operation =
			port.addOperation(
				ACTION_GetAllVerteilergruppen,
				"Liefert alle Verteilergruppen.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetAllVerteilergruppen,
			TcMessageDefinition.TYPE_STRUCT,
			"Alle Verteilergruppen.");

		operation = port.addOperation(ACTION_GetUsers, "Liefert alle User.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Users,
			TcMessageDefinition.TYPE_STRUCT,
			"Alle User.");

		operation =
			port.addOperation(ACTION_GetVerteiler, "Liefert Verteiler.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Verteiler,
			TcMessageDefinition.TYPE_STRUCT,
			"Verteilergruppen.");

		operation =
			port.addOperation(ACTION_GetVerteilerExt, "Liefert Verteiler.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Verteiler,
			TcMessageDefinition.TYPE_STRUCT,
			"Verteilergruppen.");

		operation =
			port.addOperation(
				ACTION_GetAddress,
				"Liefert einen Addressdatensatz.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_Address,
			TcMessageDefinition.TYPE_STRUCT,
			"Liefert einen Addressdatensatz.");

		operation =
			port.addOperation(
				ACTION_GetChosenAddresses,
				"Liefert AddressID's.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_ChosenAddresses,
			TcMessageDefinition.TYPE_STRUCT,
			"Liefert AddressID's.");

		operation =
			port.addOperation(
				ACTION_SearchAddress,
				"Liefert AddressID's zu einer Suchanfrage.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_SearchAddress,
			TcMessageDefinition.TYPE_STRUCT,
			"Liefert AddressID's.");

		operation =
			port.addOperation(
				ACTION_BundeslandForPLZ,
				"Liefert das Bundesland zu einer PLZ");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_BundeslandForPLZ,
			TcMessageDefinition.TYPE_STRUCT,
			"Enth�lt das entsprechende Bundesland.");

		operation =
			port.addOperation(
				ACTION_InsertAddress,
				"Erstellt einen Addressdatensatz.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_InsertAddress,
			TcMessageDefinition.TYPE_STRUCT,
			"Inserted einen Addressdatensatz.");

		operation =
			port.addOperation(
				ACTION_UpdateAddress,
				"Aktualisiert einen Addressdatensatz.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_UpdateSubscriptor,
				"Updated einen Abo-Datum.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_SetBemerkung,
				"F�gt eine Bemerkung zur Verteilergruppe hinzu.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_SetVerteiler,
				"�bernimmt �nderungen im Verteiler.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_DeleteAddress,
				"L�scht einen Adressdatensatz.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_DeleteAddress,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt an, ob die Addresse gel�scht wurde.");

		operation =
			port.addOperation(
				ACTION_CreateVerteilergruppe,
				"Erstellt eine Verteilergruppe");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_DeleteVerteilergruppe,
				"L�scht eine Verteilergruppe");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_CreateVerteiler,
				"Erstellt einen Verteiler");
		operation.setInputMessage();

		operation =
			port.addOperation(ACTION_DeleteVerteiler, "L�scht einen Verteiler");
		operation.setInputMessage();

		operation =
			port.addOperation(ACTION_CreateUser, "Erstellt einen neuen User");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_ChangeUser,
				"Bearbeitet einen vorhandenen User");
		operation.setInputMessage();

		operation = port.addOperation(ACTION_DeleteUser, "L�scht einen User");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_GetUserPwd,
				"Liefert das Passwort zu einem User");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_GetUserSpecial,
				"Liefert das Special-Flag zu einem User");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_GetUserAdmin,
				"Gibt an, ob ein User Admin-Rechte hat");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_GetUserProperties,
				"Holt die Einstellungen eines Users.");
		operation.setInputMessage();

		operation =
			port.addOperation(
				ACTION_CreateVersandAuftrag,
				"Erstellt einen neuen Versandauftrag");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_CreateVersandAuftrag,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt den neu erstellten Versandauftrag zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetVersandAuftraege,
				"Holt Versandauftr�ge");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetVersandAuftraege,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt eine Map mit Versandauftr�gen zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetDublikate,
				"Pr�ft auf Dublikate zu einem Datensatz.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetDublikate,
			TcMessageDefinition.TYPE_STRUCT,
			"Listet alle gefundenen Dublikate.");

		operation =
			port.addOperation(
				ACTION_GetLKZforLand,
				"Sucht nach dem Land zu einer LKZ.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetLKZforLand,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt das Land zu einer LKZ zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetLandforLKZ,
				"Sucht nach einer LKZ zu einem Land.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetLandforLKZ,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt die LKZ zu einem Land zur�ck.");

		operation =
			port.addOperation(
				ACTION_MailBatch,
				"Ruft Methoden aus OctopusMailBatch auf.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_MailBatch,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt ein Ergebnis in Form einer Map zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetVersion,
				"Gibt die aktuelle Version zur�ck.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetVersion,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt die aktuelle Version zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetCityForPLZ,
				"Sucht nach einem Ort zu �bergebener PLZ.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetCityForPLZ,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt den Ort zu einer PLZ zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetParameter,
				"Holt einen Parameter zu �bergebenem Key.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetParameter,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt den Parameter zu einem Key zur�ck.");

		operation =
			port.addOperation(ACTION_SetParameter, "Setzt einen Parameter.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_SetParameter,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt eine leere Map bzw. im Fehlerfall eine Map mit Fehlermeldung zur�ck.");

		operation =
			port.addOperation(
				ACTION_GetInitDatas,
				"Holt Initialisierungsdaten bei einer SSL-Verbindung.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_GetInitDatas,
			TcMessageDefinition.TYPE_STRUCT,
			"Gibt gesammelt alle Daten f�r die Contact-Initialisierung zur�ck.");

		operation.addFaultMessage(
			MSG_ERROR,
			"Die Bearbeitung verlief fehlerhaft.");
		
		operation =
			port.addOperation(ACTION_GetAdressMyMap, "Holt Addressen und gibt eine anders als normal struckturierte Map zur�ck.");
		operation.setInputMessage();
		operation.setOutputMessage();
	
		operation = 
			port.addOperation(ACTION_GetAddressList, "Holt eine Liste von Addressen");
		operation.setInputMessage();
		operation.setOutputMessage();
		
		return port;
	}

	public void init(TcModuleConfig config) {
//	    Lg.init(config.getRealPath());
	}
    
	public String getVersion() {
		return CVS.getContent("$Revision: 1.3 $")
			+ " ("
			+ CVS.getContent("$Date: 2007/01/05 16:57:23 $")
			+ ')';
	}
	
}
