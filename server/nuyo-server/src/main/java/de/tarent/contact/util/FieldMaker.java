/* $Id: FieldMaker.java,v 1.4 2007/01/05 16:57:23 asteban Exp $
 * 
 * Created on 07.07.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.util;

/**
 * Diese Klasse dient dem Erzeugen von Pseudofeldern.
 * 
 * @author mikel
 */
public class FieldMaker {
    // Briefanrede
    /**
     * Diese Methode erzeugt aus den Anrede-Teilfeldern des Datenbestandes
     * eine Briefanrede.
     * 
     * @param anrede Anrede wie 'Sehr geehrter'
     * @param herrFrau Anredetitel wie 'Herr'
     * @param akadTitel akademischer Titel wie 'Dr.'
     * @param nachname Nachmane wie 'M�ller'
     * @return vollst�ndige Briefanrede wie 'Sehr geehrter Herr Dr. M�ller'
     */
    public static String createLetterSalutation(String anrede, String herrFrau, String akadTitel, String nachname) {
        StringBuffer buffer = new StringBuffer();

        if (anrede != null)
            buffer.append(anrede).append(' ');
        if (herrFrau != null)
            buffer.append(herrFrau).append(' ');
        if (akadTitel != null)
            buffer.append(akadTitel).append(' ');
        if (nachname != null)
            buffer.append(nachname).append(' ');

        removeDoubleSpaces(buffer);
        crToBRTag(buffer);                        

        return buffer.toString().trim();
    }

    public final static String LABEL_INVALID = "N.A.\n\n\n\n\n";

    /**
     * Diese Methode erzeugt ein Adressfeld aus den �bergebenen Daten. Hierbei
     * hat eine m�gliche Postfachadresse Vorzug gegen�ber einer Stra�enadresse.
     *   
     * @param herrFrau 'Herr', 'Frau', ...
     * @param titel beruflicher Titel, etwa 'MdB'
     * @param akadTitle akademischer Titel, etwa 'Dr.'
     * @param vorname Vorname
     * @param nachname Nachname
     * @param namenszusatz Namenszusatz
     * @param institution Institutionsname
     * @param stra�e Stra�e
     * @param hausNr Hausnummer
     * @param plz Postleitzahl
     * @param stadt Stadt
     * @param plzPostfach Postleitzahl der Postfachadresse
     * @param postfach Postfach
     * @param land Land
     * @return Adresslabel der fertige zusammengesetzte Label.
     */
    public static String createLabel(String herrFrau, String titel, String akadTitle, String vorname, String nachname, String namenszusatz, String institution, String strasse, String hausNr, String plz, String stadt, String plzPostfach, String postfach, String land) {
        StringBuffer buffer = new StringBuffer();
        
        postfach = arrangeAsPairs(postfach);

        if (sufficientForPOBoxAddress(postfach, plzPostfach)) {
            // Postfach-Adressen
            if (sufficientForInstitutionAddress(institution))
                if (sufficientForPersonAddress(nachname)) {
                    // Briefkopf f�r eine Institution und eine Person mit Postfach
                    // 3.Zeile: eigene Funktion wird aufgerufen f�r Herrn_Frau
                    // 4.Zeile: Akad_Titel gefolgt von Vorname und Nachname, jeweils mit leerzeichen getrennt
                    // 5.Zeile: Institution
                    // 6.Zeile: Das Wort "Postfach" gefolgt von einem Leerzeichen und der Postfach_Nr
                    // 7.Zeile: Leerzeile
                    // 8.Zeile: PLZ_Postfach gefolgt von einem Leerzeichen und dem Ort
                    // 9.Zeile: Hier steht die Abfrage f�r ein Land au�er Deutschland; ansonsten Leerzeile
                    buffer.append(createAddressSalutation(herrFrau, titel)).append('\n')
                          .append(createAddressName(akadTitle, vorname, nachname, namenszusatz)).append('\n')
                          .append(institution).append('\n')
                          .append("Postfach ").append(postfach).append("\n\n")
                          .append(plzPostfach).append(' ').append(createAddressCity(land, stadt)).append('\n')
                          .append(createAddressCountry(land));
                } else {
                    // Briefkopf f�r eine Institution mit Postfach:
                    // 3.Zeile: Institution
                    // 4.Zeile: Das Wort "Postfach" gefolgt von einem Leerzeichen und der PLZ_Postfach
                    // 5.Zeile: Leerzeile
                    // 6.Zeile: PLZ_Postfach folgt von einem Freizeichen und dem Ort
                    // 7.Zeile: Hier steht die Abfrage f�r ein Land au�er Deutschland; ansonsten Leerzeile
                    // 8.Zeile: Leerzeile
                    // 9.Zeile: Leerzeile
                    buffer.append(createAddressInstitution(institution, namenszusatz)).append('\n')
                          .append("Postfach ").append(postfach).append("\n\n")
                          .append(plzPostfach).append(' ').append(createAddressCity(land, stadt)).append('\n')
                          .append(createAddressCountry(land)).append("\n\n");
                }
            else if (sufficientForPersonAddress(nachname)) {
                // Briefkopf f�r eine Person mit Postfach
                // 3.Zeile: eigene Funktion wird aufgerufen f�r Herrn_Frau
                // 4.Zeile: Akad_Titel gefolgt von Vorname und Nachname, jeweils mit leerzeichen getrennt
                // 5.Zeile: Das Wort "Postfach" gefolgt von einem Leerzeichen und der PLZ_Postfach
                // 6.Zeile: Leerzeile
                // 7.Zeile: PLZ_Postfach folgt von einem Freizeichen und dem Ort
                // 8.Zeile: Hier steht die Abfrage f�r ein Land au�er Deutschland; ansonsten Leerzeile
                // 9.Zeile: Leerzeile
                buffer.append(createAddressSalutation(herrFrau, titel)).append('\n')
                      .append(createAddressName(akadTitle, vorname, nachname, namenszusatz)).append('\n')
                      .append("Postfach ").append(postfach).append("\n\n")
                      .append(plzPostfach).append(' ').append(createAddressCity(land, stadt)).append('\n');
            } else {
                // Keine Institution und keine Person --> eigene Funktion
                // 3.Zeile: "N.A."
                // 4.Zeile: Leerzeile
                // 5.Zeile: Leerzeile
                // 6.Zeile: Leerzeile
                // 7.Zeile: Leerzeile
                // 8.Zeile: Leerzeile
                // 9.Zeile: Leerzeile
                buffer.append(LABEL_INVALID);
            }
        } else {
            // Stra�enadresse
            if (sufficientForInstitutionAddress(institution))
                if (sufficientForPersonAddress(nachname)) {
                    // Briefkopf f�r eine Institution und eine Person mit Adresse
                    // 3.Zeile: eigene Funktion wird aufgerufen f�r Herrn_Frau
                    // 4.Zeile: Akad_Titel gefolgt vom Vornamen und Nachnamen, jeweils getrennt durch ein Leerzeichen
                    // 5.Zeile: Institution
                    // 6.Zeile: Stra�e gefolgt von einem Leerzeichen und der Hausnummer
                    // 7.Zeile: Leerzeile
                    // 8.Zeile: PLZ gefolgt von einem Leerzeichen und dem Ort
                    // 9.Zeile: Hier steht die Abfrage f�r ein Land au�er Deutschland; ansonsten Leerzeile
                    buffer.append(createAddressSalutation(herrFrau, titel)).append('\n')
                          .append(createAddressName(akadTitle, vorname, nachname, namenszusatz)).append('\n')
                          .append(institution).append('\n')
                          .append(strasse).append(' ').append(hausNr).append("\n\n")
                          .append(plz).append(' ').append(createAddressCity(land, stadt)).append('\n')
                          .append(createAddressCountry(land));
                } else {
                    // Briefkopf f�r eine Institution mit Adresse
                    // 3.Zeile: Institution
                    // 4.Zeile: Stra�e gefolgt von einem Leerzeichen und der Hausnummer
                    // 5.Zeile: Leerzeichen
                    // 6.Zeile: PLZ gefolgt von einem Leerzeichen und dem Ort
                    // 7.Zeile: Hier steht die Abfrage f�r ein Land au�er Deutschland; ansonsten Leerzeile
                    // 8.Zeile: Leerzeile
                    // 9.Zeile: Leerzeile
                    buffer.append(createAddressInstitution(institution, namenszusatz)).append('\n')
                          .append(strasse).append(' ').append(hausNr).append("\n\n")
                          .append(plz).append(' ').append(createAddressCity(land, stadt)).append('\n')
                          .append(createAddressCountry(land)).append("\n\n");
                }
            else if (sufficientForPersonAddress(nachname)) {
                // Briefkopf f�r eine Person mit Adresse
                // 3.Zeile: eigene Funktion wird aufgerufen f�r Herrn_Frau
                // 4.Zeile: Akad_Titel gefolgt vom Vornamen und Nachnamen, jeweils getrennt durch ein Leerzeichen
                // 5.Zeile: hier findet eine Abfrage auf einen Untermieter statt --> siehe die beiden M�glichkeiten
                // 6.Zeile: abh�ngig von der Abfrage ???
                // 7.Zeile: abh�ngig von der Abfrage ???
                // 8.Zeile: abh�ngig von der Abfrage ???
                // 9.Zeile: abh�ngig von der Abfrage ???
                buffer.append(createAddressSalutation(herrFrau, titel)).append('\n')
                      .append(createAddressName(akadTitle, vorname, nachname, namenszusatz)).append('\n')
                      .append(strasse).append(' ').append(hausNr).append("\n\n")
                      .append(plz).append(' ').append(createAddressCity(land, stadt)).append('\n')
                      .append(createAddressCountry(land)).append('\n');
            } else {
                // Keine Institution und keine Person --> eigene Funktion
                // 3.Zeile: "N.A."
                // 4.Zeile: Leerzeile
                // 5.Zeile: Leerzeile
                // 6.Zeile: Leerzeile
                // 7.Zeile: Leerzeile
                // 8.Zeile: Leerzeile
                // 9.Zeile: Leerzeile
                buffer.append(LABEL_INVALID);
            }
        }

        removeDoubleSpaces(buffer);
        crToBRTag(buffer);

        return buffer.toString().trim();
    }

    /**
     * Diese Methode entfernt doppelte Leerzeichen in einem StringBuffer.
     * 
     * @param buffer der zu bearbeitende Buffer.
     * @return buffer.
     */
    public static StringBuffer removeDoubleSpaces(StringBuffer buffer) {
        if (buffer != null) {
            boolean lastCharWasSpace = false;
            int index = 0;
            while (index < buffer.length())
                if (!lastCharWasSpace)
                    lastCharWasSpace = (buffer.charAt(index++) == ' ');
                else if (buffer.charAt(index) == ' ')
                    buffer.deleteCharAt(index);
                else {
                    index++;
                    lastCharWasSpace = false;
                }
        }
        return buffer;
    }

    /**
     * Diese Methode ersetzt in einem StringBuffer '\n' durch "<br>".
     *  
     * @param buffer der zu bearbeitende Buffer.
     * @return buffer.
     */
    public static StringBuffer crToBRTag(StringBuffer buffer) {
        if (buffer != null) {
            int index = 0;
            while ((index = buffer.indexOf("\n", index)) >= 0) {
                buffer.replace(index, index + 1, "<br>");
            }
        }
        return buffer;
    }

    /**
     * Diese Methode arrangiert die druckbaren Zeichen des Originalstrings
     * in Leerzeichen-getrennte Paare. Bei ungeraden Anzahlen steht das
     * einzelne Zeichen vorne. Als druckbar werden alle Zeichen mit einem
     * Code gr��er dem des Leerzeichens verstanden.
     * 
     * @param orig der zu bearbeitende String, etwa ' 123    45'
     * @return die angeordnete Darstellung, etwa '1 23 45'
     */
    public static String arrangeAsPairs(String orig) {
        StringBuffer buffer = new StringBuffer();
        
        if (orig != null)
            for (int i = orig.length() - 1; i >= 0; i--)
                if (orig.charAt(i) > ' ') {
                    if (buffer.length() % 3 == 2)
                        buffer.insert(0, ' ');
                    buffer.insert(0, orig.charAt(i));
                }
        
        return buffer.toString();
    }

    /*
     * Erstellt Adresskopf-Fragmente
     */
    /**
     * Diese Methode liefert eine Adresskopfanrede.
     * 
     * @param herrFrau 'Herr', 'Frau, ...
     * @param titel Arbeitstitel, etwa 'MdB'
     * @return Adresskopfanrede, etwa 'Herrn MdB'
     */
    public static String createAddressSalutation(String herrFrau, String titel) {
        if (titel == null)
            titel = "";
        if (herrFrau == null)
            return titel;

        herrFrau = herrFrau.trim();
        if (herrFrau.equalsIgnoreCase("Frau/Herr"))
            return ("Damen und Herren " + titel).trim();
        if (herrFrau.equalsIgnoreCase("Herr"))
            return ("Herrn " + titel).trim();
        return (herrFrau + ' ' + titel).trim();
    }

    /**
     * Diese Methode erzeugt die Namenzeile eines Adresskopfes.
     * 
     * @param akadTitle akademischer Titel
     * @param vorname Vorname
     * @param nachname Nachname
     * @param namenszusatz Namenszusatz
     * @return Zusammensetzung der Parameter, wobei der Namenszusatz
     *  mit '\n' abgetrennt ist.
     */
    public static String createAddressName(String akadTitle, String vorname, String nachname, String namenszusatz) {
        StringBuffer buffer = new StringBuffer();
        if (akadTitle != null)
            buffer.append(akadTitle).append(' ');
        if (vorname != null)
            buffer.append(vorname).append(' ');
        if (nachname != null)
            buffer.append(nachname);
        if (namenszusatz != null && namenszusatz.length() == 0)
            buffer.append('\n').append(namenszusatz);
        return buffer.toString().trim();
    }

    /**
     * Diese Methode erzeugt eine erweiterte Institutionszeile eines
     * Adresskopfes.
     * 
     * @param institution Institution
     * @param namenszusatz Namenszusatz
     * @return Zusammensetzung der Parameter, mit '\n' getrennt
     */
    public static String createAddressInstitution(String institution, String namenszusatz) {
        if (institution == null)
            institution = "";
        return (namenszusatz == null || namenszusatz.length() == 0) ?
            institution : institution + '\n' + namenszusatz.trim();
    }

    /**
     * Diese Methode liefert den Stadt-Eintrag eines Adresskopfes.
     * 
     * @param land Land der Adresse
     * @param stadt Stadt der Adresse
     * @return die Stadt, falls au�erhalb Deutschlands in Gro�buchstaben
     *  konvertiert.
     */
    public static String createAddressCity(String land, String stadt) {
        if (stadt == null)
            return "";
        return ((land == null || land.length() == 0 || 
            "D".equals(land) || "Deutschland".equalsIgnoreCase(land)) ?
            stadt : stadt.toUpperCase()).trim();
    }

    /**
     * Diese Methode liefert den Land-Eintrag eines Adresskopfes.
     * 
     * @param land Land der Adresse
     * @return das Land in Gro�buchstaben, wenn nicht in Deutschland,
     *  sonst leer.
     */
    public static String createAddressCountry(String land) {
        return (land == null || land.length() == 0 || 
            "D".equals(land) || "Deutschland".equalsIgnoreCase(land)) ?
            "" : land.trim().toUpperCase();
    }

    /*
     * Tests von Adressdaten auf G�ltigkeit f�r gewisse Zwecke.
     */
    /**
     * Diese Methode testet, ob die �bergebenen Daten f�r eine Postfach-Angabe
     * ausreichen.
     * 
     * @param postfach Postfach-Nummer
     * @param plz Postfach-PLZ
     * @return <code>true</code>, falls beide Parameter nicht leer und
     *  nicht '0' sind.
     */
    public static boolean sufficientForPOBoxAddress(String postfach, String plz) {
        if (postfach == null || plz == null)
            return false;
        postfach = postfach.trim();
        if (postfach.length() == 0 || "0".equals(postfach))
            return false;
        plz = plz.trim();
        return plz.length() > 0 && !"0".equals(plz);
    }

    /**
     * Diese Methode ermittelt, ob eine Adresse f�r eine Personenangabe
     * in einem Label ausreichende Daten enth�lt.
     * 
     * @param nachname Nachname der Adresse.
     * @return <code>true</code>, wenn der Nachname nicht leer ist.
     */
    public static boolean sufficientForPersonAddress(String nachname) {
        return nachname != null && nachname.trim().length() > 0;
    }
    
    /**
     * Diese Methode ermittelt, ob eine Adresse f�r eine Institutionsangabe
     * in einem Label ausreichende Daten enth�lt.
     * 
     * @param institution Institution der Adresse.
     * @return <code>true</code>, wenn die Institution nicht leer ist.
     */
    public static boolean sufficientForInstitutionAddress(String institution) {
        return institution != null && institution.trim().length() > 0;
    }
}
