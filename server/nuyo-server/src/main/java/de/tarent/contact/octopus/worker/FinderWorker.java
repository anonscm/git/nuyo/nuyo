/* $Id: FinderWorker.java,v 1.4 2006/09/15 13:00:03 kirchner Exp $
 * 
 * Created on 30.06.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.worker;

import java.util.Iterator;
import java.util.Map;

import de.tarent.contact.octopus.DBDataAccessPostgres;
import de.tarent.contact.octopus.DataAccess;
import de.tarent.contact.octopus.logging.Lg;
import de.tarent.contact.octopus.worker.constants.FinderWorkerConstants;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcContentWorker;
import de.tarent.octopus.content.TcMessageDefinition;
import de.tarent.octopus.content.TcOperationDefinition;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;
import de.tarent.octopus.util.CVS;

/**
 * Dieser Worker liefert Adressdaten so, wie tarentFinder-Implementierungen
 * sie fordern.
 * 
 * @author mikel
 * @deprecated unbedingt nach TcReflectedWorker portieren, wir brauchen nur noch die Actions "getFinderServiceURL" und "getRawTntAddresses"
 */
public class FinderWorker implements TcContentWorker, FinderWorkerConstants {

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#init(de.tarent.octopus.config.TcModuleConfig)
	 */
	public void init(TcModuleConfig arg) {
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#doAction(de.tarent.octopus.config.TcConfig, java.lang.String, de.tarent.octopus.request.TcRequest, de.tarent.octopus.content.TcContent)
	 */
	public String doAction(
		TcConfig tcConfig,
		String actionName,
		TcRequest tcRequest,
		TcContent tcContent)
		throws TcContentProzessException {

        String result = RESULT_error;
        DataAccess dataAccess = new DBDataAccessPostgres();

        if (ACTION_GET_RAW_TNT_ADDRESSES.equals(actionName)) {
            String usrID = tcConfig.getPersonalConfig().getUserID().toString();


            StringBuffer out = new StringBuffer();

            //Anfrage bearbeiten
            String sQueryString;
            Lg.WORKER(tcRequest.getModule()).info(tcRequest.toString());
            sQueryString = (String) tcRequest.getParam(PARAM_COMMAND);
            if (sQueryString == null || sQueryString.length() == 0)
                return RESULT_ok;
            Lg.WORKER(tcRequest.getModule()).debug("Query bearbeiten: " + sQueryString);
            Lg.WORKER(tcRequest.getModule()).debug("Query (G) bearbeiten: " + sQueryString.toUpperCase());

            if (COMMAND_CHECK_URL.equalsIgnoreCase(sQueryString)) {
                tcContent.setField(
                                   FIELD_RAW_RESULT,
                                   "Webservice ist erreichbar.");
            } 
            else if (COMMAND_GET_CATEGORIES.equalsIgnoreCase(sQueryString)) {
                Map categories = dataAccess.getGruppen(tcConfig.getLoginname());
                //XML zusammenbauen.
                Iterator it = categories.keySet().iterator();
                out.append(
                           "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n");
                //out.append("<dataset dataformat=\"tarent.xml.address\" header=\"yes\" rc=\"\" ps=\"\" pn=\"\">\n");
                out.append("<dataset>\n");
                out.append("<R>");
                String vgname;
                while (it.hasNext()) {
                    out.append("<F>");
                    vgname = it.next().toString();
                    out.append(escape(vgname));
                    out.append(" - ");
                    out.append(escape(categories.get(vgname).toString()));
                    out.append("</F>");
                }
                out.append("</R>");
                out.append("</dataset>");
                tcContent.setField(FIELD_RAW_RESULT, out.toString());
            } 
            else if (COMMAND_GET_SUB_CATEGORIES.equalsIgnoreCase(sQueryString)) {

                if (tcRequest.getParam(PARAM_CATEGORY) == null)
                    throw new TcContentProzessException("Der Parameter "+PARAM_CATEGORY+" muss übergeben werden.");                
                String category = tcRequest.getParam(PARAM_CATEGORY).toString();                    
                out.append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n");                    
                out.append("<dataset>\n");
                out.append("<R>");
                    
                //XML zusammenbauen.
                Map subCategories = dataAccess.getVerteiler(tcConfig.getLoginname(), category);
                Iterator it = subCategories.keySet().iterator();
                String vgname;
                while (it.hasNext()) {
                    out.append("<F>");
                    vgname = it.next().toString();
                    out.append(escape(vgname));
                    out.append(" ");
                    out.append(escape(subCategories.get(vgname).toString()));
                    out.append("</F>");
                }

                out.append("</R>");
                out.append("</dataset>");
                tcContent.setField(FIELD_RAW_RESULT, out.toString());

            } else { 
                tcContent.setField(FIELD_RAW_RESULT,
                                   dataAccess.getAdresses(sQueryString, usrID));
            }
            result = RESULT_ok;
        } else if(ACTION_GET_FINDERSERVICEURL.equals(actionName)){
            //Liefert die URL zum FinderService
            String encodedUrl = tcRequest.getParamAsString("encodedUrl");
            //try to determine module
            String module = tcRequest.getParamAsString("module");
            //try to determine FinderService-Task
            String task = tcConfig.getModuleConfig().getParam("FinderService");
            //build URL
            String url = encodedUrl + "?module=" + module + "&task=" + task;
            Lg.WORKER(tcRequest.getModule()).info("FinderServiceURL: " + url);
            tcContent.setField(FIELD_FINDERSERVICEURL, url);
            result = RESULT_ok;
        } else
            throw new TcContentProzessException(
                                                "Nicht unterstützte Aktion im Worker 'FinderWorker': "
                                                + actionName);

        return result;
	}

	/**
	 * @see de.tarent.octopus.content.TcContentWorker#getWorkerDefinition()
	 */
	public TcPortDefinition getWorkerDefinition() {
		TcPortDefinition port =
			new TcPortDefinition(
				"de.tarent.contact.octopus.MapFetcher",
				"Worker zum Beziehen von Java Map's.");

		TcOperationDefinition operation =
			port.addOperation(
				ACTION_GET_RAW_TNT_ADDRESSES,
				"Liefert einen rohen XML-Strom mit Adressdaten nach Finder-Anforderungen.");
		operation.setInputMessage();
		operation.setOutputMessage().addPart(
			FIELD_RAW_RESULT,
			TcMessageDefinition.TYPE_STRUCT,
			"Auflistung aller vorhandenen Anreden.");
		operation.addFaultMessage(
			RESULT_error,
			"Die Bearbeitung verlief fehlerhaft.");
		return port;
	}

	/*
	 * Konstanten 
	 */

	/*
	 * private Hilfsmethoden
	 */
	/** Diese Klassenmethode ersetzt die Zeichen in <code>source</code>, die in
	 *  XML-Dateien speziellen Bedeutung haben, durch entsprechende Entitäten.
	 * 
	 *  @param source der String, dessen Zeichen umgesetzt werden sollen.
	 *  @return der String, in dem die speziellen Zeichen durch Entitäten ersetzt
	 *    wurden.
	 */
	private static String escape(String source) {
		StringBuffer buffer = new StringBuffer();
		if (source != null) {
			for (int index = 0; index < source.length(); index++) {
				switch (source.charAt(index)) {
					case '&' :
						buffer.append("&amp;");
						break;
					case '<' :
						buffer.append("&lt;");
						break;
					case '>' :
						buffer.append("&gt;");
						break;
					case '"' :
						buffer.append("&quot;");
						break;
					default :
						buffer.append(source.charAt(index));
				}
			}
		}
		return buffer.toString();
	}

	/**
	 * Diese Methode liefert einen Versionseintrag.
	 * 
	 * @return Version des Workers.
	 * @see de.tarent.octopus.content.TcContentWorker#getVersion()
	 */
	public String getVersion() {
		return CVS.getContent("$Revision: 1.4 $")
			+ " ("
			+ CVS.getContent("$Date: 2006/09/15 13:00:03 $")
			+ ')';
	}

}
