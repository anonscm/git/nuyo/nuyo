package de.tarent.contact.octopus;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.AbstractStatement;
import de.tarent.dblayer.sql.statement.Select;

/**
 * @author kleinw
 * 
 * Hier werden alle select - Anweisungen erstellt.
 *  
 */
public class AddressSelect {

    private Map _parameter;
    private String _category;
    private List<Object> _subCategoriesSelected;
    private Boolean _intersect;

    static final private String[] commTypesNames = {"tel_d", "tel_p", "fax_d", "fax_p", "email"};
    static final private String[] commTypesValues = {"102", "101", "104", "103", "108"};
    
    public AddressSelect(Map parameter) {
        _parameter = parameter;
    }

    public Select getSelect(boolean tcomm) throws AddressSelectException {
        _category = (String) _parameter.get("verteilergruppe");
        if (_category == null) {
            _category = (String)_parameter.get("vertgrp");
            if (_category == null) {
                throw new AddressSelectException ("Eine Kategorie muss angegeben sein.");
            }
        }
        _subCategoriesSelected = new Vector<Object>();
        for (Iterator it = _parameter.keySet().iterator(); it.hasNext();) {
            String key = (String) it.next();
            if (key.startsWith("selVert")) {
                _subCategoriesSelected.add(_parameter.get(key));
            }
        }
        if (_subCategoriesSelected.size() == 0 && _category == null) { throw new AddressSelectException(
                "Zur Auswahl von Adressen m�ssen eine oder mehrere Unterkategorien oder eine Kategorie angegeben sein."); }

        _intersect = (Boolean)_parameter.get("schnitt");

        WhereList wl = new WhereList();
        Select select = SQL.SelectDistinct(TcDBContext.getDefaultContext())
    	.select("taddress.pk_address")
    	.select("taddressext.lastname_m")
    	.from(TcDBContext.getSchemaName() + "taddress")
    	.join(TcDBContext.getSchemaName() + "taddressfolder", "taddressfolder.fk_address", "taddress.pk_address")
    	.join(TcDBContext.getSchemaName() + "taddressext", "taddressext.pk_addressext", "taddress.pk_address")
    	.join(TcDBContext.getSchemaName() + "taddresssubfolder", "taddress.pk_address",
            "taddresssubfolder.fk_address");
        wl.addAnd(Expr.equal("taddressfolder.fk_folder", _category));
        
        if (_intersect != null && _intersect.booleanValue()) {
            for (Iterator it = _subCategoriesSelected.iterator();it.hasNext();) {
                AbstractStatement sub = 
                    SQL.Select(TcDBContext.getDefaultContext())
                    	.select("taddresssubfolder.fk_address")
                    	.from(TcDBContext.getSchemaName() + "taddresssubfolder")
                    	.where(
                    			Where.and(Expr.equal("taddresssubfolder.fk_folder", _category),
                    	Expr.equal("taddresssubfolder.fk_subfolder", (it.next()))));
                wl.addAnd(Expr.in(TaddressDB.PK_PKADDRESS, sub));
            }
        }
        else {
            if (_subCategoriesSelected.size() > 0) {
            	wl.addAnd(Expr.in(TaddresssubcategoryDB.FKSUBCATEGORY, _subCategoriesSelected));
            }
        }
        
        if (tcomm) {
            for (int i=1;i<=commTypesNames.length;i++) {
                if (_parameter.containsKey(commTypesNames[i-1])) {
                    if (((String)_parameter.get(commTypesNames[i-1])).length() > 0) {
                        String name = commTypesNames[i-1];
                        String value = (String)_parameter.get(name);
                        Where expr = null;
                        if (value.indexOf("*") != -1) {
                            value = value.replace('*', '%');
                            expr = Expr.like(SQL.Function(TcDBContext.getDefaultContext(), "lower").parameter(name + ".value").toString(), value);
                        }else{
                        	expr = Expr.equal(SQL.Function(TcDBContext.getDefaultContext(), "lower").parameter(name + ".value").toString(), value);
                        }
                        select.join(TcDBContext.getSchemaName() + "tcomm " + commTypesNames[i-1], name + ".fk_address", TaddressDB.PK_PKADDRESS);
                        wl.addAnd(Expr.equal(name+".fk_commtype", commTypesValues[i-1]));
                        wl.addAnd(expr);
                    }
                }
            }
        }
        select.where(wl);
        select.orderBy(Order.asc(TaddressextDB.LASTNAMEM));
        return select;
    }
    
    static public class AddressSelectException extends Exception {
		private static final long serialVersionUID = -8087690648308871547L;

		public AddressSelectException(String msg) {
            super(msg);
        }

    }
}
