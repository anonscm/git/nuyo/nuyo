package de.tarent.contact.octopus.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tarent.contact.bean.TcommDB;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;

/**
 * @author kleinw
 * 
 * Hier entsteht die Datenbankfunktionalität für Tcomm Daten.
 *  
 */
public class TcCommDB {

    public static void saveCommData(Integer addressPk, List standardAddress, int index,
        int type) throws SQLException {
        Insert insert;
        if (standardAddress.get(index) != null) {
            insert = SQL.Insert(TcDBContext.getDefaultContext()).insert(TcommDB.FKADDRESS, addressPk)
                .insert(TcommDB.VALUE, (String) standardAddress.get(index))
                .insert(TcommDB.FKCOMMTYPE, new Integer(type))
                .table(TcommDB.getTableName());

            DB.update(TcDBContext.getDefaultContext(), insert.toString());
            
        }
    }
    
    public static void saveCommData(Integer addressPk, Map changeValues) throws SQLException {
        
        Result tr = null;
        
        Select select =
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TcommDB.getTableName())
            	.selectAs(TcommDB.FKCOMMTYPE)
            	.selectAs(TcommDB.VALUE)
            	.where(Expr.equal(TcommDB.FKADDRESS, addressPk))
            	.orderBy(Order.asc(TcommDB.FKCOMMTYPE));
        
        
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            int commType = 0;
            Map cTypes = new HashMap();
            List commTypeValues = new ArrayList();
            while (tr.resultSet().next()) {
                commType = tr.resultSet().getInt(TcommDB.FKCOMMTYPE);
                commTypeValues.add(tr.resultSet().getString(TcommDB.VALUE));
                if (tr.resultSet().next()){
                    if (tr.resultSet().getInt(TcommDB.FKCOMMTYPE) != commType) {
                        cTypes.put(new Integer(commType), commTypeValues);
                        commTypeValues = new ArrayList();
                    }
                    tr.resultSet().previous();
                } else {
                    cTypes.put(new Integer(commType), commTypeValues);
                }
            }
            
            for (Iterator it = changeValues.keySet().iterator();it.hasNext();) {
                Integer key = (Integer)it.next();

                if (changeValues.get(key) != null) {
                	WhereList wl2 = new WhereList();
                	wl2.addAnd(Expr.equal(TcommDB.FKADDRESS, addressPk));
                	wl2.addAnd(Expr.equal(TcommDB.FKCOMMTYPE, key));
                    Delete delete =
                        SQL.Delete(TcDBContext.getDefaultContext())
                        	.from(TcommDB.getTableName())
                        	.where(wl2);
                    
                    if (cTypes.containsKey(key)) {
                        //	Wenn der key exsitiert, gibt es mindestens einen 
                        //	Eintrag in Tcomm.
                        List list = (List)cTypes.get(key);
                        if (list.size() == 1) {
                            //	genau ein Eintrag, also Update
                            if (changeValues.get(key).toString().length() == 0) {
                                DB.update(TcDBContext.getDefaultContext(), delete.toString());
                            } else {
                                if (!((String)list.get(0)).equals(changeValues.get(key).toString())) {
                                	WhereList wl = new WhereList();
                                	wl.addAnd(Expr.equal(TcommDB.FKADDRESS, addressPk));
                                	wl.addAnd(Expr.equal(TcommDB.FKCOMMTYPE, key));
    	                            Update update =
    	                                SQL.Update(TcDBContext.getDefaultContext())
    	                                	.table(TcommDB.getTableName())
    	                                	.update(TcommDB.VALUE, (String)changeValues.get(key))
    	                                	.where(wl);
    	                            
    	                            DB.update(TcDBContext.getDefaultContext(), update.toString());
                                }
                            }
                        } else {
                            //	Mehrdeutigkeit bei Einträgen, also Delete und Insert.
                            DB.update(TcDBContext.getDefaultContext(), delete.toString());
                            
                            if (changeValues.get(key).toString().length() > 0) {
                                Insert insert =
                                    SQL.Insert(TcDBContext.getDefaultContext())
                                    	.table(TcommDB.getTableName())
                                    	.insert(TcommDB.FKCOMMTYPE, key)
                                    	.insert(TcommDB.FKADDRESS, addressPk)
                                    	.insert(TcommDB.VALUE, (String)changeValues.get(key));

                                DB.update(TcDBContext.getDefaultContext(), insert.toString());
                                
                            }
                        }                     
                    } else {
                        //	Es gibt keinen Eintrag, also Insert.
                        if (changeValues.get(key).toString().length() > 0) {
                            Insert insert =
                                SQL.Insert(TcDBContext.getDefaultContext())
                                	.table(TcommDB.getTableName())
                                	.insert(TcommDB.FKCOMMTYPE, key)
                                	.insert(TcommDB.FKADDRESS, addressPk)
                                	.insert(TcommDB.VALUE, (String)changeValues.get(key));
                            
                            DB.update(TcDBContext.getDefaultContext(), insert.toString());
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null)
                tr.closeAll();
        }
    }
    
}
