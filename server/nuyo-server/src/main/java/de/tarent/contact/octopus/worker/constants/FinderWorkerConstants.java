package de.tarent.contact.octopus.worker.constants;


/**
 * @author kleinw
 *
 *	Konstanten f�r den FinderWorker.
 *
 */
public interface FinderWorkerConstants {

	public final static String ACTION_GET_RAW_TNT_ADDRESSES =
		"getRawTntAddresses";
	public final static String ACTION_GET_FINDERSERVICEURL = "getFinderServiceURL";
	public final static String COMMAND_CHECK_URL = "checkurl";
	public final static String COMMAND_GET_CATEGORIES = "getvgroups";
	public final static String COMMAND_GET_SUB_CATEGORIES = "getverteiler";
	public final static String FIELD_RAW_RESULT = "RawResult";
	public final static String FIELD_FINDERSERVICEURL = "FinderServiceURL";
	public final static String PARAM_CATEGORY = "group";
	public final static String PARAM_COMMAND = "cmd";
	public final static String PARAM_USER = "usr";

}
