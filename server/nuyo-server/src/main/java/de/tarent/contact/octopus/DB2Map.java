/* $Id: DB2Map.java,v 1.2 2006/03/16 13:49:30 jens Exp $
 * 
 */

package de.tarent.contact.octopus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Diese Klasse beinhaltet Methoden und Konstanten
 *  für das Mapping von Spaltennamen und den
 *  korrespondierenden Schlüsselwerten sowie Methoden
 *  zur Übersetzung von ResultSets in Hashmaps.
 *
 * @author kleinw
 */
public class DB2Map {
	public final static Logger logger =
		Logger.getLogger(DB2Map.class.getName());

	/**
	 * Mapping Spaltennamen von TADDRESS auf Map-Schlüssel
	 */
	static public final String[][] DATABASE_FIELDS_TADDRESS =
		{ { "SALUTATION", "a1" }, {
			"TITELJOB", "a2" }, {
			"TITELACADEMIC", "a3" }, {
			"FIRSTNAME", "a4" }, {
			"LASTNAME", "a5" }, {
			"NAMESUFFIX", "a6" }, {
			"ORGANISATION", "a7" }, {
			"STREET", "a8" }, {
			"HOUSENO", "a9" }, {
			"ZIPCODE", "a10" }, {
			"CITY", "a11" }, {
			"POBOXZIPCODE", "a12" }, {
			"POBOX", "a13" }, {
			"SUBSCRIPTOR", "a16" }, {
			"SUBSCRIPTION", "a17" }, {
			"LETTERSALUTATION", "a18" }, {
			"COUNTRY", "a19" }, {
			"COUNTRYSHORT", "a20" }, {
			"REGION", "a21" }, {
			"CHANGEDBY", "currentUser" }, {
			"MIDDLENAME", "e1" }, {
			"NICKNAME", "e2" }, {
			"ORGANISATION2", "e3" }, {
			"DEPARTMENT", "e4" }, {
			"NOTEONADDRESS", "e5" }, {
			"POSITION", "e6" }, {
			"BANKNAME", "e7" }, {
			"BANKNO", "e8" }, {
			"BANKACCOUNT", "e9" }, {
			"SEX", "e10" }, {
			"YEAROFBIRTH", "e11" }, {
			"DATEOFBIRTH", "e12" }
	};

	/*
	 * öffentliche Methoden
	 */
	/**
	 * Diese Methode liest die Spalten der Folgezeile des Resultsets
	 * nach obigen Spaltenanmen aus und schreibt die Ergebniswerte in
	 * eine übergebene Map. Exceptions der Datenbankschicht werden
	 * hier abgefangen und verworfen.
	 * 
	 * @param rs ResultSet, positioniert vor den auszulesenden Datensatz.
	 * @param map Map, in die hinein ausgelesen werden soll; wenn sie
	 *  <code>null</code> ist, wird eine erstellt.
	 * @return Ergebnis-Map. 
	 */
	public Map ResultSetToMap(ResultSet rs, Map map) {
		try {
			if (rs.next())
				map = readResultSetRow(rs, map, DATABASE_FIELDS_TADDRESS);
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Fehler beim Einlesen des Datensatzes", e);
		}
		return map;
	}

	/**
	 * Diese Methode liest den aktuellen Datensatz des ResultSets nach
	 * einem übergebenen Mapping Spaltennamen -> Schlüssel aus und legt
	 * sie in einer Map ab.
	 * 
	 * @param rs ResultSet, positioniert auf den auszulesenden Datensatz.
	 * @param target Map, in die hinein ausgelesen werden soll; wenn sie
	 *  <code>null</code> ist, wird eine erstellt.
	 * @param fields Array von Arrays, in denen Tabellenspaltenname und
	 *  Map-Schlüssel stehen.
	 * @return Ergebnis-Map
	 * @throws SQLException weitergereichte Ausnahmen des Datenzugriffs
	 *  in der JDBC-Schicht. 
	 */
	public static Map readResultSetRow(
		ResultSet rs,
		Map target,
		String[][] fields)
		throws SQLException {
		if (target == null)
			target = new TreeMap();
		for (int i = 0; i < fields.length; i++)
			target.put(fields[i][1], rs.getObject(fields[i][0]));
		return target;
	}

	/**
	 * Diese Methode liest alle folgenden Datensätze des ResultSets nach
	 * einem übergebenen Mapping Spaltennamen -> Schlüssel aus und legt
	 * sie in einer Map ab, wobei eine ausgezeichnete Spalte als Schlüssel
	 * in der Map genutzt wird.
	 * 
	 * @param rs ResultSet, positioniert vor den ersten auszulesenden Datensatz.
	 * @param target Map, in die hinein ausgelesen werden soll; wenn sie
	 *  <code>null</code> ist, wird eine erstellt.
	 * @param fields Array von Arrays, in denen Tabellenspaltenname und
	 *  Map-Schlüssel stehen.
	 * @param index Name der Spalte, deren Inhalt als Schlüssel genutzt
	 *  werden soll. 
	 * @return Ergebnis-Map von Maps, die die einzelnen Datensätze darstellen.
	 * @throws SQLException weitergereichte Ausnahmen des Datenzugriffs
	 *  in der JDBC-Schicht. 
	 */
	public static Map readResultSet(
		ResultSet rs,
		Map target,
		String[][] fields,
		String index)
		throws SQLException {
		if (target == null)
			target = new TreeMap();
		while (rs.next()) {
			target.put(rs.getObject(index), readResultSetRow(rs, null, fields));
		}
		return target;
	}

	/**
	 * Diese Methode liest alle folgenden Datensätze des ResultSets aus
	 * und legt sie als kaskadiertes Mapping ab.
	 * 
	 * @param rs ResultSet, positioniert vor den ersten auszulesenden Datensatz.
	 * @param target Map, in die hinein ausgelesen werden soll; wenn sie
	 *  <code>null</code> ist, wird eine erstellt.
	 * @param indices Array von Namen von Spalten, deren Inhalte als Schlüssel
	 *  des kaskadierten Mappings verwendet werden sollen.
	 * @param value Name der Spalte, die den Endwert des Mappings liefert.
	 * @return Ergebnis-Map, ggfs kaskadiert, die die einzelnen Datensätze
	 *  enthält.
	 * @throws SQLException weitergereichte Ausnahmen des Datenzugriffs
	 *  in der JDBC-Schicht.
	 */
	public static Map readResultSetAsMapping(
		ResultSet rs,
		Map target,
		String[] indices,
		String value)
		throws SQLException {
		if (target == null)
			target = new TreeMap();
		while (rs.next()) {
			Map inner = target;
			for (int i = 0; i < indices.length - 1; i++) {
				Object index = rs.getObject(indices[i]);
				if (index == null)
					index = "";
				if (!inner.containsKey(index))
					inner.put(index, new TreeMap());
				inner = (Map) inner.get(index);
			}
			Object index = rs.getObject(indices[indices.length - 1]);
			if (index == null)
				index = "";
			inner.put(index, rs.getObject(value));
		}
		return target;
	}
}
