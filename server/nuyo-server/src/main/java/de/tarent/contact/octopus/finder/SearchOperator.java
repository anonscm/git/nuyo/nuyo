/* $Id: SearchOperator.java,v 1.2 2006/03/16 13:49:31 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Hendrik Helwich and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.finder;

/**
 * Diese Klasse stellt einen booleschen Operator in einer Baumstruktur dar.
 * 
 * @author hendrik
 */
public class SearchOperator implements SearchAtom {

    public static final byte NOT = 0;
    public static final byte AND = 1;
    public static final byte OR = 2;

    private byte type;
    private SearchAtom right = null, left = null;

    /**
     * boolescher Operator. Unterstützt werden die Operatoren NOT, AND und OR.
     * @param type 
     */
    public SearchOperator(byte type) {
        if (type < 0 || type > 2)
            throw new RuntimeException("falscher Operator");
        this.type = type;
    }

    public SearchAtom getLeft() {
        return left;
    }

    public SearchAtom getRight() {
        return right;
    }

    public void addChild(SearchAtom atom) {
        if (left == null)
            left = atom;
        else if (type != NOT)
            if (right == null)
                right = atom;
            else {
                SearchOperator sop = new SearchOperator(type);
                sop.addChild(left);
                sop.addChild(right);
                left = sop;
                right = atom;
            }
        else
            throw new RuntimeException("Operator NOT kann nur ein Kind enthalten");
    }

    public boolean isOperator() {
        return true;
    }

    public String toString() {
        StringBuffer sql = new StringBuffer();
        if (type == NOT) {
            sql.append("NOT ");
            sql.append(left.toString());
        } else {
            sql.append("(");
            sql.append(left.toString());
            if (type == AND)
                sql.append(" AND ");
            else if (type == OR)
                sql.append(" OR ");
            sql.append(right.toString());
            sql.append(")");
        }
        return sql.toString();
    }
}
