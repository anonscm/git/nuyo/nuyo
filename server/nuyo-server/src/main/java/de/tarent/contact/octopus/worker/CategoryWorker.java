package de.tarent.contact.octopus.worker;

import java.sql.SQLException;

import de.tarent.contact.octopus.db.TcCategoryDB;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.contact.octopus.worker.constants.CategoryWorkerConstants;
import de.tarent.contact.util.ResultTransform;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.request.TcRequest;

/**
 * @author kleinw
 *  
 */
public class CategoryWorker extends BaseCategoryWorker {

	/**
	 * 
	 * @param config
	 * @param request
	 * @param content
	 * @throws SQLException
	 * @throws InputParameterException
	 */
    public void getSubCategories(TcConfig config, TcRequest request,
            TcContent content) throws SQLException, InputParameterException {
        content.setField(CategoryWorkerConstants.FIELD_SUBCATEGORIES,
                ResultTransform.toSingleString(TcCategoryDB
                        .getSubCategories(request.getRequestParameters()), request
                        .getRequestParameters()));
    }
    
}
