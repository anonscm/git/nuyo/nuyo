/* $Id: LDAPContact.java,v 1.3 2007/01/05 16:57:23 asteban Exp $
 * 
 * Created on 02.05.2003 by philipp
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.ldap;

import java.util.ArrayList;
import java.util.List;

/**
 * Objekt, welches ungef�hr das repr�sentiert, was inetOrgPerson im LDAP ist...
 * 
 * @author philipp
 */
public class LDAPContact {
	private String vorname = new String("");
	private String mittelname = new String("");
	private String nachname = new String("");
	private String spitzname = new String("");
	private String userid = new String("");
	private String arbeitOrt = new String("");
	private String arbeitPLZ = new String("");
	private String arbeitStrasse = new String("");
	private String arbeitBundesstaat = new String("");
	private String arbeitLand = new String("");
	private String arbeitFirma = new String("");
	private String arbeitJob = new String("");
	private String arbeitFax = new String("");
	private String arbeitTelefon = new String("");
	private String arbeitAbteilung = new String("");
	private String handy = new String("");
	private String pager = new String("");
	private String homeTelefon = new String("");
	private String homeStrasse = new String("");
	private String email = new String("");
	private String beschreibung = new String("");
	private List users = new ArrayList();
	private String verteilergruppe = new String("");
	
	public LDAPContact(){
	}
	
	public String toString(){
	String result = new String();
		result += "Vorname: "+vorname+"\n";
		result +="Mittlerer Name: " + mittelname + "\n";
		result +="Nachname:" + nachname + "\n";
		result +="Spitzname:" + spitzname + "\n";
		result +="UserID:" + userid + "\n"; 
		result +="Gesch�ftlich:\n";
		result +="Firma: "+ arbeitFirma + "\n";
		result +="Abteilung: " + arbeitAbteilung + "\n";
		result +="Strasse: " + arbeitStrasse +"\n";
		result +="PLZ/Ort: " + arbeitPLZ + " " + arbeitOrt + "\n";
		result +="Staat: " + arbeitBundesstaat + "\n";
		result +="Land: " + arbeitLand + "\n";
		result +="Job: " + arbeitJob + "\n";
		result +="EMail: " + email + "\n";
		result +="Telefon: " + arbeitTelefon + "\n";
		result +="Fax: " + arbeitFax + "\n";
		result +="Pager: " + pager + "\n";
		result +="Handy: " + handy + "\n";
		result +="Privat: \n";
		result +="Strasse: " + homeStrasse + "\n";
		result +="Telefon: " + homeTelefon + "\n";
		result +="\n";
		result +="Beschreibung: " + beschreibung +"\n";
		
		return result;
	}
	
	/**
	 * Getter f�r Nachname
	 * @return Nachname
	 */
	public String getNachname() {
		return nachname;
	}

	/**
	 * Getter f�r Spitzname
	 * @return Spitzname
	 */
	public String getSpitzname() {
		return spitzname;
	}

	/**
	 * Getter f�r Vorname
	 * @return Vorname
	 */
	public String getVorname() {
		return vorname;
	}

	/**
	 * Setter f�r Nachname
	 * @param string Nachname
	 */
	public void setNachname(String string) {
		nachname = string;
	}

	/**
	 * Setter f�r Spitzname
	 * @param string Spitzname
	 */
	public void setSpitzname(String string) {
		spitzname = string;
	}

	/**
	 * Setter f�r Vorname
	 * @param string Vorname
	 */
	public void setVorname(String string) {
		vorname = string;
	}

	/**
	 * Getter f�r UserID
	 * @return UserID
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * Setter f�r UserID
	 * @param string UserID
	 */
	public void setUserid(String string) {
		userid = string;
	}

	/**
	 * Getter f�r ArbeitFirma
	 * @return ArbeitFirma
	 */
	public String getArbeitFirma() {
		return arbeitFirma;
	}

	/**
	 * Getter f�r ArbeitOrt
	 * @return ArbeitOrt
	 */
	public String getArbeitOrt() {
		return arbeitOrt;
	}

	/**
	 * Getter f�r ArbeitPLZ
	 * @return ArbeitPLZ
	 */
	public String getArbeitPLZ() {
		return arbeitPLZ;
	}

	/**
	 * Getter f�r ArbeitStrasse
	 * @return ArbeitStrasse
	 */
	public String getArbeitStrasse() {
		return arbeitStrasse;
	}

	/**
	 * Setter f�r ArbeitFirma
	 * @param string ArbeitFirma
	 */
	public void setArbeitFirma(String string) {
		arbeitFirma = string;
	}

	/**
	 * Setter f�r ArbeitOrt
	 * @param string ArbeitOrt
	 */
	public void setArbeitOrt(String string) {
		arbeitOrt = string;
	}

	/**
	 * Setter f�r ArbeitPLZ
	 * @param string ArbeitPLZ
	 */
	public void setArbeitPLZ(String string) {
		arbeitPLZ = string;
	}

	/**
	 * Setter f�r ArbeitStrasse
	 * @param string ArbeitStrasse
	 */
	public void setArbeitStrasse(String string) {
		arbeitStrasse = string;
	}

	/**
	 * Getter f�r ArbeitJob
	 * @return ArbeitJob
	 */
	public String getArbeitJob() {
		return arbeitJob;
	}

	/**
	 * Getter f�r Email
	 * @return Email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter f�r ArbeitJob
	 * @param string ArbeitJob
	 */
	public void setArbeitJob(String string) {
		arbeitJob = string;
	}

	/**
	 * Setter f�r Email
	 * @param string Email
	 */
	public void setEmail(String string) {
		email = string;
	}

	/**
	 * Getter f�r ArbeitFax
	 * @return ArbeitFax
	 */
	public String getArbeitFax() {
		return arbeitFax;
	}

	/**
	 * Setter f�r ArbeitFax
	 * @param string ArbeitFax
	 */
	public void setArbeitFax(String string) {
		arbeitFax = string;
	}

	/**
	 * Getter f�r ArbeitTelefon
	 * @return ArbeitTelefon
	 */
	public String getArbeitTelefon() {
		return arbeitTelefon;
	}

	/**
	 * Setter f�r ArbeitTelefon
	 * @param string ArbeitTelefon
	 */
	public void setArbeitTelefon(String string) {
		arbeitTelefon = string;
	}

	/**
	 * Getter f�r Handy
	 * @return Handy
	 */
	public String getHandy() {
		return handy;
	}

	/**
	 * Setter f�r Handy
	 * @param string Handy
	 */
	public void setHandy(String string) {
		handy = string;
	}

	/**
	 * Getter f�r HomeTelefon
	 * @return HomeTelefon
	 */
	public String getHomeTelefon() {
		return homeTelefon;
	}

	/**
	 * Getter f�r Pager
	 * @return Pager
	 */
	public String getPager() {
		return pager;
	}

	/**
	 * Setter f�r HomeTelefon
	 * @param string HomeTelefon
	 */
	public void setHomeTelefon(String string) {
		homeTelefon = string;
	}

	/**
	 * Setter f�r Pager
	 * @param string Pager
	 */
	public void setPager(String string) {
		pager = string;
	}

	/**
	 * Getter f�r ArbeitAbteilung
	 * @return ArbeitAbteilung
	 */
	public String getArbeitAbteilung() {
		return arbeitAbteilung;
	}

	/**
	 * Setter f�r ArbeitAbteilung
	 * @param string
	 */
	public void setArbeitAbteilung(String string) {
		arbeitAbteilung = string;
	}

	/**
	 * Getter f�r Beschreibung
	 * @return Beschreibung
	 */
	public String getBeschreibung() {
		return beschreibung;
	}

	/**
	 * Setter f�r Beschreibung
	 * @param string Beschreibung
	 */
	public void setBeschreibung(String string) {
		beschreibung = string;
	}

	/**
	 * Getter f�r HomeStrasse
	 * @return HomeStrasse
	 */
	public String getHomeStrasse() {
		return homeStrasse;
	}

	/**
	 * Setter f�r HomeStrase
	 * @param string HomeStrasse
	 */
	public void setHomeStrasse(String string) {
		homeStrasse = string;
	}

	/**
	 * Getter f�r ArbeitLand
	 * @return ArbeitLand
	 */
	public String getArbeitLand() {
		return arbeitLand;
	}

	/**
	 * Setter f�r ArbeitLand
	 * @param string ArbeitLand
	 */
	public void setArbeitLand(String string) {
		arbeitLand = string;
	}

	/**
	 * Getter f�r Mittelname
	 * @return Mittelname
	 */
	public String getMittelname() {
		return mittelname;
	}

	/**
	 * Setter f�r Mittelname
	 * @param string Mittelname
	 */
	public void setMittelname(String string) {
		mittelname = string;
	}

	/**
	 * Getter f�r ArbeitBundesstaat
	 * @return ArbeitBundesstaat
	 */
	public String getArbeitBundesstaat() {
		return arbeitBundesstaat;
	}

	/**
	 * Setter f�r ArbeitBundesstart
	 * @param string ArbeitBundesstaat
	 */
	public void setArbeitBundesstaat(String string) {
		arbeitBundesstaat = string;
	}

  public List getUsers() {
    return users;
  }

  /**
   * @param vector
   */
  public void setUsers(List vector) {
    users = vector;
  }

  public String getVerteilergruppe() {
    return verteilergruppe;
  }

  /**
   * @param string
   */
  public void setVerteilergruppe(String string) {
    verteilergruppe = string;
  }

}
