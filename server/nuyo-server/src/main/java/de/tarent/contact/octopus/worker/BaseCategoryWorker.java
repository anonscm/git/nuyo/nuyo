package de.tarent.contact.octopus.worker;

import de.tarent.contact.octopus.logging.Lg;
import de.tarent.contact.octopus.worker.constants.CategoryWorkerConstants;
import de.tarent.octopus.config.TcConfig;
import de.tarent.octopus.config.TcModuleConfig;
import de.tarent.octopus.content.TcAbstractContentWorker;
import de.tarent.octopus.content.TcContent;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcPortDefinition;
import de.tarent.octopus.request.TcRequest;

/**
 * @author kleinw
 *  
 */
public class BaseCategoryWorker extends TcAbstractContentWorker implements
        CategoryWorkerConstants {

    public String getVersion() {
        return null;
    }

    public TcPortDefinition getWorkerDefinition() {
        return null;
    }

    public void init(TcModuleConfig arg0) {
    }

    public String doAction(TcConfig arg0, String arg1, TcRequest arg2,
            TcContent arg3) {
        StringBuffer sb = new StringBuffer().append("Action :" + arg1);
        if (arg2 != null) {
            if (arg2.getRequestParameters() != null)
                    sb.append("\nParameter : ").append(arg2.getRequestParameters());
        }
        Lg.WORKER(arg0.getModuleConfig().getName()).debug(sb.toString());
        try {
            return super.doAction(arg0, arg1, arg2, arg3);
        } catch (TcContentProzessException e) {
            Lg.WORKER(arg0.getModuleConfig().getName())
                    .debug(e.getMessage(), e);
        }
        return "";
    }
}
