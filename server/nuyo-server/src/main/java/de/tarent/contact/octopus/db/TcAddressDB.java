package de.tarent.contact.octopus.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.bean.TaddressDB;
import de.tarent.contact.bean.TaddresscategoryDB;
import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TcategorytypeDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.octopus.LetterAddressGenerator;
import de.tarent.contact.octopus.LetterSalutationGenerator;
import de.tarent.contact.octopus.worker.WorkerFactory;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.contact.octopus.worker.constants.AddressWorkerConstants;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Order;
import de.tarent.dblayer.sql.clause.RawClause;
import de.tarent.dblayer.sql.clause.SubSelect;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.AbstractStatement;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.groupware.queries.SecurityQueries;
import de.tarent.groupware.security.AddressSecurityException;
import de.tarent.octopus.content.TcAll;

/**
 * @author kleinw
 * 
 * Funktionalit�ten rundum Adressen.
 */
public class TcAddressDB implements AddressWorkerConstants {

    /**
     * The Country to ommit in the generated address label
     */
   
	private static Logger logger = Logger.getLogger(TcAddressDB.class.getName());

    private static final int TEL_WEITERES = 111;
    private static final int FAX_WEITERES = 112;
    private static final int HANDY_WEIERES= 113;
    private static final int EMAIL_WEITERE = 114;
    private static final int HOMEPAGE_WEITERE = 115;
    private static final int HOMEPAGE_DI = 110;
    private static final int HOMEPAGE_PRI = 109;
    private static final int EMAIL2 = 107;
    private static final int EMAIL1 = 108;
    private static final int FAX_PRI = 103;
    private static final int FAX_DI = 104;
    private static final int HANDY_PRI = 105;
    private static final int HANDY_DI = 106;
    private static final int TEL_PRI = 101;
    private static final int TEL_DI = 102;

    static public List getAddressesPreview(Map map)
            throws InputParameterException, SQLException {
        List pkPreviewList = new Vector();
        TcAddressSelect addressSelect = new TcAddressSelect(map);
        Select select = null;
        if(map.containsKey("userid")) {
            select = addressSelect.getPkPreviewMyCategory((Integer)map.get("userid"));
        } else
            select = addressSelect.getPkPreviewListSelect();
        Result tr = null;
        int i=0;
       try {
            if (map.containsKey("includeSubCategories")
                    && ((Boolean) map.get("includeSubCategories"))
                            .booleanValue()) {
                logger.finest("INCLUDE!");
                        
                StringBuffer subs = new StringBuffer();
                StringBuffer result = new StringBuffer();
                tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
                Integer previewPk = null;
                String preview = null;
                while (tr.resultSet().next()) {
                    i++;
                    previewPk = new Integer(tr.resultSet().getInt(TaddressextDB.PK_PKADDRESSEXT));
                    preview = tr.resultSet().getString(TaddressextDB.LABELFN);
//                    preview = preview.replaceAll("[|]", " ");
                    preview = preview.replaceAll("#", "/;");
                    subs.append(tr.resultSet().getString(TaddresssubcategoryDB.FKSUBCATEGORY));
                    subs.append(" ");
                    if (tr.resultSet().next()) {
                        if (tr.resultSet().getInt(TaddressextDB.PK_PKADDRESSEXT) != previewPk.intValue()) {
                            fillResult(pkPreviewList, pkPreviewList, subs,result, previewPk, preview);
                            result = new StringBuffer();
                            subs = new StringBuffer();
                        }
                        tr.resultSet().previous();
                    }
                }
                if (previewPk != null && preview != null)
                        fillResult(pkPreviewList, pkPreviewList, subs, result, previewPk, preview);
            } else {
                tr = DB.result(TcDBContext.getDefaultContext(), select.toString().toString());
                while (tr.resultSet().next()) {
                    logger.finest("lese resultset");
                    pkPreviewList.add(new Integer(tr.resultSet().getInt(TaddressextDB.PK_PKADDRESSEXT)));
                    pkPreviewList.add(tr.resultSet().getString(TaddressextDB.LABELFN));
                }
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }
        logger.finest("l�nge " + pkPreviewList.size() + " " + i);
        return pkPreviewList;
    }

    static public List getAddressesPreviewByPkList(Map parameter)
            throws SQLException, InputParameterException {
        List previews = new Vector();
        if (!parameter.containsKey("addressPks")) { throw new InputParameterException(
                "F�r die Methode 'getAddressesByPk' muss ein Paramter 'addressPks' vom Typ List �bergeben werden."); }
        if (!(parameter.get("addressPks") instanceof String)) { throw new InputParameterException(
                "Der Parameter 'addressPks' muss als Typ String �bergeben werden, wurde aber als " + parameter.get("addressPks").getClass() + " �bergeben"); }
        if (parameter.containsKey("includeSubCategories")
                && !(parameter.get("includeSubCategories") instanceof Boolean)) { throw new InputParameterException(
                "Der Parameter 'includeSubCategories' muss vom Typ Boolean sein."); }
        
        
        List addressPks = new Vector();
        String pks = (String) parameter.get("addressPks");
        StringTokenizer st = new StringTokenizer(pks, " ");
        while (st.hasMoreTokens()) {
            addressPks.add(new Integer(st.nextToken()));
        }
        
        
        
        Select select = 
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TaddressextDB.getTableName())
                .selectAs(TaddressextDB.PK_PKADDRESSEXT)
                .selectAs(TaddressextDB.LABELFN)
				.join(TaddressDB.getTableName(), TaddressDB.PK_PKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
				
				/**
				 * Auskommentiert, da Sicherheitsabfrage nun in vorgeschaltetem Tasks passiert
				 * Check der Pk-Liste in 'hasUserRightToReadAddresses'
				 */
				
                .where(Expr.in(TaddressextDB.PK_PKADDRESSEXT, addressPks));
        
        if (parameter.containsKey("includeSubCategories")
                && ((Boolean) parameter.get("includeSubCategories"))
                        .booleanValue()) {
            select
            	.join(TaddresssubcategoryDB.getTableName(), TaddresssubcategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
            	.selectAs(TaddresssubcategoryDB.FKSUBCATEGORY)
            	.orderBy(Order.asc(TaddressextDB.PK_PKADDRESSEXT));
        }
        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            if (parameter.containsKey("includeSubCategories")
                    && ((Boolean) parameter.get("includeSubCategories"))
                            .booleanValue()) {
                Integer addressPk = null;
                String preview = null;
                StringBuffer subCategories = new StringBuffer();
                while (tr.resultSet().next()) {
                    addressPk = new Integer(tr.resultSet().getInt(TaddressextDB.PK_PKADDRESSEXT));
                    preview = tr.resultSet().getString(TaddressextDB.LABELFN);
                    preview = preview.replaceAll("#", "/;");
                    subCategories.append(tr.resultSet().getInt(TaddresssubcategoryDB.FKSUBCATEGORY));
                    subCategories.append(" ");
                    if (tr.resultSet().next()) {
                        if (addressPk.intValue() != tr.resultSet().getInt(TaddressextDB.PK_PKADDRESSEXT)) {
                            previews.add(
                                    new StringBuffer().append(addressPk)
                                    	.append("#").append(preview)
                                    	.append("#").append(subCategories)
                                    	.toString());
                            subCategories = new StringBuffer();
                        }
                        tr.resultSet().previous();
                    }
                }
                if (addressPk != null && preview != null) {
                    previews.add
                    	(new StringBuffer()
                    	        .append(addressPk)
                    	        .append("#")
                    	        .append(preview)
                    	        .append("#")
                    	        .append(subCategories).toString());
                    subCategories = new StringBuffer();
                }
            } else {
                while (tr.resultSet().next()) {
                    previews.add(
                        new StringBuffer(
                                tr.resultSet().getString(TaddressextDB.PK_PKADDRESSEXT))
                        	.append("#")
                        	.append(tr.resultSet().getString(TaddressextDB.LABELFN))
                        	.append("#").toString());
                }
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw (e);
        } finally {
            if (tr != null) {
                tr.close();
            }
        }
        return previews;
    }

    public static List getAddressesPreviewByFilter(Map parameter)
            throws InputParameterException, SQLException {
        List preview = new Vector();

        // Lehre Liste zur�ck geben, addressPks angegeben wurde,
        // aber keine IDs darin enthalten sind.
        if (parameter.get("addressPks") != null && "".equals(parameter.get("addressPks")))
            return preview;
        TcAddressSelect as = new TcAddressSelect(parameter);
        Map t = (Map)parameter.get("filterMap");
        boolean matchCode = false;
        //boolean followup = false;
        
        if(t.containsKey("matchCodes") && ((Boolean)t.get("matchCodes")).booleanValue())
            matchCode = true;
        //if (t.containsKey("followup") && ((Boolean)t.get("followup")).booleanValue())
        	//followup = true;
        
        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), as.getPreviewSearchSelect().toString());
            if(matchCode) {
                while (tr.resultSet().next()) {
                    preview.add(new Integer(tr.resultSet().getInt(TaddressextDB.PK_PKADDRESSEXT)));
                }
            } else {
                while (tr.resultSet().next()) {
                    preview.add(new Integer(tr.resultSet().getInt(TaddressDB.PK_PKADDRESS)));
                }
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }
        return preview;
    }

    static public List getAddressStandardDataByPk(Map parameter, String letterAddressConfigParams)
            throws SQLException, InputParameterException {
        List standardData = new Vector();
        if (!parameter.containsKey(PARAM_ADDRESSPK)) {
            throw new InputParameterException(
                    "F�r die Methode 'getAddressStandardDataByPk' muss ein Parameter 'addressPk' angegeben sein.");
        } else {
            Integer pk;
            try {
                pk = (Integer) parameter.get(PARAM_ADDRESSPK);
            } catch (ClassCastException e1) {
                throw new InputParameterException(
                        "Der Parameter 'addressPk' muss als Integer �bergeben werden.");
            }
            Result tr = null;
            try {
                Select select = 
                    SQL.Select(TcDBContext.getDefaultContext())
                    	.from(TaddressDB.getTableName())
                        .join(TaddressextDB.getTableName(),TaddressDB.PK_PKADDRESS,TaddressextDB.PK_PKADDRESSEXT)
                        .selectAs(TaddressDB.LETTERSALUTATION)
                        .selectAs(TaddressDB.SALUTATION)
                        .selectAs(TaddressDB.TITELJOB)
                        .selectAs(TaddressDB.TITELACADEMIC)
                        .selectAs(TaddressDB.FIRSTNAME)
                        .selectAs(TaddressDB.LASTNAME)
                        .selectAs(TaddressDB.ORGANISATION)
                        .selectAs(TaddressDB.POSITION)
                        .selectAs(TaddressDB.DEPARTMENT)
                        .selectAs(TaddressDB.STREET)
                        .selectAs(TaddressDB.STREET2)
                        .selectAs(TaddressDB.HOUSENO)
                        .selectAs(TaddressDB.HOUSENO2)
                        .selectAs(TaddressDB.ZIPCODE)
                        .selectAs(TaddressDB.ZIPCODE2)
                        .selectAs(TaddressDB.CITY)
                        .selectAs(TaddressDB.CITY2)
                        .selectAs(TaddressDB.POBOXZIPCODE)
                        .selectAs(TaddressDB.POBOXZIPCODE2)
                        .selectAs(TaddressDB.POBOX)
                        .selectAs(TaddressDB.POBOX2)
                        .selectAs(TaddressDB.COUNTRY)
                        .selectAs(TaddressDB.COUNTRY2)
                        .selectAs(TaddressDB.REGION)
                        .selectAs(TaddressDB.REGION2)
                        .selectAs(TaddressextDB.FON)
                        .selectAs(TaddressextDB.FONHOME)
                        .selectAs(TaddressextDB.FON3)
                        .selectAs(TaddressextDB.MOBILE)
                        .selectAs(TaddressextDB.MOBILEHOME)
                        .selectAs(TaddressextDB.MOBILE3)
                        .selectAs(TaddressextDB.FAX)
                        .selectAs(TaddressextDB.FAXHOME)
                        .selectAs(TaddressextDB.FAX3)
                        .selectAs(TaddressextDB.EMAIL)
                        .selectAs(TaddressextDB.EMAILHOME)
                        .selectAs(TaddressextDB.EMAIL3)
                        .selectAs(TaddressDB.CREATED)
                        .selectAs(TaddressDB.CHANGED)
                        .selectAs(TaddressDB.NAMESUFFIX)
                        .selectAs(TaddressextDB.URL)
                        .selectAs(TaddressextDB.URLHOME)
                        .selectAs(TaddressextDB.URL3)
                        .selectAs(TaddressDB.MIDDLENAME)
                        .selectAs(TaddressDB.NICKNAME)
                        .selectAs(TaddressDB.BANKNAME)
                        .selectAs(TaddressDB.BANKNO)
                        .selectAs(TaddressDB.BANKACCOUNT)
                        .selectAs(TaddressDB.SEX)
                        .selectAs(TaddressDB.YEAROFBIRTH)
                        .selectAs(TaddressDB.DATEOFBIRTH)
                        .selectAs(TaddressDB.NOTEONADDRESS)
                        .selectAs(TaddressDB.EXTERNALDATE)
                        .selectAs(TaddressDB.ADDRESSSUFFIX)
                        .selectAs(TaddressDB.ADDRESSSUFFIX2)
                        .selectAs(TaddressDB.FKUSER)
                        .selectAs(TaddressDB.COMMONTEXT01)
                        .selectAs(TaddressDB.COMMONTEXT02)
                        .selectAs(TaddressDB.COMMONTEXT03)
                        .selectAs(TaddressDB.COMMONTEXT04)
                        .selectAs(TaddressDB.COMMONTEXT05)
                        .selectAs(TaddressDB.COMMONTEXT06)
                        .selectAs(TaddressDB.COMMONTEXT07)
                        .selectAs(TaddressDB.COMMONTEXT08)
                        .selectAs(TaddressDB.COMMONTEXT09)
                        .selectAs(TaddressDB.COMMONTEXT10)
                        .selectAs(TaddressDB.COMMONTEXT11)
                        .selectAs(TaddressDB.COMMONTEXT12)
                        .selectAs(TaddressDB.COMMONTEXT13)
                        .selectAs(TaddressDB.COMMONTEXT14)
                        .selectAs(TaddressDB.COMMONTEXT15)
                        .selectAs(TaddressDB.COMMONTEXT16)
                        .selectAs(TaddressDB.COMMONTEXT17)
                        .selectAs(TaddressDB.COMMONTEXT18)
                        .selectAs(TaddressDB.COMMONTEXT19)
                        .selectAs(TaddressDB.COMMONTEXT20)
                        .selectAs(TaddressDB.COMMONINT01)
                        .selectAs(TaddressDB.COMMONINT02)                        
                        .selectAs(TaddressDB.COMMONINT03)                        
                        .selectAs(TaddressDB.COMMONINT04)                        
                        .selectAs(TaddressDB.COMMONINT05)                        
                        .selectAs(TaddressDB.COMMONINT06)                        
                        .selectAs(TaddressDB.COMMONINT07)                        
                        .selectAs(TaddressDB.COMMONINT08)
						.selectAs(TaddressDB.COMMONINT09)
                        .selectAs(TaddressDB.COMMONINT10)                        
                        .selectAs(TaddressDB.COMMONBOOL01)                        
                        .selectAs(TaddressDB.COMMONBOOL02)                        
                        .selectAs(TaddressDB.COMMONBOOL03)                        
                        .selectAs(TaddressDB.COMMONBOOL04)                        
                        .selectAs(TaddressDB.COMMONBOOL05)                        
                        .selectAs(TaddressDB.COMMONBOOL06)                        
                        .selectAs(TaddressDB.COMMONBOOL07)                        
                        .selectAs(TaddressDB.COMMONBOOL08)                        
                        .selectAs(TaddressDB.COMMONBOOL09)                        
                        .selectAs(TaddressDB.COMMONBOOL10)                        
                        .selectAs(TaddressDB.COMMONDATE01)                        
                        .selectAs(TaddressDB.COMMONDATE02)                        
                        .selectAs(TaddressDB.COMMONDATE03)                        
                        .selectAs(TaddressDB.COMMONDATE04)                        
                        .selectAs(TaddressDB.COMMONDATE05)                        
                        .selectAs(TaddressDB.COMMONDATE06)                        
                        .selectAs(TaddressDB.COMMONDATE07)                        
                        .selectAs(TaddressDB.COMMONDATE08)                        
                        .selectAs(TaddressDB.COMMONDATE09)                        
                        .selectAs(TaddressDB.COMMONDATE10)                        
                        .selectAs(TaddressDB.COMMONMONEY01)                        
                        .selectAs(TaddressDB.COMMONMONEY02)                        
                        .selectAs(TaddressDB.COMMONMONEY03)                        
                        .selectAs(TaddressDB.COMMONMONEY04)                        
                        .selectAs(TaddressDB.COMMONMONEY05)                        
                        .selectAs(TaddressDB.COMMONMONEY06)                        
                        .selectAs(TaddressDB.COMMONMONEY07)                        
                        .selectAs(TaddressDB.COMMONMONEY08)                        
                        .selectAs(TaddressDB.COMMONMONEY09)                        
                        .selectAs(TaddressDB.COMMONMONEY10)
						.selectAs(TaddressDB.PICTURE)
						.selectAs(TaddressDB.PICTUREEXPRESSION)
						.selectAs(TaddressDB.FOLLOWUP)
						.selectAs(TaddressDB.FOLLOWUPDATE)
						.selectAs(TaddressDB.FKUSERFOLLOWUP)
						.selectAs(TaddressDB.LETTERADDRESS)
						.selectAs(TaddressDB.LETTERSALUTATIONAUTO)						
						.selectAs(TaddressDB.LETTERADDRESSAUTO)
						.selectAs(TaddressDB.LETTERSALUTATIONSHORT)
						.selectAs(TaddressDB.STREET3)
						.selectAs(TaddressDB.HOUSENO3)
						.selectAs(TaddressDB.ZIPCODE3)
						.selectAs(TaddressDB.CITY3)
						.selectAs(TaddressDB.REGION3)
						.selectAs(TaddressDB.COUNTRY3)
						.selectAs(TaddressDB.POBOX3) 
						.selectAs(TaddressDB.POBOXZIPCODE3)
						.selectAs(TaddressDB.ADDRESSSUFFIX3)					

						.where(Expr.equal(TaddressDB.PK_PKADDRESS, pk));

                
                tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
                
                if (tr.resultSet().next()) {
                	// Falls die Briefanrede nicht on the fly generiert werden soll, nehme DB-Eintrag
                	standardData.add(tr.resultSet().getString(TaddressDB.LETTERSALUTATION)); 		// [00]
                	standardData.add(tr.resultSet().getString(TaddressDB.SALUTATION));				// [01]
                    standardData.add(tr.resultSet().getString(TaddressDB.TITELJOB));				// [02]
                    standardData.add(tr.resultSet().getString(TaddressDB.TITELACADEMIC));			// [03]
                    standardData.add(tr.resultSet().getString(TaddressDB.FIRSTNAME));				// [04]
                    standardData.add(tr.resultSet().getString(TaddressDB.LASTNAME));				// [05]
                    standardData.add(tr.resultSet().getString(TaddressDB.ORGANISATION));			// [06]
                    standardData.add(tr.resultSet().getString(TaddressDB.POSITION));				// [07]
                    standardData.add(tr.resultSet().getString(TaddressDB.DEPARTMENT));				// [08]
                    standardData.add(tr.resultSet().getString(TaddressDB.STREET));					// [09]
                    standardData.add(tr.resultSet().getString(TaddressDB.HOUSENO));					// [10]
                    standardData.add(tr.resultSet().getString(TaddressDB.ZIPCODE));					// [11]
                    standardData.add(tr.resultSet().getString(TaddressDB.CITY));					// [12]
                    standardData.add(tr.resultSet().getString(TaddressDB.POBOXZIPCODE));			// [13]
                    standardData.add(tr.resultSet().getString(TaddressDB.POBOX));					// [14]
                    standardData.add(tr.resultSet().getString(TaddressDB.COUNTRY));					// [15]
                    standardData.add(tr.resultSet().getString(TaddressDB.REGION));					// [16]
                    standardData.add(tr.resultSet().getString(TaddressextDB.FON));					// [17]
                    standardData.add(tr.resultSet().getString(TaddressextDB.FONHOME));				// [18]	
                    standardData.add(tr.resultSet().getString(TaddressextDB.MOBILE));				// [19]
                    standardData.add(tr.resultSet().getString(TaddressextDB.MOBILEHOME));			// [20]
                    standardData.add(tr.resultSet().getString(TaddressextDB.FAX));					// [21]
                    standardData.add(tr.resultSet().getString(TaddressextDB.FAXHOME));				// [22]
                    standardData.add(tr.resultSet().getString(TaddressextDB.EMAIL));				// [23]
                    standardData.add(tr.resultSet().getString(TaddressextDB.EMAILHOME));			// [24]
                    standardData.add(tr.resultSet().getDate(TaddressDB.CREATED));					// [25]
                    standardData.add(tr.resultSet().getDate(TaddressDB.CHANGED));					// [26]
                    standardData.add(tr.resultSet().getString(TaddressDB.NAMESUFFIX));				// [27]
                    standardData.add(tr.resultSet().getString(TaddressextDB.URL));					// [28]
                    standardData.add(tr.resultSet().getString(TaddressDB.MIDDLENAME));				// [29]
                    standardData.add(tr.resultSet().getString(TaddressDB.NICKNAME));				// [30]
                    standardData.add(tr.resultSet().getString(TaddressDB.BANKNAME));				// [31]
                    standardData.add(tr.resultSet().getString(TaddressDB.BANKNO));					// [32]
                    standardData.add(tr.resultSet().getString(TaddressDB.BANKACCOUNT));				// [33]	
                    standardData.add(tr.resultSet().getString(TaddressDB.SEX));						// [34]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.YEAROFBIRTH)));	// [35]
                    standardData.add(tr.resultSet().getDate(TaddressDB.DATEOFBIRTH));				// [36]
                    standardData.add(tr.resultSet().getString(TaddressDB.NOTEONADDRESS));			// [37]
                    standardData.add("<ommitted>");													// [38] //ommitted, there is a special method for getting note 
                    standardData.add(tr.resultSet().getString(TaddressDB.EXTERNALDATE));			// [39]
                    standardData.add(tr.resultSet().getString(TaddressDB.STREET2));					// [40]
                    standardData.add(tr.resultSet().getString(TaddressDB.HOUSENO2));				// [41]	
                    standardData.add(tr.resultSet().getString(TaddressDB.ZIPCODE2));				// [42]
                    standardData.add(tr.resultSet().getString(TaddressDB.CITY2));					// [43]
                    standardData.add(tr.resultSet().getString(TaddressDB.REGION2));					// [44]
                    standardData.add("");															// [45]
                    standardData.add(tr.resultSet().getString(TaddressDB.COUNTRY2));				// [46]	
                    standardData.add(tr.resultSet().getString(TaddressDB.POBOX2));					// [47]
                    standardData.add(tr.resultSet().getString(TaddressDB.POBOXZIPCODE2));			// [48]	
                    standardData.add(tr.resultSet().getString(TaddressDB.ADDRESSSUFFIX));			// [49]
                    standardData.add(tr.resultSet().getString(TaddressDB.ADDRESSSUFFIX2));			// [50]			
                    standardData.add(tr.resultSet().getString(TaddressextDB.EMAIL3));				// [51]
                    standardData.add(tr.resultSet().getString(TaddressextDB.FAX3));					// [52]	
                    standardData.add(tr.resultSet().getString(TaddressextDB.MOBILE3));				// [53]
                    standardData.add(tr.resultSet().getString(TaddressextDB.FON3));					// [54]
                    standardData.add(tr.resultSet().getString(TaddressextDB.URLHOME));				// [55]
                    standardData.add(tr.resultSet().getString(TaddressextDB.URL3));					// [56]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.FKUSER)));		// [57]
                    
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT01));			// [58]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT02));			// [59]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT03));			// [60]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT04));			// [61]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT05));			// [62]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT06));			// [63]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT07));			// [64]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT08));			// [65]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT09));			// [66]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT10));			// [67]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT11));			// [68]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT12));			// [69]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT13));			// [70]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT14));			// [71]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT15));			// [72]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT16));			// [73]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT17));			// [74]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT18));			// [75]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT19));			// [76]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONTEXT20));			// [77]
                    
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT01)));			// [78]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT02)));			// [79]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT03)));			// [80]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT04)));			// [81]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT05)));			// [82]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT06)));			// [83]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT07)));			// [84]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT08)));			// [85]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT09)));			// [86]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.COMMONINT10)));			// [87]
                   
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL01)));			// [88]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL02)));			// [89]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL03)));			// [90]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL04)));			// [91]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL05)));			// [92]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL06)));			// [93]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL07)));			// [94]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL08)));			// [95]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL09)));			// [96]
                    standardData.add(new Boolean(tr.resultSet().getBoolean(TaddressDB.COMMONBOOL10)));			// [97]
                   
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE01));			// [98]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE02));			// [99]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE03));			// [100]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE04));			// [101]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE05));			// [102]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE06));			// [103]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE07));			// [104]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE08));			// [105]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE09));			// [106]
                    standardData.add(tr.resultSet().getDate(TaddressDB.COMMONDATE10));			// [107]

                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY01));			// [108]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY02));			// [109]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY03));			// [110]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY04));			// [111]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY05));			// [112]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY06));			// [113]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY07));			// [114]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY08));			// [115]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY09));			// [116]
                    standardData.add(tr.resultSet().getString(TaddressDB.COMMONMONEY10));			// [117]
                    
                    standardData.add((byte[]) tr.resultSet().getObject(TaddressDB.PICTURE));		// [118]
                    standardData.add(tr.resultSet().getString(TaddressDB.PICTUREEXPRESSION));		// [119]
                    
                    standardData.add(intToBool(new Integer(tr.resultSet().getInt(TaddressDB.FOLLOWUP))));			// [120]
                    standardData.add(tr.resultSet().getDate(TaddressDB.FOLLOWUPDATE));								// [121]
                    standardData.add(new Integer(tr.resultSet().getInt(TaddressDB.FKUSERFOLLOWUP)));				// [122]
                    
                    standardData.add(tr.resultSet().getString(TaddressDB.LETTERADDRESS));								// [123]
                    standardData.add(intToBool(new Integer(tr.resultSet().getInt(TaddressDB.LETTERSALUTATIONAUTO))));	// [124]
                    standardData.add(intToBool(new Integer(tr.resultSet().getInt(TaddressDB.LETTERADDRESSAUTO)))); 		// [125]
                    //System.out.println("LetterAddressAuto: " +  intToBool(new Integer(tr.resultSet().getInt(TaddressDB.LETTERADDRESSAUTO))));
                    
                    standardData.add(tr.resultSet().getString(TaddressDB.LETTERSALUTATIONSHORT));						// [126]
                    standardData.add(tr.resultSet().getString(TaddressDB.STREET3));										// [127]
                    standardData.add(tr.resultSet().getString(TaddressDB.HOUSENO3));									// [128]
                    standardData.add(tr.resultSet().getString(TaddressDB.ZIPCODE3));									// [129]
                    standardData.add(tr.resultSet().getString(TaddressDB.CITY3));										// [130]
                    standardData.add(tr.resultSet().getString(TaddressDB.REGION3));										// [131]
                    standardData.add(tr.resultSet().getString(TaddressDB.COUNTRY3));									// [132]
                    standardData.add(tr.resultSet().getString(TaddressDB.POBOX3));										// [133]
                    standardData.add(tr.resultSet().getString(TaddressDB.POBOXZIPCODE3));								// [134]
                    standardData.add(tr.resultSet().getString(TaddressDB.ADDRESSSUFFIX3));								// [135]
                    
                    if ((intToBool(new Integer(tr.resultSet().getInt(TaddressDB.LETTERSALUTATIONAUTO)))).booleanValue()){
                    	//System.out.println("Generiere Briefanrede");
                    	String letterSalutation = generateLetterSalutation(
                    			tr.resultSet().getString(TaddressDB.LETTERSALUTATIONSHORT),
                    			tr.resultSet().getString(TaddressDB.FIRSTNAME),
                    			tr.resultSet().getString(TaddressDB.SALUTATION),
             					tr.resultSet().getString(TaddressDB.TITELACADEMIC),
           						tr.resultSet().getString(TaddressDB.TITELJOB),
           						tr.resultSet().getString(TaddressDB.LASTNAME));
                    	
                    	//int index = standardData.indexOf(tr.resultSet().getString(TaddressDB.LETTERSALUTATION));
                    	standardData.set(0, letterSalutation);
                    }
                    
                    if ((intToBool(new Integer(tr.resultSet().getInt(TaddressDB.LETTERADDRESSAUTO)))).booleanValue()){
                    	//System.out.println("Anschrift wird generiert");
                    	String letterAddress = LetterAddressGenerator.generateLetterAddress(
                    			tr.resultSet().getString(TaddressDB.ORGANISATION),
                    			tr.resultSet().getString(TaddressDB.SALUTATION),
                    			tr.resultSet().getString(TaddressDB.TITELACADEMIC),
           						tr.resultSet().getString(TaddressDB.FIRSTNAME),
           						tr.resultSet().getString(TaddressDB.TITELJOB),
           						tr.resultSet().getString(TaddressDB.LASTNAME),
           						tr.resultSet().getString(TaddressDB.NAMESUFFIX),
           						tr.resultSet().getString(TaddressDB.POSITION),
           						tr.resultSet().getString(TaddressDB.DEPARTMENT),
           						tr.resultSet().getString(TaddressDB.STREET),
           						tr.resultSet().getString(TaddressDB.HOUSENO),
           						tr.resultSet().getString(TaddressDB.POBOXZIPCODE),
           						tr.resultSet().getString(TaddressDB.POBOX),
           						tr.resultSet().getString(TaddressDB.ZIPCODE),
           						tr.resultSet().getString(TaddressDB.CITY),
           						tr.resultSet().getString(TaddressDB.COUNTRY),
           						letterAddressConfigParams);
                    	
                    	standardData.set(123, letterAddress);
                    }
                   // else System.out.println("Anschrift wird aus DB genommen");
                    	
                    	
                }
            } catch (SQLException e) {
                throw (e);
            } finally {
                if (tr != null) tr.close();
            }
        }
        return standardData;
    }
    
    private static Integer boolToInt(Boolean bool){    	
    	if (bool != null && bool.booleanValue()){
    		return new Integer(1);
    	}
    	return new Integer(0);    	
    }
    private static Boolean intToBool(Integer i){    	
    	if (i != null && i.intValue() == 1){
    		return Boolean.TRUE;
    	}
    	return Boolean.FALSE;    	
    }

    public static List getAddressEmailData(Map parameter)
            throws InputParameterException, SQLException {
        List emailData = new Vector();
        if (!parameter.containsKey(PARAM_ADDRESSPK))
                throw new InputParameterException(
                        "F�r die Methode 'getAddressEmailData' muss ein pk angegeben sein.");
        if (!(parameter.get(PARAM_ADDRESSPK) instanceof Integer))
                throw new InputParameterException(
                        "Der Parameter 'addressPk' muss als Integer �bergeben werden.");
        Integer addressPk = (Integer) parameter.get(PARAM_ADDRESSPK);
        Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TaddressextDB.getTableName())
                .selectAs(TaddressextDB.EMAIL).selectAs(
                        TaddressextDB.EMAILHOME).where(Expr.equal(
                        TaddressextDB.PK_PKADDRESSEXT, addressPk));
        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            while (tr.resultSet().next()) {
                emailData.add(tr.resultSet().getString(TaddressextDB.EMAIL));
                emailData.add(tr.resultSet().getString(TaddressextDB.EMAILHOME));
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }
        return emailData;
    }

    public static List getAddressProtectedData(Map parameter)
            throws InputParameterException, SQLException {
        List protectedData = new Vector();
        if (!parameter.containsKey(PARAM_ADDRESSPK)) { throw new InputParameterException(
                "F�r die Methode 'getAddressProtectedData' muss ein pk angegeben sein."); }
        if (!parameter.containsKey(PARAM_CATEGORY)) { throw new InputParameterException(
                "F�r die Methode 'getAddressProtectedData' muss eine Kategorie �bergeben sein."); }
        Integer category = null;
        Integer pk = null;
        try {
            pk = (Integer) parameter.get(PARAM_ADDRESSPK);
        } catch (ClassCastException e) {
            throw new InputParameterException(
                    "Der Parameter 'addressPk' muss als Integer �bergeben werden.");
        }
        try {
            category = (Integer) parameter.get(PARAM_CATEGORY);
        } catch (RuntimeException e1) {
            throw new InputParameterException(
                    "Der Parameter 'category' muss als Integer �bergeben werden.");
        }
        WhereList wl = new WhereList();
        wl.addAnd(Expr.equal(TaddresscategoryDB.FKADDRESS, pk));
        wl.addAnd(Expr.equal(TaddresscategoryDB.FKCATEGORY, category));
        Select select = 
            SQL.Select(TcDBContext.getDefaultContext())
            	.from(TaddresscategoryDB.getTableName())
            	.selectAs(TaddresscategoryDB.NOTE)
                .where(wl);

        Result tr = null;
        
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            if (tr.resultSet().next()) {
                protectedData.add(tr.resultSet().getString(TaddresscategoryDB.NOTE));
            }
        } catch (SQLException e2) {
            throw (e2);
        } finally {
            if (tr != null) tr.close();
        }
        return protectedData;
    }

    protected static String saveToString(Object o) {
        if (o == null)
            return null;
        return o.toString();
    }

    static public String updateAddress(Integer user_id, Map parameter, TcAll all)
            throws InputParameterException, SQLException, AddressSecurityException {
    	
    	Character questionmark = new Character('?');
    	
        StringBuffer preview = new StringBuffer();
        
        //TODO sometimes Map admits null values. Can't see whether this could be crucial
        //for below coding. I've just fixed an SQL-Exception when selecting taddresssubfolder
        //with categoryPk == null. Maybe null is allways bad here?
        if (!parameter.containsKey("categoryPk")) { throw new InputParameterException(
        "F�r das Speichern einer Adresse muss der Parameter 'categoryPk' angegeben sein."); }
        if (!parameter.containsKey("addressPk")) { throw new InputParameterException(
                "F�r das Speichern einer Adresse muss der Parameter 'addressPk' angegeben sein."); }
        if (!parameter.containsKey("subCategories")) { throw new InputParameterException(
                "F�r das Speichern einer Adresse muss der Parameter 'subCategories' angegeben sein."); }
        if (!parameter.containsKey("standardAddress")) { throw new InputParameterException(
                "F�r das Speichern einer Adresse muss der Parameter 'standardAddress' angegeben sein."); }
        Integer categoryPk = null;
        Integer addressPk = null;
        List subCategories = null;
        List standardAddress = null;

        try {
            categoryPk = (Integer)parameter.get("categoryPk");
        } catch (ClassCastException e4) {
            throw new InputParameterException(
            "Der Parameter 'categoryPk' als Integer �bergeben werden.");
        }
        
        try {
            addressPk = (Integer) parameter.get("addressPk");
            preview.append(addressPk).append(" ");
        } catch (ClassCastException e2) {
            throw new InputParameterException(
                    "F�r das Speichern einer Adresse muss der Parameter 'addressPk' als Integer �bergeben werden.");
        }
        try {
            standardAddress = (List) parameter.get("standardAddress");
        } catch (ClassCastException e1) {
            throw new InputParameterException(
                    "F�r das Speichern einer Adresse muss der Parameter 'standardAddress' als Liste �bergeben werden.");
        }
        try {
            subCategories = (List) parameter.get("subCategories");
        } catch (ClassCastException e) {
            throw new InputParameterException(
                    "F�r das Speichern einer Adresse muss der Parameter 'subCategories' als Liste �bergeben werden.");
        }
        
        int i = 0;
        
        if (WorkerFactory.getSecurityWorker(all)
        	.hasUserRightOnAddress(all, all.personalConfig().getLoginname(), new Integer(SecurityQueries.FOLDERRIGHT_EDIT), addressPk)
        	.booleanValue()){
	        	
	        if (standardAddress != null ) 
	        {
	            Update update = 
	            	SQL.Update(TcDBContext.getDefaultContext())
			            .table(TaddressDB.getTableName())
			            .update(TaddressDB.LETTERSALUTATION,(String) standardAddress.get(0))
			            .update(TaddressDB.SALUTATION, (String) standardAddress.get(1))
			            .update(TaddressDB.TITELJOB, (String) standardAddress.get(2))
			            .update(TaddressDB.TITELACADEMIC,(String) standardAddress.get(3))
			            .update(TaddressDB.FIRSTNAME, (String) standardAddress.get(4))
			            .update(TaddressDB.LASTNAME, (String) standardAddress.get(5))
			            .update(TaddressDB.ORGANISATION,(String) standardAddress.get(6))
			            .update(TaddressDB.POSITION, (String) standardAddress.get(7))
			            .update(TaddressDB.DEPARTMENT, (String) standardAddress.get(8))
			            .update(TaddressDB.STREET, (String) standardAddress.get(9))
			            .update(TaddressDB.HOUSENO, (String) standardAddress.get(10))
			            .update(TaddressDB.ZIPCODE, (String) standardAddress.get(11))
			            .update(TaddressDB.CITY, (String) standardAddress.get(12))
			            .update(TaddressDB.POBOXZIPCODE,(String) standardAddress.get(13))
			            .update(TaddressDB.POBOX, (String) standardAddress.get(14))
			            .update(TaddressDB.COUNTRY, (String) standardAddress.get(15))
			            .update(TaddressDB.REGION, (String) standardAddress.get(16))
			            .update(TaddressDB.NAMESUFFIX, (String) standardAddress.get(27))
			            .update(TaddressDB.MIDDLENAME, (String) standardAddress.get(29))
			            .update(TaddressDB.NICKNAME, (String) standardAddress.get(30))
			            .update(TaddressDB.BANKNAME, (String) standardAddress.get(31))
			            .update(TaddressDB.BANKNO, (String) standardAddress.get(32))
			            .update(TaddressDB.BANKACCOUNT,(String) standardAddress.get(33))
			            .update(TaddressDB.SEX,(String) standardAddress.get(34))
	                     .update(TaddressDB.YEAROFBIRTH,saveToString(standardAddress.get(35)))
						.update(TaddressDB.DATEOFBIRTH, standardAddress.get(36))
						.update(TaddressDB.NOTEONADDRESS, standardAddress.get(37))
			            .update(TaddressDB.STREET2,(String) standardAddress.get(40))
			            .update(TaddressDB.HOUSENO2,(String) standardAddress.get(41))
			            .update(TaddressDB.ZIPCODE2,(String) standardAddress.get(42))
			            .update(TaddressDB.CITY2,(String) standardAddress.get(43))
			            .update(TaddressDB.REGION2,(String) standardAddress.get(44))
			            .update(TaddressDB.COUNTRY2,(String) standardAddress.get(46))
			            .update(TaddressDB.POBOX2,(String) standardAddress.get(47))
			            .update(TaddressDB.POBOXZIPCODE2,(String) standardAddress.get(48))
			            .update(TaddressDB.ADDRESSSUFFIX,(String) standardAddress.get(49))
			            .update(TaddressDB.ADDRESSSUFFIX2,(String) standardAddress.get(50))
	                    .update(TaddressDB.FKUSER,saveToString(standardAddress.get(57)))
						
			            .update(TaddressDB.COMMONTEXT01,(String) standardAddress.get(58))
						.update(TaddressDB.COMMONTEXT02,(String) standardAddress.get(59))
						.update(TaddressDB.COMMONTEXT03,(String) standardAddress.get(60))
						.update(TaddressDB.COMMONTEXT04,(String) standardAddress.get(61))
						.update(TaddressDB.COMMONTEXT05,(String) standardAddress.get(62))
						.update(TaddressDB.COMMONTEXT06,(String) standardAddress.get(63))
						.update(TaddressDB.COMMONTEXT07,(String) standardAddress.get(64))
						.update(TaddressDB.COMMONTEXT08,(String) standardAddress.get(65))
						.update(TaddressDB.COMMONTEXT09,(String) standardAddress.get(66))
						.update(TaddressDB.COMMONTEXT10,(String) standardAddress.get(67))
						
						.update(TaddressDB.COMMONTEXT11,(String) standardAddress.get(68))
						.update(TaddressDB.COMMONTEXT12,(String) standardAddress.get(69))
						.update(TaddressDB.COMMONTEXT13,(String) standardAddress.get(70))
						.update(TaddressDB.COMMONTEXT14,(String) standardAddress.get(71))
						.update(TaddressDB.COMMONTEXT15,(String) standardAddress.get(72))
						.update(TaddressDB.COMMONTEXT16,(String) standardAddress.get(73))
						.update(TaddressDB.COMMONTEXT17,(String) standardAddress.get(74))
						.update(TaddressDB.COMMONTEXT18,(String) standardAddress.get(75))
						.update(TaddressDB.COMMONTEXT19,(String) standardAddress.get(76))
						.update(TaddressDB.COMMONTEXT20,(String) standardAddress.get(77))
						
						.update(TaddressDB.COMMONINT01,(Integer) standardAddress.get(78))
						.update(TaddressDB.COMMONINT02,(Integer) standardAddress.get(79))
						.update(TaddressDB.COMMONINT03,(Integer) standardAddress.get(80))
						.update(TaddressDB.COMMONINT04,(Integer) standardAddress.get(81))
						.update(TaddressDB.COMMONINT05,(Integer) standardAddress.get(82))
						.update(TaddressDB.COMMONINT06,(Integer) standardAddress.get(83))
						.update(TaddressDB.COMMONINT07,(Integer) standardAddress.get(84))
						.update(TaddressDB.COMMONINT08,(Integer) standardAddress.get(85))
						.update(TaddressDB.COMMONINT09,(Integer) standardAddress.get(86))
						.update(TaddressDB.COMMONINT10,(Integer) standardAddress.get(87))
						
							.update(TaddressDB.COMMONBOOL01, standardAddress.get(88))
						.update(TaddressDB.COMMONBOOL02,standardAddress.get(89))
						.update(TaddressDB.COMMONBOOL03,standardAddress.get(90))
						.update(TaddressDB.COMMONBOOL04,standardAddress.get(91))
						.update(TaddressDB.COMMONBOOL05,standardAddress.get(92))
						.update(TaddressDB.COMMONBOOL06,standardAddress.get(93))
						.update(TaddressDB.COMMONBOOL07,standardAddress.get(94))
						.update(TaddressDB.COMMONBOOL08,standardAddress.get(95))
						.update(TaddressDB.COMMONBOOL09,standardAddress.get(96))
						.update(TaddressDB.COMMONBOOL10,standardAddress.get(97))
						
							.update(TaddressDB.COMMONDATE01,DateWrapper( standardAddress.get(98)))
						.update(TaddressDB.COMMONDATE02,DateWrapper( standardAddress.get(99)))
						.update(TaddressDB.COMMONDATE03,DateWrapper( standardAddress.get(100)))
						.update(TaddressDB.COMMONDATE04,DateWrapper( standardAddress.get(101)))
						.update(TaddressDB.COMMONDATE05,DateWrapper( standardAddress.get(102)))
						.update(TaddressDB.COMMONDATE06,DateWrapper( standardAddress.get(103)))
						.update(TaddressDB.COMMONDATE07,DateWrapper( standardAddress.get(104)))
						.update(TaddressDB.COMMONDATE08,DateWrapper( standardAddress.get(105)))
						.update(TaddressDB.COMMONDATE09,DateWrapper( standardAddress.get(106)))
						.update(TaddressDB.COMMONDATE10,DateWrapper( standardAddress.get(107)))
						
							.update(TaddressDB.COMMONMONEY01,numString((String)standardAddress.get(108)))
						.update(TaddressDB.COMMONMONEY02,numString((String)standardAddress.get(109)))
						.update(TaddressDB.COMMONMONEY03,numString((String)standardAddress.get(110)))
						.update(TaddressDB.COMMONMONEY04,numString((String)standardAddress.get(111)))
						.update(TaddressDB.COMMONMONEY05,numString((String)standardAddress.get(112)))
						.update(TaddressDB.COMMONMONEY06,numString((String)standardAddress.get(113)))
						.update(TaddressDB.COMMONMONEY07,numString((String)standardAddress.get(114)))
						.update(TaddressDB.COMMONMONEY08,numString((String)standardAddress.get(115)))
						.update(TaddressDB.COMMONMONEY09,numString((String)standardAddress.get(116)))
						.update(TaddressDB.COMMONMONEY10,numString((String)standardAddress.get(117)))
						
						.update(TaddressDB.PICTURE, new RawClause(questionmark.toString())) // insert ? for prepared statement
						
						/**
						 * ACHTUNG!!! Keine mehrfachen '?' einf�gen!!! Die Eintr�ge werden in der Map zuf�llig sortiert, so dass 
						 * das Ersetzen der '?' sp�ter in zuf�lliger Reihenfolge passiert					 * 
						 */
						
						.update(TaddressDB.PICTUREEXPRESSION, (String) standardAddress.get(119)) 
						.update(TaddressDB.FOLLOWUP, boolToInt( (Boolean) standardAddress.get(120)));
	            		
						if (standardAddress.get(120) != null && ((Boolean) standardAddress.get(120)).booleanValue() && standardAddress.get(121) != null){
		            		// User und followupdate werden nur �berschrieben, wenn Flag gesetzt wurde und das followupdate manipuliert wurde
		            		// wurde followupdate nicht manipuliert, wird dies clientseitig durch NULL repr�sentiert
		            		
		            		update.update(TaddressDB.FOLLOWUPDATE, DateWrapper(standardAddress.get(121)));
		    				update.update(TaddressDB.FKUSERFOLLOWUP, user_id);
		        			//System.out.println("****** FK_USER_FOLLOWUP -> USER_ID: " + user_id);
		        			//System.out.println("****** FOLLOWUPDATE -> " + DateWrapper(standardAddress.get(121)));
						}
						if (standardAddress.get(120) != null && !((Boolean) standardAddress.get(120)).booleanValue()){
							
							update.update(TaddressDB.FOLLOWUPDATE, null);
							update.update(TaddressDB.FKUSERFOLLOWUP, null);
							
						}
						
	            		//.update(TaddressDB.FKUSERFOLLOWUP, (Integer)  standardAddress.get(122)) //user_id)
						
						update.update(TaddressDB.LETTERADDRESS, standardAddress.get(123))
						
						.update(TaddressDB.LETTERSALUTATIONAUTO, boolToInt((Boolean)standardAddress.get(124)) )
						.update(TaddressDB.LETTERADDRESSAUTO, boolToInt((Boolean)standardAddress.get(125)))
						.update(TaddressDB.LETTERSALUTATIONSHORT, standardAddress.get(126))
						.update(TaddressDB.STREET3, standardAddress.get(127))
						.update(TaddressDB.HOUSENO3, standardAddress.get(128))
						.update(TaddressDB.ZIPCODE3, standardAddress.get(129))
						.update(TaddressDB.CITY3, standardAddress.get(130))
						.update(TaddressDB.REGION3, standardAddress.get(131))
						.update(TaddressDB.COUNTRY3, standardAddress.get(132))
						.update(TaddressDB.POBOX3, standardAddress.get(133))
						.update(TaddressDB.POBOXZIPCODE3, standardAddress.get(134))
						.update(TaddressDB.ADDRESSSUFFIX3, standardAddress.get(135));				
						
						update.where(Expr.equal(TaddressDB.PK_PKADDRESS, addressPk));
			    
	            
	            String sql = update.toString();
	            
        
	            java.sql.Connection connection = DB.getConnection(TcDBContext.getDefaultContext());
	            PreparedStatement insertAddressAction = null;
	            
	            try {
					insertAddressAction = connection.prepareStatement(sql);
					insertAddressAction.setBytes(1,(byte[]) standardAddress.get(118) );
					insertAddressAction.execute();
				} catch (SQLException e5) {
					
					logger.log(Level.WARNING, "Fehler in Funktion updateAddress()");
					e5.printStackTrace();
				}
				finally {
		        	
		        	 if (insertAddressAction != null){
		        	 	insertAddressAction.close();
		        	 }
		        }
	        }
	        
	        if (parameter.containsKey("note")
	                && parameter.get("note") instanceof String) {
	            if (parameter.containsKey("categoryPk")
	                    && parameter.get("categoryPk") instanceof Integer) {
	            	WhereList wl = new WhereList();
	            	wl.addAnd(Expr.equal(TaddresscategoryDB.FKADDRESS, addressPk));
	            	wl.addAnd(Expr.equal(TaddresscategoryDB.FKCATEGORY, (Integer) parameter.get("categoryPk")));
	                Update update = 
	                    SQL.Update(TcDBContext.getDefaultContext())
	                    	.table(TaddresscategoryDB.getTableName())
	                        .update(TaddresscategoryDB.NOTE, (String) parameter.get("note"))
							.where(wl);
	                
	                DB.update(TcDBContext.getDefaultContext(), update.toString());
	                
	            }
	        }
	 
	        if (standardAddress != null) {
	            Map commChanges = new HashMap();
	            commChanges.put(new Integer(TEL_DI), (String)standardAddress.get(17));
	            commChanges.put(new Integer(TEL_PRI), (String)standardAddress.get(18));
	            commChanges.put(new Integer(HANDY_DI), (String)standardAddress.get(19));
	            commChanges.put(new Integer(HANDY_PRI), (String)standardAddress.get(20));
	            commChanges.put(new Integer(FAX_DI), (String)standardAddress.get(21));
	            commChanges.put(new Integer(FAX_PRI), (String)standardAddress.get(22));
	            commChanges.put(new Integer(EMAIL1), (String)standardAddress.get(23));
	            commChanges.put(new Integer(EMAIL2), (String)standardAddress.get(24));
	            commChanges.put(new Integer(HOMEPAGE_DI), (String)standardAddress.get(28));
	            commChanges.put(new Integer(EMAIL_WEITERE), (String)standardAddress.get(51));
	            commChanges.put(new Integer(FAX_WEITERES), (String)standardAddress.get(52));
	            commChanges.put(new Integer(HANDY_WEIERES), (String)standardAddress.get(53));
	            commChanges.put(new Integer(TEL_WEITERES), (String)standardAddress.get(54));
	            commChanges.put(new Integer(HOMEPAGE_PRI), (String)standardAddress.get(55));
	            commChanges.put(new Integer(HOMEPAGE_WEITERE), (String)standardAddress.get(56));
	
	            TcCommDB.saveCommData(addressPk, commChanges);
	            
	/*            TcCommDB.saveCommData(module, addressPk, standardAddress, 17, TEL_DI); //	tedi
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 18, TEL_PRI); //	tepri
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 19, HANDY_DI); //	hadi
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 20, HANDY_PRI); //	hapr
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 21, FAX_DI); //	fadi
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 22, FAX_PRI); //	fapr
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 23, EMAIL1); //	em1
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 24, EMAIL2); //	em2
	            TcCommDB.saveCommData(module, addressPk, standardAddress, 28, HOMEPAGE_DI); //	homepage
	
			    TcCommDB.saveCommData(module, addressPk, standardAddress, 51, EMAIL_WEITERE); 
			    TcCommDB.saveCommData(module, addressPk, standardAddress, 52, FAX_WEITERES); 
			    TcCommDB.saveCommData(module, addressPk, standardAddress, 53, HANDY_WEIERES);
			    TcCommDB.saveCommData(module, addressPk, standardAddress, 54, TEL_WEITERES); 
		        TcCommDB.saveCommData(module, addressPk, standardAddress, 55, HOMEPAGE_PRI); 
		        TcCommDB.saveCommData(module, addressPk, standardAddress, 56, HOMEPAGE_WEITERE); 
	*/
	        }
        } // Ende if (user darf adresse editieren)
        //else System.out.println("User durfte Adresse nicht editieren");
        
        
        if (WorkerFactory.getSecurityWorker(all)
        	.hasUserRightOnAddress(all, all.personalConfig().getLoginname(), new Integer(SecurityQueries.FOLDERRIGHT_STRUCTURE), addressPk)
        	.booleanValue()){
        	
	        //	Verkn�pfung in Taddresssubfolder aktualisieren
	        if (subCategories != null) {
	            	            	
                //System.out.println("Anzahl der zugeordneten Unterkategorien:" + subCategories.size());
	            	
                //Unterkategorien hinzuf�gen, die noch nicht in der DB sind
                for (Iterator it = subCategories.iterator();it.hasNext();) {
                    //Testen, ob UK schon zugeordnet ist...
                    Integer subCategoryPk = new Integer(it.next().toString());
                    WhereList wl = new WhereList();
                    wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKSUBCATEGORY, subCategoryPk));
                    wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKADDRESS, addressPk));
                    Select select = 
                        SQL.Select(TcDBContext.getDefaultContext())
                        .from(TaddresssubcategoryDB.getTableName())
                        .select(TaddresssubcategoryDB.PK_PK)
                        .where(wl);
                    Result r = DB.result(TcDBContext.getDefaultContext(), select.toString());
                    ResultSet rs = null;
						
                    Select select2 = 
                        SQL.Select(TcDBContext.getDefaultContext())
                        .from(TsubcategoryDB.getTableName())
                        .select(TsubcategoryDB.FKCATEGORY)
                        .where(Expr.equal(TsubcategoryDB.PK_PKSUBCATEGORY, subCategoryPk));
						
                    Result r2 = DB.result(TcDBContext.getDefaultContext(), select2.toString());
                    ResultSet rs2 = null;
						

                    Integer currentCategory = null;
                    try{
                        rs2 = r2.resultSet();
                        //System.out.println(select2.toString());
						
                        if (rs2.next()){
                            currentCategory = new Integer(rs2.getInt("fk_folder"));

                            // Niemals in eine Virtuelle Kategorie einf�gen
                            if (isCategoryVirtual(parameter, currentCategory))
                                continue;
                        }
							
                    }
                    finally{
                        if(r2!=null)r2.close();
                    }
						
						
						
                    try{
                        rs = r.resultSet();
                        if(rs.next()){
                            //Adresse schon in dieser UK vorhanden...
                            //Wir brauchen nichts zu tun
                        }else{
								
								
                            //Nicht vorhanden, wir m�ssen einf�gen
                            Insert insert =
                                SQL.Insert(TcDBContext.getDefaultContext())
                                .table(TaddresssubcategoryDB.getTableName())
                                .insert(TaddresssubcategoryDB.FKSUBCATEGORY, subCategoryPk)
                                .insert(TaddresssubcategoryDB.FKADDRESS, addressPk)
                                .insert(TaddresssubcategoryDB.FKCATEGORY, currentCategory);
			            
                            DB.update(TcDBContext.getDefaultContext(), insert.toString());
                        }
                    }finally{
                        if(r!=null)r.close();
                    }
                }
					
                //Unterkategorien l�schen, die in der DB, aber nicht in subCategories enthalten sind
                WhereList wl = new WhereList();
                //when creating a new user (in tuser) categoryPk could be null
                //below comment says FKCATEGORY not necessary, but i have mentioned that the expression
                // is neccessary to avoid that all subcategories get deleted from the address when no 
                // subcategorykeys and no categorypk are given by the client.
                // This is the case when a user is created and its address is updated.
                // So in this case we test against -1 to get a negativ statement
               
                if (categoryPk != null)
                {
                	wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKCATEGORY, categoryPk));
                }
                else 
                	wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKCATEGORY, -1));
                
                wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKADDRESS, addressPk));
                Select selectuk = 
                    SQL.Select(TcDBContext.getDefaultContext())
                    .from(TaddresssubcategoryDB.getTableName())
                    .selectAs(TaddresssubcategoryDB.PK_PK)
                    .selectAs(TaddresssubcategoryDB.FKSUBCATEGORY)
                    // Eigentlich ist es hier ausreichend auf die FKSUBCATEGORY zu selektieren,
                    // ich lasse den test auf FKCATEGORY aber vorsichtshalber auch noch drinnen.
                    .where(wl);
                Result resultuk = DB.result(TcDBContext.getDefaultContext(), selectuk.toString());
                ResultSet rsuk = resultuk.resultSet();
                while(rsuk.next()){
                    Integer pk = new Integer(rsuk.getInt(TaddresssubcategoryDB.PK_PK));
                    Integer subcategory = new Integer(rsuk.getInt(TaddresssubcategoryDB.FKSUBCATEGORY));
                    if(!subCategories.contains(subcategory)){
                        //UK in DB aber nicht mehr zugeordnet...
                        Delete delete =
                            SQL.Delete(TcDBContext.getDefaultContext())
                            .from(TaddresssubcategoryDB.getTableName())
                            .where(Expr.equal(TaddresssubcategoryDB.PK_PK, pk));
                        DB.update(TcDBContext.getDefaultContext(), delete.toString());
                    }else{
                        //Ist in DB und in subcategories...
                    }
                }					            
	        }
        } // Ende if (user darf U-kats zuweisen)
        //else System.out.println("User durfte Adresse nicht neu zuweisen");
        
        
        
		
			
	        //	Label aus der Datenbank laden
		    Select select = 
		    	SQL.Select(TcDBContext.getDefaultContext()).selectAs(TaddressextDB.LABELFN)
		        	.from(TaddressextDB.getTableName())
		        	.where(Expr.equal(TaddressextDB.PK_PKADDRESSEXT, addressPk));
		     
		    Result tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
		        
	        try {
	            if (tr.resultSet().next()) {
	                String prev = tr.resultSet().getString(TaddressextDB.LABELFN);
	                if (prev != null)
	                    prev = prev.replaceAll("#", "/;");
	                preview
	                	.append("#")
	                	.append(prev)
	                	.append("#");
	            }
	        } catch (SQLException e3) {
	            throw (e3);
	        } finally {
	            if (tr != null) tr.close();
	        }

	        for (Iterator it = subCategories.iterator(); it.hasNext();) {
	        	preview.append(it.next());
		        if (it.hasNext()) 
		        	preview.append(" ");
	        }
		 
		
		
        return preview.toString();
    }
    
    private static boolean isCategoryVirtual(Map parameter, Integer categoryPk) throws SQLException {
		Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TcategoryDB.getTableName()).select(TcategoryDB.ISVIRTUAL).where(Expr.equal(TcategoryDB.PK_PKCATEGORY, categoryPk));
		Result tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
		ResultSet rs = tr.resultSet();
		if(rs.next()){
			int isvirtual = rs.getInt(1);
			if(isvirtual!=0)return true;
		}
		return false;
	}

	private static Date DateWrapper (Object o){
		if (o == null) {
			return null;
		} else {
			if (o instanceof String) {
				try {
					return DF.parse((String) o);
				} catch (ParseException e) {
					return new Date();
				}
			} else if (o instanceof GregorianCalendar){
			    return ((GregorianCalendar)o).getTime(); 
			} else
				return (Date) o;
		}
	}
	static private final SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd");

	private static String numString (String NumString){
	    if ( NumString != null && NumString.equals(""))
	        return "0.0";
        return NumString;
	}
	
	
    static public String saveAddress(Integer user_id, Map parameter)
            throws InputParameterException, SQLException {
    	
    	Character questionmark = new Character('?');
    	
        StringBuffer preview = new StringBuffer();
        if (!parameter.containsKey("category")) { throw new InputParameterException(
                "F�r das Speichern einer Adresse muss der Parameter 'category' angegeben sein."); }
        if (!parameter.containsKey("subCategories")) { throw new InputParameterException(
                "F�r das Speichern einer Adresse muss der Parameter 'subCategories' angegeben sein."); }
        if (!parameter.containsKey("standardAddress")) { throw new InputParameterException(
                "F�r das Speichern einer Adresse muss der Parameter 'standardAddress' angegeben sein."); }
        Integer category = null;
        List subCategories = null;
        List standardAddress = null;
        List extendedAddress = null;
        String note = null;
        try {
            category = (Integer) parameter.get("category");
        } catch (ClassCastException e) {
            throw new InputParameterException(
                    "F�r das Speichern einer Adresse muss der Parameter 'category' als Integer �bergeben werden.");
        }
        try {
            subCategories = (List) parameter.get("subCategories");
        } catch (ClassCastException e) {
            throw new InputParameterException(
                    "F�r das Speichern einer Adresse muss der Parameter 'subCategories' als Liste �bergeben werden.");
        }
        try {
            standardAddress = (List) parameter.get("standardAddress");
        } catch (ClassCastException e) {
            throw new InputParameterException(
                    "F�r das Speichern einer Adresse muss der Parameter 'standardAddress' als Liste �bergeben werden.");
        }
        if (parameter.containsKey("note")) {
            try {
                note = (String) parameter.get("note");
            } catch (ClassCastException e4) {
                throw new InputParameterException(
                        "F�r das Speichern einer Adresse muss der Parameter 'note' als String �bergeben werden.");
            }
        }
        Integer nextPk = null;
        Insert insert = null;
        try {
            insert = SQL
                .Insert(TcDBContext.getDefaultContext())
                .table(TaddressDB.getTableName())
                .insert(TaddressDB.LETTERSALUTATION,(String) standardAddress.get(0))
                .insert(TaddressDB.SALUTATION,(String) standardAddress.get(1))
                .insert(TaddressDB.TITELJOB,(String) standardAddress.get(2))
                .insert(TaddressDB.TITELACADEMIC,(String) standardAddress.get(3))
                .insert(TaddressDB.FIRSTNAME,(String) standardAddress.get(4))
                .insert(TaddressDB.LASTNAME,(String) standardAddress.get(5))
                .insert(TaddressDB.ORGANISATION,(String) standardAddress.get(6))
                .insert(TaddressDB.POSITION,(String) standardAddress.get(7))
                .insert(TaddressDB.DEPARTMENT,(String) standardAddress.get(8))
                .insert(TaddressDB.STREET, (String) standardAddress.get(9))
                .insert(TaddressDB.HOUSENO,(String) standardAddress.get(10))
                .insert(TaddressDB.ZIPCODE,(String) standardAddress.get(11))
                .insert(TaddressDB.CITY, (String) standardAddress.get(12))
                .insert(TaddressDB.POBOXZIPCODE,(String) standardAddress.get(13))
                .insert(TaddressDB.POBOX, (String) standardAddress.get(14))
                .insert(TaddressDB.COUNTRY,(String) standardAddress.get(15))
                .insert(TaddressDB.REGION, (String) standardAddress.get(16))
                .insert(TaddressDB.NAMESUFFIX,(String) standardAddress.get(27))
                .insert(TaddressDB.MIDDLENAME,(String) standardAddress.get(29))
                .insert(TaddressDB.NICKNAME,(String) standardAddress.get(30))
                .insert(TaddressDB.BANKNAME,(String) standardAddress.get(31))
                .insert(TaddressDB.BANKNO, (String) standardAddress.get(32))
                .insert(TaddressDB.BANKACCOUNT,(String) standardAddress.get(33))
                .insert(TaddressDB.SEX,((String) standardAddress.get(34)).toString())
                .insert(TaddressDB.YEAROFBIRTH,(Integer) standardAddress.get(35))
                .insert(TaddressDB.DATEOFBIRTH,(standardAddress.get(36)))
                .insert(TaddressDB.NOTEONADDRESS,(String) standardAddress.get(37))
	            .insert(TaddressDB.STREET2,(String) standardAddress.get(40))
	            .insert(TaddressDB.HOUSENO2,(String) standardAddress.get(41))
	            .insert(TaddressDB.ZIPCODE2,(String) standardAddress.get(42))
	            .insert(TaddressDB.CITY2,(String) standardAddress.get(43))
	            .insert(TaddressDB.REGION2,(String) standardAddress.get(44))
	            .insert(TaddressDB.COUNTRY2,(String) standardAddress.get(46))
	            .insert(TaddressDB.POBOX2,(String) standardAddress.get(47))
	            .insert(TaddressDB.POBOXZIPCODE2,(String) standardAddress.get(48))
	            .insert(TaddressDB.ADDRESSSUFFIX,(String) standardAddress.get(49))
	            .insert(TaddressDB.ADDRESSSUFFIX2,(String) standardAddress.get(50));

            
            	if (standardAddress.get(57) != null)
            	    insert.insert(TaddressDB.FKUSER, standardAddress.get(57).toString());
            	
            	insert.insert(TaddressDB.COMMONTEXT01,(String) standardAddress.get(58))
            	.insert(TaddressDB.COMMONTEXT02,(String) standardAddress.get(59))
            	.insert(TaddressDB.COMMONTEXT03,(String) standardAddress.get(60))
            	.insert(TaddressDB.COMMONTEXT04,(String) standardAddress.get(61))
            	.insert(TaddressDB.COMMONTEXT05,(String) standardAddress.get(62))
            	.insert(TaddressDB.COMMONTEXT06,(String) standardAddress.get(63))
            	.insert(TaddressDB.COMMONTEXT07,(String) standardAddress.get(64))
            	.insert(TaddressDB.COMMONTEXT08,(String) standardAddress.get(65))
            	.insert(TaddressDB.COMMONTEXT09,(String) standardAddress.get(66))
            	.insert(TaddressDB.COMMONTEXT10,(String) standardAddress.get(67))
            	
            	.insert(TaddressDB.COMMONTEXT11,(String) standardAddress.get(68))
            	.insert(TaddressDB.COMMONTEXT12,(String) standardAddress.get(69))
            	.insert(TaddressDB.COMMONTEXT13,(String) standardAddress.get(70))
            	.insert(TaddressDB.COMMONTEXT14,(String) standardAddress.get(71))
            	.insert(TaddressDB.COMMONTEXT15,(String) standardAddress.get(72))
            	.insert(TaddressDB.COMMONTEXT16,(String) standardAddress.get(73))
            	.insert(TaddressDB.COMMONTEXT17,(String) standardAddress.get(74))
            	.insert(TaddressDB.COMMONTEXT18,(String) standardAddress.get(75))
            	.insert(TaddressDB.COMMONTEXT19,(String) standardAddress.get(76))
            	.insert(TaddressDB.COMMONTEXT20,(String) standardAddress.get(77))
            	
				.insert(TaddressDB.COMMONINT01, standardAddress.get(78))
				.insert(TaddressDB.COMMONINT02, standardAddress.get(79))
				.insert(TaddressDB.COMMONINT03, standardAddress.get(80))
				.insert(TaddressDB.COMMONINT04, standardAddress.get(81))
				.insert(TaddressDB.COMMONINT05, standardAddress.get(82))
				.insert(TaddressDB.COMMONINT06, standardAddress.get(83))
				.insert(TaddressDB.COMMONINT07, standardAddress.get(84))
				.insert(TaddressDB.COMMONINT08, standardAddress.get(85))
				.insert(TaddressDB.COMMONINT09, standardAddress.get(86))
				.insert(TaddressDB.COMMONINT10, standardAddress.get(87))
				
				.insert(TaddressDB.COMMONBOOL01, standardAddress.get(88))
				.insert(TaddressDB.COMMONBOOL02,standardAddress.get(89))
				.insert(TaddressDB.COMMONBOOL03,standardAddress.get(90))
				.insert(TaddressDB.COMMONBOOL04,standardAddress.get(91))
				.insert(TaddressDB.COMMONBOOL05,standardAddress.get(92))
				.insert(TaddressDB.COMMONBOOL06,standardAddress.get(93))
				.insert(TaddressDB.COMMONBOOL07,standardAddress.get(94))
				.insert(TaddressDB.COMMONBOOL08,standardAddress.get(95))
				.insert(TaddressDB.COMMONBOOL09,standardAddress.get(96))
				.insert(TaddressDB.COMMONBOOL10,standardAddress.get(97))
				
				.insert(TaddressDB.COMMONDATE01,DateWrapper( standardAddress.get(98)))
				.insert(TaddressDB.COMMONDATE02,DateWrapper( standardAddress.get(99)))
				.insert(TaddressDB.COMMONDATE03,DateWrapper( standardAddress.get(100)))
				.insert(TaddressDB.COMMONDATE04,DateWrapper( standardAddress.get(101)))
				.insert(TaddressDB.COMMONDATE05,DateWrapper( standardAddress.get(102)))
				.insert(TaddressDB.COMMONDATE06,DateWrapper( standardAddress.get(103)))
				.insert(TaddressDB.COMMONDATE07,DateWrapper( standardAddress.get(104)))
				.insert(TaddressDB.COMMONDATE08,DateWrapper( standardAddress.get(105)))
				.insert(TaddressDB.COMMONDATE09,DateWrapper( standardAddress.get(106)))
				.insert(TaddressDB.COMMONDATE10,DateWrapper( standardAddress.get(107)))
				
				.insert(TaddressDB.COMMONMONEY01,numString((String)standardAddress.get(108)))
				.insert(TaddressDB.COMMONMONEY02,numString((String)standardAddress.get(109)))
				.insert(TaddressDB.COMMONMONEY03,numString((String)standardAddress.get(110)))
				.insert(TaddressDB.COMMONMONEY04,numString((String)standardAddress.get(111)))
				.insert(TaddressDB.COMMONMONEY05,numString((String)standardAddress.get(112)))
				.insert(TaddressDB.COMMONMONEY06,numString((String)standardAddress.get(113)))
				.insert(TaddressDB.COMMONMONEY07,numString((String)standardAddress.get(114)))
				.insert(TaddressDB.COMMONMONEY08,numString((String)standardAddress.get(115)))
				.insert(TaddressDB.COMMONMONEY09,numString((String)standardAddress.get(116)))
				.insert(TaddressDB.COMMONMONEY10,numString((String)standardAddress.get(117)))
				
            	.insert(TaddressDB.PICTURE, new RawClause(questionmark.toString())) // insert ? for prepared statement
				
				/**
				 * ACHTUNG!!! Keine mehrfachen '?' einf�gen!!! Die Eintr�ge werden in der Map zuf�llig sortiert, so dass 
				 * das Ersetzen der '?' sp�ter in zuf�lliger Reihenfolge passiert					 * 
				 */
				
				.insert(TaddressDB.PICTUREEXPRESSION, (String) standardAddress.get(119))
            	.insert(TaddressDB.FOLLOWUP, boolToInt((Boolean) standardAddress.get(120)));
				
    			//.insert(TaddressDB.FKUSERFOLLOWUP, (Integer) standardAddress.get(122)); //user_id);
				
            	if (standardAddress.get(120) != null && ((Boolean) standardAddress.get(120)).booleanValue()){
            		
            		insert.insert(TaddressDB.FOLLOWUPDATE, DateWrapper(standardAddress.get(121)));
    				insert.insert(TaddressDB.FKUSERFOLLOWUP, user_id);
        			//System.out.println("****** FK_USER_FOLLOWUP -> USER_ID: " + user_id);
				}
				else {					
					insert.insert(TaddressDB.FOLLOWUPDATE, null);
					insert.insert(TaddressDB.FKUSERFOLLOWUP, null);									
				}
				
            	insert.insert(TaddressDB.LETTERADDRESS, (String) standardAddress.get(123))
            	.insert(TaddressDB.LETTERSALUTATIONAUTO, boolToInt((Boolean) standardAddress.get(124)))
            	.insert(TaddressDB.LETTERADDRESSAUTO, boolToInt((Boolean) standardAddress.get(125)))
				.insert(TaddressDB.LETTERSALUTATIONSHORT, standardAddress.get(126))
				.insert(TaddressDB.STREET3, standardAddress.get(127))
				.insert(TaddressDB.HOUSENO3, standardAddress.get(128))
				.insert(TaddressDB.ZIPCODE3, standardAddress.get(129))
				.insert(TaddressDB.CITY3, standardAddress.get(130))
				.insert(TaddressDB.REGION3, standardAddress.get(131))
				.insert(TaddressDB.COUNTRY3, standardAddress.get(132))
				.insert(TaddressDB.POBOX3, standardAddress.get(133))
				.insert(TaddressDB.POBOXZIPCODE3, standardAddress.get(134))
				.insert(TaddressDB.ADDRESSSUFFIX3, standardAddress.get(135));	

        } catch (ClassCastException e1) {
            throw new InputParameterException(
                "Beim Lesen der Liste ist ein unerwarteter Objecttyp aufgetreten.");
        }
        
        String sql = insert.toString();
        
        
        java.sql.Connection connection = DB.getConnection(TcDBContext.getDefaultContext());
        PreparedStatement insertAddressAction = null;
        
        try {
			insertAddressAction = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			insertAddressAction.setBytes(1,(byte[]) standardAddress.get(118) );			
			insertAddressAction.executeUpdate();
			InsertKeys keys = DB.returnGeneratedKeys(TcDBContext.getDefaultContext(), insertAddressAction, sql);
			nextPk = keys.getPkAsInteger();
        } catch (SQLException e2) {
        	
        	logger.log(Level.WARNING, "Fehler in Funktion saveAddress()");
			e2.printStackTrace();
		} 
        finally {
        	
        	 if (insertAddressAction != null){
        	 	insertAddressAction.close();
        	 }
        }
        preview.append(nextPk).append(" ");
        
        //DB.update((String) parameter.get("module"), insert.toString());
        
        Map commChanges = new HashMap();
        commChanges.put(new Integer(TEL_DI), (String)standardAddress.get(17));
        commChanges.put(new Integer(TEL_PRI), (String)standardAddress.get(18));
        commChanges.put(new Integer(HANDY_DI), (String)standardAddress.get(19));
        commChanges.put(new Integer(HANDY_PRI), (String)standardAddress.get(20));
        commChanges.put(new Integer(FAX_DI), (String)standardAddress.get(21));
        commChanges.put(new Integer(FAX_PRI), (String)standardAddress.get(22));
        commChanges.put(new Integer(EMAIL1), (String)standardAddress.get(23));
        commChanges.put(new Integer(EMAIL2), (String)standardAddress.get(24));
        commChanges.put(new Integer(HOMEPAGE_DI), (String)standardAddress.get(28));
        commChanges.put(new Integer(EMAIL_WEITERE), (String)standardAddress.get(51));
        commChanges.put(new Integer(FAX_WEITERES), (String)standardAddress.get(52));
        commChanges.put(new Integer(HANDY_WEIERES), (String)standardAddress.get(53));
        commChanges.put(new Integer(TEL_WEITERES), (String)standardAddress.get(54));
        commChanges.put(new Integer(HOMEPAGE_PRI), (String)standardAddress.get(55));
        commChanges.put(new Integer(HOMEPAGE_WEITERE), (String)standardAddress.get(56));	

        TcCommDB.saveCommData(nextPk, commChanges);
        
        insert = SQL.Insert(TcDBContext.getDefaultContext()).insert(TaddresscategoryDB.FKADDRESS, nextPk)
                .insert(TaddresscategoryDB.FKCATEGORY, category).table(TaddresscategoryDB.getTableName());
        if (note != null) {
            insert.insert(TaddresscategoryDB.NOTE, note);
        }
        DB.update(TcDBContext.getDefaultContext(), insert.toString());

        // Das Einf�gen in Oberkategorien wir automatisch �ber einen Trigger
        // am Einf�gen in die Unterkategorien erledigt.
        
        if (subCategories != null) {
            for (Iterator it = subCategories.iterator(); it.hasNext();) {
                Integer nextSubCat = (Integer) it.next();
                Select select = SQL.Select(TcDBContext.getDefaultContext()).selectAs(TsubcategoryDB.FKCATEGORY).from(TsubcategoryDB.getTableName()).where(Expr.equal(TsubcategoryDB.PK_PKSUBCATEGORY, nextSubCat));
                ResultSet rs = DB.getResultSet(TcDBContext.getDefaultContext(), select);
                if(!rs.next()){
                	
                }
                Integer fkcategory = rs.getInt(TsubcategoryDB.FKCATEGORY);
                DB.close(rs);
            	insert = SQL.Insert(TcDBContext.getDefaultContext())
                    .table(TaddresssubcategoryDB.getTableName())
                    .insert(TaddresssubcategoryDB.FKADDRESS, nextPk)
                    .insert(TaddresssubcategoryDB.FKSUBCATEGORY,nextSubCat)
                    .insert(TaddresssubcategoryDB.FKCATEGORY, fkcategory);
                //.insert(TaddresssubcategoryDB.FKCATEGORY, category);
                
                DB.update(TcDBContext.getDefaultContext(), insert.toString());

            }
        }
        Select select = SQL.Select(TcDBContext.getDefaultContext())
        	.from(TaddressextDB.getTableName())
            .selectAs(TaddressextDB.LABELFN)
            .where(Expr.equal(TaddressextDB.PK_PKADDRESSEXT, nextPk));


        Result tr = null;
        
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            if (tr.resultSet().next()) {
                preview.append("#").append(
                        tr.resultSet().getString(TaddressextDB.LABELFN))
                        .append("#");
            }
        } catch (SQLException e3) {
            throw (e3);
        } finally {
            if (tr != null) {
                tr.close();
            }
        }
        
        for (Iterator it = subCategories.iterator(); it.hasNext();) {
            preview.append(it.next());
            if (it.hasNext()) preview.append(" ");
        }
        return preview.toString();
    }

    public static void deleteAddress(Map parameter)
            throws InputParameterException, SQLException {
        if (!parameter.containsKey("addressPk")) { throw new InputParameterException(
                "F�r das L�schen einer Adresse muss der Parameter 'addressPk' �bergeben werden."); }
        if (!parameter.containsKey("category")) { throw new InputParameterException(
                "F�r das L�schen einer Adresse muss der Parameter 'category' �bergeben werden."); }
        Integer addressPk = null;
        try {
            addressPk = (Integer) parameter.get("addressPk");
        } catch (ClassCastException e) {
            throw new InputParameterException(
                    "F�r das L�schen einer Adresse muss der Parameter 'addressPk' als Integer �bergeben werden.");
        }
        Integer categoryPk = null;
        try {
            categoryPk = (Integer) parameter.get("category");
            WhereList wl = new WhereList();
            wl.addAnd(Expr.equal(TaddresscategoryDB.FKADDRESS, addressPk));
            wl.addAnd(Expr.equal(TaddresscategoryDB.FKCATEGORY, categoryPk));
            Delete delete = 
                SQL.Delete(TcDBContext.getDefaultContext())
                    .from(TaddresscategoryDB.getTableName())
                    .where(wl);
            
            DB.update(TcDBContext.getDefaultContext(), delete.toString());
            
            wl = new WhereList();
            wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKADDRESS, addressPk));
            wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKCATEGORY, categoryPk));
            delete = 
                SQL.Delete(TcDBContext.getDefaultContext()).from(TaddresssubcategoryDB.getTableName())
                    .where(wl);
            
            DB.update(TcDBContext.getDefaultContext(), delete.toString());
            
            // Test if category is Trash-folder and delete it completely if its the case
            Select select = 
            	SQL.Select(TcDBContext.getDefaultContext())
            		.from(TcategoryDB.getTableName())
            		.join(TcategorytypeDB.getTableName(), TcategoryDB.FKCATEGORYTYPE, TcategorytypeDB.PK_PK)
            		.selectAs(TcategoryDB.PK_PKCATEGORY)
            		.selectAs(TcategorytypeDB.CATEGORYTYPE)
            		.where(Expr.equal(TcategoryDB.PK_PKCATEGORY, categoryPk));
            
            Result tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
        	ResultSet result = tr.resultSet();
        	
        	if (result.next() && result.getString(TcategorytypeDB.CATEGORYTYPE).equals("Deleted Folder")){
        		
        		Delete realDelete = 
                    SQL.Delete(TcDBContext.getDefaultContext())
                        .from(TaddressDB.getTableName())
                        .where(Expr.equal(TaddressDB.PK_PKADDRESS, addressPk));
        		
        		DB.update(TcDBContext.getDefaultContext(), realDelete.toString());
        	}
        	
        	tr.close();
        	result.close();

        } catch (ClassCastException e1) {
            throw new InputParameterException(
                    "F�r das L�schen einer Adresse muss der Parameter 'category' als Integer �bergeben werden.");
        }
    }

    private static void fillResult(List sb, List pkPreviewList,
            StringBuffer subs, StringBuffer result, Integer previewPk,
            String preview) {
        result
        	.append(previewPk)
        	.append("#").append(preview).append("#")
        	.append(subs.toString())
        	.append("^");
        sb.add(result.toString());
    }
    
    

    public static String generateLetterSalutation(String anredekurz, String vorname, String herrfrau, String akad_titel, String titel, String nachname){
        return LetterSalutationGenerator.generateLetterSalutation(anredekurz, vorname, herrfrau, akad_titel, titel, nachname);
    }
    
    
    static public Map getAssociatedCategoriesByAddressPk(Map request, Map content) throws InputParameterException, SQLException{
    	
    

    	if (!content.containsKey("addressID")) {
            throw new InputParameterException(
                    "F�r die Methode 'getAssociatedCategoriesByAddressPk' muss ein Parameter 'addressID' angegeben sein.");
        } 
    	
    	if (!content.containsKey("userid")) {
            throw new InputParameterException(
                    "F�r die Methode 'getAssociatedCategoriesByAddressPk' muss ein Parameter 'userid' angegeben sein.");
        } 
    	
    	Integer pk;
    	Integer userId;
    	Map categoryPks = new HashMap();
        
        try {
            pk = (Integer) content.get("addressID");
        } catch (ClassCastException e1) {
            throw new InputParameterException(
                    "Der Parameter 'addressID' muss als Integer �bergeben werden.");
        }
    
        try {
            userId = (Integer) content.get("userid");
        } catch (ClassCastException e1) {
            throw new InputParameterException(
                    "Der Parameter 'userid' muss als Integer �bergeben werden.");
        }
    
        Result tr = null;
	
        Select select = 
        	SQL.SelectDistinct(TcDBContext.getDefaultContext())
        		.from(TaddresscategoryDB.getTableName())
        		.join(TcDBContext.getSchemaName() + "v_user_address", "v_user_address.fk_address", TaddresscategoryDB.FKADDRESS)
        		.join(TcategoryDB.getTableName(), TcategoryDB.PK_PKCATEGORY, TaddresscategoryDB.FKCATEGORY)
        		.selectAs(TaddresscategoryDB.FKADDRESS)
        		.selectAs(TaddresscategoryDB.FKCATEGORY)
        		.selectAs(TcategoryDB.CATEGORYNAME)
        		.where(Expr.equal(TaddresscategoryDB.FKADDRESS, pk));
        
        tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
    	ResultSet result = tr.resultSet();
    	
    	while (result.next()){
    		categoryPks.put(new Integer(result.getInt(TaddresscategoryDB.FKCATEGORY)), result.getString(TcategoryDB.CATEGORYNAME));
    	}
    	tr.close();
    	result.close();
    	
    	return categoryPks;
    }
    static public class TcAddressSelect {

        static private final String USERID = "userid";
        static private final String CATEGORY = "category";
        static private final String SUBCATEGORIES = "subCategories";
        static private final String INTERSECTION = "intersection";
        static private final String GENERAL_CATEGORY = "generalCategory";
        static private final String FILTER_MAP = "filterMap";
        static private final String ADDRESS_PKS = "addressPks";
        private Map _parameter;
        private Integer _category;
        private List _subCategories;
        private Boolean _generalCategory;
        private Boolean _intersection;

        static private String[] filterParameter = { "labelfn", "vorname", "nachname",
                "herrnFrau", "titel", "akadTitle", "namenszusatz",
                "institution", "stra�e", "plz", "ort", "land", "lkz",
                "bundesland", "telefonDienst", "telefonPrivat", "faxDienst", "faxPrivat", "eMailAdresse", "followupdate"};

        static private String[] dataBaseColumns = { 
        		TaddressextDB.LABELFN,
                TaddressDB.FIRSTNAME,
                TaddressDB.LASTNAME, 
                TaddressDB.SALUTATION,
                TaddressDB.TITELJOB, 
                TaddressDB.TITELACADEMIC,
                TaddressDB.NAMESUFFIX, 
                TaddressDB.ORGANISATION,
                TaddressDB.STREET, 
                TaddressDB.ZIPCODE, 
                TaddressDB.CITY,
                TaddressDB.COUNTRY, 
                TaddressDB.COUNTRYSHORT, 
                TaddressDB.REGION,
                TaddressextDB.FON, 
                TaddressextDB.FONHOME, 
                TaddressextDB.FAX,
                TaddressextDB.FAXHOME, 
                TaddressextDB.EMAIL,
				TaddressDB.FOLLOWUPDATE,
				TaddressDB.FOLLOWUP};

        static private String[] matchCodeColumns = { 
        		TaddressextDB.LABELFNM,
                TaddressextDB.FIRSTNAMEM,
                TaddressextDB.LASTNAMEM, 
                null, 
                null, 
                null, 
                null, 
                null,
                TaddressextDB.STREETM, 
                null,
				TaddressextDB.CITYM, 
                null, 
                null,
                null, 
                null, 
                null, 
                null, 
                null, 
                null,
				null,
				null};

        public TcAddressSelect(Map parameter) {
            _parameter = parameter;
        }

        public Select getPkPreviewMyCategory(Integer userId) throws InputParameterException {
        	WhereList wl = new WhereList();
        	wl.addAnd(Expr.equal(TgroupUserDB.FKUSER, userId));
        	wl.addAnd(Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1)));
            Select select = 
                SQL.SelectDistinct(TcDBContext.getDefaultContext())
                	.from(TaddressextDB.getTableName())
                	.selectAs(TaddressextDB.PK_PKADDRESSEXT)
                	.selectAs(TaddressextDB.LABELFN)
                	.selectAs(TaddresssubcategoryDB.FKSUBCATEGORY)
                	.join(TaddresscategoryDB.getTableName(), TaddresscategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
                	.join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TaddresscategoryDB.FKCATEGORY)
                	.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
                	.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
                	.join(TaddresssubcategoryDB.getTableName(), TaddresssubcategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
                	.where(wl)
                	.orderBy(Order.asc(TaddressextDB.PK_PKADDRESSEXT));
            return select;
        }
        
        public Select getPkPreviewListSelect() throws InputParameterException {
            Select pkListSelect = null;
            if (!_parameter.containsKey(CATEGORY)) {
                throw new InputParameterException(
                        "Es muss eine Kategorie angegeben sein.");
            } else {
                try {
                    _category = (Integer) _parameter.get(CATEGORY);
                } catch (RuntimeException e) {
                    throw new InputParameterException(
                            "Die Kategorie muss als Integer �bergeben sein.");
                }
            }
            if (_parameter.containsKey(SUBCATEGORIES)) {
                try {
                    _subCategories = (List) _parameter.get(SUBCATEGORIES);
                } catch (ClassCastException e) {
                    throw new InputParameterException(
                            "Die Unterkategorien m�ssen als Liste �bergeben werden.");
                }
            }
            WhereList wlpk = new WhereList();
            //	keine Unterkategorien �bergeben
            if (_subCategories == null) {
                pkListSelect = 
                    SQL.SelectDistinct(TcDBContext.getDefaultContext())
	                    .selectAs(TaddressextDB.PK_PKADDRESSEXT)
	                	.selectAs(TaddressextDB.LABELFN).from(TaddressextDB.getTableName())
	                	.join(TaddresscategoryDB.getTableName(),TaddressextDB.PK_PKADDRESSEXT,TaddresscategoryDB.FKADDRESS);
	                	wlpk.addAnd(Expr.equal(TaddresscategoryDB.FKCATEGORY, _category));
            }
            //	Unterkategorien �bergeben
            else {
                pkListSelect = 
                    SQL.SelectDistinct(TcDBContext.getDefaultContext())
                		.from(TaddressextDB.getTableName())
                    	.selectAs(TaddressextDB.PK_PKADDRESSEXT)
                    	.selectAs(TaddressextDB.LABELFN);
                try {
                    _intersection = (Boolean) _parameter.get(INTERSECTION);
                } catch (ClassCastException e) {
                    throw new InputParameterException(
                            "Das 'intersection' Attribut muss als Boolean �bergeben sein.");
                }
                //	Schnitt von Unterkategorien
                if (_intersection != null && _intersection.booleanValue()) {
                    for (Iterator it = _subCategories.iterator(); it.hasNext();) {
                    	WhereList wl = new WhereList();
                    	wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKCATEGORY, _category));
                    	wl.addAnd(Expr.equal(TaddresssubcategoryDB.FKSUBCATEGORY, (Integer)(it.next())));
                        AbstractStatement sub = SQL.Select(TcDBContext.getDefaultContext())
                        	.from(TaddresssubcategoryDB.getTableName())
                        	.select(TaddresssubcategoryDB.FKADDRESS)
                        	.where(wl);
                        wlpk.addAnd(wl);
                    }
                }
                //	Vereinigung von Unterkategorien
                else {
                    pkListSelect
                    	.join(TaddresssubcategoryDB.getTableName(),TaddresssubcategoryDB.FKADDRESS,TaddressextDB.PK_PKADDRESSEXT);
                    wlpk.addAnd(Expr.equal(TaddresssubcategoryDB.FKSUBCATEGORY, _subCategories));
                }
            }
            if (_parameter.containsKey(GENERAL_CATEGORY)) {
                try {
                    _generalCategory = (Boolean) _parameter
                            .get(GENERAL_CATEGORY);
                    if (_generalCategory.booleanValue()
                            && _subCategories != null) {
                        pkListSelect
                        	.join(TaddresscategoryDB.getTableName(),TaddresscategoryDB.FKADDRESS,TaddressextDB.PK_PKADDRESSEXT);
                        if (_intersection != null
                                && _intersection.booleanValue()) {
                            pkListSelect
                            	.join(TcategoryDB.getTableName(),TcategoryDB.PK_PKCATEGORY,TaddresscategoryDB.FKCATEGORY);
                        } else {
                            pkListSelect
                            	.join(TcategoryDB.getTableName(),TcategoryDB.PK_PKCATEGORY,TaddresssubcategoryDB.FKCATEGORY);
                        }
                        wlpk.addAnd(Expr.equal(TcategoryDB.FKCATEGORYTYPE, new Integer(10)));
                    }
                } catch (ClassCastException e) {
                    throw new InputParameterException(
                            "Das Attribut 'generalCategory' muss als Boolean angegeben werden.");
                }
            }
            if (_parameter.containsKey("includeSubCategories")) {
                try {
                    Boolean includeSubCategories = (Boolean) _parameter
                            .get("includeSubCategories");
                    if (includeSubCategories.booleanValue()) {
                        pkListSelect
                                .selectAs(TaddresssubcategoryDB.FKSUBCATEGORY);
                        if (_subCategories == null) {
                            pkListSelect
                            	.join(TaddresssubcategoryDB.getTableName(),TaddresssubcategoryDB.FKADDRESS,TaddressextDB.PK_PKADDRESSEXT);
                        }
                    }
                } catch (ClassCastException e) {
                    throw new InputParameterException(
                            "Das Attribut includeSubCategories muss als Boolean �bergeben sein.");
                }
            }
            pkListSelect
            	.join(TsubcategoryDB.getTableName(),TsubcategoryDB.PK_PKSUBCATEGORY,TaddresssubcategoryDB.FKSUBCATEGORY);
            wlpk.addAnd(Expr.equal(TsubcategoryDB.FKCATEGORY, _category));
            //pkListSelect.order(TaddressextDB.PK_PKADDRESSEXT, SQL.ASC);
            pkListSelect.selectAs(TaddressextDB.SORTORDER);
            pkListSelect.where(wlpk);
            pkListSelect.orderBy(Order.asc(TaddressextDB.SORTORDER));
            
            //System.out.println("getPkPreviewListSelect" + pkListSelect );
            return pkListSelect;
        }

        /**	Zusammenstellung eines Suchstatements */
        public Select getPreviewSearchSelect() throws InputParameterException {
            Select select = null;
            
            Select subSelect =
                SQL.Select(TcDBContext.getDefaultContext())
                	.from(TuserDB.getTableName())
                	.selectAs(TuserDB.PK_PKUSER)
                	.where(Expr.equal(TuserDB.LOGINNAME, (String)_parameter.get("username")));
            
            if (!_parameter.containsKey(ADDRESS_PKS) || _parameter.get(ADDRESS_PKS) == null) {

                    
                    select =
                        SQL.SelectDistinct(TcDBContext.getDefaultContext())
                        	.from(TaddressextDB.getTableName())
                        	.selectAs(TaddressextDB.PK_PKADDRESSEXT)
                        	.join(TaddressDB.getTableName(), TaddressDB.PK_PKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
                        	.join(TaddresscategoryDB.getTableName(), TaddresscategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
                        	.join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TaddresscategoryDB.FKCATEGORY)
							.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
							.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
                        	.where(Expr.equal(TgroupUserDB.FKUSER, new SubSelect(subSelect)))
							.whereAnd(Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1)));
                    
                    if (_parameter.containsKey(FILTER_MAP) && _parameter.get(FILTER_MAP) instanceof Map && ((Map)_parameter.get(FILTER_MAP)).size() > 0) {
                        appendConditions(select, (Map)_parameter.get(FILTER_MAP));
                        
                    return select;
                }
           } else {
               //	Hier die normale Suche entweder �ber Taddress oder Taddressext im Falle der unscharfen Suche
                try {
                    String addressPks = (String) _parameter.get(ADDRESS_PKS);
                    if (!_parameter.containsKey(FILTER_MAP)) {
                        throw new InputParameterException(
                                "F�r die Suche muss eine Map von Kriterien �bergeben werden.");
                    } else {
                        try {
                            Map filters = (Map) _parameter.get(FILTER_MAP);
                            if (filters.size() < 1) {
                                throw new InputParameterException(
                                        "Die Map der Suchkriterien muss mindestens einen Eintrag enthalten.");
                            } else {
                                StringTokenizer st = new StringTokenizer(addressPks, " ");
                                List pks = new ArrayList();
                                while (st.hasMoreTokens()) {
                                    pks.add(new Integer(st.nextToken().trim()));
                                }
                                
                                if(filters.containsKey("matchCodes") && ((Boolean)filters.get("matchCodes")).booleanValue()) {
                                    //	unscharfe Suche �ber Taddressext
                                    select =
                                        SQL.SelectDistinct(TcDBContext.getDefaultContext())
                                        	.from(TaddressextDB.getTableName())
                                        	.selectAs(TaddressextDB.PK_PKADDRESSEXT)
                                             // Taddress muss auch dabei sein, da die addressext nicht alle Spalten hat
                                            .join(TaddressDB.getTableName(), TaddressDB.PK_PKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
											.join(TaddresscategoryDB.getTableName(), TaddresscategoryDB.FKADDRESS, TaddressextDB.PK_PKADDRESSEXT)
											.join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TaddresscategoryDB.FKCATEGORY)
											.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
											.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
											.where(Expr.equal(TgroupUserDB.FKUSER, new SubSelect(subSelect)))
											.whereAnd(Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1)));
                                    		appendConditions(select, filters);
                                    		select.whereAnd(Expr.in(TaddressextDB.PK_PKADDRESSEXT, pks));
                                } else {
                                    //	scharfe Suche �ber Taddress
                                    select =
                                        SQL.SelectDistinct(TcDBContext.getDefaultContext())
                                        	.from(TaddressDB.getTableName())
                                        	.selectAs(TaddressDB.PK_PKADDRESS)
											.join(TaddressextDB.getTableName(), TaddressextDB.PK_PKADDRESSEXT, TaddressDB.PK_PKADDRESS)
											.join(TaddresscategoryDB.getTableName(), TaddresscategoryDB.FKADDRESS, TaddressDB.PK_PKADDRESS)
				                        	.join(TgroupCategoryDB.getTableName(), TgroupCategoryDB.FKCATEGORY, TaddresscategoryDB.FKCATEGORY)
											.join(TgroupUserDB.getTableName(), TgroupUserDB.FKGROUP, TgroupCategoryDB.FKGROUP)
											.join(TcategoryroleDB.getTableName(), TcategoryroleDB.PK_PK, TgroupCategoryDB.FKCATEGORYROLE)
											.where(Expr.equal(TgroupUserDB.FKUSER, new SubSelect(subSelect)))
											.whereAnd(Expr.equal(TcategoryroleDB.AUTHREAD, new Integer(1)));
											appendConditions(select, filters);
											select.whereAnd(Expr.in(TaddressDB.PK_PKADDRESS, pks));
                                }
                                
                            }
                        } catch (ClassCastException e1) {
                            throw new InputParameterException(
                                    "Die f�r die Suche ben�tigten Filter m�ssen als Map �bergeben werden.");
                        }
                    }
                } catch (ClassCastException e) {
                    throw new InputParameterException(
                            "Die PKs m�ssen f�r die Suche als Liste angegeben werden.");
                }
            }
//			try {
//				String ausgabe = select.statementToString();
//				StringTokenizer strTok = new StringTokenizer(ausgabe);
//				int counter = 0;
//				while(strTok.hasMoreTokens()){
//					counter++;
//					if (counter%10 == 0)
//						System.out.println(strTok.nextToken());
//					else
//						System.out.print(strTok.nextToken());
//				}
//			} catch (Exception e) {
//				// TODO: handle exception
//			}            
			return select;
        }

        private void appendConditions(Select select, Map filters)
                throws InputParameterException {
            boolean matchCodes = false;
            try {
                if (filters.containsKey("matchCodes")) {
                    matchCodes = ((Boolean) filters.get("matchCodes"))
                            .booleanValue();
                }
                for (int i = 0; i < filterParameter.length; i++) {
                	
                	if (filters.containsKey(filterParameter[i]) && filters.get(filterParameter[i]) instanceof String){
                	
	                    if (((String) filters.get(filterParameter[i])).length() > 0) {
	                    	
	                        String filter = (String) filters.get(filterParameter[i]);
	                        String column;
	                        
	                        if (matchCodes && matchCodeColumns[i] != null) {
	                            filter = getMatchcode(filter);
	                            column = TcDBContext.getSchemaName() + "lower_fix(" + matchCodeColumns[i]
	                                    + ")";
	                        } else {
	                            column = TcDBContext.getSchemaName() + "lower_fix(" + dataBaseColumns[i]
	                                    + ")";
	                        }
	                        if (filter.indexOf('*') != -1) {
	                        	select.whereAnd(Expr.like(column, filter.replace('*', '%').toLowerCase()));
	                        } else {
	                            select.whereAnd(Expr.like(column, filter.toLowerCase()));
	                        }
	                    }
                	}
                	else if (filters.containsKey(filterParameter[i]) && filters.get(filterParameter[i]) instanceof GregorianCalendar){                			
                			Date filter = ((GregorianCalendar) filters.get(filterParameter[i])).getTime();
                			String  column = dataBaseColumns[i].toLowerCase();
                			select.whereAnd(
                							Expr.less(column, filter));
                	}
                	
                	
                	
                }
            } catch (ClassCastException e) {
                throw new InputParameterException(
                        "Die Suchkriterien m�ssen als Strings angegeben werden.");
            }
        }

        static private String getMatchcode(String value) {
            if (value == null || value.length() == 0) return "";
            if ("1".equals(value)) return "1"; // f�r 
            value = value.toUpperCase().replaceAll("\u00C4", "AE").replaceAll("\u00D6",
                    "OE").replaceAll("\u00DC", "UE").replaceAll("SS", "S")
                    .replaceAll("CK", "K").replaceAll("DT", "D").replaceAll("-", "").replace('\u00DF', 'S');
            StringBuffer buffer = new StringBuffer(value);
            char lastChar = 0;
            int index = 0;
            while (index < buffer.length()) {
                char thisChar = buffer.charAt(index);
                if ((thisChar != lastChar)
                        && ((thisChar == '*') || ((thisChar >= 'A') && (thisChar <= 'Z')))) {
                    lastChar = thisChar;
                    index++;
                } else
                    buffer.deleteCharAt(index);
            }
            return buffer.toString();
        }
        
    }

}