/* $Id: DummyDataAccess.java,v 1.3 2007/01/05 16:57:23 asteban Exp $
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Thomas Fuchs and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.contact.octopus;

import java.util.Map;
import java.util.TreeMap;

import de.tarent.octopus.data.TarDBConnection;
import de.tarent.octopus.data.TcGenericDataAccessWrapper;

/**
 * Dieser Octopus-Datenzugriffswrapper liefert Dummy-Daten.
 * 
 * @author mikel
 */
public class DummyDataAccess extends TcGenericDataAccessWrapper {

	/**
	 * Constructor for DummyDataAccess.
	 * @param dbConnection
	 */
	public DummyDataAccess(TarDBConnection dbConnection) {
		super(dbConnection);
	}

	/**
	 * 	Get extendes fields
	 */
	public Map getExtendedFields() {
		return new TreeMap();
	}

	/**
	 * Constructor for DummyDataAccess.
	 */
	public DummyDataAccess() {
		super();
	}

	/**
	 * Diese Methode liefert eine Map der Gruppen des 
	 * derzeitigen Users
	 */
	public Map getGruppen(String userId) {
		Map result = new TreeMap();
		result.put("1", "Gruppe 1");
		result.put("2", "Gruppe 2");
		result.put("3", "Gruppe 3");
		result.put("4", "Gruppe 4");
		return result;
	}

	/**
	 * Diese Methode liefert eine Map zu einer 
	 * bestimmten Addresse
	 */
	public Map getAddress(int AdrId, String Verteilergrp) {
		Map result = new TreeMap();
		result.put("Vorname", "Michael");
		result.put("Nachname", "Ding");
		result.put("Institution", "K-DOV-43\nVolkswagen");
		result.put("Anrede", "Sehr geehrter");
		result.put("HerrFrau", "Herr");
		return result;
	}

	/**
	 * Diese Methode liefert eine Map aller 
	 * vorhandenen Bundesl�nder
	 */
	public Map getBundeslaender() {
		Map result = new TreeMap();
		result.put("1", "Bayern");
		result.put("2", "Berlin");
		result.put("3", "Hamburg");
		result.put("4", "Saarland");
		return result;
	}

	/**
	 * Diese Methode liefert eine Map aller 
	 * vorhandenen Anreden
	 */
	public Map getAnreden() {
		Map result = new TreeMap();
		result.put("1", "Sehr geehrter");
		result.put("2", "Sehr geehrte");
		result.put("3", "Dear");
		result.put("4", "Ch�r");
		return result;
	}

	/**
	 * Diese Methode pr�ft ob die �bergebene Addresse
	 * in den L�schverteiler geh�rt
	 */
	public void collectOrphan(int adrId) {
	};

	/**
	 * Diese Methode liefert eine Map aller vorhandenen
	 * Landeskennzahlen
	 */
	public Map getLKZ() {
		Map result = new TreeMap();
		result.put("1", "D");
		result.put("2", "B");
		result.put("3", "F");
		result.put("4", "NL");
		return result;
	}

	/**
	 * Diese Methode liefert eine Map aller vorhandenen
	 * L�nder
	 */
	public Map getLaender() {
		Map result = new TreeMap();
		result.put("1", "Deutschland");
		result.put("2", "Belgien");
		result.put("3", "Frankreich");
		result.put("4", "Niederlande");
		return result;
	}

	/**
	 * Diese Methode liefert die Standardverteilergruppe 
	 * des derzeitigen Users
	 */
	public Map getStandardVerteiler(String userId) {
		Map result = new TreeMap();
		result.put("verteiler", "Mein Verteiler");
		return result;
	}

	/**
	 * Diese Methode liefert die Allgemeine Verteilergruppe 
	 */
	public Map getAllgemeinenVerteiler(String userId) {
		Map result = new TreeMap();
		result.put("allgemein", "Allgemeiner Verteiler");
		return result;
	}

	/**
	 * Diese Methode liefert Verteiler 
	 */
	public Map getVerteiler(String userId) {
		Map result = new TreeMap();
		result.put("1", "Schl�ssel1");
		result.put("2", "Schl�ssel2");
		result.put("3", "Schl�ssel3");
		return result;
	}

	/**
	 * Die Methode liefert alle zur Zeit vorhandenen Verteilergruppen
	 */
	public Map getAllVerteilergruppen() {
		Map result = new TreeMap();
		result.put("1", "Schl�ssel1");
		result.put("2", "Schl�ssel2");
		result.put("3", "Schl�ssel3");
		return result;
	}

	/**
	 * Die Methode liefert alle zur Zeit vorhandenen User
	 */
	public Map getUsers() {

		Map result = new TreeMap();
		result.put("ebbi", "Eberhart");
		result.put("danki", "Dankwart");
		result.put("elli", "Elfriede");
		return result;
	}

	/**
	 * Diese Methode inserted einen Addressdatensatz 
	 */
	public Map insertAddress(Map map) {
		Map result = new TreeMap();
		result.put("adrId", new Integer(-1));
		return result;
	}

	/**
	 * Diese Methode updated einen Addressdatensatz 
	 */
	public void updateAddress(Map map) {
	};

	/**
	 * Diese Methode updated ein Abodatum (Subscriptor) 
	 */
	public void updateSubscriptor(int AdrId) {
	};

	/**
	 * Diese Methode setzt eine Bemerkung 
	 */
	public void setBemerkung(
		int adrId,
		String grp,
		String bem,
		String stichwort) {
	};

	/**
	 * Diese Methode aktualisiert einen Verteiler 
	 */
	public void setVerteiler(Map t) {
	};

	/**
	 * Diese Methode l�scht einen Adressdatensatz 
	 */
	public Map deleteAddress(int adrId, String grp) {
		return new TreeMap();
	};

	/**
	 * Diese Methode holt alle Dublikate zu einer Adresse 
	 */
	public Map getDublikate(Map map) {
		Map result = new TreeMap();
		result.put("id", new Integer(1));
		return result;
	}

	/**
	 * Diese Methode gibt die LKZ zu einem �bergebenen Land zur�ck 
	 */
	public Map getLKZforLand(String land) {
		Map result = new TreeMap();
		result.put("lkz", "D");
		return result;
	}

	/**
	 * Diese Methode gibt das Land zu einer �bergebenen LKZ zur�ck 
	 */
	public Map getLandforLKZ(String lkz) {
		Map result = new TreeMap();
		result.put("land", "Deutschland");
		return result;
	}

	public Map getChosenAddresses(Map map) {
		Map sm = new TreeMap();
		sm.put("id0", new Integer(8023));
		sm.put("id1", new Integer(8025));
		sm.put("id2", new Integer(69839));
		return sm;
	}

	public Map getVerteiler(String userId, String grp) {
		Map verteiler = new TreeMap();
		verteiler.put("1", "bla");
		return verteiler;
	}

	/**
	 * Diese Methode setzt die StandardVerteilergruppe
	 * zum eingeloggten Benutzer
	 */
	public Map setStandardVerteilergruppe(String gruppe, String UserID) {
		return new TreeMap();
	}

	/**
	 * Diese Methode erzeugt eine Verteilergruppe 
	 */
	public Map createVerteilergruppe(String key, String name) {
		return new TreeMap();
	}

	/**
	 * Diese Methode erzeugt einen Verteiler 
	 * ACHTUNG: Derzeit wird nur name1+2 verwendet!
	 */
	public Map createVerteiler(
		String gruppe,
		String key,
		String name1,
		String name2,
		String name3) {
		return new TreeMap();
	}

	/**
	 * Diese Methode erzeugt einen neuen Benutzer.
	 */
	public Map createUser(
		String userId,
		String name,
		int special,
		int admin,
		String user) {
		return new TreeMap();
	}

	public Map createVersandAuftrag(Map map) {
		return new TreeMap();
	}

	/**
	 * Diese Methode holt vorhandene Versandauftr�ge zum
	 * eingelogten User
	 * 
	 */
	public Map getVersandAuftraege(String user) {
		return new TreeMap();
	}

	/**
	 * Diese Methode ordnet einem Benutzer eine Verteilergruppe zu.
	 * 
	 * @param verteilergruppe der Verteilergruppenschl�ssel.
	 * @param userId die Benutzer-ID.
	 */
	public Map showVerteilergruppeToUser(
		String verteilergruppe,
		String userId) {
		return new TreeMap();
	}

	/**
	 * 
	 */
	public Map getBundeslandForPLZ(int plz) {
		Map result = new TreeMap();
		result.put("bundesland", "Nordrhein-Westfalen");
		return result;
	}

	/**
	 * Diese Methode sucht nach Addressen zu �bergebener Map
	 */
	public Map searchAddress(Map map) {
		Map result = new TreeMap();
		result.put("id0", new Integer(8023));
		return result;
	}

	/**
	 * Diese Methode stellt zu Methoden aus OctopusMailBatch entsprechende
	 * SQL-Anweisungen zur Verf�gung
	 */
	public Map mailBatch(Map map) {
		return new TreeMap();
	}

	/**
	 * Diese Methode liefert die aktuelle Octopus-Version
	 */
	public Map getVersion() {
		return new TreeMap();
	}

	/**
	* Diese Methode liefert den Ort zu gegebener PLZ
	*/
	public Map getCityForPLZ(int plz) {
		return new TreeMap();
	}
}
