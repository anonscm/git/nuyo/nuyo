package de.tarent.contact.octopus.db;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import de.tarent.contact.bean.TaddressextDB;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.Select;

/**
 * @author kleinw
 * 
 * Hier steht DB-Funktionalit�t bzgl. Postversand.
 *  
 */

public class TcDispatchDB {

    static public List getMailsFromPkList(Map parameter)
            throws InputParameterException, SQLException {
        List mails = new Vector();
        if (! parameter.containsKey("pkList") && (parameter.get("pkList") instanceof String))
            throw new InputParameterException("Der Parameter 'pkList' muss f�r die Methode 'getMailsFromPkList' als |-separierte Stringliste �bergeben werden.");
        
        List tmp = Arrays.asList(((String)parameter.get("pkList")).split("\\|"));

        Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TaddressextDB.getTableName())
            .selectAs(TaddressextDB.PK_PKADDRESSEXT)
            .selectAs(TaddressextDB.EMAIL)
            .where(Expr.in(TaddressextDB.PK_PKADDRESSEXT, tmp));
        
        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
            while (tr.resultSet().next()) {
                mails.add(new StringBuffer(tr.resultSet().getString(TaddressextDB.PK_PKADDRESSEXT))
                          .append("#")
                          .append(tr.resultSet().getString(TaddressextDB.EMAIL))
                          .toString());
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null) tr.close();
        }
        return mails;
    }
}
