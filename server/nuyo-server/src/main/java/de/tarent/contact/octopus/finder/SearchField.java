/* $Id: SearchField.java,v 1.3 2006/09/15 09:55:08 schmitz Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Hendrik Helwich and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.finder;

import de.tarent.contact.octopus.db.TcDBContext;


/**
 * Repr�sentation eines Suchfeldes als Knoten in einem booleschen Ausdruck.
 * 
 * @author hendrik
 */
public class SearchField implements SearchAtom {

    private String name; //name des Suchfeldes
    private String searchString = null; //Suchstring
    private boolean useMatchcode = false;
    //private short operation = -1;
    private byte operation;
    private boolean negated;

    public static final byte EQUAL = 0;
    public static final byte CONTAINS = 1;
    public static final byte LIKE = 2;
    public static final byte IN = 3;

    private static final byte NR_OF_OPERATIONS = 4;

    //Array-Index mu� mit der Nummerierung der Operatoren �bereinstimmen.
    private static final String[][] sql = { { "eq", " =", " <>" }, {
            "ct", " LIKE", " NOT LIKE" }, {
            "lk", " LIKE", " NOT LIKE" }, {
            "in", " IN", " NOT IN" }
    };
    private boolean isList = false;

    public SearchField(String name) {
        setName(name);
        setOperation(EQUAL);
    }

    /**
     * 
     * @see SearchAtom#isOperator()
     */
    public boolean isOperator() {
        return false;
    }

    public SearchAtom getLeft() {
        throw new RuntimeException("ein Suchfeld kann keine Kinder enthalten");
    }

    public SearchAtom getRight() {
        throw new RuntimeException("ein Suchfeld kann keine Kinder enthalten");
    }

    public void addChild(SearchAtom atom) {
        throw new RuntimeException("ein Suchfeld kann keine Kinder enthalten");
    }

    public String getOperation() {
        if (negated)
            return sql[operation][2];
        else
            return sql[operation][1];
    }

    public void setName(String name) {
    	this.name = name.toUpperCase();
        if (this.name.startsWith("MATCHCODE")) {
            useMatchcode = true;
//            this.name = this.name.substring(9);
        }
    }

    public String getName() {
//    	if (useMatchcode)
//            return "MATCHCODE" + name;
        return name;
    }

    public String getSearchString() {
        String str = searchString;
        if (operation == CONTAINS) {
            if (str.charAt(0) != '%')
                str = '%' + str;
            if (str.charAt(str.length() - 1) != '%')
                str += '%';
        }
        if (useMatchcode)
            return getMatchcode(name, searchString);
        else
            return str;
    }

    public void setOperation(byte opcode, boolean negated) {
        this.negated = negated;
        if (opcode < 0 || opcode >= NR_OF_OPERATIONS)
            throw new IllegalArgumentException("unbekannte Operation");
        operation = opcode;
    }

    public void setOperation(byte opcode) {
        setOperation(opcode, false);
    }

    public void setOperation(String opcode) {
        if (opcode.charAt(0) == 'n') {
            negated = true;
            opcode = opcode.substring(1);
        } else
            negated = false;
        for (byte b = 0; b < NR_OF_OPERATIONS; b++)
            if (opcode.equals(sql[b][0])) {
                operation = b;
                return;
            }
        throw new IllegalArgumentException("unbekannte Operation: " + opcode);
    }

    public void setSearchString(String searchString) {
        if (searchString.indexOf('*') != -1) {
            this.searchString = searchString.replace('*', '%');
            if (operation == EQUAL)
                operation = LIKE;
        } else
            this.searchString = searchString;
        if (searchString.charAt(0) == '(')
            isList = true;
    }

    public String toString() {
        StringBuffer sql = new StringBuffer();
        sql.append(TcDBContext.getSchemaName() + "UPPER_FIX(")
           .append(getName())
           .append(") ")
           .append(getOperation());
        if (isList) { //Liste darf nicht in Anf�hrungsstrichen stehen
            sql.append(" ");
            sql.append(getSearchString().toUpperCase());
        } else {
            sql.append(" '");
            sql.append(getSearchString().toUpperCase());
            sql.append("'");
        }
        return sql.toString();
    }

    /**
     * kopiert aus ODBCDatabase <br>
     * 
     * Diese Methode erzeugt zu einem Wert einen Matchcode passend zur �bergebenen
     * Spalte. Erlaubtes Jokerzeichen ist '*' und '%'.<br>
     * Hierbei sollte eigentlich die Stored Procedure buildMatchcode genutzt werden,
     * deren Funktionalit�t hier nachgebildet wird.
     * 
     * @param column die Spalte, zu der passend der Matchcode erzeugt wird.
     * @param value der Wert, zu dem ein Matchcode erzeugt werden soll.
     * @return der erzeugte Matchcode.
     */
    protected static String getMatchcode(String column, String value) {
        if (value == null || value.length() == 0)
            return "";

        //      set @intext = REPLACE( REPLACE( REPLACE( REPLACE( UPPER('' + @intext ), '�' , 'OE'), '�' , 'UE'), '�' , 'AE'), '�' , 'S')
        value = value.toUpperCase().replaceAll("�", "AE").replaceAll("�", "OE").replaceAll("�", "UE").replace('�', 'S');

        StringBuffer buffer = new StringBuffer(value);
        char lastChar = 0;
        int index = 0;
        while (index < buffer.length()) {
            char thisChar = buffer.charAt(index);
            //          if @letter <> @letter_old and ((ascii(@letter) > 64 and ascii(@letter) < 91) or @letter = '*')
            if ((thisChar != lastChar) && ((thisChar == '%') || (thisChar == '*') || ((thisChar >= 'A') && (thisChar <= 'Z')))) {
                lastChar = thisChar;
                index++;
            } else
                buffer.deleteCharAt(index);
        }
        if (column != null && column.length() != 0) {
            //          IF UPPER(@columnname) = 'VORNAMESHORT' set @outtext = LEFT('' + @outtext , 1)
            //          IF UPPER(@columnname) = 'STRASSE' set @outtext = LEFT('' + @outtext , 4)
            //          IF UPPER(@columnname) = 'ORT' set @outtext = LEFT('' + @outtext , 4)
            column = column.toUpperCase();
            int len = buffer.length();
            if ("VORNAMESHORT".equals(column)) {
                if (len > 1)
                    buffer.delete(1, len);
            } else if ("STRASSE".equals(column) || "ORT".equals(column))
                if (len > 4)
                    buffer.delete(4, len);
        }
        return buffer.toString();
    }

}
