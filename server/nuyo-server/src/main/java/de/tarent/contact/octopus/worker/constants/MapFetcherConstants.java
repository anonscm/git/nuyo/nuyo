package de.tarent.contact.octopus.worker.constants;



/**
 * @author kleinw
 *
 *	Hier stehen alle f�r den MapFetcher wichtigen Konstanten.
 *
 */
public interface MapFetcherConstants extends CommonConstants {

    public final static String ACTION_GetAnreden = "getAnreden";
	public final static String ACTION_GetBundeslaender = "getBundeslaender";
	public final static String ACTION_GetGruppenForUser = "getGruppenForUser";
	public final static String ACTION_GetGruppenForUserExt =
		"getGruppenForUserExt";
	public final static String ACTION_GetLKZ = "getLKZ";
	public final static String ACTION_GetLaender = "getLaender";
	public final static String ACTION_GetStandardVerteiler =
		"getStandardVerteiler";
	public final static String ACTION_SetStandardVerteilergruppe =
		"setStandardVerteilergruppe";
	public final static String ACTION_GetAllgemeinenVerteiler =
		"getAllgemeinenVerteiler";
	public final static String ACTION_GetVerteiler = "getVerteiler";
	public final static String ACTION_GetVerteilerExt = "getVerteilerExt";
	public final static String ACTION_GetAddress = "getAddress";
	public final static String ACTION_InsertAddress = "insertAddress";
	public final static String ACTION_UpdateAddress = "updateAddress";
	public final static String ACTION_UpdateSubscriptor = "updateSubscriptor";
	public final static String ACTION_SetBemerkung = "setBemerkung";
	public final static String ACTION_SetVerteiler = "setVerteiler";
	public final static String ACTION_DeleteAddress = "deleteAddress";
	public final static String ACTION_GetDublikate = "getDublikate";
	public final static String ACTION_GetLKZforLand = "getLKZforLand";
	public final static String ACTION_GetLandforLKZ = "getLandforLKZ";
	public final static String ACTION_GetChosenAddresses = "getChosenAddresses";
	public final static String ACTION_GetChosenAddressesVector =
		"getChosenAddressesVector";
	public final static String ACTION_GetAllVerteilergruppen =
		"getAllVerteilergruppen";
	public final static String ACTION_GetUsers = "getUsers";
	public final static String ACTION_CollectOrphan = "collectOrphan";
	public final static String ACTION_CreateVerteilergruppe =
		"createVerteilergruppe";
	public final static String ACTION_DeleteVerteilergruppe =
		"deleteVerteilergruppe";
	public final static String ACTION_CreateVerteiler = "createVerteiler";
	public final static String ACTION_DeleteVerteiler = "deleteVerteiler";
	public final static String ACTION_CreateUser = "createUser";
	public final static String ACTION_ChangeUser = "changeUser";
	public final static String ACTION_DeleteUser = "deleteUser";
	public final static String ACTION_GetUserPwd = "getUserPwd";
	public final static String ACTION_GetUserSpecial = "getUserSpecial";
	public final static String ACTION_GetUserAdminFlag = "getUserAdminFlag";
	public final static String ACTION_GetUserProperties = "getUserProperties";
	public final static String ACTION_GetUserAdmin = "getUserAdmin";
	public final static String ACTION_CreateVersandAuftrag =
		"createVersandAuftrag";
	public final static String ACTION_GetVersandAuftraege = "getVersandAuftraege";
	public final static String ACTION_ShowGruppeToUser =
		"showVerteilergruppeToUser";
	public final static String ACTION_DeleteGruppeFromUser =
		"deleteVerteilergruppeFromUser";
	public final static String ACTION_BundeslandForPLZ = "getBundeslandForPLZ";
	public final static String ACTION_SearchAddress = "searchAddress";
	public final static String ACTION_MailBatch = "mailBatch";
	public final static String ACTION_GetVersion = "getVersion";
	public final static String ACTION_GetCityForPLZ = "getCityForPLZ";
	public final static String ACTION_GetExtendedFields = "getExtendedFields";
	public final static String ACTION_GetSearchAddresses = "getSearchAddresses";
	public final static String ACTION_GetParameter = "getParameter";
	public final static String ACTION_SetParameter = "setParameter";
	public final static String ACTION_GetInitDatas = "getInitDatas";
	public static final String ACTION_GetAddressList = "getAddressList";
	public static final String ACTION_GetAdressMyMap = "getAddressMyMap";
	public static final String ACTION_GetUserParameter = "getUserParameter";
	public static final String ACTION_SetUserParameter = "setUserParameter";
	public static final String ACTION_GetHistory = "getHistory";
	public final static String ACTION_GetHistoryTuned = "getHistoryTuned";
	public final static String ACTION_GetVersandAuftrag = "getVersandAuftrag";
	
	public final static String FIELD_Anreden = "anreden";
	public final static String FIELD_Bundeslaender = "bundeslaender";
	public final static String FIELD_Gruppen = "gruppen";
	public final static String FIELD_LKZ = "lkz";
	public final static String FIELD_Laender = "laender";
	public final static String FIELD_StandardVerteiler = "standardverteiler";
	public final static String FIELD_AllgemeinerVerteiler =
		"allgemeinerverteiler";
	public final static String FIELD_Verteiler = "verteiler";
	public final static String FIELD_Address = "address";
	public final static String FIELD_InsertAddress = "insertaddress";
	public final static String FIELD_DeleteAddress = "deleteaddress";
	public final static String FIELD_GetDublikate = "dublikate";
	public final static String FIELD_GetLKZforLand = "lkzforland";
	public final static String FIELD_GetLandforLKZ = "landforlkz";
	public final static String FIELD_ChosenAddresses = "chosenaddresses";
	public final static String FIELD_GetAllVerteilergruppen =
		"allverteilergruppen";
	public final static String FIELD_Users = "getusers";
	public final static String FIELD_BundeslandForPLZ = "bundeslandforplz";
	public final static String FIELD_SearchAddress = "searchaddress";
	public final static String FIELD_CreateVerteilergruppe =
		"createverteilergruppe";
	public final static String FIELD_DeleteVerteilergruppe =
		"deleteverteilergruppe";
	public final static String FIELD_CreateVerteiler = "createverteiler";
	public final static String FIELD_DeleteVerteiler = "deleteverteiler";
	public final static String FIELD_CreateUser = "createuser";
	public final static String FIELD_ChangeUser = "changeuser";
	public final static String FIELD_DeleteUser = "deleteuser";
	public final static String FIELD_GetUserPwd = "getuserpwd";
	public final static String FIELD_GetUserSpecial = "getuserspecial";
	public final static String FIELD_GetUserAdmin = "getuseradmin";
	public final static String FIELD_GetUserProperties = "getuserproperties";
	public final static String FIELD_CreateVersandAuftrag =
		"createversandauftrag";
	public final static String FIELD_GetVersandAuftraege = "getversandauftraege";
	public final static String FIELD_SetStandardVerteilergruppe =
		"setstandardverteilergruppe";
	public final static String FIELD_ShowGruppeToUser = "showgruppetouser";
	public final static String FIELD_DeleteGruppeFromUser =
		"deletegruppefromuser";
	public final static String FIELD_MailBatch = "mailbatch";
	public final static String FIELD_GetVersion = "getversion";
	public final static String FIELD_GetCityForPLZ = "getcityforplz";
	public final static String FIELD_GetExtendedFields = "getextendedfields";
	public final static String FIELD_GetParameter = "getparameter";
	public final static String FIELD_SetParameter = "setparameter";
	public final static String FIELD_GetInitDatas = "getinitdatas";
	public final static String FIELD_UserParameter = "userparameter";
	public final static String FIELD_History = "history";
	public final static String FIELD_historytype = "type";
	public final static String FIELD_start = "start";
	public final static String FIELD_end = "end";
	public final static String FIELD_adrid = "adrId";
	public final static String FIELD_GetVersandAuftrag = "getversandauftrag";		
	
}
