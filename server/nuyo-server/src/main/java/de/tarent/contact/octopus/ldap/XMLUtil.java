/* $Id: XMLUtil.java,v 1.4 2007/06/15 15:58:30 fkoester Exp $
 * 
 * Created on 09.05.2003 by philipp
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.ldap;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Kleine XML-Utilities f�r den LDAPManager
 * 
 * @author philipp
 */
public class XMLUtil {

	public static Element getParsedDocument( String filename )
	throws SAXParseException, SAXException, IOException {
		//DOMParser parser = new DOMParser();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = null;
        try {
            factory.setFeature("http://apache.org/xml/features/dom/include-ignorable-whitespace", false);		
            parser = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("Fehler bei der Parser konfiguration", e);
        }
        return parser.parse(filename).getDocumentElement();
	}

	public static Node getObjectClass( Element mapping ){
		Node objectclass = null;
		if(mapping.hasChildNodes()){
			NodeList testing = mapping.getChildNodes();
			for(int i = 0; i< testing.getLength(); i++){
				//System.out.println("getObjectClass: "+ testing.item(i).getNodeName());
				if(testing.item(i).getNodeName().equals("objectclass")){
					objectclass = testing.item(i);
					//System.out.println("getObjectClass: found");
				}
			}
		}
		return objectclass;
	}
	
	public static Node getUserList( Element mapping ){
			Node userlist = null;
			if(mapping.hasChildNodes()){
				NodeList testing = mapping.getChildNodes();
				for(int i = 0; i< testing.getLength(); i++){
					//System.out.println("getObjectClass: "+ testing.item(i).getNodeName());
					if(testing.item(i).getNodeName().equals("UserList")){
						userlist = testing.item(i);
						//System.out.println("getObjectClass: found");
					}
				}
			}
			return userlist;
		}
		
		public static NodeList getrelevantChilds( Node parent ){
			NodeList kinder = null;
			Node parent2 = parent.cloneNode(true);
			Node objectclass = getObjectClass((Element) parent2);
			try{
				parent2.removeChild(objectclass);
			}
			catch (Exception e){
				System.out.println(e.getMessage());
			}
			return parent2.getChildNodes();
		}
		
	}

