/*
 * $Id: UserWorker.java,v 1.10 2007/06/15 15:58:29 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2004 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright interest in the program
 * 'tarent-octopus' (which makes passes at compilers) written by Philipp
 * Kirchner. signature of Elmar Geese, 1 June 2002 Elmar Geese, CEO tarent GmbH
 */
package de.tarent.contact.octopus.worker;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.bean.TuserparamDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.ldap.LDAPException;
import de.tarent.octopus.content.TcAll;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.content.TcReflectedWorker;
import de.tarent.octopus.data.TcDataAccessException;
import de.tarent.octopus.server.PersonalConfig;

/**
 * @author kirchner
 */
public class UserWorker extends TcReflectedWorker { 

	/**
	 * @throws TcActionDeclarationException
	 * @throws TcContentProzessException
	 */
	public UserWorker() throws TcActionDeclarationException, TcContentProzessException {
		super();
	}

	final static public String[]	INPUT_USER_AVAILABLE		= { "username"};

	final static public boolean[]	MANDATORY_USER_AVAILABLE	= { true};

	final static public String		OUTPUT_USER_AVAILABLE		= "user_available";

	/**
	 * Testet, ob User in Datenbank vorhanden
	 * 
	 * @param all -
	 *            Das TcAll Objekt
	 * @param loginname -
	 *            Der Loginname, der gestet werden soll
	 * @throws SQLException
	 * @deprecated will be removed soon to cleanup the source
	 */
	final static public Boolean user_available(TcAll all, String loginname) throws SQLException {
		Result tr = null;
		Boolean user_available = new Boolean(false);
		Select select = SQL.Select(TcDBContext.getDefaultContext()).from(TuserDB.getTableName()).select(TuserDB.PK_PKUSER).where(Expr.equal(TuserDB.LOGINNAME, loginname));
		try {
			tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
			if (tr.resultSet().next()) {
				user_available = new Boolean(true);
			}
		} catch (SQLException e) {
			throw (e);
		} finally {
			if (tr != null) tr.close();
		}

		//all.getContentObject().setField("user_available", user_available);
		return user_available;
	}


	final static public String[]	INPUT_UPDATEORCREATEUSER		= { "username"};

	final static public boolean[]	MANDATORY_UPDATEORCREATEUSER	= { true};

	final static public String		OUTPUT_UPDATEORCREATEUSER		= null;

	final static public void updateOrCreateUser(TcAll all, String loginname) throws LDAPException,
			TcDataAccessException, SQLException {
		if(user_available(all, loginname).booleanValue()){
			//Gibt es schon
			updateUser(all, getUserID(all, loginname));
		}else{
			//Gibt es nicht
			insertUser(all);
		}
		//E-Mail Adresse updaten, falls vorhanden
		if(all.personalConfig().getUserEmail()!=null&&all.personalConfig().getUserEmail().length()>0)
			updateOrInsertUserParam(all, getUserID(all, loginname), "email", all.personalConfig().getUserEmail());
	}

	private static void insertUser(TcAll all) throws LDAPException, TcDataAccessException, SQLException {
		PersonalConfig pc = all.personalConfig();
		//User anlegen
		Insert insert = SQL.Insert(TcDBContext.getDefaultContext());
		insert.table(TuserDB.getTableName());
		insert.insert(TuserDB.FIRSTNAME, pc.getUserGivenName());
		insert.insert(TuserDB.LASTNAME, pc.getUserLastName());
		insert.insert(TuserDB.LOGINNAME, pc.getUserLogin());
		List usergroups = Arrays.asList(pc.getUserGroups());
		if(usergroups.contains(PersonalConfig.GROUP_ADMINISTRATOR)){
			insert.insert(TuserDB.ISMANAGER, new Integer(1));
		}
		DB.update(TcDBContext.getDefaultContext(), insert.toString());
	}

	/**
	 * @param userid
	 * @param paramkey
	 * @param paramvalue
	 * @throws SQLException
	 */
	private static void updateOrInsertUserParam(TcAll all, Integer userid, String paramkey, String paramvalue)
			throws SQLException {
		Result tr = null;
		Select checkparam = SQL.Select(TcDBContext.getDefaultContext());
		checkparam.from(TuserparamDB.getTableName());
		checkparam.selectAs(TuserparamDB.PARAMVALUE);
		checkparam.where(Expr.equal(TuserparamDB.FKUSER, userid));
		checkparam.whereAnd(Expr.equal(TuserparamDB.PARAMNAME, "email"));
		try {
			tr = DB.result(TcDBContext.getDefaultContext(), checkparam.toString());
			if (tr.resultSet().next()) {
				String value = tr.resultSet().getString(TuserparamDB.PARAMVALUE);
				if (!value.equals(paramvalue)) {
					updateUserParam(all, userid, paramkey, paramvalue);
				}
			} else {
				insertUserParam(all, userid, paramkey, paramvalue);
			}
		} catch (SQLException e) {
			throw (e);
		} finally {
			if (tr != null) {
				tr.close();
			}
		}

	}

	/**
	 * @param userid
	 * @param paramkey
	 * @param paramvalue
	 * @throws SQLException
	 */
	private static void insertUserParam(TcAll all, Integer userid, String paramkey, String paramvalue)
			throws SQLException {
		Insert insert = SQL.Insert(TcDBContext.getDefaultContext());
		insert.table(TuserparamDB.getTableName());
		insert.insert(TuserparamDB.FKUSER, userid);
		insert.insert(TuserparamDB.PARAMNAME, paramkey);
		insert.insert(TuserparamDB.PARAMVALUE, paramvalue);
		DB.update(TcDBContext.getDefaultContext(), insert.toString());
	}

	/**
	 * @param userid
	 * @param paramkey
	 * @param paramvalue
	 * @throws SQLException
	 */
	private static void updateUserParam(TcAll all, Integer userid, String paramkey, String paramvalue)
			throws SQLException {
		Update updatemail = SQL.Update(TcDBContext.getDefaultContext());
		updatemail.table(TuserparamDB.getTableName());
		updatemail.update(TuserparamDB.PARAMVALUE, paramvalue);
		updatemail.where(Expr.equal(TuserparamDB.PARAMNAME, paramkey));
		updatemail.whereAnd(Expr.equal(TuserparamDB.PK_PK, userid));
		DB.update(TcDBContext.getDefaultContext(), updatemail.toString());
	}

	/**
	 * @param all
	 * @param userid
	 * @param userid
	 * @throws SQLException
	 */
	private static void updateUser(TcAll all, Integer userid) throws SQLException {
		PersonalConfig pc = all.personalConfig();
		Update update = SQL.Update(TcDBContext.getDefaultContext()).table(TuserDB.getTableName()).update(TuserDB.FIRSTNAME, pc.getUserGivenName()).update(
				TuserDB.LASTNAME, pc.getUserLastName());
		List usergroups = Arrays.asList(pc.getUserGroups());
		if(usergroups.contains(PersonalConfig.GROUP_ADMINISTRATOR)){
			update.update(TuserDB.ISMANAGER, new Integer(1));
		}
		update.where(Expr.equal(TuserDB.PK_PKUSER, userid));
		DB.update(TcDBContext.getDefaultContext(), update.toString());
	}

	/**
	 * @param loginname
	 * @return @throws
	 *         SQLException
	 */
	private static Integer getUserID(TcAll all, String loginname) throws SQLException {
		Result tr = null;
		Integer userid = null;
		Select select = SQL.Select(TcDBContext.getDefaultContext());
		select.from(TuserDB.getTableName());
		select.selectAs(TuserDB.PK_PKUSER);
		select.where(Expr.equal(TuserDB.LOGINNAME, loginname));
		try {
			tr = DB.result(TcDBContext.getDefaultContext(), select.toString());
			if (tr.resultSet().next()) {
				userid = new Integer(tr.resultSet().getInt(TuserDB.PK_PKUSER));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			if (tr != null) {
				tr.close();
			}
		}
		return userid;
	}
}