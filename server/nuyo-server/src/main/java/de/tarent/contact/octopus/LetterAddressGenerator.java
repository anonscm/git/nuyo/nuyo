/*
 * Created on 29.08.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.tarent.contact.octopus;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import de.tarent.contact.octopus.worker.constants.LetterAddressConfigConstants;
import de.tarent.groupware.Address;

/**
 * Generator for the letteraddress field
 * @author Nils Neumaier, tarent GmbH
 * 
 */
public class LetterAddressGenerator {
	
	public static final String HOME_COUNTRY = "DEUTSCHLAND";
	
	public static String generateLetterAddress(Address address){
		return generateLetterAddress(
			address.getInstitution(),
			address.getHerrnFrau(),
			address.getAkadTitel(),
			address.getVorname(),
			address.getTitel(),
			address.getNachname(),
			address.getNamenszusatz(),
			address.getPosition(),
			address.getAbteilung(),
			address.getStrasse(),
			address.getHausNr(),
			address.getPlzPostfach(),
			address.getPostfachNr(),
			address.getPlz(),
			address.getOrt(),
			address.getLand());
	}
	
	public static String generateLetterAddress(
        	String institution,
    		String herrfrau,
    		String akad_titel,
    		String vorname,
    		String titel,
    		String nachname,
    		String namenszusatz,
    		String position,
    		String abteilung,
    		String strasse,
    		String hausnummer,
    		String pfplz,
    		String postfach,
    		String plz,
    		String ort,
    		String land
    		){
    	
    	return generateLetterAddress( 	 
    			institution, herrfrau, akad_titel, vorname, titel, nachname, namenszusatz, position, abteilung,	strasse,
    			hausnummer,	pfplz, postfach, plz, ort, land, "");
    	
    }
    
	/**
	 * 
	 * @param institution
	 * @param herrfrau
	 * @param akad_titel
	 * @param vorname
	 * @param titel
	 * @param nachname
	 * @param namenszusatz
	 * @param position
	 * @param abteilung
	 * @param strasse
	 * @param hausnummer
	 * @param pfplz
	 * @param postfach
	 * @param plz
	 * @param ort
	 * @param land
	 * @param letterAddressConfigParams string from cinfig file which inludes keys for fields (space seperated), that should not be included in letteraddress
	 * @return
	 */
    public static String generateLetterAddress(
    	String institution,
		String herrfrau,
		String akad_titel,
		String vorname,
		String titel,
		String nachname,
		String namenszusatz,
		String position,
		String abteilung,
		String strasse,
		String hausnummer,
		String pfplz,
		String postfach,
		String plz,
		String ort,
		String land,
		String letterAddressConfigParams){
    	
    	StringTokenizer strTok = new StringTokenizer(letterAddressConfigParams);
    	List configParams = new ArrayList();
    	while (strTok.hasMoreTokens())
    		configParams.add(strTok.nextToken());
    	
    	StringBuffer z1 = new StringBuffer("");
    	StringBuffer z2 = new StringBuffer("");
    	StringBuffer z3 = new StringBuffer("");
    	StringBuffer z4 = new StringBuffer("");
    	StringBuffer z5 = new StringBuffer("");
    	StringBuffer z6 = new StringBuffer("");
    	StringBuffer z7 = new StringBuffer("");
    	
    	StringBuffer[] zeilen = { z1,z2,z3,z4,z5,z6,z7 };
    	String result = "";
    	int i = 0; // zeilenpointer
    	    	
    	if (herrfrau!= null && herrfrau.equalsIgnoreCase("Herr") && !configParams.contains(LetterAddressConfigConstants.HERRFRAU)){
    			zeilen[i].append(herrfrau);
    			zeilen[i].append("n ");// *** Herr *** aus Herr mache Herrn 
		}
		else if (herrfrau!= null && !herrfrau.equals("") && !configParams.contains(LetterAddressConfigConstants.HERRFRAU)){
			zeilen[i].append(herrfrau); // *** Frau oder andere Bezeichnung (Mr., Mrs. etc) ***
			zeilen[i].append(" ");
		}
		    		
		
		// Akademischer Titel (z.B. Prof. Dr.)
		if (akad_titel!= null && !akad_titel.equals("") && !configParams.contains(LetterAddressConfigConstants.AKADTITEL)){ 
			zeilen[i].append(akad_titel); 
			zeilen[i].append(" ");
		}
		
		//Vorname
		if (vorname != null && !vorname.equals("") && !configParams.contains(LetterAddressConfigConstants.VORNAME)){ 
			zeilen[i].append(vorname);
			zeilen[i].append(" ");
		}
		// Titel (z.B. Freiherr von)
		if (titel != null && !titel.equals("") && !configParams.contains(LetterAddressConfigConstants.TITEL)){ 
			zeilen[i].append(titel);
			zeilen[i].append(" ");
		}
		
		//Nachname
		if (nachname != null && !nachname.equals("") && !configParams.contains(LetterAddressConfigConstants.NACHNAME)){
			
			zeilen[i].append(nachname);
			if (!(namenszusatz != null && namenszusatz.length() > 0 && namenszusatz.charAt(0) == ','))
				zeilen[i].append(" ");
		}
		
		// Namenszusatz
		if (namenszusatz != null && !namenszusatz.equals("") && !configParams.contains(LetterAddressConfigConstants.NAMENSZUSATZ)){
			
			zeilen[i].append(namenszusatz);
			//zeilen[i].append(" ");
		}
		
		
		
	    		
		i++; // n�chste Zeile
		
		// Hier kommt die Position f�r den Landtag Brandenburg hin
		if (position != null && !position.equals("") && !configParams.contains(LetterAddressConfigConstants.POSITION)){
			zeilen[i].append(position);
			i++;
		}
		
		
    	if (institution != null && !institution.equals("") && !configParams.contains(LetterAddressConfigConstants.INSTISTUTION)){
			zeilen[i].append(institution); // Institution
			i++; // n�chste Zeile
    	}		
		
		if (abteilung != null && !abteilung.equals("") && !configParams.contains(LetterAddressConfigConstants.ABTEILUNG)){
			zeilen[i].append(abteilung); // Abteilung
			i++;
		}
    		
		// ************** Postfach oder Anschrift ************
		
		if (strasse != null && !strasse.equals("") && !configParams.contains(LetterAddressConfigConstants.STRASSE) && !(pfplz != null && !pfplz.equals("") && !configParams.contains(LetterAddressConfigConstants.PFPLZ) && (postfach != null && !postfach.equals("") && !configParams.contains(LetterAddressConfigConstants.POSTFACH)))){ // kein Postfach oder keine postfachplz -> Anschrift
			    			
			// Stra�e
			zeilen[i].append(strasse);
			zeilen[i].append(" ");
			if (hausnummer != null && !hausnummer.equals("") && !configParams.contains(LetterAddressConfigConstants.HAUSNUMMER)){ // HausNr.
				zeilen[i].append(hausnummer);				    				
			}    			
		}
		
		else {
			if (postfach != null && !postfach.equals("") && !configParams.contains(LetterAddressConfigConstants.POSTFACH) && pfplz != null && !pfplz.equals("") && !configParams.contains(LetterAddressConfigConstants.PFPLZ)){  // nur Postfach nehmen, wenn auch Postfachplz korrekt
		
				zeilen[i].append("Postfach ");
				zeilen[i].append(postfach);
			}
		}
		
		i++; // n�chste Zeile
		
    	//zeilen[i].append("\n"); // Zeileneinschub zwischen Adresse und Ortangabe
		
		if ((plz != null && !plz.equals("") && !configParams.contains(LetterAddressConfigConstants.PLZ)) && !(postfach != null && !postfach.equals("") && !configParams.contains(LetterAddressConfigConstants.POSTFACH) && pfplz != null && !pfplz.equals("") && !configParams.contains(LetterAddressConfigConstants.PFPLZ))){ // PLZ wenn Postfach und Postfachplz nicht korrekt sind
			zeilen[i].append(plz);
			zeilen[i].append(" ");
		}
		
		if (pfplz != null && !pfplz.equals("") && !configParams.contains(LetterAddressConfigConstants.PFPLZ)){
			if (postfach != null && !postfach.equals("") && !configParams.contains(LetterAddressConfigConstants.POSTFACH)){ // PostfachPLZ verwenden wenn es einen Postfacheintrag UND eine PostfachPLZ gibt
		
				zeilen[i].append(pfplz);
				zeilen[i].append(" ");
			}
		}
		if (ort != null && !ort.equals("") && !configParams.contains(LetterAddressConfigConstants.ORT)){ // Ort			
			zeilen[i].append(ort);
			zeilen[i].append(" ");
		}
		
		i++; // n�chste Zeile
		
		// **************** Land *****************
		   	
		if (land != null && !land.equals("") && !configParams.contains(LetterAddressConfigConstants.LAND) && !land.equalsIgnoreCase(HOME_COUNTRY)){ // Land
			zeilen[i].append(((String)land).toUpperCase()); // Land immer in GROSSBUCHSTABEN !!!
			zeilen[i].append(" ");
		}
		
		//if (adressData.get(AdressKeys.BUNDESLAND) != null && !adressData.get(AdressKeys.BUNDESLAND).equals("")){ // Bundesland
		//	zeilen[i].append(adressData.get(AdressKeys.BUNDESLAND));
		//}
    	
		
		// ***** String konkatenieren *****
		
		for (int j = 0; j < 7; j++){
			
			if (zeilen[j].length() > 0){
				result += zeilen[j].toString();
				result += "\n";
			}
		}
		
    	return result;
    }

	}
