package org.evolvis.nuyo.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.contact.bean.TaddresscategoryDB;
import de.tarent.contact.bean.TaddresssubcategoryDB;
import de.tarent.contact.bean.TcategoryDB;
import de.tarent.contact.bean.TcategoryroleDB;
import de.tarent.contact.bean.TglobalroleDB;
import de.tarent.contact.bean.TgroupCategoryDB;
import de.tarent.contact.bean.TgroupDB;
import de.tarent.contact.bean.TgroupUserDB;
import de.tarent.contact.bean.TsubcategoryDB;
import de.tarent.contact.bean.TuserDB;
import de.tarent.contact.bean.TuserparamDB;
import de.tarent.contact.bean.TuserscheduleDB;
import de.tarent.contact.octopus.db.TcDBContext;
import de.tarent.contact.octopus.worker.BaseAddressWorker.InputParameterException;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.InsertKeys;
import de.tarent.dblayer.engine.Result;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.clause.Expr;
import de.tarent.dblayer.sql.clause.Where;
import de.tarent.dblayer.sql.clause.WhereList;
import de.tarent.dblayer.sql.statement.Delete;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Select;
import de.tarent.dblayer.sql.statement.Update;
import de.tarent.groupware.Category;
import de.tarent.groupware.user.UserManagerDB;
import de.tarent.octopus.content.TcContentProzessException;
import de.tarent.octopus.security.TcSecurityException;
import de.tarent.octopus.server.LoginManager;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.server.UserManager;

public class AdminWorker{
    
    private static final String KEY_EDITUSER = "auth_edituser";
    private static final String KEY_REMOVEUSER = "auth_removeuser";
    private static final String KEY_EDITFOLDER = "auth_editfolder";
    private static final String KEY_REMOVEFOLDER = "auth_removefolder";
    private static final String KEY_EDITSCHEDULE = "auth_editschedule";
    private static final String KEY_REMOVESCHEDULE = "auth_removeschedule";
    private static final String KEY_DELETE = "auth_delete";
    private static final String KEY_ROLENAME = "rolename";
    private static final String KEY_READ = "auth_read";
    private static final String KEY_EDIT = "auth_edit";
    private static final String KEY_ADD = "auth_add";
    private static final String KEY_REMOVE = "auth_remove";
    private static final String KEY_ADDSUB = "auth_addsub";
    private static final String KEY_REMOVESUB = "auth_removesub";
    private static final String KEY_STRUCTURE = "auth_structure";
    private static final String KEY_GRANT = "auth_grant";
    
    private static final Logger logger = Logger.getLogger(AdminWorker.class.getName());
    

    
    /*Definition der Action "deleteVerteilergruppe*/
    public String[] INPUT_deleteCategory = { "key"};
    public boolean[] MANDATORY_deleteCategory = { true};
    public String OUTPUT_deleteCategory = "deleteverteilergruppe";
    /**
     * Lscht eine Kategorie
     * @param oc der Octopus-Context
     * @param key Schlssel der zu lschenden Kategorie
     */
    
    public Map<String, String> deleteCategory(OctopusContext oc, String key) {
        Map<String, String> map = new TreeMap<String, String>();
        try {
            SQL.Delete(TcDBContext.getDefaultContext())
            	.from(TaddresscategoryDB.getTableName())
            	.byId(TaddresscategoryDB.FKCATEGORY, key)
            	.execute();
            SQL.Delete(TcDBContext.getDefaultContext())
            	.from(TaddresssubcategoryDB.getTableName())
            	.byId(TaddresssubcategoryDB.FKCATEGORY, key)
            	.execute();
            SQL.Delete(TcDBContext.getDefaultContext())
            	.from(TcategoryDB.getTableName())
            	.byId(TcategoryDB.PK_PKCATEGORY, key)
            	.execute();
            SQL.Delete(TcDBContext.getDefaultContext())
            	.from(TsubcategoryDB.getTableName())
            	.byId(TsubcategoryDB.FKCATEGORY, key)
            	.execute();
            SQL.Delete(TcDBContext.getDefaultContext())
            	.from(TgroupCategoryDB.getTableName())
            	.byId(TgroupCategoryDB.FKCATEGORY, key)
            	.execute();
            return map;
        } catch (Exception e) {
            logger.log(
                Level.SEVERE,
                "AdminWorker::deleteCategory: Verbindung konnte nicht erstellt werden.",
                e);
            map.put("fehler", e.toString());
            return map;
        }
    }
    
    /* Definition der Action "createVerteiler"*/
    public String[] INPUT_createSubCategory = {"gruppe", "name1", "name2"};
    public String OUTPUT_createSubCategory = "createverteiler";
    /**
     * Erzeugt eine UnterKategorie.
     * @param oc der Octopus-Context
     * @param category FK der Kategorie
     * @param name1 Name der Unterkategorie
     * @param name2 Beschreibung der Unterkategorie
     * @return Map mit Daten der neuen Unterkategorie
     */

    public Map<String, Object> createSubCategory(OctopusContext oc, String category, String name1, String name2) {

        Map<String, Object> map = new TreeMap<String, Object>();
        try {
            
            Select select = SQL.Select(TcDBContext.getDefaultContext())
            					.selectAs("count(*)", "count")
            					.from(TsubcategoryDB.getTableName())
            					.where(Where.and(
            							Expr.equal(TsubcategoryDB.CATEGORYNAME, name1), 
            							Expr.equal(TsubcategoryDB.FKCATEGORY, category)
            							)
            						);
            ResultSet rs = DB.getResultSet(TcDBContext.getDefaultContext(), select);
            if (rs.next()) {
                if (rs.getInt("count") > 0) {
                    map.put("fehler", "Eine Unterkategorie mit dem Namen " + name1 + " ist im dieser Kategorie bereits vorhanden.");
                    return map;
                }
            }
            
            InsertKeys keys = SQL.Insert(TcDBContext.getDefaultContext())
            				.table(TsubcategoryDB.getTableName())
            				.insert(TsubcategoryDB.FKCATEGORY, category)
            				.insert(TsubcategoryDB.CATEGORYNAME, name1)
            				.insert(TsubcategoryDB.DESCRIPTION, name2)
            				.executeInsertKeys(TcDBContext.getDefaultContext());

            map.put("subcategoryId", keys.getPkAsInteger());

            return map;
        } catch (Exception e) {
            logger.log(
                Level.SEVERE,
                "DataAccess::createVerteiler: Verbindung konnte nicht erstellt werden.",
                e);
            map.put(
                "fehler",
                "Bitte andere Kurzbezeichnung vergeben, da diese in der Datenbank bereits vorhanden ist.");
            return map;
        }
    }

    
    /*Definition der Action "deleteSubCategory"*/
    public String[] INPUT_deleteSubCategory = { "verteilergruppe", "key" };
    public boolean[] MANDATORY_deleteSubCategory = { true, true };
    public String OUTPUT_deleteSubCategory = "deleteverteiler";
    /**
     * Lscht eine Unterkategorie
     * @param oc der Octopus-Context
     * @param key Schlssel der Unterkategorie
     * @param verteilergruppe Kategorie, zu der diese Unterkategorie gehrt (hat). TODO: Schlssel oder Name?
     * @return Map TODO: Was fr eine Map?
     */
    
    public Map<String, String> deleteSubCategory(OctopusContext oc, String verteilergruppe, String key) {
        Map<String, String> map = new TreeMap<String, String>();
        try {
        	SQL.Delete(TcDBContext.getDefaultContext())
        			.from(TsubcategoryDB.getTableName())
        			.byId(TsubcategoryDB.PK_PKSUBCATEGORY, key)
        			.execute();
        	SQL.Delete(TcDBContext.getDefaultContext())
        			.from(TaddresssubcategoryDB.getTableName())
        			.byId(TaddresssubcategoryDB.FKSUBCATEGORY, key)
        			.execute();
        } catch (Exception e) {
            logger.log(
                Level.SEVERE,
                "DataAccess::deleteVerteiler: Verbindung konnte nicht erstellt werden.",
                e);
            map.put("fehler", e.toString());
        }
        return map;
    }

    
    final static public String[] INPUT_CREATEORMODIFYCATEGORY = {Category.PROPERTY_ID.getKey(), Category.PROPERTY_CATEGORYNAME.getKey(), Category.PROPERTY_DESCRIPTION.getKey()};
    final static public boolean[] MANDATORY_CREATEORMODIFYCATEGORY = {false, false, false};
    final static public String OUTPUT_CREATEORMODIFYCATEGORY = "id";
    
    /**
     * 
     * @param all
     * @param categoryId
     * @param attributes
     * @return
     * @throws SQLException
     */
    final static public Integer createOrModifyCategory(OctopusContext all, Integer categoryId, String categoryName, String description) throws SQLException {
 
    		Map attributes = new HashMap();
    		attributes.put(Category.PROPERTY_CATEGORYNAME.getKey(), categoryName);
    		attributes.put(Category.PROPERTY_DESCRIPTION.getKey(), description);
    		
        InsertUpdate insertUpdate = SQL.InsertUpdate(TcDBContext.getDefaultContext())
            .table(TcategoryDB.getTableName());
        AttributeSet as = new AttributeSet(attributes, insertUpdate);
        as.add(TcategoryDB.CATEGORYNAME, Category.PROPERTY_CATEGORYNAME.getKey());
        as.add(TcategoryDB.DESCRIPTION, Category.PROPERTY_DESCRIPTION.getKey());
        
        if(categoryId == null) {
        	InsertKeys result = insertUpdate.executeInsertKeys(TcDBContext.getDefaultContext());
        	categoryId = result.getPkAsInteger();
            all.setStatus("INSERT_DONE");
        } else {
        	    insertUpdate.executeUpdate(all.getModuleName(), TcategoryDB.PK_PKCATEGORY, categoryId);
            all.setStatus("UPDATE_DONE");            
        }
        
        return categoryId;
    }
    
   
    
    final static public String[] INPUT_modifySubCategory = {"categoryPk", "subCategoryPk", "subCategoryName",  "subCategoryDescription"};
    final static public boolean[] MANDATORY_modifySubCategory = {true, true, false, false};
    final static public String OUTPUT_modifySubCategory = "";
    /**
     * 
     * @param config
     * @param request
     * @param content
     * @throws SQLException
     * @throws InputParameterException
     */
    public String modifySubCategory(OctopusContext oc, Integer categoryPk, Integer subCategoryPk, String subCategoryName, String subCategoryDescription ) throws SQLException, InputParameterException {
        String result = "";
        if ((subCategoryName == null || subCategoryName.length() == 0) && (subCategoryDescription == null || subCategoryDescription.length() == 0))
            throw new InputParameterException("Fr die Method modifySubCategory muss mindestens der Parameter 'subCategoryName' oder 'subCategoryDescription bergeben werden. ");

       
        Select exists = 
            SQL.Select(TcDBContext.getDefaultContext())
                .from(TsubcategoryDB.getTableName())
                .selectAs("count(*)", "count")
                .where(Expr.equal(TsubcategoryDB.CATEGORYNAME, subCategoryName))
                .whereAnd(Expr.equal(TsubcategoryDB.FKCATEGORY, categoryPk));
        Result tr = null;
        try {
            tr = DB.result(TcDBContext.getDefaultContext(), exists);
            if (tr.resultSet().next()) {
                //Rausgenommen, da immer Exception geschmissen wird, wenn man den Namen der UK gleich lsst - philipp
                //if (tr.resultSet().getInt("count") > 1) {
                //    result = "Fr diese Kategorie gibt es bereits eine Unterkategorie mit dem Namen.";
                //} else {
                    Update update =
                        SQL.Update(TcDBContext.getDefaultContext())
                            .table(TsubcategoryDB.getTableName());
                    if (subCategoryName != null && subCategoryName.length() > 0)
                        update.update(TsubcategoryDB.CATEGORYNAME, subCategoryName);
                    if (subCategoryDescription != null && subCategoryDescription.length() > 0)
                        update.update(TsubcategoryDB.DESCRIPTION, subCategoryDescription);

                    update.where(Expr.equal(TsubcategoryDB.PK_PKSUBCATEGORY, subCategoryPk))
                    .whereAnd(Expr.equal(TsubcategoryDB.FKCATEGORY, categoryPk));
                    DB.update(TcDBContext.getDefaultContext(), update);
                //}
            }
        } catch (SQLException e) {
            throw (e);
        } finally {
            if (tr != null)
                tr.close();
        }
        return result;
    }
    
   
  
    /************************** Rollen Tasks **********************************/
  
    
    public String[] INPUT_createOrModifyObjectRole = { "id", "attributes" };
    public boolean[] MANDATORY_createOrModifyObjectRole = { false, true };
    public String OUTPUT_createOrModifyObjectRole = "id";
    
    public Integer createOrModifyObjectRole(OctopusContext oc, Integer id, Map attributes) throws SQLException{
        InsertUpdate statement = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TcategoryroleDB.getTableName());
        //Postgres table tfolderrole: definition of attribute fields has changed from boolean to integer
        Iterator it = attributes.keySet().iterator();
        while(it.hasNext()){
            Object key = it.next();
            Object value = attributes.get(key);
            if(value instanceof Boolean){
            	attributes.put(key, ((Boolean) value?1:0));
            }
        }
      AttributeSet as = new AttributeSet(attributes, statement);
        as.add(TcategoryroleDB.ROLENAME, KEY_ROLENAME);
        as.add(TcategoryroleDB.AUTHADD, KEY_ADD);
        as.add(TcategoryroleDB.AUTHADDSUB, KEY_ADDSUB);
        as.add(TcategoryroleDB.AUTHEDIT, KEY_EDIT);
        as.add(TcategoryroleDB.AUTHGRANT, KEY_GRANT);
        as.add(TcategoryroleDB.AUTHREAD, KEY_READ);
        as.add(TcategoryroleDB.AUTHREMOVE, KEY_REMOVE);
        as.add(TcategoryroleDB.AUTHSTRUCTURE, KEY_STRUCTURE);
        as.add(TcategoryroleDB.AUTHREMOVESUB, KEY_REMOVESUB);
        if (id == null) {
            InsertKeys result = statement.executeInsertKeys(TcDBContext.getDefaultContext());
            id = result.getPkAsInteger();
        }else{
            //System.out.println(statement.update().statementToString());
            statement.executeUpdate(oc.getModuleName(), TcategoryroleDB.PK_PK, id);
        }
        return id;
    }
    

    public String[] INPUT_deleteObjectRole = { "id" };
    public boolean[] MANDATORY_deleteObjectRole = { true };
    public String OUTPUT_deleteObjectRole = "error";
    
    public String deleteObjectRole(OctopusContext oc, Integer id) throws SQLException, Exception{
        List referringGroups = SQL.Select(TcDBContext.getDefaultContext())
            .from(TgroupCategoryDB.getTableName())
            .add(TgroupCategoryDB.PK_PK, Integer.class)
            .where(Expr.equal(TgroupCategoryDB.FKCATEGORYROLE, id))
            .getList(TcDBContext.getDefaultContext());
        if (referringGroups.size() > 0){
        	logger.log(Level.INFO, "Es wurde versucht eine kategoriebezogene Rolle zu l\u00F6schen, die noch verwendet wird. Der L\u00F6schvorgang wurde abgebrochen.");
        	return "Die Rolle kann nicht gel\u00F6scht werden, da sie in Benutzung ist.";
        }
        SQL.Delete(TcDBContext.getDefaultContext()).from(TcategoryroleDB.getTableName()).where(Expr.equal(TcategoryroleDB.PK_PK, id)).execute();
        return null;
    }
    
    public String[] INPUT_createOrModifyGlobalRole = { "id", "attributes" };
    public boolean[] MANDATORY_createOrModifyGlobalRole = { false, true };
    public String OUTPUT_createOrModifyGlobalRole = "id";
    
    public Integer createOrModifyGlobalRole(OctopusContext oc, Integer id, Map<Object, Comparable> attributes) throws SQLException{
        InsertUpdate statement = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TglobalroleDB.getTableName());
        //Konvertiere die dreckigen String aus der Attribute-Map nach Boolean... Das muss eigentlich im Client im AbstractEntity gefixt werden!!!!
        Iterator it = attributes.keySet().iterator();
        while(it.hasNext()){
            Object key = it.next();
            Object value = attributes.get(key);
            if(value instanceof String){
                //Teste, ob das ein Boolean ist...
                Boolean test = Boolean.valueOf((String)value);
                if(test.toString().equals(value)){
                	//Postgres table tglobalrole: definition of attribute fields has changed from boolean to integer
                    attributes.put(key, (test?1:0));
                }
            }
            if (value instanceof Boolean)
            {
            	//Postgres table tglobalrole: definition of attribute fields has changed from boolean to integer
              attributes.put(key, ((Boolean) value?1:0));
            }
        }
        AttributeSet as = new AttributeSet(attributes, statement);
        as.add(TglobalroleDB.ROLENAME, KEY_ROLENAME);
        as.add(TglobalroleDB.AUTHDELETE, KEY_DELETE);
        as.add(TglobalroleDB.AUTHEDITCATEGORY, KEY_EDITFOLDER);
        as.add(TglobalroleDB.AUTHEDITSCHEDULE, KEY_EDITSCHEDULE);
        as.add(TglobalroleDB.AUTHEDITUSER, KEY_EDITUSER);
        as.add(TglobalroleDB.AUTHREMOVECATEGORY, KEY_REMOVEFOLDER);
        as.add(TglobalroleDB.AUTHREMOVESCHEDULE, KEY_REMOVESCHEDULE);
        as.add(TglobalroleDB.AUTHREMOVEUSER, KEY_REMOVEUSER);
        if (id == null) {
            InsertKeys result = statement.insert(TcDBContext.getDefaultContext()).executeInsertKeys(TcDBContext.getDefaultContext());
            id = result.getPkAsInteger();
        }else{
            statement.executeUpdate(oc.getModuleName(), TglobalroleDB.PK_PK, id);
        }
        return id;
    }
    

    public String[] INPUT_deleteGlobalRole = { "id" };
    public boolean[] MANDATORY_deleteGlobalRole = { true };
    public String OUPUT_deleteGlobalRole = null;
    
    public void deleteGlobalRole(OctopusContext oc, Integer id) throws SQLException, Exception {
        List referringGroups = SQL.Select(TcDBContext.getDefaultContext())
            .from(TgroupDB.getTableName())
            .add(TgroupDB.GROUPNAME, String.class)
            .where(Expr.equal(TgroupDB.FKGLOBALROLE, id))
            .getList(TcDBContext.getDefaultContext());
        if (referringGroups.size() > 0){
            String errorMessage = "Die Rolle kann nicht gelscht werden, da sie in Benutzung ist.\n\n" +
                    "Folgende Gruppen benutzen diese Rolle: \n\n";
            for (int i = 0; i < referringGroups.size(); i++ ){
                errorMessage += "- " + referringGroups.get(i) + "\n";
            }
            throw new Exception(errorMessage);
        }
        
        SQL.Delete(TcDBContext.getDefaultContext()).from(TglobalroleDB.getTableName()).where(Expr.equal(TglobalroleDB.PK_PK, id)).executeDelete(TcDBContext.getDefaultContext());
    }

    
    /*Definition der Action "deleteCategoryRight"*/
    public String[] INPUT_deleteCategoryRight = { "id" };
    public boolean[] MANDATORY_deleteCategoryRight = { true };
    public String OUTPUT_deleteCategoryRight = null;
    /**
     * Lscht ein Rechte-Tupel (Kategorie, Gruppe, Rolle).
     * @param oc
     * @throws SQLException
     */
    public void deleteCategoryRight(OctopusContext oc, Integer id) throws SQLException{
    
        Delete delete = SQL.Delete(TcDBContext.getDefaultContext()).from(TgroupCategoryDB.getTableName()).where(Expr.equal(TgroupCategoryDB.PK_PK, id));
        delete.execute();
    }
    
    /*Definition der Action "createOrModifyCategoryRight"*/
    public String[] INPUT_createOrModifyCategoryRight = { "categoryRightID", "attributes" };
    public boolean[] MANDATORY_createOrModifyCategoryRight = { false, true };
    public String OUTPUT_createOrModifyCategoryRight = "categoryRightID";
    
    public Integer createOrModifyCategoryRight(OctopusContext oc, Integer id, Map attributes) throws SQLException{
        InsertUpdate stmnt = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TgroupCategoryDB.getTableName());
        AttributeSet as = new AttributeSet(attributes, stmnt);
        as.add(TgroupCategoryDB.FKCATEGORY, "categoryID");
        as.add(TgroupCategoryDB.FKCATEGORYROLE, "roleID");
        as.add(TgroupCategoryDB.FKGROUP, "groupID");
        if(id==null){
            InsertKeys result = stmnt.executeInsertKeys(TcDBContext.getDefaultContext());
            id = result.getPkAsInteger();
        }else{
            stmnt.executeUpdate(oc.getModuleName(), TgroupCategoryDB.PK_PK, id);
        }
        return id;
    }

    /***************************** User Tasks **************************************/
    
    
    public final static String KEY_LOGIN_NAME = "login";
    public final static String KEY_FIRST_NAME = "firstname";
    public final static String KEY_LAST_NAME = "lastname";
    public final static String KEY_PASSWORD = "password";
    public final static String KEY_ADMIN = "adminflag";
    
    public static void testParamExists(String value, String paramName) 
    throws TcContentProzessException{
    if (value == null)
        throw new TcContentProzessException("Der Parameter "+paramName+" muss vorhanden sein.");
    if ("".equals(value))
        throw new TcContentProzessException("Der Parameter "+paramName+" darf nicht leer sein.");
    }
    
    final static public String[] INPUT_CREATEORMODIFYUSER = {"id", "calendarstoadd", "calendarstoremove", "groupstoadd", "groupstoremove", "attributes"};
    final static public boolean[] MANDATORY_CREATEORMODIFYUSER = {false, false, false, false, false, false};
    final static public String OUTPUT_CREATEORMODIFYUSER = "id";  
    
    final static public Integer createOrModifyUser(OctopusContext all, Integer id, List calendarsToAdd, List calendarsToRemove, List groupsToAdd, List groupsToRemove, Map attributes) 
        throws SQLException, TcContentProzessException, TcSecurityException {

        String loginname = (String) attributes.get(KEY_LOGIN_NAME);
        testParamExists(loginname, KEY_LOGIN_NAME);

        String vorname = (String) attributes.get(KEY_FIRST_NAME);
        testParamExists(vorname, KEY_FIRST_NAME);

        String nachname = (String) attributes.get(KEY_LAST_NAME);
        testParamExists(nachname, KEY_LAST_NAME);

        String passwort = (String) attributes.get(KEY_PASSWORD);
        //passwort ist nur bei neuem User Pflichtfeld
        if (id == null)
            testParamExists(passwort, KEY_PASSWORD);
        

        // Je nach verwendetem UserManager im Octopus, wird der Eintrag in
        // der TC Datenbank bereits vom UserManager des Moduls erledigt.
        // Wenn dies nicht der Fall ist, wie dies noch mit dem UserManagerDB
        // noch nachgezogen.
        LoginManager lm = all.commonConfig().getLoginManager(all.getModuleName());
        UserManager um = null;
        if (lm.isUserManagementSupported())
            um = lm.getUserManager();
        
        UserManagerDB dbum = null;
        boolean umCreatesTCUserData = false;
        if (um instanceof UserManagerDB) {
            umCreatesTCUserData = true;
            dbum = (UserManagerDB)um;
        } else
            dbum = new UserManagerDB();

        if (um == null) {
            um = dbum;
            umCreatesTCUserData = true;
        }
        dbum.setModuleName(all.getModuleName());

        try {
            // create User
            if (id == null) {            
                um.addUser(loginname, vorname, nachname, passwort);
                if (!umCreatesTCUserData)
                    dbum.addUser(loginname, vorname, nachname, passwort);
                id = dbum.getLastInsertedId();
            }

            // modify user
            else {
                um.modifyUser(loginname, vorname, nachname, passwort);
                if (!umCreatesTCUserData)
                    dbum.modifyUser(loginname, vorname, nachname, passwort);
            }
        } catch (TcSecurityException e) {
            throw new TcContentProzessException(e);
        }
        
        if (calendarsToAdd != null) {
            for (Iterator it = calendarsToAdd.iterator();it.hasNext();) {
                SQL.Insert(TcDBContext.getDefaultContext())
                    .table(TuserscheduleDB.getTableName())
                    .insert(TuserscheduleDB.FKUSER, id)
                    .insert(TuserscheduleDB.FKSCHEDULE, it.next())
                    .insert(TuserscheduleDB.CREATED, new Date())
                    .insert(TuserscheduleDB.CREATEDBY, all.requestAsString("username"))
                    .execute();
            }
        }
        
        if (calendarsToRemove != null) {
            for (Iterator it = calendarsToRemove.iterator();it.hasNext();) {
                SQL.Delete(TcDBContext.getDefaultContext())
                    .from(TuserscheduleDB.getTableName())
                    .where(
                        Where.and(
                                Expr.equal(TuserscheduleDB.FKSCHEDULE, it.next()),
                                Expr.equal(TuserscheduleDB.FKUSER, id)
                        )
                    )
                    .execute();
            }
        }
            
        if (groupsToAdd != null) {
            for (Iterator it = groupsToAdd.iterator();it.hasNext();) {
                SQL.Insert(TcDBContext.getDefaultContext())
                .table(TgroupUserDB.getTableName())
                .insert(TgroupUserDB.FKUSER, id)
                .insert(TgroupUserDB.FKGROUP, it.next())
                //FIXME: Darf das auskommentiert bleiben? ggf. ganz entfernen...
//              .insert(TusergroupDB.CREATED, new Date())
//              .insert(TusergroupDB.CREATEDBY, all.requestAsString("username"))
                .execute();
            }
        }
        
        if (groupsToRemove != null) {
            for (Iterator it = groupsToRemove.iterator();it.hasNext();) {
                SQL.Delete(TcDBContext.getDefaultContext())
                    .from(TgroupUserDB.getTableName())
                    .where(
                        Where.and(
                                Expr.equal(TgroupUserDB.FKGROUP, it.next()),
                                Expr.equal(TgroupUserDB.FKUSER, id)
                        )
                    )
                    .execute();
            }
        }
        return id;
    }
    
    final static public String[] INPUT_DELETEUSER = {KEY_LOGIN_NAME};
    final static public boolean[] MANDATORY_DELETEUSER = {true};
    final static public String OUTPUT_DELETEUSER = KEY_LOGIN_NAME;
    
    final static public String deleteUser(OctopusContext all, String loginname) 
        throws TcContentProzessException {
        try {

            LoginManager lm = all.commonConfig().getLoginManager(all.getModuleName());
            if (lm.isUserManagementSupported()) {                
                UserManager um = lm.getUserManager();
                if (um instanceof UserManagerDB)
                    ((UserManagerDB)um).setModuleName(all.getModuleName());
                um.deleteUser(loginname);
            }

            // Falls der eingestellte UserManager die Daten in der TC DB 
            // nicht mit pflegt wird hier einfach nochmal gelscht.
            // Dazu darf natrlich kein Fehler angezeigt werden, 
            // wenn der User nicht existiert.
            UserManagerDB dbum = new UserManagerDB();
            dbum.setModuleName(all.getModuleName());
            dbum.deleteUser(loginname);
            return loginname;

        } catch (TcSecurityException e) {
            throw new TcContentProzessException(e);
        }                
    }
    
    /*Definition der Action "setUserParameter"*/
    public String[] INPUT_setUserParameter = { "userId", "key", "value" };
    public boolean[] MANDATORY_setUserParameter = { true, true, true };
    public String OUTPUT_setUserParameter = "userparameter";
    /**
     * Setzt einen Parameter eines Benutzers
     * @param oc OctopusContext
     * @param userId ID des Users
     * @param key Schlssel des Parameters
     * @param value Wert des Parameters
     * @return String
     */
    public String setUserParameter(OctopusContext oc, String userId, String key, String value){
        String result = "true";
        //FIXME: ist die userID hier eine ID oder der Loginname??
        String UserID = getUserId(userId)!=null ? getUserId(userId).toString() : userId;

        try {
	        if(getUserParameter(userId, key, UserID)!=null) {
	            //Vorhanden, update
	        	SQL.Update(TcDBContext.getDefaultContext())
	        		.table(TuserparamDB.getTableName())
	        		.update(TuserparamDB.PARAMVALUE, value)
	        		.where(Where.and(
	        				Expr.equal(TuserparamDB.PARAMNAME, key),
	        				Expr.equal(TuserparamDB.FKUSER, UserID)))
	        		.execute();
	        }else {
	            //nicht vorhanden, einfgen
	        	SQL.Insert(TcDBContext.getDefaultContext())
	        		.table(TuserparamDB.getTableName())
	        		.insert(TuserparamDB.FKUSER, UserID)
	        		.insert(TuserparamDB.PARAMNAME, key)
	        		.insert(TuserparamDB.PARAMVALUE, value)
	        		.execute();
	        }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Fehler bei der Datenbankabfrage! ", e);
            result = "false";
        }
        return result;
    }

    /**
     * Holt einen UserParameter aus der Datenbank
     * @see de.tarent.contact.octopus.DataAccess#getUserParameter(java.lang.String, java.lang.String)
     */
    private String getUserParameter(String userName, String key, String userId) {
        String result=null;
        Result rs = null;
        try {
            rs = SQL.Select(TcDBContext.getDefaultContext())
            	.from(TuserparamDB.getTableName())
            	.selectAs(TuserparamDB.PARAMVALUE)
            	.where(Where.and(
            			Expr.equal(TuserparamDB.FKUSER, userId),
            			Expr.equal(TuserparamDB.PARAMNAME, key)))
            	.executeSelect(TcDBContext.getDefaultContext());
            if(rs.resultSet().next()) {
                result = rs.resultSet().getString(TuserparamDB.PARAMVALUE);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Fehler bei der Datenbankabfrage! ", e);
            result = null;
        }finally{
        	if(rs!=null){
        		rs.close();
        	}
        }
        return result;
    }
    
    /**
     * Gibt die User-ID eines Benutzer zurck oder
     * null wenn Benutzer nicht gefunden wurde.
     *
     * @param user
     */
    public Integer getUserId(String user) {
    	Integer userId = null;
    	Result rs = null;
        try {
            if (user != null && user.length() != 0) {
                rs = SQL.Select(TcDBContext.getDefaultContext())
                	.from(TuserDB.getTableName())
                	.selectAs(TuserDB.PK_PKUSER)
                	.where(Expr.in(TuserDB.LOGINNAME, user))
                	.executeSelect(TcDBContext.getDefaultContext());
                while (rs.resultSet().next()) {
                    userId = rs.resultSet().getInt(TuserDB.PK_PKUSER);
                    break;
                }
            }
            return userId;
        } catch (SQLException e) {
            logger.log(
                Level.SEVERE,
                "DataAccess::getUserId: Verbindung konnte nicht erstellt werden.",
                e);
            userId = null;
        }finally{
        	if(rs!=null){
        		rs.close();
        	}
        }
        return userId;
    }

    /***************************** Usergroup Tasks **************************************/
    
    
    public final static String KEY_NAME = "name";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_GLOBALROLE = "globalrole";
    public final static String KEY_SHORTNAME = "shortname";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    
    public String[] INPUT_CREATEORMODIFYUSERGROUP       = {"id", "attributes", "adduser", "removeuser" };
    public boolean[]    MANDATORY_CREATEORMODIFYUSERGROUP   = {false, false, false, false };
    public String       OUTPUT_CREATEORMODIFYUSERGROUP      = "id";

    public Integer createOrModifyUserGroup(OctopusContext oc, Integer userGroupId, Map<String, String> attributes, List addUser, List removeUser)
            throws SQLException {

    	
    		org.apache.log4j.Logger.getLogger("SQL").debug("aufruf : " + addUser);
            
           
            // INSERT NEW GROUP
            if (userGroupId==null) {
            	
            		InsertUpdate insert = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TgroupDB.getTableName());
                	attributes.put(KEY_SHORTNAME, "emptyShortName");
                	
                	AttributeSet insertAS = new AttributeSet(attributes, insert);
                	insertAS.add(TgroupDB.GROUPNAME, KEY_NAME);
                	insertAS.add(TgroupDB.DESCRIPTION, KEY_DESCRIPTION);
                	insertAS.add(TgroupDB.SHORTNAME, KEY_SHORTNAME);
                	insertAS.add(TgroupDB.FKGLOBALROLE, KEY_GLOBALROLE);
        			
                InsertKeys result = insert
                    .add(TgroupDB.CREATED, new Date())
                    .add(TgroupDB.CREATEDBY, oc.getConfigObject().getPersonalConfig().getUserLogin())
                    .executeInsertKeys(TcDBContext.getDefaultContext());
                userGroupId = result.getPkAsInteger();
                // overwrite shortkey because it must contain the pk of the new group
            } 
            
            String shortkey;   
            String key = (String)attributes.get(KEY_NAME);
            shortkey   = key.substring(0,Math.min(5, key.length())) + userGroupId;
            attributes.put(KEY_SHORTNAME, shortkey);

            // UPDATE EXISTING GROUP
           	
            		InsertUpdate update = SQL.InsertUpdate(TcDBContext.getDefaultContext()).table(TgroupDB.getTableName());
            
            	
            AttributeSet editAS = new AttributeSet(attributes, update);
            editAS.add(TgroupDB.GROUPNAME, KEY_NAME);
            editAS.add(TgroupDB.DESCRIPTION, KEY_DESCRIPTION);
            editAS.add(TgroupDB.SHORTNAME,KEY_SHORTNAME);
            editAS.add(TgroupDB.FKGLOBALROLE, KEY_GLOBALROLE);
            
            update
            		.add(TgroupDB.CHANGED, new Date())
            		.add(TgroupDB.CHANGEDBY, oc.requestAsString("usermame"))
            		.executeUpdate(TcDBContext.getDefaultContext(), TgroupDB.PK_PK, userGroupId);
        
            handleUserChange(oc, userGroupId, addUser, removeUser);
            
            return userGroupId;
    }

    private static void handleUserChange(OctopusContext oc, Integer groupId, List addUser, List removeUser)
            throws SQLException {
        if (removeUser != null) {
            for (Iterator it = removeUser.iterator();it.hasNext();) {
                Integer user = (Integer) it.next();
                WhereList wl = SQL.WhereList(TcDBContext.getDefaultContext());
                wl.addAnd(Expr.equal(TgroupUserDB.FKUSER, user));
                wl.addAnd(Expr.equal(TgroupUserDB.FKGROUP, groupId));
                SQL.Delete(TcDBContext.getDefaultContext())
                    .from(TgroupUserDB.getTableName())
                    .where(wl)
                    .executeDelete(TcDBContext.getDefaultContext());
            }
        }
        if (addUser != null) {
            for(Iterator it = addUser.iterator();it.hasNext();) { 
                Integer user = (Integer) it.next();
                SQL.Insert(TcDBContext.getDefaultContext())
                    .table(TgroupUserDB.getTableName())
                    .insert(TgroupUserDB.FKGROUP, groupId)
                    .insert(TgroupUserDB.FKUSER, user)
                    .executeInsert(TcDBContext.getDefaultContext());
            }
        }
    }
    
    
    public String[] INPUT_deleteUserGroup = { "usergroupid" };
    public boolean[] MANDATORY_deleteUserGroup = { true };
    public String OUTPUT_deleteUserGroup = null;
    /**
     * Lscht eine Benutzergruppe
     * @param oc
     * @param groupID
     * @throws SQLException 
     */
    public void deleteUserGroup(OctopusContext oc, Integer groupID) throws SQLException{
        //Erstmal alle Relationen aus Tgroup_user lschen...
        SQL.Delete(TcDBContext.getDefaultContext()).from(TgroupUserDB.getTableName()).where(Expr.equal(TgroupUserDB.FKGROUP, groupID)).execute();
        //Dann die Gruppe selber lschen
        SQL.Delete(TcDBContext.getDefaultContext()).from(TgroupDB.getTableName()).where(Expr.equal(TgroupDB.PK_PK, groupID)).execute();
    }
}
