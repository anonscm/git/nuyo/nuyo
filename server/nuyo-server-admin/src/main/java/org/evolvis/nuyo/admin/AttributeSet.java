package org.evolvis.nuyo.admin;

import de.tarent.dblayer.sql.Statement;
import de.tarent.dblayer.sql.statement.Insert;
import de.tarent.dblayer.sql.statement.InsertUpdate;
import de.tarent.dblayer.sql.statement.Update;

import java.util.Map;


/**
 * @author kleinw
 *
 */
public class AttributeSet {
    
    private Map _attributes;
    private Statement _statement;
    
    public AttributeSet(Map attributes, Statement statement) {
        _attributes = attributes;
        _statement = statement;
    }
    
    public void add(String column, String key) {
        if(_attributes != null) {
            if (_attributes.containsKey(key)) {
                if (_statement instanceof InsertUpdate) {
                    ((InsertUpdate)_statement).add(column, _attributes.get(key));
                } else if (_statement instanceof Insert) {
                    ((Insert)_statement).insert(column, _attributes.get(key));
                } else if (_statement instanceof Update) {
                    ((Update)_statement).update(column, _attributes.get(key));
                }
            }
        }
    }
    
}
