/*
 * Created on 16.09.2004
 *
 */
package org.evolvis.nuyo.logging;


/**
 * @author niko
 *
 */
public interface LogWriter
{
  public boolean writeLog(String text);
}
