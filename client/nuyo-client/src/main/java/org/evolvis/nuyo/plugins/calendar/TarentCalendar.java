package org.evolvis.nuyo.plugins.calendar;

import javax.swing.JDialog;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.gui.ContainerProvider;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;



/**
 * An interface to use the calendar functionality.<p>
 * The implementation of this interface you can get from {@link CalendarPlugin}.<p>
 * 
 * @see CalendarPlugin#getImplementationFor(Class)
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public interface TarentCalendar extends ContainerProvider {
    
    public void createNewTask();
    public void createNewAppointment();
    public void exportAppointmentsAndTasks();
    public JDialog getCalendarSelectionDialog();
    
    //TODO: change access: this methods only inside plugin package 
    public void addAppointmentDisplayListener(CalendarVisibleElement display);
    public void removeAppointmentDisplayListener(CalendarVisibleElement display);
    /** Returns null if nothing found. */
    public Appointment askForAppointment(Integer appointmentid);
    public void fireChangedAppointment(Appointment appointment, CalendarVisibleElement exclude);
    public void fireAddedAppointment(Appointment appointment, CalendarVisibleElement exclude);
    /** This method is thread safe! */
    public void fireRemovedAppointment(Appointment appointment, CalendarVisibleElement exclude);
    public void fireSelectedAppointment(Appointment appointment, CalendarVisibleElement exclude);
    public void fireChangeCalendar(CalendarVisibleElement exclude);
    public void fireSetViewType(String viewtype, CalendarVisibleElement exclude);
    public void fireSetVisibleInterval(ScheduleDate start, ScheduleDate end, CalendarVisibleElement exclude);
    public void fireSetGridActive(boolean usegrid, CalendarVisibleElement exclude);
    public void fireSetGridHeight(int seconds, CalendarVisibleElement exclude);
    public void selectCalendar( Integer calendarID );
    public void changeCalendar();
}
