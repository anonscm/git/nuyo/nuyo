package org.evolvis.nuyo.db.persistence;

import java.util.Collection;

/**
 * @author kleinw
 *
 *	Dieses Event beschreibt den Vorgang, dass einer Entit�t 
 *	eine odere mehre Entit�ten zugeordnet wurden.
 *
 */
public class EntityRelationEvent {

    /**	Die betreffende Entit�t */
    private IEntity _entity;
    /**	 Die gel�schten oder hinzugef�gten Entit�ten */
    private Collection _entities;
    
    
    /**	Konstruktor */
    public EntityRelationEvent(IEntity entity, Collection entities) {
        _entity = entity;
        _entities = entities;
    }
    
    
    /**	Entity holen */
    public IEntity getSource() {return _entity;}
    
    
    /** Entities holen */
    public Collection getEntities() {return _entities;}
    
}
