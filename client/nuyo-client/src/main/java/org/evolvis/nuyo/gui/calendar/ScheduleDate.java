/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author niko
 *
 */
public class ScheduleDate extends GregorianCalendar
{
  protected final static DateFormat m_oDateFormatter = SimpleDateFormat.getDateInstance();
  protected final static DateFormat m_oDateTimeFormatter = SimpleDateFormat.getDateTimeInstance();

  protected final static DateFormat m_oMonthFormatter = new SimpleDateFormat("MMMMM");
  protected final static DateFormat m_oYearFormatter = new SimpleDateFormat("yyyy");
  protected final static DateFormat m_oShortDateFormatter = new SimpleDateFormat("d.M.");
  
  
  
  public ScheduleDate()
  {
    setDate(new Date());
  }
  
  public ScheduleDate(ScheduleDate date)
  {
    setDate(date.getDate());
  }
  
  public ScheduleDate(Date date)
  {
    setDate(date);
  }
  
  public ScheduleDate(int day, int month, int year)
  {
    this.set(Calendar.MONTH, month - 1);
    this.set(Calendar.YEAR, year);
    this.set(Calendar.DAY_OF_MONTH, day);
  }
  
  
  public void setDate(Date date)
  {
    this.setTime(date);
  }

  public Date getDate()
  {
    //return (Date)(this.getTime().clone());
    return this.getTime();
  }

  public String getDateString()
  {
    return m_oDateFormatter.format(getDate());
  }
  
  public String getShortDateString()
  {
    return m_oShortDateFormatter.format(getDate());
  }
  
  public String getDateTimeString()
  {
    return m_oDateTimeFormatter.format(getDate());
  }
  
  public String getMonthString()
  {
    return m_oMonthFormatter.format(getDate());
  }
  
  public String getYearString()
  {
    return m_oYearFormatter.format(getDate());
  }
  
  public int getScheduleYear()
  {
    return this.get(Calendar.YEAR);
  }

  public int getScheduleMonth()
  {
    return this.get(Calendar.MONTH);
  }

  public int getScheduleDay()
  {
    return this.get(Calendar.DATE);
  }
  
  public int getScheduleDayOfWeek()
  {
  	return this.get(Calendar.DAY_OF_WEEK);
  }

  public int getScheduleDayOfYear()
  {
    return this.get(Calendar.DAY_OF_YEAR);
  }
  
  public int getScheduleSecondOfDay()
  {
    long millis = this.getTimeInMillis();
    
    Calendar fcal = new GregorianCalendar();
    fcal.setTime(this.getTime());
    fcal.set(Calendar.HOUR_OF_DAY, 0);
    fcal.set(Calendar.MINUTE, 0);
    long fmillis = fcal.getTimeInMillis();

    long millisofday = millis - fmillis;     
    return ((int)(millisofday / 1000));
  }
  
  public void setScheduleYear(int year)
  {
    this.set(Calendar.YEAR, year);
  }

  public void setScheduleDayOfYear(int dayofyear)
  {
    this.set(Calendar.DAY_OF_YEAR, dayofyear);
  }
  
  public void setScheduleSecondOfDay(int secondofday)
  {
    this.set(Calendar.HOUR_OF_DAY, 0);
    this.set(Calendar.MINUTE, 0);
    this.add(Calendar.SECOND, secondofday);
  }
  
  public ScheduleDate getDateByOffset(int days)
  {
    ScheduleDate newdate = new ScheduleDate();
    newdate.setDate(this.getDate());
    newdate.add(Calendar.DAY_OF_YEAR, days);
    return newdate;
  }
  
  public void addSeconds(int seconds)
  {    
    this.add(Calendar.SECOND, seconds);
  }
  
  public void addMinutes(int minutes)
  {    
    this.add(Calendar.MINUTE, minutes);
  }
  
  public void addHours(int hours)
  {    
    this.add(Calendar.HOUR, hours);
  }

  public void addDays(int days)
  {    
    this.add(Calendar.DATE, days);
  }

  public void addMonths(int months)
  {    
    this.add(Calendar.MONTH, months);
  }

  public void addYears(int years)
  {    
    this.add(Calendar.YEAR, years);
  }

  
  public ScheduleDate getDateWithAddedSeconds(int seconds)
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.add(Calendar.SECOND, seconds);
    return newdate;
  }
  
  public ScheduleDate getDateWithAddedMinutes(int minutes)
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.addMinutes(minutes);
    return newdate;
  }
  
  public ScheduleDate getDateWithAddedHours(int hours)
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.addHours(hours);
    return newdate;
  }
  
  public ScheduleDate getDateWithAddedDays(int days)
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.addDays(days);
    return newdate;
  }
  
  public ScheduleDate getDateWithAddedMonths(int months)
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.addMonths(months);
    return newdate;
  }
  
  public ScheduleDate getDateWithAddedYears(int years)
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.addYears(years);
    return newdate;
  }
  
  public ScheduleDate getMondayOfWeek()
  {
    ScheduleDate newdate = new ScheduleDate(this);
    while (!(newdate.get(ScheduleDate.DAY_OF_WEEK) == ScheduleDate.MONDAY))
    {
      newdate.add(ScheduleDate.DATE, -1);
    }
    return newdate;
  }
  
  public ScheduleDate getMondayOfMonth()
  {
    ScheduleDate newdate = new ScheduleDate(this);
    while (!(newdate.get(ScheduleDate.DAY_OF_WEEK) == ScheduleDate.MONDAY))
    {
      newdate.add(ScheduleDate.DATE, 1);
    }
    return newdate;
  }
  public ScheduleDate getFirstSecondOfDay()
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.set(Calendar.HOUR_OF_DAY, 0);
    newdate.set(Calendar.MINUTE, 0);
    newdate.set(Calendar.SECOND, 0);
    return newdate;
  }
  
  public ScheduleDate getFirstDayOfMonth()
  {
    ScheduleDate newdate = new ScheduleDate(this);
    newdate.set(Calendar.DAY_OF_MONTH, 1);
    newdate.set(Calendar.HOUR_OF_DAY, 0);
    newdate.set(Calendar.MINUTE, 0);
    newdate.set(Calendar.SECOND, 0);
    return newdate;
  }
  
  public ScheduleDate getLastSecondOfMonth()
  {
	
    ScheduleDate newdate = new ScheduleDate(this);
    
    newdate.add(Calendar.MONTH, 1);
    newdate.set(Calendar.DAY_OF_MONTH, 1);
    newdate.add(Calendar.DAY_OF_MONTH, -1);
    newdate.set(Calendar.HOUR_OF_DAY, 23);
    newdate.set(Calendar.MINUTE, 59);
    newdate.set(Calendar.SECOND, 59);
    
    return newdate;
  }
  
  public boolean isSameDay(ScheduleDate date)
  {
    return ((this.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH)) && (this.get(Calendar.MONTH) == date.get(Calendar.MONTH)) && (this.get(Calendar.YEAR) == date.get(Calendar.YEAR)));
  }
  
  
}
