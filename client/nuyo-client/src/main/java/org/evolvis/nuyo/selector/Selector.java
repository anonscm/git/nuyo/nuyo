package org.evolvis.nuyo.selector;

import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.plugins.assigner.SimpleSearchComponent;
import org.evolvis.xana.action.ActionRegistry;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.entity.EntityList;

/**
 * This class contains most of the functionality of the address-selector. 
 * It can be used for example by a frame, to run the selector standalone.
 * 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class Selector implements AddressListProvider {
  
  private static Logger logger = Logger.getLogger(Selector.class.getName());
  private JPanel mainPanel;
  private SimpleSearchComponent searchComponent;
  private EntityTableComponent tableComponent;
  private JButton defaultButton;
  private Database database;
  
  public Selector() {
    
    database= ApplicationServices.getInstance().getCurrentDatabase();
    searchComponent = new SimpleSearchComponent(database);
    searchComponent.addResultListener(this);
    tableComponent = new EntityTableComponent(searchComponent.getAddresses());
    //register "this" as DataProvider:
    ActionRegistry.getInstance().addDataProvider(this);
    defaultButton=searchComponent.getSearchButton();
  }
  
  /**
   * returns the selector user interface. If it is used in a Frame or a Dialog, 
   * additional UI-Elements can be added (such as a Menu or a Button Bar).
   * @return a JPanel that contains most UI-Elements
   */
  public JPanel getComponent(){
    if (mainPanel==null) mainPanel=buildPanel();
    return mainPanel;
  }
  
  private JPanel buildPanel(){
    
    //specify columns and rows
    FormLayout layout = new FormLayout(
                                       "left:pref:GROW, pref:GROW", //columns 
    "pref,15dlu, fill:pref:GROW");      // rows
    
    //create and configure a builder
    PanelBuilder builder = new PanelBuilder(layout);
    builder.setDefaultDialogBorder();
    
    //Obtain a reusable constraints object to place components in the grid.
    CellConstraints cc = new CellConstraints();
    
    //fill grid with component by using the builder:
    TitledBorder titledBorder=new TitledBorder("Suche");
    titledBorder.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
    Insets insets = new Insets(5,5,5,5);
    EmptyBorder emptyBorder = new EmptyBorder(insets);
    CompoundBorder border = new CompoundBorder(titledBorder,emptyBorder);
    searchComponent.getComponent().setBorder(border);
    
    builder.add(searchComponent.getComponent(), cc.xy(1,1));
    builder.add(tableComponent.getComponent(), cc.xyw(1,3,2));
    
    return builder.getPanel();
  }
  /**
   * returns the EntityList that is displayed in the Selection-Table
   * @return
   */
  public EntityList getSelection(){
    return tableComponent.getSelection();
  }
  
  public void searchCompleted(){
	// TODO remove
    //tableComponent.updateEntityList(searchComponent.getAddresses());
  }
  
  /*
   *  registers a data consumer (in this case: an AddressListConsumer)
   * @see de.tarent.contact.gui.action.DataProvider#registerDataConsumer(java.lang.Object)
   */
  public void registerDataConsumer(Object object) {
    if (object instanceof AddressListConsumer){
      ((AddressListConsumer)object).registerDataProvider(this);
      logger.finer("AddressListConsumer "+object.getClass().getName()+" wird als AddressListConsumer registriert.");
    }
  }
  
  /*
   * returns the selected Addreses
   * @see de.tarent.contact.gui.action.AddressListProvider#getAddresses()
   */
  public Addresses getAddresses() {
    return ((Addresses)this.getSelection());
  }
  
  public JButton getDefaultButton() {
    return defaultButton;
  }
  
  public void setDefaultButton(JButton defaultButton) {
    this.defaultButton = defaultButton;
  }
}
