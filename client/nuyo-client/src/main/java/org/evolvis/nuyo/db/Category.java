/* $Id: Category.java,v 1.12 2007/08/30 16:10:29 fkoester Exp $
 * 
 * Created on 06.11.2003
 */
package org.evolvis.nuyo.db;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.groupware.impl.CategoryImpl;

/**
 * This class represents a category.
 * 
 * @author Sebastian
 * @author mikel
 */
public class Category extends CategoryImpl {

    private final static Logger logger = Logger.getLogger(Category.class.getName());

    /**
     * Tests whether other contacts can be assigned to this category.
     */
    public boolean isAssignable() {
        int type = getType();
        return 
            !getVirtual().booleanValue()
            && type != TYPE_TRASH
            && type != TYPE_ALL_ADDRESSES
            && type != TYPE_REVIEW;
    }
    
    public Boolean isPrivate(){
        int type = getType();
        return new Boolean(type == TYPE_PRIVATE);
    }

    public boolean isTrash(){    	
        return getType() == TYPE_TRASH;
    }

    /*
     * Getter und Setter
     */
    /**
     * @return Id der Kategorie
     */
    public Integer getIdAsInteger(){
        return new Integer(getId());
    }

    public String getIdAsString()  {
        return "" + getId();
    }


    
   
    public SubCategory getSubCategory(String id){    		
    		return (SubCategory) getSubCategory(Integer.parseInt(id));
    }
    
    public String getName()  {
        return getCategoryName(); 
    }

    public void setName(String name)   {
    		setCategoryName(name);
    }

    /**
     * @return Typ der Kategorie
     */
    public int getType()  {
        
    		try {
            return getCategoryType().intValue();
        } catch (NumberFormatException nfe) {
            return -1;
        }
    }
    /*
     * String
     */
    public List getSubCategories(){
    		if (super.getSubCategories() != null)
    			return super.getSubCategories();
    		return new ArrayList();
    }
    
    public String toString(){
    		return getCategoryName() != null? getCategoryName(): ""+getId();	
    }
    
    
    public String toFullString() {
    
    		return "ID: " + getId() 
    			+ "\nName: " + getCategoryName()
    			+ "\nType: " + getCategoryType()
    			+ "\nDescription: " +getDescription()
    			+ "\nVirtual: " + getVirtual()
    			+ "\nPrivate: " + isPrivate()
    			+ "\nPrivateUser: " + getPrivateUser()
    			+ "\nSubCategories: " + getSubCategories()
    			+ "\nRightToReadAddress: " + getRightToReadAddress()
			+ "\nRightToAddAddress: " + getRightToAddAddress()
			+ "\nRightToRemoveAddress: " + getRightToRemoveAddress()
			+ "\nRightToEditAddress: " + getRightToEditAddress()
			+ "\nRightToAddSubCategory: " + getRightToAddSubCategory()
			+ "\nRightToRemoveSubCategory: " + getRightToRemoveSubCategory()
			+ "\nRightToStructure: " + getRightToStructure()
			+ "\nRightToGrant: " + getRightToGrant();	
    }

}
