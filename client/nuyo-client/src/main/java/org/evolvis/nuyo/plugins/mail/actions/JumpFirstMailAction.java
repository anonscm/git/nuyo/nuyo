/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class JumpFirstMailAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -704501300140854313L;

	public void actionPerformed(ActionEvent e)
	{
		DispatchEMailDialog.getInstance().jumpToFirstMail();
	}
}
