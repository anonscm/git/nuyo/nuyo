/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractListModel;

import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetListBox;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;
import org.evolvis.nuyo.util.general.ContactLookUpNotFoundException;
import org.evolvis.nuyo.util.general.DataConversionException;
import org.evolvis.nuyo.util.general.DataConverter;



/**
 * TODO: remove if not used in AddressFieldFactory.getGUIElement()!
 * @deprecated not used (only by factory)
 * 
 * @author niko
 */
public class ListField extends GUIElementAdapter implements FocusListener
{
  private TarentWidgetListBox m_oListBox;
  private String m_sLookupTableKey = null; 
  private boolean m_bShouldScroll = true;
  private List m_oData;
  private ListFieldModel m_oListFieldModel;
  private int m_iNumRows = 5;
  
  public ListField()
  {
    super();
     
    m_oData = new ArrayList();
    m_oListFieldModel = new ListFieldModel(m_oData);
    m_oListBox = new TarentWidgetListBox(m_oListFieldModel);
  }
  
  
  private class ListFieldModel extends AbstractListModel 
  {
    private List m_oList;
    
    public ListFieldModel(List list)
    {
      m_oList = list;  
    }
    
    public int getSize() 
    { 
      return m_oList.size(); 
    }

    public Object getElementAt(int index) 
    { 
      return m_oList.get(index); 
    }
  } 
  
  public String getListenerName()
  {
    return "ListField";
  }  
  
  public GUIElement cloneGUIElement()
  {
    ListField listfield = new ListField();
    listfield.m_sLookupTableKey = m_sLookupTableKey;
    listfield.m_iNumRows = m_iNumRows;
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      listfield.addParameter(param.getKey(), param.getValue());
    }
    return listfield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "ListField", new DataContainerVersion(0));
    
    ParameterDescription lookuppd = new ParameterDescription("lookup", "LookUp Table", "die in diesem Feld benutzte LookupTable.");
    lookuppd.addValueDescription("String", "der Key der Lookup-Tabelle");
    d.addParameterDescription(lookuppd);
    
    ParameterDescription scrollpd = new ParameterDescription("scroll", "Should Scroll", "soll die Liste bei Auswahl eines Eintrags scrollen? (default: true)");
    scrollpd.addValueDescription("Boolean", "true wenn gescrollt werden soll");
    d.addParameterDescription(scrollpd);
    
    ParameterDescription rowspd = new ParameterDescription("rows", "Number of visible rows", "Anzahl der sichtbaren Zeilen in der Liste");
    rowspd.addValueDescription("Integer", "beliebiger Integer-Wert (mind. 0 aber nicht zu gro�)");
    d.addParameterDescription(rowspd);
    
    ParameterDescription minsizepd = new ParameterDescription("minsize", "Minimuzm size of list", "Minimale Gr��e der Liste in Pixel (X x Y");
    minsizepd.addValueDescription("Dimension", "INTEGER x INTEGER (zwei durch 'x' getrennte Integer-Zahlen)");
    d.addParameterDescription(minsizepd);
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("lookup".equalsIgnoreCase(key)) 
    {
      m_sLookupTableKey = value;
      m_oParameterList.add(new ObjectParameter(key, value));
      return true;
    }    
    else if ("scroll".equalsIgnoreCase(key)) 
    {      
      try
      {
        m_bShouldScroll = DataConverter.parseBoolean(value).booleanValue();
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch (DataConversionException e)
      {
        //e.printStackTrace();
      }
    }
    else if ("rows".equalsIgnoreCase(key)) 
    {
      try
      {
        int rows = Integer.parseInt(value);
        m_iNumRows = rows;
        m_oListBox.getJList().setVisibleRowCount(rows);    
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe)
      {        
      }
    }    
    else if ("minsize".equalsIgnoreCase(key)) 
    {
      Dimension dim = parseDimension(value);       
      if (dim != null)
      {
        m_oListBox.setMinimumSize(dim);        
        m_oListBox.setPreferredSize(dim);        
        m_oListBox.setSize(dim);        
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
    }
    
    return false;
  }


  private Dimension parseDimension(String dimstring)
  {
    String[] coords = dimstring.split("x");
    if (coords.length == 2)
    {
      int x = Integer.parseInt(coords[0]);
      int y = Integer.parseInt(coords[1]);
      return new Dimension(x, y);        
    }
    return null;    
  }

  public boolean canDisplay(Class data)
  {
    return data.isInstance(String.class);
  }

  public Class displays()
  {
    return String.class;
  }
  
  
  public TarentWidgetInterface getComponent()
  {
    return m_oListBox;
  }

  public void setData(Object data)
  {
    if (data instanceof String)
    {
      m_oListBox.getJList().setSelectedValue(data, m_bShouldScroll);
    }
  }

  public Object getData()
  {
    return m_oListBox.getJList().getSelectedValue();
  }


  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    //System.out.println("focuslost at " + e.getComponent().getLocation());
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }
    
  public void initElement()
  {
    m_oListBox.addFocusListener(this);
    m_oData.clear();
    if (m_sLookupTableKey != null)
    {
      String[] entries;
      try
      {
        entries = getDataContainer().getDataAccess().getLookupTableEntries(m_sLookupTableKey);
        for(int i=0; i<(entries.length); i++)
        {
          m_oData.add(entries[i]);
        }      
      }
      catch (ContactLookUpNotFoundException e) {}
    }   
  }

  public void disposeElement()
  {
    m_oListBox.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oListBox.requestFocus();
  }
  
  
  public Point getOffsetForError(ErrorHint errorhint)
  {
    return(new Point(0,0));
  }

}
