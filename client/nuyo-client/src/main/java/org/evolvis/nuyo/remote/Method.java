package org.evolvis.nuyo.remote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.db.persistence.IEntity;

import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.OctopusTask;


/**
 * @author kleinw
 * 
 * Diese Klasse kapselt einen einzelnen Soapaufruf.
 *  
 */
public class Method {

    public final static Logger logger = Logger.getLogger(Method.class.getName());

    OctopusTask task;

	private OctopusResult oRes;

	private String used_content = null;

    static public OctopusConnection getDefaultConnection() {
        OctopusConnectionFactory ocConnectionFactory = OctopusConnectionFactory.getInstance();
        return ocConnectionFactory.getConnection(OctopusDatabase.OC_CONNECTION_IDENTIFIER);
    }

  
    public Method(String taskName) {
        task = getDefaultConnection().getTask(taskName);
    }
	
	public String getUsedContent(){
		return used_content;
	}
    
      
    public Method add(String paramName, Object value) {
        task.add(paramName, value);
        return this;
    }
    
    public Method addParam(String paramName, Boolean value) {
        task.add(paramName, value);
        return this;
    }
    
    public Method addParam(String paramName, String value){
        task.add(paramName, value);
        return this;
    }
    
    public Method addParam(String paramName, Integer value){
        task.add(paramName, value);
        return this;
    }

    public Method addParam(String paramName, Long value){
        task.add(paramName, value);
        return this;
    }
    
    public Method addParam(String paramName, Collection value) {
        task.add(paramName, value);
        return this;
    }

    public void addIdList(String paramName, Collection entities) {
        if (entities != null && entities.size() > 0) {
            List tmp = new ArrayList();
            for (Iterator it = entities.iterator();it.hasNext();) {
                tmp.add(new Integer(((IEntity)it.next()).getId()));
            }
            add(paramName, tmp);
        }
    }
    
    public void addFilter(ISelection filter) throws ContactDBException {
        if(filter != null && filter.encode().size() > 0) { 
            Map tmp = filter.encode();
            for(Iterator it = filter.encode().keySet().iterator();it.hasNext();) {
	            String key = (String)it.next();
	            this.add(key, tmp.get(key));
	        }
        }
    }
    
    public Object invoke() throws ContactDBException {
        try {
            oRes = task.invoke();
			Iterator it = oRes.getDataKeys();
            if (!it.hasNext())
                return null;
            
            // Wird jetzt in der OctopusClientApi gemacht.
            //             if (result instanceof Object[])
            //                 return Arrays.asList((Object[])result);
			used_content = (String)it.next();
            return oRes.getData(used_content);        
        
        } catch (OctopusCallException e) {
            logger.log(Level.WARNING, "Aufruf eines Octopus Tasks fehlgeschlagen", e);
            throw new ContactDBException(ContactDBException.EX_OCTOPUS, e);
        }
    }

	public OctopusResult getORes() {
		return oRes;
	}
	
}



//     public String resp() {
//         String respMsg = null;
//         try {
//             return _call.getResponseMessage().getSOAPPartAsString();
//         } catch (AxisFault e) {
//             return "Bei der Auswertung des Repsonsecontents trat ein Fehler auf.";
//         }
//     }

//     public String req() {
//         String respMsg = null;
//         try {
//             return _call.getMessageContext().getRequestMessage()
//                     .getSOAPPartAsString();
//         } catch (AxisFault e) {
//             return "Bei der Auswertung der Request trat ein Fehler auf.";
//         }
//     }

//     public String respSize() {
//         try {
//             return new Long(_call.getResponseMessage().getContentLength())
//                     .toString();
//         } catch (AxisFault e) {
//             return "Bei der Auswertung des Repsonsecontents trat ein Fehler auf.";
//         }
//     }

//     public String reqSize() {
//         try {
//             return new Long(_call.getMessageContext().getRequestMessage()
//                     .getContentLength()).toString();
//         } catch (AxisFault e) {
//             return "Bei der Auswertung des Requestcontents trat ein Fehler auf.";
//         }
//     }
