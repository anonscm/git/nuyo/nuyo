package org.evolvis.nuyo.gui.actions.massAssignmentActions;

import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;


public class MoveAddressesAction extends AssignOrDeassignAddressesAction {
	
	private static Logger log = Logger.getLogger(MoveAddressesAction.class.getName());

	protected CategoryTree actionPerformedImpl() {

		List addressPks = ApplicationServices.getInstance().getActionManager()
				.getAddresses().getPkList();

		if (addressPks == null || addressPks.size() == 0){
			log.warning(Messages.getString("AssignOrDeassignAddresses_No_Addresses_Warning"));
			return null;
		}

		List accessableCategories = null;
		List removeableCategories = null;
		try {
			accessableCategories = ApplicationServices.getInstance()
					.getCurrentDatabase().getCategoriesWithCategoryRightsOR(
							false, false, true, true, false, false, true, false, true);
			removeableCategories = ApplicationServices.getInstance()
					.getCurrentDatabase()
					.getAssociatedCategoriesForAddressSelection(
							ApplicationServices.getInstance().getActionManager().getAddresses().getPkList(),
							false, false, true, false, false, true, false, false, false);
		} catch (ContactDBException e1) {
			log.warning(Messages.getString("AssignOrDeassignAddresses_Error_Warning"));
		}

		return CategoryTree.createMoveCategoryTree(addressPks.size(),
				accessableCategories, removeableCategories,
				new MoveAddressesHandler(addressPks));
	}
}
