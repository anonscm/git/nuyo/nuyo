/*
 * Created on 29.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;


/**
 * @author niko
 *
 */
public class DataContainerManager
{
  private Map m_oDataContainerErrors;
  private List m_oErrorEventListener;
  private DataContainerEventListener m_oDataContainerEventListener;
  private List m_oObserverList;
  
  public DataContainerManager()
  {
    m_oDataContainerErrors = new HashMap();
    m_oErrorEventListener = new ArrayList();
    m_oObserverList =  new ArrayList();
    m_oDataContainerEventListener = new DataContainerEventListener();    
  }
  
  private void addErrorHint(ErrorHint hint)
  {    
    DataContainerErrors errors = getErrorsOfDataContainer(hint.getDataContainer());
    errors.addErrorHint(hint);
  }

  private void clearErrorHints()
  {
    m_oDataContainerErrors.clear();
    fireErrorEvent(new DataContainerErrorEvent(DataContainerErrorEvent.HIDEERRORS));
  }
  
  private void showErrorHints()
  {
    fireErrorEvent(new DataContainerErrorEvent(DataContainerErrorEvent.SHOWERRORS));
  }
  
  public DataContainerErrors getErrorsOfDataContainer(DataContainer container)
  {
    if (!m_oDataContainerErrors.containsKey(container)) m_oDataContainerErrors.put(container, new DataContainerErrors(container));
    DataContainerErrors event = (DataContainerErrors)m_oDataContainerErrors.get(container);
    return event;
  }
  
  public List getDataContainersWithErrors()
  {
    return new ArrayList(m_oDataContainerErrors.keySet());
  }
  
  public void addErrorEventListener(ErrorEventListener listener)
  {
    m_oErrorEventListener.add(listener);
  }

  public void removeErrorEventListener(ErrorEventListener listener)
  {
    m_oErrorEventListener.remove(listener);
  }
  
  private void fireErrorEvent(DataContainerErrorEvent event)
  {
    Iterator it = m_oErrorEventListener.iterator();
    while(it.hasNext())
    {
      ErrorEventListener listener = (ErrorEventListener)(it.next());
      listener.errorEvent(event);
    }
  }
  
  
  public void addDataContainerToObserve(DataContainer container)
  {
    if (!(m_oObserverList.contains(container)))
    {
      container.addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATORSTART, m_oDataContainerEventListener);
      container.addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATOREND,   m_oDataContainerEventListener);
      container.addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_DATAINVALID,    m_oDataContainerEventListener);
    }
  }
  
  public void removeDataContainerToObserve(DataContainer container)
  {
    if ((m_oObserverList.contains(container)))
    {
      container.removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATORSTART, m_oDataContainerEventListener);
      container.removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATOREND,   m_oDataContainerEventListener);
      container.removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_DATAINVALID, m_oDataContainerEventListener);
    }
  }
    
  private class DataContainerEventListener implements TarentGUIEventListener
  {
    public String getListenerName()
    {
      return "ErrorEventManager";
    }
    
    public void event(TarentGUIEvent tge)
    {
      if (tge.getName().equals(TarentGUIEvent.GUIEVENT_VALIDATORSTART))
      {
        clearErrorHints();
      }
      else if (tge.getName().equals(TarentGUIEvent.GUIEVENT_VALIDATOREND))
      {
        showErrorHints();
      }
      else if (tge.getName().equals(TarentGUIEvent.GUIEVENT_DATAINVALID))
      {
        ErrorHint errorhint = tge.getErrorHint();
        if (tge.getSourceContainer() != null) errorhint.setDataContainer(tge.getSourceContainer());
        if (errorhint != null)
        {
          addErrorHint(errorhint);
//System.out.println("ErrorName        = " + errorhint.getName());
//System.out.println("ErrorDescription = " + errorhint.getDescription());
//System.out.println("ErrorException   = " + errorhint.getException());
//if (errorhint instanceof PositionErrorHint)
//{
//  System.out.println("ErrorPosition    = " + ((PositionErrorHint)errorhint).getPosition());  
//}
        }
      } 
    }
  }

}
