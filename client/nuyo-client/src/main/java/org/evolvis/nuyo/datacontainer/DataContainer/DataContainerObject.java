/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.List;

import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;



/**
 * @author niko
 *
 */
public interface DataContainerObject extends DescriptedDataContainer, TarentGUIEventListener
{
  public void init();
  public void dispose();
  
  public boolean addParameter(String key, String value);
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor();

  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();

  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);      
  public void fireTarentGUIEvent(TarentGUIEvent e);
}
