package org.evolvis.nuyo.gui.calendar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.octopus.AppointmentImpl;
import org.evolvis.nuyo.db.octopus.UserImpl;
import org.evolvis.nuyo.db.persistence.PersistenceManager;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.WidgetPool;
import org.evolvis.nuyo.gui.fields.ScheduleNavigationField;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;


/*
 * Created on 17.07.2003
 *
 */

/**
 * @author niko
 *
 */
public class SchedulePanel extends JPanel// implements ScheduleEntryListener
{
    private static TarentLogger logger = new TarentLogger(SchedulePanel.class);
    
    private final int m_iSecondsOfNewScheduleEntrySchedule = (60 * 60) * 2;
    private final int m_iSecondsOfNewScheduleEntryToDo = (60 * 60) * 1;
    private final int SECONDSPERDAY = 24 * 60 * 60; 
    
    public final static Object TYPE_DAY = "TYPE_DAY";
    public final static Object TYPE_WEEK = "TYPE_WEEK";
    public final static Object TYPE_OTHER = "TYPE_OTHER";
    
    private JLayeredPane m_oLayerPane;
    private GridLinePanel m_oGridLinePanel;
    private BigDayNamesPanel m_oDayNamesPanel;
    private HourNamesPanel m_oHourNamesPanel;
    private JScrollPane m_oGridScrollPane;
    private JPanel m_oMainPanel;
    private ScheduleData m_oScheduleData;
    private ArrayList m_oScheduleEntryListeners;
    
    private JScrollPane m_oHourScrollPane;
    private JScrollPane m_oDayScrollPane;
    
    private int m_iFirstDay;
    private int m_iNumDays;
    private int m_bSecondGridSpace = 15 * 60;
    private ScheduleDate m_oFirstDay;  
    
    private List m_oResetEntries = null;
    private boolean m_bSnapToSecondGrid = true;
    
    private int m_oPopUpX;
    private int m_oPopUpY;
    
    private Point m_oMovingStartPoint;
    
    private SchedulePanel m_oSchedulePanel;
    private TarentCalendar m_oAppointmentDisplayManager = null;
    
    
    private ImageIcon m_oIcon_NewSchedule;
    private ImageIcon m_oIcon_NewToDo;  
    
    private ImageIcon m_oIcon_ShowWeek;  
    private ImageIcon m_oIcon_ShowDay;  
    private ImageIcon m_oIcon_ShowMonth;  
    private ImageIcon m_oIconDelete;
    private ImageIcon m_oIconEdit;
    private ImageIcon m_oIcon_Magnetraster;
    
    private Object m_oType;
    private int m_UniqueID;
    
    
    public SchedulePanel(int UniqueID, ScheduleData oScheduleData, Object type, int numdays)
    {      
        
        try {
            // Add to Registry
            SEPRegistry.getInstance().registerSchedulePanel(UniqueID);
        } catch (Exception e) {
            logger.severe("failed regestering shedule panel", e);
        }
        
        m_UniqueID = UniqueID;
        m_oType = type;
        m_oSchedulePanel = this;
        m_iNumDays = numdays;
        
        m_oScheduleData = oScheduleData;
        
        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIcon_NewSchedule = (ImageIcon)iconFactory.getIcon("new_schedule.gif");
        m_oIcon_NewToDo = (ImageIcon)iconFactory.getIcon("new_todo.gif");
        
        m_oIcon_ShowWeek = (ImageIcon)iconFactory.getIcon("calendar_week.gif");
        m_oIcon_ShowDay = (ImageIcon)iconFactory.getIcon("calendar_day.gif");  
        m_oIcon_ShowMonth = (ImageIcon)iconFactory.getIcon("calendar_month.gif");  
        
        m_oIconEdit = (ImageIcon)iconFactory.getIcon("schedule_edit.gif");  
        m_oIconDelete = (ImageIcon)iconFactory.getIcon("schedule_delete.gif");  
        
        m_oIcon_Magnetraster = (ImageIcon)iconFactory.getIcon("usegrid.gif");  
        
        m_oScheduleEntryListeners = new ArrayList();
        
        this.setLayout(new BorderLayout());
        
        m_oMainPanel = new JPanel();
        m_oMainPanel.setLayout(new BorderLayout());
        
        m_oLayerPane = new JLayeredPane();    
        
        
        setLayerPaneSize();
        
        m_oGridScrollPane = new JScrollPane(m_oLayerPane);
        m_oGridScrollPane.setBorder(new EmptyBorder(0,0,0,0));
        m_oGridScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        m_oGridScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        
        m_oFirstDay = new ScheduleDate();
        
        m_oGridLinePanel = new GridLinePanel(m_oScheduleData, m_oFirstDay, m_iNumDays);    
        m_oLayerPane.add(m_oGridLinePanel, new Integer(0));
        m_oGridLinePanel.setBounds(0,0, (m_oScheduleData.dayWidth * m_iNumDays), m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours);
        
        
        m_oDayNamesPanel = new BigDayNamesPanel(m_oScheduleData, m_oFirstDay, m_iNumDays);
        m_oDayNamesPanel.addMouseListener(new DayNamesPanelMouseListener());    
        m_oDayNamesPanel.setShowShort(true);
        m_oDayNamesPanel.setShowDate(true);
        m_oDayNamesPanel.setShowDayName(true);
        m_oDayNamesPanel.setShowDescription(true);
        m_oDayNamesPanel.setShowName(true);
        setDayNamesPanelHeight();    
        
        m_oDayScrollPane = new JScrollPane(m_oDayNamesPanel); 
        m_oDayScrollPane.setBorder(new EmptyBorder(0,0,0,0));
        m_oDayScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        m_oDayScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        
        JPanel tmppanel = new JPanel();
        tmppanel.setLayout(new BorderLayout());
        tmppanel.add(Box.createHorizontalStrut(m_oScheduleData.firstDayX), BorderLayout.WEST);
        tmppanel.add(m_oDayScrollPane, BorderLayout.CENTER);
        m_oMainPanel.add(tmppanel, BorderLayout.NORTH);
        
        m_oHourNamesPanel = new HourNamesPanel(m_oScheduleData);  
        m_oHourNamesPanel.addMouseListener(new HourNamesPanelMouseListener());    
        Dimension hournamepanelsize = new Dimension(m_oScheduleData.firstDayX, m_oScheduleData.hourHeight *  m_oScheduleData.numberOfHours);
        m_oHourNamesPanel.setPreferredSize(hournamepanelsize);
        m_oHourNamesPanel.setMinimumSize(hournamepanelsize);    
        m_oHourNamesPanel.setMaximumSize(hournamepanelsize);
        m_oHourScrollPane = new JScrollPane(m_oHourNamesPanel); 
        m_oHourScrollPane.setBorder(new EmptyBorder(0,0,0,0));
        m_oHourScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        m_oHourScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        m_oMainPanel.add(m_oHourScrollPane, BorderLayout.WEST);
        
        m_oGridScrollPane.getHorizontalScrollBar().addAdjustmentListener(new HorizontalScrollListener());
        m_oGridScrollPane.getVerticalScrollBar().addAdjustmentListener(new VerticalScrollListener()); 
        
        m_oMainPanel.add(m_oGridScrollPane, BorderLayout.CENTER);
        
        m_oMainPanel.addComponentListener(new MainPanelResizeListener());
        
        this.add(m_oGridScrollPane.getHorizontalScrollBar(), BorderLayout.SOUTH);
        this.add(m_oGridScrollPane.getVerticalScrollBar(), BorderLayout.EAST);
        
        this.add(m_oMainPanel, BorderLayout.CENTER);
        
        //Add listener to components that can bring up popup menus.
        MouseListener popupListener = new PanelPopupListener();
        m_oGridLinePanel.addMouseListener(popupListener);    
        
        MouseListener oDayNamePopupListener = new DayNamePopupListener();
        m_oDayNamesPanel.addMouseListener(oDayNamePopupListener);    
    }
    
    public int getID(){
        return m_UniqueID;
    }
    
    public ImageIcon getIconDelete()
    {
        return m_oIconDelete;
    }
    
    public ImageIcon getIconEdit()
    {
        return m_oIconEdit;
    }
    
    
    private void setDayNamesPanelHeight()
    {
        int height = m_oDayNamesPanel.getUsedHeight();
        Dimension daynamepanelsize = new Dimension((m_oScheduleData.dayWidth * m_iNumDays), height);
        m_oDayNamesPanel.setPreferredSize(daynamepanelsize);
        m_oDayNamesPanel.setMinimumSize(daynamepanelsize);    
        m_oDayNamesPanel.setMaximumSize(daynamepanelsize);
        m_oDayNamesPanel.setSize(daynamepanelsize);
        this.revalidate();
        this.repaint();    
    }
    
    
    public void repaintPanel()
    {    
        if (m_oGridLinePanel != null) m_oGridLinePanel.repaint();
        if (m_oHourNamesPanel != null) m_oHourNamesPanel.repaint();
        if (m_oDayNamesPanel != null) m_oDayNamesPanel.repaint();
    }
    
    
    private void saveDayNameSettings()
    {    
        Preferences oPreferences = getGUIListener().getPreferences(CalendarSettings.PREF_CALENDAR_NODE_NAME);
        
        oPreferences.putBoolean(m_oType.toString() + "_ShowShort", m_oDayNamesPanel.isShowShort());
        oPreferences.putBoolean(m_oType.toString() + "_ShowDate", m_oDayNamesPanel.isShowDate());
        oPreferences.putBoolean(m_oType.toString() + "_ShowDayName", m_oDayNamesPanel.isShowDayName());
        oPreferences.putBoolean(m_oType.toString() + "_ShowDescription", m_oDayNamesPanel.isShowDescription());
        oPreferences.putBoolean(m_oType.toString() + "_ShowName", m_oDayNamesPanel.isShowName());
    }
    
    public void deleteScheduleEntryPanel(ScheduleEntryPanel entry)
    {
        fireDeleteSelected(entry);  
    }
    
    public void editScheduleEntryPanel(ScheduleEntryPanel entry)
    {
        fireEditSelected(entry);  
    }
    
    private void setLayerPaneSize(){
        Dimension dimension = new Dimension(m_oScheduleData.dayWidth * m_iNumDays, m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours);
        m_oLayerPane.setPreferredSize(dimension);
        m_oLayerPane.setMinimumSize(dimension);    
        m_oLayerPane.setMaximumSize(dimension);
    }
    
    private JPopupMenu createPanelPopUpMenu(ScheduleDate scheduleDate)
    {
        JPopupMenu oPopupMenu = new JPopupMenu();
        
        // Allow user to create Appointment or ToDo
        // check current activated Calendars / Releases and add MenuItems accordingly
        
        Vector AppointmentItemVector = new Vector();
        Vector ToDoItemVector = new Vector();
        
        //Loop all Calendars
        for (Iterator iter = m_oGUIListener.getCalendarCollection().getCollection().iterator();iter.hasNext();){
            org.evolvis.nuyo.db.Calendar currentCalendar = (org.evolvis.nuyo.db.Calendar) iter.next();
            
            Integer calendarID = new Integer(currentCalendar.getId());
            
            try {
                CalendarSecretaryRelation calSecRel = currentCalendar.getCalendarSecretaryforUser(getGUIListener().getUser(getGUIListener().getCurrentUser()).getId());
                if (calSecRel != null && calSecRel.getWriteDateRange().isDefined() && calSecRel.getWriteDateRange().containsDate(scheduleDate.getDate())){
                    // User Secretary with Write Access
                    User tmpuser = UserImpl.getUserFromID(calSecRel.getSecretaryUserOwnerID());
                    String userOwnerName = tmpuser.getAttribute(User.KEY_FIRST_NAME) + " " + tmpuser.getAttribute(User.KEY_LAST_NAME); 
                    Integer userOwnerID = calSecRel.getSecretaryUserOwnerID();
                    
                    JMenuItem menuItemNewSchedule = new JMenuItem("neuer Termin in Vertretung f�r " + userOwnerName , m_oIcon_NewSchedule);
                    menuItemNewSchedule.addActionListener(new MenuItem_NewAppointmentorToDo_clicked(false, userOwnerID, calendarID));
                    AppointmentItemVector.add(menuItemNewSchedule);
                    
                    JMenuItem menuItemNewToDo = new JMenuItem("neue Aufgabe in Vertretung f�r " + userOwnerName, m_oIcon_NewToDo);
                    menuItemNewToDo.addActionListener(new MenuItem_NewAppointmentorToDo_clicked(true, userOwnerID, calendarID));
                    ToDoItemVector.add(menuItemNewToDo);
                }else if(currentCalendar.getUser().getId() == getGUIListener().getUser(null).getId()){
                    // Users own private Calendar
                    JMenuItem menuItemNewSchedule = new JMenuItem("neuer Termin", m_oIcon_NewSchedule);
                    menuItemNewSchedule.addActionListener(new MenuItem_NewAppointmentorToDo_clicked(false, null, calendarID));
                    AppointmentItemVector.add(0, menuItemNewSchedule);
                    
                    JMenuItem menuItemNewToDo = new JMenuItem("neue Aufgabe", m_oIcon_NewToDo);
                    menuItemNewToDo.addActionListener(new MenuItem_NewAppointmentorToDo_clicked(true, null, calendarID));
                    ToDoItemVector.add(0, menuItemNewToDo);
                }
            } catch (ContactDBException e) {
                // Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        // Setup Menu in a convenient way
        if (AppointmentItemVector.size() > 1){
            // Nested
            JMenu menuItemAPPS = new JMenu("Termin anlegen");
            oPopupMenu.add(menuItemAPPS);
            for (Iterator iter = AppointmentItemVector.iterator(); iter.hasNext();)
                menuItemAPPS.add((JMenuItem) iter.next());
            
            JMenu menuItemTODO = new JMenu("Aufgabe anlegen");
            oPopupMenu.add(menuItemTODO);
            for (Iterator iter = ToDoItemVector.iterator(); iter.hasNext();)
                menuItemTODO.add((JMenuItem) iter.next());
            
            oPopupMenu.addSeparator();
        }else if (AppointmentItemVector.size() == 1){
            // Plain
            oPopupMenu.add((JMenuItem) AppointmentItemVector.get(0));
            oPopupMenu.add((JMenuItem) ToDoItemVector.get(0));
            oPopupMenu.addSeparator();
        }
        
        JMenuItem menuItemMagnetraster = new JCheckBoxMenuItem("Magnetraster", m_oIcon_Magnetraster, m_bSnapToSecondGrid);
        menuItemMagnetraster.addActionListener(new MenuItem_Magnetraster_clicked());
        oPopupMenu.add(menuItemMagnetraster);
        
        JMenu menuItemVIEW = new JMenu("Ansicht");
        oPopupMenu.add(menuItemVIEW);
        
        JMenuItem menuItemVIEW_Day = new JMenuItem("Tagesansicht", m_oIcon_ShowDay);
        menuItemVIEW_Day.addActionListener(new MenuItem_VIEW_Day_clicked());
        menuItemVIEW.add(menuItemVIEW_Day);
        
        JMenuItem menuItemVIEW_Week = new JMenuItem("Wochenansicht", m_oIcon_ShowWeek);
        menuItemVIEW_Week.addActionListener(new MenuItem_VIEW_Week_clicked());
        menuItemVIEW.add(menuItemVIEW_Week);
        
        JMenuItem menuItemVIEW_Month = new JMenuItem("Monatsansicht", m_oIcon_ShowMonth);
        menuItemVIEW_Month.addActionListener(new MenuItem_VIEW_Month_clicked());
        menuItemVIEW.add(menuItemVIEW_Month);
        
        if (TYPE_DAY.equals(m_oType))
        {
            menuItemVIEW_Day.setEnabled(false);
        }
        else if (TYPE_WEEK.equals(m_oType))
        {
            menuItemVIEW_Week.setEnabled(false);
        }
        
        return oPopupMenu;    
    }
    
    public void createNewAppointmentOrToDo(boolean isTask, int day, int second, Integer userID,Integer calendarID)
    {
        Integer _userID = userID;
        Integer _calendarID = calendarID;
        boolean _writeAccess = false;
        
        if ( _userID == null){ 
            _userID = new Integer(getGUIListener().getUser(getGUIListener().getCurrentUser()).getId());
        }else{
            _writeAccess = true;
        }
        
        if (_calendarID == null)
            _calendarID = new Integer(getGUIListener().getCalendarCollection().getUserCalendar().getId());

        
        // Get Custom Color 
        Color newColor = Color.WHITE;
        // 25.10.05 Von Sebastion umgebogen, damit die Kalenderfarbe nicht jedes mal neu geholt wird
        // String colorString = m_oGUIListener.getUserParameter(null, "CalendarColor_ID_" + _calendarID);
        String colorString = m_oGUIListener.getCalendarColor(_calendarID.intValue());
        if (colorString != null)
            newColor = Color.decode(colorString);
        
        // Startdatum, Sekunde auf Null...
        ScheduleDate startdate = new ScheduleDate(m_oSchedulePanel.getFirstDay());
        startdate.addSeconds((day * SECONDSPERDAY));
        startdate.setScheduleSecondOfDay(second);
        startdate.set(Calendar.SECOND, 0);
        
        // Enddatum
        ScheduleDate enddate ;
        if (isTask){
            enddate = new ScheduleDate(startdate).getDateWithAddedSeconds(m_iSecondsOfNewScheduleEntryToDo);
        }else{
            enddate = new ScheduleDate(startdate).getDateWithAddedSeconds(m_iSecondsOfNewScheduleEntrySchedule);          
        }
        
        // Crosses dayborder? create two seps if both are in displayrange
        if ( ! startdate.isSameDay(enddate))
        { 
            ScheduleEntry firstEntry= new ScheduleEntry(m_oSchedulePanel, startdate, enddate.getFirstSecondOfDay().getDateWithAddedMinutes(-1), isTask ,_writeAccess, _userID, newColor);
            getAppointmentDisplayManager().addAppointmentDisplayListener(firstEntry);
            m_oSchedulePanel.fireAddedEntry(firstEntry,startdate,enddate, _calendarID);
            
            //(if (enddate.after(getFirstDay().getFirstSecondOfDay().getDateWithAddedDays(getNumDays()))){
                // Create only SEP in viewable DateRange
                
            //}else{
//              ScheduleEntry secondEntry= new ScheduleEntry(m_oSchedulePanel, enddate.getFirstSecondOfDay(),enddate, isJob ,_writeAccess, _userID, newColor);
//              secondEntry.setFonts(m_oFontMenu, m_oFontText, m_oFontDate, m_oFontType, m_oFontDescription);
//              getAppointmentDisplayManager().addAppointmentDisplayListener(secondEntry);
//
//              m_oSchedulePanel.fireAddedEntry(secondEntry,startdate,enddate, _calendarID);
//              
            //}
           
            
            
        }else{
        
            // One SEP only
            ScheduleEntry newentry = new ScheduleEntry(m_oSchedulePanel, startdate, enddate, isTask ,_writeAccess, _userID, newColor);
            getAppointmentDisplayManager().addAppointmentDisplayListener(newentry);
            m_oSchedulePanel.fireAddedEntry(newentry,startdate,enddate, _calendarID);
        }           
    }
    
    private class MenuItem_NewAppointmentorToDo_clicked implements ActionListener
    {
        private Integer _userID ;
        private Integer _calendarID ;
        private boolean _isJob;
        
        public MenuItem_NewAppointmentorToDo_clicked(boolean isJob, Integer userID, Integer calendarID) {
            super();
            _userID = userID;
            _calendarID = calendarID;
            _isJob = isJob;
        }
        
        public void actionPerformed(ActionEvent e)
        {
            Point point = new Point(m_oPopUpX, m_oPopUpY);
            int day = getDayOfDirectPos(point);
            int second = getSecondOfPos(point);  
            second = snapToSecondGrid(second);
            createNewAppointmentOrToDo(_isJob,day,second,_userID,_calendarID);
        }    
    }
    
    public void setSnapToGrid(boolean snap)
    {
        m_bSnapToSecondGrid = snap;
    }
    
    public void setGridHeight(int seconds)
    {
        m_bSecondGridSpace = seconds;
    }
    
    private int snapToSecondGrid(int second)  
    {
        if (! m_bSnapToSecondGrid) return second;
        else
        {
            return ((int)(((int)second) / ((int)m_bSecondGridSpace))) * ((int)m_bSecondGridSpace);
        }
    }
    
    
    public void setAppointmentDisplayManager(TarentCalendar adm)
    {
        m_oAppointmentDisplayManager = adm;
    }
    
    public TarentCalendar getAppointmentDisplayManager()
    {
        return m_oAppointmentDisplayManager;
    }
    
    private WidgetPool m_oWidgetPool;
    public void setWidgetPool(WidgetPool wp)
    {
        m_oWidgetPool = wp;
    }
    
    public WidgetPool getWidgetPool()
    {
        return (m_oWidgetPool);
    }
    
    
    private GUIListener m_oGUIListener;
    public void setGUIListener(GUIListener gl)
    {
        m_oGUIListener = gl;
        
        Preferences oPreferences = getGUIListener().getPreferences(CalendarSettings.PREF_CALENDAR_NODE_NAME);
        m_oDayNamesPanel.setShowShort(oPreferences.getBoolean(m_oType.toString() + "_ShowShort", true));
        m_oDayNamesPanel.setShowDate(oPreferences.getBoolean(m_oType.toString() + "_ShowDate", true));
        m_oDayNamesPanel.setShowDayName(oPreferences.getBoolean(m_oType.toString() + "_ShowDayName", true));
        m_oDayNamesPanel.setShowDescription(oPreferences.getBoolean(m_oType.toString() + "_ShowDescription", true));
        m_oDayNamesPanel.setShowName(oPreferences.getBoolean(m_oType.toString() + "_ShowName", true));    
        setDayNamesPanelHeight();    
    }
    
    public GUIListener getGUIListener()
    {
        return (m_oGUIListener);
    }
    
    
    private class MenuItem_VIEW_Day_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            ScheduleNavigationField oCalendarNavigationField = (ScheduleNavigationField)(getWidgetPool().getController(CalendarVisibleElement.NAVIGATOR));
            if (oCalendarNavigationField instanceof ScheduleNavigationField)
            {  
                oCalendarNavigationField.setView(ScheduleNavigationField.VIEW_DAY);
            }
        }    
    }
    
    private class MenuItem_VIEW_Week_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            ScheduleNavigationField oCalendarNavigationField = (ScheduleNavigationField)(getWidgetPool().getController(CalendarVisibleElement.NAVIGATOR));
            if (oCalendarNavigationField instanceof ScheduleNavigationField)
            {  
                oCalendarNavigationField.setView(ScheduleNavigationField.VIEW_WEEK);
            }
        }    
    }
    
    private class MenuItem_VIEW_Month_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            ScheduleNavigationField oCalendarNavigationField = (ScheduleNavigationField)(getWidgetPool().getController(CalendarVisibleElement.NAVIGATOR));
            if (oCalendarNavigationField instanceof ScheduleNavigationField)
            {  
                oCalendarNavigationField.setView(ScheduleNavigationField.VIEW_MONTH);
            }
        }    
    }
    
    
    class PanelPopupListener extends MouseAdapter 
    {
        private JPopupMenu m_oPopupMenu;
        
        public void mousePressed(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        public void mouseReleased(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) 
        {
            if (e.isPopupTrigger()) 
            {
                m_oPopUpX = e.getX();
                m_oPopUpY = e.getY();
                
                //Calculate Date
                int day = getDayOfDirectPos(e.getPoint());
                ScheduleDate startdate = new ScheduleDate(m_oSchedulePanel.getFirstDay());
                startdate.addSeconds((day * SECONDSPERDAY));
                m_oPopupMenu = createPanelPopUpMenu(startdate);
                m_oPopupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }
    
    
    private JPopupMenu createDayNamePopUpMenu()
    {
        //...where the GUI is constructed:
        //Create the popup menu.
        JPopupMenu oPopupMenu = new JPopupMenu();
        
        JCheckBoxMenuItem item;
        
        JCheckBoxMenuItem menuItemShowShort = new JCheckBoxMenuItem("Kurzansicht", m_oDayNamesPanel.isShowShort());
        menuItemShowShort.addActionListener(new MenuItem_ShowShort_clicked());
        oPopupMenu.add(menuItemShowShort);
        
        oPopupMenu.addSeparator();
        
        JCheckBoxMenuItem menuItemShowDayName = new JCheckBoxMenuItem("Wochentag", m_oDayNamesPanel.isShowDayName());
        menuItemShowDayName.addActionListener(new MenuItem_ShowDayName_clicked());
        oPopupMenu.add(menuItemShowDayName);
        
        JCheckBoxMenuItem menuItemShowDate = new JCheckBoxMenuItem("Tagesdatum", m_oDayNamesPanel.isShowDate());
        menuItemShowDate.addActionListener(new MenuItem_ShowDate_clicked());
        oPopupMenu.add(menuItemShowDate);
        
        JCheckBoxMenuItem menuItemShowHolyDayName = new JCheckBoxMenuItem("Typ / Name", m_oDayNamesPanel.isShowName());
        menuItemShowHolyDayName.addActionListener(new MenuItem_ShowName_clicked());
        oPopupMenu.add(menuItemShowHolyDayName);
        
        JCheckBoxMenuItem menuItemShowDescription = new JCheckBoxMenuItem("Beschreibung", m_oDayNamesPanel.isShowDescription());
        menuItemShowDescription.addActionListener(new MenuItem_ShowDescription_clicked());
        oPopupMenu.add(menuItemShowDescription);
        
        if (m_oDayNamesPanel.isShowShort())
        {
            menuItemShowDayName.setEnabled(false);
            menuItemShowDate.setEnabled(false);
            menuItemShowHolyDayName.setEnabled(false);
            menuItemShowDescription.setEnabled(false);
        }
        
        return oPopupMenu;    
    }
    
    
    
    
    private class MenuItem_ShowShort_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {      
            m_oDayNamesPanel.setShowShort(((JCheckBoxMenuItem)(e.getSource())).isSelected());
            setDayNamesPanelHeight();
            saveDayNameSettings();      
        }    
    }
    
    private class MenuItem_ShowDayName_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            m_oDayNamesPanel.setShowDayName(((JCheckBoxMenuItem)(e.getSource())).isSelected());
            setDayNamesPanelHeight();
            saveDayNameSettings();      
        }    
    }
    
    
    private class MenuItem_Magnetraster_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {      
            getAppointmentDisplayManager().fireSetGridActive(((JCheckBoxMenuItem)(e.getSource())).isSelected(), null);
        }    
    }
    
    
    private class MenuItem_ShowDate_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            m_oDayNamesPanel.setShowDate(((JCheckBoxMenuItem)(e.getSource())).isSelected());
            setDayNamesPanelHeight();
            saveDayNameSettings();      
        }    
    }
    
    private class MenuItem_ShowName_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            m_oDayNamesPanel.setShowName(((JCheckBoxMenuItem)(e.getSource())).isSelected());
            setDayNamesPanelHeight();
            saveDayNameSettings();      
        }    
    }
    
    private class MenuItem_ShowDescription_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            m_oDayNamesPanel.setShowDescription(((JCheckBoxMenuItem)(e.getSource())).isSelected());
            setDayNamesPanelHeight();
            saveDayNameSettings();      
        }    
    }
    
    
    class DayNamePopupListener extends MouseAdapter 
    {
        private JPopupMenu m_oPopupMenu;
        
        public void mousePressed(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        public void mouseReleased(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) 
        {
            if (e.isPopupTrigger()) 
            {
                m_oPopUpX = e.getX();
                m_oPopUpY = e.getY();
                m_oPopupMenu = createDayNamePopUpMenu();
                m_oPopupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }
    
    
    public void setFirstDay(ScheduleDate date)
    {
        m_oFirstDay = new ScheduleDate(date);
        m_oGridLinePanel.setFirstDay(m_oFirstDay); 
        m_oDayNamesPanel.setFirstDay(m_oFirstDay);    
    }
    
    public void setNumDays(int numdays)
    {
        m_iNumDays = numdays;
        m_oGridLinePanel.setNumDays(numdays);
        m_oDayNamesPanel.setNumDays(numdays);
    }
    
    public int getNumDays()
    {
        return m_iNumDays;
    }
    
    public ScheduleDate getFirstDay()
    {
        return new ScheduleDate(m_oFirstDay);
    }
    
    private class VerticalScrollListener implements AdjustmentListener
    {
        public void adjustmentValueChanged(AdjustmentEvent e)
        {
            // if (e.getValueIsAdjusting()) return; - removed to allow scrolling in 1.5 simon
            m_oHourScrollPane.getVerticalScrollBar().setValue(e.getValue());
        }
    }
    
    private class HorizontalScrollListener implements AdjustmentListener
    {
        public void adjustmentValueChanged(AdjustmentEvent e)
        {
            // if (e.getValueIsAdjusting()) return;- removed to allow scrolling in 1.5 simon
            m_oDayScrollPane.getHorizontalScrollBar().setValue(e.getValue());
            //System.out.println("ScrollValue: " + e.getValue());
        }
    }
    
    private class MainPanelResizeListener implements ComponentListener
    {
        public void componentResized(ComponentEvent e)
        {
            // Expand Panel if possible ....
            boolean needRepaint = false;
            int newHeight = m_oGridScrollPane.getHeight();
            int newWidth  = m_oGridScrollPane.getWidth();
            
            // Did the Panel's width change?
            if(m_oMainPanel.getWidth() != m_oScheduleData.maxPosX ){
                needRepaint = true;
                if (getNumDays() == 1){ 
                    // Always use full width for One Day Display
                    m_oScheduleData.maxPosX = m_oGridScrollPane.getWidth();
                    m_oScheduleData.dayWidth = m_oScheduleData.maxPosX / getNumDays();
                }else{
                    if (m_oGridScrollPane.getWidth() > m_oScheduleData.minSizeX - m_oHourNamesPanel.getWidth() ){
                        // Expand to new Size
                        m_oScheduleData.maxPosX = m_oGridScrollPane.getWidth();
                        m_oScheduleData.dayWidth = m_oScheduleData.maxPosX / getNumDays();
                        newWidth = m_oGridScrollPane.getWidth();
                    }else{
                        // Keep minSize
                        m_oScheduleData.maxPosX = m_oScheduleData.minSizeX;
                        m_oScheduleData.dayWidth = m_oScheduleData.minDayWidth;
                        newWidth = m_oScheduleData.minDayWidth * m_iNumDays;
                    }
                }
            }
            // Did the Panel's height change? 
            if(m_oMainPanel.getHeight() != m_oScheduleData.maxPosY ){
                needRepaint = true;
                if (m_oGridScrollPane.getHeight() > m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours){
                    // Expand to new Size
                    m_oScheduleData.maxPosY = m_oGridScrollPane.getHeight();
                    m_oScheduleData.hourHeight = m_oScheduleData.maxPosY / m_oScheduleData.numberOfHours;
                    newHeight = m_oMainPanel.getHeight();
                }else{
                    // Keep minSize
                    m_oScheduleData.maxPosY = m_oScheduleData.minSizeY;
                    m_oScheduleData.hourHeight = m_oScheduleData.minHourHeight;
                    newHeight = m_oScheduleData.minSizeY;
                }
            }
            
            if (needRepaint){
                resolveDayConflicts();
                setLayerPaneSize();
                setDayNamesPanelHeight();
                m_oGridLinePanel.setSize(newWidth,newHeight);
            }
            m_oGridScrollPane.getHorizontalScrollBar().setVisible((m_oGridScrollPane.getWidth() <  m_oScheduleData.minDayWidth * m_iNumDays));
            m_oGridScrollPane.getVerticalScrollBar().setVisible((m_oGridScrollPane.getHeight() <m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours));
            
        }
        public void componentMoved(ComponentEvent e) {}
        public void componentShown(ComponentEvent e) {}
        public void componentHidden(ComponentEvent e) {}           
    }
    
    
    public void addScheduleEntryListener(ScheduleEntryListener oScheduleEntryListener)
    {    
        m_oScheduleEntryListeners.add(oScheduleEntryListener);    
    }
    
    public void removeScheduleEntryListener(ScheduleEntryListener oScheduleEntryListener)
    {
        m_oScheduleEntryListeners.remove(oScheduleEntryListener);
    }
    
    public ScheduleDate computeStartDate(ScheduleEntryPanel entry, Point point)
    {
        int day = getDayOfPos(entry, point);
        ScheduleDate startdate = getDateOfDay(day);
        int second = getSecondOfPos(point);
        second = snapToSecondGrid(second);
        startdate.setScheduleSecondOfDay(second);
        return(startdate);    
    }
    
    public ScheduleDate computeEndDate(ScheduleEntryPanel entry, int size)
    {
        int seconds = getSecondOfPos(size);
        ScheduleDate startdate = entry.getStartDate();
        ScheduleDate enddate = startdate.getDateWithAddedSeconds(seconds);
        return(enddate);
    }
    
    
    public void fireIsMoving(ScheduleEntryPanel entry, boolean ismoving, int x, int y)
    {
        logger.log(Level.FINE, "fireIsMoving : " +  entry.getID());
        Point point = new Point(x,y);
        ScheduleDate startdate = computeStartDate(entry, point);
        entry.setStartDate(startdate);        
        
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).isMoving(entry, ismoving, x, y);
        }
    }
    
    public void fireIsResizing(ScheduleEntryPanel entry, boolean isresizing, int size)
    {
        // System.out.println("fireISresizing : " +  entry.getID());
        ScheduleDate enddate = computeEndDate(entry, size);
        entry.setEndDate(enddate);
        
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).isResizing(entry, isresizing, size);
        }
    }
    
    public void fireStartMoving(ScheduleEntryPanel entry, int x, int y)
    {
        m_oMovingStartPoint = new Point(x,y);
        logger.log(Level.FINE,"fireStartMoving : " +  entry.getID());
        
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).startMoving(entry, x, y);
        }
    }        
    
    public void fireStartResizing(ScheduleEntryPanel entry, int size)
    {
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).startResizing(entry, size);
        }
    }
    
    public void fireStopMoving(ScheduleEntryPanel entry, int x, int y)
    {
        
        logger.log(Level.FINE,"fireStopMoving : " +  entry.getID());
        
        ScheduleEntryPanel start_sep = SEPRegistry.getInstance().getFirstSEP(entry.getAppointment().getId());
        ScheduleEntryPanel end_sep = SEPRegistry.getInstance().getLastSEP(entry.getAppointment().getId());
        
        int duration = 0;
        try {
            duration = (int) (start_sep.getAppointment().getEnd().getTime() - start_sep.getAppointment().getStart().getTime()) / 1000;
        } catch (ContactDBException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        end_sep.setEndDate(start_sep.getStartDate().getDateWithAddedSeconds(duration));
        
        // Avoid Appointment/ToDo Ending on 0:00 -> change to 23:59:59
        if (end_sep.getEndDate().getDate().getHours() == 0 && end_sep.getEndDate().getDate().getMinutes() == 0 ){
            end_sep.setEndDate(end_sep.getEndDate().getFirstSecondOfDay().getDateWithAddedSeconds(-1));
        }
        
        try
        {
            start_sep.getAppointment().setStart(start_sep.getStartDate().getDate());
            start_sep.getAppointment().setEnd(end_sep.getEndDate().getDate());
        } 
        catch (ContactDBException e) {}
        
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).stopMoving(entry, x, y);
        }
        List allSEPS = SEPRegistry.getInstance().getSEPSforAppointmentID(entry.getAppointment().getId());
        Iterator iter = allSEPS.iterator();
        while (iter.hasNext()){
            ScheduleEntryPanel sep = (ScheduleEntryPanel) iter.next();
            sep.revalidate();
        }
        resolveDayConflicts();
    }
    
    public void fireStopResizing(ScheduleEntryPanel entry, int size)
    {
        ScheduleDate enddate = computeEndDate(entry, size);
        entry.setEndDate(enddate);
        try
        {
            entry.getAppointment().setEnd(enddate.getDate());
        } 
        catch (ContactDBException e)
        {
            e.printStackTrace();
        }
        
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).stopResizing(entry, size);
        }    
        entry.revalidate();
        resolveDayConflicts();
    }
    
    public void fireDoubleClicked(ScheduleEntryPanel entry) 
    {
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).doubleClicked(entry);
        }    
    }
    
    public void fireEditSelected(ScheduleEntryPanel entry)
    {
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).editSelected(entry);
        }    
    }
    
    public void fireDeleteSelected(ScheduleEntryPanel entry)
    {    
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ScheduleEntryListener sel = ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i)));
            sel.deleteSelected(entry);
        }    
        
        if (entry instanceof ScheduleEntry) removeScheduleEntry((ScheduleEntry)entry);
        
        fireRemovedEntry(entry);
    }
    
    public void fireAddedEntry(ScheduleEntryPanel entry, ScheduleDate StartDate,ScheduleDate EndDate, Integer calendarID)
    {
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).addedEntry(entry,StartDate,EndDate,calendarID);
        }    
    }
    
    public void fireRemovedEntry(ScheduleEntryPanel entry)
    {
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).removedEntry(entry);
        }    
    }
    
    public void fireChangedValues(ScheduleEntryPanel entry)
    {
        for(int i=0; i<(m_oScheduleEntryListeners.size()); i++)
        {
            ((ScheduleEntryListener)(m_oScheduleEntryListeners.get(i))).changedValues(entry);
        }    
    }
    
    public void resolveDayConflicts()//ScheduleEntryPanel changedentry)
    {
        List entries = SEPRegistry.getInstance().getSEPSforSchedulePaneliD(m_UniqueID);
        //Collections.sort(entries, new DateComparator()); 
        if (entries != null){
            for(int i=1; i<8; i++) {
                List entriestochange = new ArrayList();
                //First we need every entry who starts at this date and or ends at this entry's start and or enddate or inbetween
                entriestochange.addAll(filterEntriesByWeekDay(i, entries));
                //Count needed stacking depth
                Collections.sort(entriestochange, new DateComparator());
                int nlevel = countConflictLevel(entriestochange);
                
                int newlevel = alignEntries(entriestochange, nlevel);
                //if(newlevel!=nlevel)alignEntries(entriestochange, newlevel);
                int newlevel2 = reAlignEntries(entriestochange, newlevel);
                fixEntriesPerColumns(entriestochange, newlevel2);
            }
        }
    }
    
    
    
    
    
    //  private void resolveConflicts(ScheduleEntryPanel entry)
    //  {
    //    List list = getConflictingScheduleEntries(entry);
    //    
    //    list.add(entry);
    //    Collections.sort(list, new DateComparator()); 
    //
    //    int width = (m_oScheduleData.m_iDayWidth) / list.size();
    //
    //    for(int n=0; n<(list.size()); n++)
    //    {
    //      ScheduleEntryPanel e = (ScheduleEntryPanel)(list.get(n));
    //      e.updateSizeAndPosition();
    //      Rectangle rect = e.getBounds();
    //      rect.width = width;
    //      rect.x += (n * width);
    //      e.setBounds(rect);
    //      e.revalidate();
    //    }
    //  }
    
    
    //  private List resetConflicts(ScheduleEntryPanel entry)
    //  {
    //    List list = getConflictingScheduleEntries(entry);
    //    
    //    //list.add(entry);
    //    Collections.sort(list, new DateComparator()); 
    //
    //    int width = (m_oScheduleData.m_iDayWidth);
    //
    //    for(int n=0; n<(list.size()); n++)
    //    {
    //      ScheduleEntryPanel e = (ScheduleEntryPanel)(list.get(n));      
    //      e.updateSizeAndPosition();
    //      e.revalidate();
    //    }
    //    return(list);
    //  }
    
    
    /**
     * @param entriestochange
     * @param maxlevel
     */
    private void fixEntriesPerColumns(List entriestochange, int maxlevel) {
        Iterator it = entriestochange.iterator();
        while(it.hasNext()) {
            ScheduleEntryPanel sep = (ScheduleEntryPanel) it.next();
            int numcolumns = sep.getNumColumns();
            int columnpos = sep.getColumnPos();
            List conflictentries = getConflictingScheduleEntries(sep);
            Iterator it2 = conflictentries.iterator();
            while(it2.hasNext()) {
                ScheduleEntryPanel conflict = (ScheduleEntryPanel) it2.next();
                if(conflict.getNumColumns()>numcolumns) {
                    numcolumns = conflict.getNumColumns();
                }
            }
            if(numcolumns <= columnpos) {numcolumns = columnpos+1;}
            if(numcolumns > maxlevel) {numcolumns = maxlevel;}
            sep.setScheduleWidth(columnpos, numcolumns);
        }
    }
    
    
    /**
     * @param entriestochange
     * @param newlevel
     */
    private int reAlignEntries(List entriestochange, int newlevel) {
        //First we need an array to store when column is free
        int columnfree[] = new int[newlevel];
        for(int i=0;i<newlevel;i++) {
            columnfree[i]=0;
        }
        Iterator it = entriestochange.iterator();
        int maxpos = 0;
        while(it.hasNext()) {
            ScheduleEntryPanel sep = (ScheduleEntryPanel) it.next();
            int pos = 0;
            //choose column
            for(int i=0;i<newlevel;i++) {
                if(sep.getStartDate().getScheduleSecondOfDay()>=columnfree[i]) {
                    pos = i;
                    if(maxpos<pos) {maxpos=pos;}
                    break;
                }
            }
            int thiscolumns = newlevel;
            int thismaxconflicts = getConflictingScheduleEntries(sep).size()+1;
            if((thismaxconflicts < thiscolumns)&&allcolumnsfree(columnfree, sep.getStartDate().getScheduleSecondOfDay())) {
                thiscolumns=thismaxconflicts;
            }
            sep.setScheduleWidth(pos, thiscolumns);
            columnfree[sep.getColumnPos()]=sep.getEndDate().getScheduleSecondOfDay();
            
        }
        return maxpos+1;
    }
    
    
    /**
     * @param columns
     * @param startdate
     * @return
     */
    private boolean allcolumnsfree(int[] columns, int startdate) {
        for(int i=0;i<columns.length;i++) {
            if(startdate < columns[i]) {
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * @param entries
     * @param nlevel
     * @return
     */
    private int alignEntries(List entries, int nlevel) {
        int newlevel = 0;
        List alignentries = new ArrayList(entries);
        Collections.sort(alignentries, new DurationComparator());
        //Align entries
        int pos = 0;
        int size = alignentries.size();
        for(int i=0; i<size; i++) {
            if(pos<0) {
                pos = 0;
            }
            ScheduleEntryPanel sep = (ScheduleEntryPanel) alignentries.remove(0);
            //sep.setScheduleWidth(pos, nlevel);
            //if there is any upcoming entry which conflicts, increase level
            if(countConflictLevel(entries)>(pos+1)) {
                if(countConflictLevel(alignentries)>(pos+1)) {
                    pos++;
                    if(pos>newlevel)newlevel = pos;
                }
            }else {
                pos--;
            }
        }
        return newlevel+1;
    }
    
    
    /**
     * @param entries
     * @return
     */
    private int countConflictLevel(List entries) {
        //Z�hle, wie viele Ebenen �berhaupt n�tig sind...
        int nlevel = 1;
        Iterator it = entries.iterator();
        while(it.hasNext()) {
            int aktnlevel = 1;
            ScheduleEntryPanel entry = (ScheduleEntryPanel) it.next();
            aktnlevel = getConflictingScheduleEntries(entry).size()+1;
            //Wenn aktuelle Schachtelungstiefe gr��er als vorher, update
            if(aktnlevel>nlevel) {
                nlevel = aktnlevel;
            }
        }
        return nlevel;
    }
    
    
    /**
     * @param conflict Termin, der Kollisionen hervorrufen k�nnte
     * @param entries die Eintr�ge, aus denen gefiltert werden soll
     * @return Alle Eintr�ge aus entries die am ScheduleDayOfYear day beginnen
     */
    private List filterEntriesByEntry(ScheduleEntryPanel conflict, List entries) {
        List filtered = new ArrayList();
        Iterator it = entries.iterator();
        boolean add = false;
        int _startday = conflict.getStartDate().getScheduleDayOfYear();
        int _endday = conflict.getEndDate().getScheduleDayOfYear();
        while(it.hasNext()) {
            ScheduleEntryPanel entry = (ScheduleEntryPanel) it.next(); 
            add = false;
            int startday = entry.getStartDate().getScheduleDayOfYear();
            int endday = entry.getEndDate().getScheduleDayOfYear();
            //Teste, ob Starttag w�hrend des laufenden Termins liegt
            if((_startday <= startday) && (startday <= _endday)) {
                add = true;
            }
            //Dasgleiche mit dem Enddatum
            if(!add && (_startday <= endday) && (endday <= _endday)) {
                add = true;
            }
            if(add) {
                filtered.add(entry);
            }
        }
        return filtered;
    }
    
    private List filterEntriesByWeekDay(int weekday, List entries) {
        List filtered = new ArrayList();
        Iterator it = entries.iterator();
        boolean add = false;
        while(it.hasNext()) {
            ScheduleEntryPanel entry = (ScheduleEntryPanel) it.next(); 
            add = false;
            int startday = entry.getStartDate().getScheduleDayOfWeek();
            int endday = entry.getEndDate().getScheduleDayOfWeek();
            //Teste, ob Starttag w�hrend des laufenden Termins liegt
            if((startday <= weekday) && (weekday <= endday)) {
                add = true;
            }
            if(add) {
                filtered.add(entry);
            }
        }
        return filtered;
    }
    
    
    private class DateComparator implements Comparator
    {
        public int compare(Object arg0, Object arg1)
        {      
            if (((ScheduleEntryPanel)arg0).getStartDate().before(((ScheduleEntryPanel)arg1).getStartDate())) return -1;
            else if (((ScheduleEntryPanel)arg0).getStartDate().after(((ScheduleEntryPanel)arg1).getStartDate())) return 1;
            return 0;
        }
    }
    
    private class DurationComparator implements Comparator
    {
        public int compare(Object arg0, Object arg1) {
            if(((ScheduleEntryPanel)arg0).getDuration()>(((ScheduleEntryPanel)arg1).getDuration()))return -1;
            if(((ScheduleEntryPanel)arg0).getDuration()<(((ScheduleEntryPanel)arg1).getDuration()))return 1;
            return 0;
        }
        
    }
    // public methods  
    
    public void scrollToHour(int hour)
    {  
        int pos = ((m_oGridScrollPane.getVerticalScrollBar().getMaximum()  -  m_oGridScrollPane.getVerticalScrollBar().getMinimum()) / m_oScheduleData.numberOfHours) * hour;
        m_oHourScrollPane.getVerticalScrollBar().setValue(pos);
        m_oGridScrollPane.getVerticalScrollBar().setValue(pos);
    }
    
    public void scrollToDay(int day)
    {  
        int pos = ((m_oGridScrollPane.getHorizontalScrollBar().getMaximum()  -  m_oGridScrollPane.getHorizontalScrollBar().getMinimum()) / m_iNumDays) * day;
        m_oDayScrollPane.getHorizontalScrollBar().setValue(pos);
        m_oGridScrollPane.getHorizontalScrollBar().setValue(pos);
    }
    
    
    public ScheduleDate getDateOfDay(int day)
    {
        ScheduleDate firstday = m_oDayNamesPanel.getFirstDay();
        ScheduleDate date = firstday.getDateByOffset(day);
        return date;
    }
    
    public int getDayOfPos(ScheduleEntryPanel se, Point pos)
    {
        int posx = pos.x + (se.getWidth() / 2);
        for(int d = 0; d<(m_iNumDays); d++)
        {
            int x1 = (d * m_oScheduleData.dayWidth);        
            int x2 = ((d + 1)  * m_oScheduleData.dayWidth);        
            if ((posx >= x1) && (posx < x2)) return d;      
        }
        
        return(-1);              
    }
    
    
    public int getDayOfDirectPos(Point pos)
    {
        for(int d = 0; d<(m_iNumDays); d++)
        {
            int x1 = (d * m_oScheduleData.dayWidth);        
            int x2 = ((d + 1)  * m_oScheduleData.dayWidth);        
            if ((pos.x >= x1) && (pos.x < x2)) return d;      
        }
        
        return(-1);              
    }
    
    
    public int getSecondOfPos(Point pos)
    {
        return m_oHourNamesPanel.getStartSecondForPixel(pos.y);
    }
    
    public int getSecondOfPos(int y)
    {
        return m_oHourNamesPanel.getStartSecondForPixel(y);
    }
    
    public int getPixelForSecond(int second)
    {
        return m_oHourNamesPanel.getPixelForSecond(second);    
    }
    
    public ScheduleData getScheduleData()
    {
        return m_oScheduleData;
    }
    
    public void addScheduleEntrySilent(ScheduleEntryPanel se)
    {
        m_oLayerPane.add(se, new Integer(2));
        DragListener dl = new DragListener(se, this);
        se.setDragListener(dl);
        // se.updateSizeAndPosition();
        // resolveDayConflicts(se);      
        se.realized();
    }
    
    private void removeScheduleEntry(ScheduleEntry se)
    {
        List allSeps = SEPRegistry.getInstance().getSEPSforAppointmentID(se.getAppointment().getId());
        while( allSeps.size() > 0){
            ScheduleEntryPanel SEP = (ScheduleEntryPanel) allSeps.get(0);
            m_oLayerPane.remove(SEP);
            SEP.removeDragListener();
            SEPRegistry.getInstance().removeSEP(SEP,m_oSchedulePanel.getID());
            allSeps.remove(SEP);
        }
        try{
            if (! se.getAppointment().isTemporary())PersistenceManager.delete(se.getAppointment());
            AppointmentImpl.flushCache();
        }catch (ContactDBException e){
            e.printStackTrace();
        }
        resolveDayConflicts() ;//se);
        repaintPanel();
    }
    
    
    public void removeAllScheduleEntries()
    {
        List entrylist = SEPRegistry.getInstance().getSEPSforSchedulePaneliD(m_UniqueID);
        if (entrylist != null){
            Iterator entriesiterator = entrylist.iterator();      
            while(entriesiterator.hasNext())
            {
                ScheduleEntryPanel se = (ScheduleEntryPanel)(entriesiterator.next());
                se.removeDragListener();
                m_oLayerPane.remove(se);    
            }
            SEPRegistry.getInstance().removeAllSEPs(m_UniqueID);
            repaintPanel();
        }
    }
    
    public void moveScheduleEntryToTop(ScheduleEntryPanel se)
    {
        m_oLayerPane.setLayer(se, 100);      
    }
    
    public void moveScheduleEntryToNormal(ScheduleEntryPanel se)
    {
        m_oLayerPane.setLayer(se, 2);      
    }    
    
    public List getConflictingScheduleEntries(ScheduleEntryPanel se)
    {
        List list = new ArrayList();
        
        for(int i=0; i<(SEPRegistry.getInstance().getSEPSforSchedulePaneliD(m_UniqueID).size()); i++)
        {
            ScheduleEntryPanel tse = (ScheduleEntryPanel)(SEPRegistry.getInstance().getSEPSforSchedulePaneliD(m_UniqueID).get(i));
            if (!(tse.equals(se)))
            {
                if ((tse.getStartDate().after(se.getStartDate())) && (tse.getStartDate().before(se.getEndDate()))) list.add(tse);
                else if ((tse.getStartDate().before(se.getStartDate())) && (tse.getEndDate().after(se.getStartDate()))) list.add(tse);
                else if ((tse.getStartDate().after(se.getStartDate())) && (tse.getStartDate().before(se.getEndDate()))) list.add(tse);
                else if ((tse.getStartDate().before(se.getStartDate())) && (tse.getEndDate().after(se.getStartDate()))) list.add(tse);
                else if ((tse.getStartDate().equals(se.getStartDate()))) list.add(tse);
            }
        }    
        return list;
    }
    
    
    private class DayNamesPanelMouseListener implements MouseListener
    {
        public void mouseClicked(MouseEvent e)
        {
            int daynum = m_oDayNamesPanel.getDayNumOfXPos(e.getX());
            if (daynum >= 0)
            {  
                
                logger.log(Level.FINE,"DayNamesPanelMouseListener::mouseClicked() daynum=" + daynum);
            }
        }
        
        public void mouseEntered(MouseEvent e) {}
        public void mouseExited(MouseEvent e) {}
        public void mousePressed(MouseEvent e) {}
        public void mouseReleased(MouseEvent e) {}
    }
    
    private class HourNamesPanelMouseListener implements MouseListener
    {
        public void mouseClicked(MouseEvent e)
        {
            int hour = m_oHourNamesPanel.getHourOfYPos(e.getY());
            if (hour >= 0)
            {  
                
                logger.log(Level.FINE,"HourNamesPanelMouseListener::mouseClicked() hour=" + hour);
            }
        }
        
        public void mouseEntered(MouseEvent e) {}
        public void mouseExited(MouseEvent e) {}
        public void mousePressed(MouseEvent e) {}
        public void mouseReleased(MouseEvent e) {}
    }
    
    
}
