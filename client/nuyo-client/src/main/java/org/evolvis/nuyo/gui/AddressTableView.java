/*
 * Created on 09.11.2004
 *
 * 
 */
package org.evolvis.nuyo.gui;

import javax.swing.JComponent;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;


/**
 * Ein Interface f�r Address-Tabellen im ContactClient
 * 
 * @author niko
 */
public interface AddressTableView
{
  
  /**
   * Change the address set for viewing in the table
   */
  public void setAddressList(Addresses addressList);

  /**
   * Method getDragComponent. liefert das JComponent 
   * das als DragComponent benutzt werden soll  
   */    
  public JComponent getDragComponent();
  
  /**
   * Method discardTableBuffer. erkl�rt alle Daten 
   * in der Tabelle als ung�ltig
   */    
  public void discardTableBuffer();  

  
  /**
   * Method forceRefresh. erzwingt einen vollst�ndigen 
   * Refresh der Tabelle
   */    
  public void forceRefresh();
  
  /**
   * Method setNavigationCounter. Erm�glicht das Setzen des aktuellen
   * Datensatzes und der Anzahl angezeigter Datens�tze (Navigationsleiste)
   * @param currentindex (Der Index des aktuell angezeigten Datensatzes)
   * @param numentries (Die Anzahl aller zur Navigation bereitstehenden Datens�tze)
   */
  public void setNavigationCounter(int currentindex, int numentries);

  
  /**
   * Method selectAddress. Erm�glicht das Selektieren 
   * einer Adresse
   * @param index (Der Index des zu selektierenden Datensatzes)
   */  
  public void selectAddress(int index);

  
  /**
   * Method addressChanged. teilt der Tabelle mit das 
   * sich eine Adresse ge�ndert hat
   * @param address (Die Adresse die sich ge�ndert hat)
   * @param index (Der Index des Datensatzes)
   */  
  public void addressChanged(Address address, int index);
  
  
  /**
   * Method setColumnPositions. setzt die Spaltenpositionen
   * @param posistr (als String codierte Spaltenpositionen)
   */      
  public void setColumnPositions(String posistr);
  
  
  /**
   * Method getColumnPositions. liefert die Spaltenpositionen
   * @return posistr (als String codierte Spaltenpositionen)
   */        
  public String getColumnPositions();
  
  
  /**
   * Method isPreviewRowLoaded. Ist der Preview f�r eine spezielle Zeile schon geladen?
   * @return true wenn bereits geladen
   */        
  public boolean isPreviewRowLoaded(int row); 
  
  
  /**
   * Method isPreviewSortedAscending. Ist der Preview aufsteigend sortiert?
   * @return true wenn aufsteigend (A->Z) geladen (ist Default)
   */        
  public boolean isPreviewSortedAscending();

  /**
   * Method isTableReady. Ist die Tabelle g�ltig?
   * @return true wenn g�ltig
   */        
  public boolean isTableReady();
  
  /**
   * Method getNumberOfRows. Ermittelt die Anzahl der Preview-Zeilen
   * @return die Anzahl der Preview-Zeilen
   */        
  public int getNumberOfRows();
}
