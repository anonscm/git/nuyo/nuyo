/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.util.List;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.remote.Method;

import de.tarent.commons.utils.TaskManager.Context;
import de.tarent.commons.utils.TaskManager.Task;

/**
 * 
 * A Task executing a Mass-Mail-Dispatch on the server
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
class SerialMailTask implements Task
{
	private Integer actionID;
	private String messageSubject;
	private String messageBody;
	private String mailFrom;
	private List attachmentNames;
	private List attachmentData;
	private boolean delayedDispatch;
	private boolean copyToSender;
	private final static TarentLogger logger = new TarentLogger(SerialMailTask.class);
	
	public final static String PARAM_ACTIONID = "actionId";
	public final static String PARAM_MAILSUBJECT = "MailSubject";
	public final static String PARAM_MAILBODY = "MailBody";
	public final static String PARAM_MAILFROM = "MailFrom";
	public final static String PARAM_ATTACHMENT_NAMES = "AttachmentNames";
	public final static String PARAM_ATTACHMENT_DATAS = "AttachmentDatas";
	public final static String PARAM_SEND_DELAYED = "SendDelayed";
	public final static String PARAM_COPY_TO_SENDER = "CopyToSender";
	
	
	/**
	 * 
	 * Initializes a SerialMailTask using all available parameters
	 * 
	 * @param pActionID - The ID of the dispatch-job on the server. Specifies the address-list to which the email will be sent
	 * @param pMessageSubject - The subject of the email
	 * @param pMessageBody - The body of the email
	 * @param pMailFrom - The sender of the mail
	 * @param pAttachmentNames - A list containing the file-names of the attachments
	 * @param pAttachmentData - A list containing the attachment-data in Byte-Arrays
	 * @param pDelayedDispatch - if true the mail-transfer is processed later, otherwise immediately
	 * @param pCopyToSender - if an email with the template as specified by 'pMessageBody' should also be sent to the address specified by 'pMailFrom'
	 */
	
	public SerialMailTask(Integer pActionID, String pMessageSubject, String pMessageBody, String pMailFrom, List pAttachmentNames, List pAttachmentData, boolean pDelayedDispatch, boolean pCopyToSender)
	{
		actionID = pActionID;
		messageSubject = pMessageSubject;
		messageBody = pMessageBody;
		mailFrom = pMailFrom;
		attachmentNames = pAttachmentNames;
		attachmentData = pAttachmentData;
		delayedDispatch = pDelayedDispatch;
		copyToSender = pCopyToSender;
	}
	
	/**
	 * 
	 * Cancelling a SerialMailTask is (currently) not possible 
	 * 
	 */
	
	public void cancel()
	{
		
	}
	
	/**
	 * 
	 * Executes the SerialMailTask
	 *  
	 */

	public void run(Context pContext)
	{
		logger.info("Transferring mail \""+messageSubject+"\" to server. Action-ID is "+actionID+". Mail will be sent "+ (delayedDispatch ? "immediately" : "later"));
		
		pContext.setActivityDescription(Messages.getString("GUI_EMAIL_TRANSFER_PROGRESS_DIALOG_PREPARING_TRANSFER"));
		Method scheduleMail = new Method("scheduleMail");
		
		scheduleMail.addParam(PARAM_ACTIONID, actionID);
		scheduleMail.addParam(PARAM_MAILSUBJECT, messageSubject);
		scheduleMail.addParam(PARAM_MAILBODY, messageBody);
		scheduleMail.addParam(PARAM_MAILFROM, mailFrom);
		scheduleMail.add(PARAM_ATTACHMENT_NAMES, attachmentNames);
		scheduleMail.add(PARAM_ATTACHMENT_DATAS, attachmentData);
		scheduleMail.add(PARAM_SEND_DELAYED, new Boolean(delayedDispatch));
		scheduleMail.add(PARAM_COPY_TO_SENDER, new Boolean(copyToSender));
		
		try
		{
			pContext.setActivityDescription(Messages.getString("GUI_EMAIL_TRANSFER_PROGRESS_DIALOG_TRANSFER"));
			scheduleMail.invoke();
		} catch (ContactDBException e)
		{            
            if (e.getOctopusException().getCause().getCause() instanceof java.net.SocketTimeoutException) {
                logger.warning(Messages.getString("GUI_EMAIL_TRANSFER_PROGRESS_DIALOG_TRANSFER_TIMEDOUT"), e);
            } else {
                logger.warning(Messages.getString("GUI_EMAIL_TRANSFER_PROGRESS_DIALOG_TRANSFER_FAILED"), e);
            }
		}
	}

}
