/*
 * Created on 08.07.2004
 *
 */
package org.evolvis.nuyo.db;

/**
 * @author simon
 *
 */
public class MailingListWrapper {
	
	private Object m_Key;
	private SubCategory m_MailingList;
	
	
	public MailingListWrapper(Object Key, SubCategory Value){
		this.m_Key = Key;
		this.m_MailingList = Value;
	}
	
	public SubCategory getMailingList(){
		return m_MailingList;
	}
	public Object getKey(){
		return m_Key;
	}
}
