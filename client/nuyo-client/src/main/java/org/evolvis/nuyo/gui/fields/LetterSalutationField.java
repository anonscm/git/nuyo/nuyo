/*
 * Created on 14.04.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.logging.Logger;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetCheckBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.groupware.generator.LetterSalutationGenerator;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author nils
 *
 */
public class LetterSalutationField extends ContactAddressField
{  
  private static Logger logger = Logger.getLogger(PictureField.class.getName());
  
 // private JLabel m_oInfoLabel;
  HashMap adressData = new HashMap();
  
  private TarentWidgetTextField textfield;
  private TarentWidgetCheckBox checkbox;
  
  private EntryLayoutHelper m_oPanel = null;
  private TarentWidgetLabel m_oLabel; 
  
  public String getFieldName()
  {
    return "LetterSalutaion";    
  }
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public String getFieldDescription()
  {
    return "Ein Feld zur Anzeige der Briefanrede";
  }
  
  
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if(m_oPanel == null) m_oPanel = createPanel(widgetFlags);
    return m_oPanel;
  }
  
  
  public void postFinalRealize()
  {    
  }
  
  public void setData(PluginData data)
  {
  	adressData.put(AddressKeys.ANREDE, data.get(AddressKeys.ANREDE));
  	adressData.put(AddressKeys.VORNAME, data.get(AddressKeys.VORNAME));
  	adressData.put(AddressKeys.NACHNAME, data.get(AddressKeys.NACHNAME));
  	adressData.put(AddressKeys.TITEL, data.get(AddressKeys.TITEL));
  	adressData.put(AddressKeys.AKADTITEL, data.get(AddressKeys.AKADTITEL));
  	adressData.put(AddressKeys.HERRFRAU, data.get(AddressKeys.HERRFRAU));
  	
  	adressData.put(AddressKeys.LETTERSALUTATION_AUTO, data.get(AddressKeys.LETTERSALUTATION_AUTO));
  	adressData.put(AddressKeys.LETTERSALUTATION, data.get(AddressKeys.LETTERSALUTATION));
    Boolean ischecked = (Boolean) data.get(AddressKeys.LETTERSALUTATION_AUTO);
  	
  	if (ischecked != null){
  		checkbox.setSelected(ischecked.booleanValue());  		
  	}
  	else{
  		checkbox.setSelected(true);  		
  	}
  	
  	refreshTextField();
  	//textfield.setEditable(ischecked.booleanValue());
  }

  Object m_oAdressNr = null;
  
    
  public void getData(PluginData data)
  {
  	
  	data.set(AddressKeys.LETTERSALUTATION_AUTO, new Boolean(checkbox.isSelected()));
  	
  	
  	if (!("").equals((String)textfield.getData())){
  		data.set(AddressKeys.LETTERSALUTATION, (String) textfield.getData());
  	}
  }

  public boolean isDirty(PluginData data)
  {
	  if (data.get(AddressKeys.ANREDE) != null) 
	      if (!(checkbox.isSelected() == ((Boolean) data.get(AddressKeys.LETTERSALUTATION_AUTO)).booleanValue())) 
	    	  return(true);
  
	  if (!(textfield.getText().equals("")) && data.get(AddressKeys.ANREDE) != null ) //$NON-NLS-1$
	      if (!(textfield.getText().equalsIgnoreCase(data.get(AddressKeys.ANREDE).toString()))) return(true);            
	      
    return false;
  }

  public void setEditable(boolean iseditable)
  {
  	checkbox.setEnabled(iseditable);
  	
  	if (checkbox.isSelected()){
  		textfield.setEditable(false);
  		// Sets the background color to white when disabled.
  	    textfield.setBackground(Color.WHITE);
  		refreshTextField();
  	}
  	else
  	{
  		textfield.setEditable(iseditable);
  	    textfield.setBackground(Color.WHITE);
  	}
    
    if (checkbox.isSelected()){
    	checkbox.setEnabled(iseditable);
    }
    
  }

  public String getKey()
  {
    return "LETTERSALUTATION";
  }
 
  private EntryLayoutHelper createPanel(String widgetFlags)
  {
    m_oLabel = new TarentWidgetLabel();
    
    textfield = new TarentWidgetTextField();
    textfield.setEnabled(true);
    textfield.setEditable(false);
       
    checkbox = new TarentWidgetCheckBox("generieren");
    checkbox.addActionListener(new CheckboxDisabled());
    checkbox.setEnabled(false);
    
    return new EntryLayoutHelper(new TarentWidgetInterface[]
                                                             {
new TarentWidgetLabel("Briefanrede:"), checkbox, textfield
                                                             }, widgetFlags);
  }

  
  private void refreshTextField(){
	  
  	if (!checkbox.isSelected() && adressData.get(AddressKeys.LETTERSALUTATION) != null && !("").equals(adressData.get(AddressKeys.LETTERSALUTATION))){
  		textfield.setData(adressData.get(AddressKeys.LETTERSALUTATION));
  		return;
  	}
  	
  	/**
  	 * Algorithmus zur automatischen Erstellung einer AnredeFormel
  	 * von Nils
  	 */
  	//System.out.println("Generiere Feldinhalt");
  	if (checkbox.isSelected()){
  		String anrede = LetterSalutationGenerator.generateLetterSalutation(
	  			(String)adressData.get(AddressKeys.ANREDE), 
	  			(String)adressData.get(AddressKeys.VORNAME), 
	  			(String)adressData.get(AddressKeys.NACHNAME),
	  			(String)adressData.get(AddressKeys.HERRFRAU),
	  			(String)adressData.get(AddressKeys.AKADTITEL),
	  			(String)adressData.get(AddressKeys.TITEL));
	  	
	  	textfield.setData(anrede);
  	}
  	
  	else textfield.setData("");
  }
/*
 * 
 *  
 private class IconMouseListener extends MouseAdapter {
 
 }
*/
  
  private class CheckboxDisabled implements ActionListener
  {
    public void actionPerformed(ActionEvent e){
    	
    	boolean checkBoxIsChecked = checkbox.isSelected();
    	
    	textfield.setEditable(!checkBoxIsChecked);
    	
    	if (checkBoxIsChecked)
    		refreshTextField();
	}
  }

public void setDoubleCheckSensitive(boolean issensitive) {
}
}
