/* $Id: CollapsingMenuPanel.java,v 1.1 2007/01/31 14:38:05 robert Exp $
 */
 
/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.plugins.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.evolvis.nuyo.controls.JIntegerField;

import de.tarent.commons.ui.EscapeDialog;


/**
 * This panel will be used to manage the collopsing menu buttons ({@link org.evolvis.nuyo.plugins.calendar.CollapsingMenu CollapsingMenu's}).<br> 
 * It will be placed on main window.<br>
 * 
 * @see org.evolvis.nuyo.gui.MainFrameExtStyle#createCollapsingMenu()
 * <p>
 * @author niko
 * @deprecated Calendar plugin uses it. Remove it when rewritten.
 */
class CollapsingMenuPanel extends JPanel implements CollapseListener
{
  private static final long serialVersionUID = 1L;
  private CollapsingMenuLayoutManagerVertical collapsingLayout;
  private JPanel toggleButtonPanel;
  private JPanel menusBorderedPanel;
  private JPanel contentPanel;
  private JPanel menusPanel;
  private JPanel instance;
  private JScrollPane contentScrollPane;
  
  private List panelCollapseListenersList;
  private List collapseListenersList;
  private ArrayList changeListeners;
  private ArrayList menusList;
  private Object parentFrame;

  private JButton toggleButton; 
  private boolean isToShowHandCursor;
  private ImageIcon expandIcon;
  private ImageIcon collapseIcon;
  private int scrollUnitSize = 20;
  private Cursor handCursor;
  private Color bgColor;

  
  /**
   * erzeugt ein neues MenuPanel.
   */  
  public CollapsingMenuPanel()
  {
    super();
    handCursor = new Cursor(Cursor.HAND_CURSOR);
    panelCollapseListenersList = new ArrayList();    
    collapseListenersList = new ArrayList();    
    changeListeners = new ArrayList();
    menusList = new ArrayList();
    instance = this;
    
    bgColor = Color.LIGHT_GRAY;
    
    setLayout(new BorderLayout());
    
    contentPanel = new JPanel(); 
    collapsingLayout = new CollapsingMenuLayoutManagerVertical(new BorderLayout());
    collapsingLayout.setCollapsed(false);
    contentPanel.setLayout(collapsingLayout);    
  
    menusBorderedPanel = new JPanel();    
    menusBorderedPanel.setLayout(new BorderLayout());

    contentScrollPane = new JScrollPane(menusBorderedPanel);
    contentScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    contentScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    contentScrollPane.getVerticalScrollBar().setUnitIncrement(scrollUnitSize);
    
    contentPanel.add(contentScrollPane, BorderLayout.CENTER);
    add(contentPanel, BorderLayout.CENTER);
        
    menusPanel = new JPanel();
    menusPanel.setLayout(new BoxLayout(menusPanel, BoxLayout.Y_AXIS));
    menusBorderedPanel.add(menusPanel, BorderLayout.NORTH);
    
    URL imagelocation = this.getClass().getResource("/de/tarent/controls/resources/");    
    try
    {          
      expandIcon   = new ImageIcon(new URL(imagelocation, "menuexpand.gif"));
      collapseIcon   = new ImageIcon(new URL(imagelocation, "menucollapse.gif"));
    }
    catch(MalformedURLException mue)
    {
      expandIcon  = null;
      collapseIcon  = null;
    }

    toggleButtonPanel = new JPanel();
    toggleButtonPanel.setLayout(new BorderLayout());
    
    toggleButton = new JButton(collapseIcon);
    toggleButton.setFocusPainted(false);
    toggleButton.setBorderPainted(false);
    toggleButton.setMargin(new Insets(0,0,0,0));
    toggleButton.addActionListener(new toggleMenuVisibility());
    toggleButtonPanel.add(toggleButton, BorderLayout.CENTER);
    
    this.add(toggleButtonPanel, BorderLayout.EAST); 
  }


  public boolean isCollapsed() {
	  return collapsingLayout.isCollapsed();
  }
  
  public void setScrollUnitIncrement(int i)
  {
    scrollUnitSize = i;
    contentScrollPane.getVerticalScrollBar().setUnitIncrement(scrollUnitSize);
  }
  
  public void setMenuVisibilityToggleButtonToolTipText(String text)
  {
     toggleButton.setToolTipText(text);
  }


  public void addPanelCollapseListener(PanelCollapseListener cl)
  {
    panelCollapseListenersList.add(cl);
  }

  public void removePanelCollapseListener(PanelCollapseListener cl)
  {
    panelCollapseListenersList.remove(cl);
  }

  public void addCollapseListener(CollapseListener cl)
  {
    collapseListenersList.add(cl);
  }

  public void removeCollapseListener(CollapseListener cl)
  {
    collapseListenersList.remove(cl);
  }

  
  public void setMenuPanelCollapsed(boolean iscollapsed)
  {
    if (iscollapsed)
    {
      setPanelCollapsed();
    }
    else
    {        
      setPanelExpanded();
    }    
  }

  private void setPanelCollapsed()
  {
    collapsingLayout.setCollapsed(true);
    toggleButton.setIcon(expandIcon);

    instance.revalidate();
    contentPanel.revalidate();
    contentScrollPane.revalidate();
    menusPanel.revalidate();
      
    if (panelCollapseListenersList != null)
    {
      Iterator it = panelCollapseListenersList.iterator();
      while(it.hasNext())
      {
        PanelCollapseListener listener = (PanelCollapseListener)(it.next());
        listener.collapseStatusChanged(this, collapsingLayout.isCollapsed());
      }
    }    
  }

  private void setPanelExpanded()
  {
    collapsingLayout.setCollapsed(false);
    toggleButton.setIcon(collapseIcon);

    instance.revalidate();
    contentPanel.revalidate();
    contentScrollPane.revalidate();
    menusPanel.revalidate();

    if (panelCollapseListenersList != null)
    {
      Iterator it = panelCollapseListenersList.iterator();
      while(it.hasNext())
      {
        PanelCollapseListener listener = (PanelCollapseListener)(it.next());
        listener.collapseStatusChanged(this, collapsingLayout.isCollapsed());
      }
    }    
  }


  public class toggleMenuVisibility implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      if (collapsingLayout.isCollapsed())
      {
        setPanelExpanded();
      }
      else
      {        
        setPanelCollapsed();
      }
    }
  }


  /**
   * f�gt ein Menu an das Ende des MenuPanel ein.
   */  
  public void addMenu(CollapsingMenu menu)
  {
    JPanel newMenuButtonPanel = new JPanel();
    newMenuButtonPanel.setLayout(new BorderLayout());
    newMenuButtonPanel.add(menu, "North");
    menu.setBackground(bgColor); 
    menu.setContainerPanel(newMenuButtonPanel);
    menu.addCollapseListener(this);
    menusList.add(menu);  
    menusPanel.add(newMenuButtonPanel);
    if (isToShowHandCursor) menu.showHandCursor(true);
  }

  
  /**
   * entfernt ein vorher mit addMenu angef�gtes Menu.
   */  
  public void removeMenu(CollapsingMenu menu)
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      if (((CollapsingMenu)(menusList.get(i))).equals(menu))
      {
        menusPanel.remove(((CollapsingMenu)(menusList.get(i))).getContainerPanel());
      }
    }
    menusList.remove(menu);  
    menu.removeCollapseListener(this);    
  }

  /**
   * entfernt alle vorher mit addMenu angef�gte Menus.
   */  
  public void removeAllMenus()
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      CollapsingMenu menu = (CollapsingMenu)(menusList.get(i));
      {
        menu.removeCollapseListener(this);
      }
    }
    
    menusPanel.removeAll();
    menusList.clear();
  }


  /**
   * sucht nach Menu mit angegebenem Namen
   */  
  public CollapsingMenu getMenuByName(String name)
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      if (((CollapsingMenu)(menusList.get(i))).getName().equals(name))
      {
        return(((CollapsingMenu)(menusList.get(i))));
      }
    }
    return(null);
  }


  /**
   * klappt alle Menus ein
   */  
  public void collapseAll()
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      ((CollapsingMenu)(menusList.get(i))).collapse();
    }    
  }


  /**
   * klappt alle Menus aus
   */  
  public void expandAll()
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      ((CollapsingMenu)(menusList.get(i))).expand();
    }    
  }


  /**
   * klappt das angegebene Menu aus
   */  
  public void expandMenu(CollapsingMenu menu)
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      if (((CollapsingMenu)(menusList.get(i))).equals(menu))
      {
        ((CollapsingMenu)(menusList.get(i))).expand();
      }
    }
  }


  /**
   * klappt das angegebene Menu ein
   */  
  public void collapseMenu(CollapsingMenu menu)
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      if (((CollapsingMenu)(menusList.get(i))).equals(menu))
      {
        ((CollapsingMenu)(menusList.get(i))).collapse();
      }
    }
  }


  /**
   * klappt das angegebene Menu aus und alle Anderen ein 
   */  
  public void expandOnlyMenu(CollapsingMenu menu)
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      if (((CollapsingMenu)(menusList.get(i))).equals(menu))
      {
        ((CollapsingMenu)(menusList.get(i))).expand();
      }
      else
      {
        ((CollapsingMenu)(menusList.get(i))).collapse();
      }
      
    }
  }


  /**
   * klappt das angegebene Menu ein und alle Anderen aus
   */  
  public void collapseOnlyMenu(CollapsingMenu menu)
  {
    for(int i=0; i<(menusList.size()); i++)
    {
      if (((CollapsingMenu)(menusList.get(i))).equals(menu))
      {
        ((CollapsingMenu)(menusList.get(i))).collapse();
      }
      else
      {
        ((CollapsingMenu)(menusList.get(i))).expand();
      }
    }
  }


  /**
   * stellt sicher das das angegebene Menu sichtbar ist
   * scrollt wenn n�tig
   */  
  public void ensureMenuVisibility(CollapsingMenu menu)
  {
    contentScrollPane.scrollRectToVisible(menu.getBounds());
  }

  
  /**
   * setzt die Farbe des MenuPanel-Hintergrunds
   */  
  public void setBackgroundColor(Color color)
  {
    bgColor = color;
    setBackground(bgColor);
    menusPanel.setBackground(bgColor);
    menusBorderedPanel.setBackground(bgColor);
    contentScrollPane.setBackground(bgColor);
    toggleButton.setBackground(bgColor);
  }

  
  /**
   * �bergibt die Instanz der Scroll-Pane...
   */  
  public JScrollPane getScrollPane()
  {
    return(contentScrollPane);
  }

  
  public void showHandCursor(boolean show)
  {    
    for(int i=0; i<(menusList.size()); i++)
    {
      ((CollapsingMenu)(menusList.get(i))).showHandCursor(show);
    }    
    isToShowHandCursor = show;
    if (isToShowHandCursor) toggleButton.setCursor(handCursor);
    else                                    toggleButton.setCursor(null);
  }  
  
  
  private void insertContainerIntoMenu(JMenu menu, CollapsingMenuItem collapsingmenuitem, boolean isradiomenu, Component comp)
  {  
    if (comp instanceof JPanel)
    {
      for(int i=0; i<(((JPanel)comp).getComponentCount()); i++)
      {
        Component newcomp = ((JPanel)comp).getComponent(i);
        insertContainerIntoMenu(menu, collapsingmenuitem, isradiomenu, (Component)newcomp);
      }
    }
    else
    {
      if (comp instanceof JButton)
      {
        String text = ((JButton)comp).getText();
        Icon icon = ((JButton)comp).getIcon();

        if ((text == null) || (text.length() == 0))
        {
          if (collapsingmenuitem instanceof CollapsingMenuItemContainer)
          {
            String newtext = ((CollapsingMenuItemContainer)collapsingmenuitem).getComponentDescription(comp);
            if (newtext != null) text = newtext;
          }
        }

        JMenuItem menuitem = null;
        if (isradiomenu)
        {
          if ((icon != null) && (text != null))
          {
            menuitem = new JRadioButtonMenuItem(text, icon, collapsingmenuitem.isSelected());
          }
          else if ((icon == null) && (text != null))
          {
            menuitem = new JRadioButtonMenuItem(text, collapsingmenuitem.isSelected());
          }
        }
        else
        {
          if ((icon != null) && (text != null))
          {
            menuitem = new JMenuItem(text, icon);
          }
          else if ((icon == null) && (text != null))
          {
            menuitem = new JMenuItem(text);
          }
        }


        if (menuitem != null)
        {            
          menuitem.setFont(((JButton)comp).getFont());
          ActionListener[] listeners = ((JButton)comp).getActionListeners();
          for(int n=0; n<listeners.length; n++) menuitem.addActionListener(listeners[n]);
          menu.add(menuitem);
        }
      }        
      else if (comp instanceof JCheckBox)
      {
        String text = ((JCheckBox)comp).getText();
        Icon icon = ((JCheckBox)comp).getIcon();
        boolean state = ((JCheckBox)comp).isSelected();
        JCheckBoxMenuItem menuitem = null;
        if ((icon != null) && (text != null))
        {
          menuitem = new JCheckBoxMenuItem(text, icon, state);
        }
        else if ((icon == null) && (text != null))
        {
          menuitem = new JCheckBoxMenuItem(text, state);
        }

        if (menuitem != null)
        {            
          menuitem.setFont(((JCheckBox)comp).getFont());
          menuitem.addItemListener(new CheckBoxItemListener((JCheckBox)comp));          
          changeListeners.add(new changelistener(((JCheckBox)comp), new source_checkbox_changed((JCheckBox)comp, menuitem)));
          menu.add(menuitem);
        }
      }
      else if (comp instanceof JIntegerField)
      {
        if (((JIntegerField)comp).isEditable())
        {
          String text = ((JIntegerField)comp).getText();
  
          JMenuItem menuitem = null;
          
          String itemname = "";        

          if (collapsingmenuitem instanceof CollapsingMenuItemContainer)
          {
            String newtext = ((CollapsingMenuItemContainer)collapsingmenuitem).getComponentDescription(comp);
            if (newtext != null) itemname = newtext;
          }
          else
          {
            if (collapsingmenuitem.getDescription() != null) itemname = collapsingmenuitem.getDescription();
            else                                                                       itemname = "Wert: ";
          }
          
          itemname += " (" + text + ")";
          menuitem = new JMenuItem(itemname);
  
          if (menuitem != null)
          {            
            menuitem.setFont(((JIntegerField)comp).getFont());
            menuitem.addActionListener(new IntegerDialogOpener((JIntegerField)comp, itemname));
  
            menu.add(menuitem);
          }
        }
      }
    }
  }
  
  
  private class IntegerDialogOpener implements ActionListener
  {
    private JIntegerField m_oIntegerField;
    private String             m_sTitle;
    
    public IntegerDialogOpener(JIntegerField integerfield, String title)
    {
      m_oIntegerField = integerfield;
      m_sTitle = title;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      if (parentFrame instanceof JFrame) new IntegerDialog((JFrame)parentFrame, m_oIntegerField, m_sTitle);
      else if (parentFrame instanceof JDialog) new IntegerDialog((JDialog)parentFrame, m_oIntegerField, m_sTitle);
    }    
  }


  private class IntegerDialog extends EscapeDialog
  {
    private JIntegerField m_oIntegerField;
    private JIntegerField m_oParentIntegerField;
     
    public IntegerDialog(JFrame parent, JIntegerField integerfield, String title)
    {
      super(parent);
      init(integerfield, title);
    }
    
    public IntegerDialog(JDialog parent, JIntegerField integerfield, String title)
    {
      super(parent);
      init(integerfield, title);
    }
    
    private void init(JIntegerField integerfield, String title)
    {
      if (title != null) this.setTitle(title);
      this.setResizable(false);
      m_oParentIntegerField = integerfield;
      
      m_oIntegerField = new JIntegerField();
      m_oIntegerField.setIntegerValue(m_oParentIntegerField.getIntegerValue());
      m_oIntegerField.setEditable(m_oParentIntegerField.isEditable());

      m_oIntegerField.addActionListener(new WindowCloseOnReturnListener(this, m_oIntegerField, m_oParentIntegerField));
      ActionListener[] actionlisteners = m_oParentIntegerField.getActionListeners();
      for(int n=0; n<actionlisteners.length; n++) m_oIntegerField.addActionListener(actionlisteners[n]);

      m_oIntegerField.addFocusListener(new WindowCloseOnFocusLostListener(this, m_oIntegerField, m_oParentIntegerField));
      FocusListener[] focuslisteners = m_oParentIntegerField.getFocusListeners();
      for(int n=0; n<focuslisteners.length; n++) m_oIntegerField.addFocusListener(focuslisteners[n]);
            
      JLabel oLabel = new JLabel(title);
      
      JPanel panel = new JPanel();
      panel.setLayout(new BorderLayout());
      panel.add(m_oIntegerField, BorderLayout.CENTER);
      panel.add(oLabel, BorderLayout.NORTH);
      
      getContentPane().add(panel);
      pack();     
      addWindowListener(new WindowCloseListener(this, m_oIntegerField, m_oParentIntegerField));
      setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      setLocationRelativeTo(null);
      setModal(true);
      setVisible(true);      
    }    
  }


  private class WindowCloseOnFocusLostListener implements FocusListener
  {
    private JDialog m_oDialog;
    private JIntegerField m_oIntegerField;
    private JIntegerField m_oParentIntegerField;

    public WindowCloseOnFocusLostListener(JDialog oDialog, JIntegerField integerfield, JIntegerField parentintegerfield)
    {
      m_oDialog = oDialog;
      m_oParentIntegerField = parentintegerfield;
      m_oIntegerField = integerfield;
    }
    
    public void focusLost(FocusEvent e)
    {
      m_oDialog.setVisible(false);
      m_oParentIntegerField.setText(m_oIntegerField.getText());
    }

    public void focusGained(FocusEvent e) {}
  }


  private class WindowCloseOnReturnListener implements ActionListener
  {
    private JDialog m_oDialog;
    private JIntegerField m_oIntegerField;
    private JIntegerField m_oParentIntegerField;

    public WindowCloseOnReturnListener(JDialog oDialog, JIntegerField integerfield, JIntegerField parentintegerfield)
    {
      m_oDialog = oDialog;
      m_oParentIntegerField = parentintegerfield;
      m_oIntegerField = integerfield;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      m_oDialog.setVisible(false);
      m_oParentIntegerField.setText(m_oIntegerField.getText());
    }
  }


  private class WindowCloseListener extends WindowAdapter
  {
    private JDialog m_oDialog;
    private JIntegerField m_oIntegerField;
    private JIntegerField m_oParentIntegerField;
    
    public WindowCloseListener(JDialog oDialog, JIntegerField integerfield, JIntegerField parentintegerfield)
    {
      m_oDialog = oDialog;
      m_oParentIntegerField = parentintegerfield;
      m_oIntegerField = integerfield;
    }
    
    public void windowClosing(WindowEvent e) 
    {
      m_oDialog.setVisible(false);
      m_oParentIntegerField.setText(m_oIntegerField.getText());
    }    
  }


  public class CheckBoxItemListener implements ItemListener
  {  
    private JCheckBox m_oCheckBox;
    
    public CheckBoxItemListener(JCheckBox cb)
    {
      m_oCheckBox = cb;
    }
    
    public void itemStateChanged(ItemEvent e) 
    {
      boolean newstate = (e.getStateChange() == ItemEvent.SELECTED);
      if (m_oCheckBox.isSelected() != newstate) m_oCheckBox.doClick();
    }
  }
  

  private class changelistener
  {
    private AbstractButton m_oAbstractButton;
    private ChangeListener m_oChangeListener;
    
    public changelistener(AbstractButton oAbstractButton, ChangeListener oChangeListener)
    {
      m_oAbstractButton = oAbstractButton;
      m_oChangeListener = oChangeListener;
      addListener();
    }

    private void addListener()
    {
      m_oAbstractButton.addChangeListener(m_oChangeListener);
    }
        
    public void removeListener()
    {
      m_oAbstractButton.removeChangeListener(m_oChangeListener);
    }        
  }


  public class on_menu_remove implements AncestorListener
  {
    private Component m_oComponent;
    
    public on_menu_remove(Component comp)
    {
      m_oComponent = comp;
    }
    
    private void removeListeners()
    {
      while(!(changeListeners.isEmpty()))
      {
        ((changelistener)(changeListeners.remove(0))).removeListener();        
      }
    }
    
    public void ancestorRemoved(AncestorEvent event)
    {
      if (event.getSource().equals(m_oComponent)) removeListeners();
    }

    public void ancestorAdded(AncestorEvent event) {}
    public void ancestorMoved(AncestorEvent event) {}
  }


  public class source_checkbox_changed implements ChangeListener
  {    
    private JCheckBox m_oCheckBox;
    private JCheckBoxMenuItem m_oCheckBoxMenuItem;
    
    public source_checkbox_changed(JCheckBox oCheckBox, JCheckBoxMenuItem oCheckBoxMenuItem)
    {
      m_oCheckBox = oCheckBox;
      m_oCheckBoxMenuItem = oCheckBoxMenuItem;
    }
    
    public void stateChanged(ChangeEvent e)
    {
      if (m_oCheckBoxMenuItem.isSelected() != m_oCheckBox.isSelected()) m_oCheckBoxMenuItem.doClick();
    } 
  }


  private boolean menuHasSelectedMember(CollapsingMenu collapsingmenu)
  {
    for (int n = 0; n < (collapsingmenu.getNumberOfMenuItems()); n++)
    {
      if (collapsingmenu.getMenuItemByIndex(n).isSelected()) 
      {
        return(true);
      }      
    }
    return(false);
  }


  public JMenuBar createMenuBar()
  {
    JMenuBar menubar = new JMenuBar();
    
    for(int i=0; i<(menusList.size()); i++)
    {
      CollapsingMenu collapsingmenu = ((CollapsingMenu)(menusList.get(i)));
 
      boolean isradiomenu = menuHasSelectedMember(collapsingmenu);
 
      JMenu menu = new JMenu(collapsingmenu.getName());      
      menu.setFont(collapsingmenu.getMenuFont());
      
      menu.setEnabled(collapsingmenu.getMenuEnabled());
      
      for (int n = 0; n < (collapsingmenu.getNumberOfMenuItems()); n++)
      {
        CollapsingMenuItem collapsingmenuitem = collapsingmenu.getMenuItemByIndex(n);
        if (collapsingmenuitem.getItemVisible())
        {
          if (collapsingmenuitem instanceof CollapsingMenuItemDivider)
          {
            menu.addSeparator();
          }
          else
          {
            ImageIcon itemicon = collapsingmenuitem.getIcon();
            String itemname = collapsingmenuitem.getText();
            
            JMenuItem menuitem = null;

            if (isradiomenu)
            {        
              if ((itemicon != null) && (itemname != null))
              {
                menuitem = new JRadioButtonMenuItem(itemname, itemicon, collapsingmenuitem.isSelected());
              }
              else if ((itemicon == null) && (itemname != null))
              {
                menuitem = new JRadioButtonMenuItem(itemname, collapsingmenuitem.isSelected());
              }
            }
            else
            {
              if ((itemicon != null) && (itemname != null))
              {
                menuitem = new JMenuItem(itemname, itemicon);
              }
              else if ((itemicon == null) && (itemname != null))
              {
                menuitem = new JMenuItem(itemname);
              }
            }


            if (menuitem != null)
            {            
              menuitem.setFont(collapsingmenuitem.getItemFont());
              menuitem.addActionListener(collapsingmenuitem.getActionListener());
              menuitem.setEnabled(collapsingmenuitem.getItemEnabled());
              menu.add(menuitem);
            }
            else
            {              
              if (collapsingmenuitem instanceof CollapsingMenuItemContainer)
              {                
                Component comp = ((CollapsingMenuItemContainer)collapsingmenuitem).getItemComponent();
                insertContainerIntoMenu(menu, collapsingmenuitem, isradiomenu, (Component)comp);                    
              }
            }
          }
        }
      }
      menubar.add(menu);      
    }        
    menubar.addAncestorListener(new on_menu_remove(menubar));        
    return(menubar);
  }
  
  
  public void setParentFrame(Object oParentFrame)
  {
    parentFrame = oParentFrame;
  }
  
  
  public Dimension getMenuVisibilityToggleButtonPanelSize()
  {
    return(toggleButtonPanel.getSize());  
  }


  public void collapseStatusChanged(CollapsingMenu menu, boolean iscollapsed)
  {
    if (collapseListenersList != null)
    {
      Iterator it = collapseListenersList.iterator();
      while(it.hasNext())
      {
        CollapseListener listener = (CollapseListener)(it.next());
        listener.collapseStatusChanged(menu, iscollapsed);
      }
    }    
  }
  
}
