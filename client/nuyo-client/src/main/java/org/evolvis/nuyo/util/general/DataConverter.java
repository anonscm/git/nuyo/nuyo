/*
 * Created on 13.10.2004
 *
 */
package org.evolvis.nuyo.util.general;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * Diese Klasse erlaubt das Konvertieren zwischen den  
 * Datentypen "String", "Integer", "Boolean", "Date" 
 * "Double" und "Character"
 * wenn eine Konvertierung nicht m�glich ist wird
 * eine DataConversionException geworfen (also Kopf einziehen!)
 * <br>
 * Die Methoden dieser Klasse sind alle statisch (static).
 *  
 * @author niko
 */
public class DataConverter
{
  private static Map m_oSources = null;
  private static Class[] m_oAllClasses = null;
  private static Class[] m_oSrcClasses = null;
  private static Class[] m_oDstClasses = null;
  

  // die Quell-Datentypen k�nnen �ber den Key der 
  // Map m_oSources eine weitere Map erhalten 
  // die �ber den Ziel-Datentyp Zugiff auf 
  // eine Klasse vom Typ Converter bietet die die
  // eigentliche Konvertierung durchf�hrt.
  
  static
  {    
    // Source String 
    // diese Map enth�lt alle Converter die als
    // Quell-Datentyp String unterst�tzen.
    Map oStringSource = new HashMap();    
    // Ziel-Datentyp String einf�gen...
    oStringSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object;
          }      
        });
    // Ziel-Datentyp Boolean einf�gen...
    oStringSource.put(Boolean.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return parseBoolean((String)object);
          }      
        });    
    // Ziel-Datentyp Integer einf�gen...
    oStringSource.put(Integer.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            try
            {
              return new Integer(Integer.parseInt((String)object));
            }
            catch(NumberFormatException nfe) 
            {
              throw new DataConversionException(String.class, Integer.class, nfe);            
            }
          }      
        });    
    // Ziel-Datentyp Double einf�gen...
    oStringSource.put(Double.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            try
            {
              return new Double(Double.parseDouble((String)object));
            }
            catch(NumberFormatException nfe) 
            {
              throw new DataConversionException(String.class, Double.class, nfe);            
            }
          }      
        });    
    // Ziel-Datentyp Date einf�gen...
    oStringSource.put(Date.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            DateFormat df = new SimpleDateFormat(); 
            try
            {
              return df.parse((String)object);
            }
            catch (ParseException e) 
            {
              throw new DataConversionException(String.class, Date.class, e);            
            }
          }      
        });
    // Ziel-Datentyp Character einf�gen...
    oStringSource.put(Character.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            if (((String)object).length() > 0) return new Character(((String)object).charAt(0));
            else return null;
          }      
        });

    oStringSource.put(Object.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object;
          }      
        });


    // -------------------------------------------
    
    // Source Boolean
    // diese Map enth�lt alle Converter die als
    // Quell-Datentyp Boolean unterst�tzen.
    Map oBooleanSource = new HashMap();
    oBooleanSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object.toString();
          }      
        });
    oBooleanSource.put(Boolean.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object;
          }      
        });    
    oBooleanSource.put(Integer.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            if (((Boolean)object).booleanValue()) return new Integer(1);
            else return new Integer(0);
          }      
        });    
    oBooleanSource.put(Double.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            if (((Boolean)object).booleanValue()) return new Double(1.0);
            else return new Double(0.0);
          }      
        });    

    //oBooleanSource.put(Date.class, new Converter() 
    
    oBooleanSource.put(Character.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            if (((Boolean)object).booleanValue()) return new Character('1');
            else return new Character('0');
          }      
        });
    
    // -------------------------------------------
    
    
    // Source Integer
    Map oIntegerSource = new HashMap();
    oIntegerSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object.toString();
          }      
        });
    oIntegerSource.put(Boolean.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            if (((Integer)object).intValue() == 0) return new Boolean(false);
            else if (((Integer)object).intValue() == 1) return new Boolean(true);
            else return null;
          }      
        });    
    oIntegerSource.put(Integer.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {
            return object;
          }      
        });    
    oIntegerSource.put(Double.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Double(((Integer)object).intValue());
          }      
        });    
    oIntegerSource.put(Date.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Date(((Integer)object).intValue());
          }      
        });
    oIntegerSource.put(Character.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Character((char)((Integer)object).intValue());
          }      
        });

    // -------------------------------------------
    
    // Source Double
    Map oDoubleSource = new HashMap();
    oDoubleSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object.toString();
          }      
        });
    oDoubleSource.put(Boolean.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            if (((Double)object).doubleValue() == 0.0) return new Boolean(false);
            else if (((Double)object).doubleValue() == 1.0) return new Boolean(true);
            else return null;
          }      
        });    
    oDoubleSource.put(Integer.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {
            return new Integer(((Double)object).intValue());
          }      
        });    
    oDoubleSource.put(Double.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object;
          }      
        });    
    oDoubleSource.put(Date.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Date(((Double)object).intValue());
          }      
        });
    oDoubleSource.put(Character.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Character((char)((Double)object).intValue());
          }      
        });

    // -------------------------------------------

    
    // Source Date
    Map oDateSource = new HashMap();
    oDateSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            DateFormat df = new SimpleDateFormat();
            return df.format((Date)object);
          }      
        });
    //oDateSource.put(Boolean.class, new Converter() 
    oDateSource.put(Integer.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Integer((int)(((Date)object).getTime()));
          }      
        });    
    oDateSource.put(Double.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Double((int)(((Date)object).getTime()));
          }      
        });    
    oDateSource.put(Date.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {
            return object;
          }      
        });
    //oDateSource.put(Character.class, new Converter() 


    // -------------------------------------------
    
    // Source Character
    Map oCharacterSource = new HashMap();
    oCharacterSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object.toString();
          }      
        });
    oCharacterSource.put(Boolean.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return parseBoolean((Character)object);
          }      
        });    
    oCharacterSource.put(Integer.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Integer(((Character)object).charValue());
          }      
        });    
    oCharacterSource.put(Double.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return new Double(((Character)object).charValue());
          }      
        });    
    //oCharacterSource.put(Date.class, new Converter() 
    oCharacterSource.put(Character.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {
            return object;
          }      
        });


    // -------------------------------------------
    
    // Source Object
    Map oObjectSource = new HashMap();
    oObjectSource.put(String.class, new Converter() 
        {
          public Object convert(Object object) throws DataConversionException
          {       
            return object.toString();
          }      
        });

    // -------------------------------------------
    
    m_oSources = new HashMap();
    m_oSources.put(String.class, oStringSource);
    m_oSources.put(Boolean.class, oBooleanSource);
    m_oSources.put(Integer.class, oIntegerSource);
    m_oSources.put(Double.class, oDoubleSource);
    m_oSources.put(Date.class, oDateSource);
    m_oSources.put(Character.class, oCharacterSource);
    m_oSources.put(Object.class, oObjectSource);
    
    createClassArrays();
  }
  

  private interface Converter 
  {
    public Object convert(Object object) throws DataConversionException;
  }
  
  
  
  // -------------------------------------------
  

  private static void createClassArrays()
  {
    Map oAllTypes = new HashMap();
    Map oDstTypes = new HashMap();
    
    Iterator srcit = m_oSources.entrySet().iterator();
    while(srcit.hasNext())
    {
      Map.Entry entry = (Map.Entry)(srcit.next());
      Object key = entry.getKey();
      if (!(oAllTypes.containsKey(key))) oAllTypes.put(key, "X");
      
      Map dstmap = (Map)(entry.getValue());
      Iterator dstit = dstmap.keySet().iterator();
      while(dstit.hasNext())
      {
        key = dstit.next();
        if (!(oAllTypes.containsKey(key))) oAllTypes.put(key, "X");
        if (!(oDstTypes.containsKey(key))) oDstTypes.put(key, "X");
      }      
    }
    
    m_oAllClasses = new Class[oAllTypes.size()];    
    Iterator resultit = oAllTypes.keySet().iterator();
    int i = 0;
    while(resultit.hasNext())
    {
      Object key = resultit.next();
      m_oAllClasses[i++] = (Class)key;
    }      

    m_oDstClasses = new Class[oDstTypes.size()];    
    Iterator dstresultit = oDstTypes.keySet().iterator();
    int dsti = 0;
    while(dstresultit.hasNext())
    {
      Object key = dstresultit.next();
      m_oDstClasses[dsti++] = (Class)key;
    }
    
    m_oSrcClasses = new Class[m_oSources.size()];    
    Iterator srcresultit = m_oSources.keySet().iterator();
    int srci = 0;
    while(srcresultit.hasNext())
    {
      Object key = srcresultit.next();
      m_oSrcClasses[srci++] = (Class)key;
    }      
    
  }
  
  
  /**
   * Diese Methode liefert einen Array mit Class 
   * Objekten die von der Methode convert() als
   * Quelldatentyp unterst�tzt werden. 
   * 
   * @return Class[] - die unterst�tzten Datentypen
   */
  public static Class[] accepts()
  {
    return m_oSrcClasses;
  }

  /**
   * Diese Methode liefert einen Array mit Class 
   * Objekten die von der Methode convert() als
   * Quelldatentyp oder Zieldatentyp unterst�tzt 
   * werden. 
   * 
   * @return Class[] - die unterst�tzten Datentypen
   */
  public static Class[] known()
  {
    return m_oAllClasses;
  }

  /**
   * Diese Methode liefert einen Array mit Class 
   * Objekten die von der Methode convert() als
   * Zieldatentyp unterst�tzt werden. 
   * 
   * @return Class[] - die unterst�tzten Datentypen
   */
  public static Class[] results()
  {
    return m_oDstClasses;
  }

  
  /**
   * Diese Methode �berpr�ft ob die �bergebene
   * Klasse (Datentyp) von der Methode convert()
   * als Quelldatentyp unterst�tzt wird.
   * 
   * @param c (Class) - das Objekt das �berpr�ft werden soll
   * @return boolean 
   * <dl>
   *   <dt>true</dt>
   *   <dd>
   *     wenn das �bergebene Objekt prinzipiell 
   *     unterst�tzt wird. Evtl. wird jedoch
   *     der gew�nschte Zieltyp nicht unterst�tzt.
   *     Genauere Informationen liefert die Funktion
   *     isConvertable(). 
   *   </dd>
   *   <dt>false</dt>
   *   <dd>
   *     wenn der Datentyp des �bergebenen Objekts nicht unterst�tzt wird. 
   *     Eine Konvertierung mittels convert() ist prinzipiell nicht m�glich.
   *   </dd>
   * </dl>  
   */
  public static boolean accepts(Class c)
  {
    for(int i=0; i<m_oSrcClasses.length; i++)
    {
      if (m_oSrcClasses[i].equals(c)) return true;
    }
    return false;
  }  

  
  /**
   * Diese Methode testet ob zwei Datentypen prinzipiell
   * ineinander konvertiert werden k�nnen. z.B. ist eine
   * Konvertierung von Date nach Boolean v�llig unabh�ngig
   * von den Eingangsdaten immer unm�glich und diese Methode
   * liefert daher false. Eine Konvertierung von String
   * nach Integer ist z.B. prinzipiell m�glich, kann jedoch
   * abh�ngig vom Inhalt des �bergebenen String trotzdem
   * zu einem Fehler f�hren falls z.B. Buchstaben im String 
   * vorhanden sind. Diese Methode liefert in diesem Fall
   * true da ja eine Konvertierung bei richtigen Daten 
   * m�glich ist.
   * 
   * @param src (Class) der Datentyp der Quelle
   * @param dst (Class) der Datentyp in den die Quelle 
   * konvertiert werden soll
   * @return (boolean) gibt an ob die gew�nschte Konvertierung 
   * prinzipiell funktionieren kann.
   */
  public static boolean isConvertable(Class src, Class dst)
  {
    Map dstmap = (Map)(m_oSources.get(src));
    if (dstmap != null)
    {
      Converter conv = (Converter)(dstmap.get(dst));
      if (conv != null)
      {
        return true;
      }
    }
    return false;
  }  

  
  /**
   * Diese Methode f�hrt eine Konvertierung
   * zwischen Datentypen durch. Der gew�nschte
   * Datentyp wird �ber den Parameter "wanteddatatype"
   * angegeben. Wenn die Konvertierung von "sourcedata" 
   * in den durch "wanteddatatype" angegebenen Datentyp
   * nicht m�glich ist wird eine DataConversionException 
   * geworfen. 
   * 
   * @param sourcedata (Object)    - das Objekt das konvertiert werden soll
   * @param wanteddatatype (Class) - der Datentyp in den das Objekt konvertiert werden soll
   * @return das konvertierte Objekt (Object) - wenn Konvertierung m�glich war  
   * @throws DataConversionException - wenn das 
   * �bergebene Objekt nicht konvertiert werden 
   * konnte (z.B. wegen ung�ltigen Daten oder weil
   * einer der verwendeten Datentypen nicht �nterst�tzt wird)
   */
  public static Object convert(Object sourcedata, Class wanteddatatype) throws DataConversionException
  {
    // versuchen die Map mit den f�r diesen 
    // Quell-Datentyp vorhandenen Convertern aus  
    // der Map m_oSources zu holen... 
    Map dstmap = (Map)(m_oSources.get(sourcedata.getClass()));
    if (dstmap != null)
    {
      // der Quell-Datentyp wurde gefunden...
      // nun versuchen einen Spezifischen Converter
      // f�r den Ziel-Datentyp zu holen...
      Converter conv = (Converter)(dstmap.get(wanteddatatype));
      if (conv != null)
      {
        // es wurde ein spezifischer Converter
        // gefunden... nun seine convert() Methode
        // aufrufen... wenn es schief geht weil
        // der spezifische Datensatz keine f�r diese
        // Umwandlung g�ltige Daten hat wird von 
        // convert() eine DataConversionException 
        // geworfen die einfach weiter nach oben
        // weitergereicht wird...
        return conv.convert(sourcedata);
      }
      else
      {
        // es wurde kein spezifischer Converter
        // f�r den gew�nschten Ziel-Datentyp
        // gefunden... Exception werfen...
        throw new DataConversionException(sourcedata.getClass(), wanteddatatype, "Eine Konvertierung des Quellobjekts in den gew�nschten Datentyp ist nicht m�glich.");
      }
    }
    else
    {
      // Source nicht gefunden
      // der Quell-Datentyp wird nicht unterst�tzt...
      // Exception werfen...
      throw new DataConversionException(sourcedata.getClass(), wanteddatatype, "Datentyp des zu konvertierenden Objekts unbekannt.");
    }
  }
  
 
  /**
   * @param boolstring (String) - der Text der in einen Wahrheitswert umgewandelt werden soll.
   * m�gliche Werte sind: 
   * <ul type="circle">
   *   <li>true</li>
   *   <li>false</li>
   *   <li>0</li>
   *   <li>1</li>
   *   <li>yes</li>
   *   <li>no</li>
   *   <li>ja</li>
   *   <li>nein</li>
   *   <li>on</li>
   *   <li>off</li>
   *   <li>an</li>
   *   <li>aus</li>
   * </ul> 
   * @return das Ergebnis als Wahrheitswert (boolean)
   * @throws DataConversionException
   */
  public static Boolean parseBoolean(String boolstring) throws DataConversionException
  {
    if      ("true".equalsIgnoreCase(boolstring)) return new Boolean(true);
    else if ("false".equalsIgnoreCase(boolstring)) return new Boolean(false);
    else if ("0".equalsIgnoreCase(boolstring)) return new Boolean(false);
    else if ("1".equalsIgnoreCase(boolstring)) return new Boolean(true);
    else if ("yes".equalsIgnoreCase(boolstring)) return new Boolean(true);
    else if ("no".equalsIgnoreCase(boolstring)) return new Boolean(false);
    else if ("ja".equalsIgnoreCase(boolstring)) return new Boolean(true);
    else if ("nein".equalsIgnoreCase(boolstring)) return new Boolean(false);
    else if ("on".equalsIgnoreCase(boolstring)) return new Boolean(true);
    else if ("off".equalsIgnoreCase(boolstring)) return new Boolean(false);
    else if ("an".equalsIgnoreCase(boolstring)) return new Boolean(true);
    else if ("aus".equalsIgnoreCase(boolstring)) return new Boolean(false);
    else throw new DataConversionException(String.class, Boolean.class); 
  }

  /**
   * @param boolchar (Character)
   * m�gliche Werte sind: 
   * <ul type="circle">
   *   <li>0 (false)</li>
   *   <li>1 (true)</li>
   *   <li>y (true)</li>
   *   <li>j (true)</li>
   *   <li>&nbsp;[Leerzeichen] (false)</li>
   *   <li>x (true)</li>
   *   <li>X (true)</li>
   * </ul> 
   * 
   * @return das Ergebnis als Wahrheitswert (boolean)
   * @throws DataConversionException
   */
  public static Boolean parseBoolean(Character boolchar) throws DataConversionException
  {
         if ('0' == boolchar.charValue()) return new Boolean(false);
    else if ('1' == boolchar.charValue()) return new Boolean(true);
    else if ('n' == boolchar.charValue()) return new Boolean(false);
    else if ('y' == boolchar.charValue()) return new Boolean(true);
    else if ('j' == boolchar.charValue()) return new Boolean(true);
    else if (' ' == boolchar.charValue()) return new Boolean(false);
    else if ('x' == boolchar.charValue()) return new Boolean(true);
    else if ('X' == boolchar.charValue()) return new Boolean(true);
    else throw new DataConversionException(Character.class, Boolean.class); 
  }
}
