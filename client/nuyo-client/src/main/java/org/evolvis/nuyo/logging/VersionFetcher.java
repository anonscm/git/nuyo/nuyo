/*
 * Created on 17.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.io.File;
import java.util.Date;


/**
 * @author niko
 *
 */
public class VersionFetcher
{
  private String m_sResultFilename = null;
  
  public VersionFetcher(String ticketid)
  {
    this(ticketid, true);
  }
  
  public VersionFetcher(boolean generatexml)
  {
    this(null, generatexml);
  }
  
  public VersionFetcher(String ticketid, boolean generatexml)
  {
    //this(de.tarent.version.Starter.getStandardPath(), ticketid, generatexml);
  }
  
  public VersionFetcher(String inputPath, String ticketid, boolean generatexml)
  {
    //if (inputPath == null) inputPath = de.tarent.version.Starter.getStandardPath();
    if (ticketid == null) ticketid = Long.toHexString((new Date()).getTime());
    String outputPath = System.getProperty("java.io.tmpdir")+File.separator+"Versionsinformationen_" + ticketid;  
    //Handler handler = new Handler(inputPath, outputPath, "filter.xsl", true, true, generatexml);
    if (generatexml) m_sResultFilename = outputPath + ".xml";
    else  m_sResultFilename = outputPath + ".html";
  }
  
  public String getResultFilename()
  {
    return m_sResultFilename;
  }
  
  public void removeResultFile()
  {
   if (m_sResultFilename != null)
   {
     File file = new File(m_sResultFilename);
     if (file != null)
     {
       file.delete();
     }
   }
  }
  
}
