/* $Id: ObjectRole.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

/**
 * Interface f�r eine Object-Rolle wie zum Beispiel einer Rolle auf einer Kategorie
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public interface ObjectRole extends Role {
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Objekte zu lesen.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightRead();

	
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Objekte zu �ndern.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightEdit();
	
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Objekte hinzuzuf�gen.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightAdd();
	
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Objekte zu l�schen.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightRemove();
	

	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Unterkategoriezuordnungen zu �ndern.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightStructure();

	
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Unterobjekte zu erzeugen.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightAddSub();
	
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Unterobjekte zu l�schen.
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightRemoveSub();
	
	/**
	 * Gibt an, ob Rolle das Recht enth�lt, Berechtigungen auf diesem Objekt zu verleihen
	 * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
	 */
	public boolean hasRightGrant();
	
	
	/*Darf Object lesen*/
	public static final String KEY_READ = "auth_read";
	/*Darf Object ver�ndern*/
    public static final String KEY_EDIT = "auth_edit";
    /*Darf Object hinzuf�gen*/
    public static final String KEY_ADD = "auth_add";
    /*Darf Object l�schen*/
    public static final String KEY_REMOVE = "auth_remove";
    /*Darf Unterkategoriezuordnungen �ndern*/
    public static final String KEY_STRUCTURE = "auth_structure";
    /*Darf Unterobjekte hinzuf�gen*/
    public static final String KEY_ADDSUB = "auth_addsub";
    /*Darf Unterobjekte l�schen*/
    public static final String KEY_REMOVESUB = "auth_removesub";
    /*Darf Rechte auf dieses Object vergeben*/
    public static final String KEY_GRANT = "auth_grant";
    
    public static final String KEY_DELETABLE = "deletable";

    

	/*Darf Object lesen*/
	public static final int KEY_Int_READ = 1;
	/*Darf Object ver�ndern*/
    public static final int KEY_Int_EDIT = 2;
    /*Darf Object hinzuf�gen*/
    public static final int KEY_Int_ADD = 3;
    /*Darf Object l�schen*/
    public static final int KEY_Int_REMOVE = 4;
    /*Darf Unterkategoriezuordnungen �ndern*/
    public static final int KEY_Int_STRUCTURE = 5;
    /*Darf Unterobjekte hinzuf�gen*/
    public static final int KEY_Int_ADDSUB = 6;
    /*Darf Unterobjekte l�schen*/
    public static final int KEY_Int_REMOVESUB = 7;
    /*Darf Rechte auf dieses Object vergeben*/
    public static final int KEY_Int_GRANT = 8;
    
   
    public static final String[] AUTH_KEYS = { KEY_READ, KEY_EDIT, KEY_ADD, KEY_REMOVE, KEY_STRUCTURE, KEY_ADDSUB, KEY_REMOVESUB, KEY_GRANT };

}
