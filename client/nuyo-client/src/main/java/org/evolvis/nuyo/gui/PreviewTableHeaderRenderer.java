/*
 * Created on 11.08.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

/**
 * @author niko
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PreviewTableHeaderRenderer implements TableCellRenderer
{
  private Map m_oHeaderIcons = new HashMap();
  private Font m_oFont = null;
  
  public void setHeaderIcon(int column, ImageIcon icon)
  {
    m_oHeaderIcons.put(new Integer(column), icon);
  }
  
  public void clearAllHeaderIcons()
  {
    m_oHeaderIcons.clear();    
  }
  
  public void setFont(Font font)
  {
    m_oFont = font;
  }
  
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    JPanel panel = new JPanel(new BorderLayout());
    JLabel label = new JLabel(value.toString());
    label.setFont(m_oFont);
    
    int realcolumn = table.getColumnModel().getColumn(column).getModelIndex();
    ImageIcon pic = (ImageIcon)(m_oHeaderIcons.get(new Integer(realcolumn)));    
    JLabel icon = new JLabel(pic);
    
    panel.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED), new EmptyBorder(0,2,0,2)));
    panel.add(label, BorderLayout.CENTER);
    panel.add(icon, BorderLayout.EAST);    

    return panel;
  }
  
  
}
