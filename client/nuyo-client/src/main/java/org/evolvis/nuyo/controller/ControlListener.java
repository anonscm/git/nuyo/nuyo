/* $Id: ControlListener.java,v 1.30 2007/06/20 19:51:25 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import java.util.Date;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.gui.ContactGuiException;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.PanelInserter;
import org.evolvis.nuyo.gui.calendar.ScheduleData;
import org.evolvis.nuyo.util.general.DataAccess;


/**
 * Diese Schnittstelle abstrahiert die vom Control-Layer ben�tigte GUI
 * dieser Applikation.
 * 
 * TODO check for duplicate methods with Interface CommonDialogServices
 * 
 * @author mikel
 */
public interface ControlListener {

    // available views
    public final static String TAB_CONTACT = "TAB_CONTACT";
    public final static String TAB_EXTENDED = "TAB_EXTENDED";
    public final static String TAB_CATEGORY_ASSIGNMENT_VIEW = "TAB_MAILING_LIST_ASSIGNMENT";
    public final static String TAB_MAIL_ORDERS = "TAB_MAIL_ORDERS";
    public final static String TAB_ORGANIZER = "TAB_ORGANIZER";

    /*
     * Diese Methoden dienen der Initialisierung, dem �ffnen und Schlie�en der GUI.
     */

    /**
     * Initialisiert das Hauptfenster und alle anderen Fenster/Dialoge.
     * Alle erzeugten Fenster werden auf "invisible" gesetzt.
     */
    public void init() throws ContactGuiException;

    /**
     * Method openGUI. �ffnet das Hauptfenster der Applikation.
     */
    public void openGUI();

    /**
     * Method closeGUI. Schliesst das Hauptfenster der Applikation
     */
    public void closeGUI();

    public JFrame getFrame();
    public void addComponentToResize(JComponent comp);
    public void removeComponentToResize(JComponent comp);

    public ScheduleData getScheduleData();    
    
    /**
     * Diese Methode �ffnet das Fenster des Postversands.
     */
    public void openSerienVersandAlle();

//     /**
//      * Diese Methode �ffnet das Suchfenster.
//      */
//     public void openSuche();

//     /**
//      * �ffnet die Unterkategorieauswahl
//      */
//     public void openVerteilerChooser();
    
//     /**
//      * �ffnet das editierbare Unterkategorielistenfenster.
//      * 
//      * @param address die Adresse, f�r die die Unterkategorien bearbeitet
//      *  werden sollen.
//      * @param allowCancel legt fest, ob der Abbrechen-Button sichtbar ist.
//      * @param enableUse Benutzung erlauben
//      */
//     public void openVerteilerTreeWindow(Address address, boolean allowCancel, boolean enableUse);

    /**
     * Method showMessage. �ffnet einen Dialog um eine Fehlermeldung
     * anzuzeigen. Wartet bis der Benutzer den "Ok"-Knopf gedr�ckt hat.
     * 
     * @param message Der Fehlermeldungstext
     */
    public void showMessage(String message);
    public void showExtendedMessage(String message, String extendedtext); 


    /**
     * Erzeugt einen Dialog, um den Benutzer eine Auswahl zu erm�glichen.
     * 
     * @param question (String der den Fragetext enth�lt)
     * @param answers (String-Array der die m�glichen Antworten enth�lt)
     * @return int (gibt den Index der gew�hlten Antwort zur�ck)
     */
  public int askUser(String question, String[] answers, int defaultValue);
  public int askUser(String caption, String question, String[] answers, int defaultValue);

  public int askUser(String question, String[] answers, String[] tooltips, int defaultValue);
  public int askUser(String caption, String question, String[] answers, String[] tooltips, int defaultValue);

  /**
   * Method requestFilename. �ffnet einen Dateiauswahldialog
   * 
   * @param startfolder (String: das Verzeichnis welches nach dem �ffnen angezeigt wird)
   * @param extensions (String-Array: die erlaubten Dateiendungen)
   * @param extensiondescription (String: Erkl�render Text zu den Dateiendungen)
   * @return String (die ausgew�hlte Datei)
   */
  public String requestFilename(String startfolder, String presetfile, String[] extensions, String extensiondescription, boolean forSave);

  /**
   * Method requestFilename. �ffnet einen Dateiauswahldialog
   * @param startfolder (String: das Verzeichnis welches nach dem �ffnen angezeigt wird)
   * @param extensions Liste von erlaubten Dateiendungen zu einem Filter
   * @param extensiondescriptions Liste mit erkl�rende Texten zu den Filtern
   * @return String (die ausgew�hlte Datei)
   */
    public String requestFilename(String startfolder, String presetfile, String[][] extensions, String[] extensiondescriptions, boolean forSave, boolean useAcceptAllFilter);

  /**
   * Lets user choose initial subcategories for a new address. 
   * 
   * <p>Returns <code>null</code> if operation is to be aborted.</p>
   * 
   * <p>This method is called when the saving operation of a new address
   * is started.</p>
   *  
   * @return
   */
  public List requestSubCategoriesForNewAddress(Address newAddress);
    

  /**                                            
   * Liefert die zuletzt bei einem requestFilename gew�hlten Extensions
   */
  public String[] getLastChoosedFileExtensions();

    /**
     * Method showMessage. �ffnet einen Dialog um eine Informationsmeldung
     * anzuzeigen. Wartet bis der Benutzer den "Ok"-Knopf gedr�ckt hat.
     * 
     * @param message Der Meldungstext
     */
  public void showInfo(String message);
  public void showInfo(String caption, String message);

    /*
     * Diese Methoden fragen die GUI nach bestimmten Zust�nden ab.
     */
    /**
     * �berpr�ft ob sich der Inhalt der GUI-Elemente gegen�ber dem �bergebenen
     * Address-Objekt ge�ndert hat
     * 
     * @param address (Das Address-Objekt zur Gegenpr�fung)
     * @return boolean (ist true wenn sich etwas ge�ndert hat)
     * @throws ContactDBException bei Datenzugriffsproblemen
     * @throws ContactControlException bei Datenverarbeitungsproblemen
     */
    public boolean isDirty(Address address) throws ContactDBException, ContactControlException;

    /**
     * Diese Methode liest den Inhalt der GUI in ein Address-Objekt.
     * 
     * @param address
     * @throws ContactDBException bei Datenzugriffsproblemen
     * @throws ContactControlException bei Datenverarbeitungsproblemen
     */
    public void getAddress(Address address) throws ContactDBException, ContactControlException, ContactGuiException;

    /**
     * Diese Methode fordert zum Ausf�hren eines Kanals eines
     * Versandauftrages auf. Dies kann mittels des R�ckgabewertes abgelehnt
     * werden.
     * 
     * @param batch der auszuf�hrende Versandauftrag
     * @param channel der auszuf�hrende Versandkanal
     * @return <code>true</code> gdw dieser Kanal bearbeitet werden kann.
     */

    /**
     * Diese Methode fordert zum Ausf�hren eines Kanals einer
     * Adresse auf. Dies kann mittels des R�ckgabewertes abgelehnt
     * werden.
     * 
     * @param address die auszuf�hrende Adresse
     * @param channel der auszuf�hrende Versandkanal
     * @param action die konkrete Aktion, eine der Konstanten
     *  an {@link ApplicationStarter}
     * @return <code>true</code> gdw dieser Kanal bearbeitet werden kann.
     */
    public boolean executeAddress(Address address, MailBatch.CHANNEL channel, String action);

    /**
     * Diese Methode verwirft die aktuelle Adresse
     */
    public boolean cancelAddress();

    /**
     * Diese Methode verwirft die aktuelle Adresse
     */
    public boolean saveAddress();

    public void registerTableComponent(String name, JComponent component);
    public void setTableView(String name);
    public void setTableText(String name, String text);
    public void setTableStaticText(String name, String text);
    public void setTableFilterText(String name, String text);
    
    public JLayeredPane getLayeredPane(); 

    /**
     * Returns a current search mode value.
     * The possible modes are:<p>
     * <li> {@link GUIListener#ADDRESSSET_KEINFILTER},
     * <li> {@link GUIListener#ADDRESSSET_TEXTSUCHE},
     * <li> {@link GUIListener#ADDRESSSET_VERSANDAUFTRAG_ALL},
     * <li> {@link GUIListener#ADDRESSSET_VERSANDAUFTRAG_NICHTZUGEORDNET},
     * <li> {@link GUIListener#ADDRESSSET_VERSANDAUFTRAG_EMAIL},
     * <li> {@link GUIListener#ADDRESSSET_VERSANDAUFTRAG_FAX},
     * <li> {@link GUIListener#ADDRESSSET_VERSANDAUFTRAG_POST},
     * <li> {@link GUIListener#ADDRESSSET_FEHLERHAFTE_EMAILS},
     * <li> {@link GUIListener#ADDRESSSET_SINGLE_ADDRESS}.
     * <p> 
     * @return int a mode value
     */
    public int getSearchMode();

    /**
     * Diese Methode liefert zu einem Schl�ssel einen konfigurierten Mitteilungsstring.
     *  
     * @param key Schl�ssel
     * @return zugeh�riger Mitteilungsstring; falls nicht gefunden, wird <code>'!' + key + '!'</code> geliefert.
     */
    public String getMessageString(String key);
    
    
    public PanelInserter getPanelInserter();
    
    /**
     * Diese Methode �ffnet einen Dialog, um ein ContactEntry zu erzeugen
     * 
     * @param subject
     * @param note
     * @param datum
     * @param duration
     * @param incoming
     * @param category
     * @param channel
     * @param link
     * @param linkType
     */
    public void showContactEntryDialog(String subject, String note, Date datum, int duration, boolean incoming, int category, int channel, int link, int linkType, User user, Address address, boolean readonly);
    
    
    /**
     * Selektiert einen Kalender anhand seiner ID 
     * 
     * @param calendarID die ID des Kalenders
     */
    public void selectCalendar(int calendarid);
    
    /**
     * Versucht eine Adresse anzuzeigen... 
     * 
     * @param address die anzuzeigende Adresse
     * @return liefert zur�ck ob es m�glich was die Adresse zu selektieren
     */
    public boolean showAddress(Address address);

    /**
     * Diese Methode teilt mit, ob die Adresstabelle gezeigt werden soll.
     * 
     * @param show legt fest, ob die Tabelle zu sehen sein soll.
     */
    public void showTable(boolean show);

    /**
     * Refresht die History-Tablle indem sie neu geladen wird...
     */
    public void refreshHistory();    
    
    /**
     * Aktiviert ein Tab
     * 
     * @param key gibt das zu aktivierende Tab an (TAB_CONTACT, TAB_SEARCH, TAB_EMAIL, TAB_EMAIL_RECEPTION, TAB_PROTECTED, TAB_EXTENDED, TAB_ADMIN, TAB_MAILING_LIST_CHOOSER, TAB_MAILING_LIST_ASSIGNMENT, TAB_MAIL_ORDERS, TAB_JOURNAL, TAB_ORGANIZER)
     */
    public void activateTab(String key);

    
    /** 
     * Returns true if a given tab is enabled.
     * 
     * @param key of a given tab
     *  
     * @exception NullPointerException if key is invalid not registered
     */
    public boolean isTabEnabled(String key);

    
    /**
     * Setzt je nach Parameter den Mauspfeil auf "Sanduhr" bzw. "normal";
     * sollte mit dem Parameter "true" aufgerufen werden, bevor eine
     * langwierige Operation ausgef�hrt wird. Nach Beendigung der
     * Operation muss die Methode erneut aufgerufen werden, jedoch
     * nun mit dem Parameter "false"
     * 
     * @param isWaiting wenn true: Sanduhr anzeigen.
     * @deprecated replaced by new TaskManager-concept.
     */
    public void setWaiting(boolean isWaiting);

    /**
     * Diese Methode teilt mit, dass eine Suche aktiviert oder deaktiviert worden ist.
     * 
     * @param isActive neuer Suchzustand.
     * @param info ein Infotext zu einer Suche, kann null sein.
     * @param type der Typ der Suche
     */
    public void setFilterActiveIcon(boolean isActive, String info, int type);


    public DataAccess getDataAccess();
    
    public void setToolBarVisible(boolean value);

    public void setSideMenuVisible(boolean value);
    

    /**
     * Shows an address on Contact Tab.
     */
    public void setDisplayedAddress(Address address);

    /**
     * Fires an Event, to update calendar view.
     */
    public void changeCalendar();    
}