package org.evolvis.nuyo.gui.admin;

import java.text.Collator;
import java.util.Comparator;

import org.evolvis.nuyo.db.Role;


/**
 * A class to compare the role objects.
 */
public class RoleComparator implements Comparator {
    
    public int compare(Object o1, Object o2){
        String s1 = ((Role) o1).getRoleName().toLowerCase();
        String s2 = ((Role) o2).getRoleName().toLowerCase();
    
        return Collator.getInstance().compare(s1,s2);
    }
}
