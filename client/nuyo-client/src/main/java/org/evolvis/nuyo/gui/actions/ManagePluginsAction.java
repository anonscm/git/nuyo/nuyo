/**
 * 
 */
package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.plugin.PluginManagementDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ManagePluginsAction extends AbstractGUIAction {

	public void actionPerformed(ActionEvent e) {
		new PluginManagementDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame()).setVisible(true);
	}
}
