/*
 * Created on 17.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.config.ConfigManager;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.EscapeDialog;

public class PersonalMessageDialog extends EscapeDialog {
    private static final TarentLogger logger = new TarentLogger( PersonalMessageDialog.class );

    private static final String PREF_KEY_lastUsedAddresses = StartUpConstants.PREF_BUG_REPORT_LAST_USED_EMAILS_KEY;

    private JTextArea textArea;
    private String bugReport;
    private String emailToAnswer;
    private User currentUser;
    private JButton okButton;
    private Set lastUsedAddresses = new HashSet();

    public PersonalMessageDialog( Frame parent, User user ) {
        super( parent, Messages.getString( "GUI_PersonalMessageDialog_Title" ), true );

        assert currentUser == null : "user parameter is empty";
        currentUser = user;

        init( parent );
    }

    private void init( Frame parent ) {
        getContentPane().setLayout( new BorderLayout() );
        getContentPane().add( createPanel(), BorderLayout.CENTER );
        pack();
        setLocationRelativeTo( parent );
        SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                setVisible( true );
                textArea.requestFocus();
            }
        });
    }

    public JPanel createPanel() {
        FormLayout layout = new FormLayout( "pref,4dlu,130dlu,4dlu,pref",//columns
                                            "pref,4dlu,130dlu,4dlu,pref,4dlu,pref" );//rows
        layout.setColumnGroups( new int[][] { { 1, 5 } } );
        layout.setRowGroups( new int[][] { { 5, 7 } } );
        PanelBuilder builder = new PanelBuilder( layout );
        builder.setDefaultDialogBorder();

        CellConstraints cc = new CellConstraints();

        JLabel messageLabel = new JLabel( Messages.getString( "GUI_PersonalMessageDialog_Message" ) );
        messageLabel.setHorizontalAlignment( JLabel.LEFT );
        builder.add( messageLabel, cc.xyw( 1, 1, 5, CellConstraints.CENTER, CellConstraints.DEFAULT ) );

        textArea = new JTextArea();
        textArea.setEditable( true );
        textArea.setLineWrap( true );
        textArea.setWrapStyleWord( true );
        JScrollPane jscrollpane1 = new JScrollPane();
        jscrollpane1.setViewportView( textArea );
        builder.add( jscrollpane1, cc.xyw( 1, 3, 5, CellConstraints.FILL, CellConstraints.FILL ) );

        JLabel emailLabel = new JLabel( Messages.getString( "GUI_PersonalMessageDialog_Label_AnswerEmail" ) );
        builder.add( emailLabel, cc.xy( 1, 5 ) );

        JComboBox comboBox = createComboBox();
        builder.add( comboBox, cc.xy( 3, 5, CellConstraints.FILL, CellConstraints.FILL ) );

        okButton = new JButton( Messages.getString( "Button_OK" ) );
        okButton.addActionListener( new button_ok_clicked() );
        okButton.setToolTipText( Messages.getString( "GUI_PersonalMessageDialog_ToolTip_OkButton" ) );
        getRootPane().setDefaultButton( okButton );
        //update ok button enabled state
        synchronizeOkButtonWithComboBox( comboBox );
        builder.add( okButton, cc.xy( 1, 7 ) );

        JButton cancelbutton = new JButton( Messages.getString( "Button_Cancel" ) );
        cancelbutton.addActionListener( new button_cancel_clicked() );
        cancelbutton.setToolTipText( Messages.getString( "GUI_PersonalMessageDialog_ToolTip_CancelButton" ) );
        builder.add( cancelbutton, cc.xy( 5, 7 ) );

        return builder.getPanel();
    }

    private JComboBox createComboBox() {
        JComboBox mailComboBox = new JComboBox();
        mailComboBox.setEditable( true );
        mailComboBox.setToolTipText( Messages.getString( "GUI_PersonalMessageDialog_ToolTip_ComboBox" ) );
        //set current user email if known
        initComboBox( mailComboBox );
        //insert always an empty line, so that the use don't have to delete anything 
        mailComboBox.addItem( "" );
        return mailComboBox;
    }

    private void initComboBox( JComboBox mailComboBox ) {
        String[] lastUsedAddressesArray = null;
        //load last used email addresses
        String addressesAsString = ConfigManager.getPreferences().get( PREF_KEY_lastUsedAddresses, null );
        System.out.println( addressesAsString );
        if ( addressesAsString == null ) {
            //or use associated one 
            mailComboBox.addItem( getOfficeEmail() );
        }
        else {
            //add loaded addresses
            lastUsedAddressesArray = addressesAsString.split( ":" );
            for ( int i = lastUsedAddressesArray.length - 1; i >= 0; i-- ) {
                String nextItem = lastUsedAddressesArray[i];
                if ( !nextItem.equals( "" ) ) {
                    mailComboBox.addItem( lastUsedAddressesArray[i] );
                    lastUsedAddresses.add( lastUsedAddressesArray[i] );
                }
            }
        }
    }

    private void synchronizeOkButtonWithComboBox( JComboBox comboBox ) {
        comboBox.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e ) {
                final JComboBox cb = (JComboBox) e.getSource();
                String newSelection = (String) cb.getSelectedItem();
                emailToAnswer = newSelection;
                SwingUtilities.invokeLater( new Runnable() {
                    public void run() {
                        //update 'ok' button state
                        okButton.setEnabled( !emailToAnswer.equals( "" ) && emailToAnswer.contains( "@" ) );
                        if ( okButton.isEnabled() ) {
                            //update set of used addresses
                            lastUsedAddresses.add( emailToAnswer );
                        }
                    }
                } );
            }
        } );
    }

    private String getOfficeEmail() {
        String emailaddr = "";//default empty string
        try {
            if ( currentUser == null )
                return "";
            Address address = currentUser.getAssociatedAddress();
            if ( address != null ) {
                emailaddr = address.getEmailAdresseDienstlich();
                if ( emailaddr != null ) {
                    return emailaddr.trim();
                }
            }
        }
        catch ( ContactDBException e ) {
            logger.warningSilent( "[!] failed getting associated address for a current user", e );
        }
        return emailaddr;
    }

    private class button_ok_clicked implements ActionListener {
        public void actionPerformed( ActionEvent e ) {
            bugReport = textArea.getText();
            saveLastUsedAdresses();
            closeWindow();
        }
    }

    private class button_cancel_clicked implements ActionListener {
        public void actionPerformed( ActionEvent e ) {
            bugReport = null;
            emailToAnswer = null;
            closeWindow();
        }
    }

    private void saveLastUsedAdresses() {
        StringBuilder buffer = new StringBuilder();
        boolean moreThanOne = false;
        for ( Iterator iter = lastUsedAddresses.iterator(); iter.hasNext(); ) {
            if ( moreThanOne )
                buffer.append( ":" );
            Object currentItem = iter.next();
            if ( !currentItem.equals( "" ) )
                buffer.append( currentItem );
            if ( !moreThanOne )
                moreThanOne = true;
        }
        ConfigManager.getPreferences().put( PREF_KEY_lastUsedAddresses, buffer.toString() );
    }

    public String getResult() {
        return bugReport;
    }

    public String getAnswerAddress() {
        return emailToAnswer;
    }
}
