

package org.evolvis.nuyo.gui.categorytree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;

import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.SubCategory;

import de.tarent.commons.utils.SerializationHelper;

/**
 * This class denotes a category tree selection state.
 * <p>
 * It capsules the way the category tree manages its state. Through instances of
 * this class a tree's state can be retrieved as well as being set.
 * </p>
 * <p>
 * Besides the state information that affects the search filter the category
 * tree's visibility and node filter setting are managed.
 * </p>
 * @author Robert Schuster
 */
public final class SelectionState implements Cloneable
{
  public static final SelectionState DEFAULT = new SelectionState();

  List positives;

  List negatives;

  Operation op;
  
  Visibility vis;
  
  String filter;

  private SelectionState(List positives, List negatives, Operation op,
		                 Visibility vis, String filter)
  {
    this.positives = positives;
    this.negatives = negatives;
    this.op = op;
    this.vis = vis;
    this.filter = filter;
  }

  private SelectionState()
  {
    this(Collections.EMPTY_LIST, Collections.EMPTY_LIST, Operation.OR, Visibility.ALL, "");
  }

  /**
   * Creates a {@link SelectionState} instance for two given lists of
   * subcategories and an {@link Operation} instance.
   * @param posSubCat
   * @param negSubCat
   * @param op
   * @return
   */
  static SelectionState create(List/* <SubCategory> */posSubCat,
                               List/* <SubCategory> */negSubCat,
                               Operation op, Visibility vis,
                               String filter)
  {
    List pos = new ArrayList();
    Iterator ite = posSubCat.iterator();

    while (ite.hasNext())
      {
        SubCategory sub = (SubCategory) ite.next();
        pos.add(sub.getIdAsInteger());
      }

    List neg = new ArrayList();
    ite = negSubCat.iterator();

    while (ite.hasNext())
      {
        SubCategory sub = (SubCategory) ite.next();
        neg.add(sub.getIdAsInteger());
      }

    return new SelectionState(pos, neg, op, vis, filter);
  }

  /**
   * Updates the 'or', 'and' and 'not' list in the given address list parameters
   * instance.
   * @param o
   */
  public void updateAdressListParameters(AddressListParameter alp)
  {
    List orList = (op == Operation.OR) ? positives : Collections.EMPTY_LIST;
    List andList = (op == Operation.AND) ? positives : Collections.EMPTY_LIST;

    alp.setConjunctionSubcategories(andList);
    alp.setDisjunctionSubcategories(orList);
    alp.setNegatedSubcategories(new ArrayList(negatives));
  }

  public static SelectionState restoreFromPreferences(Preferences baseNode)
  {
    try
    {
      Operation op = Operation.get(baseNode.get("operation", Operation.OR.toString()));
      List positives = SerializationHelper.deserializeList(baseNode.getByteArray("positives", null));
      List negatives = SerializationHelper.deserializeList(baseNode.getByteArray("negatives", null));
      
      Visibility vis = Visibility.get(baseNode.get("visibility", Visibility.ALL.toString()));
      String filter = baseNode.get("filter", "");
      
      return new SelectionState(positives, negatives, op, vis, filter);
    }
    catch (IllegalStateException e)
    {
      return SelectionState.DEFAULT;
    }
  }

  public void storeInPreferences(Preferences baseNode)
  {
    baseNode.put("operation", op.toString());
    baseNode.putByteArray("positives", SerializationHelper.serializeList(positives));
    baseNode.putByteArray("negatives", SerializationHelper.serializeList(negatives));
    
    baseNode.put("visibility", vis.toString());
    baseNode.put("filter", filter);
  }

}
