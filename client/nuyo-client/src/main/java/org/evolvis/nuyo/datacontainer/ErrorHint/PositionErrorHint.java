/*
 * Created on 14.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorHint;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PositionErrorHint extends ErrorHintAdapter
{
  private int m_iPosition;
  
  public PositionErrorHint(String name, String description, Exception e, GUIElement guielement, DataContainer datacontainer, int pos)
  {
    super(name, description, null, guielement, datacontainer);
    this.setPosition(pos);
  }
  
  public PositionErrorHint(GUIElement guielement, DataContainer datacontainer, int pos)
  {
    super("Syntaxfehler", "Fehler an Position " + pos, null, guielement, datacontainer);
    this.setPosition(pos);
  }
  
  public void setPosition(int pos)
  {
    m_iPosition = pos;
  }
    
  public int getPosition()
  {
    return m_iPosition;
  }    
}
