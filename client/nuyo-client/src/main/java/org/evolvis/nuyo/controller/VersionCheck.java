/*
 * Created on 09.11.2004
 */
package org.evolvis.nuyo.controller;

/**
 * @author simon
 *
 */
public class VersionCheck {
    
    /**
     * @author simon
     *
     */

    private String _Reason;
    
    public VersionCheck(String ClientVersion, String DataBaseVersion) throws VersionsIncompatibleException, VersionsNotFullyCompatibleException{
        
        if (false){
            _Reason = "Client (Version: " + ClientVersion + ")\nDatenbank (Version: " + DataBaseVersion + ")\n- Es wird Clientversion > XXX ben�tigt\n";
            throw new VersionsIncompatibleException();
        }else if (false){
            _Reason = "Client (Version: " + ClientVersion + ")\nDatenbank (Version: " + DataBaseVersion + ")\n- Problem1\n- Problem2\n";
            throw new VersionsNotFullyCompatibleException();        
        }
    }
      
    public class VersionsIncompatibleException extends Exception {
        
       /* (non-Javadoc)
       * @see java.lang.Throwable#getMessage()
       */
        public String getMessage() {

            return _Reason ;
        }
    }    
    public class VersionsNotFullyCompatibleException extends Exception {
        
       /* (non-Javadoc)
       * @see java.lang.Throwable#getMessage()
       */
        public String getMessage() {

            return _Reason ;
        }
    }    

}
