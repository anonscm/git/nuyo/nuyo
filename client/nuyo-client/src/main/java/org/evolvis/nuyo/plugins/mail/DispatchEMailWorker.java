/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.utils.TaskManager;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class DispatchEMailWorker
{
	private final static TarentLogger logger = new TarentLogger(DispatchEMailWorker.class);
	
	public static void sendMessage(String pSender, String pSubject, String pBody, Integer pActionID, Iterator pAttachments, boolean pDelayedDispatch, boolean pCopyToSender)
	{
		// package attachments
		
		// the data of the files will be put as byte-array into this list
		ArrayList attachmentData = new ArrayList();
		// the file-names will be put into this separate list
		ArrayList attachmentNames = new ArrayList();

		while(pAttachments.hasNext())
		{
			File file = (File)pAttachments.next();

			try
			{
				FileInputStream fileStream = new FileInputStream(file);

				byte[] fileData = new byte[fileStream.available()];

				fileStream.read(fileData);

				attachmentData.add(fileData);
				attachmentNames.add(file.getName());
			} catch (FileNotFoundException e)
			{
				logger.warning("Failure when preparing attachments", e);
			} catch (IOException e)
			{
				logger.warning("Failure when preparing attachments", e);
			}
		}
		
		SerialMailTask mailTask = new SerialMailTask(pActionID, pSubject, pBody, pSender, attachmentNames, attachmentData, pDelayedDispatch, pCopyToSender);
		TaskManager.getInstance().register(mailTask, Messages.getString("GUI_EMAIL_TRANSFER_PROGRESS_DIALOG_TASK_NAME"), false);
	}
}
