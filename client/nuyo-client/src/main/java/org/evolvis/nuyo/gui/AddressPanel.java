/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.TarentPanel;



/**
 * <code>ContainerProvider</code> implementation which is to be used
 * for the standard and extended fields of an address.
 * 
 * <p>The container provides the two areas of a vertical splitpane.
 * At the moment instances can be created only by using the factory
 * methods {@link #createStandardAddressPanel()}
 * and {@link #createExtendedAddressPanel()}.</p>
 * 
 * @author niko
 * @author Robert Schuster
 */
class AddressPanel extends JPanel implements ContainerProvider
{
  public final static String CONTAINER_STD_LEFT = "STD1";
  public final static String CONTAINER_STD_RIGHT = "STD2";

  public final static String CONTAINER_EXT_LEFT = "EXT1";
  public final static String CONTAINER_EXT_RIGHT = "EXT2";

  private TarentPanel rightPanel;
  private TarentPanel leftPanel;
  private JSplitPane splitPane;
  
  private Preferences preferences;
  
  private String containerName1, containerName2;
  
  private String prefsDividerKey;
  
  private String providerName;
  
  public static AddressPanel createStandardAddressPanel()
  {
    return new AddressPanel(CONTAINER_STD_LEFT, CONTAINER_STD_RIGHT, StartUpConstants.PREF_ADRESS_PANEL_DIVIDER_KEY);
  }
  
  public static AddressPanel createExtendedAddressPanel()
  {
    return new AddressPanel(CONTAINER_EXT_LEFT, CONTAINER_EXT_RIGHT, StartUpConstants.PREF_EXTENDED_TOP_PANEL_DIVIDER_KEY);
  }
  
  /** Hier werden die Identifikatoren f�r die Textfelder hinterlegt,
   *  die bei FocusLost mit Inhalts�nderungen den Dublettencheck triggern. */
  private AddressPanel(String containerName1, String containerName2, String prefsDividerKey)
  {
    this.containerName1 = containerName1;
    this.containerName2 = containerName2;
    this.prefsDividerKey = prefsDividerKey;
    providerName = this.getClass().getName() + containerName1 + " - " + containerName2;
    
    GUIListener guiListener = ApplicationServices.getInstance().getActionManager(); 
    preferences = guiListener.getPreferences(StartUpConstants.PREF_PANELS_NODE_NAME);        
    
    setLayout(new BorderLayout()); 
        
    leftPanel = new TarentPanel();
    
    rightPanel = new TarentPanel();
            
    splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);

    splitPane.setDividerLocation(0.5);
    splitPane.setDividerSize(5);
    
    this.add(splitPane, BorderLayout.CENTER);
  }

  /**
   * Returns a unique name per instance.
   * 
   * <p>As this class is used for the standard and extended address
   * field this is important for the <code>AdvancedPanelInserter</code>
   * to work correctly.</p>
   */
  public String getContainerProviderName()
  {
    return providerName;     
  }
    
  public Map getContainers()
  {
    Map map = new HashMap();
    map.put(containerName1, leftPanel);
    map.put(containerName2, rightPanel);
    return map;
  }
  
  // ------------------------------------------------------------------------------------  
  
  private SplitPaneComponentListener m_oSplitPaneComponentListener = null;
  
  public void postFinalRealize()
  {
	  
  }
  
  public void setDividerPosition()
  {    
//      double pos = preferences.getDouble(prefsDividerKey, 0.4);
//      
//      if (pos >= 0 && pos <= 1)
//          splitPane.setDividerLocation(pos);
//      if (m_oSplitPaneComponentListener == null)
//      {
//        m_oSplitPaneComponentListener = new SplitPaneComponentListener();
//        leftPanel.addComponentListener(m_oSplitPaneComponentListener);        
//      }
	  // FIXME hack for setting correct (visual) divider position
	  int width = splitPane.getParent().getParent().getParent().getParent().getParent().getWidth();
	  splitPane.setDividerLocation(width / 2);
  }
  
  private class SplitPaneComponentListener implements ComponentListener
  {
    public void componentHidden(ComponentEvent e) {}
    public void componentMoved(ComponentEvent e) {}
    public void componentShown(ComponentEvent e) {}
    
    public void componentResized(ComponentEvent e)
    {
      int location = splitPane.getDividerLocation();
      int width = splitPane.getWidth();
      if (width > 0)
      {  
        double loc = ((double)location) / ((double)width);
        preferences.putDouble(prefsDividerKey, loc);        
      }      
    }
  }
  
  // ------------------------------------------------------------------------------------  
  
}
