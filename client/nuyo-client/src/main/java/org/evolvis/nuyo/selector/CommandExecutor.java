package org.evolvis.nuyo.selector;

import java.io.IOException;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.NonPersistentAddresses;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.commons.datahandling.entity.EntityList;

/**
 * The CommandExecutor class parses a command string that consists of the 
 * shell-command and an arbitrary number of placeholders. 
 * The CommandExecutor parses the String and replaces the placeholders with 
 * the specified data. It then executes the shell-command with the 
 * just created parameter-String as argument.
 * 
 * @author Steffi Tinder, tarent GmbH
 *
 */

public class CommandExecutor {
	
	private static Logger logger = Logger.getLogger(CommandExecutor.class.getName());
	
	public static final String PREFIX = "%{";
	public static final String POSTFIX = "}";
	private static final String DELIMITER_SEMICOLON = ";";
	private static final String DELIMITER_COMMA= ",";
	private static final String DELIMITER_BLANK = " ";
	private static final String DEFAULT_DELIMITER = DELIMITER_BLANK;
	
	public static final int ERR_INSUFFICIENT_DATA=2;
	public static final int ERR_EXECUTION_FAIL=1;
	
	private String command;
	private EntityList entities;
	private EntityList noDataEntities;
	private String delimiter;
	
	public CommandExecutor(EntityList entities, String commandString){
		this(entities,commandString,DEFAULT_DELIMITER);
	}
	
	public CommandExecutor(EntityList entities, String commandString, String delim){
		
		this.delimiter = delim;
		this.entities=entities;
		noDataEntities=new NonPersistentAddresses();
		command=commandString;
	}
	
	public String replaceData(String command, String delimiter){
		
		StringBuffer buffer = new StringBuffer();
			
		int postfixIndex;
		int prefixIndex = command.indexOf(PREFIX);
		int currentIndex =0;
		while (prefixIndex!=-1){
			//as long as you find some substrings that equal the prefix:
			postfixIndex=command.indexOf(POSTFIX,prefixIndex);
			if (postfixIndex!=-1){
				
				buffer.append(command.substring(currentIndex,prefixIndex));
				String placeholder=command.substring(prefixIndex+PREFIX.length(),postfixIndex);
				
				buffer.append(getData(placeholder,delimiter));
				currentIndex=postfixIndex+1;
			}
			prefixIndex=command.indexOf(PREFIX,postfixIndex);
		}
		buffer.append(command.substring(currentIndex,command.length()));
		return buffer.toString();
	}
	
	private String getData(String propertyKey, String delimiter) {
		
		StringBuffer buffer = new StringBuffer();
		Entity entity;
		String attribute;
		noDataEntities = new NonPersistentAddresses();
		try {
			for(int i=0;i<entities.getSize();i++){
				
				entity=(Entity)entities.getEntityAt(i);
				attribute = ((String)entity.getAttribute(propertyKey));
				//some attributes return null:
				if (attribute==null) attribute=""; else attribute=attribute.trim();
				if (attribute.equals("")){
					logger.info("keine Adressdaten vorhanden f�r Attribut "+propertyKey);
					noDataEntities.addEntity(entity);
				} else {
					buffer.append(attribute);
					if (i<entities.getSize()-1) buffer.append(delimiter);
				}
			}//end of for
		} catch (EntityException e) {
			logger.warning("Fehler beim extrahieren der Daten, evtl wurde eine ung�ltige Eigenschaft angefordert: "+propertyKey);
		}
		return buffer.toString();
	}
	
	protected void runCommand(String cmd) throws IOException {
		logger.info("versuche Kommando auszuf�hren: "+cmd);
		Process p = Runtime.getRuntime().exec(cmd);
	}
	
	
	public int execute(){
		
		String cmd = replaceData(command,delimiter);
		try {
			runCommand(cmd);
		} catch (IOException e) {
			logger.warning("CommandExecutor: Fehler beim Starten des Programms");
			ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Aufruf des Kommandos",e);
			return ERR_EXECUTION_FAIL;
		}
		if (noDataEntities.getSize()!=0) return ERR_INSUFFICIENT_DATA;
		return 0;
	}
	
	public EntityList getEntitiesWithInsufficientData(){
		return noDataEntities;
	}
	
}
