/* $Id: AddressChangedInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * 
 * Created on 17.10.2003
 */
package org.evolvis.nuyo.messages;

import org.evolvis.nuyo.db.Address;

/**
 * Dieses Ereignis zeigt an, dass sich eine Adresse ge�ndert hat.
 * 
 * @author mikel
 */
public class AddressChangedInfo extends AddressInfo {
    /*
     * Konstruktoren
     */
    /**
     * @param source Ereignisquelle
     * @param address betroffene Addresse
     */
    public AddressChangedInfo(Object source, Address address) {
        super(source, address);
    }

}
