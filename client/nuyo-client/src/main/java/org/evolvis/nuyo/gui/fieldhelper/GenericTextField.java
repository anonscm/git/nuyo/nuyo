/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.Color;
import java.awt.Font;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class GenericTextField extends ContactAddressTextField
{
  private TarentWidgetLabel label;
  private TarentWidgetTextField   textField; 
  
  private String key;
  private Object addressKey;
  private String tooltipKey;
  private String labelKey;
  private int contextID;
  private int maxLength;
  
  
  public GenericTextField(String panelkey, Object adresskey, int context, String tooltipkey, String labelkey, int maxlength)
  {
    key = panelkey;
    addressKey = adresskey;
    tooltipKey = tooltipkey;
    labelKey = labelkey;
    maxLength = maxlength;
    contextID = context;
  }
  
  public String getFieldName()
  {
    if (fieldName != null) return fieldName; 
    else 
    {
      if (labelKey != null) return Messages.getString(labelKey);
      else return null;
    }
  }
  
  public String getFieldDescription()
  {
    if (fieldDescription != null) return fieldDescription;
    else 
    {
      if (tooltipKey != null) return Messages.getString(tooltipKey);
      else return null;
    }
  }
  
  public int getContext()
  {
    return contextID;
  }
  
  
  public void postFinalRealize()
  {    
  }  
  
  public TarentWidgetLabel getLabel()
  {
    return label;
  }
  
  public TarentWidgetTextField getTextField()
  {
    return textField;
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createPanel(tooltipKey, labelKey, maxLength, widgetFlags);
    
    return panel;
  }

  public String getKey()
  {
    return key;
  }

  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      textField.setEditable(true);
    }
    else
    {  
      textField.setEditable(false);
      textField.setBackground(Color.WHITE);
    }
  }

  public void setDataFont(Font font)
  {
    textField.setFont(font);      
    label.setFont(font);      
  }

  public void setData(PluginData data)
  {
    Object value = data.get(addressKey);
    if (value == null) textField.setText("");
    else textField.setText(value.toString());
  }

  public void getData(PluginData data)
  {
    data.set(addressKey, textField.getText());
  }

  public boolean isDirty(PluginData data)
  {
    if (!((textField.getText().equals("")) &&  (data.get(addressKey) == null ))) //$NON-NLS-1$
      if (!(textField.getText().equals(data.get(addressKey)))) return(true);            

    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  private EntryLayoutHelper createPanel(String tooltipkey, String labelkey, int maxlength, String widgetFlags)
  {        
    textField = createTextField(getFieldDescription(), maxlength);
    label = new TarentWidgetLabel(getFieldName());     
    return(new EntryLayoutHelper(label, textField, widgetFlags));
  }

public void setDoubleCheckSensitive(boolean issensitive) {
	setDoubleCheckSensitive(textField, issensitive);	
}

}
