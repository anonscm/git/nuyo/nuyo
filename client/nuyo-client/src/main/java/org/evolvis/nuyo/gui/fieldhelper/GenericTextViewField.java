/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.Font;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 */
public class GenericTextViewField extends ContactAddressTextField
{
  protected TarentWidgetLabel m_oLabel;
  protected TarentWidgetTextField   m_oTextfield; 
  
  protected String m_sKey;
  protected Object m_AdressKey;
  protected String m_sTooltipKey;
  protected String m_sLabelKey;
  protected int m_iMaxLength;
  private int m_iContext;
  
  
  
  public GenericTextViewField(String panelkey, Object adresskey, int context, String tooltipkey, String labelkey, int maxlength)
  {
    m_sKey = panelkey;
    m_AdressKey = adresskey;
    m_sTooltipKey = tooltipkey;
    m_sLabelKey = labelkey;
    m_iMaxLength = maxlength;
    m_iContext = context;        
  }
  
  public String getFieldName()
  {
    if (fieldName != null) return fieldName; 
    else return Messages.getString(m_sLabelKey);
  }
  
  public String getFieldDescription()
  {
    if (fieldDescription != null) return fieldDescription;
    else return Messages.getString(m_sTooltipKey);    
  }
  
  public int getContext()
  {
    return m_iContext;
  }
  
  
  public boolean isWriteOnly()
  {
    return true;  
  }
  
  public void postFinalRealize()
  {    
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createPanel(m_sTooltipKey, m_sLabelKey, m_iMaxLength, widgetFlags);
    
    return panel;
  }

  public String getKey()
  {
    return m_sKey;
  }

  public void setEditable(boolean iseditable)
  {
  }

  public void setDataFont(Font font)
  {
    m_oTextfield.setFont(font);      
    m_oLabel.setFont(font);      
  }

  public void setData(PluginData data)
  {
    Object value = data.get(m_AdressKey);
    if (value != null) m_oTextfield.setText(value.toString());
    else m_oTextfield.setText("");
  }

  public void getData(PluginData data)
  {
    data.set(m_AdressKey, m_oTextfield.getText());
  }

  public boolean isDirty(PluginData data)
  {
    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  private EntryLayoutHelper createPanel(String tooltipkey, String labelkey, int maxlength, String widgetFlags)
  {    
    m_oTextfield = createTextField(getFieldDescription(), maxlength);
    m_oTextfield.setWidgetEditable(false);  
    
    m_oLabel = new TarentWidgetLabel(getFieldName());     
    return(new EntryLayoutHelper(m_oLabel, m_oTextfield, widgetFlags));
  }

public void setDoubleCheckSensitive(boolean issensitive) {
	setDoubleCheckSensitive(m_oTextfield, issensitive);
}

}
