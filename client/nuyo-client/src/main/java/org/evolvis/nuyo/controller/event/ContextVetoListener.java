/* $Id: ContextVetoListener.java,v 1.1 2006/12/11 19:12:22 aleksej Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.event;

import org.evolvis.nuyo.db.veto.Veto;

/**
 * This Interface will be used to handle Veto Exception on edit operation.<p>
 * <p>
 * WARNING: Veto is a bad strategy to disable GUI Elements.<p>
 * 
 * TODO: Implement another strategy:<p> 
 * i.e. check rights on category and contacts for table selection
 * and then update visibility of GUI Elements.
 * <p>
 * @see org.evolvis.nuyo.gui.MainFrameExtStyle#handleContextVeto(Veto)
 * 
 * @author mikel
 */
public interface ContextVetoListener {

    /**
     * Setzt die GUI-Elemente je nach �bergebenem Parameter auf "editierbar"
     * bzw. "nur lesen".
     * 
     * @param veto ein Veto, das Einschr�nkungen der Editierbarkeit enthalten
     *  kann.
     * @see org.evolvis.nuyo.db.veto.Restriction
     */
    public void handleContextVeto(Veto veto);
}
