/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * @author niko
 */
public class TarentWidgetButton extends JButton implements TarentWidgetInterface
{
  public TarentWidgetButton()
  {
    super();
  }
  
  public TarentWidgetButton(String text)
  {
    this();
    this.setData(text);
  }
  
  public TarentWidgetButton(ImageIcon icon)
  {
    this();
    this.setData(icon);
  }
  
  public TarentWidgetButton(String text, ImageIcon icon)
  {
    this();
    this.setData(text);
    this.setData(icon);
  }

  public TarentWidgetButton(ActionListener al)
  {
    super();
    this.addActionListener(al);
  }
  
  public TarentWidgetButton(String text, ActionListener al)
  {
    this();
    this.addActionListener(al);
    this.setData(text);
  }
  
  public TarentWidgetButton(ImageIcon icon, ActionListener al)
  {
    this();
    this.addActionListener(al);
    this.setData(icon);
  }
  

	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    if (data instanceof String)
    {
      this.setText((String)data);
    }
    else if (data instanceof ImageIcon)
    {
      this.setIcon((ImageIcon)data);
    }
	}

	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
    if (this.getText() != null) return(this.getText());
    else return(this.getIcon());
	}


  public JComponent getComponent()
  {
    return(this);
  }

  
  public void setLengthRestriction(int maxlen)
  {
  }
  
  public void setWidgetEditable(boolean iseditable)
  {
  }

  public boolean isEqual(Object data)
  {
    return(false);
  }

  
  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  
}
