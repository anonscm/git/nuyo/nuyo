package org.evolvis.nuyo.plugins.calendar;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.manager.CommonDialogServices;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.CalendarCollection;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.fieldhelper.CalenderManager;
import org.evolvis.nuyo.logging.TarentLogger;


public class CalendarsSelectionModelImpl implements CalendarsSelectionModel, CalendarClickedListener {

    private static final TarentLogger logger = new TarentLogger(CalendarsSelectionModelImpl.class);
    private static final long serialVersionUID = -8500429913147645586L;

    private final Map actionListenersMap;// each calendar has an unique action listener, that notifies model (CalendarClickedListener) about clicked event
    private final Map selectionListenersMap;// the model fires update event to assigned calendar listeners  
    private final Preferences selectionPreferences;
    private final CommonDialogServices dialogServices;
    private final CalenderManager calendarEventsManager;
    private CalendarCollection allCachedCalendarsCollection;
    private CalendarCollection selectedCalendarsCollection;
    private Integer currentCalendarID;
    
    public CalendarsSelectionModelImpl(final CalenderManager aCalendarEventsManager) {
        final GUIListener guiListener = ApplicationServices.getInstance().getActionManager();
        selectionPreferences = guiListener.getPreferences(StartUpConstants.PREF_CALENDAR_SELECTION);
        actionListenersMap = new HashMap();
        selectionListenersMap = new HashMap();
        dialogServices = ApplicationServices.getInstance().getCommonDialogServices();
        calendarEventsManager = aCalendarEventsManager;
        loadCalendars(guiListener);
    }

    /**
     * Select the calendar with the specified id and mark it as selected in the preferences
     */
    public void selectCalendar(final Integer aCalendarID) {
        setCalendarSelected(aCalendarID, true);
    }

    private void fireCalendarSelectionState(final Integer calendarID,final boolean isSelected) {
        if(selectionListenersMap.containsKey(calendarID)){
            final List listeners = (List) selectionListenersMap.get(calendarID);
            logger.info("[fireCalendarSelectionState] id - #listeners: " + calendarID + " - " + String.valueOf(listeners.size()));
            for(Iterator iterator = listeners.iterator();iterator.hasNext();){
                final CalendarSelectionListener nextListener = (CalendarSelectionListener) iterator.next();
                try {
                    nextListener.setSelected(isSelected);
                    logger.info("[listener] selected: "  + String.valueOf(isSelected));
                }
                catch ( Throwable e ) {
                    logger.warningSilent("[!] [CalendarSelectionModel]: selection menu item not updated: " + e.getClass().getName(), e);
                }
            }
            fireCalendarViewUpdate();
        }
    }

    private boolean existsCalendar(final Integer calendarID) {
        return null != allCachedCalendarsCollection.getCalendar(calendarID.intValue());
    }

    //This method is thread safe: all runtime exceptions will be handled.
    private void fireCalendarViewUpdate() {
        if (calendarEventsManager != null) {
            calendarEventsManager.fireChangeCalendar(null);
        } else logger.warningSilent("[!] couldn't fire 'change calendar' event");
    }

    public void fillCalendarMenu(final CollapsingMenu menu ) {
        try {
            final Collection calendars = allCachedCalendarsCollection.getCollection();
            final Iterator calendarsIterator = calendars.iterator();
            //init menu
            while ( calendarsIterator.hasNext() ) {
                final Calendar nextCalendar = (Calendar) calendarsIterator.next();
                menu.addMenuItem( createMenuItem( nextCalendar ) );
            }
            //init view
            new Thread("calendar-view-init-thread"){
                public void run() {
                    fireCalendarViewUpdate();
                }
            }.start();
        }
        catch ( ContactDBException e ) {
            logger.severe( "Failed filling collapsing menu with entries: ContactDBException", e );
        }
        catch ( Throwable e ) {
            logger.severe( "Failed filling collapsing menu with entries: " + e.getClass().getName(), e );
        }
    }

    /** Returns a panel with calendar menu items. */
    public JPanel getMenuItemsPanel() {
        //create panel
        final JPanel itemsPanel = new JPanel();
        itemsPanel.setLayout( new BoxLayout( itemsPanel, BoxLayout.Y_AXIS ) );
        //fill panel
        try {
            final Collection calendars = allCachedCalendarsCollection.getCollection();
            final Iterator calendarsIterator = calendars.iterator();
            while ( calendarsIterator.hasNext() ) {
                final Calendar nextCalendar = (Calendar) calendarsIterator.next();
                itemsPanel.add( createMenuItem( nextCalendar ) );
            }
        }
        catch ( ContactDBException e ) {
            logger.warningSilent( "[!] Failed creating calendar menu items: DB exception: " + e.getExceptionId(), e );
        }
        return itemsPanel;
    }

    //creates a new menu item add registers it as selection listener in order to notify about updates
    private CalendarMenuItem createMenuItem( final Calendar nextCalendar ) throws ContactDBException {
        final Integer calendarID = new Integer(nextCalendar.getId());
        final CalendarMenuItem menuItem = CalendarMenuItem.create(nextCalendar,getCalendarActionListener(calendarID));
        setCurrentCalendarID(calendarID);
        addSelectionListener(calendarID, menuItem);
        if(isCalendarSelected(calendarID)){
            menuItem.setSelected(true);
            selectedCalendarsCollection.add(allCachedCalendarsCollection.getCalendar(calendarID.intValue()));
        }
        return menuItem;
    }

    //register menu item as selection listener
    private void addSelectionListener( final Integer calendarID, final CalendarSelectionListener selectionListener ) {
        final List listeners;
        if( selectionListenersMap.containsKey(calendarID) ) {
            //get list of listeners
            listeners = (List) selectionListenersMap.get(calendarID); 
        } else {//create a new list
            listeners = new ArrayList();
            selectionListenersMap.put(calendarID,listeners);
        }
        listeners.add(selectionListener);
    }
    
    private boolean isCalendarSelected( Integer id ) {
        synchronized(selectionPreferences){
           return selectionPreferences.getBoolean(id.toString(),false);    
        }
    }

    private ActionListener getCalendarActionListener(final Integer id ) {
        if(!actionListenersMap.containsKey(id)){
            actionListenersMap.put(id,new CalendarMenuItemActionListener(id,this));
        }        
        return (ActionListener) actionListenersMap.get(id);
    }

    private String getLengthRestrictedString(final String text, int maxlen) {
        if (text == null)
            return "";
        if (text.length() > maxlen) {
            int lastwhitespace = text.substring(0, maxlen).lastIndexOf(" ");
            if (lastwhitespace == -1) {
                lastwhitespace = maxlen;
            }
            return (text.substring(0, lastwhitespace) + "\n" + text.substring(lastwhitespace)); //$NON-NLS-1$
        } else
            return (text);
    }

    /**
     * @return CalendarCollection Gibt die aktuell ausgew�hlten Kalender zur�ck.
     */
    public CalendarCollection getCalendarCollection() {
        return selectedCalendarsCollection;
    }

    private void loadCalendars(GUIListener guiListener) {
        try {
            User user = guiListener.getUser( guiListener.getCurrentUser(), true);
            selectedCalendarsCollection = CalendarCollection.empty(user);
            allCachedCalendarsCollection = CalendarCollection.loadAssignedCalendars(user);
        }
        catch ( ContactDBException e ) {
            logger.severe(" Failed loading calendars: DB error: " + e.getExceptionId(), e);
        }
    }
    
    public Integer getCurrentCalendarID() {
        return currentCalendarID;
    }

    private void setCurrentCalendarID( Integer calendarid ) {
        currentCalendarID = calendarid;
    }
    

    // core selection logic
    private void setCalendarSelected(final Integer calendarID,final boolean toSelect ) {
        if (existsCalendar(calendarID)) {
            final boolean isSelected;
            synchronized ( selectionPreferences ) {
                isSelected = selectionPreferences.getBoolean(calendarID.toString(),false);
                if(toSelect != isSelected){//update
                    selectionPreferences.putBoolean(calendarID.toString(), toSelect);
                }
            }
            //out of synchronized block
            if(toSelect != isSelected){
                //update collection
                if(toSelect) {
                    selectedCalendarsCollection.add(allCachedCalendarsCollection.getCalendar(calendarID.intValue()));
                } else {
                    selectedCalendarsCollection.remove(allCachedCalendarsCollection.getCalendar(calendarID.intValue()));
                }
                //fire event 
                new Thread(){
                    public void run() {//is thread is safe: no runtime exceptions will be thrown!
                        fireCalendarSelectionState(calendarID, toSelect);
                    }
                }.start();
            }
        } else {
            logger.warningSilent("[!] couldn't select a calendar: id not assigned: " + calendarID);
        }
    }

    /**
     * Toggles the Calendar Menu Item, if it is not the last one which is selected
     * 
     * @return The new selection state of the item
     */
    public void clickedCalendar(final Integer calendarID ) {
        dialogServices.setWaiting(true);
        setCurrentCalendarID(calendarID);
        final boolean toSelect = !isCalendarSelected(calendarID);
        if(toSelect) {//select
            setCalendarSelected(calendarID, true);
        } else {//unselect, but at least one should be selected 
            if(selectedCalendarsCollection.getCollection().size() > 1) {
                setCalendarSelected(calendarID, false);
            } else {//restore checkbox state
                logger.info("[!] can't unselect last calendar: " + calendarID.toString());
                fireCalendarSelectionState(calendarID, true);
            }
        }
        dialogServices.setWaiting(false);
    }
}
