/* $Id: OctopusDatabase.java,v 1.45 2007/08/30 16:10:30 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Thomas Fuchs and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import java.io.File;
import java.net.MalformedURLException;
import java.security.Security;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressClientDAO;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.CategoryClientDAO;
import org.evolvis.nuyo.db.Categorys;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.DupCheck;
import org.evolvis.nuyo.db.DupChecks;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.Schedule;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.filter.AbstractIdSelection;
import org.evolvis.nuyo.db.filter.AbstractStringSelection.UserLogin;
import org.evolvis.nuyo.db.octopus.old.OctopusConnector;
import org.evolvis.nuyo.db.octopus.old.OctopusMailBatch;
import org.evolvis.nuyo.db.persistence.PersistenceManager;
import org.evolvis.nuyo.db.veto.AddressAction;
import org.evolvis.nuyo.db.veto.CategoryAction;
import org.evolvis.nuyo.db.veto.UserAction;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.db.veto.VetoableAction;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.remote.Method;
import org.evolvis.nuyo.remote.SoapClient;
import org.evolvis.nuyo.security.SecurityManager;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusConstants;
import de.tarent.octopus.client.UserDataProvider;
import de.tarent.octopus.client.remote.OctopusRemoteConnection;
/**
 * Diese Klasse stellt eine Datenquelle auf Octopus-Webservice-Basis dar.
 * Eine Applikation braucht gew�hnlich nur eine Instanz hiervon.
 *
 * @author mikel & thomas
 */
public class OctopusDatabase extends Database {

	public static final String OC_CONNECTION_IDENTIFIER = "contact"; 

	private static Logger logger = Logger.getLogger(OctopusDatabase.class.getName());

	private SecurityManager securityManager;

	/*
	 * Daten zur Octopus-Verbindung.
	 */
	protected OctopusRemoteConnection octopusConnection = null;
	protected String octopusURL = null;
	protected String octopusModule = null;
	protected OctopusConnector connector = null;
	protected SortedMap initDaten = null;
	protected String baseURL = null;
	protected String schemaBaseURL = null;
	protected String module = null;
	static int packageIndex = 0;
	private Integer oldCategory = null;

	private SortedMap countryMap = new TreeMap();
	private SortedMap countryShortMap = new TreeMap();
	private SortedMap salutationMap = new TreeMap();
	private SortedMap regionMap = new TreeMap();
	private List dispatchOrderList = new ArrayList();    

	private Integer defaultCategory = null;
	private Integer generalCategory = null;
	private Integer generalSubCategory = null;
	private Categorys categorys;

	private Map hasUserRightOnFolder_caching = new HashMap();

	/**
	 * A management class for address objects
	 */
	AddressClientDAO addressDAO = new AddressClientDAOImpl(this);

	/**
	 * A management class for category objects
	 */
	CategoryClientDAO categoryDAO = new CategoryClientDAOImpl(this);

	/** Creates a database connection with the given configuration
	 * data.
	 * 
	 * <p>This is split from the database initialisation to allow
	 * testing whether a session can be continued.</p>
	 * 
	 * @param cd
	 * @return
	 */
	public static OctopusRemoteConnection createConnection(ConnectionParameters cd)
	{
		String module = cd.getModule();
		String url;
		try {
			url = cd.toURL().toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		String pathToSSLKeyFile = null;

		if (System.getProperty("de.tarent.contact.install.dir") != null)
			pathToSSLKeyFile = System.getProperty("de.tarent.contact.install.dir") + "tarent_store.dat";

		else  {
			File sslKeyFileForDev = new File (System.getProperty("user.dir") + "/src/main/izpack/tarent_store.dat");
			if (sslKeyFileForDev.isFile())
				pathToSSLKeyFile = System.getProperty("user.dir") + "/src/main/izpack/tarent_store.dat";
		}

		if(pathToSSLKeyFile != null){
			if (url.startsWith("https://") && System.setProperty("javax.net.ssl.trustStore", pathToSSLKeyFile ) == null){ //$NON-NLS-1$ //$NON-NLS-2$
				// SSL wanted
				logger.log(Level.FINER, "configuring for SSL"); //$NON-NLS-1$
				Security.addProvider(new BouncyCastleProvider());
				System.setProperty("javax.net.ssl.trustStore", pathToSSLKeyFile); //$NON-NLS-1$ //$NON-NLS-2$
				System.setProperty("javax.net.ssl.trustStorePassword", "tarent"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else
			logger.warning("No proper key for SSL encyption defined");

		// octopus connection configuration will be supplied as a map
		Map ocConfig = new HashMap();
		ocConfig.put(OctopusConnectionFactory.CONNECTION_TYPE_KEY, OctopusConnectionFactory.CONNECTION_TYPE_REMOTE);
		ocConfig.put(OctopusConnectionFactory.MODULE_KEY, module);
		ocConfig.put(OctopusRemoteConnection.PARAM_SERVICE_URL, url);
		ocConfig.put(OctopusRemoteConnection.AUTH_TYPE, OctopusRemoteConnection.AUTH_TYPE_SESSION);
		ocConfig.put(OctopusRemoteConnection.AUTO_LOGIN, "true");

		Environment env = ConfigManager.getEnvironment();

		if (env.getAsBoolean(Key.USE_OCTOPUS_SESSION_COOKIE)) {
			ocConfig.put(OctopusRemoteConnection.USE_SESSION_COOKIE, "true");
		}

		if("ALL".equalsIgnoreCase(env.get(Key.DEBUG)))
			ocConfig.put(OctopusRemoteConnection.CONNECTION_TRACKING, "true");

		ocConfig.put(OctopusRemoteConnection.KEEP_SESSION_ALIVE, env.get(Key.OCTOPUS_KEEP_SESSION_ALIVE));

		// getting connection factory from singleton
		OctopusConnectionFactory ocConnectionFactory = OctopusConnectionFactory.getInstance();

		// setting configuration map for this factory
		ocConnectionFactory.setConfiguration(OC_CONNECTION_IDENTIFIER, ocConfig);

		// getting and saving octopus connection
		return (OctopusRemoteConnection) ocConnectionFactory.getConnection(OC_CONNECTION_IDENTIFIER);
	}

	/** Initialization method that takes a pre-configured database connection.
	 * 
	 * <p>This method <em>MUST</em> be called before any of the other
	 * methods can be used.</p>
	 */
	public void init(OctopusRemoteConnection connection) {

		this.octopusModule = connection.getModuleName();
		this.module = this.octopusModule;
		this.octopusURL = connection.getServiceURL();
		this.baseURL = this.octopusURL;
		this.schemaBaseURL = "http://schemas.tarent.de/tccontact";

		// getting and saving octopus connection
		octopusConnection = connection;

		connector = new OctopusConnector(baseURL, "http://schemas.tarent.de/tccontact", module);

		securityManager = SecurityManager.getSecurityManager(this);
		categorys = new CategorysImpl(this);
	}

	public OctopusConnection getOctopusConnection() {
		return octopusConnection;
	}

	/**
	 * Returns a management class for address objects.
	 */
	public AddressClientDAO getAddressDAO() {
		return addressDAO;
	}

	/**
	 * Returns a management class for category objects.
	 */
	public CategoryClientDAO getCategoryDAO() {
		return categoryDAO;
	}
	/**
	 * Diese Methode berprft, ob eine intendierte Aktion grunds�tzlich
	 * erlaubt ist, und liefert ansonsten ein entsprechendes Veto.
	 *  
	 * @param action zu berprfende Aktion
	 * @return <code>null</code>, falls die Aktion grunds�tzlich erlaubt
	 *  ist, sonst ein Veto, dass Beschr�nkungen liefert.
	 * @throws ContactDBException
	 * @see org.evolvis.nuyo.db.Database#checkForVeto(org.evolvis.nuyo.db.veto.VetoableAction)
	 * 
	 */
	public Veto checkForVeto(VetoableAction action) throws ContactDBException {
		if(action instanceof AddressAction){
			AddressAction adressAction = (AddressAction) action;
			String categoryID = adressAction.getCategory();
			if(adressAction.getAction().equals(AddressAction.ACTION_CREATE)){
				int category = 0;
				try{
					category = Integer.parseInt(categoryID);
				}catch(NumberFormatException nfe){
					nfe.printStackTrace();
					return new Veto("Keine Kategorie f�r die Adresse gesetzt!", nfe.getLocalizedMessage(), true);
				}
				if(securityManager.userHasRightOnFolder(action.getUserId(), SecurityManager.FOLDERRIGHT_ADD, category)!=true){
					return new Veto(Messages.getString("AM_Addresse_Create_VETO"),Messages.getString("AM_Addresse_Create_VETO"), true);
				}
			}else if(adressAction.getAction().equals(AddressAction.ACTION_EDIT)){
				if((
						securityManager.userHasRightOnAddress(action.getUserId(), SecurityManager.FOLDERRIGHT_EDIT, adressAction.getAddress().getAdrNr())
						||
						securityManager.userHasRightOnAddress(action.getUserId(), SecurityManager.FOLDERRIGHT_STRUCTURE, adressAction.getAddress().getAdrNr())
				)!=true){
					return new Veto(Messages.getString("AM_Addresse_Edit_VETO"), null, true);
				}
			}else if(adressAction.getAction().equals(AddressAction.ACTION_DELETE)){
				if(securityManager.userHasRightOnAddress(action.getUserId(), SecurityManager.FOLDERRIGHT_REMOVE, adressAction.getAddress().getAdrNr())!=true){
					return new Veto(Messages.getString("AM_Addresse_Delete_VETO"), null, true);
				}
			}else if(adressAction.getAction().equals(AddressAction.ACTION_CLASSIFY)){
				logger.info("Classify auf AdressObject!");
			}else{
				return new Veto(Messages.getString("AM_VETO_Not_Tested"), Messages.getString("AM_VETO_Not_Tested"), false);
			}
		}else if(action instanceof CategoryAction){
			CategoryAction categoryAction = (CategoryAction) action;
			if(categoryAction.getAction().equals(CategoryAction.ACTION_CREATE)){
				if(!securityManager.userHasGlobalRight(action.getUserId(), SecurityManager.GLOBALRIGHT_EDITFOLDER)){
					return new Veto("", "", true);
				}
			}else if(categoryAction.getAction().equals(CategoryAction.ACTION_EDIT)){
				if(!securityManager.userHasGlobalRight(action.getUserId(), SecurityManager.GLOBALRIGHT_EDITFOLDER)){
					return new Veto("", "", true);
				}
			}else if(categoryAction.getAction().equals(CategoryAction.ACTION_DELETE)){
				if(!securityManager.userHasGlobalRight(action.getUserId(), SecurityManager.GLOBALRIGHT_REMOVEFOLDER)){
					return new Veto("", "", true);
				}
			}else{
				System.err.println(Messages.getString("AM_VETO_Not_Tested"));
				return new Veto(Messages.getString("AM_VETO_Not_Tested"), Messages.getString("AM_VETO_Not_Tested"), false);
			}
		}else if(action instanceof UserAction){
			UserAction userAction = (UserAction) action;
			if(userAction.getAction().equals(UserAction.ACTION_CREATE)){
				if(!securityManager.userHasGlobalRight(action.getUserId(), SecurityManager.GLOBALRIGHT_EDITUSER)){
					return new Veto("", "", true);
				}
			}else if(userAction.getAction().equals(UserAction.ACTION_EDIT)){
				if(!securityManager.userHasGlobalRight(action.getUserId(), SecurityManager.GLOBALRIGHT_EDITUSER)){
					return new Veto("", "", true);
				}
			}else if(userAction.getAction().equals(UserAction.ACTION_DELETE)){
				if(!securityManager.userHasGlobalRight(action.getUserId(), SecurityManager.GLOBALRIGHT_REMOVEUSER)){
					return new Veto("", "", true);
				}
			}else{
				System.err.println(Messages.getString("AM_VETO_Not_Tested"));
				return new Veto(Messages.getString("AM_VETO_Not_Tested"), Messages.getString("AM_VETO_Not_Tested"), false);
			}
		}else{
			logger.log(Level.FINEST, action.getClass().getName());
			return new Veto(Messages.getString("AM_VETO_Not_Tested"), Messages.getString("AM_VETO_Not_Tested"), false);
		}
		return null;
	}

	/**
	 * @see org.evolvis.nuyo.db.Database#createAddress()
	 */
	public Address createAddress() throws ContactDBException {
		return new AddressImpl();
	}

	/**
	 * Returns an address
	 * 
	 * @see org.evolvis.nuyo.db.Database#getAddress(int)
	 */
	public Address getAddress(int addrNr) throws ContactDBException {
		Method m = new Method("getAddressByPk");
		m.add("addressid", new Integer(addrNr));
		return (Address)m.invoke();
	}

	/**
	 * Diese Methode liefert die Anzahl der Adressen im Datenbestand.
	 * 
	 * @return Gesamtanzahl Adressen im Datenbestand.
	 * @throws ContactDBException
	 * @see org.evolvis.nuyo.db.Database#getAddressCount()
	 */
	public Integer getAddressCount() throws ContactDBException {
		Method m = new Method("getTotalAddressCount");
		return (Integer) m.invoke();
	}

	/**
	 * Returns a list of addresses without a filter
	 * @see org.evolvis.nuyo.db.Database#getChosenAddresses()
	 */
	public Addresses getChosenAddresses() throws ContactDBException {
		return new AddressesImpl(this);
	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getAnreden()
	 */
	public SortedMap getAnreden() throws ContactDBException {

		SortedMap tmpMap = new TreeMap();

		/* TODO: Fix this (2006-12-15)
        ACHTUNG HACK!!!
		 */
		tmpMap.put("1", "Sehr geehrter");
		tmpMap.put("2", "Sehr geehrte");
		tmpMap.put("3", "Sehr geehrte Damen und Herren");
		tmpMap.put("4", "Hallo");
		tmpMap.put("5", "Lieber");
		tmpMap.put("6", "Liebe");
		tmpMap.put("0", "");

		//return salutationMap;

		return tmpMap;

	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getBundeslaender()
	 */
	public SortedMap getBundeslaender() throws ContactDBException {
		return regionMap;    
	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getBundeslandForPLZ(int)
	 */
	public String getBundeslandForPLZ(String plz) throws ContactDBException {
		String bl = connector.getBundeslandForPLZ(plz);
		if (bl == null) bl = "";
		if (bl.equals("fehler")) 
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, 
			"Es ist ein Datenbankfehler aufgetreten!");
		return bl;
	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getCityForPLZ(int)
	 */
	public String getCityForPLZ(String plz) throws ContactDBException {
		return connector.getCityForPLZ(plz);
	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getLaender()
	 */
	public SortedMap getLaender() throws ContactDBException {
		return countryMap;

	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getLKZen()
	 */
	public SortedMap getLKZen() throws ContactDBException {
		return countryShortMap;

	}


	public SortedMap getVerteiler(String kategorie) throws ContactDBException {
		if (kategorie == null) 
			kategorie = getCategory();
		Category cat = categorys.getCategory(kategorie);
		if (cat == null) {
			//if (true) throw new RuntimeException();
			logger.log(Level.INFO, "Invalid Category ID: "+kategorie);
			return new TreeMap();
		}
		List verteilerList = cat.getSubCategories();

		SortedMap verteiler = new TreeMap();
		for (Iterator iter = verteilerList.iterator(); iter.hasNext();) {
			SubCategory subCategory = (SubCategory)iter.next();
			verteiler.put(subCategory.getIdAsString(), subCategory);

		} 
		return verteiler;
	}


	public List getCategoriesList() throws ContactDBException {
		return categorys.getAllCategorys();
	}

	/**
	 * Liste aller Kategorien, auf der ein User Berechtigungen verwalten darf 
	 */
	public List getGrantableCategoriesList() throws ContactDBException{
		return categorys.getAllGrantableCategorys();
	}


	/**
	 * @return category objects of all categories that are connected to the selected address
	 * @throws ContactDBException 
	 */
	public List getAssociatedCategoriesForSelectedAddress(Integer addressPk) throws ContactDBException{
		return getCategoryDAO().getAssociatedCategoriesForSelectedAddress(addressPk			   );
	}

	/**
	 * @return category objects of all categories that are connected to the selected address
	 * @throws ContactDBException 
	 */
	public List getAssociatedCategoriesForSelectedAddress(Integer addressPk, List filterList, boolean includeVirtual) throws ContactDBException{
		return getCategoryDAO().getAssociatedCategoriesForSelectedAddress(addressPk, filterList, includeVirtual);
	}


	/**
	 * 
	 * @return category objects of all categories that are connected to the selected address
	 */

	public List getAssociatedCategoriesForAddressSelection(List addressPks, boolean read, boolean edit, boolean add, boolean remove, boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeVirtual) throws ContactDBException{
		return getCategoryDAO().getAssociatedCategoriesForCurrentSelection(addressPks, read, edit, add, remove, addSubCat, removeSubCat, structure, grant, includeVirtual);
	}

	/**
	 * 
	 * @return category objects of all categories that are connected to the selected address
	 */

	public List getAssociatedCategoriesForAddressSelection(List addressPks, List filterList, boolean includeVirtual) throws ContactDBException{
		return getCategoryDAO().getAssociatedCategoriesForCurrentSelection(addressPks, filterList, includeVirtual);
	}
	/**
	 * Diese Methode liefert eine Map der Kategorien, die dem angegebenen
	 * Benutzer zur Verfgung stehen; der Schlssel in der Map ist der
	 * Schlssel der Kategorie.
	 * 
	 * @param user
	 *            der Benutzer, dessen Gruppen abgefragt werden; wenn der
	 *            Eintrag <code>null</code> ist, wird der aktuell eingelogte
	 *            benutzt.
	 * @return Map der Kategorien
	 * @throws ContactDBException
	 * @see #getAllCategories()
	 */
	public Map getCategories(String user) throws ContactDBException {        
		//if (user != null && ! user.equals(this.user))
		//    throw new ContactDBException(ContactDBException.EX_INVALID_USER, "Kategorien k�nnen nur f�r den aktuellen User ausgelesen werden");

		Map map = new HashMap();
		for (Iterator iter = categorys.getAllCategorys().iterator(); iter.hasNext();) {
			Category cat = (Category)iter.next();
			map.put(cat.getIdAsString(), cat);
		}
		return map;
	}

	/**
	 * 
	 * @param read
	 * @param edit
	 * @param add
	 * @param remove
	 * @param addSubCat
	 * @param removeSubCat
	 * @param structure
	 * @param grant
	 * @param includeTrash
	 * @return
	 */
	public List getCategoriesWithCategoryRightsOR(boolean read, boolean edit, boolean add, boolean remove, 
			boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeTrash) throws ContactDBException{

		List cats = new ArrayList();
		List allcats = getCategoriesList();


		for (Iterator iter = allcats.iterator(); iter.hasNext();){
			Category currCat = (Category) iter.next();

			// skip this one if its the trash and includetrash is false
			if(currCat.isTrash() && !includeTrash){
				continue;
			}
			if(read && currCat.getRightToReadAddress().booleanValue()){
				cats.add(currCat);
				continue;
			}
			if(edit && (currCat.getRightToEditAddress() != null && currCat.getRightToEditAddress().booleanValue())){
				cats.add(currCat);
				continue;
			}
			if(add && (currCat.getRightToAddAddress() != null && currCat.getRightToAddAddress().booleanValue())){
				cats.add(currCat);
				continue;
			}
			if(remove && (currCat.getRightToRemoveAddress() != null && currCat.getRightToRemoveAddress().booleanValue())){
				cats.add(currCat);
				continue;
			}
			if(addSubCat && (currCat.getRightToAddSubCategory() != null && currCat.getRightToAddSubCategory().booleanValue())){
				cats.add(currCat);
				continue;
			}
			if(removeSubCat && (currCat.getRightToRemoveSubCategory() != null && currCat.getRightToRemoveSubCategory().booleanValue())){
				cats.add(currCat);
				continue;
			}
			if(structure && (currCat.getRightToStructure() != null && currCat.getRightToStructure().booleanValue())){
				cats.add(currCat);
				continue;
			}
			if(grant && (currCat.getRightToGrant() != null && currCat.getRightToGrant().booleanValue())){
				cats.add(currCat);
				continue;
			}			
		}	
		return cats;
	}


	/**
	 * 
	 * @param read
	 * @param edit
	 * @param add
	 * @param remove
	 * @param addSubCat
	 * @param removeSubCat
	 * @param structure
	 * @param grant
	 * @param includeTrash
	 * @return
	 */
	public List getCategoriesWithCategoryRightsAND(boolean read, boolean edit, boolean add, boolean remove, 
			boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeTrash) throws ContactDBException{

		List cats = new ArrayList();
		List allcats = getCategoriesList();

		for (Iterator iter = allcats.iterator(); iter.hasNext();){
			Category currCat = (Category) iter.next();

//			skip this one if its the trash and includetrash is false
			if(currCat.isTrash() && !includeTrash){
				continue;
			}

			if(read && (currCat.getRightToReadAddress() == null || !currCat.getRightToReadAddress().booleanValue())){
				continue;
			}			
			if(edit && (currCat.getRightToEditAddress() == null || !currCat.getRightToEditAddress().booleanValue())){
				continue;
			}
			if(add && (currCat.getRightToAddAddress() == null || !currCat.getRightToAddAddress().booleanValue())){
				continue;
			}
			if(remove && (currCat.getRightToRemoveAddress() == null || !currCat.getRightToRemoveAddress().booleanValue())){
				continue;
			}
			if(addSubCat && (currCat.getRightToAddSubCategory() == null || !currCat.getRightToAddSubCategory().booleanValue())){
				continue;
			}
			if(removeSubCat && (currCat.getRightToRemoveSubCategory() == null || !currCat.getRightToRemoveSubCategory().booleanValue())){
				continue;
			}
			if(structure && (currCat.getRightToStructure() == null || !currCat.getRightToStructure().booleanValue())){
				continue;
			}
			if(grant && (currCat.getRightToGrant() == null || !currCat.getRightToGrant().booleanValue())){
				continue;
			}

			cats.add(currCat);
		}

		return cats;
	}

	/**
	 * Diese Methode liefert eine Map aller Kategorien; der Schlssel in der
	 * Map ist der Schlssel der Kategorie.
	 * 
	 * @return Map der Kategorien
	 * @throws ContactDBException
	 */
	/// TODO: CATEGORY ANPASSEN done 
	public Map getAllCategories() throws ContactDBException {
		return getCategories(null);
	}

	/**
	 * Diese Methode liefert zu dem angegebenen Benutzer die Standard-
	 * Kategorie.
	 * 
	 * @param user
	 *            der Benutzer, dessen Standardgruppe abgefragt wird; wenn der
	 *            Eintrag <code>null</code> ist, wird der aktuell eingelogte
	 *            benutzt.
	 * @return der Schlssel der Standardkategorie; falls keine eingestellt
	 *         ist, wird <code>null</code> geliefert.
	 * @throws ContactDBException
	 */
	public String getStandardCategory(String user)
	throws ContactDBException {
		return defaultCategory.toString();
	}

	public Category getVirtualRootCategory() throws ContactDBException{
		return categorys.getVirtualRootCategory();
	}

	public Category getTrashCategory() throws ContactDBException{
		return categorys.getTrashCategory();
	}

	/**
	 * Diese Methode liefert die Kategorie der allgemeinen Adressen.
	 * 
	 * @return der Schlssel der allgemeinen Kategorie; falls keine
	 *         eingestellt ist, wird <code>null</code> geliefert.
	 * @throws ContactDBException
	 */
	public String getAllgemeineCategory() throws ContactDBException {
		SortedMap sm = connector.getAllgemeinenVerteiler();
		if (sm.containsKey("fehler")) 
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, 
					"Octopus meldet folgenden Fehler:\n" + sm.get("fehler"));
		return asString(sm.get("allgemein"));
	}


	/**
	 * @see org.evolvis.nuyo.db.Database#createVersandAuftrag()
	 */
	public MailBatch createVersandAuftrag(Addresses addresses) throws ContactDBException {
		Method method = new Method("createVersandAuftragSOAP");
		method.add("addressPks", addresses.getPkListAsString());
		method.add("search", new Integer(addresses.getSize()));        
		method.add("batchID", new Integer(MailBatch.BATCH_NEW_ID));
		method.add("size", new Integer(0));
		method.add("user", getUser());

		Map result = (Map)method.invoke();
		if (result.containsKey("fehler")) 
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, asString(result.get("fehler")));

		return new OctopusMailBatch(this,
				connector, 
				octopusModule, 
				(List)result.get("batch"), 
				getUser(), 
				getCategory(), 
				true);
	}

	/**
	 * @see org.evolvis.nuyo.db.Database#getVersandAuftraege()
	 */
	public List getVersandAuftraege() throws ContactDBException {
		return dispatchOrderList;	
	}

	/**
	 * Dieses Attribut enth�lt den Benutzer. Der Benutzer ist vor anderen
	 * Filtern zu setzen.
	 * 
	 * 
	 * @param user
	 *            Der zu setzende Benutzer.
	 * @see #setPassword(String)
	 */
	public void setUser(String user) throws ContactDBException {
		super.setUser(user);
		octopusConnection.setUsername(user);
	}

	/**
	 * Dieses Attribut enh�lt das Passwort des Benutzers. Das Passwort ist
	 * nach dem Benutzer zu setzen, aber vor jeglichem Zugriff auf den
	 * Datenbestand.
	 * 
	 * Diese Methode l�scht benutzerbezogene Daten und holt mit dem .
	 * 
	 * @param password
	 *            Das zu setzende Passwort.
	 * @see #setUser(String)
	 */
	public void setPassword(String password) throws ContactDBException {
		super.setPassword(password);
		octopusConnection.setPassword(password);       
	}

	public boolean continueSession() throws ContactDBException {
		if (octopusConnection.continueSession()) {
			setUser(octopusConnection.getUsername());
			baseURL = octopusConnection.getServiceURL();
			return true;
		}
		return false;
	}

	public void login() throws ContactDBException {
		try {
			octopusConnection.login();
		} catch (Exception e) {
			if (e instanceof OctopusCallException) {
				String errorCode = ((OctopusCallException)e).getErrorCode();
				if (OctopusConstants.AUTHENTICATION_CANCELED.equals( errorCode ))
					throw new ContactDBException(ContactDBException.EX_CLIENT_LOGIN_ERROR, ((OctopusCallException)e).getCauseMessage());
			}            
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Es ist ein Fehler beim Login aufgetreten!", e);
		}
	}

	public void setUserDataProvider(UserDataProvider provider) {
		octopusConnection.setUserDataProvider(provider);
	}

	/**
	 * Diese Methode l�scht benutzerbezogene gehaltene Daten
	 */
	protected void clearUserData() throws ContactDBException {
		octopusConnection.logout();
	}

	/**
	 * sp�ter implementieren!
	 */
	public String getAdresses(String XML) throws ContactDBException {
		return null;
	}


	/*
	 * administrative Methoden
	 */

	/**
	 * Diese Methode erzeugt eine kategorie.
	 * 
	 * @param key
	 *            die id der kategorie.
	 * @param name
	 *            der Name der Kategorie.
	 * @param description die Kategoriebeschreibung.
	 */
	public void createCategory(String id, String name, String description)
	throws ContactDBException {

		Category ci = new Category();
		if (id != null)
			ci.setId(Integer.parseInt(id));
		ci.setDescription(description);
		ci.setName(name);
		getCategoryDAO().save(ci);
		categorys.loadCategorys();
	}

	/**
	 * Diese Methode editiert eine kategorie.
	 * 
	 * @param key
	 *            die id der kategorie.
	 * @param name
	 *            der Name der Kategorie.
	 * @param description die Kategoriebeschreibung.
	 */
	public void editCategory(String key, String name, String description)
	throws ContactDBException {
		Category ci = getCategory(key);
		ci.setName(name);
		ci.setDescription(description);
		getCategoryDAO().save(ci);
	}


	/**
	 * Diese Methode l�scht eine Kategorie.
	 * 
	 * @param key
	 *            die id der Kategorie.
	 */
	public void deleteCategory(String key) throws ContactDBException {
		getCategoryDAO().delete(getCategory(key));
		categorys.loadCategorys();
	}

	public void reloadCategories() throws ContactDBException{
		categorys.loadCategorys();
	}

	public void loadCategories() throws ContactDBException{
		categorys.loadCategorys();
	}



	/**
	 * Holt eine Kategorie
	 * @param key
	 * @return
	 * @throws ContactDBException
	 */
	public Category getCategory(String key) throws ContactDBException { 
		return categorys.getCategory(key);
	}




	/**
	 * Diese Methode erzeugt einen Verteiler.
	 * 
	 * @param category
	 *            the parent category.
	 * @param key
	 *            the key of this subcategory.
	 */
	public void createVerteiler(Integer category, String name, String description) throws ContactDBException {
		SubCategoryImpl subCategory = new SubCategoryImpl();
		subCategory.setParentCategory(category);
		subCategory.setSubCategoryName(name);
		subCategory.setDescription(description);
		subCategory.create();

		categorys.loadCategorys();
	}


	public void editVerteiler(String category, String key, String name, String description, String unused) throws ContactDBException {
		Category cat = categorys.getCategory(category);
		if (cat == null) {
			logger.log(Level.SEVERE, "Ungültige Kategorie ID: "+category);
			throw new ContactDBException(ContactDBException.EX_INVALID_CATEGORY_KEY, "Ungültige Kategorie ID: "+category);
		}
		SubCategoryImpl subCat = (SubCategoryImpl)cat.getSubCategory(key);
		subCat.setSubCategoryName(name);
		subCat.setDescription(description);
		subCat.commit();
	}
	/**
	 * Diese Methode l�scht eine Unterkategorie.
	 * 
	 * @param category
	 *            die Kategorie, in der die Unterkategorie gel�scht werden
	 *            soll.
	 * @param key
	 *            der Unterkategorieschlssel.
	 */
	public void deleteVerteiler(String category, String key)
	throws ContactDBException {
		Category cat = getCategory(category);
		SubCategoryImpl subCat = (SubCategoryImpl)cat.getSubCategory(key);
		cat.removeSubCategory(subCat.getId());
		subCat.delete();
	}

	/**Assigns a list of addresses(a pk-list) to the subcategories indicated by the list 
	 * addSubcategories and removes assignments to subcategories indicated by the list removeSubcategories.
	 * The elements of the lists need to implement a meaningfull toString() method. 
	 */

	public Object changeAddressAssignments(List addressesPkList, List addSubcategories, List removeSubcategories) throws ContactDBException{
		//------------

		Method method = new Method("assignAddresses");
		//put the parameters:
		method.add("addressPks",addressesPkList);
		method.add("addSubCategories",addSubcategories);
		method.add("removeSubCategories",removeSubcategories);
		method.invoke();
		return method.getORes();
	}

	/**
	 * Diese Methode erzeugt einen neuen Benutzer.
	 * 
	 * @param userId
	 *            die Benutzerkennung.
	 * @param password
	 *            das Passwort
	 * @param lastName
	 *            der Nachname des Benutzers.
	 * @param firstName
	 *            der Vorname des Benutzers.
	 * @param special
	 *            legt fest, ob der Benutzer zur Gruppe der Standardbenutzer
	 *            oder der besonderen Benutzer geh�ren soll.
	 * @param admin
	 *            legt fest, ob der Benutzer Administrator seien soll.
	 */
	public void createUser(String userId, String password, String lastName, String firstName) throws ContactDBException {
		User tmp = new UserImpl();
		tmp.setAttribute(User.KEY_FIRST_NAME, firstName);
		tmp.setAttribute(User.KEY_LAST_NAME, lastName);
		tmp.setAttribute(User.KEY_LOGIN_NAME, userId);
		tmp.setAttribute(User.KEY_PASSWORD, password);
		tmp.setAttribute(User.KEY_USER_TYPE, ""+User.USER_TYPE_OTHER);
		PersistenceManager.commit(tmp);
	}

	/**
	 * Diese Methode �ndert einen Benutzer.
	 * 
	 * @param userId
	 *            die Benutzerkennung.
	 * @param password
	 *            das Passwort
	 * @param lastName
	 *            der Nachname des Benutzers.
	 * @param firstName
	 *            der Vorname des Benutzers.
	 * @param special
	 *            legt fest, ob der Benutzer zur Gruppe der Standardbenutzer
	 *            oder der besonderen Benutzer geh�ren soll.
	 * @param admin
	 *            legt fest, ob der Benutzer Administrator seien soll.
	 */
	public void changeUser(String userId, String password, String lastName, String firstName, String email) throws ContactDBException {    	
		User u = new UserImpl(Integer.valueOf(userId));
		u.setAttribute(UserBean.KEY_FIRST_NAME, firstName);
		u.setAttribute(UserBean.KEY_LAST_NAME, lastName);
		if(password!=null&&password.length()>0){
			u.setAttribute(UserBean.KEY_PASSWORD, password);
		}
		u.setAttribute(UserBean.KEY_USER_TYPE, ""+UserBean.USER_TYPE_ADMIN);
		u.setAttribute(UserBean.KEY_EMAIL, email);
		PersistenceManager.commit(u);
	}

	/**
	 * Diese Methode l�scht einen Benutzer.
	 * 
	 * @param userId
	 *            die Benutzerkennung.
	 */
	public void deleteUser(String userId) throws ContactDBException {
		Collection users = UserImpl.getUsers(org.evolvis.nuyo.db.filter.Selection.getInstance().add(new AbstractIdSelection.UserId(userId)));
		if(users.size()!=1){
			System.out.println("FEHLER! User konnte nicht gel�scht werden");
			throw new ContactDBException(ContactDBException.EX_INVALID_USER, "User konnte nicht gefunden werden!");

		}
		UserImpl ui = (UserImpl) users.iterator().next();
		PersistenceManager.delete(ui);
	}


	/**
	 * Diese Methode liefert zu dem angegebenen Benutzer (Default: der
	 * eingelogte Benutzer) eine User-Instanz.
	 * 
	 * @param userLogin Login-Name des Benutzers; <code>null</code> referenziert den
	 *  aktuell eingelogten Benutzer.
	 * @return User-Instanz zum angegebenen Benutzer oder <code>null</code>, wenn
	 *  es keinen Benutzer zum angegebenen Login gibt.
	 * @throws ContactDBException
	 * @see org.evolvis.nuyo.db.Database#getUser(java.lang.String)
	 */
	public User getUser(String userLogin) throws ContactDBException {
		List tmp = (List)UserImpl.getUsers(org.evolvis.nuyo.db.filter.Selection.getInstance().add(new UserLogin(userLogin)));
		if (tmp == null || tmp.size() == 0)
			return null;
		return (User)tmp.get(0);
	}

	public User getUser(String userLogin, boolean enableChaching) throws ContactDBException {
		List tmp = (List)UserImpl.getUsers(org.evolvis.nuyo.db.filter.Selection.getInstance().add(new UserLogin(userLogin)), enableChaching);
		if (tmp == null || tmp.size() == 0)
			return null;
		return (User)tmp.get(0);
	}


	/**
	 * Diese Methode liefert eine Map der Benutzer-Logins auf zugeh�rige
	 * {@link User}-Instanzen.
	 * 
	 * @return Map Benutzerlogins auf User-Instanzen.
	 * @throws ContactDBException
	 * @see org.evolvis.nuyo.db.Database#getUsers()
	 */
	public Map getUsers() throws ContactDBException {
		Map users = new HashMap();
		Collection userslist = UserImpl.getUsers(null);
		for(Iterator it = userslist.iterator(); it.hasNext(); ){
			User user = (User) it.next();
			users.put(new Integer(user.getId()), user);
		}
		return users;
	}

	/**
	 * Diese Methode liefert den Namen eines Benutzers
	 * 
	 * @param userID
	 *            die Benutzerkennung.
	 * @return der Name des Benutzers.
	 */
	public String getUserName(String userID) throws ContactDBException {
		return asString(getUserNames().get(userID));
	}

	/**
	 * Diese Methode setzt den EinladungsStatus.
	 * 
	 * @param eventId
	 *            Id des events
	 * @param statusId
	 *            Einladungsstatus
	 * @return Erfolgsflag
	 */
	public void setEventStatus(int userId, int eventId, int statusId) {
		boolean result = false;

		try {
			if (user != null && getUser(getUser()).getId() == userId)
				result = connector.SetMyEventStatus(userId, eventId,statusId);
			else
				result = connector.SetEventStatus(userId, eventId,statusId);
		}
		catch (ContactDBException e) {
			result = false;
		}
	}

	/**
	 * Diese Methode setzt benutzerabh�ngige Parameter. 
	 * 
	 * @param userId
	 *            Id des Benutzers; wenn sie <code>null</code> ist, so ist
	 *            der aktuell angemeldete Benutzer gemeint.
	 * @param key
	 *            Schlssel des Parameters.
	 * @param value
	 *            Wert des Parameters.
	 * @return Erfolgsflag
	 */
	public boolean setUserParameter(String userId, String key, String value) {
		boolean result = false;

		if (userId == null) {
			userId = user;
		}
		try {
			result = connector.setUserParameter(userId, key, value);
		}
		catch (ContactDBException e) {
			result = false;
		}
		return result;
	}

	/**
	 * Diese Methode holt benutzerabh�ngigige Parameter.
	 * 
	 * @param loginname
	 *            Benutzername; wenn sie <code>null</code> ist, so ist
	 *            der aktuell angemeldete Benutzer gemeint.
	 * @param key
	 *            Schlssel des Parameters.
	 * @return Wert des Parameters.
	 */
	public String getUserParameter(String loginname, String key) throws ContactDBException {
		if(loginname == null)
			loginname = getUser();
		User user = new UserImpl(loginname);
		if(user.getId() != 0)
			return user.getParameter(key);
		else return "";
	}

	/**
	 * Diese Methode liefert das Special-Flag eines Benutzers
	 * 
	 * @param userID
	 *            die Benutzerkennung.
	 * @return das Special-Flag des Benutzers.
	 */
	public boolean isUserSpecial(String userID) throws ContactDBException {
		SortedMap sm = new TreeMap(connector.getUserSpecial(userID));
		if (sm.containsKey("fehler")) 
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, 
					"Octopus meldet folgenden Fehler:\n" + sm.get("fehler"));
		if (((Integer) sm.get("special")).intValue() == 1) return true;
		return false;
	}

	/**
	 * Diese Methode liefert das Admin-Flag eines Benutzers
	 * 
	 * @param userID
	 *            die Benutzerkennung.
	 * @return das Admin-Flag des Benutzers.
	 */
	public boolean isUserAdmin(String userID) throws ContactDBException {
		return true;
	}

	/**
	 * Diese Methode liefert eine Map der Benutzer des Systems. Schlssel ist
	 * die Benutzer-ID (Loginname), Wert der Benutzername.
	 * 
	 * @return die Map der Benutzer.
	 */
	public SortedMap getUserNames() throws ContactDBException {
		Collection tmp = UserImpl.getUsers(null);
		SortedMap users = new TreeMap();
		//	Abw�rts kompatibel
		for(Iterator it = tmp.iterator();it.hasNext();) {
			User user = (User)it.next();
			users.put(new Integer(user.getId()).toString(), user.getAttribute(User.KEY_LOGIN_NAME));
		}
		return users;

	}

	/**
	 * Diese Methode liefert eine Sammlung vorliegender Duplikatchecks.
	 * 
	 * @see org.evolvis.nuyo.db.Database#getDupChecks()
	 */
	public DupChecks getDupChecks() {
		// TODO! getDupChecks() implementieren
		return null;
	}

	/**
	 * Diese Methode liefert den Duplikatcheck mit der bergebenen ID.
	 * 
	 * @param dupCheckID
	 *            ID des nachgefragten Checks.
	 * @return der nachgefragte Check oder <code>null</code>, falls es zu
	 *         der ID keinen Duplikatcheck gibt.
	 * @see org.evolvis.nuyo.db.Database#getDupCheck(java.lang.String)
	 */
	public DupCheck getDupCheck(String dupCheckID) {
		// TODO! getDupCheck(String) implementieren
		return null;
	}

	/**
	 * Diese Methode erzeugt einen neuen Duplikatcheck.
	 * 
	 * @return der neue Duplikatcheck, eventuell noch nicht in der Datenbank
	 *         verankert.
	 * @see org.evolvis.nuyo.db.Database#createDupCheck()
	 */
	public DupCheck createDupCheck() {
		// TODO! createDupCheck() implementieren
		return null;
	}

	/**
	 * Diese Methode holt alle initialen Datens�tze. Es werden lediglich alle
	 * Aufrufe der Startprozedur gesammelt und gebndelt bergeben.
	 *  
	 */
	public void getInitDatas() throws ContactDBException {

		try {

			// Initiales Laden der Kategorien
			categorys.loadCategorys();


			// Laden weiterer statischer Daten
			StringTokenizer st = new StringTokenizer((String)SoapClient.getStaticData(getUser()), "^");
			String countries = st.nextToken();

			int i = 1;
			countryMap.put("0", null);
			for (StringTokenizer st2 = new StringTokenizer(countries, "#"); st2
			.hasMoreTokens();) {
				countryMap.put(new Integer(i++).toString(), st2.nextToken());
			}
			String countryShorts = st.nextToken();
			i = 0;
			for (StringTokenizer st2 = new StringTokenizer(countryShorts, "#"); st2
			.hasMoreTokens();) {
				countryShortMap.put(new Integer(i++).toString(), st2.nextToken());
			}
			i = 1;
			String region = st.nextToken();
			regionMap.put("0", null);
			for (StringTokenizer st2 = new StringTokenizer(region, "#"); st2
			.hasMoreTokens();) {
				regionMap.put(new Integer(i++).toString(), st2.nextToken());
			}

			String salutations = st.nextToken();
			i = 0;
			for (StringTokenizer st2 = new StringTokenizer(salutations, "#"); st2
			.hasMoreTokens();) {
				salutationMap.put(new Integer(i++).toString(), st2.nextToken());
			}


			if (st.hasMoreTokens()) {      

				try {
					defaultCategory = new Integer(st.nextToken());
				} catch (NumberFormatException e) {
					logger.log(Level.INFO, "Der Benutzer hat keine Standardkategorie zugeordnet.");
					//throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Der Benutzer hat keine Kategorien zugeordnet.");
				}
			}

			if (st.hasMoreTokens()) {     
				StringTokenizer general = new StringTokenizer(st.nextToken(), "#");

				generalCategory = new Integer(general.nextToken()); 
				generalSubCategory = new Integer(general.nextToken());
			}

			if (st.hasMoreTokens()) {
				String isAdmin = st.nextToken();
				if (isAdmin.equals("0")) {
				} else {
				}
			} else {
			}

			// Wird nichtmehr verwendet: myCategorField
			// Pers�nliche Startkategorie wird jetzt vom Client festgelegt.
			if (st.hasMoreTokens())
				st.nextToken();
			//             Integer tmpMyCategory = new Integer(test);
			//             if(tmpMyCategory.intValue() != 0)
			//                 myCategory = tmpMyCategory;

			while (st.hasMoreTokens()) {
				String dispatchOrder = st.nextToken();
				Vector tmp2 = new Vector();
				for (StringTokenizer st2 = new StringTokenizer(dispatchOrder, "#");st2.hasMoreTokens();) {
					tmp2.add(st2.nextToken());

				}
				MailBatch batch = new OctopusMailBatch(this,
						connector, 
						octopusModule, 
						tmp2, 
						getUser(), 
						getCategory(), 
						false);

				dispatchOrderList.add(batch);

			}
		} catch (NumberFormatException e) {
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Beim Einlesen der Initialdaten ist ein Fehler aufgetreten : " + e.getMessage());
		} catch (ContactDBException e) {
			throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Beim Einlesen der Initialdaten ist ein Fehler aufgetreten : " + e.getMessage(), e);
		}

	}

	/**
	 * Diese Methode liefert die Kommunikationsdatentypen, die von der
	 * DB-Umsetzung abh�ngigen k�nnen, als Abbildung von Identifikatoren auf
	 * Feldbeschreibungen vom Typ
	 * {@link org.evolvis.nuyo.db.Database.CommunicationDescription CommunicationDescription}.
	 * 
	 * @return Abbildung mit den Kommunikationsdatenbeschreibungen.
	 */
	public SortedMap getCommunicationFields() throws ContactDBException {
		SortedMap result = new TreeMap();
		result.put("telprivat", new CommunicationDescription("telprivat", 
				"Telefon Privat", "private Telefonnummer", "zu Hause", 
				CommunicationDescription.TYPE_PHONE, 20));
		result.put("teldienst", new CommunicationDescription("teldienst", 
				"Telefon Dienst", "dienstliche Telefonnummer", "im Bro", 
				CommunicationDescription.TYPE_PHONE, 20));
		result.put("emlprivat", new CommunicationDescription("emlprivat", 
				"E-Mail Privat", "private Telefonnummer", "zu Hause", 
				CommunicationDescription.TYPE_CELLULAR, 20));
		result.put("emldienst", new CommunicationDescription("emldienst", 
				"E-Mail Dienst", "dienstliche Telefonnummer", "im Bro", 
				CommunicationDescription.TYPE_CELLULAR, 20));
		//..... und so irgendwie
		return result;
	}

	/**
	 * Diese Methode liefert eine anzeigbare Darstellung der zugrundeliegenden
	 * Datenquelle.
	 * 
	 * @return Datenquellendarstellung
	 */
	public String getDisplaySource() {
		return baseURL;
		//return baseURL + "\nModul: " + module;
	}

	/**
	 * Diese Methode liefert die Version der zugrundeliegenden Datenquelle.
	 * 
	 * @return Datenquellenversion
	 * @throws ContactDBException
	 */
	public String getVersion(String clientVersion) throws ContactDBException {
		getInitDatas();
		String version = null;
		version = SoapClient.getVersion(clientVersion);
		return version;
	}

	private final static String asString(Object o) {
		return o == null ? null : o.toString();
	}


	public Object getFeature(String feature) {
		// TODO Auto-generated method stub
		return null;
	}

	public Schedule getSchedule(String userId) throws ContactDBException {
		return new ScheduleImpl(new UserImpl(userId));
	}

	/**
	 * Diese Methode erzeugt einen Historieneintrag zu einem Kontakt-Ereignis
	 * und legt diesen direkt an.
	 * 
	 * @param address externer Teilnehmer 
	 * @param user interner Teilnehmer (falls null, so wird der angemeldete Benutzer angenommen)
	 * @param contactCategory Kontaktkategorie, vergleiche Konstanten Contact.CATEGORY_*
	 * @param contactChannel Kontaktkanal, vergleiche Konstanten Contact.CHANNEL_*
	 * @param linktype Typ des Links, vergleiche Konstanten Contact.LINKTYPE_*
	 * @param link Link am Kontaktereignis
	 * @param inbound Flag, ob einkommendes (von aussen initiiertes) Kontaktereignis
	 * @param subject Betreff zum Kontaktereignis
	 * @param note Notiz zum Kontaktereignis
	 * @param structInfo strukturierte Information zum Kontaktereignis
	 * @param start Start (UTC) des Kontaktereignisses
	 * @param duration Dauer des Kontaktereignisses in Sekunden
	 * @return das neue Kontakt-Ereignis
	 * @throws ContactDBException
	 * @see org.evolvis.nuyo.db.Database#createContact(org.evolvis.nuyo.db.Address, org.evolvis.nuyo.db.User, int, int, int, java.lang.Object, boolean, java.lang.String, java.lang.String, java.lang.String, java.util.Date, int)
	 */
	public Contact createContact(Address address, User user, int contactCategory, int contactChannel, int linktype, Object link, boolean inbound, String subject, String note, String structInfo, Date start, int duration) throws ContactDBException {
		return createContact(address, user, contactCategory, contactChannel, linktype, link, inbound, subject, note, structInfo, start, duration, true);
	}

	/**
	 * Diese Methode erzeugt einen Historieneintrag zu einem Kontakt-Ereignis.
	 * Ob er in der DB angelegt wird kann ber das create-flag gesteuert werden.
	 * 
	 * @param address externer Teilnehmer 
	 * @param user interner Teilnehmer (falls null, so wird der angemeldete Benutzer angenommen)
	 * @param contactCategory Kontaktkategorie, vergleiche Konstanten Contact.CATEGORY_*
	 * @param contactChannel Kontaktkanal, vergleiche Konstanten Contact.CHANNEL_*
	 * @param linktype Typ des Links, vergleiche Konstanten Contact.LINKTYPE_*
	 * @param link Link am Kontaktereignis
	 * @param inbound Flag, ob einkommendes (von aussen initiiertes) Kontaktereignis
	 * @param subject Betreff zum Kontaktereignis
	 * @param note Notiz zum Kontaktereignis
	 * @param structInfo strukturierte Information zum Kontaktereignis
	 * @param start Start (UTC) des Kontaktereignisses
	 * @param duration Dauer des Kontaktereignisses in Sekunden
	 * @param create true, wenn das angelegte Objekt direkt in die DB geschrieben werden soll.
	 * @return das neue Kontakt-Ereignis
	 * @throws ContactDBException
	 */
	public Contact createContact(Address address, User user, int contactCategory, int contactChannel, int linktype, Object link, boolean inbound, String subject, String note, String structInfo, Date start, int duration, boolean create) throws ContactDBException {
		return new ContactImpl(address, user, new Integer(contactCategory), new Integer(contactChannel), new Integer(duration), new Boolean(inbound), subject, note, structInfo, start, create);
	}

	/**
	 * Speichert eine Liste von Contact Objekten in der DB
	 *
	 * @param Liste von Contact-Objekten
	 */
	public void commitContactList(Contact[] contacts) 
	throws ContactDBException {

		List list = new ArrayList(contacts.length);
		for (int i = 0; i < contacts.length; i++) {
			if (! (contacts[i] instanceof ContactImpl))
				throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Die Contact Objekte m�ssen vom Typ ContactImpl sen.");
			list.add(((ContactImpl)contacts[i]).getAsMap());
		}
		Method method = new Method("createContacts");
		method.add("contactlist", list);
		method.invoke();
	}

	/**
	 * TODO: Addresszugriff: remove this mehtod and change all dependencies
	 *
	 * Diese Methode liefert eine Addresses-Instanz auf Basis der Einschr�nkungen,
	 * die ber den aktuellen Benutzer und als Parameter gegeben sind.<br>
	 * Derzeit werden nicht die generischen Filter, sondern nur die Suchfilter beachtet.
	 * 
	 * @param filters eine Sammlung von Filtern (z.B. Kategorien, Unterkategorien, Versandauftr�ge,...)
	 * @param criteria eine Map von Suchfeldern und -Strings; die Schlssel sind die Feldkonstanten aus
	 *  {@link Address}, also STD_FIELD_* und CAT_FIELD_*, und zus�tzlich "" f�r die Suche in allen Feldern;
	 *  die Such-Strings sind die zu suchenden Feldinhalte, wobei '*' als Jokerzeichen erlaubt ist.
	 * @return Sammlung der Adressen, die den Filtern und Suchkriterien gengen.
	 * @throws ContactDBException
	 */
	public Addresses getAddresses(Collection filters, Map criteria)
	throws ContactDBException {
		logger.log(Level.WARNING, "remove the usage of OctopusDatabase#getAddresses"); //$NON-NLS-1$
		return new AddressesImpl(this);
	}

	public UserGroup createUserGroup(String name, String description, String shortname, int globalroleID) throws ContactDBException {
		UserGroup userGroup = new UserGroupImpl();
		userGroup.setAttribute(UserGroup.KEY_NAME, name);
		userGroup.setAttribute(UserGroup.KEY_DESCRIPTION, description);
		userGroup.setAttribute(UserGroup.KEY_SHORTNAME, shortname);
		userGroup.setAttribute(UserGroup.KEY_GLOBALROLE, String.valueOf(globalroleID));
		userGroup.setAttribute(UserGroup.KEY_GROUP_TYPE, String.valueOf(UserGroup.GROUP_TYPE_OTHER));
		return userGroup;
	}

	public Collection getUserGroups() throws ContactDBException {
		return UserGroupImpl.getUserGroups(null);
	}

	/* (non-Javadoc)
	 * @see de.tarent.contact.db.Database#getAvailablePreviewRows()
	 */
	public List getAvailablePreviewRows() throws ContactDBException {
		return SoapClient.getAvailablePreviewRows();
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see de.tarent.contact.db.Database#UserHasRightOnFolder(java.lang.String, int, int)
	 */
	public boolean userHasRightOnFolder(String UserLogin, int FolderRight, int FolderID) throws ContactDBException {
		// TODO: Eigentlich m�ssten die ganzen Rechte nicht 
		// einzeln vom Server geholt werden.

		String key = FolderRight +"|"+ FolderID +"|"+ UserLogin;
		if (null == hasUserRightOnFolder_caching.get(key)) {
			Method method = new Method("hasUserRightOnFolder");
			method.addParam("userLogin", UserLogin);
			method.addParam("folderRight", new Integer(FolderRight));
			method.addParam("folderID", new Integer(FolderID));
			hasUserRightOnFolder_caching.put(key, method.invoke());
		}
		return ((Boolean)hasUserRightOnFolder_caching.get(key)).booleanValue();
	}

	/* (non-Javadoc)
	 * @see de.tarent.contact.db.Database#UserHasGlobalRight(java.lang.String, int)
	 */
	public boolean userHasGlobalRight(String UserLogin, int GlobalRight) throws ContactDBException {
		//Erstmal eine Straight-forward-Version ohne Caching
		//TODO: Den ganzen Kram ber die neuen Beans ziehen, damit caching und persistenz da sind
		Method method = new Method("hasUserGlobalRight");
		method.addParam("userLogin", UserLogin);
		method.addParam("globalRight", new Integer(GlobalRight));
		Boolean result = (Boolean) method.invoke();
		return result.booleanValue();
	}

	public List getGlobalRoles() throws ContactDBException {
		List result = new ArrayList();
		Iterator it = GlobalRoleImpl.getGlobelRoles(null).iterator();
		while(it.hasNext()){
			result.add(it.next());
		}
		return result;
	}

	public List getObjectRoles() throws ContactDBException {
		List result = new ArrayList();
		Iterator it = ObjectRoleImpl.getObjectRoles(null).iterator();
		while(it.hasNext()){
			result.add(it.next());
		}
		return result;
	}

	public boolean userHasRightOnAddress(String UserLogin, int FolderRight, int addressID) throws ContactDBException {
		//Erstmal eine Straight-forward-Version ohne Caching
		//TODO: Den ganzen Kram ber die neuen Beans ziehen, damit caching und persistenz da sind
		Method method = new Method("hasUserRightOnAddress");
		method.addParam("userLogin", UserLogin);
		method.addParam("folderRight", new Integer(FolderRight));
		method.addParam("addressID", new Integer(addressID));
		Boolean result = (Boolean) method.invoke();
		return result.booleanValue();
	}

	public boolean mayUserWriteEvent(int eventid) throws ContactDBException {
		Method method = new Method("MayUserWriteEvent");
		method.addParam("eventid", new Integer(eventid));
		Boolean result = (Boolean) method.invoke();
		return result.booleanValue();
	}

	/**
	 * Diese Methode erzeugt zu einem Wert einen Matchcode passend zur bergebenen
	 * Spalte. Erlaubtes Jokerzeichen ist '*'.<br>
	 * Hierbei sollte eigentlich die Stored Procedure buildMatchcode genutzt werden,
	 * deren Funktionalit�t hier nachgebildet wird.
	 * 
	 * 
	 * @param column die Spalte, zu der passend der Matchcode erzeugt wird.
	 * @param value der Wert, zu dem ein Matchcode erzeugt werden soll.
	 * @return der erzeugte Matchcode.
	 */
	//Methode public gemacht um den Zugriff de.tarent.contact.db.SearchField zu erm�glichen.
	//sp�ter sollte hier eine andere L�sung gefunden werden.
	public static String getMatchcode(String column, String value) {
		if (value == null || value.length() == 0)
			return "";

		value = value.toUpperCase().replaceAll("Ä", "AE").replaceAll("Ö", "OE").replaceAll("Ü", "UE").replaceAll("ß", "S");

		StringBuffer buffer = new StringBuffer(value);
		char lastChar = 0;
		int index = 0;
		while (index < buffer.length()) {
			char thisChar = buffer.charAt(index);
			//          if @letter <> @letter_old and ((ascii(@letter) > 64 and ascii(@letter) < 91) or @letter = '*')
			if ((thisChar != lastChar) && ((thisChar == '*') || ((thisChar >= 'A') && (thisChar <= 'Z')))) {
				lastChar = thisChar;
				index++;
			} else
				buffer.deleteCharAt(index);
		}
		if (column != null && column.length() != 0) {
			//          IF UPPER(@columnname) = 'VORNAMESHORT' set @outtext = LEFT('' + @outtext , 1)
			//          IF UPPER(@columnname) = 'STRASSE' set @outtext = LEFT('' + @outtext , 4)
			//          IF UPPER(@columnname) = 'ORT' set @outtext = LEFT('' + @outtext , 4)
			column = column.toUpperCase();
			int len = buffer.length();
			if ("VORNAMESHORT".equals(column)) {
				if (len > 1)
					buffer.delete(1, len);
			} else if ("STRASSE".equals(column) || "ORT".equals(column))
				if (len > 4)
					buffer.delete(4, len);
		}
		return buffer.toString();
	}


}
