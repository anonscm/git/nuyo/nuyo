/* $Id: DupChecks.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 24.07.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.List;

/**
 * Diese Klasse stellt eine Sammlung von Duplikatchecks dar.
 * 
 * @author mikel
 */
public class DupChecks {
    /*
     * �ffentliche Methoden
     */
    public int getSize() {
        return dupCheckIds.size();
    }

    public DupCheck get(int index) throws ContactDBException {
        return db.getDupCheck(getDupCheckId(index));
    }

    public String getDupCheckId(int index) {
        if (index >= getSize())
            return null;
        Object key = dupCheckIds.get(index);
        return (key != null) ? key.toString() : null;
    }

    /*
     * Konstruktoren
     */
    /**
     * 
     */
    public DupChecks(Database db, List dupCheckIds) {
        if (dupCheckIds == null || db == null)
            throw new IllegalArgumentException("DupCheck ben�tigt Initialisierungsargumente ungleich null");
        this.db = db;
        this.dupCheckIds = dupCheckIds;
    }

    /*
     * gesch�tzte Membervariablen
     */
    protected Database db = null;
    protected List dupCheckIds = null;
}
