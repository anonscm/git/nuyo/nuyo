/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class StichworteField extends GenericTextField
{
  public StichworteField()
  {
    super("STICHWORTE", AddressKeys.STICHWORTE, CONTEXT_ADRESS, "GUI_Fields_Stichworte_ToolTip", "GUI_Fields_Stichworte", 0);
  }
}
