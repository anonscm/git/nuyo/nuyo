/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.configuration.office.OfficeConfiguration;
import org.evolvis.liboffice.configuration.office.OfficeConnectionMethod;
import org.evolvis.liboffice.configuration.office.OfficeType;
import org.evolvis.liboffice.configuration.parser.PlainOfficeConfigurationParser;
import org.evolvis.liboffice.ui.swing.conf.FirstRunWizardSwing;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeConfigurationManager
{
	private static Logger logger = Logger.getLogger(OfficeConfigurationManager.class.getName());
	private static OfficeConfigurationManager instance;
	private boolean setup = false;

	private OfficeConfigurationManager()
	{

	}

	public static OfficeConfigurationManager getInstance()
	{
		if(instance == null) instance = new OfficeConfigurationManager();
		return instance;
	}

	public int setup()
	{
		// First try to get office-configuration by tarent-contact configuration-framework
		// At least a type and path is needed for a valid configuration

		int result = -1;

		if(!setup) result = setupByContactConfigManager();

		// If not configured properly yet, try to configure by office-configurator

		if(result < 0 && ConfigManager.getEnvironment().isUserOverridable()) return setupByOfficeConfigurator();

		return result;
	}

	public int setupByOfficeConfigurator()
	{
		boolean result = OfficeAutomat.getOfficeConfigurator().setup(new FirstRunWizardSwing(ApplicationServices.getInstance().getCommonDialogServices().getFrame()));
		if(result) return 2;
		return -2;
	}

	public int setupByContactConfigManager()
	{
		String officeTypeName = ConfigManager.getEnvironment().get(Key.OFFICE_TYPE);
		String officePath = ConfigManager.getEnvironment().get(Key.OFFICE_PATH);

		if(officeTypeName != null && officeTypeName.length() > 0 && officePath != null && officePath.length() > 0)
		{
			OfficeConfiguration officeConf = new OfficeConfiguration();

			OfficeType officeType = PlainOfficeConfigurationParser.getInstance().getOfficeType(officeTypeName.substring(0, officeTypeName.indexOf('-')), officeTypeName.substring(officeTypeName.indexOf('-')+1));

			if(officeType != null)
			{
				officeConf.setType(officeType);
				officeConf.setPath(officePath);

				String connectionTypeName = ConfigManager.getEnvironment().get(Key.OFFICE_CONNECTION_TYPE);

				try
				{
					officeConf.setConnectionMethod((OfficeConnectionMethod)Class.forName(connectionTypeName).newInstance());
				} catch (InstantiationException e)
				{
					e.printStackTrace();
				} catch (IllegalAccessException e)
				{
					e.printStackTrace();
				} catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				}


				Map connectionParams = new HashMap();
				
				
				// TODO put params into map
				//officeConf.set(ConfigManager.getEnvironment().getAsInt(Key.OFFICE_PORT));
				//officeConf.setPipeName(ConfigManager.getEnvironment().get(Key.OFFICE_PIPE_NAME));
				
				officeConf.setConnectionParameters(connectionParams);

				OfficeAutomat.getOfficeConfigurator().addOfficeConfiguration(officeConf);
				OfficeAutomat.getOfficeConfigurator().setPreferredOffice(officeConf);
			}
			else
			{
				logger.warning("Office-Type named "+officeTypeName+" is not defined");
				return -1;
			}
			setup = true;
			return 1;
		}
		return -1;
	}
}