/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Events;

import java.awt.Event;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TarentGUIEvent extends Event
{
  private String m_sName;
  private DataContainer m_sSender;
  private ErrorHint m_oErrorHint;
  private DataContainer m_sSourceContainer;
  private PluginData m_oPluginData;
  
  public TarentGUIEvent(String name, DataContainer sender, ErrorHint errorhint)
  {
    super(null, 0, null);
    m_sName = name;
    m_sSender = sender;
    m_sSourceContainer = null;
    m_oErrorHint = errorhint;
  }

  public TarentGUIEvent(String name, DataContainer sender, ErrorHint errorhint, PluginData pluginData)
  {
      this(name, sender, errorhint);
      setPluginData(pluginData);
  }

  public void setPluginData(PluginData pluginData) {
      m_oPluginData = pluginData;
  }

  public PluginData getPluginData() {
      return m_oPluginData;
  }

  public void setSourceContainer(DataContainer container)
  {
    m_sSourceContainer = container;
  }
  
  public DataContainer getSourceContainer()
  {
    return m_sSourceContainer;
  }
  
  public String getName()
  {
    return(m_sName);
  }
  
  public DataContainer getSender()
  {
    return(m_sSender);
  }

  public void setSender(DataContainer sender)
  {
    m_sSender = sender;
  }
  
  public ErrorHint getErrorHint()
  {
    return(m_oErrorHint);
  }
  
  public String toString()
  {
    return("TarentGUIEvent[" + getName() + ", " + getSender() + "]");
  }
  
  public final static String GUIEVENT_FOCUSGAINED = "GUIEVENT_FOCUSGAINED"; 
  public final static String GUIEVENT_FOCUSLOST = "GUIEVENT_FOCUSLOST"; 
  public final static String GUIEVENT_VALUECHANGED = "GUIEVENT_VALUECHANGED"; 
  
  public final static String GUIEVENT_REQUESTFOCUS = "GUIEVENT_REQUESTFOCUS"; 

  public final static String GUIEVENT_STORE = "GUIEVENT_STORE"; 
  public final static String GUIEVENT_FETCH = "GUIEVENT_FETCH"; 
  public final static String GUIEVENT_SOURCECHANGED = "GUIEVENT_SOURCECHANGED"; 
  
  public final static String GUIEVENT_VALIDATE = "GUIEVENT_VALIDATE"; 
  public final static String GUIEVENT_ACCEPT = "GUIEVENT_ACCEPT"; 
  public final static String GUIEVENT_REJECT = "GUIEVENT_REJECT"; 
  
  public final static String GUIEVENT_DATAINVALID = "GUIEVENT_DATAINVALID";
  
  public final static String GUIEVENT_VALIDATORSTART = "GUIEVENT_VALIDATORSTART"; 
  public final static String GUIEVENT_VALIDATOREND = "GUIEVENT_VALIDATOREND"; 
  
}
