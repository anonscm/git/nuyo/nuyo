package org.evolvis.nuyo.resources;



import javax.swing.ImageIcon;

/**
 * @author HeikoFerger
 *
 * Dieses interface dient zur zentralen verwaltung der Ressourcen
 * damit man nicht immer ein icon �ber eine url (pfad),
 * ein neues icon obejct erzeugen , oder �ber einen controller welcher
 * diese Ressource enth�lt besorgen muss.
 * 
 * Meine bitte ist: dieses Interface beim commit einer neuen Ressource
 * selbst zu pflegen
 * 
 * CU Heiko
 */
public interface Icon {
	/**
	 * Pfade
	 *
	 */
	public String BASE_PATH				=  "src/";
	
	//public URL 	MAIL_ICONS_PATH		= 	null;
//	public 	String 	MAIL_ICONS_PATH		= 	BASE_PATH+"de/tarent/contact/gui/email/resources/";
//	public 	String 	MAIL_ICONS_PATH		= 	BASE_PATH+"de\\tarent\\contact\\gui\\email\\resources\\";
	
	/**
	 * EMail Icons
	 */
	public 	String 	MAIL_ICONS_PATH		= 	BASE_PATH + "de/tarent/contact/gui/email/resources/";
	// MIME-Types evtl. in anderen folder auslagern
	public final static  	ImageIcon	  filetype_archive 							=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_archive.gif");
	public final static  	ImageIcon	  filetype_ascii 								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_ascii.gif");
	public final static  	ImageIcon	  filetype_binary 								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_binary.gif");
	public final static  	ImageIcon	  filetype_bomb 								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_bomb.gif");
	public final static  	ImageIcon	  filetype_doc 									=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_doc.gif");
	public final static  	ImageIcon	  filetype_exe 									=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_exe.gif");
	public final static  	ImageIcon	  filetype_html									=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_html.gif");
	public final static  	ImageIcon	  filetype_image								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_image.gif");
	public final static  	ImageIcon	  filetype_java									=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_java.gif");
	public final static  	ImageIcon	  filetype_pdf									=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_pdf.gif");
	public final static  	ImageIcon	  filetype_sound								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_sound.gif");
	public final static  	ImageIcon	  filetype_unknown							=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_unknown.gif");
	public final static  	ImageIcon	  filetype_vcard								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_vcard.gif");
	public final static  	ImageIcon	  filetype_video								=  new ImageIcon(MAIL_ICONS_PATH+ "filetype_video.gif");
	// icons vom mozilla
	public final static  	ImageIcon	  mail_folder_close							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_close.gif");
	public final static  	ImageIcon	  mail_folder_drafts							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_drafts.gif");
	public final static  	ImageIcon	  mail_folder_inbox							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_inbox.gif");
	public final static  	ImageIcon	  mail_folder_open							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_open.gif");
	public final static  	ImageIcon	  mail_folder_send							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_send.gif");
	public final static  	ImageIcon	  mail_folder_store							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_store.gif");
	public final static  	ImageIcon	  mail_folder_templates					=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_templates.gif");
	public final static  	ImageIcon	  mail_folder_trash							=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_trash.gif");
	public final static  	ImageIcon	  mail_folder_unsend						=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_unsend.gif");
	public final static  	ImageIcon	  mail_folder_localfolder					=  new ImageIcon(MAIL_ICONS_PATH+ "mail_folder_localfolder.gif");
	
	// mail aktionen
	public final static  	ImageIcon	  mail_cancel									=  new ImageIcon(MAIL_ICONS_PATH+ "mail_cancel.gif");
	public final static  	ImageIcon	  mail_delete										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_delete.gif");
	public final static  	ImageIcon	  mail_detach									=  new ImageIcon(MAIL_ICONS_PATH+ "mail_detach.gif");
	public final static  	ImageIcon	  mail_forward									=  new ImageIcon(MAIL_ICONS_PATH+ "mail_forward.gif");
	public final static  	ImageIcon	  mail_in											=  new ImageIcon(MAIL_ICONS_PATH+ "mail_in.gif");
	public final static  	ImageIcon	  mail_new										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_new.gif");
	public final static  	ImageIcon	  mail_none										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_none.gif");
	public final static  	ImageIcon	  mail_out											=  new ImageIcon(MAIL_ICONS_PATH+ "mail_out.gif");
	public final static  	ImageIcon	  mail_print										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_print.gif");
	public final static  	ImageIcon	  mail_read										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_read.gif");
	public final static  	ImageIcon	  mail_reply										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_reply.gif");
	public final static  	ImageIcon	  mail_replyall									=  new ImageIcon(MAIL_ICONS_PATH+ "mail_replyall.gif");
	public final static  	ImageIcon	  mail_save										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_save.gif");
	public final static  	ImageIcon	  mail_send										=  new ImageIcon(MAIL_ICONS_PATH+ "mail_send.gif");
	// mail message icons
	public final static  	ImageIcon	  mail_message_attachment			=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_attachment.gif");
	public final static  	ImageIcon	  mail_message_priority_high			=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_priority_high.gif");
	public final static  	ImageIcon	  mail_message_priority_highest		=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_priority_highest.gif");
	public final static  	ImageIcon	  mail_message_priority_low			=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_priority_low.gif");
	public final static  	ImageIcon	  mail_message_priority_lowest		=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_priority_lowest.gif");
	public final static  	ImageIcon	  mail_message_priority_normal		=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_priority_normal.gif");
	public final static  	ImageIcon	  mail_message_read						=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_read.gif");
	public final static  	ImageIcon	  mail_message_read_forward		=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_read_forward.gif");
	public final static  	ImageIcon	  mail_message_read_reply			=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_read_reply.gif");
	public final static  	ImageIcon	  mail_message_unread					=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_unread.gif");
	public final static  	ImageIcon	  mail_message_unread_forward	=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_unread_forward.gif");
	public final static  	ImageIcon	  mail_message_unread_reply		=  new ImageIcon(MAIL_ICONS_PATH+ "mail_message_unread_reply.gif");
	// schn�de text symbole
	public final static  	ImageIcon	  text_bold										=  new ImageIcon(MAIL_ICONS_PATH+ "text_bold.gif");
	public final static  	ImageIcon	  text_italic										=  new ImageIcon(MAIL_ICONS_PATH+ "text_italic.gif");
	public final static  	ImageIcon	  text_plain										=  new ImageIcon(MAIL_ICONS_PATH+ "text_plain.gif");
	public final static  	ImageIcon	  text_underline								=  new ImageIcon(MAIL_ICONS_PATH+ "text_underline.gif");
	

	/**
	 * @author HeikoFerger
	 * allgemeine ressourcen
	 */
	public 	String 	RESOURCE_PATH		= 	BASE_PATH + "de/tarent/contact/resources/";

	public final static  	ImageIcon	  tab_adresse								=  new ImageIcon(RESOURCE_PATH+ "tab_adresse.gif");

	
}
