/* $Id: ContactControlException.java,v 1.3 2007/03/23 16:30:39 fkoester Exp $
 * 
 * Created on 10.09.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

/**
 * Diese Klasse stellt Contact-Client-interne Ausnahmen dar, die im
 * Controller-Kontext auftreten.
 *  
 * @author mikel
 */
public class ContactControlException extends ContactException {
    /*
     * Konstruktoren
     */
    public ContactControlException(int exId) {
        super(exId);
    }

    public ContactControlException(int exId, String arg0) {
        super(exId, arg0);
    }

    public ContactControlException(int exId, String arg0, Throwable arg1) {
        super(exId, arg0, arg1);
    }

    public ContactControlException(int exId, Throwable arg0) {
        super(exId, arg0);
    }

    /*
     * Fehlerkonstanten
     */
    /**
     * Die Adresse ist keiner Unterkategorie zugeordnet.
     * 
     * @see ActionManager#saveAddress()
     */
    public final static int EX_ADDRESS_NO_SUBCATEGORY = 1;

    /**
     * Die Adresse ist unvollst�ndig.
     * 
     * @see ActionManager#saveAddress()
     */
    public final static int EX_ADDRESS_INCOMPLETE = 2;

    /**
     * Problem beim Versenden einer E-Mail.
     * 
     * @see WindowsApplicationStarter#handledSingleFaxAsEMail(String, de.tarent.contact.db.Address)
     */
    public final static int EX_EMAIL_SEND_ERROR = 3;
    
    /**
     * Emailadresse stellt keine g�ltige Eingabe dar
     * 
     * @see checkEmail() in Address.java
     */
    
    public final static int EX_EMAIL1_INVALID = 4;
    
    public final static int EX_EMAIL2_INVALID = 6;
    
    /**
     * Telefonnummer stellt keine g�ltige Eingabe dar
     * 
     * @see checkPhonenumbers() in Address.java
     */

    public final static int EX_PHONE_INVALID = 5;
    
    /**
     * Saving process was interrupted at some state
     */
    public final static int EX_SAVING_INTERRUPTED = 7;
    
}
