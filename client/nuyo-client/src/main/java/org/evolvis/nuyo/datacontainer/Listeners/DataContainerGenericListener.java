/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Listeners;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAction;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAdapter;


/**
 * @author niko
 *
 */
public class DataContainerGenericListener extends DataContainerListenerAdapter
{
  public DataContainerListener cloneListener()
  {
    DataContainerGenericListener listener = new DataContainerGenericListener();
    listener.setEvent(getEvent());
    listener.setSlot(getSlot());
    listener.m_oDataContainer = m_oDataContainer;
    listener.m_oEventHandler = new DataContainerListenerEventListener();    
    
    Iterator pit = m_oParameterList.iterator();
    while(pit.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(pit.next()));
      listener.addParameter(param.getKey(), param.getValue());
    }

    Iterator ait = m_oActions.iterator();
    while(ait.hasNext())
    {
      DataContainerListenerAction action = ((DataContainerListenerAction)(ait.next()));
      listener.addAction(action.cloneAction());
    }

    return listener;
  }
  
  private String getActionsString()
  {
    String text = "";
    Iterator ait = m_oActions.iterator();
    while(ait.hasNext())
    {
      DataContainerListenerAction action = ((DataContainerListenerAction)(ait.next()));
      text += action.getDataContainerObjectDescriptor().getContainerName() + ", ";
    }
    text = text.trim();
    if (text.endsWith(",")) text = text.substring(0, text.length()-1);
    return text;
  }
  
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.LISTENER, "DataContainerGenericListener(" + getActionsString() + ")", new DataContainerVersion(0));
    return d;
  }
  

}
