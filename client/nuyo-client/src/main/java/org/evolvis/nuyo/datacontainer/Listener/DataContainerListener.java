/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Listener;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObject;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;


/**
 * @author niko
 *
 */
public interface DataContainerListener extends DataContainerObject
{
  public DataContainerListener cloneListener();
  public void init();
  public void dispose();
  
  public void addAction(DataContainerListenerAction action);
  public void removeAction(DataContainerListenerAction action);
  
  public void setEvent(TarentGUIEvent event);
  public void setSlot(String slot);
  
  public TarentGUIEvent getEvent();
  public String getSlot();

  public boolean installListener();
  public boolean disposeListener();
  
  public boolean addParameter(String key, String value);
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor();

  public DataContainer getDataContainerToListenOn();
  
  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();

  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);      
  public void fireTarentGUIEvent(TarentGUIEvent e);
  
}
