package org.evolvis.nuyo.selector;

import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.Starter;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.swing.utils.SwingIconFactory;

import de.tarent.commons.ui.swing.LoginDialog;

/**
 * Starter Klasse f�r den Selector
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class SelectorStarter extends Starter {
    
    private static Logger logger = Logger.getLogger(SelectorStarter.class.getName());
    
    public SelectorStarter(){
        super();
    }
    
    public static void main(String[] args) {
       
        SelectorStarter starter = new SelectorStarter();
        logger.finer("initialisiere Anwendung");
        starter.init(args);
    }
    
    /**
     * this method overrides a method of the Starter-Superclass 
     * in this case the application is the AddressSelector Standalone
     *
     */
    @Override
    protected void startApplication(Database database, LoginDialog loginProvider){
        
        //Caution, StartScreen still open!
        try {
            loginProvider.setStatusText("initialisiere Selector");
            loginProvider.setStatusText("lade Daten für Benutzer "+ database.getUser());
            database.loadCategories();
            
            loginProvider.setStatusText("Initialisiere Aktionen");
//          Load the action definitions.
            ActionRegistry ar = ActionRegistry.getInstance();
            ar.setIconFactory(SwingIconFactory.getInstance());
            ar.init(ConfigManager.getAppearance().getActionDefinitions());

            loginProvider.setStatusText("fertig \n");
            loginProvider.setDialogVisible(false);

//            //set look and feel:
//            try {
//                
//                UIManager.setLookAndFeel(new TarentPlasticLookAndFeel());
//            } catch (Exception e1) {}
//            
            SelectorFrame selector = new SelectorFrame();
            selector.getFrame().setVisible(true);
            
        } catch (ContactDBException e) {
            logger.warning("bei der Initialisierung ist ein Fehler aufgetreten");
            ApplicationServices.getInstance().getCommonDialogServices().publishError("Bei der Initialisierung ist ein Fehler aufgetreten.",e);
        }
        
    }
    
}

