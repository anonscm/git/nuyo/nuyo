/*
 * Created on 28.09.2004
 *
 Document document = getParsedDocument(.....);
 
 NodeList oGUIFieldsNodes = document.getElementsByTagName("guifields");
 if (oGUIFieldsNodes != null) 
 {
 errors.addAll(m_oDataContainerParser.parse(oGUIFieldsNodes));
 }      
 */
package org.evolvis.nuyo.datacontainer.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerManager;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DescriptedDataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.GenericDataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorage;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.FieldDescriptors.FieldDescriptor;
import org.evolvis.nuyo.datacontainer.FieldDescriptors.SimpleFieldDescriptor;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAction;
import org.evolvis.nuyo.datacontainer.Listeners.DataContainerGenericListener;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.util.general.DataAccess;
import org.evolvis.nuyo.util.parser.GUIParseError;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * @author niko
 *
 */
public class DataContainerParser implements DataContainerProvider {
    private AddressFieldFactory addressFieldFactory;
    private PluginLocator pluginLocator;
    private DataAccess dataAccess;
    private Map dataContainerMap;
    private Logger logger;

    public DataContainerParser( Logger aLogger, PluginLocator locator, DataAccess dataaccess, String[] pluginpath ) {
        dataContainerMap = new HashMap();
        logger = aLogger;
        pluginLocator = locator;
        dataAccess = dataaccess;

        addressFieldFactory = new AddressFieldFactory( pluginLocator, logger, pluginpath );
    }

    public DataContainerParser( Logger aLogger, PluginLocator locator, DataAccess dataaccess ) {
        dataContainerMap = new HashMap();
        logger = aLogger;
        pluginLocator = locator;
        dataAccess = dataaccess;

        String[] pluginpath = null;
        addressFieldFactory = new AddressFieldFactory( pluginLocator, logger, pluginpath );
    }

    public String dumpConfig() {
        return addressFieldFactory.dumpConfig();
    }

    public Map getDataContainerMap() {
        return dataContainerMap;
    }

    /** All registered data containers will be added to a given manager. */
    public void addAllDataContainersToObserve( DataContainerManager manager ) {
        Iterator it = dataContainerMap.values().iterator();
        DataContainer nextContainer;
        while ( it.hasNext() ) {
            nextContainer = (DataContainer) ( it.next() );
            manager.addDataContainerToObserve( nextContainer );
        }
    }

    public DataContainer getDataContainer( Object key ) {
        return (DataContainer) ( dataContainerMap.get( key ) );
    }

    public Iterator iterator() {
        return dataContainerMap.values().iterator();
    }

    public Object[] getDataContainerKeys() {
        return dataContainerMap.keySet().toArray();
    }

    public List parse( NodeList nodes ) {
        return createGUIFields( nodes );
    }

    private List createGUIFields( NodeList nodes ) {
        List errors = new ArrayList();

        if ( nodes.getLength() > 0 ) {

            Element node = null;
            for ( int i = 0; i < ( nodes.getLength() ); i++ ) {
                node = (Element) nodes.item( i );
                if ( node != null ) {
                    NodeList oGUIFieldNodes = node.getElementsByTagName( "guifield" );
                    if ( oGUIFieldNodes.getLength() > 0 ) {
                        if ( nodes.getLength() > 1 )
                            errors
                                .add( new GUIParseError( GUIParseError.ERROR_TOOMUCHGUIELEMENTNODES, "guifields Tag" ) );
                        for ( int n = 0; n < ( oGUIFieldNodes.getLength() ); n++ ) {
                            Element fieldnode = (Element) oGUIFieldNodes.item( n );
                            if ( fieldnode != null ) {
                                errors.addAll( createGUIField( fieldnode ) );
                            }
                        }
                    }
                    else {
                        // wird von tarentWizard verwendet, da eine NodeList von guifield's uebergeben wird
                        errors.addAll( createGUIField( node ) );
                    }
                }
                else
                    System.out.println( "ERROR: DataContainerParser createGUIFields node is null" );
            }
        }
        else {
            logger.config( "No guifields definition found in config. Only build in fields usable." );
        }
        return errors;
    }

    private List createGUIField( Element fieldnode ) {
        
        List errors = new ArrayList();

        String sFieldName = fieldnode.getAttribute( "name" );
        String sFieldLabel = fieldnode.getAttribute( "label" );
        String sFieldDescription = fieldnode.getAttribute( "description" );

        NodeList oDataSourceNodes = fieldnode.getElementsByTagName( "datasource" );
        NodeList oWidgetNodes = fieldnode.getElementsByTagName( "widget" );
        NodeList oValidatorNodes = fieldnode.getElementsByTagName( "validator" );
        NodeList oListenerNodes = fieldnode.getElementsByTagName( "listener" );

        if ( oDataSourceNodes.getLength() == 1 ) {
            if ( oWidgetNodes.getLength() == 1 ) {
                // alles ok... wir haben genau eine DataSource
                // und genau ein Widget...
                Element oDataSourceNode = (Element) oDataSourceNodes.item( 0 );

                String sDataSourceFieldKey = oDataSourceNode.getAttribute( "fieldkey" );
                String sDataSourceDataType = oDataSourceNode.getAttribute( "datatype" );

                // alle informationen zum erzeugen einer datasource sind nun verf�gbar...
                DataSource oDataSource = addressFieldFactory.getDataSource( sDataSourceDataType );
                DataStorage oDataStorage = addressFieldFactory.getDataStorage( sDataSourceDataType );

                if ( oDataSource != null ) {
                    if ( oDataStorage != null ) {
                        // alles ok... instanzen von DataSource und DataStorage gefunden...
                        oDataSource.setDBKey( sDataSourceFieldKey );

                        // parameter der datasource anf�gen...
                        NodeList oDataSourceParamNodes = fieldnode.getElementsByTagName( "datasourceParam" );
                        for ( int dspi = 0; dspi < ( oDataSourceParamNodes.getLength() ); dspi++ ) {
                            Element oDataSourceParamNode = (Element) oDataSourceParamNodes.item( dspi );
                            String sDataSourceParamNodeName = oDataSourceParamNode.getAttribute( "name" );
                            String sDataSourceParamNodeValue = getNodeText( oDataSourceParamNode );
                            if ( !oDataSource.addParameter( sDataSourceParamNodeName, sDataSourceParamNodeValue ) ) {
                                String accepted = getAcceptedParameterValues( oDataSource, sDataSourceParamNodeName );
                                if ( accepted != null ) {
                                    errors.add( new GUIParseError( GUIParseError.ERROR_DATASOURCE, sFieldName
                                        + ": ung�ltiger Parameter-Wert f�r DataSource", sDataSourceParamNodeName
                                        + " = " + sDataSourceParamNodeValue,
                                                                   "es wurde ein Parameter-Wert �bergeben der nicht unterst�tzt wird. Erlaubte Werte sind: "
                                                                       + accepted ) );
                                }
                                else {
                                    String params = getAcceptedParameters( oDataSource );
                                    if ( params != null ) {
                                        errors.add( new GUIParseError( GUIParseError.ERROR_DATASOURCE, sFieldName
                                            + ": unbekannter Parameter f�r DataSource", sDataSourceParamNodeName
                                            + " = " + sDataSourceParamNodeValue,
                                                                       "es wurde ein Parameter �bergeben der nicht unterst�tzt wird. Erlaubte Parameter sind: "
                                                                           + params ) );
                                    }
                                    else {
                                        errors
                                            .add( new GUIParseError( GUIParseError.ERROR_DATASOURCE, sFieldName
                                                + ": unbekannter Parameter f�r DataSource", sDataSourceParamNodeName
                                                + " = " + sDataSourceParamNodeValue,
                                                                     "diese Datenquelle unterst�tz keine Parameter." ) );
                                    }
                                }
                            }
                        }

                        // datasource und datastorage sind nun einsatzbereit...

                        // Widget erzeugen...
                        Element oWidgetNode = (Element) oWidgetNodes.item( 0 );

                        String sWidgetType = oWidgetNode.getAttribute( "widgetType" );
                        GUIElement oWidget = addressFieldFactory.getGUIElement( sWidgetType );
                        if ( oWidget != null ) {
                            // parameter des widgets anf�gen...
                            NodeList oWidgetParamNodes = oWidgetNode.getElementsByTagName( "widgetParam" );
                            for ( int wpi = 0; wpi < ( oWidgetParamNodes.getLength() ); wpi++ ) {
                                Element oWidgetParamNode = (Element) oWidgetParamNodes.item( wpi );
                                String sWidgetParamNodeName = oWidgetParamNode.getAttribute( "name" );
                                String sWidgetParamNodeValue = getNodeText( oWidgetParamNode );
                                if ( !oWidget.addParameter( sWidgetParamNodeName, sWidgetParamNodeValue ) ) {
                                    String accepted = getAcceptedParameterValues( oWidget, sWidgetParamNodeName );
                                    if ( accepted != null ) {
                                        errors.add( new GUIParseError( GUIParseError.ERROR_WIDGET, sFieldName
                                            + ": ung�ltiger Parameter-Wert f�r Widget", sWidgetParamNodeName + " = "
                                            + sWidgetParamNodeValue,
                                                                       "es wurde ein Parameter-Wert �bergeben der nicht unterst�tzt wird. Erlaubte Werte sind: "
                                                                           + accepted ) );
                                    }
                                    else {
                                        String params = getAcceptedParameters( oWidget );
                                        if ( params != null ) {
                                            errors.add( new GUIParseError( GUIParseError.ERROR_WIDGET, sFieldName
                                                + ": unbekannter Parameter f�r Widget", sWidgetParamNodeName + " = "
                                                + sWidgetParamNodeValue,
                                                                           "es wurde ein Parameter �bergeben der nicht unterst�tzt wird. Erlaubte Parameter sind:"
                                                                               + params ) );
                                        }
                                        else {
                                            errors
                                                .add( new GUIParseError( GUIParseError.ERROR_WIDGET, sFieldName
                                                    + ": unbekannter Parameter f�r Widget", sWidgetParamNodeName
                                                    + " = " + sWidgetParamNodeValue,
                                                                         "dieses Oberfl�chenelement unterst�tzt keine Parameter." ) );
                                        }
                                    }
                                }
                            }

                            // datasource, datastorage und widget sind nun einsatzbereit...

                            // DataContainer erzeugen und initialisieren... 
                            DataContainer oDataContainer = new GenericDataContainer();
                            oDataContainer.setDataMap( m_oDataMap );
                            oDataContainer.setLogger( logger );
                            oDataContainer.setDataAccess( dataAccess );
                            oDataContainer.setDataStorage( oDataStorage );
                            FieldDescriptor descriptor = new SimpleFieldDescriptor();
                            descriptor.setName( sFieldLabel );
                            descriptor.setDescription( sFieldDescription );
                            oDataContainer.setFieldDescriptor( descriptor );
                            oDataContainer.setDataSource( oDataSource );
                            oDataContainer.setGUIElement( oWidget );

                            // falls vorhanden Validatoren hinzuf�gen...
                            for ( int i = 0; i < ( oValidatorNodes.getLength() ); i++ ) {
                                Element oValidatorNode = (Element) oValidatorNodes.item( i );

                                String sValidatorType = oValidatorNode.getAttribute( "validatorType" );
                                Validator oValidator = addressFieldFactory.getValidator( sValidatorType );
                                if ( oValidator != null ) {
                                    // parameter des validators anf�gen...
                                    NodeList oValidatorParamNodes = oValidatorNode
                                        .getElementsByTagName( "validatorParam" );
                                    for ( int vpi = 0; vpi < ( oValidatorParamNodes.getLength() ); vpi++ ) {
                                        Element oValidatorParamNode = (Element) oValidatorParamNodes.item( vpi );
                                        String sValidatorParamNodeName = oValidatorParamNode.getAttribute( "name" );
                                        String sValidatorParamNodeValue = getNodeText( oValidatorParamNode );
                                        if ( !oValidator.addParameter( sValidatorParamNodeName,
                                                                       sValidatorParamNodeValue ) ) {
                                            String accepted = getAcceptedParameterValues( oValidator,
                                                                                          sValidatorParamNodeName );
                                            if ( accepted != null ) {
                                                errors
                                                    .add( new GUIParseError( GUIParseError.ERROR_VALIDATOR, sFieldName
                                                        + ": ung�ltiger Parameter-Wert f�r Validator", "\""
                                                        + sValidatorParamNodeName + " = " + sValidatorParamNodeValue
                                                        + "\"",
                                                                             "es wurde ein Parameter-Wert �bergeben der nicht unterst�tzt wird. Erlaubte Werte sind: "
                                                                                 + accepted ) );
                                            }
                                            else {
                                                String params = getAcceptedParameters( oValidator );
                                                if ( params != null ) {
                                                    errors
                                                        .add( new GUIParseError(
                                                                                 GUIParseError.ERROR_VALIDATOR,
                                                                                 sFieldName
                                                                                     + ": unbekannter Parameter f�r Validator",
                                                                                 "\"" + sValidatorParamNodeName + " = "
                                                                                     + sValidatorParamNodeValue + "\"",
                                                                                 "es wurde ein Parameter �bergeben der nicht unterst�tzt wird. Erlaubte Parameter sind: "
                                                                                     + params ) );
                                                }
                                                else {
                                                    errors
                                                        .add( new GUIParseError(
                                                                                 GUIParseError.ERROR_VALIDATOR,
                                                                                 sFieldName
                                                                                     + ": unbekannter Parameter f�r Validator",
                                                                                 "\"" + sValidatorParamNodeName + " = "
                                                                                     + sValidatorParamNodeValue + "\"",
                                                                                 "dieser Validator unterst�tzt keine Parameter." ) );
                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    // unbekannter Validator
                                    errors
                                        .add( new GUIParseError( GUIParseError.ERROR_VALIDATOR, sFieldName
                                            + ": unbekannter Validator", "\"" + sValidatorType + "\"",
                                                                 "es wurde versucht einen Validator zu benutzen der nicht vorhanden war." ) );
                                }
                                // validator ist nun initialisiert...
                                oDataContainer.addValidator( oValidator );
                            }

                            // falls vorhanden Listener hinzuf�gen...
                            for ( int i = 0; i < ( oListenerNodes.getLength() ); i++ ) {
                                Element oListenerNode = (Element) oListenerNodes.item( i );

                                String sListenerEvent = oListenerNode.getAttribute( "listenerEvent" );
                                String sListenerSlot = oListenerNode.getAttribute( "listenerSlot" );
                                DataContainerListener oListener = new DataContainerGenericListener();

                                if ( oListener != null ) {
                                    oListener.setEvent( getEventByName( sListenerEvent ) );
                                    oListener.setSlot( sListenerSlot );

                                    // listener ist nun initialisiert...
                                    oDataContainer.addListener( oListener );

                                    // parameter des listeners anf�gen...
                                    NodeList oListenerParamNodes = oListenerNode.getElementsByTagName( "listenerParam" );
                                    for ( int lpi = 0; lpi < ( oListenerParamNodes.getLength() ); lpi++ ) {
                                        Element oListenerParamNode = (Element) oListenerParamNodes.item( lpi );
                                        String sListenerParamNodeName = oListenerParamNode.getAttribute( "name" );
                                        String sListenerParamNodeValue = getNodeText( oListenerParamNode );
                                        if ( !oListener.addParameter( sListenerParamNodeName, sListenerParamNodeValue ) ) {
                                            String accepted = getAcceptedParameterValues( oListener,
                                                                                          sListenerParamNodeName );
                                            if ( accepted != null ) {
                                                errors.add( new GUIParseError( GUIParseError.ERROR_LISTENER, sFieldName
                                                    + ": ung�ltiger Parameter-Wert f�r Listener", "\""
                                                    + sListenerParamNodeName + " = " + sListenerParamNodeValue + "\"",
                                                                               "es wurde ein Parameter-Wert �bergeben der nicht unterst�tzt wird. Erlaubte Werte sind: "
                                                                                   + accepted ) );
                                            }
                                            else {
                                                String params = getAcceptedParameters( oListener );
                                                if ( params != null ) {
                                                    errors
                                                        .add( new GUIParseError(
                                                                                 GUIParseError.ERROR_LISTENER,
                                                                                 sFieldName
                                                                                     + ": unbekannter Parameter f�r Listener",
                                                                                 "\"" + sListenerParamNodeName + " = "
                                                                                     + sListenerParamNodeValue + "\"",
                                                                                 "es wurde ein Parameter �bergeben der nicht unterst�tzt wird. Erlaubte Parameter sind: "
                                                                                     + params ) );
                                                }
                                                else {
                                                    errors
                                                        .add( new GUIParseError(
                                                                                 GUIParseError.ERROR_LISTENER,
                                                                                 sFieldName
                                                                                     + ": unbekannter Parameter f�r Listener",
                                                                                 "\"" + sListenerParamNodeName + " = "
                                                                                     + sListenerParamNodeValue + "\"",
                                                                                 "dieser Listener unterst�tzt keine Parameter." ) );
                                                }
                                            }
                                        }
                                    }

                                    // actions des listeners anf�gen...
                                    NodeList oListenerActionNodes = oListenerNode.getElementsByTagName( "action" );
                                    for ( int ai = 0; ai < ( oListenerActionNodes.getLength() ); ai++ ) {
                                        Element oActionNode = (Element) oListenerActionNodes.item( ai );
                                        String sActionNodeType = oActionNode.getAttribute( "actionType" );

                                        DataContainerListenerAction action = addressFieldFactory
                                            .getAction( sActionNodeType );
                                        if ( action != null ) {
                                            // parameter der action anf�gen...
                                            NodeList oActionParamNodes = oActionNode
                                                .getElementsByTagName( "actionParam" );
                                            for ( int api = 0; api < ( oActionParamNodes.getLength() ); api++ ) {
                                                Element oActionParamNode = (Element) oActionParamNodes.item( api );
                                                String sActionParamNodeName = oActionParamNode.getAttribute( "name" );
                                                String sActionParamNodeValue = getNodeText( oActionParamNode );
                                                if ( !action.addParameter( sActionParamNodeName, sActionParamNodeValue ) ) {
                                                    String accepted = getAcceptedParameterValues( action,
                                                                                                  sActionParamNodeName );
                                                    if ( accepted != null ) {
                                                        errors
                                                            .add( new GUIParseError(
                                                                                     GUIParseError.ERROR_ACTION,
                                                                                     sFieldName
                                                                                         + ": ung�ltiger Parameter-Wert f�r Action",
                                                                                     "\"" + sActionParamNodeName
                                                                                         + " = "
                                                                                         + sActionParamNodeValue + "\"",
                                                                                     "es wurde ein Parameter-Wert �bergeben der nicht unterst�tzt wird. Erlaubte Werte sind: "
                                                                                         + accepted ) );
                                                    }
                                                    else {
                                                        String params = getAcceptedParameters( action );
                                                        if ( params != null ) {
                                                            errors
                                                                .add( new GUIParseError(
                                                                                         GUIParseError.ERROR_ACTION,
                                                                                         sFieldName
                                                                                             + ": unbekannter Parameter f�r Action",
                                                                                         "\"" + sActionParamNodeName
                                                                                             + " = "
                                                                                             + sActionParamNodeValue
                                                                                             + "\"",
                                                                                         " es wurde ein Parameter �bergeben der nicht unterst�tzt wird. Erlaubte Parameter sind: "
                                                                                             + params ) );
                                                        }
                                                        else {
                                                            errors
                                                                .add( new GUIParseError(
                                                                                         GUIParseError.ERROR_ACTION,
                                                                                         sFieldName
                                                                                             + ": unbekannter Parameter f�r Action",
                                                                                         "\"" + sActionParamNodeName
                                                                                             + " = "
                                                                                             + sActionParamNodeValue
                                                                                             + "\"",
                                                                                         "diese Action unterst�tzt keine Parameter." ) );
                                                        }
                                                    }
                                                }
                                            }

                                            oListener.addAction( action );
                                        }
                                        else {
                                            // unbekannte Action
                                            errors
                                                .add( new GUIParseError( GUIParseError.ERROR_ACTION, sFieldName
                                                    + ": unbekannte Action", "\"" + sActionNodeType + "\"",
                                                                         "es wurde versucht eine Aktion zu benutzen der nicht vorhanden war." ) );
                                        }
                                    }

                                }
                                else {
                                    // unbekannter Listener
                                    errors
                                        .add( new GUIParseError( GUIParseError.ERROR_LISTENER, sFieldName
                                            + ": unbekannter Listener", "\"" + sListenerEvent + "\"",
                                                                 "es wurde versucht einen Listener zu benutzen der nicht vorhanden war." ) );
                                }
                            }

                            // nun ist das DataContainer Objekt fertig initialisiert!
                            oDataContainer.setKey( sFieldName );
                            dataContainerMap.put( sFieldName, oDataContainer );
                            // damit sind wir fertig...
                            // errors sollte leer sein und wird am ende der methode zur�ckgegeben...
                        }
                        else
                            errors.add( new GUIParseError( GUIParseError.ERROR_WIDGET, sFieldName
                                + ": unbekannter Widgettyp", sWidgetType,
                                                           "es wurde ein Widgettyp angegeben der nicht bekannt ist." ) );
                    }
                    else
                        errors
                            .add( new GUIParseError( GUIParseError.ERROR_DATASTORAGE, sFieldName
                                + ": unbekannter Datentyp f�r DataStorage", sDataSourceDataType,
                                                     "es wurde ein Datentyp einer Datenquelle angegeben der nicht unterst�tzt wird." ) );
                }
                else
                    errors
                        .add( new GUIParseError( GUIParseError.ERROR_DATASOURCE, sFieldName
                            + ": unbekannter Datentyp f�r DataSource", sDataSourceDataType,
                                                 "es wurde ein Datentyp einer Datenquelle angegeben der nicht unterst�tzt wird." ) );
            }
            else {
                if ( oWidgetNodes.getLength() == 0 )
                    errors.add( new GUIParseError( GUIParseError.ERROR_NOWIDGET, "Name="
                        + sFieldName ) );
                else
                    errors.add( new GUIParseError( GUIParseError.ERROR_MULTIPLEWIDGETS, "Name="
                        + sFieldName ) );
            }
        }
        else {
            if ( oDataSourceNodes.getLength() == 0 )
                errors.add( new GUIParseError( GUIParseError.ERROR_NODATASOURCE, "Name="
                    + sFieldName ) );
            else
                errors.add( new GUIParseError( GUIParseError.ERROR_MULTIPLEDATASOURCES, "Name="
                    + sFieldName ) );
        }

        return errors;
    }

    private TarentGUIEvent getEventByName( String name ) {
        if ( TarentGUIEvent.GUIEVENT_FOCUSLOST.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_FOCUSLOST, null, null );
        else if ( TarentGUIEvent.GUIEVENT_FOCUSGAINED.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_FOCUSGAINED, null, null );
        else if ( TarentGUIEvent.GUIEVENT_ACCEPT.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_ACCEPT, null, null );
        else if ( TarentGUIEvent.GUIEVENT_DATAINVALID.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_DATAINVALID, null, null );
        else if ( TarentGUIEvent.GUIEVENT_FETCH.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_FETCH, null, null );
        else if ( TarentGUIEvent.GUIEVENT_REJECT.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_REJECT, null, null );
        else if ( TarentGUIEvent.GUIEVENT_REQUESTFOCUS.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_REQUESTFOCUS, null, null );
        else if ( TarentGUIEvent.GUIEVENT_SOURCECHANGED.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_SOURCECHANGED, null, null );
        else if ( TarentGUIEvent.GUIEVENT_STORE.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_STORE, null, null );
        else if ( TarentGUIEvent.GUIEVENT_VALIDATE.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_VALIDATE, null, null );
        else if ( TarentGUIEvent.GUIEVENT_VALIDATOREND.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_VALIDATOREND, null, null );
        else if ( TarentGUIEvent.GUIEVENT_VALIDATORSTART.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_VALIDATORSTART, null, null );
        else if ( TarentGUIEvent.GUIEVENT_VALUECHANGED.equalsIgnoreCase( "GUIEVENT_" + name ) )
            return new TarentGUIEvent( TarentGUIEvent.GUIEVENT_VALUECHANGED, null, null );
        else
            return null;
    }

    private String getNodeText( Element node ) {
        String text = "";
        NodeList nodelist = node.getChildNodes();
        for ( int i = 0; i < ( nodelist.getLength() ); i++ ) {
            Node subnode = (Node) nodelist.item( i );
            String nodetext = subnode.getNodeValue();
            text += nodetext;
        }

        return text;
    }

    private String getAcceptedParameterValues( DescriptedDataContainer container, String parameterkey ) {
        DataContainerObjectDescriptor descriptor = container.getDataContainerObjectDescriptor();
        List descriptions = descriptor.getParameterDescriptions();
        Iterator it = descriptions.iterator();
        ParameterDescription description = null;
        while ( it.hasNext() ) {
            ParameterDescription tmpdescription = (ParameterDescription) ( it.next() );
            if ( tmpdescription.getKey().equals( parameterkey ) ) {
                description = tmpdescription;
            }
        }

        if ( description != null ) {
            String text = "";
            if ( description.getValueDescriptions() != null ) {
                Iterator valdescit = description.getValueDescriptions().iterator();
                while ( valdescit.hasNext() ) {
                    String[] valdesc = (String[]) ( valdescit.next() );
                    text += "[\"" + valdesc[0] + "\" : " + valdesc[1] + "] ";
                }
                return text;
            }
        }

        return null;
    }

    private String getAcceptedParameters( DescriptedDataContainer container ) {
        DataContainerObjectDescriptor descriptor = container.getDataContainerObjectDescriptor();
        List descriptions = descriptor.getParameterDescriptions();
        if ( descriptions != null ) {
            String text = "";
            Iterator it = descriptions.iterator();
            while ( it.hasNext() ) {
                ParameterDescription tmpdescription = (ParameterDescription) ( it.next() );
                text += "[\"" + tmpdescription.getKey() + "\" = " + tmpdescription.getName() + " ("
                    + tmpdescription.getDescription() + ")] ";
            }
            return text;
        }
        return null;
    }

    private Map m_oDataMap = null;

    public void setDataMap( Map map ) {
        m_oDataMap = map;
    }

    public Map getDataMap() {
        return m_oDataMap;
    }

}
