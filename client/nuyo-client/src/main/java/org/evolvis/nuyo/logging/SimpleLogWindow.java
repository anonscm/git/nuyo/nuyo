/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 Copyright (C) 2002 tarent GmbH

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 tarent GmbH., hereby disclaims all copyright
 interest in the program 'tarent-contact'
 (which makes passes at compilers) written
 by Nikolai R�ther.
 signature of Elmar Geese, 1 June 2002
 Elmar Geese, CEO tarent GmbH*/
package org.evolvis.nuyo.logging;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.evolvis.nuyo.controls.TarentFileChooser;
import org.evolvis.nuyo.gui.AdvancedPanelInserter;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.PanelInserter;

import de.tarent.commons.ui.EscapeDialog;

/**
 * 
 * TODO rewrite complete dialog
 * 
 * @author niko
 * 
 */
public class SimpleLogWindow extends EscapeDialog implements LogWindow, TarentLogListener {

	private static final DateFormat	DATE_FORMATTER			= DateFormat.getDateTimeInstance();

	private static final String		SUPPORT_EMAIL_ADDRESS	= "support@tarent.de";

	private JDialog					instance;
	private JTable					logTable;
	private LogTableModel			logTableModel;
	private List					listenersList;
	
	private TarentLogger			tarentLogger;

	private ImageIcon				icon_CONFIG;
	private ImageIcon				icon_FINE;
	private ImageIcon				icon_FINER;
	private ImageIcon				icon_FINEST;
	private ImageIcon				icon_INFO;
	private ImageIcon				icon_SEVERE;
	private ImageIcon				icon_WARNING;
	private ImageIcon				icon_UNKNOWN;

	private ImageIcon				icon_SaveLog;
	private ImageIcon				icon_MailLog;
	private ImageIcon				icon_Version;

	private ImageIcon				windowIcon;

	private JLabel					configLabel;
	private JLabel					warnLabel;
	private JLabel					severeLabel;
	private JLabel					infoLabel;
	private JLabel					otherLabel;

	private JPanel					paramPanel;
	private JPanel					throwPanel;
	private JPanel					detailPanel;

	private JCheckBox				showDetailsCheckBox;
	private JSpinner				maxEntitySpinner;

	private JTable					paramTable;
	private ParamTableModel			paramTableModel;
	private JTextArea				throwTextArea;

	private boolean					isToShowIcons			= true;

	private GUIListener				guiListener;

	private String					starterPath				= ".";


	public SimpleLogWindow(GUIListener gl, TarentLogger logger, URL imagebase) {
		
		guiListener = gl;
		tarentLogger = logger;
		tarentLogger.addLogListener(this);
		listenersList = new ArrayList();
		instance = this;

		try {
			icon_CONFIG = new ImageIcon(new URL(imagebase, "loglevel_config.gif"));
			icon_FINE = new ImageIcon(new URL(imagebase, "loglevel_fine.gif"));
			icon_FINER = new ImageIcon(new URL(imagebase, "loglevel_finer.gif"));
			icon_FINEST = new ImageIcon(new URL(imagebase, "loglevel_finest.gif"));
			icon_INFO = new ImageIcon(new URL(imagebase, "loglevel_info.gif"));
			icon_SEVERE = new ImageIcon(new URL(imagebase, "loglevel_severe.gif"));
			icon_WARNING = new ImageIcon(new URL(imagebase, "loglevel_warning.gif"));
			icon_UNKNOWN = new ImageIcon(new URL(imagebase, "loglevel_unknown.gif"));
			icon_SaveLog = new ImageIcon(new URL(imagebase, "log_save.gif"));
			icon_MailLog = new ImageIcon(new URL(imagebase, "log_mail.gif"));
			windowIcon = new ImageIcon(new URL(imagebase, "showlog.gif"));
			icon_Version = new ImageIcon(new URL(imagebase, "showversion.gif"));
		} catch (MalformedURLException e) {
			isToShowIcons = false;
		}

		JPanel mainpanel = new JPanel();
		
		mainpanel.setLayout(new BorderLayout());		

		// ------------------------------------------------

		JPanel statpanel = new JPanel(new GridLayout(0, 2));

		configLabel = createLabel("0", SwingConstants.LEFT);
		warnLabel = createLabel("0", SwingConstants.LEFT);
		severeLabel = createLabel("0", SwingConstants.LEFT);
		infoLabel = createLabel("0", SwingConstants.LEFT);
		otherLabel = createLabel("0", SwingConstants.LEFT);

		statpanel.add(createLabel("Severe: ", SwingConstants.RIGHT));
		statpanel.add(severeLabel);
		statpanel.add(createLabel("Warn: ", SwingConstants.RIGHT));
		statpanel.add(warnLabel);
		statpanel.add(createLabel("Info: ", SwingConstants.RIGHT));
		statpanel.add(infoLabel);
		statpanel.add(createLabel("Config: ", SwingConstants.RIGHT));
		statpanel.add(configLabel);
		statpanel.add(createLabel("Other: ", SwingConstants.RIGHT));
		statpanel.add(otherLabel);

		// ------------------------------------------------

		JLabel titlelabel = new JLabel("Meldungen");

		// ------------------------------------------------

		JPanel titelpanel = new JPanel(new BorderLayout());
		titelpanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		titelpanel.add(titlelabel, BorderLayout.WEST);
		titelpanel.add(statpanel, BorderLayout.EAST);

		// ------------------------------------------------

		logTableModel = new LogTableModel();
		logTable = new JTable(logTableModel);
		if (isToShowIcons)
			logTable.getColumnModel().getColumn(0).setCellRenderer(new LevelCellRenderer());

		TableColumn column0 = logTable.getColumnModel().getColumn(0);
		if (isToShowIcons) {
			column0.setMinWidth(20);
			column0.setMaxWidth(20);
			column0.setPreferredWidth(20);
		} else {
			column0.setPreferredWidth(40);
		}

		TableColumn column1 = logTable.getColumnModel().getColumn(1);
		
		column1.setMinWidth(150);
		column1.setMaxWidth(150);
		column1.setPreferredWidth(150);

		logTable.getSelectionModel().addListSelectionListener(new LogListSelectionListener());
		
		JScrollPane tablescroll = new JScrollPane(logTable);
		// ------------------------------------------------

		paramPanel = new JPanel(new BorderLayout());
		paramTableModel = new ParamTableModel();
		paramTable = new JTable(paramTableModel);
		paramPanel.add(createLabel("Parameter:", SwingConstants.LEFT), BorderLayout.NORTH);
		paramPanel.add(new JScrollPane(paramTable), BorderLayout.CENTER);

		throwPanel = new JPanel(new BorderLayout());
		throwTextArea = new JTextArea();
		throwTextArea.setEditable(false);
		throwTextArea.setLineWrap(false);
		throwTextArea.setWrapStyleWord(false);
		throwPanel.add(createLabel("Exception:", SwingConstants.LEFT), BorderLayout.NORTH);
		throwPanel.add(new JScrollPane(throwTextArea), BorderLayout.CENTER);

		detailPanel = new JPanel(new GridLayout(1, 2));
		Dimension detailsize = new Dimension(0, 200);
		detailPanel.setMinimumSize(detailsize);
		detailPanel.setMaximumSize(detailsize);
		detailPanel.setPreferredSize(detailsize);
		detailPanel.add(paramPanel);
		detailPanel.add(throwPanel);
		detailPanel.setVisible(false);

		// ------------------------------------------------

		int maxentries = 1000;
		
		tarentLogger.setMaxEntriesCount(maxentries);
		
		maxEntitySpinner = new JSpinner(new SpinnerNumberModel(maxentries, 1, Integer.MAX_VALUE, 1));
		maxEntitySpinner.getModel().addChangeListener(new MaxEntriesChangeListener());

		JPanel spinnerpanel = new JPanel(new BorderLayout());
		
		spinnerpanel.add(createLabel("max. Eintr�ge: ", SwingConstants.RIGHT), BorderLayout.WEST);
		spinnerpanel.add(maxEntitySpinner, BorderLayout.EAST);

		showDetailsCheckBox = new JCheckBox("zeige Details");
		
		showDetailsCheckBox.addActionListener(new toggle_showdetails_clicked());

		JButton showversionbutton = new JButton("Version anzeigen", icon_Version);
		
		showversionbutton.addActionListener(new button_version_clicked());

		JPanel actionpanel = new JPanel(new BorderLayout());
		
		actionpanel.add(detailPanel, BorderLayout.NORTH);
		actionpanel.add(spinnerpanel, BorderLayout.WEST);
		actionpanel.add(showDetailsCheckBox, BorderLayout.EAST);
		// actionpanel.add(showversionbutton, BorderLayout.CENTER);
		actionpanel.setBorder(new EmptyBorder(3, 3, 3, 3));

		// ------------------------------------------------

		JPanel logpanel = new JPanel(new BorderLayout());
		
		logpanel.add(tablescroll, BorderLayout.CENTER);
		logpanel.add(actionpanel, BorderLayout.SOUTH);

		// ------------------------------------------------

		JButton savebutton = new JButton("speichern", icon_SaveLog);
		
		savebutton.addActionListener(new button_save_clicked());

		JButton mailbutton = new JButton("versenden", icon_MailLog);
		
		mailbutton.addActionListener(new button_mail_clicked());

		JButton showpluginsbutton = new JButton("Plugins anzeigen", icon_Version);
		
		showpluginsbutton.addActionListener(new button_showplugins_clicked());

		JPanel buttonpanel = new JPanel(new GridLayout(1, 0));
		
		buttonpanel.add(showversionbutton);
		buttonpanel.add(showpluginsbutton);
		buttonpanel.add(savebutton);
		buttonpanel.add(mailbutton);

		// ------------------------------------------------

		mainpanel.add(titelpanel, BorderLayout.NORTH);
		mainpanel.add(logpanel, BorderLayout.CENTER);
		mainpanel.add(buttonpanel, BorderLayout.SOUTH);

		// ------------------------------------------------

		setTitle(Messages.getString("GUI_Events_Window_Title"));
		
		if (isToShowIcons) ((Frame) getOwner()).setIconImage(windowIcon.getImage());
		
		getContentPane().add(mainpanel);
		
		addWindowListener(new SimpleLogWindowListener());
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        pack();
        
        setSize(640, 480);
		
		setLocationRelativeTo(null);
	}

	private class MaxEntriesChangeListener implements ChangeListener {

		public void stateChanged(ChangeEvent e) {
			int val = ((Integer) (maxEntitySpinner.getValue())).intValue();
			tarentLogger.setMaxEntriesCount(val);
		}
	}

	private class toggle_showdetails_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			detailPanel.setVisible(showDetailsCheckBox.isSelected());
		}
	}

	private class button_save_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			TarentFileChooser chooser = new TarentFileChooser();
			if (chooser.showSaveDialog(instance) == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				if (file != null) {
					String filename = file.getAbsolutePath();
					boolean result = tarentLogger.writeAll(new XMLLogFormatter(), new XMLLogListFormatter(), new FileLogWriter(filename));
					if (result == false) {
						JOptionPane.showMessageDialog(instance, "Beim Versuch die Datei\n" + filename + "\nzu schreiben ist ein Fehler aufgetreten.", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
	}

	private class button_mail_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
		    SwingUtilities.invokeLater(new Runnable(){
                public void run() {
                    PersonalMessageDialog dialog = new PersonalMessageDialog((Frame) instance.getOwner(), tarentLogger.getCurrentUser());
                    if (dialog.getResult() != null) {
                        String ticketid = Long.toHexString(new Date().getTime());

                        VersionFetcher fetcher = new VersionFetcher(ticketid);
                        String filename = fetcher.getResultFilename();
                        Vector attachments = new Vector();
                        if (filename != null) {
                            attachments.add(new String[] { "version_" + ticketid + ".xml", filename });
                        }

                        PanelInserter panelinserter = guiListener.getPanelInserter();
                        if (panelinserter instanceof AdvancedPanelInserter) {
                            String outputPath = System.getProperty("java.io.tmpdir") + File.separator + "Plugins_" + ticketid;
                            String configtext = ((AdvancedPanelInserter) panelinserter).dumpConfig();
                            try {
                                FileWriter fw = new FileWriter(outputPath);
                                fw.write(configtext);
                                fw.close();
                                attachments.add(new String[] { "plugins_" + ticketid + ".xml", outputPath });
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }

                        String ownemail = dialog.getAnswerAddress();
                        if (ownemail == null)
                            ownemail = "";

                        boolean result = tarentLogger.writeAll(new XMLLogFormatter(), new XMLLogListFormatter(), 
                                                               new MailLogWriter(ticketid, ownemail, SUPPORT_EMAIL_ADDRESS, dialog.getResult(), attachments));
                        if (result == false) {
                            JOptionPane.showMessageDialog(instance, Messages.getFormattedString("GUI_SimpleLogWindow_Error_SendingBugReportFailed", SUPPORT_EMAIL_ADDRESS), Messages.getString("ErrorDialogTitle"), JOptionPane.ERROR_MESSAGE);
                        }
                        fetcher.removeResultFile();
                    }
                }
            });
		}
	}

	private class button_version_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// VersionFetcher fetcher = new VersionFetcher(false);
			VersionFetcher fetcher = new VersionFetcher(starterPath, null, false);
			
			String filename = fetcher.getResultFilename();
			
			if (filename != null) {
			
				new HTMLViewer(filename, "Versionsinformationen", true);
			}
		}
	}

	private class button_showplugins_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			
			PanelInserter panelinserter = guiListener.getPanelInserter();
			
			if (panelinserter instanceof AdvancedPanelInserter) {
			
				String configtext = ((AdvancedPanelInserter) panelinserter).dumpConfig();
				
				new TextViewer(configtext, "Plugins");
			}
		}
	}


	private JLabel createLabel(String text, int just) {
		
		JLabel label = new JLabel(text, just);
		
		return label;
	}

	private class LogListSelectionListener implements ListSelectionListener {

		public void valueChanged(ListSelectionEvent e) {
			
			if (e.getValueIsAdjusting())
				return;

			int row = logTable.getSelectedRow();

			if (row != -1) {
			
				LogEntry entry = tarentLogger.getLogEntry(row);
				
				List params = entry.getParameters();
				
				if (params != null) {
				
					paramTableModel.setLogEntry(entry);
					
				} else {
					
					paramTableModel.setLogEntry(null);
				}
					

				String throwtext = entry.getThrowableText();

				if (throwtext != null) {
				
					throwTextArea.setText(throwtext);
					
				} else {
					
					throwTextArea.setText("");
				}
			}
		}
	}

	private class SimpleLogWindowListener extends WindowAdapter {

		public void windowClosing(WindowEvent e) {
			fireVisibilityChanged(false);
			closeWindow();
		}


		public void windowOpened(WindowEvent e) {
			fireVisibilityChanged(true);
		}
	}

	public void showWindow(boolean value) {
        //if(value && !isDisplayable()) pack();
        setVisible(value);
	}


	public boolean isWindowVisible() {
		return this.isVisible();
	}


	private void showStats() {
		int config = tarentLogger.getNumberOfEntriesByLevel(Level.CONFIG);
		int warning = tarentLogger.getNumberOfEntriesByLevel(Level.WARNING);
		int severe = tarentLogger.getNumberOfEntriesByLevel(Level.SEVERE);
		int info = tarentLogger.getNumberOfEntriesByLevel(Level.INFO);
		int other = tarentLogger.getNumberOfEntriesByLevel(Level.FINE) + tarentLogger.getNumberOfEntriesByLevel(Level.FINER) + tarentLogger.getNumberOfEntriesByLevel(Level.FINEST);

		configLabel.setText(Integer.toString(config));
		warnLabel.setText(Integer.toString(warning));
		severeLabel.setText(Integer.toString(severe));
		infoLabel.setText(Integer.toString(info));
		otherLabel.setText(Integer.toString(other));
	}


	public void removedFirstLogEntries(int numentriesremoved) {
		logTableModel.fireTableDataChanged();
		showStats();
	}


	public void addedLogEntry(LogEntry entry) {
		logTableModel.fireTableDataChanged();
		showStats();
	}

	private class LogTableModel extends AbstractTableModel {

		private static final long	serialVersionUID	= -6680581445417770935L;

		private final String[]	COLUMN_NAMES_ARRAY	= { "Typ", "Datum", "Meldung" };


		public int getColumnCount() {
			return COLUMN_NAMES_ARRAY.length;
		}


		public int getRowCount() {
			return tarentLogger.getNumberOfLogEntries();
		}


		public String getColumnName(int col) {
			return COLUMN_NAMES_ARRAY[col];
		}


		public Object getValueAt(int row, int col) {
			LogEntry entry = tarentLogger.getLogEntry(row);

			if (col == 0)
				if (isToShowIcons)
					return (entry.getLevel());
				else
					return (entry.getLevelText());
			else if (col == 1)
				return DATE_FORMATTER.format(entry.getDate());
			else if (col == 2)
				return entry.getMessage();
			else
				return "";
		}


		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}


		public boolean isCellEditable(int row, int col) {
			return false;
		}
	}


	private ImageIcon getIconForLevel(Level level) {
		if (Level.CONFIG.equals(level))
			return icon_CONFIG;
		else if (Level.FINE.equals(level))
			return icon_FINE;
		else if (Level.FINER.equals(level))
			return icon_FINER;
		else if (Level.FINEST.equals(level))
			return icon_FINEST;
		else if (Level.INFO.equals(level))
			return icon_INFO;
		else if (Level.SEVERE.equals(level))
			return icon_SEVERE;
		else if (Level.WARNING.equals(level))
			return icon_WARNING;
		return icon_UNKNOWN;
	}

	private class LevelCellRenderer implements TableCellRenderer {

		private JLabel	m_oLabel;


		public LevelCellRenderer() {
			m_oLabel = new JLabel(icon_UNKNOWN, SwingConstants.CENTER);
			m_oLabel.setOpaque(true);
		}


		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (value instanceof Level)
				m_oLabel.setIcon(getIconForLevel((Level) value));
			if (isSelected) {
				m_oLabel.setBackground(table.getSelectionBackground());
				m_oLabel.setForeground(table.getSelectionForeground());
			} else {
				m_oLabel.setBackground(Color.WHITE);
				m_oLabel.setForeground(Color.BLACK);
			}
			m_oLabel.setToolTipText("Meldung vom Typ " + ((Level) value).getLocalizedName());
			return m_oLabel;
		}
	}

	private class ParamTableModel extends AbstractTableModel {

		private static final long	serialVersionUID	= -3832350180158176079L;
		private LogEntry logEntry;


		public void setLogEntry(LogEntry entry) {
			logEntry = entry;
		}


		public int getColumnCount() {
			return 1;
		}


		public int getRowCount() {
			if (logEntry == null)
				return 0;
			if (logEntry.getParameters() == null)
				return 0;
			return logEntry.getParameters().size();
		}


		public String getColumnName(int col) {
			return "Wert";
		}


		public Object getValueAt(int row, int col) {
			
			if (logEntry == null)
				return "";
			
			List params = logEntry.getParameters();
			
			if (params == null)
				return "";
			
			return params.get(row);
		}


		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}


		public boolean isCellEditable(int row, int col) {
			return false;
		}
	}


	private void fireVisibilityChanged(boolean isvisible) {
		
		LogWindowListener listener;
		Iterator it = listenersList.iterator();

		while (it.hasNext()) {
		
			listener = (LogWindowListener) (it.next());
			
			listener.changedVisibility(isvisible);
		}
	}


	public void addLogWindowListener(LogWindowListener listener) {
		listenersList.add(listener);
	}


	public void removeLogWindowListener(LogWindowListener listener) {
		listenersList.remove(listener);
	}
}
