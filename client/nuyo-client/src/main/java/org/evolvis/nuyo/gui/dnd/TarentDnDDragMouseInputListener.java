package org.evolvis.nuyo.gui.dnd;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.TransferHandler;
import javax.swing.event.MouseInputListener;

/*
 * Created on 25.07.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/**
 * @author niko
 *
 * Dieser MouseInputListener erm�glicht einen Drag bei Komponenten 
 * die diese Funktionalit�t standardm��ig nicht bereitstellen.
 * auf die Komponente mu� sowohl ein 
 * "addMouseMotionListener(DragMouseInputListener);"
 * wie auch ein 
 * "addMouseListener(DragMouseInputListener);"
 * mit derselben Instanz dieser Klasse ausgef�hrt werden.
 * 
 * Die Konstante "m_iMinDeltaToDrag" gibt an wie weit die Maus bei 
 * gedr�ckter Maustaste verschoben werden mu� bevor der Drag beginnt.
 */
public class TarentDnDDragMouseInputListener implements MouseInputListener 
{
  private final int m_iMinDeltaToDrag = 5; 
  private MouseEvent m_oFirstMouseEvent = null;

  public void applyToComponent(JComponent component)
  {  
    component.addMouseMotionListener(this);
    component.addMouseListener(this);      
  }
  
  public void mousePressed(MouseEvent e) 
  {
    m_oFirstMouseEvent = e;
    e.consume();
  }

  public void mouseDragged(MouseEvent e) 
  {
      if (m_oFirstMouseEvent != null) 
      {
          e.consume();

          int dx = Math.abs(e.getX() - m_oFirstMouseEvent.getX());
          int dy = Math.abs(e.getY() - m_oFirstMouseEvent.getY());

          if (dx > m_iMinDeltaToDrag || dy > m_iMinDeltaToDrag) 
          {
            JComponent comp = (JComponent)e.getSource();
            TransferHandler transferhandler = comp.getTransferHandler();
            transferhandler.exportAsDrag(comp, e, TransferHandler.COPY);
            m_oFirstMouseEvent = null;
          }
      }
  }

  public void mouseReleased(MouseEvent e) 
  {
    m_oFirstMouseEvent = null;
  }

  public void mouseMoved(MouseEvent e) {}
  public void mouseClicked(MouseEvent e) {}
  public void mouseEntered(MouseEvent e) {}
  public void mouseExited(MouseEvent e) {}
}
