package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.cache.Cachable;
import org.evolvis.nuyo.db.cache.EntityCache;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.UserGroupId;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractPersistentRelation;
import org.evolvis.nuyo.db.persistence.EntityRelationEvent;
import org.evolvis.nuyo.db.persistence.EntityRelationListener;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;


/**
 * @author kleinw
 * 
 * Implementierung de.tarent.contact.db.UserGroup
 * 
 * TODO shortdescription weiter bis zum Octopus und zur�ck
 *  
 */
public class UserGroupImpl extends UserGroupBean implements EntityRelationListener, Cachable {

    //
    //	Soapanbindung
    //
    /**	Benutergruppe anlegen oder �ndern */
	private static final String METHOD_CREATE_OR_MODIFY_USERGROUP = "createOrModifyUserGroup";
	/** Alle Benutzergruppen beziehen */
	private static final String METHOD_GET_USER_GROUPS = "getUserGroups";
	/** Alle Benutzer dieser Gruppe */
    private static final String METHOD_GET_USERS_FROM_GROUP = "getUsersFromGroup";
	/** Benutzergruppe l�schen */
	private static final String METHOD_DELETE_USERGROUP = "deleteUserGroup";
    
	/** Datenbank-ID der Benutzergruppe */
    private static final String PARAM_ID = "id";
	/** Datenbank-ID der Benutzergruppe */
	private static final String PARAM_GROUPID = "usergroupid";
	/** Benutzer, die gel�scht werden sollen */
	private static final String PARAM_REMOVE_USER = "removeuser";
	/** hinzuzuf�gende Benutzer */
	private static final String PARAM_ADD_USER = "adduser";

	
    /** Zugriff auf dieses Objekt in einer anonymen Klassen */
    protected UserGroupImpl _this = this;
	
	
	/**	Initialisierung des EntityFetchers */
	static {
	    _fetcher = new AbstractEntityFetcher("getUserGroups", false) {
            public IEntity populate(ResponseData values) throws ContactDBException {
                UserGroup userGroup = new UserGroupImpl();
                ((IEntity)userGroup).populate(values);
                return (IEntity)userGroup;
            }
	    };
	}
	
	
	/**	Konstruktor f�r ein neues Objekt */
	protected UserGroupImpl() {
		_users = new AbstractPersistentRelation(this) {
		    public Collection getRelationContents() throws ContactDBException {
		        ISelection f = Selection.getInstance()
		        	.add(new UserGroupId(_id));
		        return UserImpl.getUsers(f);
            }
            public String[] getFields() {return new String[]{PARAM_ADD_USER, PARAM_REMOVE_USER};}
            public void relateTo(IEntity entity) throws ContactDBException {((UserImpl)entity).add(_this);}
			public void killRelation(IEntity entity) throws ContactDBException {((UserImpl)entity).remove(_this);}
		};
	}
    

	/**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}
	
	
    /**	Methode speichert oder �ndert Objekt. */
    public void commit() 
		throws ContactDBException {
	    prepareCommit(METHOD_CREATE_OR_MODIFY_USERGROUP);
		_users.includeInMethod(_method);
		_id = (Integer)_method.invoke();
	    _users.commmit();
	}
	
    /**	Methode l�scht Objekt aus der Datenbank. Danach ist es unbrauchbar */
	public void delete() throws ContactDBException {
		setDeleted(true);
		Iterator it = _users.getActualRelations().iterator();
		while(it.hasNext()){
			IEntity user = (IEntity) it.next();
			_users.remove(user);
		}
		if (_id != null) {
			Method method = new Method(METHOD_DELETE_USERGROUP);
			method.add(PARAM_GROUPID, _id);
			method.invoke();
		}
	}

	/**	l�dt das Objekt neu. */
	public void rollback() throws ContactDBException {
	    populate(_responseData);
	    _users.rollback();
	}

    /**	Methode zum F�llen eines Objekts durch DB-Inhalte. @param = values - Liste mit den Inhalten */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        setAttribute(UserGroup.KEY_NAME, data.getString(1));
        setAttribute(UserGroup.KEY_DESCRIPTION, data.getString(2));
        _created = data.getDate(3);
        _changed = data.getDate(4);
        setAttribute(UserGroup.KEY_GLOBALROLE, data.getString(5));      
        setAttribute(UserGroup.KEY_IS_PRIVATE, data.getBoolean(6));        
        setAttribute(UserGroup.KEY_GROUP_TYPE, data.getInteger(7));
    }
     
    
    public void relationAdded(EntityRelationEvent event) {
    }
    public void relationRemoved(EntityRelationEvent event) {
    }
    
    
	/**	Statische Methode zum Beziehen aller UserGroups */
	final static public Collection getUserGroups(ISelection selection) throws ContactDBException {return _fetcher.getEntities(selection);};
    
	


	public void updateInCache() throws ContactDBException {
        EntityCache cache = _fetcher.getCache();
        cache.store(null, Collections.singletonList(this));
		ISelection f = Selection.getInstance()
    	.add(new UserGroupId(_id));
		UserImpl._fetcher.getCache().getSelections().remove(f);
	}


	public void deleteFromCache() throws ContactDBException {
		_fetcher.getCache().delete(new Integer(getId()));
	}

}
