/*
 * Created on 12.06.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.db.Addresses;

import de.tarent.commons.ui.EscapeDialog;

/**
 * @author niko
 *
 */
public class AddressRequester extends EscapeDialog
{
  private int m_iAnswer = -1;
  private JDialog m_oThisDialog;
  
  public AddressRequester(JFrame parent, String caption, Icon icon, String question, Addresses addresses, String[] answers, int defaultanswer)
  {
    super(parent);
    setTitle(caption);
    m_oThisDialog = this;
    
    JTextArea oTextAreaQuestion = new JTextArea(question);
    oTextAreaQuestion.setEditable(false);
    oTextAreaQuestion.setBorder(null);
    oTextAreaQuestion.setLineWrap(true);
    oTextAreaQuestion.setWrapStyleWord(true);
    JScrollPane questionscroll = new JScrollPane(oTextAreaQuestion);
    questionscroll.setBorder(null);

    JPanel questionpanel = new JPanel(new BorderLayout());
    questionpanel.add(questionscroll, BorderLayout.CENTER);
    
    if (icon != null)
    {  
      JLabel iconlabel = new JLabel(icon);
      questionpanel.add(iconlabel, BorderLayout.WEST);
    }
    
    JList oList = new JList();
    oList.setBorder(null);        
    oList.setModel(new AddressesListModel(addresses));
    JScrollPane scroll = new JScrollPane(oList);
    scroll.setBorder(new EmptyBorder(10,0,0,0));
    
    JPanel buttonpanel = new JPanel(new GridLayout(1,0));
    buttonpanel.setBorder(new EmptyBorder(5,0,0,0));
    for(int i=0; i<(answers.length); i++)
    {
      JButton button_answer = new JButton(answers[i]);
      button_answer.addActionListener(new AnswerButtonClicked(i));
      buttonpanel.add(button_answer);
    }
    
    JPanel mainpanel = new JPanel(new BorderLayout());
    mainpanel.setBorder(new EmptyBorder(5,5,5,5));
    mainpanel.add(questionpanel, BorderLayout.NORTH);
    mainpanel.add(scroll, BorderLayout.CENTER);
    mainpanel.add(buttonpanel, BorderLayout.SOUTH);

    
    getContentPane().add(mainpanel);
    
    setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    this.addWindowListener(new WindowAdapter() 
    {
      public void windowClosing(WindowEvent e) 
      {
        m_oThisDialog.setVisible(false);
        m_oThisDialog.setModal(false);
      }
    });
    

    pack();
    setLocationRelativeTo(null);
    setVisible(true);
    setModal(true);
  }

  private class AddressesListModel extends AbstractListModel
  {
    private Addresses m_oAddresses;
    
    public AddressesListModel(Addresses addresses)
    {
      m_oAddresses = addresses;  
    }
    
    public int getSize() 
    { 
      return m_oAddresses.getIndexMax() + 1; 
    }
    
    public Object getElementAt(int index) 
    { 
        return m_oAddresses.get(index).getShortAddressLabel();
    }
  };  
  
  
  public int getAnswer()
  {
    return(m_iAnswer);
  }
  
  private class AnswerButtonClicked implements ActionListener
  {
    private int m_iNum;
    
    public AnswerButtonClicked(int num)
    {
      m_iNum = num;  
    }
    
    public void actionPerformed(ActionEvent e)
    {
      m_iAnswer = m_iNum;
      m_oThisDialog.setVisible(false);
      m_oThisDialog.setModal(false);
    }    
  }

}
