package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class RedoAction extends AbstractGUIAction {

    private static final long serialVersionUID = 5904507882742838326L;
    private static final TarentLogger log = new TarentLogger(RedoAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("RedoAction not implemented");
    }

}