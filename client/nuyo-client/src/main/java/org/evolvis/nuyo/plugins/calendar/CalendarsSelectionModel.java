package org.evolvis.nuyo.plugins.calendar;

import javax.swing.JPanel;

import org.evolvis.nuyo.db.CalendarCollection;


/** 
 * A calendars selection model makes it possible to have different selection views and controllers.
 *  
 */
public interface CalendarsSelectionModel {

    /** Returns a panel with initialized calendar selection items. */
    public JPanel getMenuItemsPanel();
    /**
     * Select the calendar with the specified id and mark it as selected in the preferences.
     */
    public void selectCalendar(Integer calendarid);
    public void fillCalendarMenu( CollapsingMenu calendarsMenu );
    public CalendarCollection getCalendarCollection();
    /**
     * @return ID Gibt die id des aktuell ausgew�hlten Kalenders zur�ck.
     */
    public Integer getCurrentCalendarID();
}
