/**
 * 
 */
package org.evolvis.nuyo.plugin;

import javax.swing.JFrame;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface FrameAddressListPerformer extends AddressListPerformer
{
	public JFrame getFrame();
}
