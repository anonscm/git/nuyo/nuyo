package org.evolvis.nuyo.gui.actions;
import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.action.AddressListProviderAdapter;
import org.evolvis.nuyo.logging.TarentLogger;


/**
 * Gets current selection list and exports all contacts from this list.
 * 
 * TODO: get selection from category tree (at the moment only current category used).
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ExportContactsTableAction extends AbstractContactDependentAction {

    private static final long serialVersionUID = 4180249593352068377L;
    private static final TarentLogger log = new TarentLogger(ExportContactsTableAction.class);
    private GUIListener guiListener;
    private Runnable exportExecutor;

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null){
            guiListener.userRequestExportAddresses(new AddressListProviderAdapter(guiListener.getAddresses()));
        } else log.warning("ExportSelectedCategoryContacts action is not initialized");
    }
    
    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        //guiListener.userRequestExportIntoVcard(GUIListener.RANGE_CURRENT_LIST);
    }
}
