package org.evolvis.nuyo.plugins.assigner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;
import org.evolvis.xana.action.AbstractGUIAction;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.EscapeDialog;


/**
 * This class is provides a dialog-representation of the Assigner-Plugin. 
 * The Dialog can be customized by calling the getDialog() method. 
 * It can for example be set modal to a parent frame, known by the calling class.
 * @author Steffi Tinder, tarent GmbH
 *
 */
class DialogAssigner extends AbstractGUIAction implements ActionListener, DialogAddressListPerformer {
	
    private static final long serialVersionUID = 2162940809010026046L;

	private JDialog assignerDialog;
	private AssignerComponent assigner;
	private JButton assignButton;
	private JButton removeAssignmentsButton;
	private JButton cancelButton;

	
	public DialogAssigner() {
		
		assigner = new AssignerComponent();
		
        assignButton = new AccessibleJButton("&zuweisen");
		assignButton.addActionListener(this);
		removeAssignmentsButton= new AccessibleJButton("Zuweisung &entfernen");
		removeAssignmentsButton.addActionListener(this);
		cancelButton = new AccessibleJButton("a&bbrechen");
		cancelButton.addActionListener(this);
		assignerDialog = buildDialog();
        assignerDialog.getRootPane().setDefaultButton(assignButton);
	}
	
	private JDialog buildDialog(){
		
		JDialog dialog = new EscapeDialog();
		dialog.setTitle("Kategoriezuweisung");
		
		FormLayout layout = new FormLayout(
				"pref:GROW", //columns 
		"pref, 3dlu, pref");      // rows
		
		//create and configure a builder
		DefaultFormBuilder builder = new DefaultFormBuilder(layout);
		builder.setDefaultDialogBorder();
		
		CellConstraints cc = new CellConstraints();
		builder.add(assigner.getComponent(), cc.xy(1,1));
		builder.add(ButtonBarFactory.buildRightAlignedBar(cancelButton,removeAssignmentsButton, assignButton),cc.xy(1,3));
		dialog.getContentPane().add(builder.getPanel());
		dialog.pack();
		
		return dialog;
	}
	
	public void setAddresses(Addresses addr){
		assigner.setAddresses(addr);
	}
	
	public Addresses getAddresses(){
		return assigner.getAddresses();
	}
	
	public JDialog getDialog(){
		return assignerDialog;
	}
	
	public void cancelClicked(){
		assignerDialog.setVisible(false);
	}
	
	public void assignButtonClicked(){
		assigner.assign();
		assignerDialog.setVisible(false);
	}
	
	public void removeAssignmentsClicked(){
		assigner.removeAssignments();
		assignerDialog.setVisible(false);
	}
		
	public void execute(){
		getDialog().setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==assignButton){
			assignButtonClicked();
		} else if (e.getSource()==removeAssignmentsButton){
			removeAssignmentsClicked();
		} else if (e.getSource()==cancelButton){
			cancelClicked();
		} 
	}
}
