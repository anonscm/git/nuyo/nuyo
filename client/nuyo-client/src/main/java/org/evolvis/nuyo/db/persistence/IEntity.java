package org.evolvis.nuyo.db.persistence;

import java.util.Collection;
import java.util.Date;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;


/**
 * @author kleinw
 *
 *	Eine spezielle Auspr�gung der Peristable-Schnittstelle.
 *
 *	Dieses Interface zielt speziell auf die Speicherung in 
 *	einer RDBMS ab.
 *
 */
public interface IEntity extends IPersistable {

    /**	Eindeutiger PrimaryKey aus der Datenbank */
    public int getId();
    
    
    /** Gibt an, ob das Objekt nur lokal vorhanden ist */
    public boolean isTransient();
    /**	Setzt die Eigenschaft */
    public void setTransient(boolean isTransient);
    /**	Gibt an, ob das Objekt und seine persistente Version lokal ver�ndert wurde */
    public boolean isDirty();
    /**	Setzt die Eigenschaft */
    public void setDirty(boolean isDirty);
    /**	Das Objekt ist aus der Datenquelle entfernt worden */
    public boolean isDeleted();
    /**	Setzt die Eigenschaft */
    public void setDeleted(boolean isDeleted);
    /**	Bei gewissen F�llen kann das Objekt in einen ung�ltigen Zustand verfallen */
    public boolean isInvalid();
    /**	Setzt die Eigenschaft */
    public void setInvalid(boolean isInvalid);
    
    
    /**	Zeitpunkt der Erstellung */
    public Date getCreated();
    /**	Setzen der Eigenschaft.@param - Zeitpunkt, zu dem das Objekt angelegt wurde */
    public void setCreated(Date created);
    /** Zeitpunkt der letzten �nderung */
    public Date getChanged();
    /**	Zeitpunkt der letzten �nderung.@param changed - Zeitpunkt der letzten �nderung */
    public void setChanged(Date changed);
    

    /**	Alle 1-n Beziehungen des Objekts beziehen */
    public Collection getRelations();
    
    
    /**	Hier�ber kann das Objekt neu erstellt oder wiedergef�llt werden.@param data - Die von der Datenquelle bezogenen Daten */
    public void populate(ResponseData data) throws ContactDBException;
    
    
}
