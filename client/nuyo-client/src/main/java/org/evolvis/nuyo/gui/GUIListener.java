/* $Id: GUIListener.java,v 1.35 2007/08/30 16:10:24 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.gui;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.prefs.Preferences;

import javax.swing.JFrame;

import org.evolvis.nuyo.controller.ContactException;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.controller.event.ContextVetoListener;
import org.evolvis.nuyo.controller.event.DataChangeListener;
import org.evolvis.nuyo.controller.event.MasterDataListener;
import org.evolvis.nuyo.datacontainer.parser.PluginLocator;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.CalendarCollection;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.db.Schedule;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.db.veto.VetoableAction;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.gui.dialogs.DoubletSearchDialog;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.util.general.ContactLookUpNotFoundException;
import org.evolvis.nuyo.util.general.DataAccess;


/**
 * Diese Schnittstelle abstrahiert den von der GUI ben�tigten Control-Layer
 * dieser Applikation.
 * 
 * @author mikel
 */
public interface GUIListener {

    /*
     * Die folgenden Methoden dienen der Fehler- und Warnungsausgabe
     */

    /**
     * Zeigt dem Benutzer die Fehlernachricht und loggt sie ggfs.
     * 
     * @param msg Die Mitteilung, die dem Benutzer ausgegeben wird.
     * @param e Die assoziierte Ausnahme zu der Fehlermeldung.
     */

    /**
     * Zeigt dem Benutzer die Fehlernachricht und loggt sie ggfs.
     * 
     * @param msg Die Mitteilung, die dem Benutzer ausgegeben wird.
     */


    /**
     * Liefert den ControlListener
     */
    public ControlListener getMainControlListener();
    
    /**
     * Diese Methode liefert den Applikations-Logger.
     */
    public TarentLogger getLogger();

    /*
     * Diese Methoden dienen der Verwaltung der Kategorien
     */

    /**
     * Gibt den Namen der ausgew�hlten Kategorie zur�ck.
     * 
     * @deprecated Es gibt keine "ausgewaehlte Kategorie" mehr
     * 
     * @return Name der ausgew�hlten Kategorie.
     */
    public String getNameOfSelectedCategory();

    /*
     * Die folgenden Methoden dienen der Anzeige und Auswahl der Unterkategorien,
     * die aktuell zur Anzeige zur Verfgung stehen.
     */

    /**
     * Liefert die Unterkategorien, die zur Anzeigeauswahl zur Verfgung stehen,
     * als Map. Die Keys sind die Unterkategorieschlssel und die Values sind die
     * lesbaren Namen.
     *
     * @param kategorie wenn <code>null</code>, so ist die aktuelle Kategorie gemeint.
     * @return Map mit String Keys, und String Values; bei einem DB Fehler
     *  wird null zur~ckgegeben.
     */
    public SortedMap getVerteiler(String kategorie);


    /**
     * Diese Methode liefert den aktuellen Verteilerauswahlmodus.
     * 
     * @return der aktuelle Modus, {@link #VERTEILER_MODE_ANY VERTEILER_MODE_ANY}
     *  oder {@link #VERTEILER_MODE_ALL VERTEILER_MODE_ALL}.
     */
    public int getVerteilerSelectMode();


    /**
     * Diese Konstante stellt den Modus "Vereinigung der ausgew�hlten
     * Verteiler" dar.
     */
    public final static int VERTEILER_MODE_ANY = 0;

    /**
     * Diese Konstante stellt den Modus "Schnitt der ausgew�hlten
     * Verteiler" dar.
     */
    public final static int VERTEILER_MODE_ALL = 1;

    /*
     * Diese Methoden dienen dem Bearbeiten von Adressdaten.
     */

    /**
     * Hilfsmethode f�r die Doublettensuche. 
     * Schreibt die ausgew�hlte Doublette ins Mainframe.
     * @param a Die Doublette, die ausgew�hlt wurde.
     */
    public void setDoubleAdd(Address a);

    /*
     * Diese Methoden dienen der Verwaltung von Unterkategorien zu Adressen.
     */
    
    /*
     * Diese Methoden dienen der Verwaltung von Versandauftr�gen.
     */

    /**
     * Liefert die Versandauftr�ge des aktuellen Benutzers.
     * @return Liste der Versandauftr�ge.
     */
    public List getVersandAuftraege();

    /**
     * Diese Methode erzeugt ein tempor�res Address-Sammelobjekt.
     * 
     * @return ein nicht persistentes leeres Addresses-Objekt.
     */
    public NonPersistentAddresses createNonPersistentAddresses();

    /**
     * Diese Methode erzeugt einen neuen Versandauftrag f�r den aktuellen
     * Benutzer zur aktuellen Adressauswahl.
     * 
     * @return den neuen Versandauftrag oder <code>null</code>, falls
     *  kein neuer erzeugt werden konnte.
     */
    public MailBatch createNewMailOrder();

    /**
     * Veranlasst das Versenden eines oder aller Kan�le eines
     * Versandauftrages.
     * 
     * @param batch der Versandauftrag
     * @param channel der zu verschickende Kanal
     */
    public void userRequestSendMailBatch(MailBatch batch, MailBatch.CHANNEL channel);

    /**
     * Veranlasst das Anzeigen eines oder aller Kan�le eines
     * Versandauftrages.
     * 
     * @param batch der Versandauftrag
     * @param channel der zu verschickende Kanal
     */
    public void userRequestShowMailBatch(MailBatch batch, MailBatch.CHANNEL channel);

    /*
     */

    /**
     * @param anredeKey
     * @return index der Anrede des bergebenen Anredekeys. 
     */
    public int getIndexOfAnrede(String anredeKey);

    /**
     * Liefert den Index eines Bundeslandes in der Liste der Bundesl�nder.
     * Es besteht die implizite Annahme, dass der erste Eintrag (Index 0)
     * Default ist.
     * 
     * @param bundesland das Bundesland, dessen Index gesucht wird.
     * @return gibt den Index des bergebenen Bundeslands zur�ck; wenn das
     *  Bundesland nicht gefunden wird, wird 0 zur�ckgegeben.
     * @see #getBundeslaenderList()
     */
    public int getIndexOfBundesland(String bundesland);

    /**
     * Liefert den Index eines Landes in der Liste der L�nder.
     * Es besteht die implizite Annahme, dass der erste Eintrag (Index 0)
     * Default ist.
     * 
     * @param land das Land, dessen Index gesucht wird 
     * @return gibt den Index des bergebenen Landes zur�ck.
     * @see #getLaenderList()
     */
    public int getIndexOfLand(String land);

    /**
     * @return Username Gibt den Namen des Benutzers zur�ck.
     */
    public String getCurrentUser();

     /**
     * @return CalendarCollection Gibt die aktuell ausgew�hlten Kalender zur�ck.
     */
    public CalendarCollection getCalendarCollection();
    
    /**
     * @return ID Gibt die id des aktuell ausgew�hlten Kalenders zur�ck.
     * @throws ContactException if selection model not initialized
     */
    public int getCurrentCalendarID();


    /**
     * @return Gibt den Index der ausgew�hlten Kategorie zur�ck. 
     */
    public int getSelectedCategory();

    /**
     * Liefert eine angegebene Adresse
     * 
     * @param addrNr ID der Adresse.
     * @return ein Address-Objekt.
     */
    public Address getAddress(int addrNr);

    /**
     * Diese Methode liefert eine Addresses-Instanz auf Basis der Einschr�nkungen,
     * die ber den aktuellen Benutzer und als Parameter gegeben sind.<br>
     * Derzeit werden nicht die generischen Filter, sondern nur die Suchfilter beachtet.
     * 
     * @param filters eine Sammlung von Filtern (z.B. Kategorien, Unterkategorien, Versandauftr�ge,...)
     * @param criteria eine Map von Suchfeldern und -Strings; die Schlssel sind die Feldkonstanten aus
     *  {@link Address}, also STD_FIELD_* und CAT_FIELD_*, und zus�tzlich "" f�r die Suche in allen Feldern;
     *  die Such-Strings sind die zu suchenden Feldinhalte, wobei '*' als Jokerzeichen erlaubt ist.
     * @return Sammlung der Adressen, die den Filtern und Suchkriterien gengen.
     */
    public Addresses getAddresses(Collection filters, Map criteria);
    
    /**
     * Liefert die aktuelle Adresse
     * @return Address
     */
    public Address getAddress();

    /**
     * Liefert die aktuellen Adressen
     * @return Addresses
     */
    public Addresses getAddresses();

    /**
     * Der Benutzer m�chte eine neue Adresse anlegen
     * @return TODO
     */
    public boolean userRequestNewContact();

    /**
     * Der Benutzer m�chte die aktuelle Adresse l�schen oder die aktuellen
     * �nderungen verwerfen.
     */
    public void userRequestDelete(boolean cancelInEditMode);

    /**
     * Der Benutzer m�chte den aktuellen Adresssatz einer Unterkategorie zuordnen
     */
    public Veto userRequestSetVerteiler();

    /**
     * Der Benutzer hat den Knopf "Serienversand alle" gedr�ckt
     */
    public void userRequestSerialPostDispatch();

    /**
     * Der Benutzer m�chte die aktuelle Eingabe postalisch korrigieren lassen.
     * <br>
     * Diese Methode arbeitet auf den aktuell angezeigten Feldinhalten der GUI  
     * und sollte deshalb nur im Editiermodus aufgerufen werden.
     */
    public void userRequestCorrection();
    
    /**
     * The user wants to edit the current address.
     * 
     * @throws ContactDBException if DB exception occurs
     * @exception IllegalStateException if none address was selected/found
     */
    public Veto userRequestEditable() throws ContactDBException;

    /**
     * Der Benutzer m�chte den Editiervorgang beenden
     * Je nach Parameter muss noch abgefragt werden,
     * ob etwas ge�ndert wurde und gespeichert werden soll.
     * 
     * @param action {@link #DO_DISCARD DO_DISCARD}, {@link #DO_ASK DO_ASK}
     *  oder {@link #DO_SAVE DO_SAVE}.
     * @return <code>true</code>, gdw das der Editiervorgang beendet werden konnte.
     */
    public boolean userRequestNotEditable(int action);

    public final static int DO_DISCARD = -1;
    public final static int DO_ASK = 0;
    public final static int DO_SAVE = 1;
    
    
    /**
     * Before saving an address the, probably updated, data from the gui
     * has to be taken and checked for integrity.
     * 
     * <p>Gui components check their individual values and finally it is
     * checked whether the address object as a whole makes sense.</p>
     * 
     * <p>The method returns <code>true</code> if the data is valid and was taken
     * or <code>false</code> if not. An appropriate error message is displayed in 
     * case the take operation fails.</p>
     * 
     */
    public boolean takeAddressDataFromGui();

    /**
     * Der Benutzer m�chte in die aktuelle Adresse Daten aus einer VCard
     * importieren.
     * 
     * @return <code>true</code>, gdw erfolgreich importiert wurde.
     */
    public boolean userRequestImportVcardIntoCurrentAddress();

    public final static int RANGE_CURRENT_ADDRESS = 0;
    public final static int RANGE_CURRENT_LIST = 1;

    /**
     * Der Benutzer m�chte Adressdaten in eine VCard exportieren.
     *
     * @param range gibt an, welche Adressen exportiert werden k�nnen,
     *  vergleiche GUIListener.RANGE*. 
     * @return <code>true</code>, gdw erfolgreich exportiert wurde.
     */
    public boolean userRequestExportIntoVcard(int range);

    

    /**
     * Der Benutzer m�chte Adressdaten exportieren.
     * Existierende Exportformate: Soffice
     *
     * @param addresses Liste der Adressen
     * @return <code>true</code>, gdw erfolgreich exportiert wurde.
     */
    
    public boolean userRequestExportAddresses(AddressListProvider addressesProvider);
    
    /**
     * Der Benutzer m�chte Adressdaten exportieren.
     *
     * @param addresses Liste der Adressen
     * @param numberOfAddressesToExport Anzahl der Adressen, die exportiert werden sollen. Kann abweichen von 
     * der Anzahl der uebergebenen Adressen, da evtl nicht auf allen Adressen Leserecht besteht. 
     * @return <code>true</code>, gdw erfolgreich exportiert wurde.
     */
    public boolean userRequestExportAddresses(AddressListProvider addressesProvider, int numberOfAddressesToExport);
    
    /**
     * Schreibt die Liste der Anredetexte in einen Vektor.
     * @return list Gibt diese Liste zur�ck.
     */
    public List getAnredetexteList();

    /**
     * Anzeige des n�chsten Datensatzes
     */
    public void userRequestNextAddress();

    /**
     * Anzeige des vorherigen Datensatzes
     */
    public void userRequestPrevAddress();

    /**
     * Anzeige des ersten Datensatzes
     */
    public void userRequestFirstAddress();

    /**
     * Anzeige des letzten Datensatzes
     */
    public void userRequestLastAddress();

  
    /**
     * Anzeige eines bestimmten Datensatzes
     */
    public void userRequestSelectAddress(int index);

    /**
     * Anzeige eines bestimmten Datensatzes; wenn dieser gar nicht in der
     * aktuellen Auswahl ist, wird gegebenenfalls die aktuelle Auswahl durch
     * die Auswahl der Einzeladresse ersetzt.
     * 
     * @param address die anzuzeigende Adresse
     * @param notInListAction {@link #DO_FAIL}, {@link #DO_ASK} oder {@link #DO_ENFORCE}.
     * @param description Beschreibung des gegebenenfalls erstellten Filters.
     * @param type Typ des gegebenenfalls erstellten Filters, vergleiche Konstanten ADDRESSSET_*.
     * @return <code>true</code>, falls die Adresse angezeigt wird.
     */
    public boolean userRequestSelectAddress(Address address, int notInListAction, String description, int type);

    public final static int DO_FAIL = -1;
    public final static int DO_ENFORCE = 1;

    /**
     * Tabelle an und aus schalten
     */
    public void userRequestShowTable(boolean showIt);

    /**
     * Diese Methode fordert zu einem Versandvorgang auf.
     * 
     * @param action Die gewnschte Aktion, vergleiche
     * @see org.evolvis.nuyo.controller.ApplicationStarter
     */
    public void userRequestPost(String action);
    
    /**
     * Diese Methode fordert zu einem Versandvorgang auf.
     * 
     * Hier wird im speziellen noch die gew�nschte Zieladresse 
     * �bergeben, da diese nicht immer der im Adressbereich 
     * ausgew�hlten entsprechen muss. 
     * 
     * @param action Die gewnschte Aktion, vergleiche
     * @see org.evolvis.nuyo.controller.ApplicationStarter
     */
    public void userRequestPost(String action, Address address);

    /**
     * Veranlasst den Start eines Browsers mit der angegebenen URL
     */
    public void userRequestStartBrowser();
    public void userRequestStartBrowser(String url); 

    /**
     * Veranlasst die Wahl der Telefonnummer 
     */
    public void userRequestCallPhone(String phonenumber);
    
    /**
     * Stellt fest, ob der aktuelle Benutzer <code>right</code> auf der aktuellen Adresse hat.
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean userRequestUserHasRightOnAddress(int right);
    
    /**
     * Stellt fest, ob der aktuelle Benutzer <code>right</code> auf der Adresse  <code>adress</code>hat.
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @param adress Adresse auf der das Recht bestehen muss
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean userRequestUserHasRightOnAddress(int right, Address address);


    /**
     * Stellt fest, ob der aktuelle Benutzer <code>right</code> auf einer Kategorie hat.
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean userRequestUserHasRightOnFolder(int folderId, int right);
    
    /**
     * Stellt fest, ob der aktuelle Benutzer Schreibrecht auf einem Event/termin hat.
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean mayUserWriteEvent(Integer eventid);
    
    /**
     * Veranlasst das Suchen nach Doubletten, bietet gegebenenfalls eine Liste
     * zur �bernahme an und kann sonst das Nichtvorhandensein von Dubletten
     * anzeigen.
     * 
     * <p>The method returns whether a saving operation should be continued or
     * not. In case that no duplicates are found it will always be <code>true</code>.
     * Only in case the duplicates exists the user has the option on how to continue.
     * The result of that chosing is returned.</p>
     * 
     * @param informNoDoubles wenn <code>true</code>, so wird eine Meldung
     *  ausgegeben, wenn keine Dubletten gefunden wurden.
     *  @param enableSaveButton dieses Flag steuert, ob der Speichern-Button 
     *  im Dialog angezeigt wird
     */
    public DoubletSearchDialog guiRequestCheckForDouble(boolean informNoDoubles, boolean enableSaveButton);

    /**
     * Diese Methode liefert die Kollektion der Kategorien, die der
     * angegebene Benutzer sieht, als Abbildung von Kategorieschlsseln
     * auf Kategorieobjekte.
     *
     * Von Sebastian:
     * Ich habe das Interface auf Map ge�ndert! Die Sortierung der Verteiler in dieser Map ist egal!!
     * f�r eine Sortierte Liste gibt es die Funktion getCategoriesList()
     *
     * 
     * @param user der Benutzer, dessen Gruppen abgefragt werden; wenn der
     *  Eintrag <code>null</code> ist, wird der aktuell eingelogte benutzt.
     * @return eine SortedMap der Kategorien des Benutzers.
     */
    public Map getCategories(String user);


    /**
     * Liefert das Objekt zu dem Key einer Kategorie
     *
     */
    public Category getCategory(String key);


    /**
     * Diese Methode liefert die Standardkategorie des angegebenen Benutzers.
     * 
     * @param user der Benutzer, dessen Standardkategorie gefragt ist;
     *  falls <code>null</code>, wird der aktuell eingelogte genommen.
     * @return der Schluessel der Standardkategorie des Benutzers.
     */
    public String getStandardCategory(String user);

    /**
     * Diese Methode liefert die Kollektion aller Kategorien als Abbildung
     * von Kategorieschlsseln auf Kategorienamen.
     * 
     * @return eine SortedMap aller Kategorien.
     */
    public Map getAllCategories();
    
    /**
     * Retrieves the category list from the server.
     * 
     * @throws ContactDBException
     */
    public void reloadCategories() throws ContactDBException;
    
    /**
     * Retrieves the addresses from the server.
     */
    public void reloadAddresses();

    /**
     * Diese Methode liefert die Kollektion aller Kategorien als Liste
     * @return eine Liste aller Kategorien (db.Category), in der Sortierung, mit der sie angezeigt werden sollen.
     */
    public List getCategoriesObjectList();

   
    /**
     * Liefert alle Kategorien auf die der User ein bestimmtes Recht hat
     * @param right
     * @param includeVirtual
     * @return
     */
    
    
    
    
    public List getUserCategoriesWithCategoryRight(int right, boolean includeVirtual);
    
    
    /**
     * Holt alle Kategorien, auf die der User das Recht AUTH_GRANT hat
     * @see #getCategoriesObjectList()
     * @return Liste mit Kategorien
     */
    public List getGrantableCategoriesObjectList();

    /**
     * Schreibt die Liste der Kategorien in einen Vektor.
     * @return Gibt diese Liste zur�ck.
     */
    public List getCategoriesList();


    /**
     * Diese Methode liefert zu einer PLZ das zugeh�rige Bundesland.
     * 
     * @param plz die Postleitzahl, die zu testen ist.
     * @return der Name des Bundeslandes, zu dem die PLZ geh�rt,
     *  oder <code>null</code>, falls es nicht feststellbar ist.
     * @see org.evolvis.nuyo.db.Database#getBundeslandForPLZ(int)
     */
    public String getBundeslandForPLZ(String plz);

    /**
     * Diese Methode liefert zu einer PLZ die zugeh�rige Stadt.
     *  
     * @param plz die Postleitzahl, die zu testen ist.
     * @return der Name der Stadt, zu dem die PLZ geh�rt,
     *  oder <code>null</code>, falls es nicht feststellbar ist.
     */
    public String getCityForPLZ(String plz);

    /**
     * Schreibt die Liste der L�nder in einen Vektor.
     * @return list Gibt diese Liste zur�ck.
     */
    public List getLaenderList();

    /**
     * Schreibt die Liste der LKZ (L�nderkennzahlen) in einen Vektor.
     * @return list Gibt diese Liste zur�ck.
     */
    public List getLKZList();

    /**
     * Schreibt die Liste der Bundesl�nder in einen Vektor.
     * @return list Gibt diese Liste zur�ck.
     */
    public List getBundeslaenderList();

    /**
     * Der Benutzer m�chte die Anwendung schlie�en
     */
    public void userRequestExitApplication();

    /**
     * Holt die Adresse unter dem als Parameter angegebenen Index.
     */
    public Address getAddressByIndex(int addressIndex);

    /**
     * Diese Methode liefert die Kommunikationsdatentypen, die von der DB-Umsetzung
     * abh�ngen k�nnen, als Abbildung von Identifikatoren auf Feldbeschreibungen
     * vom Typ {@link org.evolvis.nuyo.db.Database.CommunicationDescription CommunicationDescription}.
     * 
     * @return Abbildung mit den Kommunikationsdatenbeschreibungen.
     */
    public SortedMap getCommunicationFields();

    /*
     * administrative Methoden
     */
    /**
     * Diese Methode erzeugt eine Kategorie.
     * 
     * @param key der Kategorieschlssel.
     * @param name der Kategorieklartext.
     * @param description die Kategorienbeschreibung.
     */
    public void userRequestCreateCategory(String key, String name, String description);

    /**
     * Diese Methode editiert eine Kategorie.
     * 
     * @param key der Kategorieschlssel.
     * @param name der Kategorieklartext.
     * @param description die Kategorienbeschreibung.
     */
    public void userRequestEditCategory(String key, String name, String description);

    /**
     * Diese Methode l�scht eine Kategorie.
     * 
     * @param key der Kategorienschlssel.
     */
    public void userRequestDeleteCategory(String key); 


    /**
     * Diese Methode erzeugt eine Unterkategorie.
     * 
     * @param category die Kategorie, in der eine Unterkategorie
     *  erzeugt werden soll.
     * @param key der Unterkategorieschlssel.
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     *  Dieser Parameter wird nur benutzt, wenn das
     *  {@link org.evolvis.nuyo.db.Database#getFeature(String) Feature}
     *  "verteiler.kommentar" von der Datenzugriffsschicht untersttzt wird.
     */
    public void userRequestCreateVerteiler(
        Integer category,      
        String name1,
        String name2);

    /**
     * Diese Methode editiert eine Unterkategorie.
     * 
     * @param category die Kategorie, in der die Unterkategorie
     *  liegt.
     * @param key der Unterkategorieschlssel.
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     *  Dieser Parameter wird nur benutzt, wenn das
     *  {@link org.evolvis.nuyo.db.Database#getFeature(String) Feature}
     *  "verteiler.kommentar" von der Datenzugriffsschicht untersttzt wird.
     */
    public void userRequestEditVerteiler(String category, String key, String name1, String name2, String name3);

    /**
     * Diese Methode l�scht eine Unterkategorie.
     * 
     * @param category die Kategorie, in der eine Unterkategorie
     *  gel�scht werden soll.
     * @param key der Unterkategorieschlssel.
     */
    public void userRequestDeleteVerteiler(String category, String key);

    /**
     * Diese Methode erzeugt einen neuen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     * @param password das Passwort.
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param email des Users
     */
    public void userRequestCreateUser(String userId, String password, String lastName, String firstName, String email);

    /**
     * Diese Methode erzeug eine neue Benutzergruppe und liefert diese zur�ck. 
     * 
     * @param name Name der Benutzergruppe
     * @param description Beschreibung der Benutzergruppe
     * @param shortdesc Kurzbeschreibung
     * @param globalrolefk ID der globalen Rolle der Gruppe
     * @return erzeugte Benutzergruppe oder <code>null</code> im Fehlerfall
     */
    public UserGroup userRequestCreateUserGroup(String name, String description, String shortdesc, int globalrolefk);
    
    /**
     * Diese Methode �ndert einen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     * @param password das Passwort.
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param email des Users
     */
    public void userRequestEditUser(String userId, String password, String lastName, String firstName, String email);

    /**
     * Diese Methode l�scht einen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     */
    public void userRequestDeleteUser(String userId, boolean deleteAssoziatedAddress);
    
    /**
     * Diese Methode erzeugt einen Dialog zur manuellen Eingabe eines ContactEntries, falls die Parameter <code>null</code> oder 0 sind, werden Defaults eingesetzt
     * 
     * @param subject
     * @param note
     * @param datum
     * @param duration
     * @param incoming
     * @param category
     * @param channel
     * @param link
     * @param linkType
     * @param user
     * @param address
     */
    public void userRequestCreateContactEntryDialog(String subject, String note, Date datum, int duration, boolean incoming, int category, int channel, int link, int linkType, User user, Address address);
    
	/**
	 * Diese Methode erzeugt einen ContactEntry in der Datenbank
	 * 
	 * @param category
	 * @param channel
	 * @param duration
	 * @param inBound
	 * @param subject
	 * @param note
	 * @param startDate
	 */
	public Contact userRequestCreateContactEntry(Address address, User user, int category, int channel, int duration, boolean inBound, String subject, String note, Date startDate);

	/**
	 * Diese Methode erzeugt f�r alle Adressen in diesem Kanal des Versandauftrags einen ContactEntry
	 * 
	 * @param batch
	 * @param user
	 * @param MailBatchChannel
	 */
	public void userRequestCreateContactEntries(MailBatch batch, User user, MailBatch.CHANNEL MailBatchChannel);

    /**
     * Diese Methode liefert die Kollektion aller Benutzer als Abbildung
     * von Benutzer-ID auf Benutzernamen.
     * 
     * @return eine SortedMap aller Benutzer.
     */
    public SortedMap getBenutzer();


    /*
     * Diese Methoden und Variablen dienen der Event-Verarbeitung
     */
    /**
     * Diese Methode fgt einen neuen
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addDataChangeListener(DataChangeListener listener);

    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeDataChangeListener(DataChangeListener listener);

    /**
     * Diese Methode fgt einen neuen
     * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addMasterDataListener(MasterDataListener listener);

    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeMasterDataListener(MasterDataListener listener);

    /**
     * Diese Methode fgt einen neuen
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addContextVetoListener(ContextVetoListener listener);

    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeContextVetoListener(ContextVetoListener listener);

    /**
     * Setzt je nach Parameter den Mauspfeil auf "Sanduhr" bzw. "normal";
     * sollte mit dem Parameter "true" aufgerufen werden, bevor eine
     * langwierige Operation ausgefhrt wird. Nach Beendigung der
     * Operation muss die Methode erneut aufgerufen werden, jedoch
     * nun mit dem Parameter "false"
     * 
     * @param isWaiting wenn true: Sanduhr anzeigen.
     */
    public void setWaiting(boolean isWaiting);


    /**
     * Diese Methode liefert den Terminplan eines Benutzers.
     * 
     * @param userId Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Terminplan des Benutzers oder <code>null</code>, falls
     *  diese Funktionalit�t nicht vorliegt.
     */
    public Schedule getSchedule(String userId);

    /**
     * Diese Methode liefert eine Map der Benutzer-Logins auf zugeh�rige
     * {@link User}-Instanzen.
     * 
     * @return Map Benutzerlogins auf User-Instanzen.
     */
    public Collection getUserGroups();

    /**
     * Diese Methode liefert zu dem angegebenen Benutzer (Default: der
     * eingelogte Benutzer) eine User-Instanz.
     * 
     * @param userLogin Login-Name des Benutzers; <code>null</code> referenziert den
     *  aktuell eingelogten Benutzer.
     * @return User-Instanz zum angegebenen Benutzer oder <code>null</code>, wenn
     *  es keinen Benutzer zum angegebenen Login gibt.
     */
    public User getUser(String userLogin);
    
    public User getUser(String userLogin, boolean enableChaching);

    /**
     * Diese Methode liefert eine Map der Benutzer-Logins auf zugeh�rige
     * {@link User}-Instanzen.
     * 
     * @return Map Benutzerlogins auf User-Instanzen.
     */
    public Map getUsers();
    
    /**
     * Diese Methode liefert den Usernamen eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Name des Benutzers oder <code>null</code>, falls
     *  dieser Benutzer nicht existiert.
     */
    public String getUserName(String userid);

    /**
     * Speichert die Farbe eines Kaleders auf dem Server
     *
     * @param kalenderID Id des Kalenders
     * @param colorCode String mit dem Farbcode
     */
    public void setCalendarColor(int kalenderID, String colorCode);


    /**
     * Liefert die Farbe eines Kaleders auf dem Server
     *
     * @param kalenderID Id des Kalenders
     * @return Farbe des Kalenders als RGB-Code
     */
    public String getCalendarColor(int kalenderID);


    /**
     * Diese Methode liefert einen Parameter eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @param key Schlssel des Parameters.
     * @return Parameter des Benutzers oder <code>null</code>, falls
     *  dieser Benutzer nicht existiert.
     */
    public String getUserParameter(String userid, String key);

    /**
     * Diese Methode setzt einen Parameter eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @param key Schlssel des Parameters.
     * @param value neuer Wert des Parameters oder <code>null</code>, falls
     *  der Parameter gel�scht werden soll.
     */
    public void setUserParameter(String userid, String key, String value);
    
    public void setEventStatus(int userId, int eventId, int statusId);

    /**
     * Diese Methode liefert das Special-Flag eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     */
    public boolean isSpecial(String userid);

    /**
     * Diese Methode liefert das isAdmin-Flag eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     */
    public boolean isAdmin(String userid);

    public JFrame getFrame();
    
    public boolean isSearchActive();
    public int getSearchMode();
    
    
    // liefert eine Liste aller verf�gbaren CHANNELs
    public List getAvailableChannels();
    
    
    /**
     * Diese Methode testet ob die darunterliegende Datenquelle ein bestimmtes Feature untersttzt
     */
    public Object getDatabaseFeature(String feature);

    public final static int ADDRESSSET_KEINFILTER = 0;
    public final static int ADDRESSSET_TEXTSUCHE = 1;
    public final static int ADDRESSSET_VERSANDAUFTRAG_ALL = 3;
    public final static int ADDRESSSET_VERSANDAUFTRAG_NICHTZUGEORDNET = 4;
    public final static int ADDRESSSET_VERSANDAUFTRAG_EMAIL = 5;
    public final static int ADDRESSSET_VERSANDAUFTRAG_FAX = 6;
    public final static int ADDRESSSET_VERSANDAUFTRAG_POST = 7;
    public final static int ADDRESSSET_FEHLERHAFTE_EMAILS = 8;
    public final static int ADDRESSSET_SINGLE_ADDRESS = 9;

    public String getDatabaseVersion();

    /** Liefert eine Darstellbare Ausgabe der Server URL */
    public String getDisplaySource();

    	
	public PanelInserter getPanelInserter();
	
	public String requestFilename(String startfolder, String presetfile, String[] extensions, String extensiondescription, boolean forSave);
	
	/**
	 * @param contact
	 */
	public void userRequestCreateContactEntryDialog(Contact contact);
	

	public Veto userRequestCheckForVeto(VetoableAction action);
	/**
	 * Diese Methode liefert einen Preferences-Knoten im Contact-Zweig. 
	 * 
	 * @param subNode relativer Pfad des Unterknotens. Falls <code>null</code>, so wird der
	 *  contact-Basisknoten geliefert.
	 * @return ausgew�hlter Preferences-Knoten
	 */
	public Preferences getPreferences(String subNode);
	  
  public PluginLocator getPluginLocator();

  public String[] getLookupTableEntries(String lookuptablekey);
  public Object getLookupTableValue(String lookuptablekey, Object key)  throws ContactLookUpNotFoundException;
  
  public DataAccess getDataAccess();
  
  public List getAvailablePreviewRows();
  
  public void setPreviewColumnDbIDs(String[] ids);
  public String[] getPreviewColumnDbIDs();  
  public void setPreviewColumnAdressIDs(String[] ids);
  public String[] getPreviewColumnAdressIDs();
  
  public String getPreviewColumnName(String id);
  public boolean loadPreviews(int from, int to);
  public boolean sortPreviewBy(String id, boolean ascending);          
  public int getIndexOfAddressByLetter(String startswith);

  public List getGlobalRoles();          
  public List getFolderRoles();
  
  public void setCurrentJournalEntry(Contact con);
  public Contact getCurrentJournalEntry();
  
  
}
