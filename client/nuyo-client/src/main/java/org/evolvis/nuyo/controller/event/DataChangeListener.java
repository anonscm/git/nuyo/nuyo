/* $Id: DataChangeListener.java,v 1.3 2006/12/12 09:31:08 robert Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.event;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;

/**
 * Diese Schnittstelle muss von Klassen implementiert werden, die von der
 * Contact-Kontrolle �ber �nderungen in den Daten benachrichtigt werden
 * wollen.
 * 
 * @author mikel
 */
public interface DataChangeListener {
    /**
     * Diese Methode teilt mit, dass �nderungen in einem Address-Objekt
     * in die Datenbank gespeichert werden sollen.
     * 
     * @param address das zu speichernde Adressobjekt.
     * @param index der Index der zu speichernden Adresse in der
     *  aktuellen Adressensammlung; wenn -1, so ist die Adresse
     *  nicht in der Addresssammlung.
     */
    public void savingAddressChanges(Address address, int index);

    /**
     * Diese Methode teilt mit, dass �nderungen in einem Address-Objekt
     * verworfen werden sollen.
     * 
     * @param address das originale Adressobjekt.
     * @param index der Index der zur�ckgesetzten Adresse in der
     *  aktuellen Adressensammlung; wenn -1, so ist die Adresse
     *  nicht in der Addresssammlung.
     */
    public void discardingAddressChanges(Address address, int index);

    /**
     * Diese Methode teilt mit, dass sich eine Adresse in der Datenbank
     * ge�ndert hat.
     * 
     * @param address das aktualisierte Address-Objekt.
     * @param index der Index der aktualisierten Adresse in der
     *  aktuellen Adressensammlung; wenn -1, so ist die Adresse
     *  nicht in der Addresssammlung.
     */
    public void addressChanged(Address address, int index);

    /**
     * Diese Methode teilt mit, dass sich die zugrundeliegende Sammlung
     * von Adressen ge�ndert hat.
     * 
     * @param addresses die neue Adresssammlung.
     */
    public void addressesChanged(Addresses addresses);
    
    /**
     * Diese Methode teilt mit, welche Adresse in der Adress-Liste zu markieren ist.
     * 
     * @param index der Index der zu makierenden Adresse.
     */
    public void selectAddress(int index);

    /**
     * F�llt die GUI mit den �ber das Address-Objekt �bergebenen Werten.
     * Wenn als Parameter "null" �bergeben wird so werden alle Felder der
     * GUI gel�scht.
     * 
     * @param address zu setzende Adresse (wenn null werden alle Felder gel�scht).
     * @param overWrite erzwingt das erneute Setzen der Adresse, auch wenn sie scheinbar
     *  schon die aktuelle ist.
     * @throws ContactDBException bei Datenzugriffsproblemen
     */
    public void setAddress(Address address, boolean overWrite) throws ContactDBException;

    /**
     * Setzt die Titel-Leiste der Anwendung
     * 
     * @param title der neue Text der Titel-Leiste.
     */
    public void setCaption(String title);

    /**
     * Erm�glicht das Setzen des dargestellten Index des aktuellen
     * Datensatzes und der Anzahl angezeigter Datens�tze (Navigationsleiste).
     * 
     * @param currentIndex Index des aktuell angezeigten Datensatzes
     * @param numEntries Anzahl aller zur Navigation bereitstehenden Datens�tze
     */
    public void setNavigationCounter(int currentIndex, int numEntries);


    /**
     * Benachrichtigung beim �ndern einer Kategory
     */
    public void categoryChanged(Category newCategory);
}
