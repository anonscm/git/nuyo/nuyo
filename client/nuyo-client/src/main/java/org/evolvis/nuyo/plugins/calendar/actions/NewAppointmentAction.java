/**
 * 
 */
package org.evolvis.nuyo.plugins.calendar.actions;

import java.awt.event.ActionEvent;

import javax.swing.JDialog;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.plugins.calendar.ui.AppointmentDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class NewAppointmentAction extends AbstractGUIAction {

	protected JDialog appointmentDialog;
	
	public void actionPerformed(ActionEvent e) {
		
		getDialog().setVisible(true);
	}
	
	protected JDialog getDialog() {
		if(appointmentDialog == null)
			appointmentDialog = new AppointmentDialog(ApplicationServices.getInstance().getActionManager().getFrame());
		return appointmentDialog;
	}
}
