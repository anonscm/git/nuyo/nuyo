/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.config.ConfigManager;

import de.tarent.commons.utils.TaskManager.Context;
import de.tarent.commons.utils.TaskManager.Task;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeExportTask implements Task
{
	private final static Logger logger = Logger.getLogger(OfficeExportTask.class.getName());
	private int count;
	private Addresses addresses;
	private String fileName;
	private boolean showDocument;
	private String printerName;
	private String docFileName;
	private int replaceMode;
	private boolean invisibility;
	
	public OfficeExportTask(Addresses pAddresses, int pCount, String pFileName, boolean pShowDocument, String pPrinterName, String pDocFileName, int pReplaceMode, boolean pInvisibility)
	{
		addresses = pAddresses;
		count = pCount;
		fileName = pFileName;
		showDocument = pShowDocument;
		printerName = pPrinterName;
		docFileName = pDocFileName;
		replaceMode = pReplaceMode;
		invisibility = pInvisibility;
	}
	
	public void cancel()
	{
		// Not used here.
		// process checks context.isCancelled() by itself
	}

	public void run(Context ctx)
	{	
		ctx.setActivityDescription(Messages.getString("GUI_OFFICE_EXPORT_PROGRESS_DIALOG_SORTING_ADDRESSES"));
		
		// sort addresses
		try
		{
            // TODO: Addresszugriff: sort the addresses with the new address access            
            // 			((AddressesImpl)addresses).setSearchEnabled(true);
            // 			addresses.setSortField("lastname");
            // 			addresses.sortAddressesByPreview();
		}
		catch(Exception pExcp)
		{
			pExcp.printStackTrace();
		}
		
		ctx.setActivityDescription(Messages.getFormattedString("GUI_OFFICE_EXPORT_PROGRESS_DIALOG_CONNECTING_TO_OFFICE", OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getName()));
		
		// show message when connection failed. But do not show, when user cancelled the process
		if(!OfficeAutomat.getOfficeConfigurator().startPreferredOffice(ctx) && !ctx.isCancelled())
		{
			ctx.setGoal(0);
			ctx.setActivityDescription("");
			// Show individual help-text depending on whether the user is allowed to configure the office himself
			if(ConfigManager.getEnvironment().isUserOverridable())
				JOptionPane.showMessageDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame(), Messages.getFormattedString("GUI_OFFICE_EXPORT_PROGRESS_DIALOG_CONNECT_TO_OFFICE_FAILED", OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getName()), Messages.getString("GUI_OFFICE_EXPORT_PROGRESS_DIALOG_CONNECT_TO_OFFICE_FAILED_TITLE"), JOptionPane.ERROR_MESSAGE);
			else 
				JOptionPane.showMessageDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame(), Messages.getFormattedString("GUI_OFFICE_EXPORT_PROGRESS_DIALOG_CONNECT_TO_OFFICE_FAILED_ADMIN", OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getName()), Messages.getString("GUI_OFFICE_EXPORT_PROGRESS_DIALOG_CONNECT_TO_OFFICE_FAILED_TITLE"), JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		ctx.setGoal(count);
		
		long time = System.currentTimeMillis();
		OfficeAutomat.getInstance().replaceByEntityList(fileName, addresses, showDocument, printerName, docFileName, replaceMode, invisibility, ctx, count);
		if(logger.isLoggable(Level.INFO)) logger.info("Replacement of "+addresses.getSize()+" entities took "+(System.currentTimeMillis()-time)+" ms");
	}
}
