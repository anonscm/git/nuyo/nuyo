/*
 * Created on 14.04.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetCheckBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextArea;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextAreaField;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author nils
 *
 */
public class AddressLabelField extends ContactAddressTextAreaField {

    /**
     * The Country to ommit in the generated address label
     */
    public static String HOME_COUNTRY = "DEUTSCHLAND";
    private static final TarentLogger logger = new TarentLogger( AddressLabelField.class);
    private HashMap adressData = new HashMap();
    private Boolean isChecked;

    private TarentWidgetTextArea textArea;
    private TarentWidgetCheckBox checkbox;
    private EntryLayoutHelper panel = null;

    public String getFieldName() {
        return "AdressLabel";
    }

    public int getContext() {
        return CONTEXT_ADRESS;
    }

    public String getFieldDescription() {
        return "Ein Feld zur Anzeige der Adressanschrift";
    }

    public EntryLayoutHelper getPanel(String widgetFlags) {
        if ( panel == null )
            panel = createPanel(widgetFlags);
        return panel;
    }

    public void postFinalRealize() {
    }

    public void setData( PluginData data ) {

        adressData.put( AddressKeys.VORNAME, data.get( AddressKeys.VORNAME ) );
        adressData.put( AddressKeys.NACHNAME, data.get( AddressKeys.NACHNAME ) );
        adressData.put( AddressKeys.TITEL, data.get( AddressKeys.TITEL ) );
        adressData.put( AddressKeys.AKADTITEL, data.get( AddressKeys.AKADTITEL ) );
        adressData.put( AddressKeys.HERRFRAU, data.get( AddressKeys.HERRFRAU ) );
        adressData.put( AddressKeys.NAMENSZUSATZ, data.get( AddressKeys.NAMENSZUSATZ ) );
        adressData.put( AddressKeys.POSITION, data.get( AddressKeys.POSITION ) );
        adressData.put( AddressKeys.INSTITUTION, data.get( AddressKeys.INSTITUTION ) );
        adressData.put( AddressKeys.ABTEILUNG, data.get( AddressKeys.ABTEILUNG ) );
        adressData.put( AddressKeys.NAMENSZUSATZ, data.get( AddressKeys.NAMENSZUSATZ ) );

        adressData.put( AddressKeys.LETTERADDRESS, data.get( AddressKeys.LETTERADDRESS ) );

        adressData.put( AddressKeys.LETTER_AUTO, data.get( AddressKeys.LETTER_AUTO ) );
        isChecked = (Boolean) data.get( AddressKeys.LETTER_AUTO );

        if ( data.get( AddressKeys.LETTER_AUTO ) == null || data.get( AddressKeys.LETTER_AUTO ).equals( "" ) )
            isChecked = Boolean.TRUE;

        //Anschrift
        adressData.put( AddressKeys.STRASSE, data.get( AddressKeys.STRASSE ) );
        adressData.put( AddressKeys.HAUSNUMMER, data.get( AddressKeys.HAUSNUMMER ) );
        adressData.put( AddressKeys.PLZ, data.get( AddressKeys.PLZ ) );
        adressData.put( AddressKeys.ORT, data.get( AddressKeys.ORT ) );
        adressData.put( AddressKeys.POSTFACHPLZ, data.get( AddressKeys.POSTFACHPLZ ) );
        adressData.put( AddressKeys.POSTFACH, data.get( AddressKeys.POSTFACH ) );
        adressData.put( AddressKeys.LAND, data.get( AddressKeys.LAND ) );
        adressData.put( AddressKeys.BUNDESLAND, data.get( AddressKeys.BUNDESLAND ) );

        //System.out.println("ischecked: " + ischecked);
        checkbox.setSelected( isChecked.booleanValue() );
        textArea.setEditable( !isChecked.booleanValue() );
        //m_oButton_Change.setEnabled(!ischecked.booleanValue());

        refreshTextArea();
    }

    public void getData( PluginData data ) {

        isChecked = new Boolean( checkbox.isSelected() );

        data.set( AddressKeys.LETTER_AUTO, isChecked );
        data.set( AddressKeys.LETTERADDRESS, textArea.getData() != null  ? (String) textArea.getData() : null);
    }

    public boolean isDirty( PluginData data ) {

        if ( data.get( AddressKeys.LETTER_AUTO ) != null )
            if ( !( checkbox.isSelected() == ( (Boolean) data.get( AddressKeys.LETTER_AUTO ) ).booleanValue() ) )
                return ( true );

        if ( !checkbox.isSelected() && !textArea.getText().equals( "" ) )
            if ( data.get( AddressKeys.LETTERADDRESS ) == null
                || !( textArea.getText().equalsIgnoreCase( data.get( AddressKeys.LETTERADDRESS ).toString() ) ) )
                return ( true );

        return false;
    }

    public void setDoubleCheckSensitive(boolean issensitive ) {
    }

    private EntryLayoutHelper createPanel(String widgetFlags) {
        textArea = createTextArea( "dieser Text kann als Adressanschrift z.B. in Briefen verwendet werden", 300);

        checkbox = new TarentWidgetCheckBox( "Adressanschrift automatisch generieren " );
        checkbox.setToolTipText( "automatische Generierung der Adressanschrift ein-/ausschalten" );
        checkbox.addActionListener( new CheckboxDisabled() );
        checkbox.setEnabled( false );

        return new EntryLayoutHelper(new TarentWidgetInterface[]
                                                                 {
                                                                  new TarentWidgetLabel("Briefanschrift:"),
                                                                  checkbox,
                                                                  textArea
                                                                 }, widgetFlags);
    }

    public String getKey() {
        return "ADDRESSLABEL";
    }

    public void setEditable( boolean iseditable ) {
        if ( checkbox.isSelected() ) {
            textArea.setEditable( false );
        }

        else {
            textArea.setEditable( iseditable );
        }

        checkbox.setEnabled( iseditable );
        refreshTextArea();
    }

    private void refreshTextArea() {

        if ( !checkbox.isSelected() ) {
            textArea.setText( (String) adressData.get( AddressKeys.LETTERADDRESS ) );
        }
        else {

            /**
             * Falls kein Eintrag f�r einen Nachnamen existiert, soll erst gar kein Label generiert werden, um einen DB-Eintrag
             * bei der Neuerstellung einer Adresse zu vermeiden, der nur aus Zeilenumbrchen besteht
             */

            /* 
             if (adressData.get(AdressKeys.NACHNAME) == null || ("").equals(adressData.get(AdressKeys.NACHNAME))){
             m_oTextArea.setText("");
             return;					
             }
             */

            StringBuffer z1 = new StringBuffer( "" );
            StringBuffer z2 = new StringBuffer( "" );
            StringBuffer z3 = new StringBuffer( "" );
            StringBuffer z4 = new StringBuffer( "" );
            StringBuffer z5 = new StringBuffer( "" );
            StringBuffer z6 = new StringBuffer( "" );
            StringBuffer z7 = new StringBuffer( "" );

            StringBuffer[] zeilen = { z1, z2, z3, z4, z5, z6, z7 };
            String result = "";
            int i = 0; // zeilenpointer

            if ( (String) adressData.get( AddressKeys.HERRFRAU ) != null
                && ( (String) adressData.get( AddressKeys.HERRFRAU ) ).equalsIgnoreCase( "Herr" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.HERRFRAU ) );
                zeilen[i].append( "n " );// *** Herr *** aus Herr mache Herrn 
            }
            else if ( adressData.get( AddressKeys.HERRFRAU ) != null
                && !adressData.get( AddressKeys.HERRFRAU ).equals( "" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.HERRFRAU ) ); // *** Frau oder andere Bezeichnung (Mr., Mrs. etc) ***
                zeilen[i].append( " " );
            }

            //if (adressData.get(AdressKeys.POSITION) != null && !adressData.get(AdressKeys.POSITION).equals("")){
            //	zeilen[i].append(adressData.get(AdressKeys.POSITION)); // Beruf oder Position (z.B. Direktor)    			
            //	i++; // n?hste Zeile 			
            //}

            // Akademischer Titel (z.B. Prof. Dr.)
            if ( adressData.get( AddressKeys.AKADTITEL ) != null && !adressData.get( AddressKeys.AKADTITEL ).equals( "" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.AKADTITEL ) );
                zeilen[i].append( " " );
            }

            //Vorname
            if ( adressData.get( AddressKeys.VORNAME ) != null && !adressData.get( AddressKeys.VORNAME ).equals( "" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.VORNAME ) );
                zeilen[i].append( " " );
            }
            // Titel (z.B. Freiherr von)
            if ( adressData.get( AddressKeys.TITEL ) != null && !adressData.get( AddressKeys.TITEL ).equals( "" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.TITEL ) );
                zeilen[i].append( " " );
            }

            //Nachname
            if ( adressData.get( AddressKeys.NACHNAME ) != null && !adressData.get( AddressKeys.NACHNAME ).equals( "" ) ) {

                zeilen[i].append( adressData.get( AddressKeys.NACHNAME ) );
                if ( !( adressData.get( AddressKeys.NAMENSZUSATZ ) != null
                    && ( (String) adressData.get( AddressKeys.NAMENSZUSATZ ) ).length() > 0 && ( (String) adressData
                    .get( AddressKeys.NAMENSZUSATZ ) ).charAt( 0 ) == ',' ) )
                    zeilen[i].append( " " );
            }

            //Namenszusatz
            if ( adressData.get( AddressKeys.NAMENSZUSATZ ) != null
                && !adressData.get( AddressKeys.NAMENSZUSATZ ).equals( "" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.NAMENSZUSATZ ) );
                //zeilen[i].append(" ");
            }

            i++; // n?hste Zeile

            // Hier kommt die Position f�r den Landtag Brandenburg hin
            if ( adressData.get( AddressKeys.POSITION ) != null && !adressData.get( AddressKeys.POSITION ).equals( "" ) ) {
                zeilen[i].append( adressData.get( AddressKeys.POSITION ) );
                i++;
            }

            if ( adressData.get( AddressKeys.INSTITUTION ) != null
                && !adressData.get( AddressKeys.INSTITUTION ).equals( "" ) ) {

                zeilen[i].append( adressData.get( AddressKeys.INSTITUTION ) ); // Institution

                i++; // n?hste Zeile
            }

            if ( adressData.get( AddressKeys.ABTEILUNG ) != null && !adressData.get( AddressKeys.ABTEILUNG ).equals( "" ) ) {

                zeilen[i].append( adressData.get( AddressKeys.ABTEILUNG ) ); // Abteilung

                i++;
            }

            // ************** Postfach oder Anschrift ************

            if ( adressData.get( AddressKeys.STRASSE ) != null
                && !adressData.get( AddressKeys.STRASSE ).equals( "" )
                && !( adressData.get( AddressKeys.POSTFACHPLZ ) != null
                    && !adressData.get( AddressKeys.POSTFACHPLZ ).equals( "" ) && ( adressData.get( AddressKeys.POSTFACH ) != null && !adressData
                    .get( AddressKeys.POSTFACH ).equals( "" ) ) ) ) { // kein Postfach oder keine postfachplz -> Anschrift

                // Stra?
                zeilen[i].append( adressData.get( AddressKeys.STRASSE ) );
                zeilen[i].append( " " );

                if ( adressData.get( AddressKeys.HAUSNUMMER ) != null
                    && !adressData.get( AddressKeys.HAUSNUMMER ).equals( "" ) ) { // HausNr.
                    zeilen[i].append( adressData.get( AddressKeys.HAUSNUMMER ) );
                }
            }

            else {
                if ( adressData.get( AddressKeys.POSTFACH ) != null
                    && !adressData.get( AddressKeys.POSTFACH ).equals( "" )
                    && adressData.get( AddressKeys.POSTFACHPLZ ) != null
                    && !adressData.get( AddressKeys.POSTFACHPLZ ).equals( "" ) ) { // nur Postfach nehmen, wenn auch Postfachplz korrekt

                    zeilen[i].append( "Postfach " );
                    zeilen[i].append( adressData.get( AddressKeys.POSTFACH ) );
                }
            }

            i++; // n?hste Zeile

            //zeilen[i].append("\n"); // Zeileneinschub zwischen Adresse und Ortangabe

            if ( ( adressData.get( AddressKeys.PLZ ) != null && !adressData.get( AddressKeys.PLZ ).equals( "" ) )
                && !( adressData.get( AddressKeys.POSTFACH ) != null
                    && !adressData.get( AddressKeys.POSTFACH ).equals( "" )
                    && adressData.get( AddressKeys.POSTFACHPLZ ) != null && !adressData.get( AddressKeys.POSTFACHPLZ )
                    .equals( "" ) ) ) { // PLZ wenn Postfach und Postfachplz nicht korrekt sind
                zeilen[i].append( adressData.get( AddressKeys.PLZ ) );
                zeilen[i].append( " " );
            }

            if ( adressData.get( AddressKeys.POSTFACHPLZ ) != null
                && !adressData.get( AddressKeys.POSTFACHPLZ ).equals( "" ) ) {
                if ( adressData.get( AddressKeys.POSTFACH ) != null
                    && !adressData.get( AddressKeys.POSTFACH ).equals( "" ) ) { // PostfachPLZ verwenden wenn es einen Postfacheintrag UND eine PostfachPLZ gibt

                    zeilen[i].append( adressData.get( AddressKeys.POSTFACHPLZ ) );
                    zeilen[i].append( " " );
                }
            }
            if ( adressData.get( AddressKeys.ORT ) != null && !adressData.get( AddressKeys.ORT ).equals( "" ) ) { // Ort			
                zeilen[i].append( adressData.get( AddressKeys.ORT ) );
                zeilen[i].append( " " );
            }

            i++; // n?hste Zeile

            // **************** Land *****************

            if ( adressData.get( AddressKeys.LAND ) != null && !adressData.get( AddressKeys.LAND ).equals( "" )
                && !HOME_COUNTRY.equalsIgnoreCase( (String) adressData.get( AddressKeys.LAND ) ) ) { // Land
                zeilen[i].append( ( (String) adressData.get( AddressKeys.LAND ) ).toUpperCase() ); // Land immer in GROSSBUCHSTABEN !!!
                zeilen[i].append( " " );
            }

            //if (adressData.get(AdressKeys.BUNDESLAND) != null && !adressData.get(AdressKeys.BUNDESLAND).equals("")){ // Bundesland
            //	zeilen[i].append(adressData.get(AdressKeys.BUNDESLAND));
            //}

            // ***** String konkatenieren *****

            for ( int j = 0; j < 7; j++ ) {

                if ( zeilen[j].length() > 0 ) {
                    result += zeilen[j].toString();
                    result += "\n";
                }
            }

            textArea.setText( result );

        }
    }
    
    private class CheckboxDisabled implements ActionListener {
        public void actionPerformed( ActionEvent e ) {

            boolean checkBoxIsChecked = checkbox.isSelected();

            textArea.setEditable( !checkBoxIsChecked );

            if ( checkBoxIsChecked ) {
                refreshTextArea();
            }

        }
    }
}
