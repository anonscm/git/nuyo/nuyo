/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Michael Klink. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.db;

/**
 * Diese Klasse stellt eine Unterkategorie dar.
 * 
 * @author mikel
 */
public abstract class MailingList {
    /**
     * Diese Variablen halten die Informationen zu der Unterkategorie.
     */
    protected String gruppe, kennung, klartext1, klartext2, klartext3;

    // Zugriffsmethoden f�r die Attribute.
    /**
     * Returns the gruppe.
     * @return String
     */
    public String getGruppe() {
        return gruppe;
    }

    /**
     * Returns the kennung.
     * @return String
     */
    public String getKennung() {
        return kennung;
    }

    /**
     * Returns the klartext1.
     * @return String
     */
    public String getKlartext1() {
        return klartext1;
    }

    /**
     * Returns the klartext2.
     * @return String
     */
    public String getKlartext2() {
        return klartext2;
    }

    /**
     * Returns the klartext3.
     * @return String
     */
    public String getKlartext3() {
        return klartext3;
    }

    /**
     * Returns the schl�ssel.
     * @return String
     */
    public String getSchluessel() {
        return gruppe + kennung;
    }

    /**
     * Liefert den zusammengefassten Namen der Unterkategorie.
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String verteilerText = klartext1 != null ? klartext1 : "";
        if (klartext2 != null && klartext2.length() > 0)
            verteilerText += " " + klartext2;
        if (klartext3 != null && klartext3.length() > 0)
            verteilerText += " " + klartext3;
        return verteilerText;
    }
    
    public void setGruppe (String gruppe) {
        this.gruppe = gruppe;
    }
    
    public void setKennung (String kennung) {
        this.kennung = kennung;
    }
    
    public void setKlartext1 (String klartext1) {
        this.klartext1 = klartext1;
    }
    
    public void setKlartext2 (String klartext2) {
        this.klartext2 = klartext3;
    }

    public void setKlartext3 (String klartext3) {
        this.klartext3 = klartext3;
    }

    
}
