/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.logging;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * @author niko
 *
 */
public class LogEntry
{
  private Level m_oLevel;
  private String m_sMessage;
  private Throwable m_oThrowable;
  private List m_oParameters;
  private Date m_oDate; 
  
  public LogEntry(Level level, String msg)
  {
    m_oDate = new Date();
    m_oLevel = level;
    m_sMessage = msg;
    m_oThrowable = null;
    m_oParameters = null;
  }
  
  public LogEntry(Level level, String msg, Object param)
  {
    m_oDate = new Date();
    m_oLevel = level;
    m_sMessage = msg;
    m_oThrowable = null;
    m_oParameters = new ArrayList();
    m_oParameters.add(param);
  }
  
  public LogEntry(Level level, String msg, Object[] params)
  {
    m_oDate = new Date();
    m_oLevel = level;
    m_sMessage = msg;
    m_oThrowable = null;
    m_oParameters = new ArrayList();
    for(int i=0; i<(params.length); i++) m_oParameters.add(params[i]);
  }

  public LogEntry(Level level, String msg, Throwable throwable)
  {
    m_oDate = new Date();
    m_oLevel = level;
    m_sMessage = msg;
    m_oThrowable = throwable;
    m_oParameters = null;
  }
  
  
  /**
   * @return Returns the m_oLevel.
   */
  public Level getLevel()
  {
    return m_oLevel;
  }
  
  public String getLevelText()
  {
    return (m_oLevel.getLocalizedName());
  }
  
  /**
   * @return Returns the m_oParameters.
   */
  public List getParameters()
  {
    return m_oParameters;
  }
  
  /**
   * @return Returns the m_oThrowable.
   */
  public Throwable getThrowable()
  {
    return m_oThrowable;
  }
  
  public String getThrowableText()
  {
    if (m_oThrowable != null)
    {
      OutputStream os = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(os); 
      m_oThrowable.printStackTrace(ps);
      ps.flush();
      return os.toString();
    }
    return null;
  }
  
  
  /**
   * @return Returns the m_sMessage.
   */
  public String getMessage()
  {
    return m_sMessage;
  }

  /**
   * @return Returns the m_oDate.
   */
  public Date getDate()
  {
    return m_oDate;
  }
}
