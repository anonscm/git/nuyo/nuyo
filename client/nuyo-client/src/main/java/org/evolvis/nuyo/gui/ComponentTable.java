/* $Id: ComponentTable.java,v 1.3 2006/06/06 14:12:10 nils Exp $
 * 
 * Created on 21.05.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.gui;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ComponentTable extends JTable
{  
  private ComponentTableCellRenderer m_oComponentTableCellRenderer;
  private ComponentTableTableModel m_oComponentTableTableModel;
  private ComponentTableCellEditor m_oComponentTableCellEditor;
  private boolean m_bShowSelection;
  private Color m_oNormalColor;
  private Color m_oSelectedColor;
  private JTableHeader m_oTableHeader;

  static private Object[][] data = { {"a","b","c"},{"a","b","c"},{"a","b","c"} };
  static private Object[] head = {"a","b","c"};
  
  public ComponentTable()
  {
    super(data, head);
    m_oComponentTableTableModel = new ComponentTableTableModel();
    m_oComponentTableCellRenderer = new ComponentTableCellRenderer(m_oComponentTableTableModel);
    m_oComponentTableCellEditor = new ComponentTableCellEditor(m_oComponentTableTableModel);
    
    super.setModel(m_oComponentTableTableModel);
    super.setDefaultEditor(Object.class, m_oComponentTableCellEditor);
    m_oTableHeader = super.getTableHeader();    
    super.setTableHeader(null);
  }

  public ComponentTable(Color oNormalColor, Color oSelectedColor)
  {
    super(data, head);
    m_bShowSelection = true;
    m_oNormalColor = oNormalColor;
    m_oSelectedColor = oSelectedColor;

    m_oComponentTableTableModel = new ComponentTableTableModel();
    m_oComponentTableCellRenderer = new ComponentTableCellRenderer(m_oComponentTableTableModel);
    m_oComponentTableCellEditor = new ComponentTableCellEditor(m_oComponentTableTableModel);
    
    super.setModel(m_oComponentTableTableModel);    
    super.setDefaultEditor(Object.class, m_oComponentTableCellEditor);    
    m_oTableHeader = super.getTableHeader();    
    super.setTableHeader(null);
  }

  public void showTableHeader(boolean showheader)
  {
    if (showheader) super.setTableHeader(m_oTableHeader); else  super.setTableHeader(null);
  }


  public void refreshTable()
  {
    m_oComponentTableTableModel.fireTableDataChanged();
  }

  public void setColumnWidth(int column, int width)
  {
    this.getColumnModel().getColumn(column).setMaxWidth(width);
    this.getColumnModel().getColumn(column).setPreferredWidth(width);
    this.getColumnModel().getColumn(column).setMinWidth(width);
  }


  public void addColumn(String name)
  {    
    m_oComponentTableTableModel.setComponentColumnName(m_oComponentTableTableModel.getColumnCount(), name);
    super.getColumnModel().addColumn(new TableColumn());
    m_oComponentTableTableModel.fireTableStructureChanged();
  }
  
  public Component getComponentAt(int row, int column)
  {
    return((Component)(m_oComponentTableTableModel.getValueAt(row, column)));
  }
  
  public void setComponentAt(int row, int column, Component component)
  {
    if (m_bShowSelection)
    {
       if (component instanceof JComponent) ((JComponent)component).setOpaque(true); 
    } 
    m_oComponentTableTableModel.setValueAt(row, column, component);
  }

  public void removeAllComponents()
  {
    m_oComponentTableTableModel.removeAllComponents();
  }

  public void removeComponent(int row)
  {
    m_oComponentTableTableModel.removeComponent(row);
  }

  public void removeComponent(Object key)
  {
    m_oComponentTableTableModel.removeComponent(key);
  }

  public void setRowKey(int row, Object key)
  {
    m_oComponentTableTableModel.setRowKey(row, key);
  }

  public Object getRowKey(int row)
  {
    return(m_oComponentTableTableModel.getRowKey(row));
  }
  
  public int getRowCount()
  {
    if (m_oComponentTableTableModel != null) 
    {
      return(m_oComponentTableTableModel.getRowCount());
    } 
    else return(0);
  }

  public int getColumnCount()
  {
    if (m_oComponentTableTableModel != null) 
    {
      return(m_oComponentTableTableModel.getColumnCount());      
    } 
    else return(0);
  }

  public TableModel getModel()
  {
    return(m_oComponentTableTableModel);
  }
  
  public TableCellRenderer getCellRenderer(int row, int column) 
  {
     return(m_oComponentTableCellRenderer);
  }
  
  private class ComponentTableTableModel extends AbstractTableModel
  {
    private LinkedList m_oRows;
    private ArrayList m_oColumnNames;

    public ComponentTableTableModel()
    {
      m_oRows = new LinkedList();
      m_oColumnNames = new ArrayList();
    }
  
    public int getRowCount()
    {
      return(m_oRows.size());
    }
  
    public int getColumnCount()
    {
      return(m_oColumnNames.size());
    }
  
    public void setRowKey(int row, Object key)
    {
      setValueAt(row, -1, key);
    }

    public Object getRowKey(int row)
    {
      return(getValueAt(row, -1));
    }
    
    public int getRowOfKey(Object key)
    {
      for(int i=0; i<(m_oRows.size()); i++)
      {
        if (key.equals(getRowKey(i))) return(i);
      }
      return(-1);
    }
    
    public Object getValueAt(int row, int column)
    {
      LinkedList list = null;
      try
      {
        list = (LinkedList)(m_oRows.get(row));
        if (list != null)
        {
          return(list.get(column + 1));
        }
      }
      catch(IndexOutOfBoundsException iobe)
      {
      }
      return(null);
    }

    public void setValueAt(int row, int column, Object value)
    {
      LinkedList list = null;
      try
      {
        list = (LinkedList)(m_oRows.get(row));
      }
      catch(IndexOutOfBoundsException iobe)
      {
        list = new LinkedList();
        m_oRows.add(row, list);
        list.add(0, (Object)null);
      }

      if (list.size() <= (column+1)) list.add(column + 1, value); 
      else                                      list.set(column + 1, value);
    }
  
    public void removeAllComponents()
    {
      m_oRows.clear();
    }

    public void removeComponent(int row)
    {
      if (row >= 0) m_oRows.remove(row);
    }

    public void removeComponent(Object key)
    {
      removeComponent(getRowOfKey(key));    
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return(true); 
    }

    public void setComponentColumnName(int column, String name)
    {
      m_oColumnNames.add(column, name); 
    }    

    public String getColumnName(int column)
    {
      return(asString(m_oColumnNames.get(column))); 
    }
  }

  private class ComponentTableCellRenderer implements TableCellRenderer
  {         
    private ComponentTableTableModel m_oComponentTableTableModel;
    
    public ComponentTableCellRenderer(ComponentTableTableModel tablemodel)
    {
      m_oComponentTableTableModel = tablemodel;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {  
      Component comp = (Component)(m_oComponentTableTableModel.getValueAt(row, column));
      if (m_bShowSelection)
      {
        if (comp instanceof JComponent)
        {
          if (isSelected) ((JComponent)comp).setBackground(m_oSelectedColor);
          else                 ((JComponent)comp).setBackground(m_oNormalColor);
        }
      }
      return((Component)comp);
    }
  }
  
  
  public class ComponentTableCellEditor extends DefaultCellEditor
  {
    private ComponentTableTableModel m_oComponentTableTableModel;

    public ComponentTableCellEditor(ComponentTableTableModel tablemodel)
    {
      super(new JTextField());
      m_oComponentTableTableModel = tablemodel;
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) 
    { 
      Component comp = (Component)(m_oComponentTableTableModel.getValueAt(row, column));
      if (m_bShowSelection)
      {
        if (comp instanceof JComponent)
        {
          if (isSelected) ((JComponent)comp).setBackground(m_oSelectedColor);
          else                 ((JComponent)comp).setBackground(m_oNormalColor);
        }
      }
      return((Component)comp);
    }
              
    public int getClickCountToStart()
    {
      return(1); 
    }
  }
    
  private final static String asString(Object o) {
      return o == null ? null : o.toString();
  }
}
  
