/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class JumpPreviousMailAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5461942385570056009L;

	public void actionPerformed(ActionEvent e)
	{
		DispatchEMailDialog.getInstance().jumpToPreviousMail();
	}
}
