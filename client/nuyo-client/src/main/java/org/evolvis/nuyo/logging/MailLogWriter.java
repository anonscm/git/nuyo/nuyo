/*
 * Created on 16.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;



/**
 * @author niko
 *
 */
public class MailLogWriter implements LogWriter
{
  private String m_sMailGateway = "mail.tarent.de";

  private String m_sToMailAddr = null;
  private String m_sFromMailAddr = null;
  private String m_sMessage = null;
  private String m_sTicketID = null;
  private Vector m_oAttachments;
  
  
  public MailLogWriter(String ticketid, String fromemailaddr, String toemailaddr, String message, Vector attachments)
  {
    m_sToMailAddr = toemailaddr;
    m_sFromMailAddr = fromemailaddr;
    m_oAttachments = attachments;
    m_sMessage = message;
    m_sTicketID = ticketid;
  }
  
  public boolean writeLog(String text)
  {    
    String outputFile = null;
    try
    {
      // "java.io.tmpdir"
      //outputFile = System.getProperty("user.home")+File.separator+"logfile_" + m_sTicketID + ".xml";  
      outputFile = System.getProperty("java.io.tmpdir")+File.separator+"logfile_" + m_sTicketID + ".xml";  
      FileOutputStream out = new FileOutputStream(outputFile);
      out.write(text.getBytes());
      out.close();
    }
    catch (FileNotFoundException e1)
    {
      e1.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    } 
    
    
    if (outputFile != null)
    {
      m_oAttachments.add(new String[] {"logfile_" + m_sTicketID + ".xml", outputFile});      
    }

    String subject = "tarent-Contact Meldung [ID " + m_sTicketID + "]";
    
//    try
//    {       
//      if (m_sFromMailAddr.trim().length() == 0) m_sFromMailAddr = "contact@tarent.de"; 
//      MailSender sender = new MailSender(m_sMailGateway, m_sFromMailAddr, m_sToMailAddr, subject, m_sMessage, m_oAttachments, 0, false, null);
//    }
//    catch (MessagingException e)
//    {
//      e.printStackTrace();
//      deleteFile(outputFile);
//      return false;
//    }
    
    deleteFile(outputFile);
    return true;
  }

  private void deleteFile(String filename)
  {
    if (filename != null)
    {
      File file = new File(filename);
      if (file != null)
      {
        file.delete();
      }
    }
  }
  
}
