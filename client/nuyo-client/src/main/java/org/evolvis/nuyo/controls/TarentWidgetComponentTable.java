/* $Id: TarentWidgetComponentTable.java,v 1.3 2007/01/11 18:56:41 robert Exp $
 * 
 * Created on 11.04.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.evolvis.nuyo.logging.TarentLogger;


/**
 * @author d-fens
 *
 */
public class TarentWidgetComponentTable extends JPanel implements TarentWidgetInterface {
    
    private static final long serialVersionUID = 225878515014389862L;
    private static final TarentLogger logger = new TarentLogger(TarentWidgetComponentTable.class);
    private List changeListeners = new ArrayList();
    private ObjectTableModel objectTableModel;
    private JScrollPane scrollPane;
    private JTable table;
    
    private Color selectionBackgroundColor;
    private Color selectionForegroundColor;


    public TarentWidgetComponentTable( ObjectTableModel tablemodel ) {
        objectTableModel = tablemodel;
        table = new JTable( tablemodel );

        selectionBackgroundColor = table.getSelectionBackground();
        selectionForegroundColor = table.getSelectionForeground();

        initLayout();
    }

    public TarentWidgetComponentTable( TableSorter sorter ) {
        table = new JTable( sorter );

        selectionBackgroundColor = table.getSelectionBackground();
        selectionForegroundColor = table.getSelectionForeground();

        initLayout();
    }

    public TarentWidgetComponentTable( int numcolumns, Dimension dimension) {
        table = new JTable( new ObjectTableModel( numcolumns ) );
        if ( dimension != null )
            table.setPreferredScrollableViewportSize( dimension );

        initLayout();

    }

    public TarentWidgetComponentTable( int numcolumns ) {
        this( numcolumns, null);
    }

    private void initLayout() {
        scrollPane = new JScrollPane( table );
        setLayout( new BorderLayout() );
        add( scrollPane, BorderLayout.CENTER );
    }
  
    public void setData( Object data ) {
        ((ObjectTableModel) table.getModel()).removeAllRows();
        addData( data );
    }

    public void addData( Object data ) {
        if ( data instanceof Object[] ) {
            ( (ObjectTableModel) ( table.getModel() ) ).addRow( (Object[]) data );
        }
        table.revalidate();
    }

    public void addData( Object data, Object tooltips ) {
        if ( ( data instanceof Object[] ) && ( tooltips instanceof String[] ) ) {
            ( (ObjectTableModel) ( table.getModel() ) ).addRow( (Object[]) data, (String[]) tooltips );
        }
        table.revalidate();
    }

    public void removeData( Object data ) {
        if ( data instanceof Integer ) {
            ( (ObjectTableModel) ( table.getModel() ) ).removeRow( ( (Integer) data ).intValue() );
        }
        table.revalidate();
    }

    public void removeAllData() {
        ( (ObjectTableModel) ( table.getModel() ) ).removeAllRows();
        table.revalidate();
    }

    public int getNumberOfEntries() {
        return ( (ObjectTableModel) ( table.getModel() ) ).getRowCount();
    }

    public Object getData() {
        return ( null );
    }

    public JComponent getComponent() {
        return ( this );
    }

    public void setLengthRestriction( int maxlen ) {
    }

    public void setWidgetEditable( boolean iseditable ) {
        table.setEnabled( iseditable );
    }

    public boolean isEqual( Object data ) {
        return false;
    }

    public void setColumnName( int column, String name ) {
        ( (ObjectTableModel) table.getModel() ).setColumnName( column, name );
    }

    public void setColumnEditable( int col, boolean iseditable ) {
        ( (ObjectTableModel) table.getModel() ).setColumnEditable( col, iseditable );
    }

    public Object getRowKey( int row ) {
        return ( (ObjectTableModel) table.getModel() ).getRowKey( row );
    }

    public Object setRowKey( int row, Object key ) {
        return ( (ObjectTableModel) table.getModel() ).setRowKey( row, key );
    }

    public void initColumnSizes( int samplerow ) {
        ObjectTableModel model = (ObjectTableModel) table.getModel();
        if ( samplerow < model.getRowCount() ) {
            TableColumn column = null;
            Component comp = null;
            int headerWidth = 0;
            int cellWidth = 0;
            Object[] longValues = model.getRow( samplerow );

            TableCellRenderer headerRenderer = table.getTableHeader().getDefaultRenderer();

            for ( int i = 0; i < ( table.getColumnModel().getColumnCount() ); i++ ) {
                column = table.getColumnModel().getColumn( i );

                comp = headerRenderer.getTableCellRendererComponent( null, column.getHeaderValue(), false, false, 0, 0 );
                headerWidth = comp.getPreferredSize().width;

                comp = table.getDefaultRenderer( model.getColumnClass( i ) )
                    .getTableCellRendererComponent( table, longValues[i], false, false, 0, i );
                cellWidth = comp.getPreferredSize().width;

                //XXX: Before Swing 1.1 Beta 2, use setMinWidth instead.
                column.setPreferredWidth( Math.max( headerWidth, cellWidth ) );
            }
        }
        else
            logger.warning( "Die angegebene Vergleichszeile existiert nicht. (Zeile " + samplerow + " von "
                + model.getRowCount() + ")" );
    }

    public void setColumnSizes( int[] width ) {
        if ( ( width.length > 0 ) && ( width.length <= table.getColumnModel().getColumnCount() ) )
            for ( int i = 0; i < ( table.getColumnModel().getColumnCount() ); i++ ) {
                TableColumn column = table.getColumnModel().getColumn( i );
                column.setPreferredWidth( width[i] );
            }
    }

    public void setFixedColumnSize( int col, int width ) {
        TableColumn column = table.getColumnModel().getColumn( col );
        column.setPreferredWidth( width );
        column.setMaxWidth( width );
        column.setMinWidth( width );
    }

    public void setColumnSize( int col, int width ) {
        TableColumn column = table.getColumnModel().getColumn( col );
        column.setPreferredWidth( width );
    }

    public void setColumnSize( int col, int min, int pref, int max ) {
        TableColumn column = table.getColumnModel().getColumn( col );
        column.setMinWidth( min );
        column.setPreferredWidth( pref );
        column.setMaxWidth( max );
    }

    public void setColumnSize( int width ) {
        for ( int i = 0; i < ( table.getColumnModel().getColumnCount() ); i++ ) {
            TableColumn column = table.getColumnModel().getColumn( i );
            column.setPreferredWidth( width );
        }
    }

    public JTable getJTable() {
        return table;
    }

    public void setColumnComponent( int column, Object stdcomp, Object editcomp ) {
        TableCellRenderer renderer = null;
        TableCellEditor editor = null;

        if ( stdcomp instanceof JButton ) {
            renderer = new ButtonCellRenderer( (JButton) stdcomp );
        }
        else if ( stdcomp instanceof ImageIcon ) {
            renderer = new IconCellRenderer( (ImageIcon) stdcomp );
        }
        else if ( stdcomp instanceof JComboBox ) {
            renderer = new ComboCellRenderer( (JComboBox) stdcomp );
        }
        else if ( stdcomp instanceof JTextField ) {
            renderer = new TextFieldCellRenderer();
        }
        else if ( stdcomp instanceof JCheckBox ) {
            renderer = new CheckCellRenderer( (JCheckBox) stdcomp );
        }

        if ( editcomp instanceof JButton ) {
            editor = new ButtonCellEditor( (JButton) editcomp );
        }
        else if ( editcomp instanceof ImageIcon ) {
            editor = new IconCellEditor( (ImageIcon) editcomp );
        }
        else if ( editcomp instanceof JComboBox ) {
            editor = new ComboCellEditor( (JComboBox) editcomp );
        }
        else if ( editcomp instanceof JTextField ) {
            editor = new TextFieldCellEditor( (JTextField) editcomp );
        }
        else if ( editcomp instanceof JCheckBox ) {
            editor = new CheckCellEditor( (JCheckBox) editcomp );
        }

        table.getColumnModel().getColumn( column ).setCellEditor( editor );
        table.getColumnModel().getColumn( column ).setCellRenderer( renderer );
    }

    public void addWidgetChangeListener( TarentWidgetChangeListener listener ) {
        changeListeners.add( listener );
    }

    public void removeWidgetChangeListener( TarentWidgetChangeListener listener ) {
        changeListeners.remove( listener );
    }

    // ------------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------------  

    private class ComboCellRenderer implements TableCellRenderer {
        private JComboBox comboBox;

        public ComboCellRenderer( JComboBox combo ) {
            comboBox = combo;
        }

        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column ) {
            comboBox.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return comboBox;
        }
    }

    // --------------------------------------------------

    private class ComboCellEditor extends DefaultCellEditor {
        private JComboBox comboBox;

        public ComboCellEditor( JComboBox combo ) {
            super( combo );
            comboBox = combo;
        }

        public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row,
                                                     int column ) {
            comboBox.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            comboBox.setSelectedItem( value );
            return comboBox;
        }

        public Object getCellEditorValue() {
            return comboBox.getSelectedItem();
        }

    }

    // ------------------------------------------------------------------------------------------  

    // ------------------------------------------------------------------------------------------  

    private class TextFieldCellRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column ) {
            JComponent comp = (JComponent) ( super.getTableCellRendererComponent( table, value, isSelected, hasFocus,
                                                                                  row, column ) );
            comp.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return comp;
        }
    }

    // --------------------------------------------------

    private class TextFieldCellEditor extends DefaultCellEditor {
        public TextFieldCellEditor( JTextField comp ) {
            super( comp );
        }

        public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row,
                                                     int column ) {
            JComponent comp = (JComponent) ( super.getTableCellEditorComponent( table, value, isSelected, row, column ) );
            comp.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return comp;
        }
    }

    // ------------------------------------------------------------------------------------------  

    private class ButtonCellRenderer implements TableCellRenderer {
        private JButton button;

        public ButtonCellRenderer( JButton aButton ) {
            button = aButton;
        }

        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column ) {
            button.setText( value.toString() );
            button.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return button;
        }
    }

    // ------------------------------------------------------------------------------------------  

    private class ButtonCellEditor implements TableCellEditor {
        private JButton button;

        public ButtonCellEditor( JButton aButton ) {
            button = aButton;
        }

        public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row,
                                                     int column ) {
            button.setText( value.toString() );
            button.setName( row + "," + column );
            button.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return button;
        }

        public void cancelCellEditing() {
        }

        public boolean stopCellEditing() {
            return true;
        }

        public Object getCellEditorValue() {
            return button.getText();
        }

        public boolean isCellEditable( EventObject anEvent ) {
            return true;
        }

        public boolean shouldSelectCell( EventObject anEvent ) {
            return true;
        }

        public void addCellEditorListener( CellEditorListener l ) {
        }

        public void removeCellEditorListener( CellEditorListener l ) {
        }
    }

    // ------------------------------------------------------------------------------------------  
    private class CheckCellRenderer implements TableCellRenderer {
        private JCheckBox checkBox;

        public CheckCellRenderer( JCheckBox check ) {
            checkBox = check;
        }

        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column ) {
            checkBox.setName( row + "," + column );
            checkBox.setSelected( ( (Boolean) value ).booleanValue() );
            if ( isSelected ) {
                checkBox.setBackground( selectionBackgroundColor );
                checkBox.setForeground( selectionForegroundColor );
            }
            else {
                checkBox.setBackground( Color.WHITE );
                checkBox.setForeground( Color.BLACK );
            }

            checkBox.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return checkBox;
        }
    }

    // ------------------------------------------------------------------------------------------  

    private class CheckCellEditor implements TableCellEditor {
        private JCheckBox checkBox;
        private int rowIndex = -1;
        private int columnIndex = -1;

        public CheckCellEditor( JCheckBox check ) {
            checkBox = check;
        }

        public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row,
                                                     int column ) {
            rowIndex = row;
            columnIndex = column;
            checkBox.setName( row + "," + column );

            checkBox.setSelected( ( (Boolean) value ).booleanValue() );
            //System.out.println("getTableCellEditorComponent " + row + ", " + column + " = " + value);      

            if ( isSelected ) {
                checkBox.setBackground( selectionBackgroundColor );
                checkBox.setForeground( selectionForegroundColor );
            }
            else {
                checkBox.setBackground( Color.WHITE );
                checkBox.setForeground( Color.BLACK );
            }
            checkBox.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return checkBox;
        }

        public void cancelCellEditing() {
            //System.out.println("cancelCellEditing()");      
            if ( ( rowIndex != -1 ) && ( columnIndex != -1 ) ) {
                objectTableModel.setValueAt( new Boolean( checkBox.isSelected() ), rowIndex, columnIndex );
            }
            rowIndex = -1;
            columnIndex = -1;
        }

        public boolean stopCellEditing() {
            int selectedRow = table.getSelectedRow();
            int selectedColumn = table.getSelectedColumn();
            if ( ( selectedRow != -1 ) && ( selectedColumn != -1 ) ) {
                objectTableModel.setValueAt( new Boolean( checkBox.isSelected() ), selectedRow, selectedColumn );
            }
            return true;
        }

        public Object getCellEditorValue() {
            return new Boolean( checkBox.isSelected() );
        }

        public boolean isCellEditable( EventObject anEvent ) {
            return true;
        }

        public boolean shouldSelectCell( EventObject anEvent ) {
            return true;
        }

        public void addCellEditorListener( CellEditorListener l ) {
        }

        public void removeCellEditorListener( CellEditorListener l ) {
        }
    }

    // ------------------------------------------------------------------------------------------  

    // ------------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------------  

    private class IconCellRenderer implements TableCellRenderer {
        private ImageIcon icon;

        private JLabel label;

        public IconCellRenderer( ImageIcon anIcon ) {
            icon = anIcon;
            label = new JLabel( icon );
            label.setOpaque( true );
        }

        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column ) {
            if ( value instanceof ImageIcon )
                label.setIcon( (ImageIcon) value );
            if ( isSelected ) {
                label.setBackground( selectionBackgroundColor );
                label.setForeground( selectionForegroundColor );
            }
            else {
                label.setBackground( Color.WHITE );
                label.setForeground( Color.BLACK );
            }
            label.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return label;
        }
    }

    // ------------------------------------------------------------------------------------------  

    private class IconCellEditor implements TableCellEditor {
        private ImageIcon icon;

        private JLabel label;

        public IconCellEditor( ImageIcon anIcon ) {
            icon = anIcon;
            label = new JLabel( icon );
        }

        public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row,
                                                     int column ) {
            if ( value instanceof ImageIcon )
                label.setIcon( (ImageIcon) value );
            label.setToolTipText( objectTableModel.getTooltipAt( row, column ) );
            return label;
        }

        public void cancelCellEditing() {
        }

        public boolean stopCellEditing() {
            return false;
        }

        public Object getCellEditorValue() {
            return label.getIcon();
        }

        public boolean isCellEditable( EventObject anEvent ) {
            return false;
        }

        public boolean shouldSelectCell( EventObject anEvent ) {
            return true;
        }

        public void addCellEditorListener( CellEditorListener l ) {
        }

        public void removeCellEditorListener( CellEditorListener l ) {
        }

    }

    // ------------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------------  

    public static class ObjectTableModel extends AbstractTableModel {
        private int columnsNumber;
        private String[] columnsNames = null;
        private Boolean[] isEditable;
        private List rows;
        private List rowsKeys;
        private List rowsTooltips;

        public ObjectTableModel( int numcolumns ) {
            columnsNumber = numcolumns;
            columnsNames = new String[columnsNumber];
            isEditable = new Boolean[columnsNumber];
            rows = new ArrayList();
            rowsKeys = new ArrayList();
            rowsTooltips = new ArrayList();

            for ( int i = 0; i < columnsNumber; i++ ) {
                columnsNames[i] = "";
                isEditable[i] = new Boolean( false );
            }
        }

        public void setColumnName( int column, String name ) {
            columnsNames[column] = name;
        }

        public void addRow( Object[] columns ) {
            rows.add( columns );
            rowsKeys.add( new Object() );
            rowsTooltips.add( new String[columns.length] );
        }

        public void addRow( Object[] columns, Object rowkey ) {
            rows.add( columns );
            rowsKeys.add( rowkey );
            rowsTooltips.add( new String[columns.length] );
        }

        public void addRow( Object[] columns, String[] tooltips ) {
            rows.add( columns );
            rowsKeys.add( new Object() );
            rowsTooltips.add( tooltips );
        }

        public void addRow( Object[] columns, String[] tooltips, Object rowkey ) {
            rows.add( columns );
            rowsKeys.add( rowkey );
            rowsTooltips.add( tooltips );
        }

        public Object[] getRow( int row ) {
            return (Object[]) ( rows.get( row ) );
        }

        public String[] getRowTooltip( int row ) {
            return (String[]) ( rowsTooltips.get( row ) );
        }

        public Object getRowKey( int row ) {
            try {
                return rowsKeys.get( row );
            }
            catch ( IndexOutOfBoundsException iobe ) {
                return null;
            }
        }

        public int getRowWithKey( Object object ) {
            for ( int i = 0; i < ( rowsKeys.size() ); i++ ) {
                if ( object.equals( rowsKeys.get( i ) ) )
                    return i;
            }
            return -1;
        }

        public Object setRowKey( int row, Object key ) {
            return rowsKeys.set( row, key );
        }

        public void removeRow( int row ) {
            rows.remove( row );
            rowsKeys.remove( row );
            rowsTooltips.remove( row );
        }

        public void removeAllRows() {
            rows.clear();
            rowsKeys.clear();
            rowsTooltips.clear();
        }

        public int getColumnCount() {
            return columnsNumber;
        }

        public int getRowCount() {
            return rows.size();
        }

        public String getColumnName( int col ) {
            return columnsNames[col];
        }

        public Object getValueAt( int row, int col ) {
            Object[] rowdata = getRow( row );
            return rowdata[col];
        }

        public String getTooltipAt( int row, int col ) {
            String[] rowdata = getRowTooltip( row );
            String tooltip = rowdata[col];
            return tooltip;
        }

        public Class getColumnClass( int c ) {
            try {
                return getValueAt( 0, c ).getClass();
            }
            catch ( NullPointerException npe ) {
                return String.class;
            }
        }

        public boolean isCellEditable( int row, int col ) {
            return isEditable[col].booleanValue();
        }

        public void setColumnEditable( int col, boolean iseditable ) {
            isEditable[col] = new Boolean( iseditable );
        }

        public void setValueAt( Object value, int row, int col ) {
            Object[] rowdata = getRow( row );
            rowdata[col] = value;
            fireTableCellUpdated( row, col );
        }

        public void setTooltipAt( String value, int row, int col ) {
            String[] rowdata = getRowTooltip( row );
            rowdata[col] = value;
            fireTableCellUpdated( row, col );
        }
    }
}
