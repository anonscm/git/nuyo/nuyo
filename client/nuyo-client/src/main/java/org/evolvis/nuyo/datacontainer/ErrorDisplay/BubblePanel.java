/*
 * Created on 16.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorDisplay;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorage;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;


/**
 * @author niko
 *
 */
public class BubblePanel extends JPanel {
    private static final Point m_oOffset = new Point( 10, 10 );
    private Map containerMap;

    public BubblePanel() {
        containerMap = new HashMap();
        this.setLayout( null );
    }

    public void addError( ErrorHint errorhint ) {
        JComponent guicomp = errorhint.getDataContainer().getGUIElement().getComponent().getComponent();
        if ( !guicomp.isShowing() ) {
            return;
        }

        List list = (List) ( containerMap.get( errorhint.getDataContainer() ) );
        if ( list == null ) {
            list = new ArrayList();
            containerMap.put( errorhint.getDataContainer(), list );
        }

        if ( !( containsErrorHint( list, errorhint ) ) ) {
            ErrorBubble bubble = new ErrorBubble();
            bubble.setBubbleError( errorhint );
            this.add( bubble );

            list.add( new VisibleBubble( errorhint, bubble ) );

            Point panelpos = getLocationOnScreen();
            Point guicomppos = guicomp.getLocationOnScreen();

            Point elementoffset = new Point( 0, 0 );
            DataContainer datacontainer = errorhint.getDataContainer();
            if ( datacontainer != null ) {
                GUIElement guielement = datacontainer.getGUIElement();
                if ( guielement != null ) {
                    elementoffset = guielement.getOffsetForError( errorhint );
                }
            }

            Point pos = new Point( guicomppos.x - panelpos.x + m_oOffset.x + elementoffset.x,
                                   ( guicomppos.y - panelpos.y ) - ( bubble.getHeight() ) + m_oOffset.y
                                       + elementoffset.y );
            bubble.setLocation( pos.x, pos.y );
        }
        else {
            // duplicate errorhint
            new Exception( "DuplicateErrorException" ).printStackTrace();
        }

    }

    private boolean containsErrorHint( List list, ErrorHint errorhint ) {
        Iterator it = list.iterator();
        while ( it.hasNext() ) {
            VisibleBubble vb = (VisibleBubble) ( it.next() );
            if ( vb.getErrorHint().equals( errorhint ) )
                return true;
        }
        return false;
    }

    public void removeError( ErrorHint errorhint ) {
        List list = (List) ( containerMap.get( errorhint.getDataContainer() ) );
        if ( list != null ) {
            for ( int i = 0; i < ( list.size() ); i++ ) {
                if ( ( (VisibleBubble) ( list.get( i ) ) ).getErrorHint().equals( errorhint ) ) {
                    VisibleBubble vb = (VisibleBubble) ( list.remove( i ) );
                    if ( vb.getErrorBubble() != null )
                        this.remove( vb.getErrorBubble() );
                    return;
                }
            }
        }
    }

    public void removeAllErrors() {
        containerMap.clear();
        this.removeAll();
    }

    public void removeAllErrorsOfDataContainer( DataContainer container ) {
        List list = (List) ( containerMap.get( container ) );
        if ( list != null ) {
            while ( list.size() > 0 ) {
                VisibleBubble vb = (VisibleBubble) ( list.remove( 0 ) );
                if ( vb.getErrorBubble() != null )
                    this.remove( vb.getErrorBubble() );
            }
        }
    }

    public void removeAllErrorsOfDataStorage( DataStorage datastorage ) {
        Iterator it = containerMap.keySet().iterator();
        while ( it.hasNext() ) {
            DataContainer dc = (DataContainer) ( it.next() );
            if ( dc.getDataStorage().equals( datastorage ) ) {
                removeAllErrorsOfDataContainer( dc );
            }
        }
    }
    
    
    private class VisibleBubble {
        private ErrorHint errorHint;
        private ErrorBubble errorBubble;

        public VisibleBubble( ErrorHint hint, ErrorBubble bubble ) {
            errorHint = hint;
            errorBubble = bubble;
        }

        public ErrorHint getErrorHint() {
            return errorHint;
        }

        public ErrorBubble getErrorBubble() {
            return errorBubble;
        }
    }
}
