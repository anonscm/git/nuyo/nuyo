/* $Id: DupCheckResult.java,v 1.3 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 24.07.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.nuyo.db;

/**
 * Diese Klasse stellt ein Duplikatcheck-Ergebnis dar.
 * 
 * @author mikel
 */
public class DupCheckResult {
    /*
     * Konstanten f�r Resultate und darin gefundene Dubletten
     */
    public final static int STATUS_ERROR = -1;
    public final static int STATUS_NEW = 0;
    public final static int STATUS_MERGED = 1;
    public final static int STATUS_IGNORED = 2; // nur bei Dublettenfunden

    /**
     * Diese Methode liefert die Duplikate zu der Startadresse dieses
     * Duplikatcheck-Ergebnisses.
     * 
     * @return Sammlung der Duplikate zu der Startadresse.
     * @throws ContactDBException
     */
    public Addresses getDuplicateAddresses() throws ContactDBException {
        return (dupCheck != null) ? dupCheck.getDuplicateAddresses(id) : null;
    }

    /**
     * Diese Methode f�hrt Duplikate des aktuellen Duplikatchecks zusammen.
     * 
     * @param targetId Id, unter der die Duplikate zusammengef�hrt werden sollen.
     *  Bei 0 wird automatisch eine ausgew�hlt. 
     * @param addressData zusammengef�hrte Adressdaten 
     * @param addressesToMerge Adressen, die in der Zusammenf�hrung aufgehen.
     * @param deleteMerged gibt an, ob gemergte Adressen in den L�schverteiler
     *  verschoben werden sollen.
     * @throws ContactDBException bei Datenzugriffsproblemen
     */
    public void merge(int targetId, Address addressData, Addresses addressesToMerge, boolean deleteMerged) throws ContactDBException {
        if (dupCheck != null)
            dupCheck.merge(id, targetId, addressData, addressesToMerge, deleteMerged);
        setStatus(STATUS_MERGED);
    }

    /*
     * Getter und Setter
     */
    /**
     * @return Startadressenindex
     */
    public int getAddressStart() {
        return addressStart;
    }

    /**
     * @return Startadresse
     */
    public Address getStartAddress() throws ContactDBException {
        return db.getAddress(addressStart);
    }

    /**
     * @return Merge-Adresse
     */
    public Address getAddressMerge() throws ContactDBException {
        return db.getAddress(addressMerge);
    }

    /**
     * @return Duplikatcheck
     */
    public DupCheck getDupCheck() {
        return dupCheck;
    }

    /**
     * @return Duplikatcheck-Id
     */
    public int getDupCheckId() {
        return dupCheckId;
    }

    /**
     * @return Duplikatzahl
     */
    public int getDupCount() {
        return dupCount;
    }

    /**
     * @return Id
     */
    public int getId() {
        return id;
    }

    /**
     * @return Status
     */
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) throws ContactDBException {
        this.status = status;
        if (dupCheck != null)
            dupCheck.setResultStatus(id, status);
    }

    /**
     * @return Kommando
     */
    public String getUpdateCommand() {
        return updateCommand;
    }

    /*
     * Konstruktoren
     */
    public DupCheckResult(DupCheck dc, Database db, int id, int dupCheckId, int addressStart, int addressMerge, int status, int dupCount, String updateCommand) {
        this.dupCheck = dc;
        this.db = db;
        this.id = id;
        this.dupCheckId = dupCheckId;
        this.addressStart = addressStart;
        this.addressMerge = addressMerge;
        this.status = status;
        this.dupCount = dupCount;
        this.updateCommand = updateCommand;
    }

    /*
     * gesch�tzte Member-Variablen
     */
    protected DupCheck dupCheck = null;
    protected Database db = null;
    protected int id = 0;
    protected int dupCheckId = 0;
    protected int addressStart = 0;
    protected int status = 0;
    protected int dupCount = 0;
    protected int addressMerge = 0;
    protected String updateCommand = null;
}
