/* $Id: GlobalRole.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;


/**
 * Interface f�r globale Rechte wie zum Beispiel User anlegen/l�schen.
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public interface GlobalRole extends Role {

    public static int ROLE_TYPE_OTHER = 0;
    public static int ROLE_TYPE_ADMIN = 1;
    public static int ROLE_TYPE_PUBLIC = 2;

    public static final String KEY_EDITUSER = "auth_edituser";
    public static final String KEY_REMOVEUSER = "auth_removeuser";
    public static final String KEY_EDITFOLDER = "auth_editfolder";
    public static final String KEY_REMOVEFOLDER = "auth_removefolder";
    public static final String KEY_EDITSCHEDULE = "auth_editschedule";
    public static final String KEY_REMOVESCHEDULE = "auth_removeschedule";
    public static final String KEY_DELETE = "auth_delete";

    public static final String KEY_ROLE_TYPE = "roleType";
          
    public static final String[] AUTH_KEYS = { KEY_DELETE, KEY_REMOVESCHEDULE, KEY_EDITSCHEDULE, KEY_REMOVEFOLDER, KEY_EDITFOLDER, KEY_REMOVEUSER, KEY_EDITUSER };


 /**
  * gibt an, ob diese Rolle gel�scht werden darf
  * @return
  */
 
 public boolean isDeletable();
 
 public boolean isAdminRole();

        
 /**
  * Gibt an, ob Rolle das Recht enth�lt, andere User zu �ndern.
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightEditUser();
 
 /**
  * Gibt an, ob Rolle das Recht enth�lt, andere User zu l�schen.
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightRemoveUser();
 
 /**
  * Gibt an, ob Rolle das Recht enth�lt, Kategorien zu �ndern.
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightEditFolder();
 
 /**
  * Gibt an, ob Rolle das Recht enth�lt, Kategorien zu l�schen.
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightRemoveFolder();
 
 /**
  * Gibt an, ob Rolle das Recht enth�lt, Kalender zu �ndern.
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightEditSchedule();
 
 /**
  * Gibt an, ob Rolle das Recht enth�lt, Kalender zu l�schen
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightRemoveSchedule();
 
 /**
  * Gibt an, ob Rolle das Recht enth�lt, zu Adressen zu l�schen.
  * @return <code>true</code>, falls Recht besteht, <code>false</code> sonst. 
  */
 public boolean hasRightDelete();
 
}
