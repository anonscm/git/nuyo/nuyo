/* $Id: ApplicationStarter.java,v 1.9 2007/08/30 16:10:25 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import java.util.Map;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.XmlUtil;
import org.evolvis.xana.config.Environment.Key;

import de.tarent.commons.utils.SystemInfo;

/**
 * Starten von Anwendungen wie Browser, Mail, ... aus dieser Anwendung heraus.
 *
 * Da der Start von Anwendungen je nach System unterschiedlich ist,
 * ist diese Klasse  abstract und dient als Factory.
 * 
 * Eine Instanz f�r das aktuelle System soll nur �ber die Methode
 * {@link #getSystemApplicationStarter(Map, ControlListener)
 * getSystemApplicationStarter()} geholt werden.
 * 
 * Da dieser Starter zustandslos ist, wird er als Singelton vorgehalten.
 * 
 * @author Sebastian, mikel
 */
public abstract class ApplicationStarter {
    
    public final static String POST_FAX = "fax";
    public final static String POST_FAX_OFFICE = "fax_dienstlich";
    public final static String POST_FAX_PRIVATE = "fax_privat";
    public final static String POST_MAIL_POBOX = "mail_postfach";
    public final static String POST_MAIL_ZIP = "mail_plz";
    public final static String POST_EMAIL = "email";
    public final static String OPTI_EMAIL = "opt_mail:mail";
    public final static String OPTI_FAX = "opt_mail:fax";
    public final static String OPTI_POST = "opt_mail:post";

    protected static ApplicationStarter platformStarter;

    /**
     * Diese Methode liefert plattform-abh�ngig eine ApplicationStarter-Implementierung.
     */
    public static ApplicationStarter getSystemApplicationStarter() {
        String cfgUseOpenOffice = asString(ConfigManager.getEnvironment().get(Key.POST_TO_OPEN_OFFICE));

        if ( platformStarter == null ) {
            if (SystemInfo.isWindowsSystem())
                platformStarter = new WindowsApplicationStarter();
            else
                platformStarter = new ConfigurableApplicationStarter();
        }
        return platformStarter;	
    }

    
    /**
     * Startet den Browser
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public abstract String startBrowser( String url );

   /**
    * Startet den Mail Client
    * @return null, wenn alles ok ist, eine Fehlermeldung sonst
    */
   public abstract String startEMail( String address );
    
    /**
     * Startet den Mail Client
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public abstract String startEMail( Address address );
    
    /**
     * Startet den Mail Client
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public abstract String startEMail( Contact con );
    
    /**
     * Diese Methode startet ein Telefonat 
     * 
     * @param phoneNumber die anzurufende Telefonnummer
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public abstract String startPhoneCall(String phoneNumber);

    /**
     * Startet einen Postversand an eine Einzeladresse
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public abstract String startPostAction(String action, Address address);

    /**
     * Startet einen Postversand an eine Adressauswahl im Rahmen des optimierten
     * Postversands.
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public abstract String startPostAction(String action, MailBatch batch);


  /**
   * Startet einen Postversand an eine Einzeladresse
   * 
   * @return null, wenn alles ok ist, eine Fehlermeldung sonst
   */
  public abstract String startPostAction(String action, Address address, String serverurl, String username, String password);

  /**
   * Startet einen Postversand an eine Adressauswahl im Rahmen des optimierten
   * Postversands.
   * 
   * @return null, wenn alles ok ist, eine Fehlermeldung sonst
   */
  public abstract String startPostAction(String action, MailBatch batch, String serverurl, String username, String password);

    /**
     * Diese Methode erzeugt einen XML-String, der dem Finder zu �bergeben ist,
     * wenn eine Einzeladresse beschickt werden soll.
     * 
     * @param action der Aktionsbezeichner, vgl. die POST_*-Konstanten.
     * @param address die Einzeladresse, die beschickt werden soll.
     */
    protected static String createXML(String action, Address address) throws ContactDBException {
        return "<cmd><se><sf n=\"ADR_NR\">" + address.getAdrNr() + "</sf></se><co><out><addresstype>" + XmlUtil.escape(action) + "</addresstype></out></co></cmd>";
    }
    
    /**
     * Diese Methode erzeugt einen XML-String, der dem Finder zu �bergeben ist,
     * wenn ein Massenversand ausgef�hrt werden soll.
     * 
     * @param action der Aktionsbezeichner, vgl. die OPTI_*-Konstanten.
     * @param batch der Versandauftrag, der beschickt werden soll.
     */
    protected static String createXML(String action, MailBatch batch) {
        return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><cmd><se><sf n=\"ORDERID\">" + batch.getId() + "</sf><sn n=\"ordername\">" + XmlUtil.escape(batch.getName()) + "</sn></se><co><out><addresstype>" + XmlUtil.escape(action) + "</addresstype></out></co></cmd>";
    }
    

    private static int m_iUID = -1;
    public static void setUID(int uid)
    {
      m_iUID = uid;
    }

    public static String getUID()
    {
      if (m_iUID == -1) return null; 
      return Integer.toString(m_iUID);      
    }
        
  /**
   * Diese Methode erzeugt einen XML-String, der dem Finder zu �bergeben ist,
   * wenn eine Einzeladresse beschickt werden soll.
   * 
   * @param action der Aktionsbezeichner, vgl. die POST_*-Konstanten.
   * @param address die Einzeladresse, die beschickt werden soll.
   */
  protected static String createXML(String action, Address address, String serverurl, String username, String password) throws ContactDBException 
  {
      String servertag;
      String usertag;
      String passtag;
      String uidtag;
      String uid = getUID();
      
      if (serverurl == null) servertag = ""; else servertag = "<srv>" + serverurl + "</srv>"; 
      if (username == null) usertag = ""; else usertag = "<usr>" + username + "</usr>";
      if (password == null) passtag = ""; else passtag = "<psw>" + password + "</psw>";
      if (uid == null) uidtag = ""; else uidtag = "<uid>" + uid + "</uid>";
      
      return "<cmd>" + servertag + usertag + passtag + uidtag + "<se><sf n=\"ADR_NR\">" + address.getAdrNr() + "</sf></se><co><out><addresstype>" + XmlUtil.escape(action) + "</addresstype></out></co></cmd>";
  }
    
  /**
   * Diese Methode erzeugt einen XML-String, der dem Finder zu �bergeben ist,
   * wenn ein Massenversand ausgef�hrt werden soll.
   * 
   * @param action der Aktionsbezeichner, vgl. die OPTI_*-Konstanten.
   * @param batch der Versandauftrag, der beschickt werden soll.
   */
  protected static String createXML(String action, MailBatch batch, String serverurl, String username, String password) 
  {
    String servertag;
    String usertag;
    String passtag;
    String uidtag;
    String uid = getUID();
    
    if (serverurl == null) servertag = ""; else servertag = "<srv>" + serverurl + "</srv>"; 
    if (username == null) usertag = ""; else usertag = "<usr>" + username + "</usr>";
    if (password == null) passtag = ""; else passtag = "<psw>" + password + "</psw>";
    if (uid == null) uidtag = ""; else uidtag = "<uid>" + uid + "</uid>";
    
    return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><cmd>" + servertag + usertag + passtag + uidtag + "<se><sf n=\"ORDERID\">" + batch.getId() + "</sf><sn n=\"ordername\">" + XmlUtil.escape(batch.getName()) + "</sn></se><co><out><addresstype>" + XmlUtil.escape(action) + "</addresstype></out></co></cmd>";
  }
    
  private final static String asString(Object o) {
      return o == null ? null : o.toString();
  }
    
}
