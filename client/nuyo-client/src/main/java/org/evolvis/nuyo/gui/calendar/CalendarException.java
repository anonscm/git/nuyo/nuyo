package org.evolvis.nuyo.gui.calendar;

public class CalendarException extends Exception {

    private static final long serialVersionUID = -2956710501801945603L;

    public CalendarException() {
        super();
    }

    public CalendarException( String message ) {
        super( message );
    }

    public CalendarException( String message, Throwable cause ) {
        super( message, cause );
    }

    public CalendarException( Throwable cause ) {
        super( cause );
    }

}
