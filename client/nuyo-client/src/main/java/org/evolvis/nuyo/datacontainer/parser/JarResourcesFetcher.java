

package org.evolvis.nuyo.datacontainer.parser;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;


public final class JarResourcesFetcher 
{
  private PluginLocator m_oPluginLocator;
  private Map m_oSizes = new HashMap();  
  private Map m_oJarContents = new HashMap();
   
  // ist nur Preset - echter Pfad wird vom PluginLocator ermittelt...
  private String m_sJarFileName = "/opt/tarent/client/lib/tc.jar";

  public JarResourcesFetcher(PluginLocator pluginlocator) throws ResourceFetcherException
  {     
    m_oPluginLocator = pluginlocator;
    if (m_oPluginLocator != null)
    {
      String loc = m_oPluginLocator.getPluginLocationString();
      if (loc != null)
      {
        if (loc.startsWith("file:")) loc = loc.substring(5);           
        m_sJarFileName = loc;
      }
    }

    init();
  }

  public JarResourcesFetcher(URL location) throws ResourceFetcherException
  {     
    String rawloc = location.toExternalForm();
    String[] parts = rawloc.split("!");
    if (parts != null)
    {
      String loc = parts[0];
      if (loc.startsWith("jar:")) loc = loc.substring(4);           
      if (loc.startsWith("file:")) loc = loc.substring(5);           
      m_sJarFileName = loc;
    }
    init();
  }

  public List getResourceList(String prefix) 
  {    
    List list = new ArrayList();
     
    Iterator it = m_oJarContents.keySet().iterator();
    while(it.hasNext())
    {
      String key = (String)(it.next());
      if (key.startsWith(prefix))
      {
        key = key.substring(prefix.length() + 1);
        if (key.indexOf('$') == -1)
        {
          list.add(key);
        }
      }
    }
    return list;
  }

  private void init() throws ResourceFetcherException
  {
    try 
    {
      ZipFile zf = new ZipFile(m_sJarFileName);
      Enumeration e = zf.entries();
      while (e.hasMoreElements()) 
      {
        ZipEntry ze = (ZipEntry)e.nextElement();
        m_oSizes.put(ze.getName(), new Integer((int)ze.getSize()));
      }
      zf.close();
  
      FileInputStream fis = new FileInputStream(m_sJarFileName);
      BufferedInputStream bis = new BufferedInputStream(fis);
      ZipInputStream zis = new ZipInputStream(bis);
      ZipEntry ze = null;
      while ((ze = zis.getNextEntry()) != null) 
      {
        if (ze.isDirectory()) 
        {
          continue;
        }
        int size=(int)ze.getSize();
        // -1 means unknown size. 
        if (size == -1) 
        {
          size = ((Integer)m_oSizes.get(ze.getName())).intValue();
        }
        byte[] b = new byte[(int)size];
        int rb = 0;
        int chunk = 0;
        while (((int)size - rb) > 0) 
        {
          chunk = zis.read(b, rb, (int)size - rb);
          if (chunk == -1) 
          {
            break;
          }
          rb += chunk;
        }
        // add to internal resource hashtable
        m_oJarContents.put(ze.getName(), b);
      }
    } 
    catch (NullPointerException e) 
    {
      throw new ResourceFetcherException("error reading jar-file: internal error.");
    } 
    catch (FileNotFoundException e) 
    {
      throw new ResourceFetcherException("error reading jar-file: file not found.");
    } 
    catch (IOException e) 
    {
      throw new ResourceFetcherException("error reading jar-file. IO-Error.");
    }
  }
}
