package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Activates a view of a category assignment.
 *
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ViewCategoryAssignmentAction extends AbstractGUIAction {

    private static final long serialVersionUID = 2742855577990680964L;
    private ControlListener mainFrame;

    public void actionPerformed(ActionEvent e) {
        mainFrame.activateTab(MainFrameExtStyle.TAB_CATEGORY_ASSIGNMENT_VIEW);
    }
    
    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();
        setEnabled(mainFrame.isTabEnabled(MainFrameExtStyle.TAB_CATEGORY_ASSIGNMENT_VIEW));
    }
}
