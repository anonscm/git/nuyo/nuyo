/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ValidatorInterfaces;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueAcceptEvent;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class ValidatorAdapter implements Validator
{
  protected DataContainer m_oDataContainer;
  protected List m_oParameterList = new ArrayList();

  public ValidatorAdapter()
  {
    m_oDataContainer = null;
  }
    
  public abstract boolean addParameter(String key, String value);
  public abstract boolean validate(Object data);
  public abstract boolean canValidate(Class data);
  
  public Object correct(Object data)
  {
    return data;
  }
  
  public void dispose()
  {
    getDataContainer().removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATE, this);
  }

  public void init()
  {
    getDataContainer().addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATE, this);
  }

  public List getEventsConsumable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_VALIDATE);
    return list;
  }

  public List getEventsFireable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_ACCEPT);
    list.add(TarentGUIEvent.GUIEVENT_DATAINVALID);
    return list;
  }

  public void event(TarentGUIEvent tge)
  {
    if (tge.getName().equals(TarentGUIEvent.GUIEVENT_VALIDATE))
    {
      if (validate(getDataContainer().getGUIData()))
      {
        fireTarentGUIEvent(new TarentGUIValueAcceptEvent(getDataContainer(), getDataContainer().getGUIData()));
      }
      else
      {
        fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_REJECT, getDataContainer(), null));
      }
    }
  }


  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().addTarentGUIEventListener(event, handler);
  }
    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().removeTarentGUIEventListener(event, handler);
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    getDataContainer().fireTarentGUIEvent(e);
  }
 
  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }
}
