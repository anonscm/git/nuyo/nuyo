package org.evolvis.nuyo.db.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.IEntity;



/**
 * @author kleinw
 *
 *	Dies ist die Basisklasse aller Filterimplementierungen.
 *
 */
abstract public class AbstractListSelection extends AbstractSelection {

    //
    //	Instanzmerkmale
    //
    /**	Hier werden Instanzen des entsprechenden Types verwaltet */
    protected Set _entities = new HashSet();

    
    /**	Konstruktor */
    public AbstractListSelection(Collection c) {_entities.addAll(c);}
    
    
    /**	Entity hinzuf�gen.@param entity - hinzuzuf�gende Entity */
    public void add(IEntity entity) {_entities.add(entity);}
    /**	Liste von Entities hinzuf�gen.@param entities - Collection mit PersistentEntities */
    public void add(Collection entities) {_entities.addAll(entities);}
    
    
    /**	Der Filter wird einer Map hinzugef�gt.@param map - Map, in die der Filter eingef�gt wird */
    public void includeInMethod(Map map) throws ContactDBException {
        List tmp = new ArrayList();
        for(Iterator it = _entities.iterator();it.hasNext();) {
            Object o = it.next();
            if(o instanceof IEntity)
                tmp.add(new Integer(((IEntity)o).getId()));
            else if(o instanceof Integer)
                tmp.add(o);
            else if(o instanceof String)
                tmp.add(new Integer((String)o));
            else
                throw new ContactDBException(ContactDBException.EX_FILTER_ERROR, "Nicht unterst�tzter Datentyp im Filter entdeckt.");
        }
        map.put(getFilterKey(), tmp);
    }

    
    /**	Hier wird festgelegt, wie der Schl�ssel des Filters heissen soll */
    abstract String getFilterKey();
    
    
    /**	Filter f�r Kalender */
    static public class CalendarList extends AbstractListSelection {
        public CalendarList(Collection c) {super(c);}
        String getFilterKey() {return "schedules";}
    }
    
    /**	Filter f�r Benutzer */
    static public class UserList extends AbstractListSelection {
        public UserList(Collection c) {super(c);}
        String getFilterKey() {return "users";}
    }
    
    /**	Filter f�r Benutzergruppen */
    static public class UserGroupList extends AbstractListSelection {
        public UserGroupList(Collection c) {super(c);}
        String getFilterKey() {return "usergroups";}
    }
    
}
