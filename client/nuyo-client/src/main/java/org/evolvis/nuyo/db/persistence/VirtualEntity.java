package org.evolvis.nuyo.db.persistence;

/**
 * @author kleinw
 *
 *	Objekte, bei denen das 'commit' nicht zum Speichern f�hrt,
 *	sondern nur zur einer Kennzeichnung 'soll gespeichert werden'
 *	und vom Parentobjekt mitgespeichert wird.
 *
 *	Beispiel : CalendarRelation
 *
 */
public interface VirtualEntity {

}
