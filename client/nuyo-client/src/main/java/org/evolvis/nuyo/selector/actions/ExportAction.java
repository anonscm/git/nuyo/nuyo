/* $Id: ExportAction.java,v 1.5 2007/08/30 16:10:31 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Jens Neumaier. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.selector.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.DialogAddressExporter;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.dataio.ExportFilePlugin;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;

/**
 * @author Jens Neumaier, tarent GmbH
 *
 */
public class ExportAction extends AbstractGUIAction implements AddressListConsumer {
    private static final long serialVersionUID = 4359886637617234859L;

    private static TarentLogger logger = new TarentLogger(ExportAction.class);
    
    private AddressListProvider provider;
    
    public void actionPerformed(ActionEvent e) {
        
        if (provider!=null) {
            
            DialogAddressExporter performer = retrieveDialogAddressExporter();
            
            if (performer!=null) {
                performer.setAddresses(provider.getAddresses());
                performer.execute();
            } 
            else 
                logger.warning(Messages.getString(this, "Load_Plugin_Failure"));
        }
    }

    public void registerDataProvider(AddressListProvider provider) {
        this.provider=provider;
    }
    
    public DialogAddressExporter retrieveDialogAddressExporter() {
        
        Plugin exportFilePlugin = PluginRegistry.getInstance().getPlugin(ExportFilePlugin.ID);
        DialogAddressExporter dialogPerformer = null;
        if (exportFilePlugin==null) {
            logger.warning("Plugin " + ExportFilePlugin.ID + " kann nicht geladen werden");
            return null;
        }
        
        if (exportFilePlugin.isTypeSupported(DialogAddressExporter.class)) {
            dialogPerformer = (DialogAddressExporter)exportFilePlugin.getImplementationFor(DialogAddressExporter.class);
        } 
        else {
            logger.info("Angeforderter Plugin-Typ "+ DialogAddressExporter.class.getName() + " wird nicht unterst�tzt.");
        }
        
        return dialogPerformer;
    }
}

