/* $Id: TarentWidgetMutableComponentTable.java,v 1.2 2007/01/11 18:56:41 robert Exp $
 * 
 * Created on 11.04.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * @author niko
 *
 * Eine Tabelle die Components enth�lt 
 * mit Unterst�tzung f�r dynamisch �nderbare (und f�r jede Zeile verschiedene) ComboBoxen...
 */
public class TarentWidgetMutableComponentTable extends JPanel implements TarentWidgetInterface 
{  
  private JScrollPane            m_oScrollPane;
  private JTable m_oTable;
  private Color m_oSelectionBackground;
  private Color m_oSelectionForeground;
  private ObjectTableModel m_oObjectTableModel = null;

  public TarentWidgetMutableComponentTable(ObjectTableModel tablemodel) 
  {
    super();
    
    m_oObjectTableModel = tablemodel;
    m_oTable = new JTable(tablemodel);
    
    m_oSelectionBackground = m_oTable.getSelectionBackground();
    m_oSelectionForeground = m_oTable.getSelectionForeground();

    m_oScrollPane = new JScrollPane(m_oTable);

    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER); 
  }

  public TarentWidgetMutableComponentTable(TableSorter sorter) 
  {
    super();
    
    m_oTable = new JTable(sorter);
    
    m_oSelectionBackground = m_oTable.getSelectionBackground();
    m_oSelectionForeground = m_oTable.getSelectionForeground();

    m_oScrollPane = new JScrollPane(m_oTable);

    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER); 
  }
  
  
  public TarentWidgetMutableComponentTable(int numcolumns, Dimension dimension) 
  {
    super();
    
    m_oTable = new JTable(new ObjectTableModel(numcolumns));
    if (dimension != null) m_oTable.setPreferredScrollableViewportSize(dimension);
    
    m_oScrollPane = new JScrollPane(m_oTable);

    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER); 
  }

  public TarentWidgetMutableComponentTable(int numcolumns) 
  {
    this(numcolumns, null);
  }
    
  public void setData(Object data) 
  {
    ((ObjectTableModel)(m_oTable.getModel())).removeAllRows();
    addData(data);
  }

  public void addData(Object data) 
  {
    if (data instanceof Object[])
    {  
      ((ObjectTableModel)(m_oTable.getModel())).addRow((Object[])data);
    }
    m_oTable.revalidate();
  }

  public void addData(Object data, Object tooltips) 
  {
    if ( (data instanceof Object[]) && (tooltips instanceof String[]))
    {  
      ((ObjectTableModel)(m_oTable.getModel())).addRow((Object[])data, (String[])tooltips);
    }
    m_oTable.revalidate();
  }

  public void removeData(Object data) 
  {
    if (data instanceof Integer)
    {
      ((ObjectTableModel)(m_oTable.getModel())).removeRow(((Integer)data).intValue());
    }
    m_oTable.revalidate();
  }

  public void removeAllData() 
  {
    ((ObjectTableModel)(m_oTable.getModel())).removeAllRows();
    m_oTable.revalidate();
  }

  public int getNumberOfEntries()
  {
    return ((ObjectTableModel)(m_oTable.getModel())).getRowCount();    
  }
  
  public Object getData() 
  {
    return(null);
  }

  public JComponent getComponent() 
  {
    return (this);
  }

  public void setLengthRestriction(int maxlen) 
  {
  }

  public void setWidgetEditable(boolean iseditable) 
  {
    m_oTable.setEnabled(iseditable);
  }

  public boolean isEqual(Object data) 
  {
    return false;
  }

  
  public void setColumnName(int column, String name)
  {        
    ((ObjectTableModel)m_oTable.getModel()).setColumnName(column, name);
  }
  
  public void setColumnEditable(int col, boolean iseditable)
  {        
    ((ObjectTableModel)m_oTable.getModel()).setColumnEditable(col, iseditable);
  }
  
  public Object getRowKey(int row)
  {
    return ((ObjectTableModel)m_oTable.getModel()).getRowKey(row);
  }

  public Object setRowKey(int row, Object key)
  {
    return ((ObjectTableModel)m_oTable.getModel()).setRowKey(row, key);
  }
  
  public void initColumnSizes(int samplerow) 
  {
    ObjectTableModel model = (ObjectTableModel)m_oTable.getModel();
    if (samplerow < model.getRowCount())
    {  
	    TableColumn column = null;
	    Component comp = null;
	    int headerWidth = 0;
	    int cellWidth = 0;
	    Object[] longValues = model.getRow(samplerow);
	    
	    TableCellRenderer headerRenderer = m_oTable.getTableHeader().getDefaultRenderer();
	
	    for (int i = 0; i < (m_oTable.getColumnModel().getColumnCount()); i++) 
	    {
	      column = m_oTable.getColumnModel().getColumn(i);
	
	      comp = headerRenderer.getTableCellRendererComponent(null, column.getHeaderValue(), false, false, 0, 0);
	      headerWidth = comp.getPreferredSize().width;
	
	      comp = m_oTable.getDefaultRenderer(model.getColumnClass(i)).getTableCellRendererComponent(m_oTable, longValues[i], false, false, 0, i);
	      cellWidth = comp.getPreferredSize().width;
	
	      //XXX: Before Swing 1.1 Beta 2, use setMinWidth instead.
	      column.setPreferredWidth(Math.max(headerWidth, cellWidth));
	    }
    } else logger.warning("Die angegebene Vergleichszeile existiert nicht. (Zeile " + samplerow + " von " + model.getRowCount() + ")");
  }
  

  public void setColumnSizes(int[] width) 
  {
    if ((width.length > 0) && (width.length <= m_oTable.getColumnModel().getColumnCount()))
      for (int i = 0; i < (m_oTable.getColumnModel().getColumnCount()); i++) 
      {
        TableColumn column = m_oTable.getColumnModel().getColumn(i);
        column.setPreferredWidth(width[i]);
        //column.setMaxWidth(width[i]);
        //column.setMinWidth(width[i]);
        //column.setWidth(width[i]);
      }
  }
  
  public void setFixedColumnSize(int col, int width) 
  {
    TableColumn column = m_oTable.getColumnModel().getColumn(col);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public void setColumnSize(int col, int width) 
  {
    TableColumn column = m_oTable.getColumnModel().getColumn(col);
    column.setPreferredWidth(width);
//    column.setMinWidth(width);
  }
  
  public void setColumnSize(int col, int min, int pref, int max){
    TableColumn column = m_oTable.getColumnModel().getColumn(col);
    column.setMinWidth(min);
    column.setPreferredWidth(pref);
    column.setMaxWidth(max);
  }
  
  
  public void setColumnSize(int width) 
  {
    for (int i = 0; i < (m_oTable.getColumnModel().getColumnCount()); i++) 
    {
      TableColumn column = m_oTable.getColumnModel().getColumn(i);
      column.setPreferredWidth(width);
    }
  }
  
  public Point getComponentLocation(Component comp)
  {
    String name = comp.getName();
    if (name != null)
    {  
      String[] coords = name.split(",");
      if (coords.length == 2)
      {
        try
        {
          int y = Integer.parseInt(coords[0]);
          int x = Integer.parseInt(coords[1]);
          return new Point(x,y);
        }
        catch(NumberFormatException nfe) {}
      }
    }
    return null;
  }

  
  public int getComponentRow(Component comp)
  {
    String name = comp.getName();
    if (name != null)
    {  
      String[] coords = name.split(",");
      if (coords.length == 2)
      {
        try
        {
          return Integer.parseInt(coords[0]);
        }
        catch(NumberFormatException nfe) {}
      }
    }
    return -1;
  }
  
  public int getComponentColumn(Component comp)
  {
    String name = comp.getName();
    if (name != null)
    {  
      String[] coords = name.split(",");
      if (coords.length == 2)
      {
        try
        {
          return Integer.parseInt(coords[1]);
        }
        catch(NumberFormatException nfe) {}
      }
    }
    return -1;
  }
  
  
  
  
  public JTable getJTable()
  {
    return m_oTable;
  }
  
  public void setColumnComponent(int column, Object stdcomp, Object editcomp)
  {
    TableCellRenderer renderer = null;
    TableCellEditor editor = null;
    
    if (stdcomp instanceof JButton)
    {
      renderer = new ButtonCellRenderer((JButton)stdcomp);
    }  
    else if (stdcomp instanceof ImageIcon)
    {
      renderer = new IconCellRenderer((ImageIcon)stdcomp);
    }
    else if (stdcomp instanceof JComboBox)
    {
      renderer = new ComboCellRenderer((JComboBox)stdcomp);
    }
    else if (stdcomp instanceof JTextField)
    {
      renderer = new TextFieldCellRenderer();
    }
    else if (stdcomp instanceof JCheckBox)
    {
      renderer = new CheckCellRenderer((JCheckBox)stdcomp);
    }
    else if (stdcomp instanceof JLabel)
    {
      renderer = new LabelCellRenderer((JLabel)stdcomp);
    }
    
    
    if (editcomp instanceof JButton)
    {
      editor = new ButtonCellEditor((JButton)editcomp);
    }  
    else if (editcomp instanceof ImageIcon)
    {
      editor = new IconCellEditor((ImageIcon)editcomp);
    }
    else if (editcomp instanceof JComboBox)
    {
      editor = new ComboCellEditor((JComboBox)editcomp);        
    }
    else if (editcomp instanceof JTextField)
    {      
      editor = new TextFieldCellEditor((JTextField)editcomp);
    }
    else if (editcomp instanceof JCheckBox)
    {
      editor = new CheckCellEditor((JCheckBox)editcomp);
    }
    else if (editcomp instanceof JLabel)
    {
      editor = new LabelCellEditor((JLabel)editcomp);
    }
    
    m_oTable.getColumnModel().getColumn(column).setCellEditor(editor);
    m_oTable.getColumnModel().getColumn(column).setCellRenderer(renderer);
  }
  
  
  // ------------------------------------------------------------------------------------------  
  // ------------------------------------------------------------------------------------------  

  private class ComboCellRenderer implements TableCellRenderer
  {
    private JComboBox m_oCombo;
    
    public ComboCellRenderer(JComboBox oCombo)
    {
      m_oCombo = oCombo;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      List list = m_oObjectTableModel.getItemListAt(row, column);
      if (list != null)
      {  
	      m_oCombo.removeAllItems();
	      Iterator listiterator = list.iterator();
	      while (listiterator.hasNext())
	      {
	        Object item = listiterator.next();
	        m_oCombo.addItem(item);
	      }      
      }      
      
      m_oCombo.setName(row + "," + column);      
      m_oCombo.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));

      return m_oCombo;
    }      
  }

  // --------------------------------------------------
  
  public class ComboCellEditorItem
  {
    private String m_sText;
    private Object m_oKey;
    
    public ComboCellEditorItem(String text, Object key)
    {
      m_sText = text;
      m_oKey = key;      
    }
    
    public Object getKey()
    {
      return m_oKey;
    }
    
    public String getText()
    {
      return m_sText;
    }
    
    public String toString()
    {
      return m_sText;
    }
  }
  
  
  private class ComboCellEditor extends DefaultCellEditor
  {
    private JComboBox m_oCombo;
    public ComboCellEditor(JComboBox comp)
    {
      super(comp);
      m_oCombo = comp;
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      List list = m_oObjectTableModel.getItemListAt(row, column);
      if (list != null)
      {  
	      m_oCombo.removeAllItems();
	      Iterator listiterator = list.iterator();
	      while (listiterator.hasNext())
	      {
	        Object item = listiterator.next();	        
            //System.out.println("adding item " + item);	        
	        if (item != null) m_oCombo.addItem(item);
	      }      
      }      
      
      m_oCombo.setName(row + "," + column);      
      m_oCombo.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));

      m_oCombo.setSelectedItem(value);
      return m_oCombo;
    }
    
    public Object getCellEditorValue()
    {
//System.out.println("m_oCombo.getSelectedItem()=" + m_oCombo.getSelectedItem());
      return m_oCombo.getSelectedItem();
    }
    
  }
  
  
  
  // ------------------------------------------------------------------------------------------  
  
  // ------------------------------------------------------------------------------------------  

  private class TextFieldCellRenderer extends DefaultTableCellRenderer
  {
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      JComponent comp = (JComponent)(super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column));
      comp.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));

      return comp;
    }      
  }

  // --------------------------------------------------
  
  private class TextFieldCellEditor extends DefaultCellEditor
  {
    public TextFieldCellEditor(JTextField comp)
    {
      super(comp);
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      JComponent comp = (JComponent)(super.getTableCellEditorComponent(table, value, isSelected, row, column));
      comp.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));

      return comp;
    }
  }
  
  
  // ------------------------------------------------------------------------------------------  
  
  private class ButtonCellRenderer implements TableCellRenderer
  {
    private JButton m_oButton;
    
    public ButtonCellRenderer(JButton button)
    {
      m_oButton = button;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      m_oButton.setText(value.toString());
      m_oButton.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      
      return m_oButton;
    }      
  }

  // ------------------------------------------------------------------------------------------  
  
  
  private class ButtonCellEditor implements TableCellEditor
  {
    private JButton m_oButton;
    
    public ButtonCellEditor(JButton button)
    {
      m_oButton = button;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      m_oButton.setText(value.toString());
      m_oButton.setName(row + "," + column);
      m_oButton.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      
      return m_oButton;
    }

    public void cancelCellEditing() {}

    public boolean stopCellEditing()
    {
      return true;
    }

    public Object getCellEditorValue()
    {
      return m_oButton.getText();
    }

    public boolean isCellEditable(EventObject anEvent)
    {
      return true;
    }

    public boolean shouldSelectCell(EventObject anEvent)
    {
      return true;
    }

    public void addCellEditorListener(CellEditorListener l) {}
    public void removeCellEditorListener(CellEditorListener l) {}
  }
  
  

  // ------------------------------------------------------------------------------------------  
  private class CheckCellRenderer implements TableCellRenderer
  {
    private JCheckBox m_oCheck;
    
    public CheckCellRenderer(JCheckBox check)
    {
      m_oCheck = check;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      m_oCheck.setSelected(((Boolean)value).booleanValue());
      m_oCheck.setName(row + "," + column);
      if (isSelected) 
      {
        m_oCheck.setBackground(m_oSelectionBackground);
        m_oCheck.setForeground(m_oSelectionForeground);
      }
      else 
      {
        m_oCheck.setBackground(Color.WHITE);
        m_oCheck.setForeground(Color.BLACK);
      }
      
      m_oCheck.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      
      return m_oCheck;
    }      
  }

  // ------------------------------------------------------------------------------------------  
  
  private class CheckCellEditor implements TableCellEditor
  {
    private JCheckBox m_oCheck;
    private int m_iRow = -1;
    private int m_iColumn = -1;
    
    public CheckCellEditor(JCheckBox check)
    {
      m_oCheck = check;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      m_oCheck.setSelected(((Boolean)value).booleanValue());
      m_iRow = row;
      m_iColumn = column;
      m_oCheck.setName(row + "," + column);
      if (isSelected) 
      {
        m_oCheck.setBackground(m_oSelectionBackground);
        m_oCheck.setForeground(m_oSelectionForeground);
      }
      else 
      {
        m_oCheck.setBackground(Color.WHITE);
        m_oCheck.setForeground(Color.BLACK);
      }
      m_oCheck.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      
      return m_oCheck;
    }

    public void cancelCellEditing() 
    {
      m_iRow = -1;
      m_iColumn = -1;      
    }

    public boolean stopCellEditing()
    {
      if ((m_iRow != -1) && (m_iColumn != -1))
      {
        if (m_oObjectTableModel.getRowCount() > m_iRow)
        {
          if (m_oObjectTableModel.getColumnCount() > m_iColumn)
          {              
            m_oObjectTableModel.setValueAt(new Boolean(m_oCheck.isSelected()), m_iRow, m_iColumn);
          }
        }
        m_iRow = -1;
        m_iColumn = -1;      
      }

      return true;
    }

    public Object getCellEditorValue()
    {
      return new Boolean(m_oCheck.isSelected());
    }

    public boolean isCellEditable(EventObject anEvent)
    {
      return true;
    }

    public boolean shouldSelectCell(EventObject anEvent)
    {
      return true;
    }

    public void addCellEditorListener(CellEditorListener l) {}
    public void removeCellEditorListener(CellEditorListener l) {}
  }
  
  
  // ------------------------------------------------------------------------------------------  
  
  // ------------------------------------------------------------------------------------------  
  // ------------------------------------------------------------------------------------------  
  
  
  
  
  private class IconCellRenderer implements TableCellRenderer
  {
    private ImageIcon m_oIcon;
    private JLabel m_oLabel;
    
    public IconCellRenderer(ImageIcon icon)
    {
      m_oIcon = icon;
      m_oLabel = new JLabel(m_oIcon);
      m_oLabel.setOpaque(true);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      if (value instanceof ImageIcon) m_oLabel.setIcon((ImageIcon)value);
      if (isSelected) 
      {
        m_oLabel.setBackground(m_oSelectionBackground);
        m_oLabel.setForeground(m_oSelectionForeground);
      }
      else 
      {
        m_oLabel.setBackground(Color.WHITE);
        m_oLabel.setForeground(Color.BLACK);
      }
      m_oLabel.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      return m_oLabel;
    }      
  }
  
  // ------------------------------------------------------------------------------------------  

  private class IconCellEditor implements TableCellEditor
  {
    private ImageIcon m_oIcon;
    private JLabel m_oLabel;

    public IconCellEditor(ImageIcon icon)
    {
      m_oIcon = icon;
      m_oLabel = new JLabel(m_oIcon);
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      if (value instanceof ImageIcon) m_oLabel.setIcon((ImageIcon)value);
      m_oLabel.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      
      return m_oLabel;
    }

    public void cancelCellEditing()
    {
    }

    public boolean stopCellEditing()
    {
      return false;
    }

    public Object getCellEditorValue()
    {
      return m_oLabel.getIcon();
    }

    public boolean isCellEditable(EventObject anEvent)
    {
      return false;
    }

    public boolean shouldSelectCell(EventObject anEvent)
    {
      return true;
    }

    public void addCellEditorListener(CellEditorListener l)
    {
    }

    public void removeCellEditorListener(CellEditorListener l)
    {
    }
    
  }
  
  
  
  // ------------------------------------------------------------------------------------------  

  private class LabelCellEditor implements TableCellEditor
  {
    private JLabel m_oLabel;

    public LabelCellEditor(JLabel label)
    {
      m_oLabel = label;
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      if (value instanceof ImageIcon)
      {  
        m_oLabel.setIcon((ImageIcon)value);
        m_oLabel.setText("");
      }
      if (value instanceof String)
      {  
        m_oLabel.setIcon(null);
        m_oLabel.setText((String)value);
      }
      if (value instanceof TarentWidgetIconComboBox.IconComboBoxEntry)
      {  
        m_oLabel.setIcon(((TarentWidgetIconComboBox.IconComboBoxEntry)value).getIcon());
        m_oLabel.setText(((TarentWidgetIconComboBox.IconComboBoxEntry)value).getText());
      }
      
      m_oLabel.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      
      return m_oLabel;
    }

    public void cancelCellEditing()
    {
    }

    public boolean stopCellEditing()
    {
      return false;
    }

    public Object getCellEditorValue()
    {
      return m_oLabel.getIcon();
    }

    public boolean isCellEditable(EventObject anEvent)
    {
      return false;
    }

    public boolean shouldSelectCell(EventObject anEvent)
    {
      return true;
    }

    public void addCellEditorListener(CellEditorListener l)
    {
    }

    public void removeCellEditorListener(CellEditorListener l)
    {
    }
    
  }
  
  // ------------------------------------------------------------------------------------------  

  private class LabelCellRenderer implements TableCellRenderer
  {
    private JLabel m_oLabel;
    
    public LabelCellRenderer(JLabel label)
    {
      m_oLabel = label;
      m_oLabel.setOpaque(true);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      if (value instanceof ImageIcon)
      {  
        m_oLabel.setIcon((ImageIcon)value);
        m_oLabel.setText("");
      }
      if (value instanceof String)
      {  
        m_oLabel.setIcon(null);
        m_oLabel.setText((String)value);
      }
      if (value instanceof TarentWidgetIconComboBox.IconComboBoxEntry)
      {  
        m_oLabel.setIcon(((TarentWidgetIconComboBox.IconComboBoxEntry)value).getIcon());
        m_oLabel.setText(((TarentWidgetIconComboBox.IconComboBoxEntry)value).getText());
      }
      
      if (isSelected) 
      {
        m_oLabel.setBackground(m_oSelectionBackground);
        m_oLabel.setForeground(m_oSelectionForeground);
      }
      else 
      {
        m_oLabel.setBackground(Color.WHITE);
        m_oLabel.setForeground(Color.BLACK);
      }
      m_oLabel.setToolTipText(m_oObjectTableModel.getTooltipAt(row, column));
      return m_oLabel;
    }      
  }
  
  // ------------------------------------------------------------------------------------------  

  
  
  
  // ------------------------------------------------------------------------------------------  
  // ------------------------------------------------------------------------------------------  
  
  
  
  public static class ObjectTableModel extends AbstractTableModel 
  {
    private int m_iNumColumns;      
    private String[] m_sColumnNames = null;
    private Boolean[] m_bIsEditable;
    private List m_oRows;
    private List m_oRowKeys;
    private List m_oUserRowKeys;
    private List m_oTooltipRows;
    private List m_oItemListRows;
    private List m_oEditableRows;
    
    public ObjectTableModel(int numcolumns)
    {
      m_iNumColumns = numcolumns;
      m_sColumnNames = new String[m_iNumColumns];
      m_bIsEditable = new Boolean[m_iNumColumns];
      m_oRows = new ArrayList();
      m_oRowKeys = new ArrayList();
      m_oUserRowKeys = new ArrayList();
      m_oTooltipRows = new ArrayList();
      m_oItemListRows = new ArrayList();
      m_oEditableRows = new ArrayList();
      
      for(int i=0; i<m_iNumColumns; i++)
      {
        m_sColumnNames[i] = "";
        m_bIsEditable[i] = new Boolean(false);          
      }
    }
    
    public void setColumnName(int column, String name)
    {        
      m_sColumnNames[column] = name;
    }
    
    private Boolean[] getDefaultEditableRow(int size)
    {
      Boolean[] row = new Boolean[size];
      for(int i=0; i<size; i++)
      {
        row[i] = new Boolean(m_bIsEditable[i].booleanValue());
      }
      return row;
    }
    
    public int addRow(Object[] columns)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(new Object());
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(new String[columns.length]);
      m_oItemListRows.add(new ArrayList[columns.length]);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }
    
    public int addRow(Object[] columns, Object rowkey)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(rowkey);
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(new String[columns.length]);
      m_oItemListRows.add(new ArrayList[columns.length]);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }

    public int addRow(Object[] columns, String[] tooltips)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(new Object());
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(tooltips);
      m_oItemListRows.add(null);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }
    
    public int addRow(Object[] columns, String[] tooltips, Object rowkey)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(rowkey);
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(tooltips);
      m_oItemListRows.add(null);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }
    
    public int addRow(Object[] columns, List[] itemlists)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(new Object());
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(new String[columns.length]);
      m_oItemListRows.add(itemlists);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }
    
    public int addRow(Object[] columns, Object rowkey, List[] itemlists)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(rowkey);
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(new String[columns.length]);
      m_oItemListRows.add(itemlists);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }

    public int addRow(Object[] columns, String[] tooltips, List[] itemlists)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(new Object());
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(tooltips);
      m_oItemListRows.add(itemlists);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }
    
    public int addRow(Object[] columns, String[] tooltips, Object rowkey, List[] itemlists)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(rowkey);
      m_oUserRowKeys.add(new Object());
      m_oTooltipRows.add(tooltips);
      m_oItemListRows.add(itemlists);
      m_oEditableRows.add(getDefaultEditableRow(columns.length));
      return m_oRows.size()-1;
    }
    
    public Object[] getRow(int row)
    {
      try
      {
        return (Object[])(m_oRows.get(row));
      }
      catch(IndexOutOfBoundsException iobe) { return null; }
    }

    public String[] getTooltipRow(int row)
    {
      try
      {
        return (String[])(m_oTooltipRows.get(row));
      }
      catch(IndexOutOfBoundsException iobe) { return null; }
    }

    public List[] getItemListRow(int row)
    {
      try
      {
        return (List[])(m_oItemListRows.get(row));
      }
      catch(IndexOutOfBoundsException iobe) { return null; }
    }
    
    public Boolean[] getEditableRow(int row)
    {
      try
      {
        return (Boolean[])(m_oEditableRows.get(row));
      }
      catch(IndexOutOfBoundsException iobe) { return null; }
    }
    
    public Object getRowKey(int row)
    {
      try
      {
        return m_oRowKeys.get(row);
      }
      catch(IndexOutOfBoundsException iobe) { return null; }
    }

    public Object getUserRowKey(int row)
    {
      try
      {
        return m_oUserRowKeys.get(row);
      }
      catch(IndexOutOfBoundsException iobe) { return null; }
    }

    public int getRowWithKey(Object object)
    {
      for(int i=0; i<(m_oRowKeys.size()); i++)
      {
        if (object.equals(m_oRowKeys.get(i))) return i;
      }
      return -1;
    }
    
    public int getRowWithUserKey(Object object)
    {
      for(int i=0; i<(m_oUserRowKeys.size()); i++)
      {
        if (object.equals(m_oUserRowKeys.get(i))) return i;
      }
      return -1;
    }
    
    
    public Object setRowKey(int row, Object key)
    {
      return m_oRowKeys.set(row, key);
    }

    public Object setUserRowKey(int row, Object key)
    {
      return m_oUserRowKeys.set(row, key);
    }

    public void removeRow(int row)
    {
      m_oRows.remove(row);
      m_oRowKeys.remove(row);
      m_oUserRowKeys.remove(row);
      m_oTooltipRows.remove(row);
      m_oItemListRows.remove(row);
      m_oEditableRows.remove(row);
    }

    public void removeAllRows()
    {
      m_oRows.clear();
      m_oRowKeys.clear();
      m_oUserRowKeys.clear();
      m_oTooltipRows.clear();
      m_oItemListRows.clear();
      m_oEditableRows.clear();
    }

    public int getColumnCount() 
    {
      return m_iNumColumns;
    }

    public int getRowCount() 
    {
      return m_oRows.size();
    }

    public String getColumnName(int col) 
    {
      return m_sColumnNames[col];
    }

    public Object getValueAt(int row, int col) 
    {
      Object[] rowdata = getRow(row);
      return rowdata[col];
    }

    public String getTooltipAt(int row, int col) 
    {
      String[] rowdata = getTooltipRow(row);
      String tooltip = rowdata[col];
      return tooltip;
    }

    public List getItemListAt(int row, int col) 
    {
      List[] rowdata = getItemListRow(row);
      List list = rowdata[col];
      return list;
    }
    
    public Class getColumnClass(int c) 
    {
      try 
      {
        return getValueAt(0, c).getClass();
      } 
      catch(NullPointerException npe)
      {
        return String.class;  
      }
    }

    public boolean isCellEditable(int row, int col) 
    {
      Boolean[] rowdata = getEditableRow(row);
      return rowdata[col].booleanValue();
    }

    public void setColumnEditable(int col, boolean iseditable)
    {
      m_bIsEditable[col] = new Boolean(iseditable);
    }
    
    public void setCellEditable(int row, int col, boolean iseditable)
    {      
      Boolean[] rowdata = getEditableRow(row);
      rowdata[col] = new Boolean(iseditable);
      fireTableCellUpdated(row, col);
    }
    
    public void setValueAt(Object value, int row, int col) 
    {
      try
      {
        Object[] rowdata = getRow(row);
        rowdata[col] = value;
        fireTableCellUpdated(row, col);
      }
      catch(IndexOutOfBoundsException iobe)
      {        
      }
    }

    public void setItemListAt(List items, int row, int col) 
    {
      Object[] rowdata = getItemListRow(row);
      rowdata[col] = items;
    }
    
    public void setTooltipAt(String value, int row, int col) 
    {
      String[] rowdata = getTooltipRow(row);
      rowdata[col] = value;
      fireTableCellUpdated(row, col);
    }
  }
  
  private static Logger logger = Logger.getLogger(TarentWidgetMutableComponentTable.class.getName());

  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  

}

  