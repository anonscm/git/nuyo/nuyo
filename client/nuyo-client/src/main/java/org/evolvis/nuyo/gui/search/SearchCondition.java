package org.evolvis.nuyo.gui.search;

import org.evolvis.nuyo.groupware.AddressProperty;


/**
 * Is a wrapper class for the filter conditions.<p>
 * A single filter condition will be represented by a property name, a filter operator and some parameter.
 * <p>
 * It will be used by {@link org.evolvis.nuyo.gui.search.FilterModel} and {@link org.evolvis.nuyo.gui.search.FilterModelListener} 
 * in order to keep the filter definition synchronized with user input.   
 * 
 * @see org.evolvis.nuyo.gui.search.FilterModel#addCondition(SearchCondition)
 * @see org.evolvis.nuyo.gui.search.FilterModel#removeCondition(SearchCondition)
 * @see org.evolvis.nuyo.gui.search.FilterModelListener#inserted(int, SearchCondition)
 * @see org.evolvis.nuyo.gui.search.FilterModelListener#appended(SearchCondition)
 * @see org.evolvis.nuyo.gui.search.FilterModelListener#removed(int, SearchCondition)
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
class SearchCondition {
    
    private boolean isRemovable;
    private AddressProperty attribute;
    private LabeledFilterOperator operator;
    private String paramText;

    /**
     * Creates an instance.
     * @param anAttribute represents a property for a given operator.
     * @param anOperator that should be computed on given operands.
     * @param aParamText is an argument for a given operator. 
     */
    public SearchCondition( AddressProperty anAttribute, LabeledFilterOperator anOperator, String aParamText ) {
        setAttribute(anAttribute);
        setOperator(anOperator);
        setParamText(aParamText);
        isRemovable = true;
    }

    /** 
     * Says whether this condition has parameter value or not.<p>
     *   
     * @return 'true' if empty parameter
     */
    public boolean isEmpty(){
        return paramText == null || paramText.equals("");
    }

    /** 
     * Says whether this condition is allowed to be removed.<p>
     *   
     * @return 'false' if condition is not allowed to be removed (i.e. the last condition visually represented).
     */
    public boolean isRemovable(){
        return isRemovable;
    }
    
    /** 
     * Sets permission to remove this condition.<p>
     *  
     * @param value 'true'/'false' to set/unset a permission.
     */
    public void setRemovable(boolean value){
        isRemovable = value;
    }
    
    /** 
     * Returns a property wrapper (with encapsulated name). 
     * @return LabeledProperty
     */
    public AddressProperty getAttribute(){
        return attribute;
    }

    /**
     * Sets a property name. 
     * @param newAttribute a wrapper with the encapsulated property name object.
     */
    public void setAttribute( AddressProperty newAttribute ) {
        if(newAttribute == null) throw new NullPointerException("filter attribute must not be null");
        attribute = newAttribute;
    }
    
    /**
     * Returns an operator wrapper.
     * @return LabeledFilterOperator
     */
    public LabeledFilterOperator getOperator(){
        return operator;
    }

    /**
     * Sets an operator.
     * @param anOperator a wrapper with encapsulated operator object
     */
    public void setOperator( LabeledFilterOperator anOperator ) {
        if(anOperator == null) throw new NullPointerException("filter operator must not be null");
        operator = anOperator;
    }
    
    /**
     * Returns the current parameter text.
     * @return String
     */
    public String getParamText(){
        return paramText;
    }
    
    /**
     * Sets the current parameter text.
     * @param newText
     */
    public void setParamText( String newText ) {
        if( newText == null ) throw new NullPointerException("filter param text must not be null");
        paramText = newText;
    }

    /**
     * Creates and returns a copy of a given condition.
     * @param condition to copy
     * @return a created copy
     */
    public static SearchCondition copy( SearchCondition condition ) {
        return new SearchCondition(condition.getAttribute(), condition.getOperator(), condition.getParamText());
    }
    
    public boolean isCopyOf(SearchCondition condition){
        return getAttribute().equals(condition.getAttribute())
        && getOperator().equals(condition.getOperator())
        && getParamText().equals(condition.getParamText());
    }
}
