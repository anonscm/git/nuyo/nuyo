package org.evolvis.nuyo.gui;

import java.io.File;
import java.text.MessageFormat;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.manager.CommonDialogServices;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.swing.utils.SwingIconFactory;

public abstract class BaseFrame implements CommonDialogServices {

	private static final TarentLogger	log			= new TarentLogger(BaseFrame.class);
	
	private JFrame						frame;

	private String						currentDirectory;
	private FileFilter					lastSelectedFileFilter;

	private ImageIcon					iconError;
	private ImageIcon					iconInfo;
	private ImageIcon					iconQuestion;

	public BaseFrame() {

		frame = new JFrame();

		iconError = (ImageIcon)ApplicationServices.getInstance().getIconFactory().getIcon(SwingIconFactory.ERROR);
		iconInfo = (ImageIcon)ApplicationServices.getInstance().getIconFactory().getIcon(SwingIconFactory.INFO);
		iconQuestion = (ImageIcon)ApplicationServices.getInstance().getIconFactory().getIcon(SwingIconFactory.QUESTION);

	}


	// ----------- askUser -------------------------------------

	public int askUser(String caption, String question, String[] answers, int defaultValue) {
		return askUser(frame, caption, question, answers, null, defaultValue);
	}


	public int askUser(String question, String[] answers, int defaultValue) {
		return askUser(frame, question, answers, null, defaultValue);
	}


	public int askUser(String caption, String question, String[] answers, String[] tooltips, int defaultValue) {
		return askUser(frame, caption, question, answers, tooltips, defaultValue);
	}


	public int askUser(String question, String[] answers, String[] tooltips, int defaultValue) {

		return askUser(frame, question, answers, tooltips, defaultValue);
	}


	public int askUser(JFrame parent, String question, String[] answers, String[] tooltips, int defaultValue) {
		
		return askUser(parent, Messages.getString("GUI_MainFrameNewStyle_Nachfrage_tarent-contact"), question, answers, tooltips, defaultValue);
	}


	public int askUser(JFrame parent, String caption, String question, String[] answers, String[] tooltips, int defaultValue) {

		return JOptionPane.showOptionDialog(parent, question, caption, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, answers, answers[defaultValue]);
	}


	// ---------------- publishError --------------------------

	public void publishError(String msg) {
		publishError(msg, (Throwable) null);
	}


	public void publishError(String msg, Object[] params) {
		publishError(msg, params, null);
	}


	public void publishError(String caption, String msg) {
		publishError(caption, msg, (Throwable) null);
	}


	public void publishError(String caption, String msg, Object[] params) {
		publishError(caption, msg, params, null);
	}


	public void publishError(String msg, Throwable e) {
		publishError(Messages.getString("AM_Error_Default_Caption"), msg, e);
	}


	public void publishError(String msg, Object[] params, Throwable e) {
		publishError(Messages.getString("AM_Error_Default_Caption"), msg, params, e);
	}


	public void publishError(String caption, String msg, Object[] params, Throwable e) {
		publishError(caption, MessageFormat.format(msg, params), e);
	}


	public void publishError(String caption, String msg, Throwable e) {
		log.log(Level.WARNING, msg, e);

		String extendedtext = null;
		// Aller tiefste Exception als Cause Message verwenden.
		if (e != null) {
			Throwable cause = e;
			while (cause.getCause() != null)
				cause = cause.getCause();
			extendedtext = cause.getMessage();
		}
		// showExtendedMessage(msg, extendedtext);
		publishError(caption, msg, extendedtext);
	}


	public void publishError(String caption, String msg, String extendedText) {

		showExtendedMessage(msg, extendedText);
	}

	// ------------------------ requestFileName --------------------------

	class ExtensionFilter extends FileFilter {

		String[]	extensions;
		String		description;


		ExtensionFilter(String[] extensionsp, String descriptionp) {
			extensions = extensionsp;
			description = descriptionp;
		}


		// Accept all directories and all applicable files.
		public boolean accept(File f) {
			int n;

			if (f.isDirectory()) {
				return true;
			}

			String extension = getExtension(f);
			if (extension != null) {
				for (n = 0; n < (extensions.length); n++)
					if (extension.equalsIgnoreCase(extensions[n]))
						return true;
			}
			return false;
		}


		// The description of this filter
		public String getDescription() {
			return description;
		}


		public String[] getExtensions() {
			return extensions;
		}
	}


	public String getExtension(File f) {
		
		String ext = null;
		
		String s = f.getName();
		
		int i = s.lastIndexOf('.');

		if (i > 0 && i < s.length() - 1) {
		
			ext = s.substring(i + 1).toLowerCase();
		}
		
		return ext;
	}


	/**
	 * Method requestFilename. �ffnet einen Dateiauswahldialog
	 * 
	 * @param startfolder (String: das Verzeichnis welches nach dem �ffnen angezeigt wird)
	 * @param extensions Liste von erlaubten Dateiendungen zu einem Filter
	 * @param extensiondescriptions Liste mit erkl�rende Texten zu den Filtern
	 * @return String (die ausgew�hlte Datei)
	 */

	public String requestFilename(String startfolder, String presetfile, String[] extensions, String extensiondescription, boolean forSave) {
		
		String[][] extensionsList = 
			extensions == null ? null : new String[][] { extensions };
		
		String[] extensiondescriptionList = 
			extensiondescription == null ? null : new String[] { extensiondescription };
		
		return requestFilename(startfolder, presetfile, extensionsList, extensiondescriptionList, forSave, true);
	}


	public String requestFilename(String startfolder, String presetfile, String[][] extensions, String[] extensiondescriptions, boolean forSave, boolean useAcceptAllFilter) {
		JFileChooser chooser;

		if (startfolder == null)
			startfolder = currentDirectory;

		chooser = new JFileChooser(startfolder);
		if (presetfile != null)
			chooser.setSelectedFile(new File(presetfile));

		if (extensions != null) {
			for (int i = 0; i < extensions.length; i++)
				chooser.addChoosableFileFilter(new ExtensionFilter(extensions[i], extensiondescriptions[i]));

		}
		
		chooser.setAcceptAllFileFilterUsed(useAcceptAllFilter);

		if (extensions != null && extensions.length > 0)
			chooser.setFileFilter(chooser.getChoosableFileFilters()[0]);

		int retval;

		if (forSave) {
			retval = chooser.showSaveDialog(frame);
		} else {
			retval = chooser.showOpenDialog(frame);
		}
		currentDirectory = chooser.getCurrentDirectory().getPath();
		lastSelectedFileFilter = (FileFilter) chooser.getFileFilter();
		if (retval == JFileChooser.APPROVE_OPTION) {
			File file;
			file = chooser.getSelectedFile();

			return (file.getPath());
		}

		return (null);
	}


	public String[] getLastChoosedFileExtensions() {
		
		if (lastSelectedFileFilter == null)
			return null;
		
		return ((ExtensionFilter) lastSelectedFileFilter).getExtensions();
	}


	// ---------- setWaiting --------------

	public abstract void setWaiting(boolean isWaiting);


	// ---------------------- showInfo -----------------------------

	public void showInfo(String caption, String text) {
		showInfo(frame, caption, text);

	}


	public void showInfo(String text) {
		showInfo(frame, text);

	}


	public void showInfo(JFrame comp, String message) {
		showInfo(comp, null, message);
	}


	public void showInfo(JFrame comp, String caption, String message) {
		if(caption == null)
			JOptionPane.showMessageDialog(comp, message);
		else
			JOptionPane.showMessageDialog(comp, message, caption, JOptionPane.INFORMATION_MESSAGE);
	}


	// ------------------------- showMessage -------------------------------

	/**
	 * Method showMessage. �ffnet einen Dialog um eine Fehlermeldung anzuzeigen. Wartet bis der Benutzer den "Ok"-Knopf gedr�ckt hat.
	 * 
	 * @param message Der Fehlermeldungstext
	 */
	public void showMessage(String message) {
		showMessage(frame, message);
	}


	public void showMessage(String caption, String message) {
		showMessage(frame, message);
	}


	public void showMessage(JFrame comp, String message) {
		showMessage(comp, Messages.getString("GUI_Caption_Error"), message);
	}


	public void showMessage(JFrame comp, String caption, String message) {
		//optionPane.showMessageDialog(comp, message, caption, iconError, "OK", "diesen Dialog schliessen");
		JOptionPane.showMessageDialog(comp, message, caption, JOptionPane.ERROR_MESSAGE);
	}


	// -----------------------------showExtendedMessage ------------------------------------------

	public void showExtendedMessage(String message, String extendedtext) {
		showExtendedMessage(frame, message, extendedtext);
	}


	public void showExtendedMessage(String caption, String message, String extendedtext) {
		showExtendedMessage(frame, message, extendedtext);
	}


	public void showExtendedMessage(JFrame comp, String message, String extendedtext) {
		showExtendedMessage(comp, Messages.getString("GUI_MainFrameNewStyle_Error_tarent-contact"), message, extendedtext);
	}


	public void showExtendedMessage(JFrame comp, String caption, String message, String extended) {
		showExtendedMessage(comp, caption, message, extended, iconError);
	}


	public void showExtendedMessage(JFrame comp, String caption, String message, String extended, ImageIcon icon) {
		if(extended != null)
			JOptionPane.showMessageDialog(comp, message+"\r\n\n"+extended, caption, JOptionPane.ERROR_MESSAGE, icon);
		else
			JOptionPane.showMessageDialog(comp, message, caption, JOptionPane.ERROR_MESSAGE, icon);
	}


	// ------------------ getter and setter ------------------------
	public JFrame getFrame() {

		return frame;
	}
}
