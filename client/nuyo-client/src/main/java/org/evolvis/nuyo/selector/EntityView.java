package org.evolvis.nuyo.selector;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.AbstractDocument.LeafElement;
import javax.swing.text.html.HTML;

import org.evolvis.nuyo.controller.ApplicationServices;

import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.ui.JHTMLEntityForm;

/**
 * This class generates a html View of an EntityList. The data is stored in an extension of a CompoundModel, EntityListModel.
 * That makes it easy to manage a list of entities and a current entity with the same data structure/Model.
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class EntityView implements HyperlinkListener{

	private static Logger logger = Logger.getLogger(EntityView.class.getName());
	private String template="";
	private EntityListModel model;
	private BindingManager bindingManager;
	private JHTMLEntityForm entityForm;
	public static final String ACTION_NEXT = "next";
	public static final String ACTION_PREVIOUS = "prev";

	public EntityView(String htmlURL, EntityList entities){
		template = retrieveTemplate(htmlURL);
		model=constructEntityListModel(entities);
		bindingManager = new BindingManager();
		bindingManager.setModel(model);
	}
	private String retrieveTemplate(String templateURL){
		String temp = templateURL;
		if (-1 == temp.indexOf("://"))
			try {
				temp = new File(templateURL).toURL().toString();
				return temp;
			} catch (MalformedURLException e) {
				ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Laden des Templates", "Fehler beim Laden von " + temp);
			}
			return null;	
	}
	private JHTMLEntityForm buildPanel(){
		if (template!=null){
			JTextArea area = new JTextArea();
			Map cmap = new HashMap();
			cmap.put("area", JTextArea.class);
			JHTMLEntityForm form = new JHTMLEntityForm(template,cmap,bindingManager);
			form.getJEditorPane().addHyperlinkListener(this);
			return form;
		} else{
			logger.info("Kein Template vorhanden, kann Anzeige nicht erstellen");
			return null;
		}
	}

	public JPanel getComponent(){
		if (entityForm==null) entityForm = buildPanel();
		return entityForm;
	}

	private EntityListModel constructEntityListModel(EntityList entities){
		EntityListModel model = new EntityListModel();
		if (entities.getSize()>0){
			model.registerObject(EntityListModel.CURRENT_ENTITY, entities.getEntityAt(0));
		}
		model.registerEntityList(EntityListModel.ENTITY_LIST, entities);
		return model;
	}

	public void hyperlinkUpdate(HyperlinkEvent e) {
		if (e.getEventType()==EventType.ACTIVATED){
			String url = (String)((((SimpleAttributeSet)((LeafElement)e.getSourceElement()).getAttribute(HTML.Tag.A))).getAttribute(HTML.Attribute.HREF));
			if (url.equals(ACTION_NEXT)){
				model.nextEntity();
			} else if (url.equals(ACTION_PREVIOUS)){
				model.previousEntity();
			}
		} 
		// Handcursor sollte das werden, hat aber nicht geklappt bisher!
		/*else if (e.getEventType()==EventType.ENTERED){
			ApplicationServices.getInstance().getCommonDialogServices().getFrame().getContentPane().setCursor(new Cursor(Cursor.HAND_CURSOR));
		} else if (e.getEventType()==EventType.EXITED){
			ApplicationServices.getInstance().getCommonDialogServices().getFrame().getContentPane().setCursor(Cursor.getDefaultCursor());
		}*/
	}
	public void load(String html){
		entityForm.load(html);
	}
}
