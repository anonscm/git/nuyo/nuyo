/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorage;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.Events.TarentGUISourceChangedEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueAcceptEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueChangedEvent;
import org.evolvis.nuyo.datacontainer.FieldDescriptors.FieldDescriptor;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.nuyo.util.general.DataAccess;



/**
 * @author niko
 */
public class GenericDataContainer implements DataContainer, TarentGUIEventListener
{
  private String m_sKey;
  private GUIElement m_oGUIElement;
  private List m_oValidators;
  private List m_oListeners;
  private DataSource m_oDataSource;
  private FieldDescriptor m_oFieldDescriptor;
  private DataStorage m_oDataStorage;
  private Map m_oEventListener;
  private boolean m_bIsClone = false;
  private GenericDataContainer m_oParent = null;
  //private GUIListener m_oGUIListener = null;
  private DataContainerPool m_oDataContainerPool;
  
  public GenericDataContainer()
  {
    m_sKey = null;
    m_oGUIElement = null;
    m_oValidators = new ArrayList();
    m_oListeners = new ArrayList();
    m_oDataSource = null;
    m_oFieldDescriptor = null;
    m_oDataStorage = null;
    m_oEventListener = new HashMap();
  }

  
  public void setDataContainerPool(DataContainerPool pool)
  {
    m_oDataContainerPool = pool;
  }
  
  public DataContainerPool getDataContainerPool()
  {
    return m_oDataContainerPool;
  }

  private DataAccess m_oDataAccess;
  public void setDataAccess(DataAccess access)
  {
    m_oDataAccess = access;
  }
  
  public DataAccess getDataAccess()
  {
    return m_oDataAccess;
  }

  
  private Logger m_oLogger;
  public void setLogger(Logger logger)
  {
    m_oLogger = logger;
  }
  
  public Logger getLogger()
  {
    return m_oLogger;
  }

  public String getListenerName()
  {
    return "GenericDataContainer";
  }  

  public boolean isClone()
  {
    return m_bIsClone;
  }
  
  public DataContainer cloneDataContainer()
  {
    GenericDataContainer container = new GenericDataContainer();
    container.setLogger(m_oLogger);
    container.setDataAccess(m_oDataAccess);
    container.m_bIsClone = true;  
    container.m_oParent = this; 
    container.m_oEventListener = m_oEventListener;    
    container.m_sKey = m_sKey;
    container.setDataSource(m_oDataSource);
    container.setDataStorage(m_oDataStorage);
    container.setDataMap(m_oDataMap);
    
    FieldDescriptor clonedfielddescriptor = m_oFieldDescriptor.cloneFieldDescriptor();
    container.setFieldDescriptor(null);
    container.setFieldDescriptor(clonedfielddescriptor);
    
    GUIElement clonedguielement = m_oGUIElement.cloneGUIElement();
    clonedguielement.setDataContainer(container);
    container.setGUIElement(null);
    container.setGUIElement(clonedguielement);
    
    container.m_oFieldDescriptor.setDataContainer(container);
    container.m_oFieldDescriptor.init();
    
    container.m_oGUIElement.setDataContainer(container);
    container.m_oGUIElement.init();    
    container.m_oGUIElement.initElement();    
    
    container.m_oDataStorage.init();      
    container.m_oDataSource.init();
    
    Iterator vit = m_oValidators.iterator();
    while(vit.hasNext())
    {
      Validator validator = (Validator)(vit.next());
      Validator clonedvalidator = validator.cloneValidator();
      container.addValidator(clonedvalidator); 
      clonedvalidator.setDataContainer(container);
      clonedvalidator.getDataContainer().addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALIDATE, clonedvalidator);
      clonedvalidator.init();
    }
        
    Iterator lit = m_oListeners.iterator();
    while(lit.hasNext())
    {
      DataContainerListener listener = (DataContainerListener)(lit.next());
      DataContainerListener clonedlistener = listener.cloneListener();
      clonedlistener.setDataContainer(container);
      container.addListener(clonedlistener); 
      clonedlistener.init();
    }

    container.addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_FOCUSLOST, container);    
    return container;
  }
  
  public void setKey(String key)
  {
    m_sKey = key;
  }
  
  public boolean canStore(Class data)
  {
    if (m_oDataStorage != null) return(m_oDataStorage.canStore(data));
    return(false);
  }
  
  public Class stores()
  {
    if (m_oDataStorage != null) return(m_oDataStorage.stores());
    return(null);    
  }
  
  public void getData(PluginData data)
  {
    //TODO: diese Zeile wg. BUG eingef�gt!
    fireTarentGUIEvent(new TarentGUIValueAcceptEvent(this, getGUIData()));
    
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_STORE, this, null, data));
  }
  
  private PluginData m_oCurrentPluginData = null;
  public void setData(PluginData data)
  {
    if (!(data.equals(m_oCurrentPluginData)))
    {
      fireTarentGUIEvent(new TarentGUISourceChangedEvent(this, data));
      m_oCurrentPluginData = data;
    }

    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FETCH, this, null));
  }

  public void setGUIData(Object data)
  {
    if (m_oGUIElement != null) m_oGUIElement.setData(data);    
  }

  public Object getGUIData()
  {
    if (m_oGUIElement != null) return(m_oGUIElement.getData());
    return null;    
  }

  
  public void setGUIElement(GUIElement ge)
  {
    if (ge != null)
    {
      m_oGUIElement = ge;
      m_oGUIElement.setDataContainer(this);
    }
    else
    {
      if (m_oGUIElement != null)
      {
        m_oGUIElement.dispose();
        m_oGUIElement.setDataContainer(null);
      }
      m_oGUIElement = null;
    }
  }


  public void addValidator(Validator v)
  {
    if (v != null)
    {
      m_oValidators.add(v);
      v.setDataContainer(this);
    } 
  }

  public void removeValidator(Validator v)
  {
    if (v != null)
    {
      m_oValidators.remove(v);
      v.dispose();
      v.setDataContainer(null);     
    } 
  }

  public void setDataSource(DataSource io)
  {
    if (io != null)
    {
      m_oDataSource = io;
      m_oDataSource.setDataContainer(this);      
    }
    else 
    {
      if (m_oDataSource != null)
      {
        m_oDataSource.dispose();
        m_oDataSource.setDataContainer(null);
        m_oDataSource = null;
      }
    } 
  }

  
  public void setFieldDescriptor(FieldDescriptor fd)
  {
    if (fd != null)
    { 
      m_oFieldDescriptor = fd;
      m_oFieldDescriptor.setDataContainer(this);
    }    
    else 
    {
      if (m_oFieldDescriptor != null)
      {
        m_oFieldDescriptor.dispose();
        m_oFieldDescriptor.setDataContainer(null);
        m_oFieldDescriptor = null;
      }
    } 
  }

  public void setDataStorage(DataStorage ds)
  {
    if (ds != null)
    { 
      m_oDataStorage = ds;
      m_oDataStorage.setDataContainer(this);
    }        
    else 
    {
      if (m_oDataStorage != null)
      {
        m_oDataStorage.dispose();
        m_oDataStorage.setDataContainer(null);
        m_oDataStorage = null;
      }
    } 
  }


  public DataStorage getDataStorage()
  {  
    return m_oDataStorage;
  }

  public FieldDescriptor getFieldDescriptor()
  {
    return m_oFieldDescriptor;
  }

  public DataSource getDataSource()
  {
    return m_oDataSource;
  }

  public List getValidators()
  {
    return m_oValidators;
  }

  public GUIElement getGUIElement()
  {
    return m_oGUIElement;    
  }

  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {    
    if (isClone())
    {
      m_oParent.addTarentGUIEventListener(event, handler);
    }
    else
    {
      ArrayList list = (ArrayList)(m_oEventListener.get(event));
      if (list == null)
      {
        list = new ArrayList();
        m_oEventListener.put(event, list);
      }
      if (!(list.contains(handler))) 
      {
        list.add(handler);
      }
    }
  }

  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    if (isClone())
    {
      ArrayList list = (ArrayList)(m_oEventListener.get(event));
      if (list != null)
      {
        list.remove(handler);
        if (list.size() == 0) m_oEventListener.remove(event);
      }
    }
  }

  public void addErrorEventListener()
  {
    
  }
  
  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    if (isClone())
    {
      e.setSourceContainer(this);
      m_oParent.fireTarentGUIEvent(e);
    }
    else
    {
      ArrayList list = (ArrayList)(m_oEventListener.get(e.getName()));
      if (list != null)
      {
        for(int i=0; i<list.size(); i++)
        {
          TarentGUIEventListener el = (TarentGUIEventListener)(list.get(i));
          el.event(e);
        }
      }
    }
  }
  
  public List getEventsConsumable()
  {
    ArrayList list = new ArrayList();
    for(int i=0; i<(m_oValidators.size()); i++)
    {
      Validator v = (Validator)(m_oValidators.get(i));
      if ((v != null) && v.getEventsConsumable() != null) list.addAll(v.getEventsConsumable());
    }
    if ((m_oDataSource != null) && m_oDataSource.getEventsConsumable() != null) list.addAll(m_oDataSource.getEventsConsumable());
    if ((m_oFieldDescriptor != null) && m_oFieldDescriptor.getEventsConsumable() != null) list.addAll(m_oFieldDescriptor.getEventsConsumable());
    if ((m_oDataStorage != null) && m_oDataStorage.getEventsConsumable() != null) list.addAll(m_oDataStorage.getEventsConsumable());
    if ((m_oGUIElement != null) && m_oGUIElement.getEventsConsumable() != null) list.addAll(m_oGUIElement.getEventsConsumable());
    return list;
  }
    
  public List getEventsFireable()
  {
    ArrayList list = new ArrayList();
    for(int i=0; i<(m_oValidators.size()); i++)
    {
      Validator v = (Validator)(m_oValidators.get(i));
      if ((v != null) && v.getEventsFireable() != null) list.addAll(v.getEventsFireable());
    }

    if ((m_oDataSource != null) && m_oDataSource.getEventsFireable() != null) list.addAll(m_oDataSource.getEventsFireable());
    if ((m_oFieldDescriptor != null) && m_oFieldDescriptor.getEventsFireable() != null) list.addAll(m_oFieldDescriptor.getEventsFireable());
    if ((m_oDataStorage != null) && m_oDataStorage.getEventsFireable() != null) list.addAll(m_oDataStorage.getEventsFireable());
    if ((m_oGUIElement != null) && m_oGUIElement.getEventsFireable() != null) list.addAll(m_oGUIElement.getEventsFireable());
    return list;
  }

  public String getKey()
  {    
    return m_sKey;
  }

  public boolean isDirty(PluginData data)
  {
    Object storage = data.get(m_oDataSource.getDBKey());
    Object guidata = m_oDataStorage.getData();
    
    if      ((storage == null) && (guidata == null)) return false;
    else if ((storage != null) && (guidata == null)) return true;
    else if ((storage == null) && (guidata != null)) return true;
    else return !(storage.equals(guidata));    
  }


  public void validate()
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_VALIDATORSTART, this, null));          
    
    fireTarentGUIEvent(new TarentGUIValueChangedEvent(this, getGUIData()));
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_VALIDATE, this, null));

    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_VALIDATOREND, this, null));
    
    if (getValidators().size() == 0) 
    {
      fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_ACCEPT, this, null));
    }
    
  }

  
  
  public void event(TarentGUIEvent tge)
  {
  }
  
  public boolean isValid()
  {
    return getDataStorage().isValid();
  }
  
  
  private int m_iNumErrors = 0;
  public int getNumberOfErrors()
  {
    return m_iNumErrors;
  }
  
  public void setNumberOfErrors(int num)
  {
    m_iNumErrors = num;
  }


  public boolean isEventFireable(TarentGUIEvent e)
  {
    List list = getEventsFireable();
    Iterator it = list.iterator();
    while(it.hasNext())
    {      
      String eventname = (String)it.next();
      if (eventname.equals(e.getName())) return true;
    }
    return false;
  }


  public boolean isEventConsumable(TarentGUIEvent e)
  {
    List list = getEventsConsumable();
    Iterator it = list.iterator();
    while(it.hasNext())
    {      
      String eventname = (String)it.next();
      if (eventname.equals(e.getName())) return true;
    }
    return false;
  }

  
  // --------------------------------------  
  
  public void addListener(DataContainerListener l)
  {
    if (l != null)
    {
      m_oListeners.add(l);
      l.setDataContainer(this);
    } 
  }

  public void removeListener(DataContainerListener l)
  {
    if (l != null)
    {
      m_oListeners.remove(l);
      l.dispose();
      l.setDataContainer(null);     
    } 
  }

  public List getListeners()
  {
    return m_oListeners;
  }
  
  
  private Map m_oDataMap = null;
  
  public void setDataMap(Map map)
  {
    m_oDataMap = map;
  }
  
  public Map getDataMap()
  {
    return m_oDataMap;
  }
  
}
