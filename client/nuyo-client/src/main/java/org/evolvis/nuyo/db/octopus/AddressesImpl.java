package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.commons.datahandling.entity.AsyncEntityListImpl;
import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.OctopusTask;

/**
 * @author Sebastian Mancke, tarent GmbH
 * 
 * This is an asynchronous implementation for the address access.
 *
 * By default, this list represents all Addresses of the current user.
 * This set can be restricted by filters (e.g. category selection or search terms).
 *  
 */
public class AddressesImpl extends AsyncEntityListImpl implements Addresses {
	
	private static Logger logger = Logger.getLogger(AddressesImpl.class.getName());

    public static final String TASK_GET_ADDRESS_PKS = "getAddressPks";
    public static final String PARAM_ADDRESS_PKS = "addressPks";

    /** the current connection for this list */
	protected OctopusDatabase db;

    /** the current filter parameters */
    protected AddressListParameter listParameter;

	public AddressesImpl(OctopusDatabase db) {		
		this.db = db;
        
        setFetchSize(40);
        setMaxQueuedJobs(3);
        setMaxHoldEntities(400);
        setConnection(db.getOctopusConnection());
        setTaskName("getAddresses");
        setFilterParamName(AddressWorkerConstants.PARAM_ADDRESSES_FILTER);        
	}

    /**
     * Template method to override for error reorting
     */
    protected void reportError(Exception e) {
        ApplicationServices.getInstance().getCommonDialogServices()
            .publishError(Messages.getString("AddressesImpl_LoadAddressFailed"), e);
    }


    /**
     * Reloads the address set with the same AddressListParameter.
     * If the address set if not persistent, this action may do nothing. 
     */
    public void reload() {
        // should we call 'stop();' first?
        initialLoad();
    }
    
    /**
     * Sets a filter on the addresses
     * @param listParameter Filter list parameters
     */
    public void setAddressListParameter(AddressListParameter listParameter) {        
        stop();
        this.listParameter = listParameter;
        super.setSortField(listParameter.getSortField());
        super.setFilterList(listParameter.getCompleteFilterList());
        initialLoad();
    }

    /**
     * Returns the filter on the addresses
     */
    public AddressListParameter getAddressListParameter() {
        return listParameter;
    }


    /** 
     * Invokation of the main task.
     * This hook is called by the super class
     */
    protected OctopusResult callTask(LoadJob loadJob) throws OctopusCallException {
        return callTask(loadJob, getTaskName());
    }
            
    /**
     * Invocation of an AddressTask
     * @param loadJob The portion to load, may be null
     * @param taskName The task to call
     */
    protected OctopusResult callTask(LoadJob loadJob, String taskName) throws OctopusCallException {
        OctopusTask task = getConnection().getTask(taskName);
        // TODO: only query the needed column names

        // force a reload of the filter
        task.add(getFilterParamName()+"."+ListFilter.PARAM_RESET, "true");
        task.add(getFilterParamName()+"."+ListFilter.PARAM_RESET_FILTER, "true");
        
        if (loadJob != null) {
            task.add(getFilterParamName()+"."+ListFilter.PARAM_START, new Integer(loadJob.getOffset()));
            task.add(getFilterParamName()+"."+ListFilter.PARAM_LIMIT, new Integer(loadJob.getLimit()));
        }
        
        if (getFilterList() != null)
            task.add(getFilterParamName()+"."+ListFilter.PARAM_FILTER_LIST, getFilterList());

        // sort paraemters
        AddressListParameter sortListParameter = listParameter;
        if (sortListParameter == null)
            sortListParameter = new AddressListParameter();
        if (sortListParameter.getSortField() != null) {
            task.add(getFilterParamName()+"."+ListFilter.PARAM_SORT_FIELD, sortListParameter.getSortField());
            if (sortListParameter.getSortDirection() != null)
                task.add(getFilterParamName()+"."+ListFilter.PARAM_SORT_DIRECTION, sortListParameter.getSortDirection());
        }
        
        if (listParameter != null) {
            task.add(AddressWorkerConstants.PARAM_DISJUNCTION_SUBCATEGORIES, listParameter.getDisjunctionSubcategories());
            task.add(AddressWorkerConstants.PARAM_CONJUNCTION_SUBCATEGORIES, listParameter.getConjunctionSubcategories());
            task.add(AddressWorkerConstants.PARAM_NEGATED_SUBCATEGORIES, listParameter.getNegatedSubcategories());
        }
        
        return task.invoke();
    }
    

        
    /**
     * Starts the loading and blockes until the firtst result returns
     */
    public void startLoading() {
        initialLoad();        
        waitFor(0);
    }

    /**
     * Blockes until an address is loaded.
     * TODO: Addresszugriff: Enhance the error handling
     */
    public void waitFor(int index) {
        if (isSizeKnown() && getSize() == 0)
            return;
        for (int i=0; i<12000; i++) { //120 seconds
            if (super.getEntityAt(index) != null)
                return;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {}
        }
        throw new RuntimeException(Messages.getString("AddressesImpl_LoadAddressFailed"));
    }
    
    
    
    /**
     * Overwritten from AsyncEntityListImpl.
     */
    protected void updatePosition(ArrayList entityStorage, int offset, Object data) {
        entityStorage.set(offset, data);
    }
    
    /**
     * @return den maximalen Index, also Anzahl der Adressnummern - 1; wenn dieser
     * Wert kleiner als -1 ist, so ist die Anzahl der Adressnummern noch unbekannt. 
     */
    public int getIndexMax() {
        return getSize()-1;
    }
    
    
    /**
     * @param index der Index der zuholenden Adressnummer zwischen 0 und dem maximalen
     * Index.
     * @return die bezeichnete Adressnummer oder 0, wenn sie noch nicht abgeholt ist.
     * @throws IndexOutOfBoundsException wird geworfen, wenn ein ung�ltiger Index
     * angegeben wurde.
     */
    public int getAddressNumber(int index) {
        waitFor(index);
        return get(index).getId();
    }
    
    /**
     * Waits for the address to be loaded
     * and returns the specified object.
     *     
     * TODO: Addresszugriff: Integrate it into tc without blocking
     *
     *
     * @param index der Index der zuholenden Adressnummer zwischen 0 und dem maximalen
     * Index.
     * @return die bezeichnete Adresse oder null, wenn sie noch nicht abgeholt ist.
     * @throws IndexOutOfBoundsException wird geworfen, wenn ein ung�ltiger Index
     * angegeben wurde.
     * @see #getAddressNumber(int)
     */
    public Address get(int index) {
        waitFor(index);
        return (Address)getEntityAt(index);
    }
    
    public void addAddress(Address address) {
        throw new RuntimeException("addAddress() not supported by this implementation.");
    }
    

    public void addEntity(Object entity) {
        throw new RuntimeException("addEntity() not supported by this implementation.");
    }
    
    public void removeEntity(Object entity) {
        throw new RuntimeException("removeEntity() not supported by this implementation.");
    }
            
    public boolean contains(Object entity) {
        throw new RuntimeException("contains() not supported by this implementation.");
    }

    public int indexOf(Object entity) {
        throw new RuntimeException("contains() not supported by this implementation.");
    }
    
    public void clear() {
        throw new RuntimeException("AddressesImpl:clear() not supported by this implementation.");
    }

    /**
     * TODO: Addresszugriff: Proper error handling
     * Returns a list of all pks as Integer Objects.
     */
	public PrimaryKeyList getPkList() {
        try {
            OctopusResult res = callTask(null, TASK_GET_ADDRESS_PKS);
            return (PrimaryKeyList)res.getData(PARAM_ADDRESS_PKS);
        } catch (OctopusCallException oce) {
            throw new RuntimeException(oce);
        }
    }
	
    /**
     * Returns a String list of all pks sepperated by " ".
     */
	public String getPkListAsString() {
        return PrimaryKeyList.toString(" ", getPkList());
    }
    
    /**
     * Selects the addresses by the list of supplied Integer pks.
     * This method overides the current set AddressListParameter-Object.
     * If you need an additional filter, please use <code>getAddressListParameter().addPkFilter(pkList);</code>.
     */
    public void setPkFilter(List pkList) {
        AddressListParameter alp = new AddressListParameter();
        alp.setPkFilter(pkList);
        setAddressListParameter(alp);
    }
}
