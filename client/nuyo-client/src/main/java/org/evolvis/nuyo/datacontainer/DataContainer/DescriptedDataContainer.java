/*
 * Created on 24.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

/**
 * @author niko
 *
 */
public interface DescriptedDataContainer
{
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor();
}
