/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Color;

import javax.swing.SwingConstants;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class PostfachPLZ3Field extends ContactAddressTextField
{
  private TarentWidgetTextField m_oTextfield_postfachplz;
  private TarentWidgetTextField m_oTextfield_postfach;
  private TarentWidgetLabel m_oLabel_postfach;
  private TarentWidgetLabel m_oLabel_postfachplz;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }  
 
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createPostfachPlzPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "POSTFACHPLZ3";
  }

  public void postFinalRealize()
  {    
  }
  
  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      m_oTextfield_postfachplz.setEditable(true);
      m_oTextfield_postfach.setEditable(true);
    }
    else
    {  
      m_oTextfield_postfachplz.setEditable(false);
      m_oTextfield_postfachplz.setBackground(Color.WHITE);
      m_oTextfield_postfach.setEditable(false);
      m_oTextfield_postfach.setBackground(Color.WHITE);
    }
  }

 public void setData(PluginData data)
  {
    Object value = data.get(AddressKeys.POSTFACHPLZ3);
    if (value != null) m_oTextfield_postfachplz.setText(value.toString());
    else m_oTextfield_postfachplz.setText("");
    
    value = data.get(AddressKeys.POSTFACH3);
    if (value != null) m_oTextfield_postfach.setText(value.toString());    
    else m_oTextfield_postfach.setText("");
  }

  public void getData(PluginData data)
  {
    data.set(AddressKeys.POSTFACHPLZ3, m_oTextfield_postfachplz.getText());
    data.set(AddressKeys.POSTFACH3, m_oTextfield_postfach.getText());
  }

  public boolean isDirty(PluginData data)
  {
    if (!((m_oTextfield_postfachplz.getText().equals("")) &&  (data.get(AddressKeys.POSTFACHPLZ3) == null ))) //$NON-NLS-1$
      if (!(m_oTextfield_postfachplz.getText().equals(data.get(AddressKeys.POSTFACHPLZ3)))) return(true);            

    if (!((m_oTextfield_postfach.getText().equals("")) &&  (data.get(AddressKeys.POSTFACH3) == null ))) //$NON-NLS-1$
      if (!(m_oTextfield_postfach.getText().equals(data.get(AddressKeys.POSTFACH3)))) return(true);            

    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  
  
  
  
  
  
  
  public String getFieldName()
  {
    return fieldName;     
  }
  
  public String getFieldDescription()
  {
    return fieldDescription;        
  }
  
  
  
  
  private String getFieldNamePostfachPLZ()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Postfach_PLZ3");
    else return text;
  }

  private String getFieldNamePostfach()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Postfach3");
    else return text;
  }


  
  private String getFieldDescriptionPostfachPLZ()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Postfach_PLZ3_ToolTip");
    else return text;
  }
  
  private String getFieldDescriptionPostfach()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Postfach3_ToolTip");
    else return text;
  }
  
  
  
  
  public void setFieldName(String name)
  {
    fieldName = name;
    if (panel != null)
    {
      if (name != null) 
      {
        m_oLabel_postfachplz.setText(getFieldNamePostfachPLZ());
        m_oLabel_postfach.setText(getFieldNamePostfach());
      }
    }
  }  
  

  public void setFieldDescription(String description)
  {
    fieldDescription = description;
    if (panel != null)
    {
      if (description != null) 
      {
        m_oTextfield_postfachplz.setToolTipText(getFieldDescriptionPostfachPLZ());
        m_oTextfield_postfach.setToolTipText(getFieldDescriptionPostfach());
      }
    }
  }  
  
  private EntryLayoutHelper createPostfachPlzPanel(String widgetFlags)
  {
    m_oLabel_postfachplz = new TarentWidgetLabel(getFieldNamePostfachPLZ());
    m_oTextfield_postfachplz = createTextField(getFieldDescriptionPostfachPLZ(), 10);

    m_oLabel_postfach = new TarentWidgetLabel(getFieldNamePostfach() + " ", SwingConstants.RIGHT); //$NON-NLS-1$
    m_oTextfield_postfach = createTextField(getFieldDescriptionPostfach(), 20);

    return new EntryLayoutHelper(
                                   new TarentWidgetInterface[] {
                                                                m_oLabel_postfachplz,
                                                                m_oTextfield_postfachplz,
                                                                m_oLabel_postfach,
                                                                m_oTextfield_postfach },
                                   widgetFlags);
  }
  public void setDoubleCheckSensitive(boolean issensitive) {
	  setDoubleCheckSensitive(m_oTextfield_postfach, issensitive);
	  setDoubleCheckSensitive(m_oTextfield_postfachplz, issensitive);	
  } 

}
