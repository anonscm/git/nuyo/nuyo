/* $Id: Starter.java,v 1.98 2007/09/08 18:32:07 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.mail.DispatchEMailPlugin;
import org.evolvis.nuyo.plugins.mail.orders.MailOrdersPlugin;
import org.evolvis.nuyo.plugins.office.OfficeExporterPlugin;
import org.evolvis.nuyo.plugins.search.SearchPlugin;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactoryException;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.commons.ui.DeferringLoginProvider;
import de.tarent.commons.ui.swing.LoginDialog;
import de.tarent.octopus.client.remote.OctopusRemoteConnection;


/**
 * Startet Contact Anwendung.
 * 
 * Zur Effizienz werden Aufbauaktivit�ten in 3 Gruppen aufgeteilt:
 * 1) Festplatte Zugriffe (Lesen/Schreiben von Config Dateien),
 * 2) Netzwerkverkehr (DB Kommunikation)
 * 3) Prozessor Zugriffe (Hauptspeichergebrauch, Berechnungen).
 * 
 * Beispiel:
   <p>
       ThreadGroup configThreadGroup = new ThreadGroup("Config-Thread");
       configThreadGroup.setDaemon(true);//nach dem Ablaufen automatisch aufl�sen
       configThreadGroup.add(new InitGUIThread());
       configThreadGroup.add(new LoadDBDataThread());
   </p>   
 * 
 * Jede Gruppe wird parallel zu den anderen (im eigenen Thread) gestartet.
 * Alle Operationen die GUI beeinflussen (z.B. Tabellen mit Daten f�llen)
 * werden Thread-sicher in dem Dispatch-Thread ausgef�hrt:
 * dazu packt man betroffene Methode(n) in ein Runnable-Object 
 * und �bergibt es dem Event Queue, z.B:
 * <p>
 *  SwingUtilities.invokeLater(new Runnable(){
 *      public void run() {
 *          // hier findet GUI Aktualisierung statt.
 *      }
 *  });  
 * </p>
 * 
 * Seit Java 1.5 kann UncaughtExceptionHandler definiert werden, 
 * um von JVM gefangene Exceptions zu behandeln, 
 * die w�hrend Ausf�hrung eines beliebigen Thread ausgel�st wurden. 
 * 
 * Der Zeitpunkt, an dem alle Ergebnisse zusammengesammelt werden k�nnen,
 * wird wie folgt bestimmt:
 * <p>
   try {
      synchronized( threadGroup )  {
          while ( tg.activeCount() > 0 )
              tg.wait( 10 );
      }
   } catch(InterruptedException e) {
   }
 * </p>
 * 
 * DER GESAMTE ABLAUF sieht wie folgt aus:
 * 1) zeige Standard-Lade-Cursor (damit User sieht, dass das Prozess l�uft)
 * 2) init Tarent Option Dialog (wichtig f�r Errors Ausgabe von Anfang an)
 * 3) lese Config Daten:
 *  (a) Bundle mit Messages (um LoginDialog und Error-Ausgaben international zu gestalten)
 *  (b) sonstige Daten
 * 4) starte parallel new Thread("init-gui"): 
 *  (a) lade Bilder
 *  (b) lade Plugins
 *  (c) registriere Plugins bei PluginRegistry
 *  (d) lade Actions
 *  (e) init Actions
 *  (f) registriere Actions bei ActionRegistry
 * 5) zeige Standard-Cursor
 * 6) zeige LoginDialog:
 *  (a) wenn LOGIN korrekt: schlie�e LoginDialog
 *  (b) zeige Tarent-Lade-Cursor  
 *  (c) zeige anwendungs GUI.
 *  (d) GUI Buttons ausgrauen, falls n�tig.
 * 7) starte parallel new Thread("DB-data-loader"):
 *  (a) lade Daten von DB: parallel KONTAKTE und KALENDER
 *  (b) f�lle GUI mit Daten in Dispatch-Thread: SwingUtilities.invokeLater(Runnable)
 * 8) warte auf den Zeitpunkt, dass alle Daten geladen wurden
 * 9) aktiviere ausgegraute Buttons
 *10) zeige Tarent-Standard-Cursor
 *       
 * @author Sebastian
 */
public class Starter implements StartUpConstants {
    
    private static TarentLogger log = new TarentLogger(Starter.class);
    
    private long beginTime;    
    
    protected Database database;
    protected LoginDialog loginProvider;
    
    protected OctopusRemoteConnection continueSessionConnection;


    public static void main( String[] args ) {
        Starter starter = new Starter();

        starter.init(args);
    }
    
    /**
     * 
     * Runs platform specific code
     * 
     * <p>TODO: Phase this out into a separate class one day
     * together with {@link #showUnsupportedVersionComplain()}.</p>
     *
     */
    
    private void quirks()
    {
    	// Some MacOS X convenience

    	// Lets the menu-bar appear where a mac-user expects it.
    	System.setProperty("com.apple.macos.useScreenMenuBar", "true");

    	// Sets Application-Name.
    	System.setProperty("com.apple.mrj.application.apple.menu.about.name", Messages.getString("GUI_MainFrameNewStyle_Titel_tarent-contact"));
    	
    	// Enable font-anti-aliasing
    	System.setProperty("swing.aatext", "true");
    }
    
    /**
     * checks the runtime for compatibility.
     * 
     * <p>TODO: Phase this out into a separate class one day
     * together with {@link #showUnsupportedVersionComplain()}.</p>
     */
    private void checkForCompatibility()
    {
      Preferences complainPrefs = ConfigManager.getPreferences();
      
      if (complainPrefs.getBoolean(StartUpConstants.PREF_COMPLAIN_UNSUPPORTED_JAVA_VERSION, true))
        {
          // contact-client is tried and tested against the Java 1.5 and 1.6 implementation from
          // Sun Microsystems only. If run with something different a warning is printed.
          double d = new Double(System.getProperty("java.specification.version", "0.0")).doubleValue();
          if (d < 1.5 || d > 1.6 || System.getProperty("java.vendor", "none").indexOf("Sun Microsystems") < 0)
            {
              boolean complainAgain = showUnsupportedVersionComplain();
              complainPrefs.putBoolean(StartUpConstants.PREF_COMPLAIN_UNSUPPORTED_JAVA_VERSION, complainAgain);
            }
            
        }
    }
    
    /**
     * Checks if the {@link ConfigManager} reports missing documents and shows an error
     * dialog in that case.
     * 
     * <p>This method must not be called before the ConfigManager's <code>init()</code>
     * has been run.</p>
     *
     */
    private void checkForMissingConfigurationDocuments()
    {
    	List docs = ConfigManager.getMissingDocuments();
    	
    	if (docs.isEmpty())
          return;
    	
    	final JDialog d = new JDialog((JFrame) null, true);
    	d.setTitle(Messages.getString("GUI_Starter_MissingConfigDocs_Title"));
    	
    	Container p = d.getContentPane();
    	p.setLayout(new FormLayout("6dlu, pref, 3dlu, max(120dlu;pref):grow, 3dlu, pref, 6dlu", "6dlu, pref, 3dlu, fill:pref:grow, 3dlu, pref, 12dlu, pref, 6dlu"));
    	CellConstraints cc = new CellConstraints();
    	
    	p.add(new JLabel(Messages.getString("GUI_Starter_MissingConfigDocs_Info")), cc.xyw(2, 2, 5));
    	
    	p.add(new JScrollPane(new JList(docs.toArray())), cc.xy(4, 4));

    	p.add(new JLabel(Messages.getString("GUI_Starter_MissingConfigDocs_Suggestion")), cc.xyw(2, 6, 5));
    	
    	PanelBuilder pb = new PanelBuilder(new FormLayout("center:pref:grow", "pref"));
    	JButton okButton = new JButton(Messages.getString("GUI_Starter_MissingConfigDocs_OK"));
    	okButton.addActionListener(new ActionListener()
    	{
    		public void actionPerformed(ActionEvent ae)
    		{
    			d.setVisible(false);
    		}
    	});
    	pb.add(okButton, cc.xy(1,1));
    	p.add(pb.getPanel(), cc.xyw(2, 8, 5));
    	
    	d.pack();
    	d.setLocationRelativeTo(null);
    	
    	d.setVisible(true);
    }
    
    /**
     * Displays a warning dialog and returns whether the check which caused
     * the dialog to be opened should be done again at the next application started.
     * 
     * @return false if user does not want to see this warning any more
     */
    private boolean showUnsupportedVersionComplain()
    {
      // Warning dialog is presented in its own frame for the sole reason to have
      // a valid 'window entry' in the surrounding windowing environment in all cases
      // without the need to initialize a parental frame somewhere or reuse one (e.g.
      // the login window). However being on our own means that we have to implement
      // thread suspension ...
    
      // A special (privately-known) monitor object that is used
      // for thread suspension and re-awakening.
      final Object monitor = new Object();
      
      // A result array which is itself final and can be accessed
      // from an inner class to set its values (sort of a hack, but JLS compliant).
      final boolean result[] = new boolean[] { false }; 
      
      // Construction of the warning dialog.
      FormLayout l = new FormLayout("3dlu, 10dlu, 6dlu, pref:grow, 3dlu, pref:grow, 6dlu, 10dlu, 3dlu",
                                    "3dlu, pref, 12dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 12dlu, pref, 12dlu, pref, 3dlu, pref, 3dlu");
      
      final JFrame frame = new JFrame(Messages.getString("GUI_Starter_Complain_Title"));

      frame.addWindowListener(new WindowAdapter() {

    	  public void windowClosing(WindowEvent arg0) {

    		  // Makes frame invisible and stores the result.
    		  super.windowClosing(arg0);
    		  result[0] = false;

    		  // Re-awakens the main thread.
    		  synchronized (monitor)
    		  {
    			  monitor.notifyAll();
    		  }
    	  }
      });
      
      Container cp = frame.getContentPane();
      cp.setLayout(l);
      CellConstraints cc = new CellConstraints();
      
      cp.add(new JLabel(Messages.getString("GUI_Starter_Complain_Unsupported")), cc.xyw(2, 2, 7));
      
      cp.add(new JLabel(Messages.getString("GUI_Starter_Complain_Implementation_Version")), cc.xyw(3, 4, 2));
      cp.add(new JLabel(System.getProperty("java.version")), cc.xyw(6, 4, 2));

      cp.add(new JLabel(Messages.getString("GUI_Starter_Complain_Implementation_Vendor")), cc.xyw(3, 6, 2));
      cp.add(new JLabel(System.getProperty("java.vendor")), cc.xyw(6, 6, 2));

      cp.add(new JLabel(Messages.getString("GUI_Starter_Complain_VM_Name")), cc.xyw(3, 8, 2));
      cp.add(new JLabel(System.getProperty("java.vm.name")), cc.xyw(6, 8, 2));

      cp.add(new JLabel(Messages.getString("GUI_Starter_Complain_VM_Vendor")), cc.xyw(3, 10, 2));
      cp.add(new JLabel(System.getProperty("java.vm.vendor")), cc.xyw(6, 10, 2));
      
      cp.add(new JLabel(Messages.getString("GUI_Starter_Complain_Again")), cc.xyw(2, 12, 7));
      
      // yes and no button get their own panel to make their layout independent from the rest
      // of the dialog (e.g. to have equal button widths).
      JButton yesButton = new JButton(Messages.getString("GUI_Starter_Complain_Yes"));
      yesButton.addActionListener(new ActionListener()
      {
    	  public void actionPerformed(ActionEvent ae)
    	  {
    		  // Makes frame invisible and stores the result.
    		  frame.setVisible(false);
    		  result[0] = true;

    		  // Re-awakens the main thread.
    		  synchronized (monitor)
    		  {
    			  monitor.notifyAll();
    		  }
    	  }
      });
      
      frame.getRootPane().setDefaultButton(yesButton);
      
      JButton noButton = new JButton(Messages.getString("GUI_Starter_Complain_No"));
      noButton.addActionListener(new ActionListener()
      {
    	  public void actionPerformed(ActionEvent ae)
    	  {
    		  // Makes frame invisible and stores the result.
    		  frame.setVisible(false);
    		  result[0] = false;
    		  
    		  // Re-awakens the main thread.
    		  synchronized (monitor)
    		  {
    			  monitor.notifyAll();
    		  }
    	  }
      });
      
      // Buttons resemble the layout of a JOptionPane dialog: They have the same width and
      // try to stay close in the middle.
      PanelBuilder pb = new PanelBuilder(new FormLayout("3dlu:grow, pref, 3dlu, pref, 3dlu:grow", "pref"));
      pb.add(yesButton, cc.xy(2, 1));
      pb.add(noButton, cc.xy(4, 1));
      pb.getLayout().setColumnGroups(new int[][] { { 2, 4 } });
      
      cp.add(pb.getPanel(), cc.xyw(2, 14, 7));
      
      frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);
      
      // Sleeps until awakened by action events on the buttons.
      try {
    	  synchronized (monitor) {
    	    monitor.wait();
    	  }
      } catch (InterruptedException e)
      {
        // Expected (caused by monitor.notifyAll() ).
      }
      
      frame.dispose();
      
      return result[0];
    }
    
    protected void init(String[] args){
        try {
            beginTime = System.currentTimeMillis();
            
            quirks();

            initIconFactory();            
//            initLookAndFeel();
            initPluginRegistry();
            
            checkForSetUID(args);
            
            ConfigManager.getInstance().setPrefBaseNodeName("/org/evolvis/nuyo");
            ConfigManager.getInstance().setBootstrapVariant("nuyo");
            ConfigManager.getInstance().setApplicationClass(Starter.class);
            
            // Loads the configuration documents.
            ConfigManager.getInstance().init("nuyo-client");
            
            checkForMissingConfigurationDocuments();
            
            checkForCompatibility();
            
            Environment env = ConfigManager.getEnvironment();
            // set level for all logger
            Logger.getLogger("").setLevel(Level.parse(env.get(Key.DEBUG)));
            // open additional logfile
            startLogfile(env);
            
            database = new OctopusDatabase();
            
            Collection<ConnectionParameters> connectionParameters = ConnectionDefinitionWrapper.connectionDefinitionsToConnections(ConfigManager.getEnvironment().getConnectionDefinitions());
            loginProvider = new LoginDialog(getApplicationUser(), null, getLastConnection(), ((ImageIcon)SwingIconFactory.getInstance().getIcon("nuyo.png")).getImage(), connectionParameters, Messages.getString("MainFrame_Title"));
            
            TarentLogger.setParentWindow((LoginDialog)loginProvider);
            
            ActionListener listener = new LoginActionListener();
            listener.actionPerformed(new ActionEvent(loginProvider, 0, "connection_changed"));
            loginProvider.addActionListener(listener);
            loginProvider.setDialogVisible(true);
            
        } catch (NoClassDefFoundError ncdf) {
            String error = Messages.getString("Starter.errorMessage_loading1") //$NON-NLS-1$
                         + Messages.getString("Starter.errorMessage_loading2") 
                         + ncdf.getMessage() 
                         + Messages.getString("Starter.errorMessage_loading3"); //$NON-NLS-1$ //$NON-NLS-2$
            log.severe(error, ncdf);
            System.exit(4);
        }
        catch (IllegalStateException ise) {
        	log.severe(Messages.getString("Starter.errorMessage_unexpected"), ise);
        	System.exit(3);
        }
    }
    
    private void tryToConnect(Database database, LoginDialog loginProvider)
    {
    	try
		{
			connect(database, loginProvider);
		} catch (ContactDBException e)
		{
			// connection could not be established
            String errorMsg = Messages.getString("Starter.errorMessage_connection_failed");

            int exceptionId = ((ContactDBException)e).getExceptionId();
            if ( exceptionId == ContactDBException.EX_CLIENT_LOGIN_ERROR )
                errorMsg = e.getMessage();
            
            log.warning(errorMsg, e.getCause());
			loginProvider.reset();
		}
    }
    
    private void connect(Database database, LoginDialog loginProvider) throws ContactDBException
    {
		// update last-connection-value
        setLastConnection(loginProvider.getSelectedConnection().toString());
        
        // If user opted to continue a session the database will be
        // initialized with the connection used for the test
        // otherwise a new one is created with the connection information
        // provided by the LoginProvider
        
        String userName;
        boolean continueSession = loginProvider.shouldContinueSession();
        if(continueSession)
        {
        	database.init(continueSessionConnection);
        	database.continueSession();
        	userName = database.getUser();
        }
        else
        {
        	userName = loginProvider.getUserName();
        	database.init(OctopusDatabase.createConnection(loginProvider.getSelectedConnection()));
        }
        
        setApplicationUser(userName);
    	
    	loginProvider.setStatusText(Messages.getString("Starter.startscreen_connect_to"));
    	loginProvider.setStatus(5);

        if (!continueSession) {
            database.setUser(userName);
            database.setPassword(loginProvider.getPassword());
            database.login();
        }

    	ApplicationServices.getInstance().setCurrentDatabase(database);
    	
    	loginProvider.setStatusText(Messages.getString("Starter.startscreen_start_application"));
    	loginProvider.setStatus(10);
    	createApplicationDataModel();            
    	startApplication(database, loginProvider);
    	log.info("total time: " + Long.toString(System.currentTimeMillis() - beginTime) + "msec");
    }

    private void initPluginRegistry() {
        PluginRegistry.getInstance().registerPlugin(new SearchPlugin());
        if (ConfigManager.getAppearance().getAsBoolean(org.evolvis.xana.config.Appearance.Key.SHOW_SCHEDULEPANEL, true))
        		PluginRegistry.getInstance().registerPlugin(new CalendarPlugin());
        else log.info("Config parameter \"showSchedulePanel\" = FALSE. Calender will not be shown.");
        PluginRegistry.getInstance().registerPlugin(new MailOrdersPlugin());
        PluginRegistry.getInstance().registerPlugin(new OfficeExporterPlugin());
        PluginRegistry.getInstance().registerPlugin(new DispatchEMailPlugin());
        //PluginRegistry.getInstance().registerPlugin(new JabberPlugin());
    }

//    private void initLookAndFeel() {
//    	
//        UIManager.installLookAndFeel("Tarent Plastik Look and Feel (JGoodies)", TarentPlasticLookAndFeel.class.getName());
//        UIManager.installLookAndFeel("Plastik Look and Feel (JGoodies)", PlasticLookAndFeel.class.getName());
//        
//        // only supported on Windows
//        if(SystemInfo.isWindowsSystem())
//        	UIManager.installLookAndFeel("Windows Look and Feel(JGoodies)", WindowsLookAndFeel.class.getName());
//    }

    private void initIconFactory() {
        SwingIconFactory iconFactory = SwingIconFactory.getInstance();
        ApplicationServices.getInstance().setIconFactory(iconFactory);
        try {
            iconFactory.addResourcesFolder(Starter.class, CONTACT_RESOURCES_PATH);
            iconFactory.addResourcesFolder(Starter.class, CALENDAR_RESOURCES_PATH);
            iconFactory.addResourcesFolder(Starter.class, EMAIL_RESOURCES_PATH);
        }
        catch ( IconFactoryException e ) {
            log.severe(Messages.getString("GUI_Icon_Factory_Error"), e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Starts the application after basic initialisation has taken place.
     * 
     * <p>This method is to be overridden in subclasses to implement different
     * startup scenarios.</p>
     * 
     * @param currentConfiguration String der die Konfiguration angibt
     */    
    protected void startApplication(Database database, LoginDialog loginProvider){
    	ActionManager am = new ActionManager(database, loginProvider);

        // Load the action definitions.
        ActionRegistry ar = ActionRegistry.getInstance();
        ar.setIconFactory(SwingIconFactory.getInstance());
        ar.init(ConfigManager.getAppearance().getActionDefinitions());

        // Makes application visible (and does the remaining steps for this).
        am.start();
        
        // Runs the action initializations (which depend on the UI being set up
        // first).
        ar.runDelayedInitializations();
    }
    
    private void createApplicationDataModel() {
        ApplicationServices.getInstance().setApplicationModel(new ApplicationModel());
        BindingManager bm = new BindingManager();
        bm.setModel(ApplicationServices.getInstance().getApplicationModel());
        ApplicationServices.getInstance().setBindingManager(bm);
    }

    
    private void checkForSetUID(String[] args) {        
        log.log(Level.FINER,"parse Argumente"); //$NON-NLS-1$
        //setUID if more than one argument..
        if (args.length > 1) try {
            int i = Integer.parseInt(args[1]);
            ApplicationStarter.setUID(i);
        } catch(NumberFormatException nfe) {
            //do nothing here
        }
    }
    
    private final static void startLogfile(Environment env) {
        String filename = env.get(Key.LOG_FILE_NAME);
        if (filename == null || filename.trim().length() == 0)
            return;
        
        Level level = parseLogLevel(env.get(Key.LOG_FILE_LEVEL));
        Logger logger = Logger.getLogger(env.get(Key.LOG_FILE_LOGGER));
        try {
            filename = filename.trim();
            //TODO: sicherstellen, dass Verzeichnisse existieren
            Handler logHandler = new FileHandler(filename);
            logHandler.setFormatter(new SimpleFormatter());
            logHandler.setLevel(level);
            logger.addHandler(logHandler);
        } catch (Exception e) {
        	logger.log(Level.WARNING, Messages.getString("Starter.logMessage_no_logdatei1") + filename + Messages.getString("Starter.Message_no_logdatei2"), e); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }
    
    /**
     * Restores the user name from the user's configuration.
     * 
     * @return
     */
    protected String getApplicationUser()
    {
      return ConfigManager.getPreferences()
             .node(StartUpConstants.PREF_USER_NODE_NAME)
             .get("name", System.getProperty("user.name"));
    }
    
    /**
     * 
     * Returns the lastly used connection
     * 
     * @return name of the lastly used connection
     */
    
    protected String getLastConnection()
    {
    	return ConfigManager.getPreferences().node(StartUpConstants.PREF_LAST_CONNECTION_NODE_NAME).get("name", null);
    }
    
    /**
     * 
     * Updates the lastly used connection
     * 
     * @param connection - the name of the lastly used connection
     *
     */
    
    protected void setLastConnection(String connection)
    {
    	ConfigManager.getPreferences().node(StartUpConstants.PREF_LAST_CONNECTION_NODE_NAME).put("name", connection);
    }
    
    /** Stores user name in the user's configuration.
     * 
     * @param newUser
     */
    protected void setApplicationUser(String newUser) {
      ConfigManager.getPreferences()
      .node(StartUpConstants.PREF_USER_NODE_NAME)
      .put("name", newUser);
    }
    
    private static Level parseLogLevel(String value) {
      if (value != null) try {
        return Level.parse(value);
    } catch (IllegalArgumentException iaEx) {
        if ("true".equalsIgnoreCase(value))
            return Level.CONFIG;
        else if ("false".equalsIgnoreCase(value))
            return Level.OFF;
    }
      return null;
    }
    
    private class LoginActionListener implements ActionListener
    {
		public void actionPerformed(ActionEvent e)
		{
			// called when the user requests a login
			if(e.getActionCommand().equals("login"))
			{
				new Thread() {
					public void run()
					{
						tryToConnect(database, loginProvider);
					}
				}.start();
			}
			// TODO distinction of cases between "cancel" and "quit"
			// called when the user wants to cancel the startup-process
			else if(e.getActionCommand().equals("cancel"))
			{
				log.info(Messages.getString("Starter.logMessage_canceled_by_user"));
				loginProvider.setDialogVisible(false);
				System.exit(1);
			}
			// called when the user wants to quit the application
			else if(e.getActionCommand().equals("quit"))
			{
				log.info(Messages.getString("Starter.logMessage_canceled_by_user"));
				loginProvider.setDialogVisible(false);
				System.exit(1);
			}
			else if(e.getActionCommand().equals("connection_changed"))
			{
				final LoginDialog loginDialog = (LoginDialog) e.getSource();
		        final ConnectionParameters  cd = loginDialog.getSelectedConnection();
				
		        // Skips the test if no connection definition is selected to
		        // prevent a NullPointerException in the OctoputDatabase class.
		        if (cd == null)
		          return;
		        		
				// Do the test on a separate thread.
		        new Thread(new Runnable()
		        {
		          public void run()
		          {
		            OctopusRemoteConnection orc = OctopusDatabase.createConnection(cd);
		            if (orc.continueSession())
		              {
		                enableSessionContinuable(loginDialog);
		                continueSessionConnection = orc;
		              }
		          }
		        }).start();
			}
		}
		
		 /**
	       * <p>This method is called when the session can be continued. It
	       * will update the {@link DeferringLoginProvider}</p>
	       * 
	       */
	      void enableSessionContinuable(final LoginDialog loginProvider)
	      {
	        SwingUtilities.invokeLater(
	          new Runnable()
	          {
	            public void run()
	            {
	              loginProvider.setSessionContinuable(true);
	            }
	          });
	      }
    }
}