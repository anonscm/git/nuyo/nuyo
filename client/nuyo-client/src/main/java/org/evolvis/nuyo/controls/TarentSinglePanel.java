/*
 * Created on 14.04.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.controls;

import java.awt.GridLayout;

import javax.swing.JPanel;

import com.jgoodies.forms.layout.FormLayout;

/**
 * @author niko
 */
  public class TarentSinglePanel extends JPanel implements TarentPanelInterface
  {
    public TarentSinglePanel()
    {
      super();
      this.setLayout(new GridLayout(0,1));
    }
  
    public void addWidget(EntryLayoutHelper tw)
    {
      tw.addToContainer(this);
    }
    
    public void initFormLayout(String colSpec, String rowSpec)
    {
      setLayout(new FormLayout(colSpec, rowSpec));
    }
}
