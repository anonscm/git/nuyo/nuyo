package org.evolvis.nuyo.selector.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.assigner.AssignerPlugin;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;

/**
 * this ActionClass can be used to trigger the plugin for the mass category assignements
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class AssignerAction extends AbstractGUIAction implements AddressListConsumer {

	private static Logger logger = Logger.getLogger(AssignerAction.class.getName());
	private AddressListProvider provider;

	public void actionPerformed(ActionEvent e) {
		if (provider!=null){
			logger.finest("konfiguriere Plugin zur Kategoriemassenzuweisung");
			DialogAddressListPerformer performer = retrieveDialogAddressListPerformer();
			if (performer!=null){
				configureDialog(performer.getDialog());
				performer.setAddresses(provider.getAddresses());
				performer.execute();
			} else logger.info("Die Kategoriezuweisung kann nicht ausgeführt werden");
		}
	}

	public void registerDataProvider(AddressListProvider provider) {
		this.provider=provider;
	}

	public DialogAddressListPerformer retrieveDialogAddressListPerformer(){
		Plugin assignerPlugin = PluginRegistry.getInstance().getPlugin(AssignerPlugin.ID);
		DialogAddressListPerformer dialogPerformer = null;
		if (assignerPlugin==null) {
			logger.warning("Plugin "+AssignerPlugin.ID+" kann nicht geladen werden");
			return null;
		}
		if (assignerPlugin.isTypeSupported(DialogAddressListPerformer.class)){
			dialogPerformer = (DialogAddressListPerformer)assignerPlugin.getImplementationFor(DialogAddressListPerformer.class);
		} else {
			logger.info("Angeforderter Plugin-Typ "+DialogAddressListPerformer.class.getName()+ " wird nicht unterst�tzt.");
		}
		return dialogPerformer;
	}

//	TODO: setAllwaysOnTop as soon as java 5 is allowed
	public void configureDialog(JDialog dialog){
		JFrame mainFrame = ApplicationServices.getInstance().getCommonDialogServices().getFrame();
		dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		dialog.pack();
		dialog.setLocationRelativeTo(mainFrame);
		dialog.setModal(true);
	}

}
