package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SelectAllAction extends AbstractGUIAction {

    private static final long serialVersionUID = 8838123204192457499L;
    private static final TarentLogger log = new TarentLogger(SelectAllAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SelectAllAction not implemented");
    }

}