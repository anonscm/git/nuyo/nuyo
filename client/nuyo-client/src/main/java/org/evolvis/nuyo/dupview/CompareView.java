package org.evolvis.nuyo.dupview;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JLabel;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.gui.BaseFrame;

import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.datahandling.binding.CompoundModel;
import de.tarent.commons.datahandling.binding.Model;
import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.ui.JHTMLEntityForm;

/**
 * <code>CompareView</code> is a view to compare a list of Addresses. The view is 
 * designed with an HTML file.
 * <p>
 * <code>CompareView</code> uses a <code>JHTMLEntityForm</code> wich uses an 
 * extended version of HTML to render the UI. 
 * Because the count of the columns dependents of the Address count the HTML file will always
 * generated by Velocity. 
 * 
 * @author Thomas Schmitz, tarent GmbH
 * 
 */
public class CompareView extends BaseFrame{
	
	String fileURL;
	File tmpFile;
	EntityList entityList;
	CompoundModel mainModel;
	
	/**
	 * Creates a new CompareView object wich generates an HTML file of a given
	 * Velocity template. It uses this HTML file to render the UI.
	 * 
	 * @param vmTemplate the name of the Velocity template.
	 * @param entityList the Entities you want to compare.
	 */
	public CompareView(String vmTemplate, EntityList entityList) {
		super();
		this.fileURL = vmTemplate;
		this.entityList = entityList;
		/* if no protocol */
        if (-1 == fileURL.indexOf("://"))
			try {
				this.fileURL = new File(getHTMLFile(fileURL).getPath()).toURI().toURL().toString();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else
        	this.fileURL = getHTMLFile(fileURL).getPath();
        initalise();
	}
	
	/**
	 * Initialises the CompareView's components.
	 *
	 */
	private void initalise() {
		BindingManager bindingManager = new BindingManager();
    	HashMap widgetMap = new HashMap();
    	widgetMap.put("label", JLabel.class);
		bindingManager.setModel(createModel());
    	JHTMLEntityForm form = new JHTMLEntityForm(fileURL, widgetMap, bindingManager);
    	int size = (entityList.getSize() > 0) ? entityList.getSize()*200 + 400 : 400; // size of the sum of columns
    	form.setPreferredSize(new Dimension(size, 600));
    	LayoutManager layout = new BorderLayout(); 
    	this.getFrame().getContentPane().setLayout(layout);
    	this.getFrame().getContentPane().add(form, BorderLayout.CENTER);     	
    	this.getFrame().pack();
    	this.getFrame().setVisible(true);
	}
	
	/**
	 * Creates an HTML tmpFile of a Velocity template.
	 * 
	 * @param template path to the vm template
	 */
	private void createHTMLFile(String template){
		ArrayList dupList = new ArrayList();
		if(entityList != null) {
			for(int i = 0; i < entityList.getSize(); i++){
				Integer integer = new Integer(i+1);
				dupList.add(integer);
			}
		}
		StringWriter sw = new StringWriter();	
		try {
			InputStream in = this.getClass().getResourceAsStream("velocity.properties");
			Properties p = new Properties();
			p.load(in);
			Velocity.init(p);
			Template vmTemplate = Velocity.getTemplate(template);
			VelocityContext context = new VelocityContext();
			context.put("dupCount", dupList);
			context.put("this", this);
			vmTemplate.merge(context, sw);
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		try {
			tmpFile = File.createTempFile("template", ".html", null);
			FileWriter fw = new FileWriter(tmpFile, false);
			fw.flush();
			fw.write(sw.toString());
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Creates a Model wich contains the displayed data.
	 *  		 
	 * @return the created Model.
	 */
	private Model createModel() {
		GenericModel genericModel = new GenericModel();
		genericModel.generateModel(entityList);
		mainModel = genericModel.getGenericModel();		
		SelectionManager selectionManager = new SelectionManager(mainModel);		
		mainModel.registerModel("selection", selectionManager);
		mainModel.registerModel("selected", new ColumnManager(selectionManager));
		return mainModel;
	}

	/**
	 * Returns a temporary HTML file which is created
	 * by a given Velocity template file.
	 * 
	 * @param template name of the Velocity tamplate file. 
	 * @return the temporary HTML file.
	 */
	public File getHTMLFile(String template){
		createHTMLFile(template);
		return tmpFile;
	}
	
	/**
	 * Returns the new Address. This is saved under the
	 * key '0' in the mainModel.
	 * 
	 * @return the new Address
	 */
	public Address getNewAddress(){
		Address address = null;
		try {
			address = (Address) mainModel.getAttribute("0");
		} catch (EntityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return address;
	}

	public void setWaiting(boolean isWaiting) {
		// TODO Auto-generated method stub		
	}
	
	public ArrayList getEntityCategories(int i) {
		ArrayList list = new ArrayList();
		Entity entity = (Entity) entityList.getEntityAt(i);
		Map map = null;
		try {
			map = (Map)entity.getAttribute("associatedCategories");
		} catch (EntityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set set = map.entrySet();
		Iterator iter = set.iterator();
		while(iter.hasNext()) {
			Object next = iter.next();
			list.add(next);
		}
		return list;
	}
}
