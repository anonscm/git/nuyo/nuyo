/* $Id: InstanceNotUniqueException.java,v 1.3 2006/06/06 14:12:14 nils Exp $
 * 
 * Created on 20.06.2004
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.util;

/**
 * Diese Exception wird vom {@link org.evolvis.nuyo.db.util.InstanceTrunk} geworfen,
 * wenn zu einer neu hinzuzuf�genden Instanz schon eine andere mit gleicher ID existiert.
 * Die existierende Instanz wird in dieser Exception mit herausgereicht.
 * 
 * @author mikel
 */
public class InstanceNotUniqueException extends Exception {
    //
    // �ffentliche Methoden
    //
    /** ID der vorhandenen Instanz */
    public Object getId()		{ return id; }
    /** vorhandene Instanz */
    public Object getValue()	{ return value; }
    
    //
    // Konstruktor
    //
    InstanceNotUniqueException(Object id, Object value) {
        super("Eine Instanz zu " + id + " ist schon vorhanden.");
        this.id = id;
        this.value = value;
    }
    
    //
    // Membervariablen
    //
    /** ID der vorhandenen Instanz */
    private Object id = null;
    /** vorhandene Instanz */
    private Object value = null;
}
