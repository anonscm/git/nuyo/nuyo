/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.plugin.FrameMailBatchPerformer;

import de.tarent.commons.plugin.Plugin;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class DispatchEMailPlugin implements Plugin
{
	public static final String ID = "mail";
	private DispatchEMailPerformer mailHandler;
	
	public String getID()
	{
		return ID;
	}

	public Object getImplementationFor(Class pClass)
	{
		if(pClass.equals(FrameMailBatchPerformer.class))
		{
			if(mailHandler == null) init();
			return mailHandler;
		}
		return null;
	}

	public List getSupportedTypes()
	{
		ArrayList supportedTypes = new ArrayList();
		supportedTypes.add(FrameMailBatchPerformer.class);
		return supportedTypes;
	}

	public void init()
	{
		mailHandler = new DispatchEMailPerformer();
	}

	public boolean isTypeSupported(Class pClass)
	{
		return FrameMailBatchPerformer.class.equals(pClass);
	}

	public String getDisplayName() {
		return "E-Mail";
	}

}
