/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import javax.swing.JDialog;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.ui.swing.conf.FirstRunWizardSwing;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeInserter implements DialogAddressListPerformer
{
	private Addresses addresses;
	
	public OfficeInserter()
	{
		
	}

	public JDialog getDialog()
	{
		// TODO
		return null;
	}

	public void execute()
	{
		// if there is currently no valid office, setup
		OfficeAutomat.getOfficeConfigurator().setup(new FirstRunWizardSwing(ApplicationServices.getInstance().getCommonDialogServices().getFrame()));
		
		if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice() != null)
		{
			// Has preferred Office
			
			// Save configuration
			OfficeAutomat.getOfficeConfigurator().saveConfiguration(null);
			
			try
			{
				// TODO OfficeAutomat.getReplacementToolKit().replaceAllMarkersByEntityListInSingleDocument(OfficeAutomat.getOfficeController().getStarterDocument(), OfficeAutomat.getOfficeController().getStarterDocument().getDocumentFilename(), addresses, false, true, progressMonitor);
			}
			catch(Exception pExcp)
			{
				pExcp.printStackTrace();
			}
		}
	}

	public Addresses getAddresses()
	{
		return addresses;
	}

	public void setAddresses(Addresses pAddresses)
	{
		addresses = pAddresses;
	}
}