/*
 * Created on 14.04.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TableSorter;
import org.evolvis.nuyo.controls.TarentWidgetComponentTable;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetPanel;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.GUIHelper;
import org.evolvis.nuyo.gui.calendar.AppointmentComparator;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.calendar.TODOPrintContainer;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

/**
 * @author niko
 *
 */
public class ToDoListField extends ContactAddressField implements CalendarVisibleElement
{  
    private static final TarentLogger logger = new TarentLogger(ToDoListField.class);

    private ToDoListField m_oThisToDoListField;
    int m_iMaxAddressesInTooltip = 10;
    boolean m_bShowAddressesInTooltip = true;
    
    public final static Object TODOFILTER_TIME_ALL = "TODOFILTER_TIME_ALL";
    public final static Object TODOFILTER_TIME_ALL_FUTURE = "TODOFILTER_TIME_ALL_FUTURE";
    
    public final static Object TODOFILTER_TIME_TODAY = "TODOFILTER_TIME_TODAY";
    public final static Object TODOFILTER_TIME_THIS_WEEK = "TODOFILTER_TIME_THIS_WEEK";
    public final static Object TODOFILTER_TIME_THIS_MONTH = "TODOFILTER_TIME_THIS_MONTH";
    
    public final static Object TODOFILTER_TIME_WEEK = "TODOFILTER_TIME_WEEK";
    public final static Object TODOFILTER_TIME_TWO_WEEKS = "TODOFILTER_TIME_TWO_WEEKS";
    public final static Object TODOFILTER_TIME_THREE_WEEKS = "TODOFILTER_TIME_THREE_WEEKS";
    
    public final static Object TODOFILTER_TIME_SCHEDULE = "TODOFILTER_TIME_SCHEDULE";
    /*alle
    alle zukunftigen
    ----------------
    heute
    diese Woche (alle Termine der Woche, incl. der vergangenen Termin/Aufgaben)
    diesen Monat (alle Termine des Monats, incl. der vergangenen Termin/Aufgaben)
    ----------------
    7 Tage
    14 Tage 
    21 Tage
    ----------------
    Kalenderansicht
    */
    public final static Object TODOFILTER_PRIORITY_ALL = "TODOFILTER_PRIORITY_ALL";
    public final static Object TODOFILTER_PRIORITY_LOW = "TODOFILTER_PRIORITY_LOW";
    public final static Object TODOFILTER_PRIORITY_MID = "TODOFILTER_PRIORITY_MID";
    public final static Object TODOFILTER_PRIORITY_HIGH = "TODOFILTER_PRIORITY_HIGH";
    public final static Object TODOFILTER_PRIORITY_PRIVATE = "TODOFILTER_PRIORITY_PRIVATE";
    
    // --------------------------------------------------------------------------------
    protected final static DateFormat myDateTimeFormatter = new SimpleDateFormat("dd.MM.yy - HH:mm");
    protected final static DateFormat myLongDateTimeFormatter = new SimpleDateFormat("EEE, dd.MMMM.yyyy - HH:mm");
    protected final static DateFormat myMonthTimeFormatter = new SimpleDateFormat("MMMMMMMMMMMM");
    protected final static DateFormat myKWTimeFormatter = new SimpleDateFormat("w");
    
    private ScheduleDate m_oFirstDay = null;
    private ScheduleDate m_oEndDay = null;
    
    private ImageIcon m_oIcon_Level_0; 
    private ImageIcon m_oIcon_Level_25; 
    private ImageIcon m_oIcon_Level_50; 
    private ImageIcon m_oIcon_Level_75; 
    private ImageIcon m_oIcon_Level_100; 
    private ImageIcon m_oIcon_Unknown;
    
    private ImageIcon m_oIcon_Traffic_RED;
    private ImageIcon m_oIcon_Traffic_YELLOW;
    private ImageIcon m_oIcon_Traffic_GREEN;
    
    
    private ImageIcon m_oIcon_Jump; 
    private ImageIcon m_oIcon_JumpDay; 
    private ImageIcon m_oIcon_JumpWeek; 
    private ImageIcon m_oIcon_JumpMonth; 
    private ImageIcon m_oIcon_Edit;   
    
    
    private EntryLayoutHelper m_oSchedulePanel = null;
    
    private TarentWidgetIconComboBox m_oIconComboBox_Filter1;
    private TarentWidgetIconComboBox m_oIconComboBox_Filter2;
    private TarentWidgetComponentTable m_oCompTable_ToDo;
    
    
    private JCheckBox m_oCompletedCheck;
    private ActionListener m_oCheckBoxClicked;
    
    public final static Object TODOTYPE_NORMAL = "TODOTYPE_NORMAL";
    
    private int TODO_RED = 600;
    private int TODO_YELLOW = 1600;
    private int TODO_GREEN = 5000;
    
    private List currentTODOsSorted;
    
    public String getFieldName()
    {
        return "Aufgabenliste";    
    }
    
    public String getFieldDescription()
    {
        return "Ein Feld zur Auflistung von Aufgaben";
    }
    
    
    
    
    public boolean isWriteOnly()
    {
        return true;  
    }
    
    public int getContext()
    {
        return CONTEXT_USER;
    }  
    
    public EntryLayoutHelper getPanel(String widgetFlags)
    {
        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIcon_Level_0 = (ImageIcon)iconFactory.getIcon("level_0.gif"); 
        m_oIcon_Level_25 = (ImageIcon)iconFactory.getIcon("level_25.gif"); 
        m_oIcon_Level_50 = (ImageIcon)iconFactory.getIcon("level_50.gif"); 
        m_oIcon_Level_75 = (ImageIcon)iconFactory.getIcon("level_75.gif"); 
        m_oIcon_Level_100 = (ImageIcon)iconFactory.getIcon("level_100.gif"); 
        m_oIcon_Unknown = (ImageIcon)iconFactory.getIcon("history_appointment.gif");    
        
        m_oIcon_Traffic_RED = (ImageIcon)iconFactory.getIcon("IMAGE19.GIF");
        m_oIcon_Traffic_YELLOW = (ImageIcon)iconFactory.getIcon("IMAGE18.GIF");
        m_oIcon_Traffic_GREEN = (ImageIcon)iconFactory.getIcon("IMAGE17.GIF");
        
        m_oIcon_Jump = (ImageIcon)iconFactory.getIcon("jump.gif"); 
        m_oIcon_JumpDay = (ImageIcon)iconFactory.getIcon("jump_day.gif"); 
        m_oIcon_JumpWeek = (ImageIcon)iconFactory.getIcon("jump_week.gif"); 
        m_oIcon_JumpMonth = (ImageIcon)iconFactory.getIcon("jump_month.gif"); 
        m_oIcon_Edit = (ImageIcon)iconFactory.getIcon("schedule_edit.gif"); 
        
        getAppointmentDisplayManager().addAppointmentDisplayListener(this);
        
        if(m_oSchedulePanel == null) m_oSchedulePanel = createPanel(widgetFlags);
        return m_oSchedulePanel;
    }
    
    public void postFinalRealize()
    {    
    }  
    
    public void setData(PluginData data)
    {
        //fillToDoList();
    }
    
    public void getData(PluginData data)
    {
    }
    
    public boolean isDirty(PluginData data)
    {
        return false;
    }
    
    public void setEditable(boolean iseditable)
    {
    }
    
    public String getKey()
    {
        return "TODOLIST";
    }
   
    public void setDoubleCheckSensitive(boolean issensitive)
    {
    }
    
    public List getCurrentTodos(){
        return currentTODOsSorted;
    }
    
    private TarentWidgetComponentTable.ObjectTableModel m_oTableModel;
    
    private EntryLayoutHelper createPanel(String widgetFlags)
    {
        TarentWidgetPanel panel = new TarentWidgetPanel(); 
        panel.setLayout(new BorderLayout());
        
        // tabelle erzeugen...
        m_oTableModel = new TarentWidgetComponentTable.ObjectTableModel(5);    
        
        TableSorter sorter = new TableSorter(m_oTableModel); //ADDED THIS
        
        m_oTableModel.setColumnName(0, "Dringlichkeit");    
        m_oTableModel.setColumnEditable(0, false);
        
        m_oTableModel.setColumnName(1, "Status");    
        m_oTableModel.setColumnEditable(1, false);
        
        m_oTableModel.setColumnName(2, "Erledigt");    
        m_oTableModel.setColumnEditable(2, true);
        
        m_oTableModel.setColumnName(3, "Zeit");    
        m_oTableModel.setColumnEditable(3, false);
        
        m_oTableModel.setColumnName(4, "Titel");    
        m_oTableModel.setColumnEditable(4, false);
        
        m_oCompTable_ToDo = new TarentWidgetComponentTable(m_oTableModel);
        m_oCompTable_ToDo.getJTable().getSelectionModel().addListSelectionListener(new ToDoListSelectionListener());    
        m_oCompTable_ToDo.getJTable().setRowHeight(20);
        m_oCompTable_ToDo.getJTable().setCellSelectionEnabled(false);
        m_oCompTable_ToDo.getJTable().setRowSelectionAllowed(true);    
        m_oCompTable_ToDo.getJTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        m_oCompTable_ToDo.setFixedColumnSize(0, 40);
        m_oCompTable_ToDo.setFixedColumnSize(1, 40);
        m_oCompTable_ToDo.setFixedColumnSize(2, 40);
        m_oCompTable_ToDo.setColumnSize(3, 300);
        
        MouseListener popupListener = new PopupListener();
        m_oCompTable_ToDo.getJTable().addMouseListener(popupListener);
        
        JCheckBox checkbox1 = new JCheckBox();
        checkbox1.setBackground(Color.WHITE);
        //checklaf.touch(checkbox1);    
        m_oCompletedCheck = new JCheckBox();
        m_oCompletedCheck.setBackground(Color.WHITE);
        //checklaf.touch(checkbox2);    
        m_oCheckBoxClicked = new CheckBoxClicked();    
        m_oCompletedCheck.addActionListener(m_oCheckBoxClicked);
        m_oCompTable_ToDo.setColumnComponent(0, new ImageIcon(), new ImageIcon());
        m_oCompTable_ToDo.setColumnComponent(1, new ImageIcon(), new ImageIcon());
        m_oCompTable_ToDo.setColumnComponent(2, checkbox1, m_oCompletedCheck);
        m_oCompTable_ToDo.setColumnComponent(3, new JTextField(), new JTextField());
        m_oCompTable_ToDo.setColumnComponent(4, new JTextField(), new JTextField());
        
        JPanel combopanel = new JPanel(new GridLayout(1,2));
        m_oIconComboBox_Filter1 = new TarentWidgetIconComboBox();
        m_oIconComboBox_Filter2 = new TarentWidgetIconComboBox();
        
        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_alltime.gif"), "alle", TODOFILTER_TIME_ALL));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_alltime.gif"), "alle zuknftigen", TODOFILTER_TIME_ALL_FUTURE));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("calendar_next.gif"), "---", null));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_today.gif"), "heute", TODOFILTER_TIME_TODAY));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "diese Woche", TODOFILTER_TIME_THIS_WEEK));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_month.gif"), "diesen Monat", TODOFILTER_TIME_THIS_MONTH));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("calendar_next.gif"), "---", null));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "7 Tage", TODOFILTER_TIME_WEEK));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "14 Tage", TODOFILTER_TIME_TWO_WEEKS));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "21 Tage", TODOFILTER_TIME_THREE_WEEKS));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("calendar_next.gif"), "---", null));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_view.gif"), "Kalenderansicht", TODOFILTER_TIME_SCHEDULE));

        //-----------------------------------------------------------------------------------
        
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_all.gif"), "alle", TODOFILTER_PRIORITY_ALL));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_lopri.gif"), "niedrige Priorit�t", TODOFILTER_PRIORITY_LOW));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_midpri.gif"), "mittlere Priorit�t", TODOFILTER_PRIORITY_MID));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_hipri.gif"), "hohe Priorit�t", TODOFILTER_PRIORITY_HIGH));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_private.gif"), "privat", TODOFILTER_PRIORITY_PRIVATE));
        
        
        FilterComboChanged combolistener = new FilterComboChanged();
        m_oIconComboBox_Filter1.addActionListener(combolistener);    
        m_oIconComboBox_Filter2.addActionListener(combolistener);    
        
        combopanel.add(m_oIconComboBox_Filter1);
        combopanel.add(m_oIconComboBox_Filter2);
        
        JLabel label = new JLabel("Aufgaben: ");
        label.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
        panel.add(label, BorderLayout.NORTH);        
        
        JPanel firstlinepanel = new JPanel(new BorderLayout());
        firstlinepanel.add(combopanel, BorderLayout.CENTER);
        firstlinepanel.add(label, BorderLayout.WEST);
        
        JPanel mainpanel = new JPanel(new BorderLayout());
        mainpanel.add(firstlinepanel, BorderLayout.NORTH);        
        mainpanel.add(m_oCompTable_ToDo, BorderLayout.CENTER);        
        
        panel.add(mainpanel, BorderLayout.CENTER);        
        panel.setMinimumSize(new Dimension(5,5));
        
        getWidgetPool().addController("CONTROLER_TODOLIST", this);
        
        m_bUseFilterComboChanged = false;    
        m_oIconComboBox_Filter1.setSelectedItemByKey(TODOFILTER_TIME_ALL_FUTURE);
        m_oIconComboBox_Filter2.setSelectedItemByKey(TODOFILTER_PRIORITY_ALL);
        m_bUseFilterComboChanged = true;    
        
        return(new EntryLayoutHelper(new TarentWidgetInterface[] { panel }, widgetFlags));
    }
    
    
    private boolean m_bUseFilterComboChanged = true;
    private class FilterComboChanged implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (m_bUseFilterComboChanged)
            {          
                fillToDoList();
            }
        }
    }
    
    public boolean isInDisplayedInterval(Date date)
    {
        ScheduleDate[] interval = getDisplayedDateInterval();
        ScheduleDate testdate = new ScheduleDate(date);    
        return (interval[0].before(testdate) && interval[1].after(testdate)) || interval[0].equals(testdate) || interval[1].equals(testdate);
    }
    
    
    
    public void setDateInterval(ScheduleDate date, ScheduleDate enddate)
    {
        if ( (!(date.equals(m_oFirstDay))) || (!(enddate.equals(m_oEndDay))) )
        {  
            m_oFirstDay = date;
            m_oEndDay = enddate;

            // Update der Liste ist nur dann n�tig, 
            // wenn diese auf "Kalenderansicht" steht, 
            // da sie sonst vom Kalenderzeitraum unabh�ngig ist.
            Object timefilterkey = getTimefilterkey();            
            if (TODOFILTER_TIME_SCHEDULE.equals(timefilterkey))
                fillToDoList();
        }
    }   
    
    private Object getTimefilterkey() {
        return ((TarentWidgetIconComboBox.IconComboBoxEntry)m_oIconComboBox_Filter1.getSelectedItem()).getKey();
    }
    
    private ScheduleDate[] getDisplayedDateInterval()
    {
        ScheduleDate[] interval = new ScheduleDate[2]; 
        
        Object timefilterkey = getTimefilterkey();
        ScheduleDate startdate = m_oFirstDay;
        ScheduleDate enddate = m_oEndDay;
        
        if (TODOFILTER_TIME_ALL.equals(timefilterkey))
        {      
            startdate = new ScheduleDate(0,0,0);
            enddate = new ScheduleDate(0,0,9999);
        }
        else if (TODOFILTER_TIME_ALL_FUTURE.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = new ScheduleDate(0,0,9999);
        }
        else if (TODOFILTER_TIME_TODAY.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(1);
        }
        else if (TODOFILTER_TIME_THIS_WEEK.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getMondayOfWeek().getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(7);
        }
        else if (TODOFILTER_TIME_THIS_MONTH.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstDayOfMonth().getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedMonths(1);
        }
        else if (TODOFILTER_TIME_WEEK.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(7);
        }
        else if (TODOFILTER_TIME_TWO_WEEKS.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(14);
        }
        else if (TODOFILTER_TIME_THREE_WEEKS.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(21);
        }
        else if (TODOFILTER_TIME_SCHEDULE.equals(timefilterkey))
        {      
            if ((m_oFirstDay != null) && (m_oEndDay != null))
            {        
                startdate = m_oFirstDay;
                enddate = m_oEndDay;
            }
            else
            {
                getGUIListener().getLogger().severe("ToDoListField: es wurden noch nie Start und Enddatum des TerminPanels gesetzt!");
                startdate = (new ScheduleDate()).getFirstSecondOfDay();
                enddate = startdate.getDateWithAddedDays(1);        
            }
        }    
        
        interval[0] = startdate;
        interval[1] = enddate;
        return interval;
    }
    
    
    
    
    private int getSelectedAppointmentID()
    {
        int id = -1;
        int row = m_oCompTable_ToDo.getJTable().getSelectedRow();
        if (row != -1)
        {
            Appointment appointment = (Appointment)(m_oCompTable_ToDo.getRowKey(row));
            if (appointment != null)
            {  
                id = appointment.getId();
            }
        }
        return id;
    }
    
    
    private boolean selectAppointmentByID(int id)
    {
        int row = getRowWithAppointmentID(id);
        if (row != -1)
        {
            m_oCompTable_ToDo.getJTable().getSelectionModel().setSelectionInterval(row, row);
            return true;
        }
        else
        {
            m_oCompTable_ToDo.getJTable().getSelectionModel().clearSelection();
            return false;
        }
    }
    
    
    public void fillToDoList()
    {
        Object timefilterkey = ((TarentWidgetIconComboBox.IconComboBoxEntry)m_oIconComboBox_Filter1.getSelectedItem()).getKey();
        Object priorityfilterkey = ((TarentWidgetIconComboBox.IconComboBoxEntry)m_oIconComboBox_Filter2.getSelectedItem()).getKey();
        
        ScheduleDate[] interval = getDisplayedDateInterval();
        ScheduleDate startdate = interval[0];
        ScheduleDate enddate = interval[1];
        
        boolean bMustBePrivat = false;
        int iPriority = -1;        
        int selectedid = getSelectedAppointmentID();    
        
        if (TODOFILTER_PRIORITY_ALL.equals(priorityfilterkey))
        {      
            iPriority = -1;      
        }
        else if (TODOFILTER_PRIORITY_LOW.equals(priorityfilterkey))
        {      
            iPriority = 1;     
        }
        else if (TODOFILTER_PRIORITY_MID.equals(priorityfilterkey))
        {      
            iPriority = 2;      
        }
        else if (TODOFILTER_PRIORITY_HIGH.equals(priorityfilterkey))
        {      
            iPriority = 3;      
        }
        else if (TODOFILTER_PRIORITY_PRIVATE.equals(priorityfilterkey))
        {      
            bMustBePrivat = true;      
        }
        
        m_oCompTable_ToDo.removeAllData();
        if (getGUIListener().getCalendarCollection() == null) return; //May bug out... 
        
        // Check Time left to end of TODOs
        Environment env = ConfigManager.getEnvironment();
        TODO_RED = env.getAsInt(Key.TODO_RED, 1);
        TODO_YELLOW = env.getAsInt(Key.TODO_YELLOW, 2);
        TODO_GREEN = env.getAsInt(Key.TODO_GREEN, 3);
        
        getGUIListener().setWaiting(true);
        try
        {
            Collection appointments = getAppointments(startdate.getDate(), enddate.getDate());
                            
            List sortlist = new ArrayList(appointments);
            Comparator comparator = new AppointmentComparator();
            Collections.sort(sortlist, comparator);
            
            // Save for Export
            currentTODOsSorted = new ArrayList();
            
            Iterator appointmentiterator = sortlist.iterator();
            while(appointmentiterator.hasNext())
            {
                Appointment appointment = (Appointment)(appointmentiterator.next());
                int category = appointment.getAppointmentCategory();
                String subject = appointment.getAttribute(Appointment.KEY_SUBJECT);
                String description = appointment.getAttribute(Appointment.KEY_DESCRIPTION);
                Date start = appointment.getStart();
                Date end = appointment.getEnd();
                int priority = appointment.getPriority();
                boolean privat = appointment.isPrivat(); 
                boolean status = appointment.isJobCompleted(); 
                int completion = appointment.getJobCompletion();
                
                boolean mustdisplay = true;
                
                if (bMustBePrivat && !privat) mustdisplay = false;
                else
                {
                    if (iPriority != -1)
                    {
                        if (iPriority != priority) mustdisplay = false;
                    }
                }
                
                if (!(appointment.isJob())) mustdisplay = false;
                
                if (mustdisplay)
                { 
                    // Prepare new Container
                    TODOPrintContainer TODOPC = new  TODOPrintContainer();
                    String Ort = appointment.getAttribute(Appointment.KEY_LOCATION).trim();
                    if (Ort.equals("")){
                        Ort = "Keine Ortsangabe";
                        TODOPC.set_ort(" - ");
                    }else{
                        TODOPC.set_ort(Ort);
                    }
                    TODOPC.set_status(completion);
                    TODOPC.set_titel(subject);
                    TODOPC.set_prio(priority);
                    ImageIcon icon = m_oIcon_Unknown;
                    String typetext = "zu " + completion + "% abgeschlossene Aufgabe";
                    
                    if         (completion <  0 + 12) icon = m_oIcon_Level_0; 
                    else if (completion < 25 +12) icon = m_oIcon_Level_25; 
                    else if (completion < 50 + 12) icon = m_oIcon_Level_50; 
                    else if (completion < 75 + 12) icon = m_oIcon_Level_75;
                    else icon = m_oIcon_Level_100; 
                    
                    //String startdatestr = (myDateTimeFormatter.format(start)); 
                    String enddatestr = (myDateTimeFormatter.format(end));
                    TODOPC.set_faelligkeit(myLongDateTimeFormatter.format((end)));
                    TODOPC.set_monat(myMonthTimeFormatter.format(end));
                    TODOPC.set_kw(Integer.parseInt(myKWTimeFormatter.format(end)));
                    String longdatestr = (myLongDateTimeFormatter.format(start));
                    
                    
                    String users = "";
                    Collection addresses = appointment.getAddresses();
                    if ((addresses != null) && (m_bShowAddressesInTooltip))
                    {
                        if (addresses.size() > 0)
                        {                   
                            if (addresses.size() <= m_iMaxAddressesInTooltip)
                            {                   
                                Iterator addressiterator = addresses.iterator();
                                while(addressiterator.hasNext())
                                {
                                    Address address = (Address)(addressiterator.next());
                                    users += ("<b>*</b> " + address.getShortAddressLabel() + "\n");
                                }
                                if (users.endsWith("\n")) users = users.substring(0, users.length()-1);
                                users = "<b>Teilnehmer:</b><br>" + users.replaceAll("\n" , "<br>") + "<br>";
                            }
                            else
                            {
                                users = "<b>Teilnehmer:</b><br>insgesamt " + addresses.size() + " Adressen zugeordnet.<br>";                      
                            }
                        }
                    }
                    
                    
                    String priotext = "Priorit�t: " + priority + "<br>";
                    String privattext = privat ? "ist privat" : "";                 
                    
                    String encodeddescription;
                    if (description.trim().length() > 0)
                    {  
                        encodeddescription = description.trim().replaceAll("\n" , "<br>");
                    }
                    else
                    {
                        encodeddescription = "";
                    }
                    
                    String tooltiptext = "";
                    if (privat && ( appointment.getOwner().equals(getGUIListener().getUser(null)) == false)){
                        subject = ConfigManager.getAppearance().get(Appearance.Key.PRIVATE_TODO);
                        TODOPC.set_titel(subject);
                        TODOPC.set_ort(" - ");
                        tooltiptext = "<html>" + longdatestr + "<br><br><b>"+ subject + "</b><br><br>" + privattext + "</html>";
                    }else{
                        tooltiptext = "<html><i>" + typetext + "</i><br>" + longdatestr + "<br><br><b>" + subject + "</b><br><br>" + encodeddescription + "<br>" + users + "<br>" + priotext + privattext + "</html>";
                    }
                    
                    
                    
                    String[] tooltips = new String[5];
                    tooltips[0] = tooltiptext;
                    tooltips[1] = tooltiptext;
                    tooltips[2] = tooltiptext;
                    tooltips[3] = tooltiptext;
                    tooltips[4] = tooltiptext;
                    
                    // Color?
                    ImageIcon trafficIcon = null;
                    
                    Color backColor = null;
                    int mins_diff = (int) ((start.getTime() - new Date().getTime()) / (1000 * 60));
                    if (mins_diff >= TODO_GREEN){
                        trafficIcon = m_oIcon_Traffic_GREEN;
                    }else if (mins_diff >= TODO_YELLOW){
                        trafficIcon = m_oIcon_Traffic_YELLOW;
                    }else{
                        trafficIcon = m_oIcon_Traffic_RED;
                    }
                    // Update Export
                    currentTODOsSorted.add(TODOPC);
                    
                    insertToDoEntry(trafficIcon, icon, status, subject, enddatestr, appointment, tooltips, backColor);            
                }                       
            }
        } 
        catch (Exception e)
        {
            logger.severe("[ToDoListField]: filling of ToDo-list failed",e);
        }
        finally
        {
            getGUIListener().setWaiting(false);
        }
        
        m_oTableModel.fireTableDataChanged();
        m_oCompTable_ToDo.getJTable().revalidate();
        
        //if (m_oCompTable_ToDo.getNumberOfEntries() > 0) m_oCompTable_ToDo.initColumnSizes(0);
        useSelectionListener = false;
        selectAppointmentByID(selectedid);        
        useSelectionListener = true;
    }  
    
    private void insertToDoEntry(ImageIcon trafficIcon, ImageIcon icon, boolean status, String name, String date, Object key, String[] tooltips, Color color)
    {
        Object[] entry = createEntry(trafficIcon, icon, status, name, date);        
        if (entry != null) 
        {    
            m_oCompTable_ToDo.addData(entry, tooltips);      
            m_oCompTable_ToDo.setRowKey(m_oCompTable_ToDo.getNumberOfEntries() - 1, key);
        }
    }
    
    private Object[] createEntry(ImageIcon trafficIcon, ImageIcon icon, boolean status, String name, String date)
    {
        Object[] objects = new Object[5]; 
        objects[0] = trafficIcon;
        objects[1] = icon;
        objects[2] = new Boolean(status);
        objects[3] = date;
        objects[4] = name;
        
        return(objects);
    }
    
    
    private int getRowWithAppointmentID(int id)
    {
        for(int i=0; i<(m_oTableModel.getRowCount()); i++)
        {
            Appointment appointment = (Appointment)(m_oTableModel.getRowKey(i));
            if (appointment.getId() == id) return i;
        }
        return -1;
    }
    
    
    private void updateAppointmentOfRow(int row, Appointment appointment)
    {
        m_oTableModel.setRowKey(row, appointment);
    }
    
    public void updateAppointment(Appointment appointment)
    {
        if (appointment != null)
        {        
            int row = getRowWithAppointmentID(appointment.getId());
            if (row != -1)
            {
                updateAppointmentOfRow(row, appointment);
            }
        }
    }
    
    
    public void changedAppointment(Appointment appointment)
    {
        updateAppointment(appointment);
        
        int row = getRowWithAppointmentID(appointment.getId());
        //if (row != -1)
        //{
        // ver�ndertes Appointment wird momentan angezeigt... neu laden!
        fillToDoList();
        //}
    }
    
    
    
    
    
    public void addedAppointment(Appointment appointment)
    {
        fillToDoList();
    }
    
    public void removedAppointment(Appointment appointment)
    {
        fillToDoList();
    }
    
    private boolean useSelectionListener = true;
    
    public void selectedAppointment(Appointment appointment)
    {    
        int row = getRowWithAppointmentID(appointment.getId());
        if (row != -1)
        {
            useSelectionListener = false;
            m_oCompTable_ToDo.getJTable().getSelectionModel().setSelectionInterval(row, row);
            useSelectionListener = true;
        }
        else
        {
            m_oCompTable_ToDo.getJTable().clearSelection();      
        }    
    }
    
    
    public Appointment getAppointment(int appointmentid)
    {
        int row = getRowWithAppointmentID(appointmentid);
        if (row != -1)
        {
            Appointment appointment = (Appointment)(m_oTableModel.getRowKey(row));
            return appointment;      
        }
        return null;
    }
    
    
    public void setViewType(String viewtype)
    {
        // not interested in this...
    }
    
    public void setVisibleInterval(ScheduleDate start, ScheduleDate end)
    {
        setDateInterval(start, end);  
    }
    
    private class ToDoListSelectionListener implements ListSelectionListener
    {
        public void valueChanged(ListSelectionEvent e)
        {
            if (useSelectionListener)
            {  
                int row = m_oCompTable_ToDo.getJTable().getSelectedRow();
                if (row != -1)
                {
                    Appointment appointment = (Appointment)(m_oCompTable_ToDo.getRowKey(row));
                    if (appointment != null)
                    {  
                        getAppointmentDisplayManager().fireSelectedAppointment(appointment, m_oThisToDoListField);
                    }
                }
            }
        }
    }
    
    
    
    private class CheckBoxClicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            JCheckBox check = ((JCheckBox)(e.getSource())); 
            String name = check.getName();
            String[] coords = name.split(",");
            int row = -1;
            int column = -1;
            try
            {
                row = Integer.parseInt(coords[0]);
                column = Integer.parseInt(coords[1]);
            }
            catch(NumberFormatException nfe) {}
            
            Appointment appointment = (Appointment)(m_oCompTable_ToDo.getRowKey(row));
            boolean selected = check.isSelected();
            m_oCompTable_ToDo.getJTable().getModel().setValueAt(new Boolean(selected), row, column);
            
            //TODO:
            //System.out.println("CheckBoxClicked() appointment=" + appointment + " ******** selected=" + selected);
            try
            {
                appointment.setJobCompleted(selected);
                //appointment.commit();
                getAppointmentDisplayManager().fireChangedAppointment(appointment, m_oThisToDoListField);
            } 
            catch (ContactDBException e1)
            {
                e1.printStackTrace();
            }
        }
    }
    
    
    //TODO: Sortierung
    private void sortByColumn(int column, boolean ascending)
    {
        getGUIListener().getLogger().info("ToDo-List sortByColumn(" + column + ", " + ascending + ")");
    }
    
    private class TableHeaderMouseListener extends MouseAdapter
    {
        public void mouseClicked(MouseEvent e) 
        {
            TableColumnModel columnModel = m_oCompTable_ToDo.getJTable().getColumnModel();
            int viewColumn = columnModel.getColumnIndexAtX(e.getX());
            int column = m_oCompTable_ToDo.getJTable().convertColumnIndexToModel(viewColumn);
            if (e.getClickCount() == 1 && column != -1) 
            {
                //System.out.println("Sorting ...");
                int shiftPressed = e.getModifiers()&InputEvent.SHIFT_MASK;
                boolean ascending = (shiftPressed == 0);
                sortByColumn(column, ascending);
            }
        }
    }
    
    public void changedCalendarCollection()
    {
        fillToDoList();    
    }
    
    
    
    // PopUpMenu stuff...
    
    private JPopupMenu createPopUpMenu(Appointment appointment)
    {
        JPopupMenu oPopupMenu = new JPopupMenu();
        
        
        JMenuItem menuItemEDIT = new JMenuItem("<html>Aufgabe bearbeiten</html>");          
        menuItemEDIT.setIcon(m_oIcon_Edit);
        menuItemEDIT.addActionListener(new MenuItem_EDIT_clicked(appointment));
        oPopupMenu.add(menuItemEDIT);
        
        JMenu oMenuJump = new JMenu("<html>springe zu Aufgabe</html>");
        oMenuJump.setIcon(m_oIcon_Jump);
        
        JMenuItem menuItemJUMPDAY = new JMenuItem("<html>in der Tagesbersicht</html>");          
        menuItemJUMPDAY.setIcon(m_oIcon_JumpDay);
        menuItemJUMPDAY.addActionListener(new MenuItem_JUMP_clicked(appointment, ScheduleField.CARD_DAY));
        oMenuJump.add(menuItemJUMPDAY);
        
        JMenuItem menuItemJUMPWEEK = new JMenuItem("<html>in der Wochenbersicht</html>");          
        menuItemJUMPWEEK.setIcon(m_oIcon_JumpWeek);
        menuItemJUMPWEEK.addActionListener(new MenuItem_JUMP_clicked(appointment, ScheduleField.CARD_WEEK));
        oMenuJump.add(menuItemJUMPWEEK);
        
        JMenuItem menuItemJUMPMONTH = new JMenuItem("<html>in der Monatsbersicht</html>");          
        menuItemJUMPMONTH.setIcon(m_oIcon_JumpMonth);
        menuItemJUMPMONTH.addActionListener(new MenuItem_JUMP_clicked(appointment, ScheduleField.CARD_MONTH));
        oMenuJump.add(menuItemJUMPMONTH);
        
        oPopupMenu.add(oMenuJump);
        
        return oPopupMenu;
    }
    
    
    private class MenuItem_JUMP_clicked implements ActionListener
    {    
        private Appointment m_oAppointment;
        private String m_oViewType;
        
        public MenuItem_JUMP_clicked(Appointment appointment, String viewtype)
        {
            m_oAppointment = appointment;
            m_oViewType = viewtype;
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            if (getScheduleField() != null) 
            {
                jumpToDate(m_oAppointment, m_oViewType);
            }
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    private class MenuItem_EDIT_clicked implements ActionListener
    {    
        private Appointment m_oAppointment;
        
        public MenuItem_EDIT_clicked(Appointment appointment)
        {
            m_oAppointment = appointment;
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            if (m_oAppointment != null)
            {  
                getAppointmentDisplayManager().fireSelectedAppointment(m_oAppointment, m_oThisToDoListField);
            }
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    
    class PopupListener extends MouseAdapter 
    {
        public void mousePressed(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        public void mouseReleased(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) 
        {
            if (e.isPopupTrigger()) 
            {
                Point p = e.getPoint();
                int row = m_oCompTable_ToDo.getJTable().rowAtPoint(p);
                int visiblecol = m_oCompTable_ToDo.getJTable().columnAtPoint(p);
                int col = m_oCompTable_ToDo.getJTable().convertColumnIndexToModel(visiblecol);        
                
                if (row != -1)
                {
                    Appointment appointment = (Appointment)(m_oCompTable_ToDo.getRowKey(row));
                    if (appointment != null)
                    {  
                        JPopupMenu menu = createPopUpMenu(appointment);         
                        menu.show(e.getComponent(), e.getX(), e.getY());                        
                    }
                }
                
            }
            
            else if(e.getClickCount() == 2){
                
                Point p = e.getPoint();
                int row = m_oCompTable_ToDo.getJTable().rowAtPoint(p);
                int visiblecol = m_oCompTable_ToDo.getJTable().columnAtPoint(p);
                int col = m_oCompTable_ToDo.getJTable().convertColumnIndexToModel(visiblecol);        
                
                if (row != -1)
                {
                    Appointment appointment = (Appointment)(m_oCompTable_ToDo.getRowKey(row));
                    if (appointment != null)
                    {  
                        jumpToDate(appointment,getScheduleField().getCardVisible());
                    }
                }
            }
                
        }
    }
    
    
    
    // ---------------------------------------------------------------------------------------------
    
    private ScheduleField m_oScheduleField = null;
    
    private ScheduleField getScheduleField()
    {
        if (m_oScheduleField == null) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_SCHEDULEPANEL");
            if (controler instanceof ScheduleField)
            {  
                m_oScheduleField = (ScheduleField)controler;
            }
        }
        return m_oScheduleField;
    }
    
    
    public void jumpToDate(Appointment appointment, String viewtype)
    {
        try
        {
            ScheduleDate date = new ScheduleDate(appointment.getStart());    
            ScheduleDate oEndDate = date;
            if (getScheduleField() != null)
            {  
                if (ScheduleField.CARD_DAY.equals(viewtype)) oEndDate = date.getDateWithAddedDays(1);  
                else if (ScheduleField.CARD_MONTH.equals(viewtype)) oEndDate = date.getDateWithAddedMonths(1);  
                else if (ScheduleField.CARD_WEEK.equals(viewtype)) 
                {
                    date = date.getMondayOfWeek();
                    oEndDate = date.getDateWithAddedDays(7); 
                }
                
                getAppointmentDisplayManager().fireSetViewType(viewtype, this);
                getAppointmentDisplayManager().fireSetVisibleInterval(date, oEndDate, this);
                getAppointmentDisplayManager().fireSelectedAppointment(appointment, this);
            }    
        }
        catch (ContactDBException e1)
        {
            e1.printStackTrace();
        }
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridActive(boolean)
     */
    public void setGridActive(boolean usegrid)
    {
        // TODO Auto-generated method stub
        
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridHeight(int)
     */
    public void setGridHeight(int seconds)
    {
        // TODO Auto-generated method stub
        
    }

    /** Handles export event.*/
    public void clickedExportButton() {
    }
    
    /** Handles new appointment event.*/
    public void clickedNewAppointmentButton() {
    }

    /** Handles new task event.*/
    public void clickedNewTaskButton() {
    }
    
}
