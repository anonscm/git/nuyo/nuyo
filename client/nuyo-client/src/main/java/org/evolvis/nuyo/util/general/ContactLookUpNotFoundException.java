/* $Id: ContactLookUpNotFoundException.java,v 1.1 2005/11/21 10:13:03 nils Exp $
 * 
 * Created on 10.09.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.util.general;


/**
 * Diese Klasse stellt Contact-Client-interne Ausnahmen dar, die im
 * Controller-Kontext auftreten.
 *  
 * @author mikel
 */
public class ContactLookUpNotFoundException extends Exception 
{
  public ContactLookUpNotFoundException()
  {    
  }
  
  public ContactLookUpNotFoundException(int errorcode)
  {    
  }
    /*
     * Fehlerkonstanten
     */
    public final static int EX_LOOKUP_NOT_FOUND = 1;
}
