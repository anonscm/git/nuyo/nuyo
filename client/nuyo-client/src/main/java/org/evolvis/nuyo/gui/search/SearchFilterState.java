package org.evolvis.nuyo.gui.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;

import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.groupware.AddressProperty;

import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.commons.datahandling.ListFilterPropertyName;
import de.tarent.commons.utils.SerializationHelper;

/**
 * Denotes a state of the search filter panel.
 * 
 * <p>An instance of this class can be retrieved from the {@link SearchFilterPanel}
 * and later be put in to configure it.</p> 
 *
 */
public class SearchFilterState {

    /** Package private to allow read access in 
     * {@link SearchFilterPanel#setState(SearchFilterState)}.
     */
    List conditions;
    
    /**
     * Flag that is active for the first search condition restoration
     * after application startup. This effectively makes the application
     * lose the parameter values after restart which is desired.
     * 
     */
    private static boolean ignoreParams = true;
 
    SearchFilterState(List conditions){        
      this.conditions = conditions;
    }

    /** 
     * Returns a reverse polish notation of a given filter.
     * The filter notation elements follows in postfix order. 
     * For example "ab=" for "a = b" and "ac=ab&lt;OR" for "(a&lt;b) OR (a=c)".<p>
     * <p>
     * There are no duplicated conditions.
     * <p>
     * @return list of the following objects:{@link de.tarent.commons.datahandling.ListFilterPropertyName}, {@link String}, {@link de.tarent.commons.datahandling.ListFilterOperator}, that are filter operands and operators
     */
    private List getSearchFilterExpression() {
        List expressions = new ArrayList();
        HashMap groups = new HashMap();

        int conditionsCount = conditions.size();
        
        // Iterates through search conditions and groups them by their addressproperty
        for(int i = 0;i < conditionsCount; i++) {
            SearchCondition c= (SearchCondition) conditions.get(i);
            if(!c.isEmpty())
            	put(groups, c.getAttribute(), c);
        }
        
        Iterator groupIte = groups.keySet().iterator();
        if (groupIte.hasNext())
          handleGroupedSearchConditions(expressions, (List) groups.get(groupIte.next()));
        
        // The 2nd and latter expressions need an appended AND.
        while (groupIte.hasNext())
          {
        	handleGroupedSearchConditions(expressions, (List) groups.get(groupIte.next()));
        	expressions.add(ListFilterOperator.AND);
          }
        
        groups.clear();
        
        //debug(expressions);
            
        return expressions;
    }
    
    /**
     * Iterates through the list of search conditions, creates the appropriate expressions
     * for them and concatenates them with the OR operator.
     * 
     * @param expressions
     * @param searchConditions
     */
    private static void handleGroupedSearchConditions(List expressions, List searchConditions)
    {
      Iterator ite = searchConditions.iterator();
      if (ite.hasNext())
        {
    	  SearchCondition sc = (SearchCondition) ite.next();
  	      sc.getOperator().addExpression(expressions,
                    new ListFilterPropertyName(sc.getAttribute().getKey()),
                    sc.getParamText());
        }

      // The 2nd and latter expressions need an appended OR.
      while (ite.hasNext())
        {
  	      SearchCondition sc = (SearchCondition) ite.next();
  	      sc.getOperator().addExpression(expressions,
  	    		                         new ListFilterPropertyName(sc.getAttribute().getKey()),
  	    		                         sc.getParamText());
  	      expressions.add(ListFilterOperator.OR);
        }
    }
    
    /**
     * Convenience method that adds a {@link SearchCondition} instance to a list
     * which lives inside a map. The list accessed in the map through the address
     * property value of the search condition.
     * 
     * <p>In case the list does not yet exist, it will be created.</p>
     * 
     * <p>This effectively groups the search conditions by their address property
     * value which is neccessary to create ORed-expressions for those.</p>
     *  
     * @param groups
     * @param property
     * @param sc
     */
    private static void put(HashMap groups, AddressProperty property, SearchCondition sc)
    {
    	List l = (List) groups.get(property);
    	if (l == null)
    	{
    		l = new ArrayList();
    		groups.put(property, l);
    	}
    	l.add(sc);
    }
    
    /** 
     * Returns a string representation of a filter in a postfix notation.
     * It can be used for debugging. 
     */
    public String toString() {
        List notationElements = getSearchFilterExpression();
        StringBuffer buffer = new StringBuffer(notationElements.size() * 12);
        buffer.append(notationElements);
        return buffer.toString();
    }

    /**
     * Updates the filter expression of a AdressListParameters
     * instance.
     * 
     * @param o
     */
    public void updateAdressListParameters(AddressListParameter alp)
    {
      alp.setFilterList(getSearchFilterExpression());
    }
    
    /**
     * Retrieves a {@link SearchFilterState} instance by reading and
     * evaluating the value entries of the given {@link Preferences}
     * node.
     * 
     * <p>If no former instance has stored its values there the resulting
     * filter state will contain a default entry and nothing else.</p>
     * 
     * @param base
     * @return
     */
    public static SearchFilterState restoreFromPreferences(Preferences base)
    {
      LinkedList ll = new LinkedList();
      
      // Restores a list of operator names (e.g. "contains", "is_not", "date_is_greater_than"). These are the
      // values of the 'name' field in LabeledFilterOperator instances.
      Iterator operatorNames = SerializationHelper.deserializeList(base.getByteArray("operatorNames", null)).iterator();
      
      // Restores a list of parameter names (e.g. "M�ller", "Frank", "22.3.1981"). These are given
      // by the user.
      Iterator paramNames = (ignoreParams ? null : SerializationHelper.deserializeList(base.getByteArray("paramNames", null)).iterator());
      
      // Restores a list of attribute names (e.g. "nachname", "vorname", "geburtsdatum"). These are defined in 
      // the (de.tarent.groupware.)Address interface.
      Iterator attributeNames = SerializationHelper.deserializeList(base.getByteArray("attributeNames", null)).iterator();
      
      while (operatorNames.hasNext())
        {
          LabeledFilterOperator op = LabeledFilterOperator.lookup((String) operatorNames.next());
          String param = (ignoreParams ? "" : (String) paramNames.next());
          AddressProperty attribute = AddressProperty.lookup((String) attributeNames.next());
          
          ll.add(new SearchCondition(attribute, op, param));
        }
      
      // If nothing was restored provide a simple default.
      if (ll.isEmpty())
        {
          ll.add(new SearchCondition((AddressProperty) AddressProperty.getSearchProperties().get(0),
                                     LabeledFilterOperator.CONTAINS, ""));
        }
      
      // Ignoring of the parameter values should be done only one time after application startup. 
      ignoreParams = false;
      
      return new SearchFilterState(ll);
    }
    
    /**
     * Stores the current instance in the entries of the given {@link Preference}
     * node.
     * 
     * @param base
     */
    public void storeInPreferences(Preferences base)
    {
      /* How & why it is done this way:
       * The SearchFilterState instance maintains a list of 
       * SearchCondition objects which itself contain 3 important
       * objects.
       * 
       * At serialization time 3 lists are generated for each of
       * the 3 properties of the SearchCondition object.
       * 
       * The properties have the nice feature of being representable
       * as strings. For the operator and attribute names the deserializer
       * has access to lookup methods which return actual instances for
       * those strings. The third property is the search value itself
       * which is a string and needs no further handling.
       * 
       * Via Java's built-in serialization the three lists are converted
       * to byte arrays and are then stored in the Preferences.
       * 
       * The implementation was done this way to avoid overly customized
       * serialization code (readObject(), writeObeject()). With the exception
       * of the list2byte[] and byte[]2list conversion everything can be
       * found in one place and changed if needed.
       */
      
      Iterator ite = conditions.iterator();
      LinkedList operatorNames = new LinkedList();
      LinkedList paramNames = new LinkedList();
      LinkedList attributeNames = new LinkedList(); 
      
      while (ite.hasNext())
        {
          SearchCondition c = (SearchCondition) ite.next();
          
          operatorNames.add(c.getOperator().getName());
          paramNames.add(c.getParamText());
          attributeNames.add(c.getAttribute().getKey());
        }
      
      base.putByteArray("operatorNames", SerializationHelper.serializeList(operatorNames));
      base.putByteArray("paramNames", SerializationHelper.serializeList(paramNames));
      base.putByteArray("attributeNames", SerializationHelper.serializeList(attributeNames));
    }
    
    /**
     * A debug function which prints a search expression list.
     * 
     * <p>Use when in doubt. :)</p>
     * 
     * @param expressions
     */
    private static void debug(List expressions)
    {
    	System.err.println("SFS-debug: ");
    	for (Iterator ite = expressions.iterator(); ite.hasNext(); )
    	{
    		System.err.print(ite.next() + " ");
    	}
    	System.err.println();
    }
}
