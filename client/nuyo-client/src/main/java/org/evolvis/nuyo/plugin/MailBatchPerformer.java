/**
 * 
 */
package org.evolvis.nuyo.plugin;

import org.evolvis.nuyo.db.MailBatch;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface MailBatchPerformer
{
	public void setMailBatch(MailBatch pMailBatch, MailBatch.CHANNEL pChannel);
	public MailBatch getMailBatch();
	public void execute();
}
