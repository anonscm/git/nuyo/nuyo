/*
 * Created on 18.07.2003
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.ImageIcon;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

/**
 * @author niko
 *
  */
public class ScheduleTypes
{
  public final static Object CALL = "CALL";
  public final static Object MEETING = "MEETING";
  public final static Object TODO = "TODO";
  public final static Object HOLYDAY = "HOLYDAY";
  public final static Object BIRTHDAY = "BIRTHDAY";
  public final static Object SICK = "SICK";
  public final static Object EXTERN = "EXTERN";
  
  private Map scheduleTypeMap;
  
  public ScheduleTypes()
  {
    scheduleTypeMap = new HashMap();
    insertScheduleTypes();
  }
    
  public ScheduleType getDefaultScheduleType()
  {
    if (getNumberOfTypes() > 0) return getScheduleType(0);
    else return null;
  }
  
  public int getNumberOfTypes()
  {
    return(scheduleTypeMap.size());
  }
  
  public Map getScheduleTypeMap()
  {
    return scheduleTypeMap;
  }
  
  public ScheduleType getScheduleType(int key)
  {
    return (ScheduleType)(scheduleTypeMap.get(new Integer(key)));
  }

  public ScheduleType getScheduleType(Object externalkey)
  {
    Iterator it = scheduleTypeMap.values().iterator();
    while (it.hasNext())
    {  
      ScheduleType st = (ScheduleType)(it.next());
      if (st.getExternalKey().equals(externalkey)) return st;
    }
    return null;
  }

  private void insertScheduleType(ScheduleType scheduletype)
  {
    scheduleTypeMap.put(new Integer(scheduletype.getKey()), scheduletype);
  }
  
  private void insertScheduleTypes()
  {
    IconFactory iconFactory = SwingIconFactory.getInstance();
  	insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_MEETING, "Besprechung", "eine Besprechung.", (ImageIcon)iconFactory.getIcon("type_meeting.gif"), MEETING));
  	insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_TODO, "Aufgabe", "eine Aufgabe.", (ImageIcon)iconFactory.getIcon("type_meeting.gif"), TODO));
    insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_EXTERN, "Ausw�rts", "ist extern besch�ftigt.", (ImageIcon)iconFactory.getIcon("type_extern.gif"), EXTERN));
    insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_VACATION, "Urlaub", "Urlaubszeit.", (ImageIcon)iconFactory.getIcon("type_urlaub.gif"), HOLYDAY));
    insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_BIRTHDAY, "Geburtstag", "hat Geburtstag.", (ImageIcon)iconFactory.getIcon("type_birthday.gif"), BIRTHDAY));    
    insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_CALL, "Anruf", "ein Anruf mu� get�tigt werden.", (ImageIcon)iconFactory.getIcon("type_anruf.gif"), CALL));    
    insertScheduleType(new StandardScheduleType(Appointment.CATEGORY_SICK, "Krankheit", "ist krank.", (ImageIcon)iconFactory.getIcon("type_sick.gif"), SICK));
  }
}
