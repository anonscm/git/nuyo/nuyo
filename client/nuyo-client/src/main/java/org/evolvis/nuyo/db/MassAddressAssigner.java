package org.evolvis.nuyo.db;

import java.util.List;

import javax.swing.JOptionPane;

import org.evolvis.nuyo.gui.MassAssignmentErrorMsgPanel;
import org.evolvis.nuyo.remote.Method;


public class MassAddressAssigner implements Runnable{

	private final String KEY_NOTASSIGNABLEADDRESSES = "notAssingableAddresses";
	private final String KEY_NOTDEASSIGNABLEADDRESSES = "notDeassingableAddresses";
	private Boolean force = new Boolean(false);
	private List addressPks;
	private List subCategoriesToAdd;
	private List subCategoriesToRemove;
	
	private List notAssingableAddresses;
	private List notDeassingableAddresses;

	public MassAddressAssigner(List addressPks, List subCategoriesToAdd, List subCategoriesToRemove){
		this.addressPks = addressPks;
		this.subCategoriesToAdd = subCategoriesToAdd;
		this.subCategoriesToRemove = subCategoriesToRemove;
	}
	
	public void invoke(){
		invoke(false);
	}
	public void invoke(boolean ignoreError){
		

    	Method method = new Method("assignAddresses");
    	//put the parameters:
    	method.add("addressPks", addressPks);
    	method.add("addSubCategories", subCategoriesToAdd);
    	method.add("removeSubCategories", subCategoriesToRemove);
    	method.add("force", force);
    	try {
    		// TODO Progressbar anmachen
			method.invoke();
			// TODO Progressbar ausmachen
			notAssingableAddresses = (List) method.getORes().getData(KEY_NOTASSIGNABLEADDRESSES);
	    	notDeassingableAddresses = (List) method.getORes().getData(KEY_NOTDEASSIGNABLEADDRESSES);
	    	
		} catch (ContactDBException e) {
			JOptionPane.showMessageDialog(null,
				    "Fehler bei der Massenzuweisung",
				    "Bei der Massenzuweisung trat ein unerwwarteter Fehler auf.",
				    JOptionPane.ERROR_MESSAGE);
		}

		// If there are any blocked addresses popup errormessage
		if (!ignoreError && ((notAssingableAddresses != null && notAssingableAddresses.size() != 0)
			|| (notDeassingableAddresses != null && notDeassingableAddresses.size() != 0))){
			
			Object[] options = {"Ja", "Nein, Aktion abbrechen"};
			int retry = JOptionPane.showOptionDialog(null,
			new MassAssignmentErrorMsgPanel(notAssingableAddresses, notDeassingableAddresses),
			"Konflikte bei der Massenzuweisung",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.PLAIN_MESSAGE,
			null,
			options,  //the titles of buttons
			options[0]); //default button title
			
			if (retry == 0){
				force = new Boolean(true);
				invoke(true);
			}
			force = new Boolean(false);
		}	
	}

	public void run() {
		invoke();
	}
	
//	public static void main(String args[]){
//		List notAssingableAddresses = new LinkedList();
//		List notDeassingableAddresses = new LinkedList();
//		
//		AddressPreview pre = null;
//		try {
//			pre = new AddressPreview(new Integer(22), "test|test|testest|test|test|testest|");
//		} catch (PreviewFormatException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		for (int i = 0; i < 14; i++){
//			notAssingableAddresses.add(pre);
//			notDeassingableAddresses.add(pre);
//		}
//		Object[] options = {"Ja", "Nein, Aktion abbrechen"};
//		int retry = JOptionPane.showOptionDialog(null,
//		new MassAssignmentErrorMsgPanel(notAssingableAddresses, notDeassingableAddresses),
//		"Konflikte bei der Massenzuweisung",
//		JOptionPane.YES_NO_OPTION,
//		JOptionPane.PLAIN_MESSAGE,
//		null,
//		options,  //the titles of buttons
//		options[0]); //default button title
//		
//		System.out.println(retry);
//	}
}
