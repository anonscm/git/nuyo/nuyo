/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.plugins.calendar;

import java.awt.BorderLayout;
import java.awt.ScrollPane;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.TarentSinglePanel;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.gui.ContainerProvider;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.calendar.CalendarSettings;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.CalenderManager;

import de.tarent.commons.ui.EscapeDialog;


/**
 * The main calendar panel and container provider.<p>
 * Contains:
 * <ul>
 *  <li> 3 split panes: horizontal, vertical and right;  
 *  <li> and 5 containers: SHED{index}, index = [1..5].
 * </ul>
 * <p>
 * It also implements the {@link TarentCalendar} interface 
 * and can be getted from {@link CalendarPlugin} as supported interface implementation. 
 *  
 * @see ContainerProvider
 * @see TarentCalendar
 * @see CalenderPlugin
 *    
 * @author niko, aleksej (update)
 */
public class NewAppointmentPanel extends JPanel implements ContainerProvider, TarentCalendar {
  private static final long serialVersionUID = -6087311513626352317L;
  public final static Object CONTAINER_NEWHIST1 = "SHED1";
  public final static Object CONTAINER_NEWHIST2 = "SHED2";
  public final static Object CONTAINER_NEWHIST3 = "SHED3";
  public final static Object CONTAINER_NEWHIST4 = "SHED4";
  public final static Object CONTAINER_NEWHIST5 = "SHED5";
  
  private CalenderManager calendarEventsManager;
  private Object displayedAddress;

  // Der Actionmanager
  private GUIListener guiListener;
  
  private JSplitPane horizontalSplitPane;
  private JSplitPane verticalSplitPane;
  private JSplitPane rightSplitPane; 
  private SplitPaneComponentListener m_oSplitPaneComponentListenerA;
  private SplitPaneComponentListener m_oSplitPaneComponentListenerB;
  private SplitPaneComponentListener m_oSplitPaneComponentListenerC;
  
  private JPanel innerPanel;
  private JPanel rightPanel;
  
  private TarentSinglePanel panel1;
  private TarentSinglePanel panel2;
  private TarentSinglePanel panel3;
  private TarentSinglePanel panel4;
  private TarentSinglePanel panel5;
  
  private final Preferences panelsPreferences;
  private final CalendarsSelectionModel selectionModel;
  private JDialog dialog;

  public NewAppointmentPanel(CalenderManager aCalendarEventsManager, CalendarsSelectionModel aSelectionModel)
  {
    calendarEventsManager = aCalendarEventsManager;
    selectionModel = aSelectionModel;
    guiListener = ApplicationServices.getInstance().getActionManager(); 
        
    panelsPreferences = guiListener.getPreferences(StartUpConstants.PREF_PANELS_NODE_NAME);        
    
    setLayout(new BorderLayout()); 
            
    panel1 = new TarentSinglePanel();
    panel2 = new TarentSinglePanel();
    panel3 = new TarentSinglePanel();
    panel4 = new TarentSinglePanel();
    panel5 = new TarentSinglePanel();
        
    rightPanel = new JPanel(new BorderLayout());
    rightPanel.setBorder(new EmptyBorder(0,5,0,5));    
    rightSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panel2, panel5);
    rightSplitPane.setDividerLocation(0.5);
    rightSplitPane.setDividerSize(5);
    rightSplitPane.setContinuousLayout(true);
    rightPanel.add(rightSplitPane, BorderLayout.CENTER);
    
    panel1.setBorder(new EmptyBorder(0,0,0,5));    
    panel2.setBorder(new EmptyBorder(0,0,5,0));    
    panel5.setBorder(new EmptyBorder(5,0,0,0));        
    
    verticalSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, panel1, panel3);
    verticalSplitPane.setDividerLocation(0.5);
    verticalSplitPane.setDividerSize(5);
    verticalSplitPane.setContinuousLayout(true);
    
    horizontalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, verticalSplitPane, rightPanel);
    horizontalSplitPane.setDividerLocation(0.5);
    horizontalSplitPane.setDividerSize(5);
    horizontalSplitPane.setContinuousLayout(true);

    add(panel4, BorderLayout.NORTH);
    add(horizontalSplitPane, BorderLayout.CENTER);
  }
  
  // -----------------------------------------------------------------------------------

  public void postFinalRealize()
  {    
    double posA = panelsPreferences.getDouble(CalendarSettings.PREF_APPOITMENTS_PANEL_DIVIDER_A_KEY, 0.6);
    double posB = panelsPreferences.getDouble(CalendarSettings.PREF_APPOITMENTS_PANEL_DIVIDER_B_KEY, 0.6);
    double posC = panelsPreferences.getDouble(CalendarSettings.PREF_APPOITMENTS_PANEL_DIVIDER_C_KEY, 0.5);
    
    if (posA > 1.0) posA = 1.0;
    if (posB > 1.0) posB = 1.0;
    if (posC > 1.0) posC = 1.0;
    
    if (horizontalSplitPane != null) 
    {
      horizontalSplitPane.setDividerLocation(posA);
      if (m_oSplitPaneComponentListenerA == null)
      {
        m_oSplitPaneComponentListenerA = new SplitPaneComponentListener(horizontalSplitPane, CalendarSettings.PREF_APPOITMENTS_PANEL_DIVIDER_A_KEY);
        rightPanel.addComponentListener(m_oSplitPaneComponentListenerA);        
      }
    }
    
    if (verticalSplitPane != null)
    {
      verticalSplitPane.setDividerLocation(posB);
      if (m_oSplitPaneComponentListenerB == null)
      {
        m_oSplitPaneComponentListenerB = new SplitPaneComponentListener(verticalSplitPane, CalendarSettings.PREF_APPOITMENTS_PANEL_DIVIDER_B_KEY);
        panel1.addComponentListener(m_oSplitPaneComponentListenerB);        
      }
    }
    
    if (rightSplitPane != null)
    {
      rightSplitPane.setDividerLocation(posC);
      if (m_oSplitPaneComponentListenerC == null)
      {
        m_oSplitPaneComponentListenerC = new SplitPaneComponentListener(rightSplitPane, CalendarSettings.PREF_APPOITMENTS_PANEL_DIVIDER_C_KEY);
        panel2.addComponentListener(m_oSplitPaneComponentListenerC);        
      }
    }
  }
  

  private class SplitPaneComponentListener implements ComponentListener
  {
    private JSplitPane m_oSplitPane;
    private String m_sKey; 
    
    public SplitPaneComponentListener(JSplitPane split, String key)
    {
      m_oSplitPane = split;
      m_sKey = key;
    }
    
    public void componentHidden(ComponentEvent e) {}
    public void componentMoved(ComponentEvent e) {}
    public void componentShown(ComponentEvent e) {}
    
    public void componentResized(ComponentEvent e)
    {
      int location = m_oSplitPane.getDividerLocation();
      double loc = 0.5;
      
      if (m_oSplitPane.getOrientation() == JSplitPane.HORIZONTAL_SPLIT)
      {
        int width = m_oSplitPane.getWidth();
        if (width > 0)
        {  
          loc = ((double)location) / ((double)width);
        }        
      }
      else
      {
        int height = m_oSplitPane.getHeight();
        if (height > 0)
        {  
          loc = ((double)location) / ((double)height);
        }                
      }
      
      panelsPreferences.putDouble(m_sKey, loc);        
    }
  }
  
  
  // -----------------------------------------------------------------------------------
  
  public String getContainerProviderName()
  {
    return this.getClass().getName();     
  }
    
  public Map getContainers()
  {
    Map map = new HashMap();
    map.put(CONTAINER_NEWHIST1, panel1);
    map.put(CONTAINER_NEWHIST2, panel2);
    map.put(CONTAINER_NEWHIST3, panel3);
    map.put(CONTAINER_NEWHIST4, panel4);
    map.put(CONTAINER_NEWHIST5, panel5);
    return map;
  }
  
  // -----------------------------------------------------------------------------------
  
  public Object getDisplayedObject()
  {
    return displayedAddress;
  }

  public void setDisplayedObject(Object displayedobject)
  {
    displayedAddress = displayedobject;
  }

  //-----------------------------------------------
  // Wrapping methods for CalenderManager
  //-----------------------------------------------
  /* This wrapping is used to support a separate interface of calendar */

  // -----------------------------------------------------------------------------------
  public void addAppointmentDisplayListener(CalendarVisibleElement display)
  {
    calendarEventsManager.addAppointmentDisplayListener(display);
  }
  // -----------------------------------------------------------------------------------
  public void removeAppointmentDisplayListener(CalendarVisibleElement display)
  {
      calendarEventsManager.removeAppointmentDisplayListener(display);
  }
  // -----------------------------------------------------------------------------------
  /** Returns null if nothing found. */
  public Appointment askForAppointment(Integer appointmentID)
  {
      return calendarEventsManager.askForAppointment(appointmentID.intValue());
  }
  // -----------------------------------------------------------------------------------
  public void fireChangedAppointment(Appointment appointment, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireChangedAppointment(appointment, exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireAddedAppointment(Appointment appointment, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireAddedAppointment(appointment, exclude);
  }
  // -----------------------------------------------------------------------------------
  /** This method is thread safe. */
  public void fireRemovedAppointment(Appointment appointment, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireRemovedAppointment(appointment, exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireSelectedAppointment(Appointment appointment, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireSelectedAppointment(appointment, exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireChangeCalendar(CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireChangeCalendar(exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireSetViewType(String viewtype, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireSetViewType(viewtype, exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireSetVisibleInterval(ScheduleDate start, ScheduleDate end, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireSetVisibleInterval(start, end, exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireSetGridActive(boolean usegrid, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireSetGridActive(usegrid, exclude);
  }
  // -----------------------------------------------------------------------------------
  public void fireSetGridHeight(int seconds, CalendarVisibleElement exclude)
  {
      calendarEventsManager.fireSetGridHeight(seconds, exclude);
  }
  
  /** Delegates export event to calendar visible elements.*/
  public void exportAppointmentsAndTasks() {
      calendarEventsManager.fireExportAppoitmentsAndTasks();
  }

  /** Delegates task creation event to calendar visible elements.*/
  public void createNewTask() {
      calendarEventsManager.fireNewTaskCreation();
  }

  /** Delegates new appointment event to calendar visible elements.*/
  public void createNewAppointment() {
      calendarEventsManager.fireNewAppoitmentCreation();
  }

  /** Delegates a change event to calendar visible elements.*/
  public void changeCalendar() {
      calendarEventsManager.fireChangeCalendar(null);
  }
  
  public void selectCalendar( Integer calendarID ) {
      selectionModel.selectCalendar(calendarID);
  }

  public JDialog getCalendarSelectionDialog() {
        if ( dialog == null ) {
            dialog = new EscapeDialog( ApplicationServices.getInstance().getActionManager().getFrame(), Messages
                .getString( "GUI_Calender_Selection_Dialog_Title" ) );
            ScrollPane scrollPane = new ScrollPane();
            scrollPane.add( selectionModel.getMenuItemsPanel() );
            JPanel contentPane = new JPanel(new BorderLayout());
            contentPane.add(scrollPane, BorderLayout.CENTER);
            contentPane.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
            // Call .getContentPane() for 1.4-compatibitliy
            dialog.getContentPane().add(contentPane );
            dialog.setSize( 430, 200 );            
            dialog.setLocationRelativeTo( null );
        }
        return dialog;
  }
}