/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.plugins.calendar;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;

/**
 * @author niko
 * @deprecated Calendar plugin uses it. Remove it when rewritten.
*/
class CollapsingMenuLayoutManager implements LayoutManager2 
{
  protected LayoutManager2 layoutDelegate = null;
  protected boolean        collapsed = true;

  public CollapsingMenuLayoutManager(LayoutManager2 delegate) 
  {
    super();
    if (delegate == null) throw new NullPointerException("Delegate layout manager is not optional.");
    layoutDelegate = delegate;
  }


	/**
	 * @see java.awt.LayoutManager2#addLayoutComponent(java.awt.Component, java.lang.Object)
	 */
	public void addLayoutComponent(Component comp, Object constraints) 
  {
    layoutDelegate.addLayoutComponent(comp, constraints);
	}

	/**
	 * @see java.awt.LayoutManager2#maximumLayoutSize(java.awt.Container)
	 */
	public Dimension maximumLayoutSize(Container target) 
  {
		return layoutDelegate.maximumLayoutSize(target);
	}

	/**
	 * @see java.awt.LayoutManager2#getLayoutAlignmentX(java.awt.Container)
	 */
	public float getLayoutAlignmentX(Container target) 
  {
		return layoutDelegate.getLayoutAlignmentX(target);
	}

	/**
	 * @see java.awt.LayoutManager2#getLayoutAlignmentY(java.awt.Container)
	 */
	public float getLayoutAlignmentY(Container target) 
  {
		return layoutDelegate.getLayoutAlignmentY(target);
	}

	/**
	 * @see java.awt.LayoutManager2#invalidateLayout(java.awt.Container)
	 */
	public void invalidateLayout(Container target) 
  {
    layoutDelegate.invalidateLayout(target);
	}

	/**
	 * @see java.awt.LayoutManager#addLayoutComponent(java.lang.String, java.awt.Component)
	 */
	public void addLayoutComponent(String name, Component comp) 
  {
    layoutDelegate.addLayoutComponent(name, comp);
	}

	/**
	 * @see java.awt.LayoutManager#removeLayoutComponent(java.awt.Component)
	 */
	public void removeLayoutComponent(Component comp) 
  {
    layoutDelegate.removeLayoutComponent(comp);
	}

	/**
	 * @see java.awt.LayoutManager#preferredLayoutSize(java.awt.Container)
	 */
	public Dimension preferredLayoutSize(Container parent) 
  {
    Dimension dim = layoutDelegate.preferredLayoutSize(parent);
    if (collapsed)
    {
      dim.height = 0;      
    }
    return(dim);
	}

	/**
	 * @see java.awt.LayoutManager#minimumLayoutSize(java.awt.Container)
	 */
	public Dimension minimumLayoutSize(Container parent) 
  {
    Dimension dim = layoutDelegate.minimumLayoutSize(parent);
    if (collapsed)
    {
      dim.height = 0;      
    }
    return(dim);
	}

	/**
	 * @see java.awt.LayoutManager#layoutContainer(java.awt.Container)
	 */
	public void layoutContainer(Container parent) 
  {
    layoutDelegate.layoutContainer(parent);
	}

  /**
   * Returns the collapsed.
   * @return boolean
   */
  public boolean isCollapsed() 
  {
    return collapsed;
  }

  /**
   * Sets the collapsed.
   * @param collapsed The collapsed to set
   */
  public void setCollapsed(boolean collapsed) 
  {
    this.collapsed = collapsed;
  }

  

}
