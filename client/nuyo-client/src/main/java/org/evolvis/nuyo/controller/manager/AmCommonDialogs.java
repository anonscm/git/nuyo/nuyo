/* $Id: AmCommonDialogs.java,v 1.9 2006/12/12 09:31:07 robert Exp $
 * 
 * Created on 08.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import javax.swing.JFrame;

import org.evolvis.nuyo.controller.ApplicationServices;


/**
 * Diese Klasse liefert die generischen Kleindialoge, auf die der
 * {@link org.evolvis.nuyo.controller.ActionManager ActionManager}
 * aufsetzt.
 * 
 * @author mikel
 */
public class AmCommonDialogs extends AmReferences implements CommonDialogServices {
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#getMainFrame()
	public JFrame getFrame() {
		return ApplicationServices.getInstance().getCommonDialogServices().getFrame();
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String)
	
	public void publishError(String msg) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(msg);
	}
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.Object[])
	
	public void publishError(String msg, Object[] params) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(msg,params);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.String)
	
	public void publishError(String caption, String msg) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(caption, msg);
	}
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.String, java.lang.Object[])
	
	public void publishError(String caption, String msg, Object[] params) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(caption,msg,params);
	}
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.Exception)
	
	public void publishError(String msg, Throwable e) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(msg, e);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.Object[], java.lang.Exception)
	
	public void publishError(String msg, Object[] params, Throwable e) {
        try{
            ApplicationServices.getInstance().getCommonDialogServices().publishError(msg, params, e);
        }catch(Exception e1){
            logger.warningSilent(msg, e);
        }
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.String, java.lang.Exception)
	
	public void publishError(String caption, String msg, Throwable e) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(caption,msg,e);
	}
	
	public void publishError(String caption, String msg, String extendedText) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(caption,msg,extendedText);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#publishError(java.lang.String, java.lang.String, java.lang.Object[], java.lang.Exception)
	
	public void publishError(String caption, String msg, Object[] params, Throwable e) {
		ApplicationServices.getInstance().getCommonDialogServices().publishError(caption,msg,params,e);
	}
	
	/*
	 * Informationsausgaben
	 */
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#showInfo(java.lang.String)
	
	public void showInfo(String text) {
		ApplicationServices.getInstance().getCommonDialogServices().showInfo(text);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#showInfo(java.lang.String, java.lang.String)
	
	public void showInfo(String caption, String text) {
		ApplicationServices.getInstance().getCommonDialogServices().showInfo(caption,text);
	}
	
	/*
	 * Auswahlfrage
	 */
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#askUser(java.lang.String, java.lang.String[], int)
	
	public int askUser(String question, String[] answers, int defaultValue) {
		return ApplicationServices.getInstance().getCommonDialogServices().askUser(question, answers, defaultValue);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#askUser(java.lang.String, java.lang.String, java.lang.String[], int)
	
	public int askUser(String caption, String question, String[] answers, int defaultValue) {
		return ApplicationServices.getInstance().getCommonDialogServices().askUser(caption, question, answers, defaultValue);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#askUser(java.lang.String, java.lang.String[], java.lang.String[], int)
	
	public int askUser(String question, String[] answers, String[] tooltips, int defaultValue) {
		return ApplicationServices.getInstance().getCommonDialogServices().askUser(question, answers, tooltips, defaultValue);
	}
	
	
	// @see de.tarent.contact.controller.manager.CommonDialogServices#requestFilename(java.lang.String, java.lang.String, java.lang.String[], java.lang.String, boolean)
	
	public String requestFilename(String startfolder, String presetfile, String[] extensions, String extensiondescription, boolean forSave)
	{
		return ApplicationServices.getInstance().getCommonDialogServices().requestFilename(startfolder,presetfile, extensions,extensiondescription,forSave );
	}
    
    /* (non-Javadoc)
     * @see de.tarent.contact.controller.manager.CommonDialogServices#setWaiting(boolean)
     */
    public void setWaiting(boolean isWaiting) {
        getControlListener().setWaiting(isWaiting);
    }
}
