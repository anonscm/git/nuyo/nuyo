/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.plugins.mail.orders;

import java.awt.BorderLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;


/**
 * @author niko
 */
public class DispatchPanel extends JPanel implements MailOrdersManager {
    
    private DispatchRequestPanel mailOrderButtonsPanel;
    private MailBatchesPanel mailOrdersTablePanel;

    private ControlListener controlListener;
    private GUIListener guiListener;

    private JPanel mainPanel;
    private JPanel tablePanel;
    private JPanel execPanel;
    
  public DispatchPanel(GUIListener guilistener, ControlListener cl) {
    super();
    guiListener = guilistener;
    controlListener = cl;
    
    setLayout(new BorderLayout());   
    
    mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout());
        
    tablePanel = new JPanel();
    tablePanel.setLayout(new BorderLayout());
    mailOrdersTablePanel = new MailBatchesPanel(guiListener, this);
    tablePanel.add(mailOrdersTablePanel, BorderLayout.CENTER);
    controlListener.registerTableComponent("VersandAuftraege", tablePanel);
    controlListener.setTableStaticText("VersandAuftraege", " " + Messages.getString("GUI_MainFrameNewStyle_Tabelle_Titelleiste_Versand"));
    
        
    execPanel = new JPanel();
    execPanel.setLayout(new BorderLayout());    
    execPanel.add(new JLabel(""), BorderLayout.CENTER);
    JPanel splitpanel = new JPanel();
    splitpanel.setLayout(new BoxLayout(splitpanel, BoxLayout.Y_AXIS));
    splitpanel.add(execPanel);
    
    
    mainPanel.add(splitpanel, BorderLayout.NORTH);
    mainPanel.add(Box.createVerticalStrut(20), BorderLayout.SOUTH);
    add(mainPanel, BorderLayout.CENTER);
        
    clearEntries();
  }
  

  public void insertPanel(DispatchRequestPanel panel)
  {
    execPanel.removeAll();
    execPanel.add(panel, BorderLayout.CENTER);
    mainPanel.revalidate();
    mailOrderButtonsPanel = panel;
  }

  public void setNumEntries(int numentries)
  {
    if (numentries == 1)
    {
      controlListener.setTableText("VersandAuftraege", "(" + numentries + " " + Messages.getString("GUI_MainFrameNewStyle_Tabelle_Titelleiste_Versand_Eintrag") + ") ");
    }
    else
    {
      controlListener.setTableText("VersandAuftraege", "(" + numentries + " " + Messages.getString("GUI_MainFrameNewStyle_Tabelle_Titelleiste_Versand_Eintraege") + ") ");
    }
  }
  
  public void refreshList()
  {
    mailOrdersTablePanel.refreshList();
  }

  public void clearEntries()
  {
    insertPanel(new DispatchRequestPanel(guiListener, mailOrdersTablePanel, (MailBatch)null));
    if (guiListener.isSearchActive())
    {
      boolean exitsearch = false;
      switch(guiListener.getSearchMode())
      {
        case(GUIListener.ADDRESSSET_VERSANDAUFTRAG_ALL): exitsearch = true; break; 
        case(GUIListener.ADDRESSSET_VERSANDAUFTRAG_NICHTZUGEORDNET): exitsearch = true; break; 
        case(GUIListener.ADDRESSSET_VERSANDAUFTRAG_EMAIL): exitsearch = true; break; 
        case(GUIListener.ADDRESSSET_VERSANDAUFTRAG_FAX): exitsearch = true; break; 
        case(GUIListener.ADDRESSSET_VERSANDAUFTRAG_POST): exitsearch = true; break;          
      }
      // TODO: Addresszugriff: How to implement this?
      //if (exitsearch) guiListener.userRequestNoSearchFilter();
    }
    
    mailOrderButtonsPanel = null;
  }

  public void setButtonFilterOffEnabled(boolean enable) 
  {
    if (mailOrderButtonsPanel != null) mailOrderButtonsPanel.setButtonFilterOffEnabled(enable);
  }
  
  /** Fires new mail order event. */ 
  public void fireNewMailOrderEvent() {
      mailOrdersTablePanel.createNewMailOrder();
  }

  
}
