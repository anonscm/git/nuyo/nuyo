/* $Id: MasterInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 07.10.2003
 */
package org.evolvis.nuyo.messages;

import java.util.EventObject;

/**
 * Dies ist die Basisklasse f�r Informationsereignisse, die Masterdaten
 * betreffen.
 *  
 * @author mikel
 */
public class MasterInfo extends EventObject implements InfoEvent {
    /**
     * @param source Ereignisquelle
     */
    public MasterInfo(Object source) {
        super(source);
    }
}
