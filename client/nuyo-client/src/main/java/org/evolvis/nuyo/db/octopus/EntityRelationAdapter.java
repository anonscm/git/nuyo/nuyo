package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.EntityRelationEvent;
import org.evolvis.nuyo.db.persistence.EntityRelationListener;

/**
 * @author kleinw
 *
 *	Hilfsmittel f�r den EntityRelationListener, wenn nicht alle Methoden
 *	ben�tigt werden.
 *
 */
public class EntityRelationAdapter implements EntityRelationListener {

    /**	Eine oder mehrere Relationen wurden hinzugef�gt */
    public void relationAdded(EntityRelationEvent event) throws ContactDBException {}

    
    /**	Eine oder mehrere Relationen wurden gel�scht */
    public void relationRemoved(EntityRelationEvent event) throws ContactDBException {}
    
}
