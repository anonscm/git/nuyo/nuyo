package org.evolvis.nuyo.plugins.calendar;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.config.XmlUtil;
import org.evolvis.xana.utils.IconFactory;


public class CalendarMenuItem extends CollapsingMenuItemContainer implements CalendarSelectionListener {

    private static final TarentLogger logger = new TarentLogger(CalendarMenuItem.class);
    private static final long serialVersionUID = 1L;
    private static String USER_GROUP_CALENDAR;
    private static String RESOURCES_CALENDAR;
    private static String USER_CALENDAR;
    private static String CALENDAR;
    private static ImageIcon ressourceTypeIcon;
    private static ImageIcon userGroupTypeIcon;
    private static ImageIcon otherTypeIcon;
    private static ImageIcon userTypeIcon;
    
    static{
        IconFactory factory = ApplicationServices.getInstance().getIconFactory();
        userGroupTypeIcon = (ImageIcon)factory.getIcon("calendar_usergroup.gif");    
        ressourceTypeIcon = (ImageIcon)factory.getIcon("calendar_ressource.gif");    
        otherTypeIcon = (ImageIcon)factory.getIcon("calendar_other.gif");    
        userTypeIcon = (ImageIcon)factory.getIcon("calendar_user.gif");
        //TODO: load internationalized calendar item strings from properties 
        CALENDAR = "Kalender";
        USER_GROUP_CALENDAR = "Benutzergruppenkalender";
        USER_CALENDAR = "Benutzerkalender";
        RESOURCES_CALENDAR = "Ressourcenkalender";
    }
    
    private CalendarMenuItem(final ImageIcon icon,final CalendarEntryPanel itemPanel,final ActionListener actionListener, final Integer calendarID) {
        super( icon, itemPanel, actionListener );
        setUserDataObject( itemPanel );
        
        final Color highLightColor = new Color( 0xcc, 0xcc, 0xcc );
        setSelectionHighlightColor( highLightColor );
        setHighlightColor( highLightColor );
        setForegroundColor( Color.BLACK );
        setSelectionColor( Color.WHITE );
        setBackgroundColor( Color.WHITE );
        
        drawBorderWhenSelected( false );
        setSelectedItemBorder( 1 );
        setIconAlignmentY( JButton.TOP );
        setItemVisible( true );
        
        itemPanel.addActionListener(actionListener);
        itemPanel.setHighlightMouseListener(getHighlightMouseListener());
        itemPanel.setHighlightComponents(this);
        itemPanel.addColorChooserListener(new ColorChooserListener(calendarID, itemPanel));
    }
    
    public static CalendarMenuItem create(final Calendar calendar,final ActionListener actionListener) throws ContactDBException {
        final ImageIcon typeIcon;
        final String type;
        final String title;
        final String toolTip;
        final CalendarEntryPanel currentCalenderEntryPanel;

        final Integer calendarID = new Integer(calendar.getId());
        final String calendarName = XmlUtil.escape( calendar.getAttribute( Calendar.KEY_NAME ) ).trim();
        final String creatorName = XmlUtil.escape( calendar.getAttribute( Calendar.KEY_CREATOR ) ).trim();
        final String changerName = XmlUtil.escape( calendar.getAttribute( Calendar.KEY_CHANGER ) ).trim();

        switch ( calendar.getType() ) {
            case ( Calendar.TYPE_RESOURCE ):
                typeIcon = ressourceTypeIcon;
                type = RESOURCES_CALENDAR;
                break;
            case ( Calendar.TYPE_USER ):
                typeIcon = userTypeIcon;
                type = USER_CALENDAR;
                break;
            case ( Calendar.TYPE_USER_GROUP ):
                typeIcon = userGroupTypeIcon;
                type = USER_GROUP_CALENDAR;
                break;
            case ( Calendar.TYPE_OTHER ):
            default:
                typeIcon = otherTypeIcon;
                type = CALENDAR;
                break;
        }

        title = getTitleName( calendarName );
        toolTip = getToolTip( type, calendarName, creatorName, changerName );

        currentCalenderEntryPanel = new CalendarEntryPanel( title,getType( type ), typeIcon, toolTip, getCalendarColor(calendarID));
        
        CalendarMenuItem calendarItem = new CalendarMenuItem( typeIcon,currentCalenderEntryPanel, actionListener, calendarID );
        calendarItem.setText( title );
        calendarItem.setUserString( getLengthRestrictedString( calendarName, 30 ) );
        calendarItem.setUserDescription( getLengthRestrictedString( calendarName, 30 ) );
        calendarItem.setItemToolTipText( toolTip );

        return calendarItem;
    }

    private static String getType( final String currentTypeText ) {
        final StringBuffer buffer = new StringBuffer(100);
        buffer.append("(");
        buffer.append(currentTypeText);
        buffer.append(")");
        return buffer.toString();
    }

    private static String getToolTip( final String currentTypeText, final String currentName, final String currentCreator, final String currentChanger ) {
        final StringBuffer buffer = new StringBuffer(300);
        buffer.append("<html>");
        buffer.append(currentTypeText);
        buffer.append(" <b>");
        buffer.append(getLengthRestrictedString( currentName, 30 ));
        buffer.append("</b>");
        if (currentCreator != null && currentCreator.length() > 0){
            buffer.append("<br>erstellt von: ");
            buffer.append(currentCreator);
        }
        if (currentChanger != null && currentChanger.length() > 0){
            buffer.append("<br>zuletzt ge�ndert von: ");
            buffer.append(currentChanger);
        }
        buffer.append("</html>");
        return buffer.toString();
    }

    private static String getTitleName( String currentName ) {
        final StringBuffer buffer = new StringBuffer(300);
        buffer.append("<html>");
        if( currentName.length() == 0){
            buffer.append("<i>unbenannt</i>"); 
        } else buffer.append(getLengthRestrictedString( currentName, 30 ));
        buffer.append("</html>");
        return buffer.toString();
    }

    private static Color getCalendarColor(final Integer calendarID ) {
        // 25.10.05 Von Sebastion umgebogen, damit die Kalenderfarbe nicht jedes mal neu geholt wird
        // String colorString = m_oGUIListener.getUserParameter(null, "CalendarColor_ID_" + calendar.getId());
        String colorString = ApplicationServices.getInstance().getActionManager().getCalendarColor(calendarID.intValue());
        return (colorString != null) ? Color.decode(colorString) : Color.WHITE;
    }

    public void setSelected( boolean isSelected ) {
        getCheckBox().setSelected(isSelected);
        setItemSelected(isSelected);
    }

    protected JCheckBox getCheckBox() {
        return ((CalendarEntryPanel) getUserDataObject()).getCheckBox();
    }

    private static String getLengthRestrictedString(String text, int maxlen) {
        if (text == null)
            return "";
        if (text.length() > maxlen) {
            int lastwhitespace = text.substring(0, maxlen).lastIndexOf(" ");
            if (lastwhitespace == -1) {
                lastwhitespace = maxlen;
            }
            return (text.substring(0, lastwhitespace) + "\n" + text.substring(lastwhitespace));
        } else
            return (text);
    }
}
