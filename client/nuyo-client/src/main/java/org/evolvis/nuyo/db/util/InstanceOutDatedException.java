/* $Id: InstanceOutDatedException.java,v 1.3 2006/06/06 14:12:14 nils Exp $
 * 
 * Created on 20.06.2004
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.util;

/**
 * Diese Exception wird vom {@link org.evolvis.nuyo.db.util.InstanceTrunk} geworfen,
 * wenn eine Instanz im System vorhanden ist, die aber veralteten Inhalt hat. Die
 * Instanz selber wird in dieser Exception mit herausgereicht.
 * 
 * @author mikel
 */
public class InstanceOutDatedException extends Exception {
    //
    // �ffentliche Methoden
    //
    /** ID der veralteten Instanz */
    public Object getId()		{ return id; }
    /** veraltete Instanz */
    public Object getValue()	{ return value; }
    
    //
    // Konstruktor
    //
    InstanceOutDatedException(Object id, Object value) {
        super("Die Instanz zu " + id + " ist veraltet.");
        this.id = id;
        this.value = value;
    }
    
    //
    // Membervariablen
    //
    /** ID der veralteten Instanz */
    private Object id = null;
    /** veraltete Instanz */
    private Object value = null;
}
