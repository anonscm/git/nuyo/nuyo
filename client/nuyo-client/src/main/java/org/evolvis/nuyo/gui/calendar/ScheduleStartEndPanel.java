/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.gui.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.JPanel;

/**
 * @author niko
 */


public class ScheduleStartEndPanel extends JPanel 
{
  private int m_oNumPeaks = 5;
  private int m_iPeakHeight = 10;
  private boolean m_bIsTop = false;
  private Color m_oLineColor;
  private boolean m_bIsActivated;
  
  public ScheduleStartEndPanel(boolean istop, int numpeaks, int peakheight, Color color)
  {
    m_bIsTop = istop;
    m_oNumPeaks = numpeaks; 
    m_iPeakHeight = peakheight;
    m_oLineColor = color;
    m_bIsActivated = false;
  }  

  public void setActive(boolean isactive)
  {
    m_bIsActivated = isactive;
    this.repaint();
  }
  
  public boolean isActive()
  {
    return(m_bIsActivated);
  }
  
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {
    if (m_bIsActivated)
    {  
	    Rectangle clip = g.getClipBounds();
	    Insets insets = getInsets();
	    Dimension  size = getSize();    
	    int peakwidth = (size.width) / m_oNumPeaks;
	    
	    g.setColor(m_oLineColor); 
	    if (m_bIsTop)
	    {  
	      for(int i=0; i<m_oNumPeaks; i++)
	      {
	        int x = peakwidth * i; 
	        g.drawLine(x, 0, x + (peakwidth / 2), m_iPeakHeight);
	        g.drawLine(x + (peakwidth / 2), m_iPeakHeight, x + peakwidth, 0);
	      }
	    }
	    else
	    {  
	      for(int i=0; i<m_oNumPeaks; i++)
	      {
	        int x = peakwidth * i; 
	        g.drawLine(x, size.height, x + (peakwidth / 2), size.height - m_iPeakHeight);
	        g.drawLine(x + (peakwidth / 2), size.height - m_iPeakHeight, x + peakwidth, size.height);
	      }
	    }
    }
  }
}
