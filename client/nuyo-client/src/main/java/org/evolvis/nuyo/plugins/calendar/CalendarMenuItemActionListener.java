package org.evolvis.nuyo.plugins.calendar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class CalendarMenuItemActionListener implements ActionListener {

    private final Integer calendarID;
    private final CalendarClickedListener listener;
    
    public CalendarMenuItemActionListener(final Integer aCalendarID, final CalendarClickedListener aListener){
        calendarID = aCalendarID;
        listener = aListener;
    }

    public Integer getCalendarID() {
        return calendarID;
    }

    public void actionPerformed(ActionEvent e) {
        listener.clickedCalendar(calendarID);
    }
}