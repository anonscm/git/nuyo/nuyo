/*
 * Created on 30.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.Date;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * Diese Klasse stellt ein Bindeglied zwischen dem PluginData 
 * Interface und dem Address-Objekt dar. �ber diese Wrapper-Klasse
 * kann auf ein Address-Objekt �ber Key-Angaben zugegriffen werden.
 *
 * @author niko
 *
 */
public class AddressPluginData implements PluginData
{  
  private Address m_oAddress;
  
  private static Logger logger = Logger.getLogger(AddressPluginData.class.getName());
  
    
  /**
   * Dem Konstruktor muss ein Address-Objekt �bergeben werden 
   * auf das sich alle weitergehenden Operationen beziehen
   * 
   * @param address (Address) - Das Address-Objekt
   */
  public AddressPluginData(Address address)
  {
    m_oAddress = address;
  }

  /**
   * �ber diese Methode kann ein Feld im Address-Objekt gelesen werden. 
   * der Key muss vom Typ String sein und sollte �ber seinen Namespace
   * angeben welchen Teil der Daten er zugeh�rig ist.
   * z.B.: "tc.std.*" f�r Standard-Felder oder "tc.ext.*" f�r Extended-Felder 
   * 
   * @param key (Object) - Der Schl�ssel des Feldes im Address-Objekt
   * @return Object - der Inhalt des Feldes oder null wenn das Feld nicht 
   * existiert.
   *  
   */
  public Object get(Object key)
  {
    if (key instanceof String)
    {
      String keystr = ((String)key);
      
      if (keystr.startsWith("tc."))
      {
        // tarentContact Daten

        keystr = keystr.substring(3);
        if (keystr.startsWith("std."))
        {
          keystr = keystr.substring(4);
          return getStd((Object)keystr);
        } 
        else if (keystr.startsWith("ext."))
        {
            throw new RuntimeException("Configuration error: Extended Address Data is not supported any more.");
//           keystr = keystr.substring(4);
//           return getExt((Object)keystr);          
        }
      }
      else
      {
        // kein namespace angegeben... 
        // aus kompatiblit�tsgr�nden
        // die alte Methode aufrufen!
        return getStd((Object)keystr);
      }
    }
    
    return null;
  }


  /**
   * �ber diese Methode kann ein Feld im Address-Objekt geschrieben werden. 
   * der Key muss vom Typ String sein und sollte �ber seinen Namespace
   * angeben welchen Teil der Daten er zugeh�rig ist.
   * z.B.: "tc.std.*" f�r Standard-Felder oder "tc.ext.*" f�r Extended-Felder 
   * 
   * @param key (Object) - Der Schl�ssel des Feldes im Address-Objekt
   * @return boolean - false wenn das schreiben nicht m�glich war 
   *  
   */
  public boolean set(Object key, Object value)
  {    
//System.err.println("~~~~~~~~~~set(" + key + ", " + value + ")");
    if (key instanceof String)
    {
      String keystr = ((String)key);
      
      if (keystr.startsWith("tc."))
      {
        // tarentContact Daten

        keystr = keystr.substring(3);
        if (keystr.startsWith("std."))
        {
          keystr = keystr.substring(4);
          return setStd((Object)keystr, value);
        } 
        else if (keystr.startsWith("ext."))
        {
            throw new RuntimeException("Configuration error: Extended Address Data is not supported any more.");

            //           keystr = keystr.substring(4);
            //           return setExt((Object)keystr, value);          
        }
      }
      else
      {
        // kein namespace angegeben... 
        // aus kompatiblit�tsgr�nden
        // die alte Methode aufrufen!
        return setStd((Object)keystr, value);
      }
    }
    
    return false;
  }
  
  
  
  // ----------------------------------------------------------------------------------
  
  private Object getStd(Object key)
  {
      if (AddressKeys.ABTEILUNG.equals(key)) return m_oAddress.getAbteilung();
      if (AddressKeys.ADRESSNR.equals(key)) return new Integer(m_oAddress.getAdrNr());
      if (AddressKeys.AENDERUNGSDATUM.equals(key)) return m_oAddress.getAenderungsdatum();
      if (AddressKeys.AKADTITEL.equals(key)) return m_oAddress.getAkadTitel();
      if (AddressKeys.ANREDE.equals(key)) return m_oAddress.getAnrede();
      if (AddressKeys.BANKCODE.equals(key)) return m_oAddress.getBankCode();
      if (AddressKeys.BANKKONTO.equals(key)) return m_oAddress.getBankKonto();
      if (AddressKeys.BANKNAME.equals(key)) return m_oAddress.getBankName();
      if (AddressKeys.BUNDESLAND.equals(key)) return m_oAddress.getBundesland();
      if (AddressKeys.EMAIL.equals(key)) return m_oAddress.getEmailAdresseDienstlich();
      if (AddressKeys.EMAIL2.equals(key)) return m_oAddress.getEmailAdressePrivat();
      if (AddressKeys.ERFASSUNGSDATUM.equals(key)) return m_oAddress.getErfassungsdatum();
      if (AddressKeys.FAXDIENST.equals(key)) return m_oAddress.getFaxDienstlich();
      if (AddressKeys.FAXPRIVAT.equals(key)) return m_oAddress.getFaxPrivat();
      if (AddressKeys.FOLLOWUP.equals(key)) return m_oAddress.getFollowup();
      if (AddressKeys.FOLLOWUPDATE.equals(key)) return m_oAddress.getFollowupDate();
      if (AddressKeys.FK_USER_FOLLOWUP.equals(key)) return new Integer(m_oAddress.getFkUserFollowup());   
      if (AddressKeys.GEBURTSDATUM.equals(key)) return m_oAddress.getGeburtsdatum();
      if (AddressKeys.GEBURTSJAHR.equals(key)) return new Integer(m_oAddress.getGeburtsjahr());
      if (AddressKeys.GESCHLECHT.equals(key)) return m_oAddress.getGeschlecht();
      if (AddressKeys.HANDYDIENST.equals(key)) return m_oAddress.getHandyDienstlich();
      if (AddressKeys.HANDYPRIVAT.equals(key)) return m_oAddress.getHandyPrivat();
      if (AddressKeys.HAUSNUMMER.equals(key)) return m_oAddress.getHausNr();
      if (AddressKeys.HERRFRAU.equals(key)) return m_oAddress.getHerrnFrau();
      if (AddressKeys.HOMEPAGE.equals(key)) return m_oAddress.getHomepageDienstlich();
      if (AddressKeys.INSTITUTION.equals(key)) return m_oAddress.getInstitution();
      if (AddressKeys.INSTITUTION2.equals(key)) return m_oAddress.getInstitutionWeitere();
      if (AddressKeys.LAND.equals(key)) return m_oAddress.getLand();
      if (AddressKeys.LETTERADDRESS.equals(key)) return m_oAddress.getLetterAddress();
      if (AddressKeys.LETTER_AUTO.equals(key)) return m_oAddress.getLetterAddressAuto();
      if (AddressKeys.LETTERSALUTATION.equals(key)) return m_oAddress.getLetterSalutation();
      if (AddressKeys.LETTERSALUTATION_AUTO.equals(key)) return m_oAddress.getLetterSalutationAuto();
      if (AddressKeys.NACHNAME.equals(key)) return m_oAddress.getNachname();
      if (AddressKeys.NAMENSZUSATZ.equals(key)) return m_oAddress.getNamenszusatz();
      if (AddressKeys.NOTIZ.equals(key)) return m_oAddress.getNotiz();
      if (AddressKeys.ORT.equals(key)) return m_oAddress.getOrt();
      if (AddressKeys.PICTURE.equals(key)) return m_oAddress.getPicture();
      if (AddressKeys.PICTUREEXPRESSION.equals(key)) return m_oAddress.getPictureExpression();
      if (AddressKeys.PLZ.equals(key)) return m_oAddress.getPlz();
      if (AddressKeys.POSITION.equals(key)) return m_oAddress.getPosition();
      if (AddressKeys.POSTFACH.equals(key)) return m_oAddress.getPostfachNr();
      if (AddressKeys.POSTFACHPLZ.equals(key)) return m_oAddress.getPlzPostfach();
      if (AddressKeys.SPITZNAME.equals(key)) return m_oAddress.getSpitzname();
      if (AddressKeys.STRASSE.equals(key)) return m_oAddress.getStrasse();
      if (AddressKeys.TELEFONDIENST.equals(key)) return m_oAddress.getTelefonDienstlich();
      if (AddressKeys.TELEFONPRIVAT.equals(key)) return m_oAddress.getTelefonPrivat();
      if (AddressKeys.TITEL.equals(key)) return m_oAddress.getTitel();
      if (AddressKeys.VORNAME.equals(key)) return m_oAddress.getVorname();
      if (AddressKeys.WEITEREVORNAMEN.equals(key)) return m_oAddress.getWeitereVornamen();
      if (AddressKeys.EMAIL3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_E_MAIL_WEITERE);
      if (AddressKeys.FAX3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_FAX_WEITERES);
      if (AddressKeys.HANDY3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_HANDY_WEITERES);
      if (AddressKeys.TELEFON3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_TELEFON_WEITERES);      
      if (AddressKeys.INSTITUTION3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_INSTITUTION_WEITERE);
      if (AddressKeys.NAMEN3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_WEITERE_VORNAMEN);
      if (AddressKeys.HOMEPAGE2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_HOMEPAGE_PRIVAT);
      if (AddressKeys.HOMEPAGE3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_HOMEPAGE_WEITERE);
      
      if (AddressKeys.ADRESSZUSATZ2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_ADDRESSZUSATZ2);
      if (AddressKeys.BUNDESLAND2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_BUNDESLAND2);
      if (AddressKeys.HAUSNUMMER2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_HAUS_NR2);
      if (AddressKeys.LAND2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_LAND2);
      if (AddressKeys.LKZ2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_LKZ2);
      if (AddressKeys.ORT2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_ORT2);
      if (AddressKeys.PLZ2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_PLZ2);
      if (AddressKeys.POSTFACH2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_POSTFACH_NR2);
      if (AddressKeys.POSTFACHPLZ2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_PLZ_POSTFACH2);
      if (AddressKeys.STRASSE2.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_STRASSE2);
      
      if (AddressKeys.ADRESSZUSATZ3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_ADDRESSZUSATZ3);
      if (AddressKeys.BUNDESLAND3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_BUNDESLAND3);
      if (AddressKeys.HAUSNUMMER3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_HAUS_NR3);
      if (AddressKeys.LAND3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_LAND3);
      if (AddressKeys.LKZ3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_LKZ3);
      if (AddressKeys.ORT3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_ORT3);
      if (AddressKeys.PLZ3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_PLZ3);
      if (AddressKeys.POSTFACH3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_POSTFACH_NR3);
      if (AddressKeys.POSTFACHPLZ3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_PLZ_POSTFACH3);
      if (AddressKeys.STRASSE3.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_STRASSE3);
      
      if (AddressKeys.BLOCKEDFORCONSIGNMENT.equals(key)) return m_oAddress.getBlockedForConsignment();
      

      // common fields      
      if (AddressKeys.COMMONTEXT01.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT01);
      if (AddressKeys.COMMONTEXT02.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT02);
      if (AddressKeys.COMMONTEXT03.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT03);
      if (AddressKeys.COMMONTEXT04.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT04);
      if (AddressKeys.COMMONTEXT05.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT05);
      if (AddressKeys.COMMONTEXT06.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT06);
      if (AddressKeys.COMMONTEXT07.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT07);
      if (AddressKeys.COMMONTEXT08.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT08);
      if (AddressKeys.COMMONTEXT09.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT09);
      if (AddressKeys.COMMONTEXT10.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT10);
      if (AddressKeys.COMMONTEXT11.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT11);
      if (AddressKeys.COMMONTEXT12.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT12);
      if (AddressKeys.COMMONTEXT13.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT13);
      if (AddressKeys.COMMONTEXT14.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT14);
      if (AddressKeys.COMMONTEXT15.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT15);
      if (AddressKeys.COMMONTEXT16.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT16);
      if (AddressKeys.COMMONTEXT17.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT17);
      if (AddressKeys.COMMONTEXT18.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT18);
      if (AddressKeys.COMMONTEXT19.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT19);
      if (AddressKeys.COMMONTEXT20.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONTEXT20);
      
      if (AddressKeys.COMMONINT01.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT01);
      if (AddressKeys.COMMONINT02.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT02);
      if (AddressKeys.COMMONINT03.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT03);
      if (AddressKeys.COMMONINT04.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT04);
      if (AddressKeys.COMMONINT05.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT05);
      if (AddressKeys.COMMONINT06.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT06);
      if (AddressKeys.COMMONINT07.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT07);
      if (AddressKeys.COMMONINT08.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT08);
      if (AddressKeys.COMMONINT09.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT09);
      if (AddressKeys.COMMONINT10.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONINT10);

      if (AddressKeys.COMMONBOOL01.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL01);
      if (AddressKeys.COMMONBOOL02.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL02);
      if (AddressKeys.COMMONBOOL03.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL03);
      if (AddressKeys.COMMONBOOL04.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL04);
      if (AddressKeys.COMMONBOOL05.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL05);
      if (AddressKeys.COMMONBOOL06.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL06);
      if (AddressKeys.COMMONBOOL07.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL07);
      if (AddressKeys.COMMONBOOL08.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL08);
      if (AddressKeys.COMMONBOOL09.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL09);
      if (AddressKeys.COMMONBOOL10.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONBOOL10);
      
      if (AddressKeys.COMMONDATE01.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE01);
      if (AddressKeys.COMMONDATE02.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE02);
      if (AddressKeys.COMMONDATE03.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE03);
      if (AddressKeys.COMMONDATE04.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE04);
      if (AddressKeys.COMMONDATE05.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE05);
      if (AddressKeys.COMMONDATE06.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE06);
      if (AddressKeys.COMMONDATE07.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE07);
      if (AddressKeys.COMMONDATE08.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE08);
      if (AddressKeys.COMMONDATE09.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE09);
      if (AddressKeys.COMMONDATE10.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONDATE10);
      
      if (AddressKeys.COMMONMONEY01.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY01);
      if (AddressKeys.COMMONMONEY02.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY02);
      if (AddressKeys.COMMONMONEY03.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY03);
      if (AddressKeys.COMMONMONEY04.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY04);
      if (AddressKeys.COMMONMONEY05.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY05);
      if (AddressKeys.COMMONMONEY06.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY06);
      if (AddressKeys.COMMONMONEY07.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY07);
      if (AddressKeys.COMMONMONEY08.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY08);
      if (AddressKeys.COMMONMONEY09.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY09);
      if (AddressKeys.COMMONMONEY10.equals(key)) return m_oAddress.getStandardData(Address.PROPERTY_COMMONMONEY10);
      
      return null;
  }
  
  // -----------------------------------------------------------------------
  
  private boolean setStd(Object key, Object value)
  {
      if (AddressKeys.ABTEILUNG.equals(key)) m_oAddress.setAbteilung(value.toString());
      else if (AddressKeys.AKADTITEL.equals(key)) m_oAddress.setAkadTitel(value.toString());
      else if (AddressKeys.ANREDE.equals(key)) m_oAddress.setAnrede(value.toString());
      else if (AddressKeys.BANKCODE.equals(key)) m_oAddress.setBankCode(value.toString());
      else if (AddressKeys.BANKKONTO.equals(key)) m_oAddress.setBankKonto(value.toString());
      else if (AddressKeys.BANKNAME.equals(key)) m_oAddress.setBankName(value.toString());
      else if (AddressKeys.BUNDESLAND.equals(key)) m_oAddress.setBundesland(value.toString());
      else if (AddressKeys.EMAIL.equals(key)) m_oAddress.setEmailAdresseDienstlich(value.toString());
      else if (AddressKeys.EMAIL2.equals(key)) m_oAddress.setEmailAdressePrivat(value.toString());
      else if (AddressKeys.FAXDIENST.equals(key)) m_oAddress.setFaxDienstlich(value.toString());
      else if (AddressKeys.FAXPRIVAT.equals(key)) m_oAddress.setFaxPrivat(value.toString());
      else if (AddressKeys.FOLLOWUP.equals(key)) m_oAddress.setFollowup((Boolean)value);
      else if (AddressKeys.FOLLOWUPDATE.equals(key)) m_oAddress.setFollowupDate((Date)value);
      else if (AddressKeys.FK_USER_FOLLOWUP.equals(key)) m_oAddress.setFkUserFollowup(((Integer)value).intValue());
      else if (AddressKeys.GEBURTSDATUM.equals(key)) m_oAddress.setGeburtsdatum((Date)value);
      else if (AddressKeys.GEBURTSJAHR.equals(key)) m_oAddress.setGeburtsjahr(((Integer)value).intValue());
      else if (AddressKeys.GESCHLECHT.equals(key)) m_oAddress.setGeschlecht((String)value);
      else if (AddressKeys.HANDYDIENST.equals(key)) m_oAddress.setHandyDienstlich(value.toString());
      else if (AddressKeys.HANDYPRIVAT.equals(key)) m_oAddress.setHandyPrivat(value.toString());
      else if (AddressKeys.HAUSNUMMER.equals(key)) m_oAddress.setHausNr(value.toString());
      else if (AddressKeys.HERRFRAU.equals(key)) m_oAddress.setHerrnFrau(value.toString());
      else if (AddressKeys.HOMEPAGE.equals(key)) m_oAddress.setHomepageDienstlich(value.toString());
      else if (AddressKeys.INSTITUTION.equals(key)) m_oAddress.setInstitution(value.toString());
      else if (AddressKeys.INSTITUTION2.equals(key)) m_oAddress.setInstitutionWeitere(value.toString());
      else if (AddressKeys.LAND.equals(key)) m_oAddress.setLand(value.toString());
      else if (AddressKeys.LETTERADDRESS.equals(key)) if (value != null) m_oAddress.setLetterAddress(value.toString()); else m_oAddress.setLetterAddress(null);
      else if (AddressKeys.LETTER_AUTO.equals(key)) m_oAddress.setLetterAddressAuto((Boolean) value);
      else if (AddressKeys.LETTERSALUTATION.equals(key)) if (value != null) m_oAddress.setLetterSalutation(value.toString()); else m_oAddress.setLetterSalutation(null);
      else if (AddressKeys.LETTERSALUTATION_AUTO.equals(key)) m_oAddress.setLetterSalutationAuto((Boolean) value);
      else if (AddressKeys.NACHNAME.equals(key)) m_oAddress.setNachname(value.toString());
      else if (AddressKeys.NAMENSZUSATZ.equals(key)) m_oAddress.setNamenszusatz(value.toString());
      else if (AddressKeys.NOTIZ.equals(key)) m_oAddress.setNotiz(value.toString());
      else if (AddressKeys.ORT.equals(key)) m_oAddress.setOrt(value.toString());
      else if (AddressKeys.PICTURE.equals(key)) m_oAddress.setPicture((byte[])value);
      else if (AddressKeys.PICTUREEXPRESSION.equals(key)) m_oAddress.setPictureExpression((String) value);
      else if (AddressKeys.PLZ.equals(key)) m_oAddress.setPlz(value.toString());
      else if (AddressKeys.POSITION.equals(key)) m_oAddress.setPosition(value.toString());
      else if (AddressKeys.POSTFACH.equals(key)) m_oAddress.setPostfachNr(value.toString());
      else if (AddressKeys.POSTFACHPLZ.equals(key)) m_oAddress.setPlzPostfach(value.toString());
      else if (AddressKeys.SPITZNAME.equals(key)) m_oAddress.setSpitzname(value.toString());
      else if (AddressKeys.STRASSE.equals(key)) m_oAddress.setStrasse(value.toString());
      else if (AddressKeys.TELEFONDIENST.equals(key)) m_oAddress.setTelefonDienstlich(value.toString());
      else if (AddressKeys.TELEFONPRIVAT.equals(key)) m_oAddress.setTelefonPrivat(value.toString());
      else if (AddressKeys.TITEL.equals(key)) m_oAddress.setTitel(value.toString());
      else if (AddressKeys.VORNAME.equals(key)) m_oAddress.setVorname(value.toString());
      else if (AddressKeys.WEITEREVORNAMEN.equals(key)) m_oAddress.setWeitereVornamen(value.toString());
      else if (AddressKeys.EMAIL3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_E_MAIL_WEITERE, value.toString());
      else if (AddressKeys.FAX3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_FAX_WEITERES, value.toString());
      else if (AddressKeys.HANDY3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_HANDY_WEITERES, value.toString());
      else if (AddressKeys.TELEFON3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_TELEFON_WEITERES, value.toString());      
      else if (AddressKeys.INSTITUTION3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_INSTITUTION_WEITERE, value.toString());
      else if (AddressKeys.NAMEN3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_WEITERE_VORNAMEN, value.toString());
      else if (AddressKeys.HOMEPAGE2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_HOMEPAGE_PRIVAT, value.toString());
      else if (AddressKeys.HOMEPAGE3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_HOMEPAGE_WEITERE, value.toString());
      
      
      else if (AddressKeys.ADRESSZUSATZ2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_ADDRESSZUSATZ2, value.toString());
      else if (AddressKeys.BUNDESLAND2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_BUNDESLAND2, value.toString());
      else if (AddressKeys.HAUSNUMMER2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_HAUS_NR2, value.toString());
      else if (AddressKeys.LAND2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_LAND2, value.toString());
      else if (AddressKeys.LKZ2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_LKZ2, value.toString());
      else if (AddressKeys.ORT2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_ORT2, value.toString());
      else if (AddressKeys.PLZ2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_PLZ2, value.toString());
      else if (AddressKeys.POSTFACH2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_POSTFACH_NR2, value.toString());
      else if (AddressKeys.POSTFACHPLZ2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_PLZ_POSTFACH2, value.toString());
      else if (AddressKeys.STRASSE2.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_STRASSE2, value.toString());
      
      else if (AddressKeys.ADRESSZUSATZ3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_ADDRESSZUSATZ3, value.toString());
      else if (AddressKeys.BUNDESLAND3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_BUNDESLAND3, value.toString());
      else if (AddressKeys.HAUSNUMMER3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_HAUS_NR3, value.toString());
      else if (AddressKeys.LAND3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_LAND3, value.toString());
      else if (AddressKeys.LKZ3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_LKZ3, value.toString());
      else if (AddressKeys.ORT3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_ORT3, value.toString());
      else if (AddressKeys.PLZ3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_PLZ3, value.toString());
      else if (AddressKeys.POSTFACH3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_POSTFACH_NR3, value.toString());
      else if (AddressKeys.POSTFACHPLZ3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_PLZ_POSTFACH3, value.toString());
      else if (AddressKeys.STRASSE3.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_STRASSE3, value.toString());
      
      else if (AddressKeys.BLOCKEDFORCONSIGNMENT.equals(key)) m_oAddress.setBlockedForConsignment((Boolean) value);
      
      else if (AddressKeys.COMMONTEXT01.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT01, value);
      else if (AddressKeys.COMMONTEXT02.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT02, value);
      else if (AddressKeys.COMMONTEXT03.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT03, value);
      else if (AddressKeys.COMMONTEXT04.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT04, value);
      else if (AddressKeys.COMMONTEXT05.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT05, value);
      else if (AddressKeys.COMMONTEXT06.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT06, value);
      else if (AddressKeys.COMMONTEXT07.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT07, value);
      else if (AddressKeys.COMMONTEXT08.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT08, value);
      else if (AddressKeys.COMMONTEXT09.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT09, value);
      else if (AddressKeys.COMMONTEXT10.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT10, value);
      else if (AddressKeys.COMMONTEXT11.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT11, value);
      else if (AddressKeys.COMMONTEXT12.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT12, value);
      else if (AddressKeys.COMMONTEXT13.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT13, value);
      else if (AddressKeys.COMMONTEXT14.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT14, value);
      else if (AddressKeys.COMMONTEXT15.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT15, value);
      else if (AddressKeys.COMMONTEXT16.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT16, value);
      else if (AddressKeys.COMMONTEXT17.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT17, value);
      else if (AddressKeys.COMMONTEXT18.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT18, value);
      else if (AddressKeys.COMMONTEXT19.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT19, value);
      else if (AddressKeys.COMMONTEXT20.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONTEXT20, value);
      
      else if (AddressKeys.COMMONINT01.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT01, value);
      else if (AddressKeys.COMMONINT02.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT02, value);
      else if (AddressKeys.COMMONINT03.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT03, value);
      else if (AddressKeys.COMMONINT04.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT04, value);
      else if (AddressKeys.COMMONINT05.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT05, value);
      else if (AddressKeys.COMMONINT06.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT06, value);
      else if (AddressKeys.COMMONINT07.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT07, value);
      else if (AddressKeys.COMMONINT08.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT08, value);
      else if (AddressKeys.COMMONINT09.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT09, value);
      else if (AddressKeys.COMMONINT10.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONINT10, value);

      else if (AddressKeys.COMMONBOOL01.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL01, value);
      else if (AddressKeys.COMMONBOOL02.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL02, value);
      else if (AddressKeys.COMMONBOOL03.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL03, value);
      else if (AddressKeys.COMMONBOOL04.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL04, value);
      else if (AddressKeys.COMMONBOOL05.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL05, value);
      else if (AddressKeys.COMMONBOOL06.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL06, value);
      else if (AddressKeys.COMMONBOOL07.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL07, value);
      else if (AddressKeys.COMMONBOOL08.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL08, value);
      else if (AddressKeys.COMMONBOOL09.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL09, value);
      else if (AddressKeys.COMMONBOOL10.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONBOOL10, value);
      
      else if (AddressKeys.COMMONDATE01.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE01, value);
      else if (AddressKeys.COMMONDATE02.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE02, value);
      else if (AddressKeys.COMMONDATE03.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE03, value);
      else if (AddressKeys.COMMONDATE04.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE04, value);
      else if (AddressKeys.COMMONDATE05.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE05, value);
      else if (AddressKeys.COMMONDATE06.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE06, value);
      else if (AddressKeys.COMMONDATE07.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE07, value);
      else if (AddressKeys.COMMONDATE08.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE08, value);
      else if (AddressKeys.COMMONDATE09.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE09, value);
      else if (AddressKeys.COMMONDATE10.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONDATE10, value);
      
      else if (AddressKeys.COMMONMONEY01.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY01, value);
      else if (AddressKeys.COMMONMONEY02.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY02, value);
      else if (AddressKeys.COMMONMONEY03.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY03, value);
      else if (AddressKeys.COMMONMONEY04.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY04, value);
      else if (AddressKeys.COMMONMONEY05.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY05, value);
      else if (AddressKeys.COMMONMONEY06.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY06, value);
      else if (AddressKeys.COMMONMONEY07.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY07, value);
      else if (AddressKeys.COMMONMONEY08.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY08, value);
      else if (AddressKeys.COMMONMONEY09.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY09, value);
      else if (AddressKeys.COMMONMONEY10.equals(key)) m_oAddress.setStandardData(Address.PROPERTY_COMMONMONEY10, value);      
      
      else return false;

      return true;
  }
  
  
  
  
  /**
   * Mit dieser Methode kann der Datentyp eines Feldes im Address-Objekt ermittelt werden.
   * 
   * @param key - der Schl�ssel des Feldes im Address-Objekt
   * @return Class - Die Klasse des verwendeten Datentyps oder null wenn das Feld nicht existiert oder andere Probleme beim Ermitteln des Typs auftraten.
   * 
   */
  public Class getDatatype(Object key)
  {
    Class known = getKnownDatatype(key);
    if (known != null) return known;
    else
    {
      Object value = get(key);
      if (value != null) return value.getClass();
      else return null;
    }
  }  

  
  // ------- private Hilfsmethoden f�r getDatattype(); --------------
  
  private Class getKnownDatatype(Object key)
  {
    if (key instanceof String)
    {
      String keystr = ((String)key);
      
      if (keystr.startsWith("tc."))
      {
        // tarentContact Daten

        keystr = keystr.substring(3);
        if (keystr.startsWith("std."))
        {
          keystr = keystr.substring(4);
          return getKnownStdDatatype((Object)keystr);
        } 
        else if (keystr.startsWith("ext."))
        {
            throw new RuntimeException("Configuration error: Extended Address Data is not supported any more.");
//             keystr = keystr.substring(4);
            
//             try
//                 {
//                     Object value = m_oAddress.getExtendedData(keystr);
//                     if (value != null)
//                         {
//                             return value.getClass();
//                         }
//                 }
//             catch (ContactDBException e) {}
        }
      }
      else
      {
        // kein namespace angegeben... 
        // aus kompatiblit�tsgr�nden
        // die alte Methode aufrufen!
        return getKnownStdDatatype((Object)keystr);
      }
    }  
    return null;
  }
  
  
  private Class getKnownStdDatatype(Object key)
  {
    if (AddressKeys.ABTEILUNG.equals(key)) return String.class;
    else if (AddressKeys.AKADTITEL.equals(key)) return String.class;
    else if (AddressKeys.ANREDE.equals(key)) return String.class;
    else if (AddressKeys.BANKCODE.equals(key)) return String.class;
    else if (AddressKeys.BANKKONTO.equals(key)) return String.class;
    else if (AddressKeys.BANKNAME.equals(key)) return String.class;
    else if (AddressKeys.BUNDESLAND.equals(key)) return String.class;
    else if (AddressKeys.EMAIL.equals(key)) return String.class;
    else if (AddressKeys.EMAIL2.equals(key)) return String.class;
    else if (AddressKeys.FAXDIENST.equals(key)) return String.class;
    else if (AddressKeys.FAXPRIVAT.equals(key)) return String.class;
    else if (AddressKeys.FOLLOWUP.equals(key)) return Boolean.class;
    else if (AddressKeys.FOLLOWUPDATE.equals(key)) return Date.class;
    else if (AddressKeys.FK_USER_FOLLOWUP.equals(key)) return Integer.class;
    
    else if (AddressKeys.GEBURTSDATUM.equals(key)) return Date.class;
    else if (AddressKeys.GEBURTSJAHR.equals(key)) return Integer.class;
    else if (AddressKeys.GESCHLECHT.equals(key)) return Character.class;
    else if (AddressKeys.HANDYDIENST.equals(key)) return String.class;
    else if (AddressKeys.HANDYPRIVAT.equals(key)) return String.class;
    else if (AddressKeys.HAUSNUMMER.equals(key)) return String.class;
    else if (AddressKeys.HERRFRAU.equals(key)) return String.class;
    else if (AddressKeys.HOMEPAGE.equals(key)) return String.class;
    else if (AddressKeys.INSTITUTION.equals(key)) return String.class;
    else if (AddressKeys.INSTITUTION2.equals(key)) return String.class;
    else if (AddressKeys.LAND.equals(key)) return String.class;
    else if (AddressKeys.LETTERADDRESS.equals(key)) return String.class;
    else if (AddressKeys.LETTER_AUTO.equals(key)) return Boolean.class;
    else if (AddressKeys.LETTERSALUTATION.equals(key)) return String.class;
    else if (AddressKeys.LETTERSALUTATION_AUTO.equals(key)) return Boolean.class;
    else if (AddressKeys.NACHNAME.equals(key)) return String.class;
    else if (AddressKeys.NAMENSZUSATZ.equals(key)) return String.class;
    else if (AddressKeys.NOTIZ.equals(key)) return String.class;
    else if (AddressKeys.ORT.equals(key)) return String.class;
    else if (AddressKeys.PICTURE.equals(key)) return byte[].class; 
    else if (AddressKeys.PICTUREEXPRESSION.equals(key)) return String.class; 
    else if (AddressKeys.PLZ.equals(key)) return String.class;
    else if (AddressKeys.POSITION.equals(key)) return String.class;
    else if (AddressKeys.POSTFACH.equals(key)) return String.class;
    else if (AddressKeys.POSTFACHPLZ.equals(key)) return String.class;
    else if (AddressKeys.SPITZNAME.equals(key)) return String.class;
    else if (AddressKeys.STRASSE.equals(key)) return String.class;
    else if (AddressKeys.TELEFONDIENST.equals(key)) return String.class;
    else if (AddressKeys.TELEFONPRIVAT.equals(key)) return String.class;
    else if (AddressKeys.TITEL.equals(key)) return String.class;
    else if (AddressKeys.VORNAME.equals(key)) return String.class;
    else if (AddressKeys.WEITEREVORNAMEN.equals(key)) return String.class;
    else if (AddressKeys.STICHWORTE.equals(key)) return String.class;
    else if (AddressKeys.EMAIL3.equals(key)) return String.class;
    else if (AddressKeys.FAX3.equals(key)) return String.class;
    else if (AddressKeys.HANDY3.equals(key)) return String.class;
    else if (AddressKeys.TELEFON3.equals(key)) return String.class;
    else if (AddressKeys.HOMEPAGE2.equals(key)) return String.class;
    else if (AddressKeys.HOMEPAGE3.equals(key)) return String.class;
    else if (AddressKeys.INSTITUTION3.equals(key)) return String.class;
    else if (AddressKeys.NAMEN3.equals(key)) return String.class;

    else if (AddressKeys.ADRESSZUSATZ2.equals(key)) return String.class;
    else if (AddressKeys.BUNDESLAND2.equals(key)) return String.class;
    else if (AddressKeys.HAUSNUMMER2.equals(key)) return String.class;
    else if (AddressKeys.LAND2.equals(key)) return String.class;
    else if (AddressKeys.LKZ2.equals(key)) return String.class;
    else if (AddressKeys.ORT2.equals(key)) return String.class;
    else if (AddressKeys.PLZ2.equals(key)) return String.class;
    else if (AddressKeys.POSTFACH2.equals(key)) return String.class;
    else if (AddressKeys.POSTFACHPLZ2.equals(key)) return String.class;
    else if (AddressKeys.STRASSE2.equals(key)) return String.class;
    
    else if (AddressKeys.ADRESSZUSATZ3.equals(key)) return String.class;
    else if (AddressKeys.BUNDESLAND3.equals(key)) return String.class;
    else if (AddressKeys.HAUSNUMMER3.equals(key)) return String.class;
    else if (AddressKeys.LAND3.equals(key)) return String.class;
    else if (AddressKeys.LKZ3.equals(key)) return String.class;
    else if (AddressKeys.ORT3.equals(key)) return String.class;
    else if (AddressKeys.PLZ3.equals(key)) return String.class;
    else if (AddressKeys.POSTFACH3.equals(key)) return String.class;
    else if (AddressKeys.POSTFACHPLZ3.equals(key)) return String.class;
    else if (AddressKeys.STRASSE3.equals(key)) return String.class;

    else if (AddressKeys.COMMONTEXT01.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT02.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT03.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT04.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT05.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT06.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT07.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT08.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT09.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT10.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT11.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT12.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT13.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT14.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT15.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT16.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT17.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT18.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT19.equals(key)) return String.class;
    else if (AddressKeys.COMMONTEXT20.equals(key)) return String.class;
    
    else if (AddressKeys.COMMONINT01.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT02.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT03.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT04.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT05.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT06.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT07.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT08.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT09.equals(key)) return Integer.class;
    else if (AddressKeys.COMMONINT10.equals(key)) return Integer.class;

    else if (AddressKeys.COMMONBOOL01.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL02.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL03.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL04.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL05.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL06.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL07.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL08.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL09.equals(key)) return Boolean.class;
    else if (AddressKeys.COMMONBOOL10.equals(key)) return Boolean.class;
    
    else if (AddressKeys.COMMONDATE01.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE02.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE03.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE04.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE05.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE06.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE07.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE08.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE09.equals(key)) return Date.class;
    else if (AddressKeys.COMMONDATE10.equals(key)) return Date.class;
    
    else if (AddressKeys.COMMONMONEY01.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY02.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY03.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY04.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY05.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY06.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY07.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY08.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY09.equals(key)) return Double.class;
    else if (AddressKeys.COMMONMONEY10.equals(key)) return Double.class;
    
    else if (AddressKeys.BLOCKEDFORCONSIGNMENT.equals(key)) return Boolean.class;
    
    else return null;    
  }
  

  public static String getName(Object key)
  {
    if (key instanceof String)
    {
      String keystr = ((String)key);
      
      if (keystr.startsWith("tc."))
      {
        // tarentContact Daten

        keystr = keystr.substring(3);
        if (keystr.startsWith("std."))
        {
          keystr = keystr.substring(4);
          return getStdName((Object)keystr);
        } 
        else if (keystr.startsWith("ext."))
        {
          keystr = keystr.substring(4);
          return getStdName((Object)keystr);          
        }
      }
      else
      {
        // kein namespace angegeben... 
        // aus kompatiblit�tsgr�nden
        // die alte Methode aufrufen!
        return getStdName((Object)keystr);
      }
    }
    
    return null;
  }

  
  
  
  private static String getStdName(Object key)
  {
    if (AddressKeys.ABTEILUNG.equals(key)) return "Abteilung";
    else if (AddressKeys.AKADTITEL.equals(key)) return "akad. Titel";
    else if (AddressKeys.ANREDE.equals(key)) return "Anrede";
    else if (AddressKeys.BANKCODE.equals(key)) return "Bankcode";
    else if (AddressKeys.BANKKONTO.equals(key)) return "Konto";
    else if (AddressKeys.BANKNAME.equals(key)) return "Bankname";
    else if (AddressKeys.BUNDESLAND.equals(key)) return "Bundesland";
    else if (AddressKeys.EMAIL.equals(key)) return "eMail";
    else if (AddressKeys.EMAIL2.equals(key)) return "weitere eMail";
    else if (AddressKeys.FAXDIENST.equals(key)) return "Fax dienstl.";
    else if (AddressKeys.FAXPRIVAT.equals(key)) return "Fax privat";
    else if (AddressKeys.FOLLOWUP.equals(key)) return "zu pr�fen";
    else if (AddressKeys.FOLLOWUPDATE.equals(key)) return "zu pr�fen ab Datum";
    else if (AddressKeys.FK_USER_FOLLOWUP.equals(key)) return "zu pr�fen von User-ID";
    else if (AddressKeys.GEBURTSDATUM.equals(key)) return "Geburtsdatum";
    else if (AddressKeys.GEBURTSJAHR.equals(key)) return "Geburtsjahr";
    else if (AddressKeys.GESCHLECHT.equals(key)) return "Geschlecht";
    else if (AddressKeys.HANDYDIENST.equals(key)) return "Handy dienstl.";
    else if (AddressKeys.HANDYPRIVAT.equals(key)) return "Handy privat";
    else if (AddressKeys.HAUSNUMMER.equals(key)) return "Hausnummer";
    else if (AddressKeys.HERRFRAU.equals(key)) return "Herr/Frau";
    else if (AddressKeys.HOMEPAGE.equals(key)) return "Homepage";
    else if (AddressKeys.INSTITUTION.equals(key)) return "Institution";
    else if (AddressKeys.INSTITUTION2.equals(key)) return "weitere Institution";
    else if (AddressKeys.LAND.equals(key)) return "Land";
    else if (AddressKeys.LETTERADDRESS.equals(key)) return "Briefanschrift";
    else if (AddressKeys.LETTER_AUTO.equals(key)) return "Briefanschrift-Flag";
    else if (AddressKeys.LETTERSALUTATION.equals(key)) return "Briefanrede";
    else if (AddressKeys.LETTERSALUTATION_AUTO.equals(key)) return "Briefanrede-Flag";
    else if (AddressKeys.NACHNAME.equals(key)) return "Nachname";
    else if (AddressKeys.NAMENSZUSATZ.equals(key)) return "Namenszusatz";
    else if (AddressKeys.NOTIZ.equals(key)) return "Notiz";
    else if (AddressKeys.ORT.equals(key)) return "Ort";
    else if (AddressKeys.PICTURE.equals(key)) return "Bild";   
    else if (AddressKeys.PICTUREEXPRESSION.equals(key)) return "Bildname"; 
    else if (AddressKeys.PLZ.equals(key)) return "PLZ";
    else if (AddressKeys.POSITION.equals(key)) return "Position";
    else if (AddressKeys.POSTFACH.equals(key)) return "Postfach";
    else if (AddressKeys.POSTFACHPLZ.equals(key)) return "Postfach PLZ";
    else if (AddressKeys.SPITZNAME.equals(key)) return "Spitzname";
    else if (AddressKeys.STRASSE.equals(key)) return "Stra�e";
    else if (AddressKeys.TELEFONDIENST.equals(key)) return "Tel. dienstl.";
    else if (AddressKeys.TELEFONPRIVAT.equals(key)) return "Tel. privat";
    else if (AddressKeys.TITEL.equals(key)) return "Titel";
    else if (AddressKeys.VORNAME.equals(key)) return "Vorname";
    else if (AddressKeys.WEITEREVORNAMEN.equals(key)) return "weitere Vornamen";
    else if (AddressKeys.STICHWORTE.equals(key)) return "Stichworte";
    else if (AddressKeys.EMAIL3.equals(key)) return "3. eMail";
    else if (AddressKeys.FAX3.equals(key)) return "3. Fax";
    else if (AddressKeys.HANDY3.equals(key)) return "3. Handy";
    else if (AddressKeys.TELEFON3.equals(key)) return "3. Tel";      
    else if (AddressKeys.HOMEPAGE2.equals(key)) return "weitere Homepage";
    else if (AddressKeys.HOMEPAGE3.equals(key)) return "3. Homepage";
    else if (AddressKeys.INSTITUTION3.equals(key)) return "3. Institution";
    else if (AddressKeys.NAMEN3.equals(key)) return "3. Namen";
    
    else if (AddressKeys.ADRESSZUSATZ2.equals(key)) return "2. Adresszusatz";
    else if (AddressKeys.BUNDESLAND2.equals(key)) return "2. Bundesland";
    else if (AddressKeys.HAUSNUMMER2.equals(key)) return "2. Hausnummer";
    else if (AddressKeys.LAND2.equals(key)) return "2. Land";
    else if (AddressKeys.LKZ2.equals(key)) return "2. LKZ";
    else if (AddressKeys.ORT2.equals(key)) return "2. Ort";
    else if (AddressKeys.PLZ2.equals(key)) return "2. PLZ";
    else if (AddressKeys.POSTFACH2.equals(key)) return "2. Postfach";
    else if (AddressKeys.POSTFACHPLZ2.equals(key)) return "2. Postfach PLZ";
    else if (AddressKeys.STRASSE2.equals(key)) return "2. Stra�e";
    
    else if (AddressKeys.ADRESSZUSATZ3.equals(key)) return "3. Adresszusatz";
    else if (AddressKeys.BUNDESLAND3.equals(key)) return "3. Bundesland";
    else if (AddressKeys.HAUSNUMMER3.equals(key)) return "3. Hausnummer";
    else if (AddressKeys.LAND3.equals(key)) return "3. Land";
    else if (AddressKeys.LKZ3.equals(key)) return "3. LKZ";
    else if (AddressKeys.ORT3.equals(key)) return "3. Ort";
    else if (AddressKeys.PLZ3.equals(key)) return "3. PLZ";
    else if (AddressKeys.POSTFACH3.equals(key)) return "3. Postfach";
    else if (AddressKeys.POSTFACHPLZ3.equals(key)) return "3. Postfach PLZ";
    else if (AddressKeys.STRASSE3.equals(key)) return "3. Stra�e";
    
    else if (AddressKeys.BLOCKEDFORCONSIGNMENT.equals(key)) return "Sperrvermerk";
    
    else return null;    
  }


  
  public static String getDBKeyOfFieldKey(Object key)
  {
    Object dbkey = getDBKeyOfFieldKey(key);
    if (dbkey != null) return "tc.std." + dbkey;
    else return null;
  }
  
  
  
  private Object getDBKeyOfStdFieldKey(String key)
  {   
    //else if (Address.CAT_FIELD_BEMERKUNG.equals(key)) return AdressKeys.NOTIZ;
    if (Address.PROPERTY_AKAD_TITEL.equals(key)) return AddressKeys.AKADTITEL;
    
    else if (Address.PROPERTY_ANREDE.equals(key)) return AddressKeys.ANREDE;
    else if (Address.PROPERTY_BUNDESLAND.equals(key)) return AddressKeys.BUNDESLAND;
     else if (Address.PROPERTY_E_MAIL_DIENSTLICH.equals(key)) return AddressKeys.EMAIL;    
    else if (Address.PROPERTY_E_MAIL_PRIVAT.equals(key)) return AddressKeys.EMAIL2;
    else if (Address.PROPERTY_E_MAIL_WEITERE.equals(key)) return AddressKeys.EMAIL3;
    else if (Address.PROPERTY_FAX_DIENSTLICH.equals(key)) return AddressKeys.FAXDIENST;
    else if (Address.PROPERTY_FAX_PRIVAT.equals(key)) return AddressKeys.FAXPRIVAT;
    else if (Address.PROPERTY_FAX_WEITERES.equals(key)) return AddressKeys.FAX3;
    else if (Address.PROPERTY_FOLLOWUP.equals(key)) return AddressKeys.FOLLOWUP;
    else if (Address.PROPERTY_FOLLOWUPDATE.equals(key)) return AddressKeys.FOLLOWUPDATE;
    else if (Address.PROPERTY_FK_USER_FOLLOWUP.equals(key)) return AddressKeys.FK_USER_FOLLOWUP;
    else if (Address.PROPERTY_HANDY_DIENSTLICH.equals(key)) return AddressKeys.HANDYDIENST;
    else if (Address.PROPERTY_HANDY_PRIVAT.equals(key)) return AddressKeys.HANDYPRIVAT;
    else if (Address.PROPERTY_HANDY_WEITERES.equals(key)) return AddressKeys.HANDY3;
    else if (Address.PROPERTY_HAUS_NR.equals(key)) return AddressKeys.HAUSNUMMER;
    else if (Address.PROPERTY_HERRN_FRAU.equals(key)) return AddressKeys.HERRFRAU;
    else if (Address.PROPERTY_HOMEPAGE_DIENSTLICH.equals(key)) return AddressKeys.HOMEPAGE;
    else if (Address.PROPERTY_HOMEPAGE_PRIVAT.equals(key)) return AddressKeys.HOMEPAGE2;
    else if (Address.PROPERTY_HOMEPAGE_WEITERE.equals(key)) return AddressKeys.HOMEPAGE3;
    else if (Address.PROPERTY_INSTITUTION.equals(key)) return AddressKeys.INSTITUTION;
    else if (Address.PROPERTY_LAND.equals(key)) return AddressKeys.LAND;
    else if (Address.PROPERTY_LETTERADDRESS.equals(key)) return AddressKeys.LETTERADDRESS;
    else if (Address.PROPERTY_LETTER_SALUTATION_AUTO.equals(key)) return AddressKeys.LETTER_AUTO;
    else if (Address.PROPERTY_LETTERSALUTATION.equals(key)) return AddressKeys.LETTERSALUTATION;
    
    //else if (Address.PROPERTY_LKZ.equals(key)) return AdressKeys.LKZ;
    //else if (Address.PROPERTY_LKZ2.equals(key)) return AdressKeys.LKZ2;
    else if (Address.PROPERTY_NACHNAME.equals(key)) return AddressKeys.NACHNAME;
    else if (Address.PROPERTY_NAMENSZUSATZ.equals(key)) return AddressKeys.NAMENSZUSATZ;
    else if (Address.PROPERTY_ORT.equals(key)) return AddressKeys.ORT;
    else if (Address.PROPERTY_PICTURE.equals(key)) return AddressKeys.PICTURE;
    else if (Address.PROPERTY_PICTUREEXPRESSION.equals(key)) return AddressKeys.PICTUREEXPRESSION;
    else if (Address.PROPERTY_POSTFACH_NR.equals(key)) return AddressKeys.POSTFACH;
    else if (Address.PROPERTY_STRASSE.equals(key)) return AddressKeys.STRASSE;
    else if (Address.PROPERTY_TELEFON_DIENSTLICH.equals(key)) return AddressKeys.TELEFONDIENST;
    else if (Address.PROPERTY_TELEFON_PRIVAT.equals(key)) return AddressKeys.TELEFONPRIVAT;
    else if (Address.PROPERTY_TELEFON_WEITERES.equals(key)) return AddressKeys.TELEFON3;
    else if (Address.PROPERTY_TITEL.equals(key)) return AddressKeys.TITEL;
    else if (Address.PROPERTY_VORNAME.equals(key)) return AddressKeys.VORNAME;
    else if (Address.PROPERTY_PLZ .equals(key)) return AddressKeys.PLZ;
    
    else if (Address.PROPERTY_PLZ_POSTFACH.equals(key)) return AddressKeys.POSTFACHPLZ;
    
    //else if (Address.PROPERTY_ADRESSZUSATZ.equals(key)) return AddressKeys.ADDRESSZUSATZ;
    
    //else if (Address.PROPERTY_NAMEN_ERWEITERT.equals(key)) return AdressKeys.NAMEERWEITERT; //"nameErweitert";
    else if (Address.PROPERTY_SPITZNAME.equals(key)) return AddressKeys.SPITZNAME;
    //else if (Address.PROPERTY_INSTITUTION_WEITERE.equals(key)) return AdressKeys.INSTITUTIONERWEITERT; //"institutionErweitert";
    else if (Address.PROPERTY_ABTEILUNG.equals(key)) return AddressKeys.ABTEILUNG;
    else if (Address.PROPERTY_NOTIZ.equals(key)) return AddressKeys.NOTIZ; 
    else if (Address.PROPERTY_POSITION.equals(key)) return AddressKeys.POSITION;
    else if (Address.PROPERTY_BANK_NAME.equals(key)) return AddressKeys.BANKNAME;
    else if (Address.PROPERTY_BANK_CODE.equals(key)) return AddressKeys.BANKCODE;
    else if (Address.PROPERTY_BANK_KONTO.equals(key)) return AddressKeys.BANKKONTO;
    else if (Address.PROPERTY_GESCHLECHT.equals(key)) return AddressKeys.GESCHLECHT;
    else if (Address.PROPERTY_GEBURTSJAHR.equals(key)) return AddressKeys.GEBURTSJAHR;
    else if (Address.PROPERTY_GEBURTSDATUM.equals(key)) return AddressKeys.GEBURTSDATUM;
    //else if (Address.PROPERTY_LABEL.equals(key)) return AdressKeys.LABEL; //"labelfn";
    
    else if (Address.PROPERTY_STRASSE2.equals(key)) return AddressKeys.STRASSE2;
    else if (Address.PROPERTY_PLZ_POSTFACH2.equals(key)) return AddressKeys.POSTFACHPLZ2;
    else if (Address.PROPERTY_ADDRESSZUSATZ2.equals(key)) return AddressKeys.ADRESSZUSATZ2;
    else if (Address.PROPERTY_BUNDESLAND2.equals(key)) return AddressKeys.BUNDESLAND2;
    else if (Address.PROPERTY_PLZ2.equals(key)) return AddressKeys.PLZ2;
    else if (Address.PROPERTY_POSTFACH_NR2.equals(key)) return AddressKeys.POSTFACH2;
    else if (Address.PROPERTY_ORT2.equals(key)) return AddressKeys.ORT2;
    else if (Address.PROPERTY_LAND2.equals(key)) return AddressKeys.LAND2;
    else if (Address.PROPERTY_HAUS_NR2.equals(key)) return AddressKeys.HAUSNUMMER2;
    
    else if (Address.PROPERTY_STRASSE3.equals(key)) return AddressKeys.STRASSE3;
    else if (Address.PROPERTY_PLZ_POSTFACH3.equals(key)) return AddressKeys.POSTFACHPLZ3;
    else if (Address.PROPERTY_ADDRESSZUSATZ3.equals(key)) return AddressKeys.ADRESSZUSATZ3;
    else if (Address.PROPERTY_BUNDESLAND3.equals(key)) return AddressKeys.BUNDESLAND3;
    else if (Address.PROPERTY_PLZ3.equals(key)) return AddressKeys.PLZ3;
    else if (Address.PROPERTY_POSTFACH_NR3.equals(key)) return AddressKeys.POSTFACH3;
    else if (Address.PROPERTY_ORT3.equals(key)) return AddressKeys.ORT3;
    else if (Address.PROPERTY_LAND3.equals(key)) return AddressKeys.LAND3;
    else if (Address.PROPERTY_HAUS_NR3.equals(key)) return AddressKeys.HAUSNUMMER3;
    
    
    else if (Address.PROPERTY_ADDRESSZUSATZ3.equals(key)) return AddressKeys.ADRESSZUSATZ3;
    
    else if (Address.PROPERTY_BLOCKEDFORCONSIGNMENT.equals(key)) return AddressKeys.BLOCKEDFORCONSIGNMENT;
    
    return null;
  }
  
  
  
}
