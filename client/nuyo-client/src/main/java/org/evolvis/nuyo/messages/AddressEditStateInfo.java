/* $Id: AddressEditStateInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * 
 * Created on 16.10.2003
 */
package org.evolvis.nuyo.messages;

import org.evolvis.nuyo.db.Address;

/**
 * Dieses Ereignis zeigt an, dass der Bearbeitungsmodus der aktuellen
 * Adresse sich ge�ndert hat.
 * 
 * @author mikel
 */
public class AddressEditStateInfo extends AddressInfo {
    /*
     * Konstruktoren
     */
    /**
     * @param source Ereignisquelle
     * @param address betroffene Addresse
     * @param isEditable jetziger Bearbeitungsmodus
     */
    public AddressEditStateInfo(Object source, Address address, boolean isEditable) {
        super(source, address);
        this.isEditable = isEditable;
    }

    /*
     * Getter und Setter
     */
    /**
     * @return jetziger Bearbeitungsmodus
     */
    public boolean isEditable() {
        return isEditable;
    }

    /*
     * Klasse Object
     */
    public String toString() {
        return super.toString() + "[edit: " + isEditable + ']';
    }

    /*
     * gesch�tzte Variablen 
     */
    boolean isEditable;
}
