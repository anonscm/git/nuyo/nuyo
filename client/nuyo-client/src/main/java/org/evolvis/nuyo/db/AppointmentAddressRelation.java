/* $Id: AppointmentAddressRelation.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

/**
 * Diese Schnittstelle stellt Attribute der Termin-Address-Relation dar.
 * 
 * @author mikel
 */
public interface AppointmentAddressRelation extends AppointmentRequest {
    //
    // Attribute
    //
    /** zum Termin assoziierte Adresse */
    public Address getAddress() throws ContactDBException;
}
