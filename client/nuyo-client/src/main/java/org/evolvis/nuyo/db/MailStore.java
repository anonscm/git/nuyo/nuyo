/* $Id: MailStore.java,v 1.3 2006/12/12 09:31:04 robert Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink and Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.List;

import org.evolvis.nuyo.db.persistence.PersistentEntity;


/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.3 $
 */
public interface MailStore extends PersistentEntity {
	//
	// Attribute
	//
	/**
	 * ID des Stores
	 */
	public int getStoreId() throws ContactDBException;
	
	//
	// Textattribute
	//
	/**
	 * vgl. Konstanten <code>KEY_*</code> und <code>VALUE_*</code>
	 */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;
	
    /**
     * Diese Methode aktualisiert die gecacheten Informationen zu Foldern etc. 
     * 
     * @throws ContactDBException
     */
	public void reload() throws ContactDBException;
	
    //
    // Wurzelverzeichnisse
    //
	/**
	 * Liste der Wurzel-Folder (MailFolder-Instanzen)
	 * 
	 * @throws ContactDBException
	 */
    public List getRootFolders() throws ContactDBException;
    
    /**
     * Diese Methode erzeugt einen neuen Wurzel-Folder
     * 
     * @param name Name des zu erzeugenden Folders
     * @return der neu erzeugte Folder.
     * @throws ContactDBException
     */
    public MailFolder createRootFolder(String name) throws ContactDBException;

	/**
	 * Diese Methode gibt zur�ck ob deiesr Store
	 * bereit ist angezeigt zu werden.
	 * 
	 * @throws ContactDBException
	 */
	public boolean isInit() throws ContactDBException;
	
    //
    // Konstanten
    //
    /**
     * Textattribut-Schl�ssel:
     * KEY_DISPLAY - Name unter dem der User einen eMailStore angezeigt bekommen soll.
     * KEY_USER - Benutzername (zur Authentifizierung beim eMail-Server)
     * KEY_PASSWORD - Passwort (zur Authentifizierung beim eMail-Server)
     * KEY_PROTOCOL - Mail-Protokol
     * KEY_SERVER - Server-IP bzw. Domainname
     * KEY_SERVERPORT - Server-Port
     * KEY_INBOX_FOLDER - Ordner in dem empfangene Mitteilungen abgelegt werden.
     * KEY_SEND_FOLDER - Ordner in dem gesendete Mitteilungen abgelegt werden.
     * KEY_TRASH_FOLDER - Ordner in dem gel�schte Mitteilungen abgelegt werden.
     */
    public final static String KEY_DISPLAY = "display";
    public final static String KEY_USER = "user";
    public final static String KEY_PASSWORD = "password";
    public final static String KEY_PROTOCOL = "protocol";
    public final static String KEY_SERVER = "server";
    public final static String KEY_SERVERPORT = "port";
    public final static String KEY_INBOX_FOLDER = "inbox";
    public final static String KEY_SEND_FOLDER = "send";
    public final static String KEY_TRASH_FOLDER = "trash";
    
    /**
     * Textattribut-Werte:
     * VALUE_SERVERPORT_DEFAULT - Default Port vom jeweiligem Protokol verwenden.
     */
    public final static String VALUE_SERVERPORT_DEFAULT = null;
}
