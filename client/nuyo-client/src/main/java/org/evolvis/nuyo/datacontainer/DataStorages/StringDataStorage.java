/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataStorages;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorage;
import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorageAdapter;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueAcceptEvent;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StringDataStorage extends DataStorageAdapter implements TarentGUIEventListener
{
  private String m_sString = null;
  
  public String getListenerName()
  {
    return "StringDataStorage";
  }  
  
  public DataStorage cloneDataStorage()
  {
    StringDataStorage storage = new StringDataStorage();
    storage.m_sString = m_sString;    
    return storage;
  }
  
  public Class stores()
  {
    return String.class;
  }

  public List getEventsConsumable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_ACCEPT);
    list.add(TarentGUIEvent.GUIEVENT_REJECT);
    return list;
  }

  public List getEventsFireable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_VALUECHANGED);
    return list;
  }

  boolean m_bIsInitialized = false;
  public void init()
  {
    if (! m_bIsInitialized) 
    {
      getDataContainer().addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_ACCEPT, this);
      getDataContainer().addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_REJECT, this);
    }
    m_bIsInitialized = true;
  }

  public void dispose()
  {        
    getDataContainer().removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_ACCEPT, this);    
  }

  public void event(TarentGUIEvent tge)
  {
    if (tge instanceof TarentGUIValueAcceptEvent)
    {      
      Object data = ((TarentGUIValueAcceptEvent)tge).getValue();
      setData(data, false);     
      setValid(true);
    }
    else if (TarentGUIEvent.GUIEVENT_REJECT.equals(tge.getName()))
    {
      setValid(false);
    }    
  }
}
