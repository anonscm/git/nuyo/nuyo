package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SortDownAction extends AbstractGUIAction {

    private static final long serialVersionUID = -7702023780033806838L;
    private static final TarentLogger log = new TarentLogger(SortDownAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SortDownAction not implemented");
    }

}