/*
 * Created on 13.10.2004
 */
package org.evolvis.nuyo.gui;

import org.evolvis.nuyo.util.general.ContactLookUpNotFoundException;
import org.evolvis.nuyo.util.general.DataAccess;

/**
 * @author niko
 *
 * 
 */
public class GuiListenerDataAccess implements DataAccess
{
  private GUIListener m_oGUIListener;
  
  public GuiListenerDataAccess(GUIListener oGUIListener)
  {
    m_oGUIListener = oGUIListener;
  }
  
  public Object getLookupTableValue(String key, Object data) throws ContactLookUpNotFoundException
  {
    return m_oGUIListener.getLookupTableValue(key, data);
  }

  public String[] getLookupTableEntries(String key) throws ContactLookUpNotFoundException
  {
    return m_oGUIListener.getLookupTableEntries(key);
  }
}
