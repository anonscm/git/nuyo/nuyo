/* $Id: CollapsePane.java,v 1.8 2007/08/30 16:10:32 fkoester Exp $
 */
 
/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2006 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact' written
* by Robert Schuster 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 

package org.evolvis.nuyo.controls;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.plaf.UIResource;

/**
 * <p>A pane with a button and a content area of which visibility
 * can be toggled.</p>
 * 
 * <p>Clicking on the button lets all components in the content area
 * collapse under the button. This is done by switching the invisibility
 * property of the components.</p>
 * 
 * <p>In combination with other components this class is used to
 * implement the side bar menu.</p>
 * 
 * <p>In its current design and implementation the pane has the
 * content area at the bottom and gives all available space to
 * the content component. The button is at the top, has two 
 * fixed icons for its state, takes the complete horizontal
 * space of the pane and allows a text on it.</p>
 * 
 * <p>The content area is not a separate component. All items
 * added by the {@link add} methods are children of this component.
 * </p>
 * 
 * <p>Without the icons the component is not tarent-contact specific
 * and may be moved into a general GUI library.</p>
 *  
 * @author Robert Schuster
 *
 */
public class CollapsePane extends JComponent
{

  private JToggleButton trigger;
  
  private ArrayList items = new ArrayList();
  
  private boolean collapsed = false;

  public CollapsePane(String text)
  {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    
    // The button which allows toggling the visibility
    // of the content area should take the complete
    // horizontal space of the pane.
    trigger = new Trigger(text);
    trigger.setHorizontalTextPosition(SwingConstants.LEFT);
    trigger.setHorizontalAlignment(SwingConstants.TRAILING);
    trigger.setSelected(true);
    
    // BoxLayout respects a component's alignment setting
    // and these are not neccessarily well defined after
    // instantiation, so setting them is explicitly needed. 
    trigger.setAlignmentX(LEFT_ALIGNMENT);
    
    trigger.setIcon(loadIcon("/de/tarent/controls/resources/collapsed.gif"));
    trigger.setSelectedIcon(loadIcon("/de/tarent/controls/resources/expanded.gif"));
    trigger.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        toggleCollapseState();
        
        // Toggling the collapse state changes the size requirements
        // of the CollapsePane component. The following call causes
        // a recalculation of the layout.
        CollapsePane.this.getRootPane().revalidate();
      }
    });
    
    add(trigger);
  }
  
  /** Adds a child component to the <code>CollapsePane</code>.
   * 
   * <p>The child component's maximum width will be set to take
   * all available space.</p>
   * 
   */
  public Component add(Component c, int index)
  {
    // Prevents treating components implementing UIResource
    // as children of the CollapsePane whose visibility state
    // can be switched. This is used for the pane's trigger
    // button.
    if (c instanceof UIResource)
      return super.add(c, index);
    
    items.add(c);
    
    // Makes components added to the CollapsePane use the maximum
    // width possible. 
    Dimension d = getMaximumSize();
    d.width = Short.MAX_VALUE;
    
    // Type-cast neccessary for 1.4-compatibility.
    ((JComponent) c).setMaximumSize(d);
    
    c = super.add(c, index);
    
    revalidate();
    
    return c;
  }

  private ImageIcon loadIcon(String url)
  {
    return new ImageIcon(getClass().getResource(url));
  }

  private void toggleCollapseState()
  {
    collapsed = !collapsed;
    
    Iterator ite = items.iterator();
    while (ite.hasNext())
      ((JComponent) ite.next()).setVisible(!collapsed);
      
  }
  
  public String getText()
  {
    return trigger.getText();
  }

  public void setText(String t)
  {
    trigger.setText(t);
  }
  
  /** A <code>JToggleButton</code> derivate which implements
   * <code>UIResource</code> and is therefore treated specially
   * by the <code>CollapsePane</code>.
   * 
   * @see CollapsePane#add(Component, int)
   *
   */
  private static class Trigger extends JToggleButton implements UIResource
  {
    Trigger(String title)
    {
      super(title);
    }
    
    public Dimension getMaximumSize()
    {
      Dimension d = getPreferredSize();
      d.width = Short.MAX_VALUE;
      return d;
    }
  };
  
}
