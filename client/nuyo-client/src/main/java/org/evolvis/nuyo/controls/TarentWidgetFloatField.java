/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * @author niko
 */
public class TarentWidgetFloatField extends JTextField implements TarentWidgetInterface
{
  private FloatDocument     m_oFloatDocument;
  private boolean           m_iIsLengthRestricted;
  private JTextField        m_oThisTextField;
  
  public TarentWidgetFloatField()
  {
    super();
    m_oThisTextField = this;
    m_oFloatDocument = new FloatDocument();
    this.setDocument(m_oFloatDocument);    
  }

  public TarentWidgetFloatField(double preset)
  {
    super();
    m_oThisTextField = this;
    m_oFloatDocument = new FloatDocument();
    this.setDocument(m_oFloatDocument);    
    this.setText(Double.toString(preset));
  }


	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    double i = normalize(data);    
    this.setText((new Double(i)).toString());
	}


	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
    try
    {
		  return new Double(Double.parseDouble(this.getText()));
    }
    catch(NumberFormatException nfe)
    {
      return new Double(0.0);
    }
	}

  public double getDoubleValue() 
  {
    try
    {
      return Double.parseDouble(this.getText());
    }
    catch(NumberFormatException nfe)
    {
      return(0.0);
    }     
  }

  public void setDoubleValue(double i) 
  {
    this.setText((new Double(i)).toString());
  }

  public boolean isEqual(Object data)
  {
    return (getDoubleValue() == normalize(data));    
  }


  private double normalize(Object data)
  {
    if (data instanceof String)
    {
      return Double.parseDouble((String)data);
    }
    else if (data instanceof Integer)
    {
      return new Double((((Integer)data).intValue())).doubleValue();
    }
    else if (data instanceof Float)
    {
      return((double)(((Float)data).floatValue()));
    }
    else if (data instanceof Double)
    {
      return((double)((Double)data).doubleValue());
    }
    else if (data instanceof Boolean)
    {
      return( (((Boolean)data).booleanValue()) ? 1.0 : 0.0);
    }
    return(0.0);
  }


  public void setLengthRestriction(int maxlen)
  {
    if (maxlen > 0)
    {
      m_iIsLengthRestricted = true;
      m_oFloatDocument.setLengthRestriction(maxlen);
    }
    else
    {
      m_iIsLengthRestricted = false;
      m_oFloatDocument.setLengthRestriction(0);
    }
  }

  public boolean isLengthRestricted()
  {
    return(m_iIsLengthRestricted); 
  }


  public JComponent getComponent()
  {
    return(this);
  }
  
  public void setWidgetEditable(boolean iseditable)
  {
    this.setEditable(iseditable);
  }

  public class FloatDocument extends PlainDocument
  {
    private int     m_iMaxChars;
    
    public FloatDocument()
    {
      m_iMaxChars = 0;
    }
    
    public FloatDocument(int maxchars)
    {
      m_iMaxChars = maxchars;
    }
    
    public void setLengthRestriction(int maxchars)
    {
      m_iMaxChars = maxchars;
    }

    
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
    {
      if (str == null) 
      {
        return;
      }
              
      String inttext = "";
      String text = m_oThisTextField.getText();
      boolean founddot = ((text.indexOf('.')) != -1);
      int textlen = text.length();
      
      boolean founderror = false;
      
      for (int i = 0; i < str.length(); i++) 
      {
        char c = str.charAt(i);
        if (Character.isDigit(c) || c == '.' || c == '-')
        {          
          if ((c == '.') && founddot) founderror = true;
          else if ((c == '-') && ((offs + i) != 0)) founderror = true;
          if (! founderror)
          {            
            if (m_iMaxChars != 0)
            {
              if (super.getLength() < m_iMaxChars)            
              //if (inttext.length() < m_iMaxChars)
              {
                inttext = inttext + (str.charAt(i));
              }
            }       
            else
            {
              inttext = inttext + (str.charAt(i));
            }
            
          }
        }
      }
      super.insertString(offs, inttext, a);          
    }
  }


  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  

}
