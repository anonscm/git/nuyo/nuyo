/*
 * $Id: Appointment.java,v 1.4 2006/10/27 13:21:53 aleksej Exp $
 * 
 * Created on 19.05.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright interest in the program 'tarent-contact' (which makes passes at
 * compilers) written by Michael Klink.
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Diese Schnittstelle stellt Termine dar. Diese werden von {@link Schedule}-Instanzen verwaltet. <br>
 * Die hier verwendeten Date-Objekte stellen UTC-Zeiten dar.
 * 
 * @author mikel
 */
public interface Appointment extends HistoryElement {
    //
    // Methoden
    //

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();
    
    /**
     * Diese Methode initiiert das Versenden von Einladungen an die
     * zugeordneten {@link #getAddresses() Adressen}, deren
     * {@link AppointmentRequest#getRequestStatus() Zuordnungsstatus}
     * zum Termin auf {@link AppointmentRequest#STATUS_NEEDS_ACTION}
     * steht; nach Versenden der Einladung steht er auf 
     * {@link AppointmentRequest#STATUS_SENT}.
     * 
     * @return Anzahl der versendeten Einladungen
     */
    public int invite() throws ContactDBException;
    
    /**
     * Diese Methode initiiert das Versenden von Ausladungen an die
     * �bergebenen Adressen.
     * 
     * @param addresses Sammlung von Adressen, die bez�glich dieses
     * 	Termins auszuladen sind.
     * @return Anzahl der versendeten Ausladungen
     */
    public int disinvite(List addresses) throws ContactDBException;
    
    //
    // einfache Attribute
    //
    /** Startdatum und -zeit (inklusive) (UTC) */
    public Date getStart() throws ContactDBException;
    public void setStart(Date newStart) throws ContactDBException;
    
    /** Enddatum und -zeit (exclusive) (UTC) */
    public Date getEnd() throws ContactDBException;
    public void setEnd(Date newEnd) throws ContactDBException;
    
    /** Flag, ob privat */
    public boolean isPrivat() throws ContactDBException;
    public void setPrivate(boolean newIsPrivate) throws ContactDBException;
    
    /** Priorit�t */
    public int getPriority() throws ContactDBException;
    public void setPriority(int newPriority) throws ContactDBException;

    /** Flag, ob Job */
    public boolean isJob() throws ContactDBException;
    public void setJob(boolean newIsJob) throws ContactDBException;
    
    /** Flag, ob Job abgeschlossen */
    public boolean isJobCompleted() throws ContactDBException;
    public void setJobCompleted(boolean newIsJobCompleted) throws ContactDBException;

    /** Grad der Jobbearbeitung in Prozent */
    public int getJobCompletion() throws ContactDBException;
    public void setJobCompletion(int newJobCompletion) throws ContactDBException;

    /** Besitzer des Termins */
    public User getOwner() throws ContactDBException;
    public void setOwner(User newOwner) throws ContactDBException;
    
    /** Art des Termins (Urlaub, Besprechung, Ausw�rts, ...), vgl. Konstanten <code>CATEGORY_*</code> */
    public int getAppointmentCategory() throws ContactDBException;
    public void setAppointmentCategory(int newAppointmentCategory) throws ContactDBException;;

    /** Bei sich wiederholenden Terminen die Terminfolge, sonst null */
    public AppointmentSequence getAppointmentSequence() throws ContactDBException;
    /**
     * Diese Methode erzeugt in den Kalendarien des aktuellen Termins eine Terminreihe, die
     * mit dem aktuellen Termin beginnt, wenn dieser Termin noch nicht Teil einer solchen
     * Serie ist.
     * 
     * @param name Name der Terminreihe.
     * @param end Ende der Terminreihe (UTC).
     * @param frequency Abstand zwischen den Starts aufeineinanderfolgender Termine der neuen Reihe.
     * @param mayConflict wenn dieser Parameter <code>false</code> ist, wird im Falle eines Konflikts
     *  mit bestehenden anderen Terminen in den betroffenen Kalendern eine Ausnahme geworfen. Sonst
     *  wird die neue Terminreihe erzeugt.
     * @return die erzeugte Terminfolge
     * @throws ContactDBException bei Datenzugriffsproblemen. Als Sonderfall dient die Ausnahme-ID
     *  {@link ContactDBException#EX_APPOINTMENT_CONFLICTS}bei Terminkonflikten.
     */
    public AppointmentSequence createAppointmentSequence(String name, Date end, int frequency, boolean mayConflict) throws ContactDBException;
    
    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;
    
    
    /**
     * Erlaubt das Neueinlesen der Relations ( k�nnen von anderen Usern ge�ndert sein)
     */
    public void invalidateRelations();
    
    /**
     * Ist der Termin inzwischen in der DB gel�scht worden?
     */
    public boolean stillExists()  throws ContactDBException;;
    
    //
    // Sammelattribute
    //
    /** r/o-Sammlung der Kalendarien, in denen der Termin liegt */
    public Collection getCalendars() throws ContactDBException;
    /**
     * Einbettungsattribute des Termins in ein Kalendarium<br>
     * Das Abholen einer Relation impliziert ein commit().
     */
    public AppointmentCalendarRelation getRelation(Calendar calendar) throws ContactDBException;
    /** bettet den Termin in ein Kalendarium ein */
    public AppointmentCalendarRelation add(Calendar calendar) throws ContactDBException;
    /** entfernt den Termin aus einem Kalendarium */
    public void remove(Calendar calendar) throws ContactDBException;

    /** r/o-Sammlung der Adressen, die mit dem Termin assoziiert sind */
    public Collection getAddresses() throws ContactDBException;
    /**
     * Assoziationsattribute zwischen Termin und Adresse<br>
     * Das Abholen einer Relation impliziert ein commit().
     */
    public AppointmentAddressRelation getRelation(Address address) throws ContactDBException;
    /** assoziiert den Termin mit einer Adresse */
    public AppointmentAddressRelation add(Address address) throws ContactDBException;
    /** hebt die Assoziation des Termins mit einer Adresse auf */
    public void remove(Address address) throws ContactDBException;
    
    public void setTemporary(boolean temporary);
    public boolean isTemporary();    
    
    public Reminder getReminder() throws ContactDBException;
    public void setReminder(Reminder reminder) throws ContactDBException;
    public void removeReminder() throws ContactDBException;
    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: �berschrift, Beschreibung, Ort, VCAL-Dauer */
    public final static String KEY_SUBJECT = "subject";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_LOCATION = "location";
    public final static String KEY_DURATION = "duration";

    /** Ereigniskategorien: Besprechung, ausw�rts, Urlaub, Geburtstag, Anruf, Krankheit */
    public final static int CATEGORY_MEETING = 1;
    public final static int CATEGORY_EXTERN = 2;
    public final static int CATEGORY_VACATION = 3;
    public final static int CATEGORY_BIRTHDAY = 4;
    public final static int CATEGORY_CALL = 5;
    public final static int CATEGORY_SICK = 6;
    public final static int CATEGORY_TODO = 7;
    
    
    /** Priorit�ten: hoch mittel niedrig */
    public final static int PRIORITY_HIGH = 3;
    public final static int PRIORITY_MEDIUM = 2;
    public final static int PRIORITY_LOW = 1;
    
    /** ID f�r neue nichtgespeicherte Termine */
    public int APPOINTMENT_NEW = 0;
}