/*
 * $Id: PersistentEntity.java,v 1.2 2006/06/06 14:12:11 nils Exp $
 * 
 * Created on 19.05.2004
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright interest in the program 'tarent-contact' (which makes passes at
 * compilers) written by Michael Klink.
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.persistence;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;


/**
 * Diese Klasse stellt die Grundlage f�r alle Objekte, die Datanbank-Eintr�ge darstellen,
 * dar.
 * 
 * @author mikel
 */
public interface PersistentEntity {
    //
    // einfache Attribute
    //
    /** ID der Entit�t */
    public int getId();
    
    //
    // Methoden
    //
    /** l�scht die Entit�t */
    public void delete() throws ContactDBException;
    /** speichert die Entit�t */
    public void commit() throws ContactDBException;
    /** wieder sauber */
    public void rollback() throws ContactDBException;
    /**	Methode zum Bev�lkern eines neuen Objekts von der Serverresponse. */
    public void populate(ResponseData data) throws ContactDBException;
    /**	Status der Entit�t holen */
    public int getState();
    //
    // Konstanten
    //
    /** ID f�r ung�ltige / ungespeicherte / gel�schte Entit�ten */
    public final static int ID_NOT_PERSISTENT = 0;
    
    
    /**	Die Entit�t wurde neu angelegt und noch nicht in der DB gespeichert */
    public final static int STATE_NEW = 0;
    /**	Die Entit�t wurde ge�ndert, die �nderungen noch nicht gespeichert */
    public final static int STATE_MODIFIED = 1;
    /**	Die Entit�t wurde in der DB gel�scht */
    public final static int STATE_DELETED = 2;
    /**	Die Entit�t stimmt lokal mit der DB Version �berein */
    public final static int STATE_COMMITTED = 3;
    /**	Dieser Statis wird gesetzt, wenn eine Operation mit dem Objekt fehlgeschlagen ist */
    public final static int STATE_INVALID = 4;
    
}
