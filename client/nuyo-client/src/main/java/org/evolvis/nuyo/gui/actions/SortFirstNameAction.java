package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SortFirstNameAction extends AbstractGUIAction {

    private static final long serialVersionUID = -6991683325724125446L;
    private static final TarentLogger log = new TarentLogger(SortFirstNameAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SortFirstNameAction not implemented");
    }

}