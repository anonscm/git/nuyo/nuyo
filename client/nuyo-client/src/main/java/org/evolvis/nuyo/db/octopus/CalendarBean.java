package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Resource;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.CalendarId;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.EventId;
import org.evolvis.nuyo.db.filter.AbstractRangedDateSelection.RangedDate;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractPersistentRelation;
import org.evolvis.nuyo.db.persistence.IEntity;



/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.Calendar
 *
 */
abstract public class CalendarBean extends AbstractEntity implements Calendar{

	//
	//	Soapanbindung
	//
	
	//
	//	Instanzmerkmale
	//
	/** Typ des Kalenders (Pers�nlich, Gruppe, etc.) */
    protected Integer _type;
    /**	Der Besitzer des Kalenders */
    protected User _user;
    /**	Dem Kalender zugeordnete Benutzergruppe */
    protected UserGroup _userGroup;
    /**	Dem Kalender zugeordnete Resource */
    protected Resource _resource;
    /**	Dem Kalender zugeordnete Benutzer */
    protected AbstractPersistentRelation _users;
    /** Dem Kalender zugeordnete Benutzergruppen */
    protected AbstractPersistentRelation _userGroups;
    /**	Dem Kalender zugeordnete Freigaben */
    protected AbstractPersistentRelation _secretaries;
    /**	Hiermit werden eine oder mehrere Instanzen dieses Typs bezogen */
    static protected AbstractEntityFetcher _fetcher;
    
    
    /**	Termine des Kalenders. @param start-end Start- und Enddatum, @param filters Filter */
    public Collection getAppointments(Date start, Date end, Collection filters) throws ContactDBException {
        ISelection s = Selection.getInstance()
        	.add(new CalendarId(_id))
        	.add(new RangedDate(new Long(start.getTime()), new Long(end.getTime())));
       
        return AppointmentImpl.getAppointments(s);
        
    }
    
    /**	Ein Termin des Kalenders. @param id ID*/
    public Appointment getAppointment(Integer id) throws ContactDBException {
        ISelection s = Selection.getInstance()
        	.add(new EventId(id));

        Collection col = AppointmentImpl.getAppointments(s);
        if (col != null)
            return (Appointment) col.iterator().next();
        
        return null;
    }

    /**	Alle Benutzer holen */
    public Collection getUsers() throws ContactDBException {return _users.getActualRelations();}
    /**	Benutzer hinzuf�gen. @param newUser - der hinzuzuf�gene Benutzer */
    public void add(User newUser) throws ContactDBException {_users.add((IEntity)newUser);}
    /** Benutzer entfernen. @param user - der zu l�schende User */ 
    public void remove(User user) throws ContactDBException {_users.remove((IEntity)user);}
    /**	Benutzerkalender, dann einen User zur�ckgeben */
    public User getUser() throws ContactDBException {return _user;}
    
    
    /**	Kalenderfreigaben holen */
    public Collection getCalendarSecretaries() throws ContactDBException {return _secretaries.getActualRelations();}
    
    /**	Kalenderfreigaben f�r speziellen User holen */
    public CalendarSecretaryRelation getCalendarSecretaryforUser(int userid) throws ContactDBException {
        Collection secCol = _secretaries.getActualRelations();
        for (Iterator iter = secCol.iterator(); iter.hasNext();){
            CalendarSecretaryRelation calSec = (CalendarSecretaryRelation) iter.next();
            	if (calSec.getSecretaryUserID().intValue() == userid){
            	    return calSec;
            	}
        }
        return null;
    }
    /**	Benutzerkalender, dann einen User zur�ckgeben */
    public void add(CalendarSecretaryRelation newCalSec) throws ContactDBException {_secretaries.add((IEntity)newCalSec);}
    /** Benutzer entfernen. @param user - der zu l�schende User */ 
    public void remove(CalendarSecretaryRelation CalSec) throws ContactDBException {_secretaries.remove((IEntity)CalSec);}
    
	/**	Benutzergruppen holen */
    public Collection getUserGroups() throws ContactDBException {return _userGroups.getActualRelations();}
    /**	Benutzergruppe hinzuf�gen. @param newGroup - hinzuzuf�gende Benutzergruppe */
    public void add(UserGroup newGroup) throws ContactDBException {_userGroups.add((IEntity)newGroup);}
    /**	Benutzergruppe l�schen. @param group - zu l�schende Gruppe */
	public void remove(UserGroup group) throws ContactDBException {_userGroups.remove((IEntity)group);}
    /**	Gruppenkalender, dann Gruppe zur�ckgeben */
    public UserGroup getUserGroup() throws ContactDBException {return null;}


    /**	Typ des Kalenders */
    public int getType() throws ContactDBException {return (_type == null)?0:_type.intValue();}

     
	//
	//	noch nicht implementierte Methoden
	//
	
	/**	Leserecht dieses Kalenders bzgl. des users. @param newUser - Benutzer, dessen Rechte gepr�ft werden sollen */
    public boolean canRead(User newUser) throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }
	/**	Schreibrecht dieses Kalenders bzgl. des users. @param newUser - Benutzer, dessen Rechte gepr�ft werden sollen */
    public boolean canWrite(User newUser) throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }

	/**	Leserecht dieses Kalenders bzgl. der Benutzergruppe. @param group - Gruppe, deren Rechte gepr�ft werden sollen */
	public boolean canRead(UserGroup group) throws ContactDBException {
	    throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
	}
	/**	Schreibrecht dieses Kalenders bzgl. der Benutzergruppe. @param group - Gruppe, deren Rechte gepr�ft werden sollen */
	public boolean canWrite(UserGroup group) throws ContactDBException {
	    throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
	}
    
    /**	Reminders dieses Kalenders. @param start-end Anfangs - Enddatum, @param filters - Filter */
    public Collection getReminders(Date start, Date end, Collection filters) throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }
    /**	Resourcen des Kalenders */
	public Resource getResource() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
	}	

}
