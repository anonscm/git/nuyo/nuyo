package org.evolvis.nuyo.db.octopus;

import java.util.Collection;

import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.IAddress;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;


/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.IAddress
 *
 */
abstract public class AddressBean2 extends AbstractEntity implements IAddress {

    protected String _label;
    
    static protected AbstractEntityFetcher _fetcher;
    
    public Collection getSubCategories() {return null;}
    public void add(SubCategory category) {}
    public void addCategory(Category category) {}

   
    public User getAssociatedUser() {return null;}
    public void associateUser(User user) {}

    
    public Collection getCategories() {return null;}

    
    public Collection getHistory() {return null;}
    
    
    public String getShortAddressLabel() {return null;}
    
}
