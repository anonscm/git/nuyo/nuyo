/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Listener;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObject;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;


/**
 * @author niko
 *
 */
public interface DataContainerListenerAction extends DataContainerObject
{
  public DataContainerListenerAction cloneAction();
  public void init();
  public void dispose();
  
  public boolean doAction();
  
  public boolean addParameter(String key, String value);
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor();

  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();

  public void setListener(DataContainerListener listener);
  public DataContainerListener getListener();
  
  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);      
  public void fireTarentGUIEvent(TarentGUIEvent e);  
}
