/* $Id: AddressListCommand.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 09.10.2003
 */
package org.evolvis.nuyo.messages;

import java.util.EventObject;

/**
 * Dies ist die Basisklasse f�r Kommandoklassen, die die Adresslistenansicht
 * steuern.
 *  
 * @author mikel
 */
public class AddressListCommand extends EventObject implements CommandEvent {
    /**
     * @param source Quelle des Kommandos
     */
    public AddressListCommand(Object source) {
        super(source);
    }
}
