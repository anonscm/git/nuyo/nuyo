/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.HashMap;

/**
 * @author niko
 */
public class ProcentualLayout extends FlowLayout
{
  private HashMap    m_oProcentValues;
  private int        m_iGapX;
  private HashMap    m_oSpecialGapXValues;
  private Dimension  m_oPreferredSize;
  private boolean    m_oPreferredSizeValid;
  private Dimension  m_oMinimumSize;
  private boolean    m_oMinimumSizeValid;

  public ProcentualLayout() 
  {
    super();
    m_iGapX = 0;
    m_oPreferredSizeValid = false;
    m_oMinimumSizeValid = false;
    m_oProcentValues = new HashMap();
    m_oSpecialGapXValues = new HashMap();
  }

 
  public void setProcentualWidth(Component comp, int procentualwidth)
  {
    m_oProcentValues.put(comp, new Integer(procentualwidth));
    m_oPreferredSizeValid = false;    
  }

  public void setGapX(int gapwidth)
  {
    m_iGapX = gapwidth;
    m_oPreferredSizeValid = false;
  }

  public void setSpecialGapX(Component comp, int gap)
  {    
    m_oSpecialGapXValues.put(comp, new Integer(gap));
    m_oPreferredSizeValid = false;    
  }

  public int getSpecialGapX(Component comp)
  {
    if (m_oSpecialGapXValues.get(comp) == null) return(0);
    return(((Integer)(m_oSpecialGapXValues.get(comp))).intValue());
  }

  public Dimension preferredLayoutSize(Container parent) 
  {
    if (m_oPreferredSizeValid) return(m_oPreferredSize);
    
    int containersizex = 0;
    int maxcontainersizey = 0;
    int gap = 0;

    for(int i = 0; i<(parent.getComponentCount()); i++)
    {
      Component component = parent.getComponent(i);
      Dimension prefsize = component.getPreferredSize();
      gap = m_iGapX + getSpecialGapX(component);
      containersizex += (prefsize.width + gap);
      if (prefsize.height > maxcontainersizey) maxcontainersizey = prefsize.height;
    }
    
    containersizex -= gap;
    m_oPreferredSize = new Dimension(containersizex, maxcontainersizey);
    m_oPreferredSizeValid = true;
    return m_oPreferredSize;
  }

  
  public Dimension minimumLayoutSize(Container parent) 
  {
    if (m_oMinimumSizeValid) return(m_oMinimumSize);
     
    int containersizex = 0;
    int maxcontainersizey = 0;

    for(int i = 0; i<(parent.getComponentCount()); i++)
    {
      Component component = parent.getComponent(i);
      Dimension prefsize = component.getPreferredSize();
      containersizex += prefsize.width;
      if (prefsize.height > maxcontainersizey) maxcontainersizey = prefsize.height;
    }      
    m_oMinimumSize = new Dimension(containersizex, maxcontainersizey);
    m_oMinimumSizeValid = true;
    return m_oMinimumSize;
  }

  public void addLayoutComponent(String name, Component comp)
  {
    super.addLayoutComponent(name, comp);
    m_oProcentValues.put(comp, new Integer(-1));
    m_oPreferredSizeValid = false;    
    m_oMinimumSizeValid = false;
  }

  public void removeLayoutComponent(Component comp) 
  {
    super.removeLayoutComponent(comp);
    m_oProcentValues.remove(comp);
    m_oPreferredSizeValid = false;    
    m_oMinimumSizeValid = false;
  }

	public void layoutContainer(Container parent) 
  {
    int containersizex = parent.getWidth();
    int containersizey = parent.getHeight();
    int posx = 0;    
    int lostspace = 0;

    containersizex -= ((parent.getComponentCount() - 1) * m_iGapX);

    for(int i = 0; i<(parent.getComponentCount()); i++)
    {
      Component component = parent.getComponent(i);
      Dimension prefsize = component.getPreferredSize();
      int componentsizey = prefsize.height;            
      int componentsizex = 0;
            
      if (m_oProcentValues.get(component) != null)
      {
        int proc = ((Integer)(m_oProcentValues.get(component))).intValue();
        int newsizex = (int)(((((double)containersizex) / 100.0) * ((double)proc)));
        
        newsizex -= (lostspace);
        lostspace = 0;
        
        if (newsizex > component.getMinimumSize().width) 
        {
          componentsizex = newsizex - getSpecialGapX(component);
        }
        else 
        {
          componentsizex = component.getMinimumSize().width;
          lostspace += (component.getMinimumSize().width - newsizex);
        }
      }

      int gap = m_iGapX + getSpecialGapX(component);        
      component.setBounds(posx, 0, componentsizex, componentsizey);
      posx += (componentsizex + gap);        
    }    
	}
}
