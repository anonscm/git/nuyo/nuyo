/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextViewField;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 */
public class AenderungsdatumField extends GenericTextViewField
{
  protected final static DateFormat myDateFormatter = SimpleDateFormat.getDateInstance();
  
  public AenderungsdatumField()
  {
    super("AENDERUNGSDATUM", AddressKeys.AENDERUNGSDATUM, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Aenderungsdatum_ToolTip", "GUI_MainFrameNewStyle_Standard_Aenderungsdatum", 0);
  }
  
  public void setData(PluginData data)
  {
    Object value = data.get(m_AdressKey);
    if (value != null) m_oTextfield.setText(myDateFormatter.format(value)); 
    else                    m_oTextfield.setText("");
  }
}
