package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SortInstitutionAction extends AbstractGUIAction {

    private static final long serialVersionUID = -559559299538882390L;
    private static final TarentLogger log = new TarentLogger(SortInstitutionAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SortInstitutionAction not implemented");
    }

}