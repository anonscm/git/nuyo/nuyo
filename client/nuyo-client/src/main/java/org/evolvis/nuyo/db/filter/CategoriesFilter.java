/*
 * $Id: CategoriesFilter.java,v 1.3 2007/02/16 11:07:34 nils Exp $
 * 
 * Created on 25.05.2004
 */
package org.evolvis.nuyo.db.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;


/**
 * Diese Klasse stellt einen Filter dar, der eine Sammlung von Kategorien beschreibt.
 * 
 * @author mikel
 */
public class CategoriesFilter {
    //
    // Konstruktoren
    //
    /**
     * Dieser Konstruktor erzeugt einen leeren Kategorienfilter.
     */
    public CategoriesFilter() {
        this(null);
    }
    
    /**
     * Dieser Konstruktor erzeugt einen Kategorienfilter, der mit einer Sammlung von Kategorien initialisiert wird. 
     * 
     * @param categories Kategorien, die �ber ein {@link Category}-Objekt oder ein den Schl�ssel darstellendes {@link String}-Objekt repr�sentiert werden.
     * @throws IllegalArgumentException bei unerlaubten Objekten in der �bergebenen Sammlung 
     */
    public CategoriesFilter(Collection categories) {
        try {
            if (categories != null) {
                Iterator itCategories = categories.iterator();
                while (itCategories.hasNext()) {
                    Object next = itCategories.next();
                    if (next instanceof String)
                        add((String)next);
                    else if (next instanceof Category)
                        add((Category)next);
                    else
                        throw new IllegalArgumentException("Kategorien werden nur �ber ein Category-Objekt oder ein den Schl�ssel darstellendes String-Objekt dargestellt");
                }
            }
        } catch (ContactDBException e1) {
            // Fehler bei Hinzuf�gen einer Kategorie
            // DO NOTING HERE
        }
    }
    
    //
    // �ffentliche Methoden
    //
    /**
     * Diese Methode f�gt eine Kategorie �ber ihre ID hinzu.
     */
    public boolean add(String id) {
        return categoryIds.add(id);
    }

    /**
     * Diese Methode f�gt eine Kategorie �ber ein Kategorienobjekt hinzu.
     */
    public boolean add(Category category) throws ContactDBException {
        return categoryIds.add(category.getIdAsString());
    }

    /**
     * Diese Methode liefert eine r/o-Sammlung der dargestellten Kategorien.
     */
    public Set getCategoryIds() {
        return Collections.unmodifiableSet(categoryIds);
    }

    //
    // private Membervariablen
    //
    /** Menge der dargestellten Kategorien-IDs */
    private Set categoryIds = new HashSet();
}
