package org.evolvis.nuyo.plugins.calendar;

public interface CalendarSelectionListener {
    
    public void setSelected(boolean isSelected);
}
