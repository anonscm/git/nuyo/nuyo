package org.evolvis.nuyo.gui.actions;

import org.evolvis.nuyo.controller.ApplicationModel;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.Binding;

/**
 * A convenience implementation of the {@link AbstractBindingRestrictedAction}
 * which disables the action in case the address list is empty.
 * 
 * <p>An action inheriting from this class can be sure that the action
 * will be disabled when the address list is empty.</p>
 * 
 * @author Robert Schuster
 *
 */
public abstract class AbstractContactDependentAction extends AbstractBindingRestrictedAction {
	
	protected AbstractContactDependentAction()
	{
		// Nothing to be done.
	}

	protected final Binding[] initBindings() {
		// A binding that listens on changes to the currently selected address
		// in order to check the edit right. The result of that check combined
		// with whether the action is in the current context determines the
		// action's enabled state.
		return new Binding[] { new AbstractReadOnlyBinding(
				ApplicationModel.ADDRESS_LIST_KEY + ".size") {

			public void setViewData(Object arg) {
				if (arg == null)
				  return;
				
				setBindingEnabled(((Integer) arg).intValue() > 0);

				updateActionEnabledState();
			}

		} };

	}

}
