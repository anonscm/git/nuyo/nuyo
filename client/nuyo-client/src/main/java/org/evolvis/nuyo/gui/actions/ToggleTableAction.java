package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.Binding;


public class ToggleTableAction extends AbstractBindingRestrictedAction {

    private static final long	serialVersionUID	= -1231240498596706020L;
	private static final TarentLogger log = new TarentLogger(ToggleTableAction.class);
	
	private boolean isVisible = true;
	private GUIListener guiListener;

	
	public void actionPerformed(ActionEvent e) {
		if(guiListener != null) {
            isVisible = !isVisible; 
			guiListener.userRequestShowTable(isVisible);
            // Updates all affected checkboxes.
            setSelected(isVisible);
		} else {
			log.warning("ToggleTableAction failed","gui listener not found");
		}
	}
	

	protected void init() {
		log.finest("[action]: init toggle table action");
		guiListener = ApplicationServices.getInstance().getActionManager();
		isVisible = Boolean.valueOf(guiListener.getPreferences(StartUpConstants.PREF_PANELS_NODE_NAME).get(StartUpConstants.PREF_TABLE_IS_VISIBLE_KEY, "true")).booleanValue();
        // Puts checkboxes in the correct state initially.
        setSelected(isVisible);
	}
    
    @Override
    protected Binding[] initBindings() {
        //activate synchronization
        setBindingEnabled(true);
        // A binding that listens on manual changes to the address table visibility
        // in order to synchronize with menu check boxes.
        return new Binding[] { new AbstractReadOnlyBinding(
                ApplicationModel.ADDRESS_TABLE_VISIBILITY_KEY) {

            public void setViewData(Object arg) {
                if (arg == null)
                  return;
                //manual changes occured
                if(isVisible != ((Boolean) arg).booleanValue()) {
                        isVisible = !isVisible;
                        // Updates all affected checkboxes.
                        setSelected(isVisible);
                }
            }

        } };
    }
}
