package org.evolvis.nuyo.gui.calendar;
/*
 * Created on 21.07.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 
package de.tarent.contact.gui.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.tarent.controls.tarentPanel;
import de.tarent.controls.tarentWidgetButton;
import de.tarent.controls.tarentWidgetComponentListBox;
import de.tarent.controls.tarentWidgetDateSpinner;
import de.tarent.controls.tarentWidgetFactory;
import de.tarent.controls.tarentWidgetHorizontalPanel;
import de.tarent.controls.tarentWidgetIconComboBox;
import de.tarent.controls.tarentWidgetPanel;
import de.tarent.controls.tarentWidgetRadioButton;
import de.tarent.controls.tarentWidgetSeparator;
import de.tarent.controls.tarentWidgetSpacer;
import de.tarent.controls.tarentWidgetTextArea;
import de.tarent.controls.tarentWidgetTextField;
import de.tarent.controls.tarentWidgetIconComboBox.IconComboBoxEntry;

*//**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 *//*
public class TerminEditor extends JPanel
{  
  // Repeat - Type
  public final static Object REPEAT_SINGLE = "REPEAT_SINGLE";
  public final static Object REPEAT_HOURLY = "REPEAT_HOURLY";
  public final static Object REPEAT_DAYLY = "REPEAT_DAYLY";
  public final static Object REPEAT_WEEKLY = "REPEAT_WEEKLY";
  public final static Object REPEAT_YEARLY = "REPEAT_YEARLY";

  // -----------------------------------------------------------------------------

  // Remind-Type
  public final static Object REMIND_ACOUSTICAL = "REMIND_ACOUSTICAL" ;
  public final static Object REMIND_OPTICAL = "REMIND_OPTICAL" ;
  public final static Object REMIND_SMS = "REMIND_SMS" ;
  public final static Object REMIND_EMAIL = "REMIND_EMAIL" ;

  // -----------------------------------------------------------------------------
  
  //Remind time
  public final static Object REMINDTIME_NEVER = "REMINDTIME_NEVER" ;
  public final static Object REMINDTIME_15MIN = "REMINDTIME_15MIN" ;
  public final static Object REMINDTIME_30MIN = "REMINDTIME_30MIN" ;
  public final static Object REMINDTIME_1HOUR = "REMINDTIME_1HOUR" ;
  public final static Object REMINDTIME_FREE = "REMINDTIME_FREE" ;
  
  // -------------------------------------------------------------------------

  //Ressourcen (unique Keys)
  public final static Object RESSOURCE_OVERHEAD_BIG = "RESSOURCE_OVERHEAD_BIG";
  public final static Object RESSOURCE_OVERHEAD_SMALL = "RESSOURCE_OVERHEAD_SMALL";
  public final static Object RESSOURCE_CAR_TRANSPORTER = "RESSOURCE_CAR_TRANSPORTER";
  public final static Object RESSOURCE_CAR_ROLLSROYCE = "RESSOURCE_CAR_ROLLSROYCE";
  public final static Object RESSOURCE_CAR_CABRIOLET = "RESSOURCE_CAR_CABRIOLET";
  public final static Object RESSOURCE_CAR_MINI = "RESSOURCE_CAR_MINI";
  public final static Object RESSOURCE_ROOM_SMALL = "RESSOURCE_ROOM_SMALL";
  public final static Object RESSOURCE_ROOM_BIG = "RESSOURCE_ROOM_BIG";
  public final static Object RESSOURCE_LAPTOP_FAST = "RESSOURCE_LAPTOP_FAST";
  public final static Object RESSOURCE_LAPTOP_SLOW = "RESSOURCE_LAPTOP_SLOW";
  public final static Object RESSOURCE_BEAMER_VGA = "RESSOURCE_BEAMER_VGA";
  public final static Object RESSOURCE_BEAMER_XGA = "RESSOURCE_BEAMER_XVGA";

  // -------------------------------------------------------------------------
  
  //Ressourcen-Typen
  public final static Object RESSOURCETYPE_ALL = "RESSOURCETYPE_ALL";
  public final static Object RESSOURCETYPE_OVERHEAD = "RESSOURCETYPE_OVERHEAD";
  public final static Object RESSOURCETYPE_CAR = "RESSOURCETYPE_CAR";
  public final static Object RESSOURCETYPE_ROOM = "RESSOURCETYPE_ROOM";
  public final static Object RESSOURCETYPE_LAPTOP = "RESSOURCETYPE_LAPTOP";
  public final static Object RESSOURCETYPE_BEAMER = "RESSOURCETYPE_BEAMER";
  
  // -------------------------------------------------------------------------
    
  private tarentWidgetFactory m_oWidgetFactory;
  
  private URL m_oImageFolder;
  private JTabbedPane m_oTabbedPane;
  private JPanel m_oPanelTermin; 
  private JPanel m_oPanelRessourcen; 
  private JPanel m_oPanelRechte; 
  private JPanel m_oPanelAktionen; 
  private JPanel m_oPanelHistorie; 
    
  private ImageIcon m_oIcon_Termin;
  private ImageIcon m_oIcon_Ressourcen; 
  private ImageIcon m_oIcon_Rechte; 
  private ImageIcon m_oIcon_Aktionen; 
  private ImageIcon m_oIcon_Historie; 
  
  private Color m_oBGColor;
  private Color m_oTitleBGColor;
  private Color m_oButtonBGColor;
  private Color m_oButtonFGColor;
  
  private Font m_oStdFont;
  
  private ScheduleEntry m_oScheduleEntry;

  private tarentWidgetTextField m_oTextField_Name;
  private tarentWidgetIconComboBox m_oIconComboBox_Type;
  private tarentWidgetTextField m_oTextField_Ort;  
  private tarentWidgetDateSpinner m_oDateSpinner_StartDate; 
  private tarentWidgetDateSpinner m_oDateSpinner_EndDate; 
  private tarentWidgetTextArea m_oTextArea_Notizen;
  private tarentWidgetComponentListBox m_oCompListBox_BeteiligteAvailable;
  private tarentWidgetComponentListBox m_oCompListBox_BeteiligteUsed;
  private tarentWidgetComponentListBox m_oCompListBox_ResourcesAvailable;
  private tarentWidgetComponentListBox m_oCompListBox_ResourcesUsed;
  private tarentWidgetIconComboBox m_oCompListBox_ResourceTypes;
  private tarentWidgetComponentListBox m_oCompListBox_Historie;  
  private tarentWidgetIconComboBox m_oIconComboBox_RepeatType;
  private tarentWidgetIconComboBox m_oIconComboBox_ActionType;
  private tarentWidgetIconComboBox m_oIconComboBox_ActionTime;
  private tarentWidgetDateSpinner m_oDateSpinner_ActionTimeFree;    
  private tarentWidgetComponentListBox m_oCompListBox_Rechte;
  
  private DialogControl m_oDialogControl = null;
  private ScheduleTypes m_oScheduleTypes;
  
  private List m_oAvailableRessources = new ArrayList();
  
  
  public TerminEditor(ScheduleData scheduledata, ScheduleEntry entry)
  {
    m_oScheduleEntry = entry;
    m_oScheduleTypes = scheduledata.getScheduleTypes();
    
    m_oWidgetFactory = new tarentWidgetFactory();
    m_oImageFolder = scheduledata.getImageFolder();
    m_oBGColor = m_oWidgetFactory.getLookAndFeelLabel().getBackgroundColor();
    m_oTitleBGColor = new Color(0xff, 0xff, 0xff);
    m_oButtonBGColor = new Color(0x46, 0x64, 0x9e);
    m_oButtonFGColor = new Color(0xff, 0xff, 0xff);
    m_oStdFont = new Font(null, Font.PLAIN, 12);
    
    this.setLayout(new BorderLayout());
    
    m_oPanelTermin = new JPanel(new BorderLayout());
    m_oPanelRessourcen = new JPanel(new BorderLayout());
    m_oPanelRechte = new JPanel(new BorderLayout());
    m_oPanelAktionen = new JPanel(new BorderLayout());
    m_oPanelHistorie = new JPanel(new BorderLayout());

    m_oPanelTermin.setBackground(m_oBGColor);
    m_oPanelRessourcen.setBackground(m_oBGColor);
    m_oPanelRechte.setBackground(m_oBGColor);
    m_oPanelAktionen.setBackground(m_oBGColor);
    m_oPanelHistorie.setBackground(m_oBGColor);

    m_oPanelTermin.setOpaque(true);
    m_oPanelRessourcen.setOpaque(true);
    m_oPanelRechte.setOpaque(true);
    m_oPanelAktionen.setOpaque(true);
    m_oPanelHistorie.setOpaque(true);

    m_oIcon_Termin = readIcon("tab_termin.gif");
    m_oIcon_Ressourcen = readIcon("tab_ressourcen.gif");
    m_oIcon_Rechte = readIcon("tab_rechte.gif");
    m_oIcon_Aktionen = readIcon("tab_aktionen.gif");
    m_oIcon_Historie = readIcon("tab_historie.gif");

    fillRessourceList();
    
    m_oPanelTermin.add(createTerminPanel(), BorderLayout.CENTER);
    m_oPanelRessourcen.add(createRessourcenPanel(), BorderLayout.CENTER);
    m_oPanelRechte.add(createRechtePanel(), BorderLayout.CENTER);    
    m_oPanelAktionen.add(createActionPanel(), BorderLayout.CENTER);
    m_oPanelHistorie.add(createHistoriePanel(), BorderLayout.CENTER);

    Border panelborder = new EmptyBorder(2,10,2,10);
    m_oPanelTermin.setBorder(panelborder);
    m_oPanelRessourcen.setBorder(panelborder);
    m_oPanelRechte.setBorder(panelborder);    
    m_oPanelAktionen.setBorder(panelborder);
    m_oPanelHistorie.setBorder(panelborder);
    
    
    m_oTabbedPane = new JTabbedPane();  
    m_oTabbedPane.setBackground(m_oBGColor);
    m_oTabbedPane.setFont(m_oStdFont);

    m_oTabbedPane.addTab("Termin", m_oIcon_Termin, m_oPanelTermin, "xxx");
    m_oTabbedPane.addTab("Ressourcen", m_oIcon_Ressourcen, m_oPanelRessourcen, "xxx");
    m_oTabbedPane.addTab("Rechte", m_oIcon_Rechte, m_oPanelRechte, "xxx");
    m_oTabbedPane.addTab("Aktionen", m_oIcon_Aktionen, m_oPanelAktionen, "xxx");
    m_oTabbedPane.addTab("Historie", m_oIcon_Historie, m_oPanelHistorie, "xxx");
    
    this.add(m_oTabbedPane, BorderLayout.CENTER);
    this.add(createButtonPanel(), BorderLayout.SOUTH);    
    
    updateDateEntries();
    fillBeteiligteList();
    fillHistoryList();
    fillSystemUserList();
    //setScheduleType(m_oScheduleEntry.getScheduleType());    
  }
    
  
  public void initGUI()
  {
    Dimension size = m_oCompListBox_BeteiligteAvailable.getSize();        
    m_oCompListBox_BeteiligteAvailable.setMinimumSize(size);    
    m_oCompListBox_BeteiligteAvailable.setMaximumSize(size);    
    m_oCompListBox_BeteiligteAvailable.setPreferredSize(size);    
    m_oCompListBox_BeteiligteUsed.setMinimumSize(size);    
    m_oCompListBox_BeteiligteUsed.setMaximumSize(size);    
    m_oCompListBox_BeteiligteUsed.setPreferredSize(size);    
  }
  
  
  public void setDialogControl(DialogControl dc)
  {
    m_oDialogControl = dc;
  }
  
  private JPanel createButtonPanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(panel);

    tarentWidgetButton okbutton = new tarentWidgetButton("Ok");
    okbutton.addActionListener(new Button_OK_clicked());
    
    tarentWidgetButton cancelbutton = new tarentWidgetButton("Abbrechen"); 
    cancelbutton.addActionListener(new Button_CANCEL_clicked());

    JPanel buttonpanel = new JPanel(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(buttonpanel);
    buttonpanel.add(okbutton, BorderLayout.WEST);
    buttonpanel.add(Box.createHorizontalStrut(5), BorderLayout.CENTER);
    buttonpanel.add(cancelbutton, BorderLayout.EAST);
        
    panel.add(Box.createVerticalStrut(5), BorderLayout.NORTH);
    panel.add(buttonpanel, BorderLayout.EAST);
    panel.add(Box.createVerticalStrut(5), BorderLayout.SOUTH);
    
    return(panel);    
  }
  
  
  public void doOK()
  {
    if (m_oDialogControl != null) m_oDialogControl.closeDialog();
  }
  
  public void doCancel()
  {
    if (m_oDialogControl != null) m_oDialogControl.closeDialog();
  }
  
  private class Button_OK_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent arg0)
    {
      doOK();
    }
  }
  
  private class Button_CANCEL_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent arg0)
    {
      doCancel();
    }
  }
  
  private JPanel createTerminPanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(panel);
    
    tarentPanel tarentpanel = new tarentPanel(); 

    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    m_oTextField_Name = new tarentWidgetTextField();
    m_oTextField_Name.getDocument().addDocumentListener(new NameOrtDocumentListener());
    tarentpanel.addWidget("Name:", m_oTextField_Name); 
    
    m_oIconComboBox_Type = new tarentWidgetIconComboBox();
    m_oIconComboBox_Type.setSelectedColors(new Color(0xff, 0xff, 0xff), new Color(0x46, 0x64, 0x9e));
    m_oIconComboBox_Type.setUnselectedColors(new Color(0x00, 0x00, 0x00), new Color(0xff, 0xff, 0xff));    
    for (int i=0; i<(m_oScheduleTypes.getNumberOfTypes()); i++)
    {  
      ScheduleType oScheduleType = m_oScheduleTypes.getScheduleType(i);
      m_oIconComboBox_Type.addIconComboBoxEntry(new IconComboBoxEntry(oScheduleType.getIcon(), oScheduleType.getName()));
    }
    tarentpanel.addWidget("Typ:", m_oIconComboBox_Type);
    m_oIconComboBox_Type.addItemListener(new TypeItemListener());
         
    m_oTextField_Ort = new tarentWidgetTextField();
    m_oTextField_Ort.getDocument().addDocumentListener(new NameOrtDocumentListener());
    tarentpanel.addWidget("Ort:", m_oTextField_Ort); 

    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    tarentpanel.addWidget(new tarentWidgetSeparator()); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 

    m_oDateSpinner_StartDate = new tarentWidgetDateSpinner();
    m_oDateSpinner_EndDate = new tarentWidgetDateSpinner();    
    tarentpanel.addWidget("Start:", m_oDateSpinner_StartDate); 
    tarentpanel.addWidget("End:", m_oDateSpinner_EndDate); 
    updateDateEntries();
    m_oDateSpinner_StartDate.addChangeListener(new StartDateChangeListener());
    m_oDateSpinner_EndDate.addChangeListener(new EndDateChangeListener());
    
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    tarentpanel.addWidget(new tarentWidgetSeparator()); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 

    
    m_oIconComboBox_RepeatType = new tarentWidgetIconComboBox();
    m_oIconComboBox_RepeatType.setSelectedColors(new Color(0xff, 0xff, 0xff), new Color(0x46, 0x64, 0x9e));
    m_oIconComboBox_RepeatType.setUnselectedColors(new Color(0x00, 0x00, 0x00), new Color(0xff, 0xff, 0xff));    

    m_oIconComboBox_RepeatType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("repeat_dayly.gif"), "nie", REPEAT_SINGLE));
    m_oIconComboBox_RepeatType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("repeat_dayly.gif"), "st�ndlich", REPEAT_HOURLY));
    m_oIconComboBox_RepeatType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("repeat_dayly.gif"), "t�glich", REPEAT_DAYLY));
    m_oIconComboBox_RepeatType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("repeat_dayly.gif"), "w�chentlich", REPEAT_WEEKLY));
    m_oIconComboBox_RepeatType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("repeat_dayly.gif"), "j�hrlich", REPEAT_YEARLY));
    
    tarentpanel.addWidget("Wiederholung:", m_oIconComboBox_RepeatType);
    m_oIconComboBox_RepeatType.addItemListener(new RepeatTypeItemListener());
    
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    tarentpanel.addWidget(new tarentWidgetSeparator()); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    
    m_oTextArea_Notizen = new tarentWidgetTextArea(5, 40);
    m_oTextArea_Notizen.setLineWrap(true);
    m_oTextArea_Notizen.setWrapStyleWord(true);
    tarentpanel.addWidget("Notizen:", m_oTextArea_Notizen); 

    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    tarentpanel.addWidget(new tarentWidgetSeparator()); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 

    tarentWidgetPanel panel_buttons_beteiligte = new tarentWidgetPanel(new BorderLayout());
    tarentWidgetButton button_right = new tarentWidgetButton(readIcon("right.gif"));
    button_right.addActionListener(new button_right_clicked());
    tarentWidgetButton button_left = new tarentWidgetButton(readIcon("left.gif"));
    button_left.addActionListener(new button_left_clicked());
    panel_buttons_beteiligte.add(button_right, BorderLayout.NORTH);
    panel_buttons_beteiligte.add(button_left, BorderLayout.SOUTH);
    
    tarentWidgetHorizontalPanel horizpanel_beteiligte = new tarentWidgetHorizontalPanel();         
    
    m_oCompListBox_BeteiligteAvailable = new tarentWidgetComponentListBox();        
    m_oCompListBox_BeteiligteUsed = new tarentWidgetComponentListBox();    
    
    m_oCompListBox_BeteiligteAvailable.getJList().setVisibleRowCount(4);
    m_oCompListBox_BeteiligteUsed.getJList().setVisibleRowCount(4);
    
    
    horizpanel_beteiligte.addWidget(m_oCompListBox_BeteiligteAvailable, 45);
    horizpanel_beteiligte.addWidget(panel_buttons_beteiligte, 10);
    horizpanel_beteiligte.addWidget(m_oCompListBox_BeteiligteUsed, 45);
    
    tarentpanel.addWidget("Beteiligte:", horizpanel_beteiligte); 
    
    panel.add(tarentpanel, BorderLayout.NORTH);
    return(panel);
  }
  
  
  private class button_right_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      JPanel panel = (JPanel)(m_oCompListBox_BeteiligteAvailable.getJList().getSelectedValue());
      if (panel != null)
      {
        m_oCompListBox_BeteiligteAvailable.removeData(panel);
        m_oCompListBox_BeteiligteUsed.addData(panel);
      }
    }    
  }
  
  private class button_left_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      JPanel panel = (JPanel)(m_oCompListBox_BeteiligteUsed.getJList().getSelectedValue());
      if (panel != null)
      {
        m_oCompListBox_BeteiligteUsed.removeData(panel);
        m_oCompListBox_BeteiligteAvailable.addData(panel);
      }
    }    
  }
  
  
  public void setScheduleType(ScheduleType scheduletype)
  {
    m_oIconComboBox_Type.setSelectedItem((String)scheduletype.getName());
  }

  public ScheduleType getScheduleType()
  {
    IconComboBoxEntry ice = (IconComboBoxEntry)(m_oIconComboBox_Type.getSelectedItem());
    String name = ice.getText();    
    return m_oScheduleTypes.getScheduleType(name);
  }
  
  
  private class TypeItemListener implements ItemListener
  {
    public void itemStateChanged(ItemEvent arg0)
    {
      ScheduleType type = getScheduleType();
      //m_oScheduleEntry.setScheduleType(type);
    }    
  }
  
  private class NameOrtDocumentListener implements DocumentListener
  {
    public void changedUpdate(DocumentEvent arg0)
    {
      updateScheduleEntry();
    }

    public void insertUpdate(DocumentEvent arg0)
    {
      updateScheduleEntry();
    }

    public void removeUpdate(DocumentEvent arg0)
    {
      updateScheduleEntry();
    }    
  }
  
  
  private void updateScheduleEntry()
  {
    String name = m_oTextField_Name.getText();
    String ort = m_oTextField_Ort.getText();
    List SEPS = SEPRegistry.getInstance().getSEPSforAppointmentID(m_oScheduleEntry.getAppointment().getId());
    if (SEPS != null){
        Iterator myiter = SEPS.iterator();
        	while (myiter.hasNext()){
        	    ((ScheduleEntry)myiter.next()).setScheduleLabelText(name,ort);
        	}
    }
  }
  
  
  private class StartDateChangeListener implements ChangeListener
  {
    public void stateChanged(ChangeEvent arg0)
    {
      ScheduleDate startdate = new ScheduleDate((Date)(m_oDateSpinner_StartDate.getData()));
      ScheduleDate enddate = new ScheduleDate((Date)(m_oDateSpinner_EndDate.getData()));
      
      if (startdate.after(enddate))
      {
        enddate.setDate(startdate.getDateWithAddedSeconds(1).getDate());
        m_oDateSpinner_EndDate.setData(enddate.getDate());
      }
      
      // Update all SEPS
      if (SE)
      m_oScheduleEntry.setStartDate(new ScheduleDate(startdate));
      m_oScheduleEntry.setEndDate(new ScheduleDate(enddate));
      m_oScheduleEntry.updateSizeAndPosition();
    }
  }
  
  
  private class EndDateChangeListener implements ChangeListener
  {
    public void stateChanged(ChangeEvent arg0)
    {
      ScheduleDate startdate = new ScheduleDate((Date)(m_oDateSpinner_StartDate.getData()));
      ScheduleDate enddate = new ScheduleDate((Date)(m_oDateSpinner_EndDate.getData()));
      
      if (startdate.after(enddate))
      {
        startdate.setDate(enddate.getDateWithAddedSeconds(-1).getDate());
        m_oDateSpinner_StartDate.setData(startdate.getDate());
      }
      
      m_oScheduleEntry.setStartDate(new ScheduleDate(startdate));
      m_oScheduleEntry.setEndDate(new ScheduleDate(enddate));
      m_oScheduleEntry.updateSizeAndPosition();
    }
  }
  
  
  private void updateDateEntries()
  {
    m_oDateSpinner_StartDate.setData(m_oScheduleEntry.getStartDate().getDate());
    m_oDateSpinner_EndDate.setData(m_oScheduleEntry.getEndDate().getDate());        
  }

  // --------------------------------------------------------------------------------
  
  private JPanel createBeteiligterItemPanel(String iconfile, String name)
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    
    JLabel iconlabel = new JLabel(readIcon(iconfile));
    iconlabel.setBorder(new EmptyBorder(2,2,2,2)); 
    JLabel namelabel = new JLabel(name); 
    namelabel.setFont(m_oStdFont);
    
    panel.add(iconlabel, BorderLayout.WEST);    
    panel.add(namelabel, BorderLayout.CENTER);    
        
    return(panel);
  }

  // --------------------------------------------------------------------------------  
  
  private JPanel createRessourcenPanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(panel);
    
    tarentPanel tarentpanel = new tarentPanel(); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 

    
    m_oCompListBox_ResourceTypes = new tarentWidgetIconComboBox();
    m_oCompListBox_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("ressource_all.gif"), "Alle", RESSOURCETYPE_ALL));
    m_oCompListBox_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("ressource_overhead.gif"), "Overheadprojektoren", RESSOURCETYPE_OVERHEAD));
    m_oCompListBox_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("ressource_car.gif"), "Dienstwagen", RESSOURCETYPE_CAR));
    m_oCompListBox_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("ressource_room.gif"), "R�ume", RESSOURCETYPE_ROOM));
    m_oCompListBox_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("ressource_laptop.gif"), "Laptops", RESSOURCETYPE_LAPTOP));
    m_oCompListBox_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("ressource_beamer.gif"), "Beamer", RESSOURCETYPE_BEAMER));
    tarentpanel.addWidget("Ressourcetyp:", m_oCompListBox_ResourceTypes); 
    m_oCompListBox_ResourceTypes.addActionListener(new combo_ressourcetype_changed());
    
    m_oCompListBox_ResourcesAvailable = new tarentWidgetComponentListBox();
    m_oCompListBox_ResourcesAvailable.getJList().setVisibleRowCount(6);    
    tarentpanel.addWidget("verf�gbare Ressourcen:", m_oCompListBox_ResourcesAvailable); 
    tarentWidgetHorizontalPanel horizpanel_avail = new tarentWidgetHorizontalPanel();         

    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    
    tarentWidgetButton button_up = new tarentWidgetButton(readIcon("up.gif"));
    tarentWidgetButton button_down = new tarentWidgetButton(readIcon("down.gif")); 
    tarentWidgetButton button_search = new tarentWidgetButton("suchen");             
    horizpanel_avail.addWidget(button_up, 33); 
    horizpanel_avail.addWidget(button_down, 33); 
    horizpanel_avail.addWidget(button_search, 33); 
    tarentpanel.addWidget("", horizpanel_avail);

    
    button_up_clicked button_up_clicked_actionlistener = new button_up_clicked();
    button_up.addActionListener(button_up_clicked_actionlistener); 
    button_down.addActionListener(new button_down_clicked());
    button_search.addActionListener(new button_search_clicked());
    
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    m_oCompListBox_ResourcesUsed = new tarentWidgetComponentListBox();
    m_oCompListBox_ResourcesUsed.getJList().setVisibleRowCount(4);    
    tarentpanel.addWidget("zugeordnete Ressourcen:", m_oCompListBox_ResourcesUsed); 
    
    tarentWidgetHorizontalPanel horizpanel_used = new tarentWidgetHorizontalPanel();         
    
    tarentWidgetButton button_change = new tarentWidgetButton("�ndern");
    horizpanel_used.addWidget(button_change, 50);

    tarentWidgetButton button_del = new tarentWidgetButton("entfernen");
    horizpanel_used.addWidget(button_del, 50);     
    button_del.addActionListener(button_up_clicked_actionlistener);     
    tarentpanel.addWidget("", horizpanel_used);

    panel.add(tarentpanel, BorderLayout.NORTH);
    IconComboBoxEntry entry = (IconComboBoxEntry)(m_oCompListBox_ResourceTypes.getSelectedItem());
    fillFilteredRessourceListByKey(entry.getKey());
    return(panel);
  }
  
  public void addRessourceEntry(Object key, Object type, ImageIcon icon, String text, int numavail)
  {
    JPanel entry = new RessourceEntry(key, type, icon, text, numavail);
    m_oAvailableRessources.add(entry);
  }

  
  private class combo_ressourcetype_changed implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      IconComboBoxEntry entry = (IconComboBoxEntry)(m_oCompListBox_ResourceTypes.getSelectedItem());
      fillFilteredRessourceListByKey(entry.getKey());
    }    
  }
  
  
  private void fillFilteredRessourceListByKey(Object key)
  {
    m_oCompListBox_ResourcesAvailable.removeAllData(); 
    
    if (key != null)
    {
      if (RESSOURCETYPE_ALL.equals(key))
      {
        for(int i=0; i<(m_oAvailableRessources.size()); i++)
        {
          m_oCompListBox_ResourcesAvailable.addData(m_oAvailableRessources.get(i));
        }
      }
      else
      {
        for(int i=0; i<(m_oAvailableRessources.size()); i++)
        {
          RessourceEntry availentry = (RessourceEntry)(m_oAvailableRessources.get(i));
          if (availentry.getType().equals(key))
          {  
            m_oCompListBox_ResourcesAvailable.addData(availentry);
          }
        }        
      }
    }
    m_oCompListBox_ResourcesAvailable.getJList().repaint();
  }

  
  private void fillFilteredRessourceListByText(String key)
  {
    m_oCompListBox_ResourcesAvailable.removeAllData(); 
    
    if (key != null)
    {
      String text = ((String)key).toUpperCase().trim();
      for(int i=0; i<(m_oAvailableRessources.size()); i++)
      {
        RessourceEntry availentry = (RessourceEntry)(m_oAvailableRessources.get(i));
        if (availentry.getName().toUpperCase().indexOf(text) != -1)
        {  
          m_oCompListBox_ResourcesAvailable.addData(availentry);
        }
      }                
    }
    m_oCompListBox_ResourcesAvailable.getJList().repaint();
  }
  
    
  private RessourceEntry findRessourceEntryByKey(Object key)
  {
    for(int i=0; i<(m_oAvailableRessources.size()); i++)
    {
      RessourceEntry entry = (RessourceEntry)(m_oAvailableRessources.get(i));
      if (entry.getKey().equals(key)) return entry;      
    }
    return null;
  }
  
  
  private class RessourceEntry extends JPanel
  {
    private Object m_oKey;
    private Object m_oType;
    private ImageIcon m_oIcon;
    private String m_sName;
    private int m_iNumAvail;
    private boolean m_bShowNumAvail;
    
    private JLabel m_oIconLabel;
    private JLabel m_oNameLabel;
    private JLabel m_oNumLabel;
    
    public RessourceEntry(Object key, Object type, ImageIcon icon, String name, int numavail)
    {
      m_oKey = key;
      m_oType = type;
      m_oIcon = icon;      
      m_sName = name;
      m_iNumAvail = numavail;
      m_bShowNumAvail = true;
      
      this.setLayout(new BorderLayout());
	    
	    m_oIconLabel = new JLabel(m_oIcon);
	    m_oIconLabel.setBorder(new EmptyBorder(2,2,2,2)); 
	    m_oNameLabel = new JLabel(m_sName);
	    m_oNameLabel.setFont(m_oStdFont);
	    m_oNumLabel = new JLabel(Integer.toString(m_iNumAvail)); 
	    m_oNumLabel.setFont(m_oStdFont);
	    m_oNumLabel.setBorder(new EmptyBorder(2,2,2,2));
	    
	    this.add(m_oIconLabel, BorderLayout.WEST);    
	    this.add(m_oNameLabel, BorderLayout.CENTER);    
	    this.add(m_oNumLabel, BorderLayout.EAST);    
    }    
    
    public Object copy()
    {
      RessourceEntry re = new RessourceEntry(m_oKey, m_oType, m_oIcon, m_sName, m_iNumAvail);
      re.m_bShowNumAvail = m_bShowNumAvail;
      return re;
    }
    
    public int getNumAvail()
    {
      return m_iNumAvail;
    }

    public void setNumAvail(int numAvail)
    {
      m_iNumAvail = numAvail;
      m_oNumLabel.setText(Integer.toString(m_iNumAvail));
      if (m_iNumAvail <= 0) m_oNumLabel.setForeground(Color.RED);
      else m_oNumLabel.setForeground(Color.BLACK);
    }

    public ImageIcon getIcon()
    {
      return m_oIcon;
    }

    public void setIcon(ImageIcon icon)
    {
      m_oIcon = icon;
      m_oIconLabel.setIcon(m_oIcon);
    }

    public String getName()
    {
      return m_sName;
    }

    public void setName(String text)
    {
      m_sName = text;
      m_oNameLabel.setText(m_sName);
    }

    public boolean showNumAvail()
    {
      return m_bShowNumAvail;
    }

    public void setShowNumAvail(boolean showNumAvail)
    {
      m_bShowNumAvail = showNumAvail;
      m_oNumLabel.setVisible(m_bShowNumAvail);
    }

    public Object getKey()
    {
      return m_oKey;
    }

    public void setKey(Object key)
    {
      m_oKey = key;
    }

    public Object getType()
    {
      return m_oType;
    }

    public void setType(Object type)
    {
      m_oType = type;
    }
  }
  
  
  private class button_up_clicked implements ActionListener  
  {
    public void actionPerformed(ActionEvent e)
    {
      RessourceEntry re = (RessourceEntry)(m_oCompListBox_ResourcesUsed.getJList().getSelectedValue());
      if (re != null)
      {  
        RessourceEntry sourceentry = findRessourceEntryByKey(re.getKey());
        if (sourceentry != null)
        {                      
          sourceentry.setNumAvail(sourceentry.getNumAvail() + 1);
          m_oCompListBox_ResourcesUsed.removeData(re);
        }
      }
      m_oCompListBox_ResourcesUsed.getJList().repaint();
      m_oCompListBox_ResourcesAvailable.getJList().repaint();
    }    
  }
  
  private class button_down_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      RessourceEntry re = (RessourceEntry)(m_oCompListBox_ResourcesAvailable.getJList().getSelectedValue());
      if (re != null)
      {  
        if (re.getNumAvail() > 0)
        {  
          RessourceEntry newre = (RessourceEntry)(re.copy());
          newre.setShowNumAvail(false);
          re.setNumAvail(re.getNumAvail() - 1);
          m_oCompListBox_ResourcesUsed.addData(newre);
        }
        else
        {
          Toolkit.getDefaultToolkit().beep();
        }
      }
      m_oCompListBox_ResourcesUsed.getJList().repaint();
      m_oCompListBox_ResourcesAvailable.getJList().repaint();
    }   
  }
  
  
  private class RessourceSearchDialog extends JDialog
  {
    private tarentWidgetRadioButton m_oRadioButton_Type;
    private tarentWidgetRadioButton m_oRadioButton_Text;
    private tarentWidgetIconComboBox m_oCompListBox_Search_ResourceTypes;
    private tarentWidgetTextField m_oTextField_SearchText;    
    private ButtonGroup m_oButtonGroup;
    private button_cancel_clicked m_oButton_cancel_clicked_listener;
    private Object m_oSearchType;
    
    public final Object SEARCH_TYPE = "SEARCH_TYPE";
    public final Object SEARCH_TEXT = "SEARCH_TEXT";
    
    public RessourceSearchDialog()
    {
      createDialog();      
    }
    
    public RessourceSearchDialog(JDialog parent)
    {
      super(parent);
      createDialog();      
    }
    
    public RessourceSearchDialog(JFrame parent)
    {
      super(parent);
      createDialog();      
    }
    
    private void setDialogVisible(boolean visible)
    {
      this.setVisible(visible);
    }
    
    private void createDialog()
    {
      JPanel panel = new JPanel(new BorderLayout());
      m_oWidgetFactory.getLookAndFeelLabel().touch(panel);
      
      tarentPanel tarentpanel = new tarentPanel(); 
      tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
      
      JTextArea area = new JTextArea();
      area.setBackground(m_oWidgetFactory.getLookAndFeelLabel().getBackgroundColor());
      area.setBorder(null);
      area.setEditable(false);
      area.setWrapStyleWord(true);
      area.setLineWrap(true);
      area.setText("Suche nach Ressource\n\nBitte w�hlen Sie ob sie nach einem speziellen Ressource-Typ suchen wollen oder eine Volltextsuche �ber alle Ressourcen durchf�hren m�chten.");
      tarentWidgetPanel labelpanel = new tarentWidgetPanel(new BorderLayout());
      labelpanel.add(area, BorderLayout.CENTER);
      tarentpanel.addWidget("", labelpanel); 
      
      tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
      
      search_type_changed search_type_changed_listener = new search_type_changed();

      tarentPanel tarenttypepanel = new tarentPanel(); 
      
      m_oRadioButton_Type = new tarentWidgetRadioButton("Typ");
      m_oRadioButton_Type.addActionListener(search_type_changed_listener);
      m_oRadioButton_Text = new tarentWidgetRadioButton("Text");
      m_oRadioButton_Text.addActionListener(search_type_changed_listener);
      
      m_oButtonGroup = new ButtonGroup();
      m_oButtonGroup.add(m_oRadioButton_Type);
      m_oButtonGroup.add(m_oRadioButton_Text);
      
      m_oCompListBox_Search_ResourceTypes = new tarentWidgetIconComboBox();
      for(int i=0; i<(m_oCompListBox_ResourceTypes.getItemCount()); i++)
      {  
        IconComboBoxEntry item = (IconComboBoxEntry)(m_oCompListBox_ResourceTypes.getItemAt(i));        
        m_oCompListBox_Search_ResourceTypes.addIconComboBoxEntry(new IconComboBoxEntry(item.getIcon(), item.getText(), item.getKey()));      
      }      
      
      m_oTextField_SearchText = new tarentWidgetTextField("");    
        
      tarentWidgetHorizontalPanel typepanel = new tarentWidgetHorizontalPanel();               
      typepanel.addWidget(m_oRadioButton_Type, 20); 
      typepanel.addWidget(m_oCompListBox_Search_ResourceTypes, 80); 
      
      tarentWidgetHorizontalPanel textpanel = new tarentWidgetHorizontalPanel();               
      textpanel.addWidget(m_oRadioButton_Text, 20); 
      textpanel.addWidget(m_oTextField_SearchText, 80); 
      
      tarenttypepanel.addWidget("", typepanel); 
      tarenttypepanel.addWidget("", textpanel);       
      tarentpanel.addWidget("Suchen nach ", tarenttypepanel); 

      tarentWidgetHorizontalPanel buttonpanel = new tarentWidgetHorizontalPanel();               
      tarentWidgetButton button_ok = new tarentWidgetButton("Ok");
      button_ok.addActionListener(new button_ok_clicked());
      buttonpanel.addWidget(button_ok, 50);      
      tarentWidgetButton button_cancel = new tarentWidgetButton("Abbruch");
      m_oButton_cancel_clicked_listener = new button_cancel_clicked();
      button_cancel.addActionListener(m_oButton_cancel_clicked_listener);
      buttonpanel.addWidget(button_cancel, 50);
      tarentpanel.addWidget(buttonpanel); 
      
      setTypeSearchEnabled();        
      m_oButtonGroup.setSelected(m_oRadioButton_Type.getModel(), true);
      
      panel.add(tarentpanel, BorderLayout.NORTH);
      this.getContentPane().add(panel);
      this.setTitle("Suche nach Ressource");
      this.pack();
      this.setSize(480, 230);
      this.setResizable(false);
      this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      this.addWindowListener(new WindowAdapter() 
      {
        public void windowClosing(WindowEvent e) 
        {
          m_oButton_cancel_clicked_listener.doAction();
        }
      });
      
      setDialogVisible(true);
    }

    private class button_ok_clicked implements ActionListener
    {
      public void actionPerformed(ActionEvent e)
      {
        if (SEARCH_TYPE.equals(m_oSearchType))
        {  
          IconComboBoxEntry item = (IconComboBoxEntry)(m_oCompListBox_Search_ResourceTypes.getSelectedItem());
          if (item != null)
          {
            fillFilteredRessourceListByKey(item.getKey());
          }
        }
        else if (SEARCH_TEXT.equals(m_oSearchType))
        {  
          String text = m_oTextField_SearchText.getText();
          
          if (text.length() > 0)
          {
            fillFilteredRessourceListByText(text);
          }
        }
        setDialogVisible(false);
      }
    }
    
    private class button_cancel_clicked implements ActionListener
    {
      public void doAction()
      {
        setDialogVisible(false);
      }
      
      public void actionPerformed(ActionEvent e)
      {
        doAction();
      }
    }
    
    
    private void setTextSearchEnabled()
    {
      m_oSearchType = SEARCH_TEXT;
      
      m_oTextField_SearchText.setEnabled(true);
      m_oTextField_SearchText.setBackground(Color.WHITE);
      m_oCompListBox_Search_ResourceTypes.setEnabled(false);      
    }
    
    private void setTypeSearchEnabled()
    {
      m_oSearchType = SEARCH_TYPE;
      
      m_oTextField_SearchText.setEnabled(false);
      m_oTextField_SearchText.setBackground(Color.LIGHT_GRAY);
      m_oCompListBox_Search_ResourceTypes.setEnabled(true);                
    }
    
    
    private class search_type_changed implements ActionListener
    {
      public void actionPerformed(ActionEvent e)
      {
        ButtonModel model = m_oButtonGroup.getSelection();
        if (m_oRadioButton_Type.getModel().equals(model))
        {
          setTypeSearchEnabled();
        }
        else if (m_oRadioButton_Text.getModel().equals(model))
        {
          setTextSearchEnabled();
        }        
      }    
    }
  }

  
  private class button_search_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      new RessourceSearchDialog();
    }    
  }
  

  // --------------------------------------------------------------------------------
    
  private JPanel createRechtePanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(panel);
    
    tarentPanel tarentpanel = new tarentPanel(); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    m_oCompListBox_Rechte = new tarentWidgetComponentListBox();    
    m_oCompListBox_Rechte.getJList().setVisibleRowCount(20);        
    tarentpanel.addWidget("Rechte:", m_oCompListBox_Rechte); 
    //m_oCompListBox_Rechte.getJList().set
    
    panel.add(tarentpanel, BorderLayout.NORTH);
    return(panel);
  }
  
  public void addSystemUserEntry(Object key, ImageIcon icon, String vorname, String nachname)
  {
    addSystemUserEntry(new SystemUserEntry(key, icon, vorname, nachname));
  }

  public void addSystemUserEntry(SystemUserEntry entry)
  {
    //TODO: DEBUGCODE !!! Werte aus Datenbank lesen - nicht zuf�llig !!!
    entry.setRights((Math.random() > 0.5), (Math.random() > 0.5), (Math.random() > 0.5));    
    
    m_oCompListBox_Rechte.addData(entry);
  }
  
  private class SystemUserEntry extends JPanel
  {
    private Object m_oKey;
    private ImageIcon m_oIcon;
    private String m_sVorName;
    private String m_sNachName;
    
    private boolean m_bRead;
    private boolean m_bWrite;
    private boolean m_bPrivate;
    
    private JLabel m_oIconLabel;
    private JLabel m_oNameLabel;
    
    private JCheckBox m_oCheckRead;
    private JCheckBox m_oCheckWrite;
    private JCheckBox m_oCheckPrivate;
    
    
    public SystemUserEntry(Object key, ImageIcon icon, String nachname, String vorname)    
    {
      m_oKey = key;
      m_oIcon = icon;      
      m_sVorName = vorname;
      m_sNachName = nachname;
      
      this.setLayout(new BorderLayout());
      
      m_oIconLabel = new JLabel(m_oIcon);
      m_oIconLabel.setBorder(new EmptyBorder(2,2,2,2)); 

      m_oNameLabel = new JLabel(m_sNachName + ", " + m_sVorName);
      m_oNameLabel.setFont(m_oStdFont);      
      
      m_oCheckPrivate = new JCheckBox();
      m_oCheckRead = new JCheckBox();
      m_oCheckWrite = new JCheckBox();
       
      JPanel checkpanel = new JPanel(new GridLayout(1,0));
      checkpanel.add(m_oCheckPrivate);
      checkpanel.add(m_oCheckRead);
      checkpanel.add(m_oCheckWrite);
      
      this.add(m_oIconLabel, BorderLayout.WEST);    
      this.add(m_oNameLabel, BorderLayout.CENTER);    
      this.add(checkpanel, BorderLayout.EAST);    
    }    
    

    public ImageIcon getIcon()
    {
      return m_oIcon;
    }

    public void setIcon(ImageIcon icon)
    {
      m_oIcon = icon;
      m_oIconLabel.setIcon(m_oIcon);
    }

    public String getVorName()
    {
      return m_sVorName;
    }

    public void setVorName(String text)
    {
      m_sVorName = text;
      m_oNameLabel.setText(m_sNachName + ", " + m_sVorName);
    }

    public String getNachName()
    {
      return m_sNachName;
    }

    public void setNachName(String text)
    {
      m_sNachName = text;
      m_oNameLabel.setText(m_sNachName + ", " + m_sVorName);
    }

    public Object getKey()
    {
      return m_oKey;
    }

    public void setKey(Object key)
    {
      m_oKey = key;
    }

    public boolean isPrivate()
    {
      return m_bPrivate;
    }

    public void setPrivate(boolean private1)
    {
      m_bPrivate = private1;
      m_oCheckPrivate.setSelected(m_bPrivate);
    }

    public boolean isRead()
    {
      return m_bRead;
    }

    public void setRead(boolean read)
    {
      m_bRead = read;
      m_oCheckRead.setSelected(m_bRead);
    }

    public boolean isWrite()
    {
      return m_bWrite;
    }

    public void setWrite(boolean write)
    {
      m_bWrite = write;
      m_oCheckWrite.setSelected(m_bWrite);
    }

    public void setRights(boolean read, boolean write, boolean isprivate)
    {
      m_bPrivate = isprivate;
      m_oCheckPrivate.setSelected(m_bPrivate);

      m_bRead = read;
      m_oCheckRead.setSelected(m_bRead);
      
      m_bWrite = write;
      m_oCheckWrite.setSelected(m_bWrite);
    }
    
  }
  
  
  
  
  
  
  
  
  // --------------------------------------------------------------------------------
    
  private JPanel createHistoriePanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(panel);
    
    tarentPanel tarentpanel = new tarentPanel(); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    m_oCompListBox_Historie = new tarentWidgetComponentListBox();     
    m_oCompListBox_Historie.getJList().setVisibleRowCount(20);    
   
    tarentpanel.addWidget("Historie:", m_oCompListBox_Historie); 
    
    panel.add(tarentpanel, BorderLayout.NORTH);
    return(panel);
  }
    
  public void addHistoryEntry(ImageIcon icon, String text)
  {
    JPanel entry = createHistoryEntryPanel(icon, text, new ScheduleDate());
    m_oCompListBox_Historie.addData(entry);
  }

  
  private JPanel createHistoryEntryPanel(ImageIcon icon, String name, ScheduleDate date)
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    
    JLabel iconlabel = new JLabel(icon);
    iconlabel.setBorder(new EmptyBorder(2,2,2,2)); 
    JLabel namelabel = new JLabel(name); 
    namelabel.setFont(m_oStdFont.deriveFont(Font.BOLD));
    JLabel datelabel = new JLabel(date.getDateTimeString() + " "); 
    datelabel.setFont(m_oStdFont.deriveFont(Font.ITALIC));

    JPanel westpanel = new JPanel();
    westpanel.setLayout(new BorderLayout());
    westpanel.add(iconlabel, BorderLayout.WEST);    
    westpanel.add(datelabel, BorderLayout.EAST);    
    
    panel.add(westpanel, BorderLayout.WEST);    
    panel.add(namelabel, BorderLayout.CENTER);    
    
    return(panel);
  }

  // --------------------------------------------------------------------------------  
  
  private JPanel createActionPanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    m_oWidgetFactory.getLookAndFeelLabel().touch(panel);
    
    tarentPanel tarentpanel = new tarentPanel(); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    
    m_oIconComboBox_ActionTime = new tarentWidgetIconComboBox();
    m_oIconComboBox_ActionTime.setSelectedColors(new Color(0xff, 0xff, 0xff), new Color(0x46, 0x64, 0x9e));
    m_oIconComboBox_ActionTime.setUnselectedColors(new Color(0x00, 0x00, 0x00), new Color(0xff, 0xff, 0xff));    
    m_oIconComboBox_ActionTime.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remindtime_never.gif"), "nie", REMINDTIME_NEVER));
    m_oIconComboBox_ActionTime.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remindtime_15min.gif"), "15 Minuter vorher", REMINDTIME_15MIN));
    m_oIconComboBox_ActionTime.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remindtime_30min.gif"), "30 Minuten vorher", REMINDTIME_30MIN));
    m_oIconComboBox_ActionTime.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remindtime_1hour.gif"), "1 Stunde vorher", REMINDTIME_1HOUR));    
    m_oIconComboBox_ActionTime.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remindtime_free.gif"), "frei w�hlbar", REMINDTIME_FREE));    
    tarentpanel.addWidget("Erinnerung:", m_oIconComboBox_ActionTime);
    m_oIconComboBox_ActionTime.addItemListener(new ActionTimeItemListener());
    
    m_oDateSpinner_ActionTimeFree = new tarentWidgetDateSpinner();
    tarentpanel.addWidget("Zeit:", m_oDateSpinner_ActionTimeFree); 
    m_oDateSpinner_ActionTimeFree.setEnabled(false);
    
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 
    tarentpanel.addWidget(new tarentWidgetSeparator()); 
    tarentpanel.addWidget(new tarentWidgetSpacer(5)); 

    m_oIconComboBox_ActionType = new tarentWidgetIconComboBox();
    m_oIconComboBox_ActionType.setSelectedColors(new Color(0xff, 0xff, 0xff), new Color(0x46, 0x64, 0x9e));
    m_oIconComboBox_ActionType.setUnselectedColors(new Color(0x00, 0x00, 0x00), new Color(0xff, 0xff, 0xff));    
    m_oIconComboBox_ActionType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remind_acoustic.gif"), "akustisch", REMIND_ACOUSTICAL));
    m_oIconComboBox_ActionType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remind_optic.gif"), "optisch", REMIND_OPTICAL));
    m_oIconComboBox_ActionType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remind_sms.gif"), "per SMS", REMIND_SMS));
    m_oIconComboBox_ActionType.addIconComboBoxEntry(new IconComboBoxEntry(readIcon("remind_email.gif"), "per eMail", REMIND_EMAIL));    
    tarentpanel.addWidget("Signal:", m_oIconComboBox_ActionType);
    m_oIconComboBox_ActionType.addItemListener(new ActionTypeItemListener());
    m_oIconComboBox_ActionType.setEnabled(false);
    
    
    panel.add(tarentpanel, BorderLayout.NORTH);
    return(panel);
  }

  // -----------------------------------------------------------------------------
  
  public Object getActionTime()
  {
    IconComboBoxEntry ice = (IconComboBoxEntry)(m_oIconComboBox_ActionTime.getSelectedItem());
    if (ice != null) return ice.getKey();
    return null;
  }
  
  private class ActionTimeItemListener implements ItemListener
  {
    public void itemStateChanged(ItemEvent arg0)
    {
      if (arg0.getStateChange() == ItemEvent.SELECTED)
      {  
        Object type = getActionTime();
        m_oIconComboBox_ActionType.setEnabled(!(REMINDTIME_NEVER.equals(type)));
        m_oDateSpinner_ActionTimeFree.setEnabled(REMINDTIME_FREE.equals(type));
      }
    }    
  }
  
  // -----------------------------------------------------------------------------
    
  public Object getActionType()
  {
    IconComboBoxEntry ice = (IconComboBoxEntry)(m_oIconComboBox_ActionType.getSelectedItem());
    if (ice != null) return ice.getKey();
    return null;
  }
  
  private class ActionTypeItemListener implements ItemListener
  {
    public void itemStateChanged(ItemEvent arg0)
    {
      if (arg0.getStateChange() == ItemEvent.SELECTED)
      {  
        Object type = getActionType();
      }
    }    
  }
  
  // -----------------------------------------------------------------------------
  
  public Object getRepeatType()
  {
    IconComboBoxEntry ice = (IconComboBoxEntry)(m_oIconComboBox_RepeatType.getSelectedItem());
    if (ice != null) return ice.getKey();
    return null;
  }
  
  private class RepeatTypeItemListener implements ItemListener
  {
    public void itemStateChanged(ItemEvent arg0)
    {
      if (arg0.getStateChange() == ItemEvent.SELECTED)
      {  
        Object type = getRepeatType();
      }
    }    
  }
  
  
  // private Methoden...  
  private ImageIcon readIcon(String filename)
  {
    try
    {
      return (new ImageIcon(new URL(m_oImageFolder, filename)));
    }
    catch(MalformedURLException mue)
    {
      return(null);
    }
  }
  
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------  
  
  private void fillBeteiligteList()
  {
    m_oCompListBox_BeteiligteAvailable.addData(createBeteiligterItemPanel("person1.jpg", "Ferger, Heiko"));
    m_oCompListBox_BeteiligteAvailable.addData(createBeteiligterItemPanel("person5.jpg", "M�ller-Ackermann, Tom"));
    m_oCompListBox_BeteiligteAvailable.addData(createBeteiligterItemPanel("person2.jpg", "Tinder, Stefanie"));
    m_oCompListBox_BeteiligteAvailable.addData(createBeteiligterItemPanel("person4.jpg", "V�lz, Julia"));
  } 
  
  private void fillHistoryList()
  {
    addHistoryEntry(readIcon("history_added.gif"), "Termin erstellt.");
    addHistoryEntry(readIcon("history_resourceadded.gif"), "Ressource \"Overheadprojektor\" hinzugef�gt.");
    addHistoryEntry(readIcon("history_anruf.gif"), "Teilnehmer \"Heiko Ferger\" angerufen");
    addHistoryEntry(readIcon("history_resourceremoved.gif"), "Ressource \"Overheadprojektor\" entfernt.");
  }
  
  private void fillRessourceList()
  {
    addRessourceEntry(RESSOURCE_OVERHEAD_BIG, RESSOURCETYPE_OVERHEAD, readIcon("ressource_overhead.gif"), "Overheadprojektor f�r gro�e R�ume", 3);
    addRessourceEntry(RESSOURCE_OVERHEAD_SMALL, RESSOURCETYPE_OVERHEAD, readIcon("ressource_overhead.gif"), "Overheadprojektor f�r kleine R�ume", 2);
    addRessourceEntry(RESSOURCE_CAR_CABRIOLET, RESSOURCETYPE_CAR, readIcon("ressource_car.gif"), "Dienstwagen (Cabriolet)", 1);
    addRessourceEntry(RESSOURCE_CAR_MINI, RESSOURCETYPE_CAR, readIcon("ressource_car.gif"), "Dienstwagen (Mini)", 2);
    addRessourceEntry(RESSOURCE_CAR_TRANSPORTER, RESSOURCETYPE_CAR, readIcon("ressource_car.gif"), "Dienstwagen (Transporter)", 1);
    addRessourceEntry(RESSOURCE_CAR_ROLLSROYCE, RESSOURCETYPE_CAR, readIcon("ressource_car.gif"), "Dienstwagen (Rolls Royce)", 1);
    addRessourceEntry(RESSOURCE_ROOM_BIG, RESSOURCETYPE_ROOM, readIcon("ressource_room.gif"), "Raum f�r max. 200 Personen", 4);
    addRessourceEntry(RESSOURCE_ROOM_SMALL, RESSOURCETYPE_ROOM, readIcon("ressource_room.gif"), "Raum f�r max. 5 Personen", 3);
    addRessourceEntry(RESSOURCE_LAPTOP_FAST, RESSOURCETYPE_LAPTOP, readIcon("ressource_laptop.gif"), "Laptop (2 GHz)", 2);
    addRessourceEntry(RESSOURCE_LAPTOP_SLOW, RESSOURCETYPE_LAPTOP, readIcon("ressource_laptop.gif"), "Laptop (800 MHz)", 4);
    addRessourceEntry(RESSOURCE_BEAMER_VGA, RESSOURCETYPE_BEAMER, readIcon("ressource_beamer.gif"), "Beamer (VGA-Aufl�sung)", 3);
    addRessourceEntry(RESSOURCE_BEAMER_XGA, RESSOURCETYPE_BEAMER, readIcon("ressource_beamer.gif"), "Beamer (XGA-Aufl�sung)", 2);
  }
  
  private void fillSystemUserList()
  {    
    addSystemUserEntry(null, readIcon("user.gif"), "Klink", "Michael");  
    addSystemUserEntry(null, readIcon("user.gif"), "R�ther", "Nikolai");  
    addSystemUserEntry(null, readIcon("user.gif"), "Geese", "Elmar");  
    addSystemUserEntry(null, readIcon("user.gif"), "Tsch�pe", "Jan");  
    addSystemUserEntry(null, readIcon("user.gif"), "M�ller-Ackermann", "Tom");  
    addSystemUserEntry(null, readIcon("user.gif"), "Esser", "Boris");  
  }
  
}
*/