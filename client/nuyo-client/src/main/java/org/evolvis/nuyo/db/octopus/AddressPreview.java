package org.evolvis.nuyo.db.octopus;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.remote.Method;


/**
 * @author kleinw
 * 
 * Dieses Objekt kapselt die Vorschau einer Adresse, bestehend aus einem pk und
 * dem taddressext.label_fn.
 *  
 */
public class AddressPreview implements Comparable {
    
    private String _previewString;
    private Integer _addressPk = new Integer(0);
    private String _addressPreview = "";
    private List _subCategories = new Vector();
    private String[] _previewFields;
    
    // Hier ist hart eincodiert, welche 
    // Preview-Felder vom Server in welcher Reihenfolge mit gesendet wurden
    // Dies soll sp�ter dynamisch configurierbar sein
    private static List availablePreviewFields = Arrays.asList( new String[]{
                                                                    Address.PROPERTY_NACHNAME.getKey(),
                                                                    Address.PROPERTY_VORNAME.getKey(),
                                                                    Address.PROPERTY_INSTITUTION.getKey(),
                                                                    Address.PROPERTY_PLZ.getKey(),
                                                                    Address.PROPERTY_ORT.getKey(),
                                                                    Address.PROPERTY_E_MAIL_DIENSTLICH.getKey()
    } );
    
    public AddressPreview(Address address) throws ContactDBException{
        
        _previewFields = new String[availablePreviewFields.size()];
        
        //generate preview Fields
        for (int i=0;i<availablePreviewFields.size();i++){
            _previewFields[i] = (String) address.getStandardData((String)availablePreviewFields.get(i));
        }
        _addressPk = new Integer(address.getAdrNr());
        
        _addressPreview = 
            getPreviewField( Address.PROPERTY_NACHNAME.getKey()) 
            +", "+ getPreviewField(Address.PROPERTY_VORNAME.getKey()) 
            +" * "+ getPreviewField(Address.PROPERTY_E_MAIL_DIENSTLICH.getKey()) 
            +" * "+ getPreviewField(Address.PROPERTY_ORT.getKey());
    }
    
    
    public AddressPreview(Integer addressId, String preview) throws PreviewFormatException {
        this( addressId +"#"+preview );
    }
    
    public AddressPreview(String preview) throws PreviewFormatException {
        if(preview == null || "".equals(preview)) throw new PreviewFormatException(Messages.getString("AddressPreview_Empty_Data"));
        _previewString = preview;
        StringTokenizer st = new StringTokenizer(preview, "#");
        String tmp = null;
        try {
            tmp = st.nextToken().trim();
            _addressPk = new Integer(tmp);
        }
        catch (NumberFormatException e1) {
            throw new PreviewFormatException(Messages.getFormattedString("AddressPreview_PK_Parsing_Error", new Object[]{tmp, preview}));
        }
        catch (NullPointerException e1) {
            throw new PreviewFormatException(Messages.getString("AddressPreview_Empty_Data"));
        }
        
        String prevToken = st.nextToken();
        if (prevToken.endsWith("|"))
            prevToken += " ";        
        _previewFields = prevToken.split("\\|");
        
        
        //----------- THE SHORT ADDRESS LABEL ------------------
        
        
        /*if (_previewFields.length == availablePreviewFields.size()) {                    
            _addressPreview = 
                getPreviewField( Address.PROPERTY_NACHNAME) 
                +", "+ getPreviewField(Address.PROPERTY_VORNAME) 
                +" * "+ getPreviewField(Address.PROPERTY_E_MAIL_DIENST) 
                +" * "+ getPreviewField(Address.PROPERTY_ORT);
        } else {
            _addressPreview = prevToken;
        }*/
        
        StringBuffer buffer = new StringBuffer();
        String temp="";
        
        if (!((String)getPreviewField( Address.PROPERTY_VORNAME.getKey())).trim().equals("")&&
            !((String)getPreviewField( Address.PROPERTY_NACHNAME.getKey())).trim().equals("")){
        	//name:
        	buffer.append(getPreviewField( Address.PROPERTY_VORNAME.getKey()));
        	buffer.append(" ");
        	buffer.append(getPreviewField( Address.PROPERTY_NACHNAME.getKey()));
        	
        	//eMail:
        	if (!((String)getPreviewField( Address.PROPERTY_E_MAIL_DIENSTLICH.getKey())).trim().equals("")){
            	buffer.append(" * ");
            	buffer.append(getPreviewField( Address.PROPERTY_E_MAIL_DIENSTLICH.getKey()));
            }
        	//city:
        	if (!((String)getPreviewField( Address.PROPERTY_ORT.getKey())).trim().equals("")){
            	buffer.append(" * ");
            	buffer.append(getPreviewField( Address.PROPERTY_ORT.getKey()));
            }
        } else if (!((String)getPreviewField( Address.PROPERTY_INSTITUTION.getKey())).trim().equals("")){
        	//if name is available: name
        	if (!((String)getPreviewField( Address.PROPERTY_NACHNAME.getKey())).trim().equals("")){
            	buffer.append(getPreviewField( Address.PROPERTY_NACHNAME.getKey()));
            	buffer.append(", ");
            }
        	//institution
        	temp = ((String)getPreviewField(Address.PROPERTY_INSTITUTION.getKey()));
        	if (temp.length()>25) temp=temp.substring(0,25);
        	buffer.append(temp);
        	//city:
        	if (!((String)getPreviewField( Address.PROPERTY_ORT.getKey())).trim().equals("")){
            	buffer.append(" * ");
            	buffer.append(getPreviewField( Address.PROPERTY_ORT.getKey()));
            }
        } else {
        	
        	if (_previewFields.length == availablePreviewFields.size()) {                    
                buffer.append(getPreviewField( Address.PROPERTY_NACHNAME.getKey()) 
                              +", "+ getPreviewField(Address.PROPERTY_VORNAME.getKey()) 
                              +" * "+ getPreviewField(Address.PROPERTY_E_MAIL_DIENSTLICH.getKey()) 
                              +" * "+ getPreviewField(Address.PROPERTY_ORT.getKey()));
            } else {
            	buffer.append(prevToken);
            }
        	
        }
        
        _addressPreview = buffer.toString();
        
        if (st.hasMoreTokens()) {
            StringTokenizer subs = new StringTokenizer(st.nextToken(), " ");
            while (subs.hasMoreElements()) {
                try {
                    tmp = subs.nextToken().trim();
                    _subCategories.add(new Integer(tmp));
                }
                catch (NumberFormatException e) {
                    throw new PreviewFormatException(Messages.getFormattedString("AddressPreview_SubCategoryID_Parsing_Error", new Object[]{preview,tmp}));
                }
            }
        }
    }
    
    public Integer getAddressPk() {
        return _addressPk;
    }
    
    public Iterator getAvailablePreviewFieldKeys() {        
        if (_previewFields == null || _previewFields.length != availablePreviewFields.size())
            return (new LinkedList()).iterator();
        return availablePreviewFields.iterator();
    }
    
    public Object getPreviewField(String fieldKey) {
        int index = availablePreviewFields.indexOf(fieldKey);
        if (index<0 || index >= _previewFields.length)
            return null;
        return _previewFields[index];
    }
    
    public String getAddressPreview() {
        
        return _addressPreview;
    }
    
    public void setAddressPreview(String addressPreview) {
        _addressPreview = addressPreview;
    }
    
    public List getSubCategories() {
        return _subCategories;
    }
    
    public boolean inOneSubCategory(List subCategories, Integer generalSubCategory) {
        for (Iterator it = subCategories.iterator(); it.hasNext();) {
            Integer subCategory = (Integer) it.next();
            if (_subCategories.contains(subCategory)) return true;
        }
        if (generalSubCategory != null)
            if (_subCategories.contains(generalSubCategory))
                return true;
        return false;
    }
    
    public boolean inAllSubCategories(List subCategories, Integer generalSubCategory) {
        for (Iterator it = subCategories.iterator(); it.hasNext();) {
            Integer subCategory = (Integer) it.next();
            if (!_subCategories.contains(subCategory)) return false;
        }
        /*        if (generalSubCategory != null)
         if (!_subCategories.contains(generalSubCategory))
         return false;
         */        return true;
    }
    
    public int compareTo(Object o) {
        AddressPreview preview = (AddressPreview) o;
        return _addressPreview.toLowerCase().compareTo(preview.getAddressPreview().toLowerCase());
    }
    
    public boolean equals(Object o) {
        return (o instanceof AddressPreview)
        && getAddressPk().equals( ((AddressPreview)o).getAddressPk() );
    }
    
    /*TODO: weg hier public String getStringFormat() {
     return _previewString;
     }*/
    
    public Preview getPreview () {
        return new Preview(_addressPreview);
    }    
    
    /*TODO: weg hier public String getPreviewAsString () {
     return _previewString;
     }*/
    
    public String toString() {
    	if (_previewString != null)
            return new StringBuffer(_previewString).toString();
        else return _addressPreview;
    }
    
    static public Collection getPreviews(ISelection filter) throws ContactDBException {
        Method method = new Method("getPreviews");
        return null;
    }
    
    static public class Preview {
        
        private String _firstname;
        private String _lastname;
        private String _organisation;
        
        public Preview (String completePreview) {
            StringTokenizer st1 = new StringTokenizer(completePreview, ",");
            if (st1.countTokens() != 2) {
                if (st1.countTokens() == 1) {
                    _firstname = st1.nextToken();
                    _lastname = "";
                }
                else {
                    _firstname = "";
                    _lastname = "";
                }
            } else {
                _lastname = st1.nextToken().trim();
                StringTokenizer st2 = new StringTokenizer(st1.nextToken(), "[]");
                if (st2.hasMoreTokens()) {
                    _firstname = st2.nextToken().trim();
                    if (st2.hasMoreTokens())
                        _organisation = st2.nextToken().trim();
                }
            }
        }
        
        public String getFirstname() {
            return _firstname;
        }
        
        public String getLastname() {
            return _lastname;
        }
        
        public String getOrganisation () {
            return _organisation;
        }
        
    }
    
    static public class PreviewFormatException extends Exception {
        
        public PreviewFormatException(String msg) {
            super(msg);
        }
        
    }
    
}
