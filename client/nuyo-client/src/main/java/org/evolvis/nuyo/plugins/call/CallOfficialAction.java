/**
 * 
 */
package org.evolvis.nuyo.plugins.call;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class CallOfficialAction extends AbstractGUIAction
{
	SnomPhoneConnection phone;
	
	private String host;
	private String username;
	private String password;
	
	public void actionPerformed(ActionEvent event)
	{
		if(phone == null)
			init();
		
		phone.callNumber(ApplicationServices.getInstance().getActionManager().getAddress().getTelefonDienstlich());
	}

	protected void init()
	{
		super.init();
		phone = new SnomPhoneConnection(host, username, password);
	}
}
