/*
 * Created on 29.03.2004
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import org.evolvis.nuyo.controller.DoubleCheckManager;
import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetComboBox;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class Bundesland3Field extends ContactAddressTextField
{
  private TarentWidgetComboBox m_oCombobox_bundesland;  
  private TarentWidgetLabel m_oLabel_bundesland;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }  
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
    m_oCombobox_bundesland.addFocusListener(new combo_focus_doublecheck(m_oCombobox_bundesland));
  }

  public void postFinalRealize()
  {    
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createBundeslandPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "BUNDESLAND3";
  }

  public void setEditable(boolean iseditable)
  {
  	m_oCombobox_bundesland.setEnabled(iseditable);
  }
 
  public void setData(PluginData data)
  {
    Object value = data.get(AddressKeys.BUNDESLAND3);
    if (value != null) m_oCombobox_bundesland.setSelectedIndex(getGUIListener().getIndexOfBundesland(value.toString()));
    else m_oCombobox_bundesland.setSelectedIndex(0);
  }

  public void getData(PluginData data)
  {
    data.set(AddressKeys.BUNDESLAND3, m_oCombobox_bundesland.getSelectedItem().toString());    
  }

  public boolean isDirty(PluginData data)
  {
    if (!((m_oCombobox_bundesland.getSelectedItem().equals("")) && (data.get(AddressKeys.BUNDESLAND2) == null))) //$NON-NLS-1$
      if (!(m_oCombobox_bundesland.getSelectedItem().equals(data.get(AddressKeys.BUNDESLAND2)))) return(true);
    
    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  
  public String getFieldName()
  {
    if (fieldName != null) return fieldName; 
    else return Messages.getString("GUI_Fields_Bundesland3");
  }
  
  public String getFieldDescription()
  {
    if (fieldDescription != null) return fieldDescription;
    else return Messages.getString("GUI_Fields_Bundesland3_ToolTip");    
  }
  
  
  
  private EntryLayoutHelper createBundeslandPanel(String widgetFlags)
  {
    m_oCombobox_bundesland = new TarentWidgetComboBox(getGUIListener().getBundeslaenderList().toArray());
    m_oCombobox_bundesland.setEditable(false);
    m_oCombobox_bundesland.setSelectedIndex(0);
    m_oCombobox_bundesland.setToolTipText(getFieldDescription()); //$NON-NLS-1$
    getWidgetPool().addWidget("COMBO_BUNDESLAND3", m_oCombobox_bundesland);    
    
    m_oLabel_bundesland = new TarentWidgetLabel(getFieldName());
    return(new EntryLayoutHelper(m_oLabel_bundesland, m_oCombobox_bundesland, widgetFlags));
  }
  
  
  
  private class combo_focus_doublecheck implements FocusListener
  {
    TarentWidgetComboBox combo;  
    Object value;

    public combo_focus_doublecheck(TarentWidgetComboBox cb)
    {
      combo = cb;            
    }
    
    public void focusGained(FocusEvent fe)
    {
      value = combo.getSelectedItem();    
    }
    
    public void focusLost(FocusEvent fe)
    {
      if (!(combo.getSelectedItem().equals(value)))
      {          
        // trigger DoublettenCheck
          DoubleCheckManager.getInstance().checkForDoublesAndHandleResults();           
      }
    }
  }          

}
