/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.plugin.PluginData;

/**
 * @author niko
 *
 */
public interface AddressField
{
  /**
   * 
   * @deprecated
   * @param widgetFlags
   * @return
   */
  public EntryLayoutHelper getPanel(String widgetFlags);
  
  public void setData(PluginData data);
  public void getData(PluginData data);
  public boolean isDirty(PluginData data);
  public void setEditable(boolean iseditable);  
  public String getKey();
  
  public void postFinalRealize();
  public int getTimerTicks();
  public void onTimerEvent();
  public void onShow();
  public boolean isWriteOnly();
  public int getContext();
  public Object getDisplayedObject();
  public void setDisplayedObject(Object displayedobject);  
  public String getFieldName();
  public String getFieldDescription();
  public void setFieldName(String name);
  public void setFieldDescription(String description);
  public void setDoubleCheckSensitive(boolean issensitive);
}
