package org.evolvis.nuyo.selector.actions;

import java.awt.event.ActionEvent;

import org.evolvis.xana.action.AbstractGUIAction;


public class ActionExit extends AbstractGUIAction {

	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}

}
