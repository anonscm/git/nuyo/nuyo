package org.evolvis.nuyo.db.octopus;

import java.util.Collection;

import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.ScheduleId;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractPersistentRelation;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;



/**
 * @author kleinw
 *
 */
public class CalendarImpl extends CalendarBean implements Calendar {
    
    //
    //	Soapanbindung
    //
    /**	Kalendar holen */
    private static final String METHOD_GET_SCHEDULES = "getSchedules";
    /** Anlegen oder �ndern eines Kalenders */
	private static final String METHOD_CREATE_OR_MODIFY_SCHEDULE = "createOrModifySchedule";
    /** Benutzergruppen eines Kalenders */
	private static final String METHOD_GET_USERGROUPS_FROM_SCHEDULE = "getUserGroupsFromSchedule";
    /** Benutzer eines Kalenders */
	private static final String METHOD_GET_USERS_FROM_SCHEDULE = "getUsersFromSchedule";

	
    /** Benutzergruppen, die gel�scht werden sollen */
	private static final String PARAM_GROUPS_TO_REMOVE = "groupstoremove";
    /** Benutzer, die gel�scht werden sollen */
	private static final String PARAM_USERS_TO_REMOVE = "userstoremove";
    /** Benutzergruppen, die hinzugef�gt werden sollen */
	private static final String PARAM_GROUPS_TO_ADD = "groupstoadd";
    /** Benutzer, die hinzugef�gt werden sollen */
	private static final String PARAM_USERS_TO_ADD = "userstoadd";
    /** Datenbank-ID des Kalenders */
	private static final String PARAM_SCHEDULEID = "scheduleid";

	
    /** Zugriff auf dieses Objekt in einer anonymen Klassen */
    protected CalendarImpl _this = this;
	
    
	/** Den EntityFetcher gebrauchen wir statisch */
	static {
        _fetcher = new AbstractEntityFetcher(METHOD_GET_SCHEDULES, true) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    Calendar calendar = new CalendarImpl();
                    ((IEntity)calendar).populate(values);
                    return (IEntity)calendar;
                }
            };
	}
	
	
	/**	Konstruktor zur Neuerschaffung dieses Objekts */
	protected CalendarImpl() {
        _users = new AbstractPersistentRelation(this) {
            public Collection getRelationContents() throws ContactDBException {
                ISelection f = Selection.getInstance()
                	.add(new ScheduleId(_id));
                return UserImpl.getUsers(f);
            }
            public String[] getFields() {return new String[]{PARAM_USERS_TO_ADD, PARAM_USERS_TO_REMOVE};}
            public void relateTo(IEntity entity) throws ContactDBException {((UserImpl)entity).add(_this);}
			public void killRelation(IEntity entity) throws ContactDBException {((UserImpl)entity).remove(_this);}
        };
        _userGroups = new AbstractPersistentRelation(this) {
            public Collection getRelationContents() throws ContactDBException {
                ISelection f = Selection.getInstance()
                	.add(new ScheduleId(_id));
                return UserGroupImpl.getUserGroups(f);
            }
            public String[] getFields() {return new String[]{PARAM_GROUPS_TO_ADD, PARAM_GROUPS_TO_REMOVE};}
            public void relateTo(IEntity entity) throws ContactDBException {}
			public void killRelation(IEntity entity) throws ContactDBException {}
        };
        
        _secretaries = new AbstractPersistentRelation(this) {
            public Collection getRelationContents() throws ContactDBException {
                ISelection f = Selection.getInstance()
                	.add(new ScheduleId(_id));
                return CalendarSecretaryRelationImpl.getCalendarSecretaries(f);
            }
            public String[] getFields() {return null;}
            public void relateTo(IEntity entity) throws ContactDBException {}
            public void killRelation(IEntity entity) throws ContactDBException {}
        };
    }
    
	
	/** Weiterer Konstruktor */
	protected CalendarImpl(Integer id, String name, Integer type, Integer userFk) throws ContactDBException {
	    this();
	    _id = id;
	    setAttribute(Calendar.KEY_NAME, name);
	    _type = type;
	    _user = (userFk == null)?null:new UserImpl(userFk);
	}
    
	
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}

	
    /**	Methode zum Speichern des Objekts */
    public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_OR_MODIFY_SCHEDULE);
        _users.includeInMethod(_method);
        _userGroups.includeInMethod(_method);
        _id = (Integer)_method.invoke();
        _users.commmit();
        _userGroups.commmit();
    }

    
    /**	Methode zum F�llen eines Objekts durch DB-Inhalte. @param = values - Liste mit den Inhalten */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        setAttribute(Calendar.KEY_NAME, data.getString(1));
        _type = data.getInteger(2);
        _user = (data.get(3) == null)?null:new UserImpl(data.getInteger(3));
    }
    
    
    /** Methode zum L�schen */
    public void delete() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED, "Diese Funktion ist noch nicht implementiert.");
    }

    
    /** Methode zum Wiederherstellen des Objekts */
    public void rollback() throws ContactDBException {
        populate(_responseData);
        _users.rollback();
        _userGroups.rollback();
    }
   

    /**	Hier kann gem�� Filter eine Liste von Objekten bezogen werden. @param filter - Der Filter */
    static public Collection getCalendars(ISelection filter) throws ContactDBException {return _fetcher.getEntities(filter,false);}
    
    
    /**	Stringrepr�sentation des Objekts. */
    public String toString () {
        return new StringBuffer()
        	.append(_id)
        	.append(" ")
        	.append(_attributes.get(Calendar.KEY_NAME))
        	.append(" ")
        	.append(_type)
        	.toString();
    }
}
