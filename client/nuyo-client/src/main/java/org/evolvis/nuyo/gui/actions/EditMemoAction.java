package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;


public class EditMemoAction extends AbstractGUIAction {

	private static final long	serialVersionUID	= 8710957723382212120L;
	private static final TarentLogger log = new TarentLogger(EditMemoAction.class);

	public void actionPerformed(ActionEvent e) {
		log.warning("EditMemoAction not implemented");
	}

}
