/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.ErrorHint.PositionErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;



/**
 * Will be defined in appearance.xml as guifields id={1018,1019}
 * and used in "Adresspanel rechts".
 *  
 * @author niko
 */
public class RegExpField extends GUIElementAdapter implements TarentGUIEventListener, FocusListener
{
  private TarentWidgetTextField m_oTextField;
  private SpecialCharDocument m_oSpecialCharDocument;
  
  public String getListenerName()
  {
    return "RegExpField";
  }  


  public RegExpField()
  {
    super();
    m_oTextField = new TarentWidgetTextField();

    m_oSpecialCharDocument = new SpecialCharDocument();    
    ((JTextField)m_oTextField.getComponent()).setDocument(m_oSpecialCharDocument);
  }

  
  
  public class SpecialCharDocument extends PlainDocument
  {
    private int     m_iMaxChars = -1;
    private String  m_sSpecialChars = "";
    private Pattern pattern;
    private Matcher matcher;
    
    public void setLengthRestriction(int maxchars)
    {
      m_iMaxChars = maxchars;
    }

    public void setSpecialChars(String chars)
    {
      m_sSpecialChars = chars;
      pattern = Pattern.compile( m_sSpecialChars );
    }
    
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
    {
      if (str == null) 
      {
        return;
      }
              
      String inttext = "";
      for (int i = 0; i < str.length(); i++) 
      {
        char c = str.charAt(i);
        matcher = pattern.matcher(str.substring(i, i+1));
        
        //if (m_sSpecialChars.indexOf(c) != -1)
        if (matcher.matches())
        {
          if (m_iMaxChars != -1)
          {
            if (super.getLength() < m_iMaxChars)            
            {
              inttext = inttext + (c);
            }
          }       
          else
          {
            inttext = inttext + (c);
          }
        }
      }
      super.insertString(offs, inttext, a);          
    }
  }
  
  
  public GUIElement cloneGUIElement()
  {
    RegExpField textfield = new RegExpField();
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      textfield.addParameter(param.getKey(), param.getValue());
    }
    return textfield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "RegExpField", new DataContainerVersion(0));

    /*
     
    ParameterDescription acceptedpd = new ParameterDescription("accepted", "erlaubte Zeichen", "die in diesem Feld erlaubten Zeichen.");
    acceptedpd.addValueDescription("String", "alle erlaubten Zeichen");
    d.addParameterDescription(acceptedpd);
    
    **/
    
    ParameterDescription maxlenpd = new ParameterDescription("maxlen", "maximale Anzahl erlaubter Zeichen", "die maximale Anzahl in diesem Feld erlaubter Zeichen.");
    maxlenpd.addValueDescription("Integer", "ein beliebiger Integer Wert");
    d.addParameterDescription(maxlenpd);
    
    return d;
  }
  

  public boolean addParameter(String key, String value)
  { 
   	if ("maxlen".equalsIgnoreCase(key)) 
    {
      try
      {
        int len = Integer.parseInt(value);
        m_oTextField.setLengthRestriction(len);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        nfe.printStackTrace();
      }
    }
   	
    
    /*
     * 
    else if ("accepted".equalsIgnoreCase(key)) 
    {
      m_oSpecialCharDocument.setSpecialChars(value);
      m_oParameterList.add(new ObjectParameter(key, value));
      return true;
    }
    */    
 
    else if ("regexp".equalsIgnoreCase(key))
    {
    	m_oSpecialCharDocument.setSpecialChars(value);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;    	
    }
    else if ("phone".equalsIgnoreCase(key))
    {
    	m_oSpecialCharDocument.setSpecialChars("(\\(|\\)|\\/|\\-|\\+|\\d|\\s)");
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;    	
    }
    
    
    return false;
  }

  
  public boolean canDisplay(Class data)
  {
    return data.isInstance(String.class);
  }

  public Class displays()
  {
    return String.class;
  }
  

  public TarentWidgetInterface getComponent()
  {
    return m_oTextField;
  }

  public void setData(Object data)
  {
    if (data != null) m_oTextField.setData(data);
    else m_oTextField.setData("");
  }

  public Object getData()
  {
    return(m_oTextField.getData());
  }


  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }


  public void initElement()
  {
    m_oTextField.addFocusListener(this);
  }

  public void disposeElement()
  {
    m_oTextField.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oTextField.requestFocus();
  }
  
  public Point getOffsetForError(ErrorHint errorhint)
  {
    int sizex = 0;     
    if (errorhint instanceof PositionErrorHint)
    {
      String text = m_oTextField.getText();
      Graphics2D g2d = (Graphics2D)(m_oTextField.getGraphics());
      int pos = ((PositionErrorHint)errorhint).getPosition();
      for(int i=0; i<pos; i++)
      {        
        Rectangle2D rect = m_oTextField.getFont().getStringBounds(String.valueOf(text.charAt(i)), g2d.getFontRenderContext());
        sizex += rect.getWidth();
      }      
      return(new Point(sizex, 0));
    }    
    return(new Point(0,0));
  }

}

