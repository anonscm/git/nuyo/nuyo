/**
 * 
 */
package org.evolvis.nuyo.plugins.calendar.ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.toedter.calendar.JDateChooser;

import de.tarent.commons.ui.CommonDialogButtons;
import de.tarent.commons.ui.EscapeDialog;
import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AppointmentDialog extends EscapeDialog {
	
	protected JPanel mainPanel;
	protected JTabbedPane tabbedPane;
	
	protected JPanel generalPanel;
	protected JPanel notePanel;
	protected JPanel notificationPanel;
	protected JPanel participantsPanel;
	protected JPanel repetionPanel;
	protected JPanel categoriesPanel;
	
	protected JTextField titleField;
	
	protected ActionListener actionListener;
	
	public AppointmentDialog(JFrame parent) {
		super(parent, "Appointment", true);
		
		setContentPane(getMainPanel());
		
		setSize(640, 360);
		setLocationRelativeTo(parent);
	}
	
	protected JPanel getMainPanel() {
		if(mainPanel == null) {
			
			PanelBuilder builder = new PanelBuilder(new FormLayout("fill:pref:grow", "fill:pref:grow, 10dlu, pref"));
			
			builder.setDefaultDialogBorder();
			
			CellConstraints cc = new CellConstraints();
			
			builder.add(getTabbedPane(), cc.xy(1, 1));
			
			builder.add(CommonDialogButtons.getSubmitCancelAndActionButtons("Submit", "Cancel", null, null, getActionListener(), new JButton("Manage Templates")), cc.xy(1, 3));
			
			mainPanel = builder.getPanel();
		}
		return mainPanel;
	}
	
	protected JTabbedPane getTabbedPane() {
		if(tabbedPane == null) {
			tabbedPane = new JTabbedPane();
			
			tabbedPane.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(tabbedPane));
			
			tabbedPane.addTab("General", getGeneralPanel());
			tabbedPane.addTab("Repetion", getRepetionPanel());
			tabbedPane.addTab("Notification", getNotificationPanel());
			tabbedPane.addTab("Categories", getCategoriesPanel());
			tabbedPane.addTab("Note", getNotePanel());
			tabbedPane.addTab("Participants", getParticipantsPanel());
		}
		return tabbedPane;
	}
	
	private JPanel getCategoriesPanel() {
		if(categoriesPanel == null) {
			categoriesPanel = new JPanel();
		}
		return categoriesPanel;
	}

	private JPanel getRepetionPanel() {
		if(repetionPanel == null) {
			repetionPanel = new JPanel();
		}
		return repetionPanel;
	}

	private JPanel getParticipantsPanel() {
		if(participantsPanel == null) {
			participantsPanel = new JPanel();
		}
		return participantsPanel;
	}

	private JPanel getNotificationPanel() {
		if(notificationPanel == null) {
			notificationPanel = new JPanel();
		}
		return notificationPanel;
	}

	private JPanel getNotePanel() {
		if(notePanel == null) {
						
			PanelBuilder builder = new PanelBuilder(new FormLayout("fill:pref:grow", // colums
			"fill:pref:grow")); // rows
			
			builder.setDefaultDialogBorder();
			
			builder.add(new JTextArea());
			
			notePanel = builder.getPanel();
		}
		return notePanel;
	}

	private JPanel getGeneralPanel() {
		if(generalPanel == null) {
			generalPanel = new JPanel();
			
			PanelBuilder builder = new PanelBuilder(new FormLayout("pref, 3dlu, pref, 3dlu, pref, 3dlu:grow, pref", // colums
					"pref, 3dlu, pref, 10dlu, pref, 3dlu, fill:pref, 3dlu, fill:pref, 10dlu, pref, 3dlu, pref")); // rows
			
			builder.setDefaultDialogBorder();
			
			
			
			CellConstraints cc = new CellConstraints();
			
			builder.addLabel("Title:", cc.xy(1, 1));
			builder.add(getTitleField(), cc.xyw(3, 1, 5));
			
			builder.add(createPlainLabel("Location:"), cc.xy(1, 3));
			builder.add(new JTextField(), cc.xyw(3, 3, 5));
			
			builder.addSeparator("Date and Time", cc.xyw(1, 5, 7));
			
			builder.add(createPlainLabel("Start:"), cc.xy(1, 7));
			builder.add(new JDateChooser(new Date()), cc.xy(3, 7));
			builder.add(new JComboBox(new String[] { "20:00" }), cc.xy(5, 7));
			builder.add(createPlainCheckBox("All-day"), cc.xy(7, 7));
			
			builder.add(createPlainLabel("End:"), cc.xy(1, 9));
			builder.add(new JDateChooser(new Date()), cc.xy(3, 9));
			builder.add(new JComboBox(new String[] { "22:00" }), cc.xy(5, 9));
			builder.add(createPlainLabel("Duration: 2 Hours"), cc.xy(7, 9));
			
			builder.addSeparator("Options", cc.xyw(1, 11, 7));
			
			builder.add(createPlainCheckBox("Private"), cc.xy(1, 13));
			builder.add(createPlainCheckBox("Free time"), cc.xy(3, 13));
			
			generalPanel = builder.getPanel();
		}
		return generalPanel;
	}

	protected ActionListener getActionListener() {
		if(actionListener == null) {
			actionListener = new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					if(event.getActionCommand().equals("submit"))
						AppointmentDialog.this.submitAndClose();
					else if(event.getActionCommand().equals("cancel"))
						AppointmentDialog.this.cancelAndClose();
				}
			};
		}
		return actionListener;
	}
	
	protected JTextField getTitleField() {
		if(titleField == null) {
			titleField = new JTextField();
			titleField.setFont(titleField.getFont().deriveFont(Font.BOLD));
		}
		return titleField;
	}
	
	private JLabel createPlainLabel(String text) {
		JLabel label = new JLabel(text);
		label.setFont(label.getFont().deriveFont(Font.PLAIN));
		return label;
	}
	
	private JCheckBox createPlainCheckBox(String text) {
		JCheckBox checkBox = new JCheckBox(text);
		checkBox.setFont(checkBox.getFont().deriveFont(Font.PLAIN));
		return checkBox;
	}
	
	protected void submitAndClose() {
		closeWindow();
	}
	
	protected void cancelAndClose() {
		closeWindow();
	}
}
