package org.evolvis.nuyo.gui.calendar;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.gui.Messages;



/**
 * A set of calendar configuration constants.<br>
 * This set extends {@link StartUpConstants}.<p>
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public interface CalendarSettings extends StartUpConstants {
    
    public String HOLIDAY_XML_TAG_NAME = "holiday";
    public String HOLIDAY_FILE_NAME = "holidays.xml";
    public String WORKDAY = Messages.getString("GUI_Calendar_Workday");
    public String WEEKEND = Messages.getString("GUI_Calendar_Weekend");
    public String[] NAMES_OF_WEEK_DAYS = Messages.getString("GUI_Calendar_Days_of_Week").split(",",7);
    public String[] ABBREVIATIONS_OF_DAYS = Messages.getString("GUI_Calendar_Abbreviations_for_Days_of_Week").split(",",7);
    
    public boolean[] IS_WEEKEND_VALUES = {true, false, false, false, false, false, true};

    public int WORK_BEGIN_TIME_SECONDS = 8*60*60;
    public int WORK_END_TIME_SECONDS = 18*60*60;
    
    /** Preferences node name of <i>calendar</i> settings.*/
    public String PREF_CALENDAR_NODE_NAME = "calendar";
    /** Preferences key of appointments panel <i>divider A</i> (horizontal).*/
    public String PREF_APPOITMENTS_PANEL_DIVIDER_A_KEY = "TerminPanel_DividerA";
    /** Preferences key of appointments panel <i>divider B</i> (vertical).*/
    public String PREF_APPOITMENTS_PANEL_DIVIDER_B_KEY = "TerminPanel_DividerB";
    /** Preferences key of appointments panel <i>divider C</i> (to the right).*/
    public String PREF_APPOITMENTS_PANEL_DIVIDER_C_KEY = "TerminPanel_DividerC";


}
