/* $Id: AddressExporter.java,v 1.3 2006/06/06 14:12:12 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Jens Neumaier. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.plugin;

import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JDialog;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.plugins.dataio.ExporterException;


/**
 * This interface should be implemented by all plugins capable of direct stream
 * export by calling the export()-method with a given OutputStream. 
 * There may be an optional configuration dialog before exporting.
 * 
 * @author Jens Neumaier, tarent GmbH
 *
 */
public interface AddressExporter extends AddressListPerformer {
    
    /**
     * Exports the file type chosen by selected FileFilter to OutputStream
     * 
     * @param out OutputStream to export to 
     * @param fileTypeDescription known description from FileFilter to select export file type, known from getSupportedFileTypes()
     * @throws IOException
     * @throws ExporterException 
     */
    public void export(OutputStream out, String fileTypeDescription) throws IOException, ExporterException;
    
    /**
     * Returns the supported file types as FileFilter array
     * 
     * @return Array of supported file types as FileFilter
     * @see org.evolvis.nuyo.plugins.dataio.DefaultFileFilter
     * 
     */
    public FileFilter[] getSupportedFileTypes();
    
    /**
     * Returns an optional configuration dialog before export starts
     * or null if no further configuration is needed
     * 
     * @param fileType selected FileFilter to configurate exporter for
     * @return dialog to show
     */
    public JDialog getConfigurationDialog(FileFilter fileType);
    
}
