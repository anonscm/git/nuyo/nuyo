/*
 * Created on 09.04.2005
 *
 */
package org.evolvis.nuyo.db.octopus;

import java.util.Collection;

import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;
import org.evolvis.nuyo.util.DateRange;


/**
 * @author simon
 *
 */
public class CalendarSecretaryRelationImpl extends CalendarSecretaryRelationBean implements
        CalendarSecretaryRelation {
    
    
    //
    //	Soapanbindung
    //
    
    /**	Freigaben holen */
    private static final String METHOD_GET_RELEASES_FOR_SCHEDULE = "getReleasesforSchedule";
    /** Anlegen oder �ndern einer Freigabe */
	private static final String METHOD_CREATE_OR_MODIFY_RELEASE_FOR_SCHEDULE = "createOrModifyReleaseforSchedule";
	/** L�schen einer Freigabe */
	private static final String METHOD_REMOVE_RELEASE_FOR_SCHEDULE = "deleteReleaseforSchedule";
	
	/** ID des Schedules */
	private static final String PARAM_SCHEDULEID = "scheduleid";
	/** ID des Freigaben Nutzers*/
	private static final String PARAM_RELEASEUSER = "release_user";
	/** Start des Zugriffs*/
	private static final String PARAM_BEGIN_SCHEDULE = "beginschedule";
	/** Ende des Zugriffs*/
	private static final String PARAM_END_SCHEDULE = "endschedule";
	/** Start des LeseZugriffs*/
	private static final String PARAM_BEGIN_READSCHEDULE = "beginread";
	/** Ende des LeseZugriffs*/
	private static final String PARAM_END_READSCHEDULE = "endread";
	/** Start des SchreibZugriffs*/
	private static final String PARAM_BEGIN_WRITESCHEDULE = "beginwrite";
	/** Ende des SchreibZugriffs*/
	private static final String PARAM_END_WRITESCHEDULE = "endwrite";	
    
    
    /** Zugriff auf dieses Objekt in einer anonymen Klassen */
    protected CalendarSecretaryRelationImpl _this = this;
    
    /**	Konstruktor f�r dieses Objekt */
    public CalendarSecretaryRelationImpl(Integer ScheduleID, Integer SecretaryUserID) {
        _scheduleID = ScheduleID;
        _SecretaryUserID =  SecretaryUserID;
    }
    protected CalendarSecretaryRelationImpl() {
     
    }
    
	/** Den EntityFetcher gebrauchen wir statisch */
	static {
        _fetcher = new AbstractEntityFetcher(METHOD_GET_RELEASES_FOR_SCHEDULE, true) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    CalendarSecretaryRelation calendarsecretary = new CalendarSecretaryRelationImpl();
                    ((IEntity)calendarsecretary).populate(values);
                    return (IEntity)calendarsecretary;
                }
            };
	}
	
	
	
	/**	Methode zum F�llen eines Objekts durch DB-Inhalte. @param = values - Liste mit den Inhalten */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        _scheduleID = data.getInteger(1);
        _SecretaryUserID = data.getInteger(2);
        _SecretaryUserOwnerID = data.getInteger(3);
        _accessDateRange = new DateRange (data.getDate(4),data.getDate(5));
        _readDateRange = new DateRange (data.getDate(6),data.getDate(7));
        _writeDateRange = new DateRange (data.getDate(8),data.getDate(9));
    }
    
    public void validate() throws ContactDBException {
      //  throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED, "Diese Funktion ist noch nicht implementiert.");
    }

    public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_OR_MODIFY_RELEASE_FOR_SCHEDULE);
        
        _method.addParam(PARAM_SCHEDULEID,_scheduleID);
        _method.addParam(PARAM_RELEASEUSER,_SecretaryUserID);
        _method.addParam(PARAM_BEGIN_SCHEDULE,new Long(_accessDateRange.getStartDate().getTime()));
        if (_accessDateRange.hasEndDate()) 
            _method.addParam(PARAM_END_SCHEDULE,new Long(_accessDateRange.getEndDate().getTime()));

        // ensure calendar can be viewed when no explicit read-daterange was given
        if ( _readDateRange.getStartDate() == null) _readDateRange = _accessDateRange;
        
        _method.addParam(PARAM_BEGIN_READSCHEDULE,new Long(_readDateRange.getStartDate().getTime()));
        if (_readDateRange.hasEndDate())
            _method.addParam(PARAM_END_READSCHEDULE,new Long(_readDateRange.getEndDate().getTime()));
        
        if (_writeDateRange.getStartDate() != null)
            _method.addParam(PARAM_BEGIN_WRITESCHEDULE,new Long(_writeDateRange.getStartDate().getTime()));
        
        if (_writeDateRange.hasEndDate())
            _method.addParam(PARAM_END_WRITESCHEDULE,new Long(_writeDateRange.getEndDate().getTime()));
        
        _id = (Integer) _method.invoke();
          
    }

    public void delete() throws ContactDBException {
        Method method = new Method(METHOD_REMOVE_RELEASE_FOR_SCHEDULE);
		method.addParam("id", _id);
		method.invoke();
    }

    public void rollback() throws ContactDBException {
        populate(_responseData);
    }
    
    /**	Hier kann gem�� Filter eine Liste von Objekten bezogen werden. @param filter - Der Filter */
    static public Collection getCalendarSecretaries(ISelection filter) throws ContactDBException {return _fetcher.getEntities(filter, false);}
    
}
