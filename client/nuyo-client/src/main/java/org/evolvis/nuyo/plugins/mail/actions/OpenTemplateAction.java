/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OpenTemplateAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5895370248113740419L;

	public void actionPerformed(ActionEvent e)
	{
		DispatchEMailDialog.getInstance().openTemplate();
	}
}
