/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * @author niko
 */
public class ExtendedDataItemForList extends ExtendedDataItem
{
  private JPanel             m_oTitlePanel;
  private JPanel             m_oTitleBorderPanel;
  private JLabel             m_oTitleLabel;
  private JPanel             m_oPanel;
  private JPanel             m_oContainerPanel;
  private GridBagLayout      m_oGridBagLayout;
  private GridBagConstraints m_oGridBagConstraints;
  private int                m_iNextFreeRow;
  private JLabel m_oTitleIconLabel;
  
  public JPanel createPanel(ExtendedDataItemModel itemmodel)
  {
    m_oPanel = new JPanel();
    m_oPanel.setLayout(new BorderLayout());
    
    m_iNextFreeRow = 0;
    
    m_oTitleBorderPanel = new JPanel();
    m_oTitleBorderPanel.setLayout(new BorderLayout());
    m_oTitleBorderPanel.setBorder(new EmptyBorder(0,0,5,0));
    
    m_oTitlePanel = new JPanel(); 
    m_oTitlePanel.setLayout(new BorderLayout());
    m_oTitlePanel.setBorder(new EmptyBorder(3,3,3,3));
    m_oTitlePanel.setBorder(new CompoundBorder(new LineBorder(Color.BLACK, 1), m_oTitlePanel.getBorder()));
    m_oTitleBorderPanel.add(m_oTitlePanel, BorderLayout.CENTER);
    
    
    m_oTitleLabel = new JLabel(itemmodel.getTitle());
    m_oTitlePanel.add(m_oTitleLabel, BorderLayout.CENTER);
    
    m_oTitleIconLabel = new JLabel();
    m_oTitlePanel.add(m_oTitleIconLabel, BorderLayout.WEST);
    
    m_oContainerPanel = new JPanel();
    m_oGridBagLayout = new GridBagLayout();
    m_oGridBagConstraints = new GridBagConstraints();
    m_oGridBagConstraints.insets = new Insets(2,2,2,2);
    m_oContainerPanel.setLayout(m_oGridBagLayout);
    
    m_oPanel.add(m_oTitleBorderPanel, BorderLayout.NORTH);
    m_oPanel.add(m_oContainerPanel, BorderLayout.CENTER);    
    m_oPanel.setBorder(new EmptyBorder(5,5,5,5));
    m_oPanel.setBorder(new CompoundBorder(new LineBorder(Color.BLACK, 1), m_oPanel.getBorder()));
    m_oPanel.setBorder(new CompoundBorder(new EmptyBorder(2,2,2,2), m_oPanel.getBorder()));
    
    for(int i=0; i<(itemmodel.getNumberOfExtendedDataLabeledComponents()); i++)
    {
      ExtendedDataLabeledComponent labeledcomponent = itemmodel.getExtendedDataLabeledComponent(i);
      addLabeledComponent(labeledcomponent.getLabelText(), labeledcomponent.getComponent());
    }
        
    JPanel itemborderpanel = new JPanel();
    itemborderpanel.setLayout(new BorderLayout());
    itemborderpanel.add(m_oPanel, BorderLayout.NORTH);
        
    return(itemborderpanel);
  }


  public void setTitleIcon(ImageIcon icon)
  {
    m_oTitleIconLabel.setIcon(icon);
  }


  public void setTitleText(String text)
  {
    super.setName(text);
    m_oTitleLabel.setText(text);
  }
  
  private void addLabeledComponent(String labeltext, JComponent component)
  {
    JLabel label = new JLabel(labeltext);
    
    m_oGridBagConstraints.anchor = GridBagConstraints.EAST;
                
    m_oGridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
    m_oGridBagConstraints.fill      = GridBagConstraints.NONE;
    m_oGridBagConstraints.weightx   = 0.0;
    m_oGridBagConstraints.gridx     = 0;
    m_oGridBagConstraints.gridy     = m_iNextFreeRow;
    m_oGridBagLayout.setConstraints(label, m_oGridBagConstraints);
    m_oContainerPanel.add(label);
    
    m_oGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
    m_oGridBagConstraints.fill      = GridBagConstraints.HORIZONTAL;
    m_oGridBagConstraints.weightx   = 1.0;
    m_oGridBagConstraints.gridx     = 1;
    m_oGridBagConstraints.gridy     = m_iNextFreeRow;    
    m_oGridBagLayout.setConstraints(component, m_oGridBagConstraints);
    m_oContainerPanel.add(component);  
 
    m_iNextFreeRow++;  
  }
}
