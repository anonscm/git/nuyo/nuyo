/*
 * Created on 07.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.parser;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author niko
 *
 */
public class PluginLocator {
    private Logger logger;

    private URL pluginLocationURL = null;

    public PluginLocator( Logger oLogger ) {
        logger = oLogger;
        pluginLocationURL = findPluginLocation();
        if ( pluginLocationURL == null ) {
            logger.severe( "Can't find myself! Schwere Pers�nlichkeitsst�rung in Modul PluginLocator." );
        }
        else {
            logger.config( "found myself at " + getPluginLocationString() );
        }
    }

    public URL getPluginLocationURL() {
        return pluginLocationURL;
    }

    public String getPluginLocationString() {
        return ( pluginLocationURL != null ) ? pluginLocationURL.toExternalForm() : null;
    }

    private String[] getClassesFromJar( String packagename, URL location ) {
        logger.config( "reading plugins from JAR-file \"" + getPluginLocationString() + "\"" );

        JarResourcesFetcher jarresources = null;
        try {
            if ( location == null ) {
                jarresources = new JarResourcesFetcher( this );
            }
            else {
                jarresources = new JarResourcesFetcher( location );
            }
            
            if ( packagename.startsWith( "/" ) ) {
                packagename = packagename.substring( 1 );
            }
            List list = jarresources.getResourceList( packagename );
            String[] filenames = new String[list.size()];
            int fi = 0;
            for ( int i = 0; i < ( list.size() ); i++ ) {
                filenames[fi++] = (String) ( list.get( i ) );
            }
            return filenames;
        }
        catch ( ResourceFetcherException e ) {
            logger.log( Level.SEVERE, "unable to read plugin jar", e );
        }
        return null;
    }

    private String[] getClassesFromDirectory( URL pluginlocation ) {
        logger.config( "reading plugins from directory \"" + pluginlocation.toExternalForm() + "\"" );
        try {
            File file = new File( new URI( pluginlocation.toExternalForm() ) );
            if ( file.isDirectory() ) {
                File[] files = file.listFiles();

                List filenamelist = new ArrayList();
                for ( int i = 0; i < ( files.length ); i++ ) {
                    if ( files[i].getName().indexOf( '$' ) == -1 ) {
                        filenamelist.add( files[i].getName() );
                    }
                }

                String[] filenames = new String[filenamelist.size()];
                for ( int i = 0; i < ( filenamelist.size() ); i++ ) {
                    String filename = (String) ( filenamelist.get( i ) );
                    filenames[i] = filename;
                }

                return filenames;
            }
        }
        catch ( URISyntaxException use ) {
            logger.log( Level.SEVERE, "unable to read plugin directory", use );
        }
        return null;
    }


    public String[] getClasses( String packagename ) {
        URL pluginlocation = this.getClass().getResource( packagename );
        if ( pluginlocation != null ) {
            if ( pluginlocation.getProtocol().equalsIgnoreCase( "jar" ) ) {
                String[] classes = getClassesFromJar( packagename, pluginlocation );
                if ( classes != null ) {
                    return classes;
                }
                else {
                    return getClassesFromDirectory( pluginlocation );
                }
            }
            else {
                return getClassesFromDirectory( pluginlocation );
            }
        }
        return null;
    }

    private URL findPluginLocation() {
        ProtectionDomain oProtectionDomain = getClass().getProtectionDomain();
        if ( oProtectionDomain != null ) {
            CodeSource oCodeSource = oProtectionDomain.getCodeSource();
            if ( oCodeSource != null ) {
                return oCodeSource.getLocation();
            }
        }
        return null;
    }
}