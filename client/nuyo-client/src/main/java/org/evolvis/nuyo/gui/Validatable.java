/*
 * Created on 30.09.2004
 *
 */
package org.evolvis.nuyo.gui;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;

/**
 * @author niko
 *
 */
public interface Validatable
{
  public void validateAll();
  public void validate(DataContainer container);  
}
