/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class EMail3Field extends GenericTextField
{
  public EMail3Field()
  {
    super("EMAIL3", AddressKeys.EMAIL3, CONTEXT_ADRESS, "GUI_Fields_EMail3_ToolTip", "GUI_Fields_EMail3", 0);
  }
}
