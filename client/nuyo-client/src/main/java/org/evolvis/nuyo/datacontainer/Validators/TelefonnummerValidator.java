/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Validators;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.GenericErrorHint;
import org.evolvis.nuyo.datacontainer.ErrorHint.PositionErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.ValidatorAdapter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TelefonnummerValidator extends ValidatorAdapter
{
  private StyleValidator m_oStyleValidator = null;
  private String         m_sStyleValidatorKey = null;
  private Map m_oStyleValidators = new HashMap();
  
  public String getListenerName()
  {
    return "TelefonnummerValidator";
  }    
  
  public TelefonnummerValidator()
  {
    super();
    m_oStyleValidators.put("de", new TelStyleDE());
  }
    
  public Validator cloneValidator()
  {
    TelefonnummerValidator validator = new TelefonnummerValidator();
    
    if (m_sStyleValidatorKey != null)
    {
      validator.m_oStyleValidator = (StyleValidator)(validator.m_oStyleValidators.get(m_sStyleValidatorKey));
    }
 
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      validator.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    return validator;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.VALIDATOR, "TelefonnummerValidator", new DataContainerVersion(0));
    ParameterDescription descr = new ParameterDescription("locale", "L�nderkennzeichen", "das L�nderkennzeichen um die G�ltigkeitsregeln festzulegen.");    
    Iterator it = m_oStyleValidators.entrySet().iterator();
    while(it.hasNext())
    {
      Map.Entry entry = (Map.Entry)(it.next());
      String key = (String)(entry.getKey());
      StyleValidator sv = (StyleValidator)(entry.getValue());
      descr.addValueDescription(key, "�berpr�ft eine Telefonnummer nach den Regeln von \"" + sv.getLocaleName() + "\".");
    }    
    d.addParameterDescription(descr);
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("locale".equalsIgnoreCase(key)) 
    {
      StyleValidator validator = (StyleValidator)(m_oStyleValidators.get(value.toLowerCase()));
      if (validator != null)
      {
        m_oStyleValidator = validator;
        m_sStyleValidatorKey = value.toLowerCase();
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
    }    
    return false;
  }

  public boolean canValidate(Class data)
  {
    return data.isInstance(String.class);
  }

  
  private interface StyleValidator
  {
    public String getLocaleName();
    public boolean validate(String text);
  }
  


  public boolean validate(Object data)
  {
    if (data instanceof String)
    {
      if (m_oStyleValidator != null)
      {
        return m_oStyleValidator.validate((String)data);
      }
    }
    return false;
  }
  
  
  // deutsche Regeln f�r Telefonnummern...
  private class TelStyleDE implements StyleValidator
  {
    public String getLocaleName()
    {
      return "Deutschland";
    }

    public boolean validate(String text)
    {
      boolean valid = true;
      
      if (text.length() <= 1)
      {        
        GenericErrorHint eh = new GenericErrorHint("Telefonnummer zu kurz", "die Telefonnummer ist zu kurz um g�ltig zu sein", null, getDataContainer().getGUIElement(), getDataContainer()); 
        fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), eh));
        valid = false;                    
      }

      if ((text.indexOf('(') != -1) && (text.indexOf(')') == -1))
      {
        // �ffnende klammer ohne schliessende
        PositionErrorHint peh = new PositionErrorHint("Fehlende schliessende Klammer", "es wurde eine Klammer ge�ffnet ohne wieder geschlossen worden zu sein.", null, getDataContainer().getGUIElement(), getDataContainer(), text.indexOf('(')); 
        fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), peh));                    
        valid = false;                            
      } else if ((text.indexOf('(') == -1) && (text.indexOf(')') != -1))
      {
        // schliessende klammer ohne �ffnende 
        PositionErrorHint peh = new PositionErrorHint("Fehlende �ffnende Klammer", "es wurde eine Klammer geschlossen ohne vorher ge�ffnet worden zu sein.", null, getDataContainer().getGUIElement(), getDataContainer(), text.indexOf(')')); 
        fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), peh));                    
        valid = false;                            
      }
      else if ((text.indexOf('(') != -1) && (text.indexOf(')') != -1))
      {
        // beide klammern sind da 
        if (text.indexOf('(') > text.indexOf(')'))
        {
          // die klammern sind falsch rum 
          GenericErrorHint eh = new GenericErrorHint("Schliessende Klammer vor �ffnender Klammer", "es wurde eine Klammer geschlossen bevor sie ge�ffnet wurde.", null, getDataContainer().getGUIElement(), getDataContainer()); 
          fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), eh));                    
          valid = false;
        }
      }

      // TODO: hier sollten noch mehr Tests gemacht werden...
      // z.B: 
      // * ist nur ein "-" vorhanden
      // * ist die Klammerung (Landesvorwahl) auch wirklich VORNE ?
      // * doppelte Leerzeichen?
      
      return(valid);
    }
  }
}
