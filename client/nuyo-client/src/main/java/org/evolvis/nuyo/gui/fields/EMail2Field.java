/*
 * Created on 29.03.2004
 *
  */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class EMail2Field extends GenericTextField
{
  public EMail2Field()
  {
    super("EMAIL2", AddressKeys.EMAIL2, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_eMail2_Adresse_ToolTip", "GUI_MainFrameNewStyle_Standard_eMail2_Adresse", 80);
  }
}
