/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.plugin;

/**
 * @author niko
 *
 */
public interface PluginData
{
  public Object get(Object key);
  public boolean set(Object key, Object value);
  public Class getDatatype(Object key);  
}
