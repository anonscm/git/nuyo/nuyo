package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.xana.action.AbstractGUIAction;


/**
 * Separator action is an empty (dummy) action.<br>
 * The purpose of this action is to add a separator between two submenus.<br>
 * <p>
 * Target (problem) use case:<p>
 * <pre>
 * [root-menu-item]
 *   [menu-item]
 *   [submenu-item]
 *   [------------]
 *   [submenu-item]
 * </pre> 
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class SeparatorAction extends AbstractGUIAction {

    private static final long serialVersionUID = 1L;

    /** Empty action will be executed. */
    public void actionPerformed( ActionEvent e ) {
    }

}
