/* $Id: ActionManagerEvents.java,v 1.6 2006/12/11 19:16:36 aleksej Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.event.ContextVetoListener;
import org.evolvis.nuyo.controller.event.DataChangeListener;
import org.evolvis.nuyo.controller.event.MasterDataListener;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.veto.Veto;


/**
 * Diese Klasse verwaltet EventListener f�r den 
 * {@link org.evolvis.nuyo.controller.ActionManager ActionManager} und
 * feuert die entsprechenden Events.
 * 
 * @see org.evolvis.nuyo.controller.event.DataChangeListener
 * @see org.evolvis.nuyo.controller.event.ContextVetoListener
 * @author mikel
 */
public class ActionManagerEvents {
    private static final Logger logger = Logger.getLogger(ActionManagerEvents.class.getName());
    /*
     * DataChangeListener-Verwaltung
     */
    /**
     * Diese Liste verwaltet die angemeldeten
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}s.
     */
    protected final Set dataChangeListeners = new HashSet();

    /**
     * Diese Methode f�gt einen neuen
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addDataChangeListener(DataChangeListener listener) {
        if (listener != null)
            dataChangeListeners.add(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeDataChangeListener(DataChangeListener listener) {
        if (listener != null)
            dataChangeListeners.remove(listener);
    }

    /**
     * Diese Methode teilt mit, dass �nderungen in einem Address-Objekt
     * in die Datenbank gespeichert werden sollen.
     * 
     * @param address das zu speichernde Adressobjekt.
     * @param index der Index der zu speichernden Adresse in der
     *  aktuellen Adressensammlung; wenn -1, so ist die Adresse
     *  nicht in der Addresssammlung.
     */
    public void fireSavingAddressChanges(Address address, int index) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).savingAddressChanges(address, index);
    }

    /**
     * Diese Methode teilt mit, dass �nderungen in einem Address-Objekt
     * verworfen werden sollen.
     * 
     * @param address das originale Adressobjekt.
     * @param index der Index der zur�ckgesetzten Adresse in der
     *  aktuellen Adressensammlung; wenn -1, so ist die Adresse
     *  nicht in der Addresssammlung.
     */
    public void fireDiscardingAddressChanges(Address address, int index) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).discardingAddressChanges(address, index);
    }

    /**
     * Diese Methode teilt mit, dass sich eine Adresse ge�ndert hat.
     * 
     * @param address das aktualisierte Address-Objekt.
     * @param index der Index der aktualisierten Adresse in der
     *  aktuellen Adressensammlung; wenn -1, so ist die Adresse
     *  nicht in der Addresssammlung.
     */
    public void fireAddressChanged(Address address, int index) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).addressChanged(address, index);
    }

    /**
     * Diese Methode teilt mit, dass sich die zugrundeliegende Sammlung
     * von Adressen ge�ndert hat.
     * 
     * @param addresses die neue Adresssammlung.
     */
    public void fireAddressesChanged(Addresses addresses) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).addressesChanged(addresses);
    }

    /**
     * F�llt die GUI mit den �ber das Address-Objekt �bergebenen Werten.
     * Wenn als Parameter "null" �bergeben wird so werden alle Felder der
     * GUI gel�scht.
     * 
     * @param address die zu setzende Adresse (wenn null werden alle Felder gel�scht).
     * @param overWrite erzwingt das erneute Setzen der Adresse, auch wenn sie scheinbar
     *  schon die aktuelle ist.
     * @throws ContactDBException bei Datenzugriffsproblemen
     */
    public void fireSetAddress(Address address, boolean overWrite) throws ContactDBException {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).setAddress(address, overWrite);
    }


    /**
     * Setzt die Titel-Leiste der Anwendung
     * 
     * @param title der neue Text der Titel-Leiste.
     */
    public void fireSetCaption(String title) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).setCaption(title);
    }

    /**
     * Erm�glicht das Setzen des dargestellten Index des aktuellen
     * Datensatzes und der Anzahl angezeigter Datens�tze (Navigationsleiste).
     * 
     * @param currentIndex Index des aktuell angezeigten Datensatzes
     * @param numEntries Anzahl aller zur Navigation bereitstehenden Datens�tze
     */
    public void fireSetNavigationCounter(int currentIndex, int numEntries) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).setNavigationCounter(currentIndex, numEntries);
    }

    /**
     * Diese Methode teilt mit, welche Adresse in der Adress-Liste zu markieren ist.
     * 
     * @param index der Index der zu makierenden Adresse.
     */
    public void fireSelectAddress(int index) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).selectAddress(index);
    }

    /**
     * Diese Methode teilt mit, dass sich die ausgew�hlte Kategory ge�ndert hat.
     * 
     * @param newCategory die neue Kategory
     */
    public void fireCategoryChanged(Category newCategory) {
        Iterator it = dataChangeListeners.iterator();
        while (it.hasNext())
            ((DataChangeListener) it.next()).categoryChanged(newCategory);
    }


    /*
     * MasterDataListener-Verwaltung
     */
    /**
     * Diese Liste verwaltet die angemeldeten
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}s.
     */
    protected final Set masterDataListeners = new HashSet();

    /**
     * Diese Methode f�gt einen neuen
     * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addMasterDataListener(MasterDataListener listener) {
        if (listener != null)
            masterDataListeners.add(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeMasterDataListener(MasterDataListener listener) {
        if (listener != null)
            masterDataListeners.remove(listener);
    }

    /**
     * Diese Methode teilt mit, dass eine neue Kategorie dem System
     * hinzugef�gt worden ist.
     * 
     * @param categoryID ID der neuen Kategorie
     * @param categoryName Name der neuen Kategorie
     * @param categoryDescription Beschreibung der neuen Kategorie
     */
    public void fireCategoryAdded(String categoryID, String categoryName, String categoryDescription) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).categoryAdded(categoryID, categoryName, categoryDescription);
    }

    /**
     * Diese Methode teilt mit, dass eine Kategorie des System
     * editiert worden ist.
     * 
     * @param categoryID ID der Kategorie
     * @param categoryName Name der Kategorie
     * @param categoryDescription Beschreibung der Kategorie
     */
    public void fireCategoryEdited(String categoryID, String categoryName, String categoryDescription) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).categoryEdited(categoryID, categoryName, categoryDescription);
    }
    
    /**
     * Diese Methode teilt mit, dass eine Kategorie aus dem System
     * gel�scht worden ist.
     * 
     * @param categoryID ID der gel�schten Kategorie
     */
    public void fireCategoryRemoved(String categoryID) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).categoryRemoved(categoryID);
    }

    /**
     * Diese Methode teilt mit, dass eine Kategorie einem Benutzer
     * zug�nglich gemacht worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     */
    public void fireCategoryAssigned(String userID, String categoryID) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).categoryAssigned(userID, categoryID);
    }

    /**
     * Diese Methode teilt mit, dass Zugang zu einer Kategorie einem Benutzer
     * genommen worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     */
    public void fireCategoryDeassigned(String userID, String categoryID) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).categoryDeassigned(userID, categoryID);
    }
    
    /**
     * Diese Methode teilt mit, dass eine Kategorie einem Benutzer
     * als Standardkategorie zugeordnet worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     */
    public void fireStandardCategoryAssigned(String userID, String categoryID) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).standardCategoryAssigned(userID, categoryID);
    }

    /**
     * Diese Methode teilt mit, dass eine neue Unterkategorie dem System
     * hinzugef�gt worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der neuen Unterkategorie
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     */
    public void fireSubCategoryAdded(String categoryID, String id, String name1, String name2, String name3) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).subCategoryAdded(categoryID, id, name1, name2, name3);
    }

    /**
     * Diese Methode teilt mit, dass eine Unterkategorie des System
     * editiert worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der Unterkategorie
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     */
    public void fireSubCategoryEdited(String categoryID, String id, String name1, String name2, String name3) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).subCategoryEdited(categoryID, id, name1, name2, name3);
    }
    
    
    /**
     * Diese Methode teilt mit, dass eine Unterkategorie aus dem System
     * gel�scht worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der gel�schten Unterkategorie
     */
    public void fireSubCategoryRemoved(String categoryID, String id) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).subCategoryRemoved(categoryID, id);
    }

    /**
     * Diese Methode teilt mit, dass eine neue Benutzergruppe dem
     * System hinzugef�gt worden ist. 
     * 
     * @param newGroup die neue Benutzergruppe
     */
    public void fireUserGroupAdded(UserGroup newGroup) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).userGroupAdded(newGroup);
    }
    
    /**
     * Diese Methode teilt mit, dass ein neuer Benutzer dem System
     * hinzugef�gt worden ist.
     * 
     * @param userID ID des neuen Benutzers
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param isSpecial Flag f�r die Zugeh�rigkeit zu den speziellen
     *  Postversandnutzern
     * @param isAdmin Flag f�r die Zugeh�rigkeit zu den Administratoren
     * @param params eine Map zu ver�ndernder freier benutzerspezifischer Parameter.
     */
    public void fireUserAdded(String userID, String lastName, String firstName, String email) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).userAdded(userID, lastName, firstName, email);
    }


  /**
   * Diese Methode teilt mit, dass ein Benutzer
   * ge�ndert worden ist.
   * 
   * @param userID ID des bestehenden Benutzers
   * @param lastName der Nachname des Benutzers.
   * @param firstName der Vorname des Benutzers.
   * @param isSpecial Flag f�r die Zugeh�rigkeit zu den speziellen
   *  Postversandnutzern
   * @param isAdmin Flag f�r die Zugeh�rigkeit zu den Administratoren
   * @param params eine Map zu ver�ndernder freier benutzerspezifischer Parameter.
   */
  public void fireUserChanged(String userID, String lastName, String firstName, String email) {
      Iterator it = masterDataListeners.iterator();
      while (it.hasNext())
          ((MasterDataListener) it.next()).userChanged(userID, lastName, firstName, email);
  }


    /**
     * Diese Methode teilt mit, dass ein Benutzer aus dem System
     * entfernt worden ist.
     * 
     * @param userID ID des Benutzers
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#userRemoved(String)
     */
    public void fireUserRemoved(String userID) {
        Iterator it = masterDataListeners.iterator();
        while (it.hasNext())
            ((MasterDataListener) it.next()).userRemoved(userID);
    }

    /*
     * ContextVetoListener-Verwaltung
     */
    /**
     * Diese Liste verwaltet die angemeldeten
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}s.
     */
    protected final Set stateChangeListeners = new HashSet();

    /**
     * Diese Methode f�gt einen neuen
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addContextVetoListener(ContextVetoListener listener) {
        if (listener != null)
            stateChangeListeners.add(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeContextVetoListener(ContextVetoListener listener) {
        if (listener != null)
            stateChangeListeners.remove(listener);
    }

    /**
     * Setzt die GUI-Elemente je nach �bergebenem Parameter auf "editierbar"
     * bzw. "nur lesen".
     * 
     * @param veto ein Veto, das Einschr�nkungen der Editierbarkeit enthalten
     *  kann.
     * @param isEditable (wenn true: Elemente k�nnen editiert werden)
     * @see org.evolvis.nuyo.db.veto.Restriction
     */
    public void fireEditableVeto(Veto veto, boolean isEditable) {
        Iterator it = stateChangeListeners.iterator();
        logger.fine("[fire-editable]: " + isEditable);
        while (it.hasNext())
            ((ContextVetoListener) it.next()).handleContextVeto(veto);
    }
}
