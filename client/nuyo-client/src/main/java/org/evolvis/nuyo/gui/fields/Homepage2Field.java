/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class Homepage2Field extends GenericTextField
{
  public Homepage2Field()
  {
    super("HOMEPAGE2", AddressKeys.HOMEPAGE2, CONTEXT_ADRESS, "GUI_Fields_Web2_ToolTip", "GUI_Fields_Web2", 100);
  }
}
