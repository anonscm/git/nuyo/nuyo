/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.gui.AddressTable;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.mail.MailHelper;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.action.ContextChangeListener;
import org.evolvis.xana.action.MenuHelper;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;
import org.evolvis.xana.swing.utils.SwingIconFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.ColumnDescription;
import de.tarent.commons.ui.swing.ComboBoxMouseWheelNavigator;
import de.tarent.commons.utils.ByteHandlerSI;


/**
 * 
 * <p>The panel inside the DispatchEMailDialog</p>
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class DispatchEMailPanel extends JPanel implements ContextChangeListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2085543359145534570L;
	
	/* Context modes */
	public final static String EMAIL_PREVIEW_MODE = "email_preview";
	public final static String EMAIL_FIRST_MAIL_MODE = "email_first_mail";
	public final static String EMAIL_LAST_MAIL_MODE = "email_last_mail";

	private JPanel standardFieldsPanel;

	private JLabel senderLabel;
	private JComboBox senderBox;
	private JCheckBox copyToSenderBox;
	private JLabel addresseesLabel;
	//private JList addresseesList;
	private AddressTable addresseesList;
	private JLabel subjectLabel;
	private JTextField subjectField;

	private JPanel messagePanel;

	private JLabel templateLabel;
	private JTextArea messageArea;

	private JPanel attachmentPanel;

	private JLabel attachmentLabel;
	private JList attachmentList;

	private final static TarentLogger logger = new TarentLogger(DispatchEMailPanel.class);
	
	private JPopupMenu contextMenu;
	protected JMenuItem deleteItem;
	
	private final static int ADDRESSEE_LIST_SIZE = 6;
	private final static int ATTACHMENT_LIST_SIZE = 6;
	
	private List mailPanelActionListeners;
	private int actionEventIDCounter = 0;
	
	private String userMailSubject;
	private String userMailBody;
	
	private Addresses addresses;
	
	private boolean subjectFieldHadFocus;

	public DispatchEMailPanel()
	{
		FormLayout layout = new FormLayout(
				"pref:grow", // columns
		"pref, 8dlu, pref:grow, pref"); // rows

		setLayout(layout);

		CellConstraints cc = new CellConstraints();

		add(getMessagePropertiesPanel(), cc.xy(1, 1));
		add(getMessagePanel(), cc.xy(1, 3, CellConstraints.FILL, CellConstraints.FILL));
		add(getAttachmentPanel(), cc.xy(1, 4));

		setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		
		mailPanelActionListeners = new ArrayList();
		
		updateAttachmentState();
		
		ActionRegistry.getInstance().addContextChangeListener(this);
	}
	
	public void addActionListener(ActionListener pListener)
	{
		mailPanelActionListeners.add(pListener);
	}
	
	public void removeActionListener(ActionListener pListener)
	{
		mailPanelActionListeners.remove(pListener);
	}
	
	protected void fireActionEvent(Object pSource, String pActionCommand)
	{
		Iterator it = mailPanelActionListeners.iterator();
		while(it.hasNext())
			((ActionListener)it.next()).actionPerformed(new ActionEvent(pSource, actionEventIDCounter++, pActionCommand));
	}

	private JPanel getMessagePropertiesPanel()
	{
		if(standardFieldsPanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref, 4dlu, pref:grow, 4dlu, min", // columns
			"pref, 4dlu, 50dlu, 4dlu, pref"); // rows

			PanelBuilder builder = new PanelBuilder(layout);

			layout.setRowGroups(new int[][] {{ 1, 5 }});

			CellConstraints cc = new CellConstraints();

			builder.add(getSenderLabel(), cc.xy(1, 1));
			builder.add(getSenderBox(), cc.xy(3, 1));
			builder.add(getCopyToSenderBox(), cc.xy(5, 1));
			builder.add(getAddresseesLabel(), cc.xy(1, 3));
			builder.add(getAddresseesList().getTableComponent(), cc.xyw(3, 3, 3));
			builder.add(getSubjectLabel(), cc.xy(1, 5));
			builder.add(getSubjectField(), cc.xyw(3, 5, 3));

			standardFieldsPanel = builder.getPanel();
		}
		return standardFieldsPanel;
	}

	private JPanel getMessagePanel()
	{
		if(messagePanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref:grow", // columns
			"pref, 4dlu, pref:grow"); // rows

			PanelBuilder builder = new PanelBuilder(layout);

			CellConstraints cc = new CellConstraints();

			builder.add(getTemplateLabel(), cc.xy(1, 1));
			builder.add(new JScrollPane(getMessageArea()), cc.xy(1, 3, CellConstraints.FILL, CellConstraints.FILL));

			messagePanel = builder.getPanel();
		}
		return messagePanel;
	}

	private JPanel getAttachmentPanel()
	{
		if(attachmentPanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref:grow", // columns
			"8dlu, pref, 4dlu, pref:grow"); // rows

			PanelBuilder builder = new PanelBuilder(layout);

			CellConstraints cc = new CellConstraints();

			builder.add(getAttachmentLabel(), cc.xy(1, 2));
			builder.add(new JScrollPane(getAttachmentList()), cc.xy(1, 4));

			attachmentPanel = builder.getPanel();
		}
		return attachmentPanel;
	}

	private JLabel getSenderLabel()
	{
		if(senderLabel == null)
		{
			senderLabel = new JLabel(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_SENDER_LABEL"));
		}
		return senderLabel;
	}

	public JComboBox getSenderBox()
	{
		if(senderBox == null)
		{
			User currentUser = ApplicationServices.getInstance().getActionManager().getUser(null, true);
			try
			{
				Address userAddress = currentUser.getAssociatedAddress();
				
				List data = new ArrayList();
				String[] emailAddresses = { userAddress.getEmailAdresseDienstlich(), userAddress.getEmailAdressePrivat(), userAddress.getEmailAdresseWeitere() };
				
				for(int i=0; i < emailAddresses.length; i++)
				{
					if(emailAddresses[i] != null && emailAddresses[i].trim().length() > 0)
						data.add(userAddress.getVorname() + " " + userAddress.getNachname() + " <" +  emailAddresses[i] + ">");
				}
				
				
				String emailSenders = ConfigManager.getEnvironment().get(Key.EMAIL_SENDERS);
				if(emailSenders != null)
				{
					String[] emailSendersSplit = emailSenders.split(",");
					for(int i=0; i < emailSendersSplit.length; i++)
						if(emailSendersSplit[i] != null && emailSendersSplit[i].trim().length() > 0)
							data.add(emailSendersSplit[i]);
				}
					
				
				senderBox = new JComboBox(data.toArray());
				senderBox.addMouseWheelListener(new ComboBoxMouseWheelNavigator(senderBox));
			}
			catch(ContactDBException pExcp)
			{
				logger.warning(Messages.getString("GUI_EMAIL_GET_ADDRESS_ERROR"), pExcp);
			}
		}
		return senderBox;
	}

	private JCheckBox getCopyToSenderBox()
	{
		if(copyToSenderBox == null)
		{
			Action copyToSenderAction = ActionRegistry.getInstance().getAction("mail.copy.sender");
			
			copyToSenderBox = new JCheckBox(copyToSenderAction);

	        // Adds the checkbox to the list of elements whose selection state
	        // can be set through AbstractGUIAction.setSelected(boolean)
	        MenuHelper.addSynchronizationComponent( copyToSenderAction, copyToSenderBox );
		}
		return copyToSenderBox;
	}

	private JLabel getAddresseesLabel()
	{
		if(addresseesLabel == null)
		{
			addresseesLabel = new JLabel(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_ADDRESSEE_LABEL"));
		}
		return addresseesLabel;
	}
	
	public AddressTable getAddresseesList()
	{
		if(addresseesList == null)
		{
			addresseesList = new AddressTable(new ColumnDescription[]{
	                new ColumnDescription(Address.PROPERTY_E_MAIL_DIENSTLICH.getKey(), Address.PROPERTY_E_MAIL_DIENSTLICH.getLabel(), String.class) });
			addresseesList.getTable().setTableHeader(null);
			addresseesList.getTable().getSelectionModel().addListSelectionListener(new DispatchEMailPanelListSelectionListener());
			
			// we want to switch the components with tab or shift-tab
			addresseesList.getTable().addKeyListener(new KeyAdapter(){
				public void keyPressed(KeyEvent ke){
					if(ke.getKeyCode() == KeyEvent.VK_TAB)
					{
						ke.consume();
						if(ke.getModifiers() == KeyEvent.SHIFT_MASK)
							KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
						else
							KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
							
					}}});
			
		}
		return addresseesList;
	}

	private JLabel getSubjectLabel()
	{
		if(subjectLabel == null)
		{
			subjectLabel = new JLabel(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_SUBJECT_LABEL"));
		}
		return subjectLabel;
	}

	public JTextField getSubjectField()
	{
		if(subjectField == null)
		{
			subjectField = new JTextField();
			subjectField.setFont(subjectField.getFont().deriveFont(Font.BOLD));
			subjectField.addFocusListener(new FocusAdapter() {

				public void focusGained(FocusEvent e)
				{
					subjectFieldHadFocus = true;
				}
			});
		}
		return subjectField;
	}

	private JLabel getTemplateLabel()
	{
		if(templateLabel == null)
		{
			templateLabel = new JLabel();
			templateLabel.setFont(templateLabel.getFont().deriveFont(Font.PLAIN));
			templateLabel.setFont(templateLabel.getFont().deriveFont(Font.ITALIC));
			updateTemplateLabel(null);
		}
		return templateLabel;
	}

	public JTextArea getMessageArea()
	{
		if(messageArea == null)
		{
			messageArea = new JTextArea(1, 1);
			// does not work correctly yet
			//messageArea.addKeyListener(new DispatchEmailPanelKeyListener());
			messageArea.addFocusListener(new FocusAdapter() {

				public void focusGained(FocusEvent e)
				{
					subjectFieldHadFocus = false;
				}
				
			});
			// we want to switch the components with tab or shift-tab
			messageArea.addKeyListener(new KeyAdapter(){
				public void keyPressed(KeyEvent ke){
					if(ke.getKeyCode() == KeyEvent.VK_TAB)
					{
						ke.consume();
						if(ke.getModifiers() == KeyEvent.SHIFT_MASK)
							KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
						else
							 KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
							
					}}});
		   
		}
		
		return messageArea;
	}

	private JLabel getAttachmentLabel()
	{
		if(attachmentLabel == null)
			attachmentLabel = new JLabel(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_ATTACHMENT_LABEL"));
		return attachmentLabel;
	}

	public JList getAttachmentList()
	{
		if(attachmentList == null)
		{
			attachmentList = new JList(new MutableListModel());
			attachmentList.setCellRenderer(new FileListCellRenderer());
			attachmentList.setVisibleRowCount(attachmentList.getModel().getSize());
			
			contextMenu = new JPopupMenu();
			deleteItem = new JMenuItem((ImageIcon)SwingIconFactory.getInstance().getIcon("editdelete.png"));
			
			contextMenu.add(deleteItem);
			contextMenu.addSeparator();
			contextMenu.add(new JMenuItem(Messages.getString("GUI_CLOSE_CONTEXT_MENU")));
			
			attachmentList.addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
			        maybeShowPopup(e);
			    }

			    public void mouseReleased(MouseEvent e) {
			        maybeShowPopup(e);
			    }
			    
			    private void maybeShowPopup(MouseEvent e) {
			        if (e.isPopupTrigger()) {
			        	getAttachmentList().getSelectionModel().addSelectionInterval(getAttachmentList().locationToIndex(e.getPoint()),  getAttachmentList().locationToIndex(e.getPoint()));
			        	updateDeleteItemText();
			            contextMenu.show(e.getComponent(),
			                       e.getX(), e.getY());
			        }
			    }
			});
			
			ActionListener listener = new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					deleteSelectedAttachments();
				}
			};
			attachmentList.registerKeyboardAction(listener, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
			deleteItem.addActionListener(listener);
		}
		return attachmentList;
	}
	
	private void updateDeleteItemText()
	{
		if(getAttachmentList().getSelectedIndices().length > 1)
			deleteItem.setText(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_DELETE_ATTACHMENT_MULTI"));
		else
			deleteItem.setText(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_DELETE_ATTACHMENT_SINGLE"));
	}
	
	private void deleteSelectedAttachments()
	{
		Object[] selectedValues =getAttachmentList().getSelectedValues();
		for(int i=selectedValues.length-1; i >= 0; i--)
		{
			((MutableListModel)getAttachmentList().getModel()).remove(selectedValues[i]);
		}
		// need to reset selection in order to sync visual with logical selection
		getAttachmentList().clearSelection();
		updateAttachmentState();
		fireActionEvent(this, "attachments_deleted");

	}

	public void updateAttachmentState()
	{
		if(getAttachmentList().getModel().getSize() > 0)
		{
			// We have attachments. Show them!
			getAttachmentPanel().setVisible(true);
			if(getAttachmentList().getModel().getSize() <= ATTACHMENT_LIST_SIZE)
				getAttachmentList().setVisibleRowCount(getAttachmentList().getModel().getSize());
			else
				getAttachmentList().setVisibleRowCount(ATTACHMENT_LIST_SIZE);
			
			updateDeleteItemText();
			repaint();
		}
		else
		{
			// We do not have attachments. Hide List!
			getAttachmentPanel().setVisible(false);
			repaint();
		}
	}

	public void updateAddresseesState()
	{
		// TODO adapt to a JTable
		/*
		if(getAddresseesList().getModel().getSize() <= 0)
			getAddresseesList().setVisibleRowCount(1);
		else if(getAddresseesList().getModel().getSize() <= ADDRESSEE_LIST_SIZE)
			getAddresseesList().setVisibleRowCount(getAddresseesList().getModel().getSize());
		else
			getAddresseesList().setVisibleRowCount(ADDRESSEE_LIST_SIZE);
		repaint();
		*/
	}

	public void updateTemplateLabel(String templateName)
	{
		if(templateName != null)
		{
			getTemplateLabel().setText(Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_TEMPLATE_LABEL", templateName));
			getTemplateLabel().setVisible(true);
		}	
		else
			getTemplateLabel().setVisible(false);
		repaint();
	}
	
	public void insertTextAtCurrentPosition(String pText)
	{
		if(subjectFieldHadFocus)
		{
			getSubjectField().requestFocus();
			getSubjectField().setText(getSubjectField().getText().substring(0, getSubjectField().getCaretPosition()) + pText + getSubjectField().getText().substring(getSubjectField().getCaretPosition()));
		}
		else
			getMessageArea().insert(pText, getMessageArea().getCaretPosition());
	}

	public String getMessageSubject()
	{
		return getSubjectField().getText();
	}

	public void setMessageSubject(String pSubject)
	{
		getSubjectField().setText(pSubject);
	}

	public String getMessageBody()
	{
		return getMessageArea().getText();
	}

	public void setMessageBody(String pBody)
	{
		getMessageArea().setText(pBody);
	}
	
	public void setFieldsEditable(boolean pEditable)
	{
		getMessageArea().setEditable(pEditable);
		getSubjectField().setEditable(pEditable);
	}
	
	public void setupNavigationModes()
	{
		if(getAddresseesList().getSelectedIndex() == 0)
		{
			if(!ActionRegistry.getInstance().isContextEnabled(EMAIL_FIRST_MAIL_MODE))
				ActionRegistry.getInstance().enableContext(EMAIL_FIRST_MAIL_MODE);
		}
		else
		{
			if(ActionRegistry.getInstance().isContextEnabled(EMAIL_FIRST_MAIL_MODE))
				ActionRegistry.getInstance().disableContext(EMAIL_FIRST_MAIL_MODE);
		}
		
		if(getAddresseesList().getNumberOfRows() == 0 || getAddresseesList().getSelectedIndex() == getAddresseesList().getNumberOfRows()-1)
		{
			if(!ActionRegistry.getInstance().isContextEnabled(EMAIL_LAST_MAIL_MODE))
				ActionRegistry.getInstance().enableContext(EMAIL_LAST_MAIL_MODE);
		}
		else
		{
			if(ActionRegistry.getInstance().isContextEnabled(EMAIL_LAST_MAIL_MODE))
				ActionRegistry.getInstance().disableContext(EMAIL_LAST_MAIL_MODE);
		}
	}
	
	public void showPreviewMail()
	{
		setFieldsEditable(false);
		
		// If no address is selected yet, select first
		if(getAddresseesList().getTable().getSelectedRow() == -1)
			getAddresseesList().setSelectedIndex(0);

		Address currentAddress = getAddresses().get(getAddresseesList().getSelectedIndex());

		setMessageBody(MailHelper.doTemplateReplacement(userMailBody, currentAddress));
		setMessageSubject(MailHelper.doTemplateReplacement(userMailSubject, currentAddress));
	}
	
	public void showUserMail()
	{	
		setMessageSubject(userMailSubject);
		setMessageBody(userMailBody);
		setFieldsEditable(true);
	}
	
	public void setAddresses(Addresses pAddresses)
	{
		addresses = pAddresses;
		
		// TODO Replace this (is a workaround needed for not getting -1 for address-list-size)
		pAddresses.get(0);
		
		getAddresseesList().setAddressList(pAddresses);
		updateAddresseesState();
	}
	
	public Addresses getAddresses()
	{
		return addresses;
	}
	
	private class DispatchEMailPanelListSelectionListener implements ListSelectionListener
	{
		public void valueChanged(ListSelectionEvent e)
		{
			setupNavigationModes();
			if(ActionRegistry.getInstance().isContextEnabled(EMAIL_PREVIEW_MODE))
				showPreviewMail();
		}
	}

	private class AddressListCellRenderer extends DefaultListCellRenderer
	{
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
		{
			if(value instanceof Address)
			{
				Address address = (Address)value;
                JLabel label = new JLabel("<html><b>"+address.getVorname() + " " + address.getNachname() + "</b>&nbsp;" + address.getEmailAdresseDienstlich() + "</html>");
                label.setFont(label.getFont().deriveFont(Font.PLAIN));
                if(isSelected) label.setBackground(list.getSelectionBackground());
                else label.setBackground(list.getBackground());
                return label;
			}
			return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		}
	}

	private class FileListCellRenderer extends DefaultListCellRenderer
	{
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
		{
			if(value instanceof File)
			{
				JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				 
				File file = (File)value;
				label.setFont(getFont().deriveFont(Font.PLAIN));
				
				label.setText("<html><b>"+file.getName()+"</b>&nbsp;(" + ByteHandlerSI.getOptimalRepresentationForBytes(file.length()) + ")</html>");
				
				return label;
			}
			return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		}
	}
	
	private class DispatchEmailPanelKeyListener extends KeyAdapter
	{
		boolean smallerThanPressed = false;
		boolean seakMode = false;
		String tagName = "";
		
		public void keyPressed(KeyEvent pEvent)
		{		
			// we are only interested in character-inputs
			if(pEvent.getKeyChar() == KeyEvent.CHAR_UNDEFINED)
				return;
			
			if(!seakMode)
			{
				if(pEvent.getKeyChar() == '<')
					smallerThanPressed = true;
				else if(pEvent.getKeyChar() == '!')
				{
					// the combination "<!" has been typed, show tag-insert-dialog
					if(smallerThanPressed)
					{
						fireActionEvent(this, "tag_insert_requested");
						seakMode = true;
					}
				}
				else
					smallerThanPressed = false;
				
				super.keyPressed(pEvent);
			}
			else
			{
				if(pEvent.getKeyCode() == KeyEvent.VK_ENTER)
				{
					fireActionEvent(this, "insert_current_tag_requested");
					seakMode = false;
					tagName = "";
				}
				else if(pEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE)
				{
					firePropertyChange("tagName", tagName, tagName.substring(0, tagName.length()-1));
					tagName =  tagName.substring(0, tagName.length()-1);
				}
				else
				{
					firePropertyChange("tagName", tagName, tagName+pEvent.getKeyChar());
					tagName += pEvent.getKeyChar();
				}
			}
		}
	}
	
	public void contextChanged(String context, boolean isEnabled)
	{
		logger.fine( "[email-dialog context-handling]: " + context );
		if(DispatchEMailPanel.EMAIL_PREVIEW_MODE.equals(context) && !isEnabled)
		{
			showUserMail();
		}
		else if(DispatchEMailPanel.EMAIL_PREVIEW_MODE.equals(context) && isEnabled)
		{
			userMailBody = getMessageBody();
			userMailSubject = getMessageSubject();
			
			showPreviewMail();
		}
	}
}