/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class TelefonDienstlichField extends GenericTextField
{
  public TelefonDienstlichField()
  {
    super("TELEFONDIENST", AddressKeys.TELEFONDIENST, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Telefon_dienstlich_ToolTip", "GUI_MainFrameNewStyle_Standard_Telefon_dienstlich", 30);
  }
}
