package org.evolvis.nuyo.db.persistence;

import org.evolvis.nuyo.db.ContactDBException;

/**
 * @author kleinw
 *
 *	Diese Klasse bietet die M�glichkeit, nur die Methoden,
 *	die interessant sind zu implementieren.
 *
 */
public class EntityAdapter implements EntityListener {
    
    /**	Die Entit�t wurde gespeichert.@param event - Beinhaltet Daten zur Operation */	
    public void entityCommitted(EntityEvent event) throws ContactDBException {}

    
    /**	Die Entit�t wurde gel�scht.@param event - Beinhaltet Daten zur Operation */	
    public void entityDeleted(EntityEvent event) throws ContactDBException {}

    
    /**	Die Entit�t wurde neu geladen.@param event - Beinhaltet Daten zur Operation */	
    public void entityRefreshed(EntityEvent event) throws ContactDBException {}

    
    /**	Die Entit�t wurde zur�ckgesetzt.@param event - Beinhaltet Daten zur Operation */	
    public void entityRolledBack(EntityEvent event) throws ContactDBException {}
    
}
