package org.evolvis.nuyo.gui.actions.massAssignmentActions;

import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;


public class DeassignAddressesAction extends AssignOrDeassignAddressesAction{
	
	private static Logger logger = Logger.getLogger(DeassignAddressesAction.class.getName());

protected CategoryTree actionPerformedImpl() {
		
		List addressPks = ApplicationServices.getInstance().getActionManager().getAddresses().getPkList();
			
		if (addressPks == null || addressPks.size() == 0){
			logger.warning(Messages.getString("AssignOrDeassignAddresses_No_Addresses_Warning"));
			return null;
		}
			
		List accessableCategories = null;
		try {
			accessableCategories = ApplicationServices.getInstance().getCurrentDatabase().getAssociatedCategoriesForAddressSelection(
					ApplicationServices.getInstance().getActionManager().getAddresses().getPkList(), false, false, true, false, false, true, false, false, false);
		} catch (ContactDBException e1) {
			logger.warning(Messages.getString("AssignOrDeassignAddresses_Error_Warning"));
		}
		return CategoryTree.createRemoveCategoryTree(addressPks.size(), accessableCategories, accessableCategories, new AssignAddressesHandler(addressPks, false));
	}
}
