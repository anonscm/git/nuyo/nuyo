/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class EMailField extends GenericTextField
{
  public EMailField()
  {
    super("EMAIL", AddressKeys.EMAIL, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_eMail_Adresse_ToolTip", "GUI_MainFrameNewStyle_Standard_eMail_Adresse", 80);
  }
}
