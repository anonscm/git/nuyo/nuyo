package org.evolvis.nuyo.gui.actions;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.datahandling.binding.Binding;
import de.tarent.commons.datahandling.binding.BindingManager;

/**
 * This class allows actions to switch their enable state according to
 * the values of a numerb of bindings. That allows actions e.g. to be disabled
 * although their context is currently active.
 * 
 * <p>What bindings are used and how the values are interpreted is completely
 * up to the implementor.</p>
 * 
 * <p>The implementor should use the {@link #setBindingEnabled(boolean)} method
 * to change the action's enable state. Setting it to <code>false</code> means
 * the binding's value is unsuitable for the action to be available and will be
 * disabled even if the context of the action is currently active.</p>
 * 
 * <p>Some examples to clarify the issue:
 * <ul>
 * <li>an edit action should be disabled when there is not item to work on
 * (uses a single binding)</li>
 * <li>a mail action should be disabled when there is no item work on or mailing
 * on the currently selected item is forbidden (uses multiple bindings)</li>
 * </ul>
 * </p>
 * 
 * @author Robert Schuster
 *
 */
public abstract class AbstractBindingRestrictedAction extends AbstractGUIAction {

	private boolean contextEnabled = true;
	
	private boolean bindingEnabled = false;
	
	protected AbstractBindingRestrictedAction()
	{
	  Binding[] bindings = initBindings();
	  BindingManager bm = ApplicationServices.getInstance().getBindingManager();
	  for (int i=0;i<bindings.length;i++)
        bm.addBinding(bindings[i]);
	}
	
	/**
	 * A one time called method that should initialize and return the
	 * {@link Binding} instances which is are added to the {@link BindingManager}.
	 * 
	 * @return
	 */
	protected abstract Binding[] initBindings();
	
    /**
     * Sets the context enabled property of the action
     * which denotes whether the action should be enabled
     * because of their context membership.
     * 
     * <p>A call to this method causes the {@link #updateActionEnabledState()}
     * method to be called.</p> 
     * 
     * <p>This method is overridden to implement to binding
     * restriction and no subclass is allowed to change this
     * behavior further.</p>
     * 
     * @param b
     */
	public final void setEnabled(boolean b)
	{
		contextEnabled = b;
		updateActionEnabledState();
	}
	
    /** Returns an actions context enabled property.
     * 
     */
	public final boolean isEnabled()
	{
		return (bindingEnabled ? contextEnabled : false);
	}
	
	/**
	 * Sets the enabled state of the action according to whether the 
	 * action is part of the current context and the state of the
	 * binding value.
	 * 
	 * <p>The method should be called after the {@link #setBindingEnabled(boolean)}
	 * was invoked.</p>
	 */
	protected final void updateActionEnabledState()
	{
      super.setEnabled((bindingEnabled ? contextEnabled : false));
	}
	
	protected final void setBindingEnabled(boolean b)
	{
      bindingEnabled = b;
	}
    
}
