package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.office.OfficeInserterPlugin;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;

public class OfficeInsertAction extends AbstractGUIAction implements AddressListConsumer
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5045077459193180414L;

	private AddressListProvider provider;
	
	private TarentLogger logger = new TarentLogger(OfficeInsertAction.class);

	public void actionPerformed(ActionEvent pEvent)
	{
		if(provider != null)
		{
			if(provider.getAddresses() == null || provider.getAddresses().getSize() == 0)
			{
				// TODO insert entry in contact.properties
				logger.warning(Messages.getString(""));
			}
			else
			{
				DialogAddressListPerformer performer = retrieveDialogAddressListPerformer();
	        
	            if(performer != null)
	            {
	                performer.setAddresses(provider.getAddresses());
	                performer.execute();
	            }
	            else
	            {
	            	logger.warning(Messages.getString(this, "Load_Plugin_Failure"));
	            }
			}
		}	
	}

	public void registerDataProvider(AddressListProvider pProvider)
	{
		provider = pProvider;
	}
	
	public DialogAddressListPerformer retrieveDialogAddressListPerformer()
	{
		Plugin officePlugin = PluginRegistry.getInstance().getPlugin(OfficeInserterPlugin.ID);
		DialogAddressListPerformer addListPerf = null;
        if (officePlugin == null)
        {
            logger.warning("Plugin " + OfficeInserterPlugin.ID + " kann nicht geladen werden");
            return null;
        }
        
        if (officePlugin.isTypeSupported(DialogAddressListPerformer.class))
        {
            addListPerf = (DialogAddressListPerformer)officePlugin.getImplementationFor(DialogAddressListPerformer.class);
        } 
        else {
            logger.info("Angeforderter Plugin-Typ "+ DialogAddressListPerformer.class.getName() + " wird nicht unterst�tzt.");
        }
        
        return addListPerf;
	}
}
