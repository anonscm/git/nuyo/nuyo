/* $Id: GroupObjectRoleBean.java,v 1.3 2006/06/06 14:12:06 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.GroupObjectRoleConnector;
import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;

/**
 *
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
abstract public class GroupObjectRoleBean extends AbstractEntity implements GroupObjectRoleConnector {
    /**	Objekt zum Holen der Daten */
	static protected AbstractEntityFetcher _fetcher;
	/** Das Object mit dem die Verkn�pfung besteht */
	protected Object object = null;
	/** Die Gruppe mit der die Verkn�pfung besteht */
	protected UserGroup group = null;
	/** Die Rolle �ber die verkn�pft wird */
	protected ObjectRole role = null;
	
	public Object getObject(){
		return object;
	}
	public void setObject(Object o){
		object = o;
	}
	
	public ObjectRole getRole(){
		return role;
	}
	public void setRole(ObjectRole r){
		role = r;
	}
	
	public UserGroup getGroup(){
		return group;
	}
	
	public void setGroup(UserGroup ug){
		group = ug;
	}

    public boolean equals(Object o) {
        if (! (o instanceof GroupObjectRoleConnector))
            return false;
        GroupObjectRoleConnector gorc = (GroupObjectRoleConnector)o;
        return gorc.getObject().equals(getObject())
            && gorc.getRole().equals(getRole())
            && gorc.getGroup().equals(getGroup());
    }
}
