package org.evolvis.nuyo.selector.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.selector.CommandExecutor;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.commons.datahandling.entity.EntityList;



/**
 * This class is responsible for the menu-items that stand for 
 * simple shell-command actions of the tarent-selector. 
 * That is a simple action without any interaction with the calling application 
 * (like for example mailto:)
 * 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class CommandAction extends AbstractGUIAction implements AddressListConsumer{
	
	private static Logger logger = Logger.getLogger(CommandAction.class.getName());
	public static final String PROP_KEY_COMMAND = "Command";
	public static final String PROP_KEY_DELIMITER= "Delimiter";
	
	private AddressListProvider provider;
	
	
	public void actionPerformed(ActionEvent e) {
		
		if (provider!=null){
			//get command property from config
			String commandString = (String)this.getValue(PROP_KEY_COMMAND);
			String delimiter = (String) this.getValue(PROP_KEY_DELIMITER);
						
			if (commandString!=null){
				logger.finer("Kommando: "+commandString+" ");
				CommandExecutor executor = null;
				if (delimiter!=null){
					executor = new CommandExecutor(provider.getAddresses(),commandString,delimiter);
				} else {
					executor = new CommandExecutor(provider.getAddresses(),commandString);
				}
				//execute the command
				int returnCode = executor.execute();
				if (returnCode==CommandExecutor.ERR_INSUFFICIENT_DATA){
					reportInsufficientDataError(executor.getEntitiesWithInsufficientData());
				}
			} else {
				logger.warning("Konnte Kommando f�r die Aktion nicht laden");
			}
		}
	}
	
	/*
	 * this method is called by the data provider, in this case an AddressListProvider
	 * @see de.tarent.contact.gui.action.AddressListConsumer#registerDataProvider(de.tarent.contact.gui.action.AddressListProvider)
	 */
	public void registerDataProvider(AddressListProvider provider) {
		this.provider=provider;
	}
	
	private void reportInsufficientDataError(EntityList entities){
		
		StringBuffer buffer = new StringBuffer();
		Entity entity;
		for(int i=0;i<entities.getSize();i++){
			entity=(Entity)entities.getEntityAt(i);
			try {
				buffer.append(entity.getAttribute(Address.PROPERTY_SHORT_ADDRESS_LABEL.getKey()));
				buffer.append("\n");
			} catch (EntityException e) {
				logger.warning("Fehler beim Laden des Labels eines Entities");
				ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Laden des Labels eines Entities",e);
			}
		}
		ApplicationServices.getInstance().getCommonDialogServices().publishError("Hinweis","Nicht alle Adressen enthalten die ben�tigten Daten f�r die Aktion.","folgende Adressen konnten nicht verarbeitet werden:\n\n"+buffer.toString());
	}
}
