/* $Id: ShowAddressCommand.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * 
 * Created on 09.10.2003
 */
package org.evolvis.nuyo.messages;

import org.evolvis.nuyo.db.Address;

/**
 * Diese Kommandoklasse bewirkt einen Wechsel der aktuellen Addresse.
 * 
 * @author mikel
 */
public class ShowAddressCommand extends AddressCommand {
    /*
     * Konstruktoren
     */
    /**
     * @param source Quelle des Kommandos
     * @param address neue Addresse
     */
    public ShowAddressCommand(Object source, Address address) {
        super(source, address);
    }
}
