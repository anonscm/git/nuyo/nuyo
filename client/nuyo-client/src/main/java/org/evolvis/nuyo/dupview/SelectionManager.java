package org.evolvis.nuyo.dupview;

import java.util.HashMap;

import de.tarent.commons.datahandling.binding.AbstractModel;
import de.tarent.commons.datahandling.binding.DataChangedEvent;
import de.tarent.commons.datahandling.binding.Model;
import de.tarent.commons.datahandling.entity.EntityException;

/**
 * Model <code>SelectionManager</code> ought to be used for
 * Components (in an HTML widget) which provides a selection state. If the selection state of
 * the Component changes, the setAttribute methode will invoke (provided that
 * the <code>SelectionManager</code> is registered as a model in a 
 * <code>CompoundModel</code>).
 * 
 * This Model can be used e.g. for RadioButtons in an HTML widget. 
 * 
 * <p>e.g.:
 * <br>In <code>CompareView</code> the <code>SelectionManager</code> fills the rows
 * for the new Address if a RadioButton is selected. 
 * 
 * @author Thomas Schmitz, tarent GmbH
 */
public class SelectionManager extends AbstractModel{

	Model model;
	HashMap map = new HashMap();
	HashMap selections = new HashMap();
	
	/**
	 * Creates a new SelectionManager.
	 * 
	 * @param model
	 */
	public SelectionManager(Model model) {
		this.model = model;
	}
	
	/**
	 * Returns the attribute of the given key.
	 */
	public Object getAttribute(String key) throws EntityException {
		if (map.containsKey(key))
			return map.get(key);
		return model.getAttribute(key);
	}

	/**
	 * 
	 * e.g.: If you invoke this setAttribute methode like :
	 * 		 <code>selectionManager.setAttribute("vorname.1", new Boolean(true));</code>,
	 * 		 the value of "0.vorname" is made up of the value of "1.vorname"
	 */
	public void setAttribute(String key, Object value) throws EntityException {
		map.put(key, value);
		String[] pathParts = key.split("\\.");
		String row = pathParts[0];
		String column = pathParts[1];		
		Boolean selected = (value instanceof Boolean) ? (Boolean)value : Boolean.FALSE;		
		if (selected.equals(Boolean.TRUE) && !column.equals("0")) {
			model.setAttribute( "0."+row, model.getAttribute(column+"."+row));
			selections.put(key, value);
		} else if (value.equals(Boolean.FALSE)){
			if (selections.containsKey(key))
				selections.remove(key);
		}
		fireDataChanged(new DataChangedEvent(this, key));
	}
	
	/**
	 * Returns the size of the SelectionManager.
	 * 
	 * @return size of SelectionManager.
	 */
	public int getSize() {
		return map.size();
	}
	
	/**
	 * Returns the model.
	 * 
	 * @return the model.
	 */
	public Model getModel() {
		return model;
	}
}
