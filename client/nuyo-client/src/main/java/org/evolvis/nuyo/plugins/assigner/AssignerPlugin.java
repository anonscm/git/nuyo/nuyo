package org.evolvis.nuyo.plugins.assigner;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.plugin.DialogAddressListPerformer;

import de.tarent.commons.plugin.Plugin;

/**
 * This class is the plugin wrapper for the assigner-component. 
 * The plugin can provide different implementaions, 
 * each of one can be accessed throgh the method "getImplementationFor(Class type)"
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class AssignerPlugin implements Plugin {
	
	public static final String ID = "assigner";
	private DialogAssigner dialogAssigner;

	public AssignerPlugin() {
		dialogAssigner= new DialogAssigner();
	}

	public String getID() {
		return ID;
	}

	public void init() {
		//not implemented, nothing to intialise
	}

	public Object getImplementationFor(Class type) {
		
		if (type == DialogAddressListPerformer.class ){
			return dialogAssigner;
		}
		return null;
	}

	public List getSupportedTypes() {
		List list = new ArrayList();
		list.add(DialogAddressListPerformer.class);
		return list;
	}

	public boolean isTypeSupported(Class type) {
		if (type.equals(DialogAddressListPerformer.class)){
			return true;
		}
		return false;
	}

	public String getDisplayName() {
		return "Assigner";
	}

}
