/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.plugins.mail.EMailConfigurationDialog;
import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;

import de.tarent.commons.utils.SystemInfo;

/**
 * 
 * This action pops up a dialog, allowing the user to configure her E-Mail-related preferences
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class EMailConfigurationAction extends AbstractGUIAction
{
	/** <p>A list of known mail clients.</p>
	 * <p>First row is the clients name, second is its executable/path and third are the supported operating systems, comma-separated</p>
	 */
	
	public final static String [] WINDOWS_STANDARD = { "Windows Standard-Programm", "rundll32 url.dll,FileProtocolHandler mailto:{0}", "windows" }; 
	public final static String [] MOZILLA_THUNDERBIRD = { "Mozilla Thunderbird", "mozilla-thunderbird -mail mailto:{0}", "unix,macosx" };
	public final static String [] MOZILLA_SEAMONKEY = { "Mozilla Seamonkey", "mozilla -compose mailto:{0}", "unix,macosx" };
	public final static String [] KMAIL = { "KMail", "kmail {0}", "unix" };
	public final static String [] EVOLUTION = { "Evolution", "evolution mailto:{0}", "unix" };
	public final static String [] SYLPHEED_CLAWS_GTK2 = { "Sylpheed Claws GTK2", "sylpheed-claws-gtk2 --compose {0}", "unix" };
	public final static String [] MUTT = { "Mutt", "x-terminal-emulator -e mutt mailto:{0}", "unix" };
	public final static String [] OPERA = { "Opera", "opera mailto:{0}", "unix", "opera.png"};
	
	protected final static String [][] MAIL_CLIENTS = new String[][] {
		WINDOWS_STANDARD,
		MOZILLA_THUNDERBIRD,
		MOZILLA_SEAMONKEY,
		KMAIL,
		EVOLUTION,
		SYLPHEED_CLAWS_GTK2,
		MUTT,
		OPERA,
	};
	
	public void actionPerformed(ActionEvent e)
	{
		String defaultCommand;
		if(SystemInfo.isWindowsSystem())
			defaultCommand = WINDOWS_STANDARD[1];
		else
			defaultCommand = MOZILLA_THUNDERBIRD[1];
		
		EMailConfigurationDialog dialog = new EMailConfigurationDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame(), MAIL_CLIENTS, ConfigManager.getPreferences().get(Key.EMAIL_COMMAND.toString(), defaultCommand));
		dialog.pack();
		dialog.setLocationRelativeTo(ApplicationServices.getInstance().getCommonDialogServices().getFrame());
		dialog.setModal(true);
		dialog.setVisible(true);
		dialog.dispose();
	}
}
