/**
 * 
 */
package org.evolvis.nuyo.plugins.call;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import sun.misc.BASE64Encoder;
import de.tarent.commons.utils.PhoneNumberFormat;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class SnomPhoneConnection
{
	protected String host;
	protected String userName;
	protected String password;
	
	public SnomPhoneConnection(String host, String userName, String password)
	{
		this.host = host;
		this.userName = userName;
		this.password = password;
	}


	public void callNumber(String number)
	{
		URL url;
		try
		{
			url = new URL("http://"+host+"/adr.htm?number="+PhoneNumberFormat.convertToPlainDigitSequence(number));
			HttpURLConnection httpConnection;
			httpConnection = (HttpURLConnection)url.openConnection();
			
			httpConnection.setRequestProperty("Authorization", encodeUsernameAndPasswordInBase64(userName, password));

			httpConnection.connect();
			
			System.out.println(httpConnection.getResponseMessage());
			
		} catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	protected String encodeUsernameAndPasswordInBase64( String usern, String psswd ) {
		    String s = usern + ":" + psswd;	    
		    String encs = new BASE64Encoder().encode(s.getBytes());	    
		    return "Basic " + encs;
	}
}
