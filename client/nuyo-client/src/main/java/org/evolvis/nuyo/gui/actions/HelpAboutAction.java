package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.Starter;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.ui.swing.AboutDialog;
import de.tarent.commons.utils.VersionTool;


public class HelpAboutAction extends AbstractGUIAction {
	
	private static final long	serialVersionUID	= 5271059384758490688L;

	protected AboutDialog aboutDialog;
	
	public void actionPerformed(ActionEvent e) {
		
			SwingUtilities.invokeLater(new Runnable(){//thread safe
				
				public void run() {
					
					getAboutDialog().setVisible(true);
				}
			});
	}
	
	protected AboutDialog getAboutDialog()
	{
		if(aboutDialog == null)	{
			GUIListener guiListener = ApplicationServices.getInstance().getActionManager();
			aboutDialog = new AboutDialog(ApplicationServices.getInstance().getMainFrame().getFrame(),
					null,
					Messages.getString("MainFrame_Title"),
					Messages.getString("AboutDialog_Desc"),
					VersionTool.getInfoFromClass(Starter.class).getVersion("n/a"),
					VersionTool.getInfoFromClass(Starter.class).getBuildID("development build"),
					guiListener.getDisplaySource(),
					guiListener.getDatabaseVersion(),
					"/org/evolvis/nuyo/resources/contributions_de.html",
					"/org/evolvis/nuyo/resources/gpl.html");
			aboutDialog.pack();
			aboutDialog.setLocationRelativeTo(ApplicationServices.getInstance().getMainFrame().getFrame());
		}
		return aboutDialog;
	}
}
