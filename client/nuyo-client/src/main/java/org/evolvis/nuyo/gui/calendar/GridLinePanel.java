package org.evolvis.nuyo.gui.calendar;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.Date;

import javax.swing.JPanel;

/*
 * Created on 17.07.2003
 *
 */

/**
 * @author niko
 *
 */
public class GridLinePanel extends JPanel 
{
  private ScheduleData m_oScheduleData;
  private int m_iNumDays;
  private ScheduleDate m_oFirstDay;
  
  public void setFirstDay(ScheduleDate date)
  {
    m_oFirstDay = new ScheduleDate(new Date(date.getDate().getTime()));    
    this.repaint();
  }
  
  public void setNumDays(int numdays)
  {
    m_iNumDays = numdays;
    this.repaint();
  }
  
  
  public GridLinePanel(ScheduleData sd, ScheduleDate firstday, int numdays)
  {
    m_oScheduleData = sd;
    m_oFirstDay = firstday;
    m_iNumDays = numdays;
  }
    
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {        
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();
    
    // kompletten Hintergrund des Grids f�llen
    g.setColor(m_oScheduleData.freeBackgroundColor);
    g.fillRect(0, 0, (m_iNumDays * m_oScheduleData.dayWidth), (m_oScheduleData.numberOfHours * m_oScheduleData.hourHeight));

    for (int d=0; d<m_iNumDays; d++)
    {
      ScheduleDate date = m_oFirstDay.getDateByOffset(d);
      DayDescription dd = DateDescriptionFactory.instance().getDescriptionForDay(date);

      if (dd instanceof WorkDayDescription)
      {                
        WorkDayDescription wdd = (WorkDayDescription)dd;
        int x = (d * m_oScheduleData.dayWidth);
        int y = m_oScheduleData.getPixelForSecond(wdd.getStartWorkSecond());
        int w = m_oScheduleData.dayWidth;
        int h = m_oScheduleData.getPixelForSecond(wdd.getEndWorkSecond() - wdd.getStartWorkSecond());

        g.setColor(m_oScheduleData.workBackgroundColor);
        g.fillRect(x, y, w, h);        
      }
      
    }
    
    // das Stunden-Grid zeichnen
    for(int h = 0; h<25; h++)
    {
      int y = (h * m_oScheduleData.hourHeight);
      g.setColor(m_oScheduleData.gridColor);
      g.drawLine(0, y, (m_iNumDays * m_oScheduleData.dayWidth), y);
    }
      
    // das Tages-Grid zeichnen
    for(int d = 0; d<(m_iNumDays + 1); d++)
    {
      g.setColor(m_oScheduleData.gridColor);
      g.drawLine((d * m_oScheduleData.dayWidth), 0, (d * m_oScheduleData.dayWidth), (m_oScheduleData.numberOfHours * m_oScheduleData.hourHeight));
    }
  }
}
