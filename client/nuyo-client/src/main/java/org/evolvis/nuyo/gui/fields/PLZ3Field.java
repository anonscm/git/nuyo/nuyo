/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class PLZ3Field extends ContactAddressTextField
{

  // requires:
  // COMBO_BUNDESLAND
  // COMBO_LAND 
  
  private boolean m_bAlwaysOverwriteOrt = false;
  
  private TarentWidgetTextField m_oTextfield_plz;
  private TarentWidgetTextField m_oTextfield_ort;  
  private TarentWidgetLabel m_oLabel_plz;
  private TarentWidgetLabel m_oLabel_ort; 
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }  
    
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createPlzPanel(widgetFlags);
    return panel;
  }

  public void postFinalRealize()
  {    
  }
  
  public String getKey()
  {
    return "PLZ3";
  }

  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      m_oTextfield_plz.setEditable(true);      
      m_oTextfield_ort.setEditable(true);
    }
    else
    {  
      m_oTextfield_plz.setEditable(false);
      m_oTextfield_plz.setBackground(Color.WHITE);
    
      m_oTextfield_ort.setEditable(false);
      m_oTextfield_ort.setBackground(Color.WHITE);
    }
  }
 
  public void setData(PluginData data)
  {
    Object plzvalue = data.get(AddressKeys.PLZ3);
    if (plzvalue == null) m_oTextfield_plz.setText("");
    else m_oTextfield_plz.setText(plzvalue.toString());    
    
    Object ortvalue = data.get(AddressKeys.ORT3);
    if (ortvalue == null) m_oTextfield_ort.setText("");
    else m_oTextfield_ort.setText(ortvalue.toString());    
  }

  public void getData(PluginData data)
  {
    data.set(AddressKeys.PLZ3, m_oTextfield_plz.getText());
    data.set(AddressKeys.ORT3, m_oTextfield_ort.getText());
  }

  public boolean isDirty(PluginData data)
  {
    if (!((m_oTextfield_plz.getText().equals("")) &&  (data.get(AddressKeys.PLZ3) == null ))) //$NON-NLS-1$
      if (!(m_oTextfield_plz.getText().equals(data.get(AddressKeys.PLZ3)))) return(true);            

    if (!((m_oTextfield_ort.getText().equals("")) &&  (data.get(AddressKeys.ORT3) == null ))) //$NON-NLS-1$
      if (!(m_oTextfield_ort.getText().equals(data.get(AddressKeys.ORT3)))) return(true);            

    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  
  
  
  
  
  public String getFieldName()
  {
    return fieldName;     
  }
  
  public String getFieldDescription()
  {
    return fieldDescription;        
  }
  
  
  
  
  private String getFieldNamePLZ()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_PLZ3");
    else return text;
  }

  private String getFieldNameOrt()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Ort3");
    else return text;
  }


  
  private String getFieldDescriptionPLZ()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_PLZ3_ToolTip");
    else return text;
  }
  
  private String getFieldDescriptionOrt()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Ort3_ToolTip");
    else return text;
  }
  
  
  
  
  public void setFieldName(String name)
  {
    fieldName = name;
    if (panel != null)
    {
      if (name != null) 
      {
        m_oLabel_plz.setText(getFieldNamePLZ());
        m_oLabel_ort.setText(getFieldNameOrt());
      }
    }
  }  
  

  public void setFieldDescription(String description)
  {
    fieldDescription = description;
    if (panel != null)
    {
      if (description != null) 
      {
        m_oTextfield_plz.setToolTipText(getFieldDescriptionPLZ());
        m_oTextfield_ort.setToolTipText(getFieldDescriptionOrt());
      }
    }
  }  
  

  
  
  
  private EntryLayoutHelper createPlzPanel(String widgetFlags)
  {    
    m_oTextfield_plz = createTextField(getFieldDescriptionPLZ(), 10);
    m_oTextfield_plz.addFocusListener(new FocusListener() 
    {
      String text;

      public void focusGained(FocusEvent fe) 
      {
        text = m_oTextfield_plz.getText();
        getGUIListener().getLogger().info(Messages.getString("GUI_MainFrameNewStyle_Standard_PLZ_alt") + text); //$NON-NLS-1$
      }

      public void focusLost(FocusEvent fe) 
      {
        getGUIListener().getLogger().info(Messages.getString("GUI_MainFrameNewStyle_Standard_PLZ_neu") + m_oTextfield_plz.getText()); //$NON-NLS-1$
        if (!(m_oTextfield_plz.getText().equals(text))) 
          try 
          {
            // Autovervollst�ndigung des Bundesland-Feldes    
            String newBundesLand = getGUIListener().getBundeslandForPLZ(m_oTextfield_plz.getText());
            getGUIListener().getLogger().info(Messages.getString("GUI_MainFrameNewStyle_Standard_PLZ_BuLa_neu") + newBundesLand); //$NON-NLS-1$
            if (newBundesLand != null && newBundesLand.length() > 0) 
            {
              getGUIListener().getLogger().info(Messages.getString("GUI_MainFrameNewStyle_Standard_PLZ_BuLa_neu_Idx") + getGUIListener().getIndexOfBundesland(newBundesLand)); //$NON-NLS-1$
              
              if ( (getWidgetPool().isWidgetRegistered("COMBO_BUNDESLAND3")) && (getWidgetPool().isWidgetRegistered("COMBO_LAND3")))
              {  
                JComboBox m_oCombobox_bundesland = (JComboBox)(getWidgetPool().getWidget("COMBO_BUNDESLAND3"));
                JComboBox m_oCombobox_land = (JComboBox)(getWidgetPool().getWidget("COMBO_LAND3"));
                m_oCombobox_bundesland.setSelectedIndex(getGUIListener().getIndexOfBundesland(newBundesLand));
                int indexDeutschland = getGUIListener().getIndexOfLand(Messages.getString("GUI_MainFrameNewStyle_Standard_Deutschland")); //$NON-NLS-1$
                if (indexDeutschland > 0 && m_oCombobox_land.getSelectedIndex() == 0) m_oCombobox_land.setSelectedIndex(indexDeutschland);

	              // Autovervollst�ndigung des ORT-Feldes    
	              if ((m_oTextfield_ort.getText().trim().length() == 0) || (m_bAlwaysOverwriteOrt))
	              {
	                try
	                {
	                  String ort = getGUIListener().getCityForPLZ(m_oTextfield_plz.getText());
	                  //System.out.println("ermittelter Ort = " + ort);
	                  if (ort != null)
	                  {
	                    m_oTextfield_ort.setText(ort);
	                  }
	                }
	                catch(NumberFormatException nfe) { }
	                catch(Exception e) { e.printStackTrace(); }
	              }
	            }
	          } 
          }
	        catch(NumberFormatException nfe) {}
	        catch(Exception e) { e.printStackTrace(); }
        }
    });    
    m_oLabel_ort = new TarentWidgetLabel(getFieldNameOrt() + " ", SwingConstants.RIGHT); //$NON-NLS-1$
    m_oTextfield_ort = createTextField(getFieldDescriptionOrt(), 60);
    m_oLabel_plz = new TarentWidgetLabel(getFieldNamePLZ());
    
    return new EntryLayoutHelper( new TarentWidgetInterface[] { m_oLabel_plz, m_oTextfield_plz, m_oLabel_ort, m_oTextfield_ort}, widgetFlags);
  } 

  public void setDoubleCheckSensitive(boolean issensitive) {
		setDoubleCheckSensitive(m_oTextfield_ort, issensitive);
		setDoubleCheckSensitive(m_oTextfield_plz, issensitive);	
	} 
  
  
}
