/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


/**
 * @author niko
 */
public class TarentWidgetTextField extends JTextField implements TarentWidgetInterface
{
  private boolean           m_iIsLengthRestricted;
  
  private Color tempColor;
  

  public TarentWidgetTextField()
  {
    super();
    tempColor = getBackground();
    this.getDocument().addDocumentListener(new DocumentChangedListener());
  }

  public TarentWidgetTextField(String text)
  {
    super(text);
    this.getDocument().addDocumentListener(new DocumentChangedListener());
  }


  public TarentWidgetTextField(int maxlen)
  {
    super();
    setLengthRestriction(maxlen);
    this.getDocument().addDocumentListener(new DocumentChangedListener());
  }

  public TarentWidgetTextField(String text, int maxlen)
  {
    super(text);
    setLengthRestriction(maxlen);
    this.getDocument().addDocumentListener(new DocumentChangedListener());
  }

  private class DocumentChangedListener implements DocumentListener
  {
    private Object m_oValue;
    
    public DocumentChangedListener()
    {
      m_oValue = getData();
    }
    
    private void checkChanges()
    {
      if (!(getData().equals(m_oValue))) fireValueChanged(getData());
      m_oValue = getData();
    }

    public void changedUpdate(DocumentEvent e)
    {
      checkChanges();
    }

    public void insertUpdate(DocumentEvent e)
    {
      checkChanges();
    }

    public void removeUpdate(DocumentEvent e)
    {
      checkChanges();
    }
  }
  
  
	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    if (data instanceof String)
    {
      this.setText((String)data);
    }
    else
    {
      if (data == null) this.setText("");
      else this.setText(data.toString());
    }
	}

	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
		return this.getText();
	}


  public boolean isEqual(Object data)
  {
    return(this.getText().equals(data.toString()));
  }


  public void setLengthRestriction(int maxlen)
  {
    if (maxlen > 0)
    {
      m_iIsLengthRestricted = true;
      String text = this.getText();
      this.setDocument(new SizeRestrictedDocument(maxlen));
      this.setText(text);
    }
    else
    {
      m_iIsLengthRestricted = false;
      String text = this.getText();
      this.setDocument(new PlainDocument());
      this.setText(text);
    }
  }

  public boolean isLengthRestricted()
  {
    return(m_iIsLengthRestricted); 
  }

  public JComponent getComponent()
  {
    return(this);
  }

  
  public void setWidgetEditable(boolean iseditable)
  {
	// Sets the background color to white when disabled.
	setEditable(iseditable);
    setBackground((iseditable ? tempColor : Color.WHITE));
  }

  
  public class SizeRestrictedDocument extends PlainDocument
  {
    private int m_iMaxChars;
    
    public SizeRestrictedDocument(int maxchars)
    {
      m_iMaxChars = maxchars;
    }
    
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
    {
      if (str == null) return;        
      int insertsize = str.length();
      int docsize = this.getLength();
      if ((insertsize+docsize) < m_iMaxChars) 
      {
        super.insertString(offs, str, a);
      }
      else
      {
        int maxinsert = m_iMaxChars - docsize;
        if (maxinsert > 0)
        {            
          super.insertString(offs, str.substring(0, maxinsert), a);
        }
      }        
    }
  }

  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  private void fireValueChanged(Object newvalue)
  {
    Iterator it = m_oWidgetChangeListeners.iterator();
    while(it.hasNext())
    {
      TarentWidgetChangeListener listener = (TarentWidgetChangeListener)(it.next());
      listener.changedValue(newvalue);
    }
  }
  

}
