/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElementInterfaces;

import java.awt.Dimension;
import java.awt.Point;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import org.evolvis.nuyo.controls.TarentWidgetChangeListener;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObject;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface GUIElement extends DataContainerObject
{
  public GUIElement cloneGUIElement();
  public void init();
  public void initElement();
  public void dispose();  
  public boolean canDisplay(Class data);
  public Class displays();
  public TarentWidgetInterface getComponent();
  public Dimension getNeededSize();  
  public void setData(Object data);
  public Object getData();    
  public boolean addParameter(String key, String value);
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor();

  public void setDataMap(Map map);
  public Map getDataMap();
  
  public void setFocus();
  
  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();
  
  public Point getOffsetForError(ErrorHint errorhint);
  
  public void addWidgetChangeListener(TarentWidgetChangeListener listener);
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener);
  
  public List getEventsConsumable();
  public List getEventsFireable();  
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);      
  public void fireTarentGUIEvent(TarentGUIEvent e);
  
  public ImageIcon loadIcon(String filename);
  
}
