/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class BankNameField extends GenericTextField
{
  public BankNameField()
  {
    super("BANKNAME", AddressKeys.BANKNAME, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_BankName_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_BankName", 60);
  }
}
