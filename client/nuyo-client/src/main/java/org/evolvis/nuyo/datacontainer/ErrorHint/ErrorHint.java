/*
 * Created on 14.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorHint;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface ErrorHint
{
  public String getName();
  public String getDescription();
  public Exception getException();
  public DataContainer getDataContainer();
  
  public void setName(String name);
  public void setDescription(String description);
  public void setException(Exception e);  
  public void setDataContainer(DataContainer datacontainer);
}
