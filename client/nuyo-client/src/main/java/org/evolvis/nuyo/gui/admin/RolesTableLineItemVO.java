package org.evolvis.nuyo.gui.admin;

import org.evolvis.nuyo.db.Role;

/** 
 * A wrapper object (or value object VO) that manages 
 * all values of a single table line.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
interface RolesTableLineItemVO {
    public Role getRole();
    public Object getValueAtColumn(int index);
    public void setValueAtColumn(Object o, int index);
}