/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class AbteilungField extends GenericTextField
{
  public AbteilungField()
  {
    super("ABTEILUNG", AddressKeys.ABTEILUNG, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_Abteilung_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_Abteilung", 99);
  }
}
