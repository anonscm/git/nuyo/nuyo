/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.CommonDialogButtons;
import de.tarent.commons.ui.EscapeDialog;
import de.tarent.commons.ui.swing.ComboBoxMouseWheelNavigator;
import de.tarent.commons.utils.SystemInfo;

/**
 * 
 * A simple Dialog allowing the user to configure her E-Mail-Preferences
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class EMailConfigurationDialog extends EscapeDialog
{
	private JPanel mainPanel;
	private JRadioButton internalMailButton;
	private JRadioButton externalMailButton;
	private JComboBox externalMailComboBox;
	private JTextField externalMailCommandField;
	private JButton externalMailPathBrowseButton;
	private ActionListener actionListener;
	private SortedMap mailClients;
	private String tempSelected;
	private JLabel externalMailCommandLabel;
	private CommonDialogButtons buttonPanel;
	
	public EMailConfigurationDialog(Frame pParent, String[][] pMailClients, String pCommand)
	{
		super(pParent);
		setTitle(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_TITLE"));
		mailClients = setupMailClientList(pMailClients, pCommand);
		getContentPane().add(getMainPanel());
		/* currently not possible
		if(pCommand.equals("internal"))
		{
			getInternalMailButton().setSelected(true);
			getExternalMailComboBox().setSelectedIndex(0);
		}
		else
		{*/
			getExternalMailButton().setSelected(true);
			getExternalMailComboBox().setSelectedItem(tempSelected);
		//}
		updateComponentStates();
	}
	
	private SortedMap setupMailClientList(String[][] pMailClients, String pCommand)
	{
		SortedMap map = new TreeMap();
		for(int i=0; i < pMailClients.length; i++)
		{
			if(SystemInfo.isWindowsSystem())
			{
				if(pMailClients[i][2].indexOf("windows") != -1)
					map.put(pMailClients[i][0], pMailClients[i][1]);
			}
			else if(SystemInfo.isLinuxSystem())
			{
				if(pMailClients[i][2].indexOf("unix") != -1)
					map.put(pMailClients[i][0], pMailClients[i][1]);
			}
			if(pMailClients[i][1].equals(pCommand))
				tempSelected = pMailClients[i][0];
		}
		
		if(tempSelected == null && !pCommand.equals("internal"))
		{ 
			tempSelected = Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_CUSTOM_ENTRY");
			map.put(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_CUSTOM_ENTRY"), pCommand);
		}
		else
			map.put(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_CUSTOM_ENTRY"), "");
		return map;
	}
	
	public boolean isInternalMailSelected()
	{
		return getInternalMailButton().isSelected();
	}
	
	public String getSelectedMailCommand()
	{
		return getExternalMailCommandField().getText();
	}
	
	private JPanel getMainPanel()
	{
		if(mainPanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref, 5dlu, pref:grow, 5dlu, pref", // columns
			"pref, 5dlu, 0dlu, 0dlu, pref, 5dlu, pref, 10dlu, pref, 5dlu, pref, 15dlu:grow, pref"); // rows
			
			PanelBuilder builder = new PanelBuilder(layout);

			builder.setDefaultDialogBorder();

			CellConstraints cc = new CellConstraints();
			
			JLabel descriptionLabel = new JLabel(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_DESCRIPTION"));
			descriptionLabel.setFont(descriptionLabel.getFont().deriveFont(Font.PLAIN));
			
			JLabel commandDescLabel = new JLabel(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_COMMAND_DESC"));
			commandDescLabel.setFont(commandDescLabel.getFont().deriveFont(Font.PLAIN));
			commandDescLabel.setFont(commandDescLabel.getFont().deriveFont(Font.ITALIC));
			
			builder.add(descriptionLabel, cc.xyw(1, 1, 5));
			// disabled because email-dialog is not capable of non-persistent-addresses
			// builder.add(getInternalMailButton(), cc.xyw(1, 3, 4));
			//builder.add(getExternalMailButton(), cc.xyw(1, 5, 6));
			builder.add(getExternalMailComboBox(), cc.xyw(1, 7, 5));
			builder.add(commandDescLabel, cc.xyw(1, 9, 3));
			builder.add(getExternalMailCommandLabel(), cc.xy(1, 11));
			builder.add(getExternalMailCommandField(), cc.xy(3, 11));
			builder.add(getExternalMailPathBrowseButton(), cc.xy(5, 11));
			
			builder.add(buttonPanel = CommonDialogButtons.getSubmitCancelButtons(getActionListener()), cc.xyw(1, 13, 5));
			
			ButtonGroup buttonGroup = new ButtonGroup();
			buttonGroup.add(getInternalMailButton());
			buttonGroup.add(getExternalMailButton());
			
			mainPanel = builder.getPanel();
		}
		return mainPanel;
	}
	
	private JRadioButton getInternalMailButton()
	{
		if(internalMailButton == null)
		{
			internalMailButton = new JRadioButton(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_INTERNAL_OPTION"));
			internalMailButton.setSelected(true);
			internalMailButton.addActionListener(getActionListener());
		}
		return internalMailButton;
	}
	
	private JRadioButton getExternalMailButton()
	{
		if(externalMailButton == null)
		{
			externalMailButton = new JRadioButton(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_EXTERNAL_OPTION"));
			externalMailButton.addActionListener(getActionListener());
			// currently internal mail is not active
			externalMailButton.setVisible(false);
		}
		return externalMailButton;
	}
	
	public JComboBox getExternalMailComboBox()
	{
		if(externalMailComboBox == null)
		{
			externalMailComboBox = new JComboBox(mailClients.keySet().toArray());
			externalMailComboBox.addMouseWheelListener(new ComboBoxMouseWheelNavigator(externalMailComboBox));
			externalMailComboBox.addItemListener(new EMailConfigurationDialogItemListener());
		}
		return externalMailComboBox;
	}
	
	public JTextField getExternalMailCommandField()
	{
		if(externalMailCommandField == null)
		{
			externalMailCommandField = new JTextField((String)mailClients.get(getExternalMailComboBox().getSelectedItem()));
			externalMailCommandField.getDocument().addDocumentListener(new DocumentListener() {

				public void changedUpdate(DocumentEvent e) {
					checkValidity();
				}

				public void insertUpdate(DocumentEvent e) {
					changedUpdate(e);
					
				}

				public void removeUpdate(DocumentEvent e) {	
					changedUpdate(e);
				}
			});
		}
		return externalMailCommandField;
	}

	protected void checkValidity() {
//		FIXME
//		if(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_CUSTOM_ENTRY").equals(getExternalMailComboBox().getSelectedItem().toString()) &&
//				!new File(getExternalMailCommandField().getText()).isFile())
//			buttonPanel.setSubmitButtonEnabled(false);
//		else
//			buttonPanel.setSubmitButtonEnabled(true);
	}

	protected JLabel getExternalMailCommandLabel()
	{
		if(externalMailCommandLabel == null)
		{
			externalMailCommandLabel = new JLabel(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_EXTERNAL_MAIL_COMMAND"));
			externalMailCommandLabel.setLabelFor(getExternalMailCommandField());
		}
		return externalMailCommandLabel;
	}
	
	public JButton getExternalMailPathBrowseButton()
	{
		if(externalMailPathBrowseButton == null)
		{
			externalMailPathBrowseButton = new JButton(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_BROWSE_BUTTON_TEXT"));
			externalMailPathBrowseButton.setToolTipText(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_BROWSE_BUTTON_TOOLTIP"));
			externalMailPathBrowseButton.addActionListener(getActionListener());
		}
		return externalMailPathBrowseButton;
	}
	
	private ActionListener getActionListener()
	{
		if(actionListener == null)
		{
			actionListener = new EMailConfigurationDialogActionListener();
		}
		return actionListener;
	}
	
	private void updateComponentStates()
	{
		getExternalMailComboBox().setEnabled(getExternalMailButton().isSelected());
		if(!getExternalMailButton().isSelected())
		{
			getExternalMailCommandField().setEnabled(false);
			getExternalMailPathBrowseButton().setEnabled(false);
		}
		else
			updateExternalMailCommandState();
	}
	
	private void updateExternalMailCommandState()
	{	
		// set correct path
		if(mailClients != null && getExternalMailComboBox().getSelectedItem() != null && mailClients.get(getExternalMailComboBox().getSelectedItem()) != null)
			getExternalMailCommandField().setText(mailClients.get(getExternalMailComboBox().getSelectedItem()).toString());
		
		// set correct state
		if(getExternalMailComboBox().getSelectedItem() != null)
		{
			getExternalMailCommandField().setEnabled(getExternalMailComboBox().getSelectedItem().equals(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_CUSTOM_ENTRY")));
			getExternalMailPathBrowseButton().setEnabled(getExternalMailComboBox().getSelectedItem().equals(Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_CUSTOM_ENTRY")));
		}		
	}
	
	private void showMailPathDialog()
	{
		// TODO move this somehow to tarent-commons
		
		JFileChooser fileChooser = new JFileChooser();
		
		if(SystemInfo.isWindowsSystem())
		{
			fileChooser.setFileFilter(new FileFilter() {

				public boolean accept(File f)
				{
					return f.getName().endsWith(".exe");
				}

				public String getDescription()
				{
					return Messages.getString("GUI_EMAIL_CONFIGURE_DIALOG_EXECUTABLE_FILE_FILTER_DESCRIPTION");
				}
			});
		}
		
		int result = fileChooser.showOpenDialog(this);
		if(result == JFileChooser.APPROVE_OPTION)
			getExternalMailCommandField().setText(fileChooser.getSelectedFile().getAbsolutePath());
	}
	
	public void closeWindow()
	{
		super.closeWindow();
	}

	public void saveAndClose()
	{
		if(isInternalMailSelected())
			ConfigManager.getPreferences().put(Key.EMAIL_COMMAND.toString(), "internal");
		else
			ConfigManager.getPreferences().put(Key.EMAIL_COMMAND.toString(), getSelectedMailCommand());
		closeWindow();
	}

	private class EMailConfigurationDialogActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent pEvent)
		{
			if(pEvent.getSource().equals(getInternalMailButton()) || pEvent.getSource().equals(getExternalMailButton()))
				updateComponentStates();
			else if(pEvent.getSource().equals(getExternalMailPathBrowseButton()))
				showMailPathDialog();
			else if(pEvent.getActionCommand().equals("submit"))
				saveAndClose();
			else if(pEvent.getActionCommand().equals("cancel"))
				closeWindow();
		}
	}
	
	private class EMailConfigurationDialogItemListener implements ItemListener
	{
		public void itemStateChanged(ItemEvent pEvent)
		{
			updateExternalMailCommandState();
			EMailConfigurationDialog.this.checkValidity();
		}
	}
}