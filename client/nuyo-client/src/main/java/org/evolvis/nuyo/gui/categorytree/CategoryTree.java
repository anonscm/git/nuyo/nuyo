package org.evolvis.nuyo.gui.categorytree;

import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.OverlayLayout;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;

/**
 * This class allows creating and configuring the category
 * tree UI element which is used for category selection and
 * category assignment tasks.
 * 
 * <p>The <code>CategoryTree</code> is a complex component
 * containing multiple subcomponents. Whether one of them is
 * visible is decided upon the use of the tree itself.</p>
 * 
 * <p>Currently there are the following subcomponents:
 * <ul>
 * <li>a visibility panel that allows chosing the displayed tree nodes
 * and provides a textfield for subcategory search.</li>
 * <li>an operation panel where one can switch the boolean
 * operation used to compute the final search expression.</li>
 * <li>an info panel which can show an informative text
 * and provides a submit and cancel button.</li>
 * </ul>
 * </p>
 * 
 * @author Robert Schuster
 *
 */
public class CategoryTree extends JComponent
{
	private static Logger logger = Logger.getLogger(CategoryTree.class.getName());

	CategoryTreeDataModel model;

	private JTree tree;

	private JRadioButton andOperation;

	private JRadioButton orOperation;

	private JTextField findField;

	private JToggleButton viewAll;

	private JToggleButton viewSelected;

	private JToggleButton viewUnselected;

	private Behavior behavior;

	private CategoryDataUpdater dataUpdater;

	private CategoryTree(JTree tree, LayoutManager l, CategoryTreeDataModel m)
	{
		this.tree = tree;
		model = m;
		setLayout(l);
	}

	/** Retrieves a snapshot of the category tree state.
	 * 
	 * <p>This methods makes only sense when the tree is used in selection
	 * mode.</p>
	 * 
	 * @return
	 */ 
	public SelectionState getState()
	{
		return SelectionState.create(model.getSubCategories(Selection.POSITIVE),
				model.getSubCategories(Selection.NEGATIVE),
				model.getOperation(),
				model.getVisibility(),
				model.getFilter());
	}

	/**
	 * Initializes the category tree with the given state.
	 * 
	 * @param s
	 */
	public void setState(SelectionState s)
	{
		model.setOperation(s.op);

		// Reset any active selections first, otherwise the following operations
		// will have the wrong effect. Think of the selection state as being relative
		// and without a proper base state results are unpredictable.
		model.resetSelection();

		Iterator ite = s.positives.iterator();
		while (ite.hasNext())
		{
			model.setSelection((Integer) ite.next(), Selection.POSITIVE);
		}

		ite = s.negatives.iterator();
		while (ite.hasNext())
		{
			model.setSelection((Integer) ite.next(), Selection.NEGATIVE);
		}

		model.setFilter(s.filter);
		model.setVisibility(s.vis);

		// TODO: Quite hacky: Sets UI components in accordance
		// the value in the model ...
		if (s.op == Operation.AND)
			andOperation.setSelected(true);
		else if (s.op == Operation.OR)
			orOperation.setSelected(true);



		prepareModel(tree, model);

		tree.repaint();
	}

	private static void fillDataModel(CategoryTreeDataModel dm, List visibleCategories)
	{
		Iterator ite = visibleCategories.iterator();
		while (ite.hasNext())
		{
			Category cat = (Category) ite.next();
			dm.add(cat);
		}

		// if no virtual root category has been found in the category list
		// add the virtual root of the global categorys list 

		if (dm.getRootCategoryNode() == null){
			try {
				dm.add(ApplicationServices.getInstance().getCurrentDatabase().getVirtualRootCategory());
			} catch (ContactDBException e) {
				logger.warning("Something went wrong during creation of a categorytree. Root node could not be set.");
			}
		}
	}

	private static void prepareModel(final JTree t, CategoryTreeDataModel dm)
	{
		CategoryTreeDataModel.Consumer consumer = new CategoryTreeDataModel.Consumer()
		{
			public void repaint()
			{
				t.repaint();
			}
		};

		t.setModel(dm.createSnapshot(consumer));

	}

	private static JTree createTree(CategoryTreeDataModel dm)
	{
		final JTree t = new JTree();
		t.setShowsRootHandles(true);
		t.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		t.setEditable(true);

		t.setCellRenderer(dm.getRenderer());
		t.setCellEditor(dm.getEditor());
		t.addTreeExpansionListener(dm.getExpansionListener());
//		t.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
//			
//			public void valueChanged(TreeSelectionEvent e) {
//				CategoryNode node = (CategoryNode)t.getLastSelectedPathComponent();
//				Selection sel = node.nextSelection();
//				((CategoryNode)t.getModel().getRoot()).childSelectionChanged(sel);
//			}
//		});
		// needed to make tree resizable to minimum size
		t.setVisibleRowCount(1);

		return t;
	}

	private JComponent createVisibilityPanel(CategoryTreeDataModel dm)
	{
		// Use a darker background color for the head area.
		Color bg = UIManager.getDefaults().getColor("controlShadow");

		viewAll = new JToggleButton(Visibility.ALL.getLabel(), true);
		viewAll.setToolTipText(Visibility.ALL.getLabelTooltip());
		viewAll.addActionListener(new ViewSwitcher(tree, dm, Visibility.ALL));
		viewAll.setBackground(bg);
		viewAll.setMnemonic(Visibility.ALL.getLabelMnemonic());


		viewSelected = new JToggleButton(Visibility.SELECTED.getLabel());
		viewSelected.setToolTipText(Visibility.SELECTED.getLabelTooltip());
		viewSelected.addActionListener(new ViewSwitcher(tree, dm, Visibility.SELECTED));
		viewSelected.setBackground(bg);
		viewSelected.setMnemonic(Visibility.SELECTED.getLabelMnemonic());

		viewUnselected = new JToggleButton(Visibility.UNSELECTED.getLabel());
		viewUnselected.setToolTipText(Visibility.UNSELECTED.getLabelTooltip());
		viewUnselected.addActionListener(new ViewSwitcher(tree, dm, Visibility.UNSELECTED));
		viewUnselected.setBackground(bg);
		viewUnselected.setMnemonic(Visibility.UNSELECTED.getLabelMnemonic());

		ButtonGroup group = new ButtonGroup();
		group.add(viewAll);
		group.add(viewSelected);
		group.add(viewUnselected);

		// Constructs a small panel which provides a small icon (on a JLabel)
		// besides a textfield acting as its description.
		ActionListener viewSwitcher = new ViewSwitcher(tree, dm);
		JPanel findPanel = new JPanel();
		findPanel.setBackground(bg);
		findField = new JTextField();
		//findField.getDocument().addDocumentListener(new FilterUpdater(dm));
		findField.addActionListener(viewSwitcher);

		JLabel findIcon = new JLabel((ImageIcon)SwingIconFactory.getInstance().getIcon(SwingIconFactory.CATEGORY_TREE_FIND));
		findIcon.setBackground(bg);

		findPanel.setLayout(new BoxLayout(findPanel, BoxLayout.X_AXIS));
		findPanel.add(findIcon);
		findPanel.add(findField);

		JButton filterButton = new JButton(Messages.getString("GUI_CategoryTree_Filter"));
		filterButton.setToolTipText(Messages.getString("GUI_CategoryTree_Filter_ToolTip"));
		filterButton.addActionListener(viewSwitcher);

		FormLayout l = new FormLayout(
				"3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu:grow", // Columns.
				"3dlu, pref, 3dlu, pref, 3dlu" // Rows.
		);

		CellConstraints cc = new CellConstraints(); 

		JPanel visibilities = new JPanel(l);
		visibilities.setBorder(new LineBorder(Color.BLACK, 1));
		visibilities.setBackground(bg);

		visibilities.add(viewAll, cc.xy(2, 2));
		visibilities.add(viewSelected, cc.xy(4, 2));
		visibilities.add(viewUnselected, cc.xy(6, 2));

		visibilities.add(findPanel, cc.xyw(2, 4, 3));
		visibilities.add(filterButton, cc.xy(6, 4));

		return visibilities;
	}

	private JComponent createOperationPanel(CategoryTreeDataModel dm)
	{
		andOperation = new JRadioButton(Operation.AND.getLabel(), true);
		andOperation.setToolTipText(Operation.AND.getLabelToolTip());
		andOperation.addActionListener(new OperationSwitcher(dm, Operation.AND));

		orOperation = new JRadioButton(Operation.OR.getLabel());
		orOperation.setToolTipText(Operation.OR.getLabelToolTip());
		orOperation.addActionListener(new OperationSwitcher(dm, Operation.OR));

		ButtonGroup group = new ButtonGroup();
		group.add(orOperation);
		group.add(andOperation);

		JPanel operation = new JPanel();
		operation.setLayout(new BoxLayout(operation, BoxLayout.X_AXIS));
		operation.setAlignmentX(JComponent.LEFT_ALIGNMENT);

		operation.add(orOperation);
		operation.add(andOperation);

		return operation;
	}

	private JPanel createButtons(final Behavior b, ActionListener cancelListener, ActionListener submitListener)
	{
		final JButton submit = new JButton(b.getSubmitText())
		{
			public void addNotify()
			{
				super.addNotify();

				// Makes this a default button without knowing
				// the actual surrounding frame.
				getRootPane().setDefaultButton(this);
			}
		};

		if (submitListener != null)
			submit.addActionListener(submitListener);

		// Installs a selection listener which disables the submit button
		// if all nodes are set to neutral. This prevents the user from
		// submitting operations without effect. While this is just nice for
		// most tree uses in case of address creation proper initial
		// (sub-) categories are a neccessary.
		model.setSelectionListener(new CategoryTreeDataModel.SelectionListener()
		{
			public void selectionChanged(Selection rootSelection)
			{
				submit.setEnabled(rootSelection != Selection.NEUTRAL);
			}
		});

		if(b.getSubmitIconName() != null)
			submit.setIcon((ImageIcon)SwingIconFactory.getInstance().getIcon(b.getSubmitIconName()));

		JButton cancel = new JButton(Messages.getString("GUI_CategoryTree_Cancel"));
		cancel.setIcon((ImageIcon)SwingIconFactory.getInstance().getIcon("process-stop.png"));
		if (cancelListener != null)
			cancel.addActionListener(cancelListener);

		PanelBuilder pb = new PanelBuilder(new FormLayout("pref, 3dlu:grow, pref", "pref"));
		CellConstraints cc = new CellConstraints();
		pb.add(submit, cc.xy(1, 1));
		pb.add(cancel, cc.xy(3, 1));

		return pb.getPanel();
	}

	/**
	 * Constructs a category tree UI component which is suitable for
	 * selecting subcategories.
	 * 
	 * <p>Under all circumstances reuse a selection category tree instead
	 * of creating a new because each instance registers a binding.</p>
	 * 
	 * @return
	 */
	public static CategoryTree createSelectionCategoryTree() {
		return CategoryTree.createSelectionCategoryTree(
				ApplicationServices.getInstance().getActionManager().getCategoriesObjectList());
	}
	
	/**
	 * Constructs a category tree UI component which is suitable for
	 * selecting subcategories.
	 * 
	 * <p>Under all circumstances reuse a selection category tree instead
	 * of creating a new because each instance registers a binding.</p>
	 * 
	 * @return
	 */
	public static CategoryTree createSelectionCategoryTree(List categoriesObjectList)
	{
		JComponent temp;
		CategoryTreeDataModel dm = new CategoryTreeDataModel(Behavior.SELECT);
		JTree tree = createTree(dm);

		FormLayout l = new FormLayout(
				"fill:pref:grow",  // Columns
				//"pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:pref:grow" // Rows.
				"0dlu, 0dlu, 0dlu, 0dlu, pref, 3dlu, pref, 1dlu, fill:pref:grow"
		);

		CategoryTree root = new CategoryTree(tree, l, dm);
		// A selection category tree is meant to be reused instead of created
		// over and over again. Therefore it uses an updater to refresh itself
		// when category data changes.
		root.dataUpdater = new CategoryDataUpdater(tree, dm, categoriesObjectList);

		CellConstraints cc = new CellConstraints();
		//root.add(root.createVisibilityPanel(dm), cc.xy(1, 1));
		//root.add(temp = new JLabel(Messages.getString("GUI_CategoryTree_Select")), cc.xy(1, 3));

		//temp.setAlignmentX(JComponent.LEFT_ALIGNMENT);

		root.add(root.createOperationPanel(dm), cc.xy(1, 5));

		//JToggleButton showFilterOptions = new JToggleButton("Zeige Filter-Optionen");
		//showFilterOptions.setIcon(IconFactory.getInstance().getIcon("collapsed.gif"));

		FilterPanel filterPanel = new FilterPanel(new FilterUpdater(dm, tree, Visibility.ALL), new Visibility[] { Visibility.ALL, Visibility.SELECTED, Visibility.UNSELECTED }, tree, dm);

		filterPanel.setVisible(false);

		root.add(filterPanel, cc.xy(1, 7));


		JScrollPane pane = new JScrollPane(tree);

		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setLayout(new OverlayLayout(layeredPane));
		
		JPanel overlayPanel = new JPanel(new FormLayout("0dlu:grow, pref, 10dlu", // columns
				"pref" )); // rows
		overlayPanel.setOpaque(false);
		
		JToggleButton filterOptions = new JToggleButton(Messages.getString("GUI_CategoryTree_ShowFilterButtonText"));
		filterOptions.setToolTipText(Messages.getString("GUI_CategoryTree_ShowFilterButtonToolTip"));
		filterOptions.setFont(filterOptions.getFont().deriveFont(Font.PLAIN));
		filterOptions.setBackground(Color.WHITE);
		filterOptions.setHorizontalAlignment(SwingConstants.RIGHT);
		filterOptions.setOpaque(false);
		filterOptions.addActionListener(root.new VisibilityToggler(filterPanel, filterOptions));
		
		overlayPanel.add(filterOptions, cc.xy(2, 1));

		layeredPane.add(pane, JLayeredPane.DEFAULT_LAYER);
		layeredPane.add(overlayPanel, JLayeredPane.MODAL_LAYER);

		root.add(layeredPane, cc.xy(1, 9));

		return root;
	}

	private class VisibilityToggler implements ActionListener
	{
		JComponent comp;
		JToggleButton checkBox;

		public VisibilityToggler(JComponent comp, JToggleButton checkBox) {
			this.comp = comp;
			this.checkBox = checkBox;
		}

		public void actionPerformed(ActionEvent e) {
			comp.setVisible(this.checkBox.isSelected());
		}

	}

	private static CategoryTree createSetOperationCategoryTree(final Behavior b, int affectedAddressesCount,
			List visibleCategories,
			List expandedCategories,
			final AssignHandler handler)
	{
		if(visibleCategories.size() == 0)
			visibleCategories = ApplicationServices.getInstance().getActionManager().getCategoriesObjectList();
		final CategoryTreeDataModel dm = new CategoryTreeDataModel(b);

		// A set operation tree is supposed to be recreated each time and therefore
		// does not need an updater.
		fillDataModel(dm, visibleCategories);
		JTree tree = createTree(dm);
		prepareModel(tree, dm);

		// TODO: Seems like the correct solution but not sure whether it really works.
		Iterator ite = dm.getPathsFor(expandedCategories).iterator();
		while (ite.hasNext())
			tree.expandPath((TreePath) ite.next());

		FormLayout l = new FormLayout(
				"3dlu, fill:pref:grow, 3dlu",  // Columns
				"3dlu, pref, 3dlu, fill:pref:grow, 3dlu, pref, 3dlu, pref, 3dlu" // Rows.
		);

		CategoryTree root = new CategoryTree(tree, l, dm);

		root.setBehavior(b);

		CellConstraints cc = new CellConstraints();
		root.add(root.createVisibilityPanel(dm),  cc.xy(2, 2));

		root.add(new JScrollPane(tree), cc.xy(2, 4));

		JLabel label = new JLabel(b.getInfoText(affectedAddressesCount));
		root.add(label, cc.xy(2, 6));

		// Registers listeners for the cancel and submit
		// button which call the handler asynchronously to allow the
		// implementor to do blocking system calls.
		JPanel p = root.createButtons(b, 
				new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				new Thread()
				{
					public void run()
					{
						handler.cancel();
					}
				}.start();

			}
		},
		new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				new Thread()
				{
					public void run()
					{
						handler.submit(dm.getSubCategories(b.getDestinationSelection()),
								dm.getSubCategories(b.getSourceSelection()));
					}
				}.start();
			}
		});
		root.add(p, cc.xy(2, 8));

		return root;
	}

	/** Creates a category tree which is suitable for adding addresses to a number
	 * of subcategories.
	 * 
	 * <p>Provide a {@link AssignHandler} implementation to get informed about the selection
	 * operation taking place (submit) or not (cancell).</p>
	 * 
	 * @param affectedAddressesCount
	 * @param handler
	 * @return
	 */ 
	public static CategoryTree createAddCategoryTree(int affectedAddressesCount, List visibleCategories, List expandedCategories, AssignHandler handler)
	{
		return createSetOperationCategoryTree(Behavior.ADD, affectedAddressesCount, visibleCategories, expandedCategories, handler);
	}

	/** Creates a category tree which is suitable for removing addresses to a number
	 * of subcategories.
	 * 
	 * <p>Provide a {@link AssignHandler} implementation to get informed about the selection
	 * operation taking place (submit) or not (cancell).</p>
	 * 
	 * @param affectedAddressesCount
	 * @param handler
	 * @return
	 */ 
	public static CategoryTree createRemoveCategoryTree(int affectedAddressesCount, List visibleCategories, List expandedCategories, AssignHandler handler)
	{
		return createSetOperationCategoryTree(Behavior.REMOVE, affectedAddressesCount, visibleCategories, expandedCategories, handler);
	}

	/** Creates a category tree which is suitable for moving addresses to a number
	 * of subcategories.
	 * 
	 * <p>Provide a {@link AssignHandler} implementation to get informed about the selection
	 * operation taking place (submit) or not (cancell).</p>
	 * 
	 * @param affectedAddressesCount
	 * @param handler
	 * @return
	 */ 
	public static CategoryTree createMoveCategoryTree(int affectedAddressesCount, List visibleCategories, List expandedCategories, AssignHandler handler)
	{
		return createSetOperationCategoryTree(Behavior.MOVE, affectedAddressesCount, visibleCategories, expandedCategories, handler);
	}

	/** Creates a category tree which is suitable for handling the assignment of a single
	 * address.
	 * 
	 * <p>Provide a {@link AssignHandler} implementation to get informed about the selection
	 * operation taking place (submit) or not (cancell).</p>
	 * 
	 * @param affectedAddressesCount
	 * @param handler
	 * @return
	 */ 
	public static CategoryTree createSingleAssignmentCategoryTree(List visibleCategories, List expandedCategories, AssignHandler handler)
	{
		CategoryTree catTree = createSetOperationCategoryTree(Behavior.SINGLE, 0, visibleCategories, expandedCategories, handler);

		catTree.model.setSelection(expandedCategories, Selection.POSITIVE);

		return catTree;
	}

	/**
	 * An instance of this class can be used to keep the data of a {@link CategoryTree}
	 * in sync with changes done by this user.
	 *
	 */
	private static class CategoryDataUpdater extends AbstractReadOnlyBinding
	{
		JTree tree;
		CategoryTreeDataModel categoryTreeDataModel;
		List categoriesObjectList;
		
		CategoryDataUpdater(JTree tree, CategoryTreeDataModel ctdm) {
			this(tree, ctdm, ApplicationServices.getInstance().getActionManager().getCategoriesObjectList());
		}

		CategoryDataUpdater(JTree tree, CategoryTreeDataModel ctdm, List categoriesObjectList)
		{
			super(ApplicationModel.CATEGORIES_UPDATE_WATCH_KEY);

			this.tree = tree;
			this.categoryTreeDataModel = ctdm;
			this.categoriesObjectList = categoriesObjectList;

			ApplicationServices.getInstance().getBindingManager().addBinding(this);
		}

		public void setViewData(Object data) {
			// Throw away current category data
			categoryTreeDataModel.clear();

			// Recreates the category listing ...
			fillDataModel(categoryTreeDataModel,
					categoriesObjectList);

			// ... and updates the visible representation.
			prepareModel(tree, categoryTreeDataModel);

		}

	}

	private static class ViewSwitcher implements ActionListener
	{
		Visibility vis;
		CategoryTreeDataModel categoryTreeDataModel;
		JTree tree;

		ViewSwitcher(JTree tree, CategoryTreeDataModel dm, Visibility vis)
		{
			this.tree = tree;
			categoryTreeDataModel = dm;
			this.vis = vis; 
		}

		/** Creates a ViewSwitcher that will update the displayed tree
		 * element but leaves the visibility setting untouched.
		 * 
		 * <p>This is used to let the filter setting of the datamodel
		 * come to effect.</p>
		 * 
		 * @param tree
		 * @param dm
		 */
		ViewSwitcher(JTree tree, CategoryTreeDataModel dm)
		{
			this(tree, dm, null);
		}

		public void actionPerformed(ActionEvent ae)
		{
			Iterator ite = categoryTreeDataModel.getExpandedNodes();

			if (vis != null)
				categoryTreeDataModel.setVisibility(vis);

			prepareModel(tree, categoryTreeDataModel);

			while (ite.hasNext())
				tree.expandPath((TreePath) ite.next());
		}
	}

	private static class OperationSwitcher implements ActionListener
	{
		CategoryTreeDataModel catTreeDataModel;
		Operation operation = Operation.AND;

		OperationSwitcher(CategoryTreeDataModel categoryTreeDataModel, Operation operation)
		{
			this.catTreeDataModel = categoryTreeDataModel;
			this.operation = operation;
		}

		public void actionPerformed(ActionEvent ae)
		{
			catTreeDataModel.setOperation(operation); 
		}

	}

	private void setBehavior(Behavior b)
	{
		this.behavior = b;
	}

	public String getTitle()
	{
		return this.behavior.getShortBehaviorDescription();
	}

	private static class FilterUpdater implements DocumentListener
	{
		CategoryTreeDataModel categoryTreeDataModel;
		JTree tree;
		Visibility vis;

		FilterUpdater(CategoryTreeDataModel model, JTree tree, Visibility vis)
		{
			this.categoryTreeDataModel = model; 
			this.tree = tree;
			this.vis = vis; 
		}

		private void update(Document doc)
		{
			try
			{
				categoryTreeDataModel.setFilter(doc.getText(0, doc.getLength()));

				Iterator ite = categoryTreeDataModel.getExpandedNodes();

				if (vis != null)
					categoryTreeDataModel.setVisibility(vis);

				prepareModel(tree, categoryTreeDataModel);

				while (ite.hasNext())
					tree.expandPath((TreePath) ite.next());
			}
			catch (BadLocationException ble)
			{
				System.err.println("bad location exception happened: " + ble.getMessage());
			}
		}

		public void insertUpdate(DocumentEvent de)
		{
			update(de.getDocument());
		}

		public void removeUpdate(DocumentEvent de)
		{
			update(de.getDocument());
		}

		public void changedUpdate(DocumentEvent de)
		{
			update(de.getDocument());
		}

	}

	/**
	 * Handler interface for set operations.
	 * 
	 * <p>For the {@link CategoryTree} to be used for set operations
	 * an instance of this interface must be supplied. The methods get
	 * called according to what the user chose to do.</p>
	 * 
	 * <p>Currently the user can opt to execute the operation or
	 * can cancel it.</p>
	 * 
	 * <p>Both methods are not called on the Swing event dispatch
	 * thread and so are allowed to block.</p>
	 *  
	 */
	public interface AssignHandler
	{
		/**
		 * This methods gets called by the {@link CategoryTree}
		 * when the user opted to invoke the set operation.
		 * 
		 * <p>Depending on the kind of set operation the arguments
		 * are as follows:<ul>
		 * <li>assignment addition - <code>destSubCats</code>
		 * is the list of {@link org.evolvis.nuyo.db.SubCategory}
		 * instances to which the addresses should be added</li>
		 * <li>assignment removal - <code>destSubCats</code> is
		 * the list of {@link org.evolvis.nuyo.db.SubCategory}
		 * instance from which the addresses should be removed</li>
		 * <li>assignment movement - <code>destSubCats</code> are
		 * the {@link org.evolvis.nuyo.db.SubCategory} instances
		 * to which the addreses should be added and <code>sourceSubCats</code>
		 * the ones where the addresses should be removed from.</li>
		 * <li>single assignment - <code>destSubCats</code> are the
		 * the {@link org.evolvis.nuyo.db.SubCategory} instances
		 * to which the address should be added and <code>sourceSubCats</code>
		 * the ones where the address should be removed from.</li>
		 * </ul>
		 * </p>
		 * 
		 * @param destSubCats
		 * @param sourceSubCats
		 */
		void submit(List destSubCats, List sourceSubCats);

		/**
		 * This is called when the user opted to cancel the operation.
		 *
		 */
		void cancel();
	}

	public String toString()
	{
		return tree.toString();
	}

}
