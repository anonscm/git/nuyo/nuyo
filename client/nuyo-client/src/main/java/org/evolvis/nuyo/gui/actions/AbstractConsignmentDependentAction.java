package org.evolvis.nuyo.gui.actions;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.groupware.Address;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.Binding;

/**
 * A convenience implementation of the {@link AbstractBindingRestrictedAction}
 * which disables the action in case the address list is empty or the
 * address is blocked for consignmnent.
 * 
 * <p>An action inheriting from this class can be sure that the action
 * will be disabled when the address list is empty or the address is blocked
 * for consignment.</p>
 * 
 * @author Robert Schuster
 *
 */
public abstract class AbstractConsignmentDependentAction extends AbstractBindingRestrictedAction {
	
	private boolean addressListEmpty;
	
	private boolean blockedForConsignment;
	
	protected AbstractConsignmentDependentAction()
	{
		// Nothing to be done.
	}

	protected final Binding[] initBindings() {
		// A binding that listens on changes to the currently selected address
		// in order to check the edit right. The result of that check combined
		// with whether the action is in the current context determines the
		// action's enabled state.
		return new Binding[] { new AbstractReadOnlyBinding(
				ApplicationModel.ADDRESS_LIST_KEY + ".size") {

			public void setViewData(Object arg) {
				if (arg == null)
				  return;
				
				addressListEmpty = ((Integer) arg).intValue() == 0;
				setBindingEnabled(!blockedForConsignment &&
						          !addressListEmpty);

				updateActionEnabledState();
			}

		},
		new AbstractReadOnlyBinding(
				ApplicationModel.SELECTED_ADDRESS_KEY) {

			public void setViewData(Object arg) {
				if (arg == null)
				  return;
				
				blockedForConsignment = ((Address) arg).getBlockedForConsignment().booleanValue();
				
				setBindingEnabled(!blockedForConsignment &&
						          !addressListEmpty);

				updateActionEnabledState();
			}

		}
		};

	}

}
