package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;


public class NewContactAction extends AbstractGUIAction {

	private static final long	serialVersionUID	= 6967993682089861190L;
    private static final TarentLogger logger = new TarentLogger(NewContactAction.class);
    private GUIListener guiListener;
    private Runnable executor; 

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null) {
            //IMPORTANT: immediate AWT-Thread release
            SwingUtilities.invokeLater(executor);
        } else logger.warning(getClass().getName() + " not initialized");
    }
    
    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        executor = new Runnable(){// define reusable code:
            public void run() {
                guiListener.userRequestNewContact();
            }
        };
    }
}
