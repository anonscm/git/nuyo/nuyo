/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;

/**
 * @author niko
 *
 */
public class WidgetPool
{
  private Map m_oWidgetPool;
  private Map m_oControlerPool;
  
  public WidgetPool()
  {
    m_oWidgetPool = new HashMap();
    m_oControlerPool = new HashMap();
  }
  
  public void addWidget(Object key, JComponent comp)
  {
    m_oWidgetPool.put(key, comp);
  }
  
  public void addController(Object key, Object controler)
  {
    m_oControlerPool.put(key, controler);
  }
  
  public JComponent getWidget(Object key)
  {
    return (JComponent)(m_oWidgetPool.get(key));
  }
  
  public Object getController(Object key)
  {
    return (Object)(m_oControlerPool.get(key));
  }
  
  public boolean isWidgetRegistered(Object key)
  {
    return m_oWidgetPool.containsKey(key);
  }
  
  public boolean isControlerRegistered(Object key)
  {
    return m_oControlerPool.containsKey(key);
  }
  
}
