/*
 * Created on 16.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.util.List;


/**
 * @author niko
 *
 */
public interface LogListFormatter
{
  public String formatList(List strings);
}
