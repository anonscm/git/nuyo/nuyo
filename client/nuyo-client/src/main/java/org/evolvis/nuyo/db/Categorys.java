package org.evolvis.nuyo.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Verwaltungsklasse die Kategorien der Anwendung, die ein User sehen kann.
 * @author Sebastian
 */
public abstract class Categorys {


    protected List categorys = new ArrayList();
    protected Map categoryLookupTable = new HashMap();
    protected Database db;

    public Categorys(Database db) {
        this.db = db;
    }

    public Database getDB() {
        return db;
    }
    
//     public void setAllCategorys(Collection categorys) {
//         if (categorys == null) {
//             categorys = null;
//             categoryLookupTable = null;            
//             return;
//         }
        
//         //this.categorys = new ArrayList(categorys);
//         //categoryLookupTable = new HashMap(categorys.size());
//         //for (Iterator iter = categorys.iterator(); iter.hasNext();) {
//         //    (Categoryiter.next();
//         //}
        
//     }
    
    public List getAllCategorys() {
        return Collections.unmodifiableList(categorys);        
    }

    public Category getCategory(String id) {
        return (Category)categoryLookupTable.get(id);
    }

    public void add(Category cat) throws ContactDBException {
        categorys.add(cat);
        categoryLookupTable.put(cat.getIdAsString(), cat);
    }
    
    public void resetCategoryList()
    {
    	categorys.clear();
    	categoryLookupTable.clear();
    }
    
    public abstract void loadCategorys() throws ContactDBException;
    
    
    public abstract List getAllGrantableCategorys() throws ContactDBException; 
    
    public abstract Category getVirtualRootCategory() throws ContactDBException;
    
    public abstract Category getTrashCategory() throws ContactDBException;
}