package org.evolvis.nuyo.plugins.search;

import org.evolvis.nuyo.db.AddressListParameter;

import de.tarent.commons.datahandling.ListFilterProvider;


/**
 * Common interface to handle address search.
 * 
 * <p>It is expected that the implementor
 * is a complex GUI which provides the user with the neccessary means to select
 * a subset of addresses. The component is usually displayed in a model dialog.
 * </p>
 *
 * <p>After the user has successful prepared the search request the implementation
 * will return it in machine-readable form through the means of the {@link ListFilterProvider}
 * interface.</p>
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 * @author Sebastian Mancke (s.mancke@tarent.de), tarent GmbH Bonn
 */
public interface TarentSearch {
  
    /**
     * Initialization method whose main purpose is it
     * to give an {@link AddressListParameter} instance
     * to the implementor.
     * 
     * <p>This method may be called only once per {@link TarentSearch}
     * inst.</p>
     * 
     * @param o
     */
    public void init(AddressListParameter alp);

    /**
     * Returns, if a search is possible in the current state of the application
     */
    public boolean canSearch();

    /**
     * Blocking Method for showing the search to the user
     */
    public void showSearch();
    
    /**
     * Returns true, if the search criteria has changes, false otherwise.
     * In case of e.g. canceling the search dialog this method would return false.
     */
    public boolean criteriaChanged();
    
    /**
     * Possibilitie to show an additional description for the search.
     */
    public void setSearchInfo(String aSearchInfo);
    
    /**
     * Upon invocation of this method the implementor must
     * update or let update the <code>AddressListParameters</code>
     * instance it got through the {@link #init} method.
     */
    public void updateAddressListParameters();

}
