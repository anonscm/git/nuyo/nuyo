/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataStorageInterface;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueChangedEvent;
import org.evolvis.nuyo.util.general.DataConversionException;
import org.evolvis.nuyo.util.general.DataConverter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class DataStorageAdapter implements DataStorage
{
  private DataContainer m_oDataContainer;
  private boolean m_bIsValid = true;
  private Object m_oDataObject = null; 

  public DataStorageAdapter()
  {
    m_oDataContainer = null;
  }
  
  public abstract void init();
  public abstract void dispose();
  //public abstract boolean canStore(Class data);
  //public abstract Object getData();
  //public abstract void setData(Object data);
  public abstract List getEventsConsumable();
  public abstract List getEventsFireable();
  public abstract Class stores();


  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().addTarentGUIEventListener(event, handler);
  }
    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().removeTarentGUIEventListener(event, handler);
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    getDataContainer().fireTarentGUIEvent(e);
  }

  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }
  
  public void setValid(boolean valid)
  {
    m_bIsValid = valid;
  }

  public boolean isValid()
  {
    return m_bIsValid;
  }
  
  
  public boolean canStore(Class data)
  {
    return data.isInstance(stores());
  }

  public boolean addParameter(String key, String value)
  {
    return false;
  }

  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    return null;
  }
  
  protected void setData(Object data, boolean fireevent)
  {
    if (data != null)
    {
      try
      {
        Object val = DataConverter.convert(data, stores());
        if (val != null) 
        {
          if (!(val.equals(m_oDataObject)))
          {
            m_oDataObject = val; 
            if (fireevent) getDataContainer().fireTarentGUIEvent(new TarentGUIValueChangedEvent(getDataContainer(), m_oDataObject));
          }
        } else System.out.println("DataConverter.convert returned null");
      }
      catch (DataConversionException e)
      {
        e.printStackTrace();
      }
    }
    else
    {
      if (m_oDataObject != null)
      {
        m_oDataObject = null;
        if (fireevent) getDataContainer().fireTarentGUIEvent(new TarentGUIValueChangedEvent(getDataContainer(), m_oDataObject));
      }
    }
  }
  
  public void setData(Object data)
  {
    setData(data, true);
  }
  
  public Object getData()
  {
    return m_oDataObject;
  }
  
}
