package org.evolvis.nuyo.gui.calendar;
import java.awt.Color;


/*
 * Created on 16.07.2003
 *
 */

/**
 * @author niko
 *
 */
public class ScheduleData
{
	public int hourHeight = 20;   // H�he eines Stundeneintrags
	public int dayWidth = 110;    // Breite einses Tages
	public int minHourHeight = 20;   // H�he eines Stundeneintrags
	public int minDayWidth = 110;    // Breite einses Tages
	
	public int dayDistanceToBorder = 5; 			// seitlicher Abstand zum Tagesrand
	
	public int firstDayX = 30;     // Breite der Stunden-Anzeige
	public int firstHourY = 20 * 3;    // H�he der Tagesanzeige
	public int hoursDistanceToLeftBorder = 5;       // linker Rand vor der Stundenbeschriftung
	
	public int numberOfHours = 24;  
	
	public int firstWorkHour = 8;       // erste Arbeitsstunde
	public int numberOfWorkHours = 10;  // Anzahl Arbeitsstunden
	
	public Color gridColor = Color.BLACK;
	public Color workHourColor = new Color(0xA7, 0xB1, 0xCA);
	public Color freeHourColor = new Color(0x97, 0xA1, 0xBA);
	public Color workDayColor = new Color(0xA7, 0xB1, 0xCA);
	public Color freeDayColor = new Color(0x97, 0xA1, 0xBA);
	public Color holidayColor = new Color(0xa7, 0xA1, 0xBA);
	public Color dayFontColor = Color.BLACK;
	public Color hourFontColor = Color.BLACK;
	public Color workBackgroundColor = new Color(0xd8, 0xe3, 0xef); 
	public Color freeBackgroundColor = new Color(0xe8, 0xf3, 0xff);
	public Color backgroundPanelColor = Color.GRAY;
	
	// diese Werte werden vom Programm gesetzt
	public int minPosX = 0; // linker Rand
	public int minPosY = 0; // oberer Rand
	public int maxPosX = 800; // rechter Rand
	public int maxPosY = 600; // unterer Rand
	
	public int minSizeX = 800; // minimale Gr�sse
	public int minSizeY = 600; // maximale Gr�sse
	
	
	
	// minimale H�he eines Eintrags
	public int minHeightOfEntry = 10;      
	
	private ScheduleTypes scheduleTypes;
	
	public int snapGridHeight = 15 * 60;  
	public boolean useGrid = true;  
	
	public ScheduleData()
	{
		scheduleTypes = new ScheduleTypes();
	}
	
	public ScheduleData(ScheduleData sd)
	{
		hourHeight = sd.hourHeight;
		dayWidth = sd.dayWidth;
		firstDayX = sd.firstDayX;
		firstHourY = sd.firstHourY;
		hoursDistanceToLeftBorder = sd.hoursDistanceToLeftBorder;    
		numberOfHours = sd.numberOfHours;    
		firstWorkHour = sd.firstWorkHour;
		numberOfWorkHours = sd.numberOfWorkHours;     
		gridColor = sd.gridColor; 
		workHourColor = sd.workHourColor;
		freeHourColor = sd.freeHourColor; 
		workDayColor = sd.workDayColor; 
		freeDayColor = sd.freeDayColor; 
		holidayColor = sd.holidayColor; 
		dayFontColor = sd.dayFontColor; 
		hourFontColor = sd.hourFontColor; 
		workBackgroundColor = sd.workBackgroundColor;  
		freeBackgroundColor = sd.freeBackgroundColor;
		backgroundPanelColor = sd.backgroundPanelColor;     
		minPosX = sd.minPosX;
		minPosY = sd.minPosY; 
		maxPosX = sd.maxPosX;
		maxPosY = sd.maxPosY;
		minSizeX = sd.minSizeX;
		minSizeY = sd.minSizeY;		
		minHeightOfEntry = sd.minHeightOfEntry;           
		scheduleTypes = sd.scheduleTypes; 
		snapGridHeight = sd.snapGridHeight;
	}
	
	public void setBackgroundColor(Color color)
	{
		backgroundPanelColor = color;
	}
	
	public ScheduleTypes getScheduleTypes()
	{
		return scheduleTypes;
	}
	
	public int getPixelForSecond(int second)
	{
		double pixelpersecond = ((double)(hourHeight * numberOfHours)) / ((double)(24*60*60));
		int pixel = (int)(pixelpersecond * second);
		return pixel;
	}
}
