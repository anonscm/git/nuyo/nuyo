/*
 * Created on 22.02.2005
 *
 */
package org.evolvis.nuyo.security;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;



/**
 * @author simon
 */

public class SecurityManager {

    //Object(Folder) Berechtigungen
    public static final int FOLDERRIGHT_READ = 1;
    public static final int FOLDERRIGHT_EDIT = 2;
    public static final int FOLDERRIGHT_ADD = 3;
    public static final int FOLDERRIGHT_REMOVE = 4;
    public static final int FOLDERRIGHT_STRUCTURE = 5;
    public static final int FOLDERRIGHT_ADDSUB = 6;
    public static final int FOLDERRIGHT_REMOVESUB = 7;
    public static final int FOLDERRIGHT_GRANT = 8;
    
    //Globale Berechtigungen    
    public static final int GLOBALRIGHT_EDITUSER = 8;
    public static final int GLOBALRIGHT_REMOVEUSER = 9;
    public static final int GLOBALRIGHT_EDITFOLDER = 10;
    public static final int GLOBALRIGHT_REMOVEFOLDER = 11;
    public static final int GLOBALRIGHT_EDITSCHEDULE = 12;
    public static final int GLOBALRIGHT_REMOVESCHEDULE = 13;
    public static final int GLOBALRIGHT_DELETE = 14;
    
    private static SecurityManager _myManager;
    private Database _myDataBase;
	
	private static Logger log = Logger.getLogger(SecurityManager.class.getName());
    
    private SecurityManager(Database database) {
    	_myDataBase = database;
    
    }

    
    public static SecurityManager getSecurityManager(Database database) {
    	if (_myManager == null){
    		_myManager = new SecurityManager(database);
    	}
    	return _myManager;
    }

    /**
     * Gibt die Referenz auf die Singleton Instanz wieder frei;
     */
    public void free() {
        _myManager = null;
    }
    
    public boolean userHasRightOnFolder(String userLogin, int folderRight, int folderID) {
        if(userLogin==null)userLogin=_myDataBase.getUser();
        boolean result = false;
        try {
            result = _myDataBase.userHasRightOnFolder(userLogin,folderRight,folderID);
        } catch (ContactDBException e) {
			log.log(Level.WARNING, e.getMessage());
            result = false;
        }
        return result;
        
    }
    
    public boolean userHasRightOnAddress(String userLogin, int folderRight, int addressID) {
        if(userLogin==null)userLogin=_myDataBase.getUser();
        boolean result = false;
        try {
            result = _myDataBase.userHasRightOnAddress(userLogin,folderRight,addressID);
        } catch (ContactDBException e) {
			log.log(Level.WARNING, e.getMessage());
            result = false;
        }
        return result;
        
    }

	public boolean userHasGlobalRight(String userLogin, int globalRight) {
		if(userLogin==null)userLogin=_myDataBase.getUser();
        boolean result = false;
        try {
            result = _myDataBase.userHasGlobalRight(userLogin,globalRight);
        } catch (ContactDBException e) {
			log.log(Level.WARNING, e.getMessage());
            result = false;
        }
        return result;
    }
    

}
