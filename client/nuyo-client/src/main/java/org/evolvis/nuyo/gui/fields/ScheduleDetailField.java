/*
 * Created on 14.04.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ApplicationStarter;
import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentPanel;
import org.evolvis.nuyo.controls.TarentWidgetButton;
import org.evolvis.nuyo.controls.TarentWidgetCheckBox;
import org.evolvis.nuyo.controls.TarentWidgetDateSpinner;
import org.evolvis.nuyo.controls.TarentWidgetHorizontalPanel;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetMutableComponentTable;
import org.evolvis.nuyo.controls.TarentWidgetPanel;
import org.evolvis.nuyo.controls.TarentWidgetSlider;
import org.evolvis.nuyo.controls.TarentWidgetTextArea;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox.IconComboBoxEntry;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentAddressRelation;
import org.evolvis.nuyo.db.AppointmentCalendarRelation;
import org.evolvis.nuyo.db.AppointmentRequest;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.CalendarCollection;
import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.octopus.AddressImpl;
import org.evolvis.nuyo.db.octopus.UserImpl;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.PersistenceManager;
import org.evolvis.nuyo.gui.AddressSelector;
import org.evolvis.nuyo.gui.ExtendedAddressSelector;
import org.evolvis.nuyo.gui.calendar.AddressRequester;
import org.evolvis.nuyo.gui.calendar.ReminderDialog;
import org.evolvis.nuyo.gui.calendar.SEPRegistry;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.calendar.ScheduleEntryPanel;
import org.evolvis.nuyo.gui.calendar.ScheduleType;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.gui.fieldhelper.DataChange;
import org.evolvis.nuyo.gui.fieldhelper.DataChangeList;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.nuyo.util.DateRange;
import org.evolvis.xana.config.XmlUtil;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;

/**
 * @author niko
 *
 */
public class ScheduleDetailField extends ContactAddressField implements CalendarVisibleElement
{  
    private static TarentLogger logger = new TarentLogger(ScheduleDetailField.class);
    
  private boolean m_bShowInviteButton = true;
  
    private boolean m_bShowSavingAppointmentOnApplication = true;
    private boolean m_bSaveInviationOnly = false;
    
    
    private EntryLayoutHelper m_oPanel = null;
    private Appointment m_oAppointment = null;   
    
    private TarentWidgetTextField m_oTextField_Titel;
    private TarentWidgetTextArea m_oTextArea_Beschreibung;
    private TarentWidgetDateSpinner m_oDateSpinner_Beginn;
    private TarentWidgetDateSpinner m_oDateSpinner_Ende;
    private TarentWidgetIconComboBox m_oIconComboBox_Prioritaet;
    private TarentWidgetIconComboBox m_oIconComboBox_Typ;
    private TarentWidgetTextField m_oTextField_Erstellungsdatum;
    
    
    private TarentWidgetLabel m_oLabel_Erstellungsdatum;
    
    
    private TarentWidgetMutableComponentTable.ObjectTableModel m_oTeilnehmerModel;    
    
    private TarentWidgetMutableComponentTable m_oComponentTable_Teilnehmer;
    private JTabbedPane m_oTabbedPane;
    
    private TarentWidgetLabel m_oLabel_Titel;
    private TarentWidgetLabel m_oLabel_Prio;
    
    private TarentWidgetLabel m_oLabel_Ort;
    private TarentWidgetTextField m_oTextField_Ort;
    
    private Font m_oFont;
    private Font m_oEmptyDescriptionFont;
    
    private ImageIcon m_oIconJob;
    private ImageIcon m_oIconAppointment;
    
    private JButton m_oButton_SaveCurrent;
    private JButton m_oButton_DeleteCurrent;
    private JButton m_oButton_RefreshCurrent;
    
    private TarentWidgetLabel m_oLabel_Beginn;
    private TarentWidgetLabel m_oLabel_Ende;
    private TarentWidgetSlider m_oSlider_Completed;
    private TarentWidgetCheckBox m_oCheckBox_Completed;
    private TarentWidgetLabel m_oLabel_CompletedCheck;
    private TarentWidgetLabel m_oLabel_CompletedSlider;
    private TarentWidgetCheckBox m_oCheckBox_Privat;
    private TarentWidgetLabel m_oLabel_Privat;  
    private TarentWidgetLabel m_oLabel_Typ;
    
    private boolean m_bupdateCompletedSlider = true;
    
    private JLabel m_oLabelAnwesenheit;
    
    private TarentWidgetButton m_oButton_InviteAll;
    
    private JPanel reminderPanel;
    private TarentWidgetButton reminderButton;
    private TarentWidgetCheckBox reminderCheckBox;
    private ReminderDialog reminderDialog;
    
    private GridBagConstraints c; 
    
    // ------------------------------------------------------------------------------------  
    
    // ------------------------------------------------------------------------------------
    
    private ImageIcon m_oIcon_AddPerson;
    private ImageIcon m_oIcon_RemovePerson;
    private ImageIcon m_oIcon_InviteExternal;
    private ImageIcon m_oIcon_InviteAll; 
    
    private ImageIcon m_oIcon_Jump;
    private ImageIcon m_oIcon_Disinvite;
    private ImageIcon m_oIcon_SendMail;
    private ImageIcon m_oIcon_Remove;
    
    // ------------------------------------------------------------------------------------  

    public final static int TAB_ALLGEMEIN = 0;
    public final static int TAB_BEMERKUNG = 1;
    public final static int TAB_TEILNEHMER = 2;
    
    //private String[] m_sTabText = {"Allgemein", "Beschreibung" ,"Teilnehmer", "Wiederholung", "Erinnerung"};
    private String[] m_sTabText = {"Allgemein", "Notiz & Erinnerung" ,"Teilnehmer"};
    
    //  public final static Object TYPE_PHONECALL = "TYPE_PHONECALL";  
    //  public final static Object TYPE_EMAIL = "TYPE_EMAIL";  
    //  public final static Object TYPE_MEETING = "TYPE_MEETING";  
    //  public final static Object TYPE_HOLYDAY = "TYPE_HOLYDAY";  
    //  public final static Object TYPE_BIRTHDAY = "TYPE_BIRTHDAY";
    
    
    //public final static Object DISPLAY_USED = "DISPLAY_USED";
    //public final static Object DISPLAY_FREE = "DISPLAY_FREE";
    
    
    // ------------------------------------------------------------------------------------  
    
    private ButtonGroup m_oButtonGroup;
    private JRadioButton m_oRadioButton_Repeat_None;
    private JRadioButton m_oRadioButton_Repeat_Dayly;
    private JRadioButton m_oRadioButton_Repeat_Weekly;
    private JRadioButton m_oRadioButton_Repeat_Monthly;    
    private JComboBox m_oComboBox_Repeat_Select;
    private JComboBox m_oComboBox_Repeat_Unit;
    private JComboBox m_oComboBox_Repeat_Months;
    private JSpinner m_oDateSpinner_Repeat_Start;
    private JSpinner m_oDateSpinner_Repeat_End;
    private JLabel m_oLabel_Repeat_Start; 
    private JLabel m_oLabel_Repeat_End;
    private JLabel m_oLabel_Repeat_All;
    private JLabel m_oLabel_Repeat_Months;
    
    
    // ------------------------------------------------------------------------------------  
    
    private ImageIcon m_oIcon_SystemUser;
    private ImageIcon m_oIcon_ExternalUser;
    
    private TarentWidgetIconComboBox m_oIconComboBox_Anwesenheit;    
    
    private JPanel m_oPanelTeilnehmerheadline;
    
    private boolean m_bIAmInvitor;
    private int m_iInvitatorUserId;
    
    public String getFieldName()
    {
        return "Termindetails";
    }
    
    public String getFieldDescription()
    {
        return "Ein Feld zur Anzeige von Termindetails";
    }
    
    public int getContext()
    {
        return CONTEXT_NONE;
    }  
    
    
    
    public EntryLayoutHelper getPanel(String widgetFlags)
    {
        m_oFont = new Font(null, Font.PLAIN, 12);
        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIcon_SystemUser = (ImageIcon)iconFactory.getIcon("sysuser.gif");
        m_oIcon_ExternalUser = (ImageIcon)iconFactory.getIcon("extuser.gif");
        
        getWidgetPool().addController("CONTROLER_SCHEDULEDETAIL", this);
        
        getAppointmentDisplayManager().addAppointmentDisplayListener(this);
        
        if(m_oPanel == null) m_oPanel = createPanel(widgetFlags);
        return m_oPanel;
    }
    
    
    private void setPanelActive(boolean isactive)
    {
        m_oButton_SaveCurrent.setEnabled(isactive);
        // m_oButton_ResetCurrent.setEnabled(isactive);
        m_oButton_DeleteCurrent.setEnabled(isactive);
        m_oButton_RefreshCurrent.setEnabled(isactive);
        
        m_oTextField_Titel.setEnabled(isactive);
        m_oTextArea_Beschreibung.setEnabled(isactive);
        //reminderButton.setEnabled(isactive);
        
        m_oDateSpinner_Beginn.setEnabled(isactive);
        m_oDateSpinner_Ende.setEnabled(isactive);
        m_oIconComboBox_Prioritaet.setEnabled(isactive);
        m_oIconComboBox_Typ.setEnabled(isactive);
        //m_oIconComboBox_Anzeige.setEnabled(isactive);
        m_oComponentTable_Teilnehmer.setEnabled(isactive);
        m_oTabbedPane.setEnabled(isactive);
        m_oTextField_Ort.setEnabled(isactive);    
        m_oCheckBox_Completed.setEnabled(isactive);
        m_oSlider_Completed.setEnabled(isactive);
        
        m_oIconComboBox_Typ.setVisible(isactive);
        m_oLabel_Typ.setVisible(isactive);
        m_oDateSpinner_Beginn.setVisible(isactive);
        m_oLabel_Beginn.setVisible(isactive);
        m_oCheckBox_Completed.setVisible(isactive);
        m_oSlider_Completed.setVisible(isactive);
        m_oLabel_CompletedCheck.setVisible(isactive);
        m_oLabel_CompletedSlider.setVisible(isactive);
        //m_oIconComboBox_Anzeige.setVisible(isactive);
        //m_oLabel_Anzeige.setVisible(isactive);          
        
        m_oLabel_Titel.setVisible(isactive);
        m_oLabel_Ort.setVisible(isactive);
        m_oTextField_Titel.setVisible(isactive);
        m_oTextField_Ort.setVisible(isactive);
        
        m_oLabel_Ende.setVisible(isactive);
        m_oLabel_Privat.setVisible(isactive);
        m_oLabel_Prio.setVisible(isactive);
        m_oLabel_Erstellungsdatum.setVisible(isactive);    
        m_oDateSpinner_Ende.setVisible(isactive);
        m_oCheckBox_Privat.setVisible(isactive);
        m_oIconComboBox_Prioritaet.setVisible(isactive);
        m_oTextField_Erstellungsdatum.setVisible(isactive);    
        
    }
    
    
    private final static Object CHANGEREQUESTERMODE_SETAPPOINTMENT = "CHANGEREQUESTERMODE_STANDARD"; 
    private final static Object CHANGEREQUESTERMODE_RELOAD = "CHANGEREQUESTERMODE_RELOAD"; 
    private final static Object CHANGEREQUESTERMODE_INVITE = "CHANGEREQUESTERMODE_INVITE"; 
    
    private boolean openChangeRequester(DataChangeList dirtyfields, boolean istemp, Object mode)
    {
        if (m_oAppointment != null){
            try
            {
                String newsubject = m_oTextField_Titel.getText().trim();
                boolean isjob = m_oAppointment.isJob();
                String subject = m_oAppointment.getAttribute(Appointment.KEY_SUBJECT).trim();
                String name = " ";
                if (istemp)
                {
                    name = " \"" + newsubject + "\" ";              
                }
                else
                {    
                    if (newsubject.equals(subject))
                    {
                        name = " \"" + subject + "\" ";
                    }
                    else
                    {
                        name = " \"" + newsubject + "\" (vorher \"" + subject + "\") ";             
                    }
                }
                
                String datestring = "[" + new ScheduleDate(m_oAppointment.getStart()).getDateTimeString() + " - " + new ScheduleDate(m_oAppointment.getEnd()).getDateTimeString() + "] ";
                
                String changes = "";
                if ((dirtyfields.containsChanges()) && (!istemp))
                {  
                    String changesintro = "\n\n\n" + ((dirtyfields.getNumberOfChanges() == 1) ? "�nderung:\n\n" : "�nderungen:\n\n");
                    changes = changesintro + dirtyfields.getDataChangesString("\u2022 ", "\n") + "\n";
                }
                
                String question = (isjob ? "Die Aufgabe" : "Der Termin") + name + "\n" + datestring + "\nwurde " + (istemp ? "neu erstellt" : "ver�ndert") + " aber noch nicht gespeichert." + changes;
                String savetext = "";      
                String killtext = "";      
                String prequestion = "";
                
                if (CHANGEREQUESTERMODE_RELOAD.equals(mode)) 
                {  
                    savetext = "�berschreiben";      
                    killtext = "Abbrechen";      
                    prequestion = "Das Neuladen des Termins fhrt zum �berschreiben von bereits get�tigten �nderungen.\n\n";
                }
                else if (CHANGEREQUESTERMODE_SETAPPOINTMENT.equals(mode))
                {  
                    savetext = "speichern";      
                    killtext = istemp ? "l�schen" : "zur�cksetzen";      
                    prequestion = "";
                }
                
                else if (CHANGEREQUESTERMODE_INVITE.equals(mode))
                {  
                    savetext = "speichern und einladen";      
                    killtext = "Abbrechen";      
                    prequestion = "Das Einladen von Teilnehmern erfordert das vorherige Abspeichern des Termins.\n\n";
                }
                
                int result = ApplicationServices.getInstance().getCommonDialogServices().askUser(prequestion + question, new String[] {savetext, killtext}, 0);
                //int result = getGUIListener().askUser(prequestion +  question, new String[] {savetext, killtext}, 0);
            
                if (CHANGEREQUESTERMODE_RELOAD.equals(mode))  
                {  
                    if (result == 0)
                    {
                        reloadAppointment();
                    }
                    else
                    {
                        // do nothing...
                    }
                }
                else if (CHANGEREQUESTERMODE_SETAPPOINTMENT.equals(mode))
                {  
                    if (result == 0)
                    {
                        saveAppointment(true); 
                    }
                    else
                    {
                        if (istemp)
                        {
                            deleteAppointment();
                        }
                        else
                        {
                            resetAppointment();
                        }
                    }
                }
                else if (CHANGEREQUESTERMODE_INVITE.equals(mode))
                {  
                    if (result == 0)
                    {
                        saveAppointment(true);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (ContactDBException e)
            {
                logger.severe("Fehler beim �ffnen des ChangeRequesters", e);          
            }
        }
        return true;
    }
    
    
    /** private boolean isInvitator(Appointment appointment)
     {
     if (appointment != null)
     {  
     User me = getGUIListener().getUser(getGUIListener().getCurrentUser());
     if (me != null)
     {
     try
     {
     AppointmentRequest request = appointment.getRelation(me.getCalendar(true));
     if (request != null)
     {
     if (request.getRequestType() == AppointmentRequest.TYPE_INVITATION)
     {
     return true;              
     }
     }        
     } catch (ContactDBException e)
     {
     // TODO Auto-generated catch block
      logger.severe();
      }
      }
      }
      return false;
      }  
      */
    
    private boolean isInvitator(Appointment appointment)
    {
        if (appointment != null)
        {  
            try {
                
                // Test if current User is Owner 
                if (appointment.getOwner().equals(getGUIListener().getUser(null))){
                    m_iInvitatorUserId = appointment.getOwner().getId();
                    return true;
                }
                
                // Ok, maybe we have some Releases with write Access for this appointment
                if (getGUIListener().getCalendarCollection().getCalendar(appointment.getOwner().getCalendar(false).getId()) != null)    {
                    // Must be selected!
                    CalendarSecretaryRelation calsec = appointment.getOwner().getCalendar(false).getCalendarSecretaryforUser(getGUIListener().getUser(null).getId());
                    if (calsec != null ){
                        if ( calsec.getWriteDateRange().containsDate(appointment.getStart()) && 
                                calsec.getWriteDateRange().containsDate(appointment.getEnd())){
                            m_iInvitatorUserId = appointment.getOwner().getId();
                            return true;
                        }
                    }
                }
                
            } catch (ContactDBException e) {
                logger.severe("Contact DB Exception", e);
            }
        }
        // Ok, we lost
        m_iInvitatorUserId = -1;
        return false;
    }  
    
    private void setAppointmentEditable(boolean editable)
    {
        m_oButton_SaveCurrent.setEnabled(editable);
        //m_oButton_ResetCurrent.setEnabled(editable);
        m_oButton_DeleteCurrent.setEnabled(editable);
        m_oTextField_Titel.setEditable(editable);
        m_oTextArea_Beschreibung.setEditable(editable);
        m_oDateSpinner_Beginn.setEnabled(editable);
        m_oDateSpinner_Ende.setEnabled(editable);
        m_oIconComboBox_Prioritaet.setEnabled(editable);
        m_oIconComboBox_Typ.setEnabled(editable);
        //m_oIconComboBox_Anzeige.setEnabled(editable);
        m_oComponentTable_Teilnehmer.setEnabled(editable);
        
        //m_oTabbedPane.setEnabledAt(2, editable);
        //m_oTabbedPane.setEnabledAt(3, editable);
        //m_oTabbedPane.setEnabledAt(4, editable);
        m_oTextField_Ort.setEditable(editable);    
        m_oCheckBox_Completed.setEnabled(editable);
        m_oCheckBox_Privat.setEnabled(editable);
        m_oSlider_Completed.setEnabled(editable);
        m_oTextField_Erstellungsdatum.setEditable(editable);    
    }
    
    private void setAppointmentAcceptInvitationOnly(boolean acceptOnly){
        
        if (acceptOnly) m_oButton_SaveCurrent.setEnabled(true);
        m_bSaveInviationOnly = acceptOnly;
        
    }
    
    
    private void setAppointment(Appointment appointment)
    {
        
        Calendar cal = null;
        boolean readonly = false;
        
        // testen ob dieses Appointment neu ist...
        boolean isnew = false;
        if (m_oAppointment != null)
        {            
            if (appointment != null)
            {
                if (m_oAppointment.getId() != appointment.getId())
                {
                    isnew = true;
                }
            }
            else isnew = true;
        }
        else
        {
            if (appointment == null) isnew = true;
        }
        
        // testen ob gespeichert werden mu�
        if (m_oAppointment != null)
        {            
            if (appointment != null)
            {
                boolean istemp = m_oAppointment.isTemporary();
                // Appointment wurde gewechselt...        
                if ((m_oAppointment.getId() != appointment.getId()) || (istemp))
                { 
                    // Disable Savebuttons for Appointments originating from a released Calender and not in WriteRange ...
                    try {
                        CalendarSecretaryRelation calsec = m_oAppointment.getOwner().getCalendar(false).getCalendarSecretaryforUser(getGUIListener().getUser(null).getId());
                        if (calsec != null ){
                            readonly= !( calsec.getWriteDateRange().containsDate(appointment.getStart()) && 
                                         calsec.getWriteDateRange().containsDate(appointment.getEnd()));
                                
                        }
                    } catch (ContactDBException e) {
                        
                        logger.severe("failed disabling save buttons", e);
                    }
                    if (! readonly){
                        DataChangeList dirtyfields = getDirtyFields(m_oAppointment);                    
                        if ((dirtyfields.containsChanges()) || (istemp))  
                        {
                            openChangeRequester(dirtyfields, istemp, CHANGEREQUESTERMODE_SETAPPOINTMENT);
                        }
                    }
                    
                } // ende: ist anderes Appointment
            } // ende: if appointment != null
        } // ende: if m_oAppointment != null
        
        // insert data...      
        m_oAppointment = appointment;
        
                
        if (m_oAppointment != null )
        {        
            m_bIAmInvitor = isInvitator(m_oAppointment); // Invitor or write access 
            
            getGUIListener().setWaiting(true);            
            try
            {
                //TODO: Rechte umsetzen...
                
                //if ((m_oAppointment.isJob()) && ((m_oTabbedPane.getSelectedIndex() == 2) || (m_oTabbedPane.getSelectedIndex() == 2)))
                //{
                m_oTabbedPane.setSelectedIndex(0);
                //}
                
                setPanelActive(true);
                
                // je nach Typ brauchen wir unterschiedliche Eingabeelemente
                if (m_oAppointment.isJob())
                {
                    m_oIconComboBox_Typ.setVisible(false);
                    m_oLabel_Typ.setVisible(false);
                    m_oDateSpinner_Beginn.setVisible(false);
                    m_oLabel_Beginn.setVisible(false);
                    m_oCheckBox_Completed.setVisible(true);
                    m_oSlider_Completed.setVisible(true);
                    m_oLabel_CompletedCheck.setVisible(true);
                    m_oLabel_CompletedSlider.setVisible(true);
                    m_oLabel_Ende.setText("F�lligkeit:");              
                    
                    //m_oIconComboBox_Anzeige.setVisible(false);
                    //m_oLabel_Anzeige.setVisible(false);          
                    
                    m_oTabbedPane.setEnabledAt(2, false);
                    // m_oTabbedPane.setEnabledAt(3, false);
                    
                    m_oTabbedPane.setIconAt(0, m_oIconJob);          
                }
                else
                {
                    m_oIconComboBox_Typ.setVisible(true);
                    m_oLabel_Typ.setVisible(true);
                    m_oDateSpinner_Beginn.setVisible(true);
                    m_oLabel_Beginn.setVisible(true);
                    m_oCheckBox_Completed.setVisible(false);
                    m_oSlider_Completed.setVisible(false);
                    m_oLabel_CompletedCheck.setVisible(false);
                    m_oLabel_CompletedSlider.setVisible(false);
                    m_oLabel_Ende.setText("Ende:");    
                    
                    //m_oIconComboBox_Anzeige.setVisible(true);
                    //m_oLabel_Anzeige.setVisible(true);          
                    
                    m_oTabbedPane.setEnabledAt(2, true);
                    // m_oTabbedPane.setEnabledAt(3, true);
                    
                    m_oTabbedPane.setIconAt(0, m_oIconAppointment);          
                }
                
                // Add more Participiants ...
                m_oPanelTeilnehmerheadline.setVisible(m_bIAmInvitor); 
                
                // Endable/Disable GUI
                setAppointmentEditable(m_bIAmInvitor);
                
                // Additionally show the save button to allow accept an invitation?
                setAppointmentAcceptInvitationOnly(!m_bIAmInvitor);
                
                Collection calColl = m_oAppointment.getCalendars();
                for (Iterator iter = calColl.iterator(); iter.hasNext();){
                    cal = (Calendar) iter.next();
                    if (getGUIListener().getCalendarCollection().getCollection().contains(cal))
                        continue;
                }
                
                
                //m_oTextField_Titel.requestFocus();
                m_oCheckBox_Privat.setSelected(m_oAppointment.isPrivat());
                m_oTextField_Titel.setText(getString(m_oAppointment.getAttribute(Appointment.KEY_SUBJECT)));        
                m_oTextField_Ort.setText(getString(m_oAppointment.getAttribute(Appointment.KEY_LOCATION)));
                String beschreibung = getString(m_oAppointment.getAttribute(Appointment.KEY_DESCRIPTION));
                m_oTextArea_Beschreibung.setText(beschreibung);
                
                m_bDateChangedListenersActive = false; // um Kreis-Events zu vermeiden...     
                m_oDateSpinner_Beginn.setValue(m_oAppointment.getStart());
                m_oDateSpinner_Ende.setValue(m_oAppointment.getEnd());
                m_bDateChangedListenersActive = true;        
                
                //System.out.println("appointment.getJobCompletion()=" + appointment.getJobCompletion() + " appointment.isJobCompleted()=" + appointment.isJobCompleted());        
                
                m_bupdateCompletedSlider = false;// um Kreis-Events zu vermeiden...
                m_oSlider_Completed.setData(new Integer(m_oAppointment.getJobCompletion()));              
                m_oCheckBox_Completed.setData(new Boolean(m_oAppointment.isJobCompleted()));
                m_bupdateCompletedSlider = true;
                m_oCheckBox_Privat.setData(new Boolean(m_oAppointment.isPrivat()));
                
                int priority = m_oAppointment.getPriority();
                if (!m_oIconComboBox_Prioritaet.setSelectedItemByKey(new Integer(priority)))
                {
                    m_oIconComboBox_Prioritaet.setSelectedIndex(0);
                    getGUIListener().getLogger().warning("Priority des Appointment-Objekts war ungltig... habe Vorgabewert gesetzt.");          
                }
                
                int category = m_oAppointment.getAppointmentCategory();
                if (!m_oIconComboBox_Typ.setSelectedItemByKey(new Integer(category)))
                {
                    m_oIconComboBox_Typ.setSelectedIndex(0);
                    getGUIListener().getLogger().warning("AppointmentCategory des Appointment-Objekts war ungltig... habe Vorgabewert (erster Eintrag in Liste) gesetzt.");          
                }
                
                int display = m_oAppointment.getRelation(cal).getDisplayMode();
                /*
                 if (!m_oIconComboBox_Anzeige.setSelectedItemByKey(new Integer(display)))          
                {
                    m_oIconComboBox_Anzeige.setSelectedIndex(0);
                    getGUIListener().getLogger().warning("DisplayMode des AppointmentCalendarRelation-Objekts war ungltig... habe Vorgabewert (erster Eintrag in Liste) gesetzt.");
                }
                */
                
                Date erstellungsdatum = m_oAppointment.getRelation(cal).getCreation();
                if (erstellungsdatum != null)
                {  
                    m_oTextField_Erstellungsdatum.setForeground(Color.BLACK);
                    m_oTextField_Erstellungsdatum.setText((new ScheduleDate(erstellungsdatum)).getDateTimeString());
                }
                else
                {
                    m_oTextField_Erstellungsdatum.setForeground(Color.RED);
                    m_oTextField_Erstellungsdatum.setText("nicht ermittelbar");     
                    getGUIListener().getLogger().warning("Es war nicht m�glich das Erstellungsdatum des Termins zu ermitteln.");                    
                }
                
                // Tabellen leeren...
                m_oComponentTable_Teilnehmer.removeAllData();
                    // ------------------ ab hier werden die Teilnehmer in die Liste eingefgt        
                int numteilnehmer = 0;
                Collection addresses = getAllAddressesOfAppointment(m_oAppointment);
                if (addresses != null)
                {  
                    Iterator addressiterator = addresses.iterator();
                    while(addressiterator.hasNext())
                    {
                        Address address = (Address)(addressiterator.next());
                        insertTeilnehmerEntry(address);
                        numteilnehmer ++;
                    }
                }
                setNumTeilnehmer(numteilnehmer);
                
                m_oComponentTable_Teilnehmer.getJTable().revalidate();
                
                // ------------------ bis hier wurden die Teilnehmer in die Liste eingefgt
                
                
                
                // ------------------ Refresh des Erinnerung-Tabs
                setReminderStatus();
                showReminderPanel();
                
                // wenn der Termin neu ist und der Tab auf "Allgemein" steht
                // soll das Titel-Feld aktiviert und selektiert werden...
                if ((isnew) && (m_oTabbedPane.getSelectedIndex() == 0))
                {
                    if (m_bIAmInvitor)
                    {  
                        m_oTextField_Titel.requestFocus();
                        m_oTextField_Titel.selectAll();
                    }
                }
                
            } 
            catch (ContactDBException e)
            {
                logger.severe("Fehler beim Lesen eines Appointments.", e);
            }
            finally
            {
                getGUIListener().setWaiting(false);
            }      
        }
        else
        {
            // disable panel...
            setPanelActive(false);      
            m_oTextField_Titel.setText("");
            m_oTextField_Ort.setText("");
            m_oTextArea_Beschreibung.setText("");
            
            // temporary disable listeners
            m_bDateChangedListenersActive = false;
            
            m_oDateSpinner_Beginn.setValue(new Date());
            m_oDateSpinner_Ende.setValue(new Date());
            
            // re-enable listeners          
            m_bDateChangedListenersActive = true;
            
            m_oIconComboBox_Prioritaet.setSelectedIndex(0);
            m_oIconComboBox_Typ.setSelectedIndex(0);
            //m_oIconComboBox_Anzeige.setSelectedIndex(0);
            m_oComponentTable_Teilnehmer.removeAllData();
            m_oTabbedPane.setSelectedIndex(0);      
        }
    }
    
    private void setNumTeilnehmer(int numteilnehmer)
    {
        String tablabel = m_sTabText[TAB_TEILNEHMER] + " (" + numteilnehmer + ")";
        m_oTabbedPane.setTitleAt(TAB_TEILNEHMER, tablabel);
    }
    
    
    
    // schreibt die GUI-Felder ins Appointment-Objekt...  
    public boolean getAppointment(Appointment appointment)
    {
        boolean mustrefreshhistory = false;
        if (appointment != null){
            getGUIListener().setWaiting(true);            
            try
            {
                /*
                 Calendar cal = null;
                 Collection calColl = appointment.getCalendars();
                 for (Iterator iter = calColl.iterator(); iter.hasNext();){
                 cal = (Calendar) iter.next();
                 if (getGUIListener().getCalendarCollection().getCollection().contains(cal))
                 continue;
                 }*/
                
                // Determine the source Calendar for the Appointment (current user or invitator's calendar)
                // Is the current user participiant of app?
                Calendar cal = null;
                //cal = getGUIListener().getUser(null).getCalendar(true);
                //if ( ! appointment.getCalendars().contains(cal))
                // Invitators calendar
                cal = appointment.getOwner().getCalendar(false);
                
                // No CalendarCollection needed here
                
                
                appointment.setAttribute(Appointment.KEY_SUBJECT, m_oTextField_Titel.getText());
                appointment.setAttribute(Appointment.KEY_LOCATION, m_oTextField_Ort.getText());                
                appointment.setAttribute(Appointment.KEY_DESCRIPTION, m_oTextArea_Beschreibung.getText());
                
                if(appointment.isJob()){
                    appointment.setStart(new ScheduleDate((Date)(m_oDateSpinner_Ende.getValue())).getDateWithAddedHours(-1).getDate());
                }else{
                    appointment.setStart((Date)(m_oDateSpinner_Beginn.getValue()));
                }
                
                appointment.setEnd((Date)(m_oDateSpinner_Ende.getValue()));
                
                //System.out.println("appointment.setJobCompletion(" + m_oSlider_Completed.getData() + ")");        
                
                appointment.setJobCompletion(((Integer)(m_oSlider_Completed.getData())).intValue());
                
                //appointment.setJobCompleted(((Integer)(m_oSlider_Completed.getData())).intValue() == 100);
                appointment.setJobCompleted(m_oCheckBox_Completed.isSelected());
                
                appointment.setPriority( ((Integer)(((TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Prioritaet.getSelectedItem())).getKey())).intValue() );
                
                appointment.setPrivate(m_oCheckBox_Privat.isSelected());
                
                TarentWidgetIconComboBox.IconComboBoxEntry comboentry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Typ.getSelectedItem());
                if (comboentry != null)
                {  
                    Integer value = (((Integer)(comboentry.getKey())));
                    if (value != null)
                    {
                        appointment.setAppointmentCategory(value.intValue());
                    }
                }
                
                //m_oIconComboBox_Anzeige.getSelectedIndex();
                
                /*
                tarentWidgetIconComboBox.IconComboBoxEntry displaycomboentry = (tarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Anzeige.getSelectedItem());
                
                if (displaycomboentry != null)
                { 
                    Integer value = (((Integer)(displaycomboentry.getKey())));
                    if (value != null)
                    {
                        appointment.getRelation(cal).setDisplayMode(value.intValue());
                    }
                }
                */
                
                // Teilnehmerliste mit der DB syncronisieren
                mustrefreshhistory = syncTeilnehmerList(appointment);                  
                AppointmentCalendarRelation acr = appointment.getRelation(cal);
                
                
                if (acr != null)
                    PersistenceManager.commit(acr);
                //      ENDE Simon Appointment speich0rn
            }
            catch(ContactDBException cde)
            {
                logger.severe("Fehler beim Speichern eines Appointments.", cde);        
            }
            finally
            {
                if (m_bShowSavingAppointmentOnApplication) getGUIListener().setWaiting(false);
            }
        }
        return mustrefreshhistory;
    }
    
    
    
    
    public void updateAppointment(Appointment appointment)
    {
        if (appointment != null)
        {        
            getGUIListener().setWaiting(true);
            try
            {
                Calendar cal = null;
                Collection calColl = appointment.getCalendars();
                for (Iterator iter = calColl.iterator(); iter.hasNext();){
                    cal = (Calendar) iter.next();
                    if (getGUIListener().getCalendarCollection().getCollection().contains(cal))
                        continue;
                }
                String app_subject = getString(appointment.getAttribute(Appointment.KEY_SUBJECT));
                String gui_subject = m_oTextField_Titel.getText();        
                if (!(app_subject.equals(gui_subject))) 
                {
                    //System.out.println("updateAppointment() updating field m_oTextField_Titel");
                    m_oTextField_Titel.setText(app_subject);
                }
                
                String app_location = getString(appointment.getAttribute(Appointment.KEY_LOCATION));
                String gui_location = m_oTextField_Ort.getText();                
                if (!(app_location.equals(gui_location))) 
                {
                    //System.out.println("updateAppointment() updating field m_oTextField_Ort");
                    m_oTextField_Ort.setText(app_location);
                }
                
                String app_description = getString(appointment.getAttribute(Appointment.KEY_DESCRIPTION));
                String gui_description = m_oTextArea_Beschreibung.getText();
                if (!(app_description.equals(gui_description))) 
                {
                    //System.out.println("updateAppointment() updating field m_oTextArea_Beschreibung");
                    m_oTextArea_Beschreibung.setText(app_description);
                }
                
                Date app_start = appointment.getStart();
                Date gui_start = (Date)(m_oDateSpinner_Beginn.getValue());
                if (!(app_start.equals(gui_start))) 
                {
                    //System.out.println("updateAppointment() updating field m_oDateSpinner_Beginn");
                    m_bDateChangedListenersActive = false;
                    m_oDateSpinner_Beginn.setData(app_start);
                    m_bDateChangedListenersActive = true;
                }
                
                Date app_end = appointment.getEnd();
                Date gui_end = (Date)(m_oDateSpinner_Ende.getValue());
                if (!(app_end.equals(gui_end))) 
                {
                    //System.out.println("updateAppointment() updating field m_oDateSpinner_Ende");
                    m_bDateChangedListenersActive = false;
                    m_oDateSpinner_Ende.setData(app_end);
                    m_bDateChangedListenersActive = true;
                }
                
                int app_jobcompletion = appointment.getJobCompletion();
                int gui_jobcompletion = ((Integer)(m_oSlider_Completed.getData())).intValue();
                if (app_jobcompletion != gui_jobcompletion) 
                {          
                    //System.out.println("updateAppointment() updating field m_oSlider_Completed");
                    m_oSlider_Completed.setData(new Integer(app_jobcompletion));
                    updateCompletedCheck();          
                }
                
                boolean app_jobcompleted = appointment.isJobCompleted();
                boolean gui_jobcompleted = m_oCheckBox_Completed.isSelected();
                if (app_jobcompleted != gui_jobcompleted) 
                {
                    //System.out.println("updateAppointment() updating field m_oCheckBox_Completed");
                    m_oCheckBox_Completed.setSelected(app_jobcompleted);
                    updateCompletedSlider();          
                }
                
                int app_priority = appointment.getPriority();
                int gui_priority = ((Integer)(((TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Prioritaet.getSelectedItem())).getKey())).intValue();
                if (app_priority != gui_priority)
                {          
                    //System.out.println("updateAppointment() updating field m_oIconComboBox_Prioritaet");
                    if (!m_oIconComboBox_Prioritaet.setSelectedItemByKey(new Integer(app_priority)))
                    {
                        m_oIconComboBox_Prioritaet.setSelectedIndex(0);
                        getGUIListener().getLogger().warning("Priority des Appointment-Objekts war ungltig... habe Vorgabewert gesetzt.");          
                    }
                }        
                
                int app_category = appointment.getAppointmentCategory();
                int gui_category = -1;
                TarentWidgetIconComboBox.IconComboBoxEntry comboentry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Typ.getSelectedItem());
                if (comboentry != null)
                {  
                    Integer value = (((Integer)(comboentry.getKey())));
                    if (value != null)
                    {
                        gui_category = value.intValue();            
                    }
                }
                if (app_category != gui_category)
                {          
                    //System.out.println("updateAppointment() updating field m_oIconComboBox_Typ");
                    if (!m_oIconComboBox_Typ.setSelectedItemByKey(new Integer(app_category)))
                    {
                        m_oIconComboBox_Typ.setSelectedIndex(0);
                        getGUIListener().getLogger().warning("Category des Appointment-Objekts war ungltig... habe Vorgabewert gesetzt.");          
                    }
                }        
                
                setReminderStatus();
                showReminderPanel();
                
                /*
                int app_displaymode = appointment.getRelation(cal).getDisplayMode(); 
                int gui_displaymode = -1;  
                tarentWidgetIconComboBox.IconComboBoxEntry displaycomboentry = (tarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Anzeige.getSelectedItem());
                if (displaycomboentry != null)
                {  
                    Integer value = (((Integer)(displaycomboentry.getKey())));
                    if (value != null)
                    {
                        gui_displaymode = value.intValue();
                    }
                }
                if (app_displaymode != gui_displaymode)
                {          
                    //System.out.println("updateAppointment() updating field m_oIconComboBox_Anzeige");
                    if (!m_oIconComboBox_Anzeige.setSelectedItemByKey(new Integer(app_displaymode)))
                    {
                        m_oIconComboBox_Anzeige.setSelectedIndex(0);
                        getGUIListener().getLogger().warning("Category des Appointment-Objekts war ungltig... habe Vorgabewert gesetzt.");          
                    }
                }        
                */
                
                // TODO: Teilnehmerliste mit der DB vergleichen...
            }
            catch(ContactDBException cde)
            {
                logger.severe("Fehler beim Updaten eines Appointments.", cde);        
            }
            finally
            {
                if (m_bShowSavingAppointmentOnApplication) getGUIListener().setWaiting(false);
            }
        }
        
    }
    
    
    private boolean isAddressConnectedToAppointment(Appointment appointment, Address address)
    {    
        Collection addresses = getAllAddressesOfAppointment(appointment);
        if (addresses != null)
        {
            Iterator iterator = addresses.iterator();
            while(iterator.hasNext())
                {    
                    if (((Address)(iterator.next())).getAdrNr() == address.getAdrNr() ) 
                        {
                            return true;
                        }
                }
        }    
        return false;
    }
    
    private void updateTeilnehmerOfAppointment(Appointment appointment, Address address, int row)
    {
        TarentWidgetIconComboBox.IconComboBoxEntry entry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oTeilnehmerModel.getValueAt(row, 2));
        int level = ((Integer)(entry.getKey())).intValue();            
        int reqstatus = getTeilnehmerStatusOfRow(row);
        String bemerkung = m_oTeilnehmerModel.getValueAt(row, 4).toString();
        
        try
        {      
            User user = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO().getAssociatedUser(address);
            if (user != null)
            {
                
                // ist Systembenutzer
                Calendar addressusercalendar = user.getCalendar(true);
                
                if (addressusercalendar != null) 
                {
                    AppointmentCalendarRelation acr = appointment.getRelation(addressusercalendar);
                    if (acr != null)
                    {  
                        acr.setRequestStatus(reqstatus);
                        acr.setRequestLevel(level);
                        acr.setAttribute(AppointmentRequest.KEY_DESCRIPTION,bemerkung);
                        PersistenceManager.commit(acr);
                    }
                    else
                    {
                        getGUIListener().getLogger().info("AppointmentCalendarRelation ist null!");
                    }            
                }
                else getGUIListener().getLogger().severe("Systembenutzer hat keinen zugeordneten Kalender!");
            }
            else
            {
                // ist externer Teilnehmer
                AppointmentAddressRelation aar = appointment.getRelation(address);
                if (aar != null)
                {  
                    aar.setRequestStatus(reqstatus);
                    aar.setRequestLevel(level);
                    aar.setAttribute(AppointmentRequest.KEY_DESCRIPTION,bemerkung);
                    PersistenceManager.commit(aar);
                }
                else
                {
                    getGUIListener().getLogger().info("AppointmentAddressRelation ist null!");
                }
            }
        } 
        catch (ContactDBException e)
        {
            logger.severe("Fehler beim Updaten eines Benutzers zu einem Appointment", e);
        }
    }
    
    private void addTeilnehmerToAppointment(Appointment appointment, Address address, int row)
    {
        TarentWidgetIconComboBox.IconComboBoxEntry entry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oTeilnehmerModel.getValueAt(row, 2));
        int level = ((Integer)(entry.getKey())).intValue();    
        int reqstatus = getTeilnehmerStatusOfRow(row);
        
        try
        {
            User user = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO().getAssociatedUser(address);
            if (user != null)
            {
                // ist Systembenutzer
                Calendar addressusercalendar = user.getCalendar(true);
                
                if (addressusercalendar != null) 
                {
                    //if ( getUserCalendar().getId() != addressusercalendar.getId())
                    //{  
                        appointment.add(addressusercalendar);
                        AppointmentCalendarRelation acr = appointment.getRelation(addressusercalendar);
                        if (acr != null)
                        {               
                            acr.setRequestType(AppointmentRequest.TYPE_REQUEST);
                            acr.setRequestStatus(reqstatus);
                            acr.setRequestLevel(level);
                            acr.setDisplayMode(AppointmentCalendarRelation.DISPLAY_FREE);
                            PersistenceManager.commit(acr);
                        }
                        else
                        {
                            getGUIListener().getLogger().info("AppointmentCalendarRelation ist null!");
                        }
                    //}
                }
                else getGUIListener().getLogger().severe("Systembenutzer hat keinen zugeordneten Kalender!");
            }
            else
            {
                // ist externer Teilnehmer
                appointment.add(address);        
                AppointmentAddressRelation aar = appointment.getRelation(address);
                if (aar != null)
                {  
                    aar.setRequestType(AppointmentRequest.TYPE_REQUEST);
                    aar.setRequestStatus(reqstatus);
                    aar.setRequestLevel(level);
                    PersistenceManager.commit(aar);
                }
                else
                {
                    getGUIListener().getLogger().info("AppointmentAddressRelation ist null!");
                }        
            }
        } 
        catch (ContactDBException e)
        {
            logger.severe("Fehler beim Hinzuf�gen eines Benutzers zu einem Appointment", e);
        }
    }
    
    private boolean disinviteTeilnehmerList(Appointment appointment)
    {
        if (m_oAddressesToDisinvite.size() > 0)
        {  
            try
            {
                appointment.disinvite(m_oAddressesToDisinvite);
            } 
            catch (ContactDBException e)
            {
                logger.severe("Fehler beim Ausladen der Benutzer eines Appointments", e);      
                //TODO: zur Sicherheit hier erstmal l�schen... aber dann fehlen die Referenzen auf noch nicht ausgeladene Teilnehmer 
                m_oAddressesToDisinvite.clear();
                return false;
            }
            m_oAddressesToDisinvite.clear();
        }
        return true;
    }
    
    
    private void removeTeilnehmerFromAppointment(Appointment appointment, Address address)
    {    
        try
        {
            AppointmentRequest ar = getRelationOfAddress(address);
            if (ar != null) 
            {
                PersistenceManager.delete(ar);
            }
        } 
        catch (ContactDBException e)
        {
            logger.severe("Fehler beim Entfernen eines Benutzers von einem Appointment", e);
        }   
    }
    
    
    private List getAllAddressesOfAppointment(Appointment appointment)
    {
        String errormessage =   "Folgende Adressen von Teilnehmern dieses Termin werden nicht angezeigt\n" +
                                "(Sie besitzen wahrscheinlich keine entsprechende Leseberechtigung):\n\n";
        String previewString;
        boolean error = false;
        
        ArrayList addresses = new ArrayList();
        try
        {
            // interne Teilnehmer (Systembenutzer) hinzuf�gen...
            Collection calendars = appointment.getCalendars();
            Iterator it = calendars.iterator();
            while(it.hasNext())
            {
                Calendar calendar = (Calendar)(it.next());
                User user = calendar.getUser();
                
                /**
                 * Kommentar von Nils:
                 * Die Bedingung if (user.getId() > 0) soll verhindern, dass
                 * gel�schte User, deren Kalender noch mit dem Termin verknpft ist,
                 * wie in *** Bug 2021 *** beschrieben falsch als Teilnehmer auftauchen.
                 * Ungewollterweise wird bei der Abfrage des BesitzerUsers eines Kalenders
                 * im Fehlerfall (User gel�scht) scheinbar ein leerer User mit ID 0 zur�ck-
                 * geliefert, dessen assoz.Adresse dann irgendwie willkrlich belegt ist.
                 * Um das zu verhindern werden zun�chst mal alle user mit ID unter 1 augefiltert.
                 * 
                 * Das ist jedoch nur eine �bergangsl�sung! Die Anfrage nach dem User eines Kalenders
                 * sollte keinen leeren User zur�ckliefern und die assoz.Adresse auf so einem User
                 * auch nicht so ein willkrliches Resultat liefern!!!
                 */
                
                if (user != null && user.getId() > 0)
                {
                    // ist benutzerkalender (interner teilnehmer)
                    Address address = user.getAssociatedAddress();
                    
                    if (address != null)
                    {  
                        addresses.add(address);
                    }
                    else
                    { 
                        error = true;
                        
                        AddressImpl fakeAddress = new AddressImpl();
                        fakeAddress.setId(-1);
                        addresses.add(fakeAddress);
                        errormessage += "Adresse von User " + user.getLoginName() + "\n";
                        
                        // Log Ausgabe nur auf Stufe INFO. Sie im normalen Betrieb nicht auftauchen.
                        logger.info("getAssociatedAddress() lieferte keine Adresse zur�ck -> Wahrscheinlich unzureichende Rechte");
                    }
                }
            }
            
            //if (error) getGUIListener().publishError(errormessage);
            
            // externe Teilnehmer hinzuf�gen...
            addresses.addAll(appointment.getAddresses());
            
        } 
        catch (ContactDBException e)
        {
            logger.severe("Fehler beim Sammeln aller Benutzer eines Appointments", e);      
        }
        return addresses;
        
    }
    
    
    private List m_oAddressesToDisinvite = new ArrayList();
    
    private AppointmentRequest getRelationOfAddress(Address address)
    {
        AppointmentRequest relation = null;
        try
        {
            User user = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO().getAssociatedUser(address);
            if (user != null)
            {
                // interner Benutzer
                Calendar calendar = user.getCalendar(true);
                if (calendar != null)
                {  
                    relation = m_oAppointment.getRelation(calendar);
                }
            }
            else
            {
                // externer Benutzer
                relation = m_oAppointment.getRelation(address);
            }    
        } catch (ContactDBException e)
        {
            logger.severe("Fehler beim Ermitteln einer Relation zu einem Teilnehmer an einem Appointment", e);      
        }
        return relation;
    }
    
    
    private void removeAddressFromDisinviteList(Address address)
    {    
        Iterator it = m_oAddressesToDisinvite.iterator();
        while(it.hasNext())
        {
            Address listaddress = (Address)it.next();
            if (listaddress.getId() == address.getId())
            {
                m_oAddressesToDisinvite.remove(listaddress);
                return;
            }
        }
    }
    
    
    private boolean isAddressRemovable(Address address)
    {
        if (!m_bIAmInvitor) return false;
        
        if (m_bIAmInvitor )
            return (m_iInvitatorUserId != -1 && m_iInvitatorUserId != address.getAssociatedUserID().intValue());
        
        return true;
    }
    
    private boolean isAddressDisinviteable(Address address)
    {
        if (isAddressRemovable(address))
        {  
            AppointmentRequest relation = getRelationOfAddress(address);
            if (relation != null)
            {
                try
                {                   
                    int status = relation.getRequestStatus();
                    if (status != AppointmentRequest.STATUS_NEEDS_ACTION)
                    {
                        return true;
                    }
                } catch (ContactDBException e)
                {
                    logger.severe("konnte nicht ermitteln ob Teilnehmer ausladbar ist", e);      
                }
            }
        }
        return false;
    }
    
    
    private boolean addAddressToDisinviteList(Address address)
    {
        if (isAddressDisinviteable(address))
        {  
            m_oAddressesToDisinvite.add(address);
            return true;
        }
        return false;
    }
    
    private boolean addAddressesToDisinviteList(List addresses)
    {
        boolean success = true;
        Iterator it = addresses.iterator();
        while(it.hasNext())
        {
            Address address = (Address)it.next();
            if (addAddressToDisinviteList(address) == false) success = false;
        }
        return success;
    }
    
    
    
    private boolean syncTeilnehmerList(Appointment appointment)
    {
        boolean teilnehmerchanged = false;
        
        if (appointment != null)
        {  
            getGUIListener().setWaiting(true);
            
            /* fixes #1684, don't send autogenerated disinvite email !
             * 
            if (!disinviteTeilnehmerList(appointment))
            {
                getGUIListener().publishError("Der Versuch Teilnehmer auszuladen ist fehlgeschlagen.");
                //TODO: irgendwas schlimmes passieren lassen ;-)
                return false;
            }
            */
            
            Collection addresses;
            try
            {
                addresses = getAllAddressesOfAppointment(appointment);        
                if (addresses != null)
                {
                    Iterator iterator = addresses.iterator();
                    while(iterator.hasNext())
                    {
                        Address address = ((Address)(iterator.next()));
                        
                        
                        if (! isTeilnehmerInList(address))
                        {
                            removeTeilnehmerFromAppointment(appointment, address);
                            teilnehmerchanged = true;
                        }
                    }
                    
                    for(int i=0; i<(m_oTeilnehmerModel.getRowCount()); i++)
                    {
                        Address address = ((Address)(m_oTeilnehmerModel.getRowKey(i)));
                        if (! isAddressConnectedToAppointment(appointment, address))                
                        {
                            addTeilnehmerToAppointment(appointment, address, i);
                            teilnehmerchanged = true;
                        }
                        else
                        {  
                            updateTeilnehmerOfAppointment(appointment, address, i);
                        }
                    }
                }    
            } 
            finally
            {
                getGUIListener().setWaiting(false);
            }
        }
        return teilnehmerchanged;
    }
    
    
    // ----------------------------- DEBUG ----------------------------- 
    private void showTeilnehmerList()
    {
        logger.log(Level.FINE, "==========TEILNEHMERLISTE==========");    
        for(int i=0; i<(m_oTeilnehmerModel.getRowCount()); i++)
            {
                Address address = ((Address)(m_oTeilnehmerModel.getRowKey(i)));
                logger.log(Level.FINE, "#" + (i) +"="+ address.getShortAddressLabel());    
            }
        logger.log(Level.FINE, "=================================");    
    }
    
    
    
    private void showAddressCollection(Collection addresses)
    {
        logger.log(Level.FINE, "==========AddressCollection==========");    
        int i=0;
        Iterator iterator = addresses.iterator();
        while(iterator.hasNext())
            {
                Address collectionaddress = ((Address)(iterator.next()));     
                logger.log(Level.FINE, "#" + (i++) +"="+ collectionaddress.getShortAddressLabel());    
            }
        logger.log(Level.FINE, "=====================================");    
    }
    // ----------------------------- DEBUG ----------------------------- 
    
    
    private boolean IsAddressInCollection(Collection addresses, Address address)
    {
        Iterator iterator = addresses.iterator();
        while(iterator.hasNext())
            {
                Address collectionaddress = ((Address)(iterator.next()));
                if (collectionaddress.getAdrNr() == address.getAdrNr()) return true;
            }
        return false;
    }
    
    
    public final static Object FIELD_NULL = "FIELD_NULL";
    public final static Object FIELD_START = "FIELD_START"; 
    public final static Object FIELD_END = "FIELD_END"; 
    public final static Object FIELD_EXCEPTION = "FIELD_EXCEPTION";
    public final static Object FIELD_TEILNEHMERADDED = "FIELD_TEILNEHMERADDED"; 
    public final static Object FIELD_TEILNEHMERREMOVED = "FIELD_TEILNEHMERREMOVED";
    public final static Object FIELD_TEILNEHMERCHANGED = "FIELD_TEILNEHMERCHANGED";
    public final static Object FIELD_CATEGORY = "FIELD_CATEGORY"; 
    public final static Object FIELD_PRIORITY = "FIELD_PRIORITY"; 
    public final static Object FIELD_DESCRIPTION = "FIELD_DESCRIPTION"; 
    public final static Object FIELD_LOCATION = "FIELD_LOCATION"; 
    public final static Object FIELD_SUBJECT = "FIELD_SUBJECT";
    public final static Object FIELD_DISPLAY = "FIELD_DISPLAY";
    public final static Object FIELD_JOBCOMPLETED = "FIELD_JOBCOMPLETED";
    public final static Object FIELD_JOBCOMPLETION  = "FIELD_JOBCOMPLETION";
    public final static Object FIELD_PRIVAT = "FIELD_PRIVAT";
        
    
    public DataChangeList getDirtyFields(Appointment appointment)
    {
        
        DataChangeList list = new DataChangeList();
        if (appointment == null)
        {              
            list.addDataChange(new DataChange(FIELD_NULL, "appointment", "NullPointer", null, null));
        }
        
        if (m_oAppointment == null) 
        {
            list.addDataChange(new DataChange(FIELD_NULL, "m_oAppointment", "NullPointer", null, null));
        }
        
        if ((appointment == null) || (m_oAppointment == null) ) return list;
        
        getGUIListener().setWaiting(true);
        try
        {
            // Startdatum berprfen...
            if (!(m_oDateSpinner_Beginn.getValue().equals(appointment.getStart()))) list.addDataChange(new DataChange(FIELD_START, "Startzeit", "Startzeit wurde ver�ndert", appointment.getStart(), m_oDateSpinner_Beginn.getValue()));
            
            // Enddatum berprfen...
            if (!(m_oDateSpinner_Ende.getValue().equals(appointment.getEnd()))) list.addDataChange(new DataChange(FIELD_END, "Endzeit", "Endzeit wurde ver�ndert", appointment.getEnd(), m_oDateSpinner_Ende.getValue())); 
            
            // Titel berprfen...
            if (!(m_oTextField_Titel.getText().equals(getString(appointment.getAttribute(Appointment.KEY_SUBJECT))))) list.addDataChange(new DataChange(FIELD_SUBJECT, "Titel", "Titel wurde ver�ndert", getString(appointment.getAttribute(Appointment.KEY_SUBJECT)), m_oTextField_Titel.getText()));
            
            // Ort berprfen...
            if (!(m_oTextField_Ort.getText().equals(getString(appointment.getAttribute(Appointment.KEY_LOCATION))))) list.addDataChange(new DataChange(FIELD_LOCATION, "Ort", "Ort wurde ver�ndert", getString(appointment.getAttribute(Appointment.KEY_LOCATION)), m_oTextField_Ort.getText()));
            
            // Beschreibung �berpr�fen...
            if (!(m_oTextArea_Beschreibung.getText().equals(getString(appointment.getAttribute(Appointment.KEY_DESCRIPTION))))) list.addDataChange(new DataChange(FIELD_DESCRIPTION, "Beschreibung", "Beschreibung wurde ver�ndert", getString(appointment.getAttribute(Appointment.KEY_DESCRIPTION)), m_oTextArea_Beschreibung.getText()));          
            
            // Priorit�t �berpr�fen...
            if (((Integer)(((TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Prioritaet.getSelectedItem())).getKey())).intValue() != appointment.getPriority()) list.addDataChange(new DataChange(FIELD_PRIORITY, "Priorit�t", "Priorit�t wurde ver�ndert", getNameOfPriority(appointment.getPriority()),  getNameOfPriority( ((Integer)(((TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Prioritaet.getSelectedItem())).getKey())).intValue() ) )); 
            
            // Kategorie berprfen...
            if (!(appointment.isJob()))
            {  
                TarentWidgetIconComboBox.IconComboBoxEntry comboentry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Typ.getSelectedItem());        
                if (comboentry != null)
                {  
                    Integer selected = ((Integer)(comboentry.getKey()));
                    if (selected != null)
                    {  
                        int selectedcategory = selected.intValue();
                        int category = appointment.getAppointmentCategory();
                        if (selectedcategory != category) 
                        {
                            ScheduleType categorytype = getControlListener().getScheduleData().getScheduleTypes().getScheduleType(category);
                            ScheduleType selectedcategorytype = getControlListener().getScheduleData().getScheduleTypes().getScheduleType(selectedcategory);
                            String categorytypetext = "#" + category;
                            String selectedcategorytypetext = "#" + selectedcategory;
                            if (categorytype != null)
                            {  
                                categorytypetext = categorytype.getName();
                            }
                            if (selectedcategorytype != null)
                            {  
                                selectedcategorytypetext = selectedcategorytype.getName();
                            }
                            list.addDataChange(new DataChange(FIELD_CATEGORY, "Typ", "Typ wurde ver�ndert", categorytypetext, selectedcategorytypetext));
                        }
                    }
                }
            }
            
            // Teilnehmer berprfen...
            if (!(appointment.isJob()))
            {  
                for(int i=0; i<(m_oTeilnehmerModel.getRowCount()); i++)
                {
                    Address address = (Address)(m_oTeilnehmerModel.getRowKey(i));
                    if (! isAddressConnectedToAppointment(appointment, address)) 
                    {
                        list.addDataChange(new DataChange(FIELD_TEILNEHMERADDED, "Teilnehmer hinzugefgt", "Teilnehmer " + address.getShortAddressLabel() + " wurde hinzugefgt", "", address.getShortAddressLabel()));
                    }
                }
                
                Collection addresses;
                try
                {          
                    addresses = getAllAddressesOfAppointment(appointment);
                    //addresses = new ArrayList(appointment.getAddresses());
                    if (addresses != null)
                    {
                        Iterator iterator = addresses.iterator();
                        while(iterator.hasNext())
                        {
                            Address address = ((Address)(iterator.next()));              
                            if (! isTeilnehmerInList(address))
                            {
                                list.addDataChange(new DataChange(FIELD_TEILNEHMERREMOVED, "Teilnehmer entfernt", "Teilnehmer " + address.getShortAddressLabel() + " wurde entfernt", address.getShortAddressLabel(), ""));
                            }
                            else
                            {
                                // ist ein alter Teilnehmer
                                
                                int row = getRowOfTeilnehmerInListWithID(address.getId());
                                if (row != -1)
                                {  
                                    TarentWidgetIconComboBox.IconComboBoxEntry entry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oTeilnehmerModel.getValueAt(row, 2));
                                    int gui_level = ((Integer)(entry.getKey())).intValue();            
                                    int gui_reqstatus = getTeilnehmerStatusOfRow(row);
                                    String gui_bemerkung = m_oTeilnehmerModel.getValueAt(row, 4).toString();
                                    
                                    AppointmentRequest relation = getRelationOfAddress(address);
                                    if (relation != null)
                                    {  
                                        String app_bemerkung = relation.getAttribute(AppointmentRequest.KEY_DESCRIPTION);
                                        if (app_bemerkung == null || app_bemerkung.trim().equals("") ) app_bemerkung = "Bemerkung";
                                        
                                        int app_level = relation.getRequestLevel();
                                        int app_reqstatus = relation.getRequestStatus();
                                        
                                        if (app_level != gui_level)
                                        {
                                            TarentWidgetIconComboBox.IconComboBoxEntry applevelentry = getEntryOfAnwesenheit(app_level);
                                            String app_level_text = "";
                                            if (applevelentry != null) app_level_text = applevelentry.getText();
                                            
                                            TarentWidgetIconComboBox.IconComboBoxEntry guilevelentry = getEntryOfAnwesenheit(gui_level);
                                            String gui_level_text = "";
                                            if (guilevelentry != null) gui_level_text = guilevelentry.getText();
                                            
                                            list.addDataChange(new DataChange(FIELD_TEILNEHMERCHANGED, "Teilnehmer wurde ver�ndert", "Teilnehmer " + address.getShortAddressLabel() + " wurde in seinem Anwesenheitsstatus ver�ndert", app_level_text, gui_level_text));                      
                                        }
                                        
                                        if (app_reqstatus != gui_reqstatus)
                                        {
                                            list.addDataChange(new DataChange(FIELD_TEILNEHMERCHANGED, "Teilnehmer wurde ver�ndert", "Teilnehmer " + address.getShortAddressLabel() + " wurde in seinem Zusagestatus ver�ndert", getNameOfEinladeStatus(app_reqstatus), getNameOfEinladeStatus(gui_reqstatus)));                      
                                        }
                                        
                                        if (! app_bemerkung.equals(gui_bemerkung))
                                        {
                                            list.addDataChange(new DataChange(FIELD_TEILNEHMERCHANGED, "Teilnehmer wurde ver�ndert", "Die Bemerkung f�r Teilnehmer " + address.getShortAddressLabel() + " wurde ver�ndert", app_bemerkung, gui_bemerkung));                      
                                        } 
                                        
                                    }
                                }                
                            }
                        }
                    }
                }
                catch(ContactDBException cde)
                {          
                    logger.severe("Fehler beim Vergleichen der Teilnehmerdaten eines Appointments mit der GUI.", cde);
                }
            }
            
            Calendar cal = null;
            Collection calColl = appointment.getCalendars();
            for (Iterator iter = calColl.iterator(); iter.hasNext();){
                cal = (Calendar) iter.next();
                if (getGUIListener().getCalendarCollection().getCollection().contains(cal))
                    continue;
            }
            
            if (appointment.isJob())
            {  
                if (m_oCheckBox_Completed.isSelected() != appointment.isJobCompleted()) list.addDataChange(new DataChange(FIELD_JOBCOMPLETED, "Aufgabenerfllungsstatus", "Aufgabenerfllungsstatus wurde ver�ndert", new Boolean(appointment.isJobCompleted()), new Boolean(m_oCheckBox_Completed.isSelected())));
                if (m_oSlider_Completed.getValue() != appointment.getJobCompletion()) list.addDataChange(new DataChange(FIELD_JOBCOMPLETION, "Aufgabenerfllungslevel", "Aufgabenerfllungslevel wurde ver�ndert", new Integer(appointment.getJobCompletion()), new Integer(m_oSlider_Completed.getValue())));
            }
            
            if (m_oCheckBox_Privat.isSelected() != appointment.isPrivat()) list.addDataChange(new DataChange(FIELD_PRIVAT, "Privat", "Privat wurde ver�ndert", new Boolean(appointment.isPrivat()),  new Boolean(m_oCheckBox_Privat.isSelected())));
        
        }
        
        catch(ContactDBException cde)
        {
            list.addDataChange(new DataChange(FIELD_EXCEPTION, "Exception", "Exception " + cde.getMessage() + " wurde geworfen", "",  cde.getMessage()));
        }
        finally
        {
            getGUIListener().setWaiting(false);
        }
        
        return list;
    }
    
    
    private boolean isAppointmentDirty(Appointment appointment)
    {
        return (getDirtyFields(appointment).containsChanges());    
    }
    
    private String getNameOfDisplay(int display)
    {
        switch(display)
        {
        case(AppointmentCalendarRelation.DISPLAY_AWAY): return "nicht anwesend";
        case(AppointmentCalendarRelation.DISPLAY_BOOKED): return "belegt";
        case (AppointmentCalendarRelation.DISPLAY_CONDITIONALLY): return "noch unklar";
        case (AppointmentCalendarRelation.DISPLAY_FREE): return "frei";
        }
        return "unbekannt";  
    }
    
    private String getNameOfPriority(int priority)
    {
        switch(priority)
        {
        case(Appointment.PRIORITY_HIGH): return "hoch";
        case(Appointment.PRIORITY_MEDIUM): return "mittel";
        case (Appointment.PRIORITY_LOW): return "niedrig";
        }
        return "unbekannt";  
    }
    
    private String getString(Object string)
    {
        if (string == null) return "";
        else return string.toString();
    }
    
    public void postFinalRealize()
    {        
    }
    
    
    public void setData(PluginData data)
    {    
    }
    
    public void getData(PluginData data)
    {
    }
    
    public boolean isDirty(PluginData data)
    {
        return false;
    }
    
    public void setEditable(boolean iseditable)
    {
    }
    
    public String getKey()
    {
        return "SCHEDULEDETAIL";
    }
    
    public void setDoubleCheckSensitive(boolean issensitive)
    {
    }

    private EntryLayoutHelper createPanel(String widgetFlags)
    {
        // Main-Panel
        TarentWidgetPanel panel = new TarentWidgetPanel(); 
        panel.setLayout(new BorderLayout());
        
        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIconJob = (ImageIcon)iconFactory.getIcon("schedule_todo.gif");    
        m_oIconAppointment = (ImageIcon)iconFactory.getIcon("schedule_standard.gif");
        
        
        m_oIcon_AddPerson = (ImageIcon)iconFactory.getIcon("schedule_addperson.gif");
        m_oIcon_RemovePerson = (ImageIcon)iconFactory.getIcon("schedule_removeperson.gif");
        m_oIcon_InviteExternal = (ImageIcon)iconFactory.getIcon("schedule_inviteexternal.gif");
        m_oIcon_InviteAll = (ImageIcon)iconFactory.getIcon("schedule_inviteall.gif");
        
        
        m_oIcon_Jump = (ImageIcon)iconFactory.getIcon("teilnehmer_jump.gif");
        m_oIcon_Disinvite = m_oIcon_RemovePerson;
        m_oIcon_SendMail = (ImageIcon)iconFactory.getIcon("teilnehmer_sendmail.gif");
        m_oIcon_Remove = m_oIcon_RemovePerson;
        
        // ---------------------------------------------------------------------------------------
        
        // Panels f�r Tabs erzeugen... 
        TarentPanel oPanelDetails1T = new TarentPanel();
        JPanel panel1t = new JPanel(new BorderLayout());
        panel1t.setBorder(new EmptyBorder(5,5,5,5));
        panel1t.add(oPanelDetails1T, BorderLayout.NORTH);
        
        TarentPanel oPanelDetails1A = new TarentPanel();
        oPanelDetails1A.setLayout(new GridBagLayout());
        
        JPanel panel1a = new JPanel(new BorderLayout());
        panel1a.setBorder(new EmptyBorder(5,5,5,5));
        panel1a.add(oPanelDetails1A, BorderLayout.NORTH);
        
        JPanel panel1grid = new JPanel(new GridLayout(1,2));    
        panel1grid.add(panel1a);
        //panel1grid.add(panel1b);    
        
        JPanel panel1 = new JPanel(new BorderLayout());
        panel1.setBorder(new EmptyBorder(5,5,5,5));
        panel1.add(panel1t, BorderLayout.NORTH);    
        panel1.add(panel1grid, BorderLayout.CENTER);
        
        JPanel panel2a = new JPanel(new BorderLayout());
        panel2a.setBorder(new EmptyBorder(5,5,5,5));
        
        TarentPanel oPanelDetails3A = new TarentPanel();
        JPanel panel3a = new JPanel(new BorderLayout());
        panel3a.setBorder(new EmptyBorder(5,5,5,5));
        panel3a.add(oPanelDetails3A, BorderLayout.NORTH);
        
        JPanel panel4a = new JPanel(new BorderLayout());
        panel4a.setBorder(new EmptyBorder(5,5,5,5));
        
        TarentPanel oPanelDetails5A = new TarentPanel();
        JPanel panel5a = new JPanel(new BorderLayout());
        panel5a.setBorder(new EmptyBorder(5,5,5,5));
        panel5a.add(oPanelDetails5A, BorderLayout.NORTH);
        
        JPanel panel6a = new JPanel(new BorderLayout());
        panel6a.setBorder(new EmptyBorder(5,5,5,5));
        
        reminderPanel = new JPanel(new GridBagLayout());
        reminderPanel.setBorder(new EmptyBorder(5,1,5,1));
                
        
        // ---------------------------------------------------------------------------------------
        
        
        // Panels f�r Tabs fllen...
        //Panel 1
        m_oTextField_Titel = new TarentWidgetTextField();
        m_oTextField_Titel.setLengthRestriction(50);
        
        TarentPanel BeschreibungPanel = new TarentPanel();
        BeschreibungPanel.setLayout(new BorderLayout());
                
        TarentWidgetLabel BeschreibungLabel = new TarentWidgetLabel("Notizen zum Termin:");
        BeschreibungLabel.setBorder(new EmptyBorder(2,0,5,0));
        
        m_oTextArea_Beschreibung = new TarentWidgetTextArea(7, 20); 
        m_oTextArea_Beschreibung.getTextArea().setLineWrap(true);
        m_oTextArea_Beschreibung.getTextArea().setWrapStyleWord(true);
        m_oTextArea_Beschreibung.setLengthRestriction(500);
        
        BeschreibungPanel.add(BeschreibungLabel, BorderLayout.NORTH);
        BeschreibungPanel.add(m_oTextArea_Beschreibung, BorderLayout.CENTER);
        
        //reminder -------------------------------
        c = new GridBagConstraints();
        TarentWidgetLabel reminderLabel = new TarentWidgetLabel("Terminbenachrichtigung: ");
        reminderCheckBox = new TarentWidgetCheckBox();
        reminderCheckBox.setEnabled(false);     
        
        reminderButton = new TarentWidgetButton("Benachrichtigung �ndern");
        reminderButton.setMaximumSize(new Dimension(50,30));
        reminderButton.addActionListener(new ReminderButtonClicked());
        
        //c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.WEST;
        //c.weightx = 0.1;
        reminderPanel.add(reminderLabel, c);
        //c.weightx = 0.1;
        reminderPanel.add(reminderCheckBox, c);
        c.weightx = 0.8;
        c.gridwidth = GridBagConstraints.REMAINDER;
        reminderPanel.add(reminderButton,c);
        
        
        //----------------------------------------
                
        m_oTextField_Ort = new TarentWidgetTextField();
        m_oTextField_Ort.setLengthRestriction(50);
        
        m_oTextField_Erstellungsdatum = new TarentWidgetTextField();
        m_oTextField_Erstellungsdatum.setWidgetEditable(false);
        
        m_oDateSpinner_Beginn = new TarentWidgetDateSpinner();
        m_oDateSpinner_Beginn.addChangeListener(new StartDateChanged());        
        
        m_oDateSpinner_Ende = new TarentWidgetDateSpinner();
        m_oDateSpinner_Ende.addChangeListener(new EndDateChanged());        
        
        m_oSlider_Completed = new TarentWidgetSlider(0, 100, 0);
        m_oSlider_Completed.addChangeListener(new CompletedSliderChangeListener());    
        //Create the label table
        Hashtable labelTable = new Hashtable();
        for(int i=0; i<=100; i += 20)
        {  
            labelTable.put(new Integer(i), new JLabel(Integer.toString(i)));
        }    
        m_oSlider_Completed.setLabelTable(labelTable);    
        m_oSlider_Completed.setPaintLabels(true);    
        m_oSlider_Completed.setMajorTickSpacing(20);
        m_oSlider_Completed.setMinorTickSpacing(5);
        m_oSlider_Completed.setPaintTicks(true);
        
        m_oCheckBox_Completed = new TarentWidgetCheckBox(false);
        m_oCheckBox_Completed.addActionListener(new CompletedCheckActionListener());
        
        m_oCheckBox_Privat = new TarentWidgetCheckBox(false);
        
        m_oIconComboBox_Prioritaet = new TarentWidgetIconComboBox();
        m_oIconComboBox_Prioritaet.addIconComboBoxEntry(new IconComboBoxEntry((ImageIcon)iconFactory.getIcon("priority_high.gif"), getNameOfPriority(Appointment.PRIORITY_HIGH), new Integer(Appointment.PRIORITY_HIGH)));
        m_oIconComboBox_Prioritaet.addIconComboBoxEntry(new IconComboBoxEntry((ImageIcon)iconFactory.getIcon("priority_medium.gif"), getNameOfPriority(Appointment.PRIORITY_MEDIUM), new Integer(Appointment.PRIORITY_MEDIUM)));
        m_oIconComboBox_Prioritaet.addIconComboBoxEntry(new IconComboBoxEntry((ImageIcon)iconFactory.getIcon("priority_low.gif"), getNameOfPriority(Appointment.PRIORITY_LOW), new Integer(Appointment.PRIORITY_LOW)));
        
        m_oIconComboBox_Typ = new TarentWidgetIconComboBox();
        
        Map scheduletypes = getControlListener().getScheduleData().getScheduleTypes().getScheduleTypeMap();
        Iterator it = scheduletypes.values().iterator();
        while(it.hasNext())
        {
            ScheduleType scheduletype = (ScheduleType)(it.next());
            m_oIconComboBox_Typ.addIconComboBoxEntry(new IconComboBoxEntry(scheduletype.getIcon(), scheduletype.getName(), new Integer(scheduletype.getKey())));
        }    
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;   
        constraints.insets = new Insets(2, 2, 2, 10);
        constraints.gridwidth = 4;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 0.8;
        
        GridBagConstraints constraints2 = new GridBagConstraints();
        constraints2.anchor = GridBagConstraints.EAST;
        constraints2.insets = new Insets(2, 2, 2, 2);       
        
        m_oLabel_Titel = new TarentWidgetLabel("Titel:", JLabel.RIGHT);
        oPanelDetails1A.add(m_oLabel_Titel, constraints2);
        oPanelDetails1A.add(m_oTextField_Titel, constraints);

        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints2.gridx = 0;
        constraints2.gridy = 1;
                
        m_oLabel_Ort = new TarentWidgetLabel("Ort:", JLabel.RIGHT);
        oPanelDetails1A.add(m_oLabel_Ort, constraints2); 
        oPanelDetails1A.add(m_oTextField_Ort, constraints);             
        
        constraints.gridy++;
        constraints2.gridy++;
        
        m_oLabel_Beginn = new TarentWidgetLabel("Beginn:");    
        oPanelDetails1A.add(m_oLabel_Beginn, constraints2);
        oPanelDetails1A.add(m_oDateSpinner_Beginn, constraints);
        
        constraints.gridy++;
        constraints2.gridy++;
        
        m_oLabel_Ende = new TarentWidgetLabel("Ende:");    
        oPanelDetails1A.add(m_oLabel_Ende, constraints2);
        oPanelDetails1A.add(m_oDateSpinner_Ende, constraints);
        
        constraints.gridy++;
        constraints2.gridy++;
        
        m_oLabel_CompletedSlider = new TarentWidgetLabel("Status:", JLabel.RIGHT);
        m_oLabel_CompletedCheck = new TarentWidgetLabel("Fertig:", JLabel.RIGHT);
        TarentWidgetPanel statuspanel = new TarentWidgetPanel(new BorderLayout());
        statuspanel.add(m_oSlider_Completed, BorderLayout.CENTER);
        statuspanel.add(m_oCheckBox_Completed, BorderLayout.EAST);
        
        oPanelDetails1A.add(m_oLabel_CompletedSlider, constraints2);
        oPanelDetails1A.add(statuspanel, constraints);
        constraints.gridy++;
        constraints2.gridy++;

        m_oLabel_Privat = new TarentWidgetLabel("Privat:");
        oPanelDetails1A.add(m_oLabel_Privat, constraints2);
        oPanelDetails1A.add(m_oCheckBox_Privat, constraints);
        
        constraints.gridx = 4;
        constraints.gridy = 1;
        constraints2.gridx = 3;
        constraints2.gridy = 1;
        
        m_oLabel_Prio = new TarentWidgetLabel("Priorit�t:");
        oPanelDetails1A.add(m_oLabel_Prio, constraints2);
        oPanelDetails1A.add(m_oIconComboBox_Prioritaet, constraints);
        
        constraints.gridy++;
        constraints2.gridy++;
        
        m_oLabel_Typ = new TarentWidgetLabel("Typ:");
        oPanelDetails1A.add(m_oLabel_Typ, constraints2);
        oPanelDetails1A.add(m_oIconComboBox_Typ, constraints);
        
        constraints.gridy++;
        constraints2.gridy++;
        
        //m_oLabel_Anzeige = new tarentWidgetLabel("Anzeige:");
        //oPanelDetails1B.addWidget(m_oLabel_Anzeige, m_oIconComboBox_Anzeige);       
        
        m_oLabel_Erstellungsdatum = new TarentWidgetLabel("angelegt am:");    
        oPanelDetails1A.add(m_oLabel_Erstellungsdatum, constraints2);
        oPanelDetails1A.add(m_oTextField_Erstellungsdatum, constraints);
        
        
        
        // ---------------------------------------------------------------------------------------
        
        panel6a.add(BeschreibungPanel, BorderLayout.CENTER);
        
        CalendarCollection calendars = getGUIListener().getCalendarCollection();
        panel6a.add(reminderPanel, BorderLayout.AFTER_LAST_LINE);
        showReminderPanel(false);
        // ---------------------------------------------------------------------------------------
        
        m_oTeilnehmerModel = new TarentWidgetMutableComponentTable.ObjectTableModel(5);    
        
        m_oTeilnehmerModel.setColumnName(0, "Typ");    
        m_oTeilnehmerModel.setColumnEditable(0, false);   
        
        m_oTeilnehmerModel.setColumnName(1, "Name");    
        m_oTeilnehmerModel.setColumnEditable(1, false);    
        
        m_oTeilnehmerModel.setColumnName(2, "Anwesenheit");    
        m_oTeilnehmerModel.setColumnEditable(2, true);    
        
        m_oTeilnehmerModel.setColumnName(3, "Status");    
        m_oTeilnehmerModel.setColumnEditable(3, true);    
        
        m_oTeilnehmerModel.setColumnName(4, "Bemerkungen");    
        m_oTeilnehmerModel.setColumnEditable(4, true);
        
    //  m_oTeilnehmerModel.setColumnName(5, "Erinnerung");    
    //  m_oTeilnehmerModel.setColumnEditable(5, true);
        
        m_oComponentTable_Teilnehmer = new TarentWidgetMutableComponentTable(m_oTeilnehmerModel);     
        m_oComponentTable_Teilnehmer.getJTable().setRowHeight(20);
        m_oComponentTable_Teilnehmer.getJTable().setCellSelectionEnabled(false);
        m_oComponentTable_Teilnehmer.getJTable().setColumnSelectionAllowed(false);
        m_oComponentTable_Teilnehmer.getJTable().setRowSelectionAllowed(true);    
        m_oComponentTable_Teilnehmer.getJTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        

        MouseListener popupListener = new PopupListener();
        m_oComponentTable_Teilnehmer.getJTable().addMouseListener(popupListener);
        
        m_oLabelAnwesenheit = new JLabel();    
        m_oIconComboBox_Anwesenheit = new TarentWidgetIconComboBox();    
        m_oIconComboBox_Anwesenheit.addIconComboBoxEntry(new IconComboBoxEntry((ImageIcon)iconFactory.getIcon("level_desired.gif"), "erwnscht", new Integer(AppointmentCalendarRelation.LEVEL_DESIRED)));
        m_oIconComboBox_Anwesenheit.addIconComboBoxEntry(new IconComboBoxEntry((ImageIcon)iconFactory.getIcon("level_optional.gif"), "optional", new Integer(AppointmentCalendarRelation.LEVEL_OPTIONAL)));
        m_oIconComboBox_Anwesenheit.addIconComboBoxEntry(new IconComboBoxEntry((ImageIcon)iconFactory.getIcon("level_required.gif"), "erforderlich", new Integer(AppointmentCalendarRelation.LEVEL_REQUIRED)));
        
        JComboBox combo_status = new JComboBox();
        
        // -------------------------------------------------------------------------------
        
        m_oComponentTable_Teilnehmer.setColumnComponent(0, new ImageIcon(), new ImageIcon());
        m_oComponentTable_Teilnehmer.setColumnComponent(1, new JTextField(), new JTextField());
        m_oComponentTable_Teilnehmer.setColumnComponent(2, m_oLabelAnwesenheit, m_oIconComboBox_Anwesenheit);
        m_oComponentTable_Teilnehmer.setColumnComponent(3, new JTextField(), combo_status);
        m_oComponentTable_Teilnehmer.setColumnComponent(4, new JTextField(), new JTextField());
        // m_oComponentTable_Teilnehmer.setColumnComponent(5, new JCheckBox(), new JCheckBox());
        
        m_oComponentTable_Teilnehmer.setFixedColumnSize(0, 20);
        
        
        TarentWidgetButton button_addperson = new TarentWidgetButton("hinzuf�gen", m_oIcon_AddPerson);
        button_addperson.setToolTipText("Teilnehmer hinzuf�gen");        
        button_addperson.addActionListener(new button_addperson_clicked());
        
        TarentWidgetButton button_removeperson = new TarentWidgetButton("entfernen", m_oIcon_RemovePerson);
        button_removeperson.setToolTipText("selektierte Teilnehmer entfernen");    
        button_removeperson.addActionListener(new button_removeperson_clicked());
        
        
        m_oButton_InviteAll = new TarentWidgetButton("benachrichtigen", m_oIcon_InviteAll);
        m_oButton_InviteAll.setToolTipText("Die Teilnehmer werden durch eine eMail auf den Termin hingewiesen. Systembenutzer werden von der Benachrichtigung ausgenommen");    
        m_oButton_InviteAll.addActionListener(new button_invitepersons_clicked());    
    
    
    
        TarentWidgetHorizontalPanel oTeilnehmerEditPanel = new TarentWidgetHorizontalPanel(); 
        oTeilnehmerEditPanel.addWidget(button_addperson, 33);
        oTeilnehmerEditPanel.addWidget(button_removeperson, 33);
    if (m_bShowInviteButton)
    {
      oTeilnehmerEditPanel.addWidget(m_oButton_InviteAll, 34);          
    }
    else
    {
      TarentWidgetLabel dummyLabel = new TarentWidgetLabel("");  
      oTeilnehmerEditPanel.addWidget(dummyLabel, 34);          
    }
        
        m_oPanelTeilnehmerheadline = new JPanel(new BorderLayout());
        m_oPanelTeilnehmerheadline.add(new TarentWidgetLabel("Teilnehmer:"), BorderLayout.WEST);
        m_oPanelTeilnehmerheadline.add(oTeilnehmerEditPanel, BorderLayout.CENTER);    
        
        panel2a.add(m_oPanelTeilnehmerheadline, BorderLayout.NORTH);
        panel2a.add(m_oComponentTable_Teilnehmer, BorderLayout.CENTER);
        
        
        
        
        // ---------------------------------------------------------------------------------------
        //                                          R E P E A T
        // ---------------------------------------------------------------------------------------
        
        ActionListener listener = new radiobutton_repeat_clicked();
        
        
        m_oButtonGroup = new ButtonGroup();
        
        m_oRadioButton_Repeat_None = new JRadioButton("keine Wiederholung");
        m_oRadioButton_Repeat_None.addActionListener(listener);
        
        m_oRadioButton_Repeat_Dayly = new JRadioButton("t�glich");
        m_oRadioButton_Repeat_Dayly.addActionListener(listener);
        
        m_oRadioButton_Repeat_Weekly = new JRadioButton("w�chentlich");
        m_oRadioButton_Repeat_Weekly.addActionListener(listener);
        
        m_oRadioButton_Repeat_Monthly = new JRadioButton("monatlich");
        m_oRadioButton_Repeat_Monthly.addActionListener(listener);
        
        m_oButtonGroup.add(m_oRadioButton_Repeat_None);
        m_oButtonGroup.add(m_oRadioButton_Repeat_Dayly);
        m_oButtonGroup.add(m_oRadioButton_Repeat_Weekly);
        m_oButtonGroup.add(m_oRadioButton_Repeat_Monthly);
        
        m_oComboBox_Repeat_Select = new JComboBox();
        m_oComboBox_Repeat_Select.addItem("letzter");
        m_oComboBox_Repeat_Select.addItem("erster");
        m_oComboBox_Repeat_Select.addItem("zweiter");
        m_oComboBox_Repeat_Select.addItem("dritter");
        m_oComboBox_Repeat_Select.addItem("vierter");
        m_oComboBox_Repeat_Select.addItem("fnfter");
        m_oComboBox_Repeat_Select.addItem("sechster");
        m_oComboBox_Repeat_Select.addItem("7.");
        m_oComboBox_Repeat_Select.addItem("8.");
        m_oComboBox_Repeat_Select.addItem("9.");
        m_oComboBox_Repeat_Select.addItem("10.");
        m_oComboBox_Repeat_Select.addItem("11.");
        m_oComboBox_Repeat_Select.addItem("12.");
        m_oComboBox_Repeat_Select.addItem("13.");
        m_oComboBox_Repeat_Select.addItem("14.");
        m_oComboBox_Repeat_Select.addItem("15.");
        m_oComboBox_Repeat_Select.addItem("16.");
        m_oComboBox_Repeat_Select.addItem("17.");
        m_oComboBox_Repeat_Select.addItem("18.");
        m_oComboBox_Repeat_Select.addItem("19.");
        m_oComboBox_Repeat_Select.addItem("20.");
        m_oComboBox_Repeat_Select.addItem("21.");
        m_oComboBox_Repeat_Select.addItem("22.");
        m_oComboBox_Repeat_Select.addItem("23.");
        m_oComboBox_Repeat_Select.addItem("24.");
        m_oComboBox_Repeat_Select.addItem("25.");
        m_oComboBox_Repeat_Select.addItem("26.");
        m_oComboBox_Repeat_Select.addItem("27.");
        m_oComboBox_Repeat_Select.addItem("28.");
        m_oComboBox_Repeat_Select.addItem("29.");
        m_oComboBox_Repeat_Select.addItem("30.");
        m_oComboBox_Repeat_Select.addItem("31.");
        
        m_oComboBox_Repeat_Unit = new JComboBox();
        m_oComboBox_Repeat_Unit.addItem("Tag");
        m_oComboBox_Repeat_Unit.addItem("Wochentag");
        m_oComboBox_Repeat_Unit.addItem("Wochenende");
        m_oComboBox_Repeat_Unit.addItem("Montag");
        m_oComboBox_Repeat_Unit.addItem("Dienstag");
        m_oComboBox_Repeat_Unit.addItem("Mittwoch");
        m_oComboBox_Repeat_Unit.addItem("Donnerstag");
        m_oComboBox_Repeat_Unit.addItem("Freitag");
        m_oComboBox_Repeat_Unit.addItem("Samstag");
        m_oComboBox_Repeat_Unit.addItem("Sonntag");
        
        m_oLabel_Repeat_All = new JLabel("alle");
        
        m_oComboBox_Repeat_Months = new JComboBox();
        for(int i=1; i<13; i++) m_oComboBox_Repeat_Months.addItem(Integer.toString(i));
        
        m_oLabel_Repeat_Months = new JLabel("Monate");
        
        JPanel panel_lineA = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel panel_lineB = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        panel_lineA.add(m_oRadioButton_Repeat_None);
        panel_lineA.add(m_oRadioButton_Repeat_Dayly);
        panel_lineA.add(m_oRadioButton_Repeat_Weekly);
        
        panel_lineB.add(m_oRadioButton_Repeat_Monthly);
        panel_lineB.add(m_oComboBox_Repeat_Select);
        panel_lineB.add(m_oComboBox_Repeat_Unit);
        panel_lineB.add(m_oLabel_Repeat_All);
        panel_lineB.add(m_oComboBox_Repeat_Months);
        panel_lineB.add(m_oLabel_Repeat_Months);
        
        JPanel panel_lineAB = new JPanel(new BorderLayout());
        panel_lineAB.add(panel_lineA, BorderLayout.NORTH);
        panel_lineAB.add(panel_lineB, BorderLayout.SOUTH);
        
        
        // ----------------------------------------------------------------------
        m_oLabel_Repeat_Start = new JLabel("Beginn der Terminserie:");
        m_oLabel_Repeat_End = new JLabel("Ende der Terminserie:");
        
        m_oDateSpinner_Repeat_Start = new JSpinner(new SpinnerDateModel());
        m_oDateSpinner_Repeat_End = new JSpinner(new SpinnerDateModel());
        
        JPanel panel_lineC = new JPanel(new GridLayout(2,2));
        panel_lineC.add(m_oLabel_Repeat_Start);
        panel_lineC.add(m_oDateSpinner_Repeat_Start);
        panel_lineC.add(m_oLabel_Repeat_End);
        panel_lineC.add(m_oDateSpinner_Repeat_End);
        
        JPanel panel_lineCD = new JPanel(new BorderLayout());
        panel_lineCD.add(panel_lineC, BorderLayout.NORTH);
        
        // ----------------------------------------------------------------------
        
        TarentWidgetPanel oRepeatPanel =new TarentWidgetPanel(new BorderLayout());   
        oRepeatPanel.add(panel_lineAB, BorderLayout.NORTH);
        oRepeatPanel.add(panel_lineCD, BorderLayout.SOUTH);
        
        m_oRadioButton_Repeat_None.setSelected(true);
        setMonthlyEnabled(false);
        setDurationEnabled(false);
        
        // TODO: Correct layout if neccessary
        oPanelDetails3A.add(new JLabel("Wiederholung:"));
        oPanelDetails3A.add(oRepeatPanel);
        
        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        
        
        // Tabs erzeugen...    
        m_oTabbedPane = new JTabbedPane();     
        m_oTabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        m_oTabbedPane.addTab(m_sTabText[TAB_ALLGEMEIN], m_oIconAppointment, panel1, "Allgemeine Daten eines Termins");
        m_oTabbedPane.addTab(m_sTabText[TAB_BEMERKUNG], (ImageIcon)iconFactory.getIcon("schedule_comment.gif"), panel6a, "Feld f�r Bemerkungen zum Termin");
        m_oTabbedPane.addTab(m_sTabText[TAB_TEILNEHMER], (ImageIcon)iconFactory.getIcon("schedule_persons.gif"), panel2a, "Teilnehmer des Termins");
        //     m_oTabbedPane.addTab(m_sTabText[TAB_WIEDERHOLUNG], loadIcon("schedule_repeat.gif"), panel3a, "Einstellungen f�r wiederkehrende Termine");
        
        // ---------------------------------------------------------------------------------------
        m_oTabbedPane.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(m_oTabbedPane));
        
        
        JPanel buttonpanel = new JPanel(new GridLayout(1, 0, 5, 0));
        
        JPanel outerbuttonpanel = new JPanel(new BorderLayout());
        outerbuttonpanel.setBorder(new EmptyBorder(6,0,2,0));
        outerbuttonpanel.add(buttonpanel, BorderLayout.WEST);
        
        //m_oButton_SaveCurrent = new JButton(loadIcon("schedule_save.gif"));
        m_oButton_SaveCurrent = new JButton("speichern");
        m_oButton_SaveCurrent.setToolTipText("speichert den Termin");
        m_oButton_SaveCurrent.addActionListener(new button_save_clicked());
        m_oButton_SaveCurrent.setFocusPainted(false);
        
        //m_oButton_DeleteCurrent = new JButton(loadIcon("schedule_delete.gif"));
        m_oButton_DeleteCurrent = new JButton("l�schen");
        m_oButton_DeleteCurrent.setToolTipText("l�scht den Termin");
        m_oButton_DeleteCurrent.addActionListener(new button_delete_clicked());
        m_oButton_DeleteCurrent.setFocusPainted(false);
        
        //m_oButton_RefreshCurrent = new JButton(loadIcon("schedule_refresh.gif"));
        m_oButton_RefreshCurrent = new JButton("aktualisieren");
        m_oButton_RefreshCurrent.setToolTipText("aktualisiert den Termin");
        m_oButton_RefreshCurrent.addActionListener(new button_refresh_clicked());
        m_oButton_RefreshCurrent.setFocusPainted(false);
        
        buttonpanel.add(m_oButton_SaveCurrent);
        //buttonpanel.add(m_oButton_ResetCurrent);
        buttonpanel.add(m_oButton_DeleteCurrent);
        buttonpanel.add(m_oButton_RefreshCurrent);
        
        // TabPane in MainPanel einfgen...  
        panel.add(m_oTabbedPane, BorderLayout.CENTER);    
        panel.add(outerbuttonpanel, BorderLayout.NORTH);
        
        panel.setMinimumSize(new Dimension(5,5));
        setPanelActive(false);    
        
        // TODO: Make individual components accessible
        return(new EntryLayoutHelper(new TarentWidgetInterface[] { panel }, widgetFlags));
    }
    
    
    
    
    private boolean m_bDateChangedListenersActive = true;
    
    private class StartDateChanged implements ChangeListener
    {
        public void stateChanged(ChangeEvent e)
        {
            if ((m_bDateChangedListenersActive) && (m_oAppointment != null))
            {  
                long duration = 0;
                try {
                    duration = m_oAppointment.getEnd().getTime() - m_oAppointment.getStart().getTime();
                } catch (ContactDBException e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
                        
                Date start = (Date)(m_oDateSpinner_Beginn.getValue());
                Date end = (Date)(m_oDateSpinner_Ende.getValue());
                
                if (start.after(end) || start.compareTo(end) == 0)
                {
                    end = new ScheduleDate(start).getDateWithAddedSeconds((int)duration/1000).getDate();
                    m_oDateSpinner_Ende.setValue(end);
                }
                
                
                // Check if we are in Secretary Mode and have to restrict movements
                CalendarSecretaryRelation calSecRel;
                DateRange m_oWriteDateRange = null;
                try {
                    calSecRel = m_oAppointment.getOwner().getCalendar(true).getCalendarSecretaryforUser(getGUIListener().getUser(null).getId());
                    if (calSecRel != null && calSecRel.getWriteDateRange().isDefined()){
                
                        m_oWriteDateRange =     calSecRel.getWriteDateRange();
                        
                        // Is Appointment in writeRange ?
                        if (! m_oWriteDateRange.containsDate(m_oAppointment.getStart()) ||
                            ! m_oWriteDateRange.containsDate(m_oAppointment.getEnd()))
                        {
                            m_bDateChangedListenersActive = false;
                            m_oDateSpinner_Beginn.setData(m_oAppointment.getStart());
                            m_bDateChangedListenersActive = true;
                            return;
                        }
                        
                        if (! m_oWriteDateRange.containsDate(start))
                            m_oDateSpinner_Beginn.setData(m_oWriteDateRange.getStartDate());
                            
                        if (! m_oWriteDateRange.containsDate(end))
                            m_oDateSpinner_Ende.setData(m_oWriteDateRange.getEndDate());
                           
                        start = (Date)(m_oDateSpinner_Beginn.getValue());
                        end = (Date)(m_oDateSpinner_Ende.getValue());
                        
                    }
                    
                    //m_oAppointment.setStart(start);
                    //m_oAppointment.setEnd(end);

                } catch (ContactDBException e2) {
                    // TODO Auto-generated catch block
                    logger.severe("Fehler in der Behandlung eines Ver�nderten Startdatums", e2);
                    e2.printStackTrace();
                }
                
                //getAppointmentDisplayManager().fireChangedAppointment(m_oAppointment, m_oThisScheduleDetailField);
                //saveAppointment(true);
            }
        }
    }
    
    private class EndDateChanged implements ChangeListener
    {
        public void stateChanged(ChangeEvent e)
        {
            if ((m_bDateChangedListenersActive) && (m_oAppointment != null))
            {  
                long duration = 0;
                try {
                    duration = m_oAppointment.getEnd().getTime() - m_oAppointment.getStart().getTime();
                } catch (ContactDBException e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
                Date start = (Date)(m_oDateSpinner_Beginn.getValue());
                Date end = (Date)(m_oDateSpinner_Ende.getValue());
                
                // Check if we are in Secretary Mode and have to restrict movements
                CalendarSecretaryRelation calSecRel;
                DateRange m_oWriteDateRange = null;
                try {
                    calSecRel = m_oAppointment.getOwner().getCalendar(true).getCalendarSecretaryforUser(getGUIListener().getUser(null).getId());
                    if (calSecRel != null && calSecRel.getWriteDateRange().isDefined()){
                
                        m_oWriteDateRange =     calSecRel.getWriteDateRange();
                        
                        // Is Appointment in writeRange ?
                        if (! m_oWriteDateRange.containsDate(m_oAppointment.getStart()) ||
                            ! m_oWriteDateRange.containsDate(m_oAppointment.getEnd()))
                        {
                            m_bDateChangedListenersActive = false;
                            m_oDateSpinner_Ende.setData(m_oAppointment.getEnd());
                            m_bDateChangedListenersActive = true;
                            return;
                        }
                        
                        if (! m_oWriteDateRange.containsDate(start))
                            m_oDateSpinner_Beginn.setData(m_oWriteDateRange.getStartDate());
                            
                        if (! m_oWriteDateRange.containsDate(end))
                            m_oDateSpinner_Ende.setData(new ScheduleDate(m_oWriteDateRange.getEndDate()).getDateWithAddedMinutes(-1).getDate());
                           
                        start = (Date)(m_oDateSpinner_Beginn.getValue());
                        end = (Date)(m_oDateSpinner_Ende.getValue());
                        
                    }
                    
                } catch (ContactDBException e2) {
                    // TODO Auto-generated catch block
                    logger.severe("Fehler in der Behandlung eines Ver�nderten Enddatums", e2);
                    e2.printStackTrace();
                }
                
                
                
                // Fix Start-After-End Situations ...
                if (start.after(end) || start.compareTo(end) == 0)
                {
                    start = new ScheduleDate(end).getDateWithAddedSeconds((int) -(duration/1000)).getDate();
                    m_oDateSpinner_Beginn.setValue(start);
                }      

                try
                {
                    if (m_oAppointment.isJob()){
                        // Avoid Todos's Ending at 0:00 (next day)
                        ScheduleDate startDate = new ScheduleDate(end).getDateWithAddedHours(-1);
                        ScheduleDate endDate = new ScheduleDate(end);
                        if ( ! endDate.isSameDay(startDate)){
                            //Move Todo to Last or Next day
                            if ( (duration / 2 ) < (startDate.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedMinutes(-1).getDate().getTime() - startDate.getDate().getTime())){
                                // ...-> 23:59:59
                                start = startDate.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedMinutes(-1).getDateWithAddedSeconds(- (int) (duration/1000)).getDate();
                                end = new ScheduleDate(start).getDateWithAddedSeconds((int) (duration/1000)).getDate();
                            }else{
                                // 00:00:01 -> ...
                                start = startDate.getFirstSecondOfDay().getDateWithAddedDays(1).getDate();
                                end = new ScheduleDate(start).getDateWithAddedSeconds((int) (duration/1000)).getDate();
                            }
                            m_oDateSpinner_Ende.setValue(end);
                            
                            
                        }
                        // Update start
                        m_bDateChangedListenersActive = false;
                        m_oDateSpinner_Beginn.setValue(startDate.getDate());
                        m_bDateChangedListenersActive = true;
                        
                    }
                    
                    //if (m_oAppointment.getEnd().compareTo(end) == 0)
                    //  return;
                    
                    //m_oAppointment.setStart(start);
                    //m_oAppointment.setEnd(end);
                } 
                catch (ContactDBException e1)
                {
                    logger.severe("Fehler in der Behandlung eines Ver�nderten Enddatums", e1);
                }
                //getAppointmentDisplayManager().fireChangedAppointment(m_oAppointment, m_oThisScheduleDetailField);  
                //saveAppointment(true);
            }
        }
    }
    
    
    private class CompletedCheckActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            updateCompletedSlider();
        }
    }
    
    private class ReminderButtonClicked implements ActionListener
    {       
        public void actionPerformed(ActionEvent e)
        {
            if (reminderDialog == null)
                reminderDialog = new ReminderDialog(getGUIListener(), ApplicationServices.getInstance().getMainFrame().getFrame(), "Automatische Terminbenachrichtigung", getAppointment());
            else{
                reminderDialog.refresh(getAppointment());
            }
        }
    }
    
    
    private void updateCompletedSlider()
    {
        if (m_bupdateCompletedSlider){
            if (m_oCheckBox_Completed.isSelected())
            {
                m_oSlider_Completed.setData(new Integer(100));
            }
            else
            {
                m_oSlider_Completed.setData(new Integer(0));
            }    
        }
    }
    
    private void updateCompletedCheck()
    {    
        m_oCheckBox_Completed.setData(new Boolean((((Integer)(m_oSlider_Completed.getData())).intValue() == 100)));
    }
    
    
    
    
    private class CompletedSliderChangeListener implements ChangeListener
    {
        public void stateChanged(ChangeEvent e)
        {
            updateCompletedCheck();
        }
    }
    
    
    
    
    private class radiobutton_repeat_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (m_oRadioButton_Repeat_None.isSelected())
            {
                setMonthlyEnabled(false);
                setDurationEnabled(false);
            }
            else if (m_oRadioButton_Repeat_Dayly.isSelected())
            {
                setMonthlyEnabled(false);
                setDurationEnabled(true);
            }
            else if (m_oRadioButton_Repeat_Weekly.isSelected())
            {
                setMonthlyEnabled(false);
                setDurationEnabled(true);
            }
            else if (m_oRadioButton_Repeat_Monthly.isSelected())
            {
                setMonthlyEnabled(true);
                setDurationEnabled(true);
            }
        }
    }
    
    
    private void setMonthlyEnabled(boolean enabled)
    {
        m_oComboBox_Repeat_Select.setEnabled(enabled);
        m_oComboBox_Repeat_Unit.setEnabled(enabled);
        m_oLabel_Repeat_All.setEnabled(enabled);
        m_oComboBox_Repeat_Months.setEnabled(enabled);
        m_oLabel_Repeat_Months.setEnabled(enabled);    
    }
    
    private void setDurationEnabled(boolean enabled)
    {
        m_oLabel_Repeat_Start.setEnabled(enabled);
        m_oDateSpinner_Repeat_Start.setEnabled(enabled);
        m_oLabel_Repeat_End.setEnabled(enabled);
        m_oDateSpinner_Repeat_End.setEnabled(enabled);    
    }
    
    private class button_addperson_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            ExtendedAddressSelector selector = new ExtendedAddressSelector(getGUIListener(), "Adressauswahl"); 
            //AddressSelector selector = new AddressSelector(getGUIListener(), "Adressauswahl"); 
            if (AddressSelector.STATUS_OK.equals(selector.getReturnStatus()))
            {
                List addresses = selector.getSelectedAddresses();
                if (addresses != null)
                {  
                    Iterator addressiterator = addresses.iterator();
                    while(addressiterator.hasNext())
                    {          
                        Address address = (Address)(addressiterator.next());
                        if (! isTeilnehmerInList(address)) 
                        {
                            insertTeilnehmerEntry(address);
                        }
                    }
                }
                setNumTeilnehmer(m_oTeilnehmerModel.getRowCount());              
            }
        }
    }
    
    
    private class button_removeperson_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {      
            int[] rows = m_oComponentTable_Teilnehmer.getJTable().getSelectedRows();
            List ids = new ArrayList();
            
            for(int i=0; i<(rows.length); i++)
            {
                Address address = (Address)(m_oTeilnehmerModel.getRowKey(rows[i]));
                if (isAddressRemovable(address))
                    {  
                        addAddressToDisinviteList(address);          
                        ids.add(new Integer(address.getAdrNr()));
                    }
            }
            
            Iterator it = ids.iterator();
            while(it.hasNext())
            {
                int id = ((Integer)(it.next())).intValue();
                int row = getRowOfTeilnehmerInListWithID(id);        
                m_oTeilnehmerModel.removeRow(row);      
            }
            
            m_oTeilnehmerModel.fireTableDataChanged();
            setNumTeilnehmer(m_oTeilnehmerModel.getRowCount());              
        }
    }
    
    private class button_invitepersons_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                DataChangeList dirtyfields = getDirtyFields(m_oAppointment);                    
                if ((dirtyfields.containsChanges()))  
                {
                    if (!openChangeRequester(dirtyfields, m_oAppointment.isTemporary(), CHANGEREQUESTERMODE_INVITE))
                    {
                        return;
                    }
                }
                
                m_oAppointment.invite();
                reloadAppointment();        
            } 
            catch (ContactDBException e1)
            {
                ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Einladen von Teilnehmern zu einem Termin.", e1);
                logger.severe("Fehler beim Einladen von Teilnehmern zu einem Termin.", e1);
            }
        }
    }
    
    
    public Appointment getAppointment()
    {
        return m_oAppointment;
    }
    
    
    public void deleteAppointment()
    {
        if (m_oAppointment != null)
        {
            ScheduleField schedulefield = (ScheduleField)(getWidgetPool().getController("CONTROLER_SCHEDULEPANEL"));
            if (schedulefield != null)
            {
                schedulefield.deleteAppointment(m_oAppointment.getId());
            }
        }
    }
    
    
    private void refreshPanelsChanged()
    {
        if (m_oAppointment != null)
        {
            // refresh other displays...
            getAppointmentDisplayManager().fireChangedAppointment(m_oAppointment, this);
            // end refresh
        }
    }
    
    
    private void refreshPanelsDeleted()
    {
        if (m_oAppointment != null)
        {
            // refresh other displays...
            //getAppointmentDisplayManager().fireRemovedAppointment(m_oAppointment, this);      
            // end refresh
        }
    }
    
    public void reloadAppointment()
    {
        if (m_oAppointment != null)
        {      
            Integer id = new Integer(m_oAppointment.getId());
            setAppointment(null);
            try
            {
                Appointment appointment = getGUIListener().getCalendarCollection().getAppointment(id);
                setAppointment(appointment);
                getAppointmentDisplayManager().fireChangedAppointment(m_oAppointment, this);        
            } 
            catch (ContactDBException e)
            {
                logger.severe("Fehler beim Versuch einen Termin neu zu laden.", e);        
            }
        }
    }
    
    /**
     * FIXME: This Method saves the appointment - needs review, savedirty is always "true"...
     * 
     * @param savedirty
     */
    public void saveAppointment(boolean savedirty)
    {
        //System.out.println("saveAppointment()...");    
        boolean stillExists = true;
        
        if (m_oAppointment != null)
        {
            
            try {
                stillExists = m_oAppointment.stillExists();
            } catch (ContactDBException e) {
                logger.severe("Failed saving an appointment: doesn't exist in DB", e);
            }
            
            if (stillExists){
                
                if (isAppointmentDirty(m_oAppointment) || savedirty)
                {   
                    boolean mustrefreshhistory = getAppointment(m_oAppointment);
                    try
                    {
                        m_oAppointment.setTemporary(false);
                        if (savedirty)
                            ((IEntity)m_oAppointment).setDirty(true);
                        
                        if (m_bSaveInviationOnly){
                            // Save only own  OR RELEASED event status
                            AppointmentCalendarRelation acr = m_oAppointment.getRelation(getGUIListener().getUser(null).getCalendar(false));
                            if (acr != null){
                                getGUIListener().setEventStatus(getGUIListener().getUser(null).getId(), m_oAppointment.getId(),acr.getRequestStatus());
                            }else{
                                // find Releases
                                // TODO: seteEvent for released user
                                Iterator CalenderIter = getGUIListener().getCalendarCollection().getCollection().iterator();
                                while (CalenderIter.hasNext()){
                                    Calendar cal = (Calendar) CalenderIter.next();
                                    if (cal.getUser().getId() != getGUIListener().getUser(null).getId()){
                                        if ( m_oAppointment.getCalendars().contains(cal)){
                                            AppointmentCalendarRelation aACR = m_oAppointment.getRelation(cal);
                                            getGUIListener().setEventStatus(cal.getUser().getId(), m_oAppointment.getId(),aACR.getRequestStatus());
                                        }
                                    }
                                }
                            }
                        }else{
                            PersistenceManager.commit(m_oAppointment);                      
                        }
                        
                        
                        if (mustrefreshhistory) 
                        {
                            getControlListener().refreshHistory();
                        }
                        
                        refreshPanelsChanged();          
                    } 
                    catch (ContactDBException e1)
                    {
                        logger.severe("Fehler beim Speichern eines Appointments.", e1);
                    }
                } 
                else 
                {
                    if (m_oAppointment.isTemporary())
                    {  
                        logger.log(Level.FINE, "saveAppointment() m_oAppointment is NOT DIRTY... deleting Temp-Flag");
                        try
                        {
                            m_oAppointment.setTemporary(false);
                            if (m_bSaveInviationOnly){
//                               Save only own  OR RELEASED event status
                                AppointmentCalendarRelation acr = m_oAppointment.getRelation(getGUIListener().getUser(null).getCalendar(false));
                                if (acr != null){
                                    getGUIListener().setEventStatus(getGUIListener().getUser(null).getId(), m_oAppointment.getId(),acr.getRequestStatus());
                                }else{
//                                   find Releases
                                    // TODO: seteEvent for released user
                                    Iterator CalenderIter = getGUIListener().getCalendarCollection().getCollection().iterator();
                                    while (CalenderIter.hasNext()){
                                        Calendar cal = (Calendar) CalenderIter.next();
                                        if (cal.getUser().getId() != getGUIListener().getUser(null).getId()){
                                            if ( m_oAppointment.getCalendars().contains(cal)){
                                                AppointmentCalendarRelation aACR = m_oAppointment.getRelation(cal);
                                                getGUIListener().setEventStatus(cal.getUser().getId(), m_oAppointment.getId(),aACR.getRequestStatus());
                                            }
                                        }
                                    }
                                }
                            }else{
                                PersistenceManager.commit(m_oAppointment);                      
                            }
                        } 
                        catch (ContactDBException e)
                        {
                            logger.severe("Fehler beim Speichern eines Appointments um Temp-Flag zu l�schen.", e);
                        }
                    }
                    else logger.log(Level.FINE, "saveAppointment() m_oAppointment is NOT DIRTY...");        
                }
                
                // Update SEPS
                List allSeps = SEPRegistry.getInstance().getSEPSforAppointmentID(m_oAppointment.getId()); 
                if(allSeps != null && allSeps.size() > 0){
                    for (Iterator myIter = allSeps.iterator();myIter.hasNext();){
                        ScheduleEntryPanel aSEP = (ScheduleEntryPanel) myIter.next();
                        aSEP.setAppointment(m_oAppointment);
                    }
                }
            }else{
                getAppointmentDisplayManager().fireRemovedAppointment(m_oAppointment,null);
                setAppointment(null);  
                // Appointment was deleted...
            }
            
                
        } else logger.log(Level.FINE, "saveAppointment() m_oAppointment == null");    
    }
    
    public void resetAppointment()
    {
        setAppointment(m_oAppointment);    
    }
    
    
    private class button_delete_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            boolean stillExists = true;

            try {
                stillExists = m_oAppointment.stillExists();
            } catch (ContactDBException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            
            if (stillExists){
                deleteAppointment();
            }else{
                // Appointment was deleted..
                getAppointmentDisplayManager().fireRemovedAppointment(m_oAppointment,null);
                setAppointment(null);  
            }
        }
    }
    
    private class button_refresh_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {   
            boolean stillExists = true;
            
            try {
                stillExists = m_oAppointment.stillExists();
            } catch (ContactDBException ex) {
                ex.printStackTrace();
            }
            
            if (stillExists){
                m_oAppointment.invalidateRelations();
                boolean istemp = m_oAppointment.isTemporary();      
                DataChangeList dirtyfields = getDirtyFields(m_oAppointment);                          
                if ((dirtyfields.containsChanges()) || (istemp))  
                {
                    openChangeRequester(dirtyfields, istemp, CHANGEREQUESTERMODE_RELOAD);
                }               
                reloadAppointment();
            }else{
                // Appointment was deleted..
                getAppointmentDisplayManager().fireRemovedAppointment(m_oAppointment,null);
                setAppointment(null);  
            }
        }
    }
    
    private class button_save_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            saveAppointment(true);
        }
    }
    
    private class button_reset_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            resetAppointment();
        }
    }
    
    
    private ImageIcon getIconForUser(User user)
    {
        try
        {
            return (ImageIcon)SwingIconFactory.getInstance().getIcon(user.getLoginName() + ".gif");
        } 
        catch (ContactDBException e)
        {
            logger.severe("Fehler beim Versuch den Login-Namen eines Benutzers zu ermitteln um ein personalisiertes Icon zuzuordnen.", e);
            return null;
        }      
    }
    
    
    
    private ImageIcon getIconForAddress(Address address)
    {
        try
        {
            User user = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO().getAssociatedUser(address);
            if (user != null)
            {
                ImageIcon icon = getIconForUser(user);
                if (icon == null) icon = m_oIcon_SystemUser;  
                return icon;
            }
        } catch (ContactDBException e)
        {
            logger.severe("Konnte nicht ermitteln ob Teilnehmer ein Systembenutzer ist um ein entsprechendes Icon auszuw�hlen", e);
        }
        return m_oIcon_ExternalUser;
    }
    
    private String getTypeNameForAddress(Address address)
    {
        String name = "externer Teilnehmer";
        try
        {
            User user = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO().getAssociatedUser(address);
            if (user != null)
            {
                try
                {
                    String vorname = user.getAttribute(User.KEY_FIRST_NAME); 
                    String nachname = user.getAttribute(User.KEY_LAST_NAME);         
                    String loginname = user.getAttribute(User.KEY_LOGIN_NAME);         
                    name = "Systembenutzer \"" + loginname +"\" (" + vorname + " " + nachname + ")";
                }
                catch(Exception e)
                {
                    //TODO: das darf hier nicht sein !!!
                    logger.severe("interner Fehler beim Abrufen von Benutzerdaten", e);
                    name = "unbekannter Systembenutzer";          
                }
                
            }
        } catch (ContactDBException e)
        {
            logger.severe("Konnte nicht ermitteln ob Teilnehmer ein Systembenutzer ist um eine entsprechende Bezeichnung auszuw�hlen", e);
            name = "unbekannter Teilnehmer";
        }
        return name;
    }
    
    private boolean isTeilnehmerInList(Address address)
    {
        return (getRowOfTeilnehmerInListWithID(address.getAdrNr()) != -1);
    }
    
    
    private int getRowOfTeilnehmerInListWithID(int addressid)
    {
        for(int i=0; i<(m_oTeilnehmerModel.getRowCount()); i++)
            {
                if ( ((Address)(m_oTeilnehmerModel.getRowKey(i))).getAdrNr() == addressid ) return i;
            }
        return -1;
    }
    
    private TarentWidgetIconComboBox.IconComboBoxEntry getEntryOfAnwesenheit(int anwesenheit)
    {
        for(int i=0; i<(m_oIconComboBox_Anwesenheit.getItemCount()); i++)
        {
            TarentWidgetIconComboBox.IconComboBoxEntry entry = (TarentWidgetIconComboBox.IconComboBoxEntry)(m_oIconComboBox_Anwesenheit.getItemAt(i));
            if (entry.getKey().equals(new Integer(anwesenheit))) return entry;
        }
        return null;    
    }
    
    private void setAnwesenheit(int row, int anwesenheit)
    {
        TarentWidgetIconComboBox.IconComboBoxEntry entry = getEntryOfAnwesenheit(anwesenheit);
        if (entry != null) m_oTeilnehmerModel.setValueAt(entry, row, 2);
        else
        {
            getGUIListener().getLogger().warning("Bad Level at row " + row + "! Using default LEVEL_REQUIRED");
            entry = getEntryOfAnwesenheit(AppointmentRequest.LEVEL_REQUIRED);
            m_oTeilnehmerModel.setValueAt(entry, row, 2);
        }
    }
    
    
    
    
    
    private void insertTeilnehmerEntry(Address address)
    {
        removeAddressFromDisinviteList(address);                
        
        //System.out.println("insertTeilnehmerEntry(" + address + ")");        
        ImageIcon icon = getIconForAddress(address);
        String type = getTypeNameForAddress(address);
        try
        {
            
            String name = address.getShortAddressLabel();
            Object[] objects = new Object[5];     
            
            AppointmentRequest relation = getRelationOfAddress(address);
            
            if (relation != null)
            {
                String tmp = relation.getAttribute(AppointmentRequest.KEY_DESCRIPTION);
                if (tmp != null){
                    objects[4] = ( tmp.trim().equals("")? "Bemerkung":tmp);
                }else{
                    objects[4] = "Bemerkung";
                }
                
            }else{
                objects[4] = "Bemerkung";               
            }
            
            
            objects[0] = icon;
            objects[1] = name;
            objects[2] = ""; //getEntryOfAnwesenheit(AppointmentRequest.LEVEL_REQUIRED);
            objects[3] = "";    
            
            //objects[5] = new Boolean(true);
            
            int row = m_oTeilnehmerModel.addRow(objects, address);      
            
            m_oTeilnehmerModel.setTooltipAt("Typ des Teilnehmers: " + type, row, 0);
            m_oTeilnehmerModel.setTooltipAt("Teilnehmer: " + name, row, 1);
            m_oTeilnehmerModel.setTooltipAt("die Verf�gbarkeit des Teilnehmers", row, 2);
            m_oTeilnehmerModel.setTooltipAt("der Einladestatus", row, 3);
            m_oTeilnehmerModel.setTooltipAt("hier k�nnen sie einen Kommentar eingeben.", row, 4);
            //m_oTeilnehmerModel.setTooltipAt("soll dieser Teilnehmer an seine Teilname am Termin erinnert werden", row, 5);
            
            refreshTeilnehmer(address, row);
            
            m_oTeilnehmerModel.fireTableDataChanged();    
        } 
        catch (ContactDBException e)
        {    
            logger.severe("Fehler beim Hinzuf�gen eines Teilnehmers in die Liste", e);
        }
    }
    
    private void refreshTeilnehmer(Address address)
    {
        int row;
        row = getRowOfTeilnehmerInListWithID(address.getAdrNr());
        refreshTeilnehmer(address, row);
    }
    
    
    private Address m_oCurrentUserAddress = null;
    
    private Address getCurrentUserAddress()
    {
        if (m_oCurrentUserAddress == null)
        {  
            User me = getGUIListener().getUser(getGUIListener().getCurrentUser());
            if (me != null)
            {
                try
                {
                    m_oCurrentUserAddress = me.getAssociatedAddress();
                } 
                catch (ContactDBException e)
                {
                    logger.severe("Fehler beim Ermitteln der Addresse des aktuellen Benutzers", e);      
                }
            }
        }
        return m_oCurrentUserAddress;
    }
    
    private int m_iCurrentUserAddressID = -1;
    
    private int getCurrentUserAddressID()
    {
        if (m_iCurrentUserAddressID == -1)
        {
            Address address = getCurrentUserAddress();
            if (address != null)
            {  
                m_iCurrentUserAddressID = address.getId();
            }
        }
        return m_iCurrentUserAddressID;
    }
    
    private void refreshTeilnehmer(Address address, int row)
    {
        //System.out.println("refreshTeilnehmer(" + address + ", " + row + ")");
        
        try
        {
            if (row !=-1)
            {  
                int status = -1;
                int level = -1;
                
                AppointmentRequest relation = getRelationOfAddress(address);
                if (relation != null)
                {  
                    status = relation.getRequestStatus();
                    if (status <= 0)
                    {
                        getGUIListener().getLogger().info("Bad Status at row " + row + "! No status-relation for "+ address.getShortAddressLabel() + ". Using default STATUS_NEEDS_ACTION");
                        status = AppointmentRequest.STATUS_NEEDS_ACTION;
                    }
                    else if (status > 7)
                    {
                        getGUIListener().getLogger().info("Bad Status at row " + row + "! No status-relation for "+ address.getShortAddressLabel() + ". Using default STATUS_CONFIRMED");
                        status = AppointmentRequest.STATUS_CONFIRMED;
                    }
                    
                    int type = relation.getRequestType();
                    level = relation.getRequestLevel();
                    
                    boolean invitor = m_bIAmInvitor;
                    boolean lineislocked = false;
                    
                    // Its Me and My Calendar active?
                    boolean itsMe = (getCurrentUserAddressID() == address.getId() 
                                 && getGUIListener().getCalendarCollection().getCollection().contains(getGUIListener().getUser(null).getCalendar(false)));
                    
                    // Release?
                    boolean itsRelease = false;
                    
                    Integer aUserID = address.getAssociatedUserID();
                    if (aUserID.intValue() != 0)
                        itsRelease = getGUIListener().getCalendarCollection().getCalendar( UserImpl.getUserFromID(aUserID).getCalendar(false).getId()) != null;
                    
                    TeilnehmerStatus teilnehmerstatus = new TeilnehmerStatus();
                    if (status == AppointmentRequest.STATUS_NEEDS_ACTION) teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_NEEDS_ACTION);
                    
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_DECLINED);
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_TENTATIVE);
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_CONFIRMED);
                    
                    if (status != AppointmentRequest.STATUS_NEEDS_ACTION && 
                        status != AppointmentRequest.STATUS_DECLINED &&
                        status != AppointmentRequest.STATUS_TENTATIVE &&
                        status != AppointmentRequest.STATUS_CONFIRMED){

                        teilnehmerstatus.insertPossibleStatusToTeilnehmer(status);
                    }
                        
                            
                    
                    if (itsMe || itsRelease)
                    {
                        lineislocked = false;
                        
                        if (status != AppointmentRequest.STATUS_NEEDS_ACTION) teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_SENT);
                                                
                        if (!itsMe && itsRelease && !getGUIListener().mayUserWriteEvent(new Integer(m_oAppointment.getId()))){
                            lineislocked = true;
                        }
                        
                            
                    }
                    else
                    {
                        if (status != AppointmentRequest.STATUS_NEEDS_ACTION) teilnehmerstatus.insertPossibleStatusToTeilnehmer(status);  
                        lineislocked = true;
                    }
                    
                    
                    
                    
                    setAnwesenheitEditable(row, invitor);
                    setStatusEditable(row, !lineislocked);
                    setBemerkungEditable(row, invitor);
                    //setErinnerungEditable(row, invitor);
                    
                    setAnwesenheit(row, level);
                    teilnehmerstatus.attachToTableRow(row);
                    setTeilnehmerStatusOfRow(row, status);
                    //teilnehmerstatus.selectStatus(row, status);
                }
                else
                {
                    // DEFAULT nach dem Einfgen setzen... 
                    TeilnehmerStatus teilnehmerstatus = new TeilnehmerStatus();
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_NEEDS_ACTION);
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_DECLINED);
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_TENTATIVE);
                    teilnehmerstatus.insertPossibleStatusToTeilnehmer(AppointmentRequest.STATUS_CONFIRMED);
                    teilnehmerstatus.attachToTableRow(row);
                    
                    setTeilnehmerStatusOfRow(row, AppointmentRequest.STATUS_NEEDS_ACTION);
                    //teilnehmerstatus.selectStatus(row, AppointmentRequest.STATUS_NEEDS_ACTION);
                    
                    setAnwesenheit(row, AppointmentRequest.LEVEL_REQUIRED);
                    
                    setAnwesenheitEditable(row, true);
                    setStatusEditable(row, true);
                    setBemerkungEditable(row, true);
                    //setErinnerungEditable(row, true);                    
                }         
            }
        } 
        catch (ContactDBException e)
        {
            logger.severe("Fehler beim Setzen des Status eines Teilnehmers in der Liste", e);
        }
    }
    
    
    private void setAnwesenheitEditable(int row, boolean editable)
    {
        m_oTeilnehmerModel.setCellEditable(row, 2, editable);
    }
    
    private void setStatusEditable(int row, boolean editable)
    {
        m_oTeilnehmerModel.setCellEditable(row, 3, editable);
    }
    
    private void setBemerkungEditable(int row, boolean editable)
    {
        m_oTeilnehmerModel.setCellEditable(row, 4, editable);
    }
    
    private void setErinnerungEditable(int row, boolean editable)
    {
        m_oTeilnehmerModel.setCellEditable(row, 5, editable);
    }
    
    
    
    private int getTeilnehmerStatusOfRow(int row)
    {
        TarentWidgetMutableComponentTable.ComboCellEditorItem item = null;
        try {
            item = (TarentWidgetMutableComponentTable.ComboCellEditorItem)(m_oTeilnehmerModel.getValueAt(row, 3));
        } catch (ClassCastException e) {
            //return -1;
        }
        if (item != null) return ((Integer)(item.getKey())).intValue();
        return -1;
    }
    
    
    //  private TeilnehmerStatus getTeilnehmerStatusOfRow(int row)
    //  {
    //System.out.println("getTeilnehmerStatusOfRow(" + row + ")");    
    //    Object rowkey = m_oTeilnehmerModel.getUserRowKey(row);
    //System.out.println("getTeilnehmerStatusOfRow(" + row + ") rowkey=" + rowkey);    
    //    if (rowkey == null) return null;
    //    if (rowkey instanceof TeilnehmerStatus) 
    //    {
    //      TeilnehmerStatus status = (TeilnehmerStatus)rowkey;      
    //      return status;
    //    }
    //    else 
    //    {
    //      getGUIListener().getLogger().severe("UserRowKey von Zeile " + row + " der Teilnehmer-Tabelle ist nicht vom Typ TeilnehmerStatus! Inhalt=" + rowkey);
    //      return null;
    //    }
    //  }
    
    
    
    
    private String getNameOfEinladeStatus(int status)
    {
        switch(status)
        {
        
        case(AppointmentRequest.STATUS_ACCEPTED): 
            return ("angenommen");
        
        case(AppointmentRequest.STATUS_COMPLETED): 
            return ("erledigt");
        
        case(AppointmentRequest.STATUS_CONFIRMED):
            return ("best�tigt");
        
        case(AppointmentRequest.STATUS_DECLINED): 
            return ("abgelehnt");
        
        case(AppointmentRequest.STATUS_DELEGATED): 
            return ("delegiert");
        
        case(AppointmentRequest.STATUS_NEEDS_ACTION): 
            return ("offen");
        
        case(AppointmentRequest.STATUS_SENT): 
            return ("benachrichtigt");
        
        case(AppointmentRequest.STATUS_TENTATIVE): 
            return ("vorl�ufig");
        
        default: return "";    
        }
    }
    
    
    
    private void setTeilnehmerStatusOfRow(int row, int status)
    {
        TeilnehmerStatus teilnehmerstatus = (TeilnehmerStatus)(m_oTeilnehmerModel.getUserRowKey(row));
        if (teilnehmerstatus != null)
        {    
            Object item = teilnehmerstatus.getItemOfStatus(status);
            if (item != null)
            {
                m_oTeilnehmerModel.setValueAt(item, row, 3);        
            }
        }
    }
    
    private void setReminderStatus() throws ContactDBException{
        setReminderStatus(m_oAppointment.getReminder() != null);
    }
    
    private void setReminderStatus(boolean status){
        reminderCheckBox.setSelected(status);
    }
    
    
    private class TeilnehmerStatus
    {
        private List m_oComboCellEditorItemList;
        
        public TeilnehmerStatus()
        {
            m_oComboCellEditorItemList = new ArrayList();
        }
        
        public void clear()
        {
            m_oComboCellEditorItemList.clear();
        }
        
        public int getIndexOfStatus(int status)
        {
            for(int i=0; i<(m_oComboCellEditorItemList.size()); i++)
            {  
                TarentWidgetMutableComponentTable.ComboCellEditorItem value = (TarentWidgetMutableComponentTable.ComboCellEditorItem)(m_oComboCellEditorItemList.get(i));
                if (value != null)
                {
                    if (((Integer)(value.getKey())).intValue() == status) return i;
                }
            }
            return -1;      
        }
        
        public TarentWidgetMutableComponentTable.ComboCellEditorItem getItemOfStatus(int status)
        {
            for(int i=0; i<(m_oComboCellEditorItemList.size()); i++)
            {  
                TarentWidgetMutableComponentTable.ComboCellEditorItem value = (TarentWidgetMutableComponentTable.ComboCellEditorItem)(m_oComboCellEditorItemList.get(i));
                if (value != null)
                {
                    if (((Integer)(value.getKey())).intValue() == status) return value;
                }
            }
            return null;      
        }
        
        public int getStatusOfIndex(int index)
        {
            TarentWidgetMutableComponentTable.ComboCellEditorItem value = (TarentWidgetMutableComponentTable.ComboCellEditorItem)(m_oComboCellEditorItemList.get(index));
            if (value != null)
            {
                return ((Integer)(value.getKey())).intValue();  
            }
            return -1;           
        }
        
        public void attachToTableRow(int row)
        {
            m_oTeilnehmerModel.setUserRowKey(row, this);        
            m_oTeilnehmerModel.setItemListAt(m_oComboCellEditorItemList, row, 3);       
        }
        
        public void insertPossibleStatusToTeilnehmer(int status)
        {
            switch(status)
            {
            case(AppointmentRequest.STATUS_ACCEPTED): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("angenommen", new Integer(AppointmentRequest.STATUS_ACCEPTED)));
            break; 
            
            case(AppointmentRequest.STATUS_COMPLETED): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("erledigt", new Integer(AppointmentRequest.STATUS_COMPLETED)));
            break; 
            
            case(AppointmentRequest.STATUS_CONFIRMED):
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("best�tigt", new Integer(AppointmentRequest.STATUS_CONFIRMED)));
            break; 
            
            case(AppointmentRequest.STATUS_DECLINED): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("abgelehnt", new Integer(AppointmentRequest.STATUS_DECLINED)));
            break; 
            
            case(AppointmentRequest.STATUS_DELEGATED): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("delegiert", new Integer(AppointmentRequest.STATUS_DELEGATED)));
            break; 
            
            case(AppointmentRequest.STATUS_NEEDS_ACTION): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("offen", new Integer(AppointmentRequest.STATUS_NEEDS_ACTION)));
            break; 
            
            case(AppointmentRequest.STATUS_SENT): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("benachrichtigt", new Integer(AppointmentRequest.STATUS_SENT)));
            break; 
            
            case(AppointmentRequest.STATUS_TENTATIVE): 
                m_oComboCellEditorItemList.add(m_oComponentTable_Teilnehmer.new ComboCellEditorItem("vorl�ufig", new Integer(AppointmentRequest.STATUS_TENTATIVE)));
            break;
            
            default:
                break;
            }          
        }
    }


    private boolean isCurrentAppointmentID(int appointmentid)
    {
        if (m_oAppointment != null)
        {  
            if (appointmentid == m_oAppointment.getId())
            {
                return true;
            }
        }    
        return false;
    }
    
    
    public void changedAppointment(Appointment appointment)
    {
        if (isCurrentAppointmentID(appointment.getId()))
        {
            //System.out.println("changedAppointment().. updateAppointment()...");      
            updateAppointment(appointment);
        }
        else
        {
            try
            {
                PersistenceManager.commit(appointment);
            } 
            catch (ContactDBException e)
            {
                logger.severe("Failed commiting an invisible appointment", e);
            }
        }
    }
    
    public void addedAppointment(Appointment appointment)
    {
        setAppointment(appointment);
    }
    
    
    public void selectedAppointment(Appointment appointment)
    {  
        if (isCurrentAppointmentID(appointment.getId()))
        {
            //System.out.println("updateAppointment()...");      
            //updateAppointment(appointment);
        }
        else
        {      
            try {
                //System.out.println("setAppointment()...");      
                if( appointment.isPrivat() && ( appointment.getOwner().equals(getGUIListener().getUser(null)) == false) ){
                    setAppointment(null);
                }else{
                    setAppointment(appointment);
                }
            } catch (ContactDBException e) {
                logger.severe("failed after appointment selected", e);
            }
        }    
    }
    
    public void removedAppointment(Appointment appointment)
    {    
        if (isCurrentAppointmentID(appointment.getId()))
        {
            setAppointment(null);
        }
    }
    
    public Appointment getAppointment(int appointmentid)
    {
        if (m_oAppointment != null)
        {
            if (m_oAppointment.getId() == appointmentid) return m_oAppointment; 
        }
        return null;
    }
    
    public void showReminderPanel(boolean show){
        reminderPanel.setVisible(show);
    }
    
    public void showReminderPanel() throws ContactDBException{
        User currentUser = getGUIListener().getUser(null);
        Calendar userCalendar = currentUser.getCalendar(false);
        int currentUsersAddressId = currentUser.getAssociatedAddress().getId();
        boolean privateCalendarIsVisible = getGUIListener().getCalendarCollection().getCalendar(userCalendar.getId()) != null;
        boolean userIsOwnerOfAppointment = getAppointment().getOwner().getId() == currentUser.getId();
        boolean userIsRelatedToAppointment = false;
        if (!userIsOwnerOfAppointment)
            userIsRelatedToAppointment = m_oAppointment.getRelation(userCalendar) != null;
        
        showReminderPanel(privateCalendarIsVisible && (userIsOwnerOfAppointment || userIsRelatedToAppointment));
        
    }
    
    
    
    //  private Calendar getCurrentCalendar()
    //  {
    //      return getGUIListener().getCalendarCollection().getCalendar(getGUIListener().getCurrentCalendarID()); 
    //  }
    
    public void changedCalendarCollection()
    {   
        DataChangeList dirtyfields = getDirtyFields(m_oAppointment);                          
        if (dirtyfields.containsChanges())  
        {
            openChangeRequester(dirtyfields, false, CHANGEREQUESTERMODE_SETAPPOINTMENT);
        }
        //setAppointment(null);
    }
    
    public void setViewType(String viewtype)
    {
        // not interested in this...
    }
    
    public void setVisibleInterval(ScheduleDate start, ScheduleDate end)
    {
        // not interested in this...    
    }
    
    // ---------------------------------------------------------------------------------------------
    
    private JPopupMenu createPopUpMenu(Address address, int column)
    {
        JPopupMenu oPopupMenu = new JPopupMenu();
        
        String name = "unbekannt";
        name = address.getShortAddressLabel();
        
        name = XmlUtil.escape(name);
        
        int selectedrows = m_oComponentTable_Teilnehmer.getJTable().getSelectedRowCount();
        if (selectedrows > 1 && m_bIAmInvitor)
        {
            JMenuItem menuItemDISINVITEALL = new JMenuItem("<html>alle " + selectedrows + " selektierten Teilnehmer ausladen...</html>");      
            menuItemDISINVITEALL.setFont(m_oFont);
            menuItemDISINVITEALL.setIcon(m_oIcon_Disinvite);
            menuItemDISINVITEALL.addActionListener(new MenuItem_DISINVITEALL_clicked());
            oPopupMenu.add(menuItemDISINVITEALL);
            
            JMenuItem menuItemMAILTOALL = new JMenuItem("<html>eine eMail an alle " + selectedrows + " selektierten Teilnehmer versenden...</html>");      
            menuItemMAILTOALL.setFont(m_oFont);
            menuItemMAILTOALL.setIcon(m_oIcon_Disinvite);
            menuItemMAILTOALL.addActionListener(new MenuItem_MAILTOALL_clicked());
            oPopupMenu.add(menuItemMAILTOALL);
            
            oPopupMenu.add(new JPopupMenu.Separator());
            
            JMenuItem menuItemREMOVEALL = new JMenuItem("<html>alle " + selectedrows + " selektierten Teilnehmer entfernen...</html>");      
            menuItemREMOVEALL.setFont(m_oFont);    
            menuItemREMOVEALL.setIcon(m_oIcon_Remove);
            menuItemREMOVEALL.addActionListener(new MenuItem_REMOVEALL_clicked());
            oPopupMenu.add(menuItemREMOVEALL);      
        }
        else
        {
            // Nur bei gltigen Adresse soll das Hinspringen erm�glicht werden.
            if (address.getAdrNr() != -1) {                
                JMenuItem menuItemJUMP = new JMenuItem("<html>zu Addresseintrag \"<i>" + name + "</i>\" springen...</html>");
                menuItemJUMP.setFont(m_oFont);
                menuItemJUMP.setIcon(m_oIcon_Jump);
                menuItemJUMP.addActionListener(new MenuItem_JUMP_clicked(address));
                oPopupMenu.add(menuItemJUMP);
            }

            if (isAddressRemovable(address))
            {
                JMenuItem menuItemDISINVITE = new JMenuItem("<html>Teilnehmer \"<i>" + name + "</i>\" ausladen...</html>");      
                menuItemDISINVITE.setFont(m_oFont);
                menuItemDISINVITE.setIcon(m_oIcon_Disinvite);
                menuItemDISINVITE.addActionListener(new MenuItem_DISINVITE_clicked(address));
                oPopupMenu.add(menuItemDISINVITE);
            }
            
            JMenuItem menuItemSENDMAIL = new JMenuItem("<html>eine eMail an Teilnehmer \"<i>" + name + "</i>\" versenden...</html>");          
            menuItemSENDMAIL.setFont(m_oFont);    
            menuItemSENDMAIL.setIcon(m_oIcon_SendMail);
            menuItemSENDMAIL.addActionListener(new MenuItem_SENDMAIL_clicked(address));
            oPopupMenu.add(menuItemSENDMAIL);
            
            if (isAddressRemovable(address))
            {
                oPopupMenu.add(new JPopupMenu.Separator());
                
                JMenuItem menuItemREMOVE = new JMenuItem("<html>den Teilnehmer \"<i>" + name + "</i>\" entfernen...</html>");          
                menuItemREMOVE.setFont(m_oFont);    
                menuItemREMOVE.setIcon(m_oIcon_Remove);
                menuItemREMOVE.addActionListener(new MenuItem_REMOVE_clicked(address));
                oPopupMenu.add(menuItemREMOVE);
            }
        }
        
        return oPopupMenu;
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    private class MenuItem_JUMP_clicked implements ActionListener
    {
        private Address m_oAddress;
        
        public MenuItem_JUMP_clicked(Address address)
        {
            m_oAddress = address;
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            
            if (!getControlListener().showAddress(m_oAddress))
            {
                // Adresse war gar nicht in der aktuellen Adressauswahl...
                Toolkit.getDefaultToolkit().beep();
            }
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
    
    private class MenuItem_REMOVE_clicked implements ActionListener
    {
        private Address m_oAddress;
        
        public MenuItem_REMOVE_clicked(Address address)
        {
            m_oAddress = address;
        }
        
        public void actionPerformed(ActionEvent e)
        {
            int row = getRowOfTeilnehmerInListWithID(m_oAddress.getId());
            if (row != -1) 
            {                
                if (isAddressRemovable(m_oAddress)) 
                {  
                    addAddressToDisinviteList(m_oAddress);          
                    m_oTeilnehmerModel.removeRow(row);
                }
            }
            m_oTeilnehmerModel.fireTableDataChanged();
            setNumTeilnehmer(m_oTeilnehmerModel.getRowCount());                    
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    
    private class MenuItem_DISINVITE_clicked implements ActionListener
    {
        private Address m_oAddress;
        
        public MenuItem_DISINVITE_clicked(Address address)
        {
            m_oAddress = address;      
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            //System.out.println("MenuItem_DISINVITE_clicked m_oAddress=" + m_oAddress);
            addAddressToDisinviteList(m_oAddress);
            disinviteTeilnehmerList(m_oAppointment);
            refreshTeilnehmer(m_oAddress);
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    private class MenuItem_SENDMAIL_clicked implements ActionListener
    {
        private Address m_oAddress;
        
        public MenuItem_SENDMAIL_clicked(Address address)
        {
            m_oAddress = address;
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            getGUIListener().userRequestPost(ApplicationStarter.POST_EMAIL, m_oAddress);
            
            String termintext ="";
            try
            {        
                String name = m_oAddress.getShortAddressLabel();
                String subject = m_oAppointment.getAttribute(Appointment.KEY_SUBJECT);
                
                termintext += "\"" + name + "\"";
                termintext += (m_oAppointment.isJob()) ? " der Aufgabe " : " des Termins ";
                termintext += "\"" + subject + "\"";
            } catch (ContactDBException e1) {termintext ="eines unbekannten Ereignisses";}
            
            getGUIListener().userRequestCreateContactEntryDialog("EMail an Teilnehmer " + termintext + " geschrieben", "", new Date(), 0, false, Contact.CATEGORY_MAILING, Contact.CHANNEL_EMAIL, 0, 0, null, null);      
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    private class MenuItem_DISINVITEALL_clicked implements ActionListener
    {
        public MenuItem_DISINVITEALL_clicked()
        {
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            logger.log(Level.INFO, "MenuItem_DISINVITEALL_clicked");
            
            List list = new ArrayList();
            Addresses addresses = new NonPersistentAddresses();
            
            int[] selectedrows = m_oComponentTable_Teilnehmer.getJTable().getSelectedRows();
            for(int i=0; i<selectedrows.length; i++)
            {  
                Address address = (Address)(m_oTeilnehmerModel.getRowKey(selectedrows[i]));
                list.add(address);
                (addresses).addAddress(address);
            }
            
                  
            AddressRequester requester = new AddressRequester(getGUIListener().getFrame(), "Sicherheitsnachfrage", (ImageIcon)SwingIconFactory.getInstance().getIcon("warning.gif"), "Sind sie sicher das sie diese " + addresses.getSize() + " Teilnehmer ausladen m�chten?", addresses, new String[] {"Ja", "Nein"}, 1);
            if (requester.getAnswer() == 0)
            {  
                if (!addAddressesToDisinviteList(list))
                {
                    ApplicationServices.getInstance().getCommonDialogServices().publishError("Einige Teilnehmer konnten nicht ausgeladen werden!");
                }
                
                for(int i=0; i<selectedrows.length; i++)
                {  
                    Address address = (Address)(m_oTeilnehmerModel.getRowKey(selectedrows[i]));
                    refreshTeilnehmer(address, selectedrows[i]);
                }
            }      
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    private class MenuItem_MAILTOALL_clicked implements ActionListener
    {
        public MenuItem_MAILTOALL_clicked()
        {
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            
            Addresses addresses = new NonPersistentAddresses();
            
            int[] selectedrows = m_oComponentTable_Teilnehmer.getJTable().getSelectedRows();
            for(int i=0; i<selectedrows.length; i++)
            {  
                Address address = (Address)(m_oTeilnehmerModel.getRowKey(selectedrows[i]));
                addresses.addAddress(address);
            }
            
            AddressRequester requester = new AddressRequester(getGUIListener().getFrame(), "Sicherheitsnachfrage", (ImageIcon)SwingIconFactory.getInstance().getIcon("warning.gif"), "Sind sie sicher das sie an alle diese " + addresses.getSize() + " Teilnehmer eine eMail versenden m�chten?", addresses, new String[] {"Ja", "Nein"}, 1);
            if (requester.getAnswer() == 0)
            {
                logger.log(Level.INFO, "UNIMLEMENTED: send Mail to all");
                
                for(int i=0; i<selectedrows.length; i++)
                {  
                    Address address = (Address)(m_oTeilnehmerModel.getRowKey(selectedrows[i]));
                    refreshTeilnehmer(address, selectedrows[i]);
                }
            }
            
        }    
    }
    
    
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    private class MenuItem_REMOVEALL_clicked implements ActionListener
    {
        public MenuItem_REMOVEALL_clicked()
        {
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            logger.log(Level.INFO, "MenuItem_REMOVEALL_clicked");
            
            int[] selectedrows = m_oComponentTable_Teilnehmer.getJTable().getSelectedRows();
            List ids = new ArrayList();
            
            for(int i=0; i<(selectedrows.length); i++)
            {
                Address address = (Address)(m_oTeilnehmerModel.getRowKey(selectedrows[i]));
                if (isAddressRemovable(address))
                    {  
                        addAddressToDisinviteList(address);
                        ids.add(new Integer(address.getAdrNr()));
                    }
            }
            
            for(int i=0; i<(ids.size()); i++)
            {
                int row = getRowOfTeilnehmerInListWithID(((Integer)(ids.get(i))).intValue());
                m_oTeilnehmerModel.removeRow(row);        
            }
            
            m_oTeilnehmerModel.fireTableDataChanged();
            setNumTeilnehmer(m_oTeilnehmerModel.getRowCount());              
        } 
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    
    class PopupListener extends MouseAdapter 
    {
        public void mousePressed(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        public void mouseReleased(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) 
        {
            if (e.isPopupTrigger()) 
            {
                Point p = e.getPoint();
                int row = m_oComponentTable_Teilnehmer.getJTable().rowAtPoint(p);
                int visiblecol = m_oComponentTable_Teilnehmer.getJTable().columnAtPoint(p);
                int col = m_oComponentTable_Teilnehmer.getJTable().convertColumnIndexToModel(visiblecol);        
                
                Address address = (Address)(m_oTeilnehmerModel.getRowKey(row));
                JPopupMenu menu = createPopUpMenu(address, col);         
                menu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }
    
    
    
    // ---------------------------------------------------------------------------------------------
    
    
    

    // ------------------------------------------------------------------------------------------  
    
    
    
    // ------------------------------------------------------------------------------------------  
    
    
    
    // ------------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------------  
    // ------------------------------------------------------------------------------------------  
    

    
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridActive(boolean)
     */
    public void setGridActive(boolean usegrid)
    {
        // TODO Auto-generated method stub
        
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridHeight(int)
     */
    public void setGridHeight(int seconds)
    {
        // TODO Auto-generated method stub
        
    }

    /** Handles export event.*/
    public void clickedExportButton() {
    }    

    /** Handles new appointment event.*/
    public void clickedNewAppointmentButton() {
    }

    /** Handles new task event.*/
    public void clickedNewTaskButton() {
    }
    
}
