/*
 * Created on 22.03.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.swing.JLayeredPane;

import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentPanelInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetSeparator;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerErrorEvent;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerErrors;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerManager;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerPool;
import org.evolvis.nuyo.datacontainer.DataContainer.ErrorEventListener;
import org.evolvis.nuyo.datacontainer.ErrorDisplay.BubblePanel;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.parser.DataContainerParser;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Mail;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.gui.fieldhelper.AddressField;
import org.evolvis.nuyo.gui.fieldhelper.AddressPluginData;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.gui.fieldhelper.DataContainerField;
import org.evolvis.nuyo.gui.fieldhelper.FieldPool;
import org.evolvis.nuyo.gui.fieldhelper.GenericExtendedField;
import org.evolvis.nuyo.gui.fieldhelper.MailPluginData;
import org.evolvis.nuyo.gui.fieldhelper.UserPluginData;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.nuyo.util.parser.GUIParseError;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author niko
 *
 */
public class AdvancedPanelInserter implements PanelInserter, ErrorEventListener, Validatable, DataContainerPool {

    private static final TarentLogger logger = new TarentLogger(AdvancedPanelInserter.class);

    private ContainerProvider visibleContainerProvider;
    private DataContainerManager dataContainerManager;
    private DataContainerParser dataContainerParser;
    private ControlListener controlListener;
    private GUIListener guiListener;
    private BubblePanel bubblePanel;
    private FieldPool fieldPool;
    private List usedElementsList;
    private List headlinesList;
    private Map containerProvidersMap;
    private Map usedDataContainers;
    private Map containerElements;
    private Map containersMap;

    private Timer timer;
    private TimerEventThread timerEventThread;

    public AdvancedPanelInserter( GUIListener oGUIListener, ControlListener oControlListener) {
        guiListener = oGUIListener;
        controlListener = oControlListener;
        usedElementsList = new ArrayList();
        containersMap = new HashMap();
        containerProvidersMap = new HashMap();
        containerElements = new HashMap();
        headlinesList = new ArrayList();
        dataContainerManager = new DataContainerManager();
        dataContainerManager.addErrorEventListener( this );
        usedDataContainers = new HashMap();

        fieldPool = new FieldPool( guiListener, controlListener);

        dataContainerParser = new DataContainerParser( (Logger) ( guiListener.getLogger().getLogger() ),
                                                          guiListener.getPluginLocator(), guiListener
                                                              .getDataAccess() );
    }

    public String dumpConfig() {
        return dataContainerParser.dumpConfig();
    }

    /** A provided set of containers from the given Container Provider will be registered here.*/
    public void addContainers( ContainerProvider aContainerProvider ) {
        containersMap.putAll( aContainerProvider.getContainers() );
        if ( !( containerProvidersMap.containsKey( aContainerProvider.getContainerProviderName() ) ) ) {
            containerProvidersMap.put( aContainerProvider.getContainerProviderName(), aContainerProvider );

            Iterator iterator = aContainerProvider.getContainers().keySet().iterator();
            String name;
            while ( iterator.hasNext() ) {
                name = iterator.next().toString();
                containerElements.put( name, new ArrayList() );
            }
        }
    }

    /** Returns a list of all registered containers here.*/
    public List getContainerElements( String containername ) {
        List list = (List) ( containerElements.get( containername ) );
        return list;
    }

    public ContainerProvider getContainerProvider( String containername ) {
        return (ContainerProvider) ( containerProvidersMap.get( containername ) );
    }

    public Map getContainerProviderMap() {
        return containerProvidersMap;
    }

    /** Registers an element object as available and assigns it to a given container.*/
    private void putUsedElement( String containername, Object element ) {
        usedElementsList.add( element );
        List list = getContainerElements( containername );
        if ( list != null ) {
            list.add( element );
        }
    }

    private boolean isElementUsed( Object key ) {
        return usedElementsList.contains( key );
    }

    /** Parses the configuration file for the following XML-Tags:<br>
     * <pre>
     * Config.PANELCONFIG = "PanelConfig" and
     * Config.PANELDEFINITION = "PanelDefinition".
     * 
     * PanelConfig: value={name}.
     * PanelDefinition: 
     *      {guifields:{guifield: {id,name,label,description,datasource,widget}}}, 
     *      {Elements:{name}: Entry{name,flag,value,container}}.
     * </pre>
     * Then defined containers instances shall be loaded.
     * Returns a list of errors if something has being failed.
     */
    public List layoutElements() {
        List errors = new ArrayList();

        Appearance ape = ConfigManager.getAppearance();
        String panelconfig = ape.get( Appearance.Key.PANELCONFIG );
        if ( panelconfig != null ) {
            if ( panelconfig.trim().length() == 0 )
                panelconfig = null;
        }
        Element paneldef = ape.getAsElement( Appearance.Key.PANELDEFINITION );
        if ( paneldef != null ) {
            errors.addAll( insertPanelsByDescription( paneldef, panelconfig ));
        }
        else {
            guiListener.getLogger().config( "Es wurde keine PanelDefinition in der Konfigurationsdatei gefunden." );
        }

        return errors;
    }

    /* Handles "guifields" and "Elements" nodes as follows:
     * "guifields" - all data containers will be added to a dataContainerManager, 
     * "Elements" - all nodes will be inserted into the predefined panels
     * Returns list of errors if failed.  
     */
    private List insertPanelsByDescription( Element ePanelDef, String configname ) {
        List errors = new ArrayList();
        NodeList oGUIFieldsNodes = ePanelDef.getElementsByTagName( "guifields" );
        if ( oGUIFieldsNodes != null ) {
            errors.addAll( dataContainerParser.parse( oGUIFieldsNodes ) );
            dataContainerParser.addAllDataContainersToObserve( dataContainerManager );
        }
        
        NodeList layoutNodes = ePanelDef.getElementsByTagName("layout");
        if ( layoutNodes != null)
          initContainerFormLayouts(layoutNodes);

        NodeList ElementNodes = ePanelDef.getElementsByTagName( "Elements" );
        if ( ElementNodes != null ) {
            errors.addAll( insertNodesToPanel( ElementNodes, configname ) );
        }
        return errors;
    }
    
    /**
     * Iterates the given NodeList and initializes the form
     * layout of the specified container.
     * 
     * <p>Each element in the list has a "container", "colSpec"
     * and "rowSpec" attribute. The specs map directly to the
     * arguments of the {@link com.jgoodies.forms.layout.FormLayout#FormLayout(java.lang.String, java.lang.String)}
     * constructor.</p>
     * 
     * @param layoutNodes
     */
    private void initContainerFormLayouts(NodeList layoutNodes)
    {
      Element node;
      for ( int i = 0; i < layoutNodes.getLength(); i++ )
      {
        node = (Element) layoutNodes.item(i);
        
        String containerName = node.getAttribute("container").toUpperCase();
        String colSpec = node.getAttribute("colSpec");
        String rowSpec = node.getAttribute("rowSpec");
        
        TarentPanelInterface panel = getContainer(containerName);
                
        if (panel != null)
            panel.initFormLayout(colSpec, rowSpec);
//         else
//             throw new IllegalStateException("Container not found: " + containerName);
      }
      
    }

    /* Reads an element's name, gets subnodes 
     * and inserts them direct to a predefined panel.
     */
    private List insertNodesToPanel( NodeList nodes, String configname ) {
        boolean done = false;

        List errors = new ArrayList();
        String elementName;
        NodeList subnodes;
        Element node;
        for ( int i = 0; i < ( nodes.getLength() ); i++ ) {
            node = (Element) nodes.item( i );
            if ( node != null ) {
                elementName = node.getAttribute( "name" );
                if ( ( configname == null ) || ( elementName.equalsIgnoreCase( configname ) ) ) {
                    subnodes = node.getChildNodes();
                    if ( subnodes != null ) {
                        errors.addAll( insertDirectNodesToPanel( subnodes ) );
                        done = true;
                    }
                }
            }
        }

        if ( !done ) {
            String available = "";
            for ( int i = 0; i < ( nodes.getLength() ); i++ ) {
                node = (Element) nodes.item( i );
                if ( node != null ) {
                    String name = node.getAttribute( "name" );
                    available += name + ", ";
                }
            }
            available = available.trim();
            if ( available.endsWith( "," ) )
                available = available.substring( 0, available.length() - 1 );

            String error = "Die gew�nschte Panel-Definition \"" + configname
                + "\" konnte nicht gefunden werden. Vorhandene Panel-Definitionen: \"" + available + "\".";
            guiListener.getLogger().config( error );
            errors.add( new GUIParseError( GUIParseError.ERROR_MISSING, error, "" ) );
        }

        return errors;
    }

    /* Handles the elements: SEPARATOR, HEADLINE, HEADLINE, fieldName and generic element.
     * Creates new widgets and puts them to the registered containers.
     */
    private List insertDirectNodesToPanel( NodeList nodes ) {
        List errors = new ArrayList();

        Element node = null;
        String elementName;
        String elementFlags;
        String elementValue;
        String elementContainer;
        String elementTitle;        
        String elementDescription;
        EntryLayoutHelper currentWidget;
        boolean isActiveElement;

        for ( int i = 0; i < ( nodes.getLength() ); i++ ) {
            if ( nodes.item( i ) instanceof Element ) {
                node = (Element) nodes.item( i );
                elementName = node.getAttribute( "name" ).toUpperCase();
                elementFlags = node.getAttribute( "flags" );
                elementValue = node.getAttribute( "value" );
                elementContainer = node.getAttribute( "container" ).toUpperCase();

                elementTitle = node.getAttribute( "title" );
                if ( elementTitle.length() == 0 )
                    elementTitle = null;
                
                elementDescription = node.getAttribute( "description" );
                if ( elementDescription.length() == 0 )
                    elementDescription = null;

                if ( ( elementName != null ) && ( elementName.length() > 0 ) ) {
                    isActiveElement = false;
                    currentWidget = null;

                    if ( "SEPARATOR".equalsIgnoreCase( elementName ) ) {
                        TarentWidgetSeparator separator = new TarentWidgetSeparator();
                        currentWidget = new EntryLayoutHelper( new TarentWidgetLabel(elementValue), separator, elementFlags );
                        headlinesList.add( currentWidget );
                    }
                    else if ( "HEADLINE".equalsIgnoreCase( elementName ) ) {
                        currentWidget = new EntryLayoutHelper( new TarentWidgetLabel(elementValue), elementFlags );
                        headlinesList.add( currentWidget );
                    }
                    else {
                        String elementFieldName = node.getAttribute( "fieldName" );
                        if ( elementFieldName.length() > 0 ) {// field
                            DataContainerField containerField = getAdvancedElement( elementFieldName );

                            if ( containerField != null ) {

                                if ( elementTitle != null ) {
                                    containerField.getDataContainer().getFieldDescriptor().setName( elementTitle );
                                }

                                if ( elementDescription != null ) {
                                    containerField.getDataContainer().getFieldDescriptor().setDescription( elementDescription );
                                }

                                currentWidget = (EntryLayoutHelper) ( containerField.getPanel(elementFlags) );
                                fieldPool.addField( elementName, containerField );
                                addDataContainer( containerField.getDataContainer() );
                            }
                            else guiListener.getLogger().config("unable to insert element \""+ elementFieldName +"\" because no instance found!" );
                        }
                        else {// create (GenericExtendedField) and put it to fieldPool
                            currentWidget = getElement( elementName , elementFlags);
                            if ( currentWidget == null ) {
                                currentWidget = getExtendedElement( elementName, elementValue, elementDescription,
                                                             elementTitle, 80, elementFlags );
                            }
                        }

                        isActiveElement = true;
                    }

                    TarentPanelInterface panel = getContainer( elementContainer );

                    if ( currentWidget != null ) {// add widget to a panel
                        if ( panel != null ) {
                            panel.addWidget( currentWidget );
                            if ( isActiveElement ) {
                                AddressField field = getAdressField( elementName );
                                if ( field != null ) {
                                    field.setFieldName( elementTitle );
                                    field.setFieldDescription( elementDescription );

                                    putUsedElement( elementContainer, field );
                                }
                            }

                        }
                        else {
                            errors.add( new GUIParseError( GUIParseError.ERROR_CONTAINER_NOT_FOUND, elementContainer ) );
                        }
                    }
                    else {// no widget
                        if ( isElementUsed( elementName ) ) {
                            errors.add( new GUIParseError( GUIParseError.ERROR_ELEMENT_USED, elementName ) );
                        }
                        else {
                            errors.add( new GUIParseError( GUIParseError.ERROR_ELEMENT_NOT_FOUND, elementName ) );
                        }
                    }
                }
            }
        }
        return errors;
    }

    private DataContainerField getAdvancedElement( Object key ) {
        DataContainer templatecontainer = dataContainerParser.getDataContainer( key );
        if ( templatecontainer != null ) {
            DataContainer container = templatecontainer.cloneDataContainer();

            if ( container != null ) {
                DataContainerField af = new DataContainerField( container );
                return af;
            }
        }

        return null;
    }

    private EntryLayoutHelper getElement( Object key, String widgetFlags ) {
        AddressField af = fieldPool.getField( key.toString() );
        return ( af != null ) ? (EntryLayoutHelper) ( af.getPanel(widgetFlags) ) : null;
    }

    /** Creates a generic extended field and returns it's panel. */
    private EntryLayoutHelper getExtendedElement( Object guikey, String dbkey, String tooltipkey, String labelkey,
                                                  int size, String widgetFlags ) {
        ContactAddressField af = new GenericExtendedField( guikey.toString(), dbkey, tooltipkey, labelkey, size);
        if ( af != null ) {
            fieldPool.addField( guikey.toString(), af );
            return af.getPanel(widgetFlags);
        }
        return null;
    }

    public AddressField getAdressField( Object key ) {
        AddressField af = fieldPool.getField( key.toString() );
        return af;
    }

     public boolean setAddressFieldData( Object key, Object data ) {
        AddressField field = getAdressField( key );
        if ( field != null ) {
            if ( data instanceof Address ) {
                field.setData( new AddressPluginData( (Address) data ) );
                return true;
            }
            else if ( data instanceof User ) {
                field.setData( new UserPluginData( (User) data ) );
                return true;
            }
        }
        return false;
    }

    public TarentPanelInterface getContainer( Object key ) {
        return (TarentPanelInterface) ( containersMap.get( key ) );
    }

    public TarentPanelInterface getContainerDirect( Object key ) {
        return (TarentPanelInterface) ( containersMap.get( key ) );
    }

    public void postFinalRealize() {
        if ( bubblePanel == null ) {
            bubblePanel = new org.evolvis.nuyo.datacontainer.ErrorDisplay.BubblePanel();
            controlListener.getLayeredPane().add( bubblePanel, JLayeredPane.POPUP_LAYER );
            controlListener.addComponentToResize( bubblePanel );
            bubblePanel.setLocation( 0, 0 );
            bubblePanel.setSize( 800, 600 );
            bubblePanel.setVisible( true );
            bubblePanel.setOpaque( false );
        }

        // Container...
        Iterator containerprovideriterator = containerProvidersMap.values().iterator();
        while ( containerprovideriterator.hasNext() ) {
            ContainerProvider containerprovider = (ContainerProvider) ( containerprovideriterator.next() );
            containerprovider.postFinalRealize();
        }

        // Elements...
        List elements = usedElementsList;
        for ( int i = 0; i < ( elements.size() ); i++ ) {
            AddressField field = (AddressField) ( elements.get( i ) );
            if ( field != null ) {
                field.postFinalRealize();
            }
        }

        // Listeners...
        List usedelements = getUsedDataContainers();
        for ( int i = 0; i < ( usedelements.size() ); i++ ) {
            DataContainer datacontainer = (DataContainer) ( usedelements.get( i ) );
            if ( datacontainer != null ) {
                List listeners = datacontainer.getListeners();
                for ( int n = 0; n < ( listeners.size() ); n++ ) {
                    DataContainerListener listener = (DataContainerListener) ( listeners.get( n ) );
                    listener.installListener();
                }
            }
        }

    }

    /**
     * Method getAddress. Diese Methode liest den Inhalt der GUI
     * in ein Address-Objekt.
     * @param address
     * @throws ContactDBException
     */
    public void getAddress( Address address, Address displayedaddress ) throws ContactDBException {
        Map containerproviders = this.getContainerProviderMap();
        if ( containerproviders != null ) {
            // Container...      
            Iterator containerprovideriterator = this.getContainerProviderMap().values().iterator();
            while ( containerprovideriterator.hasNext() ) {
                ContainerProvider containerprovider = (ContainerProvider) ( containerprovideriterator.next() );

                Map containers = containerprovider.getContainers();
                Iterator container = containers.keySet().iterator();

                while ( container.hasNext() ) {
                    List elements = this.getContainerElements( container.next().toString() );

                    if ( elements != null ) {
                        PluginData data = new AddressPluginData( address );
                        for ( int i = 0; i < ( elements.size() ); i++ ) {
                            Object entry = elements.get( i );
                            AddressField field = (AddressField) ( elements.get( i ) );

                            if ( field.getContext() == ContactAddressField.CONTEXT_ADRESS ) {
                                if ( !displayedaddress.equals( field.getDisplayedObject() ) ) {
                                    if ( !( field.isWriteOnly() ) ) {
                                        guiListener.getLogger().warningSilent(
                                                                           "[!] getAddress(): field " + field.getKey()
                                                                               + " is invalid!" );
                                    }
                                }
                                else {
                                    field.getData( data );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean isDirty( Address address ) throws ContactDBException {

        Map containerproviders = this.getContainerProviderMap();
        if ( containerproviders != null ) {
            // Container...      
            Iterator containerprovideriterator = this.getContainerProviderMap().values().iterator();
            while ( containerprovideriterator.hasNext() ) {
                ContainerProvider containerprovider = (ContainerProvider) ( containerprovideriterator.next() );

                Map containers = containerprovider.getContainers();
                if ( containers != null ) {
                    Iterator containeriterator = containers.keySet().iterator();

                    while ( containeriterator.hasNext() ) {
                        List elements = this.getContainerElements( containeriterator.next().toString() );

                        if ( elements != null ) {
                            PluginData data = new AddressPluginData( address );
                            for ( int i = 0; i < ( elements.size() ); i++ ) {
                                Object entry = elements.get( i );
                                AddressField field = (AddressField) ( elements.get( i ) );
                                if ( ContactAddressField.CONTEXT_ADRESS == field.getContext() ) {
                                    if ( field != null && field.isDirty( data ) ) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public void setEditable( Veto veto, boolean iseditable, Address displayedaddress ) {
        if ( ( displayedaddress != null ) && ( iseditable ) ) {
            Map containerproviders = this.getContainerProviderMap();
            if ( containerproviders != null ) {
                // Container...      
                Iterator containerprovideriterator = this.getContainerProviderMap().values().iterator();
                while ( containerprovideriterator.hasNext() ) {
                    ContainerProvider containerprovider = (ContainerProvider) ( containerprovideriterator.next() );

                    Map containers = containerprovider.getContainers();
                    Iterator container = containers.keySet().iterator();
                    while ( container.hasNext() ) {
                        List elements = this.getContainerElements( container.next().toString() );
                        if ( elements != null ) {
                            PluginData data = new AddressPluginData( displayedaddress );
                            for ( int i = 0; i < ( elements.size() ); i++ ) {
                                AddressField field = (AddressField) ( elements.get( i ) );
                                if ( ContactAddressField.CONTEXT_ADRESS == field.getContext() ) {
                                    if ( !( field.isWriteOnly() ) ) {
                                        field.setData( data );
                                        field.setDisplayedObject( displayedaddress );
                                    }
                                }
                                else if ( field.getContext() == ContactAddressField.CONTEXT_UNKNOWN ) {
                                    guiListener.getLogger().severe(
                                                                       "setEditable() field \"" + field.getKey()
                                                                           + "\" hat einen unbekannten Kontext." );
                                }
                            }
                        }
                    }
                }
            }
        }

        List elements = usedElementsList;
        if ( elements != null ) {
            for ( int i = 0; i < ( elements.size() ); i++ ) {
                //Object entry = elements.get(i);
                AddressField field = (AddressField) ( elements.get( i ) );
                field.setEditable( iseditable );
            }
        }
    }
    
    public void setVisibleContainerProvider( ContainerProvider provider ) {
        visibleContainerProvider = provider;
    }

    public ContainerProvider getVisibleContainerProvider() {
        return visibleContainerProvider;
    }

    public void updateTab( Object container, boolean updateall, boolean overwrite, Address displayedaddress, Mail mail,
                          User user ) {

        if ( container instanceof ContainerProvider ) {
            Map containers = ( (ContainerProvider) container ).getContainers();
            Iterator iterator = containers.keySet().iterator();
            while ( iterator.hasNext() ) {
                String name = iterator.next().toString();
                List elements = this.getContainerElements( name );

                if ( elements != null ) {
                    for ( int i = 0; i < ( elements.size() ); i++ ) {

                        AddressField field = (AddressField) ( elements.get( i ) );

                        PluginData data = new AddressPluginData( displayedaddress );
                        PluginData maildata = new MailPluginData( mail, user );

                        try {
                            Object displayed = field.getDisplayedObject();
                            if ( ( !( displayedaddress.equals( displayed ) ) ) || overwrite) {
                                if ( ( updateall ) || ( field.getContext() == ContactAddressField.CONTEXT_ADRESS ) ) {
                                    field.setData( data );
                                    field.setDisplayedObject( displayedaddress );
                                }
                                else if ( field.getContext() == ContactAddressField.CONTEXT_UNKNOWN ) {
                                    guiListener.getLogger().severe("updateTab() field \"" + field.getKey() + "\" hat einen unbekannten Kontext." );
                                }
                                if ( field.getContext() == ContactAddressField.CONTEXT_MAIL ) {
                                    field.setData( maildata );
                                }
                            }
                            field.onShow();
                        }
                        catch ( /* This is a joke, isnt it? --> */ Throwable e ) {
                            guiListener.getLogger().severe( "updateTab() container \"" + name + "\" failed: " + e.getClass().toString() + ": " + e.getMessage(), e);
                        }
                    }
                }
                else {
                    guiListener.getLogger().config( "updateTab() container \"" + name + "\" wurde nicht gefunden." );
                }
            }
        }
        else {
            guiListener.getLogger().severe( "updateTab() container is not an instance of ContainerProvider");
        }
    }

    public void setUser( User user ) {
        Map containerproviders = this.getContainerProviderMap();
        if ( containerproviders != null ) {
            // Container...      
            Iterator containerprovideriterator = this.getContainerProviderMap().values().iterator();
            while ( containerprovideriterator.hasNext() ) {
                ContainerProvider containerprovider = (ContainerProvider) ( containerprovideriterator.next() );

                Map containers = containerprovider.getContainers();
                if ( containers != null ) {
                    Iterator containeriterator = containers.keySet().iterator();

                    while ( containeriterator.hasNext() ) {
                        List elements = this.getContainerElements( containeriterator.next().toString() );

                        if ( elements != null ) {
                            PluginData data = new UserPluginData( user );
                            for ( int i = 0; i < ( elements.size() ); i++ ) {
                                Object entry = elements.get( i );
                                AddressField field = (AddressField) ( elements.get( i ) );
                                if ( ContactAddressField.CONTEXT_USER == field.getContext() ) {
                                    field.setData( data );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------

    public void startTimerTask( long iMillisecondsPerTick ) {
        timer = new Timer( true );
        timerEventThread = new TimerEventThread();
        // in einer Sekunde anfangen ... dann jede Sekunde...
        timer.scheduleAtFixedRate( timerEventThread, 1000L, iMillisecondsPerTick );
    }

    private class TimerEventThread extends TimerTask {
        private int m_iTicks;

        public TimerEventThread() {
            m_iTicks = 0;
        }

        public void run() {
            m_iTicks++;

            if ( getVisibleContainerProvider() != null ) {
                Map containers = getVisibleContainerProvider().getContainers();
                if ( containers != null ) {
                    Iterator containeriterator = containers.keySet().iterator();

                    while ( containeriterator.hasNext() ) {
                        List elements = getContainerElements( containeriterator.next().toString() );

                        if ( elements != null ) {
                            for ( int i = 0; i < ( elements.size() ); i++ ) {
                                AddressField field = (AddressField) ( elements.get( i ) );

                                if ( field.getTimerTicks() > 0 ) {
                                    if ( ( m_iTicks % field.getTimerTicks() ) == 0 ) {
                                        field.onTimerEvent();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void errorEvent( DataContainerErrorEvent event ) {
        if ( DataContainerErrorEvent.SHOWERRORS.equals( event.getType() ) ) {
            if ( bubblePanel != null )
                bubblePanel.removeAllErrors();

            List containers = dataContainerManager.getDataContainersWithErrors();
            Iterator it = containers.iterator();
            while ( it.hasNext() ) {
                DataContainer container = (DataContainer) ( it.next() );
                DataContainerErrors errors = dataContainerManager.getErrorsOfDataContainer( container );
                Iterator eit = errors.iterator();
                while ( eit.hasNext() ) {
                    ErrorHint hint = (ErrorHint) ( eit.next() );
                    if ( bubblePanel != null )
                        bubblePanel.addError( hint );
                }
            }

            if ( bubblePanel != null ) {
                bubblePanel.repaint();
                controlListener.getLayeredPane().revalidate();
            }
        }
        else if ( DataContainerErrorEvent.HIDEERRORS.equals( event.getType() ) ) {
            if ( bubblePanel != null ) {
                bubblePanel.removeAllErrors();
                bubblePanel.repaint();
                controlListener.getLayeredPane().revalidate();
            }
        }
    }

    public void validateAll() {
        Iterator it = dataContainerParser.iterator();
        while ( it.hasNext() ) {
            DataContainer container = (DataContainer) ( it.next() );
            container.validate();
        }
    }

    public void validate( DataContainer container ) {
        container.validate();
    }

    // --------------------------------------------

    public void addDataContainer( DataContainer datacontainer ) {
        datacontainer.setDataContainerPool( this );
        usedDataContainers.put( datacontainer.getKey(), datacontainer );
    }

    public void removeDataContainer( DataContainer datacontainer ) {
        usedDataContainers.remove( datacontainer.getKey() );
    }

    public DataContainer getDataContainer( String key ) {
        return (DataContainer) ( usedDataContainers.get( key ) );
    }

    public boolean containsDataContainer( String key ) {
        return usedDataContainers.containsKey( key );
    }

    public List getUsedDataContainers() {
        return new ArrayList( usedDataContainers.values() );
    }
}
