/* $Id: UserRequest.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 16.10.2003
 */
package org.evolvis.nuyo.messages;

import java.util.EventObject;

/**
 * Dieses Ereignisobjekt spiegelt Benutzeranfragen (meist durch Buttons
 * ausgel�st) wider.
 * 
 * @author mikel
 */
public class UserRequest extends EventObject implements CommandEvent {
    /**
     * Der Konstruktor legt die �bergebenen Parameter ab.
     * 
     * @param source
     * @param request
     * @param object
     */
    public UserRequest(Object source, String request, Object object) {
        super(source);
        this.request = request;
        this.object = object;
    }

    /*
     * Getter und Setter
     */
    public Object getObject() {
        return object;
    }

    public String getRequest() {
        return request;
    }

    /*
     * gesch�tzte Variablen
     */
    String request = null;
    Object object = null; 
}
