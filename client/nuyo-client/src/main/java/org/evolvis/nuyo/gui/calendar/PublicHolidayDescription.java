/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;


/**
 * @author niko
 *
 */
public class PublicHolidayDescription
{
  private ScheduleDate m_oDate;
  private String m_sName;
  private String m_sDescription;

  public PublicHolidayDescription(ScheduleDate date, String name, String description)
  {
    m_oDate = date;
    m_sName = name;
    m_sDescription = description;
  }
  
  public ScheduleDate getDate()
  {
    return m_oDate;
  }

  public void setDate(ScheduleDate date)
  {
    m_oDate = date;
  }

  public String getDescription()
  {
    return m_sDescription;
  }

  public void setDescription(String description)
  {
    m_sDescription = description;
  }

  public String getName()
  {
    return m_sName;
  }

  public void setName(String name)
  {
    m_sName = name;
  }

}
