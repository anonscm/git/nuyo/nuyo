/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class Fax3Field extends GenericTextField
{
  public Fax3Field()
  {
    super("FAX3", AddressKeys.FAX3, CONTEXT_ADRESS, "GUI_Fields_Fax3_ToolTip", "GUI_Fields_Fax3", 0);
  }
}
