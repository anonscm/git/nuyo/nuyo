package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentAddressRelation;
import org.evolvis.nuyo.db.AppointmentCalendarRelation;
import org.evolvis.nuyo.db.AppointmentSequence;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.EventId;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;



/**
 * @author kleinw
 *
 */
abstract public class AppointmentBean extends AbstractEntity implements Appointment {

    //
    //	Soapanbindung
    //
	/** Kalendarrelationen zu diesem Appointment holen */
    private static final String METHOD_GET_SCHEDULE_RELATIONS = "getScheduleRelations";
	/** Adressrelationen zu diesem Appointment holen */
	private static final String METHOD_GET_ADDRESS_RELATIONS = "getAddressRelations";
	/** Datenbank-ID des Appointments */
	private static final String PARAM_EVENTID = "eventid";

	
	//
	//	Instanzmerkmale
	//
	/**	Termin oder Aufgabe */
    protected Boolean _isJob = new Boolean(false);
    /**	(Aufgabe) */
    protected Boolean _isCompleted = new Boolean(false);
    /**	tempor�r */
    protected Boolean _isTemporary = new Boolean(false);
    /** (Termin) privater */
    protected Boolean _isPrivate = new Boolean(false);
    /**	(Termin / Aufgabe) Priorit�t */
    protected Integer _priority = new Integer(0);
    /** (Termin) Kategorie */
    protected Integer _category = new Integer(0);
    /** (Aufgabe) Fortschritt */
    protected Integer _completition = new Integer(0);

    
    /** Startzeitpunkt */
    protected Date _start;
    /**	Endzeitpunkt */
    protected Date _end;
    /** Historisierungsdatum */
    protected Date _historyDate;
    /**	Besitzer des Termins */
    protected User _owner;
    
    /**	Map f�r Adressrelationen (externe Benutzer) */
    protected Map _addressRelations;
    /** Map f�r Kalenderrelationen (interne Benutzer) */
    protected Map _calendarRelations;
    
    /**	Hiermit k�nnen ein oder mehrere Appointments bezogen werden */
    static protected AbstractEntityFetcher _fetcher;
    
    public void invalidateRelations(){
    	_addressRelations = null;
    	_calendarRelations = null;
    }
    
    /**	Externen Benutzer hinzuf�gen. @param address - Adresse des Benutzers */
    public AppointmentAddressRelation add(Address address) throws ContactDBException {
        if (_addressRelations == null)
            getAddressRelations();
        if (!_addressRelations.containsKey(new Integer(address.getId()))) {
            AppointmentAddressRelation relation = new AddressRelationImpl(this, address);
            _addressRelations.put(new Integer(address.getId()), relation);
            return relation;
        } else {
            return (AppointmentAddressRelation)_addressRelations.get(new Integer(address.getId()));
        }
    }
    /**	einzelne Adressrelation holen. @param address - Zur Adresse dazugeh�rige Relation */
    public AppointmentAddressRelation getRelation(Address address)
		throws ContactDBException {
	    if (_addressRelations == null)
	        getAddressRelations();
	    return (AppointmentAddressRelation)_addressRelations.get(new Integer(address.getId()));
    }
    /**	Eine Adressrelation l�schen.@paramn address - Adresse, die gel�scht werden soll */
    public void remove(Address address) throws ContactDBException {
        Integer id = new Integer(address.getId());
        AddressRelationImpl aar = (AddressRelationImpl)_addressRelations.get(id);
        aar.delete();
    }
    /**	alle Adressen aller externer Teilnehmer */
    public Collection getAddresses() throws ContactDBException {
        if (_addressRelations == null)
            getAddressRelations();
        Collection addresses = new ArrayList(_addressRelations.size());
        for (Iterator it = _addressRelations.values().iterator();it.hasNext();) { 
            addresses.add(((AppointmentAddressRelation)it.next()).getAddress());
        }
        return addresses;
    }
    
    
    /**	Internen Benutzer hinzuf�gen. @param calendar - Zum Kalender dazuge�rige Relation */
    public AppointmentCalendarRelation add(Calendar calendar) throws ContactDBException {
        if (_calendarRelations == null)
            getCalendarRelations();
        if (!_calendarRelations.containsKey(new Integer(calendar.getId()))) {
            AppointmentCalendarRelation relation = new CalendarRelationImpl(this, calendar);
            _calendarRelations.put((new Integer(calendar.getId())), relation);
            return relation;
        } else 
            return (AppointmentCalendarRelation)_calendarRelations.get(new Integer(calendar.getId()));
    }
    /**	Internen Kalender l�schen. @param calendar - Der Kalender des Benutzers, der gel�scht werden soll */
    public void remove(Calendar calendar) throws ContactDBException {
        Integer id = new Integer(calendar.getId());
        CalendarRelationImpl acr = (CalendarRelationImpl)_calendarRelations.get(id);
        acr.delete();
        _calendarRelations.remove(id);
    }
    /** einzelne Kalenderrelation holen. @param calendar - Zum Kalender dazugeh�rige Relation */
    public AppointmentCalendarRelation getRelation(Calendar calendar) 
	throws ContactDBException {
		if (_calendarRelations == null)
		    getCalendarRelations();
		return (AppointmentCalendarRelation)_calendarRelations.get(new Integer(calendar.getId()));
    }
    /**	Alle Kalender aller interner Teilnehmer */
    public Collection getCalendars() throws ContactDBException {
        List calendars = new ArrayList();
        if(_calendarRelations == null)
            getCalendarRelations();
        calendars = new ArrayList();
        for (Iterator it = _calendarRelations.values().iterator();it.hasNext();) {
            calendars.add(((AppointmentCalendarRelation)it.next()).getCalendar());
        }
        return calendars;
    }
    

    /** Startzeitpunkt holen */
    public Date getStart() throws ContactDBException {return _start;}
    /**	Startzeitpunkt setzen. @param newStart - Startzeitpunkt */
    public void setStart(Date newStart) throws ContactDBException {_start = newStart;}

    
    /**	Endzeitpunkt holen */
    public Date getEnd() throws ContactDBException {return _end;}
    /** Endzeitpunkt setzen. @param newEnd - Endzeitpunkt */
    public void setEnd(Date newEnd) throws ContactDBException {_end = newEnd;}

    
    /** Kategorie setzen */
    public int getAppointmentCategory() throws ContactDBException {return (_category == null)?0:_category.intValue();}
    /**	Kategorie. @param newAppointmentCategory - Kategorie */
    public void setAppointmentCategory(int newAppointmentCategory) {_category = new Integer(newAppointmentCategory);}

    
    /** Grad der Vervollst�ndigung prozentual */
    public int getJobCompletion() throws ContactDBException {return (_completition == null)?0:_completition.intValue();}
    /** Grad der Vervollst�ndigung setzen. @param newJobCompletition - prozentuale Angabe */
    public void setJobCompletion(int newJobCompletion) {_completition = new Integer(newJobCompletion);}

    
    /**	Eigent�mer */
    public User getOwner() throws ContactDBException {return _owner;}
    /** Eigent�mer setzen. @param newOwner - der Eigent�mer */
    public void setOwner(User newOwner) throws ContactDBException {_owner = newOwner;}

    
    /** Priorit�t */
    public int getPriority() throws ContactDBException {return (_priority == null)?0:_priority.intValue();}
    /**	Priorit�t setzen. @param newPriority = die Priorit�t */
    public void setPriority(int newPriority) throws ContactDBException {_priority = new Integer(newPriority);}

    
    /**	Ist es ein Termin? */
    public boolean isJob() throws ContactDBException {return (_isJob == null)?false:_isJob.booleanValue();}
    /**	Art des Ereignisses : Termin oder Aufgabe. @param - Art des Ereignisses */
    public void setJob(boolean newIsJob) throws ContactDBException {_isJob = new Boolean(newIsJob);}

    
    /** (Aufgabe) beendet? */
    public boolean isJobCompleted() throws ContactDBException {return (_isCompleted == null)?false:_isCompleted.booleanValue();}
    /** Generelle Vervollst�ndigung setzen. @param new IsJobCompleted - Status */
    public void setJobCompleted(boolean newIsJobCompleted) throws ContactDBException {_isCompleted = new Boolean(newIsJobCompleted);}

    
    /**	privat */
    public boolean isPrivat() throws ContactDBException {return (_isPrivate == null)?false:_isPrivate.booleanValue();}
    /**	Modus setzen. @param newIsPrivate - privat oder �ffentlich */
    public void setPrivate(boolean newIsPrivate) throws ContactDBException {_isPrivate = new Boolean(newIsPrivate);}

    
    /** tempor�r */
    public boolean isTemporary() {return (_isTemporary == null)?false:_isTemporary.booleanValue();}
    /**	Tempor�r. @param temporary - tempor�r */
    public void setTemporary(boolean temporary) {_isTemporary = new Boolean(temporary);}

    
    /** Historisierungsdatum beziehen */
    public Date getHistoryDate() {return _historyDate;}
    
    /**	Adressrelationen per Soap beziehen */
    protected void getAddressRelations() throws ContactDBException {
        Collection relations =
            AddressRelationImpl.getRelations(
                Selection.getInstance()
                	.add(new EventId(_id)));
        
        _addressRelations = new HashMap();
        
        for(Iterator it = relations.iterator();it.hasNext();) {
            AppointmentAddressRelation aar = (AppointmentAddressRelation)it.next();
            _addressRelations.put(new Integer(aar.getAddress().getId()), aar);
        }
        
    }

    
    /**	Kalenderrelationen per Soap beziehen */
    protected void getCalendarRelations() throws ContactDBException {
        _calendarRelations = new HashMap();
        if(_id != null) {
            Collection relations = 
                CalendarRelationImpl.getCalendarRelations(
                    Selection.getInstance()
                    	.add(new EventId(_id)));
            
            for(Iterator it = relations.iterator();it.hasNext();) {
                AppointmentCalendarRelation acr = (AppointmentCalendarRelation)it.next();
                _calendarRelations.put(new Integer((acr.getCalendar().getId())), acr);
            }
        }  
    }

    
    /** Sequenz von Terminen holen */
    public AppointmentSequence getAppointmentSequence() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }
    
}
