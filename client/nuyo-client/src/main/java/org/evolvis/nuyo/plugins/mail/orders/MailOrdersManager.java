package org.evolvis.nuyo.plugins.mail.orders;



/**
 * Makes possible to manage the mail orders.<br>
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public interface MailOrdersManager extends DispatchRequestListener {

    public void setButtonFilterOffEnabled( boolean isenabled );

    public void fireNewMailOrderEvent();
}
