/* $Id: ContactDBException.java,v 1.3 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 10.09.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import org.evolvis.nuyo.controller.ContactException;

import de.tarent.octopus.client.OctopusCallException;

/**
 * Diese Klasse stellt Contact-Client-interne Ausnahmen dar, die im
 * Datenzugriffs-Kontext auftreten.
 *  
 * @author mikel
 */
public class ContactDBException extends ContactException {
    /*
     * Konstruktoren
     */
    public ContactDBException(int exId) {
        super(exId);
    }

    public ContactDBException(int exId, String arg0) {
        super(exId, arg0);
    }

    public ContactDBException(int exId, String arg0, Throwable arg1) {
        super(exId, arg0, arg1);
    }

    public ContactDBException(int exId, Throwable arg0) {
        super(exId, arg0);
    }
    
    public OctopusCallException getOctopusException(){
       Throwable t = this;
       while(t.getCause()!=null){
           t=t.getCause();
           if (t instanceof OctopusCallException) return (OctopusCallException)t;
       }
       return null;        
    }

    /*
     * Fehlerkonstanten
     */
    /**
     * Die Adressenummer kann nicht ge�ndert werden.
     * 
     * @see Address#setAdrNr(int)
     */
    public final static int EX_ADDRESS_NUMBER_IS_CONSTANT = 1;

    /**
     * Das Erfassungsdatum kann nicht ge�ndert werden.
     * 
     * @see Address#setErfassungsdatum(java.util.Date)
     */
    public final static int EX_ERFASSUNGSDATUM_IS_CONSTANT = 2;

    /**
     * Die Kategorie kann nicht ge�ndert werden.
     * 
     * @see Address#setVerteilergruppe(String)
     */
    public final static int EX_CATEGORY_IS_CONSTANT = 3;

    /**
     * Eine SQL-Exception ist aufgetreten.
     */
    public final static int EX_SQL_EXCEPTION = 4;

    /**
     * Adress-Basisdaten konnten nicht gefunden werden.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCAddress#checkValidity(int)
     */
    public final static int EX_ADDRESS_NO_BASE_DATA = 5;

    /**
     * Addresserzeugung fehlgeschlagen.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCAddress#commit()
     */
    public final static int EX_ADDRESS_CREATION_FAILED = 6;

    /**
     * Ung�ltiger Kategorienschl�ssel.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDatabase#createVerteilergruppe(String, String)
     */
    public final static int EX_INVALID_CATEGORY_KEY = 7;

    /**
     * Ung�ltiger Unterkategorienschl�ssel.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDatabase#createVerteiler(String, String, String, String, String)
     */
    public final static int EX_INVALID_SUBCATEGORY_KEY = 8;

    /**
     * Fehler beim Ermitteln eines Unterkategorienschl�ssels.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDatabase#createVerteiler(String, String, String, String, String)
     */
    public final static int EX_SUBCATEGORY_KEY_CREATION_FAILED = 9;

    /**
     * Ung�ltiger Benutzerschl�ssel.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDatabase#hideVerteilergruppeFromUser(String, String)
     */
    public final static int EX_INVALID_USER = 10;

    /**
     * Kategorieschl�ssel ist Standard.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDatabase#hideVerteilergruppeFromUser(String, String)
     */
    public final static int EX_CATEGORY_IS_STANDARD = 11;

    /**
     * Duplikatcheck.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDupCheck#doNextCheckStep()
     */
    public final static int EX_DUPCHECK_ERROR = 12;

    /**
     * Nicht implementiert.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCMailBatch#doChannel(org.evolvis.nuyo.db.MailBatch.CHANNEL)
     * @see org.evolvis.nuyo.db.octopus.old.OctopusMailBatch#doChannel(org.evolvis.nuyo.db.MailBatch.CHANNEL)
     */
    public final static int EX_NOT_IMPLEMENTED = 13;

    /**
     * Octopus.
     */
    public final static int EX_OCTOPUS = 14;

    /**
     * Datenbanktreiber fehlt
     * 
     * @see org.evolvis.nuyo.controller.ActionManager#getNewConnectedDatabase()
     */
    public final static int EX_DRIVER_MISSING = 15;

    /**
     * Datenbankantwort nicht schl�ssig.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCDatabase#checkForVeto(org.evolvis.nuyo.contact.db.veto.VetoableAction)
     */
    public final static int EX_DATABASE_INCONSISTENT = 16;

    /**
     * Kategoriedaten konnten nicht gefunden werden.
     * 
     * @see org.evolvis.nuyo.db.odbc.ODBCCategory#checkValidity()
     */
    public final static int EX_CATEGORY_NO_DATA = 17;

    /**
     * Ein Appointment liefert einen unerlaubten Konflikt.
     * 
     * @see Schedule#createAppointment(java.util.Collection, boolean, java.util.Date, java.util.Date, boolean)
     */
    public final static int EX_APPOINTMENT_CONFLICTS = 18;

    /**
     * Eine Kategory existiert bereits.
     * 
     * @see Database#createVerteilergruppe(String, String, String)
     */
    public final static int EX_CATEGORY_EXISTS = 19;

    /**
     * Diese Adresse besitzt keine eindeutige Kategoriezuordnung.
     */
    public final static int EX_CATEGORY_NOT_UNIQUE = 20;

    /**
     * Das Appointment ist nicht vollst�ndig.
     */
    public final static int EX_APPOINTMENT_INCOMPLETE = 21;

    /**
     * Das Appointment ist fehlerhaft.
     */

    public final static int EX_APPOINTMENT_ERROR = 22;
    
    /**
     * Das Preview ist nicht parsebar.
     */
    public final static int EX_PREVIEW_ERROR = 23;
 
    /**
     * Das Object ist fehlerhaft.
     */
    public final static int EX_OBJECT_INCOMPLETE = 24;
    
    /**
     * Das Objektmodell wurde programmatisch fehlerhaft verwendet.
     * Beispiel : Eine noch nicht existierende Gruppe wurde einem
     * User zugeordnet.
     */
    public final static int EX_OBJECT_MODEL_ERROR = 25;
    
    /**	Die vom Server gelieferten Daten sind nicht wie erwartet */
    public final static int EX_INSUFFICIENT_SERVER_DATA = 26;
    
    /**	Fehler beim Umgang mit dem Filterobjekt */
    public final static int EX_FILTER_ERROR = 27;
    
    /**	Allgemeiner Fehler bei der PersistentEntity Schnittstelle */
    public final static int EX_ENTITY_ERROR = 28;


    /**	Fehlerhafter Login, der von User verursacht wurde (z.B. falsches Passwort) */
    public final static int EX_CLIENT_LOGIN_ERROR = 29;

    /**	Fehlerhafter Login, der von Server (z.B. LDAP-Connection Fehler) */
    public final static int EX_SERVER_LOGIN_ERROR = 30;


    /**	Key, der nicht existiert. */
    public final static int EX_INVALID_PROPERTY_KEY = 31;

    /**	Die R�ckgabe vom Octopus enth�lt nicht alle ben�tigten Daten. */
    public final static int EX_MISSING_RESULT_DATA = 32;

}
