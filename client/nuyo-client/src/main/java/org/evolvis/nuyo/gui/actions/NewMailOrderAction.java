package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.mail.orders.MailOrdersManager;
import org.evolvis.nuyo.plugins.mail.orders.MailOrdersPlugin;

import de.tarent.commons.plugin.Plugin;

public class NewMailOrderAction extends AbstractContactDependentAction {

    private static final long serialVersionUID = -5282205448136974263L;
    private static final TarentLogger logger = new TarentLogger(NewMailOrderAction.class);
    private MailOrdersManager mailOrdersManager;
    private ControlListener mainFrame;

    public void actionPerformed(ActionEvent e) {
        mainFrame.activateTab(MainFrameExtStyle.TAB_MAIL_ORDERS);
        if(mailOrdersManager != null) {
            mailOrdersManager.fireNewMailOrderEvent();
        } else logger.warning(getClass().getName() + " is not initialized!");
    }
    
    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();
        
        Plugin mailOderPlugin = PluginRegistry.getInstance().getPlugin(MailOrdersPlugin.ID);
        if(mailOderPlugin != null) {
            if(mailOderPlugin.isTypeSupported(MailOrdersManager.class)){
                mailOrdersManager = (MailOrdersManager) mailOderPlugin.getImplementationFor(MailOrdersManager.class);
            } else logger.warning("Couldn't init " + getClass().getName(), "MailOrdersManager interface is not supported.");
        } else logger.warning("Couldn't init " + getClass().getName(), "MailOrderPlugin is not registered");
    }
}