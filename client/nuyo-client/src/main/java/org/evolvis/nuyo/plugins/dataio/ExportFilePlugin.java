package org.evolvis.nuyo.plugins.dataio;

import java.util.Collections;
import java.util.List;

import org.evolvis.nuyo.plugin.DialogAddressExporter;

import de.tarent.commons.plugin.Plugin;

/**
 * This plugin delivers a <code>DialogFileExporter<code>.
 * All plugins supplying an implementation complying with
 * the <code>AddressExporter<code> interface will be detected
 * and controlled by this plugin.
 * 
 * @author Jens Neumaier, tarent GmbH
 * @see org.evolvis.nuyo.plugins.dataio.DialogFileExporter
 * @see org.evolvis.nuyo.plugin.AddressExporter
 *
 */
public class ExportFilePlugin implements Plugin {

    public static final String ID = "address-export";
    
    public ExportFilePlugin() {
    }
    
    public String getID() {
        return ID;
    }

    public void init() {
    }

    public Object getImplementationFor(Class type) {
        if (type == DialogAddressExporter.class) {
            return new DialogFileExporter();
        }
        
        return null;
    }

    public List getSupportedTypes() {
        return Collections.singletonList(DialogAddressExporter.class);
    }

    public boolean isTypeSupported(Class type) {
        if (type.equals(DialogAddressExporter.class)){
            return true;
        }
        return false;
    }

	public String getDisplayName() {
		return "File Export";
	}
}
