/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Hanno Wendt.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.help.HelpBroker;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.ui.EscapeDialog;

/**
 * zeigt die Liste mit Adress-Doubletten an, falls der User dabei ist, eine zu erzeugen.
 * Er kann dann ausw�hlen in welcher er arbeiten m�chte, oder ob er den Vorgang 
 * abbrechen will.
 * 
 * @author hanno
 */

public class DoubletSearchDialog extends EscapeDialog {
	
	public final static String STATUS_ABORT = "ABORT";
	public final static String STATUS_SAVE = "SAVE";
	public final static String STATUS_JUMP_TO_DOUBLE = "JUMP TO DOUBLE";

    private static final TarentLogger logger = new TarentLogger(DoubletSearchDialog.class);

    private String status;
    private GUIListener myControlListener;
    private Address selectedAddress;
    private Addresses myAddresses;
    private Container contentPane;
    private JScrollPane panel2;
    private JPanel rootPanel;
    private JPanel panel1;
    private JPanel panel3;
    private JLabel label1;
    private int doubleCount;
    private boolean enableSaveButton = false;

    private JButton takeOverAddressButton;
    //private JButton button2;
    private JButton saveAnywayButton;
    private JButton cancelButton;

    protected MyTableModel tableModel = new MyTableModel();
    protected JTable table = new JTable( tableModel );
    protected ListSelectionModel mySelectionModel;
    protected PlanningTableRenderer renderer;
    protected Border myBorder;

    private Vector header;
    private Vector data;
    
    private JFrame parent;
    
    /**Konstruktor der Doublettensuche.
     */
    public DoubletSearchDialog( GUIListener controlListener, JFrame sender, Addresses a, boolean enableSaveButton) {
        super( sender );
        this.enableSaveButton = enableSaveButton;
        myAddresses = a;
        myControlListener = controlListener;
        parent = sender;
        init();

    }

    /**F�hrt den Aufbau des Fenster durch.*/

    private void init( ) {
        //		renderer = new PlanningTableRenderer();
        //		table.setDefaultRenderer( Object.class, renderer );
        contentPane = getContentPane();
        contentPane.setLayout( new BorderLayout( 5, 5 ) );
        rootPanel = new JPanel( new BorderLayout( 2, 2 ) );
        myBorder = new EmptyBorder( 8, 8, 8, 8 );
        setTitle( Messages.getString( "GUI_DoublettenSuche_Titel_Suche_initial" ) ); //$NON-NLS-1$
        rootPanel.setBorder( myBorder );
        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        panel1 = new JPanel( new FlowLayout() );
        doubleCount = myAddresses.getIndexMax() + 1;
        label1 = new JLabel( doubleCount + Messages.getString( "GUI_DoublettenSuche_Message_Doublette(n)_gefunden" ) ); //$NON-NLS-1$
        panel1.add( label1 );
        rootPanel.add( panel1, BorderLayout.NORTH );

        header = new Vector();

        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_AdrNr" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Nachname" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Vorname" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Strasse" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Nr" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_LKZ" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_PLZ" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Ort" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Institution" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Postfach_PLZ" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Postfach-Nr" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Bundesland" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Tel._dienstl" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Fax_dienstl" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Anrede" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_Titel" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_akad_Titel" ) ); //$NON-NLS-1$
        header.add( Messages.getString( "GUI_DoublettenSuche_Spalte_eMail" ) ); //$NON-NLS-1$

        data = getData();
        tableModel.setDataVector( data, header );

        panel2 = new JScrollPane( table );
        rootPanel.add( panel2, BorderLayout.CENTER );

        panel3 = new JPanel( new FlowLayout() );

        takeOverAddressButton = new JButton( Messages.getString( "GUI_DoublettenSuche_Button_ausgewaehlte_Adressen__zusammenfuehren" ) ); //$NON-NLS-1$

        takeOverAddressButton.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e ) {
            	status = STATUS_JUMP_TO_DOUBLE;
            	setVisible( false );
                setModal( false );
//            	new CompareView("addressDups.vm", myAddresses);
//                selectedAddress = getSelectedAddress();
//                if ( selectedAddress != null ) {
//
//                    // Fehlermeldung und abbrechen, wenn der User die Adresse nicht editieren darf
//                    if ( !myControlListener.userRequestUserHasRightOnAddress( SecurityManager.FOLDERRIGHT_EDIT,
//                                                                              selectedAddress ) ) {
//                        ApplicationServices.getInstance().getCommonDialogServices()
//                            .publishError(
//                                           Messages
//                                               .getString( "GUI_DoublettenSuche_Error_kein_schreibrecht_auf_adresse" ) );
//                        return;
//                    }
//                    myControlListener.setDoubleAdd( selectedAddress );
//                    setVisible( false );
//                    setModal( false );
//                    
//                    shouldContinue = true;
//                }
//                else
//                    ApplicationServices.getInstance().getCommonDialogServices()
//                        .publishError( Messages.getString( "GUI_DoublettenSuche_Error_keine_Adresse_ausgewaehlt" ) ); //$NON-NLS-1$
            }
        } );

//        button2 = new JButton( Messages.getString( "GUI_DoublettenSuche_Button_keine_Adresse_uebernehmen" ) ); //$NON-NLS-1$
//        button2.setFont( oFont );
//        button2.setBackground( oButtonBGColor );
//        button2.setForeground( oButtonFGColor );
//
//        button2.addActionListener( new ActionListener() {
//            public void actionPerformed( ActionEvent e ) {
//
//                setVisible( false );
//                setModal( false );
//
//            }
//        } );
        
        saveAnywayButton = new JButton(Messages.getString("GUI_DoublettenSuche_Button_SaveAnyway"));
        saveAnywayButton.setToolTipText(Messages.getString("GUI_DoublettenSuche_Button_SaveAnyway_ToolTip"));
        saveAnywayButton.addActionListener(new ActionListener()
        {
        	public void actionPerformed(ActionEvent ae)
        	{
                status = STATUS_SAVE;
                setVisible( false );
                setModal( false );
        	}
        });

        cancelButton = new JButton( Messages.getString( "GUI_DoublettenSuche_Button_Abbrechen" ) ); //$NON-NLS-1$

        cancelButton.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e ) {
                status = STATUS_ABORT;
                setVisible( false );
                setModal( false );
            }
        } );

        // TODO: Disabled for Release 2.0 as requested per Bug #2702 
        // https://bugzilla.tarent.de/bugzilla/show_bug.cgi?id=2702
        
        panel3.add( takeOverAddressButton );
        if (enableSaveButton)
        	panel3.add( saveAnywayButton );
        panel3.add( cancelButton );
        rootPanel.add( panel3, BorderLayout.SOUTH );

        contentPane.add( rootPanel, BorderLayout.CENTER );

        mySelectionModel = table.getSelectionModel();
        mySelectionModel.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );

        setTitle( Messages.getString( "GUI_DoublettenSuche_Titel_Doublettensuche" ) ); //$NON-NLS-1$
        setSize( 800, 500 );
        //setResizable(false);
        setLocationRelativeTo(parent);
        setModal( true );
        toFront();
        setVisible( true );

        addWindowListener( new WindowAdapter() {

            public void windowClosing( WindowEvent e ) {

                setVisible( false );
                setModal( false );

            }
        } );

        initHelp();
    }

    private void initHelp() {
        HelpBroker helpBroker = ApplicationServices.getInstance().getHelpBroker();
        if(helpBroker != null) {
            helpBroker.enableHelpKey( rootPanel, "duplikatsprfung.htm", helpBroker.getHelpSet() );
        } else {
            logger.warning("[!] [DoubletSearchDialog]: couldn't enable help key: help broker not exists");
        }
    }

    /**Besorgt die Daten f�r die Doublettentabelle.
     * @return Adressdaten.*/

    private Vector getData() {
        Vector data = new Vector();

        if ( myAddresses.getIndexMax() != -1 ) {
            Address currentAddress;

            for ( int i = 0; i <= myAddresses.getIndexMax(); i++ ) {
                currentAddress = myAddresses.get( i );
                Vector set = new Vector();

                String s = null;
                int n = 0;
                set.add( new Integer( currentAddress.getAdrNr() ) );
                s = currentAddress.getNachname();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getVorname();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getStrasse();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getHausNr();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getLkz();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getPlz();
                set.add( ( s != null ) ? s : "" );
                s = currentAddress.getOrt();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getInstitution();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getPlzPostfach();
                set.add( ( s != null ) ? s : "" );
                s = currentAddress.getPostfachNr();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getBundesland();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getTelefonDienstlich();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getFaxDienstlich();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getAnrede();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getTitel();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getAkadTitel();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$
                s = currentAddress.getEmailAdresseDienstlich();
                set.add( ( s != null ) ? s : "" ); //$NON-NLS-1$

                data.add( set );
            }
        }
        return data;
    }

    /**Besorgt die vom User ausgew�hlte Adresse.*/

    public Address getSelectedAddress() {
        int n = mySelectionModel.getMaxSelectionIndex();
        if ( n < 0 )
            return null;
        Vector chosenSet = (Vector) data.get( n );
        Address privAddress;
        privAddress = myAddresses.get( n );

        return privAddress;
    }

    /**Hilfsmethode f�r das Setzen der Spaltenbreite in der Tabelle. 
     */

    public void setColumnWidth( int pColumn, int pWidth ) {
        //Get the column model.
        TableColumnModel colModel = table.getColumnModel();
        //Get the column at index pColumn, and set its preferred width.
        colModel.getColumn( pColumn ).setPreferredWidth( pWidth );
    }

    /**Stellt das TableModel f�r die Tabelle zur Verf�gung.*/

    protected class MyTableModel extends DefaultTableModel {
        public boolean isCellEditable( int row, int column ) {
            return ( column == 0 );
        }

        public void setValueAt( Object value, int row, int col ) {
            Vector v = (Vector) dataVector.elementAt( row );
            v.setElementAt( value, col );
            fireTableCellUpdated( row, col );
        }
    }

    /** Stellt den Renderer f�r die Tabelle.
     */

    protected class PlanningTableRenderer extends JLabel implements TableCellRenderer {

        int[] alignments;

        protected Border _noFocusBorder;

        public PlanningTableRenderer() {
            super();
            _noFocusBorder = new EmptyBorder( 1, 2, 1, 2 );
            setOpaque( true );
            setBorder( _noFocusBorder );
            alignments = new int[0];
        }

        public void setAlignments( int[] alignments ) {
            this.alignments = alignments;
        }

        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column ) {
            if ( isSelected ) {
                setForeground( table.getSelectionForeground() );
                setBackground( table.getSelectionBackground() );
            }
            else {
                setBackground( table.getBackground() );
                setForeground( table.getForeground() );
            }

            setText( value.toString() );
            if ( column < alignments.length )
                setHorizontalAlignment( alignments[column] );
            else
                setHorizontalAlignment( JLabel.LEFT );

            return this;
        }
    }
    
    public String getStatus()
    {
    	return status != null? status : STATUS_ABORT;
    }

}
