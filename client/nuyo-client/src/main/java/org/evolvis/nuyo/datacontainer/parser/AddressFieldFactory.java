/*
 * Created on 21.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.parser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObject;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorage;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAction;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;


/**
 * @author niko
 *
 */
public class AddressFieldFactory {
    private final boolean isToShowErrorsOnMissingClasses = false;
    private final String classFileExtension = ".class";

    private String pluginFolderPath = StartUpConstants.DEFAULT_PLUGIN_FOLDER_PATH;
    private String pluginClassPath = pluginFolderPath.replace( '/', '.' );
    private PluginLocator pluginLocator;
    private Map dataStoragesMap;
    private Map dataSourcesMap;
    private Map guiElementsMap;
    private Map validatorsMap;
    private Map actionsMap;
    private Logger logger;

    public AddressFieldFactory( PluginLocator pluginlocator, Logger logger ) {
        this( pluginlocator, logger, null );
    }

    public AddressFieldFactory( PluginLocator pluginlocator, Logger aLogger, String[] pluginpath ) {
        pluginLocator = pluginlocator;
        logger = aLogger;

        guiElementsMap = new HashMap();
        validatorsMap = new HashMap();
        dataStoragesMap = new HashMap();
        dataSourcesMap = new HashMap();
        actionsMap = new HashMap();

        String[] fullpluginpath;
        if ( pluginpath != null ) {
            fullpluginpath = new String[pluginpath.length + 1];
            fullpluginpath[0] = StartUpConstants.DEFAULT_PLUGIN_FOLDER_PATH;
            for ( int i = 0; i < ( pluginpath.length ); i++ ) {
                fullpluginpath[i + 1] = pluginpath[i];
            }
        }
        else {
            fullpluginpath = new String[1];
            fullpluginpath[0] = StartUpConstants.DEFAULT_PLUGIN_FOLDER_PATH;
        }

        for ( int i = 0; i < ( fullpluginpath.length ); i++ ) {
            pluginFolderPath = fullpluginpath[i];

            pluginClassPath = pluginFolderPath.replace( '/', '.' );
            pluginClassPath = pluginClassPath.substring( 1 );

            readDataStorages();
            readDataSources();
            readValidators();
            readActions();
            readGUIElements();
        }
    }

    private void readValidators() {
        String[] names = pluginLocator.getClasses( pluginFolderPath + "Validators" );
        if ( names == null ) {
            if ( isToShowErrorsOnMissingClasses )
                logger.warning( "unable to locate validator classes." );
            return;
        }
        for ( int i = 0; i < ( names.length ); i++ ) {
            String name = names[i];
            if ( name.indexOf( '$' ) == -1 ) {
                if ( name.endsWith( classFileExtension ) ) {
                    String key = name.substring( 0, name.length() - classFileExtension.length() );
                    String classname = pluginClassPath + "Validators." + key;
                    try {
                        logger.config( "importing Validator \"" + key + "\"" );

                        Class newclass = Class.forName( classname );
                        if ( Validator.class.isAssignableFrom( newclass ) ) {
                            validatorsMap.put( key, classname );
                        }
                        else {
                            logger.severe( "unable to instanciate validator \"" + classname
                                + " because class not implementing validator interface." );
                        }
                    }
                    catch ( ClassNotFoundException e ) {
                        logger.severe( "unable to locate validator \"" + classname + "\"" );
                    }
                }
            }
        }
    }

    private void readDataStorages() {
        String[] names = pluginLocator.getClasses( pluginFolderPath + "DataStorages" );
        if ( names == null ) {
            if ( isToShowErrorsOnMissingClasses )
                logger.warning( "unable to locate datastorage classes." );
            return;
        }
        for ( int i = 0; i < ( names.length ); i++ ) {
            String name = names[i];
            if ( name.indexOf( '$' ) == -1 ) {
                if ( name.endsWith( classFileExtension ) ) {
                    String key = name.substring( 0, name.length() - classFileExtension.length() );
                    String classname = pluginClassPath + "DataStorages." + key;
                    try {
                        logger.config( "importing DataStorage \"" + key + "\"" );

                        Class newclass = Class.forName( classname );
                        if ( DataStorage.class.isAssignableFrom( newclass ) ) {
                            dataStoragesMap.put( key, classname );
                        }
                        else {
                            logger.severe( "unable to instanciate datastorage \"" + classname
                                + " because class not implementing datastorage interface." );
                        }
                    }
                    catch ( ClassNotFoundException e ) {
                        logger.severe( "unable to locate datastorage \"" + classname + "\"" );
                    }
                }
            }
        }
    }

    private void readDataSources() {
        String[] names = pluginLocator.getClasses( pluginFolderPath + "DataSources" );
        if ( names == null ) {
            if ( isToShowErrorsOnMissingClasses )
                logger.warning( "unable to locate datasource classes." );
            return;
        }
        for ( int i = 0; i < ( names.length ); i++ ) {
            String name = names[i];
            if ( name.indexOf( '$' ) == -1 ) {
                if ( name.endsWith( classFileExtension ) ) {
                    String key = name.substring( 0, name.length() - classFileExtension.length() );
                    String classname = pluginClassPath + "DataSources." + key;
                    try {
                        logger.config( "importing DataSource \"" + key + "\"" );

                        Class newclass = Class.forName( classname );
                        if ( DataSource.class.isAssignableFrom( newclass ) ) {
                            dataSourcesMap.put( key, classname );
                        }
                        else {
                            logger.severe( "unable to instanciate datasource \"" + classname
                                + " because class not implementing datasource interface." );
                        }
                    }
                    catch ( ClassNotFoundException e ) {
                        logger.severe( "unable to locate datasource \"" + classname + "\"" );
                    }
                }
            }
        }
    }

    private void readGUIElements() {
        String[] names = pluginLocator.getClasses( pluginFolderPath + "GUIElements" );
        if ( names == null ) {
            if ( isToShowErrorsOnMissingClasses )
                logger.warning( "unable to locate GUI element classes." );
            return;
        }
        for ( int i = 0; i < ( names.length ); i++ ) {
            String name = names[i];
            if ( name.indexOf( '$' ) == -1 ) {
                if ( name.endsWith( classFileExtension ) ) {
                    String key = name.substring( 0, name.length() - classFileExtension.length() );
                    String classname = pluginClassPath + "GUIElements." + key;
                    try {
                        logger.config( "importing GUIElement \"" + key + "\"" );

                        Class newclass = Class.forName( classname );
                        if ( GUIElement.class.isAssignableFrom( newclass ) ) {
                            guiElementsMap.put( key, classname );
                        }
                        else {
                            logger.severe( "unable to instanciate guielement \"" + classname
                                + " because class not implementing guielement interface." );
                        }
                    }
                    catch ( ClassNotFoundException e ) {
                        logger.severe( "unable to locate guielement \"" + classname + "\"" );
                    }
                }
            }
        }
    }

    private void readActions() {
        String[] names = pluginLocator.getClasses( pluginFolderPath + "Actions" );
        if ( names == null ) {
            if ( isToShowErrorsOnMissingClasses )
                logger.warning( "unable to locate action classes." );
            return;
        }
        for ( int i = 0; i < ( names.length ); i++ ) {
            String name = names[i];
            if ( name.indexOf( '$' ) == -1 ) {
                if ( name.endsWith( classFileExtension ) ) {
                    String key = name.substring( 0, name.length() - classFileExtension.length() );
                    String classname = pluginClassPath + "Actions." + key;
                    try {
                        logger.config( "importing Action \"" + key + "\"" );

                        Class newclass = Class.forName( classname );
                        if ( DataContainerListenerAction.class.isAssignableFrom( newclass ) ) {
                            actionsMap.put( key, classname );
                        }
                        else {
                            logger.severe( "unable to instanciate action \"" + classname
                                + " because class not implementing action interface." );
                        }
                    }
                    catch ( ClassNotFoundException e ) {
                        logger.severe( "unable to locate action \"" + classname + "\"" );
                    }
                }
            }
        }
    }

    private Class getClassForName( String classname ) {
        try {
            return Class.forName( classname );
        }
        catch ( ClassNotFoundException e ) {
        }
        return null;
    }

    public GUIElement getGUIElement( String name ) {
        if ( "textWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "TextField" );
        else if ( "multilineWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "TextAreaField" );
        else if ( "numberWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "IntegerField" );
        else if ( "checkWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "CheckBoxField" );
        else if ( "dateWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "DateField" );
        else if ( "floatWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "FloatField" );
        else if ( "comboWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "ComboField" );
        else if ( "listWidget".equalsIgnoreCase( name ) )
            return createGUIElement( "ListField" );
        else
            return createGUIElement( name );
    }

    private GUIElement createGUIElement( String key ) {
        if ( guiElementsMap.containsKey( key ) ) {
            Class c = getClassForName( (String) guiElementsMap.get( key ) );
            if ( GUIElement.class.isAssignableFrom( c ) ) {
                try {
                    return (GUIElement) ( c.newInstance() );
                }
                catch ( InstantiationException e ) {
                    e.printStackTrace();
                }
                catch ( IllegalAccessException e ) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public DataSource getDataSource( String name ) {
        if ( "Integer".equalsIgnoreCase( name ) )
            return createDataSource( "IntegerDataSource" );
        else if ( "String".equalsIgnoreCase( name ) )
            return createDataSource( "StringDataSource" );
        else if ( "Boolean".equalsIgnoreCase( name ) )
            return createDataSource( "BooleanDataSource" );
        else if ( "Date".equalsIgnoreCase( name ) )
            return createDataSource( "DateDataSource" );
        else if ( "Float".equalsIgnoreCase( name ) )
            return createDataSource( "FloatDataSource" );
        else if ( "Object".equalsIgnoreCase( name ) )
            return createDataSource( "ObjectDataSource" );
        else
            return createDataSource( name );
    }

    private DataSource createDataSource( String key ) {
        if ( dataSourcesMap.containsKey( key ) ) {
            Class c = getClassForName( (String) dataSourcesMap.get( key ) );
            if ( DataSource.class.isAssignableFrom( c ) ) {
                try {
                    DataSource datasource = (DataSource) ( c.newInstance() );
                    return datasource;
                }
                catch ( InstantiationException e ) {
                    e.printStackTrace();
                }
                catch ( IllegalAccessException e ) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public DataStorage getDataStorage( String name ) {
        if ( "Integer".equalsIgnoreCase( name ) )
            return createDataStorage( "IntegerDataStorage" );
        else if ( "String".equalsIgnoreCase( name ) )
            return createDataStorage( "StringDataStorage" );
        else if ( "Boolean".equalsIgnoreCase( name ) )
            return createDataStorage( "BooleanDataStorage" );
        else if ( "Date".equalsIgnoreCase( name ) )
            return createDataStorage( "DateDataStorage" );
        else if ( "Float".equalsIgnoreCase( name ) )
            return createDataStorage( "FloatDataStorage" );
        else if ( "Object".equalsIgnoreCase( name ) )
            return createDataStorage( "ObjectDataStorage" );
        else
            return createDataStorage( name );
    }

    private DataStorage createDataStorage( String key ) {
        if ( dataStoragesMap.containsKey( key ) ) {
            Class c = getClassForName( (String) dataStoragesMap.get( key ) );
            if ( DataStorage.class.isAssignableFrom( c ) ) {
                try {
                    DataStorage datastorage = (DataStorage) ( c.newInstance() );
                    return datastorage;
                }
                catch ( InstantiationException e ) {
                    e.printStackTrace();
                }
                catch ( IllegalAccessException e ) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public Validator getValidator( String name ) {
        Validator v = createValidator( name );
        if ( v == null )
            v = createValidator( name + "Validator" );
        return v;
    }

    private Validator createValidator( String key ) {
        if ( validatorsMap.containsKey( key ) ) {
            Class c = getClassForName( (String) validatorsMap.get( key ) );
            if ( Validator.class.isAssignableFrom( c ) ) {
                try {
                    Validator validator = (Validator) ( c.newInstance() );
                    return validator;
                }
                catch ( InstantiationException e ) {
                    e.printStackTrace();
                }
                catch ( IllegalAccessException e ) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public DataContainerListenerAction getAction( String name ) {
        if ( "refresh".equalsIgnoreCase( name ) )
            return createAction( "RefreshAction" );
        else if ( "copy".equalsIgnoreCase( name ) )
            return createAction( "CopyAction" );
        else if ( "lookup".equalsIgnoreCase( name ) )
            return createAction( "LookUpAction" );
        else {
            DataContainerListenerAction a = createAction( name );
            if ( a == null )
                a = createAction( name + "Action" );
            return a;
        }
    }

    private DataContainerListenerAction createAction( String key ) {
        if ( actionsMap.containsKey( key ) ) {
            Class c = getClassForName( (String) actionsMap.get( key ) );
            if ( DataContainerListenerAction.class.isAssignableFrom( c ) ) {
                try {
                    DataContainerListenerAction action = (DataContainerListenerAction) ( c.newInstance() );
                    return action;
                }
                catch ( InstantiationException e ) {
                    e.printStackTrace();
                }
                catch ( IllegalAccessException e ) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private String dumpDataContainer( DataContainerObject obj, String indent ) {
        String text = "";

        DataContainerObjectDescriptor dcod = obj.getDataContainerObjectDescriptor();
        if ( dcod != null ) {
            String name = dcod.getContainerName();
            String datatype = dcod.getContainerTypeName();
            List params = dcod.getParameterDescriptions();

            text += indent + "<name>" + name + "</name>\n";
            text += indent + "<datacontainertype>" + datatype + "</datacontainertype>\n";

            if ( params.size() > 0 ) {
                text += indent + "<parameters>\n";
                Iterator it = params.iterator();
                while ( it.hasNext() ) {
                    ParameterDescription desc = (ParameterDescription) ( it.next() );
                    String paramkey = desc.getKey();
                    String paramname = desc.getName();
                    String paramdesc = desc.getDescription();
                    List valdescs = desc.getValueDescriptions();
                    if ( valdescs != null ) {
                        text += indent + "  <parameter>\n";

                        text += indent + "    <parameterkey>" + paramkey + "</parameterkey>\n";
                        text += indent + "    <parametername>" + paramname + "</parametername>\n";

                        Iterator valdescsit = valdescs.iterator();
                        while ( valdescsit.hasNext() ) {
                            String[] vd = (String[]) ( valdescsit.next() );
                            String valuekey = vd[0];
                            String valuedes = vd[1];
                            text += indent + "    <valuedescription>\n";
                            text += indent + "      <valuekey>" + valuekey + "</valuekey>\n";
                            text += indent + "      <description>" + valuedes + "</description>\n";
                            text += indent + "    </valuedescription>\n";
                        }
                        text += indent + "  </parameter>\n";
                    }
                    else {
                        text += indent + "  <unknownparameters></unknownparameters>\n";
                    }
                }
                text += indent + "</parameters>\n";
            }
        }
        else {
            text += indent + "<unknown></unknown>\n";
        }
        return text;
    }

    private String dumpMap( Map map, String tagname, String indent ) {
        String text = "";
        Iterator it = map.entrySet().iterator();
        while ( it.hasNext() ) {
            Map.Entry entry = (Map.Entry) ( it.next() );
            String key = (String) ( entry.getKey() );
            String dcname = (String) ( entry.getValue() );
            DataContainerObject dc = createDataContainerObject( dcname );

            text += indent + "<" + tagname + " name=\"" + key + "\">\n";
            text += dumpDataContainer( dc, indent + "  " );
            text += indent + "</" + tagname + ">\n\n";
        }
        return text;
    }

    private DataContainerObject createDataContainerObject( String name ) {
        Class c = getClassForName( name );
        if ( DataContainerObject.class.isAssignableFrom( c ) ) {
            try {
                return (DataContainerObject) ( c.newInstance() );
            }
            catch ( InstantiationException e ) {
                e.printStackTrace();
            }
            catch ( IllegalAccessException e ) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String dumpConfig() {
        String text = "";

        text += "<plugins>\n\n";
        text += dumpMap( guiElementsMap, "GuiElement", "  " );
        text += dumpMap( validatorsMap, "Validator", "  " );
        text += dumpMap( dataStoragesMap, "GuiDataStorage", "  " );
        text += dumpMap( dataSourcesMap, "GuiDataSource", "  " );
        text += dumpMap( actionsMap, "Action", "  " );

        text += "</plugins>\n";
        return text;
    }

}
