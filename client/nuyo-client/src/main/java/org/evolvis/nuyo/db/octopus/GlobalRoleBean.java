/* $Id: GlobalRoleBean.java,v 1.3 2006/12/12 09:30:57 robert Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.GlobalRole;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;


/**
 *
 * Bean f�r eine GlobalRole
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public abstract class GlobalRoleBean extends AbstractEntity implements
		GlobalRole {

    private static Logger logger = Logger.getLogger(GlobalRoleBean.class.getName());
	
    /**	Objekt zum Holen von Rollen */
    static protected AbstractEntityFetcher _fetcher;

    public boolean isAdminRole(){
        try {
            return ROLE_TYPE_ADMIN == Integer.parseInt(getAttribute(GlobalRole.KEY_ROLE_TYPE));
        } catch (NumberFormatException nfe) {
            logger.log(Level.WARNING, "Kann Roletype von GlobalRole nicht parsen. Roletype: " + getAttribute(GlobalRole.KEY_ROLE_TYPE));
            return false;
        }        
    }
    
    public boolean isDeletable(){
    	return ! isAdminRole();
    }

	public boolean hasRightDelete() {
		return hasRight(KEY_DELETE).booleanValue();
	}

	public boolean hasRightEditFolder() {
		return hasRight(KEY_EDITFOLDER).booleanValue();
	}

	public boolean hasRightEditSchedule() {
		return hasRight(KEY_EDITSCHEDULE).booleanValue();
	}

	public boolean hasRightEditUser() {
		return hasRight(KEY_EDITUSER).booleanValue();	}

	public boolean hasRightRemoveFolder() {
		return hasRight(KEY_REMOVEFOLDER).booleanValue();	}

	public boolean hasRightRemoveSchedule() {
		return hasRight(KEY_REMOVESCHEDULE).booleanValue();	}

	public boolean hasRightRemoveUser() {
		return hasRight(KEY_REMOVEUSER).booleanValue();
	}

	public String getRoleName(){
		return getAttributeAsString(KEY_ROLENAME);
	}
	
	public void setRoleName(String name){
		setAttribute(KEY_ROLENAME, name);
	}

	public Boolean hasRight(String key){
		Object tmp = getAttributeAsObject(key);
		if(tmp!=null){
			if(tmp instanceof Boolean){
				return (Boolean) getAttributeAsObject(key);
			}
		}
		return Boolean.FALSE;
	}

	public void setRight(String key, Boolean newRight){
		setAttribute(key, newRight);
		
	}

	public String toString() {
		return getAttributeAsString(KEY_ROLENAME);
	}
	
	

}
