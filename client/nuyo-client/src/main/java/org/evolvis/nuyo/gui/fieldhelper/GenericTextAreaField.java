/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.Color;
import java.awt.Font;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextArea;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class GenericTextAreaField extends ContactAddressTextAreaField
{
  private TarentWidgetTextArea textArea; 
  private TarentWidgetLabel label;
  
  private String tooltipKey;
  private Object adressKey;
  private String labelKey;
  private String key;
  private int contextID;
  private int maxLength;  
  
  
  public GenericTextAreaField(String panelkey, Object adresskey, int aContext, String tooltipkey, String labelkey, int maxlength)
  {
    key = panelkey;
    adressKey = adresskey;
    tooltipKey = tooltipkey;
    labelKey = labelkey;
    maxLength = maxlength;
    contextID = aContext;
  }
  
  public String getFieldName()
  {
    if (fieldName != null) return fieldName; 
    else return Messages.getString(labelKey);
  }
  
  public String getFieldDescription()
  {
    if (fieldDescription != null) return fieldDescription;
    else return Messages.getString(tooltipKey);    
  }
  
  
  public int getContext()
  {
    return contextID;
  }
  
  
  public void postFinalRealize()
  {    
  }
  
  public TarentWidgetLabel getLabel()
  {
    return label;
  }
  
  public TarentWidgetTextArea getTextAreaField()
  {
    return textArea;
  }
  
  
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
    setDoubleCheckSensitive(textArea, issensitive);
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createPanel(tooltipKey, labelKey, maxLength, widgetFlags);
    
    return panel;
  }

  public String getKey()
  {
    return key;
  }

  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      textArea.setEditable(true);
    }
    else
    {  
      textArea.setEditable(false);
      textArea.setBackground(Color.WHITE);
    }
  }

  public void setDataFont(Font font)
  {
    textArea.setFont(font);      
    label.setFont(font);      
  }

  public void setData(PluginData data)
  {
    Object value = data.get(adressKey);
    if (value != null) textArea.setText(value.toString());
    else textArea.setText("");
  }

  public void getData(PluginData data)
  {
    data.set(adressKey, textArea.getText());
  }

  public boolean isDirty(PluginData data)
  {
    if (!((textArea.getText().equals("")) &&  (data.get(adressKey) == null ))) //$NON-NLS-1$
      if (!(textArea.getText().equals(data.get(adressKey)))) return(true);            

    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  private EntryLayoutHelper createPanel(String tooltipkey, String labelkey, int maxlength, String widgetFlags)
  {    
    textArea = createTextArea(getFieldDescription(), maxlength);
    label = new TarentWidgetLabel(getFieldName());     
    return(new EntryLayoutHelper(label, textArea, widgetFlags));
  }

}
