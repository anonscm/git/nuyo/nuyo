package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.Binding;
import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Context;

public class EditContactAction extends AbstractBindingRestrictedAction {

	private static final TarentLogger logger = new TarentLogger(
			EditContactAction.class);

	protected Binding[] initBindings() {
		// A binding that listens on changes to the currently selected address
		// in order to check the edit right. The result of that check combined
		// with whether the action is in the current context determines the
		// action's enabled state.
		return new Binding[] { new AbstractReadOnlyBinding(
				ApplicationModel.SELECTED_ADDRESS_KEY) {

			public void setViewData(Object arg) {
                // check if null or dummy-object
                // note: dummy object means 'nothing selected'
                // if nothing selected application model returns a dummy object and the selected address index is thereby '0'.  
                // See AmSelection.setAdress() for details about creation of dummy object
                //TODO: find a better way to be aware of a dummy-object (this is just a workaround for bug 2773)
                boolean isDummyObject = ApplicationServices.getInstance().getApplicationModel().getAddresses().getSize() == 0;
                if (arg == null || isDummyObject) {
                    setBindingEnabled(false);
                    updateActionEnabledState();
                    return;
                } 

				final Address a = (Address) arg;

				/*
				 * Deactivated because this will lead to an ugly flickering edit-button when
				 * rapidly changing contact-selection. 
				 * In the event that for the (short) moment in which the button-state does
				 * not represent the correct right to edit the contact and the user activates
				 * this action there will pop up a (hopefully) meaningful dialog.
				 * This is the same case like when the right changes on server intermediate.
				 * 
				 *   -- Fabian 
				 * 
				 */
				// Assume address is not-editable and disable component first.
				//setBindingEnabled(false);

				updateActionEnabledState();

				// Now find out the real state concurrently and update the GUI
				// accordingly.
				TaskManager.getInstance().register(
						new TaskManager.SwingTask() {
							public void runImpl(TaskManager.Context ctx) {
								GUIListener am = ApplicationServices
										.getInstance().getActionManager();
								setBindingEnabled(am
										.userRequestUserHasRightOnAddress(
												org.evolvis.nuyo.security.SecurityManager.FOLDERRIGHT_EDIT,
												a));
							}

							protected void failed(Context arg0) {
								// Nothing to do.
							}

							protected void succeeded(Context arg0) {
								updateActionEnabledState();
							}
						},
						Messages.getString("EditContactAction_CheckingEditRight"),
						false);
			}

		} };

	}

	public void actionPerformed(ActionEvent e) {

		Runnable editThread = new Runnable() {

			public void run() {
				try {
					Veto veto = ApplicationServices.getInstance()
							.getActionManager().userRequestEditable();
					if (veto != null) {
						logger.warning(veto.getMessage());
					}
				} catch (ContactDBException cdbe) {
					logger.warning("Bei der �berpr�fung der ben�tigten Rechte trat ein Fehler auf.",
								   cdbe);
				} catch (IllegalStateException e) {
                    logger.warning(e.getMessage(), e);
                }
			}
		};

		new Thread(editThread).start();
	}

}
