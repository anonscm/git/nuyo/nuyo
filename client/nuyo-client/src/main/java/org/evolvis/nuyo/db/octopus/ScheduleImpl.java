package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Schedule;
import org.evolvis.nuyo.db.User;



/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.Schedule
 *
 *	Dies ist eine �bergeordnete Klasse, die Kalendar-�bergreifenden
 *	Zugriff auf Appointments, Reminders, etc. bieten soll.
 *
 */


public class ScheduleImpl implements Schedule {
    
    //
    //	Soapanbindung	
    //
    /** Datenbank-ID des Appointments */
	private static final String PARAM_EVENTID = "eventid";
    /** Startzeitpunkt des Appointments */
	private static final String PARAM_START = "start";
    /** Endzeitpunkt des Appointments */
	private static final String PARAM_END = "end";
    /** Datenbank-ID des Benutzers */
	private static final String PARAM_USERID = "userid";

	//
	//	Instanzmerkmale
	//
	/** Dieses Objekt ist immer einem Benutzer zugeordnet */
	private User _user;
    
	/** Konstruktor zur Erzeugung des Objects */
	protected ScheduleImpl (User user) {
       _user = user;
    }
    
    /** Neues Appointment erzeugen
     * 
     *	@param calendars - Alle Kalender, in die es eingetragen werden soll
     *	@param isJob - Termin oder Aufgabe
     *	@param start - Startzeitpunkt des Appointments 
     *	@param end - Endzeitpunkt des Appointments
     *	@param mayConflict - Soll ein Konflikt ignoriert werden?
     * 
     */
    public Appointment createAppointment(Collection calendars, boolean isJob,
            Date start, Date end, boolean mayConflict, User User)
            throws ContactDBException {
        Appointment appointment = new AppointmentImpl(null, calendars, isJob, start, end);
        if (User != null){
            appointment.setOwner(User);
        }else{
            appointment.setOwner(_user);
        }

        return appointment;
    }


    /** Alle Reminder beziehen 
     *
     *	@param start - Startzeitpunkt
     *	@param end - Endzeitpunkt
     *	@filters - Filter
     *  
     */
    public Collection getReminders(Date start, Date end, Collection filters)
            throws ContactDBException {
        return new ArrayList();
    }

//     /** Beziehen eines einzelnen Appointments. @param id - Datenbank-ID des Appointments */
// 	public Appointment getAppointment(int id) throws ContactDBException {
//         Method method = new Method(METHOD_GET_EVENTS);
//         method.addParam(PARAM_EVENTID, new Integer(id));
// 		List eventList = (List)method.invoke();
// 		if (eventList != null || eventList.size() > 0) {
// 		    if (eventList.get(0) instanceof Object[]) {
// 		        Object[] entry = (Object[])eventList.get(0);
// 				Appointment appointment = new AppointmentImpl();
// 				((IEntity)appointment).populate(new ResponseData(entry));
// 				return appointment;
// 		    } else
// 		        return null;
// 		} else {
// 			return null;
// 		}
// 	}
    
}
