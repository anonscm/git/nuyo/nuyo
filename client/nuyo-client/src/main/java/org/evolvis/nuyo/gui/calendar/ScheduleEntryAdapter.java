package org.evolvis.nuyo.gui.calendar;
/*
 * Created on 18.07.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ScheduleEntryAdapter implements ScheduleEntryListener
{
  public void startMoving(ScheduleEntryPanel entry, int x, int y) {}
  public void startResizing(ScheduleEntryPanel entry, int size) {}
  public void stopMoving(ScheduleEntryPanel entry, int x, int y) {}
  public void stopResizing(ScheduleEntryPanel entry, int size) {}
  public void isMoving(ScheduleEntryPanel entry, boolean ismoving, int x, int y) {}
  public void isResizing(ScheduleEntryPanel entry, boolean isresizing, int size) {}
  public void doubleClicked(ScheduleEntryPanel entry) {}
  public void editSelected(ScheduleEntryPanel entry) {}
  public void deleteSelected(ScheduleEntryPanel entry) {}
  public void addedEntry(ScheduleEntryPanel entry, ScheduleDate startDate, ScheduleDate EndDate, Integer calendarID) {}
  public void removedEntry(ScheduleEntryPanel entry) {}
  public void changedValues(ScheduleEntryPanel entry) {}
  
}
