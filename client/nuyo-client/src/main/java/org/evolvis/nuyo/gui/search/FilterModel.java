package org.evolvis.nuyo.gui.search;

import java.awt.Component;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.evolvis.nuyo.groupware.Address;
import org.evolvis.nuyo.groupware.AddressProperty;

/**
 * Is a filter model to analyze, validate and synchronize its data with GUI.<p>
 * 
 * @see org.evolvis.nuyo.gui.search.FilterModelListener
 * @see org.evolvis.nuyo.gui.search.SearchFilterPanel
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
class FilterModel {
    
    private static final Logger LOGGER = Logger.getLogger(FilterModel.class.getName());
    private LinkedList conditions;
    private AddressProperty[] attributes;
    private Map operators;
    private FilterModelListener listener;
    
    private AttributeRenderer attributeRenderer = new AttributeRenderer();

    /**
     * Creates an instance of a filter model with the given properties/attributes.<p> 
     * The operators will be assigned only for {@link String} and {@link Date} based properties.
     * <p>
     * Here is an example of the properties array creation:
     * <pre>        
     *    LabeledProperty[] attributes = new LabeledProperty[]{
     *     new LabeledProperty("forename", String.class.getName()),
     *     new LabeledProperty("surname", String.class.getName()),
     *     new LabeledProperty("address", String.class.getName()),
     *     new LabeledProperty("birthday", Date.class.getName())
     *   }; 
     * <pre>
     *  
     * @param theAttributes array of {@link LabeledProperty} objects.
     * @exception NullPointerException if attributes missing.
     */
    FilterModel(AddressProperty[] theAttributes){
        this(theAttributes, null);
    }
    
    /** Creates a {@link FilterModel} instance which is properly configured
     * with address properties destined for search requests.
     * 
     * <p>The {@link AddressProperty} class defines a set of properties
     * which can be used in a search request. This method iterates through
     * that list and generates a {@link FilterModel} from them.</p>
     *  
     * @return
     */
    static FilterModel createSearchPropertyFilterModel()
    {
      Address.class.getName();
      
      // Retrieves all searchable address properties and wraps them up in a
      // LabeledProperty instances which are the suitable type for the 
      // search dialog.
      List searchProps = AddressProperty.getSearchPropertiesSorted();
      
      return new FilterModel((AddressProperty[]) searchProps.toArray(new AddressProperty[searchProps.size()]));
    }
    
    /**
     * Creates an instance of a filter with the given properties and operators.<p>
     * The operators are assigned to each property data type throught a map:
     * <li> a key is a {@link String} and represents a full class name of an attribute data type.
     * <li> a value is an array of valid {@link LabeledFilterOperator} instances.
     * <p>
     * Here is an example of the map creation:
     * <pre>
     *  Map operators = new HashMap();
     *  operators.put(String.class.getName(), LabeledFilterOperator.STRING_BASED_SET);
     *  operators.put(Date.class.getName(), LabeledFilterOperator.DATE_BASED_SET);
     * </pre>  
     * <p>
     *  
     * @param theAttributes is an array of {@link LabeledProperty} objects.
     * @param theOperators is a map for known property data types. 
     * @exception NullPointerException if attributes missing.
     */
    FilterModel(AddressProperty[] theAttributes, Map theOperators){
        if(theAttributes == null) new NullPointerException("filter model requires attributes");
        attributes = theAttributes;
        operators = theOperators;
        if(theOperators == null) initOperatorsMap();
        reloadData();
    }

    private void initOperatorsMap() {
        operators = new HashMap();
        operators.put(String.class, LabeledFilterOperator.STRING_BASED_SET);
        operators.put(Date.class, LabeledFilterOperator.DATE_BASED_SET);
    }

    private void reloadData() {
        conditions = new LinkedList();
        addDefaultCondition();
        if(listener != null) listener.fullUpdatePerformed();
    }

    /**
     * Sets a (visual) listener in order to synchronize a filter representation, which is given by this listener.<p> 
     * @param aListener to notify about filter changes
     */
    void setFilterModelListener( FilterModelListener aListener ) {
        listener = aListener;
    }


    private void addDefaultCondition() {
        if(attributes.length == 0 ) throw new RuntimeException("no filter attributes registered");;
        AddressProperty attr = attributes[0];
        LabeledFilterOperator[] validOperators = getOperatorsForType(attr.getDataType());
        if(validOperators.length == 0) throw new RuntimeException("no filter operators registered for attribute type: " + attr.getDataType());
        SearchCondition c = new SearchCondition(attributes[0], validOperators[0], "my parameter");
        c.setRemovable(false);
        addCondition(c);
    }

    /**
     * Returns a snapshot (= copy) of the list of
     * {@link SearchCondition} instances.
     * 
     * In order to get the up-to-date parameter values,
     * the model forces synchronisation with GUI first.
     *  
     * @return List of conditions
     */
    List getConditions() {
        if(listener != null) listener.pickParamsPerformed();
        return new LinkedList(conditions);
    }

    private int getPositionToAdd( SearchCondition conditionToAddAfter ) {
        int lastPosition = conditions.size();
        for(int i = 0; i < lastPosition; i++) {            
            if(getConditionAt(i).equals(conditionToAddAfter)) return i + 1;//after current position
        }
        return lastPosition;
    }

    private SearchCondition getConditionAt( int index ) {
        return (SearchCondition) conditions.get(index);
    }

    /**
     * Returns an array of assigned operators for a given property/attribute data type.
     * @param attributeDataType
     * @return LabeledFilterOperator[]
     */
    LabeledFilterOperator[] getOperatorsForType( Class attributeDataType ) {
        if(!operators.containsKey(attributeDataType)) {
            LOGGER.warning("[!] operators not registered for type: " + attributeDataType);
            return new LabeledFilterOperator[0];//empty
        } 
        return (LabeledFilterOperator[]) operators.get(attributeDataType);
    }

    /**
     * Clears the constructor properties/attributes and sets the given.
     * For this purpose the model will be cleared and a default condition with the first attribute will be added.<p>
     * 
     * @param theAttributes to register
     * @exception NullPointerException if attributes missing
     */
    void reregisterAttributes( AddressProperty[] theAttributes ) {
        if(theAttributes == null) throw new NullPointerException("no filter attributes to register");
        attributes = theAttributes;
        reloadData();
    }

    /**
     * Clears the current/default operators assignment.
     * For this purpose the model will be cleared and a default condition with the first operator will be added.<p>
     * 
     * Reregisters the operators.
     * @param typeToOperatorsArray
     */
    void reregisterOperators( Map typeToOperatorsArray ) {
        if(typeToOperatorsArray == null) throw new NullPointerException("no operators to register");
        operators = typeToOperatorsArray;
        reloadData();
    }

    /**
     * Appends a given condition if not yet registered,
     * otherwise creates a copy of it inserts at position after.
     * @param condition
     */
    void addCondition( SearchCondition condition ) {
        int positionToAdd = getPositionToAdd( condition );
        LOGGER.fine("[model.add] at position: " + positionToAdd);
        // make a copy of instance if already registered
        SearchCondition newCondition = ( conditions.contains( condition ) 
        ? SearchCondition.copy( condition )
        : condition );
        boolean isAppendEvent = conditions.size() == positionToAdd;
        conditions.add( positionToAdd, newCondition );
        
        if(listener != null) {
            if ( isAppendEvent )
                listener.appended(newCondition);
            else
                listener.inserted(positionToAdd, newCondition);
        }
        
        // Switches removeability depending on the number of search conditions:
        // If more than one is available than the first can be removed otherwise
        // not.
        setRemovable(0, conditions.size() > 1);
    }

    private void setRemovable( int position, boolean value ) {
        SearchCondition element = (SearchCondition) conditions.get(position);
        if(element.isRemovable() == value) return;
        element.setRemovable(value);
        if(listener != null) listener.setRemovablePerformed(element);
    }

    /**
     * Removes a given condition. If the instance of this condition is not registered, 
     * then all it's equivalent copies will be removed.<p> 
     * @param condition to remove
     * @return 'true' if condition self or it's copies removed,
     *         'false' if a given condition is not removable or not found 
     *         
     */
    boolean removeCondition( SearchCondition condition ) {
        if(!conditions.contains(condition)) return removeAllEquivalentConditionsTo(condition);
        if(!condition.isRemovable()) return false;
        int position = conditions.indexOf(condition); 
        conditions.remove(condition);
        if(conditions.size() == 1) setRemovable(0, false);
        if(listener != null) listener.removed(position, condition);
        return true;
    }
    
    /** Removes all entries currently in the model.
     * 
     * <p>Cleaning the model is needed to reinitialize
     * it with a stored state later.</p> 
     */
    void removeAllConditions()
    {
      while (!conditions.isEmpty())
        {
          SearchCondition c = (SearchCondition) conditions.removeLast();
          listener.removed(conditions.size(), c);
        }
    }
    
    private boolean removeAllEquivalentConditionsTo( SearchCondition condition ) {
        boolean atLeastOneRemoved = false;
        for(Iterator iter = getConditions().iterator(); iter.hasNext();){
            SearchCondition nextCondition = (SearchCondition) iter.next();
            if(nextCondition.isCopyOf(condition)){
                conditions.remove(nextCondition);
                atLeastOneRemoved = true;
            }
        }
        if(atLeastOneRemoved && (listener != null)) listener.fullUpdatePerformed();
        return atLeastOneRemoved;
    }

    /**
     * Removes all duplicates.
     */
    void removeDuplicates(){
        //synchronize with GUI
        int count = getConditions().size(); 
        if(count <= 1) return;//nothing to do
        //create the list of unique elements
        LinkedList uniqueConditions = new LinkedList();
        for(int i = 0; i < count; i++){
            SearchCondition currentCondition = (SearchCondition) conditions.get(i);
            //check for equivalent elements in the unique list 
            boolean duplicatesFound = false;            
            for(int j = 0; j < uniqueConditions.size(); j++){
                SearchCondition conditionToCompare = (SearchCondition) uniqueConditions.get(j);
                if(conditionToCompare.isCopyOf(currentCondition)) {
                    duplicatesFound = true;
                    break;
                }
            }
            //add unique element
            if(!duplicatesFound) uniqueConditions.add(currentCondition);
        }
        if(uniqueConditions.size() < count ) {
            //update model
            conditions = uniqueConditions;
            if(listener != null) listener.fullUpdatePerformed(); 
        }
    }

    /**
     * Returns an array the registered attributes.
     * @return LabeledProperty[] 
     */
    AddressProperty[] getAttributes() {
        return attributes;
    }

    /**
     * Returns the conditions count.
     * @return int
     */
    int getConditionsCount() {
        return conditions.size();
    }

    /**
     * Updates an attribut of a given condition and notifies a listener.  
     * @param newAttribute
     * @param condition
     */
    void updateAttribute( AddressProperty newAttribute, SearchCondition condition ) {
      AddressProperty oldAttribute = condition.getAttribute();
        if(oldAttribute.equals(newAttribute)) return;
        LOGGER.fine("[updating attribute]: " + newAttribute);
        condition.setAttribute(newAttribute);
        if(listener != null) listener.attributeChanged(condition);
        if(!oldAttribute.getDataType().equals(newAttribute.getDataType())) {
            LabeledFilterOperator[] operators = getOperatorsForType(newAttribute.getDataType()); 
            condition.setOperator(operators[0]);
            if(listener != null) listener.operatorsChanged(operators, condition);
        }
    }

    /**
     * Updates an operator of a given condition and notifies a listener.
     * @param newOperator
     * @param condition
     */
    void updateOperator( LabeledFilterOperator newOperator, SearchCondition condition ) {
        SearchCondition anElement = (SearchCondition) condition;  
        LabeledFilterOperator oldOperator = anElement.getOperator();
        if(oldOperator.equals(newOperator)) return;
        anElement.setOperator((LabeledFilterOperator) newOperator);
        if(listener != null) listener.operatorChanged(anElement);
    }

    /**
     * Updates a parameter text of a given element and notifies a listener.
     * @param newText
     * @param condition
     */
    void changeParam( String newText, SearchCondition condition ) {
        SearchCondition anElement = (SearchCondition) condition;  
        if(newText.equals(condition.getParamText())) return;
        anElement.setParamText(newText);
        if(listener != null) listener.paramChanged(anElement);
    }

    /** 
     * Updates parameter without any listener notification. 
     * @param newText is a value of a given parameter
     * @param condition to be updated
     */
    void updateParamOnly( String newText, SearchCondition condition ) {
        ((SearchCondition) condition).setParamText(newText);
    }
    
    /**
     * Returns a customized {@link ListCellRenderer} for
     * comboboxes.
     * 
     * <p>If the renderer instance is not used a combobox will
     * wrongly display an {@link AddressProperty}s key instead
     * of the label.</p>
     * 
     * @return
     */
    ListCellRenderer getAttributeRenderer()
    {
      return attributeRenderer;
    }
    
    /**
     * A customized {@link ListCellRenderer} to be used for {@link javax.swing.JComboBox}es
     * which display lists of {@link AddressProperty} instances.
     * 
     * <p>By going this route separate wrapper objects are avoided which would
     * cause trouble for the serialization and deserialization mechanism
     * of the search dialog because object identities would not be preserved.</p>
     * 
     */
    private static class AttributeRenderer extends DefaultListCellRenderer
    {
      public Component getListCellRendererComponent(JList l, Object value, int index, boolean isSelected, boolean hasCellFocus)
      {
        super.getListCellRendererComponent(l, value, index, isSelected, hasCellFocus);
        
        setText(((AddressProperty) value).getLabel());
        
        return this;
      }
    }
}
