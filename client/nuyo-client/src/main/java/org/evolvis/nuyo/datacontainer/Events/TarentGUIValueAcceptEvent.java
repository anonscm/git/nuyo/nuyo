/*
 * Created on 23.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Events;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;

/**
 * @author niko
 *
 */
public class TarentGUIValueAcceptEvent extends TarentGUIEvent
{
  private Object m_oValue = null;
  
  public TarentGUIValueAcceptEvent(DataContainer sender, Object newvalue)
  {
    super(TarentGUIEvent.GUIEVENT_ACCEPT, sender, null);
    m_oValue = newvalue;
  }
  
  public Object getValue()
  {
    return m_oValue;
  }
}
