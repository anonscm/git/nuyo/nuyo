package org.evolvis.nuyo.gui.action;

/**
 * this interface marks a class needs an Addresslist for its 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public interface AddressListConsumer {
	
	public void registerDataProvider(AddressListProvider provider);
	
}
