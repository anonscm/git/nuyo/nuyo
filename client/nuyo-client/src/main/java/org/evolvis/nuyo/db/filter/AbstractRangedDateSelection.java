package org.evolvis.nuyo.db.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author kleinw
 *
 *	Selectionelement f�r Datumseintr�ge
 *
 */
abstract public class AbstractRangedDateSelection extends AbstractSelection {

    static private final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"); 
    
    //
    //	Instanzmerkmale
    //
    /**	Startdatum */
    private Long _start;
    /** Enddatum */
    private Long _end;
    
    
    /**	Datumsangaben werden normalisiert */
    static private Calendar _calendar;
    
    
    /**	Statischer Konstruktor */
    static {_calendar = Calendar.getInstance();}
    
    
    /**	Konstruktion per Date */
    public AbstractRangedDateSelection(Date start, Date end) {
        _start = new Long(AbstractRangedDateSelection.normalize(start).getTime());
        _end = new Long(AbstractRangedDateSelection.normalize(end).getTime());
    }
    
    /**	Konstruktion per long */
    public AbstractRangedDateSelection(Long start, Long end) {
        _start = start;
        _end = end;
    }
    
    
    /**	Konstruktion per String */
    public AbstractRangedDateSelection(String start, String end) throws ParseException {
        _start = (start == null)?null:new Long(SDF.parse(start).getTime());
        _end = (end== null)?null:new Long(SDF.parse(end).getTime());
    }
    
    
    /**	SelectionElement in Map schreiben */
    public void includeInMethod(Map map) throws ContactDBException {
        if(_start!= null)
            map.put(getFilterKey() + "start", _start);
        if(_end != null)
            map.put(getFilterKey() + "end", _end);
    }
    
    
    /** Werte abholen */
    public Long getStart() {return _start;}
    /** Werte abholen */
    public Long getEnd()  {return _end;}
    
    
    /** Schl�ssel in der Map */
    abstract String getFilterKey();
    

    /**	Stunde, Minute, Sekunde, Millisekunde werden hier abgeschnitten */
    static private Date normalize(Date d) {
        _calendar.setTime(d);
        _calendar.set(Calendar.HOUR_OF_DAY, 0);
        _calendar.set(Calendar.MINUTE, 0);
        _calendar.set(Calendar.SECOND, 0);
        _calendar.set(Calendar.MILLISECOND, 0);
        Date tmp = _calendar.getTime();
        return tmp;
    }
    
    
	/**	Damit die Selection vom Cache wiedergefunden wird */
    public boolean equals(Object arg0) {
        if(arg0 instanceof AbstractRangedDateSelection) {
            AbstractRangedDateSelection selection = (AbstractRangedDateSelection)arg0;
            if(selection.getStart().longValue() == getStart().longValue())
                if(selection.getEnd().longValue() == getEnd().longValue())
                    return true;
        }
        return false;
    }
    
    
    /**	Stringrepr�sentation */
    public String toString() {
        return new StringBuffer()
        	.append(SDF.format(_start))
        	.append(" - ")
        	.append(SDF.format(_end))
        	.toString();
    }
    
    
    /**	Filter f�r Zeitintervall */
    static public class RangedDate extends AbstractRangedDateSelection {
        public RangedDate(Date start, Date end) {super(start, end);}
        public RangedDate(String start, String end) throws ParseException {super(start, end);}
        public RangedDate(Long start, Long end) {super(start, end);}
        
        String getFilterKey() {return "date";}
        
    }
    
}
