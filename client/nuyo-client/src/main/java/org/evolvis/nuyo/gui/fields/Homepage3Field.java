/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class Homepage3Field extends GenericTextField
{
  public Homepage3Field()
  {
    super("HOMEPAGE3", AddressKeys.HOMEPAGE3, CONTEXT_ADRESS, "GUI_Fields_Web3_ToolTip", "GUI_Fields_Web3", 100);
  }
}
