/*
 * Created on 18.07.2003
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

/**
 * @author niko
 *
 */
public class StandardScheduleType implements ScheduleType
{
  private String            m_sName;
  private String            m_sDescription;
  private ImageIcon     m_oIcon;
  private Map              m_oActions;
  private int           m_iKey;
  private int                 m_iDatabaseKey;
  private Object m_oExternalKey;
  
  public StandardScheduleType(int key, String name, String description, ImageIcon icon, Object externalkey)
  {
    m_sName = name;
    m_sDescription = description;
    m_oIcon = icon;
    m_oActions = new HashMap();;    
    m_iKey = key;
    m_oExternalKey = externalkey;
  }
  
  public Map getActions()
  {
    return m_oActions;
  }

  public int getKey()
  {
    return m_iKey;
  }
  

  public ImageIcon getIcon()
  {
    return m_oIcon;
  }

  public String getDescription()
  {
    return m_sDescription;
  }

  public String getName()
  {
    return m_sName;
  }

  public Object getExternalKey()
  {
    return m_oExternalKey;
  }
}
