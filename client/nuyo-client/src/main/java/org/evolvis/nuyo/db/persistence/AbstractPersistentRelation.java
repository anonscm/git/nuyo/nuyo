package org.evolvis.nuyo.db.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.remote.Method;



/**
 * @author kleinw
 *
 *	Dieses Objekt dient zur Verwaltung von 1-n Beziehungen,<br>
 *	die ein Objekt PersistentEntity besitzen kann.<br>
 *	<br>
 *	Was wirklich hinzugef�gt oder gel�scht wird, entscheidet sich<br>
 *	erst bei dem Aufruf von Parent.commit(), wo der aktuelle<br> 
 *	Datenbankstand erst geladen wird.<br>
 *<br>
 *	Kann gebraucht werden bei:<br>
 *	- UserGroup.add(user)<br>
 *	- UserGroup.remove(user)<br>
 *	- UserGroup.addManager(manager)<br>
 *	- UserGroup.removeManager(manager)<br>
 *<br>
 *	- Calendar.add(User)<br>
 *	- Calendar.remove(User)<br>
 *	- Calendar.add(UserGroup)<br>
 *	- Calendar.remove(UserGroup)<br>
 *	...<br>
 */
abstract public class AbstractPersistentRelation {

    /**	hinzuzuf�gende Relation **/
    private Collection _relationsToAdd;
    /**	zu l�schende Relation **/
    private Collection _relationsToRemove;
    /**	bereits in der Datenbank vorhandene Eintr�ge */
    private Collection _persistentRelations;
    /**	Hier werden Relations gemerkt, die geadded UND tats�chlich commited wurden. */
    private Collection _addedAndCommitted;
    /**	Hier werden Relations gemerkt, die removed UND tats�chlich commited wurden. */
    private Collection _removedAndCommitted;
    
    /**	Parentobjekt f�r 1-n Beziehungen	*/
    private IEntity _entity;
    
    
    /**	Dies ist das Objekt auf der anderen Seite der Relation */
    private IEntity _relateTo;
    
    
    public AbstractPersistentRelation(IEntity entity) {
        _entity = entity;
    }
    
    
    /**
     * @param relation - Hinzuzuf�gendes Objekt
     * @throws ContactDBException
     * 
     * Diese Methode f�gt neue Objekte ein.
     * 1. Fall - Objekt ist neu angelegt, dann wird Objekt 
     * hinzugef�gt, wenn es noch nicht in der Liste vorkommt.
     * 
     * 2. Fall - Objekt existiert in der Datenbank,
     * dann wird Relation hinzugef�gt, wenn es noch nicht in
     * der Datenbank vorhanden ist.
     */
    public void add(IEntity relation) 
    	throws ContactDBException {
        if(PersistenceManager.isTransient(relation))
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR,
                    "Das Objekt muss mit 'commit' gespeichert werden, bevor es zugeordnet werden kann.");
		if(_relationsToRemove!=null&&_relationsToRemove.contains(relation)){
			//sollte schon mal gel�scht werden, f�ge wieder hinzu...
			_relationsToRemove.remove(relation);
			relateTo(relation);
		}else{
			if(_persistentRelations!=null&&_persistentRelations.contains(relation)){
				//H�ngt schon dran, also mache nichts...
			}else if(_relationsToAdd!=null&&!_relationsToAdd.contains(relation)){
				//Soll auch nicht bereits hinzugef�gt werden
	            _relationsToAdd.add(relation);
	            _entity.setDirty(true);
				relateTo(relation);
			}else if(_relationsToAdd==null){
	            _relationsToAdd = new ArrayList();
	            _relationsToAdd.add(relation);
	            _entity.setDirty(true);
				relateTo(relation);
			}
		}
    }
    
    
    /**
     * @param relation - zu l�schendes Objekt
     * @throws ContactDBException
     *
     * Diese Methode l�scht Relations.
     * 1. Fall - Objekt ist neu, dann wird nichts in
     * die L�schliste eingetragen. 
     * 
     * 2. Fall - Objekt existiert in der Datenbank,
     * dann wird Objekt in die Liste eingetragen,
     * wenn es in der Datenbank existiert.
     * 
     */
    public void remove(IEntity relation) 
    	throws ContactDBException {
		if(_relationsToAdd!=null&&_relationsToAdd.contains(relation)){
			//Objekt ist schon in der Add-Liste
				_relationsToAdd.remove(relation);
				killRelation(relation);
		}else if(_relationsToRemove!=null&&_relationsToRemove.contains(relation)){
			//Soll schon entfernt werden, wir brauchen nichts zu tun
		}else if(_persistentRelations!=null&&_persistentRelations.contains(relation)){
			//H�ngt dran, wir k�nnen l�schen
	        if (_relationsToRemove == null)
	            _relationsToRemove = new ArrayList();
	        _relationsToRemove.add(relation);
			killRelation(relation);
		}
    }
    	
    
    /**	Vorbereitung der Soapanfrage. @param method - Soapmethode, in die die Relation eingef�gt werden soll */
    public void includeInMethod(Method method) 
    	throws ContactDBException {
        if (_relationsToAdd != null) {
            _addedAndCommitted = new ArrayList();
            if (_entity.getId() != 0) {
                _persistentRelations = getRelationContents();
                Collection tmp = new ArrayList();
                for (Iterator it = _relationsToAdd.iterator();it.hasNext();) {
                    IEntity entity = (IEntity)it.next();
                    if (!_persistentRelations.contains(entity)) {
                        tmp.add(new Integer(entity.getId()));
                        _addedAndCommitted.add(entity);
                    }
                }
                if (tmp.size() > 0) {
                    method.add(getFields()[0], tmp);
                }
            } else {
            	method.addIdList(getFields()[0], _relationsToAdd);
            	_addedAndCommitted = _relationsToAdd;
            }
        }
        if (_relationsToRemove != null) {
            if (_entity.getId() != 0) {
                if (_persistentRelations == null)
                    _persistentRelations = getRelationContents();
                Collection tmp = new ArrayList();
                _removedAndCommitted = new ArrayList();
                for (Iterator it = _relationsToRemove.iterator();it.hasNext();) {
                    IEntity entity = (IEntity)it.next();
                    if (_persistentRelations.contains(entity)) {
                        tmp.add(new Integer(entity.getId()));
                        _removedAndCommitted.add(entity);
                    }
                }
                if (tmp.size() > 0) {
                    method.add(getFields()[1], tmp);
                    
                }
            } else {
                method.addIdList(getFields()[1], _relationsToRemove);
                _removedAndCommitted = _relationsToRemove;
            }
        }
    }
	   
    
    /**	Holen des aktuellen Datenbestandes (persistente Daten + lokale Daten) */
    public Collection getActualRelations() throws ContactDBException {
        Collection actualRelations = new ArrayList();
        if (_relationsToAdd != null)
            actualRelations.addAll(_relationsToAdd);
        if (_entity.getId() != 0) {
            if (_persistentRelations == null)
                _persistentRelations = getRelationContents();
            actualRelations.addAll(_persistentRelations);
        }
        if (_relationsToRemove != null)
            actualRelations.removeAll(_relationsToRemove);
        return actualRelations;
    }
    
    
    /** Nach dem das Parentobjekt erfolgreich gespeichert wurde, werden hier die Listen angepasst. */
    public void commmit() throws ContactDBException {
        List tmp =new ArrayList();
		if(_persistentRelations != null)
			tmp.addAll(_persistentRelations);
        if(_addedAndCommitted != null){
            tmp.addAll(_addedAndCommitted);
            
        }
        if(_removedAndCommitted != null)
            tmp.removeAll(_removedAndCommitted);
        _persistentRelations = tmp;
        _relationsToAdd = null;
        _relationsToRemove = null;
        
        if(_addedAndCommitted != null && _addedAndCommitted.size() > 0)
            PersistenceManager.fireRelationAdded(new EntityRelationEvent(_entity, _addedAndCommitted));
        if(_removedAndCommitted != null && _removedAndCommitted.size() > 0)
            PersistenceManager.fireRelationRemoved(new EntityRelationEvent(_entity, _removedAndCommitted));
        
        /*
         * Hier m�ssen wir die Mengen der schon hinzugef�gten und schon gel�schten l�schen,
         * sonst wird beim n�chsten Aufruf von commit() (z.b. wenn nach dem hinzuf�gen gel�scht wird),
         * der User wieder gel�scht, da er in _removedAndCommitted enthalten ist,
         * �hnlich bl�des Ph�nomen siehe BUG 1551 
         */
        _addedAndCommitted = null;
        _removedAndCommitted = null;
    }
    
    
    /**	Hier k�nnen dieses Objekt betreffende �nderungen verworfen werden */
    public void rollback() {
        _relationsToAdd = null;
        _relationsToRemove = null;
    }
    
    
    /**	aktuell persistenten Datenbestand der Relation holen */
    abstract public Collection getRelationContents() throws ContactDBException;
    

    /**	Die Benamsung der Parameter f�r die Soapmethode */
    abstract public String[] getFields ();
    
    
	/**	Hier wird die Gegenseite der Relation gesetzt */
    abstract public void relateTo(IEntity entity) throws ContactDBException;
    
	/** Hier wird auch auf der Gegenseite die Relation gel�scht */
	abstract public void killRelation(IEntity entity) throws ContactDBException;
}
