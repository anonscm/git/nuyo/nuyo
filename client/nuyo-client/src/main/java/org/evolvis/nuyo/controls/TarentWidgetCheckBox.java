/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

/**
 * @author niko
 */
public class TarentWidgetCheckBox extends JCheckBox implements TarentWidgetInterface
{

  public TarentWidgetCheckBox(String string, boolean state) 
  {
    super(string, state);
    this.addActionListener(new ValueChangedListener());
  }

  private class ValueChangedListener implements ActionListener
  {
    private Object m_oValue;
    
    public ValueChangedListener()
    {
      m_oValue = getData();
    }
    
    public void actionPerformed(ActionEvent e)
    {
      if (!(getData().equals(m_oValue))) fireValueChanged(getData());
      m_oValue = getData();
    }
  }
  
  public TarentWidgetCheckBox(String string) 
  {
    super(string);
    this.addActionListener(new ValueChangedListener());
  }

  public TarentWidgetCheckBox()
  {
    super();
    this.addActionListener(new ValueChangedListener());
  }
  
  public TarentWidgetCheckBox(boolean state)
  {
    this();
    this.setData(new Boolean(state));
    this.addActionListener(new ValueChangedListener());
  }

  /**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    //this.setSelected(normalize(data));    
    if (this.isSelected() != normalize(data))
    {
      this.doClick();
    }    
	}


  private boolean normalize(Object data)
  {
    if (data instanceof Boolean)
    {
      return(((Boolean)data).booleanValue());
    }
    else if (data instanceof Integer)
    {
      int val = ((Integer)data).intValue();
      
      if (val == 0) return(false);
      else          return(true);
    }
    else if (data instanceof String)
    {
      if (((String)data).equalsIgnoreCase("true"))
      {
        return(true);
      }
      else if (((String)data).equalsIgnoreCase("false"))
      {
        return(false);
      }
      else if (((String)data).equalsIgnoreCase("yes"))
      {
        return(true);
      }
      else if (((String)data).equalsIgnoreCase("no"))
      {
        return(false);
      }
      else if (((String)data).equalsIgnoreCase("on"))
      {
        return(true);
      }
      else if (((String)data).equalsIgnoreCase("off"))
      {
        return(false);
      }      
    }
    return(false);
  }






	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
    return(new Boolean(this.isSelected()));    
	}

  public JComponent getComponent()
  {
    return(this);
  }

  public void setLengthRestriction(int maxlen)
  {
  }
  
  public void setWidgetEditable(boolean iseditable)
  {
    this.setEnabled(iseditable);
  }
  
  public boolean isEqual(Object data)
  {    
    return(this.isSelected() == normalize(data));
  }

  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  private void fireValueChanged(Object newvalue)
  {
    Iterator it = m_oWidgetChangeListeners.iterator();
    while(it.hasNext())
    {
      TarentWidgetChangeListener listener = (TarentWidgetChangeListener)(it.next());
      listener.changedValue(newvalue);
    }
  }
  
  
}
