package org.evolvis.nuyo.db.filter;

import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.IEntity;


/**
 * @author kleinw
 *
 */
abstract public class AbstractIdSelection implements SelectionElement {

    /**	Ein einzelner Wert */
    protected Integer _value;
    
    
    /**	Konstruktor */
    public AbstractIdSelection(Object o) {
        if(o instanceof Integer)
            _value = (Integer)o;
        else if(o instanceof IEntity)
            _value = new Integer(((IEntity)o).getId());
        else if(o instanceof String) 
            _value = new Integer((String)o);
    }
    
    
    /** Hier wird der Wert in den Filter eingetragen */
    public void includeInMethod(Map map) throws ContactDBException {map.put(getFilterKey(), _value);}
    
    
    /**	Wert nach aussen zur Verf�gung stellen */
    public Integer getId() {return _value;}
    
    
    /**	Damit die Selection vom Cache wiedererkannt werden kann */
    public boolean equals(Object arg0) {
        if(arg0 instanceof AbstractIdSelection) {
            AbstractIdSelection selection = (AbstractIdSelection)arg0;
            boolean result = (selection.getId() != null && selection.getId().intValue() == _value.intValue())?true:false;
            result &= arg0.getClass().getName().equals(this.getClass().getName());
            return result;
        } else
            return false;
    }
    

    /**	Stringrepr�sentation des Objekts */
    public String toString() {return new StringBuffer().append("id = ").append(_value).toString();}
    
    
    /** Name des Filterelements */
    abstract public String getFilterKey() throws ContactDBException;
    
    
    /**	Filterelement f�r userid*/
    static public class UserId extends AbstractIdSelection {
        public UserId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "userid";}
    }
    
    /**	Filterelement f�r usergroupid */
    static public class UserGroupId extends AbstractIdSelection {
        public UserGroupId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "usergroupid";}
    }
    
    /**	Filterelement f�r calendarid */
    static public class CalendarId extends AbstractIdSelection {
        public CalendarId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "scheduleid";}
    }
    
    /**	Filterelement f�r eventid */
    static public class EventId extends AbstractIdSelection {
        public EventId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "eventid";}
    }
    
    /**	Filterelement f�r addressid */
    static public class AddressId extends AbstractIdSelection {
        public AddressId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "addressid";}
    }

    /**	Filterelement f�r scheduleid */
    static public class ScheduleId extends AbstractIdSelection {
        public ScheduleId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "scheduleid";}
    }
    
    /**	Filterelement f�r categoryid */
    static public class CategoryId extends AbstractIdSelection {
        public CategoryId(Object o) {super(o);}
        public String getFilterKey() throws ContactDBException {return "categoryid";}
    }
	
	/** Filterelement f�r objectroleid */
	static public class ObjectRoleId extends AbstractIdSelection {
		public ObjectRoleId(Object o){super(o);}
		public String getFilterKey() throws ContactDBException {return "objectroleid";}
	}

}
