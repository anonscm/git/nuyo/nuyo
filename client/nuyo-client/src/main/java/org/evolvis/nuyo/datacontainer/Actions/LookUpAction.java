/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Actions;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAction;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerActionAdapter;
import org.evolvis.nuyo.util.general.ContactLookUpNotFoundException;


/**
 * @author niko
 *
 */
public class LookUpAction extends DataContainerListenerActionAdapter
{
  private String m_sLookupKey = null; 
  
  public DataContainerListenerAction cloneAction()
  {
    LookUpAction action = new LookUpAction();
    
    action.m_oDataContainer = m_oDataContainer;
    action.m_sLookupKey = m_sLookupKey;

    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      action.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }

    return action;
  }

  public boolean doAction()
  {
    DataContainerListener listener = getListener();
    if (listener != null)
    {
      DataContainer tolisten = listener.getDataContainerToListenOn();
      if (tolisten != null)
      {
        Object data = tolisten.getGUIData();      
        Object lookupdata;
        try
        {
          lookupdata = getDataContainer().getDataAccess().getLookupTableValue(m_sLookupKey, data);
          if (lookupdata != null)
          {
            listener.getDataContainer().getGUIElement().setData(lookupdata);
          }
          return true;
        }
        catch (ContactLookUpNotFoundException e)
        {
          getDataContainer().getLogger().severe("unable to locate lookup-table \"" + m_sLookupKey + "\" to process data \"" + data +"\" for action \"" + this.getDataContainerObjectDescriptor().getContainerName() + "\"");
        }
      }
      else
      {
        getDataContainer().getLogger().severe("unable to locate DataContainer \"" + listener.getSlot() + "\" to fetch data for action \"" + this.getDataContainerObjectDescriptor().getContainerName() + "\"");
      }
    }
    return false;
  }

  public void init()
  {
  }

  public void dispose()
  {
  }

  public boolean addParameter(String key, String value)
  {
    if ("lookup".equalsIgnoreCase(key)) 
    {
      try
      {
        m_sLookupKey = value;
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        nfe.printStackTrace();
      }
    }
    return false;
  }

  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.ACTION, "LookUpAction(\"" + m_sLookupKey + "\")", new DataContainerVersion(0));
    
    ParameterDescription lookuppd = new ParameterDescription("lookup", "LookUp Table", "die zur Aufl�sung der Quelldaten benutzte LookupTable.");
    lookuppd.addValueDescription("String", "der Key der Lookup-Tabelle");
    d.addParameterDescription(lookuppd);
    
    return d;
  }
}
