/* $Id: ToggleSideMenuPanelAction.java,v 1.4 2007/09/06 10:03:08 steffi Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.Binding;

/**
 * Showing and hiding the info panel.
 */
public class ToggleSideMenuPanelAction extends AbstractBindingRestrictedAction {
    
	private static final long	serialVersionUID	= 1423558635652739135L;
	
	protected static final Logger logger = Logger.getLogger(ToggleSideMenuPanelAction.class.getName());

    private boolean isVisible;
    private Preferences prefs;
                
	public void actionPerformed( ActionEvent e ) {
        isVisible = !isVisible;
        // Update visibility
        ApplicationServices.getInstance().getMainFrame().setSideMenuVisible(isVisible);
        // Updates all affected checkboxes.
        setSelected(isVisible);
        // Update configuration 
        prefs.put(StartUpConstants.PREF_SIDE_MENU_IS_VISIBLE_KEY, String.valueOf(isVisible));
    }
    
    protected void init() {
        GUIListener guiListener = ApplicationServices.getInstance().getActionManager();        
        prefs = guiListener.getPreferences(StartUpConstants.PREF_PANELS_NODE_NAME);
        // Not using Boolean.parseBoolean() for 1.4-compatibility
        isVisible = Boolean.valueOf(prefs.get(StartUpConstants.PREF_SIDE_MENU_IS_VISIBLE_KEY, "true")).booleanValue();
        // Update visibility
        SwingUtilities.invokeLater( new Runnable(){
            public void run() {
                // Puts checkboxes in the correct state initially.
                setSelected(isVisible);
                ApplicationServices.getInstance().getMainFrame().setSideMenuVisible(isVisible);
            }
        });
    }

    @Override
    protected Binding[] initBindings() {
        //activate synchronization
        setBindingEnabled(true);
        // A binding that listens on manual changes to the side menu visibility
        // in order to synchronize with menu check boxes.
        return new Binding[] { new AbstractReadOnlyBinding(
                ApplicationModel.SIDE_MENU_VISIBILITY_KEY) {

            public void setViewData(Object arg) {
                if (arg == null)
                  return;
                //manual changes occured
                if(isVisible != ((Boolean) arg).booleanValue()) {
                    //synchronize the state
                    isVisible = !isVisible;
                    // Updates all affected checkboxes.
                    setSelected(isVisible);
                    // Update configuration 
                    if(prefs != null)
                    	prefs.put(StartUpConstants.PREF_SIDE_MENU_IS_VISIBLE_KEY, String.valueOf(isVisible));
                    else
                    	logger.warning("Preferences are null!");
                    	
                }
            }

        } };
    }

}
