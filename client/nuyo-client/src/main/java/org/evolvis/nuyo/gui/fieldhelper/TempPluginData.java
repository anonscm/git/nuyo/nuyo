/*
 * Created on 02.07.2004
 *
  */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.HashMap;
import java.util.Map;

import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class TempPluginData implements PluginData
{
  private Map m_oMap;
  
  public TempPluginData()
  {
    m_oMap = new HashMap();
  }
  
  public Object get(Object key)
  {
    return m_oMap.get(key);
  }

  public boolean set(Object key, Object value)
  {
    return (m_oMap.put(key, value) != null);
  }

  public Class getDatatype(Object key)
  {
    Object value = get(key);
    if (value != null) return value.getClass();
    else return null;
  }  


}
