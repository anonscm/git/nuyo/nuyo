/* $Id: ToggleToolBarAction.java,v 1.5 2007/08/30 16:10:20 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;
import java.util.prefs.Preferences;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Showing and hiding the tool bar.
 */
public class ToggleToolBarAction extends AbstractGUIAction {
    
	private boolean isVisible;
    private Preferences prefs;
                
    
	public void actionPerformed( ActionEvent e ) {
	    isVisible = !isVisible;
        // Update visibility
        ApplicationServices.getInstance().getMainFrame().setToolBarVisible(isVisible);
        // Updates all affected checkboxes.
        setSelected(isVisible);
        // Update configuration 
        prefs.put(StartUpConstants.PREF_TOOL_BAR_IS_VISIBLE_KEY, String.valueOf(isVisible));
    }

    protected void init() {
        
        GUIListener guiListener = ApplicationServices.getInstance().getActionManager();        
        prefs = guiListener.getPreferences(StartUpConstants.PREF_PANELS_NODE_NAME);
        
        // Not using Boolean.parseBoolean() for 1.4-compatibility
        isVisible = Boolean.valueOf(prefs.get(StartUpConstants.PREF_TOOL_BAR_IS_VISIBLE_KEY, "true")).booleanValue();
        // Puts checkboxes in the correct state initially.
        setSelected(isVisible);
    }

}
