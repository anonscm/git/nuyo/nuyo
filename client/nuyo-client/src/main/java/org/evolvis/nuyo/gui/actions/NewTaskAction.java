package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;

public class NewTaskAction extends AbstractGUIAction {

    private static final long serialVersionUID = -356531550749529989L;
    private static final TarentLogger logger = new TarentLogger(NewTaskAction.class);
    private ControlListener mainFrame;
    private TarentCalendar calendar;
    private Runnable executor;

    public void actionPerformed(ActionEvent e) {
        if(calendar != null){
            SwingUtilities.invokeLater(executor);
        } else logger.warning(getClass().getName()+" is not initialized");
    }

    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();

        Plugin calendarPlugin = PluginRegistry.getInstance().getPlugin(CalendarPlugin.ID);
        if(calendarPlugin != null) {
           if(calendarPlugin.isTypeSupported(TarentCalendar.class)){
               calendar = (TarentCalendar) calendarPlugin.getImplementationFor(TarentCalendar.class);               
           } else logger.warning("Couldn't init " + getClass().getName(), "Tarent Calendar interface is not supported by Calender Plugin.");
        } else logger.warning("Couldn't init " + getClass().getName(), "Calender Plugin not registered.");

        executor = new Runnable(){
            public void run() {
                mainFrame.activateTab(MainFrameExtStyle.TAB_ORGANIZER);
                calendar.createNewTask();
            }
        };
    }
}