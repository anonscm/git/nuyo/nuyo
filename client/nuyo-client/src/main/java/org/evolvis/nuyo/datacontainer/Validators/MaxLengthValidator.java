/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Validators;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.PositionErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.ValidatorAdapter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MaxLengthValidator extends ValidatorAdapter
{
  private int m_iMaxLength;
   
  public String getListenerName()
  {
    return "MaxLengthValidator";
  }  
    
  public MaxLengthValidator()
  {
    super();
    m_iMaxLength = -1;
  }
   
  public Validator cloneValidator()
  {
    MaxLengthValidator validator = new MaxLengthValidator();
    validator.m_iMaxLength = m_iMaxLength;
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      validator.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    return validator;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.VALIDATOR, "MaxLengthValidator", new DataContainerVersion(0));
    d.addParameterDescription(new ParameterDescription("maxlen", "maximale L�nge", "die maximal erlaubte L�nge der Zeichenkette."));
    return d;
  }
  

  
  public boolean addParameter(String key, String value)
  {    
    if ("maxlen".equalsIgnoreCase(key)) 
    {
      try
      {
        m_iMaxLength = Integer.parseInt(value);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) {}
    }
    return false;
  }
  
  public boolean canValidate(Class data)
  {
    return data.isInstance(String.class);
  }

  public boolean validate(Object data)
  {    
    if (data instanceof String)
    {
      if (m_iMaxLength != -1)
      {
        if (((String)data).length() > m_iMaxLength)
        {
          PositionErrorHint peh = new PositionErrorHint("Zeichenkette zu lang", "es sind nur " + m_iMaxLength + " Zeichen erlaubt.", null, getDataContainer().getGUIElement(), getDataContainer(), m_iMaxLength); 
          fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), peh));                    
          return false;
        }
      }
    }
    return true;
  }

}
