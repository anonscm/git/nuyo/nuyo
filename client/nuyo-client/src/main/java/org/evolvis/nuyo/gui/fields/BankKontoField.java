/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class BankKontoField extends GenericTextField
{
  public BankKontoField()
  {
    super("BANKKONTO", AddressKeys.BANKKONTO, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_BankKonto_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_BankKonto", 60);
  }
}
