/*
 * Created on 14.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorHint;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ErrorHintCollector extends ErrorHintAdapter
{
  private List m_oErrorHints;
  
  public ErrorHintCollector(String name, String description, Exception e, GUIElement guielement, DataContainer datacontainer)
  {
    super(name, description, e, guielement, datacontainer);
    m_oErrorHints = new ArrayList();
  }

  public void addErrorHint(ErrorHint eh)
  {
    if (m_oErrorHints.size() > 0)
    {
      ErrorHint oldeh = (ErrorHint)(m_oErrorHints.get(0));
      if (!(oldeh.getDataContainer().equals(eh.getDataContainer()))) return;
    }
    m_oErrorHints.add(eh);
  }

  public void removeErrorHint(ErrorHint eh)
  {
    m_oErrorHints.remove(eh);
  }

  public String getName()
  {
    String name = "";
    for(int i=0; i<(m_oErrorHints.size()); i++)
    {
      ErrorHint eh = (ErrorHint)(m_oErrorHints.get(i));
      name += (eh.getName() + "; ");
    }
    return name;
  }

  public String getDescription()
  {
    String desc = "";
    for(int i=0; i<(m_oErrorHints.size()); i++)
    {
      ErrorHint eh = (ErrorHint)(m_oErrorHints.get(i));
      desc += (eh.getDescription() + "; ");
    }
    return desc;
  }

  public DataContainer getDataContainer()
  {
    if (m_oErrorHints.size() > 0)
    {
      ErrorHint oldeh = (ErrorHint)(m_oErrorHints.get(0));
      return oldeh.getDataContainer();
    }
    return(null);
  }


}
