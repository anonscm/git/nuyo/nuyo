/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Validators;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.ErrorHint.GenericErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.ValidatorAdapter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class EMailValidator extends ValidatorAdapter
{
  public String getListenerName()
  {
    return "EMailValidator";
  }  
  

  public EMailValidator()
  {
    super();
  }
    
  public Validator cloneValidator()
  {
    EMailValidator validator = new EMailValidator();
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      validator.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    return validator;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.VALIDATOR, "EMailValidator", new DataContainerVersion(0));
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    //m_oParameterList.add(new ObjectParameter(key, value));

    return false;
  }

  public boolean canValidate(Class data)
  {
    return data.isInstance(String.class);
  }

  public boolean validate(Object data)
  {
    boolean valid = true;
    
    if (data instanceof String)
    {
      String text = (String)data;
      if (text.length() <= 1)
      {        
        GenericErrorHint eh = new GenericErrorHint("Zeichenkette zu kurz", "die Zeichenkette ist zu kurz um eine g�ltige eMail Adresse zu sein", null, getDataContainer().getGUIElement(), getDataContainer()); 
        fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), eh));
        valid = false;                    
      }
      
      if (text.indexOf('@') == -1)
      {
        GenericErrorHint eh = new GenericErrorHint("Fehlendes Zeichen", "die Zeichenkette enth�lt kein '@' und kann daher keine g�ltige eMail Adresse sein", null, getDataContainer().getGUIElement(), getDataContainer()); 
        fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), eh));                    
        valid = false;                    
      }

      return(valid);
    }
    return false;
  }

}
