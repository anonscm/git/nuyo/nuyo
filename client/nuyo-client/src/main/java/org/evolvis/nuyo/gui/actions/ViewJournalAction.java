

package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.action.journal.JournalViewer;
import org.evolvis.nuyo.logging.TarentLogger;


/**
 * An action which causes the display of the journal viewer.
 *
 */
public class ViewJournalAction extends AbstractContactDependentAction
{

  private static final TarentLogger logger = new TarentLogger(
                                                              ViewJournalAction.class);

  private JournalViewer journalViewer;

  public void actionPerformed(ActionEvent e)
  {
    if (journalViewer != null)
      journalViewer.show();
    else
      logger.warning("Action '" + getUniqueName() + "' is not initialized.");
  }

  public void init()
  {
    ApplicationServices as = ApplicationServices.getInstance();

    journalViewer = new JournalViewer(as.getActionManager().getFrame());
  }
  
}
