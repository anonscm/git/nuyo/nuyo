/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class AddAttachmentAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7095497308906531889L;

	public void actionPerformed(ActionEvent pEvent)
	{
		DispatchEMailDialog.getInstance().appendAttachment();
	}
}
