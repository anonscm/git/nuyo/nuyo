/*
 * Created on 05.05.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author niko
 */
public class ExtendedMonatskalenderDayButton extends JButton implements MonatskalenderDayButtonInterface
{
  private List m_oJobs = new ArrayList();
  private List m_oSheds = new ArrayList();
  private Color m_oSchedulePointColor = Color.GREEN;
  private Color m_oToDoPointColor = Color.BLUE;
  private Appointment m_oAppointment;
  
  public ExtendedMonatskalenderDayButton(String text)
  {
    super(text);
  }
  
  public ExtendedMonatskalenderDayButton()
  {
    super();
  }
  
  public void setShedulePointColor(Color color)
  {
    m_oSchedulePointColor = color;
  }

  public void setToDoPointColor(Color color)
  {
    m_oToDoPointColor = color;
  }
  
  private int m_iActiveAppointmentID = -1;
  public void setActiveAppointment(Appointment appointment)
  {
    m_oAppointment = appointment;
    if (m_oAppointment != null)
    {
      m_iActiveAppointmentID = m_oAppointment.getId(); 
    }
    else m_iActiveAppointmentID = -1;
  }


  public boolean containsAppointment(Appointment appointment)
  {
    if (appointment == null) return false;
    return containsAppointment(appointment.getId());
  }

  private boolean containsAppointment(int appointmentid)
  {
    if (appointmentid == -1) return false;
    Iterator it = m_oJobs.iterator();
    while(it.hasNext())
    {
      Appointment ap = (Appointment)(it.next());
      if (appointmentid == ap.getId()) return true;
    }

    it = m_oSheds.iterator();
    while(it.hasNext())
    {
      Appointment ap = (Appointment)(it.next());
      if (appointmentid == ap.getId()) return true;
    }
    
    return false;
  }

  public void addAppointment(Appointment appointment)
  {
    try
    {
      if (appointment.isJob()) m_oJobs.add(appointment);
      else m_oSheds.add(appointment);
    } 
    catch (ContactDBException e) {}
  }
  
  public void removeAppointment(Appointment appointment)
  {
    try
    {
      if (appointment.isJob()) m_oJobs.remove(appointment);
      else m_oSheds.remove(appointment);
    } 
    catch (ContactDBException e) {}
  }
  
  public void clearAppointments()
  {
    m_oJobs.clear();
    m_oSheds.clear();
  }
  
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {        
    int maxshedwidth = 12;
    
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();

    // Hintergrund f�llen...
    g.setColor(this.getBackground());
    g.fillRect(0, 0, size.width, size.height);    
    
    // Button-Text BoundingBox ermitteln...
    Rectangle2D rect = g.getFontMetrics().getStringBounds(this.getText(), g);
    int posy = ((int)(rect.getHeight())) + 3;// + (size.height / 6);
    int posx = (int) ((size.width - rect.getWidth()) / 2.0);	    

    // Button Text zeichnen...
    g.setColor(this.getForeground());    
    g.setFont(getFont().deriveFont(Font.BOLD));
    g.drawString(this.getText(), posx, posy);
    
    // wenn Termine existieren ein K�stchen malen...
    if (m_oSheds.size() > 0)
    {
      g.setColor(m_oSchedulePointColor);
      g.fillRect(4, posy + 4, (size.width / 2) - 7, (size.height) - 4 - posy - insets.bottom);        

//      if (containsAppointment(m_iActiveAppointmentID))
//      {
//        g.setColor(Color.RED);
//        g.drawRect(4, posy + 4, (size.width / 2) - 7, (size.height) - 4 - posy - insets.bottom);
//      }

      g.setColor(Color.BLACK);    
      g.setFont(getFont());
      String shedtext = String.valueOf(m_oSheds.size());
      Rectangle2D shedtextrect = g.getFontMetrics().getStringBounds(shedtext, g);
      if ( ((size.height) - 4 - insets.bottom) - ((int)(shedtextrect.getHeight())) > posy + 4)
      {
        g.drawString(shedtext, 4 + ((int)(shedtextrect.getWidth() / 2)), (size.height) - 4 - insets.bottom);
      }
    }
    
    // wenn Aufgaben existieren ein K�stchen malen...
    if (m_oJobs.size() > 0)
    {
      g.setColor(m_oToDoPointColor);
      g.fillRect((size.width / 2) + 3, posy + 4, (size.width / 2) - 7, (size.height) - 4 - posy - insets.bottom);        

//      if (containsAppointment(m_iActiveAppointmentID))
//      {
//        g.setColor(Color.RED);
//        g.drawRect((size.width / 2) + 3, posy + 4, (size.width / 2) - 7, (size.height) - 4 - posy - insets.bottom);        
//      }

      g.setColor(Color.BLACK);    
      g.setFont(getFont());
      String jobtext = String.valueOf(m_oJobs.size());
      Rectangle2D jobtextrect = g.getFontMetrics().getStringBounds(jobtext, g);
      if ( ((size.height) - 4 - insets.bottom) - ((int)(jobtextrect.getHeight())) > posy + 4)
      {
        g.drawString(jobtext, (size.width) - 3 - ((int)(jobtextrect.getWidth() * 1.5)), (size.height) - 4 - insets.bottom);
      }
    }
  }  
  
}
