/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import javax.swing.JComponent;

/**
 * @author niko
 *
 */
public class ExtendedDataLabeledComponent 
{
  private String     m_sLabelText;
  private String     m_sToolTip;
  private JComponent m_oComponent;
  
  public ExtendedDataLabeledComponent()
  {
    m_sLabelText = "";
    m_sToolTip = "";
    m_oComponent = null;    
  }

  public ExtendedDataLabeledComponent(String labeltext, String tooltip, JComponent component)
  {
    m_sLabelText = labeltext;
    m_sToolTip = tooltip;
    m_oComponent = component;    
  }

  public void setLabelText(String labeltext)
  {
    m_sLabelText = labeltext;
  }

  public void setToolTip(String tooltip)
  {
    m_sToolTip = tooltip; 
  }

  public void setComponent(JComponent component)
  {
    m_oComponent = component;    
  }
  
  public String getLabelText()
  {
    return(m_sLabelText);
  }

  public String getToolTip()
  {
    return(m_sToolTip);
  }

  public JComponent getComponent()
  {
    return(m_oComponent);
  }
    
}
