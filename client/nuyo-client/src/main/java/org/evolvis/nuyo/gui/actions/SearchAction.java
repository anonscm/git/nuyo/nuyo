package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.search.TarentSearch;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;

public class SearchAction extends AbstractGUIAction {

    private static final long serialVersionUID = -6272492694906577662L;
    private static final TarentLogger logger = new TarentLogger(SearchAction.class);
    private TarentSearch searchEngine;
    
    public void actionPerformed(ActionEvent e) {
        if(searchEngine != null) {
            if (searchEngine.canSearch()) {
                searchEngine.showSearch();
                if (searchEngine.criteriaChanged()) {
                    searchEngine.updateAddressListParameters();
                    ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
                }
            }
        } 
        else logger.warning(Messages.getFormattedString("ActionRegistry_ActionNotInitialized", Messages.getString("Action_SearchAction")));
    }

    public void init(){
        Plugin searchPlugin = PluginRegistry.getInstance().getDefaultPluginFor(TarentSearch.class);
        if(searchPlugin != null)
            searchEngine = (TarentSearch) searchPlugin.getImplementationFor(TarentSearch.class);
        else 
            logger.warning("Couldn't init Search Action", "No Search Plugin available.");

        // Puts the alp into the search engine to let it work on it
        // (which means it will update it from time to time).
        AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
        if (alp == null) 
            logger.warning("No AddressListParameter object in application model.");
        searchEngine.init(alp);
    }
}