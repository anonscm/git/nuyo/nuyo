package org.evolvis.nuyo.db.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.cache.Cachable;



/**
 * @author kleinw
 *
 *	Diese Klasse kapselt gemeinsame Funktionalit�ten 
 *	rundum die M�glichen Operationen �ber ein PersistentEntityobjekt
 *	und verwaltet diese.
 *
 *	Hier werden auf die EntityEvents ausgel�st und an die Listener gesendet.
 *
 */
public class PersistenceManager {

    /**	Listener f�r generelle �nderungen */
    static private Collection _entityListener = new ArrayList();
    /** Listener f�r �nderungen bzgl. von Relationen */
    static private Collection _entityRelationListener = new ArrayList();
    
    private static Logger logger = Logger.getLogger(PersistenceManager.class.getName());
    
    /**	Hier kann sich eine Entity anmelden.@param listener - der Listener */
    static public void addEntityListener(EntityListener listener) {_entityListener.add(listener);}
    
    
    /**	Hier kann sich eine Entity anmelden.@param listener - der Listener */
    static public void addEntityRelationListener(EntityRelationListener listener) {_entityRelationListener.add(listener);}
    
    
    /**	Eine Speicherung des Objekts wird vorgenommmen.@param entity - die zu speichernde Entit�t */
    static public void commit(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) {
        	//System.out.println("commit(IEntity)");
	        IEntity entityToCommit = (IEntity)entity;
	        entityToCommit.validate();
	        if(entityToCommit.isTransient()) {
	        	entityToCommit.setCreated(new Date());
                entityToCommit.commit();
                if(entityToCommit.getId() != 0 || entity instanceof VirtualEntity) {
                    entityToCommit.setTransient(false);
                    entityToCommit.setDirty(false);
                    if(entity instanceof Cachable) {
                        ((Cachable)entity).updateInCache();
                    }
                    fireCommitEvent(new EntityEvent((IEntity)entity));
                } else {
                    entityToCommit.setInvalid(true);
                    throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "Fehler beim Speichern des Objekts (" + entity.getClass() + ")");
                }
            } else if(entityToCommit.isDirty()) {
            	entityToCommit.setChanged(new Date());
                entityToCommit.commit();
                if(entityToCommit.getId() != 0) {
                    entityToCommit.setTransient(false);
                    entityToCommit.setDirty(false);
                    //Wenn das zu Problemen f�hrt, �berlegen wie man sonst sicherstellen kann, das Cache aktuell ist...
                    if(entity instanceof Cachable) {
                        ((Cachable)entity).updateInCache();
                    }
                } else {
                    entityToCommit.setInvalid(true);
                    throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "Fehler beim Speichern des Objekts (" + entity.getClass() + ")");
                }
            } else{
            	logger.log(Level.INFO, "F�r ein Object [" + entity.getClass() + "] wurde ein COMMIT angefordert - Das Object hat sich aber nicht ge�ndert!");
            }
        } else {
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
        }
    }
    
    
    /**	Eine Wiederherstellung des Objekts wird vorgenommmen.@param entity - die wiederherzustellende Entit�t */
    static public void rollback(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) {
            IEntity entityToRollback = (IEntity)entity;
            if(entityToRollback.isDirty() && !entityToRollback.isTransient()) {
                entityToRollback.rollback();
                entityToRollback.setDirty(false);
            } else 
                throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "Die Funktion 'rollback' geht nur bei Objekten mit Status 'isDirty' und '!isTransient'.");
        } else {
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
        }
    }
    

    /**	Eine L�schung des Objekts wird vorgenommmen.@param entity - die zu l�schende Entit�t */
    static public void delete(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) {
            IEntity entityToDelete = (IEntity)entity;
            if(!entityToDelete.isTransient()) {
                entityToDelete.delete();
                entityToDelete.setDeleted(true);
                if(entity instanceof Cachable)
                    ((Cachable)entity).deleteFromCache();
            } else
                throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "Die Funktion 'delete' geht nur bei Objekten mit Status '!isTransient'.");
        } else
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
    }
    
    
    /**	Test, ob Objekt transient ist */
    static public boolean isTransient(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) return ((IEntity)entity).isTransient();
        else 
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
    }

    
    /**	Test, ob Objekt dirty ist */
    static public boolean isDirty(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) return ((IEntity)entity).isDirty();
        else 
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
    }
    
    
    /**	Test, ob Objekt gel�scht wurde */
    static public boolean isDeleted(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) return ((IEntity)entity).isDeleted();
        else 
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
    }
    
    
    /**	Test, ob Objekt ung�ltig ist */
    static public boolean isInvalid(Object entity) throws ContactDBException {
        if(entity instanceof IEntity) return ((IEntity)entity).isInvalid();
        else 
            throw new ContactDBException(ContactDBException.EX_OBJECT_MODEL_ERROR, "In den Persistenzmanager d�rfen nur Instanzen des Typs 'IEntity' eingesetzt werden.");
    }
    
    
    /**	Ein Commitevent feuern.@param entity - die betroffene Entit�t */
    static public void fireCommitEvent(EntityEvent event) throws ContactDBException {
        for(Iterator it = _entityListener.iterator();it.hasNext();) {
            EntityListener listener = (EntityListener)it.next();
            listener.entityCommitted(event);
        }
    }
    

    /**	Ein Deleteevent feuern.@param entity - die betroffene Entit�t */
    static public void fireDeletedEvent(EntityEvent event) throws ContactDBException {
        for(Iterator it = _entityListener.iterator();it.hasNext();) {
            EntityListener listener = (EntityListener)it.next();
            listener.entityCommitted(event);
        }
    }


    /**	Ein Requestevent feuern.@param entity - die betroffene Entit�t */
    static public void fireRefreshEvent(EntityEvent event) throws ContactDBException {
        for(Iterator it = _entityListener.iterator();it.hasNext();) {
            EntityListener listener = (EntityListener)it.next();
            listener.entityCommitted(event);
        }
    }

    
    /**	Ein Rollbackevent feuern.@param entity - die betroffene Entit�t */
    static public void fireRollbackEvent(EntityEvent event) throws ContactDBException {
        for(Iterator it = _entityListener.iterator();it.hasNext();) {
            EntityListener listener = (EntityListener)it.next();
            listener.entityCommitted(event);
        }
    }
    
    
    /** Eine Relation wurde hinzugef�gt. */
    static public void fireRelationAdded(EntityRelationEvent event) throws ContactDBException {
        for(Iterator it = _entityRelationListener.iterator();it.hasNext();) {
            EntityRelationListener listener = (EntityRelationListener)it.next();
            listener.relationAdded(event);
        }
    }

    
    /**	Eine Relation wurde gel�scht */
    static public void fireRelationRemoved(EntityRelationEvent event) throws ContactDBException {
        for(Iterator it = _entityRelationListener.iterator();it.hasNext();) {
            EntityRelationListener listener = (EntityRelationListener)it.next();
            listener.relationRemoved(event);
        }
    } 
    
}
