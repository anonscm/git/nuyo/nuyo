/*
 * Created on 07.03.2005
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author simon
 * 
 * !STATIC SINGLETON!
 * 
 *  Zentrale Verwaltung : 
 *  SchedulePanels, Appointments und ScheduleEntryPanels
 * 	(und: Ja, das redundante Speichern ist Absicht ...)
 * 
 * - Zuordnung Appointment -> ScheduleEntryPanels (Reihenfolge beachten!) 
 * - Zuordnung SchedulePanel -> SEP's (Sammlung aller angezeigten SEP's)
 * 
 * 
 */


public class SEPRegistry {
	
	private static Logger logger = Logger.getLogger(SEPRegistry.class.getName());
    
    public static int SEPPOSITION_FIRST = 0;
    public static int SEPPOSITION_LAST = 1;
    
    private static SEPRegistry _theInstance;
    private Vector _usedSEPIDs;
    private Vector _registeredSchedulePanels;
    private TreeMap _TM_AppointmentIDToSEPS; //AppointmentID -> Vector(SEP'S)
    private TreeMap _TM_SchedulePanelIDToSEPS; //SchedulePanelID -> Vector (SEP's)
        
    private SEPRegistry (){
        clear();
    }
    
    public static SEPRegistry getInstance(){
        if (_theInstance == null){
            _theInstance = new SEPRegistry();
        }
        return _theInstance;
    }
    
    public void registerSchedulePanel(int UniqueID) throws Exception{
        
        if ( _registeredSchedulePanels.contains(new Integer(UniqueID)))
            throw new Exception("SchedulePanel needs a UNIQUE ID, Failed to add [" + UniqueID +  "] to Registry!");
        
        _registeredSchedulePanels.add(new Integer(UniqueID));
    }
    
    public void addSEP(ScheduleEntryPanel SEPanel,int SchedulePanelID, int SEPPOSITION) throws Exception{
        
        if (! _registeredSchedulePanels.contains(new Integer(SchedulePanelID))) 
            throw new Exception("SchedulePanel with UNIQUE ID [" + SchedulePanelID + "] is not registered!");
        
        Vector SEPS = (Vector) _TM_SchedulePanelIDToSEPS.get(new Integer(SchedulePanelID));
        if (SEPS != null && SEPS.contains(SEPanel)){
            //throw new Exception("ScheduleEntryPanel with ID [" + SEPanel.getID() + "] is already registered! Please Optimize!");
            return;
        }
            
        //Keep ID's low
        int _currentID = 0 ;
        while (_usedSEPIDs.contains(new Integer(_currentID))) _currentID++;
        
        //Set ID for SEP
        SEPanel.setID(new Integer(_currentID));
        _usedSEPIDs.add(new Integer(_currentID));
        
        //Update SchedulePanel -> SEP's TreeMap
        if (SEPS == null){
            //First SEP to add
            Vector tmpVector = new Vector();
            	tmpVector.add(SEPanel);
            	_TM_SchedulePanelIDToSEPS.put(new Integer(SchedulePanelID),tmpVector);
        }else{
            //Update
            SEPS.add(SEPanel);
            //System.out.println("addSEP: SEPanel [" + SEPanel.getID() + "] zu Schedulepanel [" + SchedulePanelID +"]");
        }
        
        //Update Appointment-> SEP's TreeMap
        Integer AppointmentID = new Integer(SEPanel.getAppointment().getId()); 
        Vector SEPsforAppointment = (Vector) _TM_AppointmentIDToSEPS.get(AppointmentID);
        if (SEPsforAppointment == null){
            //New Appointment
            Vector tmpVector = new Vector();
            	tmpVector.add(SEPanel);
            _TM_AppointmentIDToSEPS.put(AppointmentID,tmpVector);
        }else{
            //Update - honor SEPPosition
            if (SEPPOSITION == SEPPOSITION_FIRST){
                SEPsforAppointment.add(0,SEPanel);
            }else if(SEPPOSITION == SEPPOSITION_LAST){
                SEPsforAppointment.add(SEPanel);
            }else{
                logger.log(Level.WARNING, "addSEP: called with incorrect SEPPOSITION Argument!!!");
            }
        }
        
/*        //Debug
        Vector  av = (Vector) _TM_AppointmentIDToSEPS.get(AppointmentID);
        System.out.print("addedSEP: contains now ID's{");
        Iterator iter = av.iterator();
        while (iter.hasNext()){
            ScheduleEntryPanel sebb = (ScheduleEntryPanel) iter.next();
            System.out.print(sebb.getID() + ",");
        }
        System.out.println("}");*/
    }
    
    public List getSEPSforSchedulePaneliD(int SchedulePanelID){
        return (Vector) _TM_SchedulePanelIDToSEPS.get(new Integer(SchedulePanelID));
    }

    public List getSEPSforAppointmentID(int AppointmentID){
        return (Vector)_TM_AppointmentIDToSEPS.get(new Integer(AppointmentID));
    }
    
    public void removeSEP (ScheduleEntryPanel SEPanel,int SchedulePanelID){
        //System.out.println("removeSEP: Schedulepanel [" + SchedulePanelID +"]");
        Vector tmpVector = (Vector) _TM_SchedulePanelIDToSEPS.get(new Integer(SchedulePanelID));
        tmpVector.remove(SEPanel);
        // Remove also SEP from Appointment-> SEP's TreeMap
        Vector SEPsforAppointment = (Vector) _TM_AppointmentIDToSEPS.get(new Integer(SEPanel.getAppointment().getId()));
        SEPsforAppointment.remove(SEPanel);
    }

    public void removeAllSEPs (int SchedulePanelID){
        
        //System.out.println("removeAllSEPs: Schedulepanel [" + SchedulePanelID +"]");
        Vector tmpVector = (Vector) _TM_SchedulePanelIDToSEPS.get(new Integer(SchedulePanelID));
        Iterator iter = tmpVector.iterator();
        while (iter.hasNext()){
            // Remove also SEPS from Appointment-> SEP's TreeMap
            ScheduleEntryPanel _SEP = (ScheduleEntryPanel) iter.next();
            _TM_AppointmentIDToSEPS.remove(new Integer(_SEP.getAppointment().getId()));
            _usedSEPIDs.remove(_SEP.getID());
        }
        tmpVector.clear();
    }
    
    public int getSEPCount(int AppointmentID){
        Vector tmpVector = (Vector)_TM_AppointmentIDToSEPS.get(new Integer(AppointmentID));
        if (tmpVector != null){
            return tmpVector.size();
        }else{
            return 0;
        } 
    }
    
    public ScheduleEntryPanel getFirstSEP(int AppointmentID){
        if (getSEPCount(AppointmentID) > 0){
            Vector tmpVector = (Vector)_TM_AppointmentIDToSEPS.get(new Integer(AppointmentID));
            return (ScheduleEntryPanel)tmpVector.firstElement();
        }else{
            return null;
        }
    }

    public ScheduleEntryPanel getLastSEP(int AppointmentID){
        if (getSEPCount(AppointmentID) > 0){
            Vector tmpVector = (Vector)_TM_AppointmentIDToSEPS.get(new Integer(AppointmentID));
            return (ScheduleEntryPanel)tmpVector.lastElement();
        }else{
            return null;
        }
    }

    private void clear(){
        _usedSEPIDs = new Vector();
        _TM_AppointmentIDToSEPS = new TreeMap();
        _TM_SchedulePanelIDToSEPS = new TreeMap();
        _registeredSchedulePanels = new Vector();
    }

}
