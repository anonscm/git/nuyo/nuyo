/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author niko
 *
 */
public class WorkDayDescription extends DayDescription
{
  private static Logger logger = Logger.getLogger(WorkDayDescription.class.getName());
  
  private int m_iStartWorkSecond;
  private int m_iEndWorkSecond;

  public WorkDayDescription()
  {
    super(DayDescription.WORKDAY);
  }
  
  public int getEndWorkSecond()
  {
    return m_iEndWorkSecond;
  }

  public void setEndWorkSecond(int endWorkSecond)
  {
    m_iEndWorkSecond = endWorkSecond;
  }

  public int getStartWorkSecond()
  {
    return m_iStartWorkSecond;
  }

  public void setStartWorkSecond(int startWorkSecond)
  {
    m_iStartWorkSecond = startWorkSecond;
  }

  public void dump(String text)
  {
  	logger.log(Level.FINE, text + ": " + getType().toString() + "; " + getDayName() + "; " + getDescription() + "; " + getStartWorkSecond() + "; " + getEndWorkSecond());    
  }
  
}
