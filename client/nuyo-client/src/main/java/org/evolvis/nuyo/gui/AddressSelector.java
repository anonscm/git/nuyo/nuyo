/*
 * Created on 07.05.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;

import de.tarent.commons.ui.EscapeDialog;

/**
 * @author niko
 *
 */
public class AddressSelector extends EscapeDialog
{
  public static final Object STATUS_NONE = "STATUS_NONE";
  public static final Object STATUS_OK = "STATUS_OK";
  public static final Object STATUS_CANCEL = "STATUS_CANCEL";
  
  private GUIListener m_oGUIListener;
  private JTable m_oTable_Addresses;
  private AddressTableModel m_oTableModel;
  private Object m_oStatus = STATUS_NONE;
  
  public AddressSelector(GUIListener guiListener, String caption)
  {
    super(guiListener.getFrame());
    setTitle(caption);
    m_oGUIListener = guiListener;    
    
    JPanel panel = new JPanel(new BorderLayout()); 
    
    // tabelle erzeugen...
    m_oTableModel = new AddressTableModel(2);    

    m_oTableModel.setColumnName(0, "Selektion");    
    m_oTableModel.setColumnEditable(0, true);
    
    m_oTableModel.setColumnName(1, "Adresse");    
    m_oTableModel.setColumnEditable(1, false);
    
    m_oTable_Addresses = new JTable(m_oTableModel);    
    m_oTable_Addresses.setRowHeight(20);
    m_oTable_Addresses.setCellSelectionEnabled(false);
    m_oTable_Addresses.setRowSelectionAllowed(true);    
    m_oTable_Addresses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    TableColumn column0 = m_oTable_Addresses.getColumnModel().getColumn(0);
    column0.setPreferredWidth(40);
    column0.setMaxWidth(40);
    column0.setMinWidth(40);
    
    TableColumn column1 = m_oTable_Addresses.getColumnModel().getColumn(1);
    column1.setPreferredWidth(300);

    // ------------------------------------------------------------------------------------------------------
    
    JButton button_selectall = new JButton("alles selektieren");
    button_selectall.addActionListener(new button_selectall_clicked());
    
    JButton button_selectnone = new JButton("alles deselektieren");
    button_selectnone.addActionListener(new button_selectnone_clicked());
    
    JButton button_selectinvert = new JButton("Auswahl umkehren");
    button_selectinvert.addActionListener(new button_selectinvert_clicked());
    
    JPanel selectbuttonpanel = new JPanel(new GridLayout(1,0, 5,0));
    selectbuttonpanel.add(button_selectall);
    selectbuttonpanel.add(button_selectnone);
    selectbuttonpanel.add(button_selectinvert);

    // ------------------------------------------------------------------------------------------------------
    
    JButton button_ok = new JButton("Ok");
    button_ok.addActionListener(new button_ok_clicked());
    
    JButton button_cancel = new JButton("Abbruch"); 
    button_cancel.addActionListener(new button_cancel_clicked());
    
    JPanel buttonpanel = new JPanel(new BorderLayout());
    buttonpanel.add(button_ok, BorderLayout.WEST);
    buttonpanel.add(button_cancel, BorderLayout.EAST);

    JPanel controlpanel = new JPanel(new BorderLayout());
    controlpanel.add(selectbuttonpanel, BorderLayout.NORTH);
    controlpanel.add(buttonpanel, BorderLayout.SOUTH);
    
    JScrollPane scroll = new JScrollPane(m_oTable_Addresses);
    scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    
    panel.add(scroll, BorderLayout.CENTER);
    panel.add(controlpanel, BorderLayout.SOUTH);
    
    // ------------------------------------------------------------------------------------------------------
    
    getContentPane().add(panel);
    fillAddressList();    
    pack();
    
    JFrame frame = guiListener.getFrame();
    if (frame.isShowing() && frame.isValid())
    {
      setLocationRelativeTo(frame);
    }
    else
    {
      setLocationRelativeTo(null);
    }
    
    setModal(true);
    setVisible(true);    
  }
  
  
  private void fillAddressList()
  {
    Addresses addresses = m_oGUIListener.getAddresses();
    for(int i=0; i<(addresses.getIndexMax()); i++)
    {
      Address address = addresses.get(i);
      insertAddress(address, false);
    }
  }

  
  private class button_selectall_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      for(int i=0; i<(m_oTableModel.getRowCount()); i++)
      {
        m_oTableModel.setValueAt(new Boolean(true), i, 0);
      }      
    }
  }
  
  private class button_selectnone_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      for(int i=0; i<(m_oTableModel.getRowCount()); i++)
      {
        m_oTableModel.setValueAt(new Boolean(false), i, 0);
      }      
    }
  }
  
  private class button_selectinvert_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      for(int i=0; i<(m_oTableModel.getRowCount()); i++)
      {
        Boolean value = (Boolean)(m_oTableModel.getValueAt(i, 0));
        m_oTableModel.setValueAt(new Boolean(!(value.booleanValue())), i, 0);
      }      
    }
  }
  
  
  private class button_ok_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      m_oStatus = STATUS_OK;
      closeWindow();
    }
  }
  
  private class button_cancel_clicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      m_oStatus = STATUS_CANCEL;
      closeWindow();
    }
  }
  
  
  public void insertAddress(Address address, boolean isselected)
  {
    Object[] line = new Object[2];
    line[0] = new Boolean(isselected);
    line[1] = address.getShortAddressLabel();
    m_oTableModel.addRow(line, address);
  }
  
  public Object getReturnStatus()
  {
    return m_oStatus;
  }
  
  public List getSelectedAddresses()
  {
    if (STATUS_OK.equals(m_oStatus))
    {  
	    List list = new ArrayList();
	    
	    for(int i=0; i<(m_oTableModel.getRowCount()); i++)
	    {
	      Object[] line = m_oTableModel.getRow(i);
	      if (((Boolean)(line[0])).booleanValue())
	      {
	        list.add(m_oTableModel.getRowKey(i));
	      }
	    }
	    return list;
    }
    return null;
  }
  
  
  public static class AddressTableModel extends AbstractTableModel 
  {
    private int m_iNumColumns;      
    private String[] m_sColumnNames = null;
    private Boolean[] m_bIsEditable;
    private List m_oRows;
    private List m_oRowKeys;
    
    public AddressTableModel(int numcolumns)
    {
      m_iNumColumns = numcolumns;
      m_sColumnNames = new String[m_iNumColumns];
      m_bIsEditable = new Boolean[m_iNumColumns];
      m_oRows = new ArrayList();
      m_oRowKeys = new ArrayList();
      
      for(int i=0; i<m_iNumColumns; i++)
      {
        m_sColumnNames[i] = "";
        m_bIsEditable[i] = new Boolean(false);          
      }
    }
    
    public void setColumnName(int column, String name)
    {        
      m_sColumnNames[column] = name;
    }
    
    public void addRow(Object[] columns)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(new Object());
    }
    
    public void addRow(Object[] columns, Object rowkey)
    {
      m_oRows.add(columns);
      m_oRowKeys.add(rowkey);
    }
    
    public Object[] getRow(int row)
    {
      return (Object[])(m_oRows.get(row));
    }

    public Object getRowKey(int row)
    {
      return m_oRowKeys.get(row);
    }

    public Object setRowKey(int row, Object key)
    {
      return m_oRowKeys.set(row, key);
    }

    public void removeRow(int row)
    {
      m_oRows.remove(row);
      m_oRowKeys.remove(row);
    }

    public void removeAllRows()
    {
      m_oRows.clear();
      m_oRowKeys.clear();
    }

    public int getColumnCount() 
    {
      return m_iNumColumns;
    }

    public int getRowCount() 
    {
      return m_oRows.size();
    }

    public String getColumnName(int col) 
    {
      return m_sColumnNames[col];
    }

    public Object getValueAt(int row, int col) 
    {
      Object[] rowdata = getRow(row);
      return rowdata[col];
    }

    public Class getColumnClass(int c) 
    {
      try 
      {
        return getValueAt(0, c).getClass();
      } 
      catch(NullPointerException npe)
      {
        return String.class;  
      }
    }

    public boolean isCellEditable(int row, int col) 
    {
      return m_bIsEditable[col].booleanValue();
    }

    public void setColumnEditable(int col, boolean iseditable)
    {
      m_bIsEditable[col] = new Boolean(iseditable);
    }
    
    public void setValueAt(Object value, int row, int col) 
    {
      Object[] rowdata = getRow(row);
      rowdata[col] = value;
      fireTableCellUpdated(row, col);
    }
  }
  
}
