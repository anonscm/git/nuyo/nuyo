package org.evolvis.nuyo.gui.calendar;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import org.evolvis.nuyo.db.Appointment;


/*
 * Created on 16.07.2003
 *
 */

/**
 * @author niko
 *
 */

public class ScheduleEntryPanel extends JPanel
{
  private ScheduleData m_oScheduleData;
  private Point m_oPoint;
  private DragListener m_oDragListener;
  
  private ScheduleDate m_oStartDate;  
  private int m_iNumSeconds;
  private int m_iColumnpos = 0;
  private SchedulePanel m_oSchedulePanel;
  private int m_iNumColumns = 1;  
  
  private Appointment m_oAppointment = null;

  private boolean m_bIsActive = false;
  
  private Border m_NormalBorder;
  private Border m_ActiveBorder;
  
  private boolean m_bIsJob = false;
  private boolean m_bisReleasedAndWritable = false;
  
  private boolean m_bIsEditable;
  private boolean m_bIsSelectOnly;
  
  protected Integer _userID;
  private Integer _ID;
  
  
  public ScheduleEntryPanel(SchedulePanel sp, ScheduleDate start, int seconds, boolean isJob, boolean isReleasedAndWritable, Integer userID)
  {
	m_NormalBorder = new BevelBorder(BevelBorder.RAISED);
    m_ActiveBorder = new LineBorder(Color.RED, 3);
    
    _userID = userID;
    m_bIsJob = isJob;
    m_bisReleasedAndWritable = isReleasedAndWritable;
    
    setStartDate(start);
    setDuration(seconds);
    m_oSchedulePanel = sp;
    m_oScheduleData = sp.getScheduleData();
    updateSizeAndPosition();
    m_oDragListener = null;
  }

  
  public ScheduleEntryPanel(SchedulePanel sp, ScheduleDate start, ScheduleDate end, boolean isJob, boolean isReleasedAndWritable, Integer userID)
  {
  	this(sp, start, end.getScheduleSecondOfDay() - start.getScheduleSecondOfDay(), isJob,  isReleasedAndWritable, userID);
    
  }  
    
  public boolean isJob()
  {
    return m_bIsJob;
  }
  
  public boolean isReleasedAndWritable(){
	  return m_bisReleasedAndWritable;
  }
  
  public Integer getUserId(){
      return _userID;
  }

  public Integer getID() {
      return _ID;
  }
  
  public void setID(Integer id) {
      _ID = id;
  }
  public void revalidateDisplay()
  {    
    updateSizeAndPosition();    
  }
  
  public Appointment getAppointment()
  {
    return m_oAppointment;
  }
  
  public void setAppointment(Appointment appointment)
  {
    m_oAppointment = appointment;
  }
  
  public boolean isAppointmentEditable()
  {    
    return m_bIsEditable;    
  }
  
  protected void setAppointmentEditable(boolean editable)
  {
    m_bIsEditable = editable;
    if (m_oDragListener != null) m_oDragListener.setEnabled(m_bIsEditable);
  }
  
  
  public void setActive(boolean active)
  {
    m_bIsActive = active;        
    this.setBorder((active) ? m_ActiveBorder : m_NormalBorder);
  }
  
  public boolean isActive()
  {
    return m_bIsActive;
  }

  
  public SchedulePanel getSchedulePanel()
  {
    return m_oSchedulePanel;
  }
  
  
  public void updateSizeAndPosition()
  {    
    int startday = (getStartDate().getScheduleDayOfYear());
    int firstday = (m_oSchedulePanel.getFirstDay().getScheduleDayOfYear());    
    int day = startday - firstday;
    int startposy = m_oSchedulePanel.getPixelForSecond((getStartDate().getScheduleSecondOfDay()));
    int sizey = m_oSchedulePanel.getPixelForSecond(getDuration());
    int tmp = getDuration() / 360 ;
    this.setBounds((m_oScheduleData.dayWidth * day) + m_oScheduleData.dayDistanceToBorder, startposy, m_oScheduleData.dayWidth - 2 * m_oScheduleData.dayDistanceToBorder, sizey);
    this.revalidate();
  }

  
  public ScheduleDate getStartDate()
  {
    return m_oStartDate;
  }

  public int getDuration()
  {
    return m_iNumSeconds;
  }
  
  private void setDuration(int seconds)
  {
    m_iNumSeconds = seconds;
   if (m_iNumSeconds > 86340) m_iNumSeconds = 86340; 
  }
  
  public ScheduleDate getEndDate()
  {
    ScheduleDate enddate = getStartDate().getDateWithAddedSeconds(getDuration());    
    return enddate;
  }
  
  public void setStartDate(ScheduleDate oStartDate)
  {
    m_oStartDate = new ScheduleDate(oStartDate);
  }
  
  public void setEndDate(ScheduleDate oEndDate)
  {
    long delta = oEndDate.getTimeInMillis() - getStartDate().getTimeInMillis();
    setDuration((int)(delta / 1000));
  }  
  
  public void setDragListener(DragListener dl)
  {
    if (m_oDragListener != null) removeDragListener();
    addDragListenerToComponent(this, dl);    
    m_oDragListener = dl;
    if (m_oDragListener != null) m_oDragListener.setEnabled(m_bIsEditable);    
  }
  
  public void setDragListenerToSelectOnly(boolean selectonly){
	  m_bIsSelectOnly = selectonly;
	  if (m_oDragListener != null) m_oDragListener.setSelectOnly(selectonly);
  }
  
  public boolean isDragListenerSelectOnly(){
	  return m_bIsSelectOnly;
  }
  
  
  public void removeDragListener()
  {
    if (m_oDragListener != null)
    {  
      removeDragListenerFromComponent(this);    
      m_oDragListener = null;
    }
  }
  
  public void addDragListenerToComponent(JComponent comp, DragListener dl)
  {
    comp.addMouseMotionListener(dl);
    comp.addMouseListener(dl);
  }
  
  public void removeDragListenerFromComponent(JComponent comp)
  {
    if (m_oDragListener != null)
    {  
      comp.removeMouseMotionListener(m_oDragListener);
      comp.removeMouseListener(m_oDragListener);
    }
  }
  
  
  public void setScheduleWidth()
  {
    setScheduleWidth(this.m_iColumnpos, this.m_iNumColumns);    
  }
  
  
  
  public void setScheduleWidth(int columnpos, int numentriespercolumn)
  {
  	this.m_iColumnpos = columnpos;
  	this.m_iNumColumns = numentriespercolumn;
    int startday = (getStartDate().getScheduleDayOfYear());
    int firstday = (m_oSchedulePanel.getFirstDay().getScheduleDayOfYear());    
    int day = startday - firstday;
    int startposy = m_oSchedulePanel.getPixelForSecond((getStartDate().getScheduleSecondOfDay()));
    int sizey = m_oSchedulePanel.getPixelForSecond(getDuration());
    this.setBounds((m_oScheduleData.dayWidth * day) + m_oScheduleData.dayDistanceToBorder, startposy, m_oScheduleData.dayWidth - 2 * m_oScheduleData.dayDistanceToBorder, sizey);    

    Rectangle rect = this.getBounds();
    int newwidth = (m_oScheduleData.dayWidth - 2 * m_oScheduleData.dayDistanceToBorder) / numentriespercolumn;
    this.setBounds((rect.x) + (columnpos * newwidth), rect.y, newwidth, rect.height);
    this.revalidate();
  }

  /**
   * @return Returns the m_iColumnpos.
   */
  public int getColumnPos() 
  {
	  return m_iColumnpos;
  }
  
  /**
   * @param columnpos The m_iColumnpos to set.
   */
  public void setColumnPos(int columnpos) 
  {
	  m_iColumnpos = columnpos;
  }
  /**
   * @return Returns the m_iNumColumns.
   */
  public int getNumColumns() 
  {
    return m_iNumColumns;
  }

  /**
   * @param numColumns The m_iNumColumns to set.
   */
  public void setNumColumns(int numColumns) 
  {
	  m_iNumColumns = numColumns;
  }

  public void realized() {}
  
  public void refreshDisplay() {}
  
}

