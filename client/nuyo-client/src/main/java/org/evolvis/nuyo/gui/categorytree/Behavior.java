/**
 * 
 */


package org.evolvis.nuyo.gui.categorytree;

import org.evolvis.nuyo.gui.Messages;

/**
 * A <code>Behavior</code> instance manages the way a particular category
 * tree can be used.
 * 
 * <p>Since the category tree can be used for different purposes (selection, set
 * additions/removals/moves) an easy way of slightly modifiyng the way each tree
 * behaves.</p>
 * 
 * <p>This class provides:
 * <ul>
 * <li>the default selection of a category and subcategroy</li>
 * <li>the selection change sequence</li>
 * <li>the selection value to consider a tree element as part of the source selection</li>
 * <li>the selection value to consider a tree element as part of the destination selection</li>
 * <li>the label for the button that causes the category tree's action to executed</li>
 * <li>the informational text that the set operations provides</li>
 * <li>the icon name of the submit button</li>
 * </ul>
 * </p>
 * 
 * <p>It is not needed to create any other instances than the constant ones
 * which have been declared here. Instances of this class should not be used
 * outside of the scope of the category tree.
 * </p>
 * 
 * @author Robert Schuster
 *
 */
class Behavior
{
  /**
   * <code>Behavior</code> instance for a tree that allows subcategory
   * selection.
   */
  static final Behavior SELECT = new Behavior(
                                              Selection.NEUTRAL,
                                              new Selection[] {
                                                               Selection.NEUTRAL,
                                                               Selection.POSITIVE,
                                                               Selection.NEGATIVE },
                                              null, null,
                                              null, null, null, null);

  /**
   * <code>Behavior</code> instance for a tree that allows adding addresses to
   * a subcategory
   */
  static final Behavior ADD = new Behavior(Selection.NEUTRAL,
                                           new Selection[] {
                                                            Selection.POSITIVE,
                                                            Selection.NEUTRAL },
                                           Selection.POSITIVE, null,
                                           "GUI_CategoryTree_Add_Submit",
                                           "GUI_CategoryTree_Add_Info",
                                           "list-add.png",
                                           "GUI_CategoryTree_Add_Desc");

  /**
   * <code>Behavior</code> instance for a tree that allows removing addresses from
   * a subcategory
   */
  static final Behavior REMOVE = new Behavior(
                                              Selection.NEUTRAL,
                                              new Selection[] {
                                                               Selection.NEGATIVE,
                                                               Selection.NEUTRAL },
                                              Selection.NEGATIVE, null,
                                              "GUI_CategoryTree_Remove_Submit",
                                              "GUI_CategoryTree_Remove_Info",
                                              "list-remove.png",
                                              "GUI_CategoryTree_Remove_Desc");

  /**
   * <code>Behavior</code> instance for a tree that allows moving addresses to
   * a subcategory
   */
  static final Behavior MOVE = new Behavior(
                                            Selection.NEUTRAL,//TODO: change to Selection.NEGATIVE (as soon as bug 2709 is clear)
                                            new Selection[] {
                                                             Selection.NEUTRAL,//TODO: change the place order with Selection.POSITIVE (also for bug 2709)
                                                             Selection.POSITIVE,
                                                             Selection.NEGATIVE },
                                            Selection.POSITIVE, Selection.NEGATIVE,
                                            "GUI_CategoryTree_Move_Submit",
                                            "GUI_CategoryTree_Move_Info",
                                            "mail-send-receive.png",
                                            "GUI_CategoryTree_Move_Desc");

  /**
   * <code>Behavior</code> instance for a tree that allows setting a single
   * address' categoriy assignment.
   */
  static final Behavior SINGLE = new Behavior(
                                            Selection.NEUTRAL,
                                            new Selection[] {
                                                             Selection.NEUTRAL,
                                                             Selection.POSITIVE },
                                            Selection.POSITIVE, Selection.NEUTRAL,
                                            "GUI_CategoryTree_Single_Submit",
                                            "GUI_CategoryTree_Single_Info",
                                            "mail-send-receive.png",
                                            "GUI_CategoryTree_Single_Desc");
  
  private Selection[] categorySequence;

  private Selection defaultCategorySelection;
  
  private Selection destinationSelection;
  
  private Selection sourceSelection;
  
  private String submitTextKey;
  
  private String infoTextKey;
  
  private String submitIconName;
  
  private String shortBehaviorDescKey;

  private Behavior(Selection defaultCategorySelection, Selection[] categorySeq, Selection dest, Selection source, String submitTextKey, String infoTextKey, String submitIconName, String shortBehaviorDescKey)
  {
    this.defaultCategorySelection = defaultCategorySelection;
    categorySequence = categorySeq;
    destinationSelection = dest;
    sourceSelection = source;
    this.submitTextKey = submitTextKey;
    this.infoTextKey = infoTextKey;
    this.submitIconName = submitIconName;
    this.shortBehaviorDescKey = shortBehaviorDescKey;
  }

  /**
   * Returns the next selection state after the given one.
   * 
   * <p>If the selection state is not part of the sequence
   * the first entry in the sequence is returned. This is 
   * usually in effect when a category is in the mixed state.
   * </p> 
   * 
   * @param currentSelection
   * @return
   */
  Selection nextCategorySelection(Selection currentSelection)
  {
    return next(categorySequence, currentSelection);
  }

  /**
   * Returns the default selection state in which a category
   * and subcategory should go when added to the tree.
   * 
   * @return
   */
  Selection getDefaultCategorySelection()
  {
    return defaultCategorySelection;
  }

  /**
   * Implementation for {@link #nextCategorySelection(Selection)}
   * which can work on arbitrary sequence arrays.
   * 
   * <p>This method was once needed because there where multiple
   * sequences. If this is going to be reintroduced just call
   * this method.</p>
   * 
   * @param sequence
   * @param current
   * @return
   */
  private Selection next(Selection[] sequence, Selection current)
  {
    for (int i = 0; i < sequence.length - 1; i++)
      {
        if (sequence[i] == current)
          return sequence[i + 1];
      }

    return sequence[0];
  }
  
  /**
   * Returns the localized label for the submit button, which is
   * the one that causes the category tree's action to be done.
   *  
   * @return
   */
  String getSubmitText()
  {
    return Messages.getString(submitTextKey);
  }
  
  /**
   * Returns the icon-name for the submit-button, which can be
   * found by the IconFactory
   * 
   * @return
   */
  
  String getSubmitIconName()
  {
	  return submitIconName;
  }
  
  String getShortBehaviorDescription()
  {
	  return Messages.getString(shortBehaviorDescKey);
  }
  
  /**
   * Returns the localized label for the information area which
   * exists for the set operations only.
   * 
   * <p>In case <code>affectedAddresses</code> is <code>1</code>
   * <code>_Single</code> is appended to the basic info text key
   * allowing the translators to provide a different sentence for
   * the singular case.</code>
   * 
   * @param affectedAddresses
   * @return
   */
  String getInfoText(int affectedAddresses)
  {
      return (affectedAddresses == 1)
      ? Messages.getString(infoTextKey + "_Single")
      : Messages.getFormattedString(infoTextKey, new Integer(affectedAddresses));
  }
  
  Selection getSourceSelection()
  {
    return sourceSelection;
  }
  
  Selection getDestinationSelection()
  {
    return destinationSelection;
  }

}