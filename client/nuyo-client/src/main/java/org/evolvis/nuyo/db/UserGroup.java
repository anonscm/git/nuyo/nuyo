/* $Id: UserGroup.java,v 1.4 2006/10/27 13:21:53 aleksej Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

import java.util.Collection;

/**
 * Diese Schnittstelle stellt Benutzergruppen dar.
 * 
 * @author mikel
 */
public interface UserGroup extends Comparable {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();


    /** Gibt an, ob es sich um eine private Gruppe eines Users handelt */
    public boolean isPrivate();

    /** Gibt an, ob es sich um eine private Gruppe eines Users handelt */
    public boolean isAdminGroup();
    
    //
    // Attribute
    //
    /** Benutzer in der Benutzergruppe */
    public Collection getUsers() throws ContactDBException;
    public void add(User newUser) throws ContactDBException;
    public void remove(User newUser) throws ContactDBException;

    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    //
    // Methoden
    //
    /**
     * Diese Methode holt das Kalendarium der Gruppe. Falls es noch keines gibt,
     * kann dieses automatisch erzeugt werden.
     * 
     * @param create zeigt an, ob gegebenenfalls ein Kalendarium f�r die Gruppe erzeugt
     *  werden soll.
     * @return Kalendarium der Gruppe; falls keines existiert und das Erzeugungsflag
     *  nicht gesetzt ist, wird <code>null</code> zur�ckgegeben.
     */
    public Calendar getCalendar(boolean create) throws ContactDBException;

    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Name, Beschreibung, Ersteller, letzter Bearbeiter */
    public final static String KEY_NAME = "name";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_SHORTNAME = "shortname";
    public final static String KEY_GLOBALROLE = "globalrole";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    public final static String KEY_IS_PRIVATE = "isPrivate";
    public final static String KEY_GROUP_TYPE = "groupType";


    public final static int GROUP_TYPE_ADMIN = 1;
    public final static int GROUP_TYPE_ALL   = 2;
    public final static int GROUP_TYPE_OTHER = 0;


    public int compareTo(Object o);
}

