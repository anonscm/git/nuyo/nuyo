package org.evolvis.nuyo.gui.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.datahandling.ListFilterOperator;

/**
 * Operator implementation for use within the search filter model.
 * 
 * <p>A {@link LabeledFilterOperator} encapsules an identifying key, a filter
 * operator and some properties. The class provides means for retrieving a label
 * for the instance and expression generation.</p>
 * 
 * <p>The operators provided by this classes' instances are usually of a 
 * higher level than their counterparts in {@link ListFilterOperator} and
 * resemble the needs in the user interface. Constructs like 'contains not'
 * cannot be modeled with one {@link ListFilterOperator} only. Furthermore
 * the 'contains' operation' depends on special formatting of its 2nd argument
 * (enclosing in <code>%</code>s). All of this is handled via this class.</p> 
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
class LabeledFilterOperator {
    /** Holds the index of the operator objects. */
    private final static Map INDEX = new HashMap();
    
    public final static LabeledFilterOperator CONTAINS = new LabeledFilterOperator("contains", ListFilterOperator.LIKE, "%", "%", false);
    public final static LabeledFilterOperator CONTAINS_NOT = new LabeledFilterOperator("contains_not", ListFilterOperator.LIKE, "%", "%", true);
    public final static LabeledFilterOperator IS = new LabeledFilterOperator("is", ListFilterOperator.LIKE);
    public final static LabeledFilterOperator IS_NOT = new LabeledFilterOperator("is_not", ListFilterOperator.LIKE, true);
    public final static LabeledFilterOperator BEGINS_WITH = new LabeledFilterOperator("begins_with", ListFilterOperator.LIKE, "", "%", false);
    public final static LabeledFilterOperator ENDS_WITH = new LabeledFilterOperator("ends_with", ListFilterOperator.LIKE, "%", "", false);
    public final static LabeledFilterOperator DATE_IS_GREATER_THAN = new LabeledFilterOperator("date_is_greater_than", ListFilterOperator.GT);
    public final static LabeledFilterOperator DATE_IS_LESS_THAN = new LabeledFilterOperator("date_is_less_than", ListFilterOperator.LT);

    public final static LabeledFilterOperator OR = new LabeledFilterOperator("or", ListFilterOperator.OR);
    public final static LabeledFilterOperator AND = new LabeledFilterOperator("and", ListFilterOperator.AND);
    public final static LabeledFilterOperator NOT = new LabeledFilterOperator("not", ListFilterOperator.NOT);
    
    public final static LabeledFilterOperator[] STRING_BASED_SET = new LabeledFilterOperator[] {CONTAINS,CONTAINS_NOT,IS,IS_NOT,BEGINS_WITH,ENDS_WITH};
    public final static LabeledFilterOperator[] DATE_BASED_SET = new LabeledFilterOperator[] {IS,IS_NOT,DATE_IS_GREATER_THAN, DATE_IS_LESS_THAN};
    public final static LabeledFilterOperator[] IDENTITY_BASED_SET = new LabeledFilterOperator[] {IS};
    
    private String name;
    private String label;
    private ListFilterOperator operator;
    
    /** A string which is prepended to the 2nd argument at expression generation time. 
     */
    private String prefix;
    
    /** A string which is appended to the 2nd argument at expression generation time.
     */
    private String postfix;
    
    /** A flag controlling whether the whole expression should be negated after generation.
     * 
     */
    private boolean negate;

    /** A predefined prefix to lookup for a label translation.*/
    public static String  LABEL_PREFIX = "LabeledFilterOperator_";
    
    /**
     * Creates a {@link LabeledFilterOperator} with the given identifier,
     * operator and allows specifiying whether an expression built from it
     * should be negated or not.
     * 
     * <p>Using this constructor the 2nd argument prefix and postfix is empty.</p>
     * 
     * @param name
     * @param op
     * @param negate
     */
    private LabeledFilterOperator(String name, ListFilterOperator op, boolean negate)
    {
      this(name, op, "", "", negate);
    }

    private LabeledFilterOperator(String name, ListFilterOperator op)
    {
      this(name, op, "", "", false);
    }
    
    private LabeledFilterOperator(String aName, ListFilterOperator anOperator, String prefix, String postfix, boolean negate){
        if(aName == null) throw new NullPointerException("a name of operator must not be null");
        if(anOperator == null) throw new NullPointerException("an operator must not be null");
        name = aName;
        operator = anOperator;
        this.prefix = prefix;
        this.postfix = postfix;
        this.negate = negate;
        
        INDEX.put(name, this);
    }
    
    String getName(){
        return name;
    }
    
    String getLabel() {
        if(label == null) {
            label = Messages.getString(LabeledFilterOperator.LABEL_PREFIX + name);
            boolean isNotInternationalized = label.length() > 0 
            && (label.startsWith("!") & label.endsWith("!"));   
            if(isNotInternationalized) label = operator.toString();
        }
        return label;
    }
    
    /**
     * Constructs an expression with the given operands and according
     * to this instance's properties.
     * 
     * <p>The second argument is enclosed with a prefix and a postfix
     * if those are given and the argument is not null.</p>
     * 
     * <p>The expression is appended to the given list in postfix
     * notation.</p>
     * 
     * @param expressions
     * @param operand1
     * @param operand2
     * @return
     */
    List addExpression(List expressions, Object operand1, Object operand2)
    {
        expressions.add(operand1);
        
        if (operand2 != null)
          {
            operand2 = substituteWildcard(operand2);
            expressions.add(prefix + operand2 + postfix);
          }
        
        expressions.add(operator);
        
        if (negate)
            expressions.add(NOT.operator);
        
        return expressions;
    }

    /**
     * Replaces user-given wildcard characters with the ones needed by the
     * database ('?' -> '_' and '*' -> '%'). 
     */
    private Object substituteWildcard(Object operand) {
    	// Note: operand.getClass() == String.class is a fast variant
    	// for (foo instanceof String).
        if (operand.getClass() == String.class && (operator == ListFilterOperator.LIKE || operator == ListFilterOperator.ILIKE)) {
            return ((String)operand).replace('?', '_').replace('*', '%');
        }
        return operand;
    }

    public String toString(){
        return getLabel();
    }
    
    /**
     * Looks up a name to find the associated operator object.
     * 
     * @param name of an operator
     * @return LabeledFilterOperator an assigned object to a given name
     */
    public static LabeledFilterOperator lookup(String name){
        return (LabeledFilterOperator) INDEX.get(name);
    }
}
