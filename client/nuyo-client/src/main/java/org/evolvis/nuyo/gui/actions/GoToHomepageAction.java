package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;



/**
 * Opens a homepage of the selected contact.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class GoToHomepageAction extends AbstractContactDependentAction {

	private static final long	serialVersionUID	= 5245311564767096021L;
	private static final TarentLogger log = new TarentLogger(GoToHomepageAction.class);
    private GUIListener guiListener;
    private Runnable executor;

    /**
     * Opens a homepage of the selected contact in a new thread.
     */
	public void actionPerformed(ActionEvent e) {
        if(guiListener != null) {
            // start new thread
            new Thread(executor, "view-homepage-thread").start();
        } else log.warning("Couldn't view a homepage", getClass().getName() + "is not initialized");
	}

    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        executor = new Runnable() {
            public void run() {
                guiListener.userRequestStartBrowser();
            }
        };
    }
}
