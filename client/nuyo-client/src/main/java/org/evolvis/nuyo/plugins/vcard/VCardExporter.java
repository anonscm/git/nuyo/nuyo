/* $Id: VCardExporter.java,v 1.7 2007/08/30 16:10:33 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Jens Neumaier. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.plugins.vcard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.plugin.AddressExporter;
import org.evolvis.nuyo.plugins.dataio.DefaultFileFilter;
import org.evolvis.nuyo.plugins.dataio.ExporterException;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusResult;

/**
 * Exporter for addresses in vCard-Format. 
 * 
 * @author Jens Neumaier, tarent GmbH
 * @see org.evolvis.nuyo.plugin.AddressExporter
 * 
 */
public class VCardExporter extends AbstractGUIAction implements ActionListener, AddressExporter {
    
    private static final long serialVersionUID = 6460610123988164485L;

    private static Logger logger = Logger.getLogger(VCardExporter.class.getName());
    private static String PROFILE_DEFAULT_V2 = "vCard Version 2.1 Default Profile";
    private static String PROFILE_DEFAULT_V3 = "vCard Version 3.0 Default Profile";
    
    private Addresses addresses;
    
    public VCardExporter() {        
    }
    
    public void setAddresses(Addresses addresses){
        this.addresses = addresses;
    }
    
    public Addresses getAddresses(){
        return this.addresses;
    }
    
    public void execute(){
        // execution by ExportFilePlugin
        // using export() with requested FileFilter
    }

    public void actionPerformed(ActionEvent e) {
        // no events in this plugin
    }

    /**
     * @pre addresses are not empty
     */
    public void export(OutputStream out, String fileTypeDescription) throws IOException, ExporterException {
        // getting octopus connection - RuntimeException FactoryConfigurationException will not be catched
        OctopusConnectionFactory ocFactory = OctopusConnectionFactory.getInstance();
        OctopusConnection ocConnection = ocFactory.getConnection(OctopusDatabase.OC_CONNECTION_IDENTIFIER);
        
        // preparing select filter
        List filter = new ArrayList();
        filter.add("adrNr");
        filter.add(addresses.getPkList());
        filter.add("IN");
        
        // set vCard version
        String vCardVersion;
        if (fileTypeDescription.equalsIgnoreCase(PROFILE_DEFAULT_V2))
            vCardVersion = "2.1";
        else
            vCardVersion = "3.0";
        
        // logging octopus call
        if (logger.isLoggable(Level.FINE)) logger.fine("Fordere " + addresses.getSize() + " Adressen vom Server an...");

        // calling octopus task
        OctopusResult ocResult;
        try {
            ocResult = ocConnection.getTask("getAddressesAsVCard").add("filter", filter).add("vcard_version", vCardVersion).invoke();
        } catch (OctopusCallException e) {
            throw new ExporterException(e);
        }
        
        String sVCard = (String) ocResult.getData("vcard");
        
        if (sVCard == null || sVCard.equals(""))
            throw new ExporterException(ExporterException.NO_RESULT_STRING);
        
        out.write(sVCard.getBytes());
         
    }

    public FileFilter[] getSupportedFileTypes() {
        FileFilter[] fileFilter = new FileFilter[2];
        fileFilter[0] = new DefaultFileFilter("vcf", PROFILE_DEFAULT_V2);
        fileFilter[1] = new DefaultFileFilter("vcf", PROFILE_DEFAULT_V3);
        
        return fileFilter;
    }

    public JDialog getConfigurationDialog(FileFilter fileType) {
        // no configuration needed yet
        return null;
    }

}

