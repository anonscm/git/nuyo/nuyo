package org.evolvis.nuyo.db.octopus;

import java.util.Date;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.gui.Messages;


/**
 * @author kleinw
 *	
 *	Implementierung de.tarent.contact.db.Contact
 *
 */
abstract public class ContactBean extends AbstractEntity implements Contact {

    //
    //	Instanzmerkmale
    //
    /**	Zu dem Kontakt dazugeh�rige Adresse */
    protected Address _address;
    /**	Zu dem Kontakt dazugeh�riger Benutzer */
    protected User _user;
    /** Kategorie */
    protected Integer _category;
    /** Kanal */
    protected Integer _channel;
    /**	Dauer */
    protected Integer _duration;
    /** Link */
    protected Integer _link;
    /** Linktyp */
    protected Integer _linkType;
    /** don�t know */
    protected Boolean _inbound;
    /**	Startdatum */
    protected Date _startDate;
    /**	Historisierungsdatum */
    protected Date _historyDate;
    /**	Objekt zum Holen von Daten */
	static AbstractEntityFetcher _fetcher;
    
	
    /**	Zum Kontakt dazugeh�rige Adresse */
    public Address getAddress() throws ContactDBException {return _address;}
	/** Zum Kontakt dazugeh�riger Benutzer */
    public User getUser() throws ContactDBException {return _user;}

    
    /**	Kategorie des Kontakts */
    public int getCategory() throws ContactDBException {return _category.intValue();}

    
    /**	Kanal des Kontakts */
    public int getChannel() throws ContactDBException {return _channel.intValue();}

    
    /**	Dauer des Kontakts */
    public int getDuration() throws ContactDBException {return (_duration == null)?0:_duration.intValue();}

    
    /** Link des Kontakts */
    public int getLink() throws ContactDBException {return (_link == null)?0:_link.intValue();}

    
    /**	Typ des Links */
    public int getLinkType() throws ContactDBException {return (_linkType == null)?0:_linkType.intValue();}

    
    /**	Datum des Kontakts */
    public Date getStart() throws ContactDBException {return _startDate;}

    
    /**	don�t know */
    public boolean isInbound() throws ContactDBException {return _inbound.booleanValue();}

    
    /**	Historisierungsdatum */
    public Date getHistoryDate() {return _historyDate;}
    
    
    /**	Hier stehen die Umwandlungen von int Konstanten nach String */
    static public class ContactUtils {

        /**	Gibt den Channel als String zur�ck.@param channel - Kanal */
        public static String generateChannelString(int channel) {
    		String channelstring = null;
    	    switch(channel){
    	    	case Contact.CHANNEL_DIRECT: channelstring = "Contact_Channel_Direct";
    	    	break;
    	    	case Contact.CHANNEL_EMAIL: channelstring = "Contact_Channel_Email";
    	    	break;
    	    	case Contact.CHANNEL_FAX: channelstring = "Contact_Channel_Fax";
    	    	break;
    	    	case Contact.CHANNEL_MAIL: channelstring = "Contact_Channel_Mail";
    	    	break;
    	    	case Contact.CHANNEL_NA: channelstring = "Contact_Channel_NA";
    	    	break;
    	    	case Contact.CHANNEL_PHONE: channelstring = "Contact_Channel_Phone";
    	    	break;
    	    }
    		return Messages.getString(channelstring);
    	}

        /** Gibt eine Stringrepr�sentation des Kategorietyps zur�ck.@param category - Kategorie */
    	public static String generateCategoryString(int category) {
    		String categorystring = null;
    		switch(category){
    			case Contact.CATEGORY_MAILING: categorystring = "Contact_Category_Mailing";
    			break;
    			case Contact.CATEGORY_MEMO: categorystring = "Contact_Category_Memo";
    			break;
    			case Contact.CATEGORY_REQUEST: categorystring = "Contact_Category_Request";
    			break;
    		}
    		return Messages.getString(categorystring);
    	}
    
        public static String generateDirectionString(boolean inbound)
        {
          return Messages.getString((inbound ? "Contact_Direction_Inbound" : "Contact_Direction_Outbound")); 
        }
    	
    }
    
}
