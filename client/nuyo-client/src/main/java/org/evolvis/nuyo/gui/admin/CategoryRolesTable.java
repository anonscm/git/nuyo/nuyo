package org.evolvis.nuyo.gui.admin;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;


/**
 * A table with category roles entries.
 * The first column represents roles names
 * and the rest columns show the assigned rights.
 */
class CategoryRolesTable extends JTable {
    private static final TarentLogger logger = new TarentLogger(CategoryRolesTable.class);
    private static final String[] tooltips;
    
    static {
        tooltips = new String[] { Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Name" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht1" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht2" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht3" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht4" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht5" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht6" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht7" ),
                                  Messages.getString( "GUI_Admin_Tooltip_Folder_Rolle_Recht8" ) };
    }

    
    /** Creates a roles table with a given model.*/
    CategoryRolesTable(TableModel model) {
        super(model);
        showHorScroll(true);
        initHeaderSize();
    }
    
    private void initHeaderSize() {
        logger.finest("setting header size...");
        TableColumnModel colModel = getColumnModel();
        //note: computing the width from column name string is unsuitable 
        //      because together with the html tags the width gets to long
        colModel.getColumn( 0 ).setPreferredWidth( 130 );
        for (int i = getColumnCount() - 1; i>0; i--) {
            colModel.getColumn( i ).setPreferredWidth( 80 );
        }        
    }

    /**
     * This method shows the horizontal scroll bar when required.
     * Make sure it is called for other methods like setHeaderWidth etc to work properly
     */
    public void showHorScroll( boolean show ) {
        if ( show ) setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        else setAutoResizeMode( JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS );
    }
    
    /** Computes the column size as per the Header text. */
    public int calculateWidth(int pColumn){
        String value =  getColumnName(pColumn);
        FontMetrics metrics = getGraphics().getFontMetrics();
        /* note: if getGraphics() returns null the possible workaround could be:
         * FontMetrics metrics = Toolkit.getDefaultToolkit().getFontMetrics(StartUpConstants.PLAIN_12_FONT);
         */        
        int width = metrics.stringWidth(value) + (2*getColumnModel().getColumnMargin());
        logger.finest("(" + value + ").width=" + width);
        return width;
    }
    
    public String getToolTipText(MouseEvent e) {
        java.awt.Point p = e.getPoint();
//        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);
        int realColumnIndex = convertColumnIndexToModel(colIndex);
        return tooltips[realColumnIndex];
    }
    
    protected JTableHeader createDefaultTableHeader() {
        return new JTableHeader(columnModel) {
            
            public TableCellRenderer getDefaultRenderer() {
                return new RolesHeaderRenderer();
            }
            
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
//                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColumnIndex = convertColumnIndexToModel(colIndex);
                
                return tooltips[realColumnIndex];
            }
        };
    }
    
    /** This method should be called to set the column at pColumn index to a width of pWidth. */
    public void setColumnWidth( int pColumn, int pWidth ) {
        TableColumnModel colModel = getColumnModel();
        colModel.getColumn( pColumn ).setPreferredWidth( pWidth );
    }

    /** This method would set pColumn resizable or not based on the flag: pResizable */
    public void setResizable( int pColumn, boolean pIsResize ) {
        TableColumnModel colModel = getColumnModel();
        colModel.getColumn( pColumn ).setResizable( pIsResize );
    }

    private class RolesHeaderRenderer extends DefaultTableCellRenderer{
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JComponent comp = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            comp.setPreferredSize(new Dimension(comp.getPreferredSize().width,comp.getPreferredSize().height*2));
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setHorizontalAlignment(JLabel.CENTER);
            return comp;
        }
    }
}
