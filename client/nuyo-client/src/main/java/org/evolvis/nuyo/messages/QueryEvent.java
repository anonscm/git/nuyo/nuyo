/* $Id: QueryEvent.java,v 1.3 2006/06/06 14:12:07 nils Exp $
 * 
 * Created on 16.10.2003
 */
package org.evolvis.nuyo.messages;

/**
 * Diese Schnittstelle ist von allen nachfragenden Ereignisklassen
 * zu implementieren.
 * 
 * @author mikel
 */
public interface QueryEvent {
    /**
     * Diese Methode liefert die aktuell eingestellte Antwort auf
     * die Nachfrage.
     *  
     * @return Antwort auf die Nachfrage. 
     */
    public Object getReply();

    /**
     * Diese Methode setzt eine Antwort auf die Nachfrage.
     *  
     * @param reply neue Antwort.
     */
    public void setReply(Object reply);
}
