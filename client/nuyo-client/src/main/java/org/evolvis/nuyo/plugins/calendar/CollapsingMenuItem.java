/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.plugins.calendar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *  
 * @author niko
 * @deprecated Calendar plugin uses it. Remove it when rewritten.
 */
interface CollapsingMenuItem
{
  public void setUserString(String text);  
  public String getUserString();
  public void setUserDescription(String text);  
  public String getUserDescription();
  public JButton getImageButton();
  public ImageIcon getIcon();
  public void setIcon(ImageIcon icon);
  public void setHighlightColor(Color color);
  public void setBackgroundColor(Color color);
  public void setSelectionColor(Color color);
  public void setSelectionHighlightColor(Color color);
  public void drawBorderWhenSelected(boolean drawborder);
  public void setSelectedItemBorder(int width);
  public void setItemBorder(int width);
  public void setItemToolTipText(String text);
  public void setItemEnabled(boolean isenabled);
  public boolean getItemEnabled();
  public void setItemSelected(boolean isselected);
  public boolean isSelected();
  public void setItemIndent(int indent);
  public void setUserDataObject(Object userdata);
  public void setUserDataInt(int userdata);
  public Object getUserDataObject();
  public int getUserDataInt();
  public void setItemVisible(boolean isvisible);
  public boolean getItemVisible();
  public void setItemFont(Font font);
  public Font getItemFont();
  public String getText();
  public void setText(String text);
  public ActionListener getActionListener();
  public void setActionListener(ActionListener al);
  public void setDescription(String text);
  public String getDescription();
  public void setAcceleratorKey(char key);
  public void setAcceleratorKey(String key);
  public void setAcceleratorIndex(int charnum);
}


