

package org.evolvis.nuyo.gui.action.journal;

import java.awt.Component;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.octopus.ContactBean.ContactUtils;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.utils.TaskManager;

class JournalEditorModel extends AbstractReadOnlyBinding
{

  User user;

  String userLoginName;

  Address address;

  String addressLabel;

  Date date;

  int category;

  int channel;

  boolean inbound;

  int duration;

  String subject;

  String note;

  /*
   * This creates a contact entry.
   */
  void createEntry()
  {
	TaskManager.getInstance().register(new TaskManager.Task()
	{
		public void run(TaskManager.Context ctx)
		{
			GUIListener am = ApplicationServices.getInstance().getActionManager();

		    am.userRequestCreateContactEntry(address, user, category, channel,
		                                     duration, inbound, subject, note, date);
			
		}
		
		public void cancel(){}
	}, Messages.getString("GUI_JournalEditor_CreatingEntry"), false);
  }

  DefaultComboBoxModel categoryListModel;

  ListCellRenderer categoryListRenderer = new CategoryListRenderer();

  DefaultComboBoxModel channelListModel;

  ListCellRenderer channelListRenderer = new ChannelListRenderer();

  DefaultComboBoxModel directionListModel;

  ListCellRenderer directionListRenderer = new DirectionListRenderer();

  JournalEditorModel()
  {
	super(ApplicationModel.SELECTED_ADDRESS_KEY);
	
	ApplicationServices as = ApplicationServices.getInstance();
	BindingManager bm = as.getBindingManager();
    bm.addBinding(this);

    user = as.getActionManager().getUser(null);
    try
      {
        userLoginName = user.getLoginName();
      }
    catch (ContactDBException e)
      {
        // Unlikely.
      }

    categoryListModel = new DefaultComboBoxModel();
    categoryListModel.addElement(new Integer(Contact.CATEGORY_MAILING));
    categoryListModel.addElement(new Integer(Contact.CATEGORY_MEMO));
    categoryListModel.addElement(new Integer(Contact.CATEGORY_REQUEST));

    channelListModel = new DefaultComboBoxModel();
    channelListModel.addElement(new Integer(Contact.CHANNEL_DIRECT));
    channelListModel.addElement(new Integer(Contact.CHANNEL_EMAIL));
    channelListModel.addElement(new Integer(Contact.CHANNEL_FAX));
    channelListModel.addElement(new Integer(Contact.CHANNEL_PHONE));
    channelListModel.addElement(new Integer(Contact.CHANNEL_MAIL));
    channelListModel.addElement(new Integer(Contact.CHANNEL_NA));

    directionListModel = new DefaultComboBoxModel();
    directionListModel.addElement(Boolean.FALSE);
    directionListModel.addElement(Boolean.TRUE);

  }

  private static class CategoryListRenderer extends DefaultListCellRenderer
  {
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus)
    {
      super.getListCellRendererComponent(list, value, index, isSelected,
                                         cellHasFocus);
      setText(ContactUtils.generateCategoryString(((Integer) value).intValue()));

      return this;
    }
  }

  private static class ChannelListRenderer extends DefaultListCellRenderer
  {
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus)
    {
      super.getListCellRendererComponent(list, value, index, isSelected,
                                         cellHasFocus);
      setText(ContactUtils.generateChannelString(((Integer) value).intValue()));

      return this;
    }
  }

  private static class DirectionListRenderer extends DefaultListCellRenderer
  {
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus)
    {
      super.getListCellRendererComponent(list, value, index, isSelected,
                                         cellHasFocus);
      setText(ContactUtils.generateDirectionString(((Boolean) value).booleanValue()));

      return this;
    }
  }

public void setViewData(Object arg) {
    address = (Address) arg;

    // This generates an address label in the form:
    // prename name, street #nr, postcode city
    // This is a simple solution but is otherwise
    // hardcoded and therefore bad.
    // TODO: Write a helper class that retrieves often
    // used combinations of the address property values.
    addressLabel = address.getVorname() + " " + address.getNachname() + ", "
                   + address.getStrasse() + ", " + address.getPlz() + " "
                   + address.getOrt();
}

}
