package org.evolvis.nuyo.db;

import java.util.List;

/**
 * @author Sebastian Mancke, tarent GmbH
 * 
 * This is a management inerface for address obejcts.
 *  
 */
public interface AddressClientDAO  {

    /**
     * Saves the address.
     * The the changed-date of the supplied object will be set.
     */
	public void save(Address address) throws ContactDBException;

    /**
     * Create a new address.
     * The the id, creation- and changed-date of the supplied object will be set.
     * @param Address the address to save
     * @param subcategorieList A list of integer id's with the initial subcategories
     */
	public void create(Address address, List subCategories) throws ContactDBException;

    /**
     * Deletes the address completely (not only from some categories)
     */
    public void delete(Address address) throws ContactDBException;    
    
    /**
     * Returns a list of duplicates the the address
     */
    public Addresses getDoubles(Address address, int exclude) throws ContactDBException;

    /**
     * Returns the contact history (aka "Journal") for an address
     */
	public List getHistory(Address address) throws ContactDBException;

    /**
     * Returns the user associated with an address, 
     * or null, if non is associated.
     */
 	public User getAssociatedUser(Address address) throws ContactDBException;

    /**
     * Associate a user with an address.
     */
 	public void setAssociatedUser(Address address, User user) throws ContactDBException;

}
