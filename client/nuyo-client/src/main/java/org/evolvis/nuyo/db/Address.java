/* $Id: Address.java,v 1.24 2007/03/16 12:18:19 schmitz Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.nuyo.groupware.impl.AddressImpl;

import de.tarent.commons.datahandling.binding.DataChangedEvent;
import de.tarent.commons.datahandling.binding.DataChangedListener;
import de.tarent.commons.datahandling.binding.Model;
import de.tarent.commons.datahandling.entity.Entity;

/**
 * Diese Klasse liefert die Detaildaten zu einer Adresse und erlaubt ihr
 * Ver�ndern. Die Adressdaten sind abh�ngig von der {@link #Category
 * Kategorie}, in deren Kontext die Adresse betrachtet wird.
 * 
 * Die meisten der Datenfelder werden in einer Treemap verwaltet, die einzelnen
 * Schlssel sind hier als Konstanten definierten und k�nnen ber Getter und Setter
 * gesetzt und gelesen werden.
 * 
 * @author mikel
 */
public abstract class Address extends AddressImpl implements Model, Entity {//, PersistentEntity


    public static int ADR_NR_NEW = 0;
        
    // implementation of the model interface
    // TODO: throw the events for the data changes
    protected List dataChangedListener;

    protected void fireDataChanged(DataChangedEvent e) {
        if (dataChangedListener == null)
            return;
        for (Iterator iter = dataChangedListener.iterator(); iter.hasNext();) {
            DataChangedListener listener = (DataChangedListener)iter.next();
            listener.dataChanged(e);
        }
    }    
    
    public void addDataChangedListener(DataChangedListener listener) {
        if (dataChangedListener == null)
            dataChangedListener = new ArrayList(2);
        dataChangedListener.add(listener);
    }    
    
    /**
     * Removes a DataChangedListener
     * @param listener The registered listener
     */
    public void removeDataChangedListener(DataChangedListener listener) {
        if (dataChangedListener != null) {
            dataChangedListener.remove(listener);
        }
    }
    
	public void setStandardData(String id, Object newValue) {
		super.setStandardData(id, newValue);
		fireDataChanged(new DataChangedEvent(this, id));
	}
	
 	/**
 	 * Vollst�ndiges laden aller Adressdaten.
 	 */
 	public abstract void loadAllData() throws ContactDBException;

}
