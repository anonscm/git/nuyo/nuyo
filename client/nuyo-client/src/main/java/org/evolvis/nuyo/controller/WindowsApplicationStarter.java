/* $Id: WindowsApplicationStarter.java,v 1.9 2007/08/30 16:10:25 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;
import java.util.logging.Level;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;


/**
 * Starten von Anwendungen wie Browser, Mail, ... aus dieser Anwendung heraus.
 *
 * Klasse, die unter Windows konfigurierte Kommandos ausf�hrt.
 * Sollte nur �ber die Factorymethode
 * {@link org.evolvis.nuyo.controller.ApplicationStarter#getSystemApplicationStarter(Map, ControlListener)
 * ApplicationStarter.getSystemApplicationStarter} instanziiert werden.
 * 
 * @author Sebastian, mikel
 */
public class WindowsApplicationStarter extends ApplicationStarter {
    protected String postCommunicationFileName;
    protected String postCommunicationCommand;
    protected String postOptimizedCommand;

    protected static final String DEFAULT_COMMUNICATION_FILE_NAME = "contactclient.tc";
    protected static final String DEFAULT_COMMUNICATION_COMMAND = ".\\winfinder\\controlerapp.exe Autoload";
    protected static final String DEFAULT_OPTIMIZED_COMMAND = ".\\winfinder\\finderapp2.exe Autoload";
    protected TarentLogger logger = new TarentLogger(WindowsApplicationStarter.class);

    public WindowsApplicationStarter() {
        
        Environment env = ConfigManager.getEnvironment();

        postCommunicationFileName = env.get(Key.POST_COMMUNICATION_FILE_NAME);
        if (postCommunicationFileName == null) {
            String userHome = System.getProperty("user.home");
            postCommunicationFileName =
                userHome + System.getProperty("file.separator") + DEFAULT_COMMUNICATION_FILE_NAME;
        }

        postCommunicationCommand = env.get(Key.POST_COMMUNICATION_COMMAND);
        if (postCommunicationCommand == null)
            postCommunicationCommand = DEFAULT_COMMUNICATION_COMMAND;

        postOptimizedCommand = env.get(Key.POST_OPTIMIZED_COMMAND);
        if (postOptimizedCommand == null)
            postOptimizedCommand = DEFAULT_OPTIMIZED_COMMAND;
    }

    public String startBrowser(String url) {
        if (url == null || url.length() == 0) {
            return "Es ist keine URL vorhanden.";
        }
        String cmd = "rundll32 url.dll,FileProtocolHandler " + url;
        return runWinCommand(cmd);
    }

    public String startEMail(Contact con)
    {
        String mail = null;
        try 
        {
            mail = con.getAddress().getEmailAdresseDienstlich();
            if (mail == null || mail.length() == 0) return "Es ist keine EMail vorhanden.";
        }
        catch (ContactDBException e) 
        {
            return "Es ist ein Datenbankfehler aufgetreten.";
        }

        return runEMail(mail);
    }
    
    public String startEMail(String emailAddress) {
        if (emailAddress == null || emailAddress.length() == 0)
            return "Es ist keine EMail vorhanden.";
        
        return runEMail(emailAddress);
    }
    
    public String startEMail(Address address) {
    	if(address != null) {
    		if(address.getEmailAdresseDienstlich() != null && address.getEmailAdresseDienstlich().length() > 0)
    			return startEMail(address.getEmailAdresseDienstlich());
    		else
    			return startEMail(address.getEmailAdressePrivat());
    	}
    	return "Es ist keine EMail vorhanden."; 
    }

    /* (non-Javadoc)
     * @see de.tarent.contact.controller.ApplicationStarter#startPhoneCall(java.lang.String)
     */
    public String startPhoneCall(String phoneNumber) {
        return runPhoneCall(phoneNumber);
    }

    /**
     * Startet einen Postversand an eine Einzeladresse
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, Address address) {
        try {
            return (handledSingleFaxAsEMail(action, address))
                ? null
                : runWinCommand(postCommunicationCommand + ' ' + createXML(action, address));
        } catch (ContactDBException se) {
            ActionManager.logger.log(Level.WARNING, "Fehler beim Datenbankzugriff.", se);
            return "Fehler beim Datenbankzugriff.";
        } catch (ContactControlException ce) {
            return ce.getMessage();
        }
    }

    /**
     * Startet einen Postversand an eine Adressauswahl im Rahmen des optimierten
     * Postversands.
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, MailBatch batch) {
        return runWinCommand(postOptimizedCommand + ' ' + createXML(action, batch));
    }

    /**
     * Startet einen Postversand an eine Einzeladresse
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, Address address, String serverurl, String username, String password) {
        try {
            //TODO: serverurl in XML schreiben
            return (handledSingleFaxAsEMail(action, address))
                ? null
                : runWinCommand(postCommunicationCommand + ' ' + createXML(action, address));
        } catch (ContactDBException se) {
            ActionManager.logger.log(Level.WARNING, "Fehler beim Datenbankzugriff.", se);
            return "Fehler beim Datenbankzugriff.";
        } catch (ContactControlException ce) {
            return ce.getMessage();
        }
    }

    /**
     * Startet einen Postversand an eine Adressauswahl im Rahmen des optimierten
     * Postversands.
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, MailBatch batch, String serverurl, String username, String password) {
        //TODO: serverurl in XML schreiben
        return runWinCommand(postOptimizedCommand + ' ' + createXML(action, batch));
    }

    private String runEMail(String mailAddress) {
//      String cmd = "rundll32 url.dll,MailToProtocolHandler "+ mailAddress;

        String mailCommand = ConfigManager.getPreferences().get(Key.EMAIL_COMMAND.toString(), "rundll32 url.dll,FileProtocolHandler mailto:{0}");

        String cmd = MessageFormat.format(mailCommand, new Object[] { mailAddress });
        return runWinCommand(cmd);
    }

    private String runPhoneCall(String phoneNumber) {
        String phoneCommand = ConfigManager.getEnvironment().get(Key.PHONE_COMMAND);
        if (phoneCommand == null || phoneCommand.trim().length() == 0)
            return "kein Telefonie-Kommando konfiguriert\n\n(Eintrag "
            	+ Key.PHONE_COMMAND + " mit {0} als Platzhalter f�r die Nummer)";

        String cmd = MessageFormat.format(phoneCommand, new Object[] { phoneNumber });
        return runWinCommand(cmd);
    }
    
    protected String runWinCommand(String cmd) {
        try {
            ActionManager.logger.fine("[WindowsApplicationStarter] Kommando: " + cmd);
            Process p = Runtime.getRuntime().exec(cmd);
        } catch (IOException ioe) {
            //ActionManager.logger.log(Level.WARNING, "Fehler beim Starten des Programms", ioe);
            logger.warning("Fehler beim Starten des Programms\r\n\n \""+cmd+"\"", ioe);
            return "Fehler beim Starten des Programms";
        }
        return null;
    }

    protected boolean handledSingleFaxAsEMail(String action, Address address)
        throws ContactDBException, ContactControlException {
        boolean useOfficeFax = POST_FAX_OFFICE.equalsIgnoreCase(action);
        if (POST_FAX.equalsIgnoreCase(action)) {
            String fax = address.getFaxDienstlich();
            if (fax != null && fax.trim().length() > 0)
                useOfficeFax = true;
            else {
                fax = address.getFaxPrivat();
                if (fax == null || fax.trim().length() == 0)
                    return false;
            }
        } else if ((!useOfficeFax) && !(POST_FAX_PRIVATE.equalsIgnoreCase(action)))
            return false;

        boolean result = false;
        String pattern = ConfigManager.getEnvironment().get(Key.SINGLE_FAX_AS_EMAIL);
        if (pattern != null && pattern.length() > 0)
            try {
                String fax = useOfficeFax ? address.getFaxDienstlich() : address.getFaxPrivat();
                pattern = MessageFormat.format(pattern, new Object[] { normalizeFaxForMail(fax)});
                ActionManager.logger.fine("Single Fax (" + fax + ") to E-Mail " + pattern);
                String returns = runEMail(pattern);
                if (returns != null && returns.length() > 0)
                    throw new ContactControlException(ContactControlException.EX_EMAIL_SEND_ERROR, returns);
                result = true;
            } catch (IllegalArgumentException iae) {
                ActionManager.logger.log(Level.WARNING, "E-Mail-Muster f�r Fax fehlerhaft.", iae);
            }
        return result;
    }

    protected String normalizeFaxForMail(String fax) {
        StringBuffer result = new StringBuffer();
        if (fax != null) {
            fax = fax.trim();
            for (int i = 0; i < fax.length(); i++) {
                char c = fax.charAt(i);
                if (result.length() == 0 && c == '+')
                    result.append("00");
                else if (Character.isDigit(c))
                    result.append(c);
            }
        }
        return result.toString();
    }

}
