/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author niko
 *
 */
public class ExtendedDataContainer extends JPanel
{
  public static final int TYPE_TABS = 0;
  public static final int TYPE_LIST = 1;
  public static final int TYPE_AUTO = 2;
  
  private ArrayList m_oExtendedDataItemModels;
  private int       m_iType;
  private ExtendedDataCollection m_oContainer;
  private int       m_iMaxItemsInListForTabMode;
  private JComboBox m_oTypeComboBox;
  private JPanel    m_oContainerPanel;
  private JPanel    m_oComboPanel;
  private JPanel    m_oActionPanel;
  private JLabel    m_oComboDescriptionLabel;
  private String    m_sComboDescription;
  private Color     m_oContainerBackgroundColor;
  private Color     m_oComboColor;
  private Font m_oFont;
  private Preferences m_oPreferences;
  private String m_sPreferencesKey;

  
  
  public ExtendedDataContainer(int initialType)
  {
    this(null, String.valueOf(initialType));
  }
  
  public ExtendedDataContainer(Preferences prefs, String prefKey)
  {
    super();
    m_oPreferences = prefs;
    m_sPreferencesKey = prefKey;
    
    if (m_oPreferences != null)
    {
      m_iType = m_oPreferences.getInt(m_sPreferencesKey, ExtendedDataContainer.TYPE_LIST);
    }
    else
    {
      m_iType = ExtendedDataContainer.TYPE_LIST;
      if (prefKey != null)
      {
        if (prefKey instanceof String)
        {
          try
          {
            m_iType = Integer.parseInt(prefKey);
          }
          catch(NumberFormatException nfe)
          {            
          }
        }
      }
    }
    
    m_oContainerPanel = null;
    m_iMaxItemsInListForTabMode = 10;
    m_oExtendedDataItemModels = new ArrayList();
    //m_iType = initialtype;
    m_oContainerBackgroundColor = Color.WHITE;
    m_oComboColor = Color.LIGHT_GRAY;
    m_oFont = new Font(null, Font.PLAIN, 12);
    m_sComboDescription = "Anzeigetyp: ";
    
    this.setLayout(new BorderLayout());
    
    m_oActionPanel = new JPanel();
    m_oActionPanel.setLayout(new BorderLayout());
    
    m_oComboPanel = new JPanel();
    m_oComboPanel.setLayout(new BorderLayout());
    
    m_oTypeComboBox = new JComboBox();
    m_oTypeComboBox.addItem(getTypeName(TYPE_TABS));
    m_oTypeComboBox.addItem(getTypeName(TYPE_LIST));
    m_oTypeComboBox.addItem(getTypeName(TYPE_AUTO));
    m_oTypeComboBox.setSelectedIndex(m_iType);
    m_oTypeComboBox.addActionListener(new combobox_type_changed());
    m_oTypeComboBox.setFont(m_oFont);

    m_oComboDescriptionLabel = new JLabel(m_sComboDescription);
    m_oComboDescriptionLabel.setFont(m_oFont);
    m_oComboPanel.add(m_oComboDescriptionLabel, BorderLayout.WEST);
    m_oComboPanel.add(m_oTypeComboBox, BorderLayout.CENTER);
    m_oActionPanel.add(m_oComboPanel, BorderLayout.WEST);

    this.add(m_oActionPanel, BorderLayout.SOUTH);
    
    this.setBackground(m_oContainerBackgroundColor);
    m_oActionPanel.setBackground(m_oContainerBackgroundColor);
    m_oComboPanel.setBackground(m_oContainerBackgroundColor);
    m_oTypeComboBox.setBackground(m_oComboColor);    
    
//    createCollection();
  }

  public void setFont(Font font)
  {
    if (m_oContainer != null) m_oContainer.setFont(font);
  }
  
  
  private String getTypeName(int type)
  {
    switch(type)
    {
      case(TYPE_TABS):
        return("Tabulatoren");

      case(TYPE_LIST):
        return("Liste");
      
      case(TYPE_AUTO):
        return("automatisch");
      
      default:
        return("unbekannt");
    }
  }

  private void createCollection()
  {
    //System.out.println("createCollection()"); 
    if (m_oContainerPanel != null) m_oContainerPanel.setVisible(false);
    m_oContainerPanel = createContainerPanel(m_iType);  
    m_oContainerPanel.setBackground(m_oContainerBackgroundColor);        
    this.add(m_oContainerPanel, BorderLayout.CENTER);  
    
    m_oContainerPanel.setVisible(true);
    m_oContainerPanel.revalidate();
  }



  private JPanel createContainerPanel(int type)
  {
    switch(type)
    {
      case(TYPE_TABS):
        m_oContainer = new ExtendedDataCollectionForTabs();
      break;

      case(TYPE_LIST):
        m_oContainer = new ExtendedDataCollectionForList();
      break;
      
      case(TYPE_AUTO):
        if (m_oExtendedDataItemModels.size() > m_iMaxItemsInListForTabMode)
        {
          m_oContainer = new ExtendedDataCollectionForList();
        }
        else
        {
          m_oContainer = new ExtendedDataCollectionForTabs();
        }
      break;
      
      default:
      break;
    }

    if (m_oContainerPanel != null) m_oContainerPanel.setBackground(m_oContainerBackgroundColor);        
    JPanel containerpanel = m_oContainer.createContainer(m_oExtendedDataItemModels); 
    m_oContainer.setBackgroundColor(m_oContainerBackgroundColor);
    return(containerpanel);
  }


  public void addExtendedDataItemModel(ExtendedDataItemModel item)
  {
    m_oExtendedDataItemModels.add(item);
  }

  public void removeExtendedDataItemModel(ExtendedDataItemModel item)
  {
    m_oExtendedDataItemModels.remove(item);
  }

  public int getNumberOfExtendedDataItemModels()
  {
    return m_oExtendedDataItemModels.size();
  }

  
  public void realize()
  {
    createCollection();
  }

  private class combobox_type_changed implements ActionListener
  {
    public void actionPerformed(ActionEvent e)      
    {
      m_iType = m_oTypeComboBox.getSelectedIndex();
      createCollection();     
      if (m_oPreferences != null) m_oPreferences.putInt(m_sPreferencesKey, m_iType);
    } 
  }
   
  public void setContainerBackgroundColor(Color col)
  {
    m_oContainerBackgroundColor = col;
    this.setBackground(m_oContainerBackgroundColor);
    m_oActionPanel.setBackground(m_oContainerBackgroundColor);
    m_oComboPanel.setBackground(m_oContainerBackgroundColor);    
    if (m_oContainerPanel != null) m_oContainerPanel.setBackground(m_oContainerBackgroundColor);
  }

  public void setComboBoxColor(Color col)
  {
    m_oComboColor = col;
    m_oTypeComboBox.setBackground(m_oComboColor);    
  }

  public void setComboBoxDescription(String text)
  {
    m_sComboDescription = text + " ";
    m_oComboDescriptionLabel.setText(m_sComboDescription);
  }
  
}
