package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.dialogs.DoubletSearchDialog;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Creates new Contact and imports contact data to it.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ImportAction extends AbstractGUIAction {

    private static final long   serialVersionUID    = 8122860390409416450L;
    private static final TarentLogger logger = new TarentLogger(ImportAction.class);
    private GUIListener guiListener;
    // Safe Thread invocation + better Performance: 
    //   Runnable interface is better than Thread-empty-reference: 
    //   the AWT-Thread should be released before a new thread instance is created
    private Runnable importExecutor;
    private ControlListener controlListener; 

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null) {
            //IMPORTANT: immediate AWT-Thread release
            SwingUtilities.invokeLater(importExecutor);
        } else logger.warning("Import action not initialized");
    }
    
    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        controlListener = ApplicationServices.getInstance().getMainFrame();
        importExecutor = new Runnable(){// define reusable code:
            public void run() {
                boolean canceled = !guiListener.userRequestImportVcardIntoCurrentAddress();
                if(canceled) {
                    logger.info("[!] import vcard canceled");
                    controlListener.setDisplayedAddress(null);
                    guiListener.userRequestNotEditable(GUIListener.DO_DISCARD);
                    return;
                }
                DoubletSearchDialog dialog = guiListener.guiRequestCheckForDouble(false, true);
                String status = dialog == null ? DoubletSearchDialog.STATUS_SAVE : dialog.getStatus();
            	
                // guiListener.userRequestNotEditable(GUIListener.DO_SAVE);
            }
        };
    }
}