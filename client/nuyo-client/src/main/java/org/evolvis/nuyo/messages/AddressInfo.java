/* $Id: AddressInfo.java,v 1.4 2007/08/30 16:10:33 fkoester Exp $
 * Created on 16.10.2003
 */
package org.evolvis.nuyo.messages;

import java.util.EventObject;

import org.evolvis.nuyo.db.Address;


/**
 * Dies ist die Basisklasse f�r Informationsereignisse, die die aktuelle
 * Adresse betreffen.
 *  
 * @author mikel
 */
public class AddressInfo extends EventObject implements InfoEvent {
    /*
     * Konstruktoren
     */
    /**
     * @param source Ereignisquelle
     * @param address betroffene Addresse
     */
    public AddressInfo(Object source, Address address) {
        super(source);
        this.address = address;
    }

    /*
     * Getter und Setter
     */
    /**
     * @return neue Adresse
     */
    public Address getAddress() {
        return address;
    }

    /*
     * Klasse Object
     */
    public String toString() {
        return super.toString() + "[address: " + address.getAdrNr() + ']';
    }

    /*
     * gesch�tzte Variablen 
     */
    Address address;
}
