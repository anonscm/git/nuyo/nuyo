/*
 * Created on 20.07.2004
 *
 */
package org.evolvis.nuyo.db.filter;

import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author kleinw
 *
 */
public interface SelectionElement {

    public void includeInMethod(Map map) throws ContactDBException;
    
}
