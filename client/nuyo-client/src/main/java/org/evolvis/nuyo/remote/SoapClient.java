package org.evolvis.nuyo.remote;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch.CHANNEL;
import org.evolvis.nuyo.db.octopus.old.OctopusMailBatch;

import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConstants;

/**
 * @author kleinw
 * 
 * Hier werden die Soapmethoden, die ben?igt werden, implementiert.
 *
 * TODO: Das Initialisieren der Connection erfolgt hier von Hand,
 *       soll jedoch irgendwann ber die OctopusConnectionFactory laufen
 *  
 */
public class SoapClient {

    static private final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private static Logger logger = Logger.getLogger(SoapClient.class.getName());

    
//     static public OctopusConnection getService() {
//         if (_service == null) {
//             _service = OctopusRemoteConnection();
//             _service.setAuthType(OctopusRemoteConnection.AUTH_TYPE_SESSION);
//             _service.setAutoLogin(true);
//         }
//         return _service;
//     }
    
    static public void login() throws ContactDBException {
        try {
            Method.getDefaultConnection().login();
        } catch(OctopusCallException oce) {
            int error = ContactDBException.EX_OCTOPUS; 
            String errMsg = "Serverfehler beim Beziehen von Sessiondaten";
            if (oce.getErrorCode() != null 
                && oce.getErrorCode().startsWith(OctopusConstants.AUTHENTICATION_ERROR_PREFIX)) {                    
                error = ContactDBException.EX_CLIENT_LOGIN_ERROR;
                errMsg = oce.getCauseMessage();
            }
            throw new ContactDBException(error, errMsg, oce);
        }
    }


    static public void logout() throws ContactDBException {
        try {
            Method.getDefaultConnection().logout();
        } catch(OctopusCallException oce) {
            int error = ContactDBException.EX_OCTOPUS; 
            String errMsg = "Serverfehler beim Beenden der Session";
            throw new ContactDBException(error, errMsg, oce);
        }
    }

    static public List getAvailablePreviewRows() throws ContactDBException{
        Method m = new  Method("getAvaiblePreviewFields");
	    List o = (List) m.invoke();
	    return o;
    }

    static public List getCustomPreview(List Fields, String sortField, Boolean sortOrder, Integer offset, Integer limit) throws ContactDBException {
        
        Method m1 = new  Method("getCustomPreview");
	    m1.addParam("fields", Fields );
	    m1.addParam("sortField", sortField );
	    m1.addParam("order", sortOrder);
	    m1.addParam("offset", offset);
	    m1.addParam("limit", limit);
	    
	    return (List) m1.invoke();
    }
    
    static public List getSortedPreviews(List pks,String sortField,Boolean sortOrder) throws ContactDBException{
        Method m1 = new  Method("getSortedPreviews");
	    m1.addParam("pks", pks);
	    m1.addParam("sortField", sortField );
	    m1.addParam("order", sortOrder);
	    return (List) m1.invoke();
    }
    
    static public List getAddressesPreview(Integer userId) throws ContactDBException {
        String addressesPreviewList = null;
        Method m = new Method("getPreviews");
        m.add("userid", userId);
//        m.addParam("includeSubCategories", new Boolean(true), Constants.XSD_BOOLEAN);
//        m.addParam("singleStringResult", new Boolean(true), Constants.XSD_BOOLEAN);
//  
        List tmp = (List)m.invoke();
        return tmp;
    }
    
    static public String getAddressesPreview(Integer category, List subCategories, 
    Boolean intersection, Boolean generalCategory, 
    Boolean includeSubCategories) throws ContactDBException {
        String addressesPreviewList = null;
        Method method = new Method("getPkPreviewList");
        method.add("category", category);
        if (subCategories != null) 
            method.add("subCategories", subCategories);
        if (generalCategory != null) 
            method.add("generalCategory", generalCategory);
        if (intersection != null) 
            method.add("intersection", intersection);
        if (includeSubCategories != null) 
            method.add("includeSubCategories", includeSubCategories);
        method.add("singleStringResult", new Boolean(true));
        addressesPreviewList = (String) method.invoke();
        return addressesPreviewList;
    }

    static public List getAddressesPreviewByPkList(List addressPks)
    throws ContactDBException {
        if (addressPks.size() == 0) 
        return new Vector();
        Method method = new Method("getAddressesPreviewByPkList");
        StringBuffer pks = new StringBuffer();
        for (Iterator it = addressPks.iterator(); it.hasNext();) {
            pks.append(it.next().toString());
            if (it.hasNext()) 
            pks.append(" ");
        }
        method.add("addressPks", pks.toString());
        method.add("includeSubCategories", new Boolean(true));
        method.add("singleStringResult", new Boolean(true));
        String tmp = (String) method.invoke();
        List result = new Vector();
        if (tmp.length() > 0) {
            for (StringTokenizer st = new StringTokenizer(tmp, "^"); st
                .hasMoreTokens();) {
                result.add(st.nextToken());
            }
        }
        return result;
    }

    static public String getStaticData (String userid) throws ContactDBException {
        Method method = new Method("getStaticData");
        method.add("userid", userid);
        method.add("singleStringResult", new Boolean(true));
        String test = (String)method.invoke();
        return test;
    }
    
    static public Map getAddressStandardDataByPk(Integer addressPk)
    throws ContactDBException {
    	
    	Map response = new TreeMap();
    	
        Method method = new Method("getAddressStandardDataByPk");
        method.add("addressPk", addressPk);
        //method.add("singleStringResult", Boolean.FALSE);
        method.invoke();
        //Object addressData = method.invoke();
                
        Map associatedCategories = (Map) method.getORes().getData("associatedCategories");
        response.put("associatedCategories", associatedCategories);

        List associatedSubCategories = (List) method.getORes().getData("associatedSubCategories");
        response.put("associatedSubCategories", associatedSubCategories);

        Object addressData = method.getORes().getData("address");
        
        
        if (addressData instanceof List) {
        
        	/*
        	int counter = 0;
        	System.out.println("Listengr?e:" + ((List)tmp).size());
        	
        	Object temp;
        	for (int i = 0; i<((List)tmp).size(); i++){
        		System.out.print( i + " ");
        		
        		temp = ((List)tmp).get(i);
        		
        		if (temp == null) System.out.print( "NULLWERT !!! ");
				
        		if (temp instanceof String) System.out.print("str-" + temp + " / ");
				else System.out.print(temp + " / ");
        	}
        	System.out.println("");
        	
        	*/
        	response.put("addressData", (List) addressData);
            return response;            
        }
        else {
            List result = new Vector();
            if (addressData instanceof String) {
                StringTokenizer st = new StringTokenizer((String) addressData, "^");
                while (st.hasMoreTokens()) {
                    String token = st.nextToken();
                    if (token.equals(" "))
                        result.add("");
                    else
                        result.add(new String(token));
                }
            }
            response.put("addressData", result);
            
            return response;
        }
    }

    static public List getAddressExtendedDataByPk(Integer addressPk)
    throws ContactDBException {
        List extendedData = null;
        Method method = new Method("getAddressExtendedDataByPk");
        method.add("addressPk", addressPk);
        extendedData = (List) method.invoke();
        return extendedData;
    }

    static public List getAddressProtectedDataByPk(Integer category, 
    Integer addressPk) throws ContactDBException {
        List protectedData = null;
        Method method = new Method("getAddressProtectedDataByPk");
        method.add("category", category);
        method.add("addressPk", addressPk);
        protectedData = (List) method.invoke();
        return protectedData;
    }

    static public List getAddressEmailDataByPk(
    Integer addressPk) throws ContactDBException {
        Method method = new Method("getAddressEmailDataByPk");
        method.add("addressPk", addressPk);
        return (List) method.invoke();
    }

    static public Object getPkPreviewListByFilter(String addressPks, 
    Map filter, Boolean singleStringResult) throws ContactDBException {
        Object preview = null;
        Method method = new Method("getAddressesPreviewByFilter");
        method.add("addressPks", addressPks);
        method.add("filterMap", filter);
        if (singleStringResult.booleanValue()) {
            method.add("singleStringResult", singleStringResult);
        }
        return resultToIntegerList(method.invoke());
    }

    static public String saveAddress(Integer category, List subCategories, 
    List standardData, String note)
    throws ContactDBException {
        String preview = null;
        Method method = new Method("saveAddress");
        method.add("category", category);
        method.add("subCategories", subCategories);
        method.add("standardAddress", standardData);
        method.add("note", note);
        preview = (String) method.invoke();
        return preview;
    }

    static public String updateAddress(Integer categoryPk, Integer addressPk, 
    List subCategories, 
    List standardData, String note)
    throws ContactDBException {
        String preview = null;
        Method method = new Method("updateAddress");
        method.add("categoryPk", categoryPk);
        method.add("addressPk", addressPk);
        method.add("subCategories", subCategories);
        method.add("standardAddress", standardData);
        method.add("note", note);
        preview = (String) method.invoke();
        return preview;
    }

    static public void deleteAddress(Integer categoryPk, Integer addressPk)
    throws ContactDBException {
        Method method = new Method("deleteAddress");
        method.add("addressPk", addressPk);
        method.add("category", categoryPk);
        method.invoke();
    }
    
   
    static public Object getDuplicates(Map parameter, Integer exclude)
        throws ContactDBException {
        Object result = null;
        Method method = new Method("getDublikateSOAP");
        method.add("duplicateFields", parameter);
        method.add("exclude", exclude);
        return method.invoke();
    }

    static public Object getUsers() throws ContactDBException {
        Method method = new Method("getUsersSOAP");
        return method.invoke();
    }
    
    static public String getVersion(String clientVersion)
        throws ContactDBException {
    	String version = null;

        Method method = new Method("getVersionSOAP");
        method.add("clientVersion", clientVersion);
        Map versionmap = (Map) method.invoke();
        version = (String) versionmap.get("version");
    	return version;
    }

    static public List getAddressHistory(Set types, Date start, Date end, 
    Integer addressPk) throws ContactDBException {
        List addressHistory = null;
        Method method = new Method("getHistoryTunedSOAP");
        method.add("types", types);
        method.add("start", new Long(start.getTime()));
        method.add("end", new Long(end.getTime()));
        method.add("adrId", addressPk);
        addressHistory = (List) method.invoke();
        return addressHistory;
    }

    static public List getDispatchOrder(Integer addressPk)
    throws ContactDBException {
        Method method = new Method("getVersandAuftragSOAP");
        method.addParam("mbid", addressPk);
        return (List) method.invoke();
    }
    
    static public void setDispatchOrderIntegerValue(Integer id, String name, Integer value) throws ContactDBException{
    	Method method = new Method("mailBatchSOAP");
    	method.add("function", "setIntValue");
    	method.add("id", id);
    	method.add("name", name);
    	method.add("value", value);
    	method.invoke();
    }

    static public void setDispatchOrderStringValue(Integer id, String name, String value) throws ContactDBException{
    	Method method = new Method("mailBatchSOAP");
    	method.add("function", "setStringValue");
    	method.add("id", id);
    	method.add("name", name);
    	method.add("value", value);
    	method.invoke();
    }
    
    static public Map executeDispatchOrderFunction(String function, 
    Integer dispatchOrderPk, CHANNEL channel) throws ContactDBException {
        Map functionResult = null;
        Method method = new Method("mailBatchSOAP");
        method.add("function", function);
        method.add("id", dispatchOrderPk);
        if (function.equals(OctopusMailBatch.FUNCTION_GETCHANNELCOUNT)) {
            if (channel != null && !CHANNEL.TOTAL.equals(channel)) {
                method.add("channel", channel.getChannelName());
            }
            else {
                method.add("channel", "");
            }
        }
        else if (function.equals(OctopusMailBatch.FUNCTION_SETCHANNELCOUNT)) {
        }
        return (Map) method.invoke();
    }

    /*
     * Auskommentiert bei Octopus-Sicherheits-Action
     * Methode scheint nicht mehr ben?igt zu werden und wird anscheinend durch getCategories(NEW) ersetzt
     */
   /*
    * 
     static public Object getCategoriesFromUser(String userid)
    throws ContactDBException {
        Method method = new Method("getCategoriesFromUser");
        method.add("userid", userid);
        method.add("singleStringResult", new Boolean(true));
        return resultToStringList(method.invoke());
    }
    */
    static public Object getEmailsByPkList(List pks) throws ContactDBException {
        Method method = new Method("getEmailsByPkList");
        StringBuffer sb = new StringBuffer();
        for (Iterator it = pks.iterator();it.hasNext();) {
            sb.append(it.next().toString());
            if (it.hasNext())
                sb.append("|");
        }
        method.add("pkList", sb.toString());
        method.add("singleStringResult", new Boolean(true));
        return resultToStringList(method.invoke());
    }

    /*
     * Auskommentiert beim ?erholen des Octopus (Sicherheits-Update)
     * Methode ist alt und wird wahrscheinlich nicht mehr genutzt
     *   
     */
    
    /*
     * 
    static public Object getSubCategories(Integer category) throws ContactDBException {
        Method method = new Method("getSubCategories");
        method.add("categoryPk", category);
        return resultToStringList(method.invoke());
    }
    */
    
    static public void editSubCategory (Integer category, Integer subCategory, String subCategoryName, String subCategoryDescription) throws ContactDBException {
        logger.log(Level.INFO, "editsub");
                       
        Method method = new Method("modifySubCategory");
        method.add("categoryPk", category);
        method.add("subCategoryPk", subCategory);
        if (subCategoryName != null)
            method.add("subCategoryName", subCategoryName);
        if (subCategoryDescription != null) 
            method.add("subCategoryDescription", subCategoryDescription);
        String tmp = (String)method.invoke();
        if (tmp.length() > 0) 
            throw new ContactDBException(ContactDBException.EX_OCTOPUS, tmp);
    }
    
    private static Object resultToIntegerList(Object preview) {
        if (preview instanceof List) {
            return preview;
        }
        else {
            List tmp = new Vector();
            if (preview instanceof String) {
                StringTokenizer st = new StringTokenizer((String) preview, "^");
                while (st.hasMoreTokens()) {
                    tmp.add(new Integer(st.nextToken()));
                }
            }
            return tmp;
        }
    }

    private static Object resultToStringList(Object preview) {
        if (preview instanceof List) {
            return preview;
        }
        else {
            List tmp = new Vector();
            if (preview instanceof String) {
                StringTokenizer st = new StringTokenizer((String) preview, "^");
                while (st.hasMoreTokens()) {
                    tmp.add(st.nextToken());
                }
            }
            return tmp;
        }
    }

}
