/* $Id: GlobalRoleImpl.java,v 1.4 2006/06/06 14:12:06 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.Collections;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Role;
import org.evolvis.nuyo.db.cache.Cachable;
import org.evolvis.nuyo.db.cache.EntityCache;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;


/**
 *
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class GlobalRoleImpl extends GlobalRoleBean implements Cachable {
	/**Methode um globale Rollen zu holen*/
	public static final String METHOD_GETROLES = "getGlobalRoles";
	/**Methode um die globale Rolle anzulegen oder zu ver�ndern*/
	public static final String METHOD_CREATE_OR_MODIFY_ROLE = "createOrModifyGlobalRole";
	/**Methode um eine globale Rolle zu l�schen*/
	public static final String METHOD_DELETE_ROLE = "deleteGlobalRole";
	
	//	Initialisierung des Objekts zum Holen von Daten 
    static {
        _fetcher = new AbstractEntityFetcher(METHOD_GETROLES, false) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    Role role = new GlobalRoleImpl();
                    ((IEntity)role).populate(values);
                    return (IEntity)role;
                }
            };
    }
	
	/**	Statische Methode zum Beziehen aller GlobalRoles */
	static public Collection getGlobelRoles(ISelection selection) throws ContactDBException {return _fetcher.getEntities(selection);};
    

	public void updateInCache() throws ContactDBException {
        EntityCache cache = _fetcher.getCache();
        cache.store(null, Collections.singletonList(this));
	}

	public void deleteFromCache() throws ContactDBException {
		_fetcher.getCache().delete(_id);		
	}
	
	

	public void populate(ResponseData data) throws ContactDBException {
		_responseData = data;
		setAttribute(KEY_ROLENAME, (String) data.get("Rolename"));
		setAttribute(KEY_DELETE, getBooleanIntAsBoolean((Integer)data.get("AuthDelete")));
		setAttribute(KEY_EDITFOLDER, getBooleanIntAsBoolean((Integer)data.get("AuthEditcategory")));
		setAttribute(KEY_EDITSCHEDULE, getBooleanIntAsBoolean((Integer) data.get("AuthEditschedule")));
		setAttribute(KEY_EDITUSER, getBooleanIntAsBoolean((Integer) data.get("AuthEdituser")));
		setAttribute(KEY_REMOVEFOLDER, getBooleanIntAsBoolean((Integer) data.get("AuthRemovecategory")));
		setAttribute(KEY_REMOVEUSER, getBooleanIntAsBoolean((Integer) data.get("AuthRemoveuser")));
		setAttribute(KEY_REMOVESCHEDULE, getBooleanIntAsBoolean((Integer) data.get("AuthRemoveschedule")));
		setAttribute(KEY_ROLE_TYPE, (Integer) data.get("Roletype"));
		
		_id = (Integer)data.get("Pk");
	}
	
	private String getBooleanIntAsString(Integer source){
		if(source.intValue()==1){
			return Boolean.TRUE.toString();
		}else{
			return Boolean.FALSE.toString();
		}
	}
	
	private Boolean getBooleanIntAsBoolean(Integer source){
		if(source.intValue()==1){
			return Boolean.TRUE;
		}else{
			return Boolean.FALSE;
		}
	}

	public void validate() throws ContactDBException {
		//Nichts f�rs erste
	}

	public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_OR_MODIFY_ROLE);
        Object tmp = _method.invoke();
        if(tmp instanceof Integer)_id=(Integer)tmp;
        else if(tmp instanceof String)_id=Integer.valueOf((String)tmp);
        else throw new ContactDBException(ContactDBException.EX_INVALID_USER, "Ung�ltige Daten �ber die Role zur�ckerhalten!");
	}

	public void delete() throws ContactDBException {
		Method method = new Method(METHOD_DELETE_ROLE);
		method.addParam("id", _id);
		method.invoke();
	}

	public void rollback() throws ContactDBException {
		populate(_responseData);
	}
}
