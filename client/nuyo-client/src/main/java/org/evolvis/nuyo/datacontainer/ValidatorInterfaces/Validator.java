/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ValidatorInterfaces;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObject;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface Validator extends DataContainerObject
{
  public Validator cloneValidator();
  public void init();
  public void dispose();
  
  public boolean addParameter(String key, String value);
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor();

  public boolean canValidate(Class data);

  public boolean validate(Object data);
  public Object correct(Object data);

  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();

  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);      
  public void fireTarentGUIEvent(TarentGUIEvent e);
}
