package org.evolvis.nuyo.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;

import org.evolvis.nuyo.db.octopus.AddressPreview;


public class MassAssignmentErrorMsgPanel extends JPanel {
	
	private JLabel message = new JLabel(
			"<html>Bei der Massenzuweisung traten Konflikte auf," +
			" da Sie auf einigen <br>Adressen oder Kategorien nicht über die nötigen Berechtigungen " +
			"<br>für diese Aktion verfügen.</html>");
	private JLabel headerblockedAddTable = new JLabel("Adressen, die nicht zugewiesen werden konnten:");
	private JLabel headerblockedRemoveTable = new JLabel("Adressen, deren Zuweisungen nicht aufgehoben werden konnten:");
	private JLabel question = new JLabel("<html>Möchten Sie die blockierten Zuweisungen ignorieren und " +
			"<br>die Aktion auf den restlichen Adressen durchführen?</html>");
	private JTable blockedAddTable;
	private JTable blockedRemoveTable;
	
	private Insets headerInsets = new Insets(20,0,0,0);
	private Insets tableInsets = new Insets(5,0,0,0);


	GridBagConstraints constraints = new GridBagConstraints();
	JScrollPane scrollPaneAddTable;
	JScrollPane scrollPaneRemoveTable; 
	
	public MassAssignmentErrorMsgPanel(List notAssingableAddresses, List notDeassingableAddresses){
		
		this.setLayout(new GridBagLayout());
		
		blockedAddTable = null;
		blockedRemoveTable = null;
		
		if (notAssingableAddresses != null && notAssingableAddresses.size() != 0){
			
			Object[][] data = new Object[notAssingableAddresses.size()][2];
			
			AddressPreview currentAddress;
			for (int i = 0; i < notAssingableAddresses.size(); i++){
				currentAddress = (AddressPreview) notAssingableAddresses.get(i);
			
				data[i][0] = currentAddress.getAddressPk();
				data[i][1] = currentAddress.getAddressPreview();
			}
			blockedAddTable = new JTable(data, new Object[]{"Id", "Adresslabel"});
			TableColumn col = blockedAddTable.getColumnModel().getColumn(0);
			col.setMaxWidth(50);
			blockedAddTable.setEnabled(false);
			blockedAddTable.setPreferredScrollableViewportSize(new Dimension(400,200));
			scrollPaneAddTable = new JScrollPane(blockedAddTable);
		}
		
		if (notDeassingableAddresses != null && notDeassingableAddresses.size() != 0){
			
			Object[][] data = new Object[notDeassingableAddresses.size()][2];
			
			AddressPreview currentAddress;
			for (int i = 0; i < notDeassingableAddresses.size(); i++){
				currentAddress = (AddressPreview) notDeassingableAddresses.get(i);
				
				data[i][0] = currentAddress.getAddressPk();
				data[i][1] = currentAddress.getAddressPreview();
			}
			blockedRemoveTable = new JTable(data, new Object[]{"Id", "Adresslabel"});
			TableColumn col = blockedRemoveTable.getColumnModel().getColumn(0);
			col.setMaxWidth(50);
			blockedRemoveTable.setEnabled(false);
			blockedRemoveTable.setPreferredScrollableViewportSize(new Dimension(400,200));
			scrollPaneRemoveTable = new JScrollPane(blockedRemoveTable);
		}
		
		constraints.anchor = GridBagConstraints.WEST;
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.insets = headerInsets;
		
		this.add(message, constraints);
		if (blockedAddTable != null){
			constraints.gridy++;
			this.add(headerblockedAddTable, constraints);
			constraints.gridy++;
			constraints.insets = tableInsets;
			this.add(scrollPaneAddTable, constraints);
		}
		if (blockedRemoveTable != null){
			constraints.gridy++;
			constraints.insets = headerInsets;
			this.add(headerblockedRemoveTable, constraints);
			constraints.gridy++;		
			constraints.insets = tableInsets;
			this.add(scrollPaneRemoveTable, constraints);
		}
		constraints.insets = headerInsets;
		constraints.gridy++;		
		constraints.anchor = GridBagConstraints.CENTER;
		this.add(question, constraints);
	}

}
