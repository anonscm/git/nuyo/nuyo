/* $Id: AddressShownInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 16.10.2003
 */
package org.evolvis.nuyo.messages;

import org.evolvis.nuyo.db.Address;

/**
 * Dieses Ereignis zeigt an, dass eine neue Adresse aktuelle Adresse ist.
 * 
 * @author mikel
 */
public class AddressShownInfo extends AddressInfo {
    /*
     * Konstruktoren
     */
    /**
     * @param source Ereignisquelle
     * @param address betroffene Addresse
     */
    public AddressShownInfo(Object source, Address address) {
        super(source, address);
    }
}
