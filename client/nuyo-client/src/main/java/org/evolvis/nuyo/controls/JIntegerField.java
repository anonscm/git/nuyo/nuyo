/*
 * Created on 23.06.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
 
/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 * @author niko
 *
 */
public class JIntegerField extends JTextField
{        
  int maxchars;  
      
  public JIntegerField(int cols)
  {
    super(cols);
    maxchars = 0;
  }

  public JIntegerField(int preset, int cols)
  {
    super(cols);
    this.setText(String.valueOf(preset));
    maxchars = 0;
  }

  public JIntegerField()
  {
    super();
    maxchars = 0;
  }

  public int getIntegerValue()
  {
    return Integer.parseInt(this.getText());
  }

  public void setIntegerValue(int value)
  {
    this.setText(Integer.toString(value));
  }


  protected Document createDefaultModel() 
  {
    return new IntegerDocument();
  }

  public class IntegerDocument extends PlainDocument
  {
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
    {
      if (str == null) 
      {
        return;
      }
         
      String inttext = ""; //$NON-NLS-1$
      for (int i = 0; i < str.length(); i++) 
      {
        if (Character.isDigit(str.charAt(i)))
        {
          inttext = inttext + (str.charAt(i));
        }
      }
      super.insertString(offs, inttext, a);          
    }
  }
}
