/* $Id: MailTransport.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink and Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.List;

/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public interface MailTransport {
	/**
	 * Gibt eine Liste aller  eMails zur�ck
	 * als MailWrite-Instanz zur�ck.
	 *
	 * @author HeikoFerger
	 * @return
	 * @throws ContactDBException
	 */
	public List getMessageList(String filter) throws ContactDBException;

	/**
	 * Gibt eine Liste aller  eMails zur�ck
	 * als MailWrite-Instanz zur�ck.
	 *
	 * @author HeikoFerger
	 * @return
	 * @throws ContactDBException
	 */
	public List getAll() throws ContactDBException;
	

	/**
	 * Gibt eine Liste der versendeten eMails
	 * als MailWrite-Instanz zur�ck.
	 * 
	 * @return
	 * @throws ContactDBException
	 */
	public List getSend() throws ContactDBException;

	/**
	 * Gibt eine Liste nicht versendeten eMails
	 * als MailWrite-Instanz zur�ck.
	 * 
	 * @return
	 * @throws ContactDBException
	 */
	public List getUnSend() throws ContactDBException;


	/**
	 * Gibt eine Liste der Entw�rfe von EMails (New)
	 * als MailWrite-Instanz zur�ck.
	 * 
	 * @return
	 * @throws ContactDBException
	 */
	public List getDrafts() throws ContactDBException;


	/**
	 * Gibt eine neue eMail zur�ck.
	 * 
	 * @return
	 * @throws ContactDBException
	 */
	public MailWrite newMessage() throws ContactDBException;
}
