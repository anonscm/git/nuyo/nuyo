/*
 * Created on 23.02.2005
 */
package org.evolvis.nuyo.util;

import java.util.Date;

/**
 * Diese Klasse repr�sentiert einen Datumsbereich und bietet
 * Hilfsmethoden zur Verabeitung von Datumsbereichen an.
 * Achtung: Das Anfangs/Enddatum muss nicht gesetzt sein!
 * 
 * @author simon
 */

public class DateRange {
    
    private Date _StartDate;
    private Date _EndDate;
    
    public DateRange(Date StartDate, Date EndDate) {
        _StartDate = StartDate;
        _EndDate = EndDate;
        
    }
    
    public boolean isInDateRange(DateRange GreaterOrEqualDateRange){
        if ( _StartDate.compareTo(GreaterOrEqualDateRange.getStartDate()) > 0 &&
             _EndDate.compareTo(GreaterOrEqualDateRange.getEndDate()) < 0 ){
            return false;
        }
        return false;
    }

    public boolean containsDate(Date aDate){
        if (_EndDate != null){
            return ( _StartDate.before(aDate) && _EndDate.after(aDate));
        }else{
            return _StartDate.before(aDate);
        }
    }
    
    
    //Getter/Setter
    public Date getEndDate() {
        return _EndDate;
    }
    public void setEndDate(Date endDate) {
        if (endDate.after(_StartDate)) _EndDate = endDate;
    }
    public Date getStartDate() {
        return _StartDate;
    }
    public void setStartDate(Date startDate) {
        if (startDate.before(_EndDate)) _StartDate = startDate;
    }
    public boolean hasEndDate(){
        return (_EndDate != null);
    }
    
    public boolean isDefined(){
        return (_StartDate != null);
    }
}
