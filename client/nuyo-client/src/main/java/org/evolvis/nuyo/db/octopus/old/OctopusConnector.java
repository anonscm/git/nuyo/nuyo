/*
 * $Id: OctopusConnector.java,v 1.10 2007/03/30 13:04:26 nils Exp $
 * 
 * tarent-octopus, Webservice Data Integrator and Applicationserver Copyright
 * (C) 2002 tarent GmbHi
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright interest in the program
 * 'tarent-octopus' (which makes passes at compilers) written by Thomas Fuchs,
 * Michael Klink. signature of Elmar Geese, 1 June 2002 Elmar Geese, CEO tarent
 * GmbH
 */
package org.evolvis.nuyo.db.octopus.old;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.remote.Method;


/**
 * Diese Klasse dient als Verbindungsst�ck zwischen dem ContactClient und dem
 * contact-Modul des Octopus-Frameworks.
 *
 * @deprecated Abgel�st von neuer Beanbasierter Implementierung
 * @author mikel
 */
public class OctopusConnector {


                    
    /*
     * Konstruktoren **************************************************
     */
    /**
     * Dieser Konstruktor legt die �bergebenen Daten ab.
     * 
     * @param netBaseUrl
     *                    die URL, auf der der Octopus-Server horcht.
     * @param schemaBaseUrl
     *                    die URL,
     */

    public OctopusConnector(String netBaseUrl, String schemaBaseUrl,
            String module) {
        this.netBaseUrl = netBaseUrl;
        this.schemaBaseUrl = schemaBaseUrl;
        this.module = module;
        this.netSessionUrl = null;
        OctopusConnector.user = null;
        this.password = null;

    }

    OctopusSSL sslObject = null;

    URL workUrl = null;

    boolean connectIt = true;

    int connectionTicker = 0;

    // auf "true" gesetzt, werden SOAPRequests und SOAPResults mitgeloggt.
    private boolean logSoap = false;

    int index = 0;

    /*
     * Getter und Setter **********************************************
     */
    /**
     * Liefert das Octopus-Modul, auf das zugegriffen wird.
     * 
     * @return das Octopus-Modul.
     */
    public String getModule() {
        return module;
    }

    /**
     * Liefert die Basis-URL, auf die der Octopus-Server horcht.
     * 
     * @return Basis-URL des Octopus-Servers.
     */
    public String getNetBaseUrl() {
        return netBaseUrl;
    }

    /**
     * Liefert das Passwort des verbundenen Benutzers.
     * 
     * @return Passwort.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Liefert die Basis-URL des Namensschemas, in dem die Methoden definiert
     * sind.
     * 
     * @return Basis-URL des SOAP-Namensschemas.
     */
    public String getSchemaBaseUrl() {
        return schemaBaseUrl;
    }

    /**
     * Liefert die ID des verbundenen Benutzers.
     * 
     * @return ID.
     */
    public String getUser() {
        return user;
    }

    /**
     * Setzt das Passwort des zu verbindenen Benutzers. Jeder Aufruf beendet
     * eine gegebenenfalls vorhandene Session.
     * 
     * @param password
     *                    Das zu setzende Passwort.
     */
    public void setPassword(String password) {
        this.password = password;
        try {
            closeSession();
        } catch (ContactDBException ioe) {
        }
    }

    /**
     * Setzt die ID des zu verbindenen Benutzers. Jeder Aufruf beendet eine
     * gegebenenfalls vorhandene Session.
     * 
     * @param user
     *                    Die zu setzende ID.
     */
    public void setUser(String user) {
        OctopusConnector.user = user;
        try {
            closeSession();
        } catch (ContactDBException ioe) {
        }
    }

    /*
     * �ffentliche Zugriffsmethoden ***********************************
     */
    /**
     * @deprecated use package de.tarent.contact.remote instead
     */
    public void openSession() throws ContactDBException {
        //doNothing any More
        //         closeSession();
        //         netSessionUrl = null;
        //         try {
        //             createSessionUrl();
        //         } catch (IOException ioe) {
        //             throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Fehler beim holen der neuen Session URL");
        //         }
        
        //         doLogin();
    }

    /**
     * @deprecated use package de.tarent.contact.remote instead
     */
    public void closeSession() throws ContactDBException {
        if (isSessionOpen()) try {
            doLogout();
        } finally {
            netSessionUrl = null;
        }
    }

    /**
     * @deprecated use package de.tarent.contact.remote instead
     */
    public boolean isSessionOpen() {
        return netSessionUrl != null;
    }

    /**
     * Holt alle vorhandenen Bundesl�nder im SOAP - Stil aus dem tc.contact
     * Modul
     */
    public SortedMap getBundeslaender() throws ContactDBException {
            assertLogin();
            return doGetBundeslaender();
    }

    /**
     * Holt alle vorhandenen Anreden im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getAnreden() throws ContactDBException {
            assertLogin();
            return doGetAnreden();
    }

    /**
     * Holt alle vorhandenen Landeskennzahlen im SOAP - Stil aus dem tc.contact
     * Modul
     */
    public SortedMap getLKZ() throws ContactDBException {
            assertLogin();
            return doGetLKZ();
    }

    /**
     * Holt alle vorhandenen l�nder im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getLaender() throws ContactDBException {
            assertLogin();
            return doGetLaender();
    }

    /**
     * Holt einen bestimmten Datensatz im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getAddress(int id, String verteiler)
            throws ContactDBException {
            assertLogin();
            return doGetAddress(id, verteiler);
    }

    /**
     * Holt einen den Standartverteiler (Userabh�ngig) im SOAP - Stil aus dem
     * tc.contact Modul
     */
    public SortedMap getStandardVerteiler(String user)
            throws ContactDBException {
            assertLogin();
            return doGetStandardVerteiler(user);
    }

    /**
     * Holt den Allgemeinen Verteiler im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getAllgemeinenVerteiler() throws ContactDBException {
            assertLogin();
            return doGetAllgemeinenVerteiler();
    }

    /**
     * Setzt die aktuelle Verteilerbelegung zu einer Addresse zur�ck
     */
    public void setVerteilerSelected(SortedMap m) {
        selectedVerteiler = m;
    }

    /**
     * Setzt die aktuelle Verteilerbelegung zu einer Addresse zur�ck
     */
    public SortedMap getVerteilerSelected() {
        return selectedVerteiler;
    }

    /**
     * Holt alle Verteilergruppen die zur Zeit vorhanden sind im SOAP - Stil aus
     * dem tc.contact Modul
     */
    public SortedMap getAllVerteilergruppen() throws ContactDBException {
            assertLogin();
            return doGetAllVerteilergruppen();
    }

    /**
     * Holt Verteiler im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getVerteiler(String grp) throws ContactDBException {
            assertLogin();
            return doGetVerteiler(grp);
    }

    /**
     * Holt Verteiler mit Beschreibung im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getVerteilerExt(String grp) throws ContactDBException {
            assertLogin();
            return doGetVerteilerExt(grp);
    }

    /**
     * Holt alle User im SOAP - Stil aus dem tc.contact Modul
     */
    public SortedMap getUsers() throws ContactDBException {
            assertLogin();
            return doGetUsers();
    }

    /**
     * Holt Address-ID's zur aktuellen Verteilergruppe und aktueller
     * Verteilerauswahl.
     * 
     * @param gruppe
     * @param verteiler
     * @return SortedMap
     */
    public SortedMap getChosenAddresses(String gruppe, SortedMap verteiler,
            boolean allgemein, boolean schnitt, int count, int packageCount)
            throws ContactDBException {
            assertLogin();
            SortedMap m = doGetChosenAddresses(gruppe, verteiler, allgemein,
                    schnitt, count, packageCount);

            return m;
    }

    /**
     * Erstellt einen neuen Versandauftrag.
     * 
     * @return SortedMap
     */
    public SortedMap getVersandAuftraege(String user) throws ContactDBException {
            assertLogin();
            return doGetVersandAuftraege(user);
    }


    /**
     * Diese Methode erzeugt zu einem Wert einen Matchcode passend zur
     * �bergebenen Spalte. Erlaubtes Jokerzeichen ist '*'. <br>
     * Hierbei sollte eigentlich die Stored Procedure buildMatchcode genutzt
     * werden, deren Funktionalit�t hier nachgebildet wird.
     * 
     * @param value
     *                    der Wert, zu dem ein Matchcode erzeugt werden soll.
     * @return der erzeugte Matchcode.
     */
    protected String getMatchcode(String value) {
        if (value == null || value.length() == 0) return "";
        
        value = value.toUpperCase().replaceAll("Ä", "AE").replaceAll("Ö", "OE")
        .replaceAll("Ü", "UE").replaceAll("SS", "S").replaceAll("CK", "K")
        .replaceAll("DT", "D").replaceAll("-", "").replaceAll("ß", "S");
        
        
        
        StringBuffer buffer = new StringBuffer(value);
        char lastChar = 0;
        int index = 0;
        while (index < buffer.length()) {
            char thisChar = buffer.charAt(index);
            if ((thisChar != lastChar)
                    && ((thisChar == '*') || ((thisChar >= 'A') && (thisChar <= 'Z')))) {
                lastChar = thisChar;
                index++;
            } else
                buffer.deleteCharAt(index);
        }

        return buffer.toString();
    }


    /**
     * Diese Methode l�scht eine Verteilergruppe.
     * 
     * @param key
     *                    der Verteilergruppenschl�ssel.
     */
    public Map deleteVerteilergruppe(String key) throws ContactDBException {
            assertLogin();
            Map map = doDeleteVerteilergruppe(key);
            return map;
    }

    /**
     * Diese Methode erzeugt einen Verteiler.
     * 
     * @param gruppe
     *                    die Verteilergruppe.
     * @param key
     *                    der Verteilerschl�ssel.
     * @param name1
     *                    der Verteilerklartext.
     * @param name2
     *                    der Verteilerklartext.
     * @param name3
     *                    der Verteilerklartext.
     */
    public Map createVerteiler(String gruppe, String key, String name1,
            String name2, String name3) throws ContactDBException {
            assertLogin();
            Map map = doCreateVerteiler(gruppe, key, name1, name2, name3);
            return map;
    }

    /**
     * Diese Methode l�scht einen Verteiler.
     * 
     * @param key
     *                    der Verteilerschl�ssel.
     */
    public Map deleteVerteiler(String verteilergruppe, String key)
            throws ContactDBException {
            assertLogin();
            Map map = doDeleteVerteiler(verteilergruppe, key);
            return map;

    }

    /**
     * Diese Methode erzeugt einen neuen Benutzer.
     * 
     * @param userId
     *                    die Benutzerkennung.
     * @param lastName
     *            der Nachname des Benutzers.
     * @param firstName
     *            der Vorname des Benutzers.
     * @param special
     *                    legt fest, ob der Benutzer zur Gruppe der Standardbenutzer
     *                    oder der besonderen Benutzer geh�ren soll.
     * @param admin
     *                    legt fest, ob der Benutzer Administrator seien soll.
     */
    public Map createUser(String userId, String lastName, String firstName, String password,
            boolean special, boolean admin) throws ContactDBException {
            assertLogin();
            Map map = doCreateUser(userId, lastName, firstName, password, special, admin);
            return map;
    }


    /**
     * Diese Methode holt das Special-Flag zu einem Benutzer.
     * 
     * @param userId
     *                    die Benutzerkennung.
     */
    public Map getUserSpecial(String userId) throws ContactDBException {
            assertLogin();
            Map map = doGetUserSpecial(userId);
            return map;
    }

    /**
     * Diese Methode holt das Admin-Recht zu einem Benutzer.
     * 
     * @param userId
     *                    die Benutzerkennung.
     */
    public Map getUserAdmin(String userId) throws ContactDBException {
            assertLogin();
            Map map = doGetUserAdmin(userId);
            return map;
    }

    /**
     * Diese Methode ordnet einem Benutzer eine Verteilergruppe zu.
     * 
     * @param verteilergruppe
     *                    der Verteilergruppenschl�ssel.
     * @param userId
     *                    die Benutzer-ID.
     */
    
    /*
    public Map showVerteilergruppeToUser(String verteilergruppe, String userId)
            throws ContactDBException {
            assertLogin();
            return doShowVerteilergruppeToUser(verteilergruppe, userId);
    }
    */

    /**
     * Diese Methode nimmt einem Benutzer eine Verteilergruppe weg.
     * 
     * @param verteilergruppe
     *                    der Verteilergruppenschl�ssel.
     * @param userId
     *                    die Benutzer-ID.
     */
    /*
    public Map deleteVerteilergruppeFromUser(String verteilergruppe,
            String userId) throws ContactDBException {
            assertLogin();
            return doDeleteVerteilergruppeFromUser(verteilergruppe, userId);
    }
    */

    /**
     * Diese Methode sucht Addressen.
     * 
     * @param map
     *                    Map mit Suchfeldern.
     */
    public SortedMap searchAddress(Map map) throws ContactDBException {
            assertLogin();
            SortedMap result = new TreeMap();
            result = doSearchAddress(map);
            return result;
    }

    /**
     * @see org.evolvis.nuyo.db.Database#getBundeslandForPLZ(int)
     */
    public String getBundeslandForPLZ(String plz) throws ContactDBException {
            assertLogin();
            return doGetBundeslandForPLZ(plz);
    }

    /**
     * @see org.evolvis.nuyo.db.Database#getCityForPLZ(int)
     */
    public String getCityForPLZ(String plz) throws ContactDBException {
            assertLogin();
            return doGetCityForPLZ(plz);
    }

    /**
     * @see org.evolvis.nuyo.db.Database#getVersion()
     */
    public Map getVersion() throws ContactDBException {
            assertLogin();
            return doGetVersion();
    }

    /**
     *  
     */
    public Map getInitDatas(Map params) throws ContactDBException {
            assertLogin();
            return doGetInitDatas(params);
    }

//     /**
//      * Liefert Historie zu einer Adresse
//      * @param types Welche Typen sollen zur�ckgeliefert werden (null f�r alle)
//      * @param start Start
//      * @param end	Ende
//      * @param adrid	Adresse, zu der die History ausgelesen werden soll
//      * @return Liste von Listen, innere Liste hat 2 Elemente: id des Elements und Typ
//      * @throws ContactDBException
//      * @see de.tarent.contact.db.octopus.AddressImpl#getHistory(Set, Date, Date)
//      */
//     public List getHistory(Set types, Date start, Date end, int adrid)
//             throws ContactDBException {
//         List returnlist = new ArrayList();
//         List history;
//         try {
//             assertLogin();
//             history = doGetHistory(types, start, end, adrid);
//             if(history!=null)logger.log(Level.FINE, "History: " + history.toString());
//         } catch (IOException ioe) {
//             throw new ContactDBException(ContactDBException.EX_OCTOPUS,
//                     "Fehler beim Holen der History f�r diese Adresse", ioe);
//         }
//         if(history != null) {
// 	        Iterator it = history.iterator();
// 	        while(it.hasNext()) {
// 	            String id = (String) it.next();
// 	            Object typ =  it.next();
// 	            List payload = new ArrayList();
// 	            payload.add(id);
// 	            payload.add(typ);
// 	            returnlist.add(payload);
// 	        }
//         }
//         logger.log(Level.FINE, returnlist.toString());
//         return returnlist;
//     }

//     /**
//      * @param types
//      * @param start
//      * @param end
//      * @param adrid
//      */
//     private List doGetHistory(Set types, Date start, Date end, int adrid) throws IOException {
//         Map map = new TreeMap();
//         map.put("types", types);
//         map.put("start", new Long(start.getTime()));
//         map.put("end", new Long(end.getTime()));
//         map.put("adrId", new Integer(adrid));
//         Object result = call(module, "getHistorySOAP", map);
//         if(result instanceof SoapObject) {
//             SoapObject soap = (SoapObject) result;
//             Vector returnlist = new Vector();
//             for(int i=0; i<soap.getPropertyCount(); i++) {
//                 returnlist.add(soap.getProperty(i));
//             }
//             return returnlist;
//         }
//         return null;
//     }

    /**
     * Get extended fields
     */
    public Map getExtendedFields() throws ContactDBException {
            assertLogin();
            return doGetExtendedFields();
    }

    /**
     * Holt einen UserParameter aus der Datenbank
     * 
     * @param userId
     * @param key
     * @return Parameter
     */
    public String getUserParameter(String userId, String key)
            throws ContactDBException {
            assertLogin();
            return doGetUserParameter(userId, key);
    }

    /**
     * Setzt einen UserParameter in der Datenbank
     * 
     * @param userId
     * @param key
     * @param value
     * @return false bei Fehler, true sonst
     */
    public boolean setUserParameter(String userId, String key, String value)
            throws ContactDBException {
            assertLogin();
            return doSetUserParameter(userId, key, value);
    }
    
    /**
     * Setzt EinladungsStatus per SOAP setEventStatus im Octopus
     * 
     * @param eventID
     * @param statusId
     * @return false bei Fehler, true sonst
     */
    public boolean SetEventStatus(int userId, int eventId, int statusId)
            throws ContactDBException {
            assertLogin();
            return doSetEventStatus(userId, eventId, statusId);
    }
    
    
    /**
     * Setzt EinladungsStatus per SOAP setEventStatus im Octopus
     * 
     * @param eventID
     * @param statusId
     * @return false bei Fehler, true sonst
     */
    public boolean SetMyEventStatus(int userId, int eventId, int statusId)
            throws ContactDBException {
            assertLogin();
            return doSetMyEventStatus(userId, eventId, statusId);
    }
    
    
//     /**
//      * @param id ID des Versandauftrags
//      */
//     public List getVersandAuftag(int id) throws ContactDBException {
//         try {
//             assertLogin();
//             return doGetVersandAuftrag(id);
//         } catch (IOException e) {
//             throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Fehler beim Abholen eines Versandauftrags", e);
//         }
        
//     }

    /*
     * private Hilfsmethoden ******************************************
     */
    /**
     * Diese Methode folgt den Redirects, die beim �ffnen der
     * {@link #netBaseUrl Basis-URL}ausgef�hrt werden, und legt die
     * resultierende URL als {@link #netSessionUrl Sitzungs-URL}ab.
     */
    protected void createSessionUrl() throws IOException {
        HttpURLConnection connection = null;
        workUrl = null;

        if (netBaseUrl.startsWith("https")) {
            if (sslObject == null) sslObject = new OctopusSSL(netBaseUrl);
            connection = sslObject.getConnection();
        } else
            connection = (HttpURLConnection) new URL(netBaseUrl)
                    .openConnection();

        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        HttpURLConnection.setFollowRedirects(true);
        connection.setRequestProperty("SOAPAction", "");
        connection.setRequestProperty("Content-Type", "text/xml");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.connect();
        connection.getInputStream();
        netSessionUrl = connection.getURL().toExternalForm();
        connection.disconnect();
    }

    /** 
     */
    protected Object call(String module, String task, Map params)
        throws ContactDBException {
        
    	logger.log(Level.INFO, "--- deprecated call (OctopusConnector): " + task + "\t" + params);
        Method method = new Method(task);
        if (params != null) {
            for (Iterator it = params.keySet().iterator(); it.hasNext();) {
                Object key = it.next();
                Object value = params.get(key);
                method.add(""+key, value);
            }
        }
        return method.invoke();
    }


    /**
     */
    protected void assertLogin() throws ContactDBException {
        if (!isSessionOpen()) doLogin();
    }

    /**
     */
    protected void doLogin() throws ContactDBException {
        //doNothing any More
        //         Map params = new HashMap();
        //         params.put("username", user);
        //         params.put("password", password);
        
        //         call(module, "login_SOAP", params);
    }

    /**
     */
    protected void doLogout() throws ContactDBException {
        //doNothing any More
        //         if (isSessionOpen()) {
        //             Object res = call(module, "logout_SOAP", null);
        //             if (!"ok".equals(res)) throw new ContactDBException(ContactDBException.EX_OCTOPUS, ""+res);
        //         }
    }

    
    protected SortedMap doGetExtendedFields() throws ContactDBException {
        Map ext = new TreeMap();
        try {
            ext = (Map) call(module, "getExtendedFieldsSOAP", ext);
        } catch (Exception ignore) {
        }
        /* Es gibt nur nen Fehler, wenn nix in der DB steht (NQu). */
        TreeMap m = new TreeMap(ext);
        return new TreeMap(ext);
    }

    protected SortedMap doGetUsers() throws ContactDBException {
        return new TreeMap((Map) call(module, "getUsersSOAP", null));
    }

    protected SortedMap doGetBundeslaender() throws ContactDBException {
        return new TreeMap((Map) call(module, "getBundeslaenderSOAP", null));
    }

    protected SortedMap doGetAnreden() throws ContactDBException {
        return new TreeMap((Map) call(module, "getAnredenSOAP", null));
    }

    protected SortedMap doGetLKZ() throws ContactDBException {
        return new TreeMap((Map) call(module, "getLKZSOAP", null));
    }

    protected SortedMap doGetLaender() throws ContactDBException {
        return new TreeMap((Map) call(module, "getLaenderSOAP", null));
    }

    protected SortedMap doGetAddress(int id, String verteiler)
            throws ContactDBException {
        Map map = new TreeMap();
        map.put("adrId", new Integer(id));
        map.put("verteilergruppe", verteiler);
        return new TreeMap((Map) call(module, "getAddressSOAP", map));
    }

    protected SortedMap doGetStandardVerteiler(String user) throws ContactDBException {
        Map map = new TreeMap();
        map.put("user", user);
        return new TreeMap((Map) call(module, "getStandardVerteilerSOAP", map));
    }

    protected SortedMap doGetAllVerteilergruppen() throws ContactDBException {
        return new TreeMap((Map) call(module, "getAllVerteilergruppenSOAP",
                null));
    }


    protected SortedMap doGetAllgemeinenVerteiler() throws ContactDBException {
        return new TreeMap((Map) call(module, "getAllgemeinenVerteilerSOAP",
                null));
    }

    protected SortedMap doGetVerteiler(String grp) throws ContactDBException {
        SortedMap map = new TreeMap();
        if (grp == null || grp.length() < 1) return map;
        map.put("verteilergruppe", grp);
        return new TreeMap((Map) call(module, "getVerteilerSOAP", map));
    }

    protected SortedMap doGetVerteilerExt(String grp) throws ContactDBException {
        SortedMap map = new TreeMap();
        if (grp == null || grp.length() < 1) return map;
        map.put("verteilergruppe", grp);
        return new TreeMap((Map) call(module, "getVerteilerExtSOAP", map));
    }

    protected SortedMap doGetChosenAddresses(String grp, SortedMap verteiler,
            boolean allgemein, boolean schnitt, int count, int packageCount)
            throws ContactDBException {
        int allg;
        if (allgemein)
            allg = 1;
        else
            allg = 0;
        Map map = new TreeMap();
        map.put("verteilergruppe", grp);
        map.put("size", new Integer(verteiler.size()));
        map.put("allgemein", new Integer(allg));
        map.put("schnitt", new Boolean(schnitt));
        map.put("count", new Integer(count));
        map.put("package", new Integer(packageCount));
        map.put("user", getUser());
        map.putAll(verteiler);
        return new TreeMap((Map) call(module, "getChosenAddressesSOAP", map));
        //		return new TreeMap();
    }


    protected Map doDeleteVerteilergruppe(String key) throws ContactDBException {
        Map map = new TreeMap();
        map.put("key", key);
        Object retVal = call(module, "deleteVerteilergruppeSOAP", map);
        if (retVal instanceof Map)
            return new TreeMap((Map) retVal);
        else {
            logger.warning("deleteVerteilergruppeSOAP hat kein Ergebnis geliefert.");
            return new TreeMap();
        }
    }

    protected Map doCreateVerteiler(String grp, String key, String name1,
            String name2, String name3) throws ContactDBException {
        Map map = new TreeMap();
        map.put("gruppe", grp);
        map.put("key", key);
        map.put("name1", name1);
        map.put("name2", name2);
        map.put("name3", name3);
        SortedMap sm = new TreeMap((Map) call(module, "createVerteilerSOAP",
                map));
        return sm;
    }

    protected Map doDeleteVerteiler(String verteilergruppe, String key)
            throws ContactDBException {
        Map map = new TreeMap();
        map.put("key", key);
        map.put("verteilergruppe", verteilergruppe);
        SortedMap sm = new TreeMap((Map) call(module, "deleteVerteilerSOAP",
                map));
        return sm;
    }
        
    protected SortedMap doGetVersandAuftraege(String user) throws ContactDBException {
        Map map = new TreeMap();
        map.put("user", user);
        SortedMap sm = new TreeMap((Map) call(module,
                "getVersandAuftraegeSOAP", map));
        return sm;
    }

    protected Map doCreateUser(String userId, String lastName, String firstName, String password,
            boolean special, boolean admin) throws ContactDBException {
        int ad = 0, spez = 0;
        if (admin) ad = 1;
        if (special) spez = 1;
        Map map = new TreeMap();
        map.put("userId", userId);
        map.put("lastName", lastName);
        map.put("firstName", firstName);
        map.put("pwd", password);
        map.put("special", new Integer(spez));
        map.put("admin", new Integer(ad));
        map.put("user", getUser());
        SortedMap sm = new TreeMap((Map) call(module, "createUserSOAP", map));
        return sm;
    }

    protected Map doGetUserSpecial(String userId) throws ContactDBException {
        Map map = new TreeMap();
        map.put("userId", userId);
        SortedMap sm = new TreeMap(
                (Map) call(module, "getUserSpecialSOAP", map));
        return sm;
    }

    protected Map doGetUserAdmin(String userId) throws ContactDBException {
        Map map = new TreeMap();
        map.put("userId", userId);
        SortedMap sm = new TreeMap((Map) call(module, "getUserAdminSOAP", map));
        return sm;
    }

    protected Map doShowVerteilergruppeToUser(String verteilergruppe,
            String userId) throws ContactDBException {
        Map map = new TreeMap();
        map.put("gruppe", verteilergruppe);
        map.put("userId", userId);
        SortedMap sm = new TreeMap((Map) call(module,
                "showVerteilergruppeToUserSOAP", map));
        return sm;
    }

    protected Map doDeleteVerteilergruppeFromUser(String verteilergruppe,
            String userId) throws ContactDBException {
        Map map = new TreeMap();
        map.put("verteilergruppe", verteilergruppe);
        map.put("userId", userId);
        SortedMap sm = new TreeMap((Map) call(module,
                "deleteVerteilergruppeFromUserSOAP", map));
        return sm;
    }

    protected SortedMap doSearchAddress(Map m) throws ContactDBException {
        return new TreeMap((Map) call(module, "searchAddressSOAP", m));
    }

    protected String doGetBundeslandForPLZ(String plz) throws ContactDBException {
        Map map = new TreeMap();
        map.put("plz", plz);
        Map result = new TreeMap((Map) call(module, "getBundeslandForPLZSOAP",
                map));
        if (result.containsKey("bundesland"))
                return asString(result.get("bundesland"));
        return null;

    }

    protected String doGetCityForPLZ(String plz) throws ContactDBException {
        Map citymap = new TreeMap();
        citymap.put("plz", plz);
        Map ortmap = new TreeMap((Map) call(module, "getCityForPLZSOAP",
                citymap));
        if (ortmap.containsKey("city")) return asString(ortmap.get("city"));
        if (ortmap.containsKey("fehler"))
            throw new ContactDBException(ContactDBException.EX_OCTOPUS, "Octopus meldet folgenden Fehler:\n"
                                         + ortmap.get("fehler"));
        return null;
    }

    protected Map doGetVersion() throws ContactDBException {
        Map result = new TreeMap((Map) call(module, "getVersionSOAP", null));
        return result;
    }

    protected Map doGetInitDatas(Map params) throws ContactDBException {
        Map result = new TreeMap((Map) call(module, "getInitDatasSOAP", params));
        return result;
    }

    /**
     * Holt UserParameter per SOAP vom Octopus
     * 
     * @param userId
     * @param key
     * @return Parameter
     */
    protected String doGetUserParameter(String userId, String key)
            throws ContactDBException {
        Map map = new HashMap();
        map.put("userId", new String(userId));
        map.put("key", new String(key));
        Object result = call(module, "getUserParameterSOAP", map);
        if (result == null) { return null; }
        return new String((String) result);
    }

    /**
     * Setzt UserParameter per SOAP im Octopus
     * 
     * @param userId
     * @param key
     * @param value
     * @return false bei Fehler, true sonst
     */
    protected boolean doSetUserParameter(String userId, String key, String value)
            throws ContactDBException {
        Map map = new HashMap();
        map.put("userId", new String(userId));
        map.put("key", new String(key));
        map.put("value", new String(value));
        String result = (String) call(module, "setUserParameterSOAP", map);
        return "true".equals(result);
    }
    
    /**
     * Setzt EinladungsStatus per SOAP setEventStatus im Octopus
     * 
     * @param eventID
     * @param statusId
     * @return false bei Fehler, true sonst
     */
    protected boolean doSetEventStatus(int userID, int eventId, int statusId)
            throws ContactDBException {
    	
    	Map map = new HashMap();
        map.put("userid", new Integer(userID));
        map.put("eventid", new Integer(eventId));
        map.put("statusid",  new Integer(statusId));
        String result = (String) call(module, "setEventStatus", map);
        return "true".equals(result);
    }
    
    /**
     * Setzt EinladungsStatus per SOAP setMyEventStatus im Octopus
     * 
     * @param eventID
     * @param statusId
     * @return false bei Fehler, true sonst
     */
    protected boolean doSetMyEventStatus(int userID, int eventId, int statusId)
            throws ContactDBException {
    	
    	Map map = new HashMap();
        map.put("userid", new Integer(userID));
        map.put("eventid", new Integer(eventId));
        map.put("statusid",  new Integer(statusId));
        String result = (String) call(module, "setMyEventStatus", map);
        return "true".equals(result);
    }
    

//     protected Call prepareCall(String method) throws ServiceException {
//         Call call = (Call) service.createCall();
//         call.setTargetEndpointAddress("http://localhost:8080/octopus");
//         call.setOperationName(new QName("http://schemas.tarent.de/client",
//                 method));
//         call.setReturnType(Constants.XSD_ANYTYPE);
//         call.setReturnQName(Constants.XSD_ANYTYPE);
//         call.addParameter("username", Constants.XSD_STRING, ParameterMode.IN);
//         call.addParameter("password", Constants.XSD_STRING, ParameterMode.IN);
//         return call;
//     }

    /*
     * Variablen ******************************************************
     */
    protected String netBaseUrl;

    protected String netSessionUrl;

    protected String schemaBaseUrl;

    protected String module;

    static protected String user;

    protected String password;

    protected SortedMap selectedVerteiler;

    //static protected Service service;

    static protected String username;
    
    public final static Logger logger =
		Logger.getLogger(OctopusConnector.class.getName());


    private final static String asString(Object o) {
        return o == null ? null : o.toString();
    }

    
}