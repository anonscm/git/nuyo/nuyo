/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.config.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author niko
 *
 */
public class DateDescriptionFactory implements CalendarSettings {

    private static TarentLogger logger = new TarentLogger( DateDescriptionFactory.class );
    private final static Object singletonMonitor = new Object();
    private static DateDescriptionFactory instance = null;

    private int workBeginTimeSeconds;
    private int workEndTimeSeconds;
    private List holidaysList;

    private DateDescriptionFactory() {
        init();
    }

    private void init() {
        holidaysList = new ArrayList();
        setWorktimeSeconds( WORK_BEGIN_TIME_SECONDS, WORK_END_TIME_SECONDS );
        try {
            loadPublicHolydayDescriptionFile();
        }
        catch ( XmlUtil.Exception xmlue ) {
            logger.severe( "Failed loading Calender Resources", xmlue );
            fillPublicHolydayDescriptionList();
        }
    }

    public static DateDescriptionFactory instance() {
        if ( instance == null ) {
            synchronized ( singletonMonitor ) {
                instance = new DateDescriptionFactory();
            }
        }
        return instance;
    }

    public void setWorktimeHours( int starthour, int endhour ) {
        workBeginTimeSeconds = starthour * 60 * 60;
        workEndTimeSeconds = endhour * 60 * 60;
    }

    public void setWorktimeSeconds( int startsecond, int endsecond ) {
        workBeginTimeSeconds = startsecond;
        workEndTimeSeconds = endsecond;
    }

    private void fillPublicHolydayDescriptionList() {
        logger.info( "setting default calender definitions..." );

        holidaysList.add( new PublicHolidayDescription( new ScheduleDate( 27, 2, 2004 ), "Nikodanktag",
                                                        "Feiertag in NRW" ) );
        holidaysList.add( new PublicHolidayDescription( new ScheduleDate( 2, 3, 2004 ), "Mikeldanktag",
                                                        "Feiertag in NRW" ) );
        holidaysList.add( new PublicHolidayDescription( new ScheduleDate( 3, 3, 2004 ), "Mikelhimmelfahrt",
                                                        "Feiertag in Europa" ) );
        holidaysList.add( new PublicHolidayDescription( new ScheduleDate( 4, 3, 2004 ), "Nikohimmelfahrt",
                                                        "Feiertag in Europa" ) );
    }

    private void loadPublicHolydayDescriptionFile() throws XmlUtil.Exception {

        logger.info( "reading calender resources: " + HOLIDAY_FILE_NAME + "..." );

        String res = CALENDAR_RESOURCES_PATH + HOLIDAY_FILE_NAME;
        InputStream is = getClass().getResourceAsStream(res);
        
        Document document = XmlUtil.getParsedDocument( is, res );
        NodeList nodelist = document.getElementsByTagName( HOLIDAY_XML_TAG_NAME );

        if ( nodelist.getLength() > 0 )
            logger.info( "initializing calender holidays..." );

        for ( int i = 0; i < nodelist.getLength(); i++ ) {
            Element element = (Element) ( nodelist.item( i ) );
            String date = element.getAttribute( "date" );
            String name = element.getAttribute( "name" );
            String desc = element.getAttribute( "description" );
            try {
                holidaysList.add( new PublicHolidayDescription( getScheduleDate( date ), name, desc ) );
            }
            catch ( CalendarException e ) {
                logger.severe( "Error parsing date of HolyDay-Description", e );
            }
        }
    }

    private ScheduleDate getScheduleDate( String dateString ) throws CalendarException {

        if ( dateString.indexOf( "." ) > 0 ) {// pattern [DAY.MONTH] for every year
            String[] dayAndMonth = dateString.split( "." );
            if ( dayAndMonth.length == 2 ) {
                try {
                    return new ScheduleDate( Integer.parseInt( dayAndMonth[0] ), Integer.parseInt( dayAndMonth[0] ),
                                             new Date().getYear() );
                }
                catch ( NumberFormatException e ) {
                    throw new CalendarException( "failed parsing date of a holiday", e );
                }
            }
        }

        try {// default date format

            return new ScheduleDate( DateFormat.getDateInstance().parse( dateString ) );
        }
        catch ( Exception e ) {

            throw new CalendarException( "failed parsing date of a holiday", e );
        }
    }

    public DayDescription getDescriptionForDay( ScheduleDate day ) {
        DayDescription dd = null;

        int dayofweek = day.get( Calendar.DAY_OF_WEEK );

        if ( isPublicHolyday( day ) ) {
            dd = new HolidayDescription();
            PublicHolidayDescription hd = getPublicHolyday( day );
            dd.setDayName( getDayName( dayofweek ) );
            dd.setShortDayName( getShortDayName( dayofweek ) );
            dd.setName( hd.getName() );
            dd.setDescription( hd.getDescription() );
        }
        else if ( isWeekend( dayofweek ) ) {
            dd = new WeekendDayDescription();
            dd.setDayName( getDayName( dayofweek ) );
            dd.setShortDayName( getShortDayName( dayofweek ) );
            dd.setName( WEEKEND );
            dd.setDescription( "" );
        }
        else {
            dd = new WorkDayDescription();
            dd.setDayName( getDayName( dayofweek ) );
            dd.setShortDayName( getShortDayName( dayofweek ) );
            dd.setName( WORKDAY );
            dd.setDescription( "" );
            ( (WorkDayDescription) dd ).setStartWorkSecond( workBeginTimeSeconds );
            ( (WorkDayDescription) dd ).setEndWorkSecond( workEndTimeSeconds );
        }

        return dd;
    }

    public String getDayName( int dayofweek ) {
        if ( dayofweek > 7 )
            return "";
        return NAMES_OF_WEEK_DAYS[dayofweek - 1];
    }

    public String getShortDayName( int dayofweek ) {
        if ( dayofweek > 7 )
            return "";
        return ABBREVIATIONS_OF_DAYS[dayofweek - 1];
    }

    public boolean isWeekend( int dayofweek ) {
        return IS_WEEKEND_VALUES[dayofweek - 1];
    }

    public boolean isPublicHolyday( ScheduleDate date ) {
        if ( getPublicHolyday( date ) != null )
            return true;
        return false;
    }

    public PublicHolidayDescription getPublicHolyday( ScheduleDate date ) {
        int dyear = date.getScheduleYear();
        int ddayofyear = date.getScheduleDayOfYear();

        for ( int i = 0; i < ( holidaysList.size() ); i++ ) {
            PublicHolidayDescription hd = (PublicHolidayDescription) ( holidaysList.get( i ) );

            int hdyear = hd.getDate().getScheduleYear();
            int hddayofyear = hd.getDate().getScheduleDayOfYear();

            if ( ( hdyear == dyear ) && ( hddayofyear == ddayofyear ) ) {
                return ( hd );
            }
        }
        return null;
    }

}
