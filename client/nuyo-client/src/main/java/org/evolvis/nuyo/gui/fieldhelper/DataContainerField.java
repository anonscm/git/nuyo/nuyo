/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class DataContainerField extends ContactAddressField
{  
  private DataContainer m_oDataContainer;
  private EntryLayoutHelper m_oWidget = null; 
  
  public DataContainerField(DataContainer container)
  {
    m_oDataContainer = container;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }

  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (m_oWidget == null)
    {
      TarentWidgetLabel label = new TarentWidgetLabel(m_oDataContainer.getFieldDescriptor().getName());
      TarentWidgetInterface entry = m_oDataContainer.getGUIElement().getComponent();
      
      label.setToolTipText(m_oDataContainer.getFieldDescriptor().getDescription());
      entry.getComponent().setToolTipText(m_oDataContainer.getFieldDescriptor().getDescription());
      
      m_oWidget = new EntryLayoutHelper(label, entry, widgetFlags);            
    }
    return m_oWidget;
  }

  public void setData(PluginData data)
  {
    m_oDataContainer.setData(data);
  }

  public void getData(PluginData data)
  {
    m_oDataContainer.getData(data);
  }

  public boolean isDirty(PluginData data)
  {
    Object pluginobj = data.get(m_oDataContainer.getDataSource().getDBKey());
    Object guiobj = m_oDataContainer.getGUIData();
    if      ((pluginobj == null) && (guiobj == null)) return false;
    else if ((pluginobj != null) && (guiobj == null)) return true;
    else if ((pluginobj == null) && (guiobj != null)) return true;
    else return(!(pluginobj.equals(guiobj)));
  }

  public void setEditable(boolean iseditable)
  {
    m_oWidget.getEntryComponent().setWidgetEditable(iseditable);
  }

  public String getKey()
  {
    return m_oDataContainer.getKey();
  }
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
  }

  public void postFinalRealize()
  {
  }

  public String getFieldName()
  {
    return m_oDataContainer.getFieldDescriptor().getName();
  }

  public String getFieldDescription()
  {
    return m_oDataContainer.getFieldDescriptor().getDescription();
  }
}
