/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataStorageInterface.DataStorage;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.FieldDescriptors.FieldDescriptor;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.nuyo.util.general.DataAccess;



/**
 * @author niko
 *
  */
public interface DataContainer
{
  public DataContainer cloneDataContainer();

  public String getKey();
  public void setKey(String key);
  public boolean canStore(Class data);
  public Class stores();
  public void getData(PluginData data);
  public void setData(PluginData data);
  
  public void validate();
  
  public int getNumberOfErrors();
  public void setNumberOfErrors(int num);
  
  public boolean isClone();
  public boolean isDirty(PluginData data);
  public boolean isValid();
  
  public Object getGUIData();
  public void setGUIData(Object data);
  
  public void setGUIElement(GUIElement ge);  
  public void addValidator(Validator v);  
  public void removeValidator(Validator v);  
  public void setDataSource(DataSource io);  
  public void setFieldDescriptor(FieldDescriptor fd);
  public void setDataStorage(DataStorage ds);  
  public void addListener(DataContainerListener l);  
  public void removeListener(DataContainerListener l);  

  public List getValidators();  
  public DataSource getDataSource();  
  public FieldDescriptor getFieldDescriptor();
  public DataStorage getDataStorage();  
  public GUIElement getGUIElement();  
  public List getListeners();  

  public void setDataAccess(DataAccess access);
  public DataAccess getDataAccess();
  
  public void setLogger(Logger logger);
  public Logger getLogger();

  public void setDataContainerPool(DataContainerPool pool);
  public DataContainerPool getDataContainerPool();
  
  public void setDataMap(Map map);
  public Map getDataMap();
  
  public boolean isEventFireable(TarentGUIEvent e);
  public boolean isEventConsumable(TarentGUIEvent e);
  
  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void fireTarentGUIEvent(TarentGUIEvent e);
}
