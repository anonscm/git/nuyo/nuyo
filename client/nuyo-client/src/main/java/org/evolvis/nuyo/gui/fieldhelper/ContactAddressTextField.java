/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import org.evolvis.nuyo.controller.DoubleCheckManager;
import org.evolvis.nuyo.controls.TarentWidgetTextField;


/**
 * @author niko
 *
 */
public abstract class ContactAddressTextField extends ContactAddressField {
    
    private FocusListener doubleCheckFocusListener = null;
    
    protected ContactAddressTextField()
    {
    }

    protected TarentWidgetTextField createTextField( String toolTip, int lengthRestriction ) {
        TarentWidgetTextField tf =  new TarentWidgetTextField();
        
        if ( toolTip != null )
            tf.setToolTipText( toolTip );
        if ( lengthRestriction > 0 )
            tf.setLengthRestriction( lengthRestriction );
        return tf;
    }
    
    public void setDoubleCheckSensitive(TarentWidgetTextField tf, boolean issensitive)
    {
  	  if (doubleCheckFocusListener != null) 
		  tf.removeFocusListener(doubleCheckFocusListener);

      if (issensitive){
    	  doubleCheckFocusListener = new textfield_focus_doublecheck(tf);
    	  tf.addFocusListener(doubleCheckFocusListener);
      }
    }
    
    
    private class textfield_focus_doublecheck implements FocusListener
    {
      TarentWidgetTextField textfield;  
      String text;

      public textfield_focus_doublecheck(TarentWidgetTextField tf)
      {
        textfield = tf;            
      }
      
      public void focusGained(FocusEvent fe)
      {
        text = textfield.getText();    
      }
      
      public void focusLost(FocusEvent fe)
      {
        if (!(textfield.getText().equals(text)))
        {          
          // trigger DoublettenCheck
        	DoubleCheckManager.getInstance().checkForDoublesAndHandleResults();          
        }
      }
    }          

}
