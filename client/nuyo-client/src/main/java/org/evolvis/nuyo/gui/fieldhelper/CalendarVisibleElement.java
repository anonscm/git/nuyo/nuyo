/*
 * Created on 13.05.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;

/**
 * Interface of the visible calender parts.
 * Implement this interface in order to react on GUI events.
 * Every CalendarVisibleElement will be registered at {@link CalendarManager}. 
 * 
 * @author niko
 */
public interface CalendarVisibleElement {
    public String MANAGER = "appointment-display-manager";
    public String NAVIGATOR = "appointment-display-navigator";

    public void changedAppointment( Appointment appointment );
    public void addedAppointment( Appointment appointment );
    public void removedAppointment( Appointment appointment );
    public void selectedAppointment( Appointment appointment );
    public void changedCalendarCollection();
    public Appointment getAppointment( int appointmentid );
    public void setViewType( String viewtype );
    public void setVisibleInterval( ScheduleDate start, ScheduleDate end );
    public void setGridActive( boolean usegrid );
    public void setGridHeight( int seconds );
    /** Use this method to react on export button's event.*/
    public void clickedExportButton();
    /** Use this method to react on new appointment button's event.*/
    public void clickedNewAppointmentButton();
    /** Use this method to react on new task button's event.*/
    public void clickedNewTaskButton();
}
