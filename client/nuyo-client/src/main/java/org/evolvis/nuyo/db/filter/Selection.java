package org.evolvis.nuyo.db.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author kleinw
 *
 */
public class Selection implements ISelection {

    /**	Hier werden die Filter verwaltet */
    private List _filters = new ArrayList();
    /**	statisches Filterobjekt */
    static private Selection _filter;
    
    /**	Singleton */
    private Selection() {}
    
    
    /**	Hinzuf�gen eines Filters */
    public ISelection add(SelectionElement element) {
        _filters.add(element);
        return this;
    }
    
    /** f�r Wiederverwendung */
    public void clear() {
        _filters = new ArrayList();
    }
    
    /**	Erstellt eine Maprepr�sentation des Filters */
    public Map encode() throws ContactDBException {
        Map filter = new HashMap();
        for(Iterator it = _filters.iterator();it.hasNext();) {
            SelectionElement element = (SelectionElement)it.next();
            element.includeInMethod(filter);
        }
        return filter;
    }

    
    /**	Hier kann die Liste der Filterelemente bezogen werden */
    public Collection getFilterElements() {return _filters;}

    
    /**	Hier kann der Integerwert geholt werden, wenn genau ein Filterelement vom Typ IdFilter vorhanden ist */
    public Integer isSingleIdSelection() {
        if(_filters != null && _filters.size() == 1) {
            SelectionElement element = (SelectionElement)(_filters.iterator()).next();
            if(element instanceof AbstractIdSelection) 
                return ((AbstractIdSelection)element).getId();
        }
        return null;
    }

    
    /**	Befindet sich eine IdSelection in der Selection? */
    public boolean containsIdSelection() {
        for(Iterator it = _filters.iterator();it.hasNext();) {
            SelectionElement element = (SelectionElement)it.next();
            if(element instanceof AbstractIdSelection)
                return true;
        }
        return false;
    }
    
    
    public boolean equals(Object arg) {
        if(arg instanceof ISelection) {
            ISelection s = (ISelection)arg;
            if(s.getFilterElements().size() == _filters.size()) {
                if(_filters.containsAll(s.getFilterElements()))
                    return true;
            }
        } 
        return false;
    }
    
    public String toString(){
    	return _filters.toString();
    }
    
    /**	Hier kann die Selectioninstanz geholt werden */
    static public ISelection getInstance() {
        return new Selection();
    }
    
}
