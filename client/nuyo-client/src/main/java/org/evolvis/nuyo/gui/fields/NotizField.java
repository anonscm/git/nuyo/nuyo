/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextAreaField;


/**
 * @author niko
 *
 */
public class NotizField extends GenericTextAreaField
{
  public NotizField()
  {
    super("NOTIZ", AddressKeys.NOTIZ, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_Notiz_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_Notiz", 5000);
  }
}
