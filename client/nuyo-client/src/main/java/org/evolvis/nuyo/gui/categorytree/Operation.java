package org.evolvis.nuyo.gui.categorytree;

import org.evolvis.nuyo.gui.Messages;

/** Instances of this class denote the way positively selected subcategories
 * are associated when creating the search expression.
 * 
 * <p>Additionally an instance provides a localized label and tooltip
 * for an UI component resembling the operation.</p> 

 * <p>An <code>Operation</code> instance is only needed for a category tree
 * that is used for address selection.</p>
 * 
 */
final class Operation
{
  final static Operation AND = new Operation("and", "GUI_CategoryTree_Operation_And");
  final static Operation OR = new Operation("or", "GUI_CategoryTree_Operation_Or");
  
  private String name;
  
  private String labelKey;
  
  private Operation(String name, String labelKey)
  {
    this.name = name;
    this.labelKey = labelKey;
  }
  
  static Operation get(String opname)
  {
    if (AND.name.equals(opname))
      return AND;
    else if (OR.name.equals(opname))
      return OR;
    else
      return null;
  }
  
  public String toString()
  {
    return name;
  }
  
  String getLabel()
  {
    return Messages.getString(labelKey);
  }
  
  String getLabelToolTip()
  {
    return Messages.getString(labelKey + "_ToolTip");
  }
  
}