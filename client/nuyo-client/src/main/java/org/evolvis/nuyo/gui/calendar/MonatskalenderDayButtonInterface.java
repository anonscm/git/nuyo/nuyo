/*
 * Created on 05.05.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.Color;

import org.evolvis.nuyo.db.Appointment;


/**
 * @author niko
 *
 */
public interface MonatskalenderDayButtonInterface
{
  public void setShedulePointColor(Color color);  
  public void setToDoPointColor(Color color);  
  public void addAppointment(Appointment appointment);  
  public void removeAppointment(Appointment appointment);  
  public void clearAppointments();
  public void setActiveAppointment(Appointment appointment);
  public boolean containsAppointment(Appointment appointment);
}
