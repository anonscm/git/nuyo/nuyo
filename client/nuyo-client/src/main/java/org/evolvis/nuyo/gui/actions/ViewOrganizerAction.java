package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class ViewOrganizerAction extends AbstractGUIAction {

    private static final TarentLogger logger = new TarentLogger(ViewOrganizerAction.class);
    private static final long serialVersionUID = 3037919130958073808L;
    private ControlListener mainFrame = ApplicationServices.getInstance().getMainFrame();

    public void actionPerformed(ActionEvent e) {
        if(mainFrame != null) {
            mainFrame.activateTab(MainFrameExtStyle.TAB_ORGANIZER);
        } else logger.warning(getClass().getName() + "is not initialized.");
    }
    
    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();
        setEnabled(mainFrame.isTabEnabled(MainFrameExtStyle.TAB_ORGANIZER));
    }
}
