package org.evolvis.nuyo.gui.admin;

public class RolesManagementException extends Exception {

    public RolesManagementException() {
        super();
    }

    public RolesManagementException( String message ) {
        super( message );
    }

    public RolesManagementException( String message, Throwable cause ) {
        super( message, cause );
    }

    public RolesManagementException( Throwable cause ) {
        super( cause );
    }

}
