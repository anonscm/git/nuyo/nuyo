package org.evolvis.nuyo.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.datahandling.binding.CompoundModel;
import de.tarent.commons.datahandling.binding.DataChangedEvent;
import de.tarent.commons.datahandling.binding.DataChangedListener;
import de.tarent.commons.datahandling.binding.MapModel;
import de.tarent.commons.datahandling.entity.EntityException;

/**
 * This model represents the main application model for tarent-contact.
 * It is a place for storage of essential data, relevat to multiple components.
 * Through the <code>tarent-commons binding API</code>, a louse coupling of the components is supported.
 * For the shake of brevity we denote this API simply as <code>"The Binding Framework"</code>.
 * 
 * <p>Here is a short usage guideline:</p>
 * <pre>
 *   1.step: define, what property you want to share in different places (synchronized with changes)!
 *   
 *           For example, we consider the activated/deactivated status of the search filter in a status bar
 *              
 *           String propertyName = "searchFilterActived";
 *             
 *   2.step: prepare setter method according to JavaBeans convention!
 *   
 *   Note: this method will be invoked later via reflection by "The Binding Framework"
 *         as soon as it will be notified about property changes.
 *             
 *           public void setCategoryFilterActived( boolean isActivated ){
 *             categoryFilterLabel.setText("category filter: ", isActivated ? "activated" : "disabled");
 *           }
 *             
 *   3.step:  prepare mapping key to update/retrieve your property value from "The Binding Framework":
 *  
 *             String mappingKey = ApplicationModel.ADDRESS_LIST_PARAMETER_KEY +".searchFilterEnabled";
 *   
 *    
 *   4.step: registry your property bindings at "The Binding Framework":
 *             
 *             BindingManager bm = ApplicationServices.getInstance().getBindingManager();
 *             BeanBinding b = new BeanBinding(statusBar, propertyName, mappingKey);
 *             bm.addBinding(b);
 *             
 *   5.step: notify "The Binding Framework" about your property changes:
 *           
 *   Note: the new value will be automatically deligated to all places you regestered 
 *         through the setter method from the (2) step.
 *             
 *           a) use standart method:
 *             
 *              ApplicationServices.getInstance().getApplicationModel().setAttributeUnchecked(String key, Object value);
 *             
 *           b) or prepare your own methods like this one: 
 *   
 *               ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
 *   
 *   Note:   "The Binding Framework" supports hierarchical properties with "." notation:
 *           that means all bindings for child-properperties will be updated if we change a root-property. 
 *           See de.tarent.commons.utils.Pojo for more detailes.
 *            
 *           That's why our search mapping key will be updated if we update it's root-key (ADDRESS_LIST_PARAMETER_KEY)
 *           See ApplicationModel.fireAddressListParameterChanged():
 *               
 *              public void fireAddressListParameterChanged() {
 *                fireDataChanged(new DataChangedEvent(this, ADDRESS_LIST_PARAMETER_KEY));
 *              }
 *              
 *    Have Fun with "The Binding Framework"!
 * </pre>
 * 
 * 
 * @see de.tarent.commons.datahandling.binding.CompoundModel (<code>tarent-commons binding API</code>)
 * @see de.tarent.commons.utils.Pojo (uses reflections to set/retrieve property values)
 * @see org.evolvis.nuyo.gui.StatusBar (see as example: it binds labels with properties)
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ApplicationModel extends CompoundModel {

    private static final Logger utilLogger = Logger.getLogger(ApplicationModel.class.getName());
    private static final TarentLogger logger = new TarentLogger(utilLogger);

    //models within a compound model
    /** A map key of <i>application data model</i>.*/
    public final static String ADDRESS_DATA_KEY = "addressData";
    
    //attributes within models
    /** An attribute key for a count fo all <i>available</i> contacts.*/
    public final static String TOTAL_ADDRESS_COUNT_KEY = ADDRESS_DATA_KEY + ".totalAddressCount";

    /** An attribute key for an index off the currently <i>selected</i> contact.*/
    public final static String SELECTED_ADDRESS_INDEX_KEY = ADDRESS_DATA_KEY + ".selectedAddressIndex";

    /** An attribute key for currently <i>selected</i> contact.*/
    public final static String SELECTED_ADDRESS_KEY = ADDRESS_DATA_KEY + ".selectedAddress";
    
    /** An attribute key for the current address' category assignment. */
    public final static String ADDRESS_ASSIGNMENT_KEY = ADDRESS_DATA_KEY + ".categoryAssignment";

    /** An attribute key for current selection parameter. */
    public final static String ADDRESS_LIST_PARAMETER_KEY = ADDRESS_DATA_KEY + ".addressListParameter";

    /** An attribute key for the address list.*/
    public final static String ADDRESS_LIST_KEY = ADDRESS_DATA_KEY + ".addressList";

    
    public static final String MENU_DATA_KEY = "guiData";
    /** An attribute key for the visibility state of a table.*/
    public final static String ADDRESS_TABLE_VISIBILITY_KEY = MENU_DATA_KEY + ".tableIsVisible";
    
    /** An attribute key for the visibility state of a side menu.*/    
    public static final String SIDE_MENU_VISIBILITY_KEY = MENU_DATA_KEY + ".sideMenuIsVisible";
    
    
    public final static String CATEGORY_DATA_KEY = "categoryData";
    
    public final static String CATEGORIES_UPDATE_WATCH_KEY = CATEGORY_DATA_KEY + ".updateWatch";
    
    /**
     * Key of the model for the journal data.
     */ 
    public final static String JOURNAL_KEY = "journalData";
    
    /**
     * Attribute key for journal entries (also known as HistoryElement instances);
     */
    public final static String JOURNAL_ENTRY_LIST_KEY = JOURNAL_KEY + ".entries";
    
    public ApplicationModel() {
        super();
        registerModel(ADDRESS_DATA_KEY, new CompoundModel());
        registerModel(JOURNAL_KEY, new MapModel());
        registerModel(CATEGORY_DATA_KEY, new MapModel());
        registerModel(MENU_DATA_KEY, new MapModel());

        if (utilLogger.isLoggable(Level.FINEST)) {
            addDataChangedListener(new DataChangedListener() {
                    public void dataChanged(DataChangedEvent e) {
                        logger.finest("EVENT: "+e.getAttributePath());
                    }
                });
        }
    }
    
    /**
     * Sets the current visibility state of a side menu.
     */
    public void setSideMenuVisible(Boolean isVisible) {
        setAttributeUnchecked(SIDE_MENU_VISIBILITY_KEY, isVisible);
    }
    
    /**
     * Sets the current visibility state of a table.
     */
    public void setTableVisible(Boolean isVisible) {
        setAttributeUnchecked(ADDRESS_TABLE_VISIBILITY_KEY, isVisible);
    }
    
    /**
     * Sets the total count of addresses for the active user
     */
    public void setTotalAddressCount(Integer addressCount) {
        setAttributeUnchecked(TOTAL_ADDRESS_COUNT_KEY, addressCount);
    }

    /**
     * Returns total count of addresses for the active user
     */
    public Integer getTotalAddressCount() {
        return (Integer)getAttributeUnchecked(TOTAL_ADDRESS_COUNT_KEY);
    }


    /**
     * Sets the index of the current selected address
     */
    public void setSelectedAddressIndex(Integer index) {
        setAttributeUnchecked(SELECTED_ADDRESS_INDEX_KEY, index);
    }
    
    /**
     * Returns the index of the current selected address
     */
    public Integer getSelectedAddressIndex() {
        return (Integer)getAttributeUnchecked(SELECTED_ADDRESS_INDEX_KEY);
    }


    /**
     * Sets the current selected address
     */
    public void setSelectedAddress(Address address) {
        setAttributeUnchecked(SELECTED_ADDRESS_KEY, address);
    }
    
    /**
     * Returns the current selected address
     */
    public Address getSelectedAddress() {
        return (Address)getAttributeUnchecked(SELECTED_ADDRESS_KEY);
    }


    /**
     * Sets the main address list of tarent contact.
     * The assigned label for representing of a count of loaded addresses
     * will be automatically updated by the Binding Framework.
     * Automatically means via reflection according to JavaBeans convention
     * with setter and getter methods (see {@link de.tarent.commons.utils.Pojo}). 
     * Note: the Binding Framework supports hierarchical properties with "." notation.
     * That's why a label inside Status Bar, which has been bound with 
     * <code>key = ADDRESS_LIST_KEY + ".size"</code>, will be also automatically updated. 
     * See the according bindings inside {@link org.evolvis.nuyo.gui.StatusBar#initBindings()} method for more details.
     */
    public void setAddressList(Addresses addresses) {
        setAttributeUnchecked(ADDRESS_LIST_KEY, addresses);
    }
    
    /**
     * Returns the main address list of tarent contact
     */
    public Addresses getAddresses() {
        return (Addresses)getAttributeUnchecked(ADDRESS_LIST_KEY);
    }

    /**
     * Sets the current selection parameter
     */
    public void setAddressListParameter(AddressListParameter parameter) {
        setAttributeUnchecked(ADDRESS_LIST_PARAMETER_KEY, parameter);
    }
    
    /**
     * Returns the current selection parameter
     */
    public AddressListParameter getAddressListParameter() {
        return (AddressListParameter)getAttributeUnchecked(ADDRESS_LIST_PARAMETER_KEY);
    }

    public void fireAddressListParameterChanged() {
        fireDataChanged(new DataChangedEvent(this, ADDRESS_LIST_PARAMETER_KEY));
    }
    
    public void setJournalEntryList(List /*<HistoryElement>*/ entries)
    {
      setAttributeUnchecked(JOURNAL_ENTRY_LIST_KEY, entries);
    }
    
    public List /*<HistoryElement>*/ getJournalEntryList() {
      return (List) getAttributeUnchecked(JOURNAL_ENTRY_LIST_KEY);
    }
    
    public void fireJournalEntryListChanged()
    {
      fireDataChanged(new DataChangedEvent(this, JOURNAL_ENTRY_LIST_KEY));
    }
    
    public void setCategoryAssignment(List /*<Category>*/ categories)
    {
      setAttributeUnchecked(ADDRESS_ASSIGNMENT_KEY, categories);
    }
    
    public List /*<Category>*/ getCategoryAssignment() {
      return (List) getAttributeUnchecked(ADDRESS_ASSIGNMENT_KEY);
    }
    
    public void fireCategoryAssignmentChanged()
    {
      fireDataChanged(new DataChangedEvent(this, ADDRESS_ASSIGNMENT_KEY));
    }
    
    /**
     * Modifies the categories update value causing its bindings to get informed.
     * 
     * Those can then decide whether they 
     *
     */
    public void raiseCategoriesUpdateWatch()
    {
      Integer i = (Integer) getAttributeUnchecked(CATEGORIES_UPDATE_WATCH_KEY);
      i = new Integer((i == null) ? 0 : i.intValue() + 1);
      
      setAttributeUnchecked(CATEGORIES_UPDATE_WATCH_KEY, i);
    }
    
    public void fireCategoriesUpdateWatchChanged() {
        fireDataChanged(new DataChangedEvent(this, CATEGORIES_UPDATE_WATCH_KEY));
    }
    
    /**
     * Sets an attribute with an unchecked exception in case of an error
     */
    protected void setAttributeUnchecked(String key, Object value) { 
        try {
            setAttribute(key, value);
        } catch (EntityException e) {
            throw (IllegalStateException) new IllegalStateException("Unable to set the key '"+key+"' in application model").initCause(e);
        }
    }

    /**
     * Returns Sets an attribute with an unchecked exception in case of an error
     */
    protected Object getAttributeUnchecked(String key) {
        try {
            return getAttribute(key);
        } catch (EntityException e) {
            throw (IllegalStateException) new IllegalStateException("Unable to retrieve the key '"+key+"' from the application model").initCause(e);
        }
    }
}