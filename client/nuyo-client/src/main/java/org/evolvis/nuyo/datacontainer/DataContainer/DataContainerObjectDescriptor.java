/*
 * Created on 24.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataContainerObjectDescriptor
{
  public final static Object VALIDATOR = "VALIDATOR";
  public final static Object DATASOURCE = "DATASOURCE";
  public final static Object DATASTORAGE = "DATASTORAGE";
  public final static Object GUIFIELD = "GUIFIELD";
  public final static Object LISTENER = "LISTENER";
  public final static Object ACTION = "ACTION";
  
  private Object m_oContainerType;
  private String m_sContainerName;
  private List   m_oParameterDescriptions;
  private DataContainerVersion m_oDataContainerVersion;
  
  public DataContainerObjectDescriptor(Object type, String name, DataContainerVersion version)
  {
    m_oContainerType = type;
    m_sContainerName = name;
    m_oParameterDescriptions = new ArrayList();
    m_oDataContainerVersion = version;
  }
  
  public DataContainerObjectDescriptor(Object type, String name, DataContainerVersion version, List paramdescriptions)
  {
    m_oContainerType = type;
    m_sContainerName = name;
    m_oParameterDescriptions = paramdescriptions;    
    m_oDataContainerVersion = version;
    if (m_oParameterDescriptions == null) m_oParameterDescriptions = new ArrayList();
  }
  
  public void addParameterDescription(ParameterDescription pd)
  {
    if (m_oParameterDescriptions == null) m_oParameterDescriptions = new ArrayList();
    m_oParameterDescriptions.add(pd);
  }
  
  public Object getContainerType()
  {
    return m_oContainerType;
  }
  
  public String getContainerTypeName()
  {
    return m_oContainerType.toString();
  }
  
  public String getContainerName()
  {
    return m_sContainerName;
  }
  
  public List getParameterDescriptions()
  {
    return m_oParameterDescriptions;
  }
  
  public DataContainerVersion getDataContainerVersion()
  {
    return m_oDataContainerVersion;
  }
  
  public String dump()
  {
    String text = "";
    text += m_sContainerName + " [" + m_oContainerType.toString() + "]\n";
    
    if (m_oParameterDescriptions != null)
    {
      Iterator it = m_oParameterDescriptions.iterator();
      while(it.hasNext())
      {
        String pdtext = "";
        ParameterDescription desc = (ParameterDescription)(it.next());
        pdtext += "  Parameter \"" + desc.getKey() + "\" " + desc.getName() + " (" + desc.getDescription() + ")\n";         
        List vd = desc.getValueDescriptions();
        Iterator vdit = vd.iterator();
        while(vdit.hasNext())
        {
          String[] vdt = (String[])(vdit.next());
          pdtext += "    \"" + vdt[0] + "\" = " + vdt[1] + "\n";
        }
      }
    }     
    return text;   
  }
  
}
