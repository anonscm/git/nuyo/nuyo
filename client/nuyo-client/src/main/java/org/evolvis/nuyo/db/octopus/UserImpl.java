package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.GlobalRole;
import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.cache.Cachable;
import org.evolvis.nuyo.db.cache.EntityCache;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.UserId;
import org.evolvis.nuyo.db.filter.AbstractStringSelection.UserLogin;
import org.evolvis.nuyo.db.octopus.AddressPreview.PreviewFormatException;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractPersistentRelation;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;


/**
 * @author kleinw
 *
 *	Implementierung des Interfaces de.tarent.contact.db.User.
 *
 */
public class UserImpl extends UserBean implements Cachable {
	
	private static Logger logger = Logger.getLogger(UserImpl.class.getName());
	
	//	
    //	Soapanbindung
    //
    /** Alle Kalender eines Benutzers */
	private static final String METHOD_GET_SCHEDULES = "getSchedules";
	/** Alle Benutzergruppen eines Benutzers */
	private static final String METHOD_GET_USERGROUPS = "getUserGroups";
	/** Alle Proxys eines Benutzers */
	private static final String METHOD_GET_USERPROXY = "getUserProxy";
    /** Benutzer anlegen oder �ndern */
    private static final String METHOD_CREATE_OR_MODIFY_USER = "createOrModifyUser";
    /** Benutzer l�schen */
    private static final String METHOD_DELETE_USER = "deleteUserSOAP";
    /** Dem Benutzer zugeordnete Adresse */
    private static final String METHOD_GET_USERS_ADDRESS = "getUsersAddress";
    /** Von allen Benutzern die zugeordnete Adresse */
    private static final String METHOD_GET_ADDRESSES_FROM_USERS = "getAddressesFromUsers";
	
    
    /** Datenbank-ID des Benutzers */
	private static final String PARAM_USERID = "userid";
    /** zu l�schende Kalender */
	private static final String PARAM_CALENDARS_TO_REMOVE = "calendarstoremove";
    /** hinzuzuf�gende Kalender */
	private static final String PARAM_CALENDARS_TO_ADD = "calendarstoadd";
    /** Benutzergruppen, die entfernt werden sollen */
	private static final String PARAM_GROUPS_TO_REMOVE = "groupstoremove";
    /** Benutzergruppen, die hinzugef�gt werden sollen */
	private static final String PARAM_GROUPS_TO_ADD = "groupstoadd";
    /** Kategorien, die entfernt werden sollen */
	private static final String PARAM_CATEGORIES_TO_REMOVE = "categoriestoremove";
    /** Kategorien, die hinzugef�gt werden sollen */
	private static final String PARAM_CATEGORIES_TO_ADD = "categoriestoadd";


	/**Methode um globale Rollen eines Users zu holen*/
	public static final String METHOD_GET_USER_CATEGORY_RIGHTS = "getUserCategoryRights";
	
    //
    //	Instanzmerkmale
    //
    /**	Hier wird statisch eine Liste von Benutzern und deren zugeordneten Adressen gehalten */
    private static List _addressUserList;
    
    
    /** Zugriff auf dieses Objekt in einer anonymen Klassen */
    protected UserImpl _this = this;

    GlobalRole globalRights;
    
    
    //	Initialisierung des Objekts zum Holen von Daten 
    static {
        _fetcher = new AbstractEntityFetcher("getUsers", true) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    User user = new UserImpl();
                    ((IEntity)user).populate(values);
                    return (IEntity)user;
                }
            };
    }
    
    
    /**	Konstruktor f�r ein neues Objekt */
    protected UserImpl() {
//    Alle Kategorien eines beliebigen Users braucht eigentlich niemand zu sehen.
//         _categories = new AbstractPersistentRelation(this) {
//             public Collection getRelationContents() throws ContactDBException {
//                 ISelection f = Selection.getInstance()
//                 	.add(new UserId(_id));
//                 return CategoryImpl.getCategories(f);
//            }
//             public String[] getFields() {return new String[]{PARAM_CALENDARS_TO_ADD, PARAM_CATEGORIES_TO_REMOVE};}
//             public void relateTo(IEntity entity) throws ContactDBException {((CategoryImpl)entity).add(_this);}
// 			public void killRelation(IEntity entity) throws ContactDBException {((CategoryImpl)entity).remove(_this);}
//         };
        _userGroups = new AbstractPersistentRelation(this) {
            public Collection getRelationContents() throws ContactDBException {
                ISelection f = Selection.getInstance()
                	.add(new UserId(_id));
                return UserGroupImpl.getUserGroups(f);
            }
            public String[] getFields() {return new String[]{PARAM_GROUPS_TO_ADD, PARAM_GROUPS_TO_REMOVE};}
            public void relateTo(IEntity entity) throws ContactDBException {((UserGroupImpl)entity).add(_this);}
			public void killRelation(IEntity entity) throws ContactDBException {((UserGroupImpl)entity).remove(_this);}
        };
        _calendars = new AbstractPersistentRelation(this) {
            public Collection getRelationContents() throws ContactDBException {
                ISelection f = Selection.getInstance()
                	.add(new UserId(_id));
                return CalendarImpl.getCalendars(f);
            }
            public String[] getFields() {return new String[]{PARAM_CALENDARS_TO_ADD, PARAM_CALENDARS_TO_REMOVE};}
            public void relateTo(IEntity entity) throws ContactDBException {((CalendarImpl)entity).add(_this);}
			public void killRelation(IEntity entity) throws ContactDBException {((CalendarImpl)entity).remove(_this);}
        };
    }
    
    
    /**	Ist nur die ID vorhanden, dann dieser Konstruktor */
    protected UserImpl(Integer userId) throws ContactDBException {
        this();
        ISelection selection = Selection.getInstance()
        	.add(new UserId(userId));
        List tmp = (List)_fetcher.getEntities(selection);
        if(tmp.size() > 0) {
            User user = (User)tmp.get(0);
            _id = new Integer(user.getId());
            setAttribute(User.KEY_FIRST_NAME, user.getAttribute(User.KEY_FIRST_NAME));
            setAttribute(User.KEY_LAST_NAME, user.getAttribute(User.KEY_LAST_NAME));
            setAttribute(User.KEY_LOGIN_NAME, user.getAttribute(User.KEY_LOGIN_NAME));
            setAttribute(User.KEY_USER_TYPE, user.getAttribute(User.KEY_USER_TYPE));
        }
    }
        
    /**	Konstruktion �ber den Loginnamen */
    protected UserImpl(String userLogin) throws ContactDBException {
        this();
        ISelection selection = Selection.getInstance()
        	.add(new UserLogin(userLogin));
        List tmp = (List)_fetcher.getEntities(selection);
        if(tmp.size() == 1) {
            User user = (User)tmp.get(0);
            _id = new Integer(user.getId());
            setAttribute(User.KEY_FIRST_NAME, user.getAttribute(User.KEY_FIRST_NAME));
            setAttribute(User.KEY_LAST_NAME, user.getAttribute(User.KEY_LAST_NAME));
            setAttribute(User.KEY_LOGIN_NAME, user.getAttribute(User.KEY_LOGIN_NAME));
            setAttribute(User.KEY_USER_TYPE, user.getAttribute(User.KEY_USER_TYPE));
        }        
    }
    
    public GlobalRole getGlobalRights() throws ContactDBException {
        if (globalRights == null)
            globalRights = UserGlobalRoleImpl.getRoleForUser(_id);
        return globalRights;
    }

    public ObjectRole getCategoryRights(String categoryObjectID) throws ContactDBException {
        if (categoryObjectID == null)
        	return null;
        
        if (categoryRights == null || categoryRights.get(categoryObjectID) == null ) {
            // Berechtigungen f�r alle Kategorien laden.
            Method method = new Method(METHOD_GET_USER_CATEGORY_RIGHTS);
            List entities = (List)method.invoke();
            categoryRights = new HashMap(entities.size());
            for (Iterator iter = entities.iterator(); iter.hasNext();) {
                UserObjectRoleImpl newEnity = new UserObjectRoleImpl();
                ResponseData rd = new AbstractEntityFetcher.ResponseData((Map)iter.next());
                newEnity.populate( rd );
                categoryRights.put(rd.getString("category"), newEnity);
            }
            
        }
        
        return (ObjectRole)categoryRights.get(categoryObjectID);
    }
    
    public boolean hasAnyCategoryRights(List categorys, String type) throws ContactDBException{
    	
    	Iterator iter = categorys.iterator();
    	//System.out.println( categorys.size() +  " Kategorien zu durchsuchen...");
    	
    	
    	ObjectRole currObjectRole;
    	
    	while (iter.hasNext()){
    		
    		String keyValue = (String) iter.next();
    		//System.out.println("Teste Kategorie " + keyValue + " ...");
    		int index = keyValue.indexOf(": ");
    		String key = index > 0 ? keyValue.substring(0, index) : null;
    		currObjectRole = getCategoryRights(key);
    		
    		//System.out.println("teste Unterkategorien f�r Kategorie " + key);
    		if (currObjectRole.hasRight(type).booleanValue()){
				return true;
			}
    		
    	}
    	return false; 
    }
    
            
    
    /** Ist die Entit�t vollst�ndig? */
    public void validate() throws ContactDBException {}

    
    /**	Methode speichert oder �ndert Objekt. */
    public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_OR_MODIFY_USER);
        //_categories.includeInMethod(_method);
        _userGroups.includeInMethod(_method);
        _calendars.includeInMethod(_method);
        Object tmp = _method.invoke();
        if(tmp instanceof Integer)
            _id = (Integer)tmp;
        else if(tmp instanceof String)
            _id = Integer.valueOf((String)tmp);
        else 
            throw new ContactDBException(ContactDBException.EX_INVALID_USER, "Ung�ltige Daten �ber den User zur�ckerhalten!");
        _userGroups.commmit();
        _calendars.commmit();
        //_categories.commmit();
    }

    
    /**	Methode l�scht Objekt aus der Datenbank. Danach ist es unbrauchbar */
    public void delete() throws ContactDBException {
    	Method method = new Method(METHOD_DELETE_USER);
        method.add(User.KEY_LOGIN_NAME, getAttribute(User.KEY_LOGIN_NAME));
        method.invoke();
    }
    
    
	/**	l�dt das Objekt neu. */
    public void rollback() throws ContactDBException {
        populate(_responseData);
        _userGroups.rollback();
        _calendars.rollback();
        //_categories.rollback();
    }
    
    
    /**	Methode zum F�llen eines Objekts durch DB-Inhalte. @param = values - Liste mit den Inhalten */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        _attributes.put(User.KEY_LOGIN_NAME, data.getString(1));
        _attributes.put(User.KEY_LAST_NAME, data.getString(3));
        _attributes.put(User.KEY_FIRST_NAME, data.getString(4));
        _attributes.put(User.KEY_USER_TYPE, data.getInteger(5));
    }
    
    
    /** Aktualisierung des Caches nach dem 'commit' */
    public void deleteFromCache() throws ContactDBException {
        _fetcher.getCache().delete(new Integer(getId()));
    }
    
    
    /** Aus dem Cache entfernen */
    public void updateInCache() throws ContactDBException {
        EntityCache cache = _fetcher.getCache();
        cache.store(null, Collections.singletonList(this));
    }
    
    
    /**	
     * Dem Benutzer zugeordnete Adresse holen 
     */
    public Address getAssociatedAddress() throws ContactDBException {
        Method method = new Method(METHOD_GET_USERS_ADDRESS);
        method.add(PARAM_USERID, new Integer(getId()));
        String previewString = (String)method.invoke();
        if (previewString == null)
            return null;
        try {
            AddressPreview preview = new AddressPreview(previewString);
            return ApplicationServices.getInstance().getCurrentDatabase().getAddress(preview.getAddressPk().intValue());
        } catch (PreviewFormatException pfe) {
            throw new RuntimeException(pfe);
        }
    }
    
    static public User getUserFromID(Integer UserId) throws ContactDBException{
        if (UserId == null) logger.log(Level.WARNING, "USERID NULL!!!!!!");
        return new UserImpl(UserId);
    }
    
    /**	Hier werden zu allen Usern statisch zugeordnete Adressen gehalten */
	static public User getUserFromAddress(Integer addressId) throws ContactDBException {
	    if (_addressUserList == null) {
			Method method = new Method(METHOD_GET_ADDRESSES_FROM_USERS);
			_addressUserList = (List)method.invoke();
		}
		
		if (_addressUserList == null)
			return null;
        
        Iterator it = _addressUserList.iterator();
        while (it.hasNext()) {
            List mapping = (List)it.next();
            if (addressId.equals(mapping.get(0)))
                return new UserImpl((Integer)mapping.get(1));
        }        
		return null;
    }
	

	/**	Mit dieser Methode k�nnen alle Benutzer des Systems abgeholt werden. */
	static public Collection getUsers(ISelection filter) throws ContactDBException {
        return _fetcher.getEntities(filter, false);
    }
	
	static public Collection getUsers(ISelection filter, boolean enalbeChaching) throws ContactDBException {
        return _fetcher.getEntities(filter, enalbeChaching);
    }
    
	
    /**	Stringrepr�sentation des Objekts */
    public String toString() {
        return new StringBuffer()
        	.append(_id)
        	.append(" ")
        	.append(_attributes.get(User.KEY_LOGIN_NAME))
        	.toString();
    }
}
