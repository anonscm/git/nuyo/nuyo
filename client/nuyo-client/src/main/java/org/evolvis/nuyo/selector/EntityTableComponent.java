package org.evolvis.nuyo.selector;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.db.octopus.AddressesImpl;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.xana.swing.utils.SwingIconFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.commons.datahandling.ListFilterPropertyName;
import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.commons.datahandling.entity.AsyncEntityListImpl;
import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.ui.ColumnDescription;
import de.tarent.commons.ui.EntityListTableModel;

/**
 * This class contains the table component that is used in the address-selector. 
 * The component can take a list of entities and let the user comfortably select some of them. The Entities are displayed in two different tables, 
 * one for the selectable entities and one for the selected entities.
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class EntityTableComponent extends MouseAdapter implements ActionListener, TableModelListener{
	
	private static Logger logger = Logger.getLogger(EntityTableComponent.class.getName());
	private static final String TABLE_SEARCH_RESULT = "searchResultTable";
	private static final String TABLE_SELECTED = "selectedResultTable";
	
	AsyncEntityListImpl asyncList;
	ColumnDescription[] columnDescriptions;
	private JTable searchResultTable;	// left
	private JTable selectedResultTable;	// right
	
	Addresses searchedAddresses;	
	Addresses movedAddresses;
	
	private JButton buttonMoveToSelected;
	private JButton buttonRemoveFromSelected;
	private JButton buttonMoveAllToSelected;
	private JButton removeAllFromSelected;	
	
	private JLabel labelSearchResultCount;
	private JLabel labelSelectedCount;
	private String resultHeadlinePrefix="Suchergebnis:";
	private String selectionHeadlinePrefix="Auswahl:";
	
	private String headlinePostfix = "Adressen";
	private PanelBuilder builder;	
	
	public EntityTableComponent(Addresses addresses)  {
		searchedAddresses = addresses;
		movedAddresses = new AddressesImpl((OctopusDatabase) ApplicationServices.getInstance().getCurrentDatabase());
		columnDescriptions = new ColumnDescription[]{new ColumnDescription(Address.PROPERTY_SHORT_ADDRESS_LABEL.getKey(), "Addresse", String.class),
				new ColumnDescription(Address.PROPERTY_VORNAME.getKey(), Address.PROPERTY_VORNAME.getLabel(), String.class)};
		adjustTables();				
		buildLabels();		
		buildPanel();
	}
	
	private void buildButtons(){
		buttonMoveToSelected = new JButton((ImageIcon)SwingIconFactory.getInstance().getIcon("1rightarrow.png"));
		buttonMoveToSelected.addActionListener(this);
		buttonRemoveFromSelected = new JButton((ImageIcon)SwingIconFactory.getInstance().getIcon("1leftarrow.png"));
		buttonRemoveFromSelected.addActionListener(this);
		buttonMoveAllToSelected = new JButton((ImageIcon)SwingIconFactory.getInstance().getIcon("2rightarrow.png"));
		buttonMoveAllToSelected.addActionListener(this);
		removeAllFromSelected = new JButton((ImageIcon)SwingIconFactory.getInstance().getIcon("2leftarrow.png"));
		removeAllFromSelected.addActionListener(this);
	}
	
	private void buildLabels(){
		labelSearchResultCount=new JLabel("0");
		labelSelectedCount=new JLabel("0");
		//the same font and color as the title of the titled border
		Font font = new Font(labelSearchResultCount.getFont().getFontName(),Font.BOLD,12);
		labelSearchResultCount.setFont(font);
		labelSelectedCount.setFont(font);
		labelSelectedCount.setForeground(new Color(90,90,90));
		labelSearchResultCount.setForeground(new Color(90,90,90));
	}
	
	public void buildPanel(){		
		int tableWidth = 300;
		int tableHeight = 400;		
		JScrollPane addressScrollPanel = makeScrollPane(searchResultTable,tableWidth, tableHeight);	// left table
		JScrollPane selectedScrollPanel = makeScrollPane(selectedResultTable,tableWidth, tableHeight);	// right table
		
		putHeadline(labelSearchResultCount,resultHeadlinePrefix,searchResultTable.getModel());
		putHeadline(labelSelectedCount,selectionHeadlinePrefix,selectedResultTable.getModel());
		
		FormLayout layout = new FormLayout(
				"l:p:g,4dlu,p,4dlu,l:p:g", //columns 
		"pref,1dlu,pref, 2dlu, pref, 8dlu, pref, 2dlu, pref, fill:pref:GROW");      // rows
		
		layout.setColumnGroups(new int[][]{{1,5}});
		layout.setRowGroups(new int[][]{{3,5}});
		
		builder = new PanelBuilder(layout);
			
		//Obtain a reusable constraints object to place components in the grid.
		CellConstraints cc = new CellConstraints();
		
		//the two tables
		builder.add(labelSearchResultCount, cc.xy(1,1));
		builder.add(labelSelectedCount, cc.xy(5,1));
		builder.add(addressScrollPanel, cc.xywh(1,3,1,8,"fill, fill"));
		builder.add(selectedScrollPanel, cc.xywh(5,3,1,8,"fill, fill"));
		// the buttons
		buildButtons();
		builder.add(buttonMoveToSelected, cc.xy(3,3));
		builder.add(buttonMoveAllToSelected, cc.xy(3,5));
		builder.add(buttonRemoveFromSelected, cc.xy(3,7));
		builder.add(removeAllFromSelected, cc.xy(3,9));
	}
	
	private JScrollPane makeScrollPane(JTable table,int width, int height){		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(width,height));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		return scrollPane;
	}
	
	private void  adjustTables(){
		int rowHeight = 20;
		//----------- left table -------------
		searchResultTable = new JTable();
		searchResultTable.setRowHeight(rowHeight);
		searchResultTable.addMouseListener(this);
		setAddressList(searchedAddresses, searchResultTable);
		searchResultTable.getModel().addTableModelListener(this);
		//----------- right table -------------
		selectedResultTable = new JTable();
		selectedResultTable.setRowHeight(rowHeight);
		selectedResultTable.addMouseListener(this);
		setAddressList(movedAddresses, selectedResultTable);
		selectedResultTable.getModel().addTableModelListener(this);
	}
	
	public JPanel getComponent() {
		return builder.getPanel();
	}
	
	public EntityList getSelection(){
		return movedAddresses;
	}
	
	public EntityList getSelection(JTable table, Addresses addresses){
		int[] i =  table.getSelectedRows();
		NonPersistentAddresses tempAddresses = new NonPersistentAddresses();
		tempAddresses.clear();
		for(int j = 0; j < i.length; j++){
			tempAddresses.addEntity(addresses.getEntityAt(i[j]));
		}
		return (EntityList)tempAddresses;
	}
	
	private void putHeadline(JLabel label, String prefix, TableModel model){
		label.setText(prefix + " " + model.getRowCount()+ " " + headlinePostfix);
	}
	
	/**
	 * Highlights the table rows of the specified table that contain the entities in the EntityList list
	 * 
	 */
	public void setSelected(EntityList list, String table){		
		if (table.equals(TABLE_SEARCH_RESULT)){
			searchResultTable.clearSelection();
			for (int i=0;i<list.getSize();i++){
				Entity entity=(Entity)list.getEntityAt(i);
				int rowIndex = ((EntityListTableModel)searchResultTable.getModel()).getRowOf(entity);
				searchResultTable.changeSelection(rowIndex,1,false,true);
			}			
		} else if (table.equals(TABLE_SELECTED)){			
			selectedResultTable.clearSelection();
			for (int i=0;i<list.getSize();i++){
				Entity entity=(Entity)list.getEntityAt(i);
				int rowIndex = ((EntityListTableModel)selectedResultTable.getModel()).getRowOf(entity);
				selectedResultTable.changeSelection(rowIndex,1,false,true);
			}
		}		
	}
	
//	----------------- actionPerformed + action logic ----------------------
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==buttonMoveToSelected){
			moveSelectedToRight();
		} else if (e.getSource()==buttonRemoveFromSelected){
			removeFromSelectedList();
		} else if (e.getSource()==removeAllFromSelected){
			removeAllFromSelectedList();			
		} else if (e.getSource()==buttonMoveAllToSelected){
			moveAllToSelection();			
		}
	}
	
	/**
	 * moves the specified rows to the table that contains the selected entities
	 * @param selectedRows an integer array that specifies the rows to move
	 */
	public void moveSelectedToRight(){
		AddressListParameter param = new AddressListParameter();
		PrimaryKeyList pkList = ((Addresses)getSelection(searchResultTable, searchedAddresses)).getPkList();
		for(int i = 0; i < ((EntityListTableModel) selectedResultTable.getModel()).getRowCount(); i++){
			pkList.add(new Integer(((Entity) movedAddresses.getEntityAt(i)).getId()));
		}
		param.setPkFilter(pkList);
		movedAddresses.setAddressListParameter(param);
	}
	
	/**
	 * removes the specified rows from the selection
	 * @param selectedRows an integer array that specifies the rows to remove
	 */
	public void removeFromSelectedList(){
		EntityList list = getSelection(selectedResultTable, movedAddresses);
		AddressListParameter param = new AddressListParameter();
		PrimaryKeyList pkList = movedAddresses.getPkList();
		for(int i = 0; i < list.getSize(); i++){
			pkList.remove(new Integer(((Entity)list.getEntityAt(i)).getId()));
		}
		param.setPkFilter(pkList);
		movedAddresses.setAddressListParameter(param);
	}
	
	public void removeAllFromSelectedList(){
		AddressListParameter params = new AddressListParameter();
		params.setFilterList(Arrays.asList(new Object[]{new ListFilterPropertyName(Address.PROPERTY_ID.getKey()), new Integer(-1), ListFilterOperator.LIKE}));
		movedAddresses.setAddressListParameter(params);
	}
	
	public void moveAllToSelection(){
		movedAddresses.setAddressListParameter(searchedAddresses.getAddressListParameter());
	}
	
	public void mouseClicked(MouseEvent e){
		// double click
		if ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 2)){
			if (e.getSource()==searchResultTable){
				selectedResultTable.clearSelection();
				moveSelectedToRight();
			} 
			else if (e.getSource()==selectedResultTable){
				searchResultTable.clearSelection();
				removeFromSelectedList();
			}
		}		
	}
	
	//makes sure, that the headline shows the right count of entities
	public void tableChanged(TableModelEvent e) {		
		if (e.getSource()==searchResultTable.getModel()){
			putHeadline(labelSearchResultCount,resultHeadlinePrefix,searchResultTable.getModel());
		} else if (e.getSource()==selectedResultTable.getModel()){
			putHeadline(labelSelectedCount,selectionHeadlinePrefix,selectedResultTable.getModel());
		}
	}

	private void setAddressList(Addresses addressList, JTable table) {
		if (addressList != null) {
            asyncList = (AsyncEntityListImpl) addressList;
            table.setModel(new EntityListTableModel(asyncList, columnDescriptions));
        } else {
        	table.setModel(null);
        }		
	}
	
}
