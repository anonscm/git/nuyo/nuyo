/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.groupware.AddressProperty;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

/**
 * 
 * A simple dialog for inserting Tags, e.g. in an E-Mail.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class TagInsertDialog extends JDialog
{
	protected DispatchEMailDialog parent;
	protected TarentLogger logger = new TarentLogger(TagInsertDialog.class);
	protected JList tagList;
	protected JCheckBox showAllBox;
	protected List favoriteTags;
	protected JPopupMenu contextMenuDelete;
	protected JPopupMenu contextMenuAdd;
	protected Point lastClickPoint;
	protected JPanel mainPanel;
	protected JMenuItem addItem;
	protected JMenuItem deleteItem;
	
	public TagInsertDialog(DispatchEMailDialog pParent)
	{
		super(pParent, false);
		setTitle(Messages.getString("TAG_INSERT_DIALOG_TITLE"));
		parent = pParent;

		try
		{
			setAlwaysOnTop(true);
		}
		catch(NoSuchMethodError pExcp)
		{
			logger.warningSilent("We do not use a Java-Version >=5 VM. Cannot set AlwaysOnTop");
		}

		favoriteTags = stringToAddressPropertyList(ConfigManager.getPreferences().get("favoriteEMailTags", ""));

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(getMainPanel(), BorderLayout.CENTER);
		addWindowListener(new TagInsertDialogWindowListener());
		
		// we also need to react when the parent dialog is closed in order to store the favorite tags
		parent.addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e)
			{
				saveFavoriteTagList();
				super.windowClosing(e);
			}
			
		});
		
		setSize(240, 300);
		
		// dialog should be positioned at right screen-border
		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((int)screenDim.getWidth()-(getWidth()+20), getHeight());
		
		// this window should be a toolbox-window only.
		// user can type the e-mail text and insert tags without needing to switch the windows 
		setFocusableWindowState(false);
		
		// If we already have favorite tags, show only them
		if(favoriteTags != null && favoriteTags.size() > 0)
			((MutableListModel)getTagList().getModel()).setData(favoriteTags);
	}
	
	private List stringToAddressPropertyList(String pFavoriteTagsString)
	{	
		List propList = new ArrayList();
		
		String[] favoriteTagsSplitted = pFavoriteTagsString.split(",");
		
		for(int i=0; i < favoriteTagsSplitted.length; i++)
		{
			String favoriteTag = favoriteTagsSplitted[i];
			
			AddressProperty prop = AddressProperty.lookup(favoriteTag);

			if(prop != null) propList.add(prop);
		}
		return propList;
	}
	
	private String addressPropertyListToString(List pAddressProperties)
	{
		String result = "";
		
		Iterator it = pAddressProperties.iterator();
		
		while(it.hasNext())
		{
			AddressProperty prop = (AddressProperty)it.next();
			result += prop.getKey() + ",";
		}
		if(result.length() > 0)	
			return result.substring(0, result.length()-1);
		else return result;
	}

	private JPanel getMainPanel()
	{
		if(mainPanel == null)
		{
			FormLayout layout = new FormLayout(
					"pref:grow", // columns
			"pref:grow, pref"); // rows
	
	
			PanelBuilder builder = new PanelBuilder(layout);
	
			CellConstraints cc = new CellConstraints();
	
			builder.add(new JScrollPane(getTagList()), cc.xy(1, 1, CellConstraints.FILL, CellConstraints.FILL));
			builder.add(getShowAllBox(), cc.xy(1, 2));
			
			mainPanel = builder.getPanel();
		}
		return mainPanel;
	}

	private JCheckBox getShowAllBox()
	{
		if(showAllBox == null)
		{
			showAllBox = new JCheckBox(Messages.getString("TAG_INSERT_DIALOG_SHOW_ALL_TAGS_LABEL"));
			showAllBox.setToolTipText(Messages.getString("TAG_INSERT_DIALOG_SHOW_ALL_TAGS_TOOLTIP"));
			if(favoriteTags != null && favoriteTags.size() > 0)
				showAllBox.setSelected(false);
			else
				showAllBox.setSelected(true);
			showAllBox.addActionListener(new TagInsertActionListener());
		}
		return showAllBox;
	}

	private JList getTagList()
	{
		if(tagList == null)
		{	
			// Always initialize list with the complete tag-list for getting proper preferred-sizes later
			tagList = new JList(new MutableListModel(AddressProperty.getReplacePropertiesSorted()));
			
			tagList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tagList.addMouseListener(new TagInsertMouseListener());
			tagList.addMouseMotionListener(new TagInsertMouseMotionListener());
			tagList.setCellRenderer(new TagListCellRenderer());

			// setup the context-menu for deleting tags from  favorites-list
			contextMenuDelete = new JPopupMenu();
			deleteItem = new JMenuItem((ImageIcon)SwingIconFactory.getInstance().getIcon("editdelete.png"));
			deleteItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent pEvent)
				{
					// ensure that lastClickPoint is not null (should not happen... normally)
					if(lastClickPoint != null)
					{
						// get list-element which is assigned to this mouse-click-point
						int index = tagList.locationToIndex(lastClickPoint);
						
						if(index != -1)
							((MutableListModel)tagList.getModel()).removeElementAt(index);
					}
					
					// selection does not make much sense here, complete user-interaction is based on "single-click-events"
					getTagList().clearSelection();
				}
			});
			contextMenuDelete.add(deleteItem);
			
			
			// setup the context-menu for adding tags to the favorites list
			contextMenuAdd = new JPopupMenu();
			addItem = new JMenuItem((ImageIcon)SwingIconFactory.getInstance().getIcon("bookmark_add.png"));
			addItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// ensure that lastClickPoint is not null (should not happen... normally)
					if(lastClickPoint != null)
					{
						// get list-element which is assigned to this mouse-click-point
						int index = tagList.locationToIndex(lastClickPoint);
						
						if(index != -1)
							favoriteTags.add(((MutableListModel)tagList.getModel()).getElementAt(index));
					}
					
					// selection does not make much sense here, complete user-interaction is based on "single-click-events"
					getTagList().clearSelection();
				}
			});
			contextMenuAdd.add(addItem);
			
			// and we need an option to cancel operation when popup-menus are shown
			// does nothing than making the popup go away and clearing selection
			// is there an simple implementation of Action, which is _not_ abstract?? Searched a way, not to duplicate code but.. you see the result..
			
			JMenuItem cancelAddItem = new JMenuItem(Messages.getString("GUI_CLOSE_CONTEXT_MENU"));
			cancelAddItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					getTagList().clearSelection();
				}
			});
			// and we put a nice seperator in it.
			contextMenuDelete.addSeparator();
			contextMenuDelete.add(cancelAddItem);
			
			
			JMenuItem cancelDeleteItem = new JMenuItem(Messages.getString("GUI_CLOSE_CONTEXT_MENU"));
			cancelDeleteItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					getTagList().clearSelection();
				}
			});
			// and we put a nice seperator in it.
			contextMenuAdd.addSeparator();
			contextMenuAdd.add(cancelDeleteItem);
			
		}
		return tagList;

	}
	
	public void jumpToEntry(String pEntry)
	{
		for(int i=0; i < getTagList().getModel().getSize(); i++)
		{
			String entry = ((AddressProperty)getTagList().getModel().getElementAt(i)).getLabel();
			if(entry.toLowerCase().startsWith(pEntry.toLowerCase()))
			{
				getTagList().setSelectedIndex(i);
				getTagList().scrollRectToVisible(new Rectangle(getTagList().indexToLocation(i), getTagList().getBounds().getSize()));
				return;
			}
		}
	}
	
	private class TagInsertMouseMotionListener extends MouseMotionAdapter
	{
		public void mouseMoved(MouseEvent e)
		{
			// highlight the entry under the mouse-cursor
			int index = getTagList().locationToIndex(e.getPoint());
			getTagList().getSelectionModel().addSelectionInterval(index, index);
		}
	}
	
	public String getReplacementExpressionOfSelectedEntry()
	{
		AddressProperty prop = (AddressProperty) getTagList().getModel().getElementAt(getTagList().getSelectedIndex());
		if(prop != null)
			return prop.getReplacementExpression();
		return "";
	}

	private class TagInsertMouseListener extends MouseAdapter
	{

		public void mouseExited(MouseEvent e)
		{
			getTagList().clearSelection();
		}

		public void mouseClicked(MouseEvent pEvent)
		{
			if(SwingUtilities.isLeftMouseButton(pEvent))
			{
				// the index of the element, the user clicked on
				final int index = getTagList().locationToIndex(pEvent.getPoint());
				
				// get element the user clicked on
				AddressProperty prop = (AddressProperty)getTagList().getModel().getElementAt(index);

				// insert tag into text
				parent.insertTextAtCurrentPosition(prop.getReplacementExpression());
				
				// selection does not make much sense here, complete user-interaction is based on "single-click-events"
				//getTagList().clearSelection();

				// check if it is already in favorites list, otherwise add
				if(!favoriteTags.contains(prop)) favoriteTags.add(prop);
				
				// show a nice gui-feedback. rapidly selects and deselects the inserted entry, like an echo
				/* currently deactivated because not as nice as expected any more, after selection follows mouse-cursor
				new Thread(){

					public void run()
					{
						// half a second
						int totalTime = 500;
						// 10 ms
						int stepTime = 100;
						try
						{
							while(totalTime > 0)
							{
								getTagList().getSelectionModel().addSelectionInterval(index, index);
								Thread.sleep(stepTime/2);
								getTagList().getSelectionModel().removeSelectionInterval(index, index);
								Thread.sleep(stepTime/2);
								totalTime = totalTime-stepTime;
							}
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					
				}.start();
				*/
			}
		}
		
		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }
	    
	    private void maybeShowPopup(MouseEvent pEvent)
	    {
			// if the user did a mouse-click which should be interpreted as a popup-call (system-dependend), show a popup-menu, depending on current context (favorites/all)
	    	if(pEvent.isPopupTrigger())
	    	{
	    		// save the mouse-location
				lastClickPoint = pEvent.getPoint();
				
				// set status of clicked item to "selected"
				getTagList().setSelectedIndex(getTagList().locationToIndex(lastClickPoint));
				
				// get the selected AddressProperty
				AddressProperty prop = ((AddressProperty)getTagList().getSelectedValue());
				
				// show popup-menu
				// if we are showing all tags, show context-menu "add to favorites"
				if(getShowAllBox().isSelected())
				{
					// replace text of menu item to include current tag-name
					addItem.setText(Messages.getFormattedString("TAG_INSERT_DIALOG_ADD_TO_FAVORITE_LIST", prop.getLabel()));
					
					// need to reconfigure width of popup-menu
					contextMenuAdd.doLayout();
					
					contextMenuAdd.show(pEvent.getComponent(), pEvent.getX(), pEvent.getY());
				}
				// otherwise, if we are showing the favorites-list, show context-menu "delete from favorites"
				else
				{
					// replace text of menu item to include current tag-name
					deleteItem.setText(Messages.getFormattedString("TAG_INSERT_DIALOG_REMOVE_FROM_FAVORITE_LIST", prop.getLabel()));
					
					// need to reconfigure width of popup-menu
					contextMenuDelete.doLayout();
					
					contextMenuDelete.show(pEvent.getComponent(), pEvent.getX(), pEvent.getY());
				}
	    	}
	    }
	}

	private class TagListCellRenderer extends DefaultListCellRenderer
	{
		// Just show the property-label instead of the normal toString() which would deliver the property-key
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
		{
			if(value instanceof AddressProperty)
				return super.getListCellRendererComponent(list, ((AddressProperty)value).getLabel(), index, isSelected, cellHasFocus);
			return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		}
	}

	private class TagInsertActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(getShowAllBox().isSelected())
				((MutableListModel)getTagList().getModel()).setData(AddressProperty.getReplacePropertiesSorted());
			else
				((MutableListModel)getTagList().getModel()).setData(favoriteTags);
			
			// do not select any entry after switch. is confusing
			getTagList().clearSelection();
		}
	}
	
	protected void saveFavoriteTagList()
	{
		// save favorite tags in Preferences
		ConfigManager.getPreferences().put("favoriteEMailTags", addressPropertyListToString(favoriteTags));
	}
	
	private class TagInsertDialogWindowListener extends WindowAdapter
	{
		public void windowClosing(WindowEvent e)
		{
			// save favorite tags in Preferences
			TagInsertDialog.this.saveFavoriteTagList();
			super.windowClosing(e);
			// "Tag-Insert"-Buttons should be deselected as they should represent the state of this window
			ActionRegistry.getInstance().getAction("mail.tag.insert").setSelected(false);
		}
	}
}
