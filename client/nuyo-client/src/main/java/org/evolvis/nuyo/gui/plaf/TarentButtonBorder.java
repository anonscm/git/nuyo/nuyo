package org.evolvis.nuyo.gui.plaf;

import java.awt.Color;
import java.awt.Insets;

import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

public class TarentButtonBorder extends CompoundBorder{
	
	public final static int RAISED = BevelBorder.RAISED;
	public final static int LOWERED = BevelBorder.LOWERED;
	
	public Insets insets = new Insets(1,10,1,10);
	
	private static Color highlightInner = Color.WHITE;
	private static Color highlightOuter = Color.LIGHT_GRAY;
	
	//private static Color highlightInner = Color.LIGHT_GRAY;
	//private static Color highlightOuter = Color.WHITE;
	
	private static Color shadowInner = Color.GRAY;
	private static Color shadowOuter = Color.DARK_GRAY;
	
	//private Border outerBorder;
	//private Border innerBorder=new EmptyBorder(1,10,1,10);
	
		
	public TarentButtonBorder(){
		this(TarentButtonBorder.RAISED);
	}
	
	public TarentButtonBorder(int borderType){
		super();
		//outsideBorder= new BevelBorder(borderType);
		outsideBorder= new BevelBorder(borderType,highlightOuter,highlightInner, shadowOuter, shadowInner);
		//outsideBorder= new BevelBorder(borderType,highlightOuter,highlightInner);
		insideBorder = new EmptyBorder(insets);
		
	}
	
	/*public Insets getBorderInsets(Component c){
		return insets;
	}
	public Border getInsideBorder(){
		return innerBorder;
	}
	public Border getOutsideBorder(){
			
		return outerBorder;
	}*/
	

}
