/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataSourceInterfaces;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.Events.TarentGUISourceChangedEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueChangedEvent;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class DataSourceAdapter implements DataSource, TarentGUIEventListener
{
  protected DataContainer m_oDataContainer;
  protected String m_sDBKey = null;
  protected List m_oParameterList = new ArrayList();
  
  public DataSourceAdapter()
  {
    m_oDataContainer = null;    
  }

  public void setDBKey(String key)
  {
    m_sDBKey = key;
  }

  public String getDBKey()
  {
    return m_sDBKey;
  }

  public abstract boolean writeData(PluginData data);
  public abstract boolean readData(PluginData data);  
  public abstract boolean addParameter(String key, String value);
  public abstract boolean canHandle(Class data);

  private PluginData m_oPluginData = null; 
  public void setSource(PluginData plugindata)
  {
    m_oPluginData = plugindata;
  }
  
  public PluginData getSource()
  {
    return m_oPluginData;    
  }
  
  // ---------------------------------------------
  
  public List getEventsConsumable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_SOURCECHANGED);
    list.add(TarentGUIEvent.GUIEVENT_STORE);
    list.add(TarentGUIEvent.GUIEVENT_FETCH);
    return list;
  }

  public List getEventsFireable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_VALUECHANGED);
    return list;
  }

  // ---------------------------------------------

  
  public void init()
  {
    addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_STORE, this);        
    addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_FETCH, this);        
    addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_SOURCECHANGED, this);        
  }

  public void dispose()
  {
    removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_STORE, this);        
    removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_FETCH, this);        
    removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_SOURCECHANGED, this);        
  }

  
  public void event(TarentGUIEvent tge)
  {    
    if (tge instanceof TarentGUISourceChangedEvent)
    {
      setSource(((TarentGUISourceChangedEvent)tge).getSource());
    }
    else if (TarentGUIEvent.GUIEVENT_STORE.equals(tge.getName()))
    {
      // 24.10.05 Sebastian:
      // Hinzugef�gt, um nicht immer die Daten der referenzierten 
      // PluginData schreiben zu m�ssen, sondern auch ein Datenobjekt in dem Event mitgeben zu k�nnen
      PluginData source = getSource();
      if (tge.getPluginData() != null)
          source = tge.getPluginData();
      // Store
      if (source != null)
      {
        if (!(writeData(source)))
        {
          System.err.println("unable to store data with key \"" + getDBKey() + "\" because unknown fieldkey");
          getDataContainer().getLogger().warning("unable to store data with key \"" + getDBKey() + "\" because unknown fieldkey");
        }
      }
      else
      {        
        getDataContainer().getLogger().warning("unable to store data with key \"" + getDBKey() + "\" because no valid destination");
      }
    }
    else if (TarentGUIEvent.GUIEVENT_FETCH.equals(tge.getName()))
    {

      // 24.10.05 Sebastian:
      // Hinzugef�gt, um nicht immer die Daten der referenzierten 
      // PluginData lesen zu m�ssen, sondern auch ein Datenobjekt in dem Event mitgeben zu k�nnen
      PluginData source = getSource();
      if (tge.getPluginData() != null)
          source = tge.getPluginData();

      // fetch
      if (source != null)
      {
        if (readData(source))
        {
          fireTarentGUIEvent(new TarentGUIValueChangedEvent(getDataContainer(), source.get(getDBKey())));
        }
        else
        {
          getDataContainer().getLogger().warning("unable to fetch data with key \"" + getDBKey() + "\" because unknown fieldkey");
        }
      }
      else
      {
        getDataContainer().getLogger().warning("unable to fetch data with key \"" + getDBKey() + "\" because no valid source");
      }
    }
  }

  // ---------------------------------------------
  
  
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().addTarentGUIEventListener(event, handler);
  }
    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().removeTarentGUIEventListener(event, handler);
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    getDataContainer().fireTarentGUIEvent(e);
  }

  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }
}
