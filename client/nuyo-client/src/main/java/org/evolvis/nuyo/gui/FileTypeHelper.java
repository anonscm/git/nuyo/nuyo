/*
 * Created on 01.06.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.ImageIcon;

/**
 * @author niko
 *
 */
public class FileTypeHelper
{
  private Map m_oFileTypes;
  private URL m_oImageFolder;
  
  public FileTypeHelper(URL imagefolder)
  {
    m_oImageFolder = imagefolder;
    m_oFileTypes = new HashMap();
    
    addFileType(new FileType("Portable Document Format", "PDF", "ein Format f�r formatierte Textdokumente von Adobe", new String[] {"pdf"}, loadIcon("filetype_pdf.gif"), "acroread"));
    addFileType(new FileType("Hypertext Markup Language", "HTML", "ein Format zur Darstellung von Inhalten des WorldWideWebs", new String[] {"htm", "html"}, loadIcon("filetype_html.gif"), "mozilla"));
    addFileType(new FileType("Word Document", "DOC", "ein Format f�r formatierten Text der Anwendung Word von MicroSoft", new String[] {"doc"}, loadIcon("filetype_doc.gif"), "msword"));
    addFileType(new FileType("Plain Text", "TXT", "ein Format f�r unformatierten Text", new String[] {"txt"}, loadIcon("filetype_ascii.gif"), "kwrite"));
    addFileType(new FileType("Motion Pictures Expert Group Audio Layer 3", "MP3", "ein komprimiertes Audio-Format", new String[] {"mp3"}, loadIcon("filetype_sound.gif"), "xmms"));
    addFileType(new FileType("MicroSoft Wave", "WAVE", "ein unkomprimiertes Audio-Format", new String[] {"wav"}, loadIcon("filetype_sound.gif"), "xmms"));
    addFileType(new FileType("Binary", "Bin", "ein unspezifiziertes Bin�r-Format", new String[] {"bin", "dat"}, loadIcon("filetype_binary.gif"), "khexview"));
    addFileType(new FileType("Motion Pictures Expert Group Video", "MPEG", "ein komprimiertes Video-Format", new String[] {"mpg", "mpeg"}, loadIcon("filetype_video.gif"), "xine"));
    addFileType(new FileType("Motion Pictures Expert Group Video Layer 1", "MPEG1", "ein komprimiertes Video-Format nach dem MPEG1-Standard", new String[] {"mp1", "mpg1", "mpeg1"}, loadIcon("filetype_video.gif"), "xine"));
    addFileType(new FileType("Motion Pictures Expert Group Video Layer 2", "MPEG2", "ein komprimiertes Video-Format nach dem MPEG2-Standard", new String[] {"mp2", "mpg2", "mpeg2"}, loadIcon("filetype_video.gif"), "xine"));
    addFileType(new FileType("Audio & Video Interleaved", "AVI", "ein komprimiertes Video-Format", new String[] {"avi"}, loadIcon("filetype_video.gif"), "aviplay"));
    addFileType(new FileType("DIVX", "DIVX", "ein komprimiertes Video-Format", new String[] {"divx", "divx1", "divx2", "divx3", "divx4", "divx5", "divx6"}, loadIcon("filetype_video.gif"), "aviplay"));    
    addFileType(new FileType("Executable", "EXE", "eine ausf�hrbare Datei", new String[] {"exe"}, loadIcon("filetype_exe.gif"), ""));    
    addFileType(new FileType("ZIP", "ZIP", "eine Zip-Archiv-Datei", new String[] {"zip"}, loadIcon("filetype_archive.gif"), "ark"));    
    addFileType(new FileType("RAR", "RAR", "eine Rar-Archiv-Datei", new String[] {"rar"}, loadIcon("filetype_archive.gif"), "ark"));    
    addFileType(new FileType("Gnu Zip", "GZip", "eine Gnu-Zip-Archiv-Datei", new String[] {"gz"}, loadIcon("filetype_archive.gif"), "ark"));    
    addFileType(new FileType("Tar Gnu Zip", "TarGZip", "eine Gnu-Zip gepackte Tar-Archiv-Datei", new String[] {"tgz", "tar.gz"}, loadIcon("filetype_archive.gif"), "ark"));    
    addFileType(new FileType("BZ2", "BZ2", "eine Archiv-Datei", new String[] {"bz2"}, loadIcon("filetype_archive.gif"), "ark"));            
    addFileType(new FileType("GIF", "GIF", "eine GIF-Bild-Datei", new String[] {"gif"}, loadIcon("filetype_image.gif"), "kwickshow"));    
    addFileType(new FileType("Bitmap", "BMP", "eine Bitmap-Bild-Datei", new String[] {"bmp"}, loadIcon("filetype_image.gif"), "kwickshow"));    
    addFileType(new FileType("Portable Network Graphic", "PNG", "eine PNG-Bild-Datei", new String[] {"png"}, loadIcon("filetype_image.gif"), "kwickshow"));    
    addFileType(new FileType("Joint Photographic Expert Group", "JPEG", "eine JPEG-Bild-Datei", new String[] {"jpeg", "jpg"}, loadIcon("filetype_image.gif"), "kwickshow"));    
    addFileType(new FileType("Java Archiv", "JAR", "ein Archiv mit Java-Dateien", new String[] {"jar"}, loadIcon("filetype_java.gif"), "kwickshow"));    
    addFileType(new FileType("Java Web-Archiv", "WAR", "ein Archiv mit Java-Web-Dateien", new String[] {"war"}, loadIcon("filetype_java.gif"), "kwickshow"));    
    addFileType(new FileType("Java Source", "Java", "eine Datei mit Java-Quellcode", new String[] {"java"}, loadIcon("filetype_java.gif"), "kwickshow"));    
  }
  
  public FileType getFileTypeOfFilename(String filename)  
  {
    String name = filename.trim().toUpperCase();

    Iterator typesiterator = m_oFileTypes.entrySet().iterator();
    while(typesiterator.hasNext())
    {  
      Map.Entry entry = (Map.Entry)(typesiterator.next());
      FileType filetype = (FileType)(entry.getValue());
      
      if (filetype.isValidExtension(name)) return filetype;
    }
    return null;
  }
  

  private void addFileType(FileType filetype)
  {
    m_oFileTypes.put(filetype.getShortName(), filetype);
  }
  
  public ImageIcon getIconForUnknownFileType()
  {
    return loadIcon("filetype_unknown.gif");
  }
  
  private ImageIcon loadIcon(String filename)
  {
    try
    {
      return new ImageIcon(new URL(m_oImageFolder, filename));
    } 
    catch (MalformedURLException e)
    {
      return null;
    }
  }
  
  
  // ------------------------------------------------------------------------------
  
  
  public class FileType
  {
    private String[] m_sExtensions;
    private String m_sName;
    private String m_sShortName;
    private String m_sDescription;
    private ImageIcon m_oIcon;
    private String m_sDefaultApplication;
    
    public FileType(String name, String shortname, String description, String[] extensions, ImageIcon icon, String defaultapplication)
    {
      setName(name);
      setShortName(shortname);
      setDescription(description);
      setExtensions(extensions);
      setIcon(icon);
      setDefaultApplication(defaultapplication);
    }
    
    public String getDefaultApplication()
    {
      return m_sDefaultApplication;
    }

    public void setDefaultApplication(String defaultApplication)
    {
      m_sDefaultApplication = defaultApplication;
    }

    public ImageIcon getIcon()
    {
      return m_oIcon;
    }

    public void setIcon(ImageIcon icon)
    {
      m_oIcon = icon;
    }

    public String getDescription()
    {
      return m_sDescription;
    }

    public void setDescription(String description)
    {
      m_sDescription = description;
    }

    public boolean isValidExtension(String filename)
    {
      if (filename == null) return false;
      
      String testname = filename.trim().toUpperCase();
      for(int i=0; i<(m_sExtensions.length); i++)
      {  
        if (testname.endsWith(m_sExtensions[i])) return true;
      }
      return false;      
    }
    
    public String[] getExtensions()
    {
      return m_sExtensions;
    }

    public void setExtensions(String[] extensions)
    {
      m_sExtensions = new String[extensions.length];
      for(int i=0; i<(extensions.length); i++)
      {  
        m_sExtensions[i] = extensions[i].trim().toUpperCase();
      }
    }

    public String getName()
    {
      return m_sName;
    }

    public void setName(String name)
    {
      m_sName = name;
    }
  
    public String getShortName()
    {
      return m_sShortName;
    }

    public void setShortName(String name)
    {
      m_sShortName = name;
    }
  }
    
}
