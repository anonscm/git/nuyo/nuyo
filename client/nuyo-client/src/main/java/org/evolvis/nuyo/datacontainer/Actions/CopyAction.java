/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Actions;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAction;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerActionAdapter;

/**
 * @author niko
 *
 */
public class CopyAction extends DataContainerListenerActionAdapter
{
  public DataContainerListenerAction cloneAction()
  {
    return new CopyAction();
  }

  public boolean doAction()
  {
    DataContainerListener listener = getListener();
    if (listener != null)
    {
      Object data = listener.getDataContainerToListenOn().getGUIData();
      listener.getDataContainer().getGUIElement().setData(data);      
      return true;
    }
    return false;
  }

  public void init()
  {
  }

  public void dispose()
  {
  }

  public boolean addParameter(String key, String value)
  {
    return false;
  }

  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.ACTION, "CopyAction", new DataContainerVersion(0));
    return d;
  }
}
