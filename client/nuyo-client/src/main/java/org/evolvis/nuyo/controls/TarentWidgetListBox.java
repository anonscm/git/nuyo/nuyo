/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Simon B�hler.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
/*
 * Created on 11.04.2003
 *
 */
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

/**
 * @author d-fens
 *
 */
public class TarentWidgetListBox extends JPanel implements TarentWidgetInterface 
{
  
  private JScrollPane       m_oScrollPane;
  private JList             m_oList;
 
  /**
   * @param arg0
   */
  public TarentWidgetListBox(ListModel arg0) 
  {
    super();
    m_oList = new JList(arg0);
    //m_oList.setCellRenderer(new MeinRenderer());
    m_oScrollPane = new JScrollPane(m_oList);
    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER);
  }
  
  /**
   * @param arg0
   */
  public TarentWidgetListBox(Object[] arg0) 
  {
    super();
    m_oList = new JList(arg0);
    //m_oList.setCellRenderer(new MeinRenderer());
    m_oScrollPane = new JScrollPane(m_oList);
    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER);
  }

  /**
   * @param arg0
   */
  public TarentWidgetListBox(Vector arg0) 
  {
    super();
    m_oList = new JList(arg0);
    m_oScrollPane = new JScrollPane(m_oList);
    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER);
  }

  public TarentWidgetListBox() 
  {
    super();
    m_oList = new JList();
    //m_oList.setCellRenderer(new MeinRenderer());
    m_oScrollPane = new JScrollPane(m_oList);
    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER); 
  }
    
  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setData(java.lang.Object)
   */
  public void setData(Object data) 
  {
  if (data instanceof Object[])
    {
        this.m_oList.setListData((Object[])data);
    }
  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#getData()
   */
  public Object getData() 
  {
   String text = "";
   for (int i=0; i<(this.m_oList.getModel().getSize()); i++)    
   {
     text += this.m_oList.getModel().getElementAt(i).toString() + ";";
   }
   return(text);
  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#getComponent()
   */
  public JComponent getComponent() 
  {
    return (this);
  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setLengthRestriction(int)
   */
  public void setLengthRestriction(int maxlen) 
  {
  

  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setWidgetEditable(boolean)
   */
  public void setWidgetEditable(boolean iseditable) 
  {
  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#isEqual(java.lang.Object)
   */
  public boolean isEqual(Object data) 
  {
    return false;
  }

  public void setSelectionMode (int mode)
  {
    m_oList.setSelectionMode(mode);
  }
  
  /**
   * @param arg0
   */
  public void setVisibleRowCount(int arg0)
  {
    m_oList.setVisibleRowCount(arg0);
  }

  public void setFont(Font font)
  {
    if (m_oList != null) m_oList.setFont(font);
  }
  
  public JList getJList()
  {
    return m_oList;
  }
  
  public JScrollPane getJScrollPane()
  {
    return m_oScrollPane;
  }
  
  //ToolTip Extension
  class MeinRenderer extends DefaultListCellRenderer {
    public Component getListCellRendererComponent(
      JList list,
      Object value,
      int index,
      boolean isSelected,
      boolean cellHasFocus) {
      Component comp = 
       super.getListCellRendererComponent(list, value,index, isSelected,cellHasFocus);
      if (comp instanceof JComponent && value != null) {
          ((JComponent) comp).setToolTipText(value.toString());
      }
      return comp;
    }
  };

  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  

}

  