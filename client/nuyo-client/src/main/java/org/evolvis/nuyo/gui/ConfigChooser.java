/* $Id: ConfigChooser.java,v 1.5 2007/01/08 17:56:05 aleksej Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.tarent.commons.ui.EscapeDialog;

/**
 * @author mikel
 */
public class ConfigChooser extends EscapeDialog 
{
    private Font m_oFont;
    private Color m_oBGColor;
    private Color m_oButtonBGColor;
    private Color m_oButtonFGColor;
    

    /**
     * Diese Schnittstelle wird ben�tigt, um Konfigurationen bearbeiten zu k�nnen.
     */
    public interface Editor 
    {
        public void edit(File configFile);
    }


    /**
     * Konstructor f�r ConfigChooser.
     * 
     * @throws HeadlessException
     */
    public ConfigChooser(String appTitle, File[] configs) throws HeadlessException 
    {
        super((JFrame)null, appTitle + Messages.getString("GUI_ConfigChooser_Titel_Konfigurationsauswahl")); //$NON-NLS-1$
        //configEditor = editor;
        applicationTitle = appTitle;
        initMap(configs);

        m_oFont = new Font(null, Font.PLAIN, 12);
        m_oBGColor = new Color(0xA7, 0xB1, 0xCA);
        m_oButtonBGColor = new Color(0x46, 0x64, 0x9e);
        m_oButtonFGColor = Color.WHITE;    

        setBackground(m_oBGColor);
        
        lstConfigs = new JList(entries.keySet().toArray());
        lstConfigs.setFont(m_oFont);
        lstConfigs.addListSelectionListener(new ListSelectionListener()
        {
          public void valueChanged(ListSelectionEvent lse)
          {             
            btnSelect.setEnabled((lstConfigs.getSelectedIndex() >= 0));
            //btnEdit.setEnabled((lstConfigs.getSelectedIndex() >= 0));
          }
        });

        btnSelect = new JButton(Messages.getString("GUI_ConfigChooser_Button_Auswaehlen")); //$NON-NLS-1$
        btnSelect.setFont(m_oFont);
        btnSelect.setBackground(m_oButtonBGColor);
        btnSelect.setForeground(m_oButtonFGColor);       
        btnSelect.setEnabled(false);
        btnSelect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (lstConfigs.getSelectedIndex() >= 0) {
                    lookupSelectedEntry();
                    closeWindow();
                }
            }
        });
/*        
        btnEdit = new JButton(Messages.getString("GUI_ConfigChooser_Button_Editieren")); //$NON-NLS-1$
        btnEdit.setFont(m_oFont);
        btnEdit.setBackground(m_oButtonBGColor);
        btnEdit.setForeground(m_oButtonFGColor);       
        btnEdit.setEnabled(false);
        btnEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (lstConfigs.getSelectedIndex() >= 0) {
                    lookupSelectedEntry();
                    editSelected = true;
                    configEditor.edit(selectedFile);
                    selectedFile = null;
                }
            }
        });
*/        

        btnCancel = new JButton(Messages.getString("GUI_ConfigChooser_Button_Cancel")); //$NON-NLS-1$
        btnCancel.setMnemonic(KeyEvent.VK_ESCAPE);
        btnCancel.setFont(m_oFont);
        btnCancel.setBackground(m_oButtonBGColor);
        btnCancel.setForeground(m_oButtonFGColor);       
        btnCancel.setEnabled(true);
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
              closeWindow();
            }
        });

        JPanel pnlButtons = new JPanel();
        pnlButtons.setBackground(m_oBGColor);
        pnlButtons.add(btnSelect);
        //if (editor != null) pnlButtons.add(btnEdit);
        pnlButtons.add(btnCancel);
        
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(lstConfigs, BorderLayout.CENTER);
        getContentPane().add(pnlButtons, BorderLayout.SOUTH);
        pack();

        getRootPane().setDefaultButton(btnSelect);
        setModal(true);
        setLocationRelativeTo(null);
    }

    public File selectFile() {
        setVisible(true);
        return selectedFile;
    }

    public boolean isSelectedForEdit() {
        return editSelected;
    }

    /*
     * GUI-Komponenten
     */
    protected JList lstConfigs = null;
    protected JButton btnSelect = null;
    protected JButton btnEdit = null;
    protected JButton btnCancel = null;

    /*
     * lokale Variablen
     */
    protected SortedMap entries = null;

    protected File selectedFile = null;

    protected boolean editSelected = false;
    
    protected Editor configEditor = null;

    protected String applicationTitle = null;
    /**
     * Diese Methode initialisiert die Variable {@link #entries entries} aus
     * dem �bergebenen Datei-Feld.
     * 
     * @param configs enth�lt die initial zur Auswahl stehenden Konfigurationen.
     */
    private void initMap(File[] configs) {
        entries = new TreeMap();
        for (int i = 0; i < configs.length; i++)
            entries.put(configs[i].getName(), configs[i]);
    }
    
    private void lookupSelectedEntry() {
        selectedFile = (File) entries.get(lstConfigs.getModel().getElementAt(lstConfigs.getSelectedIndex()));
    }
}
