package org.evolvis.nuyo.plugins.calendar;


/**
 * General calendar exception with it's description.
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class CalendarException extends Exception {

    private static final long serialVersionUID = 1885587997884146110L;

    public CalendarException() {
        super();
    }

    public CalendarException( String message, Throwable cause ) {
        super( message, cause );
    }

    public CalendarException( String message ) {
        super( message );
    }

    public CalendarException( Throwable cause ) {
        super( cause );
    }
}
