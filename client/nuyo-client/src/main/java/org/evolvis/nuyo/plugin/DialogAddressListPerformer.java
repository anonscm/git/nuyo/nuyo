package org.evolvis.nuyo.plugin;

import javax.swing.JDialog;

public interface DialogAddressListPerformer extends AddressListPerformer {
	
	public JDialog getDialog();

}
