/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class Adresszusatz3Field extends GenericTextField
{
  public Adresszusatz3Field()
  {
    super("ADRESSZUSATZ3", AddressKeys.ADRESSZUSATZ3, CONTEXT_ADRESS, "GUI_Fields_Adresszusatz3_ToolTip", "GUI_Fields_Adresszusatz3", 0);
  }
}
