/* $Id: StatusBar.java,v 1.23 2007/08/30 16:10:24 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyrightEnigmail
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui;

import java.awt.Font;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.binding.BeanBinding;
import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.ui.SmallTaskManagerPanel;
import de.tarent.commons.utils.TaskManager;

/**
 *  This status bar consists of a task progress bar on the left and several status labels on the right.
 *  <p>
 *  The status labels are bound to the {@link org.evolvis.nuyo.controller.ApplicationModel},
 *  which is also called <code>"The Binding Framework"</code>.
 *  The status labels are bound to appropriate properties as follows:
 *  <ul>
 *    <li> a count of loaded addresses: <code>selectedContactsCount = ApplicationModel.ADDRESS_LIST_KEY +".size"</code>,      
 *    <li> overall count of available addresses in a data base: <code>contactsCount = ApplicationModel.TOTAL_ADDRESS_COUNT_KEY</code>,
 *    <li> search filter status: <code>searchFilterActived = ApplicationModel.ADDRESS_LIST_PARAMETER_KEY +".searchFilterEnabled"</code>,
 *    <li> category filter status: <code>categoryFilterActived = ApplicationModel.ADDRESS_LIST_PARAMETER_KEY +".categoryFilterEnabled"</code>,
 *    <li> user login name: not bound (invoked only once after login completed).
 *  </ul>
 *  
 *  @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class StatusBar extends JPanel
    implements StatusListener {

    private static final Logger logger = Logger.getLogger(StatusBar.class.getName());

    private JLabel userNameLabel;
    private JLabel addressCountLabel;
    private JLabel filterLabel;
    private JLabel categoryFilterLabel;

    private final static String STATUS_ACTIVE = Messages.getString("StatusBar_LABEL_Active");
    private final static String STATUS_DISABLED = Messages.getString("StatusBar_LABEL_Disabled");
    
    private Integer totalAddressCount = new Integer(0);
    private Integer selectedAddressCount = new Integer(0);
    
    
    /**
     * Creates a panel with Formlayout (see {@link www.jgoodies.de}):
     * a task progress bar on the left and several status labels on the right.
     * @see SmallTaskManagerPanel
     */
    public StatusBar() {
    	/*
    	 * Note: The user-name label does not have a minimum size in order to
    	 * allow the TaskManagerPanel to be shown even on low screen-resolutions
    	 * and enable the user to cancel processes by the "cancel-button".
    	 * So if there is not enough space for all compontents in the status-bar
    	 * the user-name-label is the first to be hidden.
    	 * 
    	 */
    	FormLayout layout = new FormLayout("5dlu, pref, 25dlu, pref, 5dlu, pref, 25dlu, fill:0dlu:grow, right:max(p;100dlu), 5dlu",
        "center:min(pref;12dlu)");
        setLayout(layout);

        CellConstraints cc = new CellConstraints();
        addressCountLabel = createPlainLabel(Messages.getFormattedString("StatusBar_LABEL_Contacts", selectedAddressCount, totalAddressCount));
        filterLabel = createPlainLabel(Messages.getFormattedString("StatusBar_LABEL_SearchFilter", STATUS_DISABLED));
        categoryFilterLabel = createPlainLabel(Messages.getFormattedString("StatusBar_LABEL_CategoryFilter", STATUS_DISABLED));
        userNameLabel = createPlainLabel(Messages.getFormattedString("StatusBar_LABEL_User", "unknown"));
        
        add(addressCountLabel, cc.xy(2,1));
        add(filterLabel, cc.xy(4,1));
        add(categoryFilterLabel, cc.xy(6,1));
        add(userNameLabel, cc.xy(8,1));
        
        SmallTaskManagerPanel p = new SmallTaskManagerPanel();
        TaskManager.getInstance().addTaskListener(p);
        add(p, cc.xy(9,1));
    }
    
    private JLabel createPlainLabel(String text) {
    	JLabel label = new JLabel(text);
    	label.setFont(label.getFont().deriveFont(Font.PLAIN));
    	return label;
    }

    /**
     * Registers the status label bindigs at <code>"The Binding Framework"</code> (see {@link ApplicationModel}).
     */
    public void initBindings() {
        BindingManager bm = ApplicationServices.getInstance().getBindingManager();
        if(bm != null) {
            bm.addBinding(new BeanBinding(this, "searchFilterActived", ApplicationModel.ADDRESS_LIST_PARAMETER_KEY +".searchFilterEnabled" ));
            bm.addBinding(new BeanBinding(this, "categoryFilterActived", ApplicationModel.ADDRESS_LIST_PARAMETER_KEY +".categoryFilterEnabled" ));
            //Note: "The Binding Framework" supports hierarchical properties with "." notation
            //  That means the 'selectedContactsCount' property can be bound to {ADDRESS_LIST_KEY + ".size"}.  
            //  This property's value will be automatically updated 
            //  as soon as ADDRESS_LIST_KEY property value will be changed.
            //  This is the case when ApplicationModel.setAddressList() is invoked.
            bm.addBinding(new BeanBinding(this, "selectedContactsCount", ApplicationModel.ADDRESS_LIST_KEY +".size" ));
            bm.addBinding(new BeanBinding(this, "contactsCount", ApplicationModel.TOTAL_ADDRESS_COUNT_KEY ));
        } else logger.warning("[!] status labels not bound.");
    }
    
    /**
     * Sets a user login name as a text for the according label.
     * @param userName a text to be displayed
     */
    public void setUserName( String userName ) {
        if(userName == null) logger.warning("[statusBar]: can't set empty user name");
        userNameLabel.setText(Messages.getFormattedString("StatusBar_LABEL_User", userName));
    }

    /**
     * Sets an activated or deactivated status of the search filter.
     * <p>
     * The method's signature was designed according to the JavaBeans convetions
     * for <code>searchFilterActived</code> property.
     * <p>
     * It will be invoked by <code>"The Binding Framework"</code> (see {@link ApplicationModel})
     * as soon as <code>ApplicationModel.ADDRESS_LIST_PARAMETER_KEY</code> property is changed.
     * The corresponding binding will be created in {@link #initBindings()} method.
     * @param 'true' for activated and 'false' for deactivated status 
     */
    public void setSearchFilterActived( boolean isActivated ) {
        filterLabel.setText(Messages.getFormattedString("StatusBar_LABEL_SearchFilter", isActivated ? STATUS_ACTIVE : STATUS_DISABLED));
    }

    /**
     * Sets an activated or deactivated status of the category filter.
     * <p>
     * The method's signature was designed according to the JavaBeans convetions
     * for <code>categoryFilterActived</code> property.
     * <p>
     * It will be invoked by <code>"The Binding Framework"</code> (see {@link ApplicationModel})
     * as soon as <code>ApplicationModel.ADDRESS_LIST_PARAMETER_KEY</code> property is changed.
     * The corresponding binding will be created in {@link #initBindings()} method.
     * @param 'true' for activated and 'false' for deactivated status 
     */
    public void setCategoryFilterActived( boolean isActivated ) {
        categoryFilterLabel.setText(Messages.getFormattedString("StatusBar_LABEL_CategoryFilter", isActivated ? STATUS_ACTIVE : STATUS_DISABLED));
    }

    
    /**
     * Sets the count of addresses shown in the table.
     * <p>
     * The method's signature was designed according to the JavaBeans convetions
     * for <code>selectedContactsCount</code> property.
     * <p>
     * It will be invoked by <code>"The Binding Framework"</code> (see {@link ApplicationModel})
     * as soon as <code>ApplicationModel.ADDRESS_LIST_KEY</code> property is changed.
     * The corresponding binding will be created in {@link #initBindings()} method.
     * @param # of loaded addresses 
     */
    public void setSelectedContactsCount( int isCount ) {
        selectedAddressCount = new Integer(isCount);
        updateAddressCountLabel();
    }
    
    /**
     * Sets overall count of addresses currently available in a data base.
     * <p>
     * The method's signature was designed according to the JavaBeans convetions
     * for <code>contactsCount</code> property.
     * <p>
     * It will be invoked by <code>"The Binding Framework"</code> (see {@link ApplicationModel})
     * as soon as <code>ApplicationModel.TOTAL_ADDRESS_COUNT_KEY</code> property is changed.
     * The corresponding binding will be created in {@link #initBindings()} method.
     * @param overall # of addresses 
     */
    public void setContactsCount( int allCount ) {
        totalAddressCount = new Integer(allCount);
        updateAddressCountLabel();
    }
    
    private void updateAddressCountLabel() {
    	addressCountLabel.setText(Messages.getFormattedString("StatusBar_LABEL_Contacts", selectedAddressCount, totalAddressCount));
    }
}