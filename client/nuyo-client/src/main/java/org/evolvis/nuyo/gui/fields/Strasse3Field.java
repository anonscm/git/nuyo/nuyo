/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Color;

import javax.swing.SwingConstants;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class Strasse3Field extends ContactAddressTextField
{
  private TarentWidgetTextField m_oTextfield_strasse;
  private TarentWidgetTextField m_oTextfield_hausnummer;
  private TarentWidgetLabel m_oLabel_strasse;
  private TarentWidgetLabel m_oLabel_hausnummer;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createStrassePanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "STRASSE3";
  }

  public void postFinalRealize()
  {    
  }  

  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      m_oTextfield_strasse.setEditable(true);
      m_oTextfield_hausnummer.setEditable(true);
    }
    else
    {  
      m_oTextfield_strasse.setEditable(false);
      m_oTextfield_strasse.setBackground(Color.WHITE);
      m_oTextfield_hausnummer.setEditable(false);
      m_oTextfield_hausnummer.setBackground(Color.WHITE);
    }
  }
 
  public void setData(PluginData data)
  {
    Object value = data.get(AddressKeys.STRASSE3);
    if (value != null) m_oTextfield_strasse.setText(value.toString());
    else m_oTextfield_strasse.setText("");
    
    value = data.get(AddressKeys.HAUSNUMMER3);
    if (value != null) m_oTextfield_hausnummer.setText(value.toString());
    else m_oTextfield_hausnummer.setText("");
  }

  public void getData(PluginData data)
  {
    data.set(AddressKeys.STRASSE3, m_oTextfield_strasse.getText());
    data.set(AddressKeys.HAUSNUMMER3, m_oTextfield_hausnummer.getText());
  }

  public boolean isDirty(PluginData data)
  {
    if (!((m_oTextfield_strasse.getText().equals("")) &&  (data.get(AddressKeys.STRASSE3) == null ))) //$NON-NLS-1$
      if (!(m_oTextfield_strasse.getText().equals(data.get(AddressKeys.STRASSE3)))) return(true);            

    if (!((m_oTextfield_hausnummer.getText().equals("")) &&  (data.get(AddressKeys.HAUSNUMMER3) == null ))) //$NON-NLS-1$
      if (!(m_oTextfield_hausnummer.getText().equals(data.get(AddressKeys.HAUSNUMMER3)))) return(true);            

    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------
  
  
  
  public String getFieldName()
  {
    return fieldName;     
  }
  
  public String getFieldDescription()
  {
    return fieldDescription;        
  }
  
  
  
  
  private String getFieldNameStrasse()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Strasse3");
    else return text;
  }

  private String getFieldNameHausnummer()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Hausnummer3");
    else return text;
  }


  
  private String getFieldDescriptionStrasse()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Strasse3_ToolTip");
    else return text;
  }
  
  private String getFieldDescriptionHausnummer()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_Fields_Hausnummer3_ToolTip");
    else return text;
  }
  
  
  
  
  public void setFieldName(String name)
  {
    fieldName = name;
    if (panel != null)
    {
      if (name != null) 
      {
        m_oLabel_strasse.setText(getFieldNameStrasse());
        m_oLabel_hausnummer.setText(getFieldNameHausnummer());
      }
    }
  }  
  

  public void setFieldDescription(String description)
  {
    fieldDescription = description;
    if (panel != null)
    {
      if (description != null) 
      {
        m_oTextfield_strasse.setToolTipText(getFieldDescriptionStrasse());
        m_oTextfield_hausnummer.setToolTipText(getFieldDescriptionHausnummer());
      }
    }
  }  
  

  
  private EntryLayoutHelper createStrassePanel(String widgetFlags)
  {
    m_oTextfield_strasse = createTextField(getFieldDescriptionStrasse(), 60);
    m_oLabel_hausnummer = new TarentWidgetLabel(getFieldNameHausnummer() + " ", SwingConstants.RIGHT); //$NON-NLS-1$
    m_oTextfield_hausnummer = createTextField(getFieldDescriptionHausnummer(), 10);
    m_oLabel_strasse = new TarentWidgetLabel(getFieldNameStrasse());

    return new EntryLayoutHelper(
                                   new TarentWidgetInterface[] {
                                                                m_oLabel_strasse,
                                                                m_oTextfield_strasse,
                                                                m_oLabel_hausnummer,
                                                                m_oTextfield_hausnummer },
                                   widgetFlags);
  }
  public void setDoubleCheckSensitive(boolean issensitive) {
	  setDoubleCheckSensitive(m_oTextfield_strasse, issensitive);
	  setDoubleCheckSensitive(m_oTextfield_hausnummer, issensitive);	
  } 
  
}
