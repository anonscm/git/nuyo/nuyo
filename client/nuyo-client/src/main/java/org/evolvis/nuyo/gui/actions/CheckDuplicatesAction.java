package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.DoubleCheckManager;
import org.evolvis.nuyo.logging.TarentLogger;


public class CheckDuplicatesAction extends AbstractContactDependentAction {

    private static final long serialVersionUID = -5908674861542521506L;
    private static final TarentLogger log = new TarentLogger(CheckDuplicatesAction.class);
    
    public void actionPerformed(ActionEvent e) {
        DoubleCheckManager.getInstance().checkForDoublesAndHandleResults();
    }

    public void init(){
        
    }
}