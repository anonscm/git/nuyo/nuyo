/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Listener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;


/**
 * @author niko
 *
 */
public abstract class DataContainerListenerAdapter implements DataContainerListener 
{
  protected DataContainer m_oDataContainer;
  protected List m_oParameterList = new ArrayList();
  protected List m_oActions = new ArrayList();
  private TarentGUIEvent m_oEvent = null;
  private String m_sSlot = null;
  protected DataContainerListenerEventListener m_oEventHandler = null;
  private DataContainer m_oDataContainerToListenOn = null;
  public abstract DataContainerListener cloneListener();
  public abstract DataContainerObjectDescriptor getDataContainerObjectDescriptor();

  public void init()
  {
    m_oEventHandler = new DataContainerListenerEventListener();
  }

  public void dispose()
  {
  }
  
  public class DataContainerListenerEventListener implements TarentGUIEventListener
  {
    public void event(TarentGUIEvent tge)
    {
      Iterator it = m_oActions.iterator();
      while(it.hasNext())
      {
        DataContainerListenerAction action = (DataContainerListenerAction)it.next();
        if (!action.doAction())
        {
          // ERROR: action ist fehlgeschlagen
          getDataContainer().getLogger().severe("Error while performing action  \"" + action.getDataContainerObjectDescriptor().getContainerName() + "\".");
        }
      }
    }

    public String getListenerName()
    {
      return "DataContainerListenerEventListener of " + getDataContainer().getFieldDescriptor().getName();
    }
  }
  
  
  public void addAction(DataContainerListenerAction action)
  {
    action.setDataContainer(getDataContainer());
    action.setListener(this);
    m_oActions.add(action);
  }

  public void removeAction(DataContainerListenerAction action)
  {
    m_oActions.remove(action);
    action.setDataContainer(null);
    action.setListener(null);
  }

  public void setEvent(TarentGUIEvent event)
  {
    m_oEvent = event;
  }

  public TarentGUIEvent getEvent()
  {
    return m_oEvent;
  }
  
  public void setSlot(String slot)
  {
    m_sSlot = slot;
  }

  public String getSlot()
  {
    return m_sSlot;
  }

  public boolean installListener()
  {
    if (m_sSlot != null)
    {
      if (m_oEvent != null)
      {
        m_oDataContainerToListenOn = getDataContainer().getDataContainerPool().getDataContainer(m_sSlot);
        if (m_oDataContainerToListenOn != null)
        {
          if (m_oDataContainerToListenOn.isEventFireable(m_oEvent))
          {
            m_oDataContainerToListenOn.addTarentGUIEventListener(m_oEvent.getName(), m_oEventHandler);
            getDataContainer().getLogger().config("\"" + getDataContainer().getKey() + "\" installed Listener \"" + getDataContainerObjectDescriptor().getContainerName() + "\" at DataContainer \"" + m_oDataContainerToListenOn.getKey() + "\" on event \"" + m_oEvent.getName() + "\"");            
            return true;
          }
          else
          {
            // ERROR: DataContainer can't fire required event
            getDataContainer().getLogger().severe("Error while adding listener to DataContainer \"" + getDataContainer().getKey() + "\": referenced DataContainer \"" + m_oDataContainerToListenOn.getFieldDescriptor().getName() + "\" can't fire requested event \"" + m_oEvent.getName() + "\".");
          }          
        }
        else
        {          
          // ERROR: DataContainer not found
          getDataContainer().getLogger().severe("Error while adding listener to DataContainer \"" + getDataContainer().getKey() + "\": referenced DataContainer \"" + m_sSlot + "\" not found.");
        }
      }
      else
      {
        // ERROR: no event
        getDataContainer().getLogger().severe("Error while adding listener to DataContainer \"" + getDataContainer().getKey() + "\": no event configured.");
      }      
    }
    else
    {
      // ERROR: no slot
      getDataContainer().getLogger().severe("Error while adding listener to DataContainer \"" + getDataContainer().getKey() + "\": no slot configured.");
    }
    
    return false;
  }

  
  public boolean disposeListener()
  {
    if (m_oDataContainerToListenOn != null)
    {
      m_oDataContainerToListenOn.removeTarentGUIEventListener(m_oEvent.getName(), m_oEventHandler);

      getDataContainer().getLogger().config("\"" + getDataContainer().getKey() + "\" removed Listener \"" + getDataContainerObjectDescriptor().getContainerName() + "\" at DataContainer \"" + m_oDataContainerToListenOn.getKey() + "\" on event \"" + m_oEvent.getName() + "\"");            
      
      m_oDataContainerToListenOn = null;
      return true;
    }
    return false;
  }

  public boolean addParameter(String key, String value)
  {
    return false;
  }


  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }

  public DataContainer getDataContainerToListenOn()
  {
    return m_oDataContainerToListenOn;
  }

  public List getEventsConsumable()
  {
    return null;
  }

  public List getEventsFireable()
  {
    return null;
  }

  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
  }

  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
  }

  public void event(TarentGUIEvent tge)
  {
  }

  public String getListenerName()
  {
    return null;
  }
}
