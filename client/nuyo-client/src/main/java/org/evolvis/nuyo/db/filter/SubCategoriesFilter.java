/*
 * $Id: SubCategoriesFilter.java,v 1.5 2007/08/30 16:10:33 fkoester Exp $
 * 
 * Created on 25.05.2004
 */
package org.evolvis.nuyo.db.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.evolvis.nuyo.db.MailingList;
import org.evolvis.nuyo.db.SubCategory;


/**
 * Diese Klasse stellt einen Filter dar, der eine Sammlung von Unterkategorien beschreibt.
 * 
 * @author philipp
 */
public class SubCategoriesFilter {
    //
    // Konstruktoren
    //
    /**
     * Dieser Konstruktor erzeugt einen leeren Unterkategorienfilter.
     */
    public SubCategoriesFilter() {
        this(null);
    }
    
    /**
     * Dieser Konstruktor erzeugt einen Unterkategorienfilter, der mit einer Sammlung von Unterkategorien initialisiert wird. 
     * 
     * @param subcategories Unterkategorien, die �ber ein {@link MailingList}-Objekt oder ein den Schl�ssel darstellendes {@link String}-Objekt repr�sentiert werden.
     * @throws IllegalArgumentException bei unerlaubten Objekten in der �bergebenen Sammlung 
     */
    public SubCategoriesFilter(Collection subcategories) {
        if (subcategories != null) {
            Iterator itCategories = subcategories.iterator();
            while (itCategories.hasNext()) {
                Object next = itCategories.next();
                if (next instanceof String)
                    add((String)next);
                else if (next instanceof MailingList)
                    add((MailingList)next);
                else if (next instanceof SubCategory)
                    add((SubCategory)next);
                else
                    throw new IllegalArgumentException("Kategorien werden nur �ber ein Category-Objekt oder ein den Schl�ssel darstellendes String-Objekt dargestellt");
            }
        }
    }
    
    //
    // �ffentliche Methoden
    //
    /**
     * Diese Methode f�gt eine Kategorie �ber ihre ID hinzu.
     */
    public boolean add(String id) {
        return subcategoryIds.add(id);
    }

    /**
     * Diese Methode f�gt eine Kategorie �ber ein Kategorienobjekt hinzu.
     */
    public boolean add(MailingList subcategory) {
        return subcategoryIds.add(subcategory.getSchluessel());
    }

    public boolean add(SubCategory subcategory) {
        return subcategoryIds.add(subcategory.getIdAsString());
    }

    /**
     * Diese Methode liefert eine r/o-Sammlung der dargestellten Kategorien.
     */
    public Set getSubCategoryIds() {
        return Collections.unmodifiableSet(subcategoryIds);
    }

    //
    // private Membervariablen
    //
    /** Menge der dargestellten Kategorien-IDs */
    private Set subcategoryIds = new HashSet();
}
