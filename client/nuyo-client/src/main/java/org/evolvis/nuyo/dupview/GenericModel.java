package org.evolvis.nuyo.dupview;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;

import de.tarent.commons.datahandling.binding.CompoundModel;
import de.tarent.commons.datahandling.entity.EntityList;

/** 
 * This class can be used to extend a <code>Model</code> with prefixes
 * by a generic way. The <code>GenericModel</code> is saved in a 
 * <code>CompoundModel</code>. The prefixes are made by numbers 1 to n.
 * The Prefix 0 is reserved for new Objects.
 * <p>
 * e.g.: 
 * In <code>CompareView</code> a <code>GenericModel</code> is used 
 * to generate a <code>CompoundModel</code> which contains a map off
 * Entities. Each Entity gets a key '1 to <code>size of EntityList</code>'
 * as prefix. The key 0 is reserved for a new <code>Entity</code>. 
 * 
 * @author Thomas Schmitz, tarent GmbH
 *
 */
public class GenericModel{
	
	CompoundModel gModel;		// generated Model
	int count;					// count of prefixes
	
	/**
	 * Creates a new instance.
	 */
	public GenericModel() {
		gModel = new CompoundModel();
	}
	
	/** 
	 * Generates a <code>CompoundModel</code> and saves the given
	 * <code>Object</code> under the keys '1 to <code>count</code>-1'. 
	 * The key '0' is reserved for a new <code>Object</code>. If count <= 0
	 * the given Object is saved under the key '0'. 
	 * 
	 * 
	 * @param count count of keys that will be generated (1 to count).
	 * @param object <code>Object</code> that will be used as key value.
	 */	
	public void generateModel(int count, Object object) {
		this.count = count;
		if(count > 0) 
			for(int i = 0; i < count; i++) 
				gModel.registerObject(Integer.toString(i+1), object);
		else
			gModel.registerObject(Integer.toString(0), object);			
	}
	
	/** 
	 * Converts a given <code>EntityList</code> to a <code>CompoundModel</code>.
	 * Each <code>Entity</code> of the <code>EntityList</code> is mapped under the 
	 * keys '1 to sizeof EntityList'. 
	 * The key '0' is reserved for a new <code>Entity</code>. 
	 *  
	 * @param entityList the EntityList that will be converted to a CompoundModel.
	 */	
	public void generateModel(EntityList entityList) {
		if(entityList.getSize() > 0) {
			for(int i = 0; i < entityList.getSize(); i++) {
				gModel.registerObject(Integer.toString(i+1), entityList.getEntityAt(i));		
			}
		} 
		try {
			gModel.registerObject(Integer.toString(0), ApplicationServices.getInstance().getCurrentDatabase().createAddress());
		} catch (ContactDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
	
	/**
	 * Returns the generated <code>CompoundModel</code>.
	 * 
	 * @return a <code>CompoundModel</code>
	 */
	public CompoundModel getGenericModel() {
		return gModel;
	}
	
	/**
	 * Returns the count of prefixes.
	 * 
	 * @return count of prefixes.
	 */
	public int getCount(){
		return count;
	}
	
	/**
	 * Extends the <code>GenericModel</code> about the 
	 * given count.
	 * 
	 * @param count number of counts to extend the GenericModel.
	 */
	public void extendModel(int count, Object object) {
		int j = getCount();
		this.count = this.count + count;
		for(int i = 0; i < count; i++)
			gModel.registerObject(Integer.toString(i + j), object);
	}

}
