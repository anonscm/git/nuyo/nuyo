/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetTextArea;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.ErrorHint.PositionErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;



/**
 * TODO: remove if not used in AddressFieldFactory.getGUIElement()!
 * @deprecated not used (only by factory)
 *  
 * @author niko
 */
public class TextAreaField extends GUIElementAdapter implements FocusListener
{
  private TarentWidgetTextArea m_oTextArea;
  
  public TextAreaField()
  {
    super();
     
    m_oTextArea = new TarentWidgetTextArea();
  }
  
  public String getListenerName()
  {
    return "TextAreaField";
  }  
  
  public GUIElement cloneGUIElement()
  {
    TextAreaField textfield = new TextAreaField();
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      textfield.addParameter(param.getKey(), param.getValue());
    }
    return textfield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "TextField", new DataContainerVersion(0));
    ParameterDescription maxlenpd = new ParameterDescription("maxlen", "maximale Anzahl erlaubter Zeichen", "die maximale Anzahl in diesem Feld erlaubter Zeichen.");
    maxlenpd.addValueDescription("Integer", "ein beliebiger Integer Wert");
    d.addParameterDescription(maxlenpd);
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("maxlen".equalsIgnoreCase(key)) 
    {
      try
      {
        int len = Integer.parseInt(value);
        m_oTextArea.setLengthRestriction(len);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) {}
    }
    else if ("rows".equalsIgnoreCase(key)) 
    {
      try
      {
        int rows = Integer.parseInt(value);
        m_oTextArea.getTextArea().setRows(rows);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) {}
    }
    else if ("columns".equalsIgnoreCase(key)) 
    {
      try
      {
        int columns = Integer.parseInt(value);
        m_oTextArea.getTextArea().setColumns(columns);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) {}
    }
    else if ("size".equalsIgnoreCase(key)) 
    {
      try
      {
        Dimension dim = parseDimension(value);
        m_oTextArea.getTextArea().setSize(dim);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) {}
    }
    return false;
  }

  
  
  private Dimension parseDimension(String dimstring)
  {
    String[] coords = dimstring.split("x");
    if (coords.length == 2)
    {
      int x = Integer.parseInt(coords[0]);
      int y = Integer.parseInt(coords[1]);
      return new Dimension(x, y);        
    }
    return null;    
  }

  
  
  
  public boolean canDisplay(Class data)
  {
    return data.isInstance(String.class);
  }

  public Class displays()
  {
    return String.class;
  }
  
  
  public TarentWidgetInterface getComponent()
  {
    return m_oTextArea;
  }

  public void setData(Object data)
  {
    if (data instanceof String)
    {
      m_oTextArea.setData((String)data);      
    }
  }

  public Object getData()
  {
    return m_oTextArea.getData();
  }


  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    //System.out.println("focuslost at " + e.getComponent().getLocation());
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }
  
  
  public void initElement()
  {
    m_oTextArea.addFocusListener(this);
  }

  public void disposeElement()
  {
    m_oTextArea.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oTextArea.requestFocus();
  }
  
  
  public Point getOffsetForError(ErrorHint errorhint)
  {
    int sizex = 0;     
    if (errorhint instanceof PositionErrorHint)
    {
      String text = m_oTextArea.getText();
      Graphics2D g2d = (Graphics2D)(m_oTextArea.getGraphics());
      int pos = ((PositionErrorHint)errorhint).getPosition();
      for(int i=0; i<pos; i++)
      {        
        Rectangle2D rect = m_oTextArea.getFont().getStringBounds(String.valueOf(text.charAt(i)), g2d.getFontRenderContext());
        sizex += rect.getWidth();
      }      
      return(new Point(sizex, 0));
    }    
    return(new Point(0,0));
  }

}
