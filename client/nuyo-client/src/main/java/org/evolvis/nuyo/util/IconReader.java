/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Philipp Kirchner. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;

import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;

/**
 * Diese Klasse stellt Methoden zum einlesen der Icons/Logos bereit
 * 
 * @author kirchner
 *
 */
public class IconReader {
	
	public static URL imagelocation = IconReader.class.getResource("/de/tarent/contact/resources/");
	public static URL applicationlocation = null;
	
	static {
		try {
			applicationlocation = (new File(".")).toURL();
		} catch (MalformedURLException e) {
			System.err.println("URL \"\" is not parseable?");
		}
	}

	public static ImageIcon getImageIcon(Appearance.Key configEntry, String fileName) throws MalformedURLException{
		if(configEntry!=null){
			//Determine if configEntry is there...
			String fileNameConfig = ConfigManager.getAppearance().get(configEntry);
			if(fileNameConfig!=null){
				//Entry is Set
				try {
					URL iconURL = new URL(applicationlocation, fileNameConfig);
					if(iconExists(iconURL)){
						return new ImageIcon(iconURL);
					}
					iconURL = new URL(imagelocation, fileNameConfig);
					if(iconExists(iconURL)){
						return new ImageIcon(iconURL);
					}
				} catch (MalformedURLException e) {
				}
			}
		}
		return new ImageIcon(new URL(imagelocation, fileName));
	}
	
	private static boolean iconExists(URL iconURL){
		File test;
    test = new File(iconURL.getPath());
		return test.exists();	
	}
}
