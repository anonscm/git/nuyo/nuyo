/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Iterator;

import org.evolvis.nuyo.controls.TarentWidgetFloatField;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;



/**
 * TODO: remove if not used in AddressFieldFactory.getGUIElement()!
 * @deprecated not used (only by factory)
 * 
 * @author niko
 */
public class FloatField extends GUIElementAdapter implements FocusListener
{
  private TarentWidgetFloatField  m_oFloatField;
  
  public FloatField()
  {
    super();
     
    m_oFloatField = new TarentWidgetFloatField();
  }
  
  public String getListenerName()
  {
    return "FloatField";
  }  
  
  public GUIElement cloneGUIElement()
  {
    FloatField checkfield = new FloatField();
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      checkfield.addParameter(param.getKey(), param.getValue());
    }
    return checkfield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "FloatField", new DataContainerVersion(0));
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    return false;
  }

  public boolean canDisplay(Class data)
  {
    return data.isInstance(Double.class);
  }

  public Class displays()
  {
    return Double.class;
  }
  
  
  public TarentWidgetInterface getComponent()
  {
    return m_oFloatField;
  }

  public void setData(Object data)
  {
    if (data instanceof Double)
    {
      m_oFloatField.setData((Double)data);      
    }
  }

  public Object getData()
  {
    return m_oFloatField.getData();
  }


  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    //System.out.println("focuslost at " + e.getComponent().getLocation());
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }
  
  
  public void initElement()
  {
    m_oFloatField.addFocusListener(this);
  }

  public void disposeElement()
  {
    m_oFloatField.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oFloatField.requestFocus();
  }
  
  
  public Point getOffsetForError(ErrorHint errorhint)
  {
    return(new Point(0,0));
  }

}
