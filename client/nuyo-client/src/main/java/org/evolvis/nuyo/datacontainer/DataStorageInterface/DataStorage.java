/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataStorageInterface;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObject;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface DataStorage extends DataContainerObject
{
  public DataStorage cloneDataStorage();
  public void init();
  public void dispose();
  
  public boolean canStore(Class data);
  public Class stores();

  public void setData(Object data);
  public Object getData();

  public void setValid(boolean valid);
  public boolean isValid();
  
  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();
    
  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void fireTarentGUIEvent(TarentGUIEvent e);
}
