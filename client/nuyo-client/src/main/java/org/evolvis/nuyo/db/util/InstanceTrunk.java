/* $Id: InstanceTrunk.java,v 1.3 2006/06/06 14:12:14 nils Exp $
 * 
 * Created on 20.06.2004
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Diese Klasse erlaubt es, ID-indiziert Referenzen auf die Instanzen einer Klasse
 * zu halten, um damit daf�r zu sorgen, dass je ID nur eine Instanz existiert.<br>
 * Zus�tzlich wird der Zeitpunkt des letzten Updates vermerkt, so dass "veraltete"
 * Instanzen auffallen k�nnen.
 * 
 * @author mikel
 */
public class InstanceTrunk {
    //
    // �ffentliche Methoden
    //
    /**
     * Diese Methode holt den Eintrag zu einem Schl�ssel. Falls festgestellt wird,
     * dass der Eintrag zwar vorhanden, aber veraltet ist, so wird eine Ausnahme
     * geworfen.
     * 
     * @param key Schl�ssel
     * @return das vorhandene Objekt zu dem Schl�ssel, <code>null</code>, falls es
     * 	nicht vorhanden ist.
     * @throws InstanceOutDatedException Wenn Objekt zu dem Schl�ssel vorhanden ist,
     * 	dieses aber veraltet ist.
     */
    public Object get(Object key) throws InstanceOutDatedException {
        cleanUp();
        InstanceReference reference = (InstanceReference) trunk.get(key);
        if (reference != null) {
            Object value = reference.get();
            if (value != null) {
                if ((getTimeOut() <= 0) || 
                    (System.currentTimeMillis() - getTimeOut() < reference.getLastRefresh()))
                    return value;
                else
                    throw new InstanceOutDatedException(key, value);
            } else
                trunk.remove(key);
        }
        return null;
    }
    
    /**
     * Diese Methode f�gt unter einem Schl�ssel ein Objekt ein; falls unter dem
     * gleichen Schl�ssel schon ein anderes Objekt vorhanden ist, so wird eine
     * Ausnahme geworfen.
     * 
     * @param key Schl�ssel
     * @param value einzuf�gendes Objekt; <code>null</code> ruft stattdessen ein
     * 	{@link #remove(Object)} auf.
     * @throws InstanceNotUniqueException Wenn zu dem Schl�ssel schon ein anderes
     * 	Objekt vorhanden ist.
     */
    public void put(Object key, Object value) throws InstanceNotUniqueException {
        if (value == null)
            remove(key);
        cleanUp();
        Object currentValue;
        try {
            currentValue = get(key);
        } catch(InstanceOutDatedException ioe) {
            currentValue = ioe.getValue();
        }
        if (currentValue == null)
            trunk.put(key, new InstanceReference(key, value, queue));
        else if (currentValue != value)
            throw new InstanceNotUniqueException(key, currentValue);
    }
    
    /**
     * Diese Methode markiert den Eintrag zum �bergebenen Schl�ssel als markiert. 
     * 
     * @param key Schl�ssel
     */
    public void refresh(Object key) {
        cleanUp();
        InstanceReference reference = (InstanceReference) trunk.get(key);
        if (reference != null)
            reference.refresh();
    }
    
    /**
     * Diese Methode entfernt den Verweis auf ein Objekt. Dies sollte ausgef�hrt
     * werden, wenn das Objekt nicht mehr dem Schl�ssel zugeordnet sein soll, etwa
     * bei einem Objekt, das einen Datenbankeintrag representiert, nach Aufruf der
     * L�schmethode.
     * 
     * @param key Schl�ssel
     */
    public void remove(Object key) {
        cleanUp();
        trunk.remove(key);
    }
    
    /**
     * Diese Methode f�hrt gewisse Aktualisierungsaufgaben aus. 
     */
    public void cleanUp() {
        InstanceReference reference;
        while (null != (reference = (InstanceReference) queue.poll())) {
            logger.finer("Cleaning id " + reference.getKey());
            trunk.remove(reference.getKey());
        }
    }
    
    /**
     * Diese Methode liefert die Zeitspanne in Millisekunden, nach der ein
     * Eintrag als veraltet gilt. Ein Wert kleiner-gleich 0 bedeutet, dass
     * Eintr�ge nie veralten. 
     * 
     * @return Aktualit�tszeitspanne in Millisekunden.
     */
    public int getTimeOut() {
        return timeOut;
    }
    
    /**
     * Diese Methode setzt die Zeitspanne in Millisekunden, nach der ein
     * Eintrag als veraltet gilt. Ein Wert kleiner-gleich 0 bedeutet, dass
     * Eintr�ge nie veralten. 
     * 
     * @param milliSeconds Aktualit�tszeitspanne in Millisekunden.
     */
    public void setTimeOut(int milliSeconds) {
        timeOut = milliSeconds;
    }
    
    //
    // innere Klassen
    //
    /**
     * Diese Klasse h�lt schwache Referenzen auf die Instanzen. Zugleich
     * wird hier Schl�ssel der Instanz und Refresh-Zeit gemerkt.
     * 
     * @author mikel
     */
    private static class InstanceReference extends WeakReference {
        //
        // �ffentliche Methoden
        //
        /** Schl�ssel */
        public Object getKey()			{ return key; }
        /** letzter Refresh zur Referenz */
        public long getLastRefresh()	{ return refreshTime; }
        /** Refresh der Referenz */
        public void refresh()			{ refreshTime = System.currentTimeMillis(); }
        
        //
        // Konstruktor
        //
        /**
         * @param key Schl�ssel
         * @param referent Wert
         * @param q ReferenceQueue des {@link InstanceTrunk}.
         */
        public InstanceReference(Object key, Object referent, ReferenceQueue q) {
            super(referent, q);
            this.key = key;
            refresh();
        }
        
        //
        // private Membervariablen
        //
        /** Schl�ssel zu dem Referenten */
        Object key = null;
        /** letzter Refresh zur Referenz */
        long refreshTime = 0;
    }
    //
    // private Membervariablen
    //
    /** Aktualit�tszeitspanne in Millisekunden */
    private int timeOut = 0;
    /** in dieser Map werden die Referenzen verwaltet */
    private Map trunk = new HashMap();
    /** diese Queue dient dem CleanUp freigewordener Referenzen */
    private ReferenceQueue queue = new ReferenceQueue();
    /** Logger des Trunks */
    private final static Logger logger = Logger.getLogger(InstanceTrunk.class.getName());
}
