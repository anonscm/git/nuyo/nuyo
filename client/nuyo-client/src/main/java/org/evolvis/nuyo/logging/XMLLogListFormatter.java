/*
 * Created on 16.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.util.Iterator;
import java.util.List;


/**
 * @author niko
 *
 */
public class XMLLogListFormatter implements LogListFormatter
{
  public String formatList(List strings)
  {
    String text = "";
    
    text += "<loglist>" + "\n";
    
    Iterator it = strings.iterator();
    while(it.hasNext())
    {
      String entrytext = (String )(it.next());
      text += entrytext + "\n";
    }
        
    text += "</loglist>" + "\n";

    return text;
  }

}
