/* $Id: Restriction.java,v 1.2 2006/06/06 14:12:12 nils Exp $
 * Created on 29.10.2003
 */
package org.evolvis.nuyo.db.veto;

/**
 * Diese Schnittstelle enth�lt Veto-Restrictions.
 * 
 * @author mikel
 */
public interface Restriction {
    public final static Object PER_CATEGORY_DATA_ONLY = "restriction:per category data only";
    public final static Object ALLGEMEINE_EXCLUDED = "restriction: allgemeine excluded";
}
