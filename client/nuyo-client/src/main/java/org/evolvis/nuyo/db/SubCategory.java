package org.evolvis.nuyo.db;

import org.evolvis.nuyo.groupware.impl.SubCategoryImpl;

/**
 * Diese Klasse stellt eine Kategorie dar.
 * 
 * @author Sebastian
 */
public abstract class SubCategory extends SubCategoryImpl {

    
    /**
     * Dieser Konstruktor erzeugt ein uneingelesenes Kategorie-Objekt.
     * 
     * @param id Schl�ssel der Kategorie
     * @param db Datenzugriffsobjekt
     */
    
    public Integer getIdAsInteger() {
        return getId() != 0? new Integer(getId()): null ;
    }

    public String getIdAsString() {
    		return getIdAsInteger() != null ? "" + getIdAsInteger(): null;
    }    
    
    public String toString(){
    		return getSubCategoryName() != null? getSubCategoryName(): getIdAsString();
    }
}
