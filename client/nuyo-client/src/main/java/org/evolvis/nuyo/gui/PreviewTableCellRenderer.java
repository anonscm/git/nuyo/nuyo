/*
 * Created on 11.08.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * @author niko
 *
 */
public class PreviewTableCellRenderer implements TableCellRenderer
{
  private AddressTableView m_oAddressTableView = null;
  private Color m_oWaitingColor;
  private Color m_oLoadingColor;
  
  public PreviewTableCellRenderer(AddressTableView previewtable)
  {
    m_oAddressTableView = previewtable;
    m_oWaitingColor = Color.ORANGE;
    m_oLoadingColor  = Color.RED;    
  }
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    JLabel label;
    
    if (value == null)
    {
      label = new JLabel("keine Adresse vorhanden.");
    }
    else
    {
      label = new JLabel(value.toString());      
    }
    
    label.setOpaque(true);
    
    Color bg;
    Color fg;
    
    if (isSelected)
    {
      bg = table.getSelectionBackground();
      fg = table.getSelectionForeground();
    }
    else
    {
      bg = table.getBackground();
      fg = table.getForeground();      
    }

    if (! m_oAddressTableView.isPreviewSortedAscending())
    {
      row = m_oAddressTableView.getNumberOfRows() - 1 - row;
    }

    if (m_oAddressTableView.isTableReady())
    {
      if (! m_oAddressTableView.isPreviewRowLoaded(row)) fg = m_oLoadingColor;
    } else fg = m_oWaitingColor;
    
    label.setBackground(bg);
    label.setForeground(fg);
    return label;
  }
}
