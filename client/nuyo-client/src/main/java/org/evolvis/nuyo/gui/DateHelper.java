/* $Id: DateHelper.java,v 1.3 2006/06/06 14:12:10 nils Exp $
 * Created on 19.05.2003
 */
/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.gui;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author niko
 */
public class DateHelper {
    public static int getDayOfDate(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static int getMonthOfDate(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.MONTH);
    }

    public static int getYearOfDate(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.YEAR);
    }

    public static int getHourOfDate(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinuteOfDate(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.MINUTE);
    }

    public static Date clearSeconds(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.SECOND, 0);
        return (cal.getTime());
    }

    public static Date createDate(int day, int month, int year, int hour, int minute) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        return (cal.getTime());
    }

    public static Date addDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return (cal.getTime());
    }

  public static Date addHour(Date date) {
      Calendar cal = new GregorianCalendar();
      cal.setTime(date);
      cal.add(Calendar.HOUR_OF_DAY, 1);
      return (cal.getTime());
  }

  public static Date addMinute(Date date) {
      Calendar cal = new GregorianCalendar();
      cal.setTime(date);
      cal.add(Calendar.MINUTE, 1);
      return (cal.getTime());
  }

  public static Date addSecond(Date date) {
      Calendar cal = new GregorianCalendar();
      cal.setTime(date);
      cal.add(Calendar.SECOND, 1);
      return (cal.getTime());
  }

  public static void clearTime(Calendar cal) { 
      cal.set(Calendar.HOUR_OF_DAY, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
  }

    /**
     * Diese Methode liefert den letzten Montag 0:00 bis einschlie�lich dem
     * �bergebenen Tag.
     * 
     * @param date ein Datum
     * @return der letzte Montag bis dem Datum inklusive
     */
    public static Date getLastMonday(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);

        int offset = -getEuropeanDOWIndex(date);
        if (offset != 0)
            cal.add(Calendar.DATE, offset);
        clearTime(cal);
        return (cal.getTime());
    }

    /**
     * Diese Methode liefert den ersten Sonntag 23:59 ab einschlie�lich dem
     * �bergebenen Tag.
     * 
     * @param date ein Datum
     * @return der erster Sonntag ab dem Datum inklusive
     */
    public static Date getNextSunday(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);

        int offset = 6 - getEuropeanDOWIndex(date);
        if (offset != 0)
            cal.add(Calendar.DATE, offset);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return (cal.getTime());
    }

    /**
     * Diese Methode liefert den Montag der ersten Woche des Vormonats im
     * Vergleich zum Monat der Woche des �bergebenen Datums. Hierbei gilt
     * eine Woche zu dem Monat des enthaltenen abschlie�enden Sonntags
     * zugeh�rig.
     * 
     * @param date das �bergebene Datum
     * @return der Montag der ersten Woche des Vormonats.
     */
    public static Date getLastMonth(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getNextSunday(date));
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return getLastMonday(cal.getTime());
    }

    /**
     * Diese Methode liefert den Montag der Vorwoche.
     * 
     * @param date das �bergebene Datum
     * @return der Montag der Vorwoche.
     */
    public static Date getLastWeek(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, -7);
        return getLastMonday(cal.getTime());
    }

    /**
     * Diese Methode liefert den Montag der Folgewoche.
     * 
     * @param date das �bergebene Datum
     * @return der Montag der Folgewoche.
     */
    public static Date getNextWeek(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, 7);
        return getLastMonday(cal.getTime());
    }

    /**
     * Diese Methode liefert den Montag der ersten Woche des Folgemonats im
     * Vergleich zum Monat der Woche des �bergebenen Datums. Hierbei gilt
     * eine Woche zu dem Monat des enthaltenen abschlie�enden Sonntags
     * zugeh�rig.
     * 
     * @param date das �bergebene Datum
     * @return der Montag der ersten Woche des Folgemonats.
     */
    public static Date getNextMonth(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(getNextSunday(date));
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return getLastMonday(cal.getTime());
    }
    
    /**
     * Liefert den 0-basierten europ�ischen Wochentag des �bergebenen Datums, also
     * 0 f�r Montag, 1 f�r Dienstag, ...
     * 
     * @param date das zu untersuchende Datum
     * @return 0-basierter europ�ischer Wochentag
     */
    public static int getEuropeanDOWIndex(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        switch(cal.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:       return 0; 
            case Calendar.TUESDAY:      return 1; 
            case Calendar.WEDNESDAY:    return 2; 
            case Calendar.THURSDAY:     return 3; 
            case Calendar.FRIDAY:       return 4; 
            case Calendar.SATURDAY:     return 5;
            case Calendar.SUNDAY: 
            default:                    return 6; 
        }
    }
    
    private DateHelper() {
    }
}
