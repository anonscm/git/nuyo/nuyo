/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.plugins.calendar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


/**
 * A collapsing menu consists of {@link org.evolvis.nuyo.plugins.calendar.CollapsingMenuItem CollapsingMenuItems} or {@link org.evolvis.nuyo.plugins.calendar.CollapsingMenuItemContainer CollapsingMenuItemContainers}. 
 * Every collapsing menu will be added to {@link org.evolvis.nuyo.plugins.calendar.CollapsingMenuPanel CollapsingMenuPanel}.<br>
 * 
 * @see org.evolvis.nuyo.plugins.calendar.CollapsingMenuPanel#addMenu(CollapsingMenu)
 * @author niko
 * @deprecated Calendar plugin uses it. Remove it when rewritten.
 */
class CollapsingMenu extends JPanel
{
  private static final long serialVersionUID = 1L;
  private CollapsingMenuLayoutManager m_CollapsingLayout;
  private JPanel parentContainer;
  private String menuTitle;
  private String menuName;
  private Font font;

  private boolean m_bShowExpandedIcon;
  private boolean m_bShowCollapsedIcon;
  private boolean m_bIsExpandable;
  private boolean m_bIsCollapseable;
  private boolean m_bIsEnabled;
  private boolean m_bIsCollapsed;

  private JButton m_oCollapseButtonLabel;
  private JButton m_oCollapseButton;
  private ImageIcon m_oIconCollapsed;
  private ImageIcon m_oIconExpanded;
  private JPanel m_oButtonPanel;
  private JPanel m_oBorderPanel;
  private JPanel m_oMenuItemPanel;
  private Color m_oBorderLineColor;
  private Color m_oBackgroundColor;
  private Color m_oTitleColor;
  private Color m_oTextColor;
  private Color m_oDisabledTitleColor;
  private Color m_oDisabledTextColor;
  private int m_iBorderLineWidth;
  private Cursor m_oHandCursor;
  
  private CollapsingMenu instance;
  private List m_oCollapseListenerList;
  private ArrayList m_oMenuItems;
  

/**
 * Konstruktor der Klasse CollapsingMenu
 * Parameter:
 * menuname    = der Text der in der Menu�berschrift stehen soll
 * isCollapsed = der initiale Status des Menus
 *               true  = zusammengeklappt
 *               false = ausgeklappt
 */
  public CollapsingMenu(String menuname, boolean isCollapsed) 
  {
    super();
    instance = this;
    m_oCollapseListenerList = new ArrayList();
    m_bIsExpandable   = true;
    m_bIsCollapseable = true;
    m_bIsEnabled = true;
    m_bIsCollapsed = isCollapsed;

    m_oMenuItems = new ArrayList();

    font = new Font(null, Font.PLAIN, 12);

    m_oBorderLineColor = Color.black;
    m_iBorderLineWidth = 1;

    m_oHandCursor = new Cursor(Cursor.HAND_CURSOR);


    URL imagelocation = this.getClass().getResource("/de/tarent/controls/resources/");    

    m_bShowExpandedIcon = true;
    try
    {          
      m_oIconExpanded   = new ImageIcon(new URL(imagelocation, "expanded.gif"));
    }
    catch(MalformedURLException mue)
    {
      m_oIconExpanded  = null;
      m_bShowExpandedIcon = false;
      System.err.println("unable to load data for expanded icon.");
    }
    
    m_bShowCollapsedIcon = true;
    try
    {          
      m_oIconCollapsed  = new ImageIcon(new URL(imagelocation, "collapsed.gif"));
    }
    catch(MalformedURLException mue)
    {
      m_oIconCollapsed = null;
      m_bShowCollapsedIcon = false;
      System.err.println("unable to load data for collapsed icon.");
    }
    
    menuTitle = menuname;
    menuName  = menuname;
    createContainer(isCollapsed);
  }

/**
 * Konstruktor der Klasse CollapsingMenu
 * erzeugt ein initial eingeklapptes Menu
 * Parameter:
 * menuname    = der Text der in der Menu�berschrift stehen soll
 */
  public CollapsingMenu(String menuname) 
  {
    this(menuname, true); 
  }

  
  public void addCollapseListener(CollapseListener cl)
  {
    m_oCollapseListenerList.add(cl);
  }

  public void removeCollapseListener(CollapseListener cl)
  {
    m_oCollapseListenerList.remove(cl);
  }

  
  public void setMenuFont(Font aFont)
  {
    font = aFont; 
    if (font != null) m_oCollapseButtonLabel.setFont(font);    
  }
  
  public Font getMenuFont()
  {
    return(font);
  }
  

  public void setExpandable(boolean isexpandable)
  {
    m_bIsExpandable   = isexpandable;
  }
  
  public void setCollapseable(boolean iscollapseable)
  {
    m_bIsCollapseable = iscollapseable;
  }

  public boolean isExpandable()
  {
    return(m_bIsExpandable); 
  }

  public boolean isCollapseable()
  {
    return(m_bIsCollapseable); 
  }

  public boolean isCollapsed()
  {
    return(m_bIsCollapsed);
  }

  public boolean isExpanded()
  {
    return(!m_bIsCollapsed);
  }


/**
 * setzt den Namen des Menus
 * Wird nur zum Wiederauffinden innerhalb der Applikation
 * ben�tigt! Wird NIE angezeigt
 */
  public void setName(String aName)
  {
    menuName = aName; 
  }

/**
 * holt den Namen des Menus
 */
  public String getName()
  {
    return(menuName); 
  }


/**
 * setzt das Icon f�r den Status: AUSGEKLAPPT
 */
  public void setExpandedIcon(ImageIcon icon)
  {        
    m_oIconExpanded = icon;
    m_bShowExpandedIcon = true;
    if (! (m_CollapsingLayout.isCollapsed()))
    {
      installExpandedIcon();
    }
  }

/**
 * setzt das Icon f�r den Status: EINGEKLAPPT
 */
  public void setCollapsedIcon(ImageIcon icon)
  {
    m_oIconCollapsed = icon;
    m_bShowCollapsedIcon = true;
    if ((m_CollapsingLayout.isCollapsed()))
    {
      installCollapsedIcon();
    }
  }

/**
 * �ndert den �berschriftentext
 */
  public void setTitleText(String text)
  {
    m_oCollapseButtonLabel.setText(text);
  }
  
  public String getTitleText()
  {
	  return m_oCollapseButtonLabel.getText();
  }


/**
 * �ndert die Ausrichtung des �berschriftentextes
 * Parameter: das Alignment
 * z.B:
 * SwingConstants.LEFT
 * SwingConstants.RIGHT
 * SwingConstants.CENTER
 */
  public void setTitleAlignment(int alignment)
  {
    m_oCollapseButtonLabel.setHorizontalAlignment(alignment);
  }


/**
 * interne Funktion 
 */
  private void installCollapsedIcon()
  {
    if (m_bShowCollapsedIcon) m_oCollapseButton.setIcon(m_oIconCollapsed);    
  }

/**
 * interne Funktion 
 */
  private void installExpandedIcon()
  {
    if (m_bShowExpandedIcon) m_oCollapseButton.setIcon(m_oIconExpanded);    
  }


/**
 * klappt das Menu aus
 */
  public void expand()
  {
    if (m_bIsExpandable)
    {
      m_CollapsingLayout.setCollapsed(false);
      installExpandedIcon();
      this.revalidate();
      m_oMenuItemPanel.revalidate();
      m_bIsCollapsed = false;
    }
  }

  
/**
 * klappt das Menu ein
 */
  public void collapse()
  {    
    if (m_bIsCollapseable)
    {
      m_CollapsingLayout.setCollapsed(true);
      installCollapsedIcon();
      this.revalidate();
      m_oMenuItemPanel.revalidate();
      m_bIsCollapsed = true;
    }
  }

/**
 * horcht auf den Ein-Ausklapp-Knopf und f�hrt die Funktion dann aus
 */ 
  public class CollapseActionListener implements ActionListener
  {
    public void actionPerformed(ActionEvent ae) 
    {
      if (m_CollapsingLayout.isCollapsed())
      {
        expand(); 
      }
      else
      {
        collapse();
      }
      
      if (m_oCollapseListenerList != null)
      {
        Iterator it = m_oCollapseListenerList.iterator();
        while(it.hasNext())
        {
          CollapseListener listener = (CollapseListener)(it.next());
          listener.collapseStatusChanged(instance, m_CollapsingLayout.isCollapsed());
        }
      }
    }
  }
 
 
 
/**
 * intern: erzeugt die GUI-Elemente f�r das Menu
 */ 
  private void createContainer(boolean isCollapsed)
  {        
    this.setLayout(new BorderLayout());
    this.setBorder(new EmptyBorder(20,10,0,10));            
    
    m_oBorderPanel = new JPanel();
    m_oBorderPanel.setLayout(new BorderLayout());
    m_oBorderPanel.setBorder(new LineBorder(Color.black, 1));            
    this.add(m_oBorderPanel, BorderLayout.CENTER);
    
    m_oButtonPanel = new JPanel();
    m_oButtonPanel.setLayout(new BorderLayout());
       
    m_oCollapseButtonLabel = new JButton(menuTitle);   
    m_oCollapseButtonLabel.setBorderPainted(false);
    m_oCollapseButtonLabel.setFocusPainted(false);    
    m_oCollapseButtonLabel.setMargin(new Insets(0,0,0,0));  
    if (font != null) m_oCollapseButtonLabel.setFont(font);
    m_oCollapseButtonLabel.setHorizontalAlignment(SwingConstants.CENTER);
    m_oButtonPanel.add(m_oCollapseButtonLabel, BorderLayout.CENTER);


    m_oCollapseButton = new JButton();
    
    if (isCollapsed)
    {   
      installCollapsedIcon();
    }
    else
    {   
      installExpandedIcon();
    }
        
    m_oCollapseButton.setBorderPainted(false);
    m_oCollapseButton.setFocusPainted(false);    
    m_oCollapseButton.setMargin(new Insets(0,0,0,0));
    m_oButtonPanel.add(m_oCollapseButton, BorderLayout.EAST);
        
    m_oBorderPanel.add(m_oButtonPanel, BorderLayout.NORTH);
    
    CollapseActionListener cal = new CollapseActionListener();
    m_oCollapseButtonLabel.addActionListener(cal);
    m_oCollapseButton.addActionListener(cal);
        
    m_oMenuItemPanel = new JPanel();

    
    m_CollapsingLayout = new CollapsingMenuLayoutManager(new BoxLayout(m_oMenuItemPanel, BoxLayout.Y_AXIS));

    m_oMenuItemPanel.setLayout(m_CollapsingLayout);
    m_oMenuItemPanel.setBorder(new EmptyBorder(2,2,2,2));            
    m_CollapsingLayout.setCollapsed(isCollapsed);
    m_oBorderPanel.add(m_oMenuItemPanel, BorderLayout.CENTER);  

    setBackgroundColor(Color.WHITE);
    setTitleColor(new Color(0x00, 0x00, 0xff));
    setTextColor(Color.BLACK);
    m_oDisabledTitleColor = new Color(0x00, 0x00, 0xaa);
    m_oDisabledTextColor = new Color(0x66, 0x66, 0x66);
  }


/**
 * f�gt ein MenuItem in das Menu ein
 */ 
  public void addMenuItem(CollapsingMenuItem item)
  {
    m_oMenuItemPanel.add((Component)item);
    m_oMenuItems.add(item);
    m_oMenuItemPanel.revalidate(); 
  }

/**
 * l�scht ein MenuItem aus dem Menu
 */ 
  public void removeMenuItem(CollapsingMenuItem item)
  {
    m_oMenuItemPanel.remove((Component)item);
    m_oMenuItems.remove(item);
    m_oMenuItemPanel.revalidate(); 
  }

/**
 * l�scht alle MenuItem's aus dem Menu
 */ 
  public void removeAllMenuItems()
  {
    m_oMenuItems.clear();
    m_oMenuItemPanel.removeAll();   
    m_oMenuItemPanel.revalidate(); 
  }

/**
 * holt sich das Item mit dem Index "index"
 */ 
  public CollapsingMenuItem getMenuItemByIndex(int index)
  {
    return((CollapsingMenuItem)(m_oMenuItems.get(index)));       
  }
  
/**
 * holt sich das Item mit dem Index "index"
 */ 
  public int getNumberOfMenuItems()
  {
    return(m_oMenuItems.size());       
  }
  
/**
 * setzt die Hintergrundfarbe des Menus
 */ 
  public void setBackgroundColor(Color color)
  {
    m_oBackgroundColor = color;
    m_oMenuItemPanel.setBackground(color);
    m_oBorderPanel.setBackground(color);
  }
  
/**
 * setzt die Hintergrundfarbe der Menu-�berschrift-Leiste
 */ 
  public void setTitleColor(Color color)
  {
    m_oTitleColor = color;
    putTitleColor(m_oTitleColor);
  }
  
  private void putTitleColor(Color color)
  {
    m_oCollapseButton.setBackground(color);
    m_oButtonPanel.setBackground(color);
    m_oCollapseButtonLabel.setBackground(color);
  }
  
/**
 * setzt die Hintergrundfarbe der deaktivierten Menu-�berschrift-Leiste
 */ 
  public void setDisabledTitleColor(Color color)
  {
    m_oDisabledTitleColor = color;
  }
  
/**
 * setzt die Textfarbe der Menu-�berschrift-Leiste
 */ 
  public void setTextColor(Color color)
  {
    m_oTextColor = color;    
    putTextColor(color);
  }
  
  private void putTextColor(Color color)
  {
    m_oCollapseButtonLabel.setForeground(color);
  }
  
/**
 * setzt die Textfarbe der deaktivierten Menu-�berschrift-Leiste
 */ 
  public void setDisabledTextColor(Color color)
  {
    m_oDisabledTextColor = color;    
  }
  
/**
 * setzt den Ramen des Menus
 */ 
  public void setMenuBorder(int top, int left, int bottom, int right)
  {
    this.setBorder(new EmptyBorder(top, left, bottom, right));
  }

/**
 * setzt den Breite des Ramens des Menus
 */ 
  public void setBorderLineWidth(int width)
  {  
    m_iBorderLineWidth = width;
    m_oBorderPanel.setBorder(new LineBorder(m_oBorderLineColor, m_iBorderLineWidth));            
  }
  
  
/**
 * setzt den Farbe des Ramens des Menus
 */ 
  public void setBorderLineColor(Color color)
  {  
    m_oBorderLineColor = color;
    m_oBorderPanel.setBorder(new LineBorder(m_oBorderLineColor, m_iBorderLineWidth));            
  }
  
  
/**
 * setzt den Tooltip des Menus
 */ 
  public void setMenuToolTipText(String text)
  {
    m_oCollapseButton.setToolTipText("Auf- und Zuklappen des Menus \"" + text + "\"");
    m_oCollapseButtonLabel.setToolTipText(text);
  }
  
/**
 * aktiviert oder deaktiviert das Menu
 */ 
  public void setMenuEnabled(boolean isenabled)
  {
    m_bIsEnabled = isenabled;
    this.setEnabled(isenabled); 
    
    for(int i=0; i<(m_oMenuItems.size()); i++)
    {
      getMenuItemByIndex(i).setItemEnabled(isenabled);
    }
    
    if (isenabled) 
    {
      putTitleColor(m_oTitleColor);   
      putTextColor(m_oTextColor); 
    }
    else
    {
      putTitleColor(m_oDisabledTitleColor);
      putTextColor(m_oDisabledTextColor);
    }
  }


  public boolean getMenuEnabled()
  {
    return(m_bIsEnabled);
  }

  public void showHandCursor(boolean show)
  {    
    if (show)
    {
      m_oCollapseButtonLabel.setCursor(m_oHandCursor);
      m_oCollapseButton.setCursor(m_oHandCursor);
    }
    else
    {
      m_oCollapseButtonLabel.setCursor(null);
      m_oCollapseButton.setCursor(null);
    }
  }


  public void setAcceleratorKey(char key)
  {	  
  	if (Character.toUpperCase(key) >= 'A' && Character.toUpperCase(key) <= 'Z')
  	{
  		m_oCollapseButton.setMnemonic((int)Character.toUpperCase(key));
  	}
  }

  public void setAcceleratorKey(String key)
  {
	  if (key != null) 
    {
      if (key.length() > 0) setAcceleratorKey(key.charAt(0));
    } 
  }

  public void setTitleFont(Font font) {
  	m_oCollapseButtonLabel.setFont(font);
  }
    
/**
 * l�sst das Menu blinken
 */ 
  public void blinkMenu(int numblinks)
  {
    Color blinkcolor = new Color(0xff - (m_oBackgroundColor.getRed()), 0xff - (m_oBackgroundColor.getGreen()), 0xff - (m_oBackgroundColor.getBlue()));
    
    for(int i=0; i<numblinks; i++)
    {
      m_oMenuItemPanel.setBackground(blinkcolor);
      m_oBorderPanel.setBackground(blinkcolor);

      m_oMenuItemPanel.setBackground(m_oBackgroundColor);
      m_oBorderPanel.setBackground(m_oBackgroundColor);
    }
  }

  /** Sets the parent panel. */
  public void setContainerPanel( JPanel menupanel ) {
      parentContainer = menupanel;
  }

  /** Returns the parent panel. */
  public JPanel getContainerPanel() {
      return parentContainer;
  }
  
  /** Adds a JSeparator() to this menu.
   * 
   * This method exists to have the functionality of a JMenu.addSeparator()
   * in this class.
   *
   */
  public void addSeparator()
  {
	  add(new JSeparator());
  }
}
