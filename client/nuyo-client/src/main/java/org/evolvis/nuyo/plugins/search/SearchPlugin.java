package org.evolvis.nuyo.plugins.search;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.plugin.Plugin;


/**
 * Search plugin represents an access point to the search engine of tarent-contact.
 *   
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 * @author Sebastian Mancke (s.mancke@tarent.de), tarent GmbH Bonn
 */
public class SearchPlugin implements Plugin {
    
    public static final String ID = "search";

    private static final TarentLogger logger = new TarentLogger(SearchPlugin.class);
    
    protected GUIListener guiListener;
    protected SearchDialog dialog;
    
    public SearchPlugin() {
        logger.fine("Search Plugin enabled.");
    }

    public String getID() {
        return ID;
    }

    public void init() {
    }

    public Object getImplementationFor( Class aClass ) {
        if (TarentSearch.class == aClass) 
            return (TarentSearch)getSearchDialog(); 
        return null;
    }
    
    protected SearchDialog getSearchDialog() {
        if(dialog == null) {
            dialog = new SearchDialog(ApplicationServices.getInstance().getActionManager().getFrame());
        }
        return dialog;
    }
    

    public List getSupportedTypes() {
        List supportedTypes = new ArrayList();
        supportedTypes.add(TarentSearch.class);
        return supportedTypes;
    }
    
    public boolean isTypeSupported( Class aClass ) {        
        return (aClass == TarentSearch.class);
    }

	public String getDisplayName() {
		return "Search";
	}
}
