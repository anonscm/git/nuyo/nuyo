/*
 * Created on 05.05.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JButton;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class SimpleMonatskalenderDayButton extends JButton implements MonatskalenderDayButtonInterface
{
  private ArrayList m_oJobs = new ArrayList();
  private ArrayList m_oSheds = new ArrayList();
  private Color m_oSchedulePointColor = Color.RED;
  private Color m_oToDoPointColor = Color.RED;  
  
  public SimpleMonatskalenderDayButton(String text)
  {
    super(text);
  }
  
  public SimpleMonatskalenderDayButton()
  {
    super();
  }
  
  public void setShedulePointColor(Color color)
  {
    m_oSchedulePointColor = color;
  }
  
  public void setToDoPointColor(Color color)
  {
    m_oToDoPointColor = color;
  }
  
  public void addAppointment(Appointment appointment)
  {
    try
    {
      if (appointment.isJob()) m_oJobs.add(appointment);
      else m_oSheds.add(appointment);
    } 
    catch (ContactDBException e) {}
  }
  
  public void removeAppointment(Appointment appointment)
  {
    try
    {
      if (appointment.isJob()) m_oJobs.remove(appointment);
      else m_oSheds.remove(appointment);
    } 
    catch (ContactDBException e) {}
  }
  
  public void clearAppointments()
  {
    m_oJobs.clear();
    m_oSheds.clear();
  }
  
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {        
    int maxshedwidth = 12;
    
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();
    
    g.setColor(this.getBackground());
    g.fillRect(0, 0, size.width, size.height);    
    
    // Button-Text rendern...
    Rectangle2D rect = g.getFontMetrics().getStringBounds(this.getText(), g);
    int posy = (int) ((size.height) - ((int)(rect.getHeight())));
    int posx = (int) ((size.width - rect.getWidth()) / 2.0);	    
    
    String text = "(" + m_oSheds.size() + ", " + m_oJobs.size() + ")";
    Rectangle2D rect2 = g.getFontMetrics().getStringBounds(text, g);
    int posy2 = (int) ((size.height) - rect2.getHeight() - rect.getHeight());
    int posx2 = (int) ((size.width - rect2.getWidth()) / 2.0);	    
    
    g.setColor(this.getForeground());
    
    g.setFont(getFont().deriveFont(Font.BOLD));
    g.drawString(this.getText(), posx, posy2);    
    
    g.setFont(getFont().deriveFont(Font.PLAIN));
    g.drawString(text, posx2, posy);        
  }

  /* (non-Javadoc)
   * @see de.tarent.contact.gui.calendar.MonatskalenderDayButtonInterface#setActiveAppointment(de.tarent.contact.db.Appointment)
   */
  public void setActiveAppointment(Appointment appointment)
  {
    // TODO Auto-generated method stub
    
  }

  /* (non-Javadoc)
   * @see de.tarent.contact.gui.calendar.MonatskalenderDayButtonInterface#containsAppointment(de.tarent.contact.db.Appointment)
   */
  public boolean containsAppointment(Appointment appointment)
  {
    // TODO Auto-generated method stub
    return false;
  }

}
