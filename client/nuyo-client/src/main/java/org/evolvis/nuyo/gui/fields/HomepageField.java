/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class HomepageField extends GenericTextField
{
  public HomepageField()
  {
    super("HOMEPAGE", AddressKeys.HOMEPAGE, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Web-Seite_ToolTip", "GUI_MainFrameNewStyle_Standard_Web-Seite", 100);
  }
}
