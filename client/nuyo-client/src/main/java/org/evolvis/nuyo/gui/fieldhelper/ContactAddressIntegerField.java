/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import org.evolvis.nuyo.controller.DoubleCheckManager;
import org.evolvis.nuyo.controls.TarentWidgetIntegerField;


/**
 * @author niko
 *
 */
public abstract class ContactAddressIntegerField extends ContactAddressField
{
  private FocusListener m_oDoubleCheckFocusListener = null;
  
  protected ContactAddressIntegerField()
  {
  }
  
  public void setDoubleCheckSensitive(TarentWidgetIntegerField tf, boolean issensitive)
  {
    try {
		if (issensitive) 
			addTextFieldFocusListener(tf);
		else 
			removeTextFieldFocusListener(tf);
	} catch (Exception e) {
		e.printStackTrace();
	}
  }
  
  protected TarentWidgetIntegerField createTextField(String toolTip, int lengthRestriction) 
  {
    TarentWidgetIntegerField tf = new TarentWidgetIntegerField();
    if (toolTip != null) tf.setToolTipText(toolTip);
    if (lengthRestriction > 0) tf.setLengthRestriction(lengthRestriction);
    return tf;
  }

  private void addTextFieldFocusListener(TarentWidgetIntegerField tf)
  {
    if (m_oDoubleCheckFocusListener != null) removeTextFieldFocusListener(tf);

    m_oDoubleCheckFocusListener = new textfield_focus_doublecheck(tf); 
    tf.addFocusListener(m_oDoubleCheckFocusListener);
  }
  
  private void removeTextFieldFocusListener(TarentWidgetIntegerField tf)
  {
    if (m_oDoubleCheckFocusListener != null) tf.removeFocusListener(m_oDoubleCheckFocusListener);
  }
  
  
  
  protected class textfield_focus_doublecheck implements FocusListener
  {
    TarentWidgetIntegerField textfield;  
    String text;

    public textfield_focus_doublecheck(TarentWidgetIntegerField tf)
    {
      textfield = tf;            
    }
    
    public void focusGained(FocusEvent fe)
    {
      text = textfield.getText();    
    }
    
    public void focusLost(FocusEvent fe)
    {
      if (!(textfield.getText().equals(text)))
      {          
        // trigger DoublettenCheck
        DoubleCheckManager.getInstance().checkForDoublesAndHandleResults();           
      }
    }
  }          
  
}
