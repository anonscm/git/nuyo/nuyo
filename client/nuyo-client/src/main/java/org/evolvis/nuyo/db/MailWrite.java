/* $Id: MailWrite.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink and Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.evolvis.nuyo.db.persistence.PersistentEntity;


/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public interface MailWrite extends PersistentEntity {
	public void reload() throws ContactDBException;
	
	public String getSubject() throws ContactDBException;
	public void setSubject(String subject) throws ContactDBException;
	
	public String getBody() throws ContactDBException;
	public void setBody(String body) throws ContactDBException;
	
	public String getTo() throws ContactDBException;
	public void setTo(String to) throws ContactDBException;
	
	public String getCc() throws ContactDBException;
	public void setCc(String cc) throws ContactDBException;
	
	public String getBcc() throws ContactDBException;
	public void setBcc(String bcc) throws ContactDBException;
	
	public Integer getRequestAnswer() throws ContactDBException;
	public void setRequestAnswer(Integer requestAnswer) throws ContactDBException;
	
	public Integer getPriority() throws ContactDBException;
	public void setPriority(Integer priority) throws ContactDBException;
	
	public Integer getStatus() throws ContactDBException;
	
//	public Integer getCopySelf() throws ContactDBException;
//	public void setCopySelf(Integer copySelf) throws ContactDBException;
	
	public void addAttachment(String filename, InputStream inputStream, long size) throws ContactDBException, IOException;
	public void updateAttachment(int attachmentId, String filename, InputStream inputStream, long size) throws ContactDBException, IOException;
	public void removeAttachment(int attachmentId) throws ContactDBException;
	// TODO evtl. MailAttachment gehen?
	public Map getAttachmentList() throws ContactDBException; // ID => filename
	public InputStream getAttachment(int attachmentId) throws ContactDBException, IOException;
	
	public boolean isDirty() throws ContactDBException;
	
	public void save() throws ContactDBException;
	public void send() throws ContactDBException;
}