/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Listener;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;


/**
 * @author niko
 *
 */
public abstract class DataContainerListenerActionAdapter implements DataContainerListenerAction 
{
  protected List m_oParameterList = new ArrayList();

  protected DataContainer m_oDataContainer = null;
  protected DataContainerListener m_oDataContainerListener = null;
  
  public abstract DataContainerListenerAction cloneAction();
  public abstract boolean doAction();
  public abstract void init();
  public abstract void dispose();
  public abstract boolean addParameter(String key, String value);
  public abstract DataContainerObjectDescriptor getDataContainerObjectDescriptor();
  
  public void setListener(DataContainerListener listener)
  {
    m_oDataContainerListener = listener;
  }
  
  public DataContainerListener getListener()
  {
    return m_oDataContainerListener;    
  }
  
  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }

  public List getEventsConsumable()
  {
    ArrayList list = new ArrayList();
    return list;
  }

  public List getEventsFireable()
  {
    ArrayList list = new ArrayList();
    return list;
  }

  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().addTarentGUIEventListener(event, handler);
  }

  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().removeTarentGUIEventListener(event, handler);
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    getDataContainer().fireTarentGUIEvent(e);
  }

  public void event(TarentGUIEvent tge)
  {
  }

  public String getListenerName()
  {
    return "DataContainerListenerActionAdapter";
  }
}
