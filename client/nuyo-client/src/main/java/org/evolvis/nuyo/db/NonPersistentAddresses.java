/* $Id: NonPersistentAddresses.java,v 1.13 2007/08/30 16:10:29 fkoester Exp $
 * 
 * Created on 05.06.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.commons.datahandling.entity.AbstractEntityList;
import de.tarent.commons.datahandling.entity.EntityListEvent;

/**
 * Diese Klasse h�lt tempor�re (nicht persistente) Sammlungen persistenter Adressen.
 * 
 * @author mikel
 */
public class NonPersistentAddresses extends AbstractEntityList implements Addresses {
    
    private static Logger logger = Logger.getLogger(NonPersistentAddresses.class.getName());
   
    protected Database db = null;
    protected List addresses = new ArrayList();
   
    /**
     * Der Konstruktor merkt sich eine assoziierte Database.
     * 
     * @param db
     */
    public NonPersistentAddresses(Database db) {
        this.db = db;
    }
    public NonPersistentAddresses() {
       
    }
    

    /**
     * Diese Methode f�gt eine Adresse zu der Sammlung hinzu.
     * 
     * @param address die hinzuzuf�gende Adresse
     */
    public void addAddress(Address address) {
        
        if (!addresses.contains(address)){
            addresses.add(address);
            fireEntityListChanged(new EntityListEvent(this,EntityListEvent.INSERT));
        } else {
            logger.fine("Adressobjekt ist bereits in Adressliste vorhanden, wird nicht erneut hinzugef�gt");
        }
    }
    /**
     * Diese Methode entfernt eine Adresse aus der Sammlung
     * 
     * @param address die zu entfernende Adresse
     */
    
    public void removeAddress(Address address){
        addresses.remove(address);
        fireEntityListChanged(new EntityListEvent(this,EntityListEvent.DELETE));
    } 


    /**
     * Reloads the address set with the same AddressListParameter.
     * If the address set if not persistent, this action may do nothing. 
     */
    public void reload() {
        // do nothing here
    }
    
    /**
     * Diese Methode leert die Adress-Sammlung
     */
    public void clear() {
        addresses.clear();
    }

    /**
     * Diese Methode liefert eine Liste der gesammelten Adressen.
     *  
     * @return die gesammelten Adressen
     * @throws ContactDBException 
     */
    public List list() throws ContactDBException {
        /*List result = new ArrayList(addresses.size());
        Iterator it = addresses.iterator();
        while (it.hasNext())
            result.add(db.getAddress(((Integer)it.next()).intValue()));
        return result;*/
        List result = new ArrayList(addresses.size());
        result.addAll(addresses);
       
        return result;
    }

    /*
     * �berschriebene abstrakte Methoden von Addresses 
     */
    
     // @see de.tarent.contact.db.Addresses#getIndexMax()
     
    public int getIndexMax() {
        return addresses.size() - 1;
    }

    
    
     // @see de.tarent.contact.db.Addresses#getAddressNumber(int)
     
    public int getAddressNumber(int index){
        if (index >= 0 && index < addresses.size())
            return ((Address)addresses.get(index)).getAdrNr();
        return Address.ADR_NR_NEW;
    }

    
     // @see de.tarent.contact.db.Addresses#get(int)
     
    public Address get(int index) {
        /*int addrNr = getAddressNumber(index);
        if (addrNr != Address.ADR_NR_NEW) try {
            return db.getAddress(addrNr);
        } catch (ContactDBException sex) {
        }
        return null;*/
        return (Address) addresses.get(index);
    }

    
    
     //@see de.tarent.contact.db.Addresses#sortAddressesByPreview()
   
    public void sortAddressesByPreview() throws ContactDBException
    {
      // TODO Auto-generated method stub
      
    }

    /**
     * Sets a filter on the addresses
     * @param listParameter Filter list parameters
     */
    public void setAddressListParameter(AddressListParameter listParameter) {
    }

    /**
     * Returns the filter on the addresses
     */
    public AddressListParameter getAddressListParameter() {
        return null;
    }

    public void addEntity(Object entity) {
    	addAddress((Address) entity);
    }

    public void removeEntity(Object entity) {
       removeAddress((Address)entity);
    }

    public Object getEntityAt(int index) {
        return get(index);
        
    }

    public int getSize() {
        return addresses.size();
    }

    public boolean contains(Object addressObject){
        if (! (addressObject instanceof Address))
            return false;
        Address address = (Address)addressObject;
            
        for(Iterator iterator=addresses.iterator();iterator.hasNext();){
            if (((Address)iterator.next()).getAdrNr()==address.getAdrNr()) return true;
        } 
    	return false;
    }

	public int indexOf(Object entity) {
		return addresses.indexOf(entity);
	}

    /**
     * Returns a list of all pks as Integer Objects.
     */
	public PrimaryKeyList getPkList() {
        PrimaryKeyList list = new PrimaryKeyList();
        for (Iterator iter = addresses.iterator(); iter.hasNext();) {
            Address address = (Address)iter.next();
            list.add(new Integer(address.getId()));
        }
        return list;
    }
	
    /**
     * Returns a String list of all pks sepperated by " ".
     */
	public String getPkListAsString() {
        StringBuffer sb = new StringBuffer();
        for (Iterator iter = addresses.iterator(); iter.hasNext();) {
            Address address = (Address)iter.next();
            sb.append(""+address.getId());
            if (iter.hasNext())
                sb.append(" ");
        }
        
        return sb.toString();
    }
}
