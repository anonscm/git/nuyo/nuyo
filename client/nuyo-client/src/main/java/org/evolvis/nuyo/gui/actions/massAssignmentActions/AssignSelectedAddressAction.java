package org.evolvis.nuyo.gui.actions.massAssignmentActions;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.utils.TaskManager;

/**
 * An action which allows assigning the currently selected address to categories.
 * 
 */
public class AssignSelectedAddressAction extends
		AssignOrDeassignAddressesAction {
	

	private static final TarentLogger log = new TarentLogger(
			AssignSelectedAddressAction.class);

	public CategoryTree actionPerformedImpl() {
		GUIListener am = ApplicationServices.getInstance().getActionManager();

		// Puts the address' pk in a list as this is what the underlying API
		// expects.
		List addressPk = new ArrayList(1);
		addressPk.add(new Integer(am.getAddress().getId()));

		if (addressPk == null || addressPk.size() == 0){
			log.warning(Messages.getString("AssignOrDeassignAddresses_No_Addresses_Warning"));
			return null;
		}
		// Fiddles out the categories on which we can assign the address to
		// and the ones where the address is currently assigned to.
		List allCategories = null;
		List accessableCategories = null;

		try {
			allCategories = ApplicationServices.getInstance().getCurrentDatabase()
				.getCategoriesWithCategoryRightsOR(false, false, true, true, false, false, true, false, true);
			accessableCategories = ApplicationServices.getInstance().getCurrentDatabase()
				.getAssociatedCategoriesForSelectedAddress(new Integer(am.getAddress().getId()));
			
		} catch (ContactDBException e1) {
			log.warning(Messages.getString("AssignOrDeassignAddresses_Error_Warning"));
		}

		// Prepares a category tree that can display the action and makes it visible.
		CategoryTree tree = CategoryTree.createSingleAssignmentCategoryTree(
				allCategories, accessableCategories,
				new AssignSelectedAddressHandler(addressPk));
		
		return tree;
	}

	/**
	 * {@link CategoryTree.AssignHandler} implementation that is invoked
	 * after the user has chosen to continue or cancel the operation.
	 *
	 * <p>This engages a task which does the actual assignment.</p>
	 */
	protected class AssignSelectedAddressHandler implements
			CategoryTree.AssignHandler {

		private List addressPk;

		public AssignSelectedAddressHandler(List addressPk) {
			this.addressPk = addressPk;
		}

		public void cancel() {
			closeCategoryTree();
		}

		public void submit(List addSubCats, List removeSubCats) {
			List addSubCatPks = asPkList(addSubCats);
			List removeSubCatPks = asPkList(removeSubCats);

			AssignAddressesTask assignAddressesTask = new AssignAddressesTask(
					addressPk, addSubCatPks, removeSubCatPks, categoryTreeDialog);
			TaskManager.getInstance().register(assignAddressesTask,
					Messages.getString("GUI_AssignSelectedAddressAction_Update"), false);
			hideCategoryTree();
		}
	}
}
