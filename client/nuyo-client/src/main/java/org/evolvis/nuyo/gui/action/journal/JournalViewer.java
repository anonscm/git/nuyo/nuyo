package org.evolvis.nuyo.gui.action.journal;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.evolvis.nuyo.db.HistoryElement;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.EscapeDialog;

public final class JournalViewer {
	TarentLogger logger = new TarentLogger(JournalViewer.class);

	JDialog dialog;

	JList journalEntryList;

	JTextField address;

	JTextField title;

	JTextField date;

	JTextField category;

	JTextField channel;

	JTextField user;

	JTextField direction;

	JTextField duration;

	JTextArea note;

	JButton close;

	JButton createNew;

	JournalViewerModel model;
	
	JournalEditor journalEditor;

	public JournalViewer(Frame owner) {
		dialog = new EscapeDialog(owner, Messages
				.getString("GUI_JournalViewer_Title"), true);

		init();
	}

	public JournalViewer(Dialog owner) {
		dialog = new EscapeDialog(owner, Messages
				.getString("GUI_JournalViewer_Title"), true);

		init();
	}
	
	/**
	 * Sets all UI components to the empty string.
	 */
	private void clear() {
		address.setText("");
		title.setText("");
		date.setText("");
		category.setText("");
		user.setText("");
		direction.setText("");
		duration.setText("");
		note.setText("");
	}

	private void init() {
		model = new JournalViewerModel(new JournalViewerModel.Listener() {
			public void update() {
				address.setText(model.address);
				title.setText(model.title);
				date.setText(model.date);
				category.setText(model.category);
				channel.setText(model.channel);
				user.setText(model.user);
				direction.setText(model.direction);
				duration.setText(model.duration);
				note.setText(model.note);
			}
		});
		
		journalEditor = new JournalEditor(dialog);

		addContents(dialog.getContentPane());

		dialog.pack();

		dialog.setLocationRelativeTo(null);

	}

	private void addContents(Container comp) {
		FormLayout l = new FormLayout(
				"3dlu, pref:grow, 6dlu, pref, 3dlu, pref, 3dlu:grow, pref, 3dlu, pref, 3dlu",
				"3dlu, pref, 3dlu, pref, 6dlu, pref, 3dlu, pref, 3dlu, pref, 6dlu, fill:pref:grow, 12dlu, pref, 3dlu");

		l.setColumnGroups(new int[][] { { 4, 8 }, { 6, 10 } });
		comp.setLayout(l);

		CellConstraints cc = new CellConstraints();

		journalEntryList = new JList(model.getListModel());

		// Next line is not really nice. What was wanted here is that a certain
		// minimum width
		// is respected by the JList. However JList has no friendly methods for
		// this
		// (e.g. for
		// setting the minimum char width like textfields). A better way would
		// be to
		// calculate the
		// minimum width using a Font[Metrics] instance for the string below.
		journalEntryList.setFixedCellWidth(320);
		// journalEntryList.setPrototypeCellValue(new Integer("31.12.2007 title
		// title title title title title".length()));

		journalEntryList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		journalEntryList.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent e) {
				model.setSelectedEntry((HistoryElement) journalEntryList
						.getSelectedValue());
			}

		});
		journalEntryList.setCellRenderer(model.getRenderer());
		comp.add(new JScrollPane(journalEntryList), cc.xywh(2, 2, 1, 11));

		address = make();
		comp.add(address, cc.xyw(4, 2, 7));

		title = make();
		comp.add(title, cc.xyw(4, 4, 7));

		comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Date")), cc
				.xy(4, 6));
		date = make();
		comp.add(date, cc.xy(6, 6));

		comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Category")),
				cc.xy(4, 8));
		category = make();
		comp.add(category, cc.xy(6, 8));

		comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Channel")),
				cc.xy(4, 10));
		channel = make();
		comp.add(channel, cc.xy(6, 10));

		comp.add(new JLabel(Messages.getString("GUI_JournalViewer_User")), cc
				.xy(8, 6));
		user = make();
		comp.add(user, cc.xy(10, 6));

		comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Direction")),
				cc.xy(8, 8));
		direction = make();
		comp.add(direction, cc.xy(10, 8));

		comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Duration")),
				cc.xy(8, 10));
		duration = make();
		comp.add(duration, cc.xy(10, 10));

		note = new JTextArea(10, 5);
		note.setEditable(false);
		note.setLineWrap(true);
		note.setWrapStyleWord(true);
		comp.add(new JScrollPane(note), cc.xyw(4, 12, 7));

		PanelBuilder pb = new PanelBuilder(new FormLayout(
				"3dlu:grow, pref, 3dlu, pref", "pref"));

		close = new JButton(Messages.getString("GUI_JournalViewer_Close"));
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				dialog.setVisible(false);
			}
		});
		pb.add(close, cc.xy(4, 1));

		createNew = new JButton(Messages
				.getString("GUI_JournalViewer_CreateNew"));
		createNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				journalEditor.setupNewEntry();
				// Shows journal editor and updates the journal data
				// if a new entry has been created.
				if (journalEditor.show() == JOptionPane.OK_OPTION)
					model.updateJournal();
			}
		});
		pb.add(createNew, cc.xy(2, 1));

		comp.add(pb.getPanel(), cc.xyw(2, 14, 9));
	}

	private static JTextField make() {
		JTextField tf = new JTextField(10);
		tf.setEditable(false);

		return tf;
	}

	/**
	 * Makes the {@link JournalViewer} visible and blocks
	 * further execution until the dialog is made invisible
	 * by the user.
	 * 
	 * <p>Additionally the journal data for the address is
	 * updated.</p>
	 */
	public void show() {
		model.updateJournal();

		dialog.setVisible(true);

		model.reset();
		clear();
	}

}
