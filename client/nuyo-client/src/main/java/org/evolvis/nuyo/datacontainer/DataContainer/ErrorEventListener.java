/*
 * Created on 29.09.2004
 */
package org.evolvis.nuyo.datacontainer.DataContainer;


/**
 * @author niko
 *
 */
public interface ErrorEventListener
{
  public void errorEvent(DataContainerErrorEvent event);
}
