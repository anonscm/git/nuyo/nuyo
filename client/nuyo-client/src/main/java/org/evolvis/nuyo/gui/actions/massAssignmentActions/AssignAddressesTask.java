package org.evolvis.nuyo.gui.actions.massAssignmentActions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.octopus.CategoryClientDAOImpl;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Context;

class AssignAddressesTask implements TaskManager.Task {

	private static final TarentLogger log = new TarentLogger(
			AssignAddressesTask.class);

	private List addressPks;

	private List addSubcategories = null;

	private List removeSubcategories = null;

	private List removeAddressesBlocked = null;

	private List addAddressesBlocked = null;

	private List implicitlyDeletedAddresses = null;

	private Boolean forceDeletion = null;
	
	private JDialog categoryTreeDialog; 

	public AssignAddressesTask(List addressPks, List addSubCats, List removeSubCats, JDialog categoryTreeDialog) {
		this.addressPks = addressPks;
		this.addSubcategories = addSubCats;
		this.removeSubcategories = removeSubCats;
		this.categoryTreeDialog = categoryTreeDialog;
	}

	public void cancel() {
	}

	public void run(Context arg0) {

		int choosenDeleteOption = 0;

		Object[] savetyquestionOptions = {
				Messages.getString("GUI_AssignOrDeassignAddressAction_Yes"),
				Messages
						.getString("GUI_AssignOrDeassignAddressAction_Cancel") };

		String txt = null;
		if (addressPks.size() == 1) 
			txt = Messages.getString("GUI_AssignOrDeassignAddressAction_SingleAssignQuestion");
		else txt = Messages.getFormattedString("GUI_AssignOrDeassignAddressAction_AssignQuestion", new Integer(addressPks.size()));
		
		int choosenSavetyOption = JOptionPane
				.showOptionDialog(
						null,
						txt,
						Messages.getString("GUI_AssignOrDeassignAddressAction_Title"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.PLAIN_MESSAGE, null,
						savetyquestionOptions, savetyquestionOptions[0]);

		if (choosenSavetyOption == JOptionPane.NO_OPTION) {
			showCategoryTree();
			return;
		}

		callServerOperation(false, null);

		/**
		 * First check if process has been aborted because of implicitly
		 * deleted addresses and ask the user how to proceed in this case
		 */
		
		/*
		 * If there is just one subcategory in the removelist, check if its the subcategory of the trash category
		 */
		boolean justTheTrashToRemove = false;
		if (removeSubcategories != null && removeSubcategories.size() == 1){
			try {
				int removeSubCat = ((Integer)removeSubcategories.get(0)).intValue();
				justTheTrashToRemove = ApplicationServices.getInstance().getCurrentDatabase().getTrashCategory() != null &&(ApplicationServices.getInstance().getCurrentDatabase().getTrashCategory().getSubCategory(removeSubCat) != null);
			} catch (ContactDBException e) {
				log.warning("Error in massassignment. Trash category could not be acquired.");
			}
		}
			
		
		
		if (!justTheTrashToRemove && implicitlyDeletedAddresses != null && implicitlyDeletedAddresses.size() != 0) {

			Object[] deleteOptions = {
					Messages.getString("GUI_AssignOrDeassignAddressAction_ContinueWithDelete"),
					Messages.getString("GUI_AssignOrDeassignAddressAction_ContinueWithoutDelete"),
					Messages.getString("GUI_AssignOrDeassignAddressAction_Cancel"), };

			String text = Messages
					.getFormattedString(
							"GUI_AssignOrDeassignAddressAction_DeletionQuestion",
							new Object[] {
									new Integer(addressPks.size()),
									new Integer(implicitlyDeletedAddresses
											.size()) });

			choosenDeleteOption = JOptionPane.showOptionDialog(null, text,
					"Hinweis", JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE, null, deleteOptions,
					deleteOptions[0]);

			switch (choosenDeleteOption) {
			case JOptionPane.NO_OPTION:
				// retry and force without deletion of addresses
				forceDeletion = new Boolean(false);
				callServerOperation(false, forceDeletion);
				break;

			case JOptionPane.YES_OPTION:
				// retry and force with deletion of addresses
				forceDeletion = new Boolean(true);
				callServerOperation(false, forceDeletion);
				break;

			default:
				// abort and return to CategoryTree
				forceDeletion = null;
				showCategoryTree();
				return;
			}
		}
		/*
		 * If the only subcategory in the removelist is the subcategory of the trash category
		 * ignore warning and force "deletion" from trash
		 */
		else {
			forceDeletion = new Boolean(true);
			callServerOperation(false, forceDeletion);
		}

		/**
		 * If any addresses were blocked ask user what to do
		 */

		if ((addAddressesBlocked != null && addAddressesBlocked.size() != 0)
			|| (removeAddressesBlocked != null && removeAddressesBlocked	.size() != 0)) {
			
			int size = (addressPks.size() 
					- ((addAddressesBlocked != null ? addAddressesBlocked.size(): 0) 
					+ (removeAddressesBlocked != null ? removeAddressesBlocked.size(): 0)));
			
			String text = null;
			if (removeSubcategories == null)
			{
				text = Messages.getFormattedString("GUI_AssignOrDeassignAddressAction_RightsQuestion_Assign", new Object[] { new Integer(addressPks.size()), new Integer(size) });
			}
			else if (addSubcategories == null)
			{
				text = Messages.getFormattedString("GUI_AssignOrDeassignAddressAction_RightsQuestion_Remove", new Object[] { new Integer(addressPks.size()), new Integer(size) });
			}
			else if (addSubcategories != null && removeSubcategories != null)
			{
				text = Messages.getFormattedString("GUI_AssignOrDeassignAddressAction_RightsQuestion_Move", new Object[] { new Integer(addressPks.size()), new Integer(size) });
			}

			Object[] restrictedOptions = {
					Messages.getString("GUI_AssignOrDeassignAddressAction_Restricted"),
					Messages.getString("GUI_AssignOrDeassignAddressAction_Cancel")
					};

			int choosenRestrictedOption = JOptionPane.showOptionDialog(
					null, text,
					Messages.getString("GUI_AssignOrDeassignAddressAction_RestrictedTitle"),
					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE,
					null, restrictedOptions, restrictedOptions[0]);

			if (choosenRestrictedOption == JOptionPane.YES_OPTION) {
				// retry and ignore blocked addresses
				callServerOperation(true, forceDeletion);
			}
			// abort and return to CategoryTree
			else {
				showCategoryTree();
				return;
			}
			
			closeCategoryTree();
		}
		
		// After a successfull assignment change the address data in the client is outdated
		// and needs to be reloaded.
		arg0.setActivityDescription(Messages.getString("GUI_AssignOrDeassignAddressAction_ReloadingAddresses"));
		ApplicationServices.getInstance().getActionManager().reloadAddresses();
	}

	private void callServerOperation(boolean force, Boolean forceWithDeletion) {

		ApplicationServices as = ApplicationServices.getInstance();
		
		Map result = new HashMap();
		try {
			// TODO: This needs a proper method in the ActionManager (aka GUIListener)
			result = as.getCurrentDatabase().getCategoryDAO().assignOrDeassignAddresses(new Boolean(force),forceWithDeletion, addressPks, addSubcategories, removeSubcategories);
			
			// Updates the category list in the application model (this is neccessary for e.g the category assignment view ["zugeordnet tab"]
			// to be updated on each assignment change.
			as.getApplicationModel().setCategoryAssignment(as.getCurrentDatabase().getAssociatedCategoriesForSelectedAddress(
					new Integer(as.getActionManager().getAddress().getId())));

		} catch (ContactDBException e) {
			log.warning(Messages.getString("AssignOrDeassignAddresses_Error_Warning"));
		}		
		
		implicitlyDeletedAddresses = (List) result.get(CategoryClientDAOImpl.ASSIGNORDEASSIGNADDRESSES_IMPLICITLYDELTEDADDRESSES);
		addAddressesBlocked = (List) result.get(CategoryClientDAOImpl.ASSIGNORDEASSIGNADDRESSES_NOTASSIGNABLEADDRESSES);
		removeAddressesBlocked = (List) result.get(CategoryClientDAOImpl.ASSIGNORDEASSIGNADDRESSES_NOTDEASSIGNABLEADDRESSES);
	}
	
	protected void showCategoryTree() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				categoryTreeDialog.setVisible(true);
			}
		});

	}

	protected void hideCategoryTree() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				categoryTreeDialog.setVisible(false);
			}
		});
	}

	protected void closeCategoryTree() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				categoryTreeDialog.setVisible(false);
				categoryTreeDialog.dispose();
			}
		});
	}
}