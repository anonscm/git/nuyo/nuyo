/* $Id: UserGlobalRoleImpl.java,v 1.4 2006/12/12 09:30:57 robert Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.GlobalRole;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.UserId;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;


/**
 *
 * Globale Rechte eines Users die sich aus allen Rollen der Gruppen zusammen setzen.
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class UserGlobalRoleImpl extends GlobalRoleBean {

	/**Methode zum Abfragen*/
	public static final String METHOD_GET_USER_GLOBAL_RIGHT = "getUserGlobalRight";

    public final static String KEY_ENTITY = "entity";

   
	public static GlobalRole getRoleForCurrentUser() throws ContactDBException {
        return getRoleForUser(null);
    }

    /**
     * Erstellt eine Instanz der Rechte eines Users 
     * @param id ID des Users, oder null f�r den aktuellen User
     */
	public static GlobalRole getRoleForUser(Integer userId) throws ContactDBException {
        Method method = new Method(METHOD_GET_USER_GLOBAL_RIGHT);
        if (userId != null)
            method.addFilter( Selection.getInstance().add(new UserId(userId)) );  
        
        method.invoke();
        UserGlobalRoleImpl newEntity = new UserGlobalRoleImpl();
        newEntity.populate(new AbstractEntityFetcher.ResponseData((Map)method.getORes().getData(KEY_ENTITY)));
        return newEntity;
	}

	public void populate(ResponseData data) throws ContactDBException {
		_responseData = null;
		_id = new Integer(-1);
        for (int i = 0; i < AUTH_KEYS.length; i++)
            setAttribute(AUTH_KEYS[i], data.getBoolean(AUTH_KEYS[i]));
    }




	public void validate() throws ContactDBException {
        // DO NOTHING HERE
	}
	public void commit() throws ContactDBException {
        // DO NOTHING HERE
	}
	public void delete() throws ContactDBException {
        // DO NOTHING HERE
	}
	public void rollback() throws ContactDBException {
        // DO NOTHING HERE
	}

}
