/* $Id: MasterDataListener.java,v 1.4 2006/12/12 09:31:08 robert Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.event;

import org.evolvis.nuyo.db.UserGroup;

/**
 * Diese Schnittstelle muss von Klassen implementiert werden, die von der
 * Contact-Kontrolle �ber �nderungen in den Stammdaten benachrichtigt werden
 * wollen.
 * 
 * @author mikel
 */
public interface MasterDataListener {
    /**
     * Diese Methode teilt mit, dass eine neue Kategorie dem System
     * hinzugef�gt worden ist.
     * 
     * @param categoryID ID der neuen Kategorie
     * @param categoryName Name der neuen Kategorie
     * @param categoryDescription Beschreibung der neuen Kategorie
     */
    public void categoryAdded(String categoryID, String categoryName, String categoryDescription);

    /**
     * Diese Methode teilt mit, dass eine Kategorie des System
     * editiert worden ist.
     * 
     * @param categoryID ID der Kategorie
     * @param categoryName Name der Kategorie
     * @param categoryDescription Beschreibung der Kategorie
     */
    public void categoryEdited(String categoryID, String categoryName, String categoryDescription);

    /**
     * Diese Methode teilt mit, dass eine Kategorie aus dem System
     * gel�scht worden ist.
     * 
     * @param categoryID ID der gel�schten Kategorie
     */
    public void categoryRemoved(String categoryID);

    /**
     * Diese Methode teilt mit, dass eine Kategorie einem Benutzer
     * zug�nglich gemacht worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     */
    public void categoryAssigned(String userID, String categoryID);

    /**
     * Diese Methode teilt mit, dass Zugang zu einer Kategorie einem Benutzer
     * genommen worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     */
    public void categoryDeassigned(String userID, String categoryID);

    /**
     * Diese Methode teilt mit, dass eine Kategorie einem Benutzer
     * als Standardkategorie zugeordnet worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     */
    public void standardCategoryAssigned(String userID, String categoryID);

    /**
     * Diese Methode teilt mit, dass eine neue Unterkategorie dem System
     * hinzugef�gt worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der neuen Unterkategorie
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     */
    public void subCategoryAdded(String categoryID, String id, String name1, String name2, String name3);

    /**
     * Diese Methode teilt mit, dass eine Unterkategorie des System
     * editiert worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der Unterkategorie
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     */
    public void subCategoryEdited(String categoryID, String id, String name1, String name2, String name3);

    /**
     * Diese Methode teilt mit, dass eine Unterkategorie aus dem System
     * gel�scht worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der gel�schten Unterkategorie
     */
    public void subCategoryRemoved(String categoryID, String id);

    /**
     * Diese Methode teilt mit, dass eine neue Benutzergruppe dem
     * System hinzugef�gt worden ist. 
     * 
     * @param newGroup die neue Benutzergruppe
     */
    public void userGroupAdded(UserGroup newGroup);
    
    /**
     * Diese Methode teilt mit, dass ein neuer Benutzer dem System
     * hinzugef�gt worden ist.
     * 
     * @param userID ID des neuen Benutzers
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param email
     */
    public void userAdded(String userID, String lastName, String firstName, String email);

    /**
     * Diese Methode teilt mit, dass ein Benutzer aus dem System
     * entfernt worden ist.
     * 
     * @param userID ID des Benutzers
     */
    public void userRemoved(String userID);

  /**
   * Diese Methode teilt mit, dass ein Benutzer
   * ge�ndert worden ist.
   * 
   * @param userID ID des bestehenden Benutzers
   * @param lastName der Nachname des Benutzers.
   * @param firstName der Vorname des Benutzers.
   * @param email
   */
  public void userChanged(String userID, String lastName, String firstName, String email);



}
