/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextViewField;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 */
public class AenderungsdatumAlterBestandField extends GenericTextViewField
{
  protected final static DateFormat myDateFormatter = SimpleDateFormat.getDateInstance();
  
  public AenderungsdatumAlterBestandField()
  {
    super("AENDERUNGSDATUMALTERBESTAND", AddressKeys.AENDERUNGSDATUMALTERBESTAND, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_AenderungsdatumAlterBestand_ToolTip", "GUI_MainFrameNewStyle_Standard_AenderungsdatumAlterBestand", 0);
  }
  
  public void setData(PluginData data)
  {
      //	kommt immer als String
      Object value = data.get(m_AdressKey);
    if (value != null) m_oTextfield.setText(value.toString()); 
    else                    m_oTextfield.setText("");
  }
}
