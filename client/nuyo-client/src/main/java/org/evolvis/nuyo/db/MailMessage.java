/* $Id: MailMessage.java,v 1.3 2006/06/06 14:12:08 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink and Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.3 $
 */
public interface MailMessage {
	//
	// Attribute
	//
	/**
	 * Store, in dem die Mitteilung liegt
	 */
	public MailStore getStore() throws ContactDBException;
	
	/**
	 * ID des Stores, in dem die Mitteilung liegt
	 */
	public int getStoreId() throws ContactDBException;
	
	/**
	 * Folders, in dem die Mitteilung liegt
	 */
	public MailFolder getFolder() throws ContactDBException;;
	
	/**
	 * ID des Folders, in dem die Mitteilung liegt
	 */
	public int getFolderId() throws ContactDBException;
	
	/**
	 * ID der Mitteilung
	 */
	public int getMessageId() throws ContactDBException;
	
	
    /**
     * Diese Methode aktualisiert gecachete Daten zur Mitteilung. 
     * 
     * @throws ContactDBException
     */
	public void reload() throws ContactDBException;
	
	/**
	 * Diese Methode l�scht diese Mitteilung. 
	 * 
	 * @throws ContactDBException
	 */
	public void delete() throws ContactDBException;
	
	//
	// Verzeichnis-Methoden
	//
	/**
	 * Diese Methode kopiert diese Mitteilung in einen Zielfolder. 
	 * 
	 * @param target
	 * @throws ContactDBException
	 */
	public void copyTo(MailFolder target) throws ContactDBException;
	
	/**
	 * Diese Methode verschiebt diese Mitteilung in einen Zielfolder.
	 * 
	 * @param target
	 * @throws ContactDBException
	 */
	public void moveTo(MailFolder target) throws ContactDBException;
	
	
	//
	// Inhalts-Methoden
	//
	/**
	 * Betreff
	 */
	public String getSubject() throws ContactDBException;
	
	/**
	 * Inhalt als Liste der verschiedenen Bodyteile (Maps).
	 */
	public List getContent() throws ContactDBException;
	
	/**
	 * Gibt Attachment als InputStream zur�ck,
	 * dass in der eMail an Position id liegt.
	 */
	public InputStream getAttachment(int id) throws ContactDBException, IOException;
	
	/**
	 * Gibt den Dateinamen des Attachments zur�ck,
	 * dass in der eMail an Position id liegt.
	 */
	public String getAttachmentFilename(int id) throws ContactDBException;
	
	/**
	 * Versendedatum
	 */
	public Date getSentDate() throws ContactDBException;
	
	/**
	 * Gibt den eMail-Header als Map zur�ck.
	 */
	public Map getHeader() throws ContactDBException;
	/**
	 * Gibt ein Value des eMail-Header zur�ck.
	 */
	public String getHeader(String key) throws ContactDBException;
	
	/**
	 * Liste der Absender, Empf�nger, ... - Adressen, jeweils als String.
	 */
	public Collection getFrom() throws ContactDBException;
	public Collection getTo() throws ContactDBException;
	public Collection getCc() throws ContactDBException;
	public Collection getBcc() throws ContactDBException;
	public Collection getReplyTo() throws ContactDBException;
	
	/**
	 * eine Antwort existiert
	 */
	public boolean isAnswered() throws ContactDBException;
	
	/**
	 * als gel�scht markiert
	 */
	public boolean isDeleted() throws ContactDBException;
	
	/**
	 * ist ein Entwurf, noch nicht versendet
	 */
	public boolean isDraft() throws ContactDBException;
	
	/**
	 * "This message is flagged."
	 */
	public boolean isFlagged() throws ContactDBException;
	
	/**
	 * "This message is recent."
	 */
	public boolean isRecent() throws ContactDBException;
	
	/**
	 * "This message is seen."
	 */
	public boolean isSeen() throws ContactDBException;
	
	/**
	 * @param key
	 * @param newValue
	 * @throws ContactDBException
	 */
	public void setFlag(String key, boolean newValue) throws ContactDBException;
	
	/**
	 * @param key
	 * @return
	 * @throws ContactDBException
	 */
	public boolean getFlag(String key) throws ContactDBException;
	
	//
    // Konstanten
    //
	public final static String KEY_FLAG_ANSWERED = "ANSWERED";
	public final static String KEY_FLAG_DELETED = "DELETED";
	public final static String KEY_FLAG_DRAFT = "DRAFT";
	public final static String KEY_FLAG_FLAGGED = "FLAGGED";
	public final static String KEY_FLAG_RECENT = "RECENT";
	public final static String KEY_FLAG_SEEN = "SEEN";
}