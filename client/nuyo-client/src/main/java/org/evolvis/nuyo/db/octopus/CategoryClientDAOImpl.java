package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.CategoryClientDAO;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.groupware.category.CategoryList;
import org.evolvis.nuyo.remote.Method;

import de.tarent.commons.datahandling.FilterNode;
import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.octopus.client.OctopusResult;

public class CategoryClientDAOImpl implements CategoryClientDAO {
	
	private static final String TASKNAME_ASSIGNADDRESSES = "assignAddresses";
	private static final String ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_ADDRESSPKS = "addressPks";
	private static final String ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_SUBCATEGORIESTOADD = "addSubCategories";
	private static final String ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_SUBCATEGORIESTOREMOVE = "removeSubCategories";
	private static final String ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_FORCE = "force";
	private static final String ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_FORCEWITHDELETION = "forceWithDeletion";
	public static final String ASSIGNORDEASSIGNADDRESSES_NOTASSIGNABLEADDRESSES = "notAssignableAddresses";
	public static final String ASSIGNORDEASSIGNADDRESSES_NOTDEASSIGNABLEADDRESSES = "notDeassignableAddresses";
	public static final String ASSIGNORDEASSIGNADDRESSES_IMPLICITLYDELTEDADDRESSES = "implicitlyDeletedAddresses";
	
	OctopusDatabase db;

    public CategoryClientDAOImpl(OctopusDatabase db) {
        this.db = db;
    }

	public void delete(Category category) throws ContactDBException {
		    Method method = new Method("deleteVerteilergruppeSOAP");
	        method.add("key", category.getIdAsString());
	        method.invoke();
	}
	
	public void save(Category category) throws ContactDBException {
	    Method method = new Method("createOrModifyCategory");
        method.add(Category.PROPERTY_ID.getKey(), category.getId() != 0? category.getIdAsString(): null);
        method.add(Category.PROPERTY_CATEGORYNAME.getKey(), category.getCategoryName());
        method.add(Category.PROPERTY_DESCRIPTION.getKey(), category.getDescription());
        String new_id = ""+ method.invoke();
        category.setId(new Integer(new_id).intValue());
	}
	
	

	public CategoryList getAssociatedCategoriesForSelectedAddress(Integer addressPk) throws ContactDBException{
		List addressPks = new ArrayList();
		addressPks.add(addressPk);
		return getAssociatedCategories(addressPks, false, false, false, false, false, false, false, false, true);
	}
	
	
	public CategoryList getAssociatedCategoriesForSelectedAddress(Integer addressPk, List filterList, boolean includeVirtual) throws ContactDBException{
		List addressPks = new ArrayList();
		addressPks.add(addressPk);
		return getAssociatedCategories(addressPks, filterList, includeVirtual);
	}


	public CategoryList getAssociatedCategoriesForCurrentSelection(List addressPks,
			boolean read, boolean edit, boolean add, boolean remove, 
			boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean inludeVirtual) throws ContactDBException{
		
		return getAssociatedCategories(addressPks, read, edit, add, remove, addSubCat, removeSubCat, structure, grant, inludeVirtual);
	}
	
	public CategoryList getAssociatedCategoriesForCurrentSelection(List addressPks, List filterList, boolean includeVirtual) throws ContactDBException{		
		return getAssociatedCategories(addressPks, filterList, includeVirtual);
	}
	
	public CategoryList getAssociatedCategories(
			List addressPks,	 boolean read, boolean edit, boolean add, boolean remove, 
			boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean inludeVirtual) throws ContactDBException{ 
	
		List filterList = new ArrayList();
		
		// all categories have to be readable
		if (read)
			new FilterNode(Category.PROPERTY_RIGHTREAD.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToList(filterList);
		if (edit)
			new FilterNode(Category.PROPERTY_RIGHTEDIT.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (add)
			new FilterNode(Category.PROPERTY_RIGHTADD.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (remove)
			new FilterNode(Category.PROPERTY_RIGHTREMOVE.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (addSubCat)
			new FilterNode(Category.PROPERTY_RIGHTADDSUBCAT.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (removeSubCat)
			new FilterNode(Category.PROPERTY_RIGHTREMOVESUBCAT.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (structure)
			new FilterNode(Category.PROPERTY_RIGHTSTRUCTURE.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (grant)
			new FilterNode(Category.PROPERTY_RIGHTGRANT.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListOR(filterList);
		if (addressPks == null || addressPks.size() == 0)
			return null;
		
		
		return getAssociatedCategories(addressPks, filterList, inludeVirtual);
	}
	
	public CategoryList getAssociatedCategories(	List addressPks, List filterList, boolean includeVirtual) throws ContactDBException{
		
		if (addressPks == null || addressPks.size() == 0)
			return null;
		
		Method method = new Method("getAssociatedCategoriesByAddressPks");
		method.add(AddressWorkerConstants.PARAM_CATEGORIES_FILTER+"."+ListFilter.PARAM_FILTER_LIST, filterList);
		method.add("addressPks", addressPks);		
		method.invoke();
		
		OctopusResult result = method.getORes();
		CategoryList resultCategories = (CategoryList) result.getData("associatedCategories");
		
		CategoryList categoriesWithOrWithoutVirtual = new CategoryList();
		
		//TODO this is just a hack. inludevirtual should be filtered on server by filterlist
		for (Iterator iter = resultCategories.iterator(); iter.hasNext(); ){
			Category cat = (Category)iter.next();
			if ( cat.getVirtual() == null || !cat.getVirtual().booleanValue() || (cat.getVirtual().booleanValue() && includeVirtual))
				categoriesWithOrWithoutVirtual.add(cat);
		}
			
		
		return categoriesWithOrWithoutVirtual;
	}

	/**
 	 * 
 	 * @param force flag to force (de)assignment even if some addresses can't be (de)assigned
 	 * @param forceWithDeletion flag to allow deletion of addresses (cancel the last conenctions to categories)  
 	 * @param addressPks List of addresses to (de)assign
 	 * @param addSubCategories List of subcategory pks 
 	 * @param removeSubCategories List of subcategory pks
 	 * @return Map with lists of addresspks accessable with the keys "notAssignableAddresses", "notDeassignableAddresses", "implicitlyDeletedAddresses"
	 * @throws ContactDBException 
 	 */
	public Map assignOrDeassignAddresses(Boolean force, Boolean forceWithDeletion, List addressPks, List addSubCategories, List removeSubCategories) throws ContactDBException {
		
		Method method = new Method(TASKNAME_ASSIGNADDRESSES);
		// put the parameters:
		// System.out.println(CONTENT_KEY_ADDRESSPKS + ": " + addressPks);
		method.add(ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_ADDRESSPKS, addressPks);
		method.add(ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_SUBCATEGORIESTOADD, addSubCategories);
		method.add(ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_SUBCATEGORIESTOREMOVE, removeSubCategories);
		method.add(ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_FORCE, force);
		method.add(ASSIGNORDEASSIGNADDRESSES_CONTENT_KEY_FORCEWITHDELETION, forceWithDeletion);

		method.invoke();
		OctopusResult result = method.getORes();

		List addAddressesBlocked = (List) result
				.getData(ASSIGNORDEASSIGNADDRESSES_NOTASSIGNABLEADDRESSES);
		List removeAddressesBlocked = (List) result
				.getData(ASSIGNORDEASSIGNADDRESSES_NOTDEASSIGNABLEADDRESSES);
		List implicitlyDeletedAddresses = (List) result
				.getData(ASSIGNORDEASSIGNADDRESSES_IMPLICITLYDELTEDADDRESSES);
		HashMap resultMap = new HashMap();
		
		resultMap.put(ASSIGNORDEASSIGNADDRESSES_NOTASSIGNABLEADDRESSES, addAddressesBlocked);
		resultMap.put(ASSIGNORDEASSIGNADDRESSES_NOTDEASSIGNABLEADDRESSES, removeAddressesBlocked);
		resultMap.put(ASSIGNORDEASSIGNADDRESSES_IMPLICITLYDELTEDADDRESSES, implicitlyDeletedAddresses);
		return resultMap;
	}

}
