/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetComboBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class HerrFrauField extends ContactAddressTextField
{
  private TarentWidgetTextField   m_oTextfield_herrfrau;      
  private TarentWidgetComboBox    m_oCombobox_anredetext;
  private TarentWidgetLabel m_oLabel_herrfrau;
  private TarentWidgetLabel m_oLabel_anrede;  
  
  private String m_sOld_herrfrau_text;
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createHerrFrauPanel(widgetFlags);
    return panel;
  }
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public String getKey()
  {
    return "HERRFRAU";
  }

  public void postFinalRealize()
  {    
  }
  
  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      m_oTextfield_herrfrau.setEditable(true);
      m_oCombobox_anredetext.setEnabled(true);
    }
    else
    {  
      m_oTextfield_herrfrau.setEditable(false);
      m_oTextfield_herrfrau.setBackground(Color.WHITE);
      m_oCombobox_anredetext.setEnabled(false);
    }
  }
 
  public void setData(PluginData data)
  {
    Object value = data.get(AddressKeys.HERRFRAU);
    if (value == null) m_oTextfield_herrfrau.setText("");
    else m_oTextfield_herrfrau.setText(value.toString());
    
    Object anrede = data.get(AddressKeys.ANREDE);
    if (anrede != null) m_oCombobox_anredetext.setSelectedIndex(getGUIListener().getIndexOfAnrede(anrede.toString()));
    else m_oCombobox_anredetext.setSelectedIndex(0);
  }

  public void getData(PluginData data)
  {
    data.set(AddressKeys.HERRFRAU, m_oTextfield_herrfrau.getText());
    data.set(AddressKeys.ANREDE, (m_oCombobox_anredetext.getSelectedItem() != null) ? m_oCombobox_anredetext.getSelectedItem().toString() : "");
  }

  public boolean isDirty(PluginData data)
  {
    if (!(m_oTextfield_herrfrau.getText().equals("")) && data.get(AddressKeys.HERRFRAU) != null ) //$NON-NLS-1$
      if (!(m_oTextfield_herrfrau.getText().equalsIgnoreCase(data.get(AddressKeys.HERRFRAU).toString()))) return(true);            

    if (!(m_oCombobox_anredetext.getSelectedItem().equals("")) && (data.get(AddressKeys.ANREDE) != null)) //$NON-NLS-1$
      if (!(m_oCombobox_anredetext.getSelectedItem().equals(data.get(AddressKeys.ANREDE)))) {
    	  return(true);
      }
      
    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  public String getFieldName()
  {
    return fieldName;     
  }
  
  public String getFieldDescription()
  {
    return fieldDescription;        
  }
  
  
  private String getFieldNameAnrede()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_MainFrameNewStyle_Standard_Anrede");
    else return text;
  }
  
  private String getFieldNameHerrFrau()
  {
    String text = null;
    if (getFieldName() != null)
    {
      String[] names = getFieldName().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_MainFrameNewStyle_Standard_Herr_Frau");
    else return text;
  }
  

  
  private String getFieldDescriptionAnrede()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[1];  
      }      
    }
    if (text == null) return Messages.getString("GUI_MainFrameNewStyle_Standard_Anrede_ToolTip");
    else return text;
  }
  
  private String getFieldDescriptionHerrFrau()
  {
    String text = null;
    if (getFieldDescription() != null)
    {
      String[] names = getFieldDescription().split(";");
      if (names.length == 2)
      {
        text = names[0];  
      }      
    }
    if (text == null) return Messages.getString("GUI_MainFrameNewStyle_Standard_Herr_Frau_ToolTip");
    else return text;
  }
  
  
  
  
  public void setFieldName(String name)
  {
    fieldName = name;
    if (panel != null)
    {
      if (name != null) 
      {
        m_oLabel_anrede.setText(getFieldNameAnrede());
        m_oLabel_herrfrau.setText(getFieldNameHerrFrau());
      }
    }
  }  
  

  public void setFieldDescription(String description)
  {
    fieldDescription = description;
    if (panel != null)
    {
      if (description != null) 
      {
        m_oTextfield_herrfrau.setToolTipText(getFieldDescriptionHerrFrau());
        m_oCombobox_anredetext.setToolTipText(getFieldDescriptionAnrede());
      }
    }
  }  
  

  
  private EntryLayoutHelper createHerrFrauPanel(String widgetFlags)
  {       
    m_oTextfield_herrfrau = createTextField(getFieldDescriptionHerrFrau(), 20);
    m_oTextfield_herrfrau.getDocument().addDocumentListener(new textfield_herrfrau_changed_text());    
    m_oTextfield_herrfrau.addFocusListener(new textfield_herrfrau_focus());
    m_oLabel_anrede = new TarentWidgetLabel(getFieldNameAnrede()); //$NON-NLS-1$
    m_oCombobox_anredetext = new TarentWidgetComboBox(getGUIListener().getAnredetexteList().toArray());
    m_oCombobox_anredetext.setSelectedIndex(0);
    m_oCombobox_anredetext.setToolTipText(getFieldDescriptionAnrede()); //$NON-NLS-1$
    
    m_oLabel_herrfrau =  new TarentWidgetLabel(getFieldNameHerrFrau());
    
    return new EntryLayoutHelper(
                                   new TarentWidgetInterface[] {
                                                                m_oLabel_herrfrau,
                                                                m_oTextfield_herrfrau,
                                                                m_oLabel_anrede,
                                                                m_oCombobox_anredetext
                                   },
                                   widgetFlags);
  }


  public class textfield_herrfrau_focus extends FocusAdapter
  {
    public void focusGained(FocusEvent e)
    { 
    }

    public void focusLost(FocusEvent e)
    { 
      expandHerrFrauText();
    }
  }

  private void expandHerrFrauText()
  {
    String text;
    text = m_oTextfield_herrfrau.getText();
    
    if (text.length() == 1)
    {
      switch(text.toLowerCase().charAt(0)) 
      {
      case 'h':
        m_oTextfield_herrfrau.setText(Messages.getString("GUI_MainFrameNewStyle_Standard_Herr_Frau_Inhalt_Herr")); //$NON-NLS-1$
        break;
      case 'f':
        m_oTextfield_herrfrau.setText(Messages.getString("GUI_MainFrameNewStyle_Standard_Herr_Frau_Inhalt_")); //$NON-NLS-1$
        break;
      case 'd':
        m_oTextfield_herrfrau.setText(Messages.getString("GUI_MainFrameNewStyle_Standard_Herr_Frau_Inhalt_Damen_und_Herren")); //$NON-NLS-1$
        break;
      }
    }
  }

  
  public class textfield_herrfrau_changed_text implements DocumentListener
  {
    public void removeUpdate(DocumentEvent e)      
    {
    }
    
    public void changedUpdate(DocumentEvent e)      
    {
    }      
    
    public void insertUpdate(DocumentEvent e)      
    {
      String text;
      text = m_oTextfield_herrfrau.getText();
      
      if (text.length() == 1)
      {
        if (!(text.equals(m_sOld_herrfrau_text)))
        {
          switch(text.toLowerCase().charAt(0)) 
          {
          case 'h':
            m_oCombobox_anredetext.setSelectedIndex(getGUIListener().getAnredetexteList().indexOf(Messages.getString("GUI_MainFrameNewStyle_Standard_Anrede_Inhalt_Sehr_geehrter"))); //$NON-NLS-1$
            break;
          case 'f':
            m_oCombobox_anredetext.setSelectedIndex(getGUIListener().getAnredetexteList().indexOf(Messages.getString("GUI_MainFrameNewStyle_Standard_Anrede_Inhalt_Sehr_geehrte"))); //$NON-NLS-1$
            break;
          case 'd':
            m_oCombobox_anredetext.setSelectedIndex(getGUIListener().getAnredetexteList().indexOf(Messages.getString("GUI_MainFrameNewStyle_Standard_Anrede_Inhalt_Sehr_geehrte_mehrzahl"))); //$NON-NLS-1$
            break;
          }
          m_sOld_herrfrau_text = text;
        }
      }
    } 
  }

public void setDoubleCheckSensitive(boolean issensitive) {
	setDoubleCheckSensitive(m_oTextfield_herrfrau, issensitive);	
}

}
