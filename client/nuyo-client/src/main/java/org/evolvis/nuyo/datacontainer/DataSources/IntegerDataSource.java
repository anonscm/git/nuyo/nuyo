/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataSources;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSourceAdapter;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IntegerDataSource extends DataSourceAdapter
{
  private int m_iMinValue;
  private boolean m_bUseMin = false;
  private int m_iMaxValue;
  private boolean m_bUseMax = false;
  
  public String getListenerName()
  {
    return "IntegerDataSource";
  }  

  public DataSource cloneDataSource()
  {
    IntegerDataSource datasource = new IntegerDataSource();
    
    datasource.m_oDataContainer = m_oDataContainer;
    datasource.m_sDBKey = m_sDBKey ;

    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      datasource.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    
    return datasource;
  }
  
  public boolean canHandle(Class data)
  {
    return data.isInstance(Integer.class);
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.DATASOURCE, "IntegerDataSource", new DataContainerVersion(0));
    d.addParameterDescription(new ParameterDescription("maxval", "maximaler Wert", "der maximale Wert der gespeichert werden kann."));
    d.addParameterDescription(new ParameterDescription("minval", "minimaler Wert", "der minimale Wert der gespeichert werden kann."));
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("maxval".equalsIgnoreCase(key)) 
    {
      try
      {
        m_iMaxValue = Integer.parseInt(value);
        m_bUseMax = true;
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        nfe.printStackTrace();
      }
    }
    else if ("minval".equalsIgnoreCase(key)) 
    {
      try
      {
        m_iMinValue = Integer.parseInt(value);
        m_bUseMin = true;
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        nfe.printStackTrace();
      }
    }
    return false;
  }

  public boolean readData(PluginData plugindata)
  {
    Object newdata = plugindata.get(getDBKey());    
    getDataContainer().setGUIData(newdata);
    return (newdata != null);
  }
    
  public boolean writeData(PluginData plugindata)
  {
    Object data = getDataContainer().getDataStorage().getData();    
    if (data instanceof Integer)
    {
      Integer val = (Integer)data;    
      if (m_bUseMin)
      {
        if (val.intValue() < m_iMinValue) val = new Integer(m_iMinValue);
      }
      if (m_bUseMax)
      {
        if (val.intValue() > m_iMaxValue) val = new Integer(m_iMaxValue);
      }
      return plugindata.set(getDBKey(), val);
    }    
    return false;
  }

}
