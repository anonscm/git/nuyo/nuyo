/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import javax.swing.AbstractListModel;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityList;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MutableEntityListModel extends AbstractListModel
{
	EntityList data;

	public MutableEntityListModel()
	{
		this(null);
	}

	public MutableEntityListModel(EntityList pData)
	{
		data = pData;
	}

	public void setData(EntityList pData)
	{
		data = pData;
		fireContentsChanged(this, 0, getSize());
	}

	public EntityList getData()
	{
		return data;
	}

	public void add(Entity pEntity)
	{
		if(data != null)
		{	
			data.addEntity(pEntity);
			// TODO proof this
			fireContentsChanged(this, getSize()-2, getSize()-1);
		}
	}

	public void remove(Entity pEntity)
	{
		if(data != null)
		{
			int index = getData().indexOf(pEntity);
			
			if(index == -1) return;
			
			data.removeEntity(pEntity);
			fireIntervalRemoved(this, index, index);
		}
		
	}

	public Object getElementAt(int index)
	{
		if(data != null) return data.getEntityAt(index);
		return null;
	}

	public int getSize()
	{
		if(data != null)
		{
			if(data.getSize() > 0) return data.getSize();
		}
		return 0;
	}
}
