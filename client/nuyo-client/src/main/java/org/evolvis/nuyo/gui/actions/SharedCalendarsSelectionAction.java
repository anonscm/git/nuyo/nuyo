package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;


public class SharedCalendarsSelectionAction extends AbstractGUIAction {

	private static final long	serialVersionUID	= 8122860390409416450L;
	private static final TarentLogger logger = new TarentLogger(SharedCalendarsSelectionAction.class);
    private JDialog dialog;
    private Runnable executor;

	public void actionPerformed(ActionEvent e) {
        if(dialog != null && !dialog.isVisible()) {
            SwingUtilities.invokeLater(executor);
        }
	}
	
    public void init(){
        final Plugin calendarPlugin = PluginRegistry.getInstance().getPlugin(CalendarPlugin.ID);
        if(calendarPlugin != null) {
           if(calendarPlugin.isTypeSupported(TarentCalendar.class)){
               final TarentCalendar calendar = (TarentCalendar) calendarPlugin.getImplementationFor(TarentCalendar.class);
               dialog = calendar.getCalendarSelectionDialog();
               createRunnable();
           } else logger.warning("Couldn't init " + getClass().getName(), "Tarent Calendar interface is not supported by Calender Plugin.");
        } else logger.warning("Couldn't init " + getClass().getName(), "Calender Plugin not registered.");
    }

    private void createRunnable() {
        executor = new Runnable() {
            public void run() {
                dialog.setVisible( true );
            }
        };
    }
}
