/*
 * Created on 14.04.2005
 *
 */
package org.evolvis.nuyo.db;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.evolvis.nuyo.db.octopus.AppointmentImpl;
import org.evolvis.nuyo.logging.TarentLogger;



/**
 * @author simon
 *
 * Diese Klasse repr�sentiert die aktiven Kalender eines Benutzers.
 * Sie ist haupts�chlich eine Wrapperklasse die eine Collection von
 * Calendar Objekten verwaltet und den additiven Zugriff auf mehrere
 * Kalender erlaubt. 
 * 
 * Anwendungsbereich: Additives Ausw�hlen der Kalender im Menu
 * 
 */
public class CalendarCollection {
    
    private static final TarentLogger logger = new TarentLogger(CalendarCollection.class);
    private List cachedCalendars;
    private Calendar startUpCalendar;

    protected CalendarCollection(){
        cachedCalendars = new Vector();
    }

    /** Loads only the personal calendar of a given user. */
    public static CalendarCollection empty(User user) throws ContactDBException {
        CalendarCollection collection = new CalendarCollection(); 
        collection.startUpCalendar = user.getCalendar(true);
        return collection;
    }
    
    /** Loads only the personal calendar of a given user. */
    public static CalendarCollection loadPrivateCalendarOnly(User user) throws ContactDBException {
        CalendarCollection collection = new CalendarCollection(); 
        collection.startUpCalendar = user.getCalendar(true);
        collection.cachedCalendars.add(collection.startUpCalendar);
        return collection;
    }
    
    /** Loads only the personal calendar of a given user. */
    public static CalendarCollection loadAssignedCalendars(User user) throws ContactDBException {
        CalendarCollection collection = new CalendarCollection(); 
        collection.startUpCalendar = user.getCalendar(true);
        collection.cachedCalendars.addAll(user.getCalendars());
        return collection;
    }
    
    public Collection getCalendarSecretaries() throws ContactDBException {
        Vector allSecretaries = new Vector();
        for (Iterator iter = cachedCalendars.iterator(); iter.hasNext();){
            allSecretaries.addAll(((Calendar) iter.next()).getCalendarSecretaries());
        }
        return allSecretaries;
    }

    public Collection getAppointments(Date start, Date end, Collection filters) throws ContactDBException {
        Vector allAppointments = new Vector();
        synchronized ( cachedCalendars ) {//block collection object while computing
            //now safe computing - no changes will occur!
            for (Iterator iter = cachedCalendars.iterator(); iter.hasNext();){
                Calendar acak = null;
                acak = (Calendar) iter.next();
                Collection colApps = null;
                
                colApps = acak.getAppointments(start, end, filters);
                        
                for (Iterator appiter = colApps.iterator(); appiter.hasNext();){
                    Appointment app = (Appointment) appiter.next();
                    if (! allAppointments.contains(app)) 
                        // Only add Appointment to Calendarview once
                        allAppointments.add(app);
                }
            }
        }
        return allAppointments;
    }
    
    public Appointment getAppointment(final Integer appntID) throws ContactDBException {
        for (Iterator iter = cachedCalendars.iterator(); iter.hasNext();){
            final Calendar acak = (Calendar) iter.next();
            final Appointment app = acak.getAppointment(appntID);
            if ( app != null)
                    return app;
        }
        return null;
    }
    
    

    public Collection getReminders(Date start, Date end, Collection filters) throws ContactDBException {
        Vector allReminders = new Vector();
        for (Iterator iter = cachedCalendars.iterator(); iter.hasNext();){
            allReminders.addAll(((Calendar) iter.next()).getReminders(start, end, filters));
        }
        return allReminders;
    }
    
    public void add(Calendar calendar){
        if(startUpCalendar == null) startUpCalendar = calendar;
        if ( cachedCalendars.contains(calendar)){
            //System.out.println("DUPLICATE CALENDAR N O T ADDED!!!");
        }else{
            cachedCalendars.add(calendar);
            AppointmentImpl.flushCache();
        }    
    }

    public void remove(Calendar calendar){
       // if ( cachedCalendars.size() > 1 && cachedCalendars.contains(calendar))
            cachedCalendars.remove(calendar);
            AppointmentImpl.flushCache();
    }
    
    public Calendar getCalendar(int calendarID){
        for (Iterator iter = cachedCalendars.iterator(); iter.hasNext();){
            Calendar tmpCal = (Calendar) iter.next();
            if (tmpCal.getId() == calendarID)
                return tmpCal;
        }
        return null;   
    }
    
    public Collection getCollection(){
        Vector tmpVector = new Vector();
        tmpVector.addAll(cachedCalendars);
        return tmpVector;
    }
    
    public Calendar getUserCalendar(){
        return startUpCalendar;
    }
}