/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Actions;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListener;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerAction;
import org.evolvis.nuyo.datacontainer.Listener.DataContainerListenerActionAdapter;

/**
 * @author niko
 *
 */
public class RefreshAction extends DataContainerListenerActionAdapter
{
  public DataContainerListenerAction cloneAction()
  {
    return new RefreshAction();
  }

  public boolean doAction()
  {
    DataContainerListener listener = getListener();
    if (listener != null)
    {
      listener.getDataContainer().fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FETCH, getDataContainer(), null));      
      return true;
    }
    return false;
  }

  public void init()
  {
  }

  public void dispose()
  {
  }

  public boolean addParameter(String key, String value)
  {
    return false;
  }

  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.ACTION, "RefreshAction", new DataContainerVersion(0));
    return d;
  }
}
