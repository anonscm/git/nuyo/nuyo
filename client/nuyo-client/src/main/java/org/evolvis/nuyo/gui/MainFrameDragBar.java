/*
 * Created on 08.11.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MouseInputAdapter;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.BumpPanel;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Appearance.Key;
import org.evolvis.xana.utils.IconFactory;



/**
 * Manages the bottom dragbar panel of the application window.<br>
 * For each 'Tab' panel exists a table with assigned data to it.<br>
 *   
 * @see org.evolvis.nuyo.gui.MainFrameExtStyle
 * @see org.evolvis.nuyo.gui.MainFrameExtStyle.OverlaySizeListener

 * @author niko
 * 
 * TODO rewrite this class
 * 
 * <p>refactored by: 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 * 
 */
public class MainFrameDragBar {
    
    private static final TarentLogger logger = new TarentLogger(MainFrameDragBar.class);

    private MainFrameExtStyle mainFrame;
    private GUIListener guiListener;
    private Preferences panelsPreferences;

    // die hochziehbare Tabelle
    private JPanel splitPane;
    private JPanel embeddedTablePanel;
    private int splitPosition;
    private int loadedSplitPosition;
    private int oldSplitPosition;
    private JPanel tableDragBarPanel;
    private JLabel m_oDragBarLabel;
    private JLabel m_oDragBarStaticLabel;
    private JLabel m_oDragBarFilterLabel;

    private JLabel m_oDragBarPreviewBusyLabel;
    private JLabel m_oDragBarPreviewPercentLabel;

    //default position will be loaded (see Config.SHOW_DEFAULT_ADDRESS_TABLE_SIZE)
    private double defaultTablePosition = 0.2;  

    private Map m_oRegisteredTableComponents;
    private Map m_oRegisteredTableTexts;
    private Map m_oRegisteredTableStaticTexts;
    private Map m_oRegisteredTableFilterTexts;

    private boolean tableWasMoved;
    private boolean isToUseLoadedPosition;

    private JComponent currentTableComponent;
    
    private ImageIcon m_oDragBarBumpsIcon;
    private ImageIcon m_oArrowUpIcon;
    private ImageIcon m_oArrowDownIcon;

    private ImageIcon m_oIconPreviewInactive;
    private ImageIcon m_oIconPreviewActive;


    public MainFrameDragBar(GUIListener oGUIListener, MainFrameExtStyle oMainFrame) {
        guiListener = oGUIListener;
        mainFrame = oMainFrame;
        panelsPreferences = guiListener.getPreferences(StartUpConstants.PREF_PANELS_NODE_NAME);
        init();
    }


    public boolean getTableWasMoved() {
        return tableWasMoved;
    }


    public void setTableWasMoved(boolean wasmoved) {
        tableWasMoved = wasmoved;
    }


    public JPanel getTableDragBar() {
        return tableDragBarPanel;
    }


    public JPanel getSplitPane() {
        return splitPane;
    }

    /** This method will be invoked after application window has been initialized.*/
    public void realized() {
        logger.fine("[frame is initialized] realizing table...");
        setDefaultDragBarPosition();
        setSplitPanel();
    }


    public void create() {
        tableDragBarPanel = createTableDragBar();
        SplitMouseListener splitmouselistener = new SplitMouseListener();
        tableDragBarPanel.addMouseListener(splitmouselistener);
        tableDragBarPanel.addMouseMotionListener(splitmouselistener);

        embeddedTablePanel = new JPanel();
        embeddedTablePanel.setLayout(new BorderLayout());

        splitPane = new JPanel();
        splitPane.setLayout(new BorderLayout());
        splitPane.add(embeddedTablePanel, BorderLayout.CENTER); //$NON-NLS-1$
        splitPane.add(tableDragBarPanel, BorderLayout.NORTH); //$NON-NLS-1$

        embeddedTablePanel.setMinimumSize(new Dimension(0, 0));
        embeddedTablePanel.addComponentListener(new TableSizeListener());
    }


    public void init() {
        IconFactory factory = ApplicationServices.getInstance().getIconFactory();
        m_oDragBarBumpsIcon = (ImageIcon)factory.getIcon("dragbar.gif");
        m_oArrowUpIcon = (ImageIcon)factory.getIcon("arrow_up.gif");
        m_oArrowDownIcon = (ImageIcon)factory.getIcon("arrow_down.gif");
        m_oIconPreviewInactive = (ImageIcon)factory.getIcon("busyempty.gif");

        m_oRegisteredTableComponents = new HashMap();
        m_oRegisteredTableTexts = new HashMap();
        m_oRegisteredTableStaticTexts = new HashMap();
        m_oRegisteredTableFilterTexts = new HashMap();

        //loadDefaultPosition();
        //loadLastPosition();
    }


    private void loadLastPosition() {
        try{
            loadedSplitPosition = panelsPreferences.getInt(StartUpConstants.PREF_DRAG_BAR_POSITION_KEY, getDefaultPosition());
            defaultTablePosition = loadedSplitPosition; 
            splitPosition = loadedSplitPosition;
            oldSplitPosition = loadedSplitPosition;
            setSplitPanel();
            isToUseLoadedPosition = true;
        }catch (Exception e) {
            logger.warningSilent("[!] no " + StartUpConstants.PREF_DRAG_BAR_POSITION_KEY + " value found");
        }
    }


    private void loadDefaultPosition() {
        String minDragBarPosition = ConfigManager.getAppearance().get(Key.SHOW_DEFAULT_ADDRESS_TABLE_SIZE);
        if (minDragBarPosition != null) {
            try {
                double d = Double.parseDouble(minDragBarPosition);
                if ((d >= 0.0) && (d <= 1.0)) {
                    defaultTablePosition = d;
                } else {
                    logger.warningSilent("[!] Invalid value of " + Key.SHOW_DEFAULT_ADDRESS_TABLE_SIZE + ": " + minDragBarPosition + ".\n Valid range: 0.0 (not visible) -> 1.0 (full visible)");
                }
            } catch (NumberFormatException nfe) {
                logger.config("Not a floating-point-number in value for \"" + Key.SHOW_DEFAULT_ADDRESS_TABLE_SIZE + "\" valid range: 0.0 (not visible) -> 1.0 (full visible)");
            }
        }//already initilized
    }


    private void setDefaultDragBarPosition() {
        if(isToUseLoadedPosition) {
            splitPosition = loadedSplitPosition;
            logger.fine("[drag-bar] set last used position");
        } else {
            splitPosition = getDefaultPosition();
        }
        oldSplitPosition = splitPosition;
        logger.info("[drag-bar] default position: " + splitPosition);
    }


    private int getDefaultPosition() {
        return (int) (((double) getBottomPosition()) * (1.0 - defaultTablePosition)); // ist "falsch" rum ;-)
    }


    /** 
     * Will be invoked by {@link org.evolvis.nuyo.gui.OverlaySizeListener} after the JPanel has been resized.
     */
    public void resize() {
      logger.fine("[table] resizing...");
      setSplitPanel();
    }


    public void showTable(boolean show) {
        if (show) {
            setTopPosition();
        } else {
            setBottomPosition();
        }
        // sync with menu-actions
        /* AbstractGUIAction action = ActionRegistry.getInstance().getAction("toggle.table");
        if(action != null)
        	action.setSelected(show); */
    }


    public boolean isTableVisible() {
        int maxsize = getBottomPosition();
        return ((maxsize - splitPosition) > 10);
    }


    private void setSplitPanel() {
        ensureDragBarConstraints();

        Dimension panelSize = mainFrame.getOverlayPanel().getSize();
        Dimension splitSize = new Dimension(panelSize.width, (panelSize.height) - splitPosition);

        splitPane.setMinimumSize(splitSize);
        splitPane.setMaximumSize(splitSize);
        splitPane.setPreferredSize(splitSize);
        splitPane.setSize(splitSize);
        splitPane.setLocation(0, splitPosition);
        splitPane.validate();
        
        panelsPreferences.putInt(StartUpConstants.PREF_DRAG_BAR_POSITION_KEY, splitPosition);
        tableWasMoved = true;
        logger.fine("[table] resized (" + String.valueOf(splitPosition) + ") -> synchronizing toggle check boxes...");
        ApplicationServices.getInstance().getApplicationModel().setTableVisible(Boolean.valueOf(isTableVisible()));
    }


    private int getBottomPosition() {
        return mainFrame.getOverlayPanel().getHeight() - tableDragBarPanel.getHeight();
    }

    private int getTopPosition() {
        return 0;
    }

    private void ensureDragBarConstraints() {
        if (splitPosition <= 0){//over maximum
            logger.fine("[!] invalid table position: "+String.valueOf(splitPosition));
            splitPosition = getTopPosition();//top position
        } else if(splitPosition > getBottomPosition()){//under minimum
            splitPosition = getBottomPosition();
        }
    }

    private class SplitMouseListener extends MouseInputAdapter {

        private int m_iOffsetY = 0;


        public void mouseDragged(MouseEvent e) {            
            splitPosition += ((e.getY()) - (m_iOffsetY));
            logger.fine("[table:dragged]: to " + splitPosition);
            setSplitPanel();
        }


        public void mousePressed(MouseEvent e) {
            m_iOffsetY = e.getY();
            oldSplitPosition = splitPosition;
        }


        public void mouseReleased(MouseEvent e) {
            mainFrame.selectAddress(mainFrame.getCurrentIndex() - 1);
        }
    }


    public void setTableView(String name) {
        JComponent newTableComponent = (JComponent) (m_oRegisteredTableComponents.get(name));
        if (newTableComponent != null) {
            if (newTableComponent != currentTableComponent) {
                if (currentTableComponent != null) {
                    embeddedTablePanel.remove(currentTableComponent);
                }
                embeddedTablePanel.add(newTableComponent, BorderLayout.CENTER);
                setTableTextInternal(name);
                currentTableComponent = newTableComponent;
            }
        } else
            logger.config("Can't fill dragbar table with empty element: " + name + "");
    }


    public void registerTableComponent(String name, JComponent component) {
        m_oRegisteredTableComponents.put(name, component);
    }


    public void setTableText(String name, String text) {
        m_oRegisteredTableTexts.put(name, text);

        JComponent component = (JComponent) (m_oRegisteredTableComponents.get(name));
        if (component != null) {
            if (component == currentTableComponent) {
                setTableTextInternal(name);
            }
        }
    }


    public void setTableStaticText(String name, String text) {
        m_oRegisteredTableStaticTexts.put(name, text);

        JComponent component = (JComponent) (m_oRegisteredTableComponents.get(name));
        if (component != null) {
            if (component == currentTableComponent) {
                setTableTextInternal(name);
            }
        }
    }


    public void setTableFilterText(String name, String text) {
        m_oRegisteredTableFilterTexts.put(name, text);

        JComponent component = (JComponent) (m_oRegisteredTableComponents.get(name));
        if (component != null) {
            if (component == currentTableComponent) {
                setTableTextInternal(name);
            }
        }
    }

    // Hier scheint es ein bekanntest Problem zu geben:
    // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4851443
    // Die Animation stoppt einfach...

    private boolean m_bIsBusy = false;


    public void setPreviewBusy(boolean isbusy) {
        if (m_bIsBusy != isbusy) {
            ImageIcon icon = isbusy ? m_oIconPreviewActive : m_oIconPreviewInactive;
            m_oDragBarPreviewBusyLabel.setIcon(icon);
            // icon.getImage().flush();
            m_bIsBusy = isbusy;
        }
    }


    public void setPreviewPercent(int percent) {
        // m_oDragBarPreviewPercentLabel.setText(percent + "%");
    }

    /**
     * Retrieves the table texts stored for the given
     * table view name and sets them on the respective
     * labels.
     * 
     * <p>This method is called after the table view
     * has changed to update the labels.</p>
     * 
     * <p>In case that a certain text is not defined for
     * the given table view name the labels are set to
     * the empty string.</p>
     *  
     * @param name
     */
    private void setTableTextInternal(String name) {
        String text = asString(m_oRegisteredTableTexts.get(name));
        if (text != null)
            m_oDragBarLabel.setText(text);
        else
          m_oDragBarLabel.setText("");

        String filtertext = asString(m_oRegisteredTableFilterTexts.get(name));
        if (filtertext != null)
            m_oDragBarFilterLabel.setText(filtertext);
        else
            m_oDragBarFilterLabel.setText("");
    }


    public JPanel createTableDragBar() {
        JPanel tmptabledragbar = new BumpPanel(m_oDragBarBumpsIcon);
        tmptabledragbar.setToolTipText(Messages.getString("GUI_MainFrameNewStyle_Tabelle_ToolTip"));
        tmptabledragbar.setLayout(new BorderLayout());

        JPanel tabledragbar = new JPanel();
        tabledragbar.setLayout(new BorderLayout());
        tabledragbar.setBorder(new BevelBorder(BevelBorder.RAISED));
        tmptabledragbar.add(tabledragbar, BorderLayout.CENTER); //$NON-NLS-1$

        JPanel staticandfilterpanel = new JPanel();
        staticandfilterpanel.setLayout(new BorderLayout());

        m_oDragBarStaticLabel = new JLabel(""); //$NON-NLS-1$ //$NON-NLS-2$
        staticandfilterpanel.add(m_oDragBarStaticLabel, BorderLayout.WEST);

        m_oDragBarFilterLabel = new JLabel(""); //$NON-NLS-1$ //$NON-NLS-2$
        staticandfilterpanel.add(m_oDragBarFilterLabel, BorderLayout.EAST);

        // ----------------------------------------
        m_oDragBarPreviewBusyLabel = new JLabel(m_oIconPreviewInactive); //$NON-NLS-1$ //$NON-NLS-2$
        m_oDragBarPreviewBusyLabel.setBorder(new EmptyBorder(0, 3, 0, 3));

        m_oDragBarPreviewPercentLabel = new JLabel(""); //$NON-NLS-1$ //$NON-NLS-2$
        m_oDragBarPreviewPercentLabel.setBorder(new EmptyBorder(0, 0, 0, 3));

        JPanel previewpanel = new JPanel();
        previewpanel.setLayout(new BorderLayout());
        previewpanel.add(m_oDragBarPreviewBusyLabel, BorderLayout.WEST);
        previewpanel.add(m_oDragBarPreviewPercentLabel, BorderLayout.EAST);
        // ----------------------------------------

        JPanel tmptextpanel = new JPanel();
        tmptextpanel.setLayout(new BorderLayout());
        tmptextpanel.setOpaque(false);

        tmptextpanel.add(previewpanel, BorderLayout.WEST); //$NON-NLS-1$
        tmptextpanel.add(staticandfilterpanel, BorderLayout.CENTER); //$NON-NLS-1$

        tabledragbar.add(tmptextpanel, BorderLayout.CENTER); //$NON-NLS-1$

        m_oDragBarLabel = new JLabel();
        tabledragbar.add(m_oDragBarLabel, BorderLayout.EAST); //$NON-NLS-1$

        JPanel arrowpanel = new JPanel();
        arrowpanel.setLayout(new BorderLayout());

        JButton button_arrow_up = new JButton(m_oArrowUpIcon);
        button_arrow_up.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                //showTable(true);
            	if(!MainFrameDragBar.this.isTableVisible())
            	{
	            	Action action = ActionRegistry.getInstance().getAction("toggle.table");
	            	if(action != null)
	            		action.actionPerformed(e);
            	}
            }
        });
        button_arrow_up.setBorderPainted(false);
        button_arrow_up.setMargin(new Insets(0, 0, 0, 0));
        button_arrow_up.setFocusPainted(false);
        button_arrow_up.setToolTipText(Messages.getString("GUI_MainFrameNewStyle_Tabelle_Button_Up_ToolTip"));

        JButton button_arrow_down = new JButton(m_oArrowDownIcon);
        button_arrow_down.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                //showTable(false);
            	if(MainFrameDragBar.this.isTableVisible())
            	{
	            	Action action = ActionRegistry.getInstance().getAction("toggle.table");
	            	if(action != null)
	            		action.actionPerformed(e);
            	}
            }
        });
        button_arrow_down.setBorderPainted(false);
        button_arrow_down.setMargin(new Insets(0, 0, 0, 0));
        button_arrow_down.setFocusPainted(false);
        button_arrow_down.setToolTipText(Messages.getString("GUI_MainFrameNewStyle_Tabelle_Button_Down_ToolTip"));

        arrowpanel.add(button_arrow_up, BorderLayout.WEST); //$NON-NLS-1$
        arrowpanel.add(button_arrow_down, BorderLayout.EAST); //$NON-NLS-1$

        tabledragbar.add(arrowpanel, BorderLayout.WEST); //$NON-NLS-1$

        return (tmptabledragbar);
    }

    /* Moves to the last top-postion.*/
    private void setTopPosition() {
        logger.fine("[table] set top position");
        if(splitPosition == getBottomPosition()){
            splitPosition = oldSplitPosition;
            oldSplitPosition = getBottomPosition();
            setSplitPanel();
        }
    }

    /* Saves the last top-position and moves to the bottom.*/
    private void setBottomPosition() {
        logger.fine("[table] set bottom position");
        if(splitPosition != getBottomPosition()){
            oldSplitPosition = splitPosition;
            splitPosition = getBottomPosition();
            setSplitPanel();
        }
    }

    public class TableSizeListener extends ComponentAdapter {

        public void componentResized(ComponentEvent ce) {
            // FIXME: Remove if possible.

            // Dies ist der verzweifelte Versuch den Repaint Bug
            // der Tabelle loszuwerden... Grund liegt in einem
            // Bug innerhalb von Swing - dies ist nur ein Workaround!
            if (m_bMustRepaintOverlay) {
                mainFrame.getOverlayPanel().repaint();
                m_bMustRepaintOverlay = false;
            }
            
        }
    }


    private final static String asString(Object o) {
        return o == null ? null : o.toString();
    }

    private boolean m_bMustRepaintOverlay = false;

}
