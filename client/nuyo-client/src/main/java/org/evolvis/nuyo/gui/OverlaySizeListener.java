package org.evolvis.nuyo.gui;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Will be used by {@link org.evolvis.nuyo.gui.MainFrameExtStyle} to manage all table panels: one for each TAB.
 * 
 * @see org.evolvis.nuyo.gui.MainFrameExtStyle#addComponentToResize(JComponent)   
 * @see org.evolvis.nuyo.gui.MainFrameExtStyle#removeComponentToResize(JComponent)   
 */
public class OverlaySizeListener extends ComponentAdapter {
    
    private MainFrameDragBar dragBar;
    private List listOfPanelsToResize;
    private JPanel overlayPanel;
    
    /**
     * Used in {@link org.evolvis.nuyo.gui.MainFrameExtStyle#createLayeredCenterPane()}.
     * 
     * @param anOverlayPanel with JLayeredPane in the center
     * @param aDragBar to resize
     * @throws ContactGuiException
     */
    public OverlaySizeListener(JPanel anOverlayPanel, MainFrameDragBar aDragBar) {
        overlayPanel = anOverlayPanel;
        dragBar = aDragBar;
        listOfPanelsToResize = new ArrayList();
    }

    public void addComponentToResize(JComponent comp) {
        listOfPanelsToResize.add(comp);
    }


    public void removeComponentToResize(JComponent comp) {
        listOfPanelsToResize.remove(comp);
    }

    public void componentResized(ComponentEvent ce) {
        Dimension panelsize = overlayPanel.getSize();

        Iterator comit = listOfPanelsToResize.iterator();
        while (comit.hasNext()) {
            Object obj = comit.next();
            if (obj instanceof JComponent) {
                JComponent comp = ((JComponent) obj);
                comp.setMinimumSize(panelsize);
                comp.setMaximumSize(panelsize);
                comp.setPreferredSize(panelsize);
                comp.setSize(panelsize);
            }
        }

        overlayPanel.revalidate();
        dragBar.resize();
    }
}