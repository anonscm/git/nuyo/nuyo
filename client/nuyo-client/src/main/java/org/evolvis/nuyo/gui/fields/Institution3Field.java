/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextAreaField;


/**
 * @author niko
 *
 */
public class Institution3Field extends GenericTextAreaField
{
  public Institution3Field()
  {
    super("INSTITUTION3", AddressKeys.INSTITUTION3, CONTEXT_ADRESS, "GUI_Fields_Institution3_ToolTip", "GUI_Fields_Institution3", 60);
  }
}
