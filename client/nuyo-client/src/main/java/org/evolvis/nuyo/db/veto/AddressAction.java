/* $Id: AddressAction.java,v 1.4 2006/12/12 09:31:07 robert Exp $
 * Created on 29.10.2003
 */
package org.evolvis.nuyo.db.veto;

import org.evolvis.nuyo.db.Address;

/**
 * Diese Klasse stellt eine Address-Aktion dar, die durch ein
 * {@link org.evolvis.nuyo.db.veto.Veto} untersagt werden kann.
 *  
 * @author mikel
 */
public class AddressAction extends VetoableAction {
    /*
     * Getter und Setter
     */
    public Address getAddress() {
        return address;
    }
	public String getCategory() {
		return category;
	}
	

    /**
     * gesch�tzter Konstruktor
     */
    protected AddressAction(Object action, String userId, Address address) {
        super(action, userId);
        this.address = address;
    }

    protected AddressAction(Object action_create, String userId, Address address2, String category) {
		this(action_create, userId, address2);
		this.category = category;
	}

	/*
     * private Daten
     */
    private Address address;
	private String category;
}
