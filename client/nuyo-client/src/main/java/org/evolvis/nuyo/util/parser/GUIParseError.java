/*
 * Created on 22.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.util.parser;

/**
 * @author niko
 *
 */
public class GUIParseError
{
  public final static Object ERROR_ELEMENT_NOT_FOUND = "ERROR_ELEMENT_NOT_FOUND";
  public final static Object ERROR_CONTAINER_NOT_FOUND = "ERROR_CONTAINER_NOT_FOUND";
  public final static Object ERROR_BAD_SYNTAX = "ERROR_BAD_SYNTAX";
  public final static Object ERROR_MISSING = "ERROR_MISSING";
  public final static Object ERROR_ELEMENT_USED = "ERROR_ELEMENT_USED";
  public final static Object ERROR_NOGUIELEMENTS = "ERROR_NOGUIELEMENTS";
  public final static Object ERROR_TOOMUCHGUIELEMENTNODES = "ERROR_TOOMUCHGUIELEMENTNODES";  
  
  public final static Object ERROR_NODATASOURCE = "ERROR_NODATASOURCE";
  public final static Object ERROR_NOWIDGET = "ERROR_NOWIDGET";
  public final static Object ERROR_MULTIPLEDATASOURCES = "ERROR_MULTIPLEDATASOURCES";
  public final static Object ERROR_MULTIPLEWIDGETS = "ERROR_MULTIPLEWIDGETS";
  
  public final static Object ERROR_DATASOURCE = "ERROR_DATASOURCE";
  public final static Object ERROR_DATASTORAGE = "ERROR_DATASTORAGE";
  public final static Object ERROR_WIDGET = "ERROR_WIDGET";
  public final static Object ERROR_VALIDATOR = "ERROR_VALIDATOR";
  public final static Object ERROR_LISTENER = "ERROR_LISTENER";
  public final static Object ERROR_ACTION = "ERROR_ACTION";
    
  public final static Object ERROR_WIZARD = "ERROR_WIZARD";
  public final static Object ERROR_WIZARDPAGE = "ERROR_WIZARDPAGE";
  
  
  // ---------------------------------------------
  
  private String m_sErrorName;
  private String m_sObjectName;
  private String m_sErrorDescription;
  
  public GUIParseError(Object type, String objectname)
  {
    m_sErrorName = getErrorNameOfType(type);
    m_sObjectName = objectname;
    m_sErrorDescription = "";
  }
  
  public GUIParseError(Object type, String errorname, String objectname)
  {
    m_sErrorName = errorname;
    m_sObjectName = objectname;
    m_sErrorDescription = "";
  }
  
  public GUIParseError(Object type, String errorname, String objectname, String errordescription)
  {
    m_sErrorName = errorname;
    m_sObjectName = objectname;
    m_sErrorDescription = errordescription;
  }
  
  private String getErrorNameOfType(Object type)
  {
    if (ERROR_ELEMENT_NOT_FOUND.equals(type)) return "unbekanntes Eingabefeld";
    else if (ERROR_CONTAINER_NOT_FOUND.equals(type)) return "unbekannter Container";
    else if (ERROR_BAD_SYNTAX.equals(type)) return "fehlerhafte Syntax";
    else if (ERROR_ELEMENT_USED.equals(type)) return "Eingabefeld wurde bereits zugeordnet";
    else if (ERROR_NOGUIELEMENTS.equals(type)) return "es wurden keine Eingabefeld-GUI-Beschreibungen (guifields Tag) konfiguriert";
    else if (ERROR_TOOMUCHGUIELEMENTNODES.equals(type)) return "es wurden wurden mehr als ein guifields Tag gefunden. Das ist evtl. nicht von ihnen gew�nscht.";
    else return "unbekannter Fehler";
  }
  
  public String toString()
  {
    if (m_sErrorDescription.length() > 0) return m_sErrorName + " \"" + m_sObjectName + "\" Grund: " + m_sErrorDescription;
    else return m_sErrorName + " \"" + m_sObjectName + "\"";
  }
  
}
