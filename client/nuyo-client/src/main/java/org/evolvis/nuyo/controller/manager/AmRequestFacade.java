/* $Id: AmRequestFacade.java,v 1.69 2007/08/30 16:10:28 fkoester Exp $
 * 
 * Created on 09.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink.
 *  
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import java.io.File;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ApplicationStarter;
import org.evolvis.nuyo.controller.ContactControlException;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressClientDAO;
import org.evolvis.nuyo.db.AddressExporter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.MailBatch.CHANNEL;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.db.veto.VetoableAction;
import org.evolvis.nuyo.gui.ContactGuiException;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.PanelInserter;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.gui.dialogs.DoubletSearchDialog;
import org.evolvis.nuyo.gui.fieldhelper.AddressField;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.plugin.FrameMailBatchPerformer;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;

import de.tarent.commons.plugin.Plugin;
import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Context;


/**
 * This class is responsible for processing complex tasks of the
 * {@link org.evolvis.nuyo.controller.ActionManager ActionManager},
 * in particular the ones requiring communication with external components. 
 * 
 * @author mikel
 */
public class AmRequestFacade extends AmActionFacade {

    private Contact currentJournalEntry;
    /** Flag, ob der Benutzer vor Beendigung der Applikation gefragt werden soll. */
    protected boolean isToAskBeforeExitApplication;


    /** 
     * TODO: Addresszugriff: Disable search filter
     */
    public void userRequestNoSearchFilter() {
        //setAddressFilter(null);
//         boolean wasSearch = getDb().isSearch() || isSpecialAddressSet();
//         setSpecialAddressSet(false);
//         getDb().setSearch(false);
//         //reset search context
//         setViewMode();
//         if (wasSearch) try {
//             Addresses a = getDb().getChosenAddresses();
//             loadcategory(a, -1, true);
//         } catch (ContactDBException e) {
//             publishError(getControlListener().getMessageString("AM_NoSearch_DbError"), e);
//         }
    }

    
    //
    // Navigation in der Adressliste
    //
    /** Show first address */
    public void userRequestFirstAddress() {
        if (allowDiscardAddress(true))
            setAddressPosition(0);
    }

    /** Show previous address */
    public void userRequestPrevAddress() {
        if (allowDiscardAddress(true))
            setAddressPosition(getAddressPosition() - 1);
    }

    /** Show next address */
    public void userRequestNextAddress() {
        if (allowDiscardAddress(true))
            setAddressPosition(getAddressPosition() + 1);
    }

    /** Show last address */
    public void userRequestLastAddress() {
        if (allowDiscardAddress(true))
            setAddressPosition(getAddresses().getIndexMax());
    }

    /** Show address at given index */
    public void userRequestSelectAddress(int index) {
        if ((getAddressPosition() != index) && allowDiscardAddress(true)){
            setAddressPosition(index);
        }
    }

    /**
     * Anzeige eines bestimmten Datensatzes; wenn dieser gar nicht in der
     * aktuellen Auswahl ist, wird gegebenenfalls die aktuelle Auswahl durch
     * die Auswahl der Einzeladresse ersetzt.
     * 
     * @param address die anzuzeigende Adresse
     * @param notInListAction {@link GUIListener#DO_FAIL}, {@link GUIListener#DO_ASK} oder {@link GUIListener#DO_ENFORCE}.
     * @param description Beschreibung des gegebenenfalls erstellten Filters.
     * @param type Typ des gegebenenfalls erstellten Filters, vergleiche Konstanten ADDRESSSET_*.
     * @return <code>true</code>, falls die Adresse angezeigt wird.
     */
    public boolean userRequestSelectAddress(Address address, int notInListAction, String description, int type) {
        if (address == null)
            return false;
        int newPosition = -1;
        try {
            newPosition = getAddressPosition(address);
        } catch (ContactDBException e) {
            publishError(getMessageString("AM_SelectAddress_DbError"), e);
        }
        if (newPosition == getAddressPosition())
            return true;
        if (!allowDiscardAddress(true))
            return false;
        if (newPosition >= 0) {
            setAddressPosition(newPosition);
            return true;
        }
        switch(notInListAction) {
        case GUIListener.DO_FAIL:
            return false;
        case GUIListener.DO_ASK:
            Object[] params = {"nicht ermittelbar", "nicht ermittelbar", "nicht ermittelbar", "nicht ermittelbar"};
            params = new Object[] {address.getShortAddressLabel(), address.getVorname(), address.getNachname(), address.getInstitution()};
            String question = MessageFormat.format(getMessageString("AM_SelectAddress_QueryReally"), params);
            if (askUser(question, getMessageString("Buttons_Yes_NO_CANCEL").split("[:]", 3), 0) != 0)
                return false;
        case GUIListener.DO_ENFORCE:
            NonPersistentAddresses addresses = new NonPersistentAddresses(getDb());
            addresses.addAddress(address);
            load(addresses, 0, true);
            return true;
        default:
            logger.warning(MessageFormat.format(getMessageString("AM_SelectAddress_UnknownAction"), new Object[]{new Integer(notInListAction)}));
            return false;
        }
    }

    //
    // Bearbeiten der aktuellen Adresse
    //
    /**
     * The user wants to create a new address
     */
    public boolean userRequestNewContact() {
        if (!allowDiscardAddress(true)) 
            return false;

        setNewAddress();
        setNewMode();
        return true;
    }

    /**
     * The user wants to edit the current address
     */
    public Veto userRequestEditable() throws ContactDBException {
        if (isNewAddressMode() || isEditMode()) return null;
        Veto veto = getDb().checkForVeto(VetoableAction.editAddress(getAddress(), getDb().getUser(), getNameOfSelectedCategory()));
        if (veto == null || !veto.isForbidden()) {
            setEditMode();
            getEventsManager().fireEditableVeto(veto, true);
        }
        logger.fine("[!] veto on edit");
        return veto;
    }

    /**
     * Der Benutzer m�chte den Editiervorgang beenden
     * Je nach Parameter muss noch abgefragt werden,
     * ob etwas ge�ndert wurde und gespeichert werden soll.
     * 
     * @param action {@link GUIListener#DO_DISCARD DO_DISCARD},
     *  {@link GUIListener#DO_ASK DO_ASK} oder {@link GUIListener#DO_SAVE DO_SAVE}.
     * @return <code>true</code>, gdw, dass der Editiervorgang beendet werden konnte.
     */
    public boolean userRequestNotEditable(int action) {
        try {
            switch (action) {
            case GUIListener.DO_DISCARD:
                cancelAddress();
                break;// edit-modus disabled
            case GUIListener.DO_SAVE:
                saveAddress();
                break;//edit-modus activated & new-modus deactivated
            case GUIListener.DO_ASK:
                if (allowDiscardAddress(true)) break;//discarding succedded
            default:
                return false;//discarding failed
            }
        } catch (ContactControlException e) {
            String infoKey;
            switch (e.getExceptionId()) {
            case ContactControlException.EX_ADDRESS_INCOMPLETE:
                infoKey = "AM_NotEditable_AddressIncomplete";
                break;
            case ContactControlException.EX_ADDRESS_NO_SUBCATEGORY:
                infoKey = "AM_NotEditable_NoSubCategory";
                break;
            case ContactControlException.EX_EMAIL1_INVALID:
                infoKey = "AM_Discard_Email1Invalid";
                break;               
            case ContactControlException.EX_EMAIL2_INVALID:
                infoKey = "AM_Discard_Email2Invalid";
                break;                
            case ContactControlException.EX_PHONE_INVALID:
                infoKey = "AM_Discard_PhoneInvalid";
                break;
            case ContactControlException.EX_SAVING_INTERRUPTED:
            	infoKey = "AM_Saving_Interrupted";
            	break;
            default:
                infoKey = "AM_NotEditable_Other";
            }
            if(!infoKey.equals("AM_Saving_Interrupted"))
            {
            	logger.log(Level.INFO, getControlListener().getMessageString(infoKey), e);
            	showInfo(getControlListener().getMessageString(infoKey));
            }
            else
            {
            	logger.log(Level.INFO, getControlListener().getMessageString(infoKey));
            }
            return false;
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_NotEditable_DbError"), sex);
            return false;
        }
        //if succedded only
        if(wasInSearchModeBeforeEdit())
        	setSearchMode();
        else
        	setViewMode();
        return true;
    }

    /**
     * TODO: Addresszugriff: Check handling for delete of an address
     *
     * The user wants to delete the current address or cancel the current edit-operation
     * 
     */
    public void userRequestDelete(boolean cancelInEditMode) {
        if ((isNewAddressMode() || isEditMode()) && cancelInEditMode) {
            if (allowDiscardAddress(false)) {
                if(isEditMode()) setViewMode();
                //else: stay in current context (i.e. in SEARCH_MODE)
            }
        } else try {
            Veto veto = getDb().checkForVeto(VetoableAction.deleteAddress(getAddress(), getDb().getUser()));

            if (veto != null) {
                publishError(getControlListener().getMessageString("AM_Delete_VetoErrorCaption"), veto.getMessage());
                if (veto.isForbidden()) return;
            }

            String categoryId = getNameOfSelectedCategory();
            String categoryName = null;
            String categoryDescription = null;
            if (categoryId != null) {
                Category category = getDb().getCategory(categoryId);
                if (category != null) {
                    categoryName = category.getName();
                    categoryDescription = category.getDescription();
                    if (categoryDescription.equals(categoryName))
                        categoryDescription = "";
                    if (categoryName.equals(categoryId))
                        categoryName = "";
                }
            }
            String question = MessageFormat.format(getMessageString("AM_Delete_QueryReally"), new Object[] {categoryId, categoryName, categoryDescription, getAddress().getShortAddressLabel(), getAddress().getVorname(), getAddress().getNachname(), getAddress().getInstitution()});

            if (askUser(question, getControlListener().getMessageString("Buttons_Yes_NO").split("[:]", 3), 0) == 0) {

                // Vor dem L�schen wird die Adresse nun zun�chst gespeichert, so dass die neue
                // Zuordnung auch in die Datenbank bernommen wird.
                // Weist man einer Adresse im Bearbeiten-Modus einer anderen Kategorie zu und
                // clickt dann auf "L�schen", wird die Adresse der anderen Kategorie zugewiesen und erst anschlie�end aus der aktuellen gel�scht.
                getControlListener().saveAddress();
                getDb().getAddressDAO().delete(getAddress());
                reload(true);
                if (isEditMode()) setViewMode();
                updateTotalAddressCount();

                //else: stay in current context (i.e. in SEARCH_MODE)
            }
        } catch (ContactDBException e) {
            publishError(getControlListener().getMessageString("AM_Delete_DbError"), e);
        }
    }

    /**
     * // TODO: Addresszugriff: Enable subcategory selection
     * Der Benutzer m�chte dem aktuellen Adresssatz eine Unterkategorie zuordnen
     */
    public Veto userRequestSetVerteiler() {
        return null;
        //         Veto veto = null;
        //         try {
        //             veto = getDb().checkForVeto(VetoableAction.classifyAddress(getAddress(), getDb().getUser()));

        //             if (veto != null) {
        //                 publishError(getControlListener().getMessageString("AM_SubCategories_VetoErrorCaption"), veto.getMessage());
        //                 if (veto.isForbidden()) return veto;
        //             }
        //             getControlListener().openVerteilerTreeWindow(getAddress(), true, false);
        //         } catch (ContactDBException e) {
        //             publishError(getControlListener().getMessageString("AM_SubCategories_DbError"), e);
        //         }
        //         return veto;
    }

    /**
     * Der Benutzer m�chte die aktuelle Eingabe postalisch korrigieren lassen.
     * <br>
     * Diese Methode arbeitet auf den aktuell angezeigten Feldinhalten der GUI  
     * und sollte deshalb nur im Editiermodus aufgerufen werden.
     * 
     * TODO: This method is not implemented?!?
     */
    public void userRequestCorrection() {
        PanelInserter panelinserter = getControlListener().getPanelInserter();
        FieldWrapper strasseUndNummer = new FieldWrapper(panelinserter.getAdressField(AddressKeys.STRASSE));
        FieldWrapper plzUndStadt = new FieldWrapper(panelinserter.getAdressField(AddressKeys.PLZ));
        FieldWrapper plzPostfach = new FieldWrapper(panelinserter.getAdressField(AddressKeys.POSTFACHPLZ));
        
        Environment env = ConfigManager.getEnvironment();

            String hostname = env.get(Key.ASSO_ZAM_SERVER_HOSTNAME, "localhost");
            int port = env.getAsInt(Key.ASSO_ZAM_SERVER_PORT, 20006);
        
            logger.warning("user request correction not implemented","AmRequestFacade#userRequestCorrection");
    }
    
    public static class FieldWrapper implements PluginData {
        AddressField field;
        Map data;
        public FieldWrapper(AddressField field) {
            this.field = field;
            this.data = new TreeMap();
            refreshData();
        }
        public void refreshData() { field.getData(this); }
        public void commitData() { field.setData(this); }
        public Object get(Object key) { return data.get(key); }
        public boolean set(Object key, Object value) { return (data.put(key, value) != null); }
        public Class getDatatype(Object key)
        {
          Object val = data.get(key); 
          if (val != null) return val.getClass();
          else return null;
        }
    }

    //
    // weitere Adress-bezogene Aktionen
    //
    /**
     * Veranlasst das Suchen nach Doubletten, bietet gegebenenfalls eine Liste
     * zur �bernahme an und kann sonst das Nichtvorhandensein von Dubletten
     * anzeigen.
     * 
     * <p>The method returns whether a saving operation should be continued or
     * not. In case that no duplicates are found it will always be <code>true</code>.
     * Only in case the duplicates exists the user has the option on how to continue.
     * The result of that chosing is returned.</p>
     * 
     * @param informNoDoubles wenn <code>true</code>, so wird eine Meldung
     *  ausgegeben, wenn keine Dubletten gefunden wurden.
     * @param enableSaveButton dieses Flag steuert, ob der Speichern-Button 
     *  im Dialog angezeigt wird
     */
    public DoubletSearchDialog guiRequestCheckForDouble(boolean informNoDoubles, boolean enableSaveButton) {
    	DoubletSearchDialog dialog = null;
    	
        logger.info("checking contact duplicates...");
        try {
            Address data = getDb().createAddress();
            getControlListener().getAddress(data);
            Addresses dupes = getDb().getAddressDAO().getDoubles(data, getAddress().getAdrNr());
            if (dupes != null && dupes.getIndexMax() >= 0) {
            	dialog = new DoubletSearchDialog(ApplicationServices.getInstance().getActionManager(), getFrame(), dupes, enableSaveButton);
        	} else if (informNoDoubles)
                showInfo(getControlListener().getMessageString("AM_Doubles_Caption"), getControlListener().getMessageString("AM_Doubles_NoneFound"));
        } catch (ContactControlException ex) {
            publishError(getControlListener().getMessageString("AM_Doubles_GuiError"), ex);
        } catch (ContactGuiException ex) {
            // unvollst�ndige Adressen brauchen nicht ueberprueft zu werden
            if (ex.getExceptionId() == ContactGuiException.EX_ADDRESS_INVALID)
                logger.info("Dublettencheckaufruf bei ung\u00FCltiger Adresse ignoriert.");
            else
                publishError(getControlListener().getMessageString("AM_Doubles_GuiError"), ex);
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_Doubles_DbError"));
        }
        
        return dialog;
    }

    /**
     * Der Benutzer m�chte in die aktuelle Adresse Daten aus einer VCard
     * importieren.
     * 
     * @return <code>true</code>, gdw erfolgreich importiert wurde.
     * @see org.evolvis.nuyo.gui.GUIListener#userRequestImportVcardIntoCurrentAddress()
     */
    public boolean userRequestImportVcardIntoCurrentAddress() {
        if(!isNewAddressMode()) {
            boolean canImport = userRequestNewContact();
            if(!canImport) {
                logger.warningSilent("[!] can't import: failed creating new contact");
                return false;
            }
        }
        File vcardFile = chooseVcardFile(true, false, false, null);
        logger.fine("[vcard-file]: " + (vcardFile == null ? "null" : "importing data..."));        
        return vcardFile != null ? importVcardIntoCurrentAddress(vcardFile) : false;
    }

    /**
     * Der Benutzer m�chte Kontaktdaten in eine VCard exportieren.
     *
     * @param range gibt an, welche Adressen exportiert werden k�nnen,
     *  vergleiche GUIListener.RANGE*. 
     * @return <code>true</code>, gdw erfolgreich exportiert wurde.
     * @see org.evolvis.nuyo.gui.GUIListener#userRequestExportIntoVcard(int)
     */
    public boolean userRequestExportIntoVcard(int range) {
        String presetFileName = null;
        switch(range) {
        case GUIListener.RANGE_CURRENT_LIST:
                presetFileName = getMessageString("AM_VCARD_Export_Preset_Category");
            break;
        case GUIListener.RANGE_CURRENT_ADDRESS:
                Object[] params = {getAddress().getVorname(), getAddress().getNachname(), getAddress().getInstitution()};
                presetFileName = MessageFormat.format(getMessageString("AM_VCARD_Export_Preset_Address"), params);
            break;
        }
        File vcardFile = chooseVcardFile(false, true, true, presetFileName);
        return vcardFile != null ? exportIntoVcard(vcardFile, range) : false;
    }
    
    

    /**
     * Der Benutzer m�chte Adressdaten exportieren.
     * Existierende Exportformate: Soffice
     *
     * @param addresses Liste der Adressen
     * @return <code>true</code>, gdw erfolgreich exportiert wurde.
     */
    
    public boolean userRequestExportAddresses(AddressListProvider addressesProvider) {
    	return userRequestExportAddresses(addressesProvider, addressesProvider.getAddresses().getSize());
    }
        

    /**
     * Der Benutzer m�chte Adressdaten exportieren.
     *
     * @param addresses Liste der Adressen
     * @param numberOfAddressesToExport Anzahl der Adressen, die exportiert werden sollen. Kann abweichen von 
     * der Anzahl der uebergebenen Adressen, da evtl nicht auf allen Adressen Leserecht besteht. 
     * @return <code>true</code>, gdw erfolgreich exportiert wurde.
     */
    public boolean userRequestExportAddresses(AddressListProvider addressesProvider, int numberOfAddressesToExport) {
        try {
            String[][] extensions = AddressExporter.getExtensions();
            String[] descriptions = AddressExporter.getDescriptions();
            String[] fileFormats = AddressExporter.getFileFormats();
            File file = chooseExportFile(true, true, extensions, descriptions, "addressexport."+extensions[0][0]);
            if (file == null)
                return false;

            String[] choosenExtensions = getLastChoosedFileExtensions();
            String fileFormat = fileFormats[0];
            for (int i = 0; i < extensions.length; i++) {
                // Vorsicht, hier verlasse ich mich darauf, 
                // dass die Instanz des Arrays eine von denen ist, die ich herein gesteckt haben.
                if (extensions[i] == choosenExtensions)
                    fileFormat = fileFormats[i];
            }
            AddressExporter exporter = new AddressExporter(addressesProvider, file, fileFormat, numberOfAddressesToExport);
            TaskManager.getInstance().register(exporter, getControlListener().getMessageString("AM_Export_Caption"), true, TaskManager.DEFAULT_PRIORITY+10);
            return true;
        } catch (Exception e) {
            publishError(getControlListener().getMessageString("AM_Export_Error"), e);
        }
        return false;
    }


    /** Browserstart mit der URL aus der aktuellern Adresse */
    public void userRequestStartBrowser() {
        String url = getAddress().getHomepageDienstlich();
        userRequestStartBrowser(url);
    }

    //
    // Versandauftrag-bezogene Aktionen
    //
    /**
     * Diese Methode fordert zu einem Versandvorgang auf.
     * 
     * @param action Die gewnschte Aktion, vergleiche
     * @see org.evolvis.nuyo.controller.ApplicationStarter
     */
    public void userRequestPost(String action) {
    	if (ApplicationStarter.POST_MAIL_ZIP.equals(action)){
    		checkAndPost(getAddress().getPlz(), getAddress().getOrt(),
    				getControlListener().getMessageString("AM_Post_NoStreetAddress"),
    				ApplicationStarter.POST_MAIL_ZIP);
    	} else if (ApplicationStarter.POST_MAIL_POBOX.equals(action)){
    		checkAndPost(getAddress().getPlzPostfach(), getAddress().getPostfachNr(),
    				getControlListener().getMessageString("AM_Post_NoPoBoxAddress"),
    				ApplicationStarter.POST_MAIL_POBOX);
    	}
    	else if (ApplicationStarter.POST_EMAIL.equals(action)) 
    	{
    		// check if an e-mail-address is present
    		if((getAddress().getEmailAdresseDienstlich() != null && getAddress().getEmailAdresseDienstlich().trim().length() > 0) || (getAddress().getEmailAdressePrivat() != null && getAddress().getEmailAdressePrivat().trim().length() > 0))
    			userRequestPost(action, getAddress());
    		else
    			publishError(getControlListener().getMessageString("AM_Post_NoEMailAddress"));
    	}

    }
    
    /**
     * Diese Methode fordert zu einem Versandvorgang auf.
     * 
     * Hier wird im speziellen noch die gew�nschte Zieladresse 
     * �bergeben, da diese nicht immer der im Adressbereich 
     * ausgew�hlten entsprechen muss. 
     * 
     * @param action Die gewnschte Aktion, vergleiche
     * @see org.evolvis.nuyo.controller.ApplicationStarter
     */
    public void userRequestPost(String action, Address address) {
        
    	if(ApplicationStarter.POST_EMAIL.equals(action))
    	{
    		NonPersistentAddresses addresses = new NonPersistentAddresses();
    		addresses.addAddress(ApplicationServices.getInstance().getActionManager().getAddress());

    		// If user configured emails to single addressees send with the internal email-program, use it
    		/* TODO Does not work yet, address-table does not want a NonPersistentAddresses-Object
    		if(ConfigManager.getPreferences().get(Key.EMAIL_COMMAND.toString(), "internal").equals("internal"))
    		{
    			DispatchEMailDialog.getInstance().setAddresses(addresses);

    			DispatchEMailDialog.getInstance().setVisible(true); 
    		}
    		// otherwise, start external email-program
    		else
    		{
    		*/
    			ApplicationStarter.getSystemApplicationStarter().startEMail(address);
    		//}

    		ApplicationServices.getInstance().getActionManager().userRequestCreateContactEntryDialog(Messages.getString("GUI_Dispatch_EMail_Subject"), "", new Date(), 0, false, Contact.CATEGORY_MAILING, Contact.CHANNEL_EMAIL, 0, 0, null, null);
    	}
    	else if(ApplicationStarter.POST_FAX_OFFICE.equals(action))
    	{
    		
    	}
    	else if(ApplicationStarter.POST_FAX_PRIVATE.equals(action))
    	{
    	
    	}
    }

    /**
     * Veranlasst das Versenden eines oder aller Kan�le eines
     * Versandauftrages. Es wird zun�chst versucht, dies die GUI
     * machen zu lassen, w�hrend als Fall-Back der ApplicationStarter
     * benutzt wird.
     * 
     * TODO This method is only used for e-mail. So remove stuff not related to e-mail
     * 
     * @param batch Versandauftrag
     * @param channel der zu verschickende Kanal
     */
    public void userRequestSendMailBatch(final MailBatch batch, final MailBatch.CHANNEL channel) {
    	
    	TaskManager.getInstance().register(new TaskManager.SwingTask() {

    		FrameMailBatchPerformer performer;
    		
			protected void failed(Context arg0)
			{
				logger.warning(Messages.getString("AM_SubCategory_Get_DbError"));
			}

			protected void runImpl(Context arg0) throws de.tarent.commons.utils.TaskManager.SwingTask.Exception
			{
		        if (MailBatch.CHANNEL.ALL == channel) {
		            userRequestSendMailBatch(batch, MailBatch.CHANNEL.MAIL);
		            userRequestSendMailBatch(batch, MailBatch.CHANNEL.POST);
		            userRequestSendMailBatch(batch, MailBatch.CHANNEL.FAX);
		            return;
		        }
				
				
				/************************************************************************
	            F�r emails soll kein Journaleintrag beim ausf�hren gemacht werden,
	            da dieser noch einmal beim letzendlichen Abschicken im Serienmail-Tab
	            generiert wird
				 ************************************************************************/
				if (!MailBatch.CHANNEL.MAIL.equals(channel))
					userRequestCreateContactEntries(batch, null, channel);

				//if (getControlListener().executeMailBatch(batch, channel)) return;

				if(PluginRegistry.getInstance().isPluginAvailable("mail"))
				{
					Plugin mailPlugin = PluginRegistry.getInstance().getPlugin("mail");

					if(mailPlugin.isTypeSupported(FrameMailBatchPerformer.class))
					{
						performer = (FrameMailBatchPerformer)mailPlugin.getImplementationFor(FrameMailBatchPerformer.class);
						performer.setMailBatch(batch, channel);
						performer.getFrame().setSize(800, 600);
						performer.getFrame().setLocationRelativeTo(ApplicationServices.getInstance().getActionManager().getFrame());
					}
				}	
			}

			protected void succeeded(Context arg0)
			{
				performer.getFrame().setVisible(true);
			}
    		
    	}, getMessageString("GUI_EMAIL_DISPATCH_DIALOG_INIT"), false);
    }

    /**
     * 
     * Arranges for showing one or all channels of a mail-batch
     * 
     * @param batch the mail-batch
     * @param the channel of the mail-batch to show
     */
    public void userRequestShowMailBatch(final MailBatch batch, final MailBatch.CHANNEL channel) { 

        // getPkList() may block, so we do this asynchronous

        TaskManager.getInstance().register(new TaskManager.SwingTask(){
                List pkList;
                ContactDBException exception = null;
                public void prepare(TaskManager.Context ctx) {
                    getControlListener().activateTab(ControlListener.TAB_CONTACT);
                }

                public void runImpl(TaskManager.Context ctx) throws TaskManager.SwingTask.Exception {
                    try {
                        pkList = batch.getChannelAddresses(channel).getPkList();                        
                    } catch (ContactDBException ex) {
                        exception = ex;
                        throw new TaskManager.SwingTask.Exception(getControlListener().getMessageString("AM_SubCategory_Get_DbError"));
                    }
                }

                public void succeeded(TaskManager.Context ctx) {
                    setAddressSet(pkList, getControlListener().getMessageString("GUI_VersandAuftrag_Versandauftrag"));
                }
                
                public void failed(TaskManager.Context ctx) {
                    publishError(getControlListener().getMessageString("AM_SubCategory_Get_DbError"));
                }
            }, getControlListener().getMessageString("GUI_VersandAuftrag_LadeAdressen"), false);
    }

    //
    // Freie externe Aktionen
    //
    /**
     * Browserstart mit freier URL
     * 
     * @param url die anzuzeigende URL
     */
    public void userRequestStartBrowser(String url) {
        String result = (url == null || url.length() == 0) ? getControlListener().getMessageString("AM_Browser_NoUrl")
                : ApplicationStarter.getSystemApplicationStarter().startBrowser(url);
        if (result != null) publishError(result);
    }
    
    /**
     * Stellt fest, ob der aktuelle Benutzer <code>right</code> auf der aktuellen Adresse hat.
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean userRequestUserHasRightOnAddress(int right){
        try {
            return getDb().userHasRightOnAddress(getDb().getUser(),right,getAddress().getId());
        } catch (ContactDBException e) {
            return false;
        }
    }

    /**
     * Stellt fest, ob der aktuelle Benutzer <code>right</code> auf der Adresse  <code>adress</code>hat.
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @param adress Adresse auf der das Recht bestehen muss
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean userRequestUserHasRightOnAddress(int right, Address address){
        try {
            return getDb().userHasRightOnAddress(getDb().getUser(),right,address.getId());
        } catch (ContactDBException e) {
            return false;
        }
    }

    /**
     * Stellt fest, ob der aktuelle Benutzer <code>right</code> auf einer Kategorie hat
     * 
     * @param right @see de.tarent.contact.security.SecurityManager
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean userRequestUserHasRightOnFolder(int folderId, int right){
        try {
            return getDb().userHasRightOnFolder(getDb().getUser(),right,folderId);
        } catch (ContactDBException e) {
            return false;
        }
    }
    
    /**
     * Stellt fest, ob ein User ein Event editieren darf
     * @return <code>true</code> falls Recht besteht, <code>false</code> sonst.
     * @throws ContactDBException 
     */
    public boolean mayUserWriteEvent(Integer eventID){
        try {
            return getDb().mayUserWriteEvent(eventID.intValue());
        } catch (ContactDBException e) {
            return false;
        }
    }

    //
    // direktes Steuern der GUI
    //

//     /** Unterkategorienauswahl �ffnen */
//     public void userRequestChangeVerteiler() {
//         getControlListener().openVerteilerChooser();
//     }
    
//     /** Suchfilterfenster �ffnen */
//     public void userRequestSearchFilter() {
//         getControlListener().openSuche();
//     }

    /** "Serienversand alle"-Fenster �ffnen */
    public void userRequestSerialPostDispatch() {
        getControlListener().openSerienVersandAlle();
    }

    /** Adresstabelle �ffnen und schlie�en */
    public void userRequestShowTable(boolean showIt) {
        getControlListener().showTable(showIt);
        getPreferences("menus").put("table.isVisible",Boolean.toString(showIt));
    }

    //
    // Applikation steuern
    //
    /** Anwendung schlie�en */
    public void userRequestExitApplication() {
        boolean isExitAllowed = false;
        if (isEditMode()) {
            String[] answers = getControlListener().getMessageString("AM_Exit_EditMode_Query_3Choices").split("[:]", 3);
            String[] tooltips = getControlListener().getMessageString("AM_Exit_EditMode_Query_3Tooltips").split("[:]", 3);
            int result = askUser(
                            getControlListener().getMessageString("AM_Exit_EditMode_Query"),
                            answers, tooltips, 0);
            isExitAllowed = isReadyToQuit( result );
        } else if (isNewAddressMode()) {
            String[] answers = getControlListener().getMessageString("AM_Exit_NewMode_Query_3Choices").split("[:]", 3);
            int result = askUser(
                            getControlListener().getMessageString("AM_Exit_NewMode_Query"),
                            answers, 0);
            isExitAllowed = isReadyToQuit( result );
       } else if (isToAskBeforeExitApplication) {
            if (askUser(
                    getControlListener().getMessageString("AM_Exit_Query"),
                    getControlListener().getMessageString("Buttons_Yes_NO").split("[:]", 2), 0) == 0) isExitAllowed = true;
        } else 
            isExitAllowed = true;

        if (isExitAllowed) {
            System.exit(0);
        }
    }

    /** Invokes cancel for '0' and save for '1'.*/
    private boolean isReadyToQuit( int result ) {
        switch ( result ) {
        case 0:
            return getControlListener().cancelAddress();
        case 1:
            return getControlListener().saveAddress();
        default:
            return false;
        }
    }
    
    //
    // administrative Methoden
    //
    /**
     * Diese Methode erzeugt eine Kategorie.
     * 
     * @param key Kategorienschlssel.
     * @param name Kategorienklartext.
     * @param description die Kategorienbeschreibung.
     */
    public void userRequestCreateCategory(String key, String name, String description) {
        try {
            setWaiting(true);
            getDb().createCategory(key, name, description);
            updateLists();
            getEventsManager().fireCategoryAdded(key, name, description);
        } catch (ContactDBException ex) {
            if (ex.getExceptionId() == ContactDBException.EX_INVALID_CATEGORY_KEY) {
                publishError(getControlListener().getMessageString("AM_Category_Create_InvalidKey"));
            } else {
                publishError(getControlListener().getMessageString("AM_Category_Create_DbError"), ex);
            }
        } finally {
            setWaiting(false);
        }
        ApplicationServices.getInstance().getApplicationModel().raiseCategoriesUpdateWatch();
    }

    /**
     * Diese Methode editiert eine Kategorie.
     * 
     * @param key der Kategorieschlssel.
     * @param name der Kategorieklartext.
     * @param description die Kategorienbeschreibung.
     */
    public void userRequestEditCategory(String key, String name, String description) {
        try {
            setWaiting(true);
            getDb().editCategory(key, name, description);
            updateLists();
            getEventsManager().fireCategoryEdited(key, name, description);
        } catch (ContactDBException ex) {
            if (ex.getExceptionId() == ContactDBException.EX_INVALID_CATEGORY_KEY) {
                publishError(getControlListener().getMessageString("AM_Category_Edit_InvalidKey"));
            } else {
                publishError(getControlListener().getMessageString("AM_Category_Edit_DbError"), ex);
            }
        } finally {
            setWaiting(false);
        }
        ApplicationServices.getInstance().getApplicationModel().raiseCategoriesUpdateWatch();
    }

    /**
     * Diese Methode l�scht eine Kategorie.
     * 
     * @param key der Kategorienschlssel.
     */
    public void userRequestDeleteCategory(String key) {
        try {
            setWaiting(true);
            getDb().deleteCategory(key);
            updateLists();
            getEventsManager().fireCategoryRemoved(key);
        } catch (ContactDBException ex) {
            if (ex.getExceptionId() == ContactDBException.EX_INVALID_CATEGORY_KEY) {
                publishError(getControlListener().getMessageString("AM_Category_Delete_InvalidKey"));
            } else {
                publishError(getControlListener().getMessageString("AM_Category_Delete_DbError"), ex);
            }
        } finally {
            setWaiting(false);
        }
        ApplicationServices.getInstance().getApplicationModel().raiseCategoriesUpdateWatch();
    }

    /**
     * Diese Methode erzeugt eine Unterkategorie.
     * 
     * @param category die Kategorie, in der eine Unterkategorie
     *  erzeugt werden soll.
     * @param key der Unterkategorieschlssel.
     * @param name1 Unterkategorieklartext, Teil 1.
     * @param name2 Unterkategorieklartext, Teil 2.
     * @param name3 Unterkategorieklartext, Teil 3.
     */
    public void userRequestCreateVerteiler(Integer category, String name1, String name2) {
   	
        try {
            setWaiting(true);
            getDb().createVerteiler(category, name1, name2);
            getEventsManager().fireSubCategoryAdded(category.toString(), "", name1, name2, "");
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_SubCategory_Create_DbError"), ex);
        } finally {
            setWaiting(false);
        }
        ApplicationServices.getInstance().getApplicationModel().raiseCategoriesUpdateWatch();
    }

    /**
     * Diese Methode editiert eine Unterkategorie.
     * 
     * @param category die Kategorie, in der die Unterkategorie
     *  liegt.
     * @param key der Unterkategorieschlssel.
     * @param name1 der Unterkategorieklartext, Teil 1.
     * @param name2 der Unterkategorieklartext, Teil 2.
     * @param name3 der Unterkategorieklartext, Teil 3.
     *  Dieser Parameter wird nur benutzt, wenn das
     *  {@link org.evolvis.nuyo.db.Database#getFeature(String) Feature}
     *  "verteiler.kommentar" von der Datenzugriffsschicht untersttzt wird.
     */
    public void userRequestEditVerteiler(String category, String key, String name1, String name2, String name3) {
        try {
            setWaiting(true);
            getDb().editVerteiler(category, key, name1, name2, name3);
            
            getEventsManager().fireSubCategoryEdited(category, key, name1, name2, name3);
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_SubCategory_Edit_DbError"), ex);
        } finally {
            setWaiting(false);
        }
        ApplicationServices.getInstance().getApplicationModel().raiseCategoriesUpdateWatch();
    }

    /**
     * Diese Methode l�scht eine Unterkategorie.
     * 
     * @param category die Kategorie, in der eine Unterkategorie
     *  gel�scht werden soll.
     * @param key der Unterkategorieschlssel.
     */
    public void userRequestDeleteVerteiler(String category, String key) {
        try {
            setWaiting(true);
            getDb().deleteVerteiler(category, key);
            getEventsManager().fireSubCategoryRemoved(category, key);
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_SubCategory_Delete_DbError"), ex);
        } finally {
            setWaiting(false);
        }
        ApplicationServices.getInstance().getApplicationModel().raiseCategoriesUpdateWatch();
    }


    /**
     * Diese Methode erzeug eine neue Benutzergruppe und liefert diese zur�ck. 
     * 
     * @param name Name der Benutzergruppe
     * @param description Beschreibung der Benutzergruppe
     * @param manager Manager der Benutzergruppe
     * @return erzeugte Benutzergruppe oder <code>null</code> im Fehlerfall
     */
    public UserGroup userRequestCreateUserGroup(String name, String description, String shortdesc, int globalroleID) {
        try {
            setWaiting(true);
            UserGroup newGroup = getDb().createUserGroup(name, description, shortdesc, globalroleID);
            if (newGroup != null)
                getEventsManager().fireUserGroupAdded(newGroup);
            return newGroup; 
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_UserGroup_Create_DbError"), ex);
        } finally {
            setWaiting(false);
        }
        return null;
    }
    
    /**
     * Diese Methode erzeugt einen neuen Benutzer.
     * 
     * @param userLogin die Benutzerkennung.
     * @param password das Passwort.
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param email des Users
     */
    public void userRequestCreateUser(String userLogin, String password,
            String lastName, String firstName, String email) {
        try {
            setWaiting(true);
            getDb().createUser(userLogin, password, lastName, firstName);
            
            User user = getUser(userLogin);
            int userID = user.getId();

            // Zugriff auf den Kalender des Users, 
            // damit dieser angelegt wird.
            user.getCalendar(true);
            
            // Speichern der Email in der assoziierten Adresse
            Address userAddress = user.getAssociatedAddress();
            if (userAddress != null) {
                userAddress.loadAllData();
                userAddress.setEmailAdresseDienstlich(email);
                getDb().getAddressDAO().save(userAddress);
            } else {
                logger.log(Level.WARNING, "Kann assoziierte Adresse zum User "+userLogin +" nicht finden");
            }

            // und in den User Params
            getDb().setUserParameter("" + userID, "email", email);
            // Kategorien neu Laden
            reloadCategories();
            getEventsManager().fireUserAdded(userLogin, lastName, firstName, email);
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_User_Create_DbError"), ex);
        } finally {
            setWaiting(false);
        }
    }

    /**
     * Diese Methode �ndert einen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     * @param password das Passwort.
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param email des Users
     */
    public void userRequestEditUser(String userId, String password, String lastName, String firstName, String email) {
        try {
            //System.out.println("AmRequestFacade.userRequestEditUser");
            setWaiting(true);
            getDb().changeUser(userId, password, lastName, firstName, email);
          
            /***************************************************************
             * Hinweis von Nils:
             * Die emailadresse wird nun serverseitig vom Userobjekt 
             * in die assoziierte Adresse bertragen
             ***************************************************************
             
            // Speichern der Email in der assoziierten Adresse
            Address userAddress = getUser(getUserName(userId)).getAssociatedAddress();
            if (userAddress != null) {
                userAddress.loadAllData();
                userAddress.setEmailAdresse(email);
                userAddress.commit();
            } else {
                logger.log(Level.WARNING, "Kann assoziierte Adresse zum User "+getUserName(userId) +" nicht finden");
            }

            */

            // und in den User Params
            
            getDb().setUserParameter(userId, "email", email);

            getEventsManager().fireUserChanged(userId, lastName, firstName, email);
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_User_Change_DbError"), ex);
        } finally {
            setWaiting(false);
        }
    }

    /**
     * TODO: Addresszugriff: Check if deletion of address is wanted here
     *
     * Diese Methode l�scht einen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     */
    public void userRequestDeleteUser(String userId, boolean deleteAssoziatedAddress) {
        try {
            setWaiting(true);
            if (deleteAssoziatedAddress) {
                Address userAddress = getUser(getUserName(userId)).getAssociatedAddress();
                if (userAddress != null) {                    
                    getDb().getAddressDAO().delete(userAddress);
                } else
                    logger.log(Level.WARNING, "Kann assoziierte Adresse zum User "+getUserName(userId) +" nicht finden");
            }
            getDb().deleteUser(userId);
            // Kategorien neu Laden
            reloadCategories();
            getEventsManager().fireUserRemoved(userId);
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_User_Delete_DbError"), ex);
        } finally {
            setWaiting(false);
        }
    }

    
    public PanelInserter getPanelInserter()
    {
      return getControlListener().getPanelInserter();
    }

    public void userRequestCallPhone(String phoneNumber) 
    {
        String result = (phoneNumber == null || phoneNumber.trim().length() == 0) ? 
                getControlListener().getMessageString("AM_Phone_Call_No_Number")
                : ApplicationStarter.getSystemApplicationStarter()
                        .startPhoneCall(phoneNumber);
        if (result != null) publishError(result);
    } 
    
    
    public void userRequestCreateContactEntryDialog(Contact contact){
        try 
        {
            getControlListener().showContactEntryDialog(contact.getAttribute(Contact.KEY_SUBJECT),
                    contact.getAttribute(Contact.KEY_NOTE), contact.getStart(), contact.getDuration(), contact.isInbound(), contact.getCategory(), contact.getChannel(), contact.getLink(), contact.getLinkType(), contact.getUser(), contact.getAddress(), true);
        }
        catch (ContactDBException e) 
        {
            e.printStackTrace();
        }
    }
    
    public void userRequestCreateContactEntryDialog(String subject, String note, Date datum, int duration, boolean incoming, int category, int channel, int link, int linkType, User user, Address address)
    {
        getControlListener().showContactEntryDialog(subject, note, datum, duration, incoming, category, channel, link, linkType, user, address, false);
    }
    
    public void setCurrentJournalEntry(Contact con)
    {
        currentJournalEntry = con;
    }
    
    public Contact getCurrentJournalEntry()
    {
        return currentJournalEntry;
    }
    
    /**
     * @param category
     * @param channel
     * @param duration
     * @param inBound
     * @param subject
     * @param note
     * @param startDate
     */
    public Contact userRequestCreateContactEntry(Address address, User user, int category, int channel, int duration, boolean inBound, String subject, String note, Date startDate) {
        try {
            Contact con = getDb().createContact(address, user, category, channel, 0, null, inBound, subject, note, null, startDate, duration);
            
            //Das sorgt f�r eine Refresh der Address-bezogenen Daten in der GUI, 
            //ist aber wahrscheinlich alles andere als die feine Englische....
            setAddress();
            
            return con;
            
        } catch (ContactDBException e) {
            publishError(Messages.getString("CreateContactEntry_Failure"), e);
        }
        return null;
    }
    
    
    /**
     * Diese Methode erzeugt f�r alle Adressen in diesem Kanal des Versandauftrags einen ContactEntry
     * 
     * @param batch
     * @param user
     * @param MailBatchChannel
     */
    public void userRequestCreateContactEntries(MailBatch batch, User user, CHANNEL MailBatchChannel){
        try {
            user = getUser(getCurrentUser());                
            Addresses adresses = batch.getChannelAddresses(MailBatchChannel);
            int size = adresses.getIndexMax()+1;
            //Determine channel and category
            int category;
            int channel;
            if(CHANNEL.MAIL.equals(MailBatchChannel)){
                category = Contact.CATEGORY_MAILING;
                channel = Contact.CHANNEL_EMAIL;
            }else if(CHANNEL.POST.equals(MailBatchChannel)){
                category = Contact.CATEGORY_MAILING;
                channel = Contact.CHANNEL_MAIL;
            }else if(CHANNEL.FAX.equals(MailBatchChannel)){
                category = Contact.CATEGORY_MAILING;
                channel = Contact.CHANNEL_FAX;
            }else{
                publishError("Fehler beim Erzeugen der Contact-Eintr�ge: unbekannter Kanal");
                return;
            }
            String subject = batch.getName();
            String note = "Kanal: " + MailBatchChannel + " wurde ausgef�hrt!";
            
            Contact[] contacts = new Contact[size];
            
            for(int i=0; i<size; i++){
                Address address = adresses.get(i);
                contacts[i] = getDb().createContact(address, user, category, channel, 0, null, false, subject, note, null, new Date(), 0, false);
            }
            getDb().commitContactList(contacts);
          
            setAddress();
        } catch (ContactDBException e) {
            publishError("Es ist ein Fehler beim Anlegen der Kontaktinformationen aufgetreten.", e);
        }
    }
    
    public Veto userRequestCheckForVeto(VetoableAction action){
        try {
            return getDb().checkForVeto(action);
        } catch (ContactDBException e) {
            return new Veto("Fehler beim Testen des Rechts", e.getLocalizedMessage(), true);
        }
    }




    public void userRequestCreateRandomTestAddresses() {
        try {
            final int count = 10;
            final List subCategories = getControlListener().requestSubCategoriesForNewAddress(null);

            int res = askUser("Anlegen von Testdaten", "Sollen wirklich "+count+" Testadressen angelegt werden?" , 
                              new String[] {"Ja, anlegen", "Abbrechen"}, 1);
        
            if (res == 0) {
                TaskManager.Task executor = new TaskManager.Task(){
                        boolean cancel = false;
                        public void run(TaskManager.Context cntx) {
                        
                            try {
                                cntx.setActivityDescription("Testdaten anlegen");
                            
                                cntx.setGoal(count);
                            
                                for (int i = 0; i < count && !cancel; i++) {
                                    if (i % 10 == 0)
                                        cntx.setCurrent(i);
                                    AddressClientDAO adao = getDb().getAddressDAO();
                                
                                    Address address = getDb().createAddress();
                                    address.setVorname(getRandomString(8));
                                    address.setNachname(getRandomString(12));
                                    adao.create(address, subCategories);
                                }
                            } catch (Exception e) {
                                publishError("Fehler beim Generieren von Testdaten", e);                                        
                            }
                        }
                        
                        public void cancel() {
                            cancel = true;
                        }
                    };
            
                TaskManager.getInstance().register(executor, "Test Adressen", true);

            }

        } catch (Exception e) {
            publishError("Fehler beim Generieren von Testdaten", e);                                        
        }
    }

    static String getRandomString(int length) {
        byte[] ba = new byte[length];
        for (int i = 0; i < length; i++) {
            ba[i] = (byte)('a' + ((Math.random() * 5000) % 50));
        }
        return new String(ba);
    }

}
