/* $Id: MailFolder.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink and Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.List;

/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public interface MailFolder {
	//
	// Attribute
	//
	/**
	 * Store, in dem der Folder liegt
	 */
	public MailStore getStore() throws ContactDBException;
	
	/**
	 * ID des Stores, in dem der Folder liegt
	 */
	public int getStoreId() throws ContactDBException;
	
	/**
	 * ID des Folders
	 */
	public int getFolderId() throws ContactDBException;
	
	//
	// Textattribute
	//
	/**
	 * vgl. Konstanten <code>KEY_*</code> und <code>VALUE_*</code>
	 */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;
    public void confirm() throws ContactDBException;
    
    /**
     * Diese Methode aktualisiert gecachete Daten zum Folder. 
     * 
     * @throws ContactDBException
     */
	public void reload() throws ContactDBException;
	
	/**
	 * Diese Methode l�scht diesen MailStore. 
	 * 
	 * @throws ContactDBException
	 */
	public void delete() throws ContactDBException;
	
	/**
	 * Diese Methode sagt dem Server das die Nachrichten,
	 * die zum l�schen markiert sind, gel�scht werden sollen.
	 * 
	 * @throws ContactDBException
	 */
	public void expunge() throws ContactDBException;
	
	/**
	 * Gibt den Status zur�ck, wie weit sich der Server
	 * bereits mit dem Mail-Server verbunden hat.
	 * 
	 * @return
	 * @throws ContactDBException
	 */
	public int getStatus() throws ContactDBException;
	
	/**
	 * Gibt den Status zur�ck, ob der Server sich
	 * bereits mit dem Mail-Server synchronisiert hat.
	 * 
	 * @return boolean ob dieser Folder bereits 
	 * @throws ContactDBException
	 */
	public boolean isSynchronized() throws ContactDBException;
	
	public boolean isInit() throws ContactDBException;
	
    //
    // Unterverzeichnisse
    //
	/** Liste der Sub-Folder (MailFolder-Instanzen) */
	public List getSubFolders() throws ContactDBException;
	
	/**
	 * Diese Methode erzeugt einen neuen Sub-Folder
	 * 
	 * @param name Name des zu erzeugenden Folders
	 * @return der neu erzeugte Folder.
	 * @throws ContactDBException
	 */
    public MailFolder createSubFolder(String name) throws ContactDBException;
    
    
    //
    // Mitteilungen
    //
    /**
     * Diese Methode liefert eine Liste von Mitteilungen (MailMessage-Instanzen)
     */
	public List getMessageList() throws ContactDBException;
	
	
	//
    // Konstanten
    //
    /**
     * Textattribut-Schl�ssel:
     * KEY_NAME - Ordnername
     * KEY_FULLNAME - Ordnername inkl. Pfad (z.B. 'INBOX/Send' oder 'news.linux.kernel')
     * KEY_STATUS
     * KEY_COUNT_ALL
     * KEY_COUNT_NEW
     * KEY_COUNT_UNREAD
     * KEY_COUNT_DELETED
     * KEY_SORT_KEY
     * KEY_SORT_REVERSE
     */
    public final static String KEY_NAME = "name";
    public final static String KEY_FULLNAME = "fullname";
    public final static String KEY_STATUS = "status";
    public final static String KEY_COUNT_ALL = "all";
    public final static String KEY_COUNT_NEW = "new";
    public final static String KEY_COUNT_UNREAD = "unread";
    public final static String KEY_COUNT_DELETED = "deleted";
    public final static String KEY_SORT_KEY = "sortKey";
    public final static String KEY_SORT_REVERSE = "sortReverse";
}
