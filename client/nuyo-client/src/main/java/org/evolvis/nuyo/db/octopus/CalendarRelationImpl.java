package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentCalendarRelation;
import org.evolvis.nuyo.db.AppointmentReminder;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.VirtualEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;



/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.AppointmentCalendarRelation
 *
 */
public class CalendarRelationImpl extends RequestBean implements AppointmentCalendarRelation, VirtualEntity {

    //
    //	Instanzmerkmale
    //
    /** Anzeigemodus */
    private Integer _displayMode;
    /** der dazugeh�rige Kalender */
    private Calendar _calendar;
    /** die dazugeh�rgen Reminder */
    private Collection _reminders = new HashSet();
    
    /** Objekt zum Holen von Kalenderrelationen */
    static private AbstractEntityFetcher _fetcher;
    
    
    static {
        _fetcher = new AbstractEntityFetcher("getScheduleRelations", true) {
            public IEntity populate(ResponseData response) throws ContactDBException {
                AppointmentCalendarRelation acr = new CalendarRelationImpl(null, null);
                ((IEntity)acr).populate(response);
                return (IEntity)acr;
            }
        };
    }
    
    
    /**	Konstruktor f�r dieses Objekt */
    protected CalendarRelationImpl(Appointment appointment, Calendar calendar) {
        _appointment = appointment;
        _calendar = calendar;
    }
    
    /**	Alle Reminder dieser Kalenderrelation beziehen */
    public Collection getReminders() throws ContactDBException {
        if (_reminders == null) {
            _reminders = new HashSet();
            Method method = new Method("getRemindersFromRelation");
            method.add("relationid", _id);
            List list = (List)method.invoke();
            if (list != null) {
	            for (Iterator it = list.iterator();it.hasNext();) {
	                List entry = (List)it.next();
	                AppointmentReminder reminder = new ReminderImpl();
	                ((IEntity)reminder).populate(new ResponseData(entry));
	                _reminders.add(reminder);
	            }
            }
        }
        return _reminders;
    }
    /**	Ein lokales Reminderobjekt wird angelegt und dieser Relation zugeordnet */
    public AppointmentReminder createReminder() throws ContactDBException {
        ReminderImpl ar = new ReminderImpl(_appointment, _calendar);
        _reminders.add(ar);
        return ar;
    }
    /**	Reminder von der Kalenderrelation l�schen */
    public void remove(AppointmentReminder reminder) throws ContactDBException {
    }
    
    
    /** Kalender dieser Relation beziehen */
    public Calendar getCalendar() throws ContactDBException {return _calendar;}

    
    /**	Displaymodus beziehen */
    public int getDisplayMode() throws ContactDBException {return (_displayMode == null)?0:_displayMode.intValue();}
    /** Displaymodus setzen.@param newDisplayMode - zu setzender Modus */
    public void setDisplayMode(int newDisplayMode) throws ContactDBException {_displayMode = new Integer(newDisplayMode);}

    
    /**	Anzahl der internen Teilnehmer */
    public int getParticipantCount() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }

    
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}

   
    /** Diese Relation wird vom Appointment gespeichert */
    public void commit() throws ContactDBException {setDirty(true);}

    /**	L�scht die Zuordnung zum Appointment */
    public void delete() throws ContactDBException {
    	setDeleted(true);
    }

    
    /**	Stellt Ursprungsform wieder her */
    public void rollback() throws ContactDBException {
        populate(_responseData);
    }

    
    /**	Neues Objekt mit DB - Inhalten f�llen */
    public void populate(ResponseData response) throws ContactDBException {
        _responseData = response;
        _creation = response.getDate(11);
        _id = response.getInteger(0);
        _type = response.getInteger(1);
        _status = response.getInteger(2);
        _level = response.getInteger(3);
        setAttribute(KEY_DISPLAY_NAME, response.getString(4));
        setAttribute(KEY_DESCRIPTION, response.getString(5));
        _displayMode = response.getInteger(6);
        _calendar = new CalendarImpl(response.getInteger(7), response.getString(8), response.getInteger(9), response.getInteger(10));
    }

    
    /**	Hier k�nnen gem�� Filter CalendarRelations abgeholt werden.@param selection - Auswahl der Relationen */
    static public Collection getCalendarRelations(ISelection selection) throws ContactDBException {return _fetcher.getEntities(selection,false);}
    
    
    /**	Stringrepr�sentation des Objekts */
    public String toString() {
        return new StringBuffer()
        	.append("CalendarRelation : ")
        	.append(_id)
        	.append(" ")
        	.append(_type)
        	.append(" ")
        	.append(_status)
        	.append(" ")
        	.append(_level)
        	.append(_attributes)
        	.toString();
    }
 
}

 
