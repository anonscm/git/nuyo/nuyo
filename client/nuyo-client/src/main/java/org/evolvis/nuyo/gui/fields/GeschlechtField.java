/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Color;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox.IconComboBoxEntry;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.gui.GUIHelper;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class GeschlechtField extends ContactAddressTextField
{
  // f�r die ComboBox "Geschlecht"
  private static final String GESCHLECHT_STRING_MAENNLICH = "m�nnlich";
  private static final String GESCHLECHT_STRING_WEIBLICH = "weiblich";
  private static final String GESCHLECHT_STRING_FIRMA = "Firma";
  private static final String GESCHLECHT_STRING_UNBEKANNT = "unbekannt";
  
  private TarentWidgetIconComboBox m_oCombobox_Geschlecht;
  private TarentWidgetLabel m_oLabel_Geschlecht;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
    //TODO
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createGeschlechtPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "GESCHLECHT";
  }

  public void postFinalRealize()
  {    
  }
  
  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      m_oCombobox_Geschlecht.setEnabled(true);
    }
    else
    {  
      m_oCombobox_Geschlecht.setEnabled(false);
    }
  }
  
  public void setData(PluginData data)
  {
      m_oCombobox_Geschlecht.setSelectedItem(getStringOfGeschlechtChar( (String)data.get(AddressKeys.GESCHLECHT)));
  }

  public void getData(PluginData data)
  {
      data.set(AddressKeys.GESCHLECHT, getCharOfGeschlechtString((((IconComboBoxEntry)m_oCombobox_Geschlecht.getSelectedItem()).getText())));
  }

  public boolean isDirty(PluginData data)
  {
      if (!((m_oCombobox_Geschlecht.getSelectedItem().equals("")) && (data.get(AddressKeys.GESCHLECHT) == null || "".equals(data.get(AddressKeys.GESCHLECHT)) || Address.GESCHLECHT_UNDEFINED.equals(data.get(AddressKeys.GESCHLECHT))))) //$NON-NLS-1$
          if (!(m_oCombobox_Geschlecht.isEqual(getStringOfGeschlechtChar((String)data.get(AddressKeys.GESCHLECHT)))))
              return (true);    
    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------
  
  public String getFieldName()
  {
    return Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geschlecht");    
  }
  
  public String getFieldDescription()
  {
    return Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geschlecht_ToolTip");
  }
  
  
  
  private EntryLayoutHelper createGeschlechtPanel(String widgetFlags)
  {               
    m_oCombobox_Geschlecht = new TarentWidgetIconComboBox();    
    m_oCombobox_Geschlecht.setToolTipText(Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geschlecht_ToolTip")); //$NON-NLS-1$

    m_oCombobox_Geschlecht.setSelectedColors(Color.BLACK, new Color(0x9c, 0x99, 0xcd));
    m_oCombobox_Geschlecht.setUnselectedColors(Color.BLACK, Color.WHITE);

    m_oCombobox_Geschlecht.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper.getIcon(GUIHelper.ICON_SEX_UNKNOWN), GESCHLECHT_STRING_UNBEKANNT));
    m_oCombobox_Geschlecht.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper.getIcon(GUIHelper.ICON_SEX_MALE), GESCHLECHT_STRING_MAENNLICH));
    m_oCombobox_Geschlecht.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper.getIcon(GUIHelper.ICON_SEX_FEMALE), GESCHLECHT_STRING_WEIBLICH));
    m_oCombobox_Geschlecht.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper.getIcon(GUIHelper.ICON_SEX_FIRMA), GESCHLECHT_STRING_FIRMA));

    m_oLabel_Geschlecht = new TarentWidgetLabel(Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geschlecht"));
    return(new EntryLayoutHelper(m_oLabel_Geschlecht, m_oCombobox_Geschlecht, widgetFlags));
  }
  
    private String getCharOfGeschlechtString(String geschlecht) {
        if (GESCHLECHT_STRING_MAENNLICH.equals(geschlecht))
            return (Address.GESCHLECHT_M);
        else if (GESCHLECHT_STRING_WEIBLICH.equals(geschlecht))
            return (Address.GESCHLECHT_W);
        else if (GESCHLECHT_STRING_FIRMA.equals(geschlecht))
            return (Address.GESCHLECHT_FIRMA);
        else
            return (Address.GESCHLECHT_UNDEFINED);
    }

    private String getStringOfGeschlechtChar(String geschlechtChar) {
        if (Address.GESCHLECHT_M.equals(geschlechtChar))
            return (GESCHLECHT_STRING_MAENNLICH);
        else if (Address.GESCHLECHT_W.equals(geschlechtChar))
            return (GESCHLECHT_STRING_WEIBLICH);
        else if (Address.GESCHLECHT_FIRMA.equals(geschlechtChar))
            return (GESCHLECHT_STRING_FIRMA);
        else
            return (GESCHLECHT_STRING_UNBEKANNT);
    }
  
}
