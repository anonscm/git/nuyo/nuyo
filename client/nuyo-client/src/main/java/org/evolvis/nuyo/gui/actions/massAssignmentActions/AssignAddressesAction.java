package org.evolvis.nuyo.gui.actions.massAssignmentActions;

import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;


public class AssignAddressesAction extends AssignOrDeassignAddressesAction {

	private static Logger logger = Logger.getLogger(AssignAddressesAction.class.getName());

	protected CategoryTree actionPerformedImpl() {

		List addressPks = ApplicationServices.getInstance().getActionManager()
				.getAddresses().getPkList();

		if (addressPks == null || addressPks.size() == 0){
			logger.warning(Messages.getString("AssignOrDeassignAddresses_No_Addresses_Warning"));
			return null;
		}

		List allCategories = null;
		List accessableCategories = null;

		try {
			allCategories = ApplicationServices.getInstance()
					.getCurrentDatabase()
					.getCategoriesWithCategoryRightsOR(
							false, false, true, false, false, false, true, false, false);
			accessableCategories = ApplicationServices.getInstance()
					.getCurrentDatabase()
					.getAssociatedCategoriesForAddressSelection(ApplicationServices.getInstance().getActionManager().getAddresses().getPkList(), false,false,false,false,false,false,false,false,false);;

		} catch (ContactDBException e1) {
			logger.warning(Messages.getString("AssignOrDeassignAddresses_Error_Warning"));
		}

		return CategoryTree.createAddCategoryTree(addressPks.size(),
				allCategories, accessableCategories,
				new AssignAddressesHandler(addressPks, true));
	}
}
