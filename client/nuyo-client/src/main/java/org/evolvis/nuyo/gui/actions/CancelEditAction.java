package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Aborts the edit mode.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class CancelEditAction extends AbstractGUIAction {
    private static final long serialVersionUID = -6487239901378551397L;
    private static final TarentLogger logger = new TarentLogger(CancelEditAction.class);
    private ControlListener controlListener; 
    private GUIListener guiListener;
    private Runnable executor;

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null) {
            //IMPORTANT: immediate AWT-Thread release
            SwingUtilities.invokeLater(executor);
        } else logger.warning(getClass().getName() + " not initialized");
    }
    
    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        controlListener = ApplicationServices.getInstance().getMainFrame();
        executor = new Runnable(){// define reusable code:
            public void run() {
                controlListener.setDisplayedAddress(null);
                guiListener.userRequestNotEditable(GUIListener.DO_DISCARD);
                guiListener.userRequestFirstAddress();
            }
        };
    }
}
