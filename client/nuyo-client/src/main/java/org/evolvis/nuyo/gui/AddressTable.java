package org.evolvis.nuyo.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.octopus.AddressesImpl;

import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.entity.AsyncEntityListImpl;
import de.tarent.commons.ui.ColumnDescription;
import de.tarent.commons.ui.EntityListTableModel;

public class AddressTable extends MouseAdapter implements ListSelectionListener, AddressTableView {

    private static final Logger logger = Logger.getLogger(AddressTable.class.getName());

    public static final String SELECTED_INDEX_KEY = "selectedIndex";

    AsyncEntityListImpl list;
    JTable table;
    ThreadQ tq = new ThreadQ();
    
    List selectionListeners;

    int lastSelection;

    ColumnDescription[] columnDescriptions;

    public AddressTable(AsyncEntityListImpl pList, TableModel tm, ColumnDescription[] columnDescs) {
    	
    	list = pList;
    	columnDescriptions = columnDescs;
    	
    	selectionListeners = new ArrayList();
    	
        table = new JTable() {
                public void tableChanged(TableModelEvent e) {                    
                    super.tableChanged(e);
                    // restoring the selection
                    if (getSelectionModel() != null) {
                        if (list != null && lastSelection < list.getSize()) {
                            getSelectionModel().setSelectionInterval(lastSelection, lastSelection);
                            // if re restore the selection here we are always junpimg back while scrolling
                            //scrollRectToVisible(getCellRect(lastSelection, 0, false));
                        }
                        else {
                            getSelectionModel().setSelectionInterval(0, 0);
                            scrollRectToVisible(getCellRect(0, 0, false));
                        }
                    }
                }
            };
        table.getSelectionModel().addListSelectionListener(this);
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getTableHeader().addMouseListener(this);
        if(tm != null) table.setModel(tm);
        tq.start();
    }

    public AddressTable(AsyncEntityListImpl list, TableModel tm) {
    	this(list, tm, new ColumnDescription[]{
                new ColumnDescription(Address.PROPERTY_VORNAME.getKey(), Address.PROPERTY_VORNAME.getLabel(), String.class),
                new ColumnDescription(Address.PROPERTY_NACHNAME.getKey(), Address.PROPERTY_NACHNAME.getLabel(), String.class),
                new ColumnDescription(Address.PROPERTY_PLZ.getKey(), Address.PROPERTY_PLZ.getLabel(), String.class),
                new ColumnDescription(Address.PROPERTY_ORT.getKey(), Address.PROPERTY_ORT.getLabel(), String.class),
                new ColumnDescription(Address.PROPERTY_INSTITUTION.getKey(), Address.PROPERTY_INSTITUTION.getLabel(), String.class),
    	});
    }
    
    public AddressTable(ColumnDescription[] columnDescs)
    {
    	this(null, null, columnDescs);
    }
    
    public AddressTable()
    {
    	this(null, null);
    }
    
    public void addListSelectionListener(ListSelectionListener listener)
    {
    	selectionListeners.add(listener);
    }
    
    protected void fireSelectionEvent(int index)
    {
    	Iterator it = selectionListeners.iterator();
    	
    	while(it.hasNext())
    		((ListSelectionListener)it.next()).valueChanged(new ListSelectionEvent(this, index, index, false));
    }

    public void mouseClicked(MouseEvent e) {
        TableColumnModel colModel = table.getColumnModel();
        int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
        int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
        if (modelIndex < 0)
            return;
        AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
        String key = columnDescriptions[modelIndex].getAttributeName();        
        
        if (key != null && key.equals(alp.getSortField())) {
            alp.toggleSortDirection();
        } else {
            alp.setSortField(key);
            alp.setSortDirection(ListFilter.DIRECTION_ASC);
        }
        ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
    }
    
    /**
     * TODO: Addresszugriff: handle the case, where NonPersistentAddresses are supplied
     */
    public void setAddressList(Addresses addressList) {
        if (addressList != null) {
            list = (AddressesImpl)addressList;
            TableModel tm = new EntityListTableModel((AddressesImpl)addressList, columnDescriptions);
            table.setModel(tm);
        } else {
            table.setModel(null);
        }
    }


    /**
     * Sets the index of the selected address
     */
    public void setSelectedIndex(int newSelectedIndex) {
        table.getSelectionModel().setSelectionInterval(newSelectedIndex, newSelectedIndex);
        table.scrollRectToVisible(table.getCellRect(newSelectedIndex, 0, false));
    }

    /**
     * Returns the index of the selected address
     */
    public int getSelectedIndex() {
        return lastSelection;
    }

    
    public JComponent getTableComponent() {
        return new JScrollPane(table);
    }
    
    public JTable getTable()
    {
    	return table;
    }
    
    public void valueChanged(ListSelectionEvent e) {
        // sleep for a time, to avoid direct loading while scrolling through the table
        final int selected = table.getSelectedRow();
        if (selected == -1 || list.getSize() <= 0)
            return;
        
        lastSelection = selected;
        tq.enq(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(80);
                        if (selected == lastSelection) {
                            int i = 0;
                            Object entity = null;
                            while (null == (entity = (Object)list.getEntityAt(selected))) {
                                Thread.sleep(50);
                                if (i++ > 60) { // 3 sec
                                    logger.info("waiting for entity at position '"+selected+"' timed out.");
                                    return;
                                }
                            }
                            // notify interested classes about new selection
                            fireSelectionEvent(selected);
                        }
                    } catch (InterruptedException ie) {
                        // ignore
                    } catch (Exception e) {
                        logger.log(Level.WARNING, "Error while selecting item", e);
                    }
                }});
    }

    public class ThreadQ extends Thread {
        Runnable r = null;
        Object monitor = "mon";

        public void enq(Runnable r) {
            synchronized (monitor) {
                this.r = r;
            }
        }
        
        public synchronized void run() {
            while (true) {
                try {
                    Runnable run = null;
                    synchronized (monitor) {
                        if (r != null) {
                            run = r;
                            r = null;
                        }
                    }
                    if (run != null)
                        run.run();
                    Thread.sleep(5);                    
                } catch (InterruptedException ie) {
                    // ignore
                }
            }
        }
    }
    

    // interface AddressTableView -- begin
    /**
     * Method getDragComponent. liefert das JComponent 
     * das als DragComponent benutzt werden soll  
     */    
    public JComponent getDragComponent() { 
        return getTableComponent();
    }
  
    /**
     * Method discardTableBuffer. erkl�rt alle Daten 
     * in der Tabelle als ung�ltig
     */    
    public void discardTableBuffer() {
        // nop
    }
  
    /**
     * Method forceRefresh. erzwingt einen vollst�ndigen 
     * Refresh der Tabelle
     */    
    public void forceRefresh() {
        // nop
    }
  
    /**
     * Method setNavigationCounter. Erm�glicht das Setzen des aktuellen
     * Datensatzes und der Anzahl angezeigter Datens�tze (Navigationsleiste)
     * @param currentindex (Der Index des aktuell angezeigten Datensatzes)
     * @param numentries (Die Anzahl aller zur Navigation bereitstehenden Datens�tze)
     */
    public void setNavigationCounter(int currentindex, int numentries) {
        // nop
    }

  
    /**
     * TODO: Addresszugriff: Implement
     * Method selectAddress. Erm�glicht das Selektieren 
     * einer Adresse
     * @param index (Der Index des zu selektierenden Datensatzes)
     */  
    public void selectAddress(int index) {
        table.getSelectionModel().setSelectionInterval(index, index);
    }

  
    /**
     * Method addressChanged. teilt der Tabelle mit das 
     * sich eine Adresse ge�ndert hat
     * @param address (Die Adresse die sich ge�ndert hat)
     * @param index (Der Index des Datensatzes)
     */  
    public void addressChanged(Address address, int index) {
        // nop
    }

    /**
     * Method setColumnPositions. setzt die Spaltenpositionen
     * @param posistr (als String codierte Spaltenpositionen)
     */      
    public void setColumnPositions(String posistr) {
        // nop
    }
  
  
    /**
     * Method getColumnPositions. liefert die Spaltenpositionen
     * @return posistr (als String codierte Spaltenpositionen)
     */        
    public String getColumnPositions() {
        // nop
        return null;
    }
  
  
    /**
     * Method isPreviewRowLoaded. Ist der Preview f�r eine spezielle Zeile schon geladen?
     * @return true wenn bereits geladen
     */        
    public boolean isPreviewRowLoaded(int row) {
        // nop
        return true;
    }
  
  
    /**
     * Method isPreviewSortedAscending. Ist der Preview aufsteigend sortiert?
     * @return true wenn aufsteigend (A->Z) geladen (ist Default)
     */        
    public boolean isPreviewSortedAscending() {
        // nop
        return true;
    }

    /**
     * Method isTableReady. Ist die Tabelle g�ltig?
     * @return true wenn g�ltig
     */        
    public boolean isTableReady() {
        // nop
        return true;
    }
    
    /**
     * Method getNumberOfRows. Ermittelt die Anzahl der Preview-Zeilen
     * @return die Anzahl der Preview-Zeilen
     */        
    public int getNumberOfRows() {
    	if(list != null)
    		return list.getSize();
    	return 0;
    }

    // interface AddressTableView -- end


}