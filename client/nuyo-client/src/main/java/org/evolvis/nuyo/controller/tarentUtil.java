/*
 * Created on 07.10.2004
 *
 */
package org.evolvis.nuyo.controller;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author niko
 *
 */
public class tarentUtil
{
  
  
  public static URL toURL(String filename)
  {
    if (!(filename.startsWith("file:")))
    {     
      if (!(filename.startsWith("///")))
      {
        if ((filename.startsWith("//")))
        {
          filename = "/" + filename; 
        }
        else if ((filename.startsWith("/")))
        {
          filename = "//" + filename; 
        }      
      }
      
      filename = "file:" + filename;
    }    
    try
    {
      return new URL(filename);
    }
    catch (MalformedURLException e)
    {
      return null;
    }
  }

  
  
  public static String toURLString(String filename)
  {
    if (!(filename.startsWith("file:")))
    {     
      if (!(filename.startsWith("///")))
      {
        if ((filename.startsWith("//")))
        {
          filename = "/" + filename; 
        }
        else if ((filename.startsWith("/")))
        {
          filename = "//" + filename; 
        }      
      }
      
      filename = "file:" + filename;
    }    
    try
    {
      URL url = new URL(filename);
      return url.toExternalForm();
    }
    catch (MalformedURLException e)
    {
      return null;
    }
  }
  
  
  
}
