/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class CopyToSenderAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2273652792801206171L;
	
	private boolean isSelected = false;
	
	protected void init()
	{
		super.init();
		setSelected(isSelected);
	}

	public void actionPerformed(ActionEvent e)
	{
		isSelected = !isSelected; 
		setSelected(isSelected);
		DispatchEMailDialog.getInstance().setCopyToSender(isSelected);
	}
}
