/* $Id: Database.java,v 1.31 2007/08/30 16:10:29 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.db.veto.VetoableAction;

import de.tarent.octopus.client.remote.OctopusRemoteConnection;

/**
 * Diese Klasse stellt eine Datenquelle dar. Eine Applikation braucht gew�hnlich
 * nur eine Instanz hiervon.
 * 
 * @author mikel
 */
public abstract class Database {
    /**
     * Dieser Logger soll f�r Ausgaben im Datenbankkontext benutzt werden. Dieser
     * Logger hat als Parent zumindest den Logger des Konfigurationsobjekts.
     * 
     * @see org.evolvis.nuyo.config.Config
     */
    public final static Logger logger = Logger.getLogger(Database.class.getPackage().getName());

    /** Initialization method that takes a pre-configured database connection.
     * 
     * <p>This method <em>MUST</em> be called before any of the other
     * methods can be used.</p>
     */
    abstract public void init(OctopusRemoteConnection connection);
    
    /**
     * Diese Methode liefert die Version der zugrundeliegenden Datenquelle.
     * 
     * @return Datenquellenversion
     * @throws ContactDBException
     */
    abstract public String getVersion(String clientVersion) throws ContactDBException;

    /**
     * Diese Methode erledigt das initiale Abholen der Kategorien.
     * @throws ContactDBException
     */
    abstract public void getInitDatas() throws ContactDBException;
    
    /**
     * Returns a management class for address objects.
     */
    abstract public AddressClientDAO getAddressDAO();
    
    /**
     * Returns a management class for category objects.
     */
    abstract public CategoryClientDAO getCategoryDAO();

    /** 
     * Diese Methode liefert ein Feature einer Datenquelle zur�ck, zum Beispiel ob eine Unterkategorie einen Kommentar hat oder nicht
     * @return feature
     */
    abstract public Object getFeature(String feature);
    
    /**
     * Diese Methode liefert eine anzeigbare Darstellung der
     * zugrundeliegenden Datenquelle.
     * 
     * @return Datenquellendarstellung
     * @throws ContactDBException
     */
    abstract public String getDisplaySource() throws ContactDBException;

    /**
     * Diese Methode berprft, ob eine intendierte Aktion grunds�tzlich
     * erlaubt ist, und liefert ansonsten ein entsprechendes Veto.
     *  
     * @param action zu berprfende Aktion
     * @return <code>null</code>, falls die Aktion grunds�tzlich erlaubt
     *  ist, sonst ein Veto, dass Beschr�nkungen liefert.
     * @throws ContactDBException
     */
    abstract public Veto checkForVeto(VetoableAction action) throws ContactDBException;

    /**
     * Liefert ein Address-Objekt, das beim commit eine neue Adresse anlegt.
     * 
     * @return ein Address-Objekt, das nach Fllen und commit eine neue Adresse
     * in der Datenbank anlegt.
     */
    abstract public Address createAddress() throws ContactDBException;

    /**
     * Liefert ein Address-Objekt zu der bergebenen Adressnummer mit Zusatzdaten
     * aus der aktuellen Kategorie.
     * 
     * @param addrNr Die Adressnummer, zu der das Address-Objekt geholt werden soll.
     * @return das Address-Objekt zu der Adressnummer oder null, wenn es solch eines
     *  nicht gibt.
     */
    abstract public Address getAddress(int addrNr) throws ContactDBException;

    /**
     * Diese Methode liefert die Anzahl der Adressen im Datenbestand.
     * 
     * @return Gesamtanzahl Adressen im Datenbestand.
     * @throws ContactDBException
     */
    abstract public Integer getAddressCount() throws ContactDBException;

    /**
     * Diese Methode liefert eine Addresses-Instanz auf Basis der Einschr�nkungen,
     * die ber Kategorie, User und AllgemeineAdressen gegeben sind.
     * 
     * @return Sammlung der Adressen, die den derzeitigen Filtern gengen.
     * @throws ContactDBException
     */
    abstract public Addresses getChosenAddresses() throws ContactDBException;

    /**
     * Diese Methode liefert eine Addresses-Instanz auf Basis der Einschr�nkungen,
     * die ber den aktuellen Benutzer und als Parameter gegeben sind.<br>
     * Derzeit werden nicht die generischen Filter, sondern nur die Suchfilter beachtet.
     * 
     * @param filters eine Sammlung von Filtern (z.B. Kategorien, Unterkategorien, Versandauftr�ge,...)
     * @param criteria eine Map von Suchfeldern und -Strings; die Schlssel sind die Feldkonstanten aus
     *  {@link Address}, also STD_FIELD_* und CAT_FIELD_*, und zus�tzlich "" f�r die Suche in allen Feldern;
     *  die Such-Strings sind die zu suchenden Feldinhalte, wobei '*' als Jokerzeichen erlaubt ist.
     * @return Sammlung der Adressen, die den Filtern und Suchkriterien gengen.
     * @throws ContactDBException
     */
    abstract public Addresses getAddresses(Collection filters, Map criteria) throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der Anreden; der Schlssel in der Map ist der
     * Schlssel der Anrede.
     * 
     * @return Map der Anreden
     * @throws ContactDBException
     */
    abstract public SortedMap getAnreden() throws ContactDBException;
    
    /**
     * Diese Methode erledigt die Massenzuweisung von Adressen zu Unterkategorien 
     * sowie das Entfernen von Zuweisungen zu Unterkategorien.
     * @param addressesPkList Liste mit Pks von Adressen, bei denen die Aenderungen der Zuordnungen durchgefuehrt werden sollen.
     * @param addSubcategories Liste mit Unterkategorie IDs, zu denen die Adressen hinzugefuegt werden sollen
     * @param removeSubcategories Liste mit Unterkategorie IDs bei denen die Zuweisung aufgehoben werden soll.
     */
    abstract public Object changeAddressAssignments(List addressesPkList, List addSubcategories, List removeSubcategories) throws ContactDBException;
    
    /**
     * Diese Methode liefert eine Map der Bundesl�nder; der Schlssel in der Map ist
     * der Schlssel des Bundeslandes.
     * 
     * @return Map der Bundesl�nder
     * @throws ContactDBException
     */
    abstract public SortedMap getBundeslaender() throws ContactDBException;

    /**
     * Diese Methode liefert zu einer PLZ das zugeh�rige Bundesland.
     * 
     * @param plz die Postleitzahl, die zu testen ist.
     * @return der Name des Bundeslandes, zu dem die PLZ geh�rt,
     *  oder <code>null</code>, falls es nicht feststellbar ist.
     * @throws ContactDBException
     */
    abstract public String getBundeslandForPLZ(String plz) throws ContactDBException;

    /**
     * Diese Methode liefert zu einer PLZ die zugeh�rige Stadt.
     *  
     * @param plz die Postleitzahl, die zu testen ist.
     * @return der Name der Stadt, zu dem die PLZ geh�rt,
     *  oder <code>null</code>, falls es nicht feststellbar ist.
     * @throws ContactDBException
     */
    abstract public String getCityForPLZ(String plz) throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der L�nder; der Schlssel in der Map ist der
     * Schlssel des Landes.
     * 
     * @return Map der L�nder
     * @throws ContactDBException
     */
    abstract public SortedMap getLaender() throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der LKZen; der Schlssel in der Map ist der
     * Schlssel der LKZ.
     * 
     * @return Map der LKZen
     * @throws ContactDBException
     */
    abstract public SortedMap getLKZen() throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der Unterkategorien in der angegebenen Katgorie;
     * der Schlssel in der Map ist der Schlssel der Unterkategorie.
     * 
     * @param kategorie wenn <code>null</code>, so ist die aktuelle Kategorie gemeint.
     * @return Map von MailingList-Instanzen, die Unterkategorie der aktuellen Kategorie
     * @throws ContactDBException
     */
    abstract public SortedMap getVerteiler(String kategorie) throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der Kategorien, die dem angegebenen Benutzer
     * zur Verfgung stehen; der Schlssel in der Map ist der Schlssel der Kategorie.
     *
     * Von Sebastian:
     * Ich habe das Interface auf Map ge�ndert! Die Sortierung der Verteiler in dieser Map ist egal!!
     * f�r eine Sortierte Liste gibt es die Funktion getCategoriesList()
     * 
     * @param user der Benutzer, dessen Gruppen abgefragt werden; wenn der
     *  Eintrag <code>null</code> ist, wird der aktuell eingelogte benutzt.
     * @return Map der Kategorien
     * @throws ContactDBException
     * @see #getAllCategories()
     */
    abstract public Map getCategories(String user) throws ContactDBException;

    /**
     * Liste aller Kategorien, die die User sehen darf.
     */
    abstract public List getCategoriesList() throws ContactDBException;

    
    /**
	 * 
	 * @param read
	 * @param edit
	 * @param add
	 * @param remove
	 * @param addSubCat
	 * @param removeSubCat
	 * @param structure
	 * @param grant
	 * @param includeTrash
	 * @return
	 */
	abstract public List getCategoriesWithCategoryRightsOR(boolean read, boolean edit, boolean add, boolean remove, 
			boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeTrash) throws ContactDBException;
	
	/**
	 * 
	 * @param read
	 * @param edit
	 * @param add
	 * @param remove
	 * @param addSubCat
	 * @param removeSubCat
	 * @param structure
	 * @param grant
	 * @param includeTrash
	 * @return
	 */
	abstract public List getCategoriesWithCategoryRightsAND(boolean read, boolean edit, boolean add, boolean remove, 
			boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeTrash) throws ContactDBException;
	
		
		
    /**
     * Liste aller Kategorien, auf der ein User Berechtigungen verwalten darf 
     */
    abstract public List getGrantableCategoriesList() throws ContactDBException;

    
    /**
     * @return category objects of all categories that are connected to the selected address
     */
    abstract public List getAssociatedCategoriesForSelectedAddress(Integer addressPk) throws ContactDBException;
    
    
    /**
     * @return category objects of all categories that are connected to the selected address filtered by filterList
     */
    abstract public List getAssociatedCategoriesForSelectedAddress(Integer addressPk, List filterList, boolean includeVirtual) throws ContactDBException;
    /**
     * 
     * @param edit 
     * @param add
     * @param remove
     * @param addSubCat
     * @param removeSubCat
     * @param structure
     * @param grant
     * @param includeVirtual inlude or exlude virtual categories
	 * @return list of category objects of all categories that are connected to the selected addresses and have proper rights
     * @throws ContactDBException
     */
    
    abstract public List getAssociatedCategoriesForAddressSelection(List addressPks, boolean read, boolean edit, boolean add, boolean remove, boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeVirtual) throws ContactDBException;
    
    /**
     * 
     * @param filterList
     * @return list of category objects of all categories that are connected to the selected addresses and have proper rights
     * @throws ContactDBException
     */
    abstract public List getAssociatedCategoriesForAddressSelection(List addressPks, List filterList, boolean includeVirtual) throws ContactDBException;
    
    /**
     * Diese Methode liefert eine Map aller Kategorien; der Schlssel in der
     * Map ist der Schlssel der Kategorie.
     *
     * Von Sebastian:
     * Ich habe das Interface auf Map ge�ndert! Die Sortierung der Verteiler in dieser Map ist egal!!
     * f�r eine Sortierte Liste gibt es die Funktion getcategoriesList()
     *
     * 
     * @return Map der Kategorien
     * @throws ContactDBException
     */
    abstract public Map getAllCategories() throws ContactDBException;


    /**
     * Liefert die Kategorie zu einem Schlssel zur�ck
     */
    abstract public Category getCategory(String key) throws ContactDBException;

    /**
     * TODO: Addresszugriff: remove this
     * Dieses Attribut enth�lt die aktuelle Kategorie.
     * @return Kategorienschlssel
     */
    public String getCategory() {
        return verteilergruppe;
    }

    /**
     * Diese Methode liefert zu dem angegebenen Benutzer die Standard-
     * Kategorie.
     * 
     * @param user der Benutzer, dessen Standardgruppe abgefragt wird; wenn der
     *  Eintrag <code>null</code> ist, wird der aktuell eingelogte benutzt.
     * @return der Schlssel der Standardkategorie; falls keine
     *  eingestellt ist, wird <code>null</code> geliefert.
     * @throws ContactDBException
     */
    abstract public String getStandardCategory(String user) throws ContactDBException;

    abstract public Category getVirtualRootCategory() throws ContactDBException;
    
    abstract public Category getTrashCategory() throws ContactDBException;
    
    /**
     * Diese Methode liefert die Kategorie der allgemeinen Adressen.
     * 
     * @return der Schlssel der allgemeinen Kategorie; falls keine
     *  eingestellt ist, wird <code>null</code> geliefert.
     * @throws ContactDBException
     */
    abstract public String getAllgemeineCategory() throws ContactDBException;

    /**
     * Dieses Attribut enth�lt die aktuelle Kategorie.
     * 
     * @param category Der zu setzende Kategorieschlssel. Zuvor ist
     * der Benutzer zu setzen.
     * @see #setUser(String)
     */
    public void setVerteilergruppe(String category) throws ContactDBException {
        this.verteilergruppe = category;
    }


    /**
     * Diese Methode besagt, ob bei einer bestehenden Verteilerauswahl
     * diese als Schnittmenge der ausgew�hlten Verteiler zu verstehen ist.
     * 
     * @return true, falls die auchgew�hlten Adressen Schnittmenge der
     *  ausgew�hlten Verteiler seien sollen.
     */
    public boolean isVerteilerSelectionIntersection() {
        return verteilerSelectionIntersection;
    }

    /**
     * Diese Methode legt fest, ob bei einer bestehenden Verteilerauswahl
     * diese als Schnittmenge der ausgew�hlten Verteiler zu verstehen ist.
     * 
     * @param verteilerSelectionIntersection true, falls die auchgew�hlten
     *  Adressen Schnittmenge der ausgew�hlten Verteiler seien sollen.
     */
    public void setVerteilerSelectionIntersection(boolean verteilerSelectionIntersection) {
        this.verteilerSelectionIntersection = verteilerSelectionIntersection;
    }

    /**
     * Diese Methode erzeugt einen neuen Versandauftrag f�r den aktuellen
     * Benutzer bei der aktuellen Auswahl.
     */
    public abstract MailBatch createVersandAuftrag(Addresses addresses) throws ContactDBException;

    /**
     * Diese Methode liefert Versandauftr�ge des aktuellen Benutzers.
     * 
     * @return eine Liste der bestehenden Versandauftr�ge, absteigend nach
     *  Datum sortiert.
     */
    public abstract List getVersandAuftraege() throws ContactDBException;

    /**
     * Diese Methode liefert zu dem angegebenen Benutzer (Default: der
     * eingelogte Benutzer) eine User-Instanz.
     * 
     * @param userLogin Login-Name des Benutzers; <code>null</code> referenziert den
     *  aktuell eingelogten Benutzer.
     * @return User-Instanz zum angegebenen Benutzer oder <code>null</code>, wenn
     *  es keinen Benutzer zum angegebenen Login gibt.
     * @throws ContactDBException
     */
    public abstract User getUser(String userLogin) throws ContactDBException;
    

    public abstract User getUser(String userLogin, boolean enableChaching) throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der Benutzer-Logins auf zugeh�rige
     * {@link User}-Instanzen.
     * 
     * @return Map Benutzerlogins auf User-Instanzen.
     * @throws ContactDBException
     */
    public abstract Map getUsers() throws ContactDBException;
    
    /**
     * 
     * Diese Methode liefert alle Benutzergruppen zur�ck.
     * 
     * @return Collection - Liste aller Benutzergruppen
     * @throws ContactDBException
     */
    public abstract Collection getUserGroups() throws ContactDBException;
    
    /**
     * Dieses Attribut enth�lt den Benutzer.
     * 
     * @return der Benutzer, als der die Zugriffe auf den Datenbestand
     *  ausgefhrt werden sollen.
     * @see #getPassword()
     */
    public String getUser() {
        return user;
    }

    /**
     * Dieses Attribut enth�lt den Benutzer. Der Benutzer ist vor anderen
     * Filtern zu setzen.
     * 
     * @param user Der zu setzende Benutzer.
     * @see #setPassword(String)
     */
    public void setUser(String user) throws ContactDBException {
        this.user = user;
    }

    /**
     * Dieses Attribut enth�lt das Passwort des Benutzers.
     * 
     * @return das Passwort, mit dem die Zugriffe auf den Datenbestand
     *  ausgefhrt werden sollen.
     * @see #getUser()
     */
    public String getPassword() {
        return password;
    }

    /**
     * Dieses Attribut enth�lt das Passwort des Benutzers. Das Passwort ist
     * nach dem Benutzer zu setzen, aber vor jeglichem Zugriff auf den
     * Datenbestand.
     * 
     * @param password Das zu setzende Passwort.
     * @see #setUser(String)
     */
    public void setPassword(String password) throws ContactDBException {
        this.password = password;
    }



    /**
     * St�sst die Authentifizierung an und prft ihre Gltigkeit, 
     * wenn dies vom darunterliegenden System untersttzt wird.
     * 
     */
    public void login() throws ContactDBException {
        // Do nothing here
    }


    /**
     * Nimmt eine evtl. bestehende Session dieses Rechners zum Server wieder auf.
     * @return true, wenn eine gltige Session besteht, false sonst.
     */
    public boolean continueSession() throws ContactDBException {
        // Do nothing here
        return false;
    }


    /**
     * Liefert, ob ein spezieller Suchfilter (vgl {@link #setSearchParameters(boolean, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String)
     * setSearchParameters()}) benutzt werden soll.
     * 
     * @return das Such-Flag
     */
    public boolean isSearch() {
        return search;
    }

    /**
     * Legt fest, ob ein spezieller Suchfilter (vgl {@link #setSearchParameters(boolean, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String)
     * setSearchParameters()}) benutzt werden soll.
     * 
     * @param search Das zu setzende Such-Flag.
     */
    public void setSearch(boolean search) {
        this.search = search;
    }

    /**
     * Mit dieser Methode werden Suchparameter gesetzt. Ob sie genutzt werden, h�ngt
     * vom SuchFlag ab.
     * 
     * @see #setSearch(boolean)
     * @see #isSearch()
     */
    public void setSearchParameters(
        boolean matchcode,
		boolean followup,
        String herrnFrau,
        String nachname,
        String vorname,
        String titel,
        String akadTitel,
        String namenszusatz,
        String institution,
        String strasse,
        String plz,
        String ort,
        String land,
        String lkz,
        String bundesland,
        String tel_dienstlich,
        String tel_privat,
        String fax_dienstlich,
        String fax_privat,
        String email,
		Date followupdate)
    {
        this.matchcode = matchcode;
        this.followup = followup;
        this.herrnFrau = herrnFrau;
        this.nachname = nachname;
        this.vorname = vorname;
        this.titel = titel;
        this.akadTitel = akadTitel;
        this.namenszusatz = namenszusatz;
        this.institution = institution;
        this.strasse = strasse;
        this.plz = plz;
        this.ort = ort;
        this.land = land;
        this.lkz = lkz;
        this.bundesland = bundesland;

        this.tel_dienstlich = tel_dienstlich;
        this.tel_privat = tel_privat;
        this.fax_dienstlich = fax_dienstlich;
        this.fax_privat = fax_privat;
        this.email = email;
        this.followupdate = followupdate;
    }

    /**
     * Diese Methode liefert den Terminplan eines Benutzers.
     * 
     * @param userId Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Terminplan des Benutzers oder <code>null</code>, falls
     *  diese Funktionalit�t nicht vorliegt.
     */
    abstract public Schedule getSchedule(String userId) throws ContactDBException;

    /**
     * Diese Methode erzeugt einen Historieneintrag zu einem Kontakt-Ereignis.  
     * und legt diesen direkt an.
     * 
     * @param address externer Teilnehmer 
     * @param user interner Teilnehmer (falls null, so wird der angemeldete Benutzer angenommen)
     * @param contactCategory Kontaktkategorie, vergleiche Konstanten Contact.CATEGORY_*
     * @param contactChannel Kontaktkanal, vergleiche Konstanten Contact.CHANNEL_*
     * @param linktype Typ des Links, vergleiche Konstanten Contact.LINKTYPE_*
     * @param link Link am Kontaktereignis
     * @param inbound Flag, ob einkommendes (von aussen initiiertes) Kontaktereignis
     * @param subject Betreff zum Kontaktereignis
     * @param note Notiz zum Kontaktereignis
     * @param structInfo strukturierte Information zum Kontaktereignis
     * @param start Start (UTC) des Kontaktereignisses
     * @param duration Dauer des Kontaktereignisses in Sekunden
     * @return das neue Kontakt-Ereignis
     * @throws ContactDBException
     */
    abstract public Contact createContact(Address address, User user, int contactCategory, int contactChannel, int linktype, Object link, boolean inbound, String subject, String note, String structInfo, Date start, int duration) throws ContactDBException;
    
    /**
     * Diese Methode erzeugt einen Historieneintrag zu einem Kontakt-Ereignis.
     * Ob er in der DB angelegt wird kann ber das create-flag gesteuert werden.
     * 
     * @param address externer Teilnehmer 
     * @param user interner Teilnehmer (falls null, so wird der angemeldete Benutzer angenommen)
     * @param contactCategory Kontaktkategorie, vergleiche Konstanten Contact.CATEGORY_*
     * @param contactChannel Kontaktkanal, vergleiche Konstanten Contact.CHANNEL_*
     * @param linktype Typ des Links, vergleiche Konstanten Contact.LINKTYPE_*
     * @param link Link am Kontaktereignis
     * @param inbound Flag, ob einkommendes (von aussen initiiertes) Kontaktereignis
     * @param subject Betreff zum Kontaktereignis
     * @param note Notiz zum Kontaktereignis
     * @param structInfo strukturierte Information zum Kontaktereignis
     * @param start Start (UTC) des Kontaktereignisses
     * @param duration Dauer des Kontaktereignisses in Sekunden
     * @param create true, wenn das angelegte Objekt direkt in die DB geschrieben werden soll.
     * @return das neue Kontakt-Ereignis
     * @throws ContactDBException
     */
    abstract public Contact createContact(Address address, User user, int contactCategory, int contactChannel, int linktype, Object link, boolean inbound, String subject, String note, String structInfo, Date start, int duration, boolean create) throws ContactDBException;    

    /**
     * Speichert eine Liste von Contact Objekten in der DB
     *
     * @param Liste von Contact-Objekten
     */
    abstract public void commitContactList(Contact[] contacts) throws ContactDBException;

    /*
     * administrative Methoden
     */

    /**
     * Diese Methode erzeugt eine Kategorie.
     * 
     * @param key der Kategorieschlssel.
     * @param name der Kategorieklartext.
     * @param description die Kategorienbeschreibung.
     */
    abstract public void createCategory(String key, String name, String description) throws ContactDBException;

    /**
     * Diese Methode editiert eine Kategorie.
     * 
     * @param key der Kategorieschlssel.
     * @param name der Kategorieklartext.
     * @param description die Kategorienbeschreibung.
     */
    abstract public void editCategory(String key, String name, String description) throws ContactDBException;

    /**
     * Diese Methode l�scht eine Kategorie.
     * 
     * @param key der Kategorieschlssel.
     */
    abstract public void deleteCategory(String key) throws ContactDBException;
    
    /**
     * Diese Methode l�dt die Kategorielisten
     * @throws ContactDBException
     */
    abstract public void loadCategories() throws ContactDBException;
    
    /**
     * Diese Methode l�dt die Kategorieliste neu vom Server 
     * @throws ContactDBException
     */
    
    abstract public void reloadCategories() throws ContactDBException;
    
     /**
     * Diese Methode erzeugt eine Unterkategorie.
     * 
     * @param category die Kategorie, in der die Unterkategorie
     *  erzeugt werden soll.
     * @param key der Unterkategorieschlssel.
     * @param name1 der Unterkategorieklartext Teil 1.
     * @param name2 der Unterkategorieklartext Teil 2.
     * @param name3 der Unterkategorieklartext Teil 3.
     *  Dieser Parameter wird nur benutzt, wenn das {@link #getFeature(String) Feature}
     *  "verteiler.kommentar" von der Datenzugriffsschicht untersttzt wird.
     */
    abstract public void createVerteiler(Integer category, String name1, String name2)
        throws ContactDBException;

    /**
     * Diese Methode editiert eine Unterkategorie.
     * 
     * @param category die Kategorie, in der die Unterkategorie
     *  liegt.
     * @param key der Unterkategorieschlssel.
     * @param name1 der Unterkategorieklartext Teil 1.
     * @param name2 der Unterkategorieklartext Teil 2.
     * @param name3 der Unterkategorieklartext Teil 3. 
     *  Dieser Parameter wird nur benutzt, wenn das {@link #getFeature(String) Feature}
     *  "verteiler.kommentar" von der Datenzugriffsschicht untersttzt wird.
     */
    abstract public void editVerteiler(String category, String key, String name1, String name2, String name3)
        throws ContactDBException;

    /**
     * Diese Methode l�scht eine Unterkategorie.
     * 
     * @param category die Kategorie, in der die Unterkategorie
     *  gel�scht werden soll.
     * @param key der Unterkategorieschlssel.
     */
    abstract public void deleteVerteiler(String category, String key) throws ContactDBException; 



    /**
     * Diese Methode erzeugt eine neue Benutzergruppe mit Name, Beschreibung und Manager.
     * 
     * @param name Name der Gruppe
     * @param description Beschreibung der Gruppe
     * @param shortname Kurzname der Gruppe
     * @return Benutzergruppe
     */
    abstract public UserGroup createUserGroup(String name, String description, String shortname, int globalroleID) throws ContactDBException;
    
    /**
     * Diese Methode erzeugt einen neuen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     * @param password das Passwort
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param special legt fest, ob der Benutzer zur Gruppe der Standardbenutzer
     *  oder der besonderen Benutzer geh�ren soll.
     * @param admin legt fest, ob der Benutzer Administrator seien soll.
     */
    abstract public void createUser(String userId, String password, String lastName, String firstName) throws ContactDBException;

    /**
     * Diese Methode �ndert einen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     * @param password das Passwort
     * @param lastName der Nachname des Benutzers.
     * @param firstName der Vorname des Benutzers.
     * @param email, emailadresse des users
     */
    abstract public void changeUser(String userId, String password, String lastName, String firstName, String email) throws ContactDBException;

    /**
     * Diese Methode l�scht einen Benutzer.
     * 
     * @param userId die Benutzerkennung.
     */
    abstract public void deleteUser(String userId) throws ContactDBException;

    /**
     * Diese Methode liefert eine Map der Benutzer des Systems. Schlssel
     * ist die Benutzer-ID, Wert der Benutzername.
     * 
     * @return die Map der Benutzer.
     */
    abstract public SortedMap getUserNames() throws ContactDBException;




  /**
   * Diese Methode liefert den Namen eines Benutzers
   * 
   * @param userID die Benutzerkennung.
   * @return der Name des Benutzers.
   */
  abstract public String getUserName(String userID) throws ContactDBException;


    // Es sollte niemals n�tig sein das User Passwort nochmal anzufragen.
    //   /**
    //    * Diese Methode liefert das Passwort eines Benutzers
    //    * 
    //    * @param userID die Benutzerkennung.
    //    * @return das Passwort des Benutzers.
    //    */
    //   abstract public String getUserPassword(String userID) throws ContactDBException;

  /**
   * Diese Methode liefert das Special-Flag eines Benutzers
   * 
   * @param userID die Benutzerkennung.
   * @return das Special-Flag des Benutzers.
   */
  abstract public boolean isUserSpecial(String userID) throws ContactDBException;

  /**
   * Diese Methode liefert das Admin-Flag eines Benutzers
   * 
   * @param userID die Benutzerkennung.
   * @return das Admin-Flag des Benutzers.
   */
  abstract public boolean isUserAdmin(String userID) throws ContactDBException;




  /**
   * Diese Methode setzt benutzerabh�ngige Parameter.
   * 
   * @param userId Id des Benutzers; wenn sie <code>null</code> ist,
   *  so ist der aktuell angemeldete Benutzer gemeint.
   * @param key Schlssel des Parameters.
   * @param value Wert des Parameters.
   * @return Erfolgsflag
   */
  abstract public boolean setUserParameter(String userId, String key, String value) throws ContactDBException;


  /**
  * Diese Methode setzt den EinladungsStatus.
  * 
  * @param userId
  *            Id des users
  * @param eventId
  *            Id des events
  * @param statusId
  *            Einladungsstatus
  * @return Erfolgsflag
  */
  abstract public void setEventStatus(int userId, int eventId, int statusId) throws ContactDBException;
  /**
   * Diese Methode holt benutzerabh�ngige Parameter.
   * 
   * @param userId Id des Benutzers; wenn sie <code>null</code> ist,
   *  so ist der aktuell angemeldete Benutzer gemeint.
   * @param key Schlssel des Parameters.
   * @return Wert des Parameters.
   */
  abstract public String getUserParameter(String userId, String key) throws ContactDBException;

    /**
     * Diese Methode liefert eine Sammlung vorliegender Duplikatchecks.
     * 
     * @return aktuell vorhandene Duplikatchecks.
     */
    abstract public DupChecks getDupChecks() throws ContactDBException;

    /**
     * Diese Methode liefert den Duplikatcheck mit der bergebenen ID.
     * 
     * @param dupCheckID ID des nachgefragten Checks.
     * @return der nachgefragte Check oder <code>null</code>, falls
     *  es zu der ID keinen Duplikatcheck gibt.
     */
    abstract public DupCheck getDupCheck(String dupCheckID) throws ContactDBException;

    /**
     * Diese Methode erzeugt einen neuen Duplikatcheck.
     * 
     * @return der neue Duplikatcheck, eventuell noch nicht in der Datenbank
     *  verankert.
     */
    abstract public DupCheck createDupCheck();
    
    /*
     * Methoden f�r die Webschnittstelle.
     */

    /**
     * Diese Methode erzeugt zu einer XML-Adress-Suchanfrage eine XML-Ergebnis-Tabelle
     * 
     * @param XML die XML-Suchanfrage
     * @return String XML-Ergebnis-Tabelle
     * @throws ContactDBException bei Datenzugriffsproblemen
     */
    abstract public String getAdresses(String XML) throws ContactDBException;

    /*
     * Diese Methoden dienen der Verwaltung der zur Verfgung stehenden
     * Kommunikationsdatentypen und erweiterten Felder.
     */

    /**
     * Diese Klasse dient der Beschreibung erweiterter Datenfelder.
     */
    public static class FieldDescription {
        /**
         * Der Konstruktor erzeugt aus den bergebenen Daten eine Feldbeschreibung.
         * 
         * @param id Identifikator des Feldes
         * @param displayName Bezeichner des Feldes f�r die Darstellung
         * @param tooltip Tooltip des Feldes f�r die Darstellung
         * @param displayWidth Breite des Feldes f�r die Darstellung
         */
        public FieldDescription(String id, String displayName, String tooltip, int displayWidth) {
            this.id = id;
            this.displayName = displayName;
            this.tooltip = tooltip;
            this.displayWidth = displayWidth;
        }

        /*
         * Getter-Methoden f�r die Attribute
         */
        public String getDisplayName() {
            return displayName;
        }
        public String getId() {
            return id;
        }
        public int getDisplayWidth() {
            return displayWidth;
        }
        public String getTooltip() {
            return tooltip;
        }

        /*
         * Variablen f�r die Attribute
         */
        protected String id = null;
        protected String displayName = null;
        protected String tooltip = null;
        protected int displayWidth = 0;

    }

    /**
     * Diese Methode liefert die Kommunikationsdatentypen, die von der DB-Umsetzung
     * abh�ngen k�nnen, als Abbildung von Identifikatoren auf Feldbeschreibungen
     * vom Typ {@link Database.CommunicationDescription CommunicationDescription}.
     * 
     * @return Abbildung mit den Kommunikationsdatenbeschreibungen.
     */
    abstract public SortedMap getCommunicationFields() throws ContactDBException;
    
    /**
     * Diese Methode liefert die Previewfelder die zur Darstellung der Vorschaudaten
     * benutzt werden k�nnen.
     *  
     * @return Liste der Feldnamen.
     */
    abstract public List getAvailablePreviewRows() throws ContactDBException;
    
    /**
     * Diese Methode berprft ob der Benutzer das geforderte Recht auf den Folder besitzt. 
     *  
     * @return Benutzer hat das Recht oder nicht.
     */
    abstract public boolean userHasRightOnFolder(String userLogin, int folderRight, int folderID) throws ContactDBException;
    
    /**
     * Diese Methode berprft ob der Benutzer das geforderte Recht auf den Folder besitzt. 
     *  
     * @return Benutzer hat das Recht oder nicht.
     */
    abstract public boolean userHasRightOnAddress(String userLogin, int folderRight, int addressID) throws ContactDBException;


    /**
     * Diese Methode berprft ob der Benutzer das Event editieren darf
     *  
     * @return Benutzer hat das Recht oder nicht.
     */
    abstract public boolean mayUserWriteEvent(int eventid) throws ContactDBException;

	/**
     * Diese Methode berprft ob der Benutzer das geforderte Gloale Recht besitzt. 
     *  
     * @return Benutzer hat das Recht oder nicht.
     */
    abstract public boolean userHasGlobalRight(String userLogin, int globalRight) throws ContactDBException;


	/**
	 * Diese Methode holt alle globalen Rollen
	 * 
	 * @return Liste mit Rollen
	 * @throws ContactDBException
	 */
	abstract public List getGlobalRoles() throws ContactDBException;
	
	/**
	 * Diese Methode holt alle globalen Rollen
	 * 
	 * @return Liste mit Rollen
	 * @throws ContactDBException
	 */
	abstract public List getObjectRoles() throws ContactDBException;

	/**
     * Diese Klasse beschreibt einen Eintragstyp der Kommunikationsdaten. 
     */
    public static class CommunicationDescription extends FieldDescription {
        public final static int TYPE_EMAIL = 1;
        public final static int TYPE_FAX = 2;
        public final static int TYPE_PHONE = 4;
        public final static int TYPE_CELLULAR = 12;
        /**
         * Der Konstruktor erzeugt aus den bergebenen Daten eine
         * Kommunikationsdatenbeschreibung.
         * 
         * @param id Identifikator des Kommunikationsdatums
         * @param displayName Bezeichner des Kommunikationsdatums f�r die Darstellung
         * @param tooltip Tooltip des Kommunikationsdatums f�r die Darstellung
         * @param comment Kommentar zum Kommunikationsdatums
         * @param displayWidth Breite des Feldes f�r die Darstellung
         */
        public CommunicationDescription(
            String id,
            String displayName,
            String tooltip,
            String comment,
            int commType,
            int displayWidth) {
            super(id, displayName, tooltip, displayWidth);
            this.comment = comment;
            this.commType = commType;
        }

        /*
         * Getter-Methoden f�r die Attribute
         */
        public String getComment() {
            return comment;
        }

        public int getCommType() {
            return commType;
        }

        /*
         * Variablen f�r die Attribute
         */
        protected String comment = null;
        protected int commType = 0;
    }

    /*
     * Hilfsmethoden
     */
    /**
     * Diese Methode sichert einen String so ab, das er in SQL-Statements
     * in einfachen Anfhrungsstrichen sicher ist.
     * 
     * Hierbei wird '\'' durch ein rechtes einfaches Anfhrungszeichen und
     * '\0' durch einen zentrierten Punkt ersetzt.
     * 
     * Zus�tzlich wird <code>null</code> durch einen Leerstring ersetzt.
     * 
     * @param string der abzusichernde String
     * @return der gesicherte String
     */
    public static String toSQLSafe(Object string) {
        if (string == null)
            return "";
            
        String result = string.toString().replaceAll("\\","" ).replaceAll("\0", "");
        return result;
    }

    /**
     * Diese Methode liefert die bergebene Zahl als String in einfachen
     * Anfhrungsstrichen zur�ck, wenn sie nicht 0 ist, und sonst den
     * SQL-Bezeichner <code>NULL</code>. Sie wird bei Feldern ben�tigt,
     * die als ints gefhrt werden und f�r die 0 das Zeichen f�r 'nicht
     * gesetzt' ist. 
     * 
     * @param value der umzuformende Wert.
     * @return die String-Darstellung der Eingabe.
     */
    public static String toStringWithNULL(int value) {
        return value != 0 ? "'" + Integer.toString(value) + "'" : "NULL";
    }

    /**
     * Dieser Datumsformatierer wird benutzt, um Datumsangaben f�r die SQL-�bergabe
     * vorzubereiten. Benutzt wird das Format "yyyy-dd-MM".
     * @see SimpleDateFormat
     */
    public static final SimpleDateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyy-dd-MM");

    /**
     * Dieser Datumsformatierer wird benutzt, um Datumzeitangaben f�r die SQL-�bergabe
     * vorzubereiten. Benutzt wird das Format "yyyy-dd-MM HH:mm:ss".
     * @see SimpleDateFormat
     */
    public static final SimpleDateFormat SQL_DATETIME_FORMAT = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");

    /*
     * Diese Variablen enthalten die Filter f�r die ausgew�hlten Adressen.
     */
    protected String verteilergruppe = "";
    protected String user = "";
    protected String password = "";

    protected boolean verteilerSelectionIntersection = false;
    protected boolean search = false;
    protected boolean matchcode = false;
    protected boolean followup = false;
    protected String herrnFrau;
    protected String nachname;
    protected String vorname;
    protected String titel;
    protected String akadTitel;
    protected String namenszusatz;
    protected String institution;
    protected String strasse;
    protected String plz;
    protected String ort;
    protected String land;
    protected String lkz;
    protected String bundesland;

    protected String tel_dienstlich;
    protected String tel_privat;
    protected String fax_dienstlich;
    protected String fax_privat;
    protected String email;
    protected Date followupdate;

}
