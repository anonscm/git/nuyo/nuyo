/*
 * Created on 25.05.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.logging.TarentLogger;


/**
 * Calendar visible elements should be registered here,
 * in order to dispatch gui events to them. 
 * 
 * @see CalendarVisibleElement
 * 
 * @author niko
 */
public class CalenderManager {
    private static final TarentLogger logger = new TarentLogger(CalenderManager.class);

    private List listeners;

    private ControlListener controlListener;

    public CalenderManager( ControlListener aControlListener ) {
        listeners = new ArrayList();
        controlListener = aControlListener;
    }

    public void addAppointmentDisplayListener( CalendarVisibleElement display ) {
        listeners.add( display );
    }

    public void removeAppointmentDisplayListener( CalendarVisibleElement display ) {
        listeners.remove( display );
    }

    // -----------------------------------------------------------------------------------

    public Appointment askForAppointment( int appointmentid ) {
        CalendarVisibleElement currentDisplay;
        Appointment appointment;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            appointment = currentDisplay.getAppointment( appointmentid );
            if ( appointment != null )
                return appointment;
        }
        return null;
    }

    // -----------------------------------------------------------------------------------

    public void fireChangedAppointment( Appointment appointment, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) ){
                currentDisplay.changedAppointment( appointment );
            }
        }
    }

    public void fireAddedAppointment( Appointment appointment, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.addedAppointment( appointment );
        }
    }

    public void fireRemovedAppointment( Appointment appointment, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.removedAppointment( appointment );
        }
    }

    public void fireSelectedAppointment( Appointment appointment, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.selectedAppointment( appointment );
        }
    }

    /**
     * Fires update event for each {@link CalendarVisibleElement}.
     * This method is thread safe: all runtime exceptions will be handled.
     * 
     * @param exclude CalendarVisibleElement
     */
    public void fireChangeCalendar(final CalendarVisibleElement exclude ) {
        for ( int i = 0; i < listeners.size(); i++ ) {
            final CalendarVisibleElement currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) ){
                try {
                    currentDisplay.changedCalendarCollection();
                }
                catch ( RuntimeException e ) {
                    logger.warningSilent("[!] [CalenderManager]: CalendarVisibleElement not updated: " + e.getClass().getName(), e);
                }
            }
        }
    }

    public void fireSetViewType( String viewtype, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.setViewType( viewtype );
        }
    }

    public void fireSetVisibleInterval( ScheduleDate start, ScheduleDate end, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.setVisibleInterval( start, end );
        }
    }

    public void fireSetGridActive( boolean usegrid, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.setGridActive( usegrid );
        }
    }

    public void fireSetGridHeight( int seconds, CalendarVisibleElement exclude ) {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            if ( !currentDisplay.equals( exclude ) )
                currentDisplay.setGridHeight( seconds );
        }
    }

    /** Fires export event to all panels, that implement CalendarVisibleElement interface.*/
    public void fireExportAppoitmentsAndTasks() {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            currentDisplay.clickedExportButton();
        }
    }

    /** Fires new task creation event to all panels, that implement CalendarVisibleElement interface.*/
    public void fireNewTaskCreation() {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            currentDisplay.clickedNewTaskButton();
        }
    }

    /** Fires new appointment creation event to all panels, that implement CalendarVisibleElement interface.*/
    public void fireNewAppoitmentCreation() {
        CalendarVisibleElement currentDisplay;
        for ( int i = 0; i < listeners.size(); i++ ) {
            currentDisplay = ( (CalendarVisibleElement) listeners.get( i ) );
            currentDisplay.clickedNewAppointmentButton();
        }
    }
}
