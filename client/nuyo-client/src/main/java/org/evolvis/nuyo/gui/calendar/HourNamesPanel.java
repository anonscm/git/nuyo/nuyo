package org.evolvis.nuyo.gui.calendar;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.JPanel;

/*
 * Created on 17.07.2003
 *
 */

/**
 * @author niko
 *
 */


public class HourNamesPanel extends JPanel 
{
  private ScheduleData m_oScheduleData;
    
  public HourNamesPanel(ScheduleData sd)
  {
    m_oScheduleData = sd;
  }
    
  public boolean isOpaque()
  {
    return(true); 
  }
  
  public int getStartSecondForPixel(int y)
  {
    int pixelperday = (m_oScheduleData.hourHeight * 24);
    int secondsperday = 24 * 60 *60;    
    double fakpix = ((double)y) / ((double)pixelperday);
    int second = (int)(((double)secondsperday) * fakpix);    
    return second;
  }

  
  public int getPixelForSecond(int second)
  {
    int pixelperday = (m_oScheduleData.hourHeight * 24);
    int secondsperday = 24 * 60 *60;    
    double fakpix = ((double)second) / ((double)secondsperday);
    int pixel = (int)(((double)pixelperday) * fakpix);    
    return pixel;
  }
  
  protected void paintComponent(Graphics g) 
  {        
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();

    g.setColor(m_oScheduleData.backgroundPanelColor);
    g.fillRect(0, 0, size.width, size.height);    
    
    // die Stunden f�llen
    g.setColor(m_oScheduleData.freeHourColor);
    g.fillRect(0, 0, m_oScheduleData.firstDayX, (m_oScheduleData.numberOfHours * m_oScheduleData.hourHeight));

    // die Stunden der Arbeitszeit f�llen
    g.setColor(m_oScheduleData.workHourColor);
    g.fillRect(0, 
                  (m_oScheduleData.firstWorkHour * m_oScheduleData.hourHeight), 
                  m_oScheduleData.firstDayX, 
                  ((m_oScheduleData.numberOfWorkHours /*+ 1*/) * m_oScheduleData.hourHeight) /*- m_oScheduleData.m_iFirstHourY */);

    // das Stunden-Grid zeichnen
    for(int h = 0; h<25; h++)
    {
      int y = (h * m_oScheduleData.hourHeight);
      g.setColor(m_oScheduleData.gridColor);
      g.drawLine(0, y, m_oScheduleData.firstDayX, y);
      if (h < m_oScheduleData.numberOfHours) 
      {
        g.setColor(m_oScheduleData.hourFontColor);
        g.drawString(h + "h", m_oScheduleData.hoursDistanceToLeftBorder, y + (m_oScheduleData.hourHeight / 2) + 1);
      }
    }      
  }
  
  
  public int getHourOfYPos(int x)
  {
    if (x> (m_oScheduleData.hourHeight * 24)) return -1;
    return x / (m_oScheduleData.hourHeight);
  }
  
}

