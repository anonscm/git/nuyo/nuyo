/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataSources;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSourceAdapter;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StringDataSource extends DataSourceAdapter implements TarentGUIEventListener
{
  private int m_iMaxLength = -1;
  
  public String getListenerName()
  {
    return "StringDataSource";
  }  

  public DataSource cloneDataSource()
  {
    StringDataSource datasource = new StringDataSource();
    
    datasource.m_oDataContainer = m_oDataContainer;
    datasource.m_sDBKey = m_sDBKey ;
    datasource.m_iMaxLength = m_iMaxLength;

    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      datasource.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    
    return datasource;
  }
  
  public boolean canHandle(Class data)
  {
    return data.isInstance(String.class);
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.DATASOURCE, "StringDataSource", new DataContainerVersion(0));
    d.addParameterDescription(new ParameterDescription("maxlen", "maximale L�nge", "die maximale L�nge der Zeichenkette die gespeichert werden kann."));
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("maxlen".equalsIgnoreCase(key)) 
    {
      try
      {
        m_iMaxLength = Integer.parseInt(value);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        nfe.printStackTrace();
      }
    }
    return false;
  }

  public boolean readData(PluginData plugindata)
  {
    Object newdata = plugindata.get(getDBKey());    
    getDataContainer().setGUIData(newdata);
    if (newdata == null) newdata="";
    return (newdata != null);
  }

  public boolean writeData(PluginData plugindata)
  {
    if (getDataContainer().isValid())
    {
      Object data = getDataContainer().getDataStorage().getData();
      if (data instanceof String)
      {
        String text = (String)data;
        if (m_iMaxLength != -1)
        {
          if (text.length() > m_iMaxLength)
          {
            text = text.substring(0, m_iMaxLength);          
          }        
        }
        return plugindata.set(getDBKey(), text);
      }
    }
    else
    {
      System.err.println("ERROR unable to store data \"" + getDBKey() + "\" to " + plugindata + ": data not valid!");    
    }
    return false;
  }
  
}
