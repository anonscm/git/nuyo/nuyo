/*
 * Created on 19.06.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

/**
 * @author niko
 *
 */
public class DataChange
{
  private Object m_oFieldKey;
  private String m_sFieldName;
  private String m_sChangeDescription;
  private Object m_oBeforeValue;
  private Object m_oAfterValue;
  
  public DataChange(Object fieldkey, String fieldname, String description, Object before, Object after)
  {
    m_oFieldKey = fieldkey;
    m_sFieldName = fieldname;
    m_sChangeDescription = description;
    m_oBeforeValue = before;
    m_oAfterValue = after;    
  }
  
  public Object getAfterValue()
  {
    return m_oAfterValue;
  }

  public void setAfterValue(Object afterValue)
  {
    m_oAfterValue = afterValue;
  }

  public Object getBeforeValue()
  {
    return m_oBeforeValue;
  }

  public void setBeforeValue(Object beforeValue)
  {
    m_oBeforeValue = beforeValue;
  }

  public Object getFieldKey()
  {
    return m_oFieldKey;
  }

  public void setFieldKey(Object fieldKey)
  {
    m_oFieldKey = fieldKey;
  }

  public String getChangeDescription()
  {
    return m_sChangeDescription;
  }

  public void setChangeDescription(String changeDescription)
  {
    m_sChangeDescription = changeDescription;
  }

  public String getFieldName()
  {
    return m_sFieldName;
  }

  public void setFieldName(String fieldName)
  {
    m_sFieldName = fieldName;
  }

}
