/* $Id: CommandEvent.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 07.10.2003
 */
package org.evolvis.nuyo.messages;

/**
 * Events, die diese Schnittstelle implementieren, fordern die Ausf�hrung
 * einer bestimmten Aktion an.
 * 
 * @author mikel
 */
public interface CommandEvent {

}
