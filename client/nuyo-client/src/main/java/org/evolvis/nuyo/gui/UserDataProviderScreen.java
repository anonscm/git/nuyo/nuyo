/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.controller.manager.AmRequestFacade;

import de.tarent.commons.ui.EscapeDialog;
import de.tarent.octopus.client.UserDataProvider;

/**
 * Dialog um zwischedurch nach Userdaten zu fragen,
 * z.B. wenn die Session abgelaufen ist.
 *
 * 
 *
 */
public class UserDataProviderScreen implements UserDataProvider {
    
    private EscapeDialog m_oThisDialog;
    //     private boolean m_bResult;
    //private ImageIcon m_oStartScreenGfx;
    //   private ImageIcon m_oStartAnimGfx;
    private ImageIcon m_oStartStillGfx;
    //private ImageIcon m_oWindowGfx;
  
    private JPanel     m_oPanel_Control;  
    private JPanel     m_oPanel_Main;
  
    private JTextField m_oTextField_Username;
    private JPasswordField m_oTextField_Password;
    
    private JButton m_oButton_Ok;
    private JButton m_oButton_Cancel;
    private JButton m_oButton_Exit;
  
  
    private JLabel m_oTarentLabel; 
    
    boolean userHasCanceled;
    boolean hasDoneInit = false;
    Frame owner;
    AmRequestFacade amRequestFacade;

    
    public UserDataProviderScreen(Frame owner, AmRequestFacade amRequestFacade) {
        this.owner = owner;
        this.amRequestFacade = amRequestFacade;
    }

    protected synchronized void init() {
        if (hasDoneInit)
            return;
        hasDoneInit = true;
    
        URL imagelocation = this.getClass().getResource("/de/tarent/contact/resources/"); 
        try {   
            //                 m_oStartScreenGfx = new ImageIcon(new URL(imagelocation, "StartScreen2.gif"));
            //                 m_oStartAnimGfx    = new ImageIcon(new URL(imagelocation, "tarentAnim.gif"));
            m_oStartStillGfx       = new ImageIcon(new URL(imagelocation, "tarentStill.gif"));
        }
        catch (MalformedURLException mue){
            // DO NOTHING HERE
        }    

        m_oPanel_Main = new JPanel();
        m_oPanel_Main.setLayout(new BorderLayout());
    
        m_oPanel_Control = new JPanel();
        m_oPanel_Control.setLayout(new BorderLayout());

        JPanel panel_entry = new JPanel();
        panel_entry.setLayout(new BorderLayout());
        panel_entry.setBorder(new EmptyBorder(5,5,5,5));    
        
        JPanel panel_entry_label = new JPanel(); 
        panel_entry_label.setLayout(new BoxLayout(panel_entry_label, BoxLayout.Y_AXIS));

        JPanel panel_entry_input = new JPanel(); 
        panel_entry_input.setLayout(new BoxLayout(panel_entry_input, BoxLayout.Y_AXIS));

        panel_entry.add(panel_entry_label, BorderLayout.WEST); 
        panel_entry.add(panel_entry_input, BorderLayout.CENTER); 

        JPanel panel_buttons = new JPanel();
        panel_buttons.setLayout(new BorderLayout());
    
        JLabel label_username = new JLabel(Messages.getString("GUI_UserDataProviderScreen_Benutzername")); 
        m_oTextField_Username = new JTextField();
        panel_entry_label.add(label_username, BorderLayout.WEST); 
        panel_entry_input.add(m_oTextField_Username, BorderLayout.CENTER); 
    
        JLabel label_password = new JLabel(Messages.getString("GUI_UserDataProviderScreen_Passwort")); 
        m_oTextField_Password = new JPasswordField(); 
        panel_entry_label.add(label_password, BorderLayout.WEST); 
        panel_entry_input.add(m_oTextField_Password, BorderLayout.CENTER); 

        m_oPanel_Control.add(panel_entry, BorderLayout.CENTER); 
    
        m_oButton_Ok = new JButton(Messages.getString("GUI_UserDataProviderScreen_Button_Ok")); 
        m_oButton_Ok.setToolTipText(Messages.getString("GUI_UserDataProviderScreen_Button_Ok_ToolTip")); 
        m_oButton_Ok.addActionListener(new button_ok_clicked());
   
        // Cancel ist disabled, da eine Benutzung ohne Session derzeit nicht sinnvoll ist.
        //         m_oButton_Cancel = new JButton(Messages.getString("GUI_UserDataProviderScreen_Button_Cancel")); 
        //         m_oButton_Cancel.setToolTipText(Messages.getString("GUI_UserDataProviderScreen_Button_Cancel_ToolTip")); 
        //         m_oButton_Cancel.setFont(m_oFont);
        //         m_oButton_Cancel.setCursor(m_oHandCursor);
        //         m_oButton_Cancel.setBackground(m_oButtonBGColor);
        //         m_oButton_Cancel.setForeground(m_oButtonFGColor);    
        //         m_oButton_Cancel.addActionListener(new button_cancel_clicked());

        m_oButton_Exit = new JButton(Messages.getString("GUI_UserDataProviderScreen_Button_Exit")); 
        m_oButton_Exit.setToolTipText(Messages.getString("GUI_UserDataProviderScreen_Button_Exit_ToolTip")); 
        m_oButton_Exit.addActionListener(new button_exit_clicked());


        JPanel panel_ok_cancel = new JPanel(new BorderLayout());
        panel_ok_cancel.add(m_oButton_Ok, BorderLayout.WEST);
        panel_ok_cancel.add(Box.createHorizontalStrut(5), BorderLayout.CENTER);
        //panel_ok_cancel.add(m_oButton_Cancel, BorderLayout.EAST);


        JPanel panel_exit = new JPanel(new BorderLayout());
        //         panel_exit.add(m_oButton_Ok, BorderLayout.WEST);
        //         panel_exit.add(Box.createHorizontalStrut(5), BorderLayout.CENTER);
        panel_exit.add(m_oButton_Exit, BorderLayout.EAST);

        panel_buttons.add(panel_ok_cancel, BorderLayout.WEST);
        panel_buttons.add(panel_exit, BorderLayout.EAST);
    
        m_oPanel_Control.add(panel_buttons, BorderLayout.SOUTH); 
        m_oPanel_Control.setBorder(new EmptyBorder(5,5,5,5));

        m_oPanel_Main.add(m_oPanel_Control, BorderLayout.SOUTH); 

        JPanel gfxpanel = new JPanel();
        gfxpanel.setLayout(new BorderLayout());
        gfxpanel.setOpaque(true);
        gfxpanel.setBackground(Color.WHITE);
        gfxpanel.setBorder(new EmptyBorder(100,0,5,0));
    
//         JButton label_gfx = new JButton(m_oStartScreenGfx);
//         label_gfx.setBorder(null);
//         label_gfx.setFocusPainted(false);
//         label_gfx.setCursor(m_oHandCursor);
//         label_gfx.setBackground(Color.WHITE);
//         label_gfx.addActionListener(new button_info_clicked());
        m_oTarentLabel = new JLabel(m_oStartStillGfx);
            
//         gfxpanel.add(label_gfx, BorderLayout.NORTH);
        gfxpanel.add(m_oTarentLabel, BorderLayout.SOUTH);

        m_oPanel_Main.add(gfxpanel, BorderLayout.WEST); 


        m_oThisDialog = new EscapeDialog(owner, true);
        m_oThisDialog.setModal(true);
        m_oThisDialog.setTitle(Messages.getString("GUI_UserDataProviderScreen_Title")); 
        m_oThisDialog.getContentPane().add(m_oPanel_Main);
            
        JTextArea m_oLabel_Wait = new JTextArea();
        m_oLabel_Wait.setWrapStyleWord(true);
        m_oLabel_Wait.setEditable(false);
        m_oLabel_Wait.setText(Messages.getString("GUI_UserDataProviderScreen_Message"));
        m_oLabel_Wait.setBorder(new EmptyBorder(10,10,10,10));
        //m_oLabel_Wait.setHorizontalAlignment(SwingConstants.CENTER);
        //m_oLabel_Wait.setOpaque(true);
        m_oLabel_Wait.setBackground(Color.WHITE);
        m_oLabel_Wait.setForeground(Color.BLACK);
        m_oLabel_Wait.setFont(new Font(null, Font.BOLD, 12));
        JPanel msgPanel = new JPanel(new BorderLayout());
        msgPanel.setBackground(Color.WHITE);
        msgPanel.add(m_oLabel_Wait);
        m_oPanel_Main.add(msgPanel, BorderLayout.CENTER); 

            
        m_oThisDialog.getRootPane().setDefaultButton(m_oButton_Ok);
    
        m_oThisDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);        
        m_oThisDialog.addWindowListener(new WindowAdapter() { 
                public void windowClosing(WindowEvent e) {
                    cancelWindow();
                }});
    
        m_oThisDialog.pack();
        m_oThisDialog.setLocationRelativeTo(null);
    }


    public boolean requestUserData(String message, String usernamePreselection) {
        init();

        // if more than one threads are calling this dialog
        // all others only wait ant return.
        if (m_oThisDialog.isVisible()) {
            try {
                m_oThisDialog.wait();
            } catch (InterruptedException ie) {
                // do nothing here
            }
            return true;
        }
        synchronized (m_oThisDialog) {
            if (usernamePreselection != null)
                m_oTextField_Username.setText(usernamePreselection);
            else
                m_oTextField_Password.requestFocus();
            userHasCanceled = true;
            m_oThisDialog.setVisible(true);
            return ! userHasCanceled;
        }
    }


    public String getUsername() {
        return m_oTextField_Username.getText();
    }
    
    public String getPassword() {
        return new String(m_oTextField_Password.getPassword());
    }  
    
    public void cancelWindow() { 
        userHasCanceled = true;
        m_oThisDialog.setVisible(false);
        m_oThisDialog.dispose();
    }    

    public class button_ok_clicked implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            userHasCanceled = false;        
            m_oThisDialog.setVisible(false);
            m_oThisDialog.dispose();
        } 
    }
    

    public class button_cancel_clicked implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            cancelWindow();
        } 
    }    

    public class button_exit_clicked implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            amRequestFacade.userRequestExitApplication();
        } 
    }

}
