/* $Id: VcardUtil.java,v 1.7 2007/08/30 16:10:25 fkoester Exp $
 * Created on 01.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import net.sf.vcard4j.java.Type;
import net.sf.vcard4j.java.VCard;
import net.sf.vcard4j.java.type.ADR;
import net.sf.vcard4j.java.type.AGENT;
import net.sf.vcard4j.java.type.BDAY;
import net.sf.vcard4j.java.type.CATEGORY;
import net.sf.vcard4j.java.type.CLASS;
import net.sf.vcard4j.java.type.EMAIL;
import net.sf.vcard4j.java.type.FN;
import net.sf.vcard4j.java.type.GEO;
import net.sf.vcard4j.java.type.KEY;
import net.sf.vcard4j.java.type.LABEL;
import net.sf.vcard4j.java.type.LOGO;
import net.sf.vcard4j.java.type.MAILER;
import net.sf.vcard4j.java.type.N;
import net.sf.vcard4j.java.type.NICKNAME;
import net.sf.vcard4j.java.type.NOTE;
import net.sf.vcard4j.java.type.ORG;
import net.sf.vcard4j.java.type.PHOTO;
import net.sf.vcard4j.java.type.PROID;
import net.sf.vcard4j.java.type.REV;
import net.sf.vcard4j.java.type.ROLE;
import net.sf.vcard4j.java.type.SORT_STRING;
import net.sf.vcard4j.java.type.SOUND;
import net.sf.vcard4j.java.type.TEL;
import net.sf.vcard4j.java.type.TITLE;
import net.sf.vcard4j.java.type.TZ;
import net.sf.vcard4j.java.type.UID;
import net.sf.vcard4j.java.type.URL;
import net.sf.vcard4j.java.type.VERSION;
import net.sf.vcard4j.java.type.X;
import net.sf.vcard4j.parser.DomParser;
import net.sf.vcard4j.parser.VCardParseException;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.config.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.tarent.commons.utils.TaskManager;

/**
 * Diese Klasse bietet statische Hilfsmethoden zum Importieren und Exportieren
 * von VCards aus dem ContactClient. Hierbei wird die Bibliothek vCard4J ab
 * Version 1.1.3 benutzt.
 * 
 * @author mikel
 */
public class VcardUtil {
    //
    // �ffentliche statische Methoden
    //
    /**
     * Diese statische Methode importiert die Daten einer VCard in ein
     * bestehendes Address-Objekt.
     * 
     * @param vcardReader
     *            ein Reader, der eine VCard ausliest.
     * @param address
     *            eine Address, in die hinein die Daten gelangen sollen.
     * @return <code>true</code>, gdw erfolgreich importiert wurde.
     */
    public static boolean importVcardData(Reader vcardReader, Address address)
            throws ContactDBException {
        try {
            NodeList cardNodes = readVcardNodeList(vcardReader);
            if (cardNodes.getLength() > 0) {
                Node cardNode = cardNodes.item(0);
                VCard card = new VCard((Element) cardNode);
                Iterator it = card.getTypes();
                Map classedTypes = new HashMap();
                while (it.hasNext()) {
                    Type type = (Type) it.next();
                    if (type == null) continue;
                    Class typeClass = type.getClass();
                    List types = (List) classedTypes.get(typeClass);
                    if (types == null) {
                        types = new LinkedList();
                        classedTypes.put(typeClass, types);
                    }
                    types.add(type);
                }
                fillTypesIntoAddress(classedTypes, address);
                return true;//succeeded!
            } else
                logger.warning("Keine vcard-Knoten");
        } catch (VCardParseException e) {
            logger.log(Level.WARNING, "Parser-Fehler beim Import einer VCard",
                    e);
        } catch (IOException e) {
            logger.log(Level.WARNING, "IO-Fehler beim Import einer VCard", e);
        } catch (ParserConfigurationException e) {
            logger.log(Level.WARNING, "Parse-Fehler beim Import einer VCard", e);
        } catch (RuntimeException e) {
            logger.log(Level.WARNING, "Fehler beim Import einer VCard", e);
        }
        return false;
    }

    /**
     * Diese Methode exportiert alle Addressen aus einer Addresssammlung
     * im VCARD-Format.
     * 
     * @param addresses zu exportierende Addresssammlung.
     * @param writer VCARD-Ausgabe
     */
    public static void exportVcardData(final Addresses addresses, final File vcardFile, final String vcardEncoding) throws IOException, ContactDBException {
        
        TaskManager.Task exportTask = new TaskManager.Task() {
                boolean cancel = false;
                
                public void run(TaskManager.Context cntx) {
                    try {
                        Writer vcardWriter = new OutputStreamWriter(new FileOutputStream(vcardFile), vcardEncoding);
                        cntx.setGoal(addresses.getIndexMax());                        
                        for (int i = 0; i <= addresses.getIndexMax() && !cancel; i++) {
                            writeVcardData(addresses.get(i), vcardWriter);
                            vcardWriter.write('\n');
                            vcardWriter.flush();
                            
                            if (i % 10 == 0) {
                                cntx.setCurrent(i);
                                cntx.setActivityDescription(Messages.getFormattedString("Export_Status", new Integer(i), new Integer(addresses.getIndexMax())));
                            }
                        }

                        vcardWriter.close();
                    } catch (ContactDBException e) {
                        ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("AM_VCARD_Export_DbError"), e);
                    } catch (IOException e) {
                        ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("AM_VCARD_Export_IoError"), "unknown", e);
                    }
                }
                
                public void cancel() {
                    cancel = true;
                }
            };
            

        TaskManager.getInstance().register(exportTask, Messages.getString("AM_Export_Caption"), true, TaskManager.DEFAULT_PRIORITY+10);
    }


    /**
     * Diese Methode exportiert eine Adresse im VCARD-Format.
     * 
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    public static void exportVcardData(Address address, final File vcardFile, final String vcardEncoding) throws ContactDBException, IOException {
        Writer vcardWriter = new OutputStreamWriter(new FileOutputStream(vcardFile), vcardEncoding);
        writeVcardData(address, vcardWriter);
        vcardWriter.close();
    }

    private static void writeVcardData(Address address, Writer writer) throws ContactDBException {
        PrintWriter printWriter = new PrintWriter(writer);
        try {
            writeType("BEGIN", null, "VCARD", printWriter);
            writeType("VERSION", null, "3.0", printWriter);
            writeNameTypes(address, printWriter);
            writeOrganizationTypes(address, printWriter);
            writeAdrTypes(address, printWriter);
            writeCommunicationTypes(address, printWriter);
            writeBdayTypes(address, printWriter);
            writeMiscTypes(address, printWriter);
            
            writeType("END", null, "VCARD", printWriter);

        } finally {
            printWriter.flush();
        }
    }

    //
    // gesch�tzte Hilfsmethoden
    //
    /**
     * Diese Methode liest eine VCard-Datei ein und liefert die erzeugten
     * vcard-Nodes als Liste zur�ck.
     * 
     * @param vcardReader
     *            ein Reader, der eine VCard ausliest.
     * @return Liste von vcard-DOM-Nodes
     * @throws VCardParseException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    protected static NodeList readVcardNodeList(Reader vcardReader)
            throws VCardParseException, IOException, ParserConfigurationException {
        DomParser domParser = new DomParser();
        Document document = XmlUtil.createDocument();
        domParser.parse(vcardReader, document);
        NodeList cardNodes = document.getElementsByTagName("vcard");
        return cardNodes;
    }

    /**
     * Diese Methode f�llt Typen aus einer klassifizierten Map in eine
     * Adresse.
     * 
     * @param classedTypes Types-Class-indizierte Map von Listen von Types.
     * @param address zu f�llendes Adressobjekt.
     */
    protected static void fillTypesIntoAddress(Map classedTypes, Address address)
            throws ContactDBException {
        StringBuffer moreData = new StringBuffer();
        Iterator itTypeLists = classedTypes.entrySet().iterator();
        while (itTypeLists.hasNext()) {
            Map.Entry entry = (Map.Entry) itTypeLists.next();
            Class typeClass = (Class) entry.getKey();
            List types = (List) entry.getValue();
            if (typeClass == ADR.class)
                fillAdrTypesIntoAddress(types, address, moreData, ADR_TYPE_PRIORITIES);
            else if (typeClass == AGENT.class)
                fillAgentTypesIntoAddress(types, address, moreData);
            else if (typeClass == BDAY.class)
                fillBdayTypesIntoAddress(types, address, moreData);
            else if (typeClass == CATEGORY.class)
                fillCategoryTypesIntoAddress(types, address, moreData);
            else if (typeClass == CLASS.class)
                fillClassTypesIntoAddress(types, address, moreData);
            else if (typeClass == EMAIL.class)
                fillEmailTypesIntoAddress(types, address, moreData, EMAIL_TYPE_PRIORITIES);
            else if (typeClass == FN.class)
                fillFnTypesIntoAddress(types, address, moreData);
            else if (typeClass == GEO.class)
                fillGeoTypesIntoAddress(types, address, moreData);
            else if (typeClass == KEY.class)
                fillKeyTypesIntoAddress(types, address, moreData);
            else if (typeClass == LABEL.class)
                fillLabelTypesIntoAddress(types, address, moreData);
            else if (typeClass == LOGO.class)
                fillLogoTypesIntoAddress(types, address, moreData);
            else if (typeClass == MAILER.class)
                fillMailerTypesIntoAddress(types, address, moreData);
            else if (typeClass == N.class)
                fillNTypesIntoAddress(types, address, moreData);
            else if (typeClass == NICKNAME.class)
                fillNicknameTypesIntoAddress(types, address, moreData);
            else if (typeClass == NOTE.class)
                fillNoteTypesIntoAddress(types, address, moreData);
            else if (typeClass == ORG.class)
                fillOrgTypesIntoAddress(types, address, moreData);
            else if (typeClass == PHOTO.class)
                fillPhotoTypesIntoAddress(types, address, moreData);
            else if (typeClass == PROID.class)
                fillProidTypesIntoAddress(types, address, moreData);
            else if (typeClass == REV.class)
                fillRevTypesIntoAddress(types, address, moreData);
            else if (typeClass == ROLE.class)
                fillRoleTypesIntoAddress(types, address, moreData);
            else if (typeClass == SORT_STRING.class)
                fillSortStringTypesIntoAddress(types, address, moreData);
            else if (typeClass == SOUND.class)
                fillSoundTypesIntoAddress(types, address, moreData);
            else if (typeClass == TEL.class)
                fillTelTypesIntoAddress(types, address, moreData);
            else if (typeClass == TITLE.class)
                fillTitleTypesIntoAddress(types, address, moreData);
            else if (typeClass == TZ.class)
                fillTzTypesIntoAddress(types, address, moreData);
            else if (typeClass == UID.class)
                fillUidTypesIntoAddress(types, address, moreData);
            else if (typeClass == URL.class)
                fillUrlTypesIntoAddress(types, address, moreData);
            else if (typeClass == VERSION.class)
                fillVersionTypesIntoAddress(types, address, moreData);
            else if (typeClass == X.class)
                fillXTypesIntoAddress(types, address, moreData);
            else
                fillOtherTypesIntoAddress(types, address, moreData);
        }
        address.setNotiz(moreData.toString());
    }

    /**
     * Diese Methode gibt einen VCARD-Type aus. Hierbei werden in Werten
     * Sonderzeichen escape-t.
     * 
     * @param name Name des Types
     * @param attributes Map von Attributnamen auf Attributwert oder Liste
     *  von Attributwerden
     * @param value ein einzelner Wert 
     * @param writer VCARD-Ausgabe
     */
    protected static void writeType(String name, Map attributes, String value, PrintWriter writer) {
        writeType(name, attributes, Collections.singletonList(value), writer);
    }

    /**
     * Diese Methode gibt einen VCARD-Type aus. Hierbei werden in Werten 
     * Sonderzeichen escape-t.
     * 
     * @param name Name des Types
     * @param attributes Map von Attributnamen auf Attributeinzelwerte oder Listen
     *  von Attributeinzelwerten
     * @param values eine Liste (StructuredType) von Einzelwerten oder
     *  Listen (ListType) von Einzelwerten.  
     * @param writer VCARD-Ausgabe
     */
    protected static void writeType(String name, Map attributes, List values, PrintWriter writer) {
        writer.print(escapeVcardString(name, true));
        if (attributes != null) {
            Iterator itAtts = attributes.entrySet().iterator();
            while(itAtts.hasNext()) {
                Map.Entry entry = (Map.Entry) itAtts.next();
                writer.print(';');
                writer.print(escapeVcardString(entry.getKey().toString(), true));
                writer.print('=');
                writeVcardListOrSimple(entry.getValue(), writer, true);
            }
        }
        writer.print(':');
        if (values != null) {
            Iterator itValues = values.iterator();
            if (itValues.hasNext())
                writeVcardListOrSimple(itValues.next(), writer, false);
            while(itValues.hasNext()) {
                writer.print(';');
                writeVcardListOrSimple(itValues.next(), writer, false);
            };
        }
        writer.print('\n');
    }

    /**
     * Diese Methode gibt einen VCARD-Type aus. Sie ist eine Spezialisierung,
     * die ein Attribut TYPE mit �bergebenen Werten erzeugt. Hierbei werden in Werten 
     * Sonderzeichen escape-t. 
     * 
     * @param name Name des Types
     * @param types Array von Werten f�r das TYPE-Attribut
     * @param entry Wert
     * @param writer VCARD-Ausgabe
     */
    protected static void writeTypedType(String name, Object[] types, String entry, PrintWriter writer) {
        if (entry != null && entry.length() != 0) {
            Map m = Collections.singletonMap("TYPE", Arrays.asList(types));
            writeType(name, m, entry, writer);
        }
    }

    /**
     * Diese Methode gibt die Namenstypen N, FN und NAME aus.
     *  
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    protected static void writeNameTypes(Address address, PrintWriter writer) throws ContactDBException {
        String s = address.getShortAddressLabel();
        if (s != null && s.length() > 0)
            writeType("NAME", null, s, writer);
        s = address.getSpitzname();
        if (s != null && s.length() > 0)
            writeType("NICKNAME", null, s, writer);
        s = address.getPosition();
        if (s != null && s.length() > 0)
            writeType("TITEL", null, s, writer);

        StringBuffer buffer = new StringBuffer();
        appendNamePart(buffer, address.getAkadTitel());
        appendNamePart(buffer, address.getVorname());
        appendNamePart(buffer, address.getWeitereVornamen());
        appendNamePart(buffer, address.getNachname());
        appendNamePart(buffer, address.getNamenszusatz());
        writeType("FN", null, buffer.toString(), writer);

        List l = new ArrayList();
        l.add(address.getNachname());
        l.add(address.getVorname());
        l.add(address.getWeitereVornamen());
        l.add(address.getAkadTitel());
        l.add(address.getNamenszusatz());
        writeType("N", null, l, writer);
    }

    /**
     * Diese Methode gibt den Geburtstagstypen BDAY aus.
     *  
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    protected static void writeBdayTypes(Address address, PrintWriter writer) throws ContactDBException {
        Date date = address.getGeburtsdatum();
        if (date != null && date.getTime() != 0) { // Achtung: 1.1.1970-geborene �bersprungen
            writeType("BDAY", null, VCARD_DATETIME_FORMAT.format(date), writer);
        }
    }

    /**
     * Diese Methode gibt die Organisationstypen ORG und TITLE aus.
     *  
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    protected static void writeOrganizationTypes(Address address, PrintWriter writer) throws ContactDBException {
        String s = address.getPosition();
        if (s != null && s.length() > 0)
            writeType("TITEL", null, s, writer);
        List l = new ArrayList();
        s = address.getInstitution();
        if (s != null && s.length() > 0)
            l.add(s);
        s = address.getInstitutionWeitere();
        if (s != null && s.length() > 0)
            l.add(s);
        s = address.getAbteilung();
        if (s != null && s.length() > 0)
            l.add(s);
        if (l.size() > 0)
            writeType("ORG", null, l, writer);
    }

    /**
     * Diese Methode gibt den Adresstypen ADR aus.
     *  
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    protected static void writeAdrTypes(Address address, PrintWriter writer) throws ContactDBException {
        String strasseHausnr = null;
        {
            StringBuffer buffer = new StringBuffer();
            appendNamePart(buffer, address.getStrasse());
            appendNamePart(buffer, address.getHausNr());
            strasseHausnr = buffer.toString();
        }
        boolean isPostfach = (strasseHausnr.length() == 0 ||
            address.getPlz() == null || address.getPlz().length() == 0) &&
            address.getPlzPostfach() != null && address.getPlzPostfach().length() > 0;
        List l = new ArrayList();
        l.add(address.getPostfachNr());
        l.add("");
        l.add(strasseHausnr);
        l.add(address.getOrt());
        l.add(address.getBundesland());
        l.add(isPostfach ? address.getPlzPostfach() : address.getPlz());
        l.add(address.getLand());
        
        Iterator itL = l.iterator();
        boolean empty = true; 
        while(empty && itL.hasNext()) {
            String s = (String) itL.next();
            if (s != null && s.length() > 0)
                empty = false;
        }
        if (!empty)
            writeType("ADR", null, l, writer);
    }

    /**
     * Diese Methode gibt die Kommunikationstypen TEL, EMAIL und URL aus.
     *  
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    protected static void writeCommunicationTypes(Address address, PrintWriter writer) throws ContactDBException {
        writeTypedType("TEL", new Object[] {"voice", "work"}, address.getTelefonDienstlich(), writer);
        writeTypedType("TEL", new Object[] {"voice", "home"}, address.getTelefonPrivat(), writer);
        writeTypedType("TEL", new Object[] {"fax", "work"}, address.getFaxDienstlich(), writer);
        writeTypedType("TEL", new Object[] {"fax", "home"}, address.getFaxPrivat(), writer);
        writeTypedType("TEL", new Object[] {"cell", "voice", "work"}, address.getHandyDienstlich(), writer);
        writeTypedType("TEL", new Object[] {"cell", "voice", "home"}, address.getHandyPrivat(), writer);
        writeTypedType("EMAIL", new Object[] {"internet"}, address.getEmailAdresseDienstlich(), writer);
        String s = address.getHomepageDienstlich();
        if (s != null && s.length() != 0)
            writeType("URL", null, s, writer);
        
        // Kurzformen
        writeTypedType("TEL", new Object[] {"work"}, address.getTelefonDienstlich(), writer);
        writeTypedType("TEL", new Object[] {"home"}, address.getTelefonPrivat(), writer);
        writeTypedType("TEL", new Object[] {"cell", "work"}, address.getHandyDienstlich(), writer);
        writeTypedType("TEL", new Object[] {"cell", "home"}, address.getHandyPrivat(), writer);
        s = address.getTelefonDienstlich();
        if (s == null || s.length() == 0)
            s = address.getTelefonPrivat();
        writeTypedType("TEL", new Object[] {"voice"}, s, writer);
        s = address.getFaxDienstlich();
        if (s == null || s.length() == 0)
            s = address.getFaxPrivat();
        writeTypedType("TEL", new Object[] {"fax"}, s, writer);
        s = address.getHandyDienstlich();
        if (s == null || s.length() == 0)
            s = address.getHandyPrivat();
        writeTypedType("TEL", new Object[] {"cell"}, s, writer);
    }

    /**
     * Diese Methode gibt die Typen CATEGORY, NOTE und REV aus.
     *  
     * @param address zu exportierende Addresse.
     * @param writer VCARD-Ausgabe
     */
    protected static void writeMiscTypes(Address address, PrintWriter writer) throws ContactDBException {
        Date d = address.getAenderungsdatum();
        if (d == null || d.getTime() == 0)
            d = address.getErfassungsdatum();
        if (d != null && d.getTime() != 0)
            writeType("REV", null, VCARD_DATETIME_FORMAT.format(d), writer);
        
        String s = address.getNotiz();
        if (s != null && s.length() != 0)
            writeType("NOTE", null, s, writer);
    }
    
    //
    // private Hilfsmethoden
    //
    /**
     * Diese Methode f�llt ADR-Typen aus einer Liste in eine Adresse.
     * 
     * @param adrTypes Liste von ADR-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     * @param priorities Array von ADR-Untertypen zur Bestimmung der Hauptadresse.
     */
    private static void fillAdrTypesIntoAddress(List adrTypes, Address address, StringBuffer moreData, int[] priorities) throws ContactDBException {
        ADR mainAdr = null;
        if (adrTypes.size() == 0)
            return;
        if (adrTypes.size() > 1)
            for (int i=0; mainAdr == null && i < priorities.length; i++) {
                Iterator itTypes = adrTypes.iterator();
                while(itTypes.hasNext() && mainAdr == null) {
                    ADR thisAdr = (ADR) itTypes.next();
                    if (thisAdr == null)
                        continue;
                    ADR.Parameters theseParameters = (ADR.Parameters) thisAdr.getParameters();
                    if (theseParameters == null)
                        continue;
                    if (theseParameters.containsTYPE(priorities[i]))
                        mainAdr = thisAdr;
                }
            }
        if (mainAdr == null)
            mainAdr = (ADR) adrTypes.get(0);
        fillAdrTypeIntoAddress(mainAdr, address);
        Iterator itTypes = adrTypes.iterator();
        while(itTypes.hasNext()) {
            ADR thisAdr = (ADR) itTypes.next();
            if (thisAdr != mainAdr)
                fillAdrTypeIntoBuffer(thisAdr, moreData);
        }
    }

    /**
     * Diese Methode �bernimmt Adressdaten aus dem ADR-Typ in die Adresse.
     * 
     * @param adr ADR-Typ
     * @param address zu f�llendes Adressobjekt
     * @throws ContactDBException
     */
    private static void fillAdrTypeIntoAddress(ADR adr, Address address) throws ContactDBException {
        // TODO: TYPE-Informationen festhalten?
        address.setLand(adr.getCountry());
        address.setBundesland(adr.getRegion());
        address.setOrt(adr.getLocality());
        address.setPlz(adr.getPcode());
        address.setStrasse(adr.getStreet());
        address.setHausNr(adr.getExtadd());
        address.setPostfachNr(adr.getPobox());
    }

    /**
     * Diese Methode �bernimmt Adressdaten aus dem ADR-Typ in Zusatzdaten.
     * 
     * @param adr ADR-Typ
     * @param buffer Buffer f�r Zusatzdaten
     */
    private static void fillAdrTypeIntoBuffer(ADR adr, StringBuffer buffer) {
        ADR.Parameters params = (ADR.Parameters) adr.getParameters();
        buffer.append("VCARD[ADR");
        if (params.containsTYPE(ADR.Parameters.TYPE_DOM)) buffer.append(" DOM");
        if (params.containsTYPE(ADR.Parameters.TYPE_HOME)) buffer.append(" HOME");
        if (params.containsTYPE(ADR.Parameters.TYPE_INTL)) buffer.append(" INTL");
        if (params.containsTYPE(ADR.Parameters.TYPE_PARCEL)) buffer.append(" PARCEL");
        if (params.containsTYPE(ADR.Parameters.TYPE_POSTAL)) buffer.append(" POSTAL");
        if (params.containsTYPE(ADR.Parameters.TYPE_PREF)) buffer.append(" PREF");
        if (params.containsTYPE(ADR.Parameters.TYPE_WORK)) buffer.append(" WORK");
        buffer.append("]:\n");
        appendPrefixedConditionally(buffer, "- Postfach  ", adr.getPobox());
        appendPrefixedConditionally(buffer, "- Extension ", adr.getExtadd());
        appendPrefixedConditionally(buffer, "- Stra�e    ", adr.getStreet());
        appendPrefixedConditionally(buffer, "- PLZ       ", adr.getPcode());
        appendPrefixedConditionally(buffer, "- Lokalit�t ", adr.getLocality());
        appendPrefixedConditionally(buffer, "- Region    ", adr.getRegion());
        appendPrefixedConditionally(buffer, "- Land      ", adr.getCountry());
    }

    /**
     * Diese Methode tr�gt in einen Buffer eine Zeile mit Wert und Pr�fix ein, falls
     * der Wert nicht null oder leer ist.
     * 
     * @param buffer zu f�llender Buffer
     * @param prefix Pr�fix
     * @param value Wert
     */
    private static void appendPrefixedConditionally(StringBuffer buffer, String prefix, String value) {
        if (value != null && value.length() != 0)
            buffer.append(prefix).append(value).append('\n');
    }

    /**
     * Diese Methode f�llt AGENT-Typen aus einer Liste in eine Adresse.
     * 
     * @param agentTypes Liste von AGENT-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillAgentTypesIntoAddress(List agentTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itAgents = agentTypes.iterator();
        while (itAgents.hasNext())
            moreData.append("VCARD[AGENT]: ")
                    .append(((AGENT)itAgents.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt BDAY-Typen aus einer Liste in eine Adresse.
     * 
     * @param bdayTypes Liste von BDAY-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillBdayTypesIntoAddress(List bdayTypes, Address address, StringBuffer moreData) throws ContactDBException {
        try {
            Iterator itBdays = bdayTypes.iterator();
            if (itBdays.hasNext())
                address.setGeburtsdatum(((BDAY)itBdays.next()).get().getDate());
            while (itBdays.hasNext())
                moreData.append("VCARD[BDAY]: ")
                        .append(((BDAY)itBdays.next()).get())
                        .append('\n');
        } catch (RuntimeException rte) {
            logger.log(Level.WARNING, "Fehler beim Parsen eines Geburtstagseintrags", rte);
        }
    }
    
    /**
     * Diese Methode f�llt CATEGORY-Typen aus einer Liste in eine Adresse.
     * 
     * @param categoryTypes Liste von CATEGORY-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillCategoryTypesIntoAddress(List categoryTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itCategories = categoryTypes.iterator();
        StringBuffer keywords = new StringBuffer();
        moreData.append("VCARD[CATEGORIES]:\n");
        while (itCategories.hasNext()) {
            CATEGORY category = (CATEGORY) itCategories.next();
            for (int i = 0; i < category.size(); i++) {
                appendPrefixedConditionally(moreData, "- ", category.get(i));
                keywords.append(' ').append(category.get(i));
            }
        }
    }
    
    /**
     * Diese Methode f�llt CLASS-Typen aus einer Liste in eine Adresse.
     * 
     * @param classTypes Liste von CLASS-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillClassTypesIntoAddress(List classTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itClasses = classTypes.iterator();
        while (itClasses.hasNext())
            moreData.append("VCARD[CLASS]: ")
                    .append(((CLASS)itClasses.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt EMAIL-Typen aus einer Liste in eine Adresse.
     * 
     * @param emailTypes Liste von EMAIL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     * @param priorities Array von EMAIL-Untertypen zur Bestimmung der Haupt-E-Mail.
     */
    private static void fillEmailTypesIntoAddress(List emailTypes, Address address, StringBuffer moreData, int[] priorities) throws ContactDBException {
        EMAIL mainEmail = null;
        if (emailTypes.size() == 0)
            return;
        if (emailTypes.size() > 1)
            for (int i=0; mainEmail == null && i < priorities.length; i++) {
                Iterator itTypes = emailTypes.iterator();
                while(itTypes.hasNext() && mainEmail == null) {
                    EMAIL thisEmail = (EMAIL) itTypes.next();
                    if (thisEmail == null)
                        continue;
                    EMAIL.Parameters theseParameters = (EMAIL.Parameters) thisEmail.getParameters();
                    if (theseParameters == null)
                        continue;
                    if (theseParameters.containsTYPE(priorities[i]))
                        mainEmail = thisEmail;
                }
            }
        if (mainEmail == null)
            mainEmail = (EMAIL) emailTypes.get(0);
        
        // TODO: Parameter der Haupt-E-Mail beachten
        address.setEmailAdresseDienstlich(mainEmail.get());
        Iterator itTypes = emailTypes.iterator();
        while(itTypes.hasNext()) {
            EMAIL thisEmail = (EMAIL) itTypes.next();
            if (thisEmail != mainEmail)
                fillEmailTypeIntoBuffer(thisEmail, moreData);
        }
    }

    /**
     * Diese Methode �bernimmt E-Mail-Daten aus dem EMAIL-Typ in Zusatzdaten.
     * 
     * @param email EMAIL-Typ
     * @param buffer Buffer f�r Zusatzdaten
     */
    private static void fillEmailTypeIntoBuffer(EMAIL email, StringBuffer buffer) {
        EMAIL.Parameters params = (EMAIL.Parameters) email.getParameters();
        buffer.append("VCARD[EMAIL");
        if (params.containsTYPE(EMAIL.Parameters.TYPE_INTERNET)) buffer.append(" INTERNET");
        if (params.containsTYPE(EMAIL.Parameters.TYPE_PREF)) buffer.append(" PREF");
        if (params.containsTYPE(EMAIL.Parameters.TYPE_X400)) buffer.append(" X400");
        buffer.append("]: ").append(email.get()).append('\n');
    }

    /**
     * Diese Methode f�llt FN-Typen aus einer Liste in eine Adresse.
     * 
     * @param fnTypes Liste von FN-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillFnTypesIntoAddress(List fnTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itFns = fnTypes.iterator();
        while (itFns.hasNext())
            moreData.append("VCARD[FN]: ")
                    .append(((FN)itFns.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt GEO-Typen aus einer Liste in eine Adresse.
     * 
     * @param geoTypes Liste von GOE-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillGeoTypesIntoAddress(List geoTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itGeos = geoTypes.iterator();
        while (itGeos.hasNext()) {
            GEO geo = (GEO) itGeos.next();
            moreData.append("VCARD[GEO]: L�nge ")
                    .append(geo.getLongitude())
                    .append(", Breite ")
                    .append(geo.getLatitude())
                    .append('\n');
        }
    }

    /**
     * Diese Methode f�llt KEY-Typen aus einer Liste in eine Adresse.
     * 
     * @param keyTypes Liste von KEY-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillKeyTypesIntoAddress(List keyTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itKeys = keyTypes.iterator();
        while (itKeys.hasNext())
            moreData.append("VCARD[KEY]: ")
                    .append(((KEY)itKeys.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt LABEL-Typen aus einer Liste in eine Adresse.
     * 
     * @param labelTypes Liste von LABEL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillLabelTypesIntoAddress(List labelTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itLabels = labelTypes.iterator();
        while (itLabels.hasNext()) {
            LABEL label = (LABEL) itLabels.next();
            LABEL.Parameters params = (LABEL.Parameters) label.getParameters();
            moreData.append("VCARD[LABEL");
            if (params.containsTYPE(LABEL.Parameters.TYPE_DOM)) moreData.append(" DOM");
            if (params.containsTYPE(LABEL.Parameters.TYPE_HOME)) moreData.append(" HOME");
            if (params.containsTYPE(LABEL.Parameters.TYPE_INTL)) moreData.append(" INTL");
            if (params.containsTYPE(LABEL.Parameters.TYPE_PARCEL)) moreData.append(" PARCEL");
            if (params.containsTYPE(LABEL.Parameters.TYPE_POSTAL)) moreData.append(" POSTAL");
            if (params.containsTYPE(LABEL.Parameters.TYPE_PREF)) moreData.append(" PREF");
            if (params.containsTYPE(LABEL.Parameters.TYPE_WORK)) moreData.append(" WORK");
            moreData.append("]: ")
                    .append(label.get()) // TODO: Encoding beachten
                    .append('\n');
        }
    }

    /**
     * Diese Methode f�llt LOGO-Typen aus einer Liste in eine Adresse.
     * 
     * @param logoTypes Liste von LOGO-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillLogoTypesIntoAddress(List logoTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itLogos = logoTypes.iterator();
        while (itLogos.hasNext()) {
            LOGO logo = (LOGO) itLogos.next();
            LOGO.Parameters params = (LOGO.Parameters) logo.getParameters();
            moreData.append("VCARD[LOGO");
            if (params.containsENCODING(LOGO.Parameters.ENCODING_B)) moreData.append(" ENCODING:B");
            if (params.containsVALUE(LOGO.Parameters.VALUE_URI)) moreData.append(" VALUE:URI");
            moreData.append("]: ")
                    .append(logo.get()) // TODO: Encoding beachten
                    .append('\n');
        }
    }

    /**
     * Diese Methode f�llt MAILER-Typen aus einer Liste in eine Adresse.
     * 
     * @param mailerTypes Liste von MAILER-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillMailerTypesIntoAddress(List mailerTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itMailers = mailerTypes.iterator();
        while (itMailers.hasNext())
            moreData.append("VCARD[MAILER]: ")
                    .append(((MAILER)itMailers.next()).get())
                    .append('\n');
    }
    
    /**
     * Diese Methode f�llt N-Typen aus einer Liste in eine Adresse.
     * 
     * @param nTypes Liste von N-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillNTypesIntoAddress(List nTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itNs = nTypes.iterator();
        if (itNs.hasNext()) {
            N n = (N) itNs.next();
            address.setNachname(n.getFamily());
            address.setVorname(n.getGiven());
            address.setWeitereVornamen(n.getOther());
            address.setTitel(n.getPrefix());
            address.setNamenszusatz(n.getSuffix());
        }
        while (itNs.hasNext()) {
            N n = (N) itNs.next();
            moreData.append("VCARD[N]:\n");
            appendPrefixedConditionally(moreData, "- Nachname ", n.getFamily());
            appendPrefixedConditionally(moreData, "- Vorname  ", n.getGiven());
            appendPrefixedConditionally(moreData, "- weitere  ", n.getOther());
            appendPrefixedConditionally(moreData, "- Titel    ", n.getPrefix());
            appendPrefixedConditionally(moreData, "- Zusatz   ", n.getSuffix());
        }
    }
    
    /**
     * Diese Methode f�llt NICKNAME-Typen aus einer Liste in eine Adresse.
     * 
     * @param nicknameTypes Liste von NICKNAME-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillNicknameTypesIntoAddress(List nicknameTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itNicknames = nicknameTypes.iterator();
        StringBuffer buffer = new StringBuffer();
        while (itNicknames.hasNext()) {
            NICKNAME nickname = (NICKNAME) itNicknames.next();
            for (int i = 0; i < nickname.size(); i++)
                buffer.append(' ').append(nickname.get(i));
        }
        address.setSpitzname(buffer.toString().trim());
    }
    
    /**
     * Diese Methode f�llt NOTE-Typen aus einer Liste in eine Adresse.
     * 
     * @param noteTypes Liste von NOTE-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillNoteTypesIntoAddress(List noteTypes, Address address, StringBuffer moreData) throws ContactDBException {
        // do not export the "per category data any more"
        //         Iterator itNotes = noteTypes.iterator();
        //         if (itNotes.hasNext())
        //             address.setBemerkung(((NOTE)itNotes.next()).get());
        //         while (itNotes.hasNext())
        //             moreData.append("VCARD[NOTE]: ")
        //                     .append(((NOTE)itNotes.next()).get())
        //                     .append('\n');
    }

    /**
     * Diese Methode f�llt ORG-Typen aus einer Liste in eine Adresse.
     * 
     * @param orgTypes Liste von ORG-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillOrgTypesIntoAddress(List orgTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itOrgs = orgTypes.iterator();
        if (itOrgs.hasNext()) {
            ORG org = (ORG) itOrgs.next();
            address.setInstitution(org.getOrgname());
            address.setAbteilung(org.getOrgunit());
        }
        while (itOrgs.hasNext()) {
            ORG org = (ORG) itOrgs.next();
            moreData.append("VCARD[ORG]:\n");
            appendPrefixedConditionally(moreData, "- Name      ", org.getOrgname());
            appendPrefixedConditionally(moreData, "- Abteilung ", org.getOrgunit());
        }
    }
    
    /**
     * Diese Methode f�llt PHOTO-Typen aus einer Liste in eine Adresse.
     * 
     * @param photoTypes Liste von PHOTO-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillPhotoTypesIntoAddress(List photoTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itPhotos = photoTypes.iterator();
        while (itPhotos.hasNext()) {
            PHOTO photo = (PHOTO) itPhotos.next();
            PHOTO.Parameters params = (PHOTO.Parameters) photo.getParameters();
            moreData.append("VCARD[PHOTO");
            if (params.containsENCODING(PHOTO.Parameters.ENCODING_B)) moreData.append(" ENCODING:B");
            if (params.containsVALUE(PHOTO.Parameters.VALUE_URI)) moreData.append(" VALUE:URI");
            moreData.append("]: ")
                    .append(photo.get()) // TODO: Encoding beachten
                    .append('\n');
        }
    }
    
    /**
     * Diese Methode f�llt PROID-Typen aus einer Liste in eine Adresse.
     * (Ist dies ein fehlgelaufenes PRODID?)
     * 
     * @param proidTypes Liste von PROID-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillProidTypesIntoAddress(List proidTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itProids = proidTypes.iterator();
        while (itProids.hasNext())
            moreData.append("VCARD[PROID]: ")
                    .append(((PROID)itProids.next()).get())
                    .append('\n');
    }
    
    /**
     * Diese Methode f�llt REV-Typen aus einer Liste in eine Adresse.
     * 
     * @param revTypes Liste von REV-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillRevTypesIntoAddress(List revTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itRevs = revTypes.iterator();
        while (itRevs.hasNext())
            moreData.append("VCARD[REV]: ")
                    .append(((REV)itRevs.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt ROLE-Typen aus einer Liste in eine Adresse.
     * 
     * @param roleTypes Liste von ROLE-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillRoleTypesIntoAddress(List roleTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itRoles = roleTypes.iterator();
//        if (itRoles.hasNext())
//            address.setPosition(((ROLE)itRoles.next()).get());
        while (itRoles.hasNext())
            moreData.append("VCARD[ROLE]: ")
                    .append(((ROLE)itRoles.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt SORT-STRING-Typen aus einer Liste in eine Adresse.
     * 
     * @param sortStringTypes Liste von SORT-STRING-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillSortStringTypesIntoAddress(List sortStringTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itSortStrings = sortStringTypes.iterator();
        while (itSortStrings.hasNext())
            moreData.append("VCARD[SORT-STRING]: ")
                    .append(((SORT_STRING)itSortStrings.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt SOUND-Typen aus einer Liste in eine Adresse.
     * 
     * @param soundTypes Liste von SOUND-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillSoundTypesIntoAddress(List soundTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itSounds = soundTypes.iterator();
        while (itSounds.hasNext()) {
            SOUND photo = (SOUND) itSounds.next();
            SOUND.Parameters params = (SOUND.Parameters) photo.getParameters();
            moreData.append("VCARD[SOUND");
            if (params.containsENCODING(SOUND.Parameters.ENCODING_B)) moreData.append(" ENCODING:B");
            if (params.containsVALUE(SOUND.Parameters.VALUE_URI)) moreData.append(" VALUE:URI");
            moreData.append("]: ")
                    .append(photo.get()) // TODO: Encoding beachten
                    .append('\n');
        }
    }
    
    /**
     * Diese Methode f�llt TEL-Typen aus einer Liste in eine Adresse.
     * Genauer wird in dieser Methode in Handy-, Fax-, Telefon- und
     * sonstige Nummern unterteilt, die dann wiederum separat behandelt
     * werden. 
     * 
     * @param telTypes Liste von TEL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillTelTypesIntoAddress(List telTypes, Address address, StringBuffer moreData) throws ContactDBException {
        List cellTypes = new ArrayList();
        List faxTypes = new ArrayList();
        List voiceTypes = new ArrayList();
        Iterator itTels = telTypes.iterator();
        while(itTels.hasNext()) {
            TEL tel = (TEL) itTels.next();
            TEL.Parameters params = (TEL.Parameters) tel.getParameters();
            boolean hasKindInfo = false;
            if (params.containsTYPE(TEL.Parameters.TYPE_CELL) ||
                params.containsTYPE(TEL.Parameters.TYPE_CAR)) {
                hasKindInfo = true;
                cellTypes.add(tel);
            } else if (params.containsTYPE(TEL.Parameters.TYPE_VOICE) ||
                params.containsTYPE(TEL.Parameters.TYPE_MSG) ||
                params.containsTYPE(TEL.Parameters.TYPE_VIDEO)) {
                hasKindInfo = true;
                voiceTypes.add(tel);
            }
            if (params.containsTYPE(TEL.Parameters.TYPE_FAX)) {
                hasKindInfo = true;
                faxTypes.add(tel);
            }
            if (params.containsTYPE(TEL.Parameters.TYPE_PAGER) ||
                params.containsTYPE(TEL.Parameters.TYPE_BBS) ||
                params.containsTYPE(TEL.Parameters.TYPE_MODEM)) {
                hasKindInfo = true;
            }
            if (!hasKindInfo)
                voiceTypes.add(tel);
        }
        Set usedTypes = new HashSet();
        fillVoiceTelTypesIntoAddress(voiceTypes, address, usedTypes);
        fillFaxTelTypesIntoAddress(faxTypes, address, usedTypes);
        fillCellTelTypesIntoAddress(cellTypes, address, usedTypes);
        
        itTels = telTypes.iterator();
        while(itTels.hasNext()) {
            TEL tel = (TEL) itTels.next();
            if (usedTypes.contains(tel)) continue;
            TEL.Parameters params = (TEL.Parameters) tel.getParameters();
            moreData.append("VCARD[TEL");
            if (params.containsTYPE(TEL.Parameters.TYPE_BBS)) moreData.append(" BBS");
            if (params.containsTYPE(TEL.Parameters.TYPE_CAR)) moreData.append(" CAR");
            if (params.containsTYPE(TEL.Parameters.TYPE_CELL)) moreData.append(" CELL");
            if (params.containsTYPE(TEL.Parameters.TYPE_FAX)) moreData.append(" FAX");
            if (params.containsTYPE(TEL.Parameters.TYPE_HOME)) moreData.append(" HOME");
            if (params.containsTYPE(TEL.Parameters.TYPE_ISDN)) moreData.append(" ISDN");
            if (params.containsTYPE(TEL.Parameters.TYPE_MODEM)) moreData.append(" MODEM");
            if (params.containsTYPE(TEL.Parameters.TYPE_MSG)) moreData.append(" MSG");
            if (params.containsTYPE(TEL.Parameters.TYPE_PAGER)) moreData.append(" PAGER");
            if (params.containsTYPE(TEL.Parameters.TYPE_PCS)) moreData.append(" PCS");
            if (params.containsTYPE(TEL.Parameters.TYPE_PREF)) moreData.append(" PREF");
            if (params.containsTYPE(TEL.Parameters.TYPE_VIDEO)) moreData.append(" VIDEO");
            if (params.containsTYPE(TEL.Parameters.TYPE_VOICE)) moreData.append(" VOICE");
            if (params.containsTYPE(TEL.Parameters.TYPE_WORK)) moreData.append(" WORK");
            moreData.append("]: ")
                    .append(tel.get())
                    .append('\n');
        }
    }
    
    /**
     * Diese Methode f�llt ausgew�hlte Voice-TEL-Typen aus einer Liste in
     * eine Adresse und gibt die benutzten zur�ck.
     * 
     * @param telTypes Liste von Voice-TEL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param usedTypes Menge benutzter Voice-Typen; diese Menge
     *  wird hier durch die genutzten erg�nzt. 
     */
    private static void fillVoiceTelTypesIntoAddress(List telTypes, Address address, Set usedTypes) throws ContactDBException {
        TEL[] chosen = new TEL[2];
        chooseTelTypes(telTypes, chosen);
        TEL work = chosen[0];
        TEL home = chosen[1];
        if (work != null) {
            address.setTelefonDienstlich(work.get());
            usedTypes.add(work);
        }
        if (home != null) {
            address.setTelefonPrivat(home.get());
            usedTypes.add(home);
        }
    }

    /**
     * Diese Methode f�llt ausgew�hlte Fax-TEL-Typen aus einer Liste in
     * eine Adresse und gibt die benutzten zur�ck.
     * 
     * @param telTypes Liste von Fax-TEL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param usedTypes Menge benutzter Voice-Typen; diese Menge
     *  wird hier durch die genutzten erg�nzt. 
     */
    private static void fillFaxTelTypesIntoAddress(List telTypes, Address address, Set usedTypes) throws ContactDBException {
        TEL[] chosen = new TEL[2];
        chooseTelTypes(telTypes, chosen);
        TEL work = chosen[0];
        TEL home = chosen[1];
        if (work != null) {
            address.setFaxDienstlich(work.get());
            usedTypes.add(work);
        }
        if (home != null) {
            address.setFaxPrivat(home.get());
            usedTypes.add(home);
        }
    }

    /**
     * Diese Methode f�llt ausgew�hlte Cell-TEL-Typen aus einer Liste in
     * eine Adresse und gibt die benutzten zur�ck.
     * 
     * @param telTypes Liste von Cell-TEL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param usedTypes Menge benutzter Voice-Typen; diese Menge
     *  wird hier durch die genutzten erg�nzt. 
     */
    private static void fillCellTelTypesIntoAddress(List telTypes, Address address, Set usedTypes) throws ContactDBException {
        TEL[] chosen = new TEL[2];
        chooseTelTypes(telTypes, chosen);
        TEL work = chosen[0];
        TEL home = chosen[1];
        if (work != null) {
            address.setHandyDienstlich(work.get());
            usedTypes.add(work);
        }
        if (home != null) {
            address.setHandyPrivat(home.get());
            usedTypes.add(home);
        }
    }

    /**
     * Diese Methode w�hlt aus einer TEL-Typen-Liste eine WORK- und eine HOME-Variante
     * aus.
     * @param telTypes Liste von TEL-Typen
     * @param chosen Ausgabe-Array, auf 0 wird die WORK-Variante gelegt und auf
     *  1 die HOME-Variante.
     */
    private static void chooseTelTypes(List telTypes, TEL[] chosen) {
        TEL work = null;
        boolean workPref = false;
        TEL home = null;
        boolean homePref = false;
        TEL neither = null;
        boolean neitherPref = false;
        Iterator itTels = telTypes.iterator();
        while (itTels.hasNext()) {
            TEL tel = (TEL) itTels.next();
            TEL.Parameters params = (TEL.Parameters) tel.getParameters();
            boolean isHome = params.containsTYPE(TEL.Parameters.TYPE_HOME);
            boolean isWork = params.containsTYPE(TEL.Parameters.TYPE_WORK);
            boolean isPref = params.containsTYPE(TEL.Parameters.TYPE_PREF);
            if (isWork && (work == null || (isPref && !workPref)))
                work = tel;
            if (isHome && (home == null || (isPref && !homePref)))
                home = tel;
            if ((!(isHome || isWork)) && (neither == null || (isPref && !neitherPref)))
                neither = tel;
        }
        chosen[0] = (work == null) ? neither : work;
        chosen[1] = (home == null) ? neither : home;
    }
    
    /**
     * Diese Methode f�llt TITLE-Typen aus einer Liste in eine Adresse.
     * 
     * @param titleTypes Liste von TITLE-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillTitleTypesIntoAddress(List titleTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itTitles = titleTypes.iterator();
        if (itTitles.hasNext())
            address.setPosition(((TITLE)itTitles.next()).get());
        while (itTitles.hasNext())
            moreData.append("VCARD[TITLE]: ")
                    .append(((TITLE)itTitles.next()).get())
                    .append('\n');
    }
    
    /**
     * Diese Methode f�llt TZ-Typen aus einer Liste in eine Adresse.
     * 
     * @param tzTypes Liste von TZ-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillTzTypesIntoAddress(List tzTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itTzs = tzTypes.iterator();
        while (itTzs.hasNext())
            moreData.append("VCARD[TZ]: ")
                    .append(((TZ)itTzs.next()).get())
                    .append('\n');
    }
    
    /**
     * Diese Methode f�llt UID-Typen aus einer Liste in eine Adresse.
     * 
     * @param uidTypes Liste von UID-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillUidTypesIntoAddress(List uidTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itUids = uidTypes.iterator();
        while (itUids.hasNext())
            moreData.append("VCARD[UID]: ")
                    .append(((UID)itUids.next()).get())
                    .append('\n');
    }
    
    /**
     * Diese Methode f�llt URL-Typen aus einer Liste in eine Adresse.
     * 
     * @param urlTypes Liste von URL-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillUrlTypesIntoAddress(List urlTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itUrls = urlTypes.iterator();
        if (itUrls.hasNext())
            address.setHomepageDienstlich(((URL)itUrls.next()).get());
        while (itUrls.hasNext())
            moreData.append("VCARD[URL]: ")
                    .append(((URL)itUrls.next()).get())
                    .append('\n');
    }
    
    /**
     * Diese Methode f�llt VERSION-Typen aus einer Liste in eine Adresse.
     * 
     * @param versionTypes Liste von VERSION-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillVersionTypesIntoAddress(List versionTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itVersions = versionTypes.iterator();
        while (itVersions.hasNext())
            moreData.append("VCARD[VERSION]: ")
                    .append(((VERSION)itVersions.next()).get())
                    .append('\n');
    }

    /**
     * Diese Methode f�llt X-Typen aus einer Liste in eine Adresse.
     * Hier landen alle nicht-erkannten Typen.
     * 
     * @param xTypes Liste von X-Typen
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillXTypesIntoAddress(List xTypes, Address address, StringBuffer moreData) throws ContactDBException {
        Iterator itXs = xTypes.iterator();
        while (itXs.hasNext()) {
            X x = (X)itXs.next();
            moreData.append("VCARD[X - ")
                    .append(x.getName())
                    .append("]: ")
                    .append(x.get())
                    .append('\n');
        }
    }

    /**
     * Diese Methode tr�gt in die Zusatzdaten Infos zu einem sonstigen Type ein.
     * 
     * @param otherTypes homogene Liste eines sonstigen Typs.
     * @param address zu f�llendes Adressobjekt
     * @param moreData Buffer f�r Zusatzdaten
     */
    private static void fillOtherTypesIntoAddress(List otherTypes, Address address, StringBuffer moreData) {
        if (otherTypes.size() > 0) {
            Type type = (Type) otherTypes.get(0);
            moreData.append("VCARD[")
                    .append(type.getName())
                    .append("]: ")
                    .append(otherTypes.size())
                    .append(otherTypes.size() > 1 ? " Eintr�ge\n" : " Eintrag\n")
                    .append(type.getClass().getName()).append('\n');
        }
    }

    /**
     * Diese Methode gibt eine VCARD-Wert-Liste oder einen VCARD-Einzelwert aus.
     *  
     * @param value eine List-Instanz (dann wird dies als Liste ausgegeben) oder
     *  ein sonstiges Objekt (wird als Einzelwert ausgegeben).
     * @param escapeColon Flag, ob ':' escape-t werden soll.
     * @param writer VCARD-Ausgabe
     */
    private static void writeVcardListOrSimple(Object value, PrintWriter writer, boolean escapeColon) {
        if (value instanceof List) {
            List list = (List) value;
            Iterator itList = list.iterator();
            if (itList.hasNext())
                writer.print(escapeVcardString(itList.next().toString(), escapeColon));
            while (itList.hasNext()) {
                writer.print(',');
                writer.print(escapeVcardString(itList.next().toString(), escapeColon));
            }
        } else if (value != null)
            writer.print(escapeVcardString(value.toString(), escapeColon));
    }

    /**
     * Diese Methode escape-t in einem String die kritischen Zeichen
     * ',', ';', ':', '\' und NEWLINE nach VCARD-Art.
     * 
     * @param string der String mit potentiell gef�hrlichen Zeichen.
     * @param escapeColon Flag, ob ':' escape-t werden soll.
     * @return String mit escape-ten kritischen Zeichen
     */
    private static String escapeVcardString(String string, boolean escapeColon) {
        StringBuffer result = new StringBuffer();
        for(int i = 0; i < string.length(); i++)
            switch(string.charAt(i)) {
            case '\n':
                result.append("\\n");
                break;
            case ':':
                if (!escapeColon) {
                    result.append(':');
                    break;
                }
            case ',':
            case ';':
            case '\\':
                result.append('\\');
            default:
                result.append(string.charAt(i));
            }
        return result.toString();
    }

    /**
     * Diese Methode erg�nzt einen Namensanfang um den n�chsten Namensteil.
     * 
     * @param buffer Buffer, der den bisher erzeugten Namen h�lt. 
     * @param part n�chster hinzuzuf�gender Namensteil.
     */
    private static void appendNamePart(StringBuffer buffer, String part) {
        if (part != null && part.length() != 0) {
            if (buffer.length() > 0)
                buffer.append(' ');
            buffer.append(part);
        }
    }

    //
    // private Hilfsvariablen
    //
    /** VCARD-Datumsformatierer */
    private final static SimpleDateFormat VCARD_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd"); 
    /** VCARD-DatumZeitformatierer */
    private final static SimpleDateFormat VCARD_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"); 
    /** der Logger dieser Klasse */
    private final static Logger logger = Logger.getLogger(VcardUtil.class.getPackage().getName());
    /** ADR-Typ-Untertypen-Priorit�tenliste */
    private final static int[] ADR_TYPE_PRIORITIES = new int[] {
        ADR.Parameters.TYPE_PREF, ADR.Parameters.TYPE_WORK,
        ADR.Parameters.TYPE_HOME, ADR.Parameters.TYPE_POSTAL,
        ADR.Parameters.TYPE_PARCEL, ADR.Parameters.TYPE_INTL,
        ADR.Parameters.TYPE_DOM };
    /** EMAIL-Typ-Untertypen-Priorit�tenliste */
    private final static int[] EMAIL_TYPE_PRIORITIES = new int[] {
        EMAIL.Parameters.TYPE_PREF, EMAIL.Parameters.TYPE_INTERNET,
        EMAIL.Parameters.TYPE_X400 };
    //
    // statische Initialisierung
    //
    static {
        logger.setLevel(Level.ALL);
    }
}
