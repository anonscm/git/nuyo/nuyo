/*
 * Created on 13.10.2004
 *
 */
package org.evolvis.nuyo.util.general;

/**
 * @author niko
 *
 * Diese Exception wird vom DataConverter geworfen
 * wenn eine Konvertierung nicht m�glich war.
 */
public class DataConversionException extends Exception
{
  public DataConversionException(Class src, Class dst)
  {
    super(createText(src, dst));
  }

  public DataConversionException(Class src, Class dst, String message)
  {
    super(createText(src, dst) + ": " + message);
  }

  public DataConversionException(Class src, Class dst, Throwable cause)
  {
    super(createText(src, dst), cause);
  }

  public DataConversionException(Class src, Class dst, String message, Throwable cause)
  {
    super(createText(src, dst) + ": " + message, cause);
  }

  private static String createText(Class src, Class dst)
  {    
    return "unable to convert from " + src.getName() + " to " + dst.getName();
  }
}
