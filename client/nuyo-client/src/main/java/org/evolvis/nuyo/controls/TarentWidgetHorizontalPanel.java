/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * @author niko
 */
public class TarentWidgetHorizontalPanel extends JPanel implements TarentWidgetInterface
{
  private ProcentualLayout  m_oProcentualLayout;

  public TarentWidgetHorizontalPanel()
  {
    super();
    m_oProcentualLayout = new ProcentualLayout();
    this.setLayout(m_oProcentualLayout);
  }

  public void addWidget(TarentWidgetInterface widget, int procentualwidth)
  {
    this.add(widget.getComponent());
    m_oProcentualLayout.setProcentualWidth(widget.getComponent(), procentualwidth);
  }

  public void addWidget(TarentWidgetInterface widget, int procentualwidth, int specialgap)
  {
    this.add(widget.getComponent());
    m_oProcentualLayout.setProcentualWidth(widget.getComponent(), procentualwidth);
    m_oProcentualLayout.setSpecialGapX(widget.getComponent(), specialgap);
  }

  public void addWidget(EntryLayoutHelper labeledwidget, int procentualtitlewidth, int procentualentrywidth)
  {
    this.addWidget(labeledwidget.getTitleComponent(), procentualtitlewidth);
    this.addWidget(labeledwidget.getEntryComponent(), procentualentrywidth);
  }

  public void addWidget(EntryLayoutHelper labeledwidget, int procentualtitlewidth, int specialtitlegap, int procentualentrywidth, int specialentrygap)
  {
    this.addWidget(labeledwidget.getTitleComponent(), procentualtitlewidth);
    this.addWidget(labeledwidget.getEntryComponent(), procentualentrywidth);
  }

	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
	}

	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
		return null;
	}

  public JComponent getComponent()
  {
    return(this);
  }

  public void setLengthRestriction(int maxlen)
  {
  }

  public void setWidgetEditable(boolean iseditable)
  {
  }
  
  public boolean isEqual(Object data)
  {
    return(false);
  }
  
  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  
}
