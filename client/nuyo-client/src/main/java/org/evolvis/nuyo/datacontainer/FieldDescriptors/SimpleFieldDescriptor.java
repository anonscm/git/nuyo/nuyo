/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.FieldDescriptors;

import java.util.List;

/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SimpleFieldDescriptor extends FieldDescriptorAdapter
{
  public FieldDescriptor cloneFieldDescriptor()
  {
    SimpleFieldDescriptor fielddescriptor = new SimpleFieldDescriptor();

    fielddescriptor.m_oDataContainer = m_oDataContainer;

    fielddescriptor.m_sName = m_sName;
    fielddescriptor.m_sDescription = m_sDescription;
    fielddescriptor.m_sShortDescription = m_sShortDescription;
    fielddescriptor.m_cKey = m_cKey;
    return fielddescriptor;
  }
  
  
  
  public String getCurrentStatusText()
  {
    return getName();
  }

  public List getEventsConsumable()
  {
    return null;
  }

  public List getEventsFireable()
  {
    return null;
  }

  public void init()
  {
  }

  public void dispose()
  {
  }

}
