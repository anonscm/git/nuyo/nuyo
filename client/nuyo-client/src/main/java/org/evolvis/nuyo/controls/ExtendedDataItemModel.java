/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.ImageIcon;

/**
 * @author niko
 */
public class ExtendedDataItemModel 
{
  private String         m_sTitle;
  private String         m_sToolTip;
  private ImageIcon m_oIcon;

  private ArrayList   m_oLabeledComponents;


  private ExtendedDataItemModel(String title, String tooltip, ArrayList components, Color bgcol, Color fgcol, Color cbgcol, Font font)
  {
    m_sTitle                      = title;
    m_sToolTip                    = tooltip;
    m_oLabeledComponents          = components;
    m_oIcon = null;
  }

  public ExtendedDataItemModel(String title, String tooltip, Color bgcol, Color fgcol, Color cbgcol, Font font)
  {
    this(title, tooltip, new ArrayList(), bgcol, fgcol, cbgcol, font);
  }

  public ExtendedDataItemModel(String title)
  {
    this(title, "", new ArrayList(), Color.WHITE, Color.BLACK, Color.GRAY, new Font(null, Font.PLAIN, 12));
  }

  public ExtendedDataItemModel()
  {
    this("", "", Color.WHITE, Color.BLACK, Color.GRAY, new Font(null, Font.PLAIN, 12));
  }

  // -----------------------------------------------

  public void setIcon(ImageIcon icon)
  {
    m_oIcon = icon;
  }

  public ImageIcon getIcon()
  {
    return(m_oIcon);
  }



  public void setTitle(String title)
  {
    m_sTitle = title;
  }

  public void setToolTip(String text)
  {
    m_sToolTip = text;
  }

  public String getTitle()
  {
    return(m_sTitle);
  }

  public String getToolTip()
  {
    return(m_sToolTip);
  }

  public void addLabeledComponent(ExtendedDataLabeledComponent lc)
  {
    m_oLabeledComponents.add(lc);
  }
  
  public void removeLabeledComponent(ExtendedDataLabeledComponent lc)
  {
    m_oLabeledComponents.remove(lc);
  }
  
  public void removeAllLabeledComponents()
  {
    m_oLabeledComponents.clear();
  }
  
  public int getNumberOfExtendedDataLabeledComponents()
  {
    return( m_oLabeledComponents.size() );
  }  
  
  public ExtendedDataLabeledComponent getExtendedDataLabeledComponent(int index)
  {
    return( (ExtendedDataLabeledComponent)(m_oLabeledComponents.get(index)) );
  }  
}
