

package org.evolvis.nuyo.gui.action.journal;

import java.awt.Component;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.HistoryElement;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.octopus.ContactBean.ContactUtils;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Context;

class JournalViewerModel
{
  static TarentLogger logger = new TarentLogger(JournalViewerModel.class);

  DefaultListModel journalEntries = new DefaultListModel();

  Renderer renderer = new Renderer();
  
  private Address addr;

  String address;

  String title;

  String date;

  String category;

  String channel;

  String user;

  String direction;

  String duration;

  String note;

  Listener listener;

  JournalViewerModel(Listener l)
  {
    listener = l;
    BindingManager bm = ApplicationServices.getInstance().getBindingManager();

    bm.addBinding(new AddressBinding());
    bm.addBinding(new JournalEntriesBinding());
  }

  void addJournalEntry(HistoryElement entry)
  {
    journalEntries.addElement(entry);
  }

  void removeAllJournalEntries()
  {
    journalEntries.clear();
  }

  ListCellRenderer getRenderer()
  {
    return renderer;
  }

  ListModel getListModel()
  {
    return journalEntries;
  }

  /**
   * Sets the currently selected journal entry which allows the outside to
   * access the entries properties afterwards.
   * @param entry
   */
  void setSelectedEntry(HistoryElement entry)
  {
    if (entry instanceof Contact)
      {
        Contact c = (Contact) entry;
        try
          {
            title = c.getAttribute(Contact.KEY_SUBJECT);
            direction = ContactUtils.generateDirectionString(c.isInbound());
            channel = ContactUtils.generateChannelString(c.getChannel());

            duration = String.valueOf(c.getDuration());

            category = ContactUtils.generateCategoryString(c.getCategory());
            note = c.getAttribute(Contact.KEY_NOTE);
            user = c.getUser().getLoginName();
          }
        catch (ContactDBException e)
          {
            // Very unlikely as data was pushed into the component ...
            throw new IllegalStateException(e);
          }
      }
    else if (entry instanceof Appointment)
      {
        Appointment a = (Appointment) entry;
        try
          {
            title = a.getAttribute(Appointment.KEY_SUBJECT);
            direction = Messages.getString("GUI_JournalViewer_NotApplicable");
            channel = ContactUtils.generateChannelString(Contact.CHANNEL_DIRECT);
            
            duration = a.getAttribute(Appointment.KEY_DURATION);
            
            user = Messages.getString("GUI_JournalViewer_NotApplicable");
            category = Messages.getString("GUI_JournalViewer_NotApplicable");
            note = a.getAttribute(Appointment.KEY_DESCRIPTION);
          }
        catch (ContactDBException e)
          {
            // Very unlikely as data was pushed into the component ...
            throw new IllegalStateException(e);
          }
      }
    else if (entry instanceof MailBatch)
      {
        MailBatch mb = (MailBatch) entry;
        title = mb.getName();
        direction = Messages.getString("GUI_JournalViewer_NotApplicable");
        channel = direction;
        duration = direction;
        user = mb.getInitiatorName();
        category = direction;
        note = direction;
      }
    else if (entry == null)
      {
        title = direction = channel = duration = category = note = user = "";
      }
    else
      {
        logger.warningSilent("Unknown journal entry class: " + entry.getClass());
      }
    
    if (entry != null)
      date = StartUpConstants.DATE_FORMAT.format(entry.getHistoryDate()) + " ";
    
    // Some fields may now be null, check that and set them to ""
    // to prevent the user from seeing "null" printed.
    if (note == null)
      note = "";

    listener.update();
  }

  private static class Renderer extends DefaultListCellRenderer
  {
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus)
    {
      super.getListCellRendererComponent(list, value, index, isSelected,
                                         cellHasFocus);

      HistoryElement entry = (HistoryElement) value;

      String text = StartUpConstants.DATE_FORMAT.format(entry.getHistoryDate())
                    + " ";
      if (value instanceof Contact)
        {
          Contact c = (Contact) value;
          try
            {
              text += c.getAttribute(Contact.KEY_SUBJECT);
            }
          catch (ContactDBException e)
            {
              // Very unlikely as data was pushed into the component ...
              throw new IllegalStateException(e);
            }
        }
      else if (value instanceof Appointment)
        {
          Appointment a = (Appointment) value;
          try
            {
              text += a.getAttribute(Appointment.KEY_SUBJECT);
            }
          catch (ContactDBException e)
            {
              // Very unlikely as data was pushed into the component ...
              throw new IllegalStateException(e);
            }
        }
      else if (value instanceof MailBatch)
        {
          MailBatch mb = (MailBatch) value;
          text += mb.getName();
        }
      else
        {
          logger.warningSilent("Unknown journal entry class: "
                               + value.getClass());
        }

      setText(text);

      return this;
    }
  }
  
  void reset()
  {
    title = direction = channel = duration = category = note = user = "";
  }

  private final class AddressBinding extends AbstractReadOnlyBinding
  {
    AddressBinding()
    {
      super(ApplicationModel.SELECTED_ADDRESS_KEY);
    }

    /**
     * Gets called whenever the selected address changes. It in turn refreshes
     * the address label.
     */
    public void setViewData(Object o)
    {
      if (o == null)
        return;
      
      addr = (Address) o;

      // This generates an address label in the form:
      // prename name, street #nr, postcode city
      // This is a simple solution but is otherwise
      // hardcoded and therefore bad.
      // TODO: Write a helper class that retrieves often
      // used combinations of the address property values. 
      address = addr.getVorname() + " " + addr.getNachname() + ", " + addr.getStrasse() + ", " + addr.getPlz() + " " + addr.getOrt();
    }
  }

  private final class JournalEntriesBinding extends AbstractReadOnlyBinding
  {
    JournalEntriesBinding()
    {
      super(ApplicationModel.JOURNAL_ENTRY_LIST_KEY);
    }

    public void setViewData(Object o)
    {
      removeAllJournalEntries();

      if (o == null)
        return;

      Iterator entries = ((List) o).iterator();
      while (entries.hasNext())
        addJournalEntry((HistoryElement) entries.next());
    }

  }

  static interface Listener
  {
    void update();
  }

  /**
   * Triggers the retrieval of the current address' journal
   * data.
   * 
   * <p>The retrieval is done as a {@link TaskManager.Task} at
   * whose end the gui is updated (by using the
   * {@link JournalViewerModel.Listener} instance).</p> 
   *
   */
  void updateJournal()
  {
    TaskManager.getInstance().registerBlocking(new TaskManager.SwingTask()
    {
      protected void runImpl(TaskManager.Context ctx)
      {
        ApplicationServices as = ApplicationServices.getInstance();
        ApplicationModel am = as.getApplicationModel();
        Database db = as.getCurrentDatabase();
           
        try
          {
            am.setJournalEntryList(db.getAddressDAO().getHistory(addr));
          }
        catch (ContactDBException e)
          {
            logger.warning("Unable to retrieve journal data");
          }
      }

      protected void failed(Context arg)
      {
      }

	  protected void succeeded(Context arg)
	  {
        listener.update();
	  }
    }, Messages.getString("GUI_JournalViewer_Updating"), false);
    
 }
 
}
