/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import org.evolvis.nuyo.controller.DoubleCheckManager;
import org.evolvis.nuyo.controls.TarentWidgetTextArea;


/**
 * @author niko
 *
 */
public abstract class ContactAddressTextAreaField extends ContactAddressField
{
  private FocusListener doubleCheckFocusListener = null;
  
  protected ContactAddressTextAreaField()
  {
  }
  
  public void setDoubleCheckSensitive(TarentWidgetTextArea ta, boolean issensitive)
  {
    if (issensitive) addTextAreaFocusListener(ta);
    else removeTextAreaFocusListener(ta);
  }
  
  protected TarentWidgetTextArea createTextArea(String toolTip, int lengthRestriction) 
  {
    TarentWidgetTextArea ta = new TarentWidgetTextArea("", lengthRestriction);
    ta.setLineWrap(true);
    ta.setWrapStyleWord(true);
    if (toolTip != null) ta.setToolTipText(toolTip);
    if (lengthRestriction > 0) ta.setLengthRestriction(lengthRestriction);
    return ta;
  }

  private void addTextAreaFocusListener(TarentWidgetTextArea ta)
  {
    if (doubleCheckFocusListener != null) removeTextAreaFocusListener(ta);

    doubleCheckFocusListener = new textarea_focus_doublecheck(ta); 
    ta.addFocusListener(doubleCheckFocusListener);
  }
  
  private void removeTextAreaFocusListener(TarentWidgetTextArea ta)
  {
    if (doubleCheckFocusListener != null) ta.removeFocusListener(doubleCheckFocusListener);
  }
  
  private class textarea_focus_doublecheck implements FocusListener
  {
    TarentWidgetTextArea textarea;  
    String text;

    public textarea_focus_doublecheck(TarentWidgetTextArea ta)
    {
      textarea = ta;            
    }
    
    public void focusGained(FocusEvent fe)
    {
      text = textarea.getText();    
    }
    
    public void focusLost(FocusEvent fe)
    {
      if (!(textarea.getText().equals(text)))
      {          
        // trigger DoublettenCheck
        DoubleCheckManager.getInstance().checkForDoublesAndHandleResults();       
      }
    }
  }          
  
}
