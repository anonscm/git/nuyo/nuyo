/**
 * TODO is this the right place?
 */
package org.evolvis.nuyo.gui.action;

import org.evolvis.nuyo.db.Addresses;

/**
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class AddressListProviderAdapter implements AddressListProvider
{
	Addresses addresses;
	
	public AddressListProviderAdapter(Addresses pAddresses)
	{
		addresses = pAddresses;
	}
	
	public Addresses getAddresses()
	{
		return addresses;
	}

	public void registerDataConsumer(Object object)
	{
		// TODO anything to do here?
	}

}
