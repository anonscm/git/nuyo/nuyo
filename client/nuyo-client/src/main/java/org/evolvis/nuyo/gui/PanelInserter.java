/*
 * Created on 22.03.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.controls.TarentPanelInterface;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Mail;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.gui.fieldhelper.AddressField;


/**
 * @author niko
 *
 */
public interface PanelInserter
{
  public void addContainers(ContainerProvider oContainerProvider);
  public List getContainerElements(String containername);
  public ContainerProvider getContainerProvider(String containername);
  public Map getContainerProviderMap();
  public List layoutElements();

  public AddressField getAdressField(Object key);
  
  public boolean setAddressFieldData(Object key, Object data);
  public TarentPanelInterface getContainer(Object key);
  public TarentPanelInterface getContainerDirect(Object key);
  public void postFinalRealize();
  
  public void getAddress(Address address, Address displayedaddress) throws ContactDBException;
  public boolean isDirty(Address address) throws ContactDBException;
  public void setEditable(Veto veto, boolean iseditable, Address displayedaddress);
  public void setVisibleContainerProvider(ContainerProvider provider);
  public ContainerProvider getVisibleContainerProvider();
  public void updateTab(Object container, boolean updateall, boolean overwrite, Address displayedaddress, Mail mail, User user);
  public void setUser(User user);
  public void startTimerTask(long iMillisecondsPerTick);

}
