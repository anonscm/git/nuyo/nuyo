

package org.evolvis.nuyo.gui.plaf;

import java.awt.Color;

import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * <p>This class provides a single method to modify the UI defaults
 * of the currently installed look and feel. By doing so it is 
 * possible to not depend on a certain look and feel class when overriding
 * or adding UI defaults.</p>
 * 
 * <p>Please add the defaults for components used by tarent-contact here.
 * For individual color schemes use a subclass.</p>
 * 
 * @author Robert Schuster
 */
public class TarentUIDefaultsModifier
{

  /**
   * Overridden to support customized colors, UIs, ..
   */
  public static void modifyUIDefaults()
  {
    // Please document your UI defaults changes and additions.
    UIDefaults defaults = UIManager.getLookAndFeelDefaults();

    // Colors and Borders for the sidemenu in tarent-contact
    defaults.put("tarent.sidemenu.menu.background", Color.WHITE);
    defaults.put("tarent.sidemenu.menu.border", new LineBorder(Color.BLACK, 1));
    defaults.put("tarent.sidemenu.item.background", Color.WHITE);
    defaults.put("tarent.sidemenu.item.border", new EmptyBorder(2, 2, 2, 2));
    
  }

}
