/**
 * 
 */
package org.evolvis.nuyo.plugins.search;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;
import org.evolvis.nuyo.gui.search.SearchFilterPanel;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.EscapeDialog;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SearchDialog extends EscapeDialog implements TarentSearch {

	protected JPanel mainPanel;

	protected JButton cancelButton, submitButton, resetButton;

	protected SearchModel model;

	protected CategoryTree categoryTree;

	protected SearchFilterPanel searchFilterPanel;

	protected boolean criteriaChanged = false;

	protected JLabel addressSourceLabel;
	
	protected Preferences preferences;
	
	protected List categoriesObjectList;
	
	public SearchDialog(Frame owner) {
		this(owner, ApplicationServices.getInstance().getActionManager().getPreferences("searchDialog"));
	}

	public SearchDialog(Frame owner, Preferences preferences) {
		this(owner, preferences,
				ApplicationServices.getInstance().getActionManager().getCategoriesObjectList());
	}
	
	public SearchDialog(Frame owner, Preferences preferences, List categoriesObjectList) {

		super(owner, Messages.getString("GUI_SearchDialog_Title"), true);
		this.preferences = preferences;
		this.categoriesObjectList = categoriesObjectList;

		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				SearchDialog.this.cancelSearchDialog();
			}

		});

		setContentPane(getMainPanel());
		setSize(720, 500);
		setLocationRelativeTo(owner);
	}

	protected JPanel getMainPanel() {
		if(mainPanel == null) {

			FormLayout layout = new FormLayout(
					"5dlu, pref:grow", // columns
			"pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:pref:grow, 10dlu, pref"); // rows

			PanelBuilder builder = new PanelBuilder(layout);
			builder.setDefaultDialogBorder();

			CellConstraints cc = new CellConstraints();

			builder.add(addressSourceLabel = createPlainLabel(Messages.getFormattedString("StatusBar_LABEL_Source", "")), cc.xyw(1, 1, 2));

			builder.add(createSeparator("Suchkriterien"), cc.xyw(1, 3, 2));

			JScrollPane pane = new JScrollPane(searchFilterPanel= SearchFilterPanel.createSearchDialogPanel());

			pane.setBorder(BorderFactory.createEmptyBorder());

			builder.add(pane, cc.xy(2, 5));

			builder.add(createSeparator("Kategorieauswahl"), cc.xyw(1, 7, 2));

			builder.add(categoryTree = CategoryTree.createSelectionCategoryTree(categoriesObjectList), cc.xy(2, 9));

			PanelBuilder pb = new PanelBuilder(new FormLayout("max(80dlu;pref), 3dlu, pref, 3dlu:grow, pref", "pref"));
			pb.add(getSubmitButton(), cc.xy(1, 1));
			pb.add(getResetButton(), cc.xy(3, 1));
			pb.add(getCancelButton(), cc.xy(5, 1));

			builder.add(pb.getPanel(), cc.xyw(1, 11, 2));

			mainPanel = builder.getPanel();
		}
		return mainPanel;
	}

	private JLabel createPlainLabel(String text) {
		JLabel label = new JLabel(text);
		label.setFont(label.getFont().deriveFont(Font.PLAIN));
		return label;
	}

	protected JButton getCancelButton() {
		if(cancelButton == null) {
			cancelButton = new JButton(Messages.getString("GUI_SearchDialog_Cancel"));
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent ae)
				{
					cancelSearchDialog();
				}
			});
			cancelButton.setIcon((ImageIcon)SwingIconFactory.getInstance().getIcon("process-stop.png"));
		}
		return cancelButton;
	}

	protected JButton getSubmitButton() {
		if(submitButton == null) {
			submitButton = new JButton(Messages.getString("GUI_SearchDialog_Search"));
			getRootPane().setDefaultButton(submitButton);
			submitButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent ae)
				{
					criteriaChanged = true;

					// Puts the new filter and selection states into
					// the model to allow updating the AddressFieldParameters
					// object in it later.
					model.setSelectionState(categoryTree.getState());
					model.setFilterState(searchFilterPanel.getState());

					setVisible(false);
				}
			});
			submitButton.setIcon((ImageIcon)SwingIconFactory.getInstance().getIcon("system-search.png"));
		}
		return submitButton;
	}

	protected JButton getResetButton() {
		if(resetButton == null) {
			// Reset button which unconditionally clears all the value parts in the
			// search filter components. E.g. "Mueller" in a search condition like this:
			// "Surname", "is", "Mueller"
			resetButton = new JButton(Messages.getString("GUI_SearchDialog_Reset"));
			resetButton.setToolTipText(Messages.getString("GUI_SearchDialog_Reset_ToolTip"));
			resetButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent ae)
				{
					searchFilterPanel.clearParams();
				}
			});
			resetButton.setIcon((ImageIcon)SwingIconFactory.getInstance().getIcon("edit-clear.png"));
		}
		return resetButton;
	}

	protected JComponent createSeparator(String text) {
		JPanel header = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 0.0;
		gbc.weighty = 1.0;
		gbc.anchor = GridBagConstraints.SOUTHWEST;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridwidth = 1;
		gbc.gridheight = 3;
		if (text != null && text.length() > 0) {
			header.add(createTitle(text, 4), gbc);
		}

		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridheight = 1;
		JSeparator separator = new JSeparator();
		header.add(Box.createGlue(), gbc);
		gbc.weighty = 0.0;
		header.add(separator, gbc);
		gbc.weighty = 1.0;
		header.add(Box.createGlue(), gbc);

		return header;
	}

	private JLabel createTitle(String textWithMnemonic, int gap) {
		JLabel label = new TitleLabel(textWithMnemonic);
		label.setVerticalAlignment(SwingConstants.CENTER);
		label.setBorder(BorderFactory.createEmptyBorder(1, 0, 1, gap));
		return label;
	}

	private static class TitleLabel extends JLabel {

		private TitleLabel() {
			// Do nothing
		}

		private TitleLabel(String text) {
			super(text);
		}

		public void updateUI() {
			super.updateUI();
			Color foreground =
				UIManager.getColor("TitledBorder.titleColor");
			if (foreground != null)
				setForeground(foreground);
			setFont(UIManager.getFont("TitledBorder.font").deriveFont(20f));
		}

	}

	/**
	 * @see de.tarent.contact.plugins.search.TarentSearch#canSearchsetLayout(layout);()
	 */
	public boolean canSearch() {
		//logger.warningSilent("[!] EnhancedSearchDialog.canSearch() is not implemented");
		return true;//FIXME: what does this mean?
	}

	/**
	 * @see org.evolvis.nuyo.plugins.search.TarentSearch#criteriaChanged()
	 */
	public boolean criteriaChanged() {
		return criteriaChanged;
	}

	/**
	 * @see org.evolvis.nuyo.plugins.search.TarentSearch#init(org.evolvis.nuyo.db.AddressListParameter)
	 */
	public void init(AddressListParameter alp) {
		model = new SearchModel(alp, preferences);

	}

	/**
	 * @see org.evolvis.nuyo.plugins.search.TarentSearch#setSearchInfo(java.lang.String)
	 */
	public void setSearchInfo(String aSearchInfo) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.evolvis.nuyo.plugins.search.TarentSearch#showSearch()
	 */
	public void showSearch() {
		categoryTree.setState(model.getSelectionState());
		searchFilterPanel.setState(model.getFilterState());

		addressSourceLabel.setText(Messages.getFormattedString("StatusBar_LABEL_Source", model.getAddressSourceLabel()));
		setVisible(true);
	}

	/**
	 * @see org.evolvis.nuyo.plugins.search.TarentSearch#updateAddressListParameters()
	 */
	public void updateAddressListParameters() {
		// Delegates to the model which keeps the instance.
		model.updateAddressListParameters();
	}
	
	public CategoryTree getCategoryTree() {
		return categoryTree;
	}

	private void cancelSearchDialog() {
		criteriaChanged = false;
		setVisible(false);
	}
}