/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Validators;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.PositionErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.ValidatorAdapter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SpecialCharValidator extends ValidatorAdapter
{
  private String m_sAcceptedChars;
   
  
  public String getListenerName()
  {
    return "SpecialCharValidator";
  }  
  
  public SpecialCharValidator()
  {
    super();
    m_sAcceptedChars = "";
  }
   
  public Validator cloneValidator()
  {
    SpecialCharValidator validator = new SpecialCharValidator();
    validator.m_sAcceptedChars = m_sAcceptedChars;
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      validator.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    return validator;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.VALIDATOR, "SpecialCharValidator", new DataContainerVersion(0));
    d.addParameterDescription(new ParameterDescription("accepted", "die erlaubten Zeichen", "die erlaubten Zeichen."));
    return d;
  }
  
  public boolean addParameter(String key, String value)
  {    
    if ("accepted".equalsIgnoreCase(key)) 
    {
      m_sAcceptedChars = value;
      m_oParameterList.add(new ObjectParameter(key, value));
      return true;
    }
    return false;
  }
  
  public boolean canValidate(Class data)
  {
    return data.isInstance(String.class);
  }

  public boolean validate(Object data)
  {    
    boolean founderrors = false;
    if (data instanceof String)
    {
      for(int i=0; i<((String)data).length(); i++)
      {
        if (m_sAcceptedChars.indexOf(((String)data).charAt(i)) == -1) 
        {
          PositionErrorHint peh = new PositionErrorHint("ung�ltiges Zeichen", "ung�ltiges Zeichen '" + ((String)data).charAt(i) + "' an Position " + (i+1) + " gefunden.\n\nG�ltige Zeichen sind: \"" + m_sAcceptedChars + "\"", null, getDataContainer().getGUIElement(), getDataContainer(), i); 
          fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_DATAINVALID, getDataContainer(), peh));                    
          founderrors = true;
        } 
      }
    }
    return(founderrors);
  }
}
