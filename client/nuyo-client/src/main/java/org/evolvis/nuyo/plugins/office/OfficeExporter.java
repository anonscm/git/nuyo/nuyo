/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.filefilter.ExtendableFileFilter;
import org.evolvis.liboffice.officefileformats.OfficeFileFormat;
import org.evolvis.liboffice.ui.swing.TemplateSelectorDialog;
import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;

import de.tarent.commons.utils.TaskManager;


/**
 * 
 * This class implements the <code>OfficeExporterPlugin</code> which realises Office-Integration for tarent-contact
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeExporter implements DialogAddressListPerformer
{
	private Addresses addresses;
	private JFileChooser fileChooser;
	
	private Logger logger = Logger.getLogger(OfficeExporter.class.getName());
	
	/**
	 * 
	 */
	public OfficeExporter()
	{
		
	}
	
	public void setAddresses(Addresses pAddresses)
	{
		addresses = pAddresses;
	}
	
	public Addresses getAddresses()
	{
		return addresses;
	}
	
	public JDialog getDialog()
	{
		// TODO
		return null;
	}
	
	public void execute()
	{
		OfficeConfigurationManager.getInstance().setup();
		OfficeAutomat.getPrinterConfigurator().setup();
		
		if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice() != null)
		{
			// Has preferred Office
			
			// Setup environment-variables
			OfficeAutomat.getOfficeConfigurator().getPreferredOffice().setupEnvironmentVariables();
			
			// Save configuration
			OfficeAutomat.getOfficeConfigurator().saveConfiguration(null);
			
			TemplateSelectorDialog selector = null;
			
			Preferences prefs = Preferences.userRoot().node(StartUpConstants.PREF_BASE_NODE_NAME).node("office");
			
			// TODO Replace this (is a workaround needed for not getting -1 for address-list-size)
			getAddresses().get(0);
			
			// Show File-Dialog for selecting Template
			selector = new TemplateSelectorDialog(prefs.get("templatePath", System.getProperty("user.home")), OfficeAutomat.getPrinterConfigurator().getAvailablePrinterNamesAsArray(), true, false, false, false, OfficeAutomat.REPLACE_MODE_SINGLE, true, addresses.getSize());
			
			Iterator it = OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getSeperateFileFiltersLoad().iterator();
			
			// Add separate file-filters for each supported format
			while(it.hasNext())
			{
				selector.addChoosableFileFilter((FileFilter) it.next());
			}
			
			// Set FileFilter with all supported file-formats of the used office
			selector.addChoosableFileFilter(OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getFileFilterLoad());
			
			selector.createDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame());
			
			if(selector.getReturnCode() == TemplateSelectorDialog.RETURNCODE_OK)
			{
				// Load all addresses
				for(int i=0; i < getAddresses().getSize(); i++)
					getAddresses().get(i);
				
				String printerName = null;
				String docFileName = null;
				if(selector.getDocumentPrint()) printerName = selector.getSelectedPrinterName();
				if(selector.getDocumentSaveDoc())
				{
					fileChooser = new JFileChooser();
					// Only allow the given export-filters
					fileChooser.setAcceptAllFileFilterUsed(false);
					List fileFilters = OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getSeparateFileFiltersSave();
					Iterator it2 = fileFilters.iterator();
					while(it2.hasNext())
					{
						fileChooser.addChoosableFileFilter((FileFilter)it2.next());
					}
	            	fileChooser.showSaveDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame());
	            	if(fileChooser.getSelectedFile() == null)
	            		return;
	            	docFileName = checkExtension(fileChooser.getSelectedFile().getAbsolutePath());
				}
				
				prefs.put("templatePath", new File(selector.getSelectedFilename()).getParent());
				
				int count = addresses.getSize();
				if(selector.getCount() != -1) count = selector.getCount();
				
				TaskManager.getInstance().register(new OfficeExportTask(addresses, count, selector.getSelectedFilename(), selector.getDocumentShow(), printerName, docFileName, selector.getSelectedReplaceMode(), selector.getInvisibility()), "Office-Export", true);
			}
		}
	}
	
	private String checkExtension(String pFileName)
	{
		if(pFileName == null)
			return null;
		
		// If the fileName already has a valid extension, do nothing
		if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getFileFilterSave().accept(new File(pFileName)))
				return pFileName;
		else
		{
			ExtendableFileFilter fileFilter = (ExtendableFileFilter) fileChooser.getFileFilter();
			List fileFormats = fileFilter.getSupportedFileFormats();
			if(fileFormats.size() == 1)
			{
				OfficeFileFormat fileFormat = (OfficeFileFormat) fileFormats.get(0);
				// Take the first (mostly there will be only one) extension
				pFileName = pFileName + "." + fileFormat.getSuffix(0);
			}
			else
			{
				// There should be only one File-Format assigned to every FileFilter
				throw new RuntimeException("More than one File-Format assigned to FileFilter \""+fileFilter.getDescription()+"\"");
			}
		}
		return pFileName;
	}
}