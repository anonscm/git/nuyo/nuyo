/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.text.ParseException;
import java.util.Date;

import javax.swing.JOptionPane;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressDateField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class GeburtsdatumField extends ContactAddressDateField
{
  
  private TarentWidgetLabel m_oLabel_Geburtsdatum;
  // representation format 
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public void postFinalRealize()
  {    
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createGeburtsdatumPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "GEBURTSDATUM";
  }

  public void setData(PluginData data)
  {
    setComplained(false);
    
    Date gebdate = ((Date)(data.get(AddressKeys.GEBURTSDATUM)));
    setLastDate(gebdate);

    setTextFieldFromDate(gebdate);
  }

  public void getData(PluginData data)
  {
	Date dateToSave = getDateFromTextField();
	if (dateToSave == null && !dateField.getText().equals("")){
		
		if (!getComplained()){
			JOptionPane.showMessageDialog(null, Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum_ErrorMsg") + Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum_FormatPattern_Msg"));
			setComplained(true);
    	}
		 
        // Use the last known date which was ok to be written into
        // the database.
        
        // TODO in my opinion the address-saving-process should be interrupted! -- Fabian
        dateToSave = getLastDate();
		}
		
	data.set(AddressKeys.GEBURTSDATUM, (dateToSave));
	 
  }

  public boolean isDirty(PluginData data)
  {
	Date dateToSave = null;
	
	try {
		dateToSave = shortYearDateFormat.parse(dateField.getText());
	} catch (ParseException e) {
		// do nothing
	}
	
    if (dateToSave == null)
    {
      if (((Date)(data.get(AddressKeys.GEBURTSDATUM))) != null) 
      {
        return(true);
      } 
    }
    else
    {
      if (((Date)(data.get(AddressKeys.GEBURTSDATUM))) != null)
      {
        if (!(((Date)(data.get(AddressKeys.GEBURTSDATUM))).equals(dateToSave))) 
        {
          return(true);
        } 
      }
    }
    
    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------
  
  public String getFieldName()
  {
    return Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum_FormatPattern");    
  }
  
  public String getFieldDescription()
  {
    return Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum_ToolTip");
  }
 
  private EntryLayoutHelper createGeburtsdatumPanel(String widgetFlags)
  { 
	setShortDateFormat(Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum_ShortYearFormatPattern"));
	setDateformat(Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum_FormatPattern"));
	m_oLabel_Geburtsdatum = new TarentWidgetLabel(Messages.getString("GUI_MainFrameNewStyle_ErweitertTop_Geburtsdatum"));
	return(new EntryLayoutHelper(m_oLabel_Geburtsdatum, dateField, widgetFlags));
  }

public void setDoubleCheckSensitive(boolean issensitive) {	
}
  
}
