package org.evolvis.nuyo.gui.assignmentview;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.evolvis.nuyo.gui.Messages;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;


/**
 * Viewer component for the category assignment of an address.
 * 
 * <p>This implementation is suitable for embedding it into contact's
 * tabs.</p>
 * 
 */
public class CategoryAssignmentViewer extends JPanel {

	private JList categoryList;
	
	private CategoryAssignmentModel model;
	
	/**
	 * Prepares child components, sets up layout and thus creates the
	 * fully configured viewer component.
	 *
	 */
	public CategoryAssignmentViewer()
	{
		FormLayout l = new FormLayout("3dlu, pref:grow, 3dlu", // Columns.
				                      "3dlu, pref, 3dlu, fill:pref:grow, 3dlu"); // Rows.
		
		setLayout(l);
		
		CellConstraints cc = new CellConstraints();
		
		add(new JLabel(Messages.getString("GUI_CategoryAssignmentViewer_Label")), cc.xy(2, 2));
		add(new JScrollPane(categoryList = new JList()), cc.xy(2, 4));
		
		model = new CategoryAssignmentModel();
		
		categoryList.setModel(model.getListModel());
		categoryList.setCellRenderer(model.getRenderer());
		
	}
}
