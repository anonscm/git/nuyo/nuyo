/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Dimension;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.ScrollPaneConstants;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetListBox;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class AssociatedCategoriesField extends ContactAddressTextField
{
  
    private static final TarentLogger logger = new TarentLogger(AssociatedCategoriesField.class);

    private Map associatedCategories;
  
  private TarentWidgetListBox m_oListBox_categories;
  private TarentWidgetLabel m_oLabel_categories;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }  
  
  public boolean isWriteOnly()
  {
    return true;  
  }
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createVerteilerPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "ASSOCIATEDCATEGORIES";
  }

  public void postFinalRealize()
  {    
  }
  
  
  public void setEditable(boolean iseditable)
  {
  }

  public void setData(PluginData data)
  {
	  associatedCategories = (Map)data.get(AddressKeys.ASSOCIATEDCATEGORIES);
      if(associatedCategories == null) {
          logger.warningSilent("[!] [AssociatedCategoriesField]: set data failed: plugin data is empty: key=" + getKey());
          return;
      }
	  fillCategoriesTextfield();
  }

  public void getData(PluginData data)
  {
  }

  public boolean isDirty(PluginData data)
  {
    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  public String getFieldName()
  {
    return Messages.getString("GUI_MainFrameNewStyle_Standard_AssociatedCategories");    
  }
  
  public String getFieldDescription()
  {
    return Messages.getString("GUI_MainFrameNewStyle_Standard_AssociatedCategories_ToolTip");
  }
  
  
  private EntryLayoutHelper createVerteilerPanel(String widgetFlags)
  {
    m_oListBox_categories = new TarentWidgetListBox();
    m_oListBox_categories.getJScrollPane().setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    m_oListBox_categories.setToolTipText(Messages.getString("GUI_MainFrameNewStyle_Standard_AssociatedCategories_ToolTip"));     //$NON-NLS-1$
    m_oListBox_categories.setVisibleRowCount(3);
    m_oListBox_categories.setMinimumSize(new Dimension(200, 30));
    m_oListBox_categories.setWidgetEditable(false);
    m_oLabel_categories = new TarentWidgetLabel(Messages.getString("GUI_MainFrameNewStyle_Standard_AssociatedCategories"));
    
    return(new EntryLayoutHelper(m_oLabel_categories, m_oListBox_categories, widgetFlags));
  }

  
  private void fillCategoriesTextfield()
  {
    Set list = new TreeSet(); 
    String categoryName;
    
    Iterator addressCategoryIterator = associatedCategories.values().iterator();
    
    while(addressCategoryIterator.hasNext())
        { 
          categoryName = asString(addressCategoryIterator.next());
           
          if (categoryName != null)
              list.add(categoryName);
          else
            list.add("?????");
        
        }
    
    m_oListBox_categories.setData(list.toArray());            
  }

  private final static String asString(Object o) 
  {
    return o == null ? null : o.toString();
  }

}
