/* $Id: ContactGuiException.java,v 1.3 2006/09/22 09:11:43 aleksej Exp $
 * 
 * Created on 10.09.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.gui;

import org.evolvis.nuyo.controller.ContactException;

/**
 * Diese Klasse stellt Contact-Client-interne Ausnahmen dar, die im
 * GUI-Kontext auftreten.
 *  
 * @author niko
 */
public class ContactGuiException extends ContactException {
    /*
     * Konstruktoren
     */
    public ContactGuiException(String msg) {
        super(-1, msg);
    }

    public ContactGuiException(int exId, String arg0) {
        super(exId, arg0);
    }

    public ContactGuiException(int exId, String arg0, Throwable arg1) {
        super(exId, arg0, arg1);
    }

    public ContactGuiException(int exId, Throwable arg0) {
        super(exId, arg0);
    }

    /*
     * Fehlerkonstanten
     */
    /**
     * Eine ben�tigte Ressource konte nichtr gefunden werden...
     * 
     * @see MainFrameExtStyle#init()
     */
    public final static int EX_RESSOURCE_NOT_AVAILABLE = 1;
    public final static int EX_INIT_FAILED = 2;
    public final static int EX_ADDRESS_INVALID = 3;
    
}
