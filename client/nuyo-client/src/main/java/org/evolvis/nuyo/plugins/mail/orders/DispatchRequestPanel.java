/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Hanno Wendt.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.plugins.mail.orders;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.gui.action.AddressListProviderAdapter;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugins.office.OfficeExportAction;

import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Context;

/**
 * VersandAuftragPanel
 * 
 * @author hanno
 */
public class DispatchRequestPanel extends JPanel 
{
	private boolean m_bEnableEMail = false;  
	private boolean m_bEnableFAX = false;  
	private boolean m_bEnablePost = false;  

	private JPanel rootPanel;
	private JPanel m_oTextPanel;
	private JPanel m_oVersandartenPanel;
	private JPanel m_oButtonPanel;
	private JLabel m_lblVersandauftrag;
	private JLabel m_lblDatumZeit;
	private JTextField m_txtVersandauftrag;
	private JTextField m_txtDatumZeit;
	private JLabel m_lblInsgesamt;
	private JLabel m_lblNichtZugeordnet;
	private JLabel m_lblEMail;
	private JLabel m_lblFax;
	private JLabel m_lblPost;
	private JTextField m_txtInsgesamt;
	private JTextField m_txtNichtZugeordnet;
	private JTextField m_txtEMail;
	private JTextField m_txtFax;
	private JTextField m_txtPost;
	private JButton m_btnInsgesamtAnzeigen;
	private JButton m_btnInsgesamtExportieren;
	private JButton m_btnNichtZugeordnetAnzeigen;
	private JButton m_btnNichtZugeordnetExportieren;
	private JButton m_btnEMailZuweisen;
	private JButton m_btnEMailAusfuehren;
	private JButton m_btnEMailAnzeigen;
	private JButton m_btnEMailExportieren;
	private JButton m_btnFaxZuweisen;
	private JButton m_btnFaxAusfuehren;
	private JButton m_btnFaxAnzeigen;
	private JButton m_btnFaxExportieren;
	private JButton m_btnPostZuweisen;
	private JButton m_btnPostAusfuehren;
	private JButton m_btnPostAnzeigen;
	private JButton m_btnPostExportieren;
	private Box empty1;
	private Box empty2;
	private Box empty3;
	private Box empty4;
	//private JButton m_btnReset;
	private JButton m_btnClose;
	private JButton m_btnApply;    
	private JButton m_btnResetAssignment;
	private GUIListener myGUIListener;
	private MailBatch m_oMailBatch;
	private MailBatchesPanel m_oVersandauftraegePanel;

	private TarentLogger logger = new TarentLogger(DispatchRequestPanel.class);
	/**
	 * Dieser Konstruktor wird benutzt, wenn ein Versandauftrag aus der
	 * Historie angezeigt werden soll die anzuzeigenden Daten werden als
	 * zus�tzliche Parameter �bergeben.
	 */

	public DispatchRequestPanel(GUIListener am, MailBatchesPanel parent, MailBatch oMailBatch) 
	{
		super();
		m_oVersandauftraegePanel = parent;

		List channels = am.getAvailableChannels();
		if (channels.contains(MailBatch.CHANNEL.FAX)) m_bEnableFAX = true; 
		if (channels.contains(MailBatch.CHANNEL.MAIL)) m_bEnableEMail = true;
		if (channels.contains(MailBatch.CHANNEL.POST)) m_bEnablePost = true;

		myGUIListener = am;
		m_oMailBatch = oMailBatch;
		init1(this);
	}

	/**
	 * Diese Methode wird verwendet um ge�nderte Versandauftr�ge zur�ckzuspeichern.
	 */
	private void init1(JPanel contentPane) 
	{
		rootPanel = new JPanel();
		rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.Y_AXIS));

		m_oTextPanel = new JPanel(new GridLayout(2, 2, 5, 5));

		m_lblVersandauftrag = new JLabel(Messages.getString("GUI_VersandAuftrag_Versandauftrag") + ": ", JLabel.RIGHT); //$NON-NLS-1$
		m_lblDatumZeit = new JLabel(Messages.getString("GUI_VersandAuftrag_Datum_Zeit") + ": ", JLabel.RIGHT); //$NON-NLS-1$

		m_txtVersandauftrag = new JTextField();
		m_txtVersandauftrag.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_Name_ToolTip")); //$NON-NLS-1$
		m_txtVersandauftrag.addFocusListener(new FocusListener() 
		{
			private String m_sText;
			public void focusLost(FocusEvent fe) 
			{
				myGUIListener.setWaiting(true);              
				try 
				{
					if (m_sText != m_txtVersandauftrag.getText()) 
					{
						m_sText = m_txtVersandauftrag.getText();
						m_oMailBatch.setName(m_sText);
						m_oVersandauftraegePanel.refreshList();
					}
				} 
				catch (ContactDBException sex) 
				{
				}
				myGUIListener.setWaiting(false);              
			}
			public void focusGained(FocusEvent fe) 
			{
				m_sText = m_txtVersandauftrag.getText();
			}
		});

		m_txtDatumZeit = new JTextField();
		m_txtDatumZeit.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_DatumZeit_ToolTip")); //$NON-NLS-1$
		m_txtDatumZeit.setEditable(false);

		m_oTextPanel.add(m_lblVersandauftrag);
		m_oTextPanel.add(m_txtVersandauftrag);
		m_oTextPanel.add(m_lblDatumZeit);
		m_oTextPanel.add(m_txtDatumZeit);

//		m_oVersandartenPanel = new JPanel(new GridLayout(5, 5, 5, 5));
		int rows = 7;
		if (! m_bEnableEMail) rows--;
		if (! m_bEnableFAX) rows--;
		if (! m_bEnablePost) rows--;
		m_oVersandartenPanel = new JPanel(new GridLayout(rows, 6, 5, 5));

		m_lblInsgesamt = new JLabel(Messages.getString("GUI_VersandAuftrag_Insgesamt") + ": ", JLabel.RIGHT); //$NON-NLS-1$
		m_lblNichtZugeordnet = new JLabel(Messages.getString("GUI_VersandAuftrag_Nicht_zugeordnet") + ": ", JLabel.RIGHT); //$NON-NLS-1$
		m_lblEMail = new JLabel(Messages.getString("GUI_VersandAuftrag_eMail") + ": ", JLabel.RIGHT); //$NON-NLS-1$
		m_lblFax = new JLabel(Messages.getString("GUI_VersandAuftrag_Fax") + ": ", JLabel.RIGHT); //$NON-NLS-1$
		m_lblPost = new JLabel(Messages.getString("GUI_VersandAuftrag_Post") + ": ", JLabel.RIGHT); //$NON-NLS-1$

		m_txtInsgesamt = new JTextField(""); //$NON-NLS-1$
		m_txtInsgesamt.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_Insgesamt_ToolTip")); //$NON-NLS-1$
		m_txtInsgesamt.setEditable(false);

		m_txtNichtZugeordnet = new JTextField(""); //$NON-NLS-1$
		m_txtNichtZugeordnet.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_NichtZugeordnet_ToolTip")); //$NON-NLS-1$
		m_txtNichtZugeordnet.setEditable(false);

		m_txtEMail = new JTextField(""); //$NON-NLS-1$
		m_txtEMail.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_EMail_ToolTip")); //$NON-NLS-1$
		m_txtEMail.setEditable(false);

		m_txtFax = new JTextField(""); //$NON-NLS-1$
		m_txtFax.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_Fax_ToolTip")); //$NON-NLS-1$
		m_txtFax.setEditable(false);

		m_txtPost = new JTextField(""); //$NON-NLS-1$
		m_txtPost.setToolTipText(Messages.getString("GUI_VersandAuftrag_TextField_Post_ToolTip")); //$NON-NLS-1$
		m_txtPost.setEditable(false);

		m_btnInsgesamtAnzeigen = new JButton(Messages.getString("GUI_VersandAuftrag_Insgesamt_Anzeigen")); //$NON-NLS-1$
		m_btnInsgesamtAnzeigen.setToolTipText(Messages.getString("GUI_VersandAuftrag_Insgesamt_Anzeigen_ToolTip")); //$NON-NLS-1$
		m_btnInsgesamtAnzeigen.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				myGUIListener.userRequestShowMailBatch(m_oMailBatch, MailBatch.CHANNEL.ALL);
			}
		});

		m_btnInsgesamtExportieren = new JButton(Messages.getString("GUI_VersandAuftrag_Insgesamt_Exportieren")); //$NON-NLS-1$
		m_btnInsgesamtExportieren.setToolTipText(Messages.getString("GUI_VersandAuftrag_Insgesamt_Exportieren_ToolTip")); //$NON-NLS-1$
		m_btnInsgesamtExportieren.addActionListener(new ExportAddressesActionListener(MailBatch.CHANNEL.ALL));
        

		m_btnNichtZugeordnetAnzeigen = new JButton(Messages.getString("GUI_VersandAuftrag_Nicht_zuweisen_Anzeigen")); //$NON-NLS-1$
		m_btnNichtZugeordnetAnzeigen.setToolTipText(Messages.getString("GUI_VersandAuftrag_Nicht_zuweisen_Anzeigen_ToolTip")); //$NON-NLS-1$
		m_btnNichtZugeordnetAnzeigen.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				myGUIListener.userRequestShowMailBatch(m_oMailBatch, MailBatch.CHANNEL.NOT_ASSIGNED);
			}
		});
		m_btnNichtZugeordnetExportieren = new JButton(Messages.getString("GUI_VersandAuftrag_Nicht_zuweisen_Exportieren")); //$NON-NLS-1$
		m_btnNichtZugeordnetExportieren.setToolTipText(Messages.getString("GUI_VersandAuftrag_Nicht_zuweisen_Exportieren_ToolTip")); //$NON-NLS-1$
		m_btnNichtZugeordnetExportieren.addActionListener(new ExportAddressesActionListener(MailBatch.CHANNEL.NOT_ASSIGNED));


		m_btnEMailZuweisen = new JButton(Messages.getString("GUI_VersandAuftrag_eMail_Zuweisen")); //$NON-NLS-1$
		m_btnEMailZuweisen.setToolTipText(Messages.getString("GUI_VersandAuftrag_eMail_Zuweisen_ToolTip")); //$NON-NLS-1$
		m_btnEMailZuweisen.addActionListener(new AssignAddressesActionListener(MailBatch.CHANNEL.MAIL));

		m_btnEMailAusfuehren = new JButton(Messages.getString("GUI_VersandAuftrag_eMail_Ausfuehren")); //$NON-NLS-1$
		m_btnEMailAusfuehren.setToolTipText(Messages.getString("GUI_VersandAuftrag_eMail_Ausfuehren_ToolTip")); //$NON-NLS-1$
		m_btnEMailAusfuehren.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				myGUIListener.setWaiting(true);              
				if (m_oMailBatch.getSentMail() == MailBatch.NOT_ASSIGNED) ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_VersandAuftrag_eMail_Ausfuehren_Fehler_keine_Adressen")); //$NON-NLS-1$
				else 
				{
					myGUIListener.userRequestSendMailBatch(m_oMailBatch, MailBatch.CHANNEL.MAIL);
					try 
					{
						m_oMailBatch.setSentMail(MailBatch.SENT);
					} 
					catch (ContactDBException ex) 
					{
						ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_VersandAuftrag_eMail_Ausfuehren_Fehler_Markieren_des_Kanals_fehlgeschlagen"), ex); //$NON-NLS-1$
					}
					adjustButtons();
				}
				myGUIListener.setWaiting(false);              
			}
		});
		m_btnEMailAnzeigen = new JButton(Messages.getString("GUI_VersandAuftrag_eMail_Anzeigen")); //$NON-NLS-1$
		m_btnEMailAnzeigen.setToolTipText(Messages.getString("GUI_VersandAuftrag_eMail_Anzeigen_ToolTip")); //$NON-NLS-1$
		m_btnEMailAnzeigen.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				myGUIListener.userRequestShowMailBatch(m_oMailBatch, MailBatch.CHANNEL.MAIL);
			}
		});

		m_btnEMailExportieren = new JButton(Messages.getString("GUI_VersandAuftrag_eMail_Exportieren")); //$NON-NLS-1$
		m_btnEMailExportieren.setToolTipText(Messages.getString("GUI_VersandAuftrag_eMail_Exportieren_ToolTip")); //$NON-NLS-1$
		m_btnEMailExportieren.addActionListener(new ExportAddressesActionListener(MailBatch.CHANNEL.MAIL));


		m_btnFaxZuweisen = new JButton(Messages.getString("GUI_VersandAuftrag_Fax_Zuweisen")); //$NON-NLS-1$
		m_btnFaxZuweisen.setToolTipText(Messages.getString("GUI_VersandAuftrag_Fax_Zuweisen_ToolTip")); //$NON-NLS-1$
		m_btnFaxZuweisen.addActionListener(new AssignAddressesActionListener(MailBatch.CHANNEL.FAX));


		m_btnFaxAusfuehren = new JButton(Messages.getString("GUI_VersandAuftrag_Fax_Ausfuehren")); //$NON-NLS-1$
		m_btnFaxAusfuehren.setToolTipText(Messages.getString("GUI_VersandAuftrag_Fax_Ausfuehren_ToolTip")); //$NON-NLS-1$

		m_btnFaxAusfuehren.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				executeMailBatch(MailBatch.CHANNEL.FAX, e);
			}	
		});

		m_btnFaxAnzeigen = new JButton(Messages.getString("GUI_VersandAuftrag_Fax_Anzeigen")); //$NON-NLS-1$
		m_btnFaxAnzeigen.setToolTipText(Messages.getString("GUI_VersandAuftrag_Fax_Anzeigen_ToolTip")); //$NON-NLS-1$
		m_btnFaxAnzeigen.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				myGUIListener.userRequestShowMailBatch(m_oMailBatch, MailBatch.CHANNEL.FAX);
			}
		});
		m_btnFaxExportieren = new JButton(Messages.getString("GUI_VersandAuftrag_Fax_Exportieren")); //$NON-NLS-1$
		m_btnFaxExportieren.setToolTipText(Messages.getString("GUI_VersandAuftrag_Fax_Exportieren_ToolTip")); //$NON-NLS-1$
		m_btnFaxExportieren.addActionListener(new ExportAddressesActionListener(MailBatch.CHANNEL.FAX));


		m_btnPostZuweisen = new JButton(Messages.getString("GUI_VersandAuftrag_Post_Zuweisen")); //$NON-NLS-1$
		m_btnPostZuweisen.setToolTipText(Messages.getString("GUI_VersandAuftrag_Post_Zuweisen_ToolTip")); //$NON-NLS-1$
		m_btnPostZuweisen.addActionListener(new AssignAddressesActionListener(MailBatch.CHANNEL.POST));

		m_btnPostAusfuehren = new JButton(Messages.getString("GUI_VersandAuftrag_Post_Ausfuehren")); //$NON-NLS-1$
		m_btnPostAusfuehren.setToolTipText(Messages.getString("GUI_VersandAuftrag_Post_Ausfuehren_ToolTip")); //$NON-NLS-1$

		m_btnPostAusfuehren.addActionListener(new ActionListener()
		{
			public void actionPerformed(final ActionEvent e)
			{
                SwingUtilities.invokeLater(new Runnable(){
                    public void run() {
                        int count = Integer.parseInt(m_txtPost.getText());
                        if(count > 1000) {
                            int antwort = ApplicationServices.getInstance().getCommonDialogServices()
                            .askUser(Messages.getString("GUI_Consignment_Post_Warning_Dialog_Title"), 
                                     Messages.getString("GUI_Consignment_Post_Warning_Dialog_Message"), 
                                     new String[]{Messages.getString("GUI_Consignment_Post_Warning_Dialog_Answer_proceed"),
                                                  Messages.getString("GUI_Consignment_Post_Warning_Dialog_Answer_cancel")}, 1);
                            if(antwort == 1) {
                                return;
                            }
                        }
                        executeMailBatch(MailBatch.CHANNEL.POST, e);
                    }});
			}	
		});

		m_btnPostAnzeigen = new JButton(Messages.getString("GUI_VersandAuftrag_Post_Anzeigen")); //$NON-NLS-1$
		m_btnPostAnzeigen.setToolTipText(Messages.getString("GUI_VersandAuftrag_Post_Anzeigen_ToolTip")); //$NON-NLS-1$
		m_btnPostAnzeigen.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				myGUIListener.userRequestShowMailBatch(m_oMailBatch, MailBatch.CHANNEL.POST);
			}
		});


		m_btnPostExportieren = new JButton(Messages.getString("GUI_VersandAuftrag_Post_Exportieren")); //$NON-NLS-1$
		m_btnPostExportieren.setToolTipText(Messages.getString("GUI_VersandAuftrag_Post_Exportieren_ToolTip")); //$NON-NLS-1$
		m_btnPostExportieren.addActionListener(new ExportAddressesActionListener(MailBatch.CHANNEL.POST));



		empty1 = new Box(BoxLayout.X_AXIS);
		empty2 = new Box(BoxLayout.X_AXIS);
		empty3 = new Box(BoxLayout.X_AXIS);
		empty4 = new Box(BoxLayout.X_AXIS);

		m_oVersandartenPanel.add(m_lblInsgesamt);
		m_oVersandartenPanel.add(m_txtInsgesamt);
		m_oVersandartenPanel.add(empty1);
		m_oVersandartenPanel.add(empty2);
		m_oVersandartenPanel.add(m_btnInsgesamtAnzeigen);
		m_oVersandartenPanel.add(m_btnInsgesamtExportieren);
		m_oVersandartenPanel.add(m_lblNichtZugeordnet);
		m_oVersandartenPanel.add(m_txtNichtZugeordnet);
		m_oVersandartenPanel.add(empty3);
		m_oVersandartenPanel.add(empty4);
		m_oVersandartenPanel.add(m_btnNichtZugeordnetAnzeigen);
		m_oVersandartenPanel.add(m_btnNichtZugeordnetExportieren);


		if (m_bEnableEMail)
		{          
			m_oVersandartenPanel.add(m_lblEMail);
			m_oVersandartenPanel.add(m_txtEMail);
			m_oVersandartenPanel.add(m_btnEMailZuweisen);
			m_oVersandartenPanel.add(m_btnEMailAusfuehren);
			m_oVersandartenPanel.add(m_btnEMailAnzeigen);
			m_oVersandartenPanel.add(m_btnEMailExportieren);
		}

		if (m_bEnableFAX)
		{          
			m_oVersandartenPanel.add(m_lblFax);
			m_oVersandartenPanel.add(m_txtFax);
			m_oVersandartenPanel.add(m_btnFaxZuweisen);
			m_oVersandartenPanel.add(m_btnFaxAusfuehren);
			m_oVersandartenPanel.add(m_btnFaxAnzeigen);
			m_oVersandartenPanel.add(m_btnFaxExportieren);
		}

		if (m_bEnablePost)
		{          
			m_oVersandartenPanel.add(m_lblPost);
			m_oVersandartenPanel.add(m_txtPost);
			m_oVersandartenPanel.add(m_btnPostZuweisen);
			m_oVersandartenPanel.add(m_btnPostAusfuehren);
			m_oVersandartenPanel.add(m_btnPostAnzeigen);
			m_oVersandartenPanel.add(m_btnPostExportieren);
		}

		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
//		TODO

//		"Zuweisung zur�cksetzen"
		m_btnResetAssignment = new JButton(Messages.getString("GUI_VersandAuftrag_Zuweisung_zuruecksetzen"));
		m_btnResetAssignment.setToolTipText(Messages.getString("GUI_VersandAuftrag_Zuweisung_zuruecksetzen_ToolTip")); //$NON-NLS-1$
		m_btnResetAssignment.addActionListener(new ResetChannelActionListener()); 

		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(m_btnResetAssignment);
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));
		m_oVersandartenPanel.add(new Box(BoxLayout.X_AXIS));


		rootPanel.add(m_oTextPanel);
		rootPanel.add(Box.createVerticalStrut(5));
		rootPanel.add(m_oVersandartenPanel);

		contentPane.setLayout(new BorderLayout(8, 8));
		contentPane.add(rootPanel, BorderLayout.CENTER);
		plantIt();

		setButtonFilterOffEnabled(myGUIListener.isSearchActive());
		adjustButtons();
	}
	
	protected void executeMailBatch(final MailBatch.CHANNEL channel, final ActionEvent e)
	{
		TaskManager.getInstance().register(new TaskManager.SwingTask() {
			
			OfficeExportAction officeExportAction = new OfficeExportAction();

			protected void failed(Context arg0)
			{
				logger.warning(Messages.getString("AM_SubCategory_Get_DbError"));
			}

			protected void runImpl(Context arg0) throws Exception
			{
				try
				{
					officeExportAction.registerDataProvider(new AddressListProviderAdapter(m_oMailBatch.getChannelAddresses(channel)));
				} catch (ContactDBException e)
				{
					throw new Exception(Messages.getString("AM_SubCategory_Get_DbError"), e);
				}
			}

			protected void succeeded(Context arg0)
			{
				officeExportAction.actionPerformed(e);
			}
		},  Messages.getString("GUI_VersandAuftrag_LadeAdressen"), false);
	}



	public void setButtonFilterOffEnabled(boolean enable) 
	{
        // do nothing here anymore
	}



	/** Setzt die bisherigen Werte beim Erzeugen eines Auftragsfensters 
	 *  mit viewAndChangeAuftrag in die Textfelder ein.*/

	private void plantIt() 
	{
		if (m_oMailBatch != null) 
		{
			m_txtVersandauftrag.setText(m_oMailBatch.getName());
			m_txtDatumZeit.setText(m_oMailBatch.getDateTime());
			m_txtInsgesamt.setText(Integer.toString(m_oMailBatch.getTotal()));
			m_txtNichtZugeordnet.setText(Integer.toString(m_oMailBatch.getNotAssigned()));
			m_txtEMail.setText(Integer.toString(m_oMailBatch.getMergeMail()));
			m_txtFax.setText(Integer.toString(m_oMailBatch.getMergeFax()));
			m_txtPost.setText(Integer.toString(m_oMailBatch.getMergePost()));

			repaint();
		}
	}

	/** Besorgt beim Umbenennen den Schreibzugriff f�r den RenameHelper.
	 * */

	protected void renameHelp(String s) 
	{
		m_txtVersandauftrag.setText(s);
	}

	/** Stellt den Vektor mit den neuen Daten f�r die History-ansicht 
	 * in MailBatchesDialog zur Verf�gung.
	 * */

	protected Vector writeBack() 
	{
		Vector hector = new Vector();
		hector.addElement(m_txtVersandauftrag.getText());
		hector.addElement(null); // TODO: was soll das hier???
		hector.addElement(m_txtDatumZeit.getText());
		hector.addElement(m_txtInsgesamt.getText());
		hector.addElement(m_txtEMail.getText());
		hector.addElement(m_txtFax.getText());
		hector.addElement(m_txtPost.getText());
		hector.addElement(m_txtNichtZugeordnet.getText());

		return hector;
	}

	/**
	 * Diese Methode setzt die verschiedenen Buttons auf en- oder disabled je
	 * nach Status des Versandauftrages.
	 */
	protected void adjustButtons() 
	{
		if (m_oMailBatch != null) 
		{
			adjustButtons(m_oMailBatch.getSentFax(), m_btnFaxExportieren, m_btnFaxAnzeigen, m_btnFaxAusfuehren, m_btnFaxZuweisen, m_lblFax, Messages.getString("GUI_VersandAuftrag_Fax")); //$NON-NLS-1$
			adjustButtons(m_oMailBatch.getSentMail(), m_btnEMailExportieren, m_btnEMailAnzeigen, m_btnEMailAusfuehren, m_btnEMailZuweisen, m_lblEMail, Messages.getString("GUI_VersandAuftrag_eMail")); //$NON-NLS-1$
			adjustButtons(m_oMailBatch.getSentPost(), m_btnPostExportieren, m_btnPostAnzeigen, m_btnPostAusfuehren, m_btnPostZuweisen, m_lblPost, Messages.getString("GUI_VersandAuftrag_Post")); //$NON-NLS-1$
			//TODO          
			m_btnResetAssignment.setEnabled((MailBatch.ASSIGNED_NOT_SENT == m_oMailBatch.getSentFax()) || (MailBatch.ASSIGNED_NOT_SENT == m_oMailBatch.getSentMail()) || (MailBatch.ASSIGNED_NOT_SENT == m_oMailBatch.getSentPost()));
			//m_btnReset.setEnabled((MailBatch.ASSIGNED_NOT_SENT == m_oMailBatch.getSentFax()) || (MailBatch.ASSIGNED_NOT_SENT == m_oMailBatch.getSentMail()) || (MailBatch.ASSIGNED_NOT_SENT == m_oMailBatch.getSentPost()));

			// disable "Ausfuehren"-buttons if there are no addresses in channel
			m_btnEMailAusfuehren.setEnabled(m_oMailBatch.getMergeMail() != 0);
			m_btnFaxAusfuehren.setEnabled(m_oMailBatch.getMergeFax() != 0);
			m_btnPostAusfuehren.setEnabled(m_oMailBatch.getMergePost() != 0);
			
			// disable "Anzeigen"-buttons if there are no addresses in channel
			m_btnEMailAnzeigen.setEnabled(m_oMailBatch.getMergeMail() != 0);
			m_btnFaxAnzeigen.setEnabled(m_oMailBatch.getMergeFax() != 0);
			m_btnPostAnzeigen.setEnabled(m_oMailBatch.getMergePost() != 0);
			
			// disable "Exportieren"-buttons if there are no addresses in channel
			m_btnEMailExportieren.setEnabled(m_oMailBatch.getMergeMail() != 0);
			m_btnFaxExportieren.setEnabled(m_oMailBatch.getMergeFax() != 0);
			m_btnPostExportieren.setEnabled(m_oMailBatch.getMergePost() != 0);
		} 
		else 
		{
			m_btnFaxAnzeigen.setEnabled(false);
			m_btnFaxAusfuehren.setEnabled(false);
			m_btnFaxZuweisen.setEnabled(false);
			m_btnFaxExportieren.setEnabled(false);
			m_btnEMailAnzeigen.setEnabled(false);
			m_btnEMailAusfuehren.setEnabled(false);
			m_btnEMailZuweisen.setEnabled(false);
			m_btnEMailExportieren.setEnabled(false);
			m_btnPostAnzeigen.setEnabled(false);
			m_btnPostExportieren.setEnabled(false);
			m_btnPostAusfuehren.setEnabled(false);
			m_btnPostZuweisen.setEnabled(false);
			m_btnResetAssignment.setEnabled(false);
			//m_btnReset.setEnabled(false);
			m_btnInsgesamtAnzeigen.setEnabled(false);
			m_btnInsgesamtExportieren.setEnabled(false);
			m_btnNichtZugeordnetAnzeigen.setEnabled(false);
			m_btnNichtZugeordnetExportieren.setEnabled(false);
			m_txtVersandauftrag.setEnabled(false);
		}
	}

	private void adjustButtons(int state, JButton btnExportieren, JButton btnAnzeigen, JButton btnAusfuehren, JButton btnZuweisen, JLabel lblLine, String prefix) 
	{
		switch (state) 
		{
		case MailBatch.NOT_ASSIGNED :
			btnExportieren.setEnabled(false);
			btnAnzeigen.setEnabled(false);
			btnAusfuehren.setEnabled(false);
			btnZuweisen.setEnabled(true);
			break;

		case MailBatch.SENT :
			btnExportieren.setEnabled(true);
			lblLine.setText(prefix + " (" + Messages.getString("GUI_VersandAuftrag_ausgefuehrt_postfix") + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		case MailBatch.ASSIGNED_NOT_SENT:
			btnExportieren.setEnabled(true);
			btnAnzeigen.setEnabled(true);
			btnAusfuehren.setEnabled(true);
			btnZuweisen.setEnabled(false);
			break;

		default: break;
		}
	}   


    public class ExportAddressesActionListener implements ActionListener, AddressListProvider {

        MailBatch.CHANNEL channel;
        
        public ExportAddressesActionListener(MailBatch.CHANNEL channel) {
            this.channel = channel;
            
        }
        
        public void actionPerformed(ActionEvent e) {
            myGUIListener.userRequestExportAddresses(this, getNumberOfAdresses());
        }

        public Addresses getAddresses() {
            try {
                return m_oMailBatch.getChannelAddresses(channel);
            } catch (ContactDBException ex) {
                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_VersandAuftrag_Button_Export_Fehler"), ex); //$NON-NLS-1$
                return null;
            }
        }
        
        public int getNumberOfAdresses(){
        	
        	if (channel.equals(MailBatch.CHANNEL.FAX))
            	return m_oMailBatch.getMergeFax();
        	if (channel.equals(MailBatch.CHANNEL.MAIL))
            	return m_oMailBatch.getMergeMail();
        	if (channel.equals(MailBatch.CHANNEL.NOT_ASSIGNED))
            	return m_oMailBatch.getNotAssigned();
        	if (channel.equals(MailBatch.CHANNEL.POST))
            	return m_oMailBatch.getMergePost();
        	if (channel.equals(MailBatch.CHANNEL.TOTAL))
            	return m_oMailBatch.getTotal();

        	return 0;
        }
        
        public void registerDataConsumer(Object object) {
            // do nothing here?
        }
    }


    public class AssignAddressesActionListener extends TaskManager.SwingTask implements ActionListener {

        MailBatch.CHANNEL channel;
        ContactDBException exception = null;
        
        public AssignAddressesActionListener(MailBatch.CHANNEL channel) {
            this.channel = channel;
        }

        public void actionPerformed(ActionEvent e) {
            TaskManager.getInstance().register(this, Messages.getString("GUI_VersandAuftrag_eMail_Zuweisen"), false);
        }

        public void prepare(TaskManager.Context ctx) {
            // do nothing here
        }

        public void runImpl(TaskManager.Context ctx) throws TaskManager.SwingTask.Exception {
            try {
                m_oMailBatch.setChannel(channel);
                
            } catch (ContactDBException ex) {
                exception = ex;
                throw new TaskManager.SwingTask.Exception(Messages.getString("GUI_VersandAuftrag_Zuweisen_Fehler"));
            }
        }
        
        public void succeeded(TaskManager.Context ctx) {
            plantIt();
            adjustButtons();
            m_oVersandauftraegePanel.refreshList();
        }
        
        public void failed(TaskManager.Context ctx) {
            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_VersandAuftrag_Zuweisen_Fehler"), exception); //$NON-NLS-1$
        }
    }

    public class ResetChannelActionListener extends TaskManager.SwingTask implements ActionListener {

        ContactDBException exception = null;
        
        public void actionPerformed(ActionEvent e) {
            TaskManager.getInstance().register(this, Messages.getString("GUI_VersandAuftrag_eMail_Zuweisen"), false);
        }

        public void prepare(TaskManager.Context ctx) {
            // do nothing here
        }

        public void runImpl(TaskManager.Context ctx) throws TaskManager.SwingTask.Exception {
            try {

                m_oMailBatch.resetChannel(MailBatch.CHANNEL.MAIL);
                m_oMailBatch.resetChannel(MailBatch.CHANNEL.FAX);
                m_oMailBatch.resetChannel(MailBatch.CHANNEL.POST);
                
            } catch (ContactDBException ex) {
                exception = ex;
                throw new TaskManager.SwingTask.Exception(Messages.getString("GUI_VersandAuftrag_Button_Zuruecksetzen_Fehler"));
            }
        }
        
        public void succeeded(TaskManager.Context ctx) {
            plantIt();
            adjustButtons();
            m_oVersandauftraegePanel.refreshList();
        }
        
        public void failed(TaskManager.Context ctx) {
            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_VersandAuftrag_Zuweisen_Fehler"), exception); //$NON-NLS-1$
        }
    }
}
