

package org.evolvis.nuyo.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.xana.config.DataFormatException;
import org.evolvis.xana.config.XmlUtil;
import org.w3c.dom.Node;

import de.tarent.commons.plugin.Plugin;

/**
 * The PluginRegistry manages Plugins for tarent-contact or any component that
 * uses this infrastructure. Plugins can be registered in different ways. Either
 * by calling the registerPlugin method within java code or by configuring the
 * plugins with xml.
 * @author Steffi Tinder, tarent GmbH
 */
public class PluginRegistry
{

  private static Logger logger = Logger.getLogger(PluginRegistry.class.getName());

  private static PluginRegistry instance;

  private static Map plugins = new HashMap();

  private static final String TAG_PLUGIN_ID = "id";

  private static final String TAG_PLUGIN_IMPLEMENTATION = "implementation";

  private static final Object singletonMonitor = new Object();

  private PluginRegistry()
  {
  }

  public static PluginRegistry getInstance()
  {

    synchronized (singletonMonitor)
      {

        if (instance == null)
          {

            instance = new PluginRegistry();
          }
      }
    return instance;
  }

  public void registerPlugin(Plugin plugin)
  {
    plugins.put(plugin.getID(), plugin);
  }

  public void init(Iterator pluginDefinitions)
  {
    List plugins = new ArrayList();

    try
      {
        while (pluginDefinitions.hasNext())
          {
            Map pluginMap = XmlUtil.getParamMap((Node) pluginDefinitions.next());
            plugins.add(pluginMap);
          }
      }
    catch (DataFormatException e)
      {
        logger.warning("Fehler beim Laden der Plugin-Daten");
        ApplicationServices.getInstance().getCommonDialogServices().publishError(
                                                                                 "Fehler beim Laden der Plugin-Daten",
                                                                                 e);
      }

    registerPlugins(plugins);
  }

  private void registerPlugins(List pluginDataMaps)
  {

    Map currentPluginParams;
    String implementation = "";
    String id = "";

    // iterate through the plugins in the list
    Iterator iterator = pluginDataMaps.iterator();
    try
      {
        while (iterator.hasNext())
          {

            currentPluginParams = (Map) iterator.next();

            if (!(currentPluginParams.containsKey(PluginRegistry.TAG_PLUGIN_ID) && currentPluginParams.containsKey(PluginRegistry.TAG_PLUGIN_IMPLEMENTATION)))
              {
                logger.warning("unvollst�ndiger Konfigurationseintrag f�r Plugin");
              }
            else
              {

                id = (String) currentPluginParams.get(PluginRegistry.TAG_PLUGIN_ID);
                implementation = (String) currentPluginParams.get(PluginRegistry.TAG_PLUGIN_IMPLEMENTATION);

                Plugin plugin = (Plugin) Class.forName(implementation).newInstance();
                logger.fine("f�ge Plugin " + id + " hinzu");
                registerPlugin(plugin);
              }
          }
      }
    catch (InstantiationException e)
      {
        logger.warning("Fehler beim Instantiieren der Klasse " + implementation);
      }
    catch (IllegalAccessException e)
      {
        logger.warning("Zugriffsproblem beim Laden der Klasse "
                       + implementation);
      }
    catch (ClassNotFoundException e)
      {
        logger.warning("Fehler beim Laden der Klasse " + implementation
                       + ". Klasse wurde nicht gefunden");
      }

  }

  public Plugin getPlugin(String pluginID)
  {
    return (Plugin) plugins.get(pluginID);
  }

  /**
   * Returns a list of all registered plugins, wich support the supplied
   * interface.
   */
  public List<Plugin> getSupportedPlugins(Class interfaceToSupport)
  {
    List<Plugin> supportedPlugins = new ArrayList<Plugin>();
    Iterator iter = plugins.values().iterator();

    while (iter.hasNext())
      {
        Plugin plugin = (Plugin) iter.next();
        if (plugin.isTypeSupported(interfaceToSupport))
          supportedPlugins.add(plugin);
      }

    return supportedPlugins;
  }

  public Plugin getDefaultPluginFor(Class interfaceToSupport)
  {
    Iterator iter = plugins.values().iterator();

    while (iter.hasNext())
      {
          Plugin plugin = (Plugin) iter.next();
          if (plugin.isTypeSupported(interfaceToSupport))
              return plugin;
      }

    return null;
  }


  public boolean isPluginAvailable(String pluginID)
  {
    return plugins.containsKey(pluginID);
  }

  public Map<String, Plugin> getRegisteredPlugins() {
	  return plugins;
  }
}
