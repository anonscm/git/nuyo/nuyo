/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Validators;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.ValidatorAdapter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IntegerRangeValidator extends ValidatorAdapter
{
  private int m_iMinimum = 0;
  private int m_iMaximum = 0;
  private boolean m_bUseMinimum = false;
  private boolean m_bUseMaximum = false;
  
  public String getListenerName()
  {
    return "IntegerRangeValidator";
  }  
  

  public IntegerRangeValidator()
  {
    super();
  }
    
  public Validator cloneValidator()
  {
    IntegerRangeValidator validator = new IntegerRangeValidator();
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      validator.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    return validator;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.VALIDATOR, "IntegerRangeValidator", new DataContainerVersion(0));
    ParameterDescription maxvalpd = new ParameterDescription("maxval", "maximaler Wert", "der maximale Wert der erlaubt ist.");
    maxvalpd.addValueDescription("-", "keine maximale Beschr�nkung");
    maxvalpd.addValueDescription("Integer", "ein beliebiger Integer Wert");
    d.addParameterDescription(maxvalpd);
    
    ParameterDescription minvalpd = new ParameterDescription("minval", "minimaler Wert", "der minimale Wert der erlaubt ist.");
    minvalpd.addValueDescription("-", "keine minimale Beschr�nkung");
    minvalpd.addValueDescription("Integer", "ein beliebiger Integer Wert");
    d.addParameterDescription(minvalpd);
    return d;
  }
  
  
  public boolean addParameter(String key, String value)
  {    
    if ("minval".equalsIgnoreCase(key)) 
    {
      try
      {
        m_iMinimum = Integer.parseInt(value);
        m_oParameterList.add(new ObjectParameter(key, value));
        m_bUseMinimum = true;
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        if ("-".equals(value))
        {
          m_bUseMinimum = false;        
          return true;
        }
        else return false;
      }
    }    
    else if ("maxval".equalsIgnoreCase(key)) 
    {
      try
      {
        m_iMaximum = Integer.parseInt(value);
        m_oParameterList.add(new ObjectParameter(key, value));
        m_bUseMaximum = true;
        return true;
      }
      catch(NumberFormatException nfe) 
      {
        if ("-".equals(value))
        {
          m_bUseMaximum = false;        
          return true;
        }
        else return false;
      }
    }
    
    return false;
  }

  public boolean canValidate(Class data)
  {
    return data.isInstance(Integer.class);
  }

  public boolean validate(Object data)
  {
    if (data instanceof Integer)
    {
      int intval = ((Integer)data).intValue();
      
      if (m_bUseMinimum)
      {
        if (intval > m_iMinimum) return false;
      }

      if (m_bUseMaximum)
      {
        if (intval > m_iMaximum) return false;
      }
    }
    return true;
  }

}
