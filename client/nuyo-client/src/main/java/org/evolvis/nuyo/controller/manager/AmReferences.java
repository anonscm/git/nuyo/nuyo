/* $Id: AmReferences.java,v 1.10 2007/08/30 16:10:28 fkoester Exp $
 * Created on 08.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ActionManager;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.controller.event.ContextVetoListener;
import org.evolvis.nuyo.controller.event.DataChangeListener;
import org.evolvis.nuyo.controller.event.MasterDataListener;
import org.evolvis.nuyo.datacontainer.parser.PluginLocator;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.security.SecurityManager;
import org.evolvis.xana.config.ConfigManager;



/**
 * Diese Klasse liefert die Referenzen, auf die der
 * {@link org.evolvis.nuyo.controller.ActionManager ActionManager}
 * aufsetzt.
 * 
 * @author mikel
 */
public class AmReferences implements StartUpConstants {
    /** Logger for {@link org.evolvis.nuyo.controller.ActionManager ActionManager}.*/
    public final static TarentLogger logger = new TarentLogger(Logger.getLogger(ActionManager.class.getPackage().getName()));
    /** Hier wird die Datenzugriffsschicht gehalten. */
    private Database db;
    /** Hier wird die GUI als Listener registriert. */
    private ControlListener controlListener;
    /** Hier wird der SecurityManager gehalten. */
    private SecurityManager securityManager;
    /** Hier wird die Ereignisverwaltung gehalten. */
    private final ActionManagerEvents eventsManager = new ActionManagerEvents();
    /** User preferences of tarent-contact.*/
    private final Preferences personalPreferences = ConfigManager.getPreferences();
    //
    private PluginLocator m_oPluginLocator;

    //
    // Getter und Setter
    //
    /** Die GUI als Listener. */
    protected ControlListener getControlListener() {
        return controlListener;
    }
    /** Die GUI als Listener. */
    protected void setControlListener(ControlListener newControlListener) {
        controlListener = newControlListener;
    }

    /** Datenzugriffsschicht */
    protected Database getDb() {
        return db;
    }
    /** Datenzugriffsschicht */
    protected void setDb(Database newDb) {
        db = newDb;
		securityManager = SecurityManager.getSecurityManager(db);
    }

    /** Ereignisverwaltung */
    protected ActionManagerEvents getEventsManager() {
        return eventsManager;
    }

    /** Applikations-Logger */
    public TarentLogger getLogger() {
        return logger;
    }

    /**
     * Diese Methode liefert einen Preferences-Knoten im Contact-Zweig. 
     * 
     * @param subNode relativer Pfad des Unterknotens. Falls <code>null</code>, so wird der
     *  contact-Basisknoten geliefert.
     * @return ausgew�hlter Preferences-Knoten
     */
    public Preferences getPreferences(String subNode) {
        while (subNode != null && subNode.startsWith("/"))
            subNode = subNode.substring(1);
        return subNode == null ? personalPreferences : personalPreferences.node(subNode);
    }

    /*
     * Diese Methoden und Variablen dienen der Event-Verarbeitung
     */
    /**
     * Diese Methode f�gt einen neuen
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     * @see org.evolvis.nuyo.controller.manager.ActionManagerEvents
     */
    public void addDataChangeListener(DataChangeListener listener) {
        getEventsManager().addDataChangeListener(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.DataChangeListener DataChangeListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     * @see org.evolvis.nuyo.controller.manager.ActionManagerEvents
     */
    public void removeDataChangeListener(DataChangeListener listener) {
        getEventsManager().removeDataChangeListener(listener);
    }

    /**
     * Diese Methode f�gt einen neuen
     * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addMasterDataListener(MasterDataListener listener) {
        getEventsManager().addMasterDataListener(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeMasterDataListener(MasterDataListener listener) {
        getEventsManager().removeMasterDataListener(listener);
    }

    /**
     * Diese Methode f�gt einen neuen
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     * @see org.evolvis.nuyo.controller.manager.ActionManagerEvents
     */
    public void addContextVetoListener(ContextVetoListener listener) {
        getEventsManager().addContextVetoListener(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link org.evolvis.nuyo.controller.event.ContextVetoListener ContextVetoListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     * @see org.evolvis.nuyo.controller.manager.ActionManagerEvents
     */
    public void removeContextVetoListener(ContextVetoListener listener) {
        getEventsManager().removeContextVetoListener(listener);
    }

    //
    // sonstiges
    //
    protected final static String asString(Object o) {
        return o == null ? null : o.toString();
    }

    protected String getStringFromMap(Map param, String key) {
        Object obj = param.get(key);
        return obj == null ? "" : asString(obj);
    }

    protected boolean getBooleanFromMap(Map param, String key) {
        Object obj = param.get(key);
        return obj instanceof Boolean ? ((Boolean) obj).booleanValue() : false;
    }
    
    protected Date getDateFromMap(Map param, String key){
    	Object obj = param.get(key);
    	return (obj != null && obj instanceof Date) ? (Date) obj : new Date();  
    }

    public PluginLocator getPluginLocator()
    {
      return m_oPluginLocator;
    }
    
    protected void setPluginLocator(PluginLocator pluginlocator)
    {
      m_oPluginLocator = pluginlocator;
    }
	
	public SecurityManager getSecurityManager() {
		return securityManager;
	}
	
	public void setSecurityManager(SecurityManager securityManager) {
		this.securityManager = securityManager;
	}
	
    
}
