package org.evolvis.nuyo.controller.manager;

import javax.swing.JFrame;

public interface CommonDialogServices {

	//
	// Getter und Setter
	//
	/** Hauptfenster der GUI */
	public JFrame getFrame();
	
	//
	// Standard-Kleindialog-Aufrufe
	//
	/*
	 * Fehlerausgaben
	 */
	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 * 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 */
	public void publishError(String msg);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 * 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param params Parameter, die in die Mitteilung eingef�gt werden m�ssen.
	 */
	public void publishError(String msg, Object[] params);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 *
	 * @param caption Titel eines eventuell benutzten Standardmeldungsfensters. 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 */
	public void publishError(String caption, String msg);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 *
	 * @param caption Titel eines eventuell benutzten Standardmeldungsfensters. 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param params Parameter, die in die Mitteilung eingef�gt werden m�ssen.
	 */
	public void publishError(String caption, String msg, Object[] params);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 * 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param e Throwable, deren Beschreibung ausgegeben werden soll.
	 */
	public void publishError(String msg, Throwable e);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 * 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param params Parameter, die in die Mitteilung eingef�gt werden m�ssen.
	 * @param e Throwable, deren Beschreibung ausgegeben werden soll.
	 */
	public void publishError(String msg, Object[] params, Throwable e);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 *
	 * @param caption Titel eines eventuell benutzten Standardmeldungsfensters. 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param e Throwable, deren Beschreibung ausgegeben werden soll.
	 */
	public void publishError(String caption, String msg, Throwable e);

	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 *
	 * @param caption Titel eines eventuell benutzten Standardmeldungsfensters. 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param extendedText Text, der auf Wunsch vom Benutzer zus�tzlich angeschaut werden kann.
	 */
	public void publishError(String caption, String msg, String extendedText);
	/**
	 * Diese Methode zeigt dem Benutzer die Fehlernachricht und loggt sie.
	 *
	 * @param caption Titel eines eventuell benutzten Standardmeldungsfensters. 
	 * @param msg Mitteilung, die dem Benutzer ausgegeben wird.
	 * @param params Parameter, die in die Mitteilung eingef�gt werden m�ssen.
	 * @param e Throwable, deren Beschreibung ausgegeben werden soll.
	 */
	public void publishError(String caption, String msg, Object[] params, Throwable e);

	/*
	 * Informationsausgaben
	 */
	/**
	 * Diese Methode zeigt dem Benutzer einen Informationstext.
	 * 
	 * @param text Informationstext
	 */
	public void showInfo(String text);

	/**
	 * Diese Methode zeigt dem Benutzer einen Informationstext.
	 * 
	 * @param caption Titel des Informationsfensters 
	 * @param text Informationstext
	 */
	public void showInfo(String caption, String text);

	/*
	 * Auswahlfrage
	 */
	/**
	 * Diese Methode stellt dem Benutzer eine Auswahlfrage.
	 * 
	 * @param question Text der Frage
	 * @param answers m�gliche Antworten
	 * @param defaultValue Standardantwort (0-basierter Index)
	 */
	public int askUser(String question, String[] answers, int defaultValue);

	/**
	 * Diese Methode stellt dem Benutzer eine Auswahlfrage.
	 * 
	 * @param caption Titel des Fragefensters 
	 * @param question Text der Frage
	 * @param answers m�gliche Antworten
	 * @param defaultValue Standardantwort (0-basierter Index)
	 */
	public int askUser(String caption, String question, String[] answers,
			int defaultValue);

	/**
	 * Diese Methode stellt dem Benutzer eine Auswahlfrage.
	 * 
	 * @param question Text der Frage
	 * @param answers m�gliche Antworten
	 * @param tooltips Tooltips zu den Antworten
	 * @param defaultValue Standardantwort (0-basierter Index)
	 */
	public int askUser(String question, String[] answers, String[] tooltips,
			int defaultValue);

	//
	// Sonstige GUI-Wrapper
	//
	/**
	 * Setzt je nach Parameter den Mauspfeil auf "Sanduhr" bzw. "normal";
	 * sollte mit dem Parameter "true" aufgerufen werden, bevor eine
	 * langwierige Operation ausgef�hrt wird. Nach Beendigung der Operation
	 * muss die Methode erneut aufgerufen werden, jedoch nun mit dem Parameter
	 * "false"
	 * 
	 * @param isWaiting
	 *            wenn true: Sanduhr anzeigen.
	 */
	public void setWaiting(boolean isWaiting);

	public String requestFilename(String startfolder, String presetfile,
			String[] extensions, String extensiondescription, boolean forSave);

}