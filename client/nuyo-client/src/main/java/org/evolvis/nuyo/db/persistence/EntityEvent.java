package org.evolvis.nuyo.db.persistence;


/**
 * @author kleinw
 *
 *	Dieses Event wird an alle Entities gesendet, die sich bei der Basisklasse 
 *	aller Entities angemeldet haben.
 *
 */
public class EntityEvent {
    
    /**	Die zum Event geh�rende Entit�t*/
    private IEntity _entity;
    
    
    /**	Konstruktor */
    public EntityEvent(IEntity entity) {_entity = entity;}
    
    
    /** dazugeh�rige Entit�t holen */
    public IEntity getSource() {return _entity;}
    
}
