/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class HandyDienstlichField extends GenericTextField
{
  public HandyDienstlichField()
  {
    super("HANDYDIENST", AddressKeys.HANDYDIENST, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Handy_dienstlich_ToolTip", "GUI_MainFrameNewStyle_Standard_Handy_dienstlich", 30);
  }
}
