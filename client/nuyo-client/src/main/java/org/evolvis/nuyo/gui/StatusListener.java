/* $Id: StatusListener.java,v 1.3 2007/01/12 12:44:59 aleksej Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui;

/** 
 * Interface for status panel.
 */
public interface StatusListener {

    /**
     * Sets an user name.
     */
    public void setUserName(String userName);
    
    /**
     * Sets a corresponding text label for search filter status. 
     * @param isActivated
     */
    public void setSearchFilterActived(boolean isActivated);
    
    /**
     * Sets a corresponding text label for category filter status. 
     * @param isActivated
     */
    public void setCategoryFilterActived(boolean isActivated);
    
    /**
     * Sets the count of contacts in form: {isCount} from {allCount}.
     * @param isCount
     */
    public void setSelectedContactsCount(int isCount);

    /**
     * Sets the count of contacts in form: {isCount} from {allCount}.
     * @param allCount
     */
    public void setContactsCount(int allCount);
}