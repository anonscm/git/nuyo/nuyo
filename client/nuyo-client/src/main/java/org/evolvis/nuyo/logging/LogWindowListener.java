/*
 * Created on 15.09.2004
 *
 */
package org.evolvis.nuyo.logging;

/**
 * @author niko
 *
 */
public interface LogWindowListener
{
  public void changedVisibility(boolean isvisible);
}
