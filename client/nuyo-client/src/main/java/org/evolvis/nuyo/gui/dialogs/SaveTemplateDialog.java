package org.evolvis.nuyo.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.TarentPanel;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.ui.EscapeDialog;

public class SaveTemplateDialog extends EscapeDialog {
	
	private GridBagConstraints constraints; 
		
	private TarentWidgetTextField fileNameField;
	private File templateDir;
	private String content;
	private String fileName;
	
	
	public SaveTemplateDialog(JFrame sender, String windowname, File templateDir, String content, String fileName){
		
		super(sender, true);
		constraints = new GridBagConstraints();
		this.templateDir = templateDir;
		this.content = content;
		this.fileName = fileName;
		
		this.getContentPane().setLayout(new BorderLayout());
		this.setLocation(400,400);
		this.setTitle(windowname);
		this.setVisible(false); 
		
		TarentPanel spacePanel = new TarentPanel();
		spacePanel.setBorder(new EmptyBorder(8,8,8,8));
		
		/************************************************
		 	Speichern und Abbrechen Buttons
	 	************************************************/ 
		
		TarentPanel buttonPanel = new TarentPanel();
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.setBorder(new EmptyBorder(8,8,8,8));
		
		JButton saveButton = new JButton("�bernehmen");
		saveButton.setToolTipText("�bernimmt die Einstellungen zur Terminbenachrichtigung f�r den Termin");
		saveButton.addActionListener(new saveButtonClicked());
		//saveButton.setFocusPainted(false);
		
		JButton cancelButton = new JButton("Abbrechen");
		cancelButton.setToolTipText("bricht diesen Dialog ab und verwirft die �nderungen");
		cancelButton.addActionListener(new cancelButtonClicked());
		
		buttonPanel.add(saveButton, BorderLayout.EAST);
		buttonPanel.add(cancelButton, BorderLayout.WEST);
				
		TarentPanel dialogPanel = new TarentPanel();
		dialogPanel.setLayout(new GridBagLayout());
		
		TarentWidgetLabel fileNameFieldDescription = new TarentWidgetLabel("Name der neuen Vorlage:                                                            ");
		fileNameField = new TarentWidgetTextField();	
		fileNameField.setLengthRestriction(100);

		constraints.anchor = GridBagConstraints.WEST;
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.insets = new Insets( 5, 0, 5, 0);
		constraints.gridx = 0;
		constraints.gridy = 0;
		dialogPanel.add(fileNameFieldDescription, constraints);
		constraints.gridy = 1;
		dialogPanel.add(fileNameField, constraints);
		
		spacePanel.add(dialogPanel);
		
		this.getContentPane().add(spacePanel, BorderLayout.CENTER);
		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		refresh();
	}
	
	private void refresh(){
		refresh(content, fileName);
	}
	private void refresh(String content, String fileName){
		
		fileNameField.setText(fileName != null? fileName : "");
		this.content = content;
		openWindow();
	}
	
	public void closeWindow(){
		fileName = "";
		fileNameField.setText(fileName);
		setVisible(false); 
		setModal(false);
	}
	 
	public void openWindow(){
		pack();
	 	setVisible(true);  
	 	setModal(true);
	}
	
	
	private class saveButtonClicked implements ActionListener
	{		
		public void actionPerformed(ActionEvent e)
		{	
			boolean write = false;
			String name = fileNameField.getText().trim();
			File outputFile = new File(templateDir.getAbsolutePath() + File.separator + name + ".txt");
			//Schon vorhanden?
			if (outputFile.exists()) {
				//�berschreiben?
				if (ApplicationServices.getInstance().getCommonDialogServices().askUser("überschreiben", "Wollen sie die Vorlage überschreiben?", new String[] { "Ja", "Nein" }, 2) == 0) {
					write = true;
				}
			} else {file:///home/nils/Tarent/workspace/contact-client unchanged/src/main/java/de/tarent/co
				write = true;
			}
			if (write) {
				FileWriter out;
				try {
					out = new FileWriter(outputFile);
					out.write(content);
					out.close();
				} catch (IOException ex) {
					ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_MainFrameNewStyle_Error_tarent-contact"), Messages.getString("GUI_MainFrameNewStyle_EMAIL_Vorlage_neu_Fehler"));
				}
			}
			
			closeWindow();
		}
	}
	private class cancelButtonClicked implements ActionListener
	{		
		public void actionPerformed(ActionEvent e)
		{	
			closeWindow();
			fileNameField.setText("");
		}
	}
}
