/*
 * Created on 29.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;


/**
 * @author niko
 *
 */
public class DataContainerErrors
{
  private List          m_oErrorHints;
  
  public DataContainerErrors(DataContainer container)
  {
    m_oErrorHints = new ArrayList();
  }
  
  public void addErrorHint(ErrorHint hint)
  {
    m_oErrorHints.add(hint);
  }

  public void removeErrorHint(ErrorHint hint)
  {
    m_oErrorHints.remove(hint);
  }

  public void clearErrorHints()
  {
    m_oErrorHints.clear();
  }

  public int getNumberOfErrorHints()
  {
    return m_oErrorHints.size();
  }
  
  public ErrorHint getErrorHint(int index)
  {
    return (ErrorHint)(m_oErrorHints.get(index));
  }
  
  public Iterator iterator()
  {
    return m_oErrorHints.iterator();
  }  
}
