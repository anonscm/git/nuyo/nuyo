package org.evolvis.nuyo.db.octopus;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentAddressRelation;
import org.evolvis.nuyo.db.AppointmentCalendarRelation;
import org.evolvis.nuyo.db.AppointmentReminder;
import org.evolvis.nuyo.db.AppointmentSequence;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Reminder;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;



/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.Appointment
 *
 */
public class AppointmentImpl extends AppointmentBean {//implements Cachable {

    //
    //	Soapanbindung
    //
	/** Appointments aus diesem Kalendar holen */
	private static final String METHOD_GET_EVENTS = "getEvents";
    /** Anlegen oder �ndern eines Appointments */
    private static final String METHOD_CREATE_OR_MODIFY_EVENT = "createOrModifyEvent";
    /** Appointment l�schen */
	private static final String METHOD_DELETE_EVENT = "deleteEvent";
	/** Ist das Appointment in der DB noch vorhanden? */
	private static final String METHOD_EVENT_STILL_EXISTS = "existsEvent";
	/** Einladung an Adressen verschicken */
	private static final String METHOD_INVITE = "invite";
    /** Ausladung an Adressen verschicken */
	private static final String METHOD_DISINVITE = "disinvite";
	/** Reminder f�r aktuellen User laden */
	private static final String METHOD_GETMYREMINDER = "getMyReminder";
	/** neuen Reminder f�r aktuellen User anlegen oder �ndern */
	private static final String METHOD_CREATEORMODIFYMYREMINDER = "createOrModifyMyReminder";
	/** l�scht den Reminder f�r den aktuellen User */
	private static final String METHOD_DELETEMYREMINDER ="deleteMyReminder";
	
	/** auszuladende Adressen */
    private static final String PARAM_ADDRESSES_TO_DISINVITE = "addressestodisinvite";
    /** Besitzer des Appointments */
	private static final String PARAM_OWNER = "owner";
    /** zu l�schende Reminder */
	private static final String PARAM_REMINDERS_REMOVE = "remindersremove";
    /** Reminder anlegen oder �ndern */
	private static final String PARAM_REMINDERS_CREATE_OR_MODIFY = "reminderscreateormodify";
    /** zu l�schende Kalenderrelationen */
	private static final String PARAM_SCHEDULE_RELATIONS_REMOVE = "schedulerelationsremove";
    /** Kalenderrelationen anlegen oder �ndern */
	private static final String PARAM_SCHEDULE_RELATIONS_CREATE_OR_MODIFY = "schedulerelationscreateormodify";
    /** zu l�schende Adressen */
	private static final String PARAM_ADDRESS_RELATIONS_REMOVE = "addressrelationsremove";
    /** Adressrelationen anlegen oder �ndern */
	private static final String PARAM_ADDRESSRELATIONSCREATEORMODIFY = "addressrelationscreateormodify";
    /** ID des Eigent�mers */
	private static final String PARAM_USERID = "userid";
    /** Kategorie des Appointments */
	private static final String PARAM_CATEGORY = "category";
    /** Vervollst�ndigungsgrad */
	private static final String PARAM_COMPLETITION = "completition";
    /** Priorit�t */
	private static final String PARAM_PRIORITY = "priority";
    /** Vervollst�ndigungsstatus */
	private static final String PARAM_IS_COMPLETED = "iscompleted";
    /** privat */
	private static final String PARAM_IS_PRIVATE = "isprivate";
    /** Termin oder Aufgabe */
	private static final String PARAM_IS_JOB = "isjob";
    /** Attribute  */
	private static final String PARAM_ATTRIBUTES = "attributes";
    /** Endzeitpunkt */
	private static final String PARAM_END = "end";
    /** Startzeitpunkt */
	private static final String PARAM_START = "start";
    /** Datenbank ID des Appointments */
	private static final String PARAM_EVENTID = "eventid";
	
	private static final String PARAM_REMINDEROFFSET = "reminderoffset";
	
	private static final String PARAM_REMINDERNOTE = "remindernote";
	
	public final static String P_HAS_REMINDER = "hasreminder"; // Boolean
    public final static String P_REMINDER_OFFSET = "reminderoffset"; // Long
    public final static String P_REMINDER_NOTE = "remindernote"; // String

	
	
	

	//
	//	Instanzmerkmale
	//
	/**	Formatierung f�r die Stringausgabe */
	final static SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy kk.mm.ss");

	
	//	Objekt zur Beschaffung von Appointmentobjekten
	static {
	    _fetcher = new AbstractEntityFetcher(METHOD_GET_EVENTS, true) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    Appointment appointment = new AppointmentImpl();
                    ((IEntity)appointment).populate(values);
                    return (IEntity)appointment;
                }
            };
	}
	
	
    /** Leerer Konstruktor */
    protected AppointmentImpl() {}        

    
    /**	Konstruktor zur Erzeugung des Objekts */
    protected AppointmentImpl(Integer id, Collection calendars, boolean isJob, Date startDate, Date endDate) throws ContactDBException {
        _start= startDate;
        _end= endDate;
        _isJob = new Boolean(isJob);
        _id = id;
        if (_calendarRelations == null)
            getCalendarRelations();
        if (calendars != null) {
	        for (Iterator it = calendars.iterator();it.hasNext();) { 
	            Calendar calendar = (Calendar)it.next();
	            CalendarRelationImpl relation = new CalendarRelationImpl(this, calendar);
	            relation.commit();
	            _calendarRelations.put(new Integer(calendar.getId()), relation);
	        }
        }
    }

    
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException { 
        if(_start == null) throw new ContactDBException(ContactDBException.EX_OBJECT_INCOMPLETE, "F�r das Speichern eines Appointments muss ein Startdatum �bergeben werden.");
        if(!isJob() && _end == null) throw new ContactDBException(ContactDBException.EX_OBJECT_INCOMPLETE, "F�r das Speichern eines Termins muss ein Enddatum gesetzt sein.");
    }
   
    
    /** Speichern eines Termins samt Relations und Reminders */
    public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_OR_MODIFY_EVENT);
        if(!_isJob.booleanValue() && _end == null) throw new ContactDBException(ContactDBException.EX_OBJECT_INCOMPLETE, "F�r das Speichern eines Appointments muss ein Enddatum angegeben sein.");
        if(_owner == null) throw new ContactDBException(ContactDBException.EX_OBJECT_INCOMPLETE, "F�r das Speichern eines Appointments muss ein Owner angegeben sein.");
        _method.add(PARAM_EVENTID, _id);
        _method.add(PARAM_START, new Long(_start.getTime()));
        _method.add(PARAM_END, new Long(_end.getTime()));
        _method.add(PARAM_ATTRIBUTES, _attributes);
        _method.add(PARAM_IS_JOB, _isJob);
        _method.add(PARAM_IS_PRIVATE, _isPrivate);
        _method.add(PARAM_IS_COMPLETED, _isCompleted);
        _method.add(PARAM_PRIORITY, _priority);
        _method.add(PARAM_COMPLETITION, _completition);
        _method.add(PARAM_CATEGORY, _category);
        _method.add(PARAM_USERID, new Integer(_owner.getId()));

        if (_addressRelations != null) {
            List relations = new Vector();
            List relationsToRemove = new Vector();
            for (Iterator it = _addressRelations.values().iterator();it.hasNext();) {
                AddressRelationImpl aar = (AddressRelationImpl)it.next();
                if (((IEntity)aar).isDeleted()) 
                    relationsToRemove.add(new Integer(aar.getId()));
                else if (!((IEntity)aar).isTransient()) {
                    List tmp = new Vector();
                    if (aar.getId() != 0) {
                        tmp.add(new Integer(aar.getId()));
                    } else
                        tmp.add(null);
                    tmp.add(new Integer(aar.getRequestType()));
                    tmp.add(new Integer(aar.getRequestStatus()));
                    tmp.add(new Integer(aar.getRequestLevel()));
                    if (_attributes != null) {
                        tmp.add(aar.getAttribute(AppointmentAddressRelation.KEY_DESCRIPTION));
                        tmp.add(aar.getAttribute(AppointmentAddressRelation.KEY_DISPLAY_NAME));
                    } else {
                        tmp.add(null);
                        tmp.add(null);
                    }
                    tmp.add(new Integer(aar.getAddress().getId()));
                    relations.add(tmp);
                }
            }
            _method.add(PARAM_ADDRESSRELATIONSCREATEORMODIFY, relations);
            _method.add(PARAM_ADDRESS_RELATIONS_REMOVE, relationsToRemove);
        }
        
        if (_calendarRelations != null) {
            List relations = new Vector();
            List relationsToRemove = new Vector();
            List reminders = new Vector();
            List remindersToRemove = new Vector(); 
            for (Iterator it = _calendarRelations.values().iterator();it.hasNext();) {
                CalendarRelationImpl acr = (CalendarRelationImpl)it.next();
                if (((IEntity)acr).isDeleted()) 
                    relationsToRemove.add(new Integer(acr.getId()));
                else if (!((IEntity)acr).isTransient()) {
                    List tmp = new Vector();
                    if (acr.getId() != 0)
                        tmp.add(new Integer(acr.getId()));
                    else
                        tmp.add(null);
                    tmp.add(new Integer(acr.getRequestType()));
                    tmp.add(new Integer(acr.getRequestStatus()));
                    tmp.add(new Integer(acr.getRequestLevel()));
                    tmp.add(acr.getAttribute(AppointmentCalendarRelation.KEY_DESCRIPTION));
                    tmp.add(acr.getAttribute(AppointmentCalendarRelation.KEY_DISPLAY_NAME));
                    tmp.add(new Integer(acr.getCalendar().getId()));
                    tmp.add(new Integer(acr.getDisplayMode()));
                    relations.add(tmp);
                }
                if (acr.getReminders() != null) {
                    for (Iterator it2 = acr.getReminders().iterator();it2.hasNext();) {
                        ReminderImpl ar = (ReminderImpl)it2.next();
                        if (((IEntity)ar).isDeleted()) 
                            remindersToRemove.add(new Integer(ar.getId()));
                        else {
                            if (!((IEntity)ar).isTransient()) {
                                List tmp = new Vector();
                                if (ar.getId() != 0)
                                    tmp.add(new Integer(ar.getId()));
                                else
                                    tmp.add(null);
                                tmp.add(new Integer(ar.getChannel()));
                                tmp.add(ar.getAttribute(AppointmentReminder.KEY_NOTE));
                                tmp.add(new Integer(ar.getTimeUnit()));
                                tmp.add(new Integer(ar.getTimeOffset()));
                                tmp.add(new Integer(acr.getCalendar().getId()));
                                reminders.add(tmp);
                            }
                        }
                    }
                }
            }
            _method.add(PARAM_SCHEDULE_RELATIONS_CREATE_OR_MODIFY, relations);
            _method.add(PARAM_SCHEDULE_RELATIONS_REMOVE, relationsToRemove);
            _method.add(PARAM_REMINDERS_CREATE_OR_MODIFY, reminders);
            _method.add(PARAM_REMINDERS_REMOVE, remindersToRemove);
        } 
        
        // Check if cache must be invalidated when adding a appointment
        if (_id == null){
        	_fetcher.getCache().flush();
        }
        
        _id = (Integer)_method.invoke();

        if(!isInvalid()) {
	        _addressRelations = null;
	        _calendarRelations = null;
	        getCalendarRelations();
        }

    }
    
    
    /**	Termin l�schen */
    public void delete() throws ContactDBException {
        Method method = new Method(METHOD_DELETE_EVENT);
        method.add(PARAM_EVENTID, _id);
        method.add(PARAM_OWNER, _owner.getLoginName());
        method.invoke();
    }

    
    /**	Urspr�nglichen Stand wieder herstellen */
    public void rollback() throws ContactDBException {populate(_responseData);}
    
    
    /**	Bef�llung des Objekts aus DB-Inhalten. @param list - Liste mit Werten */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        _isJob = data.getBoolean(1);
        _category = data.getInteger(2);
        _start = data.getDate(3);
        _end = data.getDate(4);
        setAttribute(Appointment.KEY_SUBJECT, data.getString(5));
        setAttribute(Appointment.KEY_DESCRIPTION,  data.getString(6));
        setAttribute(Appointment.KEY_DURATION,  data.getString(7));
        setAttribute(Appointment.KEY_LOCATION, data.getString(8));
        _isPrivate = data.getBoolean(9);
        _isCompleted = data.getBoolean(10);
        _owner = new UserImpl(data.getInteger(11));
        _completition = data.getInteger(12);
        _priority = data.getInteger(13);
    }
    
    
//    /** Nach einem Speichern muss der Cache geupdated werden */
//    public void updateInCache() throws ContactDBException {
//        EntityCache cache = _fetcher.getCache();
//        cache.store(Collections.singletonList(this));
//        for(Iterator it = cache.getSelections().iterator();it.hasNext();) {
//            ISelection selection = (ISelection)it.next();
//            for(Iterator it2 = selection.getFilterElements().iterator();it2.hasNext();) {
//                SelectionElement element = (SelectionElement)it2.next();
//                if(element instanceof AbstractRangedDateSelection) {
//                    AbstractRangedDateSelection ards = (AbstractRangedDateSelection)element;
//                    if(getStart().before(new Date(ards.getEnd().longValue())) && getEnd().after(new Date(ards.getStart().longValue()))) {
//                        Collection c = (Collection)cache.getValues().get(cache.getSelections().indexOf(selection));
//                        Integer i = new Integer(getId());
//                        // Nur rein wenn nicht schon drin
//                        if (! c.contains(i)) c.add(i);
//                    }
//                }
//            }
//        }
//    }
//    
//    
//    /**	Element aus dem Cache l�schen */
//    public void deleteFromCache() throws ContactDBException {_fetcher.getCache().delete(new Integer(getId()));}
    
    /**	Termin in DB vorhanden */
    public boolean stillExists() throws ContactDBException {
           Method method = new Method(METHOD_EVENT_STILL_EXISTS);
            method.add(PARAM_EVENTID, _id);
            return ((Boolean)method.invoke()).booleanValue();
    }
    
    /**	Einladung schicken */
    public int invite() throws ContactDBException {
        if (_owner != null) {
            Method method = new Method(METHOD_INVITE);
            method.add(PARAM_EVENTID, _id);
            method.add("userId", new Integer(_owner.getId()));
            return ((Integer)method.invoke()).intValue();
        } else {
            throw new ContactDBException(ContactDBException.EX_APPOINTMENT_ERROR, "Der Kalendar des Einladers konnte nicht gefunden werden.");
        }
    }

    
    /**	Ausladung schicken */	
    public int disinvite(List addresses) throws ContactDBException {
        if (addresses != null && addresses.size() > 0) {
            Method method = new Method(METHOD_DISINVITE);
            method.add(PARAM_EVENTID, _id );
            method.add("userId", new Integer(_owner.getId()));
            List tmp = new Vector();
            for (Iterator it = addresses.iterator();it.hasNext();) {
                tmp.add(new Integer(((Address)it.next()).getId()));
            }
            method.add(PARAM_ADDRESSES_TO_DISINVITE, tmp);
            return ((Integer)method.invoke()).intValue();
        } else
            return 0;
    }

    
    /**	Terminsequenz erzeugen */
    public AppointmentSequence createAppointmentSequence(String name, Date end, int frequency, boolean mayConflict) throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }

    
    /**	Hier kann eine Menge von Appointments bezogen werden */
   	static public Collection getAppointments(ISelection filter) throws ContactDBException {
   		
   		return _fetcher.getEntities(filter,false);
   	
   		
   	}
    
   	/**	Hier kann der Cache geflusht werden (beim Wechseln der Kalendeselektion */
   	static public void flushCache() { _fetcher.getCache().flush();}
   
   	
    /**	Stringrepr�senation des Objekts */
    public String toString() {
        return new StringBuffer()
        	.append(_id)
        	.append(" ")
        	.append(SDF.format(_start)).append("-")
        	.append(SDF.format(_end))
        	.append(" subject=")
        	.append(_attributes.get(KEY_SUBJECT))
        	.append(" description=")
        	.append(_attributes.get(KEY_DESCRIPTION))
        	.append(" duration=")
        	.append(_attributes.get(KEY_DURATION))
        	.append(" location=")
        	.append(_attributes.get(KEY_LOCATION))
        	.append(" isJob=")
        	.append(_isJob)
        	.append(" isJobCompleted=")
        	.append(_isCompleted)
        	.append(" isPrivate=")
        	.append(_isPrivate)
        	.append(" isOwner=")
        	.append(_owner)
        	.toString();
    }
    
    /**
     * gets the reminder for this appointment and the current user
     * returns null if no reminder is set for this appointment and user 
     */
    public Reminder getReminder() throws ContactDBException{
	    Method method = new Method(METHOD_GETMYREMINDER);
        method.add(PARAM_EVENTID, _id );
        Map tmp = (Map) method.invoke();
        
        Reminder reminder;
        
        Boolean hasReminder;
        Long reminderOffset;
        String description;
        
        if (tmp != null){
        	hasReminder = (Boolean)tmp.get(P_HAS_REMINDER);
        	reminderOffset = (Long) tmp.get(P_REMINDER_OFFSET);
        	description = (String) tmp.get(P_REMINDER_NOTE);
        	
        	if (hasReminder.booleanValue()) 
        		return new Reminder(description, _id.intValue() , reminderOffset.longValue());
        }            
        return null;        
	}

	public void setReminder(Reminder reminder) throws ContactDBException {
		
		Method method = new Method(METHOD_CREATEORMODIFYMYREMINDER);
        method.add(PARAM_EVENTID, _id );
        method.add(PARAM_REMINDEROFFSET, new Long(reminder.getOffSet()));
        method.add(PARAM_REMINDERNOTE, reminder.getDescription());
        method.invoke();
	}
	
	public void removeReminder() throws ContactDBException {
		
		Method method = new Method(METHOD_DELETEMYREMINDER);
        method.add(PARAM_EVENTID, _id );
        method.invoke();
		
	}
}
