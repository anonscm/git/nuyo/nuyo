package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Shows all contacts of a current category.
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ClearSearchFilterAction extends AbstractGUIAction {

    private static final long serialVersionUID = -8633278734167151549L;
    private static final TarentLogger logger = new TarentLogger(ClearSearchFilterAction.class);

    public void actionPerformed(ActionEvent e) {
        AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
        if (alp != null) {
            alp.clearFilter();
            ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
        }
    }
    
    public void init() {
    }
}