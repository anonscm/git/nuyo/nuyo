/* $Id: ObjectRoleImpl.java,v 1.8 2007/08/30 16:10:30 fkoester Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Role;
import org.evolvis.nuyo.db.cache.Cachable;
import org.evolvis.nuyo.db.cache.EntityCache;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;

import de.tarent.octopus.client.OctopusResult;

/**
 *
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class ObjectRoleImpl extends ObjectRoleBean implements Cachable {

	/**Methode um globale Rollen eines Users zu holen*/
	public static final String METHOD_GETROLES = "getObjectRoles";
	/**Methode um die globale Rolle anzulegen oder zu ver�ndern*/
	public static final String METHOD_CREATE_OR_MODIFY_ROLE = "createOrModifyObjectRole";
	/**Methode um eine globale Rolle zu l�schen*/
	public static final String METHOD_DELETE_ROLE = "deleteObjectRole";
	
	//	Initialisierung des Objekts zum Holen von Daten 
    static {
        _fetcher = new AbstractEntityFetcher(METHOD_GETROLES, false) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    Role role = new ObjectRoleImpl();
                    ((IEntity)role).populate(values);
                    return (IEntity)role;
                }
            };
    }
	
	/**	Statische Methode zum Beziehen aller GlobalRoles */
	static public List getObjectRoles(ISelection selection) throws ContactDBException {
		
		List objectRoles = new ArrayList(_fetcher.getEntities(selection));
		Collections.sort(objectRoles, new Comparator(){
			
			public int compare(Object o1, Object o2){
	    		String s1 = ((Role) o1).getRoleName().toLowerCase();
	    		String s2 = ((Role) o2).getRoleName().toLowerCase();
			
	    		return s1.compareTo(s2);
	    	}
		});
		
        return objectRoles;
    }


	public void updateInCache() throws ContactDBException {
        EntityCache cache = _fetcher.getCache();
        cache.store(null, Collections.singletonList(this));
	}

	public void deleteFromCache() throws ContactDBException {
		_fetcher.getCache().delete(_id);
	}

	public void populate(ResponseData data) throws ContactDBException {
		_responseData = data;
		setAttribute(KEY_ROLENAME, (String) data.get("Rolename"));
		setAttribute(KEY_ADD, getBooleanIntAsBoolean((Integer)data.get("AuthAdd")));
		setAttribute(KEY_ADDSUB, getBooleanIntAsBoolean((Integer)data.get("AuthAddsub")));
		setAttribute(KEY_EDIT, getBooleanIntAsBoolean((Integer) data.get("AuthEdit")));
		setAttribute(KEY_GRANT, getBooleanIntAsBoolean((Integer) data.get("AuthGrant")));
		setAttribute(KEY_READ, getBooleanIntAsBoolean((Integer) data.get("AuthRead")));
		setAttribute(KEY_REMOVE, getBooleanIntAsBoolean((Integer) data.get("AuthRemove")));
		setAttribute(KEY_STRUCTURE, getBooleanIntAsBoolean((Integer) data.get("AuthStructure")));
		setAttribute(KEY_REMOVESUB, getBooleanIntAsBoolean((Integer) data.get("AuthRemovesub")));
		
		if ( (((Integer) data.get("Roletype")).intValue() == 1)){ //|| ( ((Integer) data.get("Roletype")).intValue() == 2) ){
			setAttribute(KEY_DELETABLE, Boolean.FALSE);
			//System.out.println("Rolle \"" + ((String) data.get("Rolename")) + "\" kann nicht gel�scht werden");
		}
		else setAttribute(KEY_DELETABLE, Boolean.TRUE);
		
		_id = (Integer)data.get("Pk");
	}
	
	private String getBooleanIntAsString(Integer source){
		
		if (source == null)
			return "null";
		if(source.intValue()==1){
			return Boolean.TRUE.toString();
		}else{
			return Boolean.FALSE.toString();
		}
	}

	private Boolean getBooleanIntAsBoolean(Integer source){
		if (source == null)
			return Boolean.FALSE;
		if(source.intValue()==1){
			return Boolean.TRUE;
		}else{
			return Boolean.FALSE;
		}
	}

	public void validate() throws ContactDBException {
		//Nichts f�rs erste
	}

	public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_OR_MODIFY_ROLE);
        Object tmp = _method.invoke();
        if(tmp instanceof Integer)_id=(Integer)tmp;
        else if(tmp instanceof String)_id=Integer.valueOf((String)tmp);
        else throw new ContactDBException(ContactDBException.EX_INVALID_USER, "Ung�ltige Daten �ber die Role zur�ckerhalten!");
	}

	public void delete() throws ContactDBException {
		Method method = new Method(METHOD_DELETE_ROLE);
		method.addParam("id", _id);
		method.invoke();
		OctopusResult result = method.getORes();
        String errorMessage = (String)result.getData("error");
        if (errorMessage != null && errorMessage.length() > 0){
        	throw new ContactDBException(ContactDBException.EX_ENTITY_ERROR, errorMessage);
        }
        
	}

	public void rollback() throws ContactDBException {
		populate(_responseData);
	}
	
	public String toString(){
		return getAttributeAsString(KEY_ROLENAME);
	}
}
