/*
 tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 Copyright (C) 2002 tarent GmbH

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 tarent GmbH., hereby disclaims all copyright
 interest in the program 'tarent-contact'
 (which makes passes at compilers) written
 by Nikolai R�ther.
 signature of Elmar Geese, 1 June 2002
 Elmar Geese, CEO tarent GmbH
 */


package org.evolvis.nuyo.controls;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

/**
 * @author niko
 */
public class TarentWidgetDateSpinner extends JSpinner implements
    TarentWidgetInterface
{
  public TarentWidgetDateSpinner()
  {
    super(new SpinnerDateModel());
  }

  public TarentWidgetDateSpinner(String dateFormat)
  {
    super(new SpinnerDateModel());
    
    setEditor(new DateEditor(this, dateFormat));
  }
  
  public TarentWidgetDateSpinner(Date date)
  {
    super(new SpinnerDateModel());
    this.setData(date);
  }

  /**
   * @see TarentWidgetInterface#setData(Object)
   */
  public void setData(Object data)
  {
    if (data instanceof Date)
      {
        ((SpinnerDateModel) this.getModel()).setValue((Date) data);
      }
  }

  /**
   * @see TarentWidgetInterface#getData()
   */
  public Object getData()
  {
    return (((SpinnerDateModel) this.getModel()).getDate());
  }

  public JFormattedTextField getJFormattedTextField()
  {
    return (getJFormattedTextFieldRecursive(this.getEditor()));
  }

  private JFormattedTextField getJFormattedTextFieldRecursive(Component comp)
  {
    if (comp instanceof JPanel)
      {
        for (int i = 0; i < ((JPanel) comp).getComponentCount(); i++)
          {
            JFormattedTextField tf = getJFormattedTextFieldRecursive(((JPanel) comp).getComponent(i));
            if (tf != null)
              return (tf);
          }
      }
    else
      {
        if (comp instanceof JFormattedTextField)
          {
            return ((JFormattedTextField) comp);
          }
      }
    return (null);
  }

  public JComponent getComponent()
  {
    return (this);
  }

  public void setLengthRestriction(int maxlen)
  {
  }

  public void setWidgetEditable(boolean iseditable)
  {
    this.setEnabled(iseditable);
  }

  public boolean isEqual(Object data)
  {
    if (data instanceof Date)
      {
        return ((((SpinnerDateModel) this.getModel()).getDate()).equals(((Date) data)));
      }
    else
      {
        return (false);
      }
  }

  private List m_oWidgetChangeListeners = new ArrayList();

  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }

  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);
  }

}
