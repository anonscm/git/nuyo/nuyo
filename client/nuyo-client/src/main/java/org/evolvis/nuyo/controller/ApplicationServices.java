package org.evolvis.nuyo.controller;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.net.URL;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.manager.CommonDialogServices;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.BeanShellService;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.utils.IconFactory;

import de.tarent.commons.datahandling.binding.BindingManager;

/**
 * This class keeps different application services, 
 * that can be used by every component of the application. 
 * The implemetations of the services can be registered an replaced during execution.
 *  
 * 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class ApplicationServices {
	
	private static final TarentLogger logger = new TarentLogger(ApplicationServices.class);
	
	private static ApplicationServices instance;
	private CommonDialogServices commonDialogServices;
	private ControlListener controlListener;
	private Database currentDatabase;
	private GUIListener guiListener;    
	private IconFactory iconFactory;
        private BeanShellService bshService = new BeanShellService();
        
    private HelpBroker helpBroker;

    private BindingManager bindingManager;

    /**
     * The main application model
     */
    private ApplicationModel applicationModel;
    
    private static final Object singletonMonitor = new Object();

	private ApplicationServices(){
	}

	/**
	 * This message returns the Instance of ApplicationServices. 
	 * If it does not exist, it will be created at this point.
	 * @return the instance of ApplicationServices
	 */
	public static ApplicationServices getInstance(){

		synchronized (singletonMonitor) {

			if (instance == null){
		
				instance = new ApplicationServices();
			}
		}
		
		return instance;
	}
	/**
	 * This method returns the currently registered implementation of 
	 * CommonDialogServices or null if no implementation is registered.
	 * @return the registered CommonDialogServices-Implementation
	 */
	public CommonDialogServices getCommonDialogServices() {
		if(commonDialogServices == null) logger.fine("[ApplicationServices]: Common Dialog Services not found");
		return commonDialogServices;
	}
	/**
	 * Registers an implementation of CommonDialogServices.
	 * @param commonDialogServices the Implementation of CommonDialogServices that shall be registered
	 */
	public void setCommonDialogServices(CommonDialogServices commonDialogServices) {
		this.commonDialogServices = commonDialogServices;
	}
	/**
	 * @return the registered IconFactory
	 */
	public IconFactory getIconFactory() {
		if(iconFactory == null) logger.fine("[ApplicationServices]: iconFactory not found");
		return iconFactory;
	}
	/**
	 * @param iconFactory the Instance of IconFactory that shall be registered.
	 */
	public void setIconFactory(IconFactory iconFactory) {
		instance.iconFactory = iconFactory;
	}
	
	/**
	 * registers the current database
	 * @param currentDatabase
	 */
	public void setCurrentDatabase(Database currentDatabase) {
		this.currentDatabase = currentDatabase;
	}

	/**
	 *  @return the current Database
	 */
	public Database getCurrentDatabase() {
		if(currentDatabase == null) logger.fine("[ApplicationServices]: current Database not found");
		return currentDatabase;
	}

	/**
	 * @return the ActionManager
	 */
    public GUIListener getActionManager() {
    	if(guiListener == null) logger.fine("[ApplicationServices]: ActionManager not found");
        return guiListener;
    }

    /**
     * sets the ActionManager
     */
    public void setActionManager(GUIListener newGuiListener) {
        this.guiListener = newGuiListener;
    }

    /**
     * @return the MainFrame
     */
    public ControlListener getMainFrame() {
    	if(controlListener == null) logger.fine("[ApplicationServices]: Main Frame not found");
        return controlListener;
    }

    /**
     * sets the MainFrame
     */
    public void setMainFrame(ControlListener newControlListener) {
        this.controlListener = newControlListener;
    }

    /**
     * Returns the current help broker
     */
    public HelpBroker getHelpBroker() {
        //set up look and feel by each invocation (this is a workaround for bug 2758)
        //TODO: a better way should be found to update the l&f + restore lazy initialization
        try {
            logger.fine("initializing help...");
            ClassLoader loader = getClass().getClassLoader();            
            URL url = HelpSet.findHelpSet(loader, StartUpConstants.HELP_SET_PATH);
            HelpSet helpSet = new HelpSet(loader, url);
            helpBroker = helpSet.createHelpBroker();
            Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
            helpBroker.setLocation(new Point(screenDimension.width/2 - 300, 150));
//            helpBroker.setCurrentID("bertarentcontact.htm");
        }
        catch ( HelpSetException e ) {
            logger.warning("[!] failed loading help set: " + StartUpConstants.HELP_SET_PATH);
        }
        catch ( Exception e ) {
            logger.warning("[!] interupted exception while initializing help broker: " + e.getMessage(), e);
        }
        return helpBroker;
    }


    public BindingManager getBindingManager() {
        if(bindingManager == null) {
            logger.warning("[!] binding manager not initialized!");
        }
        return bindingManager;
    }

    public void setBindingManager(BindingManager bm) {
        if(bindingManager != null) {
            logger.warning("[!] can't set binding manager: already exists");
        }
        bindingManager = bm;
    }

    public ApplicationModel getApplicationModel() {
        if(applicationModel == null) {
            logger.warning("[!] binding manager not initialized!");
        }
        return applicationModel;
    }

    public void setApplicationModel(ApplicationModel model) {
        if(applicationModel != null) {
            logger.warning("[!] can't set binding manager: already exists");
        }
        applicationModel = model;
    }
    
    /**
     * Provides access to the application's {@link BeanShellService} instance.
     * 
     * @return
     */
    public BeanShellService getConsole()
    {
      return bshService;
    }
}