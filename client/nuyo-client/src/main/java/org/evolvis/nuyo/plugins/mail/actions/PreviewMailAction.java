/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailPanel;
import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.action.ActionRegistry;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class PreviewMailAction extends AbstractGUIAction
{
	protected void init()
	{
		super.init();
	}

	public void actionPerformed(ActionEvent e)
	{
		setSelected(!isSelected());
		
		if(isSelected)
			//ActionRegistry.getInstance().switchContext(DispatchEMailDialog.EMAIL_EDIT_MODE, DispatchEMailDialog.EMAIL_PREVIEW_MODE);
			ActionRegistry.getInstance().enableContext(DispatchEMailPanel.EMAIL_PREVIEW_MODE);
		else
			//ActionRegistry.getInstance().switchContext(DispatchEMailDialog.EMAIL_PREVIEW_MODE, DispatchEMailDialog.EMAIL_EDIT_MODE);
			ActionRegistry.getInstance().disableContext(DispatchEMailPanel.EMAIL_PREVIEW_MODE);
	}
}
