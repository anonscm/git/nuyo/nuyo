/* $Id: ChangeAddressListCommand.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 09.10.2003
 */
package org.evolvis.nuyo.messages;

import org.evolvis.nuyo.db.Addresses;

/**
 * Diese Kommandoklasse bewirkt einen Wechsel der aktuellen Addressauswahl.
 * 
 * @author mikel
 */
public class ChangeAddressListCommand extends AddressListCommand {
    /*
     * Konstruktoren
     */
    /**
     * @param source Quelle des Kommandos
     * @param addresses neue Addressauswahl
     * @param title neuer Titel
     * @param description neue Beschreibung
     */
    public ChangeAddressListCommand(Object source, Addresses addresses, String title, String description) {
        super(source);
        this.addresses = addresses;
        this.title = title;
        this.description = description;
    }

    /*
     * Getter und Setter
     */
    /**
     * @return Addressauswahl
     */
    public Addresses getAddresses() {
        return addresses;
    }

    /**
     * @return Beschreibung
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return Titel
     */
    public String getTitle() {
        return title;
    }

    /*
     * gesch�tzte Variablen 
     */
    Addresses addresses;
    String title;
    String description;
}
