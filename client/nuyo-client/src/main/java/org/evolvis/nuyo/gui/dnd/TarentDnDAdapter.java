/*
 * Created on 03.02.2004
 *
 */
package org.evolvis.nuyo.gui.dnd;

import java.awt.event.MouseEvent;

import javax.swing.JComponent;

/**
 * @author niko
 *
 */
public class TarentDnDAdapter implements TarentDnDListener
{
  public void objectDropped(JComponent comp, Object obj)
  {
    //System.out.println("objectDropped(" + comp + ", " + obj + ") type=" + obj.getClass().getName());
  }

  public TarentDnDData objectDragged(JComponent comp)
  {
    //System.out.println("objectDragged(" + comp + ")");
    return new TarentDnDData();
  }
    
  public void exportDone(JComponent comp)
  {
    comp.dispatchEvent(new MouseEvent(comp, MouseEvent.MOUSE_RELEASED, 0, MouseEvent.BUTTON1_MASK, 0, 0, 1, false, MouseEvent.BUTTON1)); 
  }
}
