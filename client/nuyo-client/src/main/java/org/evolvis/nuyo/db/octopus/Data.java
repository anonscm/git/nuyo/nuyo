package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentReminder;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Resource;
import org.evolvis.nuyo.db.Schedule;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;

/**
 * @author kleinw
 *	
 *	Diese Klasse existiert vorr�bergehend f�r die JUnits im srcTest Verzeichnis.
 *
 */
public class Data {

    static public Appointment createAppointment() throws ContactDBException {return new AppointmentImpl();}
    static public Calendar createCalendar() throws ContactDBException {return new CalendarImpl();}
    static public User createUser() throws ContactDBException {return new UserImpl();}
    static public User createUser(String login) throws ContactDBException {return new UserImpl(login);}
    static public User createUser(Integer id) throws ContactDBException {return new UserImpl(id);}
    static public Resource createResource() throws ContactDBException {return new ResourceImpl();}
    static public AppointmentReminder createReminder() throws ContactDBException {return new ReminderImpl();}
    static public UserGroup createUserGroup() throws ContactDBException {return new UserGroupImpl();}
    static public Schedule createSchedule(String user) throws ContactDBException {return new ScheduleImpl(new UserImpl(user));}
    static public Category createCategory() throws ContactDBException {return new Category();}
    
}
