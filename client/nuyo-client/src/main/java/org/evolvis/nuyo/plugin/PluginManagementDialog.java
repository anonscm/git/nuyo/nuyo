/**
 * 
 */
package org.evolvis.nuyo.plugin;

import javax.swing.JFrame;
import javax.swing.JList;

import de.tarent.commons.ui.EscapeDialog;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class PluginManagementDialog extends EscapeDialog {
	
	protected JList pluginList;

	public PluginManagementDialog(JFrame parent) {
		super(parent, "Plugins");
		
		getContentPane().add(getPluginList());
		pack();
		setLocationRelativeTo(parent);
	}
	
	protected JList getPluginList() {
		if(pluginList == null) {
			pluginList = new JList(PluginRegistry.getInstance().getRegisteredPlugins().values().toArray());
//			pluginList.setCellRenderer(new DefaultListCellRenderer() {
//				@Override
//				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
//					return super.getListCellRendererComponent(list, ((Plugin)value).getDisplayName(), index, isSelected, cellHasFocus);
//				}
//			});
		}
		return pluginList;
	}
}
