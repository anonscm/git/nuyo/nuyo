package org.evolvis.nuyo.gui.dnd;
import javax.swing.JComponent;

/*
 * Created on 24.07.2003
 *
 */

/**
 * @author niko
 *
 */
public interface TarentDnDListener
{
  // diese Methode wird aufgerufen wenn eine DnD-Operation erfolgreich 
  // abgeschlossen wurde. Der Parameter comp enth�lt die Komponente 
  // �ber der fallengelassen wurde. tarentDnDData ist das Objekt das der 
  // Funktion objectDragged() beim Start der DnD-Operation �bergeben 
  // wurde (s.u.)  
  public void objectDropped(JComponent comp, Object obj);
  
  
  // diese Methode wird aufgerufen wenn eine DnD-Operation �ber einer 
  // Komponente gestartet wird. Der Parameter comp enth�lt diese Komponente
  // �ber der gestartet wurde. Die Methode soll ein tarentDnDData-Objekt zur�ckgeben
  // welches nach erfolgreichem fallenlassen �ber einer anderen Komponente der Methode
  // objectDropped() �bergeben wird (s.o.)
  public TarentDnDData objectDragged(JComponent comp);
  
  // wird Aufgerufen wenn eine DnD Aktion abgeschlossen wurde... egal ob
  // erfolgreich oder nicht!
  public void exportDone(JComponent comp);
  
}
