/* $Id: ContactAction.java,v 1.5 2006/12/12 09:30:59 robert Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui;


/**
 * Schnittstelle zum Kapseln von Aktionen in TarentContact
 */
public interface ContactAction extends javax.swing.Action {

    /**
     * Liefert einen global eindeutigen Namen, �ber den die Action angesprochen werden kann.
     */
    public String getUniqueName();

    /**
     * Gibt an, ob die Action in dem entsprechenden Container (Men�, Toolbar, ..) angezeigt werden soll.
     *
     */
    public boolean attachToContainer(String containerName);

    /**
     * Liefert den Men�bereich, unter dem die Action angezeigt werden soll. Derzeit sind die Konstanten unter MenuBar erlaubt.
     */
    public String getMenuSection();
}