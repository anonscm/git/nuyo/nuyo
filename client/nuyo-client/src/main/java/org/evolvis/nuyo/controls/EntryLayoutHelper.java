/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 Copyright (C) 2002 tarent GmbH

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 tarent GmbH., hereby disclaims all copyright
 interest in the program 'tarent-contact'
 (which makes passes at compilers) written
 by Nikolai R�ther.
 signature of Elmar Geese, 1 June 2002
 Elmar Geese, CEO tarent GmbH*/


package org.evolvis.nuyo.controls;

import java.awt.Container;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.logging.TarentLogger;

import com.jgoodies.forms.layout.CellConstraints;


/**
 * This class' task is to translate a string containing layout
 * information for the {@link com.jgoodies.forms.layout.FormLayout}
 * and put components into a container according to the
 * given layout information.
 * 
 * <p>Historically this class had a very different use
 * and provides certain features for legacy support
 * only.</p>
 * 
 * <p>The current implementation is closely tied to all
 * classes implementing the {@link org.evolvis.nuyo.gui.fieldhelper.AddressField}
 * interface. These classes must create an <code>EntryLayoutHelper</code>
 * instance within their {@link org.evolvis.nuyo.gui.fieldhelper.AddressField#getPanel(String)}
 * method. This is the only place where <code>EntryLayoutHelper</code> 
 * shall be used.
 * </p>
 * 
 * <p>In the past instances of {@link TarentWidgetInterface} have been
 * stacked together building one big widget which controlled the layout
 * on its own. With the <code>EntryLayoutHelper</code> those subcomponents
 * have to be given to it at instantiation time along with the layout
 * information. The layout information will be parsed, error checked and
 * the components will be put into a container according to the layout
 * information.</p>
 * 
 * <p>The layout information itself comes from the appearance configuration
 * document as a widget's flags (hence the variable name). A layout string
 * resembles the method calls that are available in the {@link CellConstraints}
 * class, e.g. <code>xy(3,4) xyw(2,12,3) xywh(33,44,1,3)</code>. This string
 * contains three constraints. Please make sure you do not violate the 
 * string formatting to much otherwise the parser will fail.</p>
 * 
 * <p>The amount of contraints in a layout string directly depends on the
 * {@link org.evolvis.nuyo.gui.fieldhelper.AddressField} implementing
 * class that uses it. If you do something wrong the parser will provide
 * the correct number in its exception message.</p> 
 *  
 * <p>Note: The whole concept of {@link org.evolvis.nuyo.gui.fieldhelper.AddressField}
 * implementations is under serious reconsideration and will change.</p>
 * 
 * @author Robert Schuster
 */
public class EntryLayoutHelper
{

  private static final TarentLogger logger = new TarentLogger(
                                                              EntryLayoutHelper.class);

  private TarentWidgetInterface[] widgets;

  private int[][] decodedFlags;

  /**
   * Creates an instance for a variable number of widgets.
   * 
   * <p>The number of widgets and the amount of layout contraints in the
   * <code>widgetFlags</code> string must match. Otherwise an exception will
   * be thrown when parsing them.</p>
   * 
   * @param widgets
   * @param widgetFlags
   */
  public EntryLayoutHelper(TarentWidgetInterface[] widgets, String widgetFlags)
  {
    this.widgets = widgets;
    decodedFlags = decodeWidgetFlags(widgetFlags);
  }

  /**
   * Creates an instance for a single widget which is usually a label
   * thus being a title component.
   * 
   * See {@link #EntryLayoutHelper(TarentWidgetInterface[], String)}
   * for more details.
   * 
   * @param title
   * @param widgetFlags
   */
  public EntryLayoutHelper(TarentWidgetInterface title, String widgetFlags)
  {
    this(new TarentWidgetInterface[] { title }, widgetFlags);
  }

  /**
   * Creates an instance for a two widgets.
   * 
   * See {@link #EntryLayoutHelper(TarentWidgetInterface[], String)}
   * for more details.
   * 
   * @param title
   * @param widgetFlags
   */
  public EntryLayoutHelper(TarentWidgetInterface title,
                             TarentWidgetInterface component, String widgetFlags)
  {
    this(new TarentWidgetInterface[] { title, component }, widgetFlags);
  }

  /**
   * Returns the widget that the old system considered to be the data
   * containing widget (= the entry component).
   * 
   * <p>In the new system this is always the second component
   * managed by the <code>EntryLayoutHelper</code>.</p>
   * 
   * <p>This method will fail for title only components and should be avoided
   * at all costs.</p>
   * 
   * @return
   * @deprecated
   */
  public TarentWidgetInterface getEntryComponent()
  {
    return widgets[1];
  }

  /**
   * Returns the widget that the old system considered to be the
   * title component.
   * 
   * <p>In the new system this is always the first component
   * managed by the <code>EntryLayoutHelper</code>.</p>
   * 
   * @return
   * @deprecated
   */
  public TarentWidgetInterface getTitleComponent()
  {
    return widgets[0];
  }

  /**
   * Adds the components managed by this instance
   * to the given container by using the layout information
   * provided and parsed at creation time.
   * 
   * <p>The container must use the {@link com.jgoodies.forms.layout.FormLayout}
   * otherwise the method will fail.</p<
   * 
   * @param c
   */
  public void addToContainer(final Container c)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        int[][] d = decodedFlags;

        if (d != null && d.length > 0)
          {
            CellConstraints cc = new CellConstraints();
            for (int i = 0; i < widgets.length; i++)
              {
                if (d.length > i && d[i] != null)
                  c.add(widgets[i].getComponent(), cc.xywh(d[i][0], d[i][1],
                                                           d[i][2], d[i][3]));
                else
                  logger.warningSilent("missing cellconstraints: widget not added for: "
                                       + widgets[0]);
              }
          }
        else
          {
            throw new IllegalStateException("no cell constraints available");
          }

      }
    });
  }

  private static Pattern DECODE_PATTERN;

  /**
   * Parses the layout information string and puts the result into an
   * array which can then be used later to add the components correctly
   * layouted.
   * 
   * <p>E.g. <code>xy(3,2) xyw(6,2,3) xywh(10,2,3,3)</code> becomes
   * <code>new int[][] { new int[] { 3, 2, 1, 1 }, new int[] { 6, 2, 3, 1}, new int[] { 10, 2, 3, 3}}</code></p>
   * 
   * <p>As you see, for simplicity, missing entries are treated as
   * <code>1</code> which is what the {@link com.jgoodies.forms.layout.FormLayout}
   * would do as well.</p>
   * 
   * <p>In case the number of decoded contraints does not match to the number
   * of managed components an {@link IllegalStateException} is thrown.</p>
   * 
   * @param widgetFlags
   * @return
   */
  private int[][] decodeWidgetFlags(String widgetFlags)
  {
    if (DECODE_PATTERN == null)
      {
        DECODE_PATTERN = Pattern.compile("\\s*\\(\\s*|,|\\s*\\)\\s*");
      }

    String[] split = DECODE_PATTERN.split(widgetFlags);

    int[][] ret = new int[widgets.length][];

    int i = 0;
    int idx = 0;
    try
      {
        while (i < split.length)
          {
            if (idx >= ret.length)
              throw new IllegalStateException("Too many layout constraints given. Element needs only " + ret.length + " - widget flags: " + widgetFlags);
            
            split[i] = split[i].trim();

            if (split[i].equals("xy"))
              {
                ret[idx] = new int[] { Integer.parseInt(split[i + 1]),
                                      Integer.parseInt(split[i + 2]), 1, 1 };

                i += 3;
              }
            else if (split[i].equals("xyw"))
              {
                ret[idx] = new int[] { Integer.parseInt(split[i + 1]),
                                      Integer.parseInt(split[i + 2]),
                                      Integer.parseInt(split[i + 3]), 1 };

                i += 4;
              }
            else if (split[i].equals("xywh"))
              {
                ret[idx] = new int[] { Integer.parseInt(split[i + 1]),
                                      Integer.parseInt(split[i + 2]),
                                      Integer.parseInt(split[i + 3]),
                                      Integer.parseInt(split[i + 4]) };

                i += 5;
              }
            else
              {
                logger.warningSilent("superflous stuff discarded");
                i++;
              }

            idx++;
          }
      }
    catch (ArrayIndexOutOfBoundsException e)
      {
        throw (IllegalStateException) 
          new IllegalStateException("Error parsing widget flags '"
                                        + widgetFlags + ". Parsed array: " + Arrays.toString(split)).initCause(e);
      }
    
    if (idx < ret.length - 1)
      throw new IllegalStateException("Too few layout constraints given. Elements needs " + ret.length + " - widget flags: " + widgetFlags);

    return ret;
  }

}