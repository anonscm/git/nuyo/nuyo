/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class FaxPrivatField extends GenericTextField
{
  public FaxPrivatField()
  {
    super("FAXPRIVAT", AddressKeys.FAXPRIVAT, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Fax_privat_ToolTip", "GUI_MainFrameNewStyle_Standard_Fax_privat", 30);
  }
}
