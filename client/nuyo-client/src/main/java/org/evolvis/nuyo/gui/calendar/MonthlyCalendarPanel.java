/*
 * Created on 11.03.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.fieldhelper.WidgetPool;
import org.evolvis.nuyo.gui.fields.ScheduleNavigationField;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;
import org.evolvis.xana.swing.utils.SwingIconFactory;


/**
 * @author niko
 *
 */
public class MonthlyCalendarPanel extends JPanel
{
  private GUIListener m_oGUIListener;
  private ScheduleData m_oScheduleData;  
  private Date m_oStartDate = new Date();
  private GregorianCalendar m_oGregorianCalendar;
  private JPanel m_oGridPanel; 
  private JSpinner m_oSpinnerYear;
  private JComboBox m_oComboMonth;
  
  private Font m_oFontDayButton;
  private Font m_oFontDayLabel;
  
  private ImageIcon m_oIconWeekSelect;
  
  private Map m_oDayButtonMap;
  private Color m_oSchedulePointColor = Color.RED;
  
  private WidgetPool m_oWidgetPool;
  
  public MonthlyCalendarPanel(GUIListener guiListener, WidgetPool widgetpool, ScheduleData oScheduleData)
  {
    m_oDayButtonMap = new HashMap();
    this.setLayout(new BorderLayout());    
    
    m_oGUIListener = guiListener; 
    m_oScheduleData = oScheduleData;
    m_oWidgetPool = widgetpool;
    
    m_oFontDayButton = new Font(null, Font.PLAIN, 12);
    m_oFontDayLabel = new Font(null, Font.BOLD, 12);
    m_oIconWeekSelect = (ImageIcon)SwingIconFactory.getInstance().getIcon("weekselect.gif");
    
    
    m_oGridPanel = new JPanel(new GridLayout(0,8));
        
    m_oGregorianCalendar = new GregorianCalendar();
    //m_oGregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
    m_oGregorianCalendar.setTime(m_oStartDate);
    
    createGrid();
    this.add(m_oGridPanel, BorderLayout.CENTER);
  }

  private ScheduleEntryPanel m_oActiveScheduleEntryPanel;
  public void setActiveEntry(ScheduleEntryPanel entry)
  {
    m_oActiveScheduleEntryPanel = entry;    
    setActiveDayButton(getActiveAppointment());
  }
  
  private void setActiveDayButton(Appointment appointment)
  {
    if (appointment == null) return;
    Iterator it = m_oDayButtonMap.values().iterator();
    while(it.hasNext())
    {
      MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(it.next());
      daybutton.setActiveAppointment(appointment); 
    }    
  }
  
  public Appointment getActiveAppointment()
  {
    if (m_oActiveScheduleEntryPanel != null) return m_oActiveScheduleEntryPanel.getAppointment();
    else return null;
  }
  
  
  private List m_oFontDayButtonComponents = new ArrayList();
  private List m_oFontDayLabelComponents = new ArrayList();
  
  public void setFonts(Font daynamefont, Font daybuttonfont)
  {
    m_oFontDayButton = daybuttonfont;
    m_oFontDayLabel = daynamefont;
    
    for(int i=0; i<(m_oFontDayButtonComponents.size()); i++)
    {
      JComponent comp = (JComponent)(m_oFontDayButtonComponents.get(i));
      comp.setFont(m_oFontDayButton);
    }
    
    for(int i=0; i<(m_oFontDayLabelComponents.size()); i++)
    {
      JComponent comp = (JComponent)(m_oFontDayLabelComponents.get(i));
      comp.setFont(m_oFontDayLabel);
    }
  }
  
  
  public void setShedulePointColor(Color color)
  {
    m_oSchedulePointColor = color;
  }
  
  public void setStartDate(Date date)
  {
    m_oStartDate = date;
    createGrid();
    this.revalidate();
    m_oGridPanel.repaint(); 
  }
  
  public void setFirstDay(ScheduleDate date)
  {
    ScheduleDate firstday = date.getFirstDayOfMonth();
    //System.out.println("MonthlyCalendarPanel::setFirstDay(" + firstday.getDateTimeString() + ")");    
    setStartDate(firstday.getDate());
  }
  
  
  private String[] m_sDayNames = {"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"};
  
  private void createGrid()
  {
    m_oGridPanel.removeAll();
    m_oFontDayLabelComponents.clear();
    m_oFontDayButtonComponents.clear();
    
    int x;    
    m_oGregorianCalendar.setTime(m_oStartDate);
    m_oGregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);
    //m_oGregorianCalendar.setFirstDayOfWeek(Calendar.MONDAY);
    
    int month = m_oGregorianCalendar.get(Calendar.MONTH);
    int currentmonth = month;

    m_oGridPanel.add(new JLabel("", JLabel.CENTER));    
    
    for(x=0; x<7; x++)
    {
      JButton daynamebutton = new JButton(m_sDayNames[x]);
      daynamebutton.setFocusPainted(false);        
      daynamebutton.setAlignmentX(JButton.CENTER_ALIGNMENT);
      daynamebutton.setFont(m_oFontDayLabel);
      m_oFontDayLabelComponents.add(daynamebutton);
      daynamebutton.setForeground(Color.BLACK);
      daynamebutton.setBorderPainted(false);
      daynamebutton.addActionListener(new WeekdayButtonClicked(x));
      m_oGridPanel.add(daynamebutton);    
    }

        
    JButton weekbutton = new JButton(m_oIconWeekSelect);
    weekbutton.setFocusPainted(false);        
    weekbutton.setAlignmentX(JButton.RIGHT_ALIGNMENT);
    weekbutton.setBorderPainted(false);
    weekbutton.addActionListener(new WeekButtonClicked(m_oGregorianCalendar.getTime()));
    weekbutton.setToolTipText(getToolTipTextForWeekButton(m_oGregorianCalendar.getTime()));
    m_oGridPanel.add(weekbutton);    
    
    int gap = (getEuropeanDayNum(m_oGregorianCalendar.get(Calendar.DAY_OF_WEEK)))-1;
    
    for(x=0; x<(getEuropeanDayNum(m_oGregorianCalendar.get(Calendar.DAY_OF_WEEK)))-1; x++)
    {
      m_oGridPanel.add(new JLabel("", JLabel.CENTER));    
    }
    
    while(currentmonth == month)
    {      
      int day = m_oGregorianCalendar.get(Calendar.DAY_OF_MONTH);        
      //JButton daybutton = new JButton(Integer.toString(day));

      //SimpleMonatskalenderDayButton daybutton = new SimpleMonatskalenderDayButton(Integer.toString(day));
      ExtendedMonatskalenderDayButton daybutton = new ExtendedMonatskalenderDayButton(Integer.toString(day));      

      daybutton.setShedulePointColor(AppointmentDefault.getColorOfAppointment(false));
      daybutton.setToDoPointColor(AppointmentDefault.getColorOfAppointment(true));
      
      daybutton.setFocusPainted(false);        
      daybutton.setFont(m_oFontDayButton);
      m_oFontDayButtonComponents.add(daybutton);
      daybutton.setForeground(Color.BLACK);
      daybutton.setBackground(m_oScheduleData.freeBackgroundColor);
      daybutton.setAlignmentX(JButton.CENTER_ALIGNMENT);
      daybutton.addActionListener(new DayButtonClicked(m_oGregorianCalendar.getTime()));
      daybutton.setToolTipText(getToolTipTextForDayButton(m_oGregorianCalendar.getTime()));
      
      m_oDayButtonMap.put(new Integer(day), daybutton);      
      
      m_oGridPanel.add(daybutton);        

      boolean newline = (getEuropeanDayNum(m_oGregorianCalendar.get(Calendar.DAY_OF_WEEK)) == getEuropeanDayNum(Calendar.SUNDAY)); 

      m_oGregorianCalendar.add(Calendar.DAY_OF_MONTH, 1);    
      currentmonth = m_oGregorianCalendar.get(Calendar.MONTH);
      
      if ((newline) && (currentmonth == month))
      {
        weekbutton = new JButton(m_oIconWeekSelect);
        weekbutton.setFocusPainted(false);        
        weekbutton.setAlignmentX(JButton.RIGHT_ALIGNMENT);
        weekbutton.setBorderPainted(false);
        weekbutton.addActionListener(new WeekButtonClicked(m_oGregorianCalendar.getTime()));
        weekbutton.setToolTipText(getToolTipTextForWeekButton(m_oGregorianCalendar.getTime()));
        m_oGridPanel.add(weekbutton);    
      }
            
    }
  }
  
  public void addAppointment(int day, Appointment appointment)
  {
    MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
    if (daybutton != null) daybutton.addAppointment(appointment);    
  }
  
  public void addAppointment(Appointment appointment)
  {
	  
    try
    {
      ScheduleDate date = new ScheduleDate(appointment.getStart());
      ScheduleDate enddate = new ScheduleDate(appointment.getEnd());
      
      int lastday;
            
      /** If time is set to 00:00:00 we will take the day before as last day of this appointment**/
      if (enddate.get(Calendar.SECOND) == 0 && enddate.get(Calendar.MINUTE) == 0 && enddate.get(Calendar.HOUR_OF_DAY) == 0){
    	  //take the day before
    	  enddate.add(Calendar.DAY_OF_MONTH, -1);
    	    
    	  lastday = enddate.get(Calendar.DAY_OF_MONTH);
      }
      else{
    	  lastday = enddate.get(Calendar.DAY_OF_MONTH);
      }
      
      if (date.get(Calendar.MONTH) == enddate.get(Calendar.MONTH)){
	      for (int day = date.get(Calendar.DAY_OF_MONTH); day <= lastday; day++){
	          MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
	          if (daybutton != null) daybutton.addAppointment(appointment);
	      }
      }
      
      /** 
       * If only one date (startdate or enddate) is placed in the currently displayed month
       * we have to choose, which one (or both) lies outside of the displayed area
       */
      else {
    	  
    	  GregorianCalendar cal = new GregorianCalendar();    	  
    	  
    	  int month = m_oStartDate.getMonth();
    	  int monthOfStartDate = date.get(Calendar.MONTH);
    	  int monthOfEndDate = enddate.get(Calendar.MONTH);
    	  
    	  if (month == monthOfStartDate){
    		  // Fill MonthPanel from startdate to end of month
    		  
    		  cal.setTime(date.getDate());
    		  for (int day = date.get(Calendar.DAY_OF_MONTH); cal.get(Calendar.MONTH) == month; cal.add(Calendar.DAY_OF_MONTH, 1)){    
    	          MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
    	          day++;
    	          if (daybutton != null) daybutton.addAppointment(appointment);
    	      }
    	  }
    	  
    	  if (month == monthOfEndDate){
    		  // Fill MonthPanel from start of month to enddate
    		  
    		  for (int day = 1; day <= lastday; day++){    
    	          MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
    	          if (daybutton != null) daybutton.addAppointment(appointment);
    	      }
    	  }
    	  
    	  if (month > monthOfStartDate && month < monthOfEndDate){
    		  // Fill the whole monthpanel
    		  cal.set(Calendar.MONTH, month);
    		  cal.set(Calendar.DAY_OF_MONTH, 1);
    		  for (int day = 1; cal.get(Calendar.MONTH) == month; cal.add(Calendar.DAY_OF_MONTH, 1)){    
    	          MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
    	          day++;
    	          if (daybutton != null) daybutton.addAppointment(appointment);
    	      }
    	  }
      }
      
          
    } catch (ContactDBException e) {}
  }
  
  public void removeAppointment(int day, Appointment appointment)
  {
    MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
    if (daybutton != null) daybutton.removeAppointment(appointment);        
  }
  
  public void removeAllAppointments(int day)
  {
    MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(m_oDayButtonMap.get(new Integer(day)));
    if (daybutton != null) daybutton.clearAppointments();        
  }
  
  public void removeAllAppointments()
  {
  	
    Iterator iterator = m_oDayButtonMap.values().iterator();
    while (iterator.hasNext())
    {  
      MonatskalenderDayButtonInterface daybutton = (MonatskalenderDayButtonInterface)(iterator.next());
      if (daybutton != null) daybutton.clearAppointments();
    }
  }
  
  private int getEuropeanDayNum(int americandaynum)
  {
    switch(americandaynum)
    {
      case(Calendar.SUNDAY): return(Calendar.SATURDAY);
      case(Calendar.MONDAY): return(Calendar.SUNDAY);
      case(Calendar.TUESDAY): return(Calendar.MONDAY);
      case(Calendar.WEDNESDAY): return(Calendar.TUESDAY);
      case(Calendar.THURSDAY): return(Calendar.WEDNESDAY);
      case(Calendar.FRIDAY): return(Calendar.THURSDAY);
      case(Calendar.SATURDAY): return(Calendar.FRIDAY);
    }
    return 0;
  }
  
  
  private String getToolTipTextForWeekButton(Date date)
  {
    ScheduleDate start = new ScheduleDate(date).getFirstSecondOfDay();
    ScheduleDate end = start.getDateWithAddedDays(7);
    return "wechselt zur Wochenansicht vom " + start.getDateString() + " zum " + end.getDateString() + ".";
  }
  
  private String getToolTipTextForDayButton(Date date)
  {
    ScheduleDate start = new ScheduleDate(date).getFirstSecondOfDay();
    return "wechselt zur Tagesansicht vom " + start.getDateString() + ".";
  }
  
  
  private class WeekdayButtonClicked implements ActionListener
  {
    private int m_oIndex;
    
    public WeekdayButtonClicked(int index)
    {
      m_oIndex = index;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      //System.out.println("WeekdayButtonClicked() index=" + m_oIndex);
    }    
  }  
  
  
  private class WeekButtonClicked implements ActionListener
  {
    private Date m_oDate;
    
    public WeekButtonClicked(Date date)
    {
      m_oDate = date;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      ScheduleNavigationField calendarNavigationField = (ScheduleNavigationField)(m_oWidgetPool.getController("CONTROLER_SCHEDULENAVIGATION"));
      if (calendarNavigationField instanceof ScheduleNavigationField)
      {  
        calendarNavigationField.setView(ScheduleNavigationField.VIEW_WEEK);
        calendarNavigationField.setCurrentDate(new ScheduleDate(m_oDate));
      }
    }    
  }  
  
  
  private class DayButtonClicked implements ActionListener
  {
    private Date m_oDate;
    
    public DayButtonClicked(Date date)
    {
      m_oDate = date;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      m_oGUIListener.getLogger().log(Level.FINE, "DayButtonClicked() date=" + m_oDate);
      
      ScheduleNavigationField oCalendarNavigationField = (ScheduleNavigationField)(m_oWidgetPool.getController("CONTROLER_SCHEDULENAVIGATION"));
      if (oCalendarNavigationField instanceof ScheduleNavigationField)
      {  
        oCalendarNavigationField.setView(ScheduleNavigationField.VIEW_DAY);
        oCalendarNavigationField.setCurrentDate(new ScheduleDate(m_oDate));
      }
    }    
  }  
  
  
  private TarentCalendar m_oAppointmentDisplayManager;
  
  public void setAppointmentDisplayManager(TarentCalendar adm)
  {
    m_oAppointmentDisplayManager = adm;
  }
  
  public TarentCalendar getAppointmentDisplayManager()
  {
    return m_oAppointmentDisplayManager;
  }
}
