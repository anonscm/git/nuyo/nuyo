/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.Validators;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.Validator;
import org.evolvis.nuyo.datacontainer.ValidatorInterfaces.ValidatorAdapter;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IntegerValidator extends ValidatorAdapter
{
  public String getListenerName()
  {
    return "IntegerValidator";
  }  
    
  public IntegerValidator()
  {
    super();
  }
    
  public Validator cloneValidator()
  {
    IntegerValidator validator = new IntegerValidator();
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      validator.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    return validator;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.VALIDATOR, "IntegerValidator", new DataContainerVersion(0));
    return d;
  }

  public boolean addParameter(String key, String value)
  {    
    //m_oParameterList.add(new ObjectParameter(key, value));

    return false;
  }

  public boolean canValidate(Class data)
  {
    return data.isInstance(Integer.class);
  }

  public boolean validate(Object data)
  {
    if (data instanceof Integer)
    {
      return(true);
    }
    return false;
  }

}
