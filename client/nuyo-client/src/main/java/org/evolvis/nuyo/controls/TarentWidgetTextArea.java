/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * @author niko
 */
public class TarentWidgetTextArea extends JPanel implements TarentWidgetInterface
{
	private boolean           m_iIsLengthRestricted;
	private JTextArea         m_oTextArea;
	private JScrollPane       m_oScrollPane;

	public TarentWidgetTextArea()
	{    
		super();
		m_oTextArea = new JTextArea();
		setTabNavigationBehavior(m_oTextArea);
		m_oScrollPane = new JScrollPane(m_oTextArea);
		this.setLayout(new BorderLayout());    
		this.add(m_oScrollPane, BorderLayout.CENTER);
		m_iIsLengthRestricted = false;    
	}

	public TarentWidgetTextArea(String text)
	{
		super();
		m_oTextArea = new JTextArea(text);
		setTabNavigationBehavior(m_oTextArea);
		m_oScrollPane = new JScrollPane(m_oTextArea);
		this.setLayout(new BorderLayout());    
		this.add(m_oScrollPane, BorderLayout.CENTER);
		m_iIsLengthRestricted = false;    
	}

	public TarentWidgetTextArea(String text, int maxlen)
	{
		super();
		m_oTextArea = new JTextArea();
		setTabNavigationBehavior(m_oTextArea);
		m_oScrollPane = new JScrollPane(m_oTextArea);
		this.setLayout(new BorderLayout());    
		this.add(m_oScrollPane, BorderLayout.CENTER);
		setLengthRestriction(maxlen);
		m_oTextArea.setText(text);
	}

	public TarentWidgetTextArea(String text, int rows, int columns)
	{
		super();
		m_oTextArea = new JTextArea(text, rows, columns);
		setTabNavigationBehavior(m_oTextArea);
		m_oScrollPane = new JScrollPane(m_oTextArea);
		this.setLayout(new BorderLayout());    
		this.add(m_oScrollPane, BorderLayout.CENTER);
		m_iIsLengthRestricted = false;    
	}

	public TarentWidgetTextArea(String text, int rows, int columns, int maxlen)
	{
		super();
		m_oTextArea = new JTextArea(rows, columns);
		setTabNavigationBehavior(m_oTextArea);
		m_oScrollPane = new JScrollPane(m_oTextArea);
		this.setLayout(new BorderLayout());    
		this.add(m_oScrollPane, BorderLayout.CENTER);
		setLengthRestriction(maxlen);
		m_oTextArea.setText(text);
	}

	public TarentWidgetTextArea(int rows, int columns)
	{
		super();
		m_oTextArea = new JTextArea(rows, columns);
		setTabNavigationBehavior(m_oTextArea);
		m_oScrollPane = new JScrollPane(m_oTextArea);
		this.setLayout(new BorderLayout());    
		this.add(m_oScrollPane, BorderLayout.CENTER);
		m_iIsLengthRestricted = false;    
	}
	
	/**
	 * Changes the default JTextArea-behavior to "Navigation by Tab"
	 * 
	 * @param textArea
	 */
	private void setTabNavigationBehavior(JTextArea textArea)
	{
		textArea.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent ke){
				if(ke.getKeyCode() == KeyEvent.VK_TAB)
				{
					ke.consume();
					if(ke.getModifiers() == KeyEvent.SHIFT_MASK)
						KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
					else
						KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();

				}}});
	}

	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	 public void setData(Object data) 
	{
		 if (data instanceof String)
		 {
			 m_oTextArea.setText((String)data);
		 }
		 else
		 {
			 m_oTextArea.setText(data.toString());
		 }
	}

	 /**
	  * @see TarentWidgetInterface#getData()
	  */
	 public Object getData() 
	 {
		 return m_oTextArea.getText();
	 }


	 public void setText(String text)
	 {
		 if (text != null) this.setData(text); 
		 else              this.setData(""); 
	 }

	 public String getText() 
	 {
		 return m_oTextArea.getText();
	 }


	 public void setEditable(boolean iseditable)
	 {
		 m_oTextArea.setEditable(iseditable);
	 }

	 public void setEnabled(boolean isenabled)
	 {
		 m_oTextArea.setEnabled(isenabled);
	 }



	 public void setLengthRestriction(int maxlen)
	 {
		 if (maxlen > 0)
		 {
			 m_iIsLengthRestricted = true;
			 String text = m_oTextArea.getText();
			 m_oTextArea.setDocument(new SizeRestrictedDocument(maxlen));
			 m_oTextArea.setText(text);
		 }
		 else
		 {
			 m_iIsLengthRestricted = false;
			 String text = m_oTextArea.getText();
			 m_oTextArea.setDocument(new PlainDocument());
			 m_oTextArea.setText(text);
		 }
	 }

	 public boolean isLengthRestricted()
	 {
		 return(m_iIsLengthRestricted); 
	 }


	 public boolean isEqual(Object data)
	 {
		 return(this.getText().equals(data.toString()));
	 }

	 public JComponent getComponent()
	 {
		 return(this);
	 }

	 public void setWidgetEditable(boolean iseditable)
	 {
		 m_oTextArea.setEditable(iseditable);
		 if (!iseditable) m_oTextArea.setBackground(Color.WHITE);
	 }


	 public JTextArea getTextArea()
	 {
		 return(m_oTextArea);
	 }

	 public JComponent getScrollPane()
	 {
		 return(m_oScrollPane);
	 }

	 public void setLineWrap(boolean use)
	 {
		 m_oTextArea.setLineWrap(use);        
	 }

	 public void setWrapStyleWord(boolean use)
	 {
		 m_oTextArea.setWrapStyleWord(use);        
	 }

	 public void setToolTipText(String text)
	 {
		 m_oTextArea.setToolTipText(text);
		 m_oScrollPane.setToolTipText(text);
	 }

	 // JScrollPane.VERTICAL_SCROLLBAR_ALWAYS
	 public void setVerticalScrollBarPolicy(int policy)
	 {
		 m_oScrollPane.setVerticalScrollBarPolicy(policy);
	 }

	 public void setHorizontalScrollBarPolicy(int policy)
	 {
		 m_oScrollPane.setHorizontalScrollBarPolicy(policy);
	 }

	 public void setSize(Dimension size)
	 {
		 m_oScrollPane.setPreferredSize(size);
		 m_oScrollPane.setMaximumSize(size);
		 m_oScrollPane.setMinimumSize(size);
	 }  

	 public class SizeRestrictedDocument extends PlainDocument
	 {
		 private int m_iMaxChars;

		 public SizeRestrictedDocument(int maxchars)
		 {
			 m_iMaxChars = maxchars;
		 }

		 public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
		 {
			 if (str == null) return;        
			 int insertsize = str.length();
			 int docsize = this.getLength();
			 if ((insertsize+docsize) < m_iMaxChars) 
			 {
				 super.insertString(offs, str, a);
			 }
			 else
			 {
				 int maxinsert = m_iMaxChars - docsize;
				 if (maxinsert > 0)
				 {            
					 super.insertString(offs, str.substring(0, maxinsert), a);
				 }
			 }        
		 }
	 }

	 private List m_oWidgetChangeListeners = new ArrayList();
	 public void addWidgetChangeListener(TarentWidgetChangeListener listener)
	 {
		 m_oWidgetChangeListeners.add(listener);
	 }

	 public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
	 {
		 m_oWidgetChangeListeners.remove(listener);    
	 }


}
