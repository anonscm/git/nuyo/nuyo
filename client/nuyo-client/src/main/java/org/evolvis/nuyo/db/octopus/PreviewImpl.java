package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;

/**
 * @author kleinw
 *
 */
public class PreviewImpl extends PreviewBean {

    static {
        _fetcher = new AbstractEntityFetcher("getPreviews", true) {
            public IEntity populate(ResponseData response) throws ContactDBException {
                return null;
            }
        };
    }

    
    
}
