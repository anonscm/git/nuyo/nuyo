package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;


public class DeleteAction extends AbstractContactDependentAction {

    private static final long serialVersionUID = -5908674861542521506L;
    private static final TarentLogger log = new TarentLogger(DeleteAction.class);
    private GUIListener guiListener;
    private Runnable deleteExecutor;

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null){
            SwingUtilities.invokeLater(deleteExecutor);
        } else log.warning("Delete action is not initialized");
    }

    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        deleteExecutor = new Runnable(){
            public void run() {
                guiListener.userRequestDelete(false);
            }
        };
    }
}