/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

/**
 * @author niko
 *
 */
public class AddressKeys
{
  public final static Object HERRFRAU = "HERRFRAU"; // DONE
  public final static Object ANREDE = "ANREDE"; // DONE
  public final static Object TITEL = "TITEL"; // DONE
  public final static Object AKADTITEL = "AKADTITEL"; // DONE
  public final static Object VORNAME = "VORNAME"; // DONE
  public final static Object NACHNAME = "NACHNAME"; // DONE
  public final static Object NAMENSZUSATZ = "NAMENSZUSATZ"; // DONE
  public final static Object INSTITUTION = "INSTITUTION"; // DONE
  public final static Object INSTITUTION2 = "INSTITUTION2"; // DONE
  public final static Object INSTITUTION3 = "INSTITUTION3";
  
  public final static Object POSITION = "POSITION"; // DONE
  public final static Object ABTEILUNG = "ABTEILUNG"; // DONE
  public final static Object STRASSE = "STRASSE"; // DONE
  public final static Object HAUSNUMMER = "HAUSNUMMER"; // DONE
  public final static Object PLZ = "PLZ"; // DONE
  public final static Object ORT = "ORT"; // DONE
  public final static Object POSTFACHPLZ = "POSTFACHPLZ"; // DONE
  public final static Object POSTFACH = "POSTFACH"; // DONE
  public final static Object LAND = "LAND"; // DONE
  public final static Object BUNDESLAND = "BUNDESLAND"; // DONE
  public final static Object VERTEILER = "VERTEILER"; // DONE
  public final static Object ADRESSNR = "ADRESSNR"; // DONE
  public final static Object ERFASSUNGSDATUM = "ERFASSUNGSDATUM"; // DONE
  public final static Object AENDERUNGSDATUM = "AENDERUNGSDATUM"; // DONE
  public final static Object AENDERUNGSDATUMALTERBESTAND = "AENDERUNGSDATUMALTERBESTAND"; // DONE
  public final static Object TELEFONDIENST = "TELEFONDIENST"; // DONE
  public final static Object TELEFONPRIVAT = "TELEFONPRIVAT"; // DONE
  public final static Object TELEFON3 = "TELEFON3";      
  
  public final static Object HANDYDIENST = "HANDYDIENST"; // DONE
  public final static Object HANDYPRIVAT = "HANDYPRIVAT"; // DONE
  public final static Object HANDY3 = "HANDY3";
  
  public final static Object FAXDIENST = "FAXDIENST"; // DONE
  public final static Object FAXPRIVAT = "FAXPRIVAT"; // DONE
  public final static Object FAX3 = "FAX3";
  
  public final static Object EMAIL = "EMAIL"; // DONE
  public final static Object EMAIL2 = "EMAIL2"; // DONE
  public final static Object EMAIL3 = "EMAIL3";
  
  public final static Object HOMEPAGE = "HOMEPAGE"; // DONE  
  public final static Object HOMEPAGE2 = "HOMEPAGE2";
  public final static Object HOMEPAGE3 = "HOMEPAGE3";
  
  public final static Object WEITEREVORNAMEN = "WEITEREVORNAMEN"; // DONE
  public final static Object SPITZNAME = "SPITZNAME"; // DONE
  public final static Object NOTIZ = "NOTIZ"; // DONE
  public final static Object BANKNAME = "BANKNAME"; // DONE
  public final static Object BANKCODE = "BANKCODE"; // DONE
  public final static Object BANKKONTO = "BANKKONTO"; // DONE
  public final static Object GESCHLECHT = "GESCHLECHT"; // DONE
  public final static Object GEBURTSJAHR = "GEBURTSJAHR"; // DONE
  public final static Object GEBURTSDATUM = "GEBURTSDATUM"; // DONE
  public final static Object HISTORY = "HISTORY"; // DONE
  public final static Object PICTURE = "PICTURE"; // DONE
  public final static Object PICTUREEXPRESSION = "PICTUREEXPRESSION"; // DONE
  
  public final static Object LETTERADDRESS = "LETTERADDRESS";
  public final static Object LETTER_AUTO = "LETTER_AUTO";
  public final static Object LETTERSALUTATION = "LETTERSALUTATION";
  public final static Object LETTERSALUTATION_AUTO = "LETTERSALUTATION_AUTO"; 
  
  public final static Object FOLLOWUP = "FOLLOWUP";
  public final static Object FOLLOWUPDATE = "FOLLOWUPDATE";
  public final static Object FK_USER_FOLLOWUP = "FK_USER_FOLLOWUP";
  
  public final static Object STICHWORTE = "STICHWORTE"; // DONE    
  //public final static Object ADRESSZUSATZ = "ADRESSZUSATZ";
  public final static Object ADRESSZUSATZ2 = "ADRESSZUSATZ2";
  public final static Object ADRESSZUSATZ3 = "ADRESSZUSATZ3";
  public final static Object NAMEN3 = "NAMEN3";
    
  public final static Object BUNDESLAND2 = "BUNDESLAND2";
  public final static Object HAUSNUMMER2 = "HAUSNUMMER2";
  public final static Object LAND2 = "LAND2";
  public final static Object LKZ2 = "LKZ2";
  public final static Object ORT2 = "ORT2";
  public final static Object PLZ2 = "PLZ2";
  public final static Object POSTFACH2 = "POSTFACHNR2";
  public final static Object POSTFACHPLZ2 = "POSTFACHPLZ2";
  public final static Object STRASSE2 = "STRASSE2";
  
  public final static Object BUNDESLAND3 = "BUNDESLAND3";
  public final static Object HAUSNUMMER3 = "HAUSNUMMER3";
  public final static Object LAND3 = "LAND3";
  public final static Object LKZ3 = "LKZ3";
  public final static Object ORT3 = "ORT3";
  public final static Object PLZ3 = "PLZ3";
  public final static Object POSTFACH3 = "POSTFACHNR3";
  public final static Object POSTFACHPLZ3 = "POSTFACHPLZ3";
  public final static Object STRASSE3 = "STRASSE3"; 
  
  
  public final static Object COMMONTEXT01 ="COMMONTEXT01";
  public final static Object COMMONTEXT02 ="COMMONTEXT02";
  public final static Object COMMONTEXT03 ="COMMONTEXT03";
  public final static Object COMMONTEXT04 ="COMMONTEXT04";
  public final static Object COMMONTEXT05 ="COMMONTEXT05";
  public final static Object COMMONTEXT06 ="COMMONTEXT06";
  public final static Object COMMONTEXT07 ="COMMONTEXT07";
  public final static Object COMMONTEXT08 ="COMMONTEXT08";
  public final static Object COMMONTEXT09 ="COMMONTEXT09";
  public final static Object COMMONTEXT10 ="COMMONTEXT10";
  public final static Object COMMONTEXT11 ="COMMONTEXT11";
  public final static Object COMMONTEXT12 ="COMMONTEXT12";
  public final static Object COMMONTEXT13 ="COMMONTEXT13";
  public final static Object COMMONTEXT14 ="COMMONTEXT14";
  public final static Object COMMONTEXT15 ="COMMONTEXT15";
  public final static Object COMMONTEXT16 ="COMMONTEXT16";
  public final static Object COMMONTEXT17 ="COMMONTEXT17";
  public final static Object COMMONTEXT18 ="COMMONTEXT18";
  public final static Object COMMONTEXT19 ="COMMONTEXT19";
  public final static Object COMMONTEXT20 ="COMMONTEXT20";
  
  public final static Object COMMONINT01 ="COMMONINT01";
  public final static Object COMMONINT02 ="COMMONINT02";
  public final static Object COMMONINT03 ="COMMONINT03";
  public final static Object COMMONINT04 ="COMMONINT04";
  public final static Object COMMONINT05 ="COMMONINT05";
  public final static Object COMMONINT06 ="COMMONINT06";
  public final static Object COMMONINT07 ="COMMONINT07";
  public final static Object COMMONINT08 ="COMMONINT08";
  public final static Object COMMONINT09 ="COMMONINT09";
  public final static Object COMMONINT10 ="COMMONINT10";

  public final static Object COMMONBOOL01 ="COMMONBOOL01";
  public final static Object COMMONBOOL02 ="COMMONBOOL02";
  public final static Object COMMONBOOL03 ="COMMONBOOL03";
  public final static Object COMMONBOOL04 ="COMMONBOOL04";
  public final static Object COMMONBOOL05 ="COMMONBOOL05";
  public final static Object COMMONBOOL06 ="COMMONBOOL06";
  public final static Object COMMONBOOL07 ="COMMONBOOL07";
  public final static Object COMMONBOOL08 ="COMMONBOOL08";
  public final static Object COMMONBOOL09 ="COMMONBOOL09";
  public final static Object COMMONBOOL10 ="COMMONBOOL10";
  
  public final static Object COMMONDATE01 ="COMMONDATE01";
  public final static Object COMMONDATE02 ="COMMONDATE02";
  public final static Object COMMONDATE03 ="COMMONDATE03";
  public final static Object COMMONDATE04 ="COMMONDATE04";
  public final static Object COMMONDATE05 ="COMMONDATE05";
  public final static Object COMMONDATE06 ="COMMONDATE06";
  public final static Object COMMONDATE07 ="COMMONDATE07";
  public final static Object COMMONDATE08 ="COMMONDATE08";
  public final static Object COMMONDATE09 ="COMMONDATE09";
  public final static Object COMMONDATE10 ="COMMONDATE10";
  
  public final static Object COMMONMONEY01 ="COMMONMONEY01";
  public final static Object COMMONMONEY02 ="COMMONMONEY02";
  public final static Object COMMONMONEY03 ="COMMONMONEY03";
  public final static Object COMMONMONEY04 ="COMMONMONEY04";
  public final static Object COMMONMONEY05 ="COMMONMONEY05";
  public final static Object COMMONMONEY06 ="COMMONMONEY06";
  public final static Object COMMONMONEY07 ="COMMONMONEY07";
  public final static Object COMMONMONEY08 ="COMMONMONEY08";
  public final static Object COMMONMONEY09 ="COMMONMONEY09";
  public final static Object COMMONMONEY10 ="COMMONMONEY10";
  
  public final static Object ASSOCIATEDCATEGORIES = "ASSOCIATEDCATEGORIES";
  public final static Object BLOCKEDFORCONSIGNMENT = "BLOCKEDFORCONSIGNMENT";
  
  
}
