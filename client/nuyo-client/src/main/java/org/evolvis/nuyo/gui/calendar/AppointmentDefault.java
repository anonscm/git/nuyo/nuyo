/*
 * Created on 14.06.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.Color;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentCalendarRelation;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.PersistenceManager;


/**
 * @author niko
 *
 */
public class AppointmentDefault
{
  public static void setToDefaults(Appointment appointment, boolean istemp) throws ContactDBException
  {
    appointment.setAppointmentCategory(Appointment.CATEGORY_MEETING);	        
    appointment.setJobCompleted(false);	        
    appointment.setJobCompletion(0);    
    appointment.setPriority(Appointment.PRIORITY_MEDIUM);	        
    appointment.setPrivate(false);
    
    if (appointment.isJob())
    {  
    	appointment.setAppointmentCategory(Appointment.CATEGORY_TODO);
    	appointment.setAttribute(Appointment.KEY_SUBJECT, "Aufgabe");
    }
    else
    {
      appointment.setAttribute(Appointment.KEY_SUBJECT, getDefaultNameOfAppointmentCategory(appointment.getAppointmentCategory()));      
    }
    appointment.setAttribute(Appointment.KEY_DESCRIPTION, "");	            
    appointment.setAttribute(Appointment.KEY_LOCATION, "");	        
    
    appointment.setTemporary(istemp);
  }

  public static void initialize(Appointment appointment, Calendar calendar) throws ContactDBException
  {
    if (calendar.getUserGroup() != null)
    {
      // Gruppenkalender
      appointment.getRelation(calendar).setDisplayMode(AppointmentCalendarRelation.DISPLAY_FREE);
      PersistenceManager.commit(appointment.getRelation(calendar));
    }
    else
    {  
      // Benutzer / Ressourcenkalender
      appointment.getRelation(calendar).setDisplayMode(AppointmentCalendarRelation.DISPLAY_BOOKED);
      PersistenceManager.commit(appointment.getRelation(calendar));
    }
  }

  
  private static String getDefaultNameOfAppointmentCategory(int category)
  {
    switch(category)
    {
      case(Appointment.CATEGORY_BIRTHDAY): return "Geburtstag";
      case(Appointment.CATEGORY_CALL): return "Anruf";
      case(Appointment.CATEGORY_EXTERN): return "Ausw�rtstermin";
      case(Appointment.CATEGORY_MEETING): return "Besprechung";
      case(Appointment.CATEGORY_SICK): return "Krankheit";
      case(Appointment.CATEGORY_VACATION): return "Urlaub";
    }
    return "unbenannt";
  }
  
  public static Color getColorOfAppointment(boolean isjob)
  {
    if (isjob)
    {  
      return(new Color(0xaa, 0xaa, 0xdd));    
    }
    else
    {
      return(new Color(0xaa, 0xdd, 0xaa));      
    }        
  }
  
  public static Color getColorOfAppointment(Appointment appointment) throws ContactDBException
  {
    return getColorOfAppointment(appointment.isJob());
  }
  
}
