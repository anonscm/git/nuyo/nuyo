/*
 * Created on 07.05.2004
 * 
 */
package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox.IconComboBoxEntry;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.filter.CategoriesFilter;
import org.evolvis.nuyo.db.filter.SubCategoriesFilter;
import org.evolvis.nuyo.gui.dnd.TarentDnDAdapter;
import org.evolvis.nuyo.gui.dnd.TarentDnDData;
import org.evolvis.nuyo.gui.dnd.TarentDnDTransferHandler;
import org.evolvis.xana.utils.IconFactory;

import de.tarent.commons.ui.EscapeDialog;

/**
 * TODO: Addresszugriff: replace this class by the new address selector
 * @author niko
 * 
 */
public class ExtendedAddressSelector extends EscapeDialog {

	public static final Object			STATUS_NONE		= "STATUS_NONE";
	public static final Object			STATUS_OK		= "STATUS_OK";
	public static final Object			STATUS_CANCEL	= "STATUS_CANCEL";

	private final boolean				m_bAskForIcon	= true;

	private GUIListener					m_oGUIListener;
	private JTable						m_oTable_Addresses;
	private AddressTableModel			m_oAddressesTableModel;
	private Object						m_oStatus		= STATUS_NONE;

	private JTable						m_oTable_Selected;
	private AddressTableModel			m_oSelectedTableModel;

	private TarentWidgetIconComboBox	m_oIconComboBox_Category;
	private TarentWidgetIconComboBox	m_oIconComboBox_SubCategory;
	private TarentWidgetIconComboBox	m_oIconComboBox_Searchfield;
	private JTextField					m_oTextField_Searchtext;

	private JButton						m_oButton_Search;

	private JLabel						m_oLabelLeft;
	private JLabel						m_oLabelRight;

	private Font						m_oFont;

	private JPanel						m_oMainPanel;

	private ImageIcon					m_oIcon_SystemUser;
	private ImageIcon					m_oIcon_ExternalUser;

	public ExtendedAddressSelector(GUIListener guiListener, String caption) {
		super(guiListener.getFrame());
		setTitle(caption);
		m_oGUIListener = guiListener;

		m_oIcon_SystemUser = GUIHelper.getIcon(GUIHelper.ICON_SYSUSER);//loadIcon("sysuser.gif");
		m_oIcon_ExternalUser = GUIHelper.getIcon(GUIHelper.ICON_EXTUSER);//loadIcon("extuser.gif");

		m_oFont = GUIHelper.getFont(GUIHelper.FONT_FORMULAR);

		// ------------------------------------------------------------------------------------------------------

		ListDnDListenerSource sourcelistener = new ListDnDListenerSource();
		TarentDnDTransferHandler m_oDnDDataTransferHandlerSourceAdress = new TarentDnDTransferHandler(sourcelistener);
		m_oDnDDataTransferHandlerSourceAdress.addDataFlavor(DestAddressDnDData.class, false, true);
		m_oDnDDataTransferHandlerSourceAdress.addDataFlavor(SourceAddressDnDData.class, true, false);

		ListDnDListener listener = new ListDnDListener();
		TarentDnDTransferHandler m_oDnDDataTransferHandlerAdress = new TarentDnDTransferHandler(listener);
		m_oDnDDataTransferHandlerAdress.addDataFlavor(DestAddressDnDData.class, true, false);
		m_oDnDDataTransferHandlerAdress.addDataFlavor(SourceAddressDnDData.class, false, true);

		// ------------------------------------------------------------------------------------------------------

		JPanel addressespanel = new JPanel(new BorderLayout());

		// tabelle erzeugen...
		m_oAddressesTableModel = new AddressTableModel(2);
		m_oAddressesTableModel.setColumnName(0, "Typ");
		m_oAddressesTableModel.setColumnEditable(0, false);
		m_oAddressesTableModel.setColumnName(1, "Adresse");
		m_oAddressesTableModel.setColumnEditable(1, false);
		m_oTable_Addresses = new JTable(m_oAddressesTableModel);
		m_oTable_Addresses.setToolTipText("Eintr�ge die in die Selektion �bernommen werden k�nnen");
		m_oTable_Addresses.setRowHeight(20);
		m_oTable_Addresses.setCellSelectionEnabled(false);
		m_oTable_Addresses.setRowSelectionAllowed(true);
		m_oTable_Addresses.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		m_oTable_Addresses.addMouseListener(new DoubleClickMouseListenerToRight());
		TableColumn column0 = m_oTable_Addresses.getColumnModel().getColumn(0);
		column0.setPreferredWidth(40);
		column0.setMaxWidth(40);
		column0.setMinWidth(40);
		TableColumn column1 = m_oTable_Addresses.getColumnModel().getColumn(1);
		column1.setPreferredWidth(300);

		// ------------------------------------------------------------------------------------------------------
        IconFactory factory = ApplicationServices.getInstance().getIconFactory();
		ImageIcon icon_right = (ImageIcon)factory.getIcon("move_right.gif");
		ImageIcon icon_left = (ImageIcon)factory.getIcon("move_left.gif");

		JButton button_right = new JButton("�bernehmen", icon_right);
		button_right.setToolTipText("die in der linken Liste markierten Eintr�ge in die (rechte) Auswahlliste �bernehmen");
		button_right.setHorizontalTextPosition(SwingConstants.LEFT);
		button_right.setFont(m_oFont);
		button_right.addActionListener(new button_right_clicked());

		JButton button_left = new JButton("entfernen", icon_left);
		button_left.setToolTipText("die in der rechten Liste markierten Eintr�ge entfernen");
		button_left.setHorizontalTextPosition(SwingConstants.RIGHT);
		button_left.setFont(m_oFont);
		button_left.addActionListener(new button_left_clicked());

		// ------------------------------------------------------------------------------------------------------

		m_oLabelLeft = new JLabel("", JLabel.RIGHT);
		m_oLabelLeft.setFont(m_oFont);
		m_oLabelLeft
				.setToolTipText("Anzahl der �ber die Suche gefundenen Eintr�ge die in die Selektion �bernommen werden k�nnen");

		m_oLabelRight = new JLabel("", JLabel.LEFT);
		m_oLabelRight.setFont(m_oFont);
		m_oLabelRight.setToolTipText("Anzahl der in die Selektion �bernommenen Eintr�ge");

		// ------------------------------------------------------------------------------------------------------

		JButton button_ok = new JButton("Ok");
		button_ok.setToolTipText("die Auswahl �bernehmen und diesen Dialog schliessen");
		button_ok.setFont(m_oFont);
		button_ok.addActionListener(new button_ok_clicked());

		JButton button_cancel = new JButton("Abbruch");
		button_cancel.setToolTipText("die Auswahl verwerfen und diesen Dialog schliessen");
		button_cancel.setFont(m_oFont);
		button_cancel.addActionListener(new button_cancel_clicked());

		JPanel buttonpanel = new JPanel(new BorderLayout());
		buttonpanel.setBorder(new EmptyBorder(0, 5, 0, 0));
		buttonpanel.add(button_ok, BorderLayout.NORTH);
		buttonpanel.add(button_cancel, BorderLayout.SOUTH);

		JScrollPane addressesscroll = new JScrollPane(m_oTable_Addresses);
		addressesscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		addressesscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// DnD
		m_oTable_Addresses.setTransferHandler(m_oDnDDataTransferHandlerSourceAdress); // erlaubt
																					  // das
																					  // "fallenlassen"
																					  // auf
																					  // dieser
																					  // Komponente
		addressesscroll.setTransferHandler(m_oDnDDataTransferHandlerSourceAdress); // erlaubt
																				   // das
																				   // "fallenlassen"
																				   // auf
																				   // dieser
																				   // Komponente
		m_oTable_Addresses.setDragEnabled(true); // erlaubt das "ziehen" aus
												 // dieser Komponente

		addressespanel.add(addressesscroll, BorderLayout.CENTER);

		JPanel button_right_panel = new JPanel(new BorderLayout());
		button_right_panel.add(button_right, BorderLayout.EAST);
		button_right_panel.add(m_oLabelRight, BorderLayout.CENTER);

		addressespanel.add(button_right_panel, BorderLayout.NORTH);

		// ------------------------------------------------------------------------------------------------------

		m_oIconComboBox_Category = new TarentWidgetIconComboBox();
		m_oIconComboBox_Category.setToolTipText("Kategorie in der gesucht werden soll");
		m_oIconComboBox_Category.addActionListener(new category_selected());

		m_oIconComboBox_SubCategory = new TarentWidgetIconComboBox();
		m_oIconComboBox_SubCategory.setToolTipText("Unterkategorie in der gesucht werden soll");

		m_oIconComboBox_Searchfield = new TarentWidgetIconComboBox();
		m_oIconComboBox_Searchfield.setToolTipText("Adressfeld in dem gesucht werden soll");

		m_oTextField_Searchtext = new JTextField();
		m_oTextField_Searchtext.setToolTipText("der Begriff nach dem gesucht werden soll");
		m_oTextField_Searchtext.addActionListener(new button_search_clicked());

		JPanel panel_search_category = new JPanel(new BorderLayout());
		JLabel label_search_category = new JLabel("Kategorie:", JLabel.RIGHT);
		label_search_category.setBorder(new EmptyBorder(0, 5, 0, 5));
		label_search_category.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
		panel_search_category.add(label_search_category, BorderLayout.WEST);
		panel_search_category.add(m_oIconComboBox_Category, BorderLayout.CENTER);

		JPanel panel_search_subcategory = new JPanel(new BorderLayout());
		JLabel label_search_subcategory = new JLabel("Unterkategorie:", JLabel.RIGHT);
		label_search_subcategory.setBorder(new EmptyBorder(0, 5, 0, 5));
		label_search_subcategory.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
		panel_search_subcategory.add(label_search_subcategory, BorderLayout.WEST);
		panel_search_subcategory.add(m_oIconComboBox_SubCategory, BorderLayout.CENTER);

		JPanel panel_search_field = new JPanel(new BorderLayout());
		JLabel label_search_field = new JLabel("Suchfeld:", JLabel.RIGHT);
		label_search_field.setBorder(new EmptyBorder(0, 5, 0, 5));
		label_search_field.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
		panel_search_field.add(label_search_field, BorderLayout.WEST);
		panel_search_field.add(m_oIconComboBox_Searchfield, BorderLayout.CENTER);

		JPanel panel_search_text = new JPanel(new BorderLayout());
		JLabel label_search_text = new JLabel("Suchbegriff:", JLabel.RIGHT);
		label_search_text.setBorder(new EmptyBorder(0, 5, 0, 5));
		label_search_text.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
		panel_search_text.add(label_search_text, BorderLayout.WEST);
		panel_search_text.add(m_oTextField_Searchtext, BorderLayout.CENTER);

		JPanel panel_search_fieldtext = new JPanel(new GridLayout(2, 1));
		panel_search_fieldtext.add(panel_search_field);
		panel_search_fieldtext.add(panel_search_text);

		JPanel panel_search_categories = new JPanel(new GridLayout(2, 1));
		panel_search_categories.add(panel_search_category);
		panel_search_categories.add(panel_search_subcategory);

		JPanel searchentrypanel = new JPanel(new GridLayout(1, 2));
		searchentrypanel.add(panel_search_categories);
		searchentrypanel.add(panel_search_fieldtext);

		JPanel searchpanel = new JPanel(new BorderLayout());

		m_oButton_Search = new JButton("suchen");
		m_oButton_Search.setToolTipText("Suche ausf�hren...");
		m_oButton_Search.addActionListener(new button_search_clicked());

		searchpanel.add(searchentrypanel, BorderLayout.CENTER);
		searchpanel.add(m_oButton_Search, BorderLayout.EAST);

		JPanel toppanel = new JPanel(new BorderLayout());
		toppanel.add(searchpanel, BorderLayout.CENTER);
		toppanel.add(buttonpanel, BorderLayout.EAST);

		// ------------------------------------------------------------------------------------------------------

		// tabelle erzeugen...
		m_oSelectedTableModel = new AddressTableModel(2);
		m_oSelectedTableModel.setColumnName(0, "Typ");
		m_oSelectedTableModel.setColumnEditable(0, false);
		m_oSelectedTableModel.setColumnName(1, "Adresse");
		m_oSelectedTableModel.setColumnEditable(1, false);
		m_oTable_Selected = new JTable(m_oSelectedTableModel);
		m_oTable_Selected.setToolTipText("In die Selektion �bernommene Eintr�ge");
		m_oTable_Selected.setRowHeight(20);
		m_oTable_Selected.setCellSelectionEnabled(false);
		m_oTable_Selected.setRowSelectionAllowed(true);
		m_oTable_Selected.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		m_oTable_Selected.addMouseListener(new DoubleClickMouseListenerToLeft());
		TableColumn columnb0 = m_oTable_Selected.getColumnModel().getColumn(0);
		columnb0.setPreferredWidth(40);
		columnb0.setMaxWidth(40);
		columnb0.setMinWidth(40);
		TableColumn columnb1 = m_oTable_Selected.getColumnModel().getColumn(1);
		columnb1.setPreferredWidth(300);

		JPanel selectedpanel = new JPanel(new BorderLayout());

		JScrollPane selectedscroll = new JScrollPane(m_oTable_Selected);
		selectedscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		selectedscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// DnD
		m_oTable_Selected.setTransferHandler(m_oDnDDataTransferHandlerAdress); // erlaubt
																			   // das
																			   // "fallenlassen"
																			   // auf
																			   // dieser
																			   // Komponente
		selectedscroll.setTransferHandler(m_oDnDDataTransferHandlerAdress); // erlaubt
																			// das
																			// "fallenlassen"
																			// auf
																			// dieser
																			// Komponente
		m_oTable_Selected.setDragEnabled(true); // erlaubt das "ziehen" aus
												// dieser Komponente

		selectedpanel.add(selectedscroll, BorderLayout.CENTER);

		JPanel button_left_panel = new JPanel(new BorderLayout());
		button_left_panel.add(button_left, BorderLayout.WEST);
		button_left_panel.add(m_oLabelLeft, BorderLayout.CENTER);

		selectedpanel.add(button_left_panel, BorderLayout.NORTH);

		// ------------------------------------------------------------------------------------------------------

		addressespanel.setBorder(new EmptyBorder(0, 0, 0, 5));
		selectedpanel.setBorder(new EmptyBorder(0, 5, 0, 0));

		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		split.setDividerSize(5);
		split.setLeftComponent(addressespanel);
		split.setRightComponent(selectedpanel);
		split.setBorder(new EmptyBorder(5, 0, 0, 0));

		m_oMainPanel = new JPanel(new BorderLayout());
		m_oMainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		m_oMainPanel.add(split, BorderLayout.CENTER);
		m_oMainPanel.add(toppanel, BorderLayout.NORTH);
		getContentPane().add(m_oMainPanel);
		fillCategoryList();
		fillFieldList();
		pack();

		// center frame on screen...
		JFrame frame = guiListener.getFrame();
		if (frame.isShowing() && frame.isValid()) {
			setLocationRelativeTo(frame);
		} else {
			setLocationRelativeTo(null);
		}

		// set Label-Size...
		Dimension size;
		if (label_search_category.getWidth() > label_search_field.getWidth()) {
			size = new Dimension(label_search_category.getWidth(), label_search_category.getHeight());
		} else {
			size = new Dimension(label_search_field.getWidth(), label_search_field.getHeight());
		}

		label_search_category.setMinimumSize(size);
		label_search_category.setMaximumSize(size);
		label_search_category.setPreferredSize(size);
		label_search_category.setSize(size);
		label_search_field.setMinimumSize(size);
		label_search_field.setMaximumSize(size);
		label_search_field.setPreferredSize(size);
		label_search_field.setSize(size);

		setListSizeLabels();
        getRootPane().getInputMap().put(KeyStroke.getKeyStroke (KeyEvent.VK_ESCAPE, 0), "schliessen");
        getRootPane().getActionMap().put("schliessen", new AbstractAction () {
            public void actionPerformed (ActionEvent event) {
    			m_oStatus = STATUS_CANCEL;
    			setVisible(false);
    			setModal(false);
            }
        });
		// open window....
		setModal(true);
		setVisible(true);
		
	}

	private void setListSizeLabels() {
		int numaddresses = m_oAddressesTableModel.getRowCount();
		int numselected = m_oSelectedTableModel.getRowCount();
		m_oLabelLeft.setText(numselected + " Adressen ausgew�hlt");
		m_oLabelRight.setText(numaddresses + " Adressen in Suchergebnis");
	}

	private void fillSubCategoryList(String kategoryId) {
		m_oIconComboBox_SubCategory.removeAllItems();
		m_oIconComboBox_SubCategory.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper
				.getIcon(GUIHelper.ICON_PERSON), "<Alle Unterkategorien>", null));
		if(kategoryId!=null){
			Map subcategories = m_oGUIListener.getVerteiler(kategoryId);
			Iterator it = subcategories.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry entry = (Map.Entry) (it.next());
				SubCategory list = (SubCategory) entry.getValue();
				m_oIconComboBox_SubCategory.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper
						.getIcon(GUIHelper.ICON_PERSON), list.toString(), list));
				
	
			}
		}
	}

	private void fillCategoryList() {
		Iterator iterator = m_oGUIListener.getCategoriesObjectList().iterator();
//             m_oIconComboBox_Category.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper
//              					.getIcon(GUIHelper.ICON_PERSON), "<Alle Kategorien>", null));
		while (iterator.hasNext()) {
			Category cat = (Category) (iterator.next());
			m_oIconComboBox_Category
                .addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper
                                                            .getIcon(GUIHelper.ICON_PERSON), cat.getName(), cat));
		}
	}

	private void fillFieldList() {
		String[] fieldNames = {"<Label>", "Herr/Frau", "Nachname", "Vorname", "Titel", "akadamischer Titel",
				"Namenszusatz", "Institution", "Stra�e", "Postleitzahl", "Ort", "Land", "Landeskennzeichen",
				"Bundesland", "Telefon dienstlich", "Telefon privat", "Fax dienstlich", "Fax privat", "E-Mail"};
		String[] fieldKeys = {Address.PROPERTY_SHORT_ADDRESS_LABEL.getKey(), Address.PROPERTY_HERRN_FRAU.getKey(), Address.PROPERTY_NACHNAME.getKey(),
                              Address.PROPERTY_VORNAME.getKey(), Address.PROPERTY_TITEL.getKey(), Address.PROPERTY_AKAD_TITEL.getKey(),
                              Address.PROPERTY_NAMENSZUSATZ.getKey(), Address.PROPERTY_INSTITUTION.getKey(), Address.PROPERTY_STRASSE.getKey(),
                              Address.PROPERTY_PLZ.getKey(), Address.PROPERTY_ORT.getKey(), Address.PROPERTY_LAND.getKey(), Address.PROPERTY_LKZ.getKey(),
                              Address.PROPERTY_BUNDESLAND.getKey(), Address.PROPERTY_TELEFON_DIENSTLICH.getKey(), Address.PROPERTY_TELEFON_PRIVAT.getKey(),
                              Address.PROPERTY_FAX_DIENSTLICH.getKey(), Address.PROPERTY_FAX_PRIVAT.getKey(), Address.PROPERTY_E_MAIL_DIENSTLICH.getKey()};
		for (int i = 0; i < fieldNames.length; i++) {
			m_oIconComboBox_Searchfield.addIconComboBoxEntry(new IconComboBoxEntry(GUIHelper
					.getIcon(GUIHelper.ICON_PERSON), fieldNames[i], fieldKeys[i]));
		}
	}

	private void setWaiting(boolean wait) {
		if (wait) this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		else this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		m_oGUIListener.setWaiting(wait);
	}

	private void fillAddressList() {
		setWaiting(true);
		m_oAddressesTableModel.removeAllRows();

		Object category = ((IconComboBoxEntry) m_oIconComboBox_Category.getSelectedItem()).getKey();
		Object subcategory = ((IconComboBoxEntry) m_oIconComboBox_SubCategory.getSelectedItem()).getKey();
		CategoriesFilter filter = new CategoriesFilter();
        try {
            if (category != null) 
                filter.add((Category) category);
        } catch (ContactDBException e1) {
            // Fehler bei Hinzuf�gen einer Kategorie
            // DO NOTING HERE
        }
		SubCategoriesFilter filter2 = new SubCategoriesFilter();
		if (subcategory != null) filter2.add((SubCategory) subcategory);
		//List filters = Collections.singletonList(filter);
		List filters = new ArrayList();
		filters.add(filter);
		filters.add(filter2);
		Map criteria = Collections.EMPTY_MAP;

		String fieldkey = (String) (((IconComboBoxEntry) (m_oIconComboBox_Searchfield.getSelectedItem())).getKey());
		String searchtext = "*" + m_oTextField_Searchtext.getText().trim() + "*";
		//criteria = Collections.singletonMap(fieldkey, searchtext);
		criteria = new HashMap();
		criteria.put(fieldkey, searchtext);
		criteria.put("matchCodes", Boolean.TRUE);

		Addresses addresses = m_oGUIListener.getAddresses(filters, criteria);

		for (int i = 0; i <= (addresses.getIndexMax()); i++) {
			Address address = addresses.get(i);
			insertAddress(address);
		}
		m_oAddressesTableModel.fireTableDataChanged();
		setWaiting(false);
		setListSizeLabels();
	}

	private class button_search_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			fillAddressList();
		}
	}

	private boolean isInSelectedList(Address address) {
        for (int i = 0; i < (m_oSelectedTableModel.getRowCount()); i++) {
            Address listaddress = (Address) (m_oSelectedTableModel.getRowKey(i));
            if (listaddress.getAdrNr() == address.getAdrNr()) return true;
        }

		return false;
	}

	private void moveRight(int selected) {
		int[] selectedrows = new int[1];
		selectedrows[0] = selected;
		moveRight(selectedrows);
	}

	private void moveRight(int[] selectedrows) {
		boolean founderrors = false;

		for (int i = 0; i < (selectedrows.length); i++) {
			if (selectedrows[i] != -1) {
				Address address = (Address) (m_oAddressesTableModel.getRowKey(selectedrows[i]));
				if (!isInSelectedList(address)) insertSelected(address);
				else founderrors = true;
			}
		}

		if (founderrors) Toolkit.getDefaultToolkit().beep();
		m_oSelectedTableModel.fireTableDataChanged();
		setListSizeLabels();
	}

	private class category_selected implements ActionListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent arg0) {
			int selected = m_oIconComboBox_Category.getSelectedIndex();
			IconComboBoxEntry entry = (IconComboBoxEntry) m_oIconComboBox_Category.getItemAt(selected);
			if (entry.getKey() != null) {
                fillSubCategoryList(((Category) entry.getKey()).getIdAsString());
                
			}else{
				fillSubCategoryList(null);
			}
		}
	}

	private class button_right_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			int[] selectedrows = m_oTable_Addresses.getSelectedRows();
			moveRight(selectedrows);
		}
	}

	private void moveLeft(int row) {
		int[] selectedrows = new int[1];
		selectedrows[0] = row;
		moveLeft(selectedrows);

	}

	private void moveLeft(int[] selectedrows) {
		Address[] selectedaddresses = new Address[selectedrows.length];

		for (int i = 0; i < (selectedrows.length); i++) {
			if (selectedrows[i] != -1) {
				Address address = (Address) (m_oSelectedTableModel.getRowKey(selectedrows[i]));
				selectedaddresses[i] = address;
			}
		}

		for (int i = 0; i < (selectedrows.length); i++) {
			if (selectedrows[i] != -1) {
				Address address = selectedaddresses[i];
				int row = m_oSelectedTableModel.getRowWithKey(address);
				if (row != -1) m_oSelectedTableModel.removeRow(row);
			}
		}

		m_oSelectedTableModel.fireTableDataChanged();
		setListSizeLabels();
	}

	private class button_left_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			int[] selectedrows = m_oTable_Selected.getSelectedRows();
			moveLeft(selectedrows);
		}
	}

	private class button_ok_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			m_oStatus = STATUS_OK;
			setVisible(false);
			setModal(false);
		}
	}

	private class escape_pressed implements KeyListener {

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 */
		public void keyPressed(KeyEvent arg0) {
			m_oGUIListener.getLogger().log(Level.FINE, arg0.toString());
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
		 */
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
		 */
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class button_cancel_clicked implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			m_oStatus = STATUS_CANCEL;
            closeWindow();
		}
	}

	private String	m_sUserIconDirectory	= null;

	private void setUserIconDirectory(String directory) {
		if ((directory.endsWith(File.separator))) {
			m_sUserIconDirectory = directory;
		} else {
			m_sUserIconDirectory = directory + File.separator;
		}
	}

	private ImageIcon getIconForUser(User user) {
		if (!m_bAskForIcon) return m_oIcon_ExternalUser;
		ImageIcon icon = null;
		if (m_sUserIconDirectory != null) {
			String loginname;
			try {
				loginname = user.getLoginName();
				String filename = m_sUserIconDirectory + loginname + ".gif";
				try {
					icon = new ImageIcon(new URL(filename));
				} catch (MalformedURLException e) {}
			} catch (ContactDBException e) {
				e.printStackTrace();
			}

		}
		return icon;
	}

	private ImageIcon getIconForAddress(Address address) {
        return null;
// TODO: Addresszugriff: make this code working or remove the whole class
// 		try {
// 			if (address.getAssociatedUser() != null) {
// 				ImageIcon icon = getIconForUser(address.getAssociatedUser());
// 				if (icon == null) icon = m_oIcon_SystemUser;
// 				return icon;
// 			}
// 		} catch (ContactDBException e) {
// 			m_oGUIListener
// 					.getLogger()
// 					.log(
// 							Level.SEVERE,
// 							"Konnte nicht ermitteln ob Teilnehmer ein Systembenutzer ist um ein entsprechendes Icon auszuw�hlen",
// 							e);
// 		}
// 		return m_oIcon_ExternalUser;
	}

	public void insertAddress(Address address) {
		Object[] line = new Object[2];
		line[0] = getIconForAddress(address);
        line[1] = address.getShortAddressLabel();
		m_oAddressesTableModel.addRow(line, address);
	}

	public void insertSelected(Address address) {
		Object[] line = new Object[2];
		line[0] = getIconForAddress(address);
        line[1] = address.getShortAddressLabel();
		m_oSelectedTableModel.addRow(line, address);
	}

	public Object getReturnStatus() {
		return m_oStatus;
	}

	public List getSelectedAddresses() {
		List list = new ArrayList();
		for (int i = 0; i < (m_oSelectedTableModel.getRowCount()); i++) {
			Address address = (Address) (m_oSelectedTableModel.getRowKey(i));
			list.add(address);
		}

		return list;
	}

	public static class AddressTableModel extends AbstractTableModel {

		private int			m_iNumColumns;
		private String[]	m_sColumnNames	= null;
		private Boolean[]	m_bIsEditable;
		private List		m_oRows;
		private List		m_oRowKeys;

		public AddressTableModel(int numcolumns) {
			m_iNumColumns = numcolumns;
			m_sColumnNames = new String[m_iNumColumns];
			m_bIsEditable = new Boolean[m_iNumColumns];
			m_oRows = new ArrayList();
			m_oRowKeys = new ArrayList();

			for (int i = 0; i < m_iNumColumns; i++) {
				m_sColumnNames[i] = "";
				m_bIsEditable[i] = new Boolean(false);
			}
		}

		public void setColumnName(int column, String name) {
			m_sColumnNames[column] = name;
		}

		public void addRow(Object[] columns) {
			m_oRows.add(columns);
			m_oRowKeys.add(new Object());
		}

		public void addRow(Object[] columns, Object rowkey) {
			m_oRows.add(columns);
			m_oRowKeys.add(rowkey);
		}

		public Object[] getRow(int row) {
			return (Object[]) (m_oRows.get(row));
		}

		public Object getRowKey(int row) {
			return m_oRowKeys.get(row);
		}

		public Object setRowKey(int row, Object key) {
			return m_oRowKeys.set(row, key);
		}

		public int getRowWithKey(Object key) {
			for (int i = 0; i < (m_oRowKeys.size()); i++) {
				if (getRowKey(i).equals(key)) return i;
			}
			return -1;
		}

		public void removeRow(int row) {
			m_oRows.remove(row);
			m_oRowKeys.remove(row);
		}

		public void removeAllRows() {
			m_oRows.clear();
			m_oRowKeys.clear();
		}

		public int getColumnCount() {
			return m_iNumColumns;
		}

		public int getRowCount() {
			return m_oRows.size();
		}

		public String getColumnName(int col) {
			return m_sColumnNames[col];
		}

		public Object getValueAt(int row, int col) {
			Object[] rowdata = getRow(row);
			return rowdata[col];
		}

		public Class getColumnClass(int c) {
			try {
				return getValueAt(0, c).getClass();
			} catch (NullPointerException npe) {
				return String.class;
			}
		}

		public boolean isCellEditable(int row, int col) {
			return m_bIsEditable[col].booleanValue();
		}

		public void setColumnEditable(int col, boolean iseditable) {
			m_bIsEditable[col] = new Boolean(iseditable);
		}

		public void setValueAt(Object value, int row, int col) {
			Object[] rowdata = getRow(row);
			rowdata[col] = value;
			fireTableCellUpdated(row, col);
		}
	}

	// --------------------------------------------------------------------------------

	public class DestAddressDnDData extends TarentDnDData {

		private int[]	m_iRows;

		public DestAddressDnDData(int[] rows) {
			setRows(rows);
		}

		public int[] getRows() {
			return m_iRows;
		}

		public void setRows(int[] rows) {
			m_iRows = rows;
		}
	}

	public class SourceAddressDnDData extends TarentDnDData {

		private int[]	m_iRows;

		public SourceAddressDnDData(int[] rows) {
			setRows(rows);
		}

		public int[] getRows() {
			return m_iRows;
		}

		public void setRows(int[] rows) {
			m_iRows = rows;
		}
	}

	public class ListDnDListener extends TarentDnDAdapter {

		public void objectDropped(JComponent comp, Object obj) {
			if (obj instanceof SourceAddressDnDData) {
				int[] rows = ((SourceAddressDnDData) obj).getRows();

				m_oGUIListener.getLogger().log(Level.FINE,"moveAdressRight(" + rows + ")");
				moveRight(rows);
			}
		}

		public TarentDnDData objectDragged(JComponent comp) {
			int[] rows = m_oTable_Selected.getSelectedRows();
			return new DestAddressDnDData(rows);
		}
	}

	public class ListDnDListenerSource extends TarentDnDAdapter {

		public void objectDropped(JComponent comp, Object obj) {
			if (obj instanceof DestAddressDnDData) {
				int[] rows = ((DestAddressDnDData) obj).getRows();

				m_oGUIListener.getLogger().log(Level.FINE,"moveAdressLeft(" + rows + ")");
				moveLeft(rows);
			}
		}

		public TarentDnDData objectDragged(JComponent comp) {
			int rows[] = m_oTable_Addresses.getSelectedRows();
			return new SourceAddressDnDData(rows);
		}
	}

	// --------------------------------------------------------------------------------

	private class DoubleClickMouseListenerToRight extends MouseAdapter {

		public void mouseClicked(MouseEvent e) {
			if ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 2)) {
				int[] selectedrows = m_oTable_Addresses.getSelectedRows();
				//In den selectedRows ist nicht n�tigerweise auch die aktuell
				// doppelgeklickte vorhanden..
				int row = m_oTable_Addresses.rowAtPoint(e.getPoint());
				boolean in = false;
				for (int i = 0; !in && i < selectedrows.length; i++) {
					if (row == selectedrows[i]) in = true;
				}
				if (in) {
					//Wenn vorhanden, verschiebe alle selektierten
					moveRight(selectedrows);
				} else {
					//Ansonsten selektiere aktuelle und verschiebe diese
					m_oTable_Addresses.clearSelection();
					m_oTable_Addresses.setRowSelectionInterval(row, row);
					moveRight(row);
				}
			}
		}
	}

	private class DoubleClickMouseListenerToLeft extends MouseAdapter {

		public void mouseClicked(MouseEvent e) {
			if ((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() == 2)) {
				int[] selectedrows = m_oTable_Addresses.getSelectedRows();
				int row = m_oTable_Addresses.rowAtPoint(e.getPoint());
				boolean in = false;
				for (int i = 0; !in && i < selectedrows.length; i++) {
					if (row == selectedrows[i]) in = true;
				}
				if (in) {
					//Wenn vorhanden, verschiebe alle selektierten
					moveRight(selectedrows);
				} else {
					//Ansonsten selektiere aktuelle und verschiebe diese
					m_oTable_Addresses.clearSelection();
					m_oTable_Addresses.setRowSelectionInterval(row, row);
					moveLeft(row);
				}
			}
		}
	}

}