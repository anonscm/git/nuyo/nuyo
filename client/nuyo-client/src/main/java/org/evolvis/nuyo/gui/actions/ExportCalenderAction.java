package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.plugin.Plugin;


/**
 * Exports user appoitments and tasks.<p>
 * At the moment SXC-Format will be used.<p>
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ExportCalenderAction extends AbstractGUIAction {

    private static final long serialVersionUID = -5205480337280550434L;
    private static final TarentLogger logger = new TarentLogger(ExportCalenderAction.class);
    private TarentCalendar calendar;
    private Runnable executor;

    public void actionPerformed(ActionEvent e) {
        if(calendar != null){
            SwingUtilities.invokeLater(executor);
        } else logger.warning(getClass().getName()+" is not initialized");
    }

    public void init(){
        Plugin calendarPlugin = PluginRegistry.getInstance().getPlugin(CalendarPlugin.ID);
        if(calendarPlugin != null) {
           if(calendarPlugin.isTypeSupported(TarentCalendar.class)){
               calendar = (TarentCalendar) calendarPlugin.getImplementationFor(TarentCalendar.class);               
           } else logger.warning("Couldn't init Export Calendar Action", "Tarent Calendar interface is not supported by Calender Plugin.");
        } else logger.warning("Couldn't init Export Calendar Action", "Calender Plugin not registered.");

        executor = new Runnable(){
            public void run() {
                calendar.exportAppointmentsAndTasks();
            }
        };
    }
}
