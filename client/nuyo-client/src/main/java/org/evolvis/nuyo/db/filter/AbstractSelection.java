package org.evolvis.nuyo.db.filter;

import org.evolvis.nuyo.db.persistence.IEntity;

/**
 * @author kleinw
 *
 */
abstract public class AbstractSelection implements SelectionElement {

    private IEntity _value;
    
    public Integer getValue() {return new Integer(_value.getId());}
    
    
}
