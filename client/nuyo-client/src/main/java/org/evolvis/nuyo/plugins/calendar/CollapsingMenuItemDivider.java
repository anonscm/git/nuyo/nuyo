/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.plugins.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

/**
 * @author niko
 * @deprecated Calendar plugin uses it. Remove it when rewritten.
 */
class CollapsingMenuItemDivider extends JPanel implements CollapsingMenuItem
{
  private boolean    m_bIsEnabled;
  private boolean    m_bIsSelected;
  private boolean    m_bIsVisible;
  private Object     m_oUserData;
  private int        m_iUserData;
  private String     m_sUserString;
  private String     m_sUserDescription;  
  private JSeparator m_Separator; 
  
  public CollapsingMenuItemDivider()
  {
    super();    
    m_bIsEnabled = true;
    m_bIsSelected = false;
    m_bIsVisible = true;
    this.setLayout(new BorderLayout());

    m_Separator = new JSeparator(SwingConstants.HORIZONTAL);
    this.add(m_Separator, BorderLayout.CENTER);
  }

  
  public ImageIcon getIcon()
  {
    return(null);
  }
   

  public ActionListener getActionListener()
  {
    return(null);
  }

  public void setActionListener(ActionListener al)
  {
  }


   
  public void setUserString(String text)
  {  
    m_sUserString = text;
  }
  
  public String getUserString()
  {  
    return(m_sUserString);
  }

  public void setUserDescription(String text)
  {  
    m_sUserDescription = text;
  }
  
  public String getUserDescription()
  {  
    return(m_sUserDescription);
  }
  
  
  
  
  
  public JButton getImageButton()
  {
    return(null);
  }

  public JComponent getTextButton()
  {
    return(null);
  }
  
  public void setText(String text)
  {
  }

  public String getText()
  {
    return("");
  }

  public void setIcon(ImageIcon icon)
  {
  }


  public void setForegroundColor(Color color)
  {
    m_Separator.setForeground(color);
  }

  public void setHighlightColor(Color color)
  {
  }

  public void setBackgroundColor(Color color)
  {
  }

  public void setSelectionColor(Color color)
  {
  }

  public void setSelectionHighlightColor(Color color)
  {
  }

  public void drawBorderWhenSelected(boolean drawborder)
  {
  }

  public void setSelectedItemBorder(int width)
  {    
  }


  public void setItemBorder(int width)
  {    
  }

  public void setItemToolTipText(String text)
  {
  }

  public void setItemEnabled(boolean isenabled)
  {
    m_bIsEnabled = isenabled;
  }

  public boolean getItemEnabled()
  {
    return(m_bIsEnabled);
  }


  public void setItemSelected(boolean isselected)
  {
    m_bIsSelected = isselected;
  }

  public boolean isSelected()
  {
    return(m_bIsSelected);
  }
  
  
  public void setItemIndent(int indent)
  {
  }

  public void setItemVisible(boolean isvisible)
  {
    m_bIsVisible = isvisible;
    this.setVisible(m_bIsVisible);
  }
  
  public boolean getItemVisible()
  {
    return(m_bIsVisible);
  }


  public void setUserDataObject(Object userdata)
  {
    m_oUserData = userdata;
  }

  public void setUserDataInt(int userdata)
  {
    m_iUserData = userdata;
  }
  
  public Object getUserDataObject()
  {
    return(m_oUserData);
  }

  public int getUserDataInt()
  {
    return(m_iUserData);
  }

  public void setItemFont(Font font)
  {
  }

  public Font getItemFont()
  {
    return(null);
  }

  public void setDescription(String text)
  {
  }
  
  public String getDescription()
  {
    return(null);
  }
  
  public void setAcceleratorKey(char key)
  {	  
//	if (Character.toUpperCase(key) >= 'A' && Character.toUpperCase(key) <= 'Z')
//	{
//		button.setMnemonic((int)Character.toUpperCase(key));
//	}
  }

  public void setAcceleratorKey(String key)
  {
    if (key != null) 
    {
      if (key.length() > 0) setAcceleratorKey(key.charAt(0));
    } 
  }
  
  public void setAcceleratorIndex(int charnum)
  {
    //m_oTextButton.setDisplayedMnemonicIndex(charnum);
  }  
  

}
