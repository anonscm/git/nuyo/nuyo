/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Iterator;

import org.evolvis.nuyo.controls.TarentWidgetComboBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;
import org.evolvis.nuyo.util.general.ContactLookUpNotFoundException;



/**
 * TODO: remove if not used in AddressFieldFactory.getGUIElement()!
 * @deprecated not used (only by factory)
 * 
 * @author niko
 */
public class ComboField extends GUIElementAdapter implements FocusListener
{
  private TarentWidgetComboBox m_oComboBox;
  private String m_sLookupTableKey = null; 
  private String[] m_sLookupTableValues = null; 
  
  public ComboField()
  {
    super();
     
    m_oComboBox = new TarentWidgetComboBox();
  }
  
  public String getListenerName()
  {
    return "ComboField";
  }  
  
  public GUIElement cloneGUIElement()
  {
    ComboField combofield = new ComboField();
    combofield.m_sLookupTableKey = m_sLookupTableKey;
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      combofield.addParameter(param.getKey(), param.getValue());
    }
    return combofield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "ComboField", new DataContainerVersion(0));
    
    ParameterDescription lookuppd = new ParameterDescription("lookup", "LookUp Table", "die in diesem Feld benutzte LookupTable.");
    lookuppd.addValueDescription("String", "der Key der Lookup-Tabelle");
    d.addParameterDescription(lookuppd);
    
    ParameterDescription valuespd = new ParameterDescription("values", "Values", "die in diesem Feld verwendeten Werte.");
    valuespd.addValueDescription("String", "durch \",\" getrennte Werte");    
    d.addParameterDescription(valuespd);
    
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("lookup".equalsIgnoreCase(key)) 
    {
      m_sLookupTableKey = value;
      m_sLookupTableValues = null;
      m_oParameterList.add(new ObjectParameter(key, value));
      return true;
    }    
    else if ("values".equalsIgnoreCase(key)) 
    {
      m_sLookupTableKey = null;
      m_sLookupTableValues = value.split(",");
      m_oParameterList.add(new ObjectParameter(key, value));
      return true;
    }    
    return false;
  }

  public boolean canDisplay(Class data)
  {
    return data.isInstance(String.class);
  }

  public Class displays()
  {
    return String.class;
  }
  

  public TarentWidgetInterface getComponent()
  {
    return m_oComboBox;
  }

  public void setData(Object data)
  {
    if (data instanceof String)
    {
      m_oComboBox.setSelectedItem(data);      
    }
    else if (data instanceof String[])
    {
      m_oComboBox.setData(data);      
    }
  }

  public Object getData()
  {
    return m_oComboBox.getSelectedItem();
  }


  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    //System.out.println("focuslost at " + e.getComponent().getLocation());
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }
  
  
  public void initElement()
  {
    m_oComboBox.addFocusListener(this);
    m_oComboBox.removeAllItems();
    
    String[] entries = null;
    if (m_sLookupTableKey != null)
    {
      try
      {
        entries = getDataContainer().getDataAccess().getLookupTableEntries(m_sLookupTableKey);
      }
      catch (ContactLookUpNotFoundException e) {}      
    }
    else if (m_sLookupTableValues != null)
    {
      entries = m_sLookupTableValues;
    }

    if (entries != null)
    {
      for(int i=0; i<(entries.length); i++)
      {
        m_oComboBox.addItem(entries[i]);
      }
    }    
  }

  public void disposeElement()
  {
    m_oComboBox.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oComboBox.requestFocus();
  }
  
  
  public Point getOffsetForError(ErrorHint errorhint)
  {
    return(new Point(0,0));
  }

}
