package org.evolvis.nuyo.dupview;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * This action invokes a <code>CompareView</code>.
 * 
 * @author Thomas Schmitz, tarent GmbH
 *
 */
public class CompareAction extends AbstractGUIAction implements AddressListConsumer {
	
	AddressListProvider provider;
	
	/* (non-Javadoc)
	 * @see de.tarent.contact.gui.action.AddressListConsumer#registerDataProvider(de.tarent.contact.gui.action.AddressListProvider)
	 */
	public void registerDataProvider(AddressListProvider provider) {
		this.provider = provider;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		SwingUtilities.invokeLater(new Runnable(){			
			public void run() {
				new CompareView("addressDups.vm", provider.getAddresses());				
			}			
		});		
	}

}
