/*
 * Created on 01.12.2004
 *
 */
package org.evolvis.nuyo.datacontainer.parser;

import java.util.Iterator;
import java.util.Map;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;



/**
 * @author niko
 *
 */
public interface DataContainerProvider
{
  public Object[] getDataContainerKeys();
  public DataContainer getDataContainer(Object key);
  public Iterator iterator();
  public Map getDataContainerMap();  
}
