/*
 * Created on 26.11.2004
 *
 */
package org.evolvis.nuyo.controls;

/**
 * @author niko
 *
 */
public interface TarentWidgetChangeListener
{
  public void changedValue(Object newvalue);
}
