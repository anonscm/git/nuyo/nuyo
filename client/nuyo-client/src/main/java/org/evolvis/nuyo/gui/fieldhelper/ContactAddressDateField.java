package org.evolvis.nuyo.gui.fieldhelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.gui.Messages;


abstract public class ContactAddressDateField extends ContactAddressField {

	  
	/**
	 * Flags whether the instance has once complained about an invalid
	 * date format.
	 */
	private boolean complained;
	  
	/**
	 * A copy of the Date instance given to the UI component.
	 * 
	 * <p>This is used to restore the last working entry.</p>
	 */
	private Date dateCopy;
	  
	protected SimpleDateFormat dateFormat = new SimpleDateFormat(Messages.getString("GUI_ContactAddressDateField_FormatPattern"));
	  // parsing format 
	protected SimpleDateFormat shortYearDateFormat = new SimpleDateFormat(Messages.getString("GUI_ContactAddressDateField_ShortYearFormatPattern"));

	protected TarentWidgetTextField dateField = new TarentWidgetTextField("");
	  
	protected Date getDateFromTextField(){
		
		Date dateToSave = null;
		if (!dateField.getText().equals("")){
			try {			
	            // we use short year format in order to handle both short and full year inputs (see bug 2706)
	            // it works due to not strict pattern parsing: DateFormat#isLenient() returns true by default.
	            dateToSave = shortYearDateFormat.parse(dateField.getText());
	            
			} catch (ParseException e) {
				try {
					dateToSave = dateFormat.parse(dateField.getText());
				} catch (Exception e2) {
					return null;
				}
			}
	  	}		
		return dateToSave;
	}
	
	protected void setTextFieldFromDate(Date date){
		 
	    if (date == null)
	    {
	      dateField.setText("");
	    }
	    else 
	    {
	      dateField.setText(dateFormat.format(date));
	    }
	}

	public void setEditable(boolean iseditable) {
		dateField.setEnabled(iseditable);
	}
	
	/**
	 * Stores last valid Date
	 * @param dateToSave
	 */
	protected void setLastDate(Date dateToSave){
		dateCopy = dateToSave;
	}
	
	protected Date getLastDate(){
		return dateCopy;
	}
	
	protected void setComplained(boolean c){
		complained = c;
	}
	
	protected boolean getComplained(){
		return complained;
	}

	protected void setDateformat(String format){
		dateFormat = new SimpleDateFormat(format);
	}
	
	protected void setShortDateFormat(String format){
		shortYearDateFormat = new SimpleDateFormat(format);
	}
}
