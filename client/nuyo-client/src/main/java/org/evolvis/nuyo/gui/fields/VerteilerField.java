/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.Dimension;

import javax.swing.JScrollPane;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetListBox;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class VerteilerField extends ContactAddressTextField
{
  private final boolean m_bShowFullVerteilerName = false;
  
  private TarentWidgetListBox m_oListBox_verteiler;
  private TarentWidgetLabel m_oLabel_verteiler;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }  
  
  public boolean isWriteOnly()
  {
    return true;  
  }
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createVerteilerPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "VERTEILER";
  }

  public void postFinalRealize()
  {    
  }
  
  
  public void setEditable(boolean iseditable)
  {
  }

  public void setData(PluginData data)
  {
    fillVerteilerTextfield();
  }

  public void getData(PluginData data)
  {
  }

  public boolean isDirty(PluginData data)
  {
    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  public String getFieldName()
  {
    return Messages.getString("GUI_MainFrameNewStyle_Standard_Unterkategorien");    
  }
  
  public String getFieldDescription()
  {
    return Messages.getString("GUI_MainFrameNewStyle_Standard_Unterkategorien_ToolTip");
  }
  
  private EntryLayoutHelper createVerteilerPanel(String widgetFlags)
  {
    m_oListBox_verteiler = new TarentWidgetListBox();
    m_oListBox_verteiler.getJScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    m_oListBox_verteiler.setToolTipText(Messages.getString("GUI_MainFrameNewStyle_Standard_Unterkategorien_ToolTip"));     //$NON-NLS-1$
    m_oListBox_verteiler.setVisibleRowCount(3);
    m_oListBox_verteiler.setMinimumSize(new Dimension(200, 30));
    m_oListBox_verteiler.setWidgetEditable(false);
    m_oLabel_verteiler = new TarentWidgetLabel(Messages.getString("GUI_MainFrameNewStyle_Standard_Unterkategorien"));
    
    return(new EntryLayoutHelper(m_oLabel_verteiler, m_oListBox_verteiler, widgetFlags));
  }

  
  private void fillVerteilerTextfield()
  {
      // commented for new Category Concept
      //TODO: Addresszugriff: Show the addresses categories
//       List list = new ArrayList();
      
      
//       SortedMap verteiler = getGUIListener().getVerteiler(null);
//       SortedMap addressverteiler = getGUIListener().getVerteilerOfAddress();
    
//       if ((verteiler != null) && (addressverteiler != null) )
//           {
//               Iterator addressverteileriterator = addressverteiler.keySet().iterator();   
//               String key;
//               SubCategory verteilerdata;
//               //String verteilerstr = ""; //$NON-NLS-1$
        
//               while(addressverteileriterator.hasNext())
//                   { 
//                       key = asString(addressverteileriterator.next());
//                       verteilerdata = (SubCategory)(verteiler.get(key));
          
//                       if (verteilerdata != null) 
//                           {
//                               try {
//                                   if (m_bShowFullVerteilerName) list.add(verteilerdata.toString());
//                                   else list.add(verteilerdata.getName().toString());
//                               } catch (ContactDBException e){
//                                   // DO NOTHING HERE
//                               }
//                           } 
//                       else
//                           {
//                               list.add("?????");
//                           }

//                       //if (m_bShowFullVerteilerName) verteilerstr += "; ";
//                       //else                                             verteilerstr += key + " ";
//                   }          
//               m_oListBox_verteiler.setData(list.toArray());            
//           }
//       else
//           {
//               m_oListBox_verteiler.setData(null);
//           }

  }

  private final static String asString(Object o) 
  {
    return o == null ? null : o.toString();
  }

}
