/* $Id: DatabaseInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 07.10.2003
 */
package org.evolvis.nuyo.messages;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;

/**
 * Diese Informationsereignisklasse wird benutzt, um �ber einen Wechsel der
 * Datenzugriffsinstanz zu informieren.
 *   
 * @author mikel
 */
public class DatabaseInfo extends MasterInfo {
    /**
     * @param source Ereignisquelle
     * @param db neue / ge�nderte Datenzugriffsinstanz
     */
    public DatabaseInfo(Object source, Database db) {
        super(source);
        this.db = db;
    }

    /*
     * Klasse Object
     */
    public String toString() {
        try {
            return super.toString() + "[database: " + db.getDisplaySource() + ']';
        } catch (ContactDBException e) {
            return super.toString() + "[database]";
        }
    }

    /*
     * Getter und Setter
     */
    public Database getDb() {
        return db;
    }

    /*
     * gesch�tzte Variablen
     */
    protected Database db;
}
