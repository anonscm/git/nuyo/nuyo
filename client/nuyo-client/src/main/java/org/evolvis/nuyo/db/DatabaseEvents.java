/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Michael Klink. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.db;

import java.util.HashSet;
import java.util.Set;

import org.evolvis.nuyo.db.event.AddressListener;


/**
 * Diese Klasse verwaltet EventListener f�r das 
 * {@link Database Database-Objekt} und
 * feuert die entsprechenden Events.
 * 
 * @see org.evolvis.nuyo.db.event.AddressListener
 * @author mikel
 */
public class DatabaseEvents {
    /*
     * AddressListener-Verwaltung
     */
    /**
     * Diese Liste verwaltet die angemeldeten
     * {@link AddressListener AddressListener}s.
     */
    protected final Set addressListeners = new HashSet();

    /**
     * Diese Methode f�gt einen neuen
     * {@link AddressListener AddressListener}
     * zu den verwalteten Listenern hinzu.
     * 
     * @param listener der neue Listener.
     */
    public void addAddressListener(AddressListener listener) {
        if (listener != null)
            addressListeners.add(listener);
    }
    
    /**
     * Diese Methode nimmt einen
     * {@link AddressListener AddressListener}
     * aus der Liste der verwalteten Listener heraus.
     * 
     * @param listener der alte Listener.
     */
    public void removeAddressListener(AddressListener listener) {
        if (listener != null)
            addressListeners.remove(listener);
    }
}
