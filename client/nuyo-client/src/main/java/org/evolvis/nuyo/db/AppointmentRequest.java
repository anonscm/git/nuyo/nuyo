/* $Id: AppointmentRequest.java,v 1.4 2006/10/27 13:21:53 aleksej Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

import java.util.Date;

/**
 * Diese Schnittstelle beschreibt allgemein Terminanfragen, wie sie an
 * Kalendarien oder Adressen geheftet werden k�nnen.
 * 
 * @author mikel
 */
public interface AppointmentRequest {
    
    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();

    
    //
    // Attribute
    //
    /** Zuordnungsdatum und -zeit des Termins (UTC) zu Terminanfrage */
    public Date getCreation() throws ContactDBException;

    /** Der Termin, der Angefragt ist */
    public Appointment getAppointment() throws ContactDBException;

    /** Eintrag anzeigen als Termin-Anfrage/Termin-Einladung, vgl. Konstanten <code>TYPE_*</code> */
    public int getRequestType() throws ContactDBException;
    public void setRequestType(int newRequestType) throws ContactDBException;
    
    /** 0=noch nicht versendet, 1=versendet, 2=zugesagt, 3=abgelehnt, vgl. Konstanten <code>STATUS_*</code> */
    public int getRequestStatus() throws ContactDBException;
    public void setRequestStatus(int newRequestStatus) throws ContactDBException;
    
    /** Teilnahme ist erforderlich/erw�scht/optional..., vgl. Konstanten <code>LEVEL_*</code> */
    public int getRequestLevel() throws ContactDBException;
    public void setRequestLevel(int newRequestLevel) throws ContactDBException;

    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Anzeigename, ausf�hrlichere Notiz */
    public final static String KEY_DISPLAY_NAME = "display";
    public final static String KEY_DESCRIPTION = "description";
    
    /** Request-Level: erforderlich, erw�nscht, optional */
    public final static int LEVEL_REQUIRED = 1;
    public final static int LEVEL_DESIRED = 2;
    public final static int LEVEL_OPTIONAL = 3;
    
    /** Request-Typ: Termin-Anfrage - This event person does the invitations to the event */
    public final static int TYPE_REQUEST = 1;
    /** Request-Typ: Termin-Einladung - This event person is invited to the event */
    public final static int TYPE_INVITATION = 2;
    
    /** Request-Status: offen, gesendet, abgelehnt, delegiert, erledigt, angenommen, vorl�ufig, best�tigt */
    public final static int STATUS_NEEDS_ACTION = 0;
    public final static int STATUS_SENT = 1;
    public final static int STATUS_DECLINED = 2;
    public final static int STATUS_DELEGATED = 3;
    public final static int STATUS_COMPLETED = 4;
    public final static int STATUS_ACCEPTED = 5;
    public final static int STATUS_TENTATIVE = 6;
    public final static int STATUS_CONFIRMED = 7;
}
