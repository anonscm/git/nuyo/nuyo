package org.evolvis.nuyo.plugins.vcard;

import java.util.Collections;
import java.util.List;

import org.evolvis.nuyo.plugin.AddressExporter;

import de.tarent.commons.plugin.Plugin;

/**
 * This plugin delivers a <code>VCardExporter<code>.
 * This VCardExporter (implementing <code>AddressExporter<code>) 
 * will be catched and controlled by an activated <code>ExportFilePlugin</code>.  
 * 
 * @author Jens Neumaier, tarent GmbH
 * @see org.evolvis.nuyo.plugins.vcard.VCardExporter
 * @see org.evolvis.nuyo.plugins.dataio.ExportFilePlugin
 *
 */
public class VCardPlugin implements Plugin {
    
    static public final String ID = "vcard";
    
    public VCardPlugin() {        
    }

    public String getID() {
        return ID;
    }

    public void init() {
        
    }

    public Object getImplementationFor(Class type) {
        
        if (type == AddressExporter.class) {
            return new VCardExporter();
        }
        return null;
    }

    public List getSupportedTypes() {
        List list = Collections.singletonList(AddressExporter.class);
        return list;
    }

    public boolean isTypeSupported(Class type) {
        if (type.equals(AddressExporter.class)){
            return true;
        }
        return false;
    }

	public String getDisplayName() {
		return "vCard Export";
	}

}

