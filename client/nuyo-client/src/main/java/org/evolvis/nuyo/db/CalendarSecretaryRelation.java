/*
 * Created on 31.03.2005
 *
 */
package org.evolvis.nuyo.db;

import org.evolvis.nuyo.util.DateRange;

/**
 * @author simon
 *
 * Verwaltet die Zuordnung der Kalender Freigaben an Benutzer
 * 
 */
public interface CalendarSecretaryRelation {
    
    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();
    
    //
    // Attribute
    //
    
    /** zugreifender Benutzer */
    public Integer getSecretaryUserID() throws ContactDBException;
    
    /** zugeh�riger Kalenderbesitzer */
    public Integer getSecretaryUserOwnerID() throws ContactDBException;
    
    /** zugeh�riger Kalender */
    public Integer getCalendarID() throws ContactDBException;
    
    /** Zugriff auf den Kalender zu aktuellem Zeitpunkt abfragen */
    public DateRange getAccessDateRange();

    /** Zugriff auf den Kalender setzen */
    public void setAccessDateRange(DateRange AccessDateRange);

    /** Leseberechtgung der Termine in angegebenen Zeitraum abfragen */
    public DateRange getReadDateRange();

    /** Zugriff auf den Kalender setzen */
    public void setReadDateRange(DateRange ReadDateRange);

    /** Berechtigung, Termine im angegebenen Zeitraum zu erstellen */
    public DateRange getWriteDateRange();
    
    /** Zugriff auf den Kalender setzen */
    public void setWriteDateRange(DateRange WriteDateRange);
    

}
