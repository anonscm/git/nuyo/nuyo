/* $Id: ConfigurableApplicationStarter.java,v 1.12 2007/08/30 16:10:25 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Map;
import java.util.logging.Level;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;

import de.tarent.commons.plugin.Plugin;


/**
 * Starten von Anwendungen wie Browser, Mail, ... aus dieser Anwendung heraus.
 *
 * Klasse, die Systemunabh�ngig configurierte Kommandos ausf�hrt.
 * Sollte nur �ber die Factorymethode
 * {@link org.evolvis.nuyo.controller.ApplicationStarter#getSystemApplicationStarter(Map, ControlListener)
 * ApplicationStarter.getSystemApplicationStarter} instanziiert werden.
 * 
 * @author Sebastian, mikel
 */
public class ConfigurableApplicationStarter extends ApplicationStarter {
    
    private final static String CONST_EMAIL_ADDRESS = "\\{0\\}";
    private final static String CONST_EMAIL_SUBJECT = "\\{1\\}";
    private final static String CONST_EMAIL_BODY    = "\\{2\\}";
    
    private TarentLogger logger = new TarentLogger(getClass());
    public ConfigurableApplicationStarter() {
    }

    /**
     * Startet den Browser
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startBrowser(String url) {
        String browser_command;
        browser_command = asString(ConfigManager.getEnvironment().get(Key.BROWSER_COMMAND));
        if (browser_command == null || browser_command.length() == 0) {
            return "Browser noch nicht konfiguriert";
        }

        if (!(url.toUpperCase().startsWith("HTTP://"))) {
            url = "http://" + url;
        }

        String goodurl = null;
        try {
            URL temp_url;
            temp_url = new URL(url);
            goodurl = temp_url.toString();
        } catch (MalformedURLException mue) {
            return "Die URL ist ung�ltig.";
        }

        String cmd = browser_command + " " + goodurl;
        return runCommand(cmd);
    }

    /**
     * Startet den Mail Client
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startEMail(String emailAddress) {
    	
    	// See de.tarent.contact.plugins.mail.actions.EMailConfigurationAction for more
    	
        String mailCommand = asString(ConfigManager.getPreferences().get(Key.EMAIL_COMMAND.toString(), "mozilla-thunderbird -mail mailto:{0}"));
        
        if (emailAddress == null || emailAddress.length() == 0)
            return "Es ist keine EMail-Adresse vorhanden.";

        mailCommand =  mailCommand.replaceAll(CONST_EMAIL_ADDRESS, emailAddress);
        mailCommand =  mailCommand.replaceAll(CONST_EMAIL_SUBJECT, "");
        mailCommand =  mailCommand.replaceAll(CONST_EMAIL_BODY, "");
        
        //String cmd = mail_command + /*" " +*/
        //mail;
        
        return runCommand( mailCommand );
    }
    
    public String startEMail(Contact con)
    {
        try
		{
			return startEMail(con.getAddress());
		} catch (ContactDBException e)
		{
			// TODO I18n
			logger.warning("Sending E-Mail failed", e);
		}
		return null;
    }
    
    public String startEMail(Address address) {
    	if(address != null) {
    		System.out.println("address is not null");
    		if(address.getEmailAdresseDienstlich() != null && address.getEmailAdresseDienstlich().length() > 0) {
    			System.out.println("using email dienstlich");
    			return startEMail(address.getEmailAdresseDienstlich());
    		}
    		else {
    			System.out.println("using email private");
    			return startEMail(address.getEmailAdressePrivat());
    		}    			
    	}
    	System.out.println("address is null");
    	return "Es ist keine EMail-Adresse vorhanden."; 
    }
    

    public String startPhoneCall(String phoneNumber) {
        String phoneCommand = ConfigManager.getEnvironment().get(Key.PHONE_COMMAND);
        if (phoneCommand == null || phoneCommand.trim().length() == 0)
            return "kein Telefonie-Kommando konfiguriert\n\n(Eintrag "
            	+ Key.PHONE_COMMAND.toString() + " mit {0} als Platzhalter f�r die Nummer)";

        String cmd = MessageFormat.format(phoneCommand, new Object[] { phoneNumber });
        return runCommand(cmd);
    }
    
    /**
     * Startet einen Postversand an eine Einzeladresse
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, Address address) {
    	return startOfficePlugin(action, address, null, null, null);
    }

    /**
     * Startet einen Postversand an eine Adressauswahl im Rahmen des optimierten
     * Postversands.
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, MailBatch batch) {
        return startOfficePlugin(action, batch, null, null, null);
    }

    /**
     * Startet einen Postversand an eine Einzeladresse
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, Address address, String serverurl, String username, String password) {
        return startOfficePlugin(action, address, serverurl, username, password);
    }

    /**
     * Startet einen Postversand an eine Adressauswahl im Rahmen des optimierten
     * Postversands.
     * 
     * @return null, wenn alles ok ist, eine Fehlermeldung sonst
     */
    public String startPostAction(String action, MailBatch batch, String serverurl, String username, String password) {
    	return startOfficePlugin(action, batch, serverurl, username, password);
    }
    
    private String startOfficePlugin(String pAction, Object pData, String pServerURL, String pUserName, String pPassword)
    {
    	if(PluginRegistry.getInstance().isPluginAvailable("office_exporter"))
    	{
    		Plugin exportPlugin = PluginRegistry.getInstance().getPlugin("office_exporter");
    		
    		if(exportPlugin.isTypeSupported(DialogAddressListPerformer.class))
    		{
    			DialogAddressListPerformer officeExporter = (DialogAddressListPerformer) exportPlugin.getImplementationFor(DialogAddressListPerformer.class);
    			
    			if(pData != null)
    			{
    				if(pData instanceof MailBatch)
    				{
    					System.out.println("Action: "+pAction);
    					
    					MailBatch mailBatch = (MailBatch)pData;
    					
    					try
    					{
    						officeExporter.setAddresses(mailBatch.getChannelAddresses(MailBatch.CHANNEL.FAX));
    					}
    					catch(Exception pExcp)
    					{
    						logger.warningSilent(pExcp.getMessage());
    						// TODO I18n
    						return "An error occured during office-export: "+pExcp.getLocalizedMessage();
    					}
    				}
    				else if(pData instanceof Address)
    				{
    					NonPersistentAddresses addresses = new NonPersistentAddresses();
    					addresses.addAddress((Address)pData);
    					officeExporter.setAddresses(addresses);
    				}
    				
    				officeExporter.execute();
    			}
    			else
    				return "No Addresses selected";
    		}
    		else
    			return "Office-Plugin does not support the type: "+DialogAddressListPerformer.class.getName();
    	}
    	else
    		return "Office-Plugin not available. Ask you system-administrator to install it.";
    	
    	return null;
    }

    protected String runCommand(String cmd) {
        try {
            ActionManager.logger.fine("[ConfigurableApplicationStarter] Kommando: " + cmd);
            Process p = Runtime.getRuntime().exec(cmd);
        } catch (IOException ioe) {
            ActionManager.logger.log(Level.WARNING, "ConfigurableApplicationStarter: Fehler beim Starten des Programms", ioe);
            return "Fehler beim Starten des Programms";
        }
        return null;
    }

    private final static String asString(Object o) {
        return o == null ? null : o.toString();
    }

}
