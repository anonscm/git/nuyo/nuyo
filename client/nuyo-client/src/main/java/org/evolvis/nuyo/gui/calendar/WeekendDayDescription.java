/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

/**
 * @author niko
 *
 */
public class WeekendDayDescription extends DayDescription
{
  public WeekendDayDescription()
  {
    super(DayDescription.WEEKEND);
  }
}
