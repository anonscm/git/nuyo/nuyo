/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * @author niko
 */
public class TarentWidgetIntegerField extends JTextField implements TarentWidgetInterface
{
  private IntegerDocument   m_oIntegerDocument;
  private boolean           m_iIsLengthRestricted;


  public TarentWidgetIntegerField()
  {
    super();
    m_oIntegerDocument = new IntegerDocument();
    this.setDocument(m_oIntegerDocument);    
    this.getDocument().addDocumentListener(new DocumentChangedListener());
  }

  public TarentWidgetIntegerField(int preset)
  {
    super();
    m_oIntegerDocument = new IntegerDocument();
    this.setDocument(m_oIntegerDocument);    
    this.setText(Integer.toString(preset));
    this.getDocument().addDocumentListener(new DocumentChangedListener());
  }

  
  private class DocumentChangedListener implements DocumentListener
  {
    private Object m_oValue;
    
    public DocumentChangedListener()
    {
      m_oValue = getData();
    }
    
    private void checkChanges()
    {
      if (!(getData().equals(m_oValue))) fireValueChanged(getData());
      m_oValue = getData();
    }

    public void changedUpdate(DocumentEvent e)
    {
      checkChanges();
    }

    public void insertUpdate(DocumentEvent e)
    {
      checkChanges();
    }

    public void removeUpdate(DocumentEvent e)
    {
      checkChanges();
    }
  }
  
  

	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    int i = normalize(data);    
    this.setText((new Integer(i)).toString());
	}


	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
    try
    {
		  return Integer.decode(this.getText());
    }
    catch(NumberFormatException nfe)
    {
      return new Integer(0);
    }
	}

  public int getIntegerValue() 
  {
    try
    {
      return Integer.decode(this.getText()).intValue();
    }
    catch(NumberFormatException nfe)
    {
      return(0);
    }     
  }

  public void setIntegerValue(int i) 
  {
    this.setText((new Integer(i)).toString());
  }

  public boolean isEqual(Object data)
  {
    return ((Integer.decode(this.getText()).intValue()) == normalize(data));    
  }


  private int normalize(Object data)
  {
    if (data instanceof String)
    {
      return Integer.decode((String)data).intValue();
    }
    else if (data instanceof Integer)
    {
      return(((Integer)data).intValue());
    }
    else if (data instanceof Float)
    {
      return(Math.round((((Float)data).floatValue())));
    }
    else if (data instanceof Boolean)
    {
      return( (((Boolean)data).booleanValue()) ? 1 : 0);
    }
    return(0);
  }


  public void setLengthRestriction(int maxlen)
  {
    if (maxlen > 0)
    {
      m_iIsLengthRestricted = true;
      m_oIntegerDocument.setLengthRestriction(maxlen);
    }
    else
    {
      m_iIsLengthRestricted = false;
      m_oIntegerDocument.setLengthRestriction(0);
    }
  }

  public boolean isLengthRestricted()
  {
    return(m_iIsLengthRestricted); 
  }

  public JComponent getComponent()
  {
    return(this);
  }
  
  public void setWidgetEditable(boolean iseditable)
  {
    this.setEditable(iseditable);
    if (!iseditable) this.setBackground(Color.WHITE);    
  }


  public class IntegerDocument extends PlainDocument
  {
    private int     m_iMaxChars;
    
    public IntegerDocument()
    {
      m_iMaxChars = 0;
    }
    
    public IntegerDocument(int maxchars)
    {
      m_iMaxChars = maxchars;
    }
    
    public void setLengthRestriction(int maxchars)
    {
      m_iMaxChars = maxchars;
    }

    
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
    {
      if (str == null) 
      {
        return;
      }
              
      String inttext = "";
      for (int i = 0; i < str.length(); i++) 
      {
        if (Character.isDigit(str.charAt(i)))
        {
          if (m_iMaxChars != 0)
          {
            if (super.getLength() < m_iMaxChars)            
            //if (inttext.length() < m_iMaxChars)
            {
              inttext = inttext + (str.charAt(i));
            }
          }       
          else
          {
            inttext = inttext + (str.charAt(i));
          }
        }
      }
      super.insertString(offs, inttext, a);          
    }
  }

  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  private void fireValueChanged(Object newvalue)
  {
    Iterator it = m_oWidgetChangeListeners.iterator();
    while(it.hasNext())
    {
      TarentWidgetChangeListener listener = (TarentWidgetChangeListener)(it.next());
      listener.changedValue(newvalue);
    }
  }
  

}
