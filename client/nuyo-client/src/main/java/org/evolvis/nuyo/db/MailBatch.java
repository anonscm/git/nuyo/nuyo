/* $Id: MailBatch.java,v 1.3 2006/09/01 19:48:20 aleksej Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink.
 *  
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a mass mail order (in german: 'Serien-Versandauftrag').
 * A mail order can be executed via three channels: email, fax and post.
 *  
 * On DB abstraction level any action on a mail batch will be called as a 'tAction'.
 * At the moment there is only one 'tAction' type: a Mail Order (Versandauftrag).
 * 
 * @author mikel
 */
public abstract class MailBatch implements HistoryElement {
    /**
     * Diese Variablen halten die Informationen zu dem Versandauftrag.
     */
    protected boolean filtered;
    protected int id, mergeMail, mergeFax, mergePost, notAssigned, total, sentMail, sentFax, sentPost;
    protected String name, userId, userName, dateTime, recSrc, filter;

    public final static int BATCH_NEW_ID = 0;    

    /**
     * L�scht diesen Versandauftrag.
     * 
     * @throws ContactDBException bei Problemen in der Datenbank
     */
    public abstract void delete() throws ContactDBException;
    
    /**
     * Diese Klasse dient der typensicheren und vergleichsschnellen
     * Aufz�hlung der Kan�le.
     */
    public final static class CHANNEL {
        public final static CHANNEL MAIL = new CHANNEL("Mail");
        public final static CHANNEL FAX = new CHANNEL("Fax");
        public final static CHANNEL POST = new CHANNEL("Post");
        public final static CHANNEL NOT_ASSIGNED = new CHANNEL("NotAssigned");
        public final static CHANNEL ALL = new CHANNEL("");
        public final static CHANNEL TOTAL = ALL;
        
        private final String channel;
        
        private CHANNEL(String channel) {
            this.channel = channel;
        }
        
        public String getChannelName() {
            return channel;
        }
        
        public String toString() {
            return channel;
        }
        
        public boolean equals(Object other) {
            return this == other;
        }
    }
        
    /**
     * Ordnet Adressen dem �bergebenen Kanal zu.
     * 
     * @throws ContactDBException bei Problemen in der Datenbank
     */
    public abstract void setChannel(CHANNEL Channel) throws ContactDBException;
    
    /**
     * Gibt Adressen dem �bergebenen Kanal frei.
     * 
     * @throws ContactDBException bei Problemen in der Datenbank
     */
    public abstract void resetChannel(CHANNEL Channel) throws ContactDBException;

    /**
     * F�hrt den �bergebenen Kanal aus.
     * 
     * @throws ContactDBException bei Problemen in der Datenbank
     */
    public abstract void doChannel(CHANNEL Channel) throws ContactDBException;
    
    /**
     * Diese Methode liefert die Adressen zum angegeben Kanal.
     * 
     * @throws ContactDBException bei Problemen in der Datenbank
     */
    public abstract Addresses getChannelAddresses(CHANNEL Channel) throws ContactDBException;
    
    /**
     * Liefert die ID des Versandauftrags.
     * @return ID des Auftrags.
     */
    public int getId() {
        return id;
    }

    /**
     * Liefert den Bezeichner des Auftrags.
     * @return Name des Auftrags.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Setzt den Bezeichner des Auftrags.
     * @param name Name des Auftrags.
     */
    public void setName(String name) throws ContactDBException {
        this.name = name;
    }

    /**
     * Liefert die ID des beauftragenden Benutzers.
     * @return ID des Benutzers.
     */
    public String getInitiatorID() {
        return userId;
    }

    /**
     * Liefert den Namen des beauftragenden Benutzers.
     * @return Namen des Benutzers.
     */
    public String getInitiatorName() {
        return userName;
    }
    
    /**
     * Liefert den Timestamp des Auftrags.
     * @return Timestamp des Auftrags in String-Form.
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Returns the recSrc.
     * @return String
     */
    public String getRecSrc() {
        return recSrc;
    }

    /**
     * Zeigt an, ob die Adressauswahl des Auftrags gefiltert ist. Wenn sie
     * es ist, liefert {@link #getFilter() getFilter()} den Filter.
     * @return das Filterflag des Auftrags.
     */
    public boolean isFiltered() {
        return filtered;
    }

    /**
     * Liefert den Filter des Auftrags; nur relevant, wenn der Auftrag als
     * {@link #isFiltered() gefiltert} markiert ist.
     * @return eingestellter Filter als String.
     */
    public String getFilter() {
        return filter;
    }

    /**
     * Liefert die Anzahl der als Fax zu versendenden Exemplare.
     * @return Anzahl der Fax-Auftr�ge.
     */
    public int getMergeFax() {
        return mergeFax;
    }

    /**
     * Liefert die Anzahl der als Email zu versendenden Exemplare.
     * @return Anzahl der Email-Auftr�ge.
     */
    public int getMergeMail() {
        return mergeMail;
    }

    /**
     * Liefert die Anzahl der als Brief zu versendenden Exemplare.
     * @return Anzahl der Brief-Auftr�ge.
     */
    public int getMergePost() {
        return mergePost;
    }

    /**
     * Liefert die Anzahl der nicht einer Versandart zugewiesenen Adressen.
     * @return Anzahl der nicht zugewiesenen Adressen.
     */
    public int getNotAssigned() {
        return notAssigned;
    }

    /**
     * Liefert den Status der als Fax zu versendenden Exemplare.
     *
     * @return {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *  {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet, aber nicht
     *  ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public int getSentFax() {
        return sentFax;
    }

    /**
     * Setzt den Status der als Fax zu versendenden Exemplare.
     *
     * @param sentFax {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *  {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet, aber nicht
     *  ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public void setSentFax(int sentFax) throws ContactDBException {
        this.sentFax = sentFax;
    }

    /**
     * Liefert den Status der als Email zu versendenden Exemplare.
     *
     * @return {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *  {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet, aber nicht
     *  ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public int getSentMail() {
        return sentMail;
    }

    /**
     * Setzt den Status der als Email zu versendenden Exemplare.
     *
     * @param sentMail {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *  {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet, aber nicht
     *  ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public void setSentMail(int sentMail) throws ContactDBException {
        this.sentMail = sentMail;
    }

    /**
     * Liefert den Status der als Brief zu versendenden Exemplare.
     *
     * @return {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *  {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet, aber nicht
     *  ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public int getSentPost() {
        return sentPost;
    }

    /**
     * Setzt den Status der als Brief zu versendenden Exemplare.
     *
     * @param sentPost {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *  {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet, aber nicht
     *  ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public void setSentPost(int sentPost) throws ContactDBException {
        this.sentPost = sentPost;
    }

    /**
     * Liefert die Gesamtanzahl Zieladressen.
     * @return Gesamtanzahl Versandadressen.
     */
    public int getTotal() {
        return total;
    }

    /**
     * Ist Wert der Attribute {@link #getSentFax() SentFax},
     * {@link #getSentMail() SentMail} und {@link #getSentPost() SentPost},
     * wenn diese Versandart noch nicht zugewiesen wurde.
     * 
     * @see #ASSIGNED_NOT_SENT
     * @see #SENT
     */    
    public final static int NOT_ASSIGNED = 0;

    /**
     * Ist Wert der Attribute {@link #getSentFax() SentFax},
     * {@link #getSentMail() SentMail} und {@link #getSentPost() SentPost},
     * wenn diese Versandart zugewiesen, aber nicht versendet wurde.
     * 
     * @see #NOT_ASSIGNED
     * @see #SENT
     */    
    public final static int ASSIGNED_NOT_SENT = 1;

    /**
     * Ist Wert der Attribute {@link #getSentFax() SentFax},
     * {@link #getSentMail() SentMail} und {@link #getSentPost() SentPost},
     * wenn diese Versandart zugewiesen und versendet wurde.
     * 
     * @see #NOT_ASSIGNED
     * @see #ASSIGNED_NOT_SENT
     */    
    public final static int SENT = 2;

    //
    // Schnittstelle HistoryElement
    //
    /**
     * Diese Methode liefert den UTC-Zeitpunkt, an dem dieses HistoryElement
     * einzutragen ist.
     * 
     * @return UTC-History-Zeitpunkt.
     * @see HistoryElement#getHistoryDate()
     */
    public Date getHistoryDate() {
        try {
            return batchDateFormatter.parse(getDateTime());
        } catch (ParseException e) {
            return null;
        }
    }
    
    private final static SimpleDateFormat batchDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
}
