package org.evolvis.nuyo.plugins.calendar;

public interface CalendarClickedListener {
    
    public void clickedCalendar(Integer calendarID);
}
