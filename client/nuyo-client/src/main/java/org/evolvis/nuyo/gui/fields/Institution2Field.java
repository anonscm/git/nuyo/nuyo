/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextAreaField;


/**
 * @author niko
 */
public class Institution2Field extends GenericTextAreaField
{
  public Institution2Field()
  {
    super("INSTITUTION2", AddressKeys.INSTITUTION2, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_Institution2_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_Institution2", 60);
  }
}
