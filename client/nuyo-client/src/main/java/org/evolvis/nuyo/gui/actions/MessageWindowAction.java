package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.logging.LogWindow;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;


public class MessageWindowAction extends AbstractGUIAction {

	private static final long	serialVersionUID	= 4182605491017897611L;
	private static final TarentLogger log = new TarentLogger(MessageWindowAction.class);

	
	public void actionPerformed(ActionEvent e) {
		
		LogWindow logWindow =  ApplicationServices.getInstance().getActionManager().getLogger().getLogWindow();
		
		if (logWindow != null) {
			
			logWindow.showWindow(true);
		}
		else
			log.warning("GUI_MessageWindowAction_Init_Error", "MessageWindowAction has no LogWindow reference.");
	}
}
