/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextAreaField;


/**
 * @author niko
 *
  */
public class InstitutionField extends GenericTextAreaField
{
  public InstitutionField()
  {
    super("INSTITUTION", AddressKeys.INSTITUTION, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Institution_ToolTip", "GUI_MainFrameNewStyle_Standard_Institution", 300);
  }
}
