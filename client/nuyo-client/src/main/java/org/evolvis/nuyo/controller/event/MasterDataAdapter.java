/* $Id: MasterDataAdapter.java,v 1.2 2006/06/06 14:12:14 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.event;

import java.util.Map;

import org.evolvis.nuyo.db.UserGroup;


/**
 * Dieser Adapter liefert eine Dummy-Implementierung von
 * {@link org.evolvis.nuyo.controller.event.MasterDataListener MasterDataListener}.
 * 
 * @author mikel
 */
public class MasterDataAdapter implements MasterDataListener {
    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#categoryAdded(String, String, String)
     */
    public void categoryAdded(String categoryID, String categoryName, String categoryDescription) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#categoryEdited(java.lang.String, java.lang.String, java.lang.String)
     */
    public void categoryEdited(String categoryID, String categoryName, String categoryDescription) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#categoryRemoved(java.lang.String)
     */
    public void categoryRemoved(String categoryID) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#categoryAssigned(String, String)
     */
    public void categoryAssigned(String userID, String categoryID) {
    }

    /**
     * Diese Methode teilt mit, dass Zugang zu einer Kategorie einem Benutzer
     * genommen worden ist.
     * 
     * @param userID ID des Benutzers
     * @param categoryID ID der Kategorie
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#categoryDeassigned(String, String)
     */
    public void categoryDeassigned(String userID, String categoryID) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#standardCategoryAssigned(String, String)
     */
    public void standardCategoryAssigned(String userID, String categoryID) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#subCategoryAdded(String, String, String, String, String)
     */
    public void subCategoryAdded(
        String categoryID,
        String id,
        String name1,
        String name2,
        String name3) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#subCategoryEdited(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public void subCategoryEdited(String categoryID, String id, String name1, String name2, String name3) {
    }

    /**
     * Diese Methode teilt mit, dass eine Unterkategorie aus dem System
     * gel�scht worden ist.
     * 
     * @param categoryID ID der zugeh�rigen Oberkategorie
     * @param id ID der gel�schten Unterkategorie
     */
    public void subCategoryRemoved(String categoryID, String id) {
    }

    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#userGroupAdded(org.evolvis.nuyo.db.UserGroup)
     */
    public void userGroupAdded(UserGroup newGroup) {
    }
    
    /**
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#userAdded(String, String, String, boolean, boolean, Map)
     */
    public void userAdded(String userID, String lastName, String firstName, String email) {
    }

    /**
     * Diese Methode teilt mit, dass ein Benutzer aus dem System
     * entfernt worden ist.
     * 
     * @param userID ID des Benutzers
     * @see org.evolvis.nuyo.controller.event.MasterDataListener#userRemoved(String)
     */
    public void userRemoved(String userID) {
    }
    
    
    public void userChanged(String userID, String lastName, String firstName, String email) {
    }


}
