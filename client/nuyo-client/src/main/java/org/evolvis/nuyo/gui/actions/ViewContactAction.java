package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Activates a view of the current contact.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ViewContactAction extends AbstractGUIAction {

    private static final long serialVersionUID = 2742855577990680964L;
    private static final TarentLogger logger = new TarentLogger( ViewContactAction.class );
    private ControlListener mainFrame;

    public void actionPerformed(ActionEvent e) {
        if(mainFrame != null) {
            mainFrame.activateTab(MainFrameExtStyle.TAB_CONTACT);
        } else logger.warning(getClass().getName() + "is not initialized.");
    }
    
    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();
        setEnabled(mainFrame.isTabEnabled(MainFrameExtStyle.TAB_CONTACT));
    }
}
