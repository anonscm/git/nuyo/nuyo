/* $Id: LoginInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * Created on 07.10.2003
 */
package org.evolvis.nuyo.messages;

/**
 * Diese Informationsereignisklasse wird benutzt, um �ber einen Wechsel des
 * angemeldeten Benutzers zu informieren.
 *   
 * @author mikel
 */
public class LoginInfo extends MasterInfo {
    /**
     * @param source Ereignisquelle
     * @param account Login-Konto-Name
     * @param password Passwort
     */
    public LoginInfo(Object source, String account, String password) {
        super(source);
        this.account = account;
        this.password = password;
    }

    /*
     * Klasse Object
     */
    public String toString() {
        return super.toString() + "[account: " + account + ']';
    }

    /*
     * Getter und Setter
     */
    /**
     * @return Login-Konto-Name
     */
    public String getAccount() {
        return account;
    }

    /**
     * @return Passwort
     */
    public String getPassword() {
        return password;
    }

    /*
     * gesch�tzte Variablen
     */
    protected String account;
    protected String password;
}
