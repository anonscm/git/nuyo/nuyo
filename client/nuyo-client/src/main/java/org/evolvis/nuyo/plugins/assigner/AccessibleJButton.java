package org.evolvis.nuyo.plugins.assigner;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;


/**
 * This Class extends the JButton so that it is possible to set the Mnemorics through an & in the Label.
 * 
 * 
 * @author Sebastian Mancke, tarent GmbH
 *
 */
class AccessibleJButton extends JButton {
    
    public AccessibleJButton(String text) {
        super();
        setText(text);
    }

    public AccessibleJButton(String text, Icon icon) {
        super(icon);
        setText(text);
    }

    public AccessibleJButton(Icon icon) {
        super(icon);
    }

    public AccessibleJButton(Action action) {
        super(action);
    }
    
    public void setText(String text) {
        int index;             
        if (-1 != (index = text.indexOf('&'))) {
            StringBuffer sb = new StringBuffer(text);
            sb.deleteCharAt(index);
            setMnemonic(text.charAt(index+1));
            
            super.setText(sb.toString());            
        } else
            super.setText(text);
    }
}