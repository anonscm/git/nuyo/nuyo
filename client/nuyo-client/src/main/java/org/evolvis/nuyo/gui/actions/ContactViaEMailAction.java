package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ApplicationStarter;
import org.evolvis.nuyo.logging.TarentLogger;


/**
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class ContactViaEMailAction extends AbstractConsignmentDependentAction
{

	private static final long	serialVersionUID	= 1425057359684033032L;
	private static final TarentLogger logger = new TarentLogger(ContactViaEMailAction.class);

	public void actionPerformed(ActionEvent e)
	{
		ApplicationServices.getInstance().getActionManager().userRequestPost(ApplicationStarter.POST_EMAIL);
	}
}