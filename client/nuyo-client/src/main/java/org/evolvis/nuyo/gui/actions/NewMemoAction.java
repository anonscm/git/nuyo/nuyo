package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;



public class NewMemoAction extends AbstractContactDependentAction {

	private static final long	serialVersionUID	= 3851946322204265938L;


    public void actionPerformed(ActionEvent e) {
        ApplicationServices.getInstance().getActionManager().userRequestCreateContactEntryDialog(null, null, null, 0, false, 0, 0, 0, 0, null, null);
    }
}
