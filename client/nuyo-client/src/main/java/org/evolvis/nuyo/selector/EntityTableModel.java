package org.evolvis.nuyo.selector;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.swing.utils.SwingIconFactory;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.datahandling.entity.EntityListEvent;
import de.tarent.commons.datahandling.entity.EntityListListener;

public class EntityTableModel extends AbstractTableModel implements EntityListListener {
	
	public final static TarentLogger logger = new TarentLogger(Logger.getLogger(EntityTableModel.class.getName()));
	public static final String STD_FIELD_LABEL = "labelfn";
	
	private EntityList entities;
	private Map icons;
	private String[] columnNames;
	private ImageIcon iconAddressEntry;	
	
	public EntityTableModel(EntityList list){
		this();
		entities = list;       
	}
	
	public EntityTableModel(){
		
		entities = new NonPersistentAddresses();
		icons= new HashMap();
		columnNames = new String[2];
		iconAddressEntry = (ImageIcon)ApplicationServices.getInstance().getIconFactory().getIcon(SwingIconFactory.USER);	
	}
	
	public int getColumnCount() {
		return 2;
	}
	
	public int getRowCount() {
		return entities.getSize();
	}
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (columnIndex==0) {
			return (Object) icons.get(((org.evolvis.nuyo.db.Addresses)entities).get(rowIndex));
		} else if (columnIndex==1){
			try {
				return ((Entity)entities.getEntityAt(rowIndex)).getAttribute(STD_FIELD_LABEL);
			} catch (EntityException e) {
				logger.warning("Fehler beim Laden eines Datensatzes");
			}
		}
		return null;
	}
	/**
	 * this method adds a row to the TableModel
	 * @param entity
	 */
	public void addRow(Entity entity){
		if (!entities.contains(entity)){
			entities.addEntity(entity);
			icons.put(entity,iconAddressEntry);
		}
	}
	/**
	 * This method updates the data of the table model 
	 * @param list
	 */
	public void setData(EntityList list){
		entities = list;
		updateIcons();
	}
	
	/**
	 * This method removes a row with a given index
	 * @param index the index of the row to be removed
	 */
	public void removeRow(int index){
		icons.remove(entities.getEntityAt(index));
		entities.removeEntity(entities.getEntityAt(index));
		logger.finer("entferne Zeile "+ index);
	}
	
	/**
	 * This method removes several rows at once. 
	 * @param rowsToRemove
	 */
	public void removeRows(int[] rowsToRemove){		
		Entity[] entityArray = new Entity[rowsToRemove.length];
		//get entities by index
		logger.finer("entferne Zeilen ");
		for (int i=0;i<rowsToRemove.length;i++){
			entityArray[i]=(Entity)entities.getEntityAt(rowsToRemove[i]);
		}
		for (int i=0;i<entityArray.length;i++){
			entities.removeEntity(entityArray[i]);
			icons.remove(entityArray[i]);
		}		
	}
	
	/**
	 * removes all rows at once
	 *
	 */
	public void removeAllRows(){
		entities.clear();
	}
	
	/**
	 * this method updates the ArrayList that contains the icons
	 * 
	 */
	private void updateIcons(){
		icons.clear();
		
		for (int i =0; i<entities.getSize();i++){
			//TODO: unterscheidung von Addresse und SysUser
			icons.put(entities.getEntityAt(i),iconAddressEntry);
		}
	}
	
	public void entityListChanged(EntityListEvent e) {
		//TODO: fireTableDataChanged();
	}
	
	public String getColumnName(int colNum){
		if (colNum<columnNames.length) return (String)columnNames[colNum];
		return "";
	}
	public void setColumnName(int colNum, String name){
		if (colNum<columnNames.length) 
			columnNames[colNum]=name;
	}
	
	public Entity getEntity(int index){
		return (Entity)entities.getEntityAt(index);
	}
	
	public EntityList getEntityList(){
		return entities;
	}
	
	public boolean contains(Entity entity){
		return entities.contains(entity);
	}
	
	public int getRowOf(Entity entity){
		return entities.indexOf(entity);
		
	}
	
	//to display the icon as an icon and not as a String
	public Class getColumnClass(int c) {
		try {
			return getValueAt(0, c).getClass();
		} catch (NullPointerException npe) {
			return String.class;
		}
	}
	
}
