/*
 * Created on 09.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;

import org.evolvis.nuyo.controller.tarentUtil;


/**
 * @author niko
 *
 */
public class HTMLViewer
{
  private JEditorPane m_oEditorPane;
  private JFrame m_oFrame = null;
  private boolean m_bDeleteFileOnClose;
  private String m_sURL;

  public HTMLViewer(String filename)  
  {
    this(filename, filename, false);
  }  
  
  public HTMLViewer(String filename, String title)  
  {
    this(filename, title, false);
  }  
  
  public HTMLViewer(String filename, String title, boolean deletefileonclose)
  {
    
    m_bDeleteFileOnClose = deletefileonclose;
    
    try
    {    
      m_oEditorPane = new JEditorPane();
      m_oEditorPane.setEditable(false);
      URL helpURL =  tarentUtil.toURL(filename);
      //helpURL = new URL(filename);
      
      if  (helpURL == null)
      {
        shutdown();
        return;                  
      }
      
      if (helpURL != null) 
      {
        m_sURL = tarentUtil.toURLString(filename);
        try 
        {
          m_oEditorPane.setPage(helpURL);
        } 
        catch (IOException e) 
        {
          System.err.println("Attempted to read a bad URL: " + helpURL);
          shutdown();
          return;                  
        }
      } 
      else 
      {
        System.err.println("file not found: \"" + filename + "\"");
        shutdown();
        return;                  
      }
  
      //    Put the editor pane in a scroll pane.
      JScrollPane editorScrollPane = new JScrollPane(m_oEditorPane);
      editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      editorScrollPane.setPreferredSize(new Dimension(250, 145));
      editorScrollPane.setMinimumSize(new Dimension(10, 10));    
  
      m_oEditorPane.addHyperlinkListener(new Hyperactive());
                
      JPanel panel = new JPanel(new BorderLayout());
      panel.add(editorScrollPane, BorderLayout.CENTER);    
          
      m_oFrame = new JFrame(title);
      
      m_oFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      m_oFrame.addWindowListener(new ViewerWindowListener());
      m_oFrame.getContentPane().add(panel);
      m_oFrame.pack();      
      m_oFrame.setVisible(true);
    }
    catch(Exception e)
    {
      System.err.println("rendering of document failed.");      
      shutdown();
    }      
  }

  private void shutdown()
  {
    if (m_oFrame != null) m_oFrame.setVisible(false);
    if (m_bDeleteFileOnClose)
    {
      if (m_sURL != null)
      {
        File f = new File(m_sURL);
        f.delete();
      }
    }    
  }
  

  private class ViewerWindowListener extends WindowAdapter
  {
    public void windowClosing(WindowEvent e)
    {
      shutdown();
    }    
  }
  
  private class Hyperactive implements HyperlinkListener 
  {    
    public void hyperlinkUpdate(HyperlinkEvent e) 
    {
      if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) 
      {
        JEditorPane pane = (JEditorPane) e.getSource();
        if (e instanceof HTMLFrameHyperlinkEvent) 
        {
          HTMLFrameHyperlinkEvent  evt = (HTMLFrameHyperlinkEvent)e;          
          HTMLDocument doc = (HTMLDocument)pane.getDocument();          
          doc.processHTMLFrameHyperlinkEvent(evt);
        } 
        else 
        {
          try 
          {
            pane.setPage(e.getURL());
          } 
          catch (Throwable t) 
          {
            t.printStackTrace();
          }          
        }               
      }
    }
  }
  
}
