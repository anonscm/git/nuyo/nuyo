package org.evolvis.nuyo.gui.fields;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentFileChooser;
import org.evolvis.nuyo.controls.TarentWidgetButton;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.xana.swing.utils.SwingIconFactory;

/**
 * @author niko
 * @author aleksej (refactored all logic)
 * @author robert (refactored change events)
 */
public class PictureField extends ContactAddressField
{  
  private static final ImageIcon DUMMY_ICON = (ImageIcon)SwingIconFactory.getInstance().getIcon( "dummy_picture.gif" );
  private static TarentLogger logger = new TarentLogger(PictureField.class);

  private int maxHeight = 45 * 2;
  private int maxWidth  = 35 * 2;
  
  private final long max_file_size = 500000;
  
  private Object addressNr;
  private ImageIcon picture;

  private File pictureFile;
  private String pictureName;
  private byte[] pictureBytes;
  private PicturePreview previewFrame;
  

  
  private TarentWidgetLabel iconLabel; 
  private TarentWidgetLabel infoLabel;
  private TarentWidgetButton changeButton;
  private TarentWidgetButton deleteButton;
  private EntryLayoutHelper picturePanel;
  
  public String getFieldName()
  {
    return "Bild";    
  }
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public String getFieldDescription()
  {
    return "Ein Feld zur Anzeige eines Bildes";
  }
  
  
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if(picturePanel == null) picturePanel = createPanel(widgetFlags);
    return picturePanel;
  }
  
  
  public void postFinalRealize()
  {    
  }
  
  public void setData(PluginData data)
  {
    pictureFile = null;
    pictureName = (String) data.get(AddressKeys.PICTUREEXPRESSION);
    logger.fine("Bildname: " + pictureName);
    
    addressNr = data.get( AddressKeys.ADRESSNR );
    pictureBytes = (byte[]) data.get(AddressKeys.PICTURE);
    picture = pictureBytes != null ? new ImageIcon( pictureBytes ) : null;

    if(picture == null) {//reset picture to DUMMY
        logger.info("ImageIcon: not available -> setting dummy...");
        if(DUMMY_ICON == iconLabel.getIcon()) {
            logger.info("already dummy -> return");
            return;
        }//is not a dummy -> set dummy...
    }
    else {
        logger.info("ImageIcon: available -> loading...");
        if(DUMMY_ICON == picture) logger.warningSilent("[!] it's a dummy ImageIcon: error in logic (for dummy use case ImageIcon should be null) -> optimize please");
        else {
            if(picture.equals(iconLabel.getIcon())) {
                logger.fine("current picture is the same -> loading canceled.");
                return;
            }//is a new image -> load...
        }
    }
    setIcon( picture );
  }

  /**
   * Wandelt Datei in Bytearray um
   * 
   * @param file
   * @return byte[] is an array of pixels or <code>null</code> if error occured.
   */
  private byte[] getBytesFromFile(File file) {
      if ( file != null ) {
          FileInputStream is = null;
          byte[] result = null;
          try {
              is = new FileInputStream( file );
              long length = file.length();
              if ( length > Integer.MAX_VALUE ) {
                  logger.warningSilent("Picture file is too large!" ); // long muss in int umgewandelt werden
              }
              result = new byte[(int) length];
              // File in byte[] einlesen
              int offset = 0;
              int numRead = 0;
              while ( offset < result.length && ( numRead = is.read( result, offset, result.length - offset ) ) >= 0 ) {
                  offset += numRead;
              }
              // Test ob file ganz eingelsen wurde
              if ( offset < result.length ) {
                  logger.warning(Messages.getFormattedString("GUI_PictureField_LoadingPictureError", file.getAbsolutePath()));
              }
          } catch (FileNotFoundException e) {
              logger.warning(Messages.getFormattedString("GUI_PictureField_FileNotFoundException", file.getAbsolutePath()), e);
          } catch (IOException e) {
              logger.warning(Messages.getString("GUI_PictureField_IOException"), e);
          } finally {
              if(is != null)
                  try {
                      is.close();
                  }catch ( IOException e ) {
                  }
          }

          return result;
      }

      return null;
  }
  
  //load a new picture or reset it (if null as parameter)
  private void setIcon(ImageIcon newImageIcon) {
      boolean isDummyPicture = false;
      StringBuilder info = new StringBuilder(50).append("<html>");
      ImageIcon loadedIcon = null;
      if ( newImageIcon != null ) {//load a new one
          int counter = 0;
          String pleaseWait = Messages.getString("Message_PLEASE_WAIT");
          while ( newImageIcon.getImageLoadStatus() == java.awt.MediaTracker.LOADING ) {
              infoLabel.setText(pleaseWait+ " " + counter++ );
          }
          if ( newImageIcon.getImageLoadStatus() == java.awt.MediaTracker.COMPLETE ) {
              loadedIcon = newImageIcon;
              picture = newImageIcon;
              info.append("<b>").append(Messages.getString("GUI_PictureField_Info_Size")).append("</b> ").append(picture.getIconWidth()).append(" x ").append(picture.getIconHeight()).append("<br>");
          }
          else {
              info.append("<font color=\"#ff0000\">").append("GUI_PictureField_Info_LoadingErrorOccured").append("</font><br>");
          }
      } else {//do reset
          if(picture == null && DUMMY_ICON == iconLabel.getIcon()) return;//nothing to reset
      }
      
      if(loadedIcon == null) {//set dummy picture if failed or reset action
          isDummyPicture = true;
          reset();
          loadedIcon = DUMMY_ICON;
          info.append("<font color=\"#000000\">").append(Messages.getString("GUI_PictureField_Info_NO_PICTURE")).append("</font><br>");
          iconLabel.setToolTipText(Messages.getString("GUI_PictureField_Info_NO_PICTURE"));
      }

      ImageIcon scaledIcon = getScaledIcon( loadedIcon );

      if ( !isDummyPicture ) {
          info.append("<b>").append(Messages.getString("GUI_PictureField_Info_ScaledSize")).append("</b> ").append(scaledIcon.getIconWidth()).append(" x ").append(scaledIcon.getIconHeight()).append("<br>");
          iconLabel.setToolTipText(Messages.getString("GUI_PictureField_Info_ClickForPreview"));
      }

      info.append("</html>");
      iconLabel.setIcon( scaledIcon );
      infoLabel.setText( info.toString() );
      Dimension size = new Dimension( scaledIcon.getIconWidth(), scaledIcon.getIconHeight() );
      iconLabel.setMinimumSize( size );
      iconLabel.setSize( size );
  }
  
  private ImageIcon getScaledIcon( ImageIcon loadedIcon ) {
      ImageIcon adjustedIcon = null;
      int height = loadedIcon.getIconHeight();
      int width = loadedIcon.getIconWidth();
      if ( height > maxHeight ) {
          if ( width > maxWidth ) {
              logger.fine("picture: to big width and height: is = " + width + " x " + height );
              logger.fine("picture: to big width and height: max = " + maxWidth + " x " + maxHeight );
              if ( height != 0 ) {
                  double aspect = ( (double) width ) / ( (double) height );
                  logger.fine("aspect=" + aspect );
                  adjustedIcon = new ImageIcon( loadedIcon.getImage().getScaledInstance( (int) ( ( (double) maxHeight ) * aspect ), maxHeight,Image.SCALE_FAST ) );
                  logger.fine("newsize = " + (int) ( ( (double) maxHeight ) * aspect ) + " x " + maxHeight );
              }
          }
          else {
              logger.fine("picture: too big height: scaling to " +  maxHeight);
              adjustedIcon = new ImageIcon( loadedIcon.getImage().getScaledInstance( -1, maxHeight, Image.SCALE_FAST ) );
          }
      }
      else {
          if ( width > maxWidth ) {
              logger.fine("picture: too big width: scaling to " + maxWidth);
              adjustedIcon = new ImageIcon( loadedIcon.getImage().getScaledInstance( maxWidth, -1, Image.SCALE_FAST ) );
          }
          else {
              logger.fine("picture: no scaling required" );
              adjustedIcon = loadedIcon;
          }
      }
      return adjustedIcon;
  }

  
  public void getData(PluginData data)  {
      if ( pictureFile != null ) {
          logger.fine( "picturefile = " + pictureFile.getAbsolutePath() );
          data.set(AddressKeys.PICTURE, getBytesFromFile( pictureFile ) );
          data.set(AddressKeys.PICTUREEXPRESSION, pictureName);
      }
      else if ( pictureBytes == null ) {
          logger.info("deleting picture bytes in a DB");
          data.set(AddressKeys.PICTURE, null );
          data.set(AddressKeys.PICTUREEXPRESSION, null );
      }
  }

  public boolean isDirty(PluginData data) {
      //get bytes array of a picture from DB
      Object dbContent = data.get( AddressKeys.PICTURE );
      if ( picture == null ) {
          return dbContent != null;
      }
      else {
          return dbContent == null || !Arrays.equals(pictureBytes, (byte[]) dbContent);
      }
  }

  public void setEditable(boolean iseditable)
  {
    changeButton.setEnabled(iseditable);
    deleteButton.setEnabled(iseditable);
  }

  public String getKey()
  {
    return "PICTURE";
  }

  public void setDoubleCheckSensitive(boolean issensitive)
  {
  }
  
  private void deletePicture() {
      reset();
      setIcon(null);
      /**
       * The pictureBytes will be removed from DB as soon as getData() will be invoked, 
       * i.e. after:
       * 1) saving
       * 2) doubles checking
       * 3) edit canceled
       * 4) other contact selected, previous, next, last, first
       * 5) deleting any contact
       * 6) correction request
       * 7) getDirtyFields from AdvancedPanelInserter
       */
  }
  
  /* sets DB parameters to null:
   * will be delegated to a DB as soon as getData() will be invoked.*/
  private void reset() {
    picture = null;
    pictureFile = null;
    pictureName = null;      
    pictureBytes = null;
  }

  private EntryLayoutHelper createPanel(String widgetFlags)
  {
    iconLabel = new TarentWidgetLabel();
    iconLabel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));

    iconLabel.addMouseListener(new IconMouseListener());
    
    changeButton = new TarentWidgetButton(Messages.getString("Button_Change"));
    changeButton.setToolTipText(Messages.getString("GUI_PictureField_ChangeButton_Tooltip"));
    changeButton.addActionListener(new ChangeButtonClicked());
    changeButton.setEnabled(false);

    deleteButton = new TarentWidgetButton(Messages.getString("Button_Delete"));
    deleteButton.setToolTipText(Messages.getString("GUI_PictureField_DeleteButton_Tooltip"));
    deleteButton.addActionListener(new DeleteButtonClicked());
    deleteButton.setEnabled(false);
    
    infoLabel = new TarentWidgetLabel("");
    
    setIcon(null);
    
    return(new EntryLayoutHelper(new TarentWidgetInterface[] {
                                                                new TarentWidgetLabel("Bild:"),
                                                                iconLabel,
                                                                infoLabel,
                                                                changeButton,
                                                                deleteButton },
                                                                widgetFlags));
  }

  
  private class IconMouseListener extends MouseAdapter
  {
    public void mouseClicked(MouseEvent e)
    {
      if (picture != null)
      {
        if (e.getClickCount() >= 1)
        {
          if(previewFrame == null) previewFrame = new PicturePreview();
          previewFrame.reloadImage(picture);
          previewFrame.setVisible(!previewFrame.isVisible());  
        }
      }
    }

    public void mouseEntered(MouseEvent e)
    {
      if (picture != null)
      {
        iconLabel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(0x00, 0xff, 0x00), new Color(0x00, 0x88, 0x00)));
      }
      else
      {
        iconLabel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(0xff, 0x00, 0x00), new Color(0x88, 0x00, 0x00)));
      }
    }

    public void mouseExited(MouseEvent e)
    {
      iconLabel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
    }
  }

  private class ChangeButtonClicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      TarentFileChooser chooser = new TarentFileChooser();
      int returnval = chooser.showOpenDialog(getGUIListener().getFrame());
      if (returnval == JFileChooser.APPROVE_OPTION) {  
        File file = chooser.getSelectedFile();
        if (file != null && file.exists()) {
            if (file.length() <= max_file_size) {
              if (file.canRead()) {
                String filename = "file:" + file.getAbsolutePath();             
                ImageIcon loadedIcon = loadExternalIcon(filename);
                if (loadedIcon != null) {
                  pictureFile = file; // file global speichern (wird bei Abruf in byte[] umgewandelt)
                  pictureName = file.getName(); // Dateinamen fr PICTUREEXPRESSION speichern
                  picture = loadedIcon;
                  setIcon(picture);
                }
              }
            } else getControlListener().showInfo(Messages.getFormattedString("GUI_PictureField_TooLargePictureError", ""+max_file_size));
        }        
      }
    }
  }
  
  private class DeleteButtonClicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      deletePicture();
    }
  }
  
  private ImageIcon loadExternalIcon(String filename)
  {
    try
    {
      URL imageurl = new URL(filename);
      return new ImageIcon(imageurl);
    } 
    catch (MalformedURLException e) {
      logger.warningSilent("[!] couldn't load an image file: " + e.getMessage());
      return null;
    }    
  }
  

private class PicturePreview extends JFrame {
      private final int heightoffset = 40;
      private final int widthoffset = 20;
      private JLabel showlabel;
      
      public PicturePreview(){
          addWindowListener( new WindowAdapter(){
              public void windowClosing(WindowEvent e){
                  closePreview();
              }
          });
          setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
          showlabel = new JLabel( picture );
          JScrollPane showscroll = new JScrollPane( showlabel );
          showscroll.addMouseListener(new MouseAdapter(){
              public void mouseClicked(MouseEvent e) {
                  closePreview();
              }
              public void mouseEntered(MouseEvent e){
                  setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              }
              public void mouseExited(MouseEvent e){
                  setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              }
          });
          JPanel showpanel = new JPanel( new BorderLayout() );
          showpanel.add( showscroll, BorderLayout.CENTER );
          getContentPane().add( showpanel );
      }
      
      public void reloadImage(ImageIcon anImageIcon){
          setTitle(getPreviewTitle(anImageIcon));
          showlabel.setIcon(anImageIcon);
          int width = Math.min( 640 - widthoffset, anImageIcon.getIconWidth() );
          int height = Math.min( 480 - heightoffset, anImageIcon.getIconHeight() );
          if(!isDisplayable()) pack(); 
          setSize( width + widthoffset, height + heightoffset );
          setLocationRelativeTo( null );
      }
      
      private String getPreviewTitle(ImageIcon anImageIcon) {
          StringBuilder title = new StringBuilder(70);
          title.append(Messages.getString("GUI_PictureField_Picture"));
          if ( addressNr != null ) {
              title.append(Messages.getString("GUI_PictureField_FromContactNr")).append(" ").append(addressNr);
              title.append(" (").append(anImageIcon.getIconWidth()).append("x").append(anImageIcon.getIconHeight()).append(")");
          }
          else {
              title.append(" (").append(anImageIcon.getIconWidth()).append("x").append(anImageIcon.getIconHeight()).append(")");
          }
          return title.toString();
      }
      
      public void closePreview() {
          if(!isVisible()) return;
          setVisible(false);
          dispose();
      }
  }
}
