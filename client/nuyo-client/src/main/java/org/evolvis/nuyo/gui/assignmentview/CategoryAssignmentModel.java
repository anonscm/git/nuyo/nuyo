package org.evolvis.nuyo.gui.assignmentview;

import java.awt.Component;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.groupware.Category;
import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.utils.TaskManager;

/**
 * Model class for the category assignemt viewer.
 * 
 * <p>
 * Hides the nitty-gritty details of acquiring and formatting the data.
 * </p>
 * 
 */
class CategoryAssignmentModel extends AbstractReadOnlyBinding {

	public final static Logger logger = Logger.getLogger(CategoryAssignmentModel.class.getName());

	DefaultListModel listModel = new DefaultListModel();

	/** Provides natural language order for . */
	Collator collator = Collator.getInstance();

	/** Does Category list sorting. */
	Comparator catComparator;

	/** Does SubCategory list sorting. */
	Comparator subCatComparator;

	CategoryAssignmentModel() {
		super(ApplicationModel.ADDRESS_ASSIGNMENT_KEY);

		BindingManager bm = ApplicationServices.getInstance()
				.getBindingManager();

		bm.addBinding(this);
		// Become updated when something changes with the category data
		// itself.
		bm.addBinding(new CategoryDataUpdater());
	}

	/**
	 * Returns a {@link ListModel} implementation suitable for a {@link JList}
	 * of the category assignment.
	 * 
	 * @return
	 */
	ListModel getListModel() {
		return listModel;
	}

	ListCellRenderer getRenderer() {
		return new Renderer();
	}

	public void setViewData(Object arg) {
		List cats = (List) arg;

		if (cats == null)
			return;

		if (catComparator == null) {
			catComparator = new Comparator() {

				public int compare(Object src, Object dst) {
					return collator.compare(((Category) src).getCategoryName(),
							((Category) dst).getCategoryName());
				}

			};
		}

		if (subCatComparator == null) {
			subCatComparator = new Comparator() {

				public int compare(Object src, Object dst) {
					return collator.compare(((SubCategory) src)
							.getSubCategoryName(), ((SubCategory) dst)
							.getSubCategoryName());
				}

			};
		}

		// Removes any entries from a different address
		listModel.clear();

		// Sorts the Category entries and then iterates through
		// them.
		Collections.sort(cats, catComparator);

		Iterator catIte = cats.iterator();
		while (catIte.hasNext()) {
			Category cat = (Category) catIte.next();
			listModel.addElement(cat);

			List subCats = cat.getSubCategories();
			Collections.sort(subCats, subCatComparator);
			Iterator subCatIte = subCats.iterator();
			while (subCatIte.hasNext()) {
				SubCategory subCat = (SubCategory) subCatIte.next();

				// SubCategory entries should be indented.
				listModel.addElement(subCat);
			}
			
			// Appends an empty line at the end of the subcategory list
			// (even when it is empty) to better group the entries.
			listModel.addElement("");
		}
	}
	
	/**
	 * When categories or subcategories have been renamed, added or removed
	 * this updater reloads the associated categories for the current address
	 * which in turn provokes an update to the displayed associated categories.
	 */
	private static class CategoryDataUpdater extends AbstractReadOnlyBinding
	{
		CategoryDataUpdater()
		{
			super(ApplicationModel.CATEGORIES_UPDATE_WATCH_KEY);
		}
		
		public void setViewData(Object arg) {
			if (arg == null)
			  return;
			
			TaskManager.getInstance().register(new TaskManager.AbstractTask()
			{
				public void run(TaskManager.Context ctx)
				{
					ApplicationServices as = ApplicationServices.getInstance();
					ApplicationModel am = as.getApplicationModel();
					try {
						am.setCategoryAssignment(ApplicationServices.getInstance().getCurrentDatabase().getAssociatedCategoriesForSelectedAddress(
								new Integer(ApplicationServices.getInstance().getActionManager().getAddress().getId())));
					} catch (ContactDBException e) {
						//TODO: Bessere Fehlerausgabe machen...
						logger.warning("Error in CategoryDataUpdater");
					}
				}
			}, Messages.getString("GUI_CategoryAssignmentViewer_Updating"), false);
		}

	}

	private static class Renderer extends DefaultListCellRenderer
	{
		public Component getListCellRendererComponent(JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus)
		{
		  super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		  
		  if (value instanceof Category)
		  {
			  Category cat = (Category) value;
			  setText(cat.getCategoryName());
			  setToolTipText(cat.getDescription());
		  }
		  else if (value instanceof SubCategory)
		  {
			  SubCategory scat = (SubCategory) value;
			  setText("   - " + scat.getSubCategoryName());
			  setToolTipText(scat.getDescription());
		  }
		  else if (value instanceof String)
		  {
			  setText("");
		  }
			
		  return this;
		}
	}
}
