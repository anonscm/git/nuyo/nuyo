package org.evolvis.nuyo.db.octopus;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.User;


/**
 * @author Sebastian Mancke, tarent GmbH
 * 
 * This Object represents an Address for the contact client. It extends the server-side address with
 * persistence logic.
 *  
 */
public class AddressImpl extends Address {

    private static Logger logger = Logger.getLogger(AddressImpl.class.getName());

	private AddressPreview _preview;
	private Integer _category;
	private List _standardData;
	private List _protectedData;
	private String email;
	private User _user;
	private boolean _standardDataChanged = false;
	private Map _associatedCategories;
	
	
    public AddressImpl() {
		super();
    }

    public void loadAllData() {
        // do nothing at the moment, since all addresses should be complete
    }

// 	/**
// 	 * Der Login des Benutzers, der diesem Addressobjekt zugeordnet ist. Wenn
// 	 * keine Zuordnung besteht: <code>null/code>
// 	 */
// 	public String getAssociatedUserLogin() throws ContactDBException {
//         User user = new UserImpl(getAssociatedUserID());
//         if (user == null)
//             return null;
//         return user.getLoginName();
// 	}

	public String toString() {
        return getShortAddressLabel();
	}

//     /* (non-Javadoc)
//      * @see de.tarent.contact.db.PersistentEntity#populate(java.util.List)
//      */
//     public void populate(ResponseData values) {
//         // TODO Auto-generated method stub
        
//     }

//     /* (non-Javadoc)
//      * @see de.tarent.contact.db.PersistentEntity#serialize()
//      */
//     public Object serialize() {
//         // TODO Auto-generated method stub
//         return null;
//     }

//     /* (non-Javadoc)
//      * @see de.tarent.contact.db.PersistentEntity#getState()
//      */
//     public int getState() {
//         // TODO Auto-generated method stub
//         return 0;
//     }


//     public String asStringWithNull(Object data) {
//         return (data == null) ? "" : ""+data;
//     }

    
}
