/*
 * Created on 18.07.2003
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.util.Map;

import javax.swing.ImageIcon;

/**
 * @author niko
 *
 */
public interface ScheduleType
{
  public Map getActions();
  public ImageIcon getIcon();
  public String getDescription();
  public String getName();
  public int getKey();
  public Object getExternalKey();
}
