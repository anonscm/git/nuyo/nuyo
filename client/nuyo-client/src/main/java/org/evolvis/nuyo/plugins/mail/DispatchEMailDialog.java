package org.evolvis.nuyo.plugins.mail;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.PropertyResourceBundle;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.action.ActionContainerException;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;
import org.evolvis.xana.swing.MenuBar;
import org.evolvis.xana.swing.ToolBar;
import org.evolvis.xana.swing.utils.SwingIconFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.utils.ByteHandlerSI;
import de.tarent.commons.utils.TextFileParser;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 * 
 *
 */
public class DispatchEMailDialog extends JFrame implements ActionListener, PropertyChangeListener
{
	private DispatchEMailPanel mainPanel;

	private JPanel statusBar;

	private JLabel addresseesLabel;
	private JLabel attachmentsLabel;
	private JLabel dispatchLabel;

	private static DispatchEMailDialog instance;

	private static TarentLogger logger = new TarentLogger(DispatchEMailDialog.class);

	private File currentTemplateFile;

	private MailBatch mailBatch;

	private TagInsertDialog tagInsertDialog;

	private boolean dispatchNightly = false;
	private boolean copyToSender = false;

	private DispatchEMailDialog()
	{
		super();

		setTitle(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_TITLE"));
		setIconImage(((ImageIcon)SwingIconFactory.getInstance().getIcon("internet-mail.png")).getImage());	
		
		ActionRegistry.getInstance().disableContext(DispatchEMailPanel.EMAIL_PREVIEW_MODE);
		getMainPanel().setupNavigationModes();
		AbstractGUIAction tagInsertAction = ActionRegistry.getInstance().getAction("mail.tag.insert");
		if(tagInsertAction != null)
			tagInsertAction.setSelected(false);

		// Set MenuBar
		MenuBar menuBar = new MenuBar(StartUpConstants.MAILDIALOG_MENU_BAR_ID, PropertyResourceBundle.getBundle( StartUpConstants.MENU_BUNDLE_NAME, StartUpConstants.LOCALE));
		ActionRegistry.getInstance().addContainer(menuBar);
		menuBar.initActions();

		setJMenuBar(menuBar);


		// Set ToolBar
		ToolBar toolBar = new ToolBar(StartUpConstants.MAILDIALOG_TOOL_BAR_ID);
		ActionRegistry.getInstance().addContainer(toolBar);
		try
		{
			toolBar.initActions();
		} catch (ActionContainerException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setJMenuBar(menuBar);
		getContentPane().add(toolBar, BorderLayout.PAGE_START);

		getContentPane().add(getMainPanel(), BorderLayout.CENTER);
		getContentPane().add(getStatusBar(), BorderLayout.SOUTH);
		
		addWindowListener(new DispatchEMailDialogWindowListener());
	}

	public static DispatchEMailDialog getInstance()
	{
		if(instance == null)
			instance = new DispatchEMailDialog();

		return instance;
	}

	private JPanel getStatusBar()
	{
		if(statusBar == null)
		{
			FormLayout layout = new FormLayout(
					"pref, pref:grow, pref, pref, pref:grow, pref, 100dlu, pref:grow", // columns
			"pref"); // rows

			PanelBuilder builder = new PanelBuilder(layout);

			builder.setBorder(BorderFactory.createEmptyBorder(2, 15, 2, 15));

			CellConstraints cc = new CellConstraints();

			builder.add(getAddresseesLabel(), cc.xy(1, 1));
			builder.add(new JSeparator(JSeparator.VERTICAL), cc.xy(3, 1)).setPreferredSize(new Dimension(5, 10));
			builder.add(getAttachmentsLabel(), cc.xy(4, 1));
			builder.add(new JSeparator(JSeparator.VERTICAL), cc.xy(6, 1)).setPreferredSize(new Dimension(5, 10));
			builder.add(getDispatchLabel(), cc.xy(7, 1));

			statusBar = builder.getPanel();

		}
		return statusBar;
	}

	private JLabel getAddresseesLabel()
	{
		if(addresseesLabel == null)
		{
			addresseesLabel = new JLabel();
			addresseesLabel.setFont(addresseesLabel.getFont().deriveFont(Font.PLAIN));
			updateAddressCount();
		}
		return addresseesLabel;
	}

	private void updateAddressCount()
	{
		if(getMainPanel().getAddresses() != null)
		{
			if (getMainPanel().getAddresses().getSize() != 1)
			{
				getAddresseesLabel().setText(Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_ADDRESS_COUNT_MULTI", new Integer(getMainPanel().getAddresses().getSize())));
			}
			else
			{
				getAddresseesLabel().setText(Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_ADDRESS_COUNT_SINGLE", new Integer(getMainPanel().getAddresses().getSize())));
			}
		}
		else
			getAddresseesLabel().setText(Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_ADDRESS_COUNT_MULTI", new Integer(0)));
	}

	private void updateAttachmentsLabel()
	{
		if(getMainPanel().getAttachmentList().getModel().getSize() == 0)
			getAttachmentsLabel().setText(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_ATTACHMENT_NONE"));
		else
			getAttachmentsLabel().setText(Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_ATTACHMENT_SIZE", ByteHandlerSI.getOptimalRepresentationForBytes(getAttachmentsSize())));
	}

	private void updateDispatchLabel()
	{
		if(dispatchNightly)
			getDispatchLabel().setText(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_DISPATCH_NIGHTLY"));
		else
			getDispatchLabel().setText(Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_STATUS_DISPATCH_IMMEDIATELY"));
	}

	private long getAttachmentsSize()
	{
		Iterator attachmentsIt = ((MutableListModel)getMainPanel().getAttachmentList().getModel()).getData().iterator();
		long sizeInBytes = 0;

		while(attachmentsIt.hasNext())
		{
			File file = (File)attachmentsIt.next();
			sizeInBytes += file.length();
		}
		return sizeInBytes;
	}

	private JLabel getAttachmentsLabel()
	{
		if(attachmentsLabel == null)
		{
			attachmentsLabel = new JLabel();
			attachmentsLabel.setFont(attachmentsLabel.getFont().deriveFont(Font.PLAIN));
			updateAttachmentsLabel();
		}
		return attachmentsLabel;	
	}

	private JLabel getDispatchLabel()
	{
		if(dispatchLabel == null)
		{
			dispatchLabel = new JLabel();
			dispatchLabel.setFont(dispatchLabel.getFont().deriveFont(Font.PLAIN));
			updateDispatchLabel();
		}
		return dispatchLabel;	
	}

	private DispatchEMailPanel getMainPanel()
	{
		if(mainPanel == null)
		{
			mainPanel = new DispatchEMailPanel();
			mainPanel.addActionListener(this);
			mainPanel.addPropertyChangeListener(this);
		}
		return mainPanel;
	}

	public void appendAttachment()
	{
		JFileChooser fileChooser = new JFileChooser(ConfigManager.getPreferences().get("email.attachmentPath", System.getProperty("user.home")));
		// allow appending multiple files at once
		fileChooser.setMultiSelectionEnabled(true);
		int returnVal = fileChooser.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			File[] files = fileChooser.getSelectedFiles();
			for(int i=0; i < files.length; i++)
				((MutableListModel)getMainPanel().getAttachmentList().getModel()).add(files[i]);
			
			getMainPanel().updateAttachmentState();
			if(!(getExtendedState() == JFrame.MAXIMIZED_BOTH)) setSize(getSize());
			else setSize(getSize());
			ConfigManager.getPreferences().put("email.attachmentPath", fileChooser.getSelectedFile().getParent());
		}
		updateAttachmentsLabel();
	}

	public void showUserMail()
	{	
		getMainPanel().showUserMail();
	}
	
	public void showPreviewMail()
	{
		getMainPanel().showPreviewMail();
	}

	public Address getPreviewAddress()
	{
		//return (Address)getMainPanel().getAddresseesList().getSelectedValue();
		return null;
	}

	public String getMessageSubject()
	{
		return getMainPanel().getMessageSubject();
	}

	public String getMessageBody()
	{
		return getMainPanel().getMessageBody();
	}

	public String getSender()
	{
		return getMainPanel().getSenderBox().getSelectedItem().toString();
	}

	public Integer getActionID()
	{
		if(mailBatch != null) return new Integer(mailBatch.getId());
		return new Integer(0);
	}

	public void setAddresses(Addresses pAddressees)
	{
		getMainPanel().setAddresses(pAddressees);
		
		updateAddressCount();
	}

	public void setMailBatch(MailBatch pMailBatch, MailBatch.CHANNEL pChannel)
	{
		try
		{
			setAddresses(pMailBatch.getChannelAddresses(pChannel));
			mailBatch = pMailBatch;
			//channel = pChannel;
		} catch (ContactDBException e)
		{
			logger.warning("Failed getting Mail-Batch-addresses", e);
		}
	}

	public void openTemplate()
	{
		JFileChooser fileChooser = new JFileChooser(ConfigManager.getPreferences().get("email.templatePath", System.getProperty("user.home")));
		fileChooser.setFileFilter(new FileFilter() {

			public boolean accept(File f)
			{
				return (f.getName().endsWith(".txt") || f.isDirectory());
			}

			public String getDescription()
			{
				return Messages.getString("EMAIL_TEMPLATE_FILE_FILTER_DESC");
			}
		});
		int selection = fileChooser.showOpenDialog(this);
		if(selection == JFileChooser.APPROVE_OPTION)
		{
			try
			{
				getMainPanel().setMessageBody(TextFileParser.readFile(fileChooser.getSelectedFile()));
				currentTemplateFile = fileChooser.getSelectedFile();
				getMainPanel().updateTemplateLabel(currentTemplateFile.getName());
				ConfigManager.getPreferences().put("email.templatePath", currentTemplateFile.getParent());
				//ActionRegistry.getInstance().getAction("mail.template.delete").setEnabled(true);
			} catch (IOException e)
			{
				logger.warning(Messages.getFormattedString("GUI_EMAIL_OPEN_TEMPLATE_IO_ERROR", fileChooser.getSelectedFile().getName()), e);
			}
		}
	}

	public void saveCurrentTemplate()
	{
		if(currentTemplateFile != null && currentTemplateFile.getName().length() > 0)
			saveCurrentTemplateToFile(currentTemplateFile);
		else
			saveCurrentTemplateAs();
	}

	public void saveCurrentTemplateAs()
	{
		JFileChooser fileChooser = new JFileChooser(ConfigManager.getPreferences().get("email.templatePath", System.getProperty("user.home")));
		fileChooser.setFileFilter(new FileFilter() {

			public boolean accept(File f)
			{
				return (f.getName().endsWith(".txt") || f.isDirectory());
			}

			public String getDescription()
			{
				return Messages.getString("EMAIL_TEMPLATE_FILE_FILTER_DESC");
			}
		});

		int selection = fileChooser.showSaveDialog(this);
		if(selection == JFileChooser.APPROVE_OPTION)
			saveCurrentTemplateToFile(fileChooser.getSelectedFile());
	}

	public void saveCurrentTemplateToFile(File pFile)
	{
		try
		{
			File file = pFile;
			
			// append suffix ".txt" to filename if missing
			if(pFile.getName().toString().indexOf(".txt") == -1)
				file = new File(pFile.getAbsolutePath() + ".txt");
			
			TextFileParser.writeTextToFile(file, getMessageBody());
			currentTemplateFile = file;
			ConfigManager.getPreferences().put("email.templatePath", currentTemplateFile.getParent());
			getMainPanel().updateTemplateLabel(currentTemplateFile.getName());
			AbstractGUIAction deleteAction = ActionRegistry.getInstance().getAction("mail.template.delete");
			if(deleteAction != null)
				deleteAction.setEnabled(true);

		} catch (IOException e)
		{
			logger.warning(Messages.getFormattedString("GUI_EMAIL_SAVE_TEMPLATE_IO_ERROR", pFile.getName()), e);
		}

	}

	public void deleteCurrentTemplate()
	{ 
		if(currentTemplateFile != null && currentTemplateFile.getName().length() > 0)
		{
			Object[] options = { Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_DELETE_TEMPLATE_DELETE_OPTION"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_DELETE_TEMPLATE_CANCEL_OPTION") };
			if(JOptionPane.showOptionDialog(this, Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_DELETE_TEMPLATE_MESSAGE", currentTemplateFile.getName()), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_DELETE_TEMPLATE_TITLE"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]) == 0)
			{
				currentTemplateFile.delete();
				getMainPanel().setMessageBody("");
				getMainPanel().updateTemplateLabel(null);
				currentTemplateFile = null;
				Action deleteAction = ActionRegistry.getInstance().getAction("mail.template.delete");
				if(deleteAction != null)
					deleteAction.setEnabled(true);
			}
		}
	}

	public void sendMessage()
	{		
		// Possibly need to disable preview-mode (See bug #2858 https://bugzilla.tarent.de/bugzilla/show_bug.cgi?id=2858)
		showUserMail();
		
		// Ask the user if he *really* wants to send the message.
		// The number of addresses on which this demand will be shown can be configured
		if(getMainPanel().getAddresses().getSize() >= ConfigManager.getEnvironment().getAsInt(Key.EMAIL_DEMAND_MINIMUM))
			if(JOptionPane.showConfirmDialog(this, Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_REALLY_SEND", new Integer(getMainPanel().getAddresses().getSize())), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_REALLY_SEND_TITLE"), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
				return;
		
		// Check if subject is empty
		if(getMessageSubject() == null || getMessageSubject().trim().length() == 0)
		{
			Object[] options = { Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_SUBJECT_ENTER_SUBJECT"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_SUBJECT_SEND_OPTION") };
			if(JOptionPane.showOptionDialog(this, Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_SUBJECT_MESSAGE"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_SUBJECT_TITLE"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]) != 1)
			{
				getMainPanel().getSubjectField().requestFocus();
				return;
			}
		}


		// Check if text is referring to missing attachment
		while(getMainPanel().getAttachmentList().getModel().getSize() == 0 && checkForAttachmentKeyword(getMessageBody()))
		{
			/* show a message to the user with three options:
			 * 
			 * 1) append an attachment
			 * 2) send without attachment
			 * 3) cancel
			 */

			Object [] options = { Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_ATTACHMENT_ATTACH_OPTION"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_ATTACHMENT_SEND_OPTION"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_ATTACHMENT_CANCEL_OPTION") };
			int selection = JOptionPane.showOptionDialog(this, Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_ATTACHMENT_MESSAGE"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_MISSING_ATTACHMENT_TITLE"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
			if(selection == 0)
			{
				appendAttachment();
			}
			else if(selection == 1)
			{
				// just proceed
			}
			else
			{
				// if user wants to cancel or dialog is closed, return
				return;
			}
		}


		// If email should not be dispatched nightly, Check whether attachment-size is above administrator-specified size.
		if(!dispatchNightly)
		{
			long attachmentSize = getAttachmentsSize();
			long attachmentSizeLimit = ConfigManager.getEnvironment().getAsLong(Key.EMAIL_ATTACHMENT_SIZE_LIMIT, Long.MAX_VALUE);
			if(attachmentSize > attachmentSizeLimit)
			{
				Object[] options = { Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_LARGE_ATTACHMENT_OPTION_NIGHTLY"), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_LARGE_ATTACHMENT_OPTION_IMMEDIATELY") , Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_LARGE_ATTACHMENT_OPTION_CANCEL")};
				int selection = JOptionPane.showOptionDialog(this, Messages.getFormattedString("GUI_EMAIL_DISPATCH_DIALOG_LARGE_ATTACHMENT_MESSAGE", ByteHandlerSI.getOptimalRepresentationForBytes(getAttachmentsSize())), Messages.getString("GUI_EMAIL_DISPATCH_DIALOG_LARGE_ATTACHMENT_TITLE"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
				if(selection == 0) 
				{
					// dispatch email nightly
					dispatchNightly = true;
				}
				else if(selection == 1)
				{
					// just proceed
				}
				else
				{
					// if user wants to cancel or dialog is closed, return
					return;
				}
			}
		}

		// Package attachments

		// Get list of attachment-files
		Iterator attachmentIterator = ((MutableListModel)getMainPanel().getAttachmentList().getModel()).getData().iterator();

		DispatchEMailWorker.sendMessage(getSender(), getMessageSubject(), getMessageBody(), getActionID(), attachmentIterator, dispatchNightly, copyToSender);

		setVisible(false);
		instance = null;
		dispose();
	}
	
	/**
	 * Checks a text for occurence of some keywords which may refer to an missing attachment
	 * 
	 * @param text the text to check
	 * @return true if text contains at least one of the keywords
	 */
	
	private boolean checkForAttachmentKeyword(String text)
	{
		String [] attachmentKeywords = new String[] { "attached", "Attachment", "attachment", "Anhang", "anhang", "Anbei", "anbei", "anh�ngend" };
		
		for(int i=0; i < attachmentKeywords.length; i++)
		{
			if(text.contains(attachmentKeywords[i]))
				return true;
		}
		return false;
	}

	public void setDispatchNightly(boolean pDispatchNightly)
	{
		dispatchNightly = pDispatchNightly;
		updateDispatchLabel();
	}
	
	public void setCopyToSender(boolean copyToSender)
	{
		this.copyToSender = copyToSender;
	}

	public void insertTextAtCurrentPosition(String pText)
	{
		getMainPanel().insertTextAtCurrentPosition(pText);
	}

	public void insertTag()
	{
		getTagInsertDialog().setVisible(!getTagInsertDialog().isVisible());
		AbstractGUIAction tagInsertAction = ActionRegistry.getInstance().getAction("mail.tag.insert");
		if(tagInsertAction != null)
			tagInsertAction.setSelected(getTagInsertDialog().isVisible());
		
		//if(!getMainPanel().getSubjectField().hasFocus())
			//getMainPanel().getMessageArea().requestFocus();
	}

	public TagInsertDialog getTagInsertDialog()
	{
		if(tagInsertDialog == null)
			tagInsertDialog = new TagInsertDialog(this);
		
		return tagInsertDialog;
	}

	public void jumpToFirstMail()
	{
		if(getMainPanel().getAddresseesList().getNumberOfRows() > 0)
			getMainPanel().getAddresseesList().setSelectedIndex(0);
		
		if(ActionRegistry.getInstance().isContextEnabled(DispatchEMailPanel.EMAIL_PREVIEW_MODE))
				showPreviewMail();
		
		getMainPanel().setupNavigationModes();
	}

	public void jumpToPreviousMail()
	{
		if(getMainPanel().getAddresseesList().getSelectedIndex() > 0)
			getMainPanel().getAddresseesList().setSelectedIndex(getMainPanel().getAddresseesList().getSelectedIndex()-1);
		
		if(ActionRegistry.getInstance().isContextEnabled(DispatchEMailPanel.EMAIL_PREVIEW_MODE))
			showPreviewMail();
		
		getMainPanel().setupNavigationModes();
	}

	public void jumpToNextMail()
	{
		if(getMainPanel().getAddresseesList().getNumberOfRows() > 0 && getMainPanel().getAddresseesList().getSelectedIndex() < getMainPanel().getAddresseesList().getNumberOfRows()-1)
			getMainPanel().getAddresseesList().setSelectedIndex(getMainPanel().getAddresseesList().getSelectedIndex()+1);
		
		if(ActionRegistry.getInstance().isContextEnabled(DispatchEMailPanel.EMAIL_PREVIEW_MODE))
			showPreviewMail();
		
		getMainPanel().setupNavigationModes();
	}

	public void jumpToLastMail()
	{
		if(getMainPanel().getAddresseesList().getNumberOfRows() > 0)
			getMainPanel().getAddresseesList().setSelectedIndex(getMainPanel().getAddresseesList().getNumberOfRows()-1);
		
		if(ActionRegistry.getInstance().isContextEnabled(DispatchEMailPanel.EMAIL_PREVIEW_MODE))
			showPreviewMail();
		
		getMainPanel().setupNavigationModes();
	}

	private class DispatchEMailDialogWindowListener extends WindowAdapter
	{		
		/*
		 * The Subject-Field should get the focus at start so he can immeadiately
		 * start composing the email
		 * 
		 */
		public void windowActivated(WindowEvent e) {
			getMainPanel().getSubjectField().requestFocusInWindow();
		}

		public void windowClosing(WindowEvent e)
		{
			// dispose this dialog
			instance = null;
			super.windowClosing(e);
		}
	}

	public void actionPerformed(ActionEvent pEvent)
	{
		if(pEvent.getActionCommand().equals("attachments_deleted"))
			updateAttachmentsLabel();	
		else if(pEvent.getActionCommand().equals("tag_insert_requested"))
		{
			getTagInsertDialog().setVisible(true);
			AbstractGUIAction tagInsertAction = ActionRegistry.getInstance().getAction("mail.tag.insert");
			if(tagInsertAction != null)
				tagInsertAction.setSelected(getTagInsertDialog().isVisible());
			//visualize tag seek mode
		}
		else if(pEvent.getActionCommand().equals("insert_current_tag_requested"))
		{
			String repEx = getTagInsertDialog().getReplacementExpressionOfSelectedEntry();
			insertTextAtCurrentPosition(repEx.substring(2, repEx.length()));
		}
	}

	public void propertyChange(PropertyChangeEvent evt)
	{	
		if(evt.getPropertyName().equals("tagName"))
			getTagInsertDialog().jumpToEntry(evt.getNewValue().toString());
	}
}