package org.evolvis.nuyo.db;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.groupware.category.CategoryList;

/**
 * @author Sebastian Mancke, tarent GmbH
 * 
 * This is a management inerface for address obejcts.
 *  
 */
public interface CategoryClientDAO  {

    /**
     * Saves the address.
     * If the address was new (invalid id), the id of the supplied object will be set.
     */
	public void save(Category category) throws ContactDBException;

    /**
     * Deletes the address completely (not only from some categories)
     */
	public void delete(Category category) throws ContactDBException;    
    
	/**
	 * returns a list of all categories connected to the given set of addresses.
 	 * several conditions can be chosen to filter the set of visible categories.
 	 * 
	 * @param addresPks list of adress Pks
	 * @param read flag to get only those categories on which i have permission to read
	 * @param edit flag to get only those categories on which i have permission to edit
	 * @param add flag to get only categories on which i have permission to add addresses
	 * @param remove flag to get only categories on which i have permission to remove addresses
	 * @param addSubCat	flag to get only categories on which i have permission to add subcategories
	 * @param removeSubCat flag to get only categories on which i have permission to remove subcategories
	 * @param structure flag to get only categories on which i have permission to structure addresses 
	 * @param grant	flag to get only categories on which i have the grant right
	 * @param includeVirtual inlude or exlude virtual categories
	 * @return returns a List of Category objects. 
	 * @throws ContactDBException 
	 */
 	public CategoryList getAssociatedCategories(List addressPks,boolean read, boolean edit, boolean add, boolean remove, boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeVirtual) throws ContactDBException;


 	/**
 	 * returns a list of all categories connected to the current selection of addresses.
 	 * several conditions can be chosen to filter the set of visible categories.
 	 * 
	 * @param read flag to get only those categories on which i have permission to read
	 * @param edit flag to get only those categories on which i have permission to edit
	 * @param add flag to get only categories on which i have permission to add addresses
	 * @param remove flag to get only categories on which i have permission to remove addresses
	 * @param addSubCat	flag to get only categories on which i have permission to add subcategories
	 * @param removeSubCat flag to get only categories on which i have permission to remove subcategories
	 * @param structure flag to get only categories on which i have permission to structure addresses 
	 * @param grant	flag to get only categories on which i have the grant right
	 * @param includeVirtual inlude or exlude virtual categories
	 * @return returns a List of Category objects. 
	 * @throws ContactDBException 
	 */

 	public CategoryList getAssociatedCategoriesForCurrentSelection(List addresPks, boolean read, boolean edit, boolean add, boolean remove, boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeVirtual) throws ContactDBException;
		
 	
 	public CategoryList getAssociatedCategoriesForCurrentSelection(List addressPks, List Filter, boolean includeVirtual) throws ContactDBException;
	
 		
 	/**
 	 * 
 	 * @return list of all visible categories and subcategories that are connected to the currently selected address.
 	 */
 	public CategoryList getAssociatedCategoriesForSelectedAddress(Integer addressPk) throws ContactDBException;
 	
 	
 	/**
 	 * 
 	 * @return list of all visible categories and subcategories that are connected to the currently selected address.
 	 */
 	public CategoryList getAssociatedCategoriesForSelectedAddress(Integer addressPk, List filterList, boolean includeVirtual) throws ContactDBException;
 	
 	/**
 	 * 
 	 * @param force flag to force (de)assignment even if some addresses can't be (de)assigned
 	 * @param forceWithDeletion flag to allow deletion of addresses (cancel the last conenctions to categories)  
 	 * @param addressPks List of addresses to (de)assign
 	 * @param addSubCategories List of subcategory pks 
 	 * @param removeSubCategories List of subcategory pks
 	 * @return Map with lists of addresspks accessable with the keys "notAssignableAddresses", "notDeassignableAddresses", "implicitlyDeletedAddresses"
 	 */
 	public Map assignOrDeassignAddresses(Boolean force, Boolean forceWithDeletion, List addressPks, List addSubCategories, List removeSubCategories)  throws ContactDBException ;
}
