package org.evolvis.nuyo.db;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.commons.datahandling.ListFilterProvider;
import de.tarent.commons.datahandling.PrimaryKeyList;

/**
 * This is a representation for the search parameters of address lists.
 */
public class AddressListParameter implements ListFilterProvider {

    List disjunctionSubcategories;
    List conjunctionSubcategories;
    List negatedSubcategories;
    List filterList;

    String sortField = Address.PROPERTY_NACHNAME.getKey();
    String sortDirection = ListFilter.DIRECTION_ASC;
    List pkFilter;
    String addressSourceLabel;
    
    /**
     * Disables any filtering. The sort-order and -direction still remain.
     */
    public void clearFilter() {
        disjunctionSubcategories = null;
        conjunctionSubcategories = null;
        negatedSubcategories = null;
        filterList = null;        
        pkFilter = null;
        setAddressSourceLabel(null);
    }

    /**
     * Returns the complete filter list, incuding the search Filter and the pkFilter, as specified in {@see de.tarent.commons.datahandling.ListFilterProvider}
     */    
    public List getCompleteFilterList() {
        // if one is null, return the other one
        if (pkFilter == null || pkFilter.size() == 0)
            return filterList;
        
        ArrayList combinedList = new ArrayList(filterList != null ? filterList.size() + 4 : 3);

        combinedList.add(Address.PROPERTY_ID.getKey());
        combinedList.add(PrimaryKeyList.getPrimaryKeysListFor(pkFilter));
        combinedList.add(ListFilterOperator.IN);

        if (filterList != null) {
            combinedList.addAll(filterList);
            if (filterList.size() > 0)
                combinedList.add(ListFilterOperator.AND);
        }
        return combinedList;
    }

    /**
     * Returns, if a category-filter enabled
     */
    public boolean isCategoryFilterEnabled() {
        return (disjunctionSubcategories != null && disjunctionSubcategories.size() > 0)
            || (conjunctionSubcategories != null && conjunctionSubcategories.size() > 0)
            || (negatedSubcategories != null && negatedSubcategories.size() > 0);
    }

    /**
     * Returns, if a search filter is enabled
     */
    public boolean isSearchFilterEnabled() {
        return (filterList != null && filterList.size() > 0);
    }

    /**
     * Set a list of address pks as filter
     */
    public void setPkFilter(List newPkFilter) {
        this.pkFilter = newPkFilter;
    }

    /**
     * Return the current pk filter
     */
    public List getPkFilter() {
        return this.pkFilter;
    }


    /**
     * Returns the sort direction of the list
     */
    public String getSortDirection() {
        return sortDirection;
    }

    /**
     * Sets the sort direction of the list
     */
    public void setSortDirection(String newSortDirection) {
        this.sortDirection = newSortDirection;
    }
    
    public void toggleSortDirection() {
        if (ListFilter.DIRECTION_ASC.equals(sortDirection))
            sortDirection = ListFilter.DIRECTION_DESC;
        else
            sortDirection = ListFilter.DIRECTION_ASC;
    }

    /**
     * Returns the field for sorting
     */
    public String getSortField() {
        return sortField;
    }
    
    /**
     * Sets the field for sorting
     */
    public void setSortField(String newSortField) {
        this.sortField = newSortField;
    }

    /**
     * Returns the filter list for the search Filter, as specified in {@see de.tarent.commons.datahandling.ListFilterProvider}
     */    
    public List getFilterList() {
        return filterList;
    }

    /**
     * Sets the filter list, as specified in {@see de.tarent.commons.datahandling.ListFilterProvider}
     */    
    public void setFilterList(List newFilterList) {
        this.filterList = newFilterList;
    }


    /**
     * Returns the list of negated subcategories. If the address is in <b>one</b> of these categories, it will not be returned.
     */
    public List getNegatedSubcategories() {
        return negatedSubcategories;
    }

    /**
     * Sets the list of negated subcategories. If the address is in <b>one</b> of these categories, it will not be returned.
     */
    public void setNegatedSubcategories(List newNegatedSubcategories) {
        this.negatedSubcategories = newNegatedSubcategories;
    }
    
    /**
     * Returns the list of conjunction subcategories. A address has to be in <b>all</b> of this subcategories to be returned
     */
    public List getConjunctionSubcategories() {
        return conjunctionSubcategories;
    }

    /**
     * Sets the list of conjunction subcategories. A address has to be in <b>all</b> of this subcategories to be returned
     */
    public void setConjunctionSubcategories(List newConjunctionSubcategories) {
        this.conjunctionSubcategories = newConjunctionSubcategories;
    }
    
    /**
     * Returns the list of disjunction subcategories. A address has to be in <b>one</b> of this subcategories to be returned
     */
    public List getDisjunctionSubcategories() {
        return disjunctionSubcategories;
    }

    /**
     * Sets the list of disjunction subcategories. A address has to be in <b>one</b> of this subcategories to be returned
     */
    public void setDisjunctionSubcategories(List newDisjunctionSubcategories) {
        this.disjunctionSubcategories = newDisjunctionSubcategories;
    }

    /**
     * Set the caption to identify the selection by the user
     */
    public String getAddressSourceLabel() {
        if (addressSourceLabel == null)
            return Messages.getString("GUI_AdressListe_All_Addresses");
        return addressSourceLabel;
    }

    /**
     * Return the caption to identify the selection by the user, null means all-addresses
     */
    public void setAddressSourceLabel(String newAddressSourceLabel) {
        this.addressSourceLabel = newAddressSourceLabel;
    }


}