package org.evolvis.nuyo.gui.calendar;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.Date;

import javax.swing.JPanel;

/*
 * Created on 17.07.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DayNamesPanel extends JPanel 
{
  private ScheduleData m_oScheduleData;
  private int m_iNumDays;
  private ScheduleDate m_oFirstDay;
    
  public DayNamesPanel(ScheduleData sd, ScheduleDate firstday, int numdays)
  {
    m_oScheduleData = sd;
    m_oFirstDay = firstday;
    m_iNumDays = numdays;
  }
    
  public void setFirstDay(ScheduleDate date)
  {
    m_oFirstDay = new ScheduleDate(new Date(date.getDate().getTime()));
    this.repaint();
  }
  
  public void setNumDays(int numdays)
  {
    m_iNumDays = numdays;
    this.repaint();
  }
  
  
  public ScheduleDate getFirstDay()
  {
    return new ScheduleDate(new Date(m_oFirstDay.getDate().getTime()));
  }
  
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {        
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();

    g.setColor(m_oScheduleData.backgroundPanelColor);
    g.fillRect(0, 0, size.width, size.height);    
    
    // die Tage f�llen
    g.setColor(m_oScheduleData.freeDayColor);
    g.fillRect(0, 0, (m_oScheduleData.dayWidth * m_iNumDays), m_oScheduleData.firstHourY);
    
    // das Tages-Grid zeichnen
    for(int d = 0; d<(m_iNumDays); d++)
    {
      ScheduleDate date = m_oFirstDay.getDateByOffset(d);
      DayDescription dd = DateDescriptionFactory.instance().getDescriptionForDay(date);
      
      if (d < m_iNumDays)
      {  
        if (dd instanceof WorkDayDescription)
        {        
          g.setColor(m_oScheduleData.workDayColor);
        }
        else if (dd instanceof HolidayDescription)
        {        
          g.setColor(m_oScheduleData.holidayColor);
        }
        else if (dd instanceof WeekendDayDescription)
        {        
          g.setColor(m_oScheduleData.freeDayColor);
        }
        int x = (d * m_oScheduleData.dayWidth);
        int y = 0;
        int w = m_oScheduleData.dayWidth;
        int h = m_oScheduleData.firstHourY;
        g.fillRect(x, y, w, h);        
      }
      
      g.setColor(m_oScheduleData.gridColor);
      g.drawLine((d * m_oScheduleData.dayWidth), 0, (d * m_oScheduleData.dayWidth), m_oScheduleData.firstHourY);
      if (d < m_iNumDays) //TODO
      { 
        g.setColor(m_oScheduleData.dayFontColor); 
        
        String linetext = dd.getShortDayName() + " " + date.getShortDateString();
        
        Rectangle2D rect_1 = g.getFontMetrics().getStringBounds(linetext, g);
        int height_1 = (int)(rect_1.getHeight());
        int posx_1 = (int)(((m_oScheduleData.dayWidth) - rect_1.getWidth()) / 2.0);
        
        int posy = (m_oScheduleData.hourHeight / 2) + 2;
        g.drawString(linetext, ((d * m_oScheduleData.dayWidth)) + posx_1, posy);
      }       
    }
  }
  
  public int getDayNumOfXPos(int x)
  {
    if (x> (m_oScheduleData.dayWidth * m_iNumDays)) return -1;
    return x / (m_oScheduleData.dayWidth);
  }
  
}

