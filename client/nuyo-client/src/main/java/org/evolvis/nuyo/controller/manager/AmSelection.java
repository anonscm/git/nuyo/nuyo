/* $Id: AmSelection.java,v 1.51 2007/10/11 13:30:56 nils Exp $
 * 
 * Created on 09.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink.
 *  
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ContactControlException;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.octopus.AddressesImpl;
import org.evolvis.nuyo.db.octopus.SubCategoryImpl;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.db.veto.VetoableAction;
import org.evolvis.nuyo.gui.ContactGuiException;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressPluginData;
import org.evolvis.xana.action.ActionRegistry;

import de.tarent.commons.datahandling.binding.BeanBinding;
import de.tarent.commons.datahandling.binding.BindingManager;
import de.tarent.commons.datahandling.entity.EntityListEvent;
import de.tarent.commons.datahandling.entity.EntityListListener;


/**
 * Diese Klasse verwaltet den aktuellen Adressbereich und die aktuelle
 * Adresse, auf denen der
 * {@link org.evolvis.nuyo.controller.ActionManager ActionManager} arbeitet.
 * 
 * @author mikel
 */
public class AmSelection extends AmDatabaseFacade implements EntityListListener {

    /** 
     * The applications data model on which the AmSelection instance registers its data
     * sources so that users of this data can be informed about updates conveniently. 
     */
    private ApplicationModel applicationModel;

    //
    // private Membervariablen
    //
    /** aktuelle Adressenliste */
    private Addresses currentListOfAddresses;
    /** aktuelle Adresse */
    private Address currentAddress;
//     /** "Einbeziehen Allgemeiner Adressen" */
//     private boolean allgemeineAdressen = false;
//     /** nicht-Standard-Addressmenge */
//     private boolean useSpecialAddressSet = false;
    /** Um Synchronisierungsprobleme zu vermeiden, kann mit diesem Flag untersagt
     *  werden, dass die aktuelle Adresse der GUI �bergeben wird. */
    private boolean setAddressAllowed = true;
    /** Diese Variable erlaubt das Erzwingen, dass bei Dubletten�bernahme zur
     *  Verteilerauswahl gebl�ttert wird. */
    private boolean enforceShowVerteilerSelection = false;
//    /** Modus: Bearbeiten einer bestehenden Adresse */
//    private boolean editMode = false;
//    /** Modus: Bearbeiten einer neuen Adresse */
//    private boolean newAddressMode = false;
    
    /** variable to remember if search-mode was active before new-/edit-action */
    private boolean wasInSearchMode;
    
    /**
     * Sets the application model for this <code>AmSelection</code> instance
     * and updates every binding that it provides to the model.
     * 
     * @param applicationModel
     */ 
    public void initApplicationModel(ApplicationModel applicationModel)
    {
        assert (applicationModel != null);
        this.applicationModel = applicationModel;

        applicationModel.setAddressListParameter(new AddressListParameter());                
        updateTotalAddressCount();       

        BindingManager bm = ApplicationServices.getInstance().getBindingManager();
        bm.addBinding(new BeanBinding(this, "AddressListParameter", ApplicationModel.ADDRESS_LIST_PARAMETER_KEY));
    }
    
    /** aktuelle Adresse */
    public Address getAddress() {
        return currentAddress;
    }

    /** aktuelle Adressenliste */
    public Addresses getAddresses() {
        return currentListOfAddresses;
    }

    /** Position der aktuellen Adresse in der aktuellen Adressliste */
    protected int getAddressPosition() {
        Integer pos = applicationModel.getSelectedAddressIndex();
        if (pos != null)
            return pos.intValue();
        return -1;
    }

    /** Position der aktuellen Adresse in der aktuellen Adressliste */
    protected void setAddressPosition(int newPosition) {
        if (currentListOfAddresses != null && newPosition >= 0) {
            applicationModel.setSelectedAddressIndex(new Integer(newPosition));
        	setAddress();
        }
    }

    /** Returns 'true' if a new address will be processed. */
    protected boolean isNewAddressMode() {
        return ActionRegistry.getInstance().isContextEnabled(NEW_MODE);
    }

    /** Returns 'true' if an existing address will be processed. */
    protected boolean isEditMode() {
        return ActionRegistry.getInstance().isContextEnabled(EDIT_MODE);
    }
    
    /** Returns 'true' if nothing is processing. */
    protected boolean isViewMode() {
        return ActionRegistry.getInstance().isContextEnabled(VIEW_MODE);
    }
    
    /** Returns 'true' if address searching will be processed. */
    protected boolean isSearchMode() {
        return ActionRegistry.getInstance().isContextEnabled(SEARCH_MODE);
    }
    
    /**
     * Whether the search-context was activated before edit-/new-mode 
     * 
     * @return true if search-context was activated
     */
    protected boolean wasInSearchModeBeforeEdit()
    {
    	return wasInSearchMode;
    }
    
    /** 
     * Enables/disables a context mode.<p>
     *  
     * @param mode of some context
     */
    protected void setContextMode(String mode, boolean enabled) {
        if(enabled) ActionRegistry.getInstance().enableContext(mode);
        else ActionRegistry.getInstance().disableContext(mode);
    }
    
    /** Enables 'view' context mode but disables 'new', 'edit' and 'search' mode.*/
    protected void setViewMode() {
        if(isNewAddressMode()) switchMode(NEW_MODE, VIEW_MODE);
        else if(isEditMode()) switchMode(EDIT_MODE, VIEW_MODE);
        else if(isSearchMode()) switchMode(SEARCH_MODE, VIEW_MODE);
        else setContextMode(VIEW_MODE, true);
    }

    /** Enables 'new' context mode but disables 'view' and 'search' mode.*/
    protected void setNewMode() {
        if(isViewMode()) {
        	wasInSearchMode = false;
        	switchMode(VIEW_MODE, NEW_MODE);
        }
        else if(isSearchMode())  {
        	wasInSearchMode = true;
        	switchMode(SEARCH_MODE, NEW_MODE);
        }
        else setContextMode(NEW_MODE, true);
    }

    /** Enables 'edit' context mode but disables 'view' and 'search' mode.*/
    protected void setEditMode() {
        if(isViewMode()) {
        	wasInSearchMode = false;
        	switchMode(VIEW_MODE, EDIT_MODE);
        }
        else if(isSearchMode()) {
        	wasInSearchMode = true;
        	switchMode(SEARCH_MODE, EDIT_MODE);
    	}
        else setContextMode(EDIT_MODE, true);
    }
    
    /** Enables 'search' context mode but disables 'new', 'edit' and 'view' mode.*/
    protected void setSearchMode() {
    	if(isNewAddressMode()) switchMode(NEW_MODE, SEARCH_MODE);
    	else if(isEditMode()) switchMode(EDIT_MODE, SEARCH_MODE);
        else if(isViewMode()) switchMode(VIEW_MODE, SEARCH_MODE);
        else setContextMode(SEARCH_MODE, true);
    }
    
    /**
     * Disables 'fromContext' and enables 'toContext' context mode.<p>
     * 
     * @see org.evolvis.nuyo.gui.action.ActionRegistry#switchContext(String, String)
     * 
     * @param fromContext name
     * @param toContext name
     */
    protected void switchMode(String fromContext, String toContext) {
        ActionRegistry.getInstance().switchContext(fromContext, toContext);
    }
    
    //
    // Methoden, die auf der aktuellen Adressselektion arbeiten
    //
    /**
     * Diese Methode regelt den Aufruf des Nachfrage-Fensters, 
     * falls der Benutzer innerhalb des Editier- oder Neu-Modus
     * Aktionen unternehmen m�chte, die den Modus oder den aktuellen
     * Datensatz �ndern.
     * 
     * @param defaultToSafe legt fest, ob Speichern/Anlegen oder Verwerfen
     *  Default-Button ist.
     */
    protected boolean allowDiscardAddress(boolean defaultToSafe) {
        try {
        	String choicesKey = null;
            String questionKey = null;

            if (isEditMode()
                && (getControlListener().isDirty(currentAddress))) {
                // TODO: Addresszugriff: Enforce Category selection
                //|| (currentAddress.getVerteiler().isEmpty() && !getVerteilergruppe(getNameOfSelectedVerteilergruppe()).isVirtual()) || enforceShowVerteilerSelection)) {
            	choicesKey = "AM_Discard_EditMode_Query_3Choices";
                questionKey = "AM_Discard_EditMode_Query";
            } else if (isNewAddressMode()) {
                choicesKey = "AM_Discard_NewMode_Query_3Choices";
                questionKey = "AM_Discard_NewMode_Query";
            } else {
                return true;
            }
           
            switch (askUser(getControlListener().getMessageString(questionKey), getControlListener().getMessageString(choicesKey).split("[:]", 3), defaultToSafe ? 0 : 1)) {
            case 0:
            	takeAddressDataFromGui();
                saveAddress();
                break;//saved
            case 1:
                cancelAddress();
                break;//canceled
            case 2:
                return false;//not discardeed
            }
            
        } catch (ContactControlException e) {
            String infoKey;
            switch (e.getExceptionId()) {
            case ContactControlException.EX_ADDRESS_INCOMPLETE:
                infoKey = "AM_Discard_AddressIncomplete";
                break;
            case ContactControlException.EX_ADDRESS_NO_SUBCATEGORY:
                infoKey = "AM_Discard_NoSubCategory";
                break;
            case ContactControlException.EX_EMAIL1_INVALID:
                infoKey = "AM_Discard_Email1Invalid";
                break;                
            case ContactControlException.EX_EMAIL2_INVALID:
                infoKey = "AM_Discard_Email2Invalid";
                break;                
            case ContactControlException.EX_PHONE_INVALID:
                infoKey = "AM_Discard_PhoneInvalid";
                break;
            default:
                infoKey = "AM_Discard_Other";
            }
            logger.log(Level.INFO, getControlListener().getMessageString(infoKey), e);
            showInfo(getControlListener().getMessageString(infoKey));
            return false;
        } catch (ContactDBException e) {
            publishError(getControlListener().getMessageString("AM_Discard_DbError"), e);
            return false;
        }
        setViewMode();
        return true;//ok, modi successfully disgarded
    }

   	
    /**
     * Diese Methode beendet einen Editiermodus der GUI und zeigt die letzte
     * unver�nderte Adresse an.
     */
    protected void cancelAddress() throws ContactDBException {
        logger.info("aborting edit modus...");
        enforceShowVerteilerSelection = false;
        getEventsManager().fireDiscardingAddressChanges(currentAddress, getAddressPosition());
        if (isNewAddressMode()) {
            setAddress();
            logger.info("clearing address fields...");
            getEventsManager().fireSetAddress(currentAddress, false);
        } else {
            logger.info("preparing to show the last viewed contact...");
            getEventsManager().fireSetAddress(currentAddress, true);
        }
    }
    
    /**
     * Before saving an address the - probably updated - data from the gui
     * has to be taken and checked for integrity.
     * 
     * <p>Gui components check their individual values and finally it is
     * checked whether the address object as a whole makes sense.</p>
     * 
     * <p>The method returns <code>true</code> if the data is valid and was taken
     * or <code>false</code> if not. An appropriate error message is displayed in 
     * case the take operation fails.</p>
     * 
     */
    public boolean takeAddressDataFromGui()
    {
    	boolean isContactModifiable = isNewAddressMode() || isEditMode();        

    	try
    	{
    		
    	if (isContactModifiable) {
    		// get changed data from gui fields 
    		getControlListener().getAddress(currentAddress);
        	checkDataIntegrity();
    	}
    	 } catch (ContactControlException e) {
             String infoKey;
             switch (e.getExceptionId()) {
             case ContactControlException.EX_ADDRESS_INCOMPLETE:
                 infoKey = "AM_NotEditable_AddressIncomplete";
                 break;
             case ContactControlException.EX_ADDRESS_NO_SUBCATEGORY:
                 infoKey = "AM_NotEditable_NoSubCategory";
                 break;
             case ContactControlException.EX_EMAIL1_INVALID:
                 infoKey = "AM_Discard_Email1Invalid";
                 break;               
             case ContactControlException.EX_EMAIL2_INVALID:
                 infoKey = "AM_Discard_Email2Invalid";
                 break;                
             case ContactControlException.EX_PHONE_INVALID:
                 infoKey = "AM_Discard_PhoneInvalid";
                 break;                
             default:
                 infoKey = "AM_NotEditable_Other";
             }
             logger.log(Level.INFO, getControlListener().getMessageString(infoKey), e); 
             //showInfo(getControlListener().getMessageString(infoKey));
             
             Object[] options = {
            		 		Messages.getString("AM_OptionDialog_Abort_Button"), 
            		 		Messages.getString("AM_OptionDialog_Save_Anyway_Button")};
             int choice = JOptionPane.showOptionDialog(
            		 getControlListener().getFrame(),
            		 getControlListener().getMessageString(infoKey),
            		 Messages.getString("AM_OptionDialog_Incorrect_Address"),
            		 JOptionPane.YES_NO_OPTION,
            		 JOptionPane.WARNING_MESSAGE,
            		 null,     //don't use a custom Icon
            		 options,  //the titles of buttons
            		 options[0]); //default button title
             
             return choice == 1;
             
         } catch (ContactDBException sex) {
             publishError(getMessageString("AM_NotEditable_DbError"), sex);
             return false;
         } catch (ContactGuiException e) {
             publishError(getMessageString("AM_NotEditable_GuiError"), e);
             return false;
         }    	
    	
    	return true;
    }
    
    /**
     * Diese Methode speichert eine neue oder bearbeitete Adresse in der GUI ab
     * und �berf�hrt die GUI in einen Nicht-Bearbeiten-Modus.
     * 
     * <p>The caller <b>must</b> call {@link #takeAddressDataFromGui()} first
     * in order to make the data to be saved current.</p> 
     */
    protected void saveAddress() throws ContactDBException, ContactControlException {
    	logger.info("saving contact...");
    	
    	currentAddress.trimAddressData();
    	
    	getEventsManager().fireSavingAddressChanges(currentAddress, getAddressPosition());

    	if (isNewAddressMode()) {
    		List subCategories = getControlListener().requestSubCategoriesForNewAddress(currentAddress);

    		// Do not save & do not switch context if user opted to abort the operation or user did not select any category.
    		if (subCategories == null || subCategories.size() == 0)
    			throw new ContactControlException(ContactControlException.EX_SAVING_INTERRUPTED);

    		// Translates the List<SubCategory> into a list of their IDs.
    		List subCategoryPks = new ArrayList(subCategories.size());
    		for (Iterator iter = subCategories.iterator(); iter.hasNext();) {
    			subCategoryPks
    			.add(((SubCategoryImpl) iter.next()).getIdAsInteger());
    		}

    		getDb().getAddressDAO().create(currentAddress, subCategoryPks);
    	}
    	else {
    		//save
    		getDb().getAddressDAO().save(currentAddress);
    	}
    	//reload(false);
    	getEventsManager().fireAddressChanged(currentAddress, getAddressPosition());

    	if (isNewAddressMode()) {//switch from NEW to EDIT mode 
    		switchMode(NEW_MODE, EDIT_MODE);
    		reload(false);
    		updateTotalAddressCount();
    	}
    	getEventsManager().fireSetAddress(currentAddress, true);
    }
    
    /** 
     * Sets a new value of a total address count in a data model.
     * A value will be loaded from DB.
     * 
     */
    protected void updateTotalAddressCount() {
      try {
          applicationModel.setTotalAddressCount(getDb().getAddressCount());
      } catch (ContactDBException e) {
          throw (IllegalStateException) new IllegalStateException("Unable to retrieve total address cound").initCause(e);
      }
    }
    
    private void checkDataIntegrity() throws ContactDBException, ContactControlException, ContactGuiException {
        //get empty object
        Address addressToSave = getDb().createAddress();
        //fill with data
        getControlListener().getAddress(addressToSave);
        
        //check integrity
        /**
         * removed completion check
         */
        /*
        if (!addressToSave.isComplete())
            throw new ContactControlException(ContactControlException.EX_ADDRESS_INCOMPLETE,
                                              getControlListener().getMessageString("AM_Exception_AddressIncomplete"));
      	*/
        if(!addressToSave.checkEmailDienstlich())
            throw new ContactControlException(ContactControlException.EX_EMAIL1_INVALID,
                                              getControlListener().getMessageString("AM_Discard_Email1Invalid"));
        if(!addressToSave.checkEmailPrivat())
            throw new ContactControlException(ContactControlException.EX_EMAIL2_INVALID,
                                              getControlListener().getMessageString("AM_Discard_Email2Invalid"));
        if(!addressToSave.checkPhonenumbers())
            throw new ContactControlException(ContactControlException.EX_PHONE_INVALID,
                                              getControlListener().getMessageString("AM_Discard_PhoneInvalid"));
    }

    /**
     * Diese Methode stellt, falls vorhanden, die Adresse aus der aktuellen 
     * Kategorie bereit, die angezeigt werden soll.
     */
    protected void setAddress() {
        setAddress(true);
    }

    /**
     * This method prepares an address (if exists) from the current category to be shown. 
     * 
     * @param 'publish' is a boolean flag, wether an error message should be directly published (if any occurs)
     * @return error message or null if no errors  
     */
    protected String setAddress(boolean publish) {
        if (isNewAddressMode())
            return null;
                
        Address newAddress = null;
        if (currentListOfAddresses == null) {
            String error = getControlListener().getMessageString("AM_Address_Set_NoAddresses");
            if (publish) publishError(error);
            return error;
        }

        int indexMax = currentListOfAddresses.getIndexMax();
        if (indexMax < -1) {
            String error = getControlListener().getMessageString("AM_Address_Set_AddressesNotLoaded");
            if (publish)
                publishError(error);
            else
                return error;
        } else if (indexMax == -1) {
            logger.info("[!] selection not possible");
        } else try {
            if (getAddressPosition() < 0) {
                if (currentAddress != null)
                    setAddressPosition(getAddressPosition(currentAddress));
                if (getAddressPosition() < 0)
                    setAddressPosition(0);
            } else if (getAddressPosition() > currentListOfAddresses.getIndexMax())
                setAddressPosition(currentListOfAddresses.getIndexMax());
            newAddress = currentListOfAddresses.get(getAddressPosition());
        } catch (Exception e) {
            String error = getControlListener().getMessageString("AM_Address_Set_Error");
            if(publish) publishError(error, e);
            return error;
        }

        if (newAddress == null) {
            try {
                //create a dummy-object in order to refresh TAB-containers
                newAddress = getDb().createAddress();
            } catch (ContactDBException se) {
                String error = Messages.getString("AM_Error_NoDummyAddress");
                if (publish) publishError(error, se);
                return error;
            }
        }
        currentAddress = newAddress;

        if (setAddressAllowed) try {
            getEventsManager().fireSetAddress(currentAddress, false);
            getEventsManager().fireSelectAddress(getAddressPosition());
            getEventsManager().fireSetNavigationCounter(getAddressPosition() + 1, indexMax + 1);
        } catch (ContactDBException se) {
            publishError(getControlListener().getMessageString("AM_Address_Set_DbError"), se);
        }
        
        applicationModel.setSelectedAddress(currentAddress);
        // Changing the selected address, changes the category assignment
        // as well. So we need to tell everybody about this.
        applicationModel.setCategoryAssignment(getAssociatedCategoriesForSelectedAddress());
        
        return null;
    }

    /**
     * Hilfsmethode f�r die Doublettensuche. 
     * Schreibt die ausgew�hlte Doublette ins Mainframe.
     * @param a Die Doublette, die ausgew�hlt wurde.
     */
    public void setDoubleAdd(Address a) {
        try {
            currentAddress = a;
            getEventsManager().fireSetAddress(a, false);
            getEventsManager().fireSetNavigationCounter(-1, currentListOfAddresses.getIndexMax() + 1);
            if (isNewAddressMode()) {
                switchMode(NEW_MODE, EDIT_MODE);
            }
            if (isEditMode()) {
                Veto veto = getDb().checkForVeto(VetoableAction.editAddress(getAddress(), getDb().getUser()));
                if (veto != null) {
                    String caption = getControlListener().getMessageString("AM_Address_SetDouble_VetoErrorCaption");
                    if (veto.isForbidden())
                        publishError(caption, veto.getMessage());
                    else
                        showInfo(caption, veto.getMessage());
                    getEventsManager().fireEditableVeto(veto, !veto.isForbidden());
                }

            }
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_Address_SetDouble_DbError"));
        }
    }

    /**
     * Eine neue Adresse wird in der Datenquelle angelegt und dem
     * ControlListener als anzuzeigen �bermittelt.
     */
    protected void setNewAddress() {
        try {
            currentAddress = getDb().createAddress();
            getEventsManager().fireSetAddress(currentAddress, true);
            getEventsManager().fireSetNavigationCounter(-1, (currentListOfAddresses != null) ? currentListOfAddresses.getIndexMax() + 1 : -1);
        } catch (ContactDBException se) {
            publishError(getControlListener().getMessageString("AM_Address_SetNew_DbError"), se);
        }
    }

    /**
     * Diese Methode liefert den Index einer Adresse in der aktuellen Adressauswahl.
     * 
     * @param address zu suchende Adresse.
     * @return Position in der aktuellen Adressauswahl; -1, falls nicht gefunden
     * @throws ContactDBException
     */
    protected int getAddressPosition(Address address) throws ContactDBException {
        int adrNr = address.getAdrNr();
        int indexMax = currentListOfAddresses.getIndexMax();
        for (int i = 0; i <= indexMax; i++) {
            if (i % 100 == 0) {
                int waitIndex = i + 100;
                if (waitIndex > indexMax) waitIndex = indexMax;
            }
            if (currentListOfAddresses.getAddressNumber(i) == adrNr)
                return i;
        }
        return -1;
    }
    
    
    //
    // Methoden, die auf der aktuellen Adressselektion arbeiten
    //
//     /**
//      * �ndert die aktuelle Kategorie, -
//      * falls der Benutzer wieder dieselbe ausw�hlt passiert nichts, 
//      * falls eine andere gew�hlt wurde, wird diese aus der Datenbank geholt.
//      * @param newVerteilergruppe Wird �ber die Auswahl vom Benutzer gesetzt. - 
//      * Bestimmt die Kategorie, die geholt werden soll. 
//      */
//     protected void changeVerteilergruppe(String newVerteilergruppe) {
//         Addresses newAddresses;
//         String oldVerteilergruppe = getDb().getVerteilergruppe();
//         boolean oldVerteilerIntersection = getDb().isVerteilerSelectionIntersection();

//         // Rausspringen, wenn sich die Kategorie nicht ge�ndert hat.
//         if (useSpecialAddressSet) {
//             checkFilterContext();
//             useSpecialAddressSet = false;
//         } 
//         else if (newVerteilergruppe.equals(oldVerteilergruppe)) 
//             return;

//         try {
//             getDb().setVerteilergruppe(newVerteilergruppe);
//             getDb().setAllgemeineAddressen(getEinbeziehenAllgemeinerAdressen());
//             getDb().setVerteilerSelectionIntersection(false);
//             newAddresses = getDb().getChosenAddresses();
//             getPreferences(null).put(Key.START_VERTEILERGRUPPE.toString(), newVerteilergruppe);
//         } catch (ContactDBException e) {
//             publishError(getControlListener().getMessageString("AM_Category_Change_DbError"), e);
//             return;
//         }
//         String error = null;
//         if ((error = loadVerteilergruppe(newAddresses, 0, false)) != null) {
//             try {
//                 publishError(error);
//                 getDb().setVerteilergruppe(oldVerteilergruppe);
//                 getDb().setVerteilerSelectionIntersection(
//                         oldVerteilerIntersection);
//                 newAddresses = getDb().getChosenAddresses();
//             } catch (ContactDBException e) {
//                 publishError(getControlListener().getMessageString("AM_Category_ChangeUndo_DbError"), e);
//                 return;
//             }
//             loadVerteilergruppe(newAddresses, -1, true);
//         } else {
//             try {
//                 getEventsManager().fireCategoryChanged( getDb().getVerteilergruppe(newVerteilergruppe) );            
//             } catch (ContactDBException e) {
//                 publishError(getControlListener().getMessageString("AM_Category_Show_DbError"), e);
//             }
//         }
//     }


    /**
     * Loads the initial address set for the user.
     */
    public void loadInitialAddresses() {
        try {
            AddressesImpl newAddresses = (AddressesImpl)getDb().getChosenAddresses();

            // blocks until the first request returns
            newAddresses.startLoading();
            load(newAddresses, 0, true);
        } catch (Exception e) {
            publishError(getControlListener().getMessageString("AM_SubCategory_GetByAddress_DbError"), e);
        }
    }

    
    /**
     * Diese Methode holt die Adresse mit dem als Parameter angegebenen Index.
     *                                          
     * TODO: Addresszugriff: Implement a wait method for addresses not already loaded.
     * 
     * @param addressIndex Index der Adresse in der aktuellen Adressauswahl.
     * @return Adresse an dem angegebenen Index, sonst null
     */
    public Address getAddressByIndex(int addressIndex) {
        Address newAddress = null;

        if (currentListOfAddresses != null) {
            int indexMax = currentListOfAddresses.getIndexMax();
            if (indexMax >= 0 && addressIndex <= indexMax) try {
                newAddress = currentListOfAddresses.get(addressIndex);
            } catch (IndexOutOfBoundsException e) {
                publishError(getControlListener().getMessageString("AM_Address_GetByIndex_Invalid"), e);
            }
        }

        return newAddress;
    }

    /**
     * Sets the AddressListParameter and triggers the refresh of the addresslist according to loading this parameters.
     * Normaly this method should not be called directly. It is intended to be called by the BindingManager whenever the 
     * AddressListParamert Object in the Application Model changes.
     * <p>The List of addresses is not directly linked to the change event in the model, 
     * because the the action manager has to control the reload. (e.g. for saving of unsaved data)</p>
     */
    public void setAddressListParameter(AddressListParameter alp) {
        if (currentListOfAddresses != null)
            currentListOfAddresses.setAddressListParameter(alp);
        
        if (alp.isSearchFilterEnabled() || alp.isCategoryFilterEnabled() || alp.getPkFilter() != null)
            setSearchMode();
        else 
            setViewMode();

        setAddressPosition(0);
    }

    /**
     * Implementation of the interface EntityListListener.
     * This method reacts on changes in the addresslist.
     */
    public void entityListChanged(EntityListEvent e) {
        Address addressAtSelection = null;
        if (getAddressPosition() < currentListOfAddresses.getSize())
            addressAtSelection = (Address)currentListOfAddresses.getEntityAt(getAddressPosition());
        if (addressAtSelection != null && addressAtSelection != currentAddress)
            setAddress();
    }

    /**
     * Sets a list of address pks as the base address set. This prefil until 
     * @param pkList List of Integer pk Objects
     * @param label Caption to identify the selection by the user, null means all-addresses
     */
    public void setAddressSet(List pkList, String label) {
        AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
        alp.setPkFilter(pkList);
        alp.setAddressSourceLabel(label);
        ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
    }
    
    /**
     * L�dt die Kategorie, die als Parameter �bergeben wird.
     * Die erste Adresse aus der Kategorie wird f�r die Anzeige 
     * im Mainframe geholt.
     * 
     * @param newAddresses Ein Objekt vom Typ Addresses wird �bergeben.
     * @param index der Index, auf den positioniert werden soll.
     * @param publish bestimmt, ob Fehler in Messageboxen ausgegeben werden sollen.
     */
    protected String load(Addresses newAddresses, int index, boolean publish) {
        if (currentListOfAddresses != null) 
            currentListOfAddresses.removeEntityListListener(this);

        currentListOfAddresses = newAddresses;

        currentListOfAddresses.addEntityListListener(this);

        getEventsManager().fireAddressesChanged(currentListOfAddresses);
        applicationModel.setAddressList(currentListOfAddresses);
        setAddressPosition(index);
        return setAddress(publish);
    }

    /**
     * Reloads the current address set
     */
    protected void reload(boolean keepAddressPosition) {
        currentListOfAddresses.reload();
        if (!keepAddressPosition)
            setAddressPosition(0);
        getEventsManager().fireAddressesChanged(currentListOfAddresses);
        setAddress(true);
    }
    
    /**
     * Retrieves the current set of addresses from the
     * server.
     * 
     * <p>In contrast to {@link #reload(boolean)} this represents
     * a public interface to the functionality used by actions.</p>
     *
     */
    public void reloadAddresses()
    {
    	reload(false);
    }
 
//     /**
//      * Liefert den Selectstatus der Unterkategorien, die im Verteilerchooser
//      * angezeigt werden sollen. Die Keys sind die Verteilerschl�ssel und die
//      * Values null, wenn die Unterkategorie nicht ausgew�hlt ist und != null,
//      * sonst.
//      * 
//      * @return Map mit String Keys, und Values, die null sind oder auf irgend
//      *         ein Objekt zeigen, Bei einem DB Fehler wird null zur�ck gegeben.
//      */
//     public SortedMap getVerteilerSelectState() {
//         try {
//             return getDb().getVerteilerSelectState();
//         } catch (ContactDBException se) {
//             publishError(
//                     getControlListener().getMessageString("AM_SubCategory_GetSelect_DbError"),
//                     se);
//             return null;
//         }
//     }

    /**
     * Diese Methode liefert den aktuellen Verteilerauswahlmodus.
     * 
     * @return der aktuelle Modus,
     *         {@link GUIListener#VERTEILER_MODE_ANY VERTEILER_MODE_ANY}oder
     *         {@link GUIListener#VERTEILER_MODE_ALL VERTEILER_MODE_ALL}.
     */
    public int getVerteilerSelectMode() {
        return getDb().isVerteilerSelectionIntersection() ? GUIListener.VERTEILER_MODE_ALL
                : GUIListener.VERTEILER_MODE_ANY;
    }

//     /**
//      * Setzt den neuen Status der Unterkategorieanzeigeauswahl �ber eine Map.
//      * Die Keys sind die Unterkategorieschl�ssel und die Values null, wenn die
//      * Unterkategorie nicht ausgew�hlt werden soll, und != null sonst.
//      * 
//      * @param map
//      *            Map mit String Keys, und Values, die null sind oder auf
//      *            irgend ein Objekt zeigen.
//      * @param mode
//      *            der Verteilerauswahlmodus,
//      *            {@link GUIListener#VERTEILER_MODE_ANY VERTEILER_MODE_ANY}oder
//      *            {@link GUIListener#VERTEILER_MODE_ALL VERTEILER_MODE_ALL}.
//      */
//     public void selectVerteiler(Map map, int mode) {
//         Addresses newAds;
//         try {
//             setWaiting(true);
//             for (Iterator i = map.keySet().iterator(); i.hasNext();) {
//                 String key = asString(i.next());
//                 if (map.get(key) != null)
//                     getDb().selectVerteiler(key, true);
//                 else
//                     getDb().selectVerteiler(key, false);
//             }
//             getDb().setAllgemeineAddressen(allgemeineAdressen);
//             getDb().setVerteilerSelectionIntersection(
//                     mode == GUIListener.VERTEILER_MODE_ALL);
//             newAds = getDb().getChosenAddresses();
//             if (useSpecialAddressSet) {
//                 checkFilterContext();
//                 useSpecialAddressSet = false;
//             }
//             loadVerteilergruppe(newAds, -1, true);
//         } catch (ContactDBException se) {
//             publishError(
//                     getControlListener().getMessageString("AM_SubCategory_Select_DbError"),
//                     se);
//             return;
//         } finally {
//             setWaiting(false);
//         }
//     }

//     /**
//      * Diese Methode setzt die zugrundeliegende Adressauswahl.
//      * 
//      * @param addresses
//      *            die neue Adressauswahl; falls <code>null</code>, so wird
//      *            auf die normale Adressauswahl zur�ckgeschaltet.
//      * @param info
//      *            Information �ber die Auswahl.
//      */
//     public void setAddressSet(Addresses addresses, String info, int type) {
//         if (addresses == null) {
//             useSpecialAddressSet = false;
//             setViewMode();
//         } else {
//             currentListOfAddresses = addresses;
//             useSpecialAddressSet = true;
//             getControlListener().setFilterActiveIcon(true, info, type);
//         }
//         reloadVerteilergruppe(false);
//     }

    //
    // ControlListener-Zugriffsmethoden
    //
    /**
     * Diese Methode zeigt, ob derzeit die Darstellungsschicht von
     * einem Suchkontext ausgeht.
     */
    public boolean isSearchActive() {
        return isSearchMode();
    }

    /**
     * Diese Methode liefert den aktuellen Suchmodus der Darstellungsschicht.
     */
    public int getSearchMode() {
        return (getControlListener().getSearchMode());
    }
    
    
    
    public boolean loadPreviews(int from, int to)
    {      
      return false;
    }
    
    private String m_sSortByID = null;
    private boolean m_sSortAscending = true;
    
    // TODO: Addresszugriff: sort!
    public boolean sortPreviewBy(String id, boolean ascending)
    {
//         m_sSortByID = id;
//       m_sSortAscending = ascending;
      
//       currentListOfAddresses.setSortField(m_sSortByID);
//       try
//       {
//           currentListOfAddresses.sortAddressesByPreview();
//       }
//       catch (ContactDBException e)
//       {
//         e.printStackTrace();
//       }
      
//       //m_oTransitionList = createTransitionList();      
      
      return true;
    }
    
    
    private List m_oTransitionList = null;
    
    private class TransitionListElement
    {
      private char m_cChar;
      private int  m_iIndex;
      
      public TransitionListElement(char c, int index)
      {
        m_cChar = c;
        m_iIndex = index;        
      }
      
      public char getChar()
      {
        return m_cChar;
      }
      
      public int getIndex()
      {
        return m_iIndex;
      }
    }
    
    private List createTransitionList()
    {
      List list = new ArrayList();

      if (currentListOfAddresses.getIndexMax() > 0)
      {            
        char cLastChar = getFirstChar(0);
        list.add(new TransitionListElement(cLastChar, 0));
        if (currentListOfAddresses.getIndexMax() > 1)
        {      
          for(int i=1; i<=(currentListOfAddresses.getIndexMax()); i++)
          {
            char cChar = getFirstChar(i);
            
            if (cChar != cLastChar)
            {
              list.add(new TransitionListElement(cChar, i));
            }
          }
        }
      }
      return list;
    }
    
    private char getFirstChar(int index)
    {
      Address adr = getAddressByIndex(index);
      AddressPluginData data = new AddressPluginData(adr);
      String text = (String)(data.get(m_sSortByID));
      return text.toUpperCase().charAt(0);
    }
    
    
    
    //private LetterFinder m_oLetterFinder = new LetterFinder();     
    //TODO: Simon: implement
    public int getIndexOfAddressByLetter(String startswith)
    {
      if (m_sSortByID == null) return -1;
      if (m_oTransitionList == null) return -1;
      
      char cChar = startswith.toUpperCase().charAt(0);      
      Iterator it = m_oTransitionList.iterator();
      while(it.hasNext())
      {
        TransitionListElement el = (TransitionListElement)(it.next());
        if (el.getChar() == cChar) return el.getIndex();
      }
      return -1;      
    }
    
    
    //
    // private Filhsmethoden
    //
    /**
     * Diese Methode setzt den Filter Kontext passend zum Search-Status der DB-Schicht. Sie ist nur einzusetzen,
     * wenn sicher die Standard-Address-Auswahl �ber Kategorie/Unterkategorie/ggfs Textfilter genommen wird,
     * nicht exceptionell.
     */
    private void checkFilterContext() {
        if (getDb().isSearch())
            setSearchMode();
        else
            setViewMode();
    }

}
