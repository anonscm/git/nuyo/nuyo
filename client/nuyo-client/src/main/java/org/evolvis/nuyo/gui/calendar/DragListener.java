package org.evolvis.nuyo.gui.calendar;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.List;

import javax.swing.event.MouseInputAdapter;

import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.util.DateRange;


/*
 * Created on 17.07.2003
 *
 */

/**
 * @author niko
 *
 */
public class DragListener extends MouseInputAdapter 
{
  private SchedulePanel m_oSchedulePanel;
  private ScheduleData m_oScheduleData;
  private boolean m_bIsInResizeMode = false;
  private int m_iOffsetX = 0;
  private int m_iOffsetY = 0;
  private Point m_oPos;
  private Point m_oLastPosition = new Point(0,0);
  private Point m_oInitialPosition = new Point(0,0);
  private int m_oLastHeight = 0;
  
  int movedY;
  int movedX;
  
  private ScheduleEntryPanel m_oScheduleEntry;
    
  private Cursor m_oNormalCursor; 
  private Cursor m_oResizeCursorBottom; 
  private Cursor m_oMoveCursor; 
  private int m_iResizeFuzzyBorder = 5;
  
  private int m_iNumDays;
  
  private int m_iNumClicksToEdit = 1;
  
  private int m_AppointmentID;
  
  private boolean m_bIsEnabled = true;
  private boolean m_bIsSecretaryMode = false;
  private boolean m_bIsSelectOnly  = false;
  private DateRange m_oWriteDateRange ; 
  
  public DragListener(ScheduleEntryPanel comp, SchedulePanel sp)
  {
    m_oSchedulePanel = sp;
    m_oScheduleData = sp.getScheduleData();
    m_oScheduleEntry = comp;
    m_AppointmentID = m_oScheduleEntry.getAppointment().getId();
    m_iNumDays = sp.getNumDays();
    m_oPos = comp.getLocation();
    m_oNormalCursor = new Cursor(Cursor.DEFAULT_CURSOR);
    m_oResizeCursorBottom = new Cursor(Cursor.S_RESIZE_CURSOR);
    m_oMoveCursor = new Cursor(Cursor.MOVE_CURSOR);
    this.setConstraints(0, 0, (m_oScheduleData.dayWidth * m_iNumDays), m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours);
    
    // Check if we are in Secretary Mode and have to restrict movements
    CalendarSecretaryRelation calSecRel;
	try {
		calSecRel = comp.getAppointment().getOwner().getCalendar(true).getCalendarSecretaryforUser(m_oSchedulePanel.getGUIListener().getUser(null, true).getId());
		if (calSecRel != null && calSecRel.getWriteDateRange().isDefined()){
	    	m_oWriteDateRange = 	calSecRel.getWriteDateRange();
	    	m_bIsSecretaryMode = true;
	    	//System.out.println("DRAGLISTENER: CALSEC found: " + m_oWriteDateRange.getStartDate().toString());
	    }
	} catch (ContactDBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      
  }
    
  public boolean isEnabled()
  {
    return m_bIsEnabled;
  }
  
  public void setEnabled(boolean enabled)
  {
    m_bIsEnabled = enabled;
  }
  
  public void setSelectOnly(boolean selectonly){
	  m_bIsSelectOnly = selectonly;
  }
  
  private void setConstraints(Rectangle constraints)
  {
    m_oScheduleData.minPosX = constraints.x;
    m_oScheduleData.minPosY = constraints.y;
    m_oScheduleData.maxPosX = constraints.width;
    m_oScheduleData.maxPosY = constraints.height;          
  }
    
  private void setConstraints(int minx, int miny, int maxx, int maxy)
  {
    setConstraints(new Rectangle(minx, miny, maxx, maxy));
  }
  
  private boolean crossesDayStartBorder(Point pos){
      return(pos.y < m_oScheduleData.minPosY);
  }

  private boolean crossesDayEndBorder(Point pos){
      return(pos.y > (m_oScheduleData.maxPosY - m_oScheduleEntry.getHeight()));
  }

  private Point ensurePositionConstraints(ScheduleEntryPanel sep, Point pos)
  {
    Point _pos = pos;
    if (pos.x > (m_oScheduleData.maxPosX - sep.getWidth())) _pos.x = m_oScheduleData.maxPosX - sep.getWidth();
    if (pos.y > (m_oScheduleData.maxPosY - sep.getHeight())) _pos.y = m_oScheduleData.maxPosY - sep.getHeight();      
    if (pos.x < m_oScheduleData.minPosX) _pos.x = m_oScheduleData.minPosX;
    if (pos.y < m_oScheduleData.minPosY) _pos.y = m_oScheduleData.minPosY;
    return(_pos);
  }

  private Dimension ensureSizeConstraints(Dimension dim, Point Position)
  {
    if (dim.height < m_oScheduleData.minHeightOfEntry) dim.height = m_oScheduleData.minHeightOfEntry;
    if ((dim.height + Position.y) > ((m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours))) dim.height = (((m_oScheduleData.hourHeight * m_oScheduleData.numberOfHours)) - Position.y);  
    return(dim); 
  }
    
  private Point snapToGrid(Point pos)
  {
    int linewithmindelta = m_oSchedulePanel.getDayOfPos(m_oScheduleEntry, pos);
    if (linewithmindelta != -1) pos.x = (linewithmindelta * m_oScheduleData.dayWidth);              
    return(pos);
  }
    
  private boolean isAtResizePosition(Point mousepos)
  {
    
	  if (m_bIsSelectOnly) return false;
    
    // Only the last SEP can be resized!
    if (SEPRegistry.getInstance().getSEPCount(m_AppointmentID) > 1){
        //MultiDay
        //Cheaper Op first
        if (mousepos.y > (m_oScheduleEntry.getHeight() - m_iResizeFuzzyBorder)){
            return (SEPRegistry.getInstance().getLastSEP(m_AppointmentID).getID() == m_oScheduleEntry.getID());
        }else
            return false;     
    }else{
        if (mousepos.y > (m_oScheduleEntry.getHeight() - m_iResizeFuzzyBorder))
            return true;
        else
            return false;
    }
  }

  private boolean setCursorForPosition(Point p)
  {
    boolean isresize = isAtResizePosition(p);
    if (isresize && (!(m_oScheduleEntry.isJob()))) m_oScheduleEntry.setCursor(m_oResizeCursorBottom);
    else          //  m_oScheduleEntry.setCursor(m_oNormalCursor);
        m_oScheduleEntry.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    return(isresize);
  }
    
    
  private boolean m_bMoveButtonDown = false; 
  
  public void mouseDragged(MouseEvent e) 
  {
/**   
 * Important: while moving/dragging/whatever only the SEP that is grabbed receives this event(s),
 *     			  so we have to update all the other SEP's related to the current Appointment from here.
 *   			  This is archived by fireing the appropriate ScheduleEntryListener events, so the SEP's know
 *   			  that it's time to update their displays.
 *
 * Short logic rundown:
 * 		
 * 		multiday appointment?
 * 			movement up? 		
 * 				move first sep
 *	 				crosses daystartborder?
 * 						create new first sep
 * 						resize last sep
 * 						last too small?
 * 							erase
 * 			movement down?
 * 				move last sep
 * 					crosses dayendborder?
 * 						create new last sep
 * 						resize first sep	
 * 						first sep too small?
 * 							erase
 * 		one day appointment?
 * 			move sep
 * 			crosses daystartborder?
 * 				create new first sep
 * 				last sep too small?
 * 					erase
 * 			crosses dayendborder?
 * 				create new last sep
 * 				first sep too small?
 * 					erase
 * 			else
 * 				move normal 	
 **/
       
    if (isEnabled() && ! m_bIsSelectOnly)
    {  
	    if (m_bMoveButtonDown)
	    {
	        
	        //How much are we moving?
	    	
		      m_oScheduleEntry.setCursor(m_oMoveCursor);
		      movedX = e.getX() - (m_iOffsetX);
		      movedY = e.getY() - (m_iOffsetY);	      
		      if (m_oPos.y < 0)
		          m_iOffsetY += movedY; 
		      // Remember for undo (Secretary Mode)
		      Point backUpPosition = m_oLastPosition;
		      // Update ...
		      m_oLastPosition = m_oPos;
		      //System.out.println("movey:" + movedY);
		      
		      
	        if (m_bIsInResizeMode)
		    { 
	          Dimension dim = m_oScheduleEntry.getSize();
		      dim.height += (e.getY() - dim.height);
		      dim = ensureSizeConstraints(dim,m_oScheduleEntry.getLocation());
		      m_oScheduleEntry.setSize(dim);
		      m_oSchedulePanel.fireIsResizing(m_oScheduleEntry, true, m_oScheduleEntry.getHeight());
		    }else{ 
		      
		        if (SEPRegistry.getInstance().getSEPCount(m_AppointmentID) > 1){
		          /*
		          // Multiday
		          
		          if (movedY < 0){
		              
			          // Move First SEP
				      ScheduleEntry firstSEP = (ScheduleEntry) SEPRegistry.getInstance().getFirstSEP(m_AppointmentID);
				      
				      m_oPos = new Point(firstSEP.getLocation().x + movedX,firstSEP.getLocation().y+ movedY );
				      m_oPos = ensurePositionConstraints(m_oPos);
				      
				      if (crossesDayStartBorder(m_oPos)){
				          // 0:00 -> yesterday / Create New FIRST SEP
				          
				          int secondsoffset = m_oSchedulePanel.getSecondOfPos(m_oScheduleData.m_iMaxPosY - Math.abs(m_oScheduleData.m_iMinPosY - firstSEP.getLocation().y));
			              int duration = m_oSchedulePanel.getSecondOfPos(m_oScheduleData.m_iMaxPosY - firstSEP.getLocation().y);
			              ScheduleDate newstartDate =  firstSEP.getStartDate().getFirstSecondOfDay().getDateWithAddedDays(-1);
			              newstartDate.addSeconds(secondsoffset);
			              ScheduleEntry newSEP = new ScheduleEntry(m_oSchedulePanel,newstartDate,newstartDate.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-2),m_oScheduleEntry.isJob(),m_oScheduleEntry.getUserId());
			              m_oSchedulePanel.setDefaultFontsScheduleEntryPanel(newSEP);
			              newSEP.setAppointment(m_oScheduleEntry.getAppointment());
			              try {
			                  // Add new SEP to Registry
		                      SEPRegistry.getInstance().addSEP(newSEP,m_oSchedulePanel.getID(),SEPRegistry.SEPPOSITION_FIRST);
			              } catch (Exception e1) {
			                  // TODO Auto-generated catch block
			                  e1.printStackTrace();
			              }
	
			              // Add to SchedulePanel
			              newSEP.setActive(true);
			              m_oSchedulePanel.addScheduleEntrySilent(newSEP);
			              
			              // Adjust last SEP
			              ScheduleEntryPanel lastSEP = SEPRegistry.getInstance().getLastSEP(m_AppointmentID);
		                  Dimension dim = lastSEP.getSize();
			              dim.height += movedY;
			              if (dim.height <= 0){
		                      //Delete
		                      SEPRegistry.getInstance().removeSEP(lastSEP,m_oSchedulePanel.getID());
		                      lastSEP = null;
		                  }else{
		                      //Resize
				              dim = ensureSizeConstraints(dim);
				              lastSEP.setSize(dim);
				              m_oSchedulePanel.fireIsResizing(lastSEP, true, lastSEP.getHeight());
		                  }
	
				      }
		          }else if (movedY > 0){
		              // Movement down

			          // Move Last SEP
				      ScheduleEntry lastSEP = (ScheduleEntry) SEPRegistry.getInstance().getLastSEP(m_AppointmentID);
				      lastSEP.setLocation(lastSEP.getLocation().x + movedX,lastSEP.getLocation().y + movedY);
				      	      
				      if (crossesDayEndBorder(lastSEP.getLocation())){
				          // 0:00 -> tomorrow / Create New LAST SEP
				          int duration = m_oSchedulePanel.getSecondOfPos(m_oScheduleData.m_iMinPosY + movedY);
			              ScheduleDate newendDate =  lastSEP.getStartDate().getFirstSecondOfDay().getDateWithAddedDays(1);
			              newendDate.addSeconds(duration);
			              ScheduleEntry newSEP = new ScheduleEntry(m_oSchedulePanel,newendDate.getFirstSecondOfDay(),newendDate,m_oScheduleEntry.isJob(),m_oScheduleEntry.getUserId());
			              m_oSchedulePanel.setDefaultFontsScheduleEntryPanel(newSEP);
			              newSEP.setAppointment(m_oScheduleEntry.getAppointment());
			              try {
			                  // Add new SEP to Registry
		                      SEPRegistry.getInstance().addSEP(newSEP,m_oSchedulePanel.getID(),SEPRegistry.SEPPOSITION_LAST);
			              } catch (Exception e1) {
			                  // TODO Auto-generated catch block
			                  e1.printStackTrace();
			              }
	
			              // Add to SchedulePanel
			              newSEP.setActive(true);
			              m_oSchedulePanel.addScheduleEntrySilent(newSEP);
			              
			              // Adjust First SEP
			              ScheduleEntryPanel firstSEP = SEPRegistry.getInstance().getFirstSEP(m_AppointmentID);
			              Point p = firstSEP.getLocation();
			              p.y += movedY;
			              firstSEP.setLocation(p);
			              //ensurePositionConstraints(firstSEP.getLocation());
			              Dimension dim = firstSEP.getSize();
			              dim.height -= movedY;
			              if (dim.height <= 0){
		                      //Delete
		                      SEPRegistry.getInstance().removeSEP(firstSEP,m_oSchedulePanel.getID());
		                      firstSEP = null;
		                  }else{
		                      //Resize
				              dim = ensureSizeConstraints(dim);
				              firstSEP.setSize(dim);
				              m_oSchedulePanel.fireIsResizing(firstSEP, true, firstSEP.getHeight());
		                  }
	
				      }
		          }else{
		              // only X-Axis Movement
		              
		          }
		      */}
		      else{
		          // One Day
		          Point new_m_oPos = new Point(m_oScheduleEntry.getLocation().x + movedX,m_oScheduleEntry.getLocation().y + movedY );
		         /* if (crossesDayStartBorder(new_m_oPos)){
		              
			          // 0:00 -> yesterday / Create New FIRST SEP
			          
		              //Negative Movement -> time
			          int duration = m_oSchedulePanel.getSecondOfPos(- new_m_oPos.y);
			          // offset
			          int offset =   m_oSchedulePanel.getSecondOfPos(m_oLastPosition.y);
			          int moveit = duration + offset;
			          // New SEP Duration & Start 
		              ScheduleDate newstartDate =  m_oScheduleEntry.getStartDate().getFirstSecondOfDay().getDateWithAddedSeconds( - moveit );
		              ScheduleEntry newSEP = new ScheduleEntry(m_oSchedulePanel,newstartDate,newstartDate.getDateWithAddedSeconds(duration), ! m_oScheduleEntry.isJob(),m_oScheduleEntry.getUserId());
		              m_oSchedulePanel.setDefaultFontsScheduleEntryPanel(newSEP);
		              newSEP.setAppointment(m_oScheduleEntry.getAppointment());
		              try {
		                  // Add new SEP to Registry
	                      SEPRegistry.getInstance().addSEP(newSEP,m_oSchedulePanel.getID(),SEPRegistry.SEPPOSITION_FIRST);
		              } catch (Exception e1) {
		                  // TODO Auto-generated catch block
		                  e1.printStackTrace();
		              }

		              // Add to SchedulePanel
		              newSEP.setActive(m_oScheduleEntry.isActive());
		              m_oSchedulePanel.addScheduleEntrySilent(newSEP);
		              
		              // Adjust last SEP (this )
	                  Dimension dim = m_oScheduleEntry.getSize();
		              dim.height -= new_m_oPos.y;
		              if (dim.height <= 0){
	                      //Delete
	                      SEPRegistry.getInstance().removeSEP(m_oScheduleEntry,m_oSchedulePanel.getID());
	                      m_oScheduleEntry = null;
	                  }else{
	                      //Resize
	                      dim = ensureSizeConstraints(dim,m_oScheduleEntry.getLocation());
	                      m_oScheduleEntry.setSize(dim);
			              
			              m_oScheduleEntry.setLocation(m_oScheduleEntry.getLocation().x,0);
			              m_oSchedulePanel.fireIsResizing(m_oScheduleEntry, true, m_oScheduleEntry.getHeight());
	                  }

			      }*/
		          
		          
			      m_oPos = ensurePositionConstraints(m_oScheduleEntry,new_m_oPos);

			      // Calculate time to avoid moving into forbidden areas ... (Secretary Function)
			      if (m_bIsSecretaryMode){
			      	if (SEPRegistry.getInstance().getSEPCount(m_AppointmentID) == 1){
			      		// One SEP (me)
			      		ScheduleDate startDate = m_oSchedulePanel.computeStartDate(m_oScheduleEntry,m_oPos);
			      		ScheduleDate endDate = m_oSchedulePanel.computeEndDate(m_oScheduleEntry,m_oScheduleEntry.getHeight());
			      		
			      		if (! m_oWriteDateRange.containsDate(startDate.getDate()) || ! m_oWriteDateRange.containsDate(endDate.getDate()) ){
			      			// No no no ... back to square one ...
			      			m_oPos = backUpPosition;
			      			m_oPos.y += movedY;
			      			
			      			m_oPos = ensurePositionConstraints(m_oScheduleEntry,m_oPos);
			      			m_oLastPosition = m_oPos;
			      		}
						
			      	}else{
			      		// Multiday
				      	// First SEP
			      		ScheduleEntry firstSEP = (ScheduleEntry) SEPRegistry.getInstance().getFirstSEP(m_AppointmentID);
				      	// Last SEP
			      		ScheduleEntry lastSEP = (ScheduleEntry) SEPRegistry.getInstance().getFirstSEP(m_AppointmentID);
			      	}
			      }
			      m_oScheduleEntry.setLocation(m_oPos);
			      m_oSchedulePanel.fireIsMoving(m_oScheduleEntry, true, m_oScheduleEntry.getX(), m_oScheduleEntry.getY());
			   
		      }   
		    }
	    }
    }
  }
  
  public void mousePressed(MouseEvent e) 
  {
	  boolean stillExists = false;
	  try {
		  stillExists = m_oScheduleEntry.getAppointment().stillExists();
	  } catch (ContactDBException e1) {
		  // TODO Auto-generated catch block
		  e1.printStackTrace();
	  }
	  
	  if (stillExists){
		  m_oInitialPosition = new Point(m_oPos);
		  
		  if (isEnabled() && ! m_bIsSelectOnly)
		  {  
			  // Simulate doubleclick - set always active!  
			  m_oSchedulePanel.fireDoubleClicked(m_oScheduleEntry);
			  
			  m_oLastPosition = new Point(m_oPos);
			  m_oInitialPosition = new Point(m_oPos);
			  
			  m_oLastHeight = m_oScheduleEntry.getHeight();
			  
			  if (e.getButton() == MouseEvent.BUTTON1)
			  {  
				  m_bIsInResizeMode = setCursorForPosition(e.getPoint());
				  
				  if (m_oScheduleEntry.isJob())
				  {
					  if (m_bIsInResizeMode) return;
				  }        
				  
				  m_bMoveButtonDown = true;
				  m_iOffsetX = e.getX();
				  m_iOffsetY = e.getY();
				  m_oSchedulePanel.moveScheduleEntryToTop(m_oScheduleEntry);
				  if (m_bIsInResizeMode) 
					  m_oSchedulePanel.fireStartResizing(m_oScheduleEntry, m_oScheduleEntry.getHeight());
				  else 
					  m_oSchedulePanel.fireStartMoving(m_oScheduleEntry, m_oScheduleEntry.getX(), m_oScheduleEntry.getY());
			  }
		  }else{
			  m_oSchedulePanel.fireDoubleClicked(m_oScheduleEntry);
		  }
	  }else{
		  // App deleted
		  m_oSchedulePanel.getAppointmentDisplayManager().fireRemovedAppointment(m_oScheduleEntry.getAppointment(),null);
	  }
  }
    
  public void mouseReleased(MouseEvent e) 
  {
    m_bMoveButtonDown = false;
    m_oScheduleEntry.setCursor(m_oNormalCursor);

    if (m_bIsInResizeMode) 
    {
      if (! (m_oLastHeight == m_oScheduleEntry.getHeight()))
      {
        m_oSchedulePanel.fireStopResizing(m_oScheduleEntry, m_oScheduleEntry.getHeight());
      }
    }
    else
    {
        if (m_bIsSelectOnly) return;
       // if ( m_oInitialPosition.equals(m_oScheduleEntry.getLocation()))
       //     return; // Nothing moved, just selected, do nothing here
        
        if ( m_oInitialPosition.equals(snapToGrid(m_oScheduleEntry.getLocation())))
            return; // Nothing moved, just selected, do nothing here
            
	    // Loop all SEP'S
	    List allSEPS = SEPRegistry.getInstance().getSEPSforAppointmentID(m_AppointmentID);
	    Iterator iter = allSEPS.iterator();
	    
	    int changesPixels = 0;
        if (allSEPS.size() == 1){
            // One day
            m_oSchedulePanel.moveScheduleEntryToNormal(m_oScheduleEntry);
            m_oScheduleEntry.setScheduleWidth();
            m_oPos = snapToGrid(m_oScheduleEntry.getLocation());
            m_oScheduleEntry.setLocation(m_oPos);
            // Update Startdate ...
            m_oSchedulePanel.fireIsMoving(m_oScheduleEntry,true,m_oScheduleEntry.getX(),m_oScheduleEntry.getY());
            
        }else{
            // Multi day
            while( iter.hasNext()){
                // Snap to grid with first SEP and adjust last one's end / maybe Delete?
                ScheduleEntryPanel asep = (ScheduleEntryPanel) iter.next();
                if (asep.getID() == SEPRegistry.getInstance().getFirstSEP(m_AppointmentID).getID()){
                    Point oldPos = asep.getLocation();
              	  	Point newpos = snapToGrid(asep.getLocation());
              	  	if (asep.getID() == m_oScheduleEntry.getID()) m_oPos = newpos;
              	  	
              	  	// Save changes ...
              	  	changesPixels =  newpos.y - oldPos.y;
              	  	asep.setLocation(newpos);
              	  	asep.setSize(ensureSizeConstraints(asep.getSize(),asep.getLocation()));
              	    // Calculate new Date ..
              	  	m_oSchedulePanel.fireIsMoving(asep,true,asep.getX(),asep.getY());
              	  	
                }else if (asep.getID() == SEPRegistry.getInstance().getLastSEP(m_AppointmentID).getID()){
                    
                    asep.setSize(asep.getSize().width,asep.getSize().height + changesPixels);
                }
                
                m_oSchedulePanel.moveScheduleEntryToNormal(asep);
                m_oScheduleEntry.setScheduleWidth();
                
            }
        }
	    m_oSchedulePanel.fireStopMoving(m_oScheduleEntry, m_oScheduleEntry.getX(), m_oScheduleEntry.getY());
    }
  }        
    
  public void mouseEntered(MouseEvent e)
  { 
    if (isEnabled() && !m_bIsSelectOnly)
    {  
      setCursorForPosition(e.getPoint());
    }
  }
    
  public void mouseExited(MouseEvent e)
  {    
    if (isEnabled() ) if (! m_bIsInResizeMode) setCursorForPosition(e.getPoint());
  }

  public void mouseMoved(MouseEvent e)
  { 
    if (isEnabled())
    {       
      setCursorForPosition(e.getPoint());
    }
  }
   
  public void mouseClicked(MouseEvent e)
  {    
    
	// Anzahl klicks um in den Edit-Mode zu wechseln...
  //  if (e.getClickCount() == m_iNumClicksToEdit) m_oSchedulePanel.fireDoubleClicked(m_oScheduleEntry);
  }
 
}

