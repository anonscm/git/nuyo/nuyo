/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.util.Calendar;
import java.util.Date;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetConfigurableDateSpinner;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericIntegerField;



/**
 * @author niko
 *
 */
public class GeburtsjahrField extends GenericIntegerField
{
  // requires:
  // DATESPINNER_GEBURTSDATUM
  private YearDocumentListener m_oYearDocumentListener;
  
  
  public GeburtsjahrField()
  {
    super("GEBURTSJAHR", AddressKeys.GEBURTSJAHR, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_Geburtsjahr_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_Geburtsjahr", 60);
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    EntryLayoutHelper widget = super.getPanel(widgetFlags);
    if (widget != null)
    {  
      m_oYearDocumentListener = new YearDocumentListener();
      getIntegerField().getDocument().addDocumentListener(m_oYearDocumentListener);
      getWidgetPool().addWidget("INTEGERFIELD_GEBURTSJAHR", getIntegerField());
    }
    return widget;
  }
  
  
  
  private class YearDocumentListener implements DocumentListener
  {
    public void fieldChanged()
    {
      getIntegerField().getDocument().removeDocumentListener(m_oYearDocumentListener);
      try
        {
          int gebyear = ((Integer)(getIntegerField().getData())).intValue();
          Calendar calendar = Calendar.getInstance();
          TarentWidgetConfigurableDateSpinner spinner = (TarentWidgetConfigurableDateSpinner)(getWidgetPool().getWidget("DATESPINNER_GEBURTSDATUM"));
          if (spinner != null)
          {  
	          calendar.setTime(((Date)(spinner.getData())));
	          int year = calendar.get(Calendar.YEAR);
	          if (gebyear != year)
	          {
	            calendar.set(Calendar.YEAR, gebyear);
	            //TODO !!!
	            //spinner.setData(calendar.getTime());          
	          }
          }
        }
        catch(NumberFormatException nfe)
        {
        }
        getIntegerField().getDocument().addDocumentListener(m_oYearDocumentListener);        
    }

    public void insertUpdate(DocumentEvent e)
    {
      fieldChanged();
    }

    public void removeUpdate(DocumentEvent e)
    {
      fieldChanged();
    }

    public void changedUpdate(DocumentEvent e)
    {
      fieldChanged();
    }
  }
  
  
}
