/*
 * Created on 18.07.2003
 */
package org.evolvis.nuyo.gui.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Appearance.Key;



/**
 * @author niko
 *
  */
public class ScheduleEntry extends ScheduleEntryPanel implements CalendarVisibleElement
{
    private static Logger logger = Logger.getLogger(ScheduleEntry.class.getName());
    
    private final static DateFormat myTimeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT);
    private final static DateFormat myDateFormatter = DateFormat.getDateInstance(); 
    
    private JLabel m_oStartLabel;
    private JLabel m_oEndLabel;
    private JLabel m_oTypeLabel;  
    private JLabel m_oDescriptionLabel; 
    
    private Font m_oFontDate;
    private Font m_oFontType;
    private Font m_oFontDescription;
    private Font m_oMenuFont;
    
    private Color m_oColor;
    private Color m_oDarkerColor;
    private Color m_oDisabledColor;
    private Color m_oDisabledDarkerColor;
    
    
    private JPanel m_oNamePanel;  
    private ScheduleStartEndPanel m_oScheduleStartPanel;
    private ScheduleStartEndPanel m_oScheduleEndPanel;
    
    private String m_sName = "";
    private String m_sDescription = "";
    private String m_sTypeDescription = "";
    private String m_sTypeName = "";
    
    private ScheduleEntry m_oThisScheduleEntry;  
    private SchedulePanel m_oSchedulePanel = null;
    
    private ImageIcon m_oIconDelete;
    private ImageIcon m_oIconEdit;
    
    private boolean m_bCanEdit;
    
    
    
    public ScheduleEntry(SchedulePanel sp, ScheduleDate start, ScheduleDate end, boolean isJob, boolean isReleasedAndWritable, Integer userID, Color color)
    {
        super(sp, start, end, isJob, isReleasedAndWritable, userID); 
        m_oSchedulePanel = sp;
        m_oThisScheduleEntry = this;
        
        m_oIconDelete = sp.getIconDelete(); 
        m_oIconEdit = sp.getIconEdit(); 
        
        m_oStartLabel = new JLabel(createDateString(getStartDate().getDate()), SwingConstants.CENTER);
        m_oEndLabel = new JLabel(createDateString(getEndDate().getDate()), SwingConstants.CENTER);
        m_oTypeLabel = new JLabel("", SwingConstants.CENTER);
        m_oDescriptionLabel = new JLabel("", SwingConstants.CENTER);
        
        m_oScheduleStartPanel = new ScheduleStartEndPanel(true, 5, 10, Color.BLACK);
        m_oScheduleStartPanel.add(m_oStartLabel);
        m_oScheduleEndPanel = new ScheduleStartEndPanel(false, 5, 10, Color.BLACK); 
        m_oScheduleEndPanel.add(m_oEndLabel);
        
        m_oNamePanel = new JPanel(new BorderLayout());
        m_oNamePanel.add(m_oTypeLabel, BorderLayout.NORTH);
        m_oNamePanel.add(m_oDescriptionLabel, BorderLayout.CENTER);    
        
        this.setLayout(new BorderLayout());
        
        if (!isJob()) this.add(m_oScheduleStartPanel, BorderLayout.NORTH);
        this.add(m_oNamePanel, BorderLayout.CENTER);    
        if (!isJob()) this.add(m_oScheduleEndPanel, BorderLayout.SOUTH);
        
        setColor(color);
        
        // PopUpMenu einrichten...
        MouseListener popupListener = new PopupListener();
        this.addMouseListener(popupListener);        
        m_oNamePanel.addMouseListener(popupListener);
        m_oTypeLabel.addMouseListener(popupListener);
        m_oDescriptionLabel.addMouseListener(popupListener);    
        m_oStartLabel.addMouseListener(popupListener);
        m_oEndLabel.addMouseListener(popupListener);
        
        // --------------------------------------------------------------------------------
        setActive(false);
        
        //revalidateDisplay();  
        
        // Listener um �nderungen an der Gr��e des Panels zu erfahren...
        // ist n�tig um Abh�ngig von der Gr��e Labels ein-/auszublenden...
        this.addComponentListener(new LabelVisibilityListener());    
    }
    
    // ------------------------------------------------------------------------------------
    
    
    public void setFonts(Font menufont, Font textfont, Font datefont, Font typefont, Font descriptionfont)
    {
        m_oMenuFont = menufont;
        m_oFontDate = datefont;
        m_oFontType = typefont;
        m_oFontDescription = descriptionfont;
        
        m_oStartLabel.setFont(m_oFontDate);
        m_oEndLabel.setFont(m_oFontDate);
        m_oTypeLabel.setFont(m_oFontType);
        m_oDescriptionLabel.setFont(m_oFontDescription);
    }
    
    // ------------------------------------------------------------------------------------
    
    
    public void realized() 
    {
        computeLabelVisibility();
    }
    
    /**
     * wird aufgerufen wenn sich etwas an den Daten des dargestellten 
     *  Appointments ge�ndert hat und die Anzeige aufgefrischt werden mu�
     */
    public void revalidateDisplay()
    {
        super.revalidateDisplay();
        Appointment appointment = getAppointment();    
        if (appointment != null)
        {  
            try
            {
                // First SEP only
                if (SEPRegistry.getInstance().getFirstSEP(appointment.getId()).getID() == getID()){
                    // wenn das Start-Datum im Editor ver�ndert wird mu� geupdated werden
                    if (!(appointment.getStart().equals(getStartDate().getDate())))
                    {
                        //logger.log(Level.INFO, "SE::revalidateDisplay: FirstSep changes date!");
                        this.setStartDate(new ScheduleDate(appointment.getStart()));
                        updateSizeAndPosition();
                    }
                }
                // Last SEP only
                if (SEPRegistry.getInstance().getLastSEP(appointment.getId()).getID() == getID()){
                    // wenn das End-Datum im Editor ver�ndert wird mu� geupdated werden
                    if (!(appointment.getEnd().equals(getEndDate().getDate())))
                    {
                        //System.out.println("SE::revalidateDisplay:" + getID() +"\n" + appointment.getEnd().toLocaleString() + " != "+ getEndDate().getDate().toLocaleString() );
                        
                        // Simon: fixed bug #1001
                        if (isJob()) this.setStartDate(new ScheduleDate(appointment.getEnd()).getDateWithAddedHours(-1));
                        
                        this.setEndDate(new ScheduleDate(appointment.getEnd()));
                        updateSizeAndPosition();
                    }
                }
                
                // hier werden die Labels des Entries mit den Daten aus dem Appointment gef�llt...
                
                String name = appointment.getAttribute(Appointment.KEY_SUBJECT);
                String description = appointment.getAttribute(Appointment.KEY_DESCRIPTION);
                
                setScheduleType(m_oSchedulePanel.getScheduleData().getScheduleTypes().getScheduleType(appointment.getAppointmentCategory()));
                setScheduleLabelText(name, description);
                setDateLabels();            
                setLabels();      
                this.revalidate();
            } 
            catch (ContactDBException e)
            {
                logger.log(Level.SEVERE, "de.tarent.contact.gui.calendar.ScheduleEntry.revalidateDisplay: SEPRegistry has no SEP for Appointment.");
                e.printStackTrace();
            }
        }
    }
    
    
    public void refreshDisplay() 
    {
        setDateLabels();
    }
    
    /**
     *  dieser Listener soll abh�ngig von der Gr��e des Eintrags Labels ein-/ausblenden...
     */
    private class LabelVisibilityListener implements ComponentListener
    {
        public void componentHidden(ComponentEvent e) {}
        public void componentMoved(ComponentEvent e) {}
        
        public void componentResized(ComponentEvent e)
        {
            computeLabelVisibility();
        }
        
        public void componentShown(ComponentEvent e)
        {
            computeLabelVisibility();
        }    
    }
    
    
    // blendet abh�ngig von der Gr��e des Eintrags Labels ein bzw. aus
    private void computeLabelVisibility()
    {    
        Dimension paneldim = this.getSize();
        Dimension namepaneldim = m_oNamePanel.getSize();
        
        int startdateheight = getTextSize(m_oStartLabel.getText(), m_oFontDate).height;
        int enddateheight = getTextSize(m_oEndLabel.getText(), m_oFontDate).height;    
        int typeheight = getTextSize(m_oTypeLabel.getText(), m_oFontType).height;
        int descrheight = m_oDescriptionLabel.getPreferredSize().height;
        
        int availableheight = (paneldim.height) - ((startdateheight) + (enddateheight)) - (this.getInsets().bottom) - (this.getInsets().top) - 20;
        int availablenameheight = (namepaneldim.height) - 20;
        
        boolean showStartLabel = false;
        boolean showEndLabel = false;
        boolean showTypeLabel = false;
        boolean showDescriptionLabel = false;
        
        if (availableheight > (startdateheight + enddateheight + typeheight + descrheight))
        {
            showStartLabel = true;
            showEndLabel = true;
        }
        
        if (availablenameheight > (typeheight + descrheight))
        {
            showTypeLabel = true;
            showDescriptionLabel = true;      
        }
        else
        {
            showDescriptionLabel = true;      
        }
        
        m_oTypeLabel.setVisible(showTypeLabel);
        m_oDescriptionLabel.setVisible(showDescriptionLabel);
        
        if (m_oScheduleStartPanel.isActive())
        {
            m_oStartLabel.setVisible(showStartLabel);  
        }
        else
        {
            m_oScheduleStartPanel.setVisible(showStartLabel);      
        }
        
        
        if (m_oScheduleEndPanel.isActive())
        {
            m_oStartLabel.setVisible(showEndLabel);  
        }
        else
        {
            m_oScheduleEndPanel.setVisible(showEndLabel);      
        }
        
        this.revalidate();    
    }
    
    // ------------------------------------------------------------------------------------
    
    // ermittelt die Gr��e eines Textes in Pixeln...
    private Dimension getTextSize(String text, Font font)
    {
        if (text.length() > 0)
        {  
            Graphics2D g = (Graphics2D)(this.getGraphics());
            if (g != null)
            {
                FontRenderContext frc = g.getFontRenderContext();
                if (frc != null)
                {
                    TextLayout layout = new TextLayout(text, font, frc);
                    if (layout != null)
                    {
                        Rectangle2D bounds = layout.getBounds();
                        if (bounds != null)
                        {  
                            return new Dimension((int)(bounds.getWidth()), (int)(bounds.getHeight()));
                        }
                    }
                }
            }
        }
        
        return new Dimension(0,0);
    }
    
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    
    // an allen Componenten des Entries mu� gehorcht werden ob 
    // verschoben/vergr��ert etc. wurde...
    public void setDragListener(DragListener dl)
    {
        super.setDragListener(dl);
        addDragListenerToComponent(m_oNamePanel, dl);
        addDragListenerToComponent(m_oTypeLabel, dl);
        addDragListenerToComponent(m_oDescriptionLabel, dl);    
        addDragListenerToComponent(m_oStartLabel, dl);
        addDragListenerToComponent(m_oEndLabel, dl);
    }
    
    
    public void removeDragListener()
    {
        removeDragListenerFromComponent(m_oStartLabel);
        removeDragListenerFromComponent(m_oEndLabel);
        removeDragListenerFromComponent(m_oNamePanel);
        removeDragListenerFromComponent(m_oTypeLabel);
        removeDragListenerFromComponent(m_oDescriptionLabel);
        super.removeDragListener();
    }
    
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    
    
    public Color getColor()
    {
        return m_oColor;
    }
    
    private void setColorEditable(boolean iseditable)
    {
        if (m_oColor != null)
        {
            if (iseditable)
            {
                setColorToComponents(m_oColor, m_oDarkerColor);         
            }
            else
            {  
                setColorToComponents(m_oDisabledColor, m_oDisabledDarkerColor);         
            }
        }
    }
    
    public void setColor(Color color)
    {
        float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        
        if (isJob())
        {
            m_oColor = Color.getHSBColor(hsb[0]*0.9f, hsb[1], hsb[2]); 
            m_oDarkerColor = Color.getHSBColor(hsb[0]*0.9f, hsb[1], 0.2f); // Dark color (Labels)
        }else{
            m_oColor = color; 
            m_oDarkerColor = Color.getHSBColor(hsb[0], hsb[1], 0.2f); // Dark color (Labels)

        }
        
        //One disabled color
        m_oDisabledColor = Color.getHSBColor(hsb[0], (hsb[1] / 4.0f), hsb[2]);
        m_oDisabledDarkerColor = Color.getHSBColor(hsb[0], (hsb[1] / 4.0f), 0.2f);

        
        // Kleinis Version:
        /*  if (isJob())
         {
         m_oDisabledColor = Color.getHSBColor(hsb[0]*0.6f, (hsb[1] / 3.0f), hsb[2]);
         m_oDisabledDarkerColor = Color.getHSBColor(hsb[0]*0.f, (hsb[1] / 3.0f), hsb[2]*0.7f);
         }
         else
         {
         m_oDisabledColor = Color.getHSBColor(hsb[0], (hsb[1] / 3.0f), hsb[2]);
         m_oDisabledDarkerColor = Color.getHSBColor(hsb[0], (hsb[1] / 3.0f), hsb[2]*0.7f);
         }
         */    
        setColorToComponents(m_oColor, m_oDarkerColor);
    }
    
    
    private void setColorToComponents(Color color, Color darker)
    {
        m_oDescriptionLabel.setBackground(color);    
        m_oStartLabel.setForeground(darker);
        m_oEndLabel.setForeground(darker);
        m_oTypeLabel.setForeground(darker);
        m_oDescriptionLabel.setForeground(darker);
        this.setForeground(darker);
        m_oNamePanel.setBackground(color);
        this.setBackground(color);
    }
    
    
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    
    
    // blendet die Zickzack-Linie an Anfang ein-/aus
    public void setFuzzyStart(boolean fuzzystart)
    {    
        m_oScheduleStartPanel.setActive(fuzzystart);
    }
    
    // blendet die Zickzack-Linie an Ende ein-/aus  
    public void setFuzzyEnd(boolean fuzzyend)
    {    
        m_oScheduleEndPanel.setActive(fuzzyend);
    }
    
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    
    private String createDateString(Date date)
    {
        return myTimeFormatter.format(date);
    }
    
    public void setScheduleLabelText(String name, String description)
    {
        m_sName = name;
        m_sDescription = description;
    }
    
    public void setScheduleType(ScheduleType type)
    {
        if (type != null) 
        {
            m_oTypeLabel.setIcon(type.getIcon());
            m_sTypeDescription = type.getDescription();
            m_sTypeName = type.getName();
        }
        else
        {
            m_oTypeLabel.setIcon(null);
            m_sTypeDescription = "";
            m_sTypeName = "";
        }
    }
    
    
    public void setLabels()
    {
        
        String startdate = createDateString(getStartDate().getDate());
        String enddate = createDateString(getEndDate().getDate());
        
        String type = "";
        if (m_sTypeName.length() > 0)
        {
            if (m_sTypeDescription.length() > 0)
            {
                // type = "<i>" + m_sTypeName + " (" + m_sTypeDescription + ")</i><br>";
                type = "Typ: <i>" + m_sTypeDescription + "</i><br>";
            }
            else
            {
                type = "Typ: <i>" + m_sTypeName + "</i><br>";
            }
        }
        
        String edittext = "";
        if (! isAppointmentEditable())
        {
            edittext = "<br><br><font color=\"ff0000\">" + (isJob()? "Diese Aufgabe" : "Dieser Termin") + " ist f�r Sie nicht ver�nderbar da Sie nicht die erforderlichen Rechte besitzen.</font>";
        }
        
        String tooltip = null;
        
        Appearance ape = ConfigManager.getAppearance();
        
        try {
            if (getAppointment().isPrivat() && (getAppointment().getOwner().equals(m_oSchedulePanel.getGUIListener().getUser(null)) == false)){
                if (isJob())
                {
                    m_oDescriptionLabel.setText("<html><p align=\"center\">" + ape.get(Key.PRIVATE_TODO) + "</p></html>");
                    tooltip = "<html>" + type + "<br>F�llig: " + enddate + "<br><br>" + edittext + "</html>";
                }
                else
                {  
                    m_oDescriptionLabel.setText("<html><p align=\"center\">" + ape.get(Key.PRIVATE_DATE) + "</p></html>");
                    tooltip = "<html>" + type + "<br>Beginn: " + startdate + "<br>Ende: " + enddate + "<br><br>" + edittext + "</html>";
                }
                
            }else{
                m_oDescriptionLabel.setText("<html><p align=\"center\">" + m_sName + "</p></html>");
                
                if (m_sDescription.trim().length() > 0)
                {  
                    String encodeddescription = m_sDescription.trim().replaceAll("\n" , "<br>");
                    if (isJob())
                    {  
                        tooltip = "<html>" + type + "<b>" + m_sName + "</b><br>F�llig: " + enddate + "<br><br>" + encodeddescription + edittext + "</html>";
                    }
                    else
                    {  
                        tooltip = "<html>" + type + "<b>" + m_sName + "</b><br>Beginn: " + startdate + "<br>Ende: " + enddate + "<br><br>" + encodeddescription + edittext + "</html>";
                    }
                }
                else
                {  
                    if (isJob())
                    {  
                        tooltip = "<html>" + type + "<b>" + m_sName + "</b><br>F�llig: " + enddate + edittext + "</html>";
                    }
                    else
                    {  
                        tooltip = "<html>" + type + "<b>" + m_sName + "</b><br>Beginn: " + startdate + "<br>Ende: " + enddate + edittext + "</html>";
                    }
                }    
            }
        } catch (ContactDBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        this.setToolTipText(tooltip);        
        m_oNamePanel.setToolTipText(tooltip);              
        m_oTypeLabel.setToolTipText(tooltip);        
        m_oDescriptionLabel.setToolTipText(tooltip);
        m_oEndLabel.setToolTipText(tooltip);
        m_oStartLabel.setToolTipText(tooltip);    
    }
    
    
    public void setDateLabels()
    {
        m_oStartLabel.setText(createDateString(getStartDate().getDate()));
        m_oEndLabel.setText(createDateString(getEndDate().getDate()));
    }
    
    
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------
    
    private JPopupMenu createPopUpMenu()
    {
        //...where the GUI is constructed:
        //Create the popup menu.
        JPopupMenu oPopupMenu = new JPopupMenu();
        
        JMenuItem menuItemEDIT;
        JMenuItem menuItemDELETE;
        
        if (isJob())
        {
            if (isAppointmentEditable() && (! super.isDragListenerSelectOnly()))
            {  
                menuItemEDIT = new JMenuItem("Aufgabe bearbeiten", m_oIconEdit);
                menuItemEDIT.setFont(m_oMenuFont);
                menuItemEDIT.addActionListener(new MenuItem_EDIT_clicked());
                oPopupMenu.add(menuItemEDIT);
                
                menuItemDELETE = new JMenuItem("Aufgabe l�schen", m_oIconDelete);      
                menuItemDELETE.setFont(m_oMenuFont);
                menuItemDELETE.addActionListener(new MenuItem_DELETE_clicked());
                oPopupMenu.add(menuItemDELETE);
                
            }
            else
            {
                menuItemEDIT = new JMenuItem("Aufgabe anzeigen", m_oIconEdit);
                menuItemEDIT.setFont(m_oMenuFont);
                menuItemEDIT.addActionListener(new MenuItem_EDIT_clicked());
                oPopupMenu.add(menuItemEDIT);           
            }
        }
        else
        {
            if (isAppointmentEditable() && (! super.isDragListenerSelectOnly()))
            {  
                menuItemEDIT = new JMenuItem("Termin bearbeiten", m_oIconEdit);
                menuItemEDIT.setFont(m_oMenuFont);
                menuItemEDIT.addActionListener(new MenuItem_EDIT_clicked());
                oPopupMenu.add(menuItemEDIT);
                
                menuItemDELETE = new JMenuItem("Termin l�schen", m_oIconDelete);      
                menuItemDELETE.setFont(m_oMenuFont);
                menuItemDELETE.addActionListener(new MenuItem_DELETE_clicked());
                oPopupMenu.add(menuItemDELETE);
            }
            else
            {
                menuItemEDIT = new JMenuItem("Termin anzeigen", m_oIconEdit);
                menuItemEDIT.setFont(m_oMenuFont);
                menuItemEDIT.setFont(m_oMenuFont);
                menuItemEDIT.addActionListener(new MenuItem_EDIT_clicked());
                oPopupMenu.add(menuItemEDIT);           
            }
        }
        
        return oPopupMenu;    
    }
    
    private class MenuItem_EDIT_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {      
            getSchedulePanel().editScheduleEntryPanel(m_oThisScheduleEntry);
        }    
    }
    
    private class MenuItem_DELETE_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            getSchedulePanel().deleteScheduleEntryPanel(m_oThisScheduleEntry);
        }    
    }
    
    class PopupListener extends MouseAdapter 
    {
        private JPopupMenu m_oPopupMenu;
        
        public PopupListener()
        {
            //m_oPopupMenu = menu;
        }
        
        public void mousePressed(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        public void mouseReleased(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) 
        {
            if (e.isPopupTrigger()) 
            {
                m_oPopupMenu = createPopUpMenu();
                m_oPopupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }
    
    private boolean isEditable(Appointment appointment)
    {
        if (appointment != null)
        {  
            try {
                
                // Test if current User is Owner 
                if (appointment.getOwner().equals(getSchedulePanel().getGUIListener().getUser(null, true)))
                    return true;
                
                // Not owner and private
                if (appointment.isPrivat()) return false;
                
                // Release selected and write access?
                if (getSchedulePanel().getGUIListener().getCalendarCollection().getCalendar(appointment.getOwner().getCalendar(false).getId()) != null)
                {
                    CalendarSecretaryRelation calSec = appointment.getOwner().getCalendar(false).getCalendarSecretaryforUser(getSchedulePanel().getGUIListener().getUser(null).getId());
                    // if (calSec != null) never null??
                    if  (calSec.getSecretaryUserOwnerID().intValue() == appointment.getOwner().getId() &&
                            calSec.getWriteDateRange().containsDate(appointment.getStart()) &&
                            calSec.getWriteDateRange().containsDate(appointment.getEnd())) 
                        return true;
                }
                
            } catch (ContactDBException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        // Ok, we lost
        return false;
    }  
    
    public void setAppointment(Appointment appointment)
    {
        super.setAppointment(appointment);
        m_bCanEdit = isEditable(appointment);
        setAppointmentEditable(m_bCanEdit);
    }
    
    protected void setAppointmentEditable(boolean editable)
    {
        super.setAppointmentEditable(editable);
        setColorEditable(editable);
    }
    
    
    public Integer getUserID(){
        return _userID;
    }
    // ------------------------------ CalendarVisibleElement -----------------------------
    
    public void changedAppointment(Appointment appointment)
    {
        if (appointment.getId() == getAppointment().getId())
        {
            setAppointment(appointment);
            revalidateDisplay();
            m_oSchedulePanel.resolveDayConflicts();
        }
    }
    
    public void addedAppointment(Appointment appointment)
    {
        // not interested in this...
    }
    
    public Appointment getAppointment(int appointmentid)
    {    
        return getAppointment();
    }
    
    public void removedAppointment(Appointment appointment)
    {
        // not interested in this...
    }
    
    public void selectedAppointment(Appointment appointment)
    {
        // not interested in this...
    }
    
    public void changedCalendarCollection()
    {
        // not interested in this...
    }
    
    public void setViewType(String viewtype)
    {
        // not interested in this...
    }
    
    public void setVisibleInterval(ScheduleDate start, ScheduleDate end)
    {
        // not interested in this...    
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridActive(boolean)
     */
    public void setGridActive(boolean usegrid)
    {
        // TODO Auto-generated method stub
        
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridHeight(int)
     */
    public void setGridHeight(int seconds)
    {
        // TODO Auto-generated method stub
    }

    /** Handles export event.*/
    public void clickedExportButton() {
    }

    /** Handles new appointment event.*/
    public void clickedNewAppointmentButton() {
    }

    /** Handles new task event.*/
    public void clickedNewTaskButton() {
    }
    
}
