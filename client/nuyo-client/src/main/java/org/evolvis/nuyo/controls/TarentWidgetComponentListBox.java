/* $Id: TarentWidgetComponentListBox.java,v 1.2 2007/01/11 18:56:41 robert Exp $
 * 
 * Created on 11.04.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @author d-fens
 *
 */
public class TarentWidgetComponentListBox extends JPanel implements TarentWidgetInterface 
{  
  private JScrollPane            m_oScrollPane;
  private JList                        m_oList;

  /**
   * 
   */
  public TarentWidgetComponentListBox() 
  {
    super();
    m_oList = new JList();
    m_oList.setModel(new DefaultListModel());
    m_oList.setCellRenderer(new ComponentListRenderer());
    m_oScrollPane = new JScrollPane(m_oList);
    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER); 
  }
  
  
  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setData(java.lang.Object)
   */
  public void setData(Object data) 
  {
    ((DefaultListModel)m_oList.getModel()).removeAllElements();
    addData(data);
  }

  public void addData(Object data) 
  {
    if (data instanceof Component[])
    {
      for(int i=0; i<((Component[])data).length; i++)
      {
        ((DefaultListModel)m_oList.getModel()).addElement(((Component[])data)[i]);
      }      
    }
    else if (data instanceof Component)
    {
      ((DefaultListModel)m_oList.getModel()).addElement(data);
    }
  }



  public void removeData(Object data) 
  {
    if (data instanceof Component)
    {
      ((DefaultListModel)m_oList.getModel()).removeElement(data);
    }
  }

  public void removeAllData() 
  {
    ((DefaultListModel)m_oList.getModel()).removeAllElements();
  }

  public int getNumberOfEntries()
  {
    return ((DefaultListModel)m_oList.getModel()).getSize();    
  }
  
  

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#getData()
   */
  public Object getData() 
  {
   return(null);
  }

    /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#getComponent()
   */
  public JComponent getComponent() 
  {
    return (this);
  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setLengthRestriction(int)
   */
  public void setLengthRestriction(int maxlen) 
  {
  

  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setWidgetEditable(boolean)
   */
  public void setWidgetEditable(boolean iseditable) 
  {
  }

  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#isEqual(java.lang.Object)
   */
  public boolean isEqual(Object data) 
  {
    return false;
  }

  public void setSelectionMode (int mode)
  {
    m_oList.setSelectionMode(mode);
  }
  
  /**
   * @param arg0
   */
  public void setVisibleRowCount(int arg0)
  {
    m_oList.setVisibleRowCount(arg0);
  }

  public JList getJList()
  {
    return m_oList;
  }
  
  
  private void setSelectedRecursive(Component comp, boolean enable)
  {
    if (comp instanceof JPanel)
    {
      for (int i=0; i<((JPanel)comp).getComponentCount(); i++)
      {
        setSelectedRecursive(((JPanel)comp).getComponent(i), enable);
      }
    }

    ((JComponent)comp).setOpaque(true);
    comp.setBackground(enable ? Color.WHITE : Color.LIGHT_GRAY);    
    comp.setBackground(enable ? Color.LIGHT_GRAY : Color.WHITE);    
  }


  class ComponentListRenderer extends DefaultListCellRenderer 
  {
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) 
    {          
      setSelectedRecursive((Component)(value), isSelected);
      return (Component)(value);
    }
  }
  
  
  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  
}

  