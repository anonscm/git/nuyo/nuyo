package org.evolvis.nuyo.db.octopus;

import java.util.Collection;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;


/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.IAddress
 *
 */
public class AddressImpl2 extends AddressBean2 {

    //
    //	Soapanbindung
    //
    /** Methode zum Holen von Adressvorschauen */
    private static final String METHOD_GETADDRESSES = "getAddresses";
    
    
    static {
        _fetcher = new AbstractEntityFetcher(METHOD_GETADDRESSES, true) {
            public IEntity populate(ResponseData response) throws ContactDBException {
                AddressImpl2 address = new AddressImpl2();
                address.populate(response);
                return address;
            }
        };
    }
    
    public void validate() throws ContactDBException {
    }

    public void commit() throws ContactDBException {
    }

    public void delete() throws ContactDBException {
    }

    public void rollback() throws ContactDBException {
    }

    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(1);
        _label = data.getString(2);
    }
    
    static public Collection getAddresses(ISelection selection) throws ContactDBException {return _fetcher.getEntities(selection);}
    
}
