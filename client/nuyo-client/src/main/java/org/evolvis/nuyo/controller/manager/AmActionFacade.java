/* $Id: AmActionFacade.java,v 1.20 2007/08/30 16:10:28 fkoester Exp $
 * Created on 09.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.VcardUtil;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment;
import org.evolvis.xana.config.Environment.Key;



/**
 * Diese Klasse �bernimmt die Bearbeitung von komplexeren Aufgaben des
 * {@link org.evolvis.nuyo.controller.ActionManager ActionManagers},
 * die insbesondere Kommunikation mit externen Komponenten erfordern.
 * 
 * @author mikel
 */
public class AmActionFacade extends AmSelection {
    //
    // Versandauftr�ge
    //
    /**
     * Diese Methode f�hrt nach einer Parameter�berpr�fung eine Postaktion aus
     * 
     * @param checkString1 Dieser String wird getestet, ob er nicht-leer ist.
     * @param checkString2 Dieser String wird getestet, ob er nicht-leer ist.
     * @param msg Diese Mitteilung wird bei Fehlschlagen der
     *  Parameter�berpr�fung publiziert.
     * @param action Diese Postaktion wird ausgef�hrt.
     */
    protected void checkAndPost(String checkString1, String checkString2, String msg, String action) {
        if (checkString1 == null || checkString1.trim().length() == 0)
            publishError(msg);
    }

    /**
     * Diese Methode erzeugt einen neuen Versandauftrag f�r den aktuellen
     * Benutzer zur aktuellen Adressauswahl.
     * 
     * @return den neuen Versandauftrag oder <code>null</code>, falls kein
     *         neuer erzeugt werden konnte.
     */
    public MailBatch createNewMailOrder() {
        try {
            setWaiting(true);
            return getDb().createVersandAuftrag(getAddresses());
        } catch (ContactDBException ex) {
            publishError(getControlListener().getMessageString("AM_MailBatch_Create_DbError"), ex);
        } finally {
            setWaiting(false);
        }
        return null;
    }

    /**
     * Liefert die Historie der Versandauftr�ge des aktuellen Benutzers.
     * 
     * @return Liste der Versandauftr�ge des aktuellen Benutzers
     */
    public List getVersandAuftraege() {
        if (getDb() != null) try {
            return getDb().getVersandAuftraege();
        } catch (ContactDBException se) {
            publishError(getControlListener().getMessageString("AM_MailBatch_Get_DbError"), se);
        }
        return null;
    }

    //
    // VCARD-Aktionen
    //
    protected File chooseVcardFile(boolean mustBeReadable, boolean mustBeWritable, boolean enforceExtension, String presetFileName) {
        String lastFolderName = lastVcardFolder == null ? null : lastVcardFolder.getPath();
        Preferences vcardPreferences = getPreferences(StartUpConstants.PREF_VCARD_SUB_NODE);
        File vcardFile = null;
        while (vcardFile == null) {
            String fileName = getControlListener().requestFilename(
                    vcardPreferences.get(StartUpConstants.PREF_VCARD_DIR_KEY, lastFolderName), presetFileName, vcardFileExtensions, getControlListener().getMessageString("AM_VCARD_FileTypeDescription"), mustBeWritable);
            logger.fine("[vcard-file-name]: " + fileName);
            if (fileName == null || fileName.equals("")) return null;
            vcardFile = new File(fileName);
            if (enforceExtension && vcardFile.getName().indexOf('.') < 0) {
                fileName = MessageFormat.format(getMessageString("AM_VCARD_Extension_Enforcer"), new Object[] {fileName});
                vcardFile = new File(fileName);
            }
            
            vcardPreferences.put(StartUpConstants.PREF_VCARD_DIR_KEY, vcardFile.getParentFile().getPath());
            if (mustBeReadable && !vcardFile.canRead()) {
                publishError(getControlListener().getMessageString("AM_FILE_NoRead"), new Object[]{fileName});
                vcardFile = null;
            } else if (mustBeWritable && vcardFile.exists()) {
                if (!vcardFile.canWrite()) {
                    publishError(getControlListener().getMessageString("AM_FILE_NoWrite"), new Object[]{fileName});
                    vcardFile = null;
                } else {
                    switch (askUser(MessageFormat.format(getControlListener().getMessageString("AM_FILE_QueryOverwrite"), new Object[]{fileName}),
                            getControlListener().getMessageString("AM_FILE_QueryOverwrite_3Choices").split("[:]",3), 1)) {
                    case 0:
                        break;
                    case 1:
                        vcardFile = null;
                        break;
                    case 2:
                        return null;
                    }
                }
            }
        }

        lastVcardFolder = vcardFile.getParentFile();
        return vcardFile;
    }

    protected File chooseExportFile(boolean mustBeReadable, boolean mustBeWritable, String[][] extensions, String[] descriptions, String presetFileName) {
        String lastFolderName = lastExportFolder == null ? null : lastExportFolder.getPath();
        Preferences preferences = getPreferences(StartUpConstants.PREF_VCARD_SUB_NODE);

        File file = null;
        while (file == null) {
            String fileName = getControlListener()
                .requestFilename(preferences.get(StartUpConstants.PREF_EXPORT_DIR_KEY, lastFolderName),
                                 presetFileName, 
                                 extensions,
                                 descriptions, 
                                 true,
                                 false);
            if (fileName == null) return null;
            file = new File(fileName);
            if (file.getName().indexOf('.') < 0 && getLastChoosedFileExtensions() != null && getLastChoosedFileExtensions().length > 0) {
                file = new File(fileName + "." + getLastChoosedFileExtensions()[0]);
            }            
            
            if (file != null && file.getParentFile() != null)
                preferences.put(StartUpConstants.PREF_EXPORT_DIR_KEY, file.getParentFile().getPath());

            if (file.exists()) {
                if (mustBeReadable && !file.canRead()) {
                    publishError(getControlListener().getMessageString("AM_FILE_NoRead"), new Object[]{fileName});
                    file = null;
                } else if (mustBeWritable &&!file.canWrite()) {
                    publishError(getControlListener().getMessageString("AM_FILE_NoWrite"), new Object[]{fileName});
                    file = null;
                } else {
                    switch (askUser(MessageFormat.format(getControlListener().getMessageString("AM_FILE_QueryOverwrite"), new Object[]{fileName}),
                                    getControlListener().getMessageString("AM_FILE_QueryOverwrite_3Choices").split("[:]",3), 1)) {
                    case 0:
                        break;
                    case 1:
                        file = null;
                        break;
                    case 2:
                        return null;
                    }
                }
            }
        }
        
        lastExportFolder = file.getParentFile();
        return file;
    }

    public String[] getLastChoosedFileExtensions() {
        return getControlListener().getLastChoosedFileExtensions();
    }

    protected boolean exportIntoVcard(File vcardFile, int range) {
        try {
            if (GUIListener.RANGE_CURRENT_ADDRESS == range) {
                VcardUtil.exportVcardData(getAddress(), vcardFile, vcardEncoding);
                showInfo(getControlListener().getMessageString("AM_VCARD_Export_Success"));                
            } else if (GUIListener.RANGE_CURRENT_LIST == range) {
                VcardUtil.exportVcardData(getAddresses(), vcardFile, vcardEncoding);
            } else
                logger.warning(MessageFormat.format(getControlListener().getMessageString("AM_VCARD_Export_RangeUnknown"), new Object[]{new Integer(range)}));
            return true;            
        } catch (ContactDBException e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Export_DbError"), e);
        } catch (IOException e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Export_IoError"), new Object[]{vcardFile}, e);
        }
        return false;
    }
    
    protected boolean importVcardIntoCurrentAddress(File vcardFile) {
        if(vcardFile == null) {
            logger.warningSilent("can't import vcard: null");
            return false;
        }
        boolean success = false;
        try {
            Reader vcardReader = new InputStreamReader(new FileInputStream(vcardFile), vcardEncoding);
            success            = VcardUtil.importVcardData(vcardReader, getAddress());
            getEventsManager().fireSetAddress(getAddress(), true);
            vcardReader.close();
            return true;
        } catch (FileNotFoundException e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Import_NoFind"), new Object[]{vcardFile}, e);
        } catch (ContactDBException e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Import_DbError"), e);
        } catch (UnsupportedEncodingException e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Import_EncodingError"), new Object[]{vcardEncoding}, e);
        } catch (IOException e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Import_IoError"), e);
        } catch (Throwable e) {
            publishError(getControlListener().getMessageString("AM_VCARD_Import_Unexpected_Error")+": " + e.getClass().getName(), e);
        } 
        return success;
    }

    //
    // Sonstige Aktionen
    //
    
    public List getAvailableChannels()
    {
      Environment env = ConfigManager.getEnvironment();
      List channels = new ArrayList();
      if (env.getAsBoolean(Key.POST_ENABLEFAX, true)) channels.add(MailBatch.CHANNEL.FAX);
      if (env.getAsBoolean(Key.POST_ENABLEEMAIL, true)) channels.add(MailBatch.CHANNEL.MAIL);
      if (env.getAsBoolean(Key.POST_ENABLEPOST, true)) channels.add(MailBatch.CHANNEL.POST);
      return channels;
    }
    
    
    //
    // private Member-Variablen
    //
    /** das zuletzt benutzte VCARD-Verzeichnis */
    private File lastVcardFolder = null;
    /** die bekannten Erweiterungen f�r VCARD-Dateien */
    private static String[] vcardFileExtensions = new String[] {"vcf"};
    /** das f�r Import und Export zu benutzende Encoding */
    private static String vcardEncoding = "UTF-8";

    /** das zuletzt benutzte Export-Verzeichnis */
    private File lastExportFolder = null;
}
