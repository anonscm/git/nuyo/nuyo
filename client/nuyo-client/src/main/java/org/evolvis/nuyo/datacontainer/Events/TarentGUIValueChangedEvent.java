/*
 * Created on 23.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Events;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;

/**
 * @author niko
 *
 */
public class TarentGUIValueChangedEvent extends TarentGUIEvent
{
  private Object m_oValue = null;
  
  public TarentGUIValueChangedEvent(DataContainer sender, Object newvalue)
  {
    super(TarentGUIEvent.GUIEVENT_VALUECHANGED, sender, null);
    m_oValue = newvalue;
  }
  
  public Object getValue()
  {
    return m_oValue;
  }
}
