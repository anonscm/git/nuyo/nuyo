/*
 * Created on 01.08.2005
 *
 */
package org.evolvis.nuyo.gui.calendar;

/**
 * @author simon
 *
 * 
 */
public class TODOPrintContainer {
	
	private String _monat;
	private int _kw;
	private String _titel;
	private String _ort;
	private String _faelligkeit;
	private int _status;
	private int _prio;
	
	public String get_faelligkeit() {
		return _faelligkeit;
	}
	public void set_faelligkeit(String _faelligkeit) {
		this._faelligkeit = _faelligkeit;
	}
	public int get_kw() {
		return _kw;
	}
	public void set_kw(int _kw) {
		this._kw = _kw;
	}
	public String get_monat() {
		return _monat;
	}
	public void set_monat(String _monat) {
		this._monat = _monat;
	}
	public String get_ort() {
		return _ort;
	}
	public void set_ort(String _ort) {
		this._ort = _ort;
	}
	public int get_prio() {
		return _prio;
	}
	public void set_prio(int _prio) {
		this._prio = _prio;
	}
	public int get_status() {
		return _status;
	}
	public void set_status(int _status) {
		this._status = _status;
	}
	public String get_titel() {
		return _titel;
	}
	public void set_titel(String _titel) {
		this._titel = _titel;
	}
}
