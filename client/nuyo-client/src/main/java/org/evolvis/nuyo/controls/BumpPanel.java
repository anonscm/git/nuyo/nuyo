/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * @author niko
 */


public class BumpPanel extends JPanel 
{
  Image m_oImage;
  
  public BumpPanel(Image image)
  {
    m_oImage = image;
  }
  
  public BumpPanel(ImageIcon icon)
  {
    m_oImage = icon.getImage();
  }
    
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {        
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();

    int borderX = 2;
    int borderY = 2;
    
    size.width  -= (insets.left + insets.right);
    size.height -= (insets.bottom + insets.top);

    int imageSizeX = m_oImage.getWidth(this);
    int imageSizeY = m_oImage.getHeight(this);

    int gfxX = size.width  - (2 * borderX);
    int gfxY = size.height - (2 * borderY);

    int x;
    int y;
    for(x = borderX; x < gfxX; x += imageSizeX)
    {
      for(y = borderY; y < gfxY; y += imageSizeY)
      {
        g.drawImage(m_oImage, x,y, this);
      }      
      if ((gfxY - (y*imageSizeY)) > 0)
      {
        g.drawImage(m_oImage, x, y + imageSizeY, imageSizeX, gfxY - (y * imageSizeY), this);
      }
    }
  }
}
