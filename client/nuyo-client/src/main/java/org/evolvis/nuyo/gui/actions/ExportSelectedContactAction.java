package org.evolvis.nuyo.gui.actions;
import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;



/**
 * Exports the selected contact.
 * At the moment is only vcard-format supported.
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ExportSelectedContactAction extends AbstractContactDependentAction {

    private static final long   serialVersionUID    = 8122860390409416450L;
    private static final TarentLogger log = new TarentLogger(ExportSelectedContactAction.class);
    private GUIListener guiListener;
    private Runnable exportExecutor;

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null){
            SwingUtilities.invokeLater(exportExecutor);
        } else log.warning("Export action is not initialized");
    }

    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        exportExecutor = new Runnable(){
            public void run() {
                guiListener.userRequestExportIntoVcard(GUIListener.RANGE_CURRENT_ADDRESS);
            }
        };
    }
}