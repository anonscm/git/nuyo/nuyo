/* $Id: AbstractContactAction.java,v 1.6 2006/12/12 09:30:59 robert Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui;

import java.util.ArrayList;
import java.util.List;

/**
 * Einfache Implementierung einer ContactAction
 */
public abstract class AbstractContactAction
    extends javax.swing.AbstractAction 
    implements ContactAction {

    public static String UNIQUE_NAME = "uniqueName";
    public static String MENU_SECTION = "menuSection";
    public static String ACTION_TYPE = "actionType";

    public static String TYPE_TRIGGER = "trigger";
    public static String TYPE_CHECK = "check";
    public static String TYPE_CHOISE = "choise";

    protected List containerList = null;

    public AbstractContactAction(String uniqueName, String name, String menuSection) {
        super(name);
        putValue(UNIQUE_NAME, uniqueName);
        putValue(MENU_SECTION, menuSection);
        putValue(ACTION_TYPE, TYPE_TRIGGER);
        init();
    }  
    
    /** 
     * Template Methode zum �berschreiben in Unterklassen
     */
    protected void init() {
        
    }

    /**
     * Liefert einen global eindeutigen Namen, �ber den die Action angesprochen werden kann.
     */
    public String getUniqueName() {
        return ""+getValue(UNIQUE_NAME);
    }

    /**
     * Gibt an, ob die Action in dem entsprechenden Container (Men�, Toolbar, ..) angezeigt werden soll.
     *
     */
    public boolean attachToContainer(String containerName) {
        if (containerList == null)
            return true;
        return containerList.contains(containerName);
    }

    /**
     * F�gt einen Container hinzu, in dem die Action angezeigt werden soll.
     */
    public void addDisplayContainer(String containerName) {
        if (containerList == null)
            containerList = new ArrayList(3);
        containerList.add(containerName);
    }

    /**
     * Liefert den Men�bereich, unter dem die Action angezeigt werden soll. Derzeit sind die Konstanten unter MenuBar erlaubt.
     */
    public String getMenuSection() {
        return ""+getValue(MENU_SECTION);
    }
}