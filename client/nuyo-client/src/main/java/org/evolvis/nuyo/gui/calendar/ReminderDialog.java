package org.evolvis.nuyo.gui.calendar;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.controls.TarentPanel;
import org.evolvis.nuyo.controls.TarentWidgetCheckBox;
import org.evolvis.nuyo.controls.TarentWidgetComboBox;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.controls.TarentWidgetTextField;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Reminder;
import org.evolvis.nuyo.gui.GUIListener;

import de.tarent.commons.ui.EscapeDialog;

public class ReminderDialog extends EscapeDialog {

	
	JFrame sender;
	protected GUIListener myGUIListener;
	private Appointment app;
	private TarentPanel reminderPanel;
	private TarentPanel buttonPanel;
	private boolean appointmentHadReminder; //flag that remembers if the appointment had a reminder on startup
	private TarentWidgetTextField reminderShortDescription;
	private TarentWidgetCheckBox reminderCheckbox;
	private TarentWidgetComboBox reminderOffsetHours;
	private TarentWidgetComboBox reminderOffsetMinutes;
	private TarentWidgetComboBox reminderOffsetDays;
	
	private GridBagConstraints constraints; 
	
	private String [] offsetHours;
	private String [] offsetMinutes;
	private String [] offsetDays;
	
	
		
	public ReminderDialog(GUIListener guiListener, JFrame sender, String windowname, Appointment appointment)
	{
		super(sender, true);
		myGUIListener = guiListener; 
		this.sender = sender;
		app = appointment;
		constraints = new GridBagConstraints();
		offsetHours = new String []{"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"};
		offsetMinutes = new String []{"0","5","10","15","20","25","30","35","40","45","50","55"};  	
		offsetDays = new String []{"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14"};  
		this.getContentPane().setLayout(new BorderLayout());
		this.setLocation(400,400);
		this.setTitle(windowname);
		this.setVisible(false); 
		
		TarentPanel spacePanel = new TarentPanel();
		spacePanel.setBorder(new EmptyBorder(8,8,8,8));
		
		/************************************************
		 	Speichern und Abbrechen Buttons
	 	************************************************/ 
		
		buttonPanel = new TarentPanel();
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.setBorder(new EmptyBorder(8,8,8,8));
		
		JButton saveButton = new JButton("Speichern");
		saveButton.setToolTipText("�bernimmt die Einstellungen zur Terminbenachrichtigung f�r den Termin");
		saveButton.addActionListener(new saveButtonClicked());
		//saveButton.setFocusPainted(false);
		
		JButton cancelButton = new JButton("Abbrechen");
		cancelButton.setToolTipText("bricht diesen Dialog ab und verwirft die �nderungen");
		cancelButton.addActionListener(new cancelButtonClicked());
		
		buttonPanel.add(saveButton, BorderLayout.EAST);
		buttonPanel.add(cancelButton, BorderLayout.WEST);
		
		/************************************************
	 		Eintr�ge zur Terminbenachrichtigung
	 	************************************************/ 
		reminderPanel = new TarentPanel();
		reminderPanel.setLayout(new GridBagLayout());
		
		reminderCheckbox = new TarentWidgetCheckBox();
		reminderCheckbox.setText("Email-Benachrichtigung vor Terminbeginn");
		reminderCheckbox.addActionListener(new CheckBoxClicked());
		
		TarentWidgetLabel reminderHourLabel = new TarentWidgetLabel(" Std ");
		TarentWidgetLabel reminderMinutesLabel = new TarentWidgetLabel(" Minuten ");
		TarentWidgetLabel reminderDaysLabel = new TarentWidgetLabel(" Tage ");
		
		
		reminderOffsetHours = new TarentWidgetComboBox(offsetHours);
		reminderOffsetMinutes = new TarentWidgetComboBox(offsetMinutes);
		reminderOffsetDays = new TarentWidgetComboBox(offsetDays);
		
		TarentWidgetLabel reminderDescriptionLabel = new TarentWidgetLabel("Bemerkung: ");
		reminderShortDescription = new TarentWidgetTextField();	
		reminderShortDescription.setLengthRestriction(100);
		
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets( 5, 5, 5, 5);
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 6;		
		reminderPanel.add(reminderCheckbox, constraints);
	
		constraints.fill = GridBagConstraints.NONE;	
		constraints.insets = new Insets( 5, 5, 5, 1);		
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 1;		
		reminderPanel.add(reminderOffsetDays, constraints);
		
		constraints.gridx = 1;
		constraints.insets = new Insets( 5, 1, 5, 5);
		reminderPanel.add(reminderDaysLabel, constraints);
		
		constraints.gridx = 2;
		constraints.insets = new Insets( 5, 5, 5, 1);		
		reminderPanel.add(reminderOffsetHours, constraints);
		
		constraints.gridx = 3;
		constraints.insets = new Insets( 5, 1, 5, 5);
		reminderPanel.add(reminderHourLabel, constraints);
		
		constraints.gridx = 4;
		constraints.insets = new Insets( 5, 5, 5, 1);		
		reminderPanel.add(reminderOffsetMinutes, constraints);
		
		constraints.gridx = 5;
		constraints.insets = new Insets( 5, 1, 5, 5);
		reminderPanel.add(reminderMinutesLabel, constraints);
		
		constraints.insets = new Insets( 5, 5, 5, 5);
		constraints.gridwidth = 6;
		constraints.gridy = 2;
		constraints.gridx = 0;
		reminderPanel.add(reminderDescriptionLabel, constraints);

		constraints.gridy = 3;
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		
		reminderPanel.add(reminderShortDescription, constraints);
		
		//********************************************************
		
		spacePanel.add(reminderPanel);
		this.getContentPane().add(spacePanel, BorderLayout.CENTER);
		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		/**********************************************************
			Fill panel with data
		/**********************************************************/
            
        getRootPane().setDefaultButton(saveButton);
		refresh();
	  }
	  
	  public void closeWindow(){
		  	  setVisible(false); 
			  setModal(false);
	  }
	  
	  public void openWindow(){
	  	pack();
	  	setVisible(true);  
	  	setModal(true);
	  }
	  
	  
	private class saveButtonClicked implements ActionListener
	{		
		public void actionPerformed(ActionEvent e)
		{	
			
			try {
				if (reminderCheckbox.isSelected()){
					long offset = Integer.parseInt(reminderOffsetDays.getSelectedItem().toString()) * 86400 + Integer.parseInt(reminderOffsetHours.getSelectedItem().toString()) * 3600 + Integer.parseInt(reminderOffsetMinutes.getSelectedItem().toString()) * 60;
					app.setReminder(new Reminder(reminderShortDescription.getText(), app.getId(), offset));
					
				}
				else{
					if (appointmentHadReminder)
						app.removeReminder();
				}
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} catch (ContactDBException e1) {
				e1.printStackTrace();
			}
				
			closeWindow();
			
		}
	}
	
	private class cancelButtonClicked implements ActionListener
	{		
		public void actionPerformed(ActionEvent e)
		{
			closeWindow();
		}
	}
	
	private class CheckBoxClicked implements ActionListener
	{		
		public void actionPerformed(ActionEvent e)
		{
			boolean checkBoxIsChecked = reminderCheckbox.isSelected();
			if (!checkBoxIsChecked){
				reminderOffsetDays.setSelectedIndex(0);
				reminderOffsetHours.setSelectedIndex(0);
				reminderOffsetMinutes.setSelectedIndex(0);
				reminderShortDescription.setText("");
			}
			reminderOffsetDays.setEnabled(checkBoxIsChecked);
			reminderOffsetHours.setEnabled(checkBoxIsChecked);
			reminderOffsetMinutes.setEnabled(checkBoxIsChecked);
			reminderShortDescription.setEnabled(checkBoxIsChecked);						
		}
	}

	private void refresh(){
		refresh(app);
	}
	
	public void refresh(Appointment appointment) {
		
		app = appointment;		
		Reminder reminder = null;
		
		try {
			reminder = appointment.getReminder();
		} catch (ContactDBException e) {
			e.printStackTrace();
		}
		boolean remNotNull = (reminder != null);
		
		appointmentHadReminder = remNotNull;
		reminderOffsetDays.setEnabled(remNotNull);
		reminderOffsetHours.setEnabled(remNotNull);
		reminderOffsetMinutes.setEnabled(remNotNull);
		reminderShortDescription.setEnabled(remNotNull);	
		reminderCheckbox.setSelected(remNotNull);
		
		if (remNotNull){
			reminderOffsetDays.setSelectedIndex((int) Math.floor(reminder.getOffSet()/86400) ); 
			int tmp1 = (int) (reminder.getOffSet() - (Math.floor(reminder.getOffSet()/86400) * 86400));
			reminderOffsetHours.setSelectedIndex((int) Math.floor(tmp1/3600));
			int tmp2 = (int) (reminder.getOffSet() - (Math.floor(reminder.getOffSet()/3600) * 3600));
			reminderOffsetMinutes.setSelectedItem("" + (int) Math.floor(tmp2 / 60));
			reminderShortDescription.setText(reminder.getDescription());
			
		}
		else{
			reminderOffsetDays.setSelectedIndex(0);
			reminderOffsetHours.setSelectedIndex(0);
			reminderOffsetMinutes.setSelectedIndex(0);
			reminderShortDescription.setText("");
		}
		
		openWindow();		
	}
}
