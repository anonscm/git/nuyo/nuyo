package org.evolvis.nuyo.db.octopus;

import java.util.Collection;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentAddressRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.VirtualEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;




/**
 * @author kleinw
 *
 *	Implementierung AppointmentAddressRelation.
 *
 */
public class AddressRelationImpl extends RequestBean implements AppointmentAddressRelation, VirtualEntity {

    //
    //	Instanzmerkmale
    //
    /**	Zur Relation dazugeh�rige Adresse */
    private Address _address;

    
    /**	Objekt zum Holen von Adressrelationen */
    static private AbstractEntityFetcher _fetcher;
    
    
    /**	Initialisierung des Fetchers */
    static {
        _fetcher = new AbstractEntityFetcher("getAddressRelations", true) {
            public IEntity populate(ResponseData response) throws ContactDBException {
                AppointmentAddressRelation aar = new AddressRelationImpl(null, null);
                ((IEntity)aar).populate(response);
                return (IEntity)aar;
            }
        };
    }
    
    
    /**	Konstruktor f�r dieses Objekt */
    protected AddressRelationImpl(Appointment appointment, Address address) {
        _appointment = appointment;
        _address = address;
    }
    
    
    /**	Adresse der Relation beziehen */
    public Address getAddress() throws ContactDBException {return _address;}

    
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}
    
    
    /**	Dieses Objekt wird vom Appointment selbst gespeichert */
    public void commit() throws ContactDBException {setDirty(true);}
 
    
    /**	L�scht diese Relation vom Appointment */
    public void delete() throws ContactDBException {setDeleted(true);}

    
    /**	Stellt Ursprungsdaten wieder her */
    public void rollback() throws ContactDBException {populate(_responseData);}

    
    /**	
     * TODO: Addresszugriff: Remove this code
     * 
     * Methode zum Bef�llen des Objekts aus DB - Inhalten 
     */
    public void populate(ResponseData data) throws ContactDBException {
//         _responseData = data;
//         _id = data.getInteger(0);
//         _type = data.getInteger(1);
//         _status = data.getInteger(2);
//         _level = data.getInteger(3);
//         setAttribute(KEY_DESCRIPTION,data.getString(4));
//         try {
//             AddressPreview preview = new AddressPreview(data.getInteger(5), data.getString(6));
//             _address = new AddressImpl();
//         } catch (AddressPreview.PreviewFormatException pfe) {
//             throw new ContactDBException(ContactDBException.EX_PREVIEW_ERROR, pfe);
//         }
    }
    
    
    /**	Hier�ber werden AddressRelations abgeholt */
    static public Collection getRelations(ISelection selection) throws ContactDBException {return _fetcher.getEntities(selection,false);}
    
    
    /**	Stringrepr�sentation des Objekts */
    public String toString() {
        return new StringBuffer()
        	.append("AddressRelation : ")
        	.append(_id)
        	.append(" ")
        	.append(_type)
        	.append(" ")
        	.append(_status)
        	.append(" ")
        	.append(_level)
        	.append(_attributes)
        	.toString();
    }
    
}
