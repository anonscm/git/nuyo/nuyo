/* $Id: DupCheck.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 24.07.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.db;

import java.util.Date;
import java.util.List;

/**
 * Diese Klasse stellt einen Duplikatcheck dar.
 * 
 * @author mikel
 */
public abstract class DupCheck {
    /*
     * Konstanten
     */
    public final static int STATUS_ERROR = -2;
    public final static int STATUS_NO_DUPCHECK = -1;
    public final static int STATUS_IDLE = 0;
    public final static int STATUS_RUNNING = 1;
    public final static int STATUS_PAUSING = 2; /* f�r aufrufende Instanzen */
    public final static int STATUS_DONE = 3; /* 'READY' */

    /*
     * �ffentliche Methoden
     */
    /**
     * Diese Methode liefert eine Liste von DupCheckResult-Instanzen,
     * den Ergebnissen dieser Suche.
     */
    abstract public List getResults() throws ContactDBException;

    /**
     * Diese Methode l�scht den aktuellen Duplikatcheck. 
     * 
     * @throws ContactDBException
     */
    abstract public void delete() throws ContactDBException;

    /**
     * Diese Methode schreibt �nderungen dieses Duplikatchecks in der
     * Datenbank fest. Ein neuer Duplikatcheck wird an dieser Stelle
     * gegebenenfalls angelegt.
     * 
     * @throws ContactDBException
     */
    abstract public void commit() throws ContactDBException;

    /**
     * Diese Methode macht �nderungen an diesem Duplikatcheck in der
     * Datenbank r�ckg�ngig.
     * 
     * @throws ContactDBException
     */
    abstract public void rollback() throws ContactDBException;

    /**
     * Diese Methode f�hrt den n�chsten Schritt in einer Duplikatssuche aus.
     * Beim ersten Aufruf initialisiert die Methode entsprechende Daten.
     * 
     * @throws ContactDBException
     */
    abstract public void doNextCheckStep() throws ContactDBException;

    /*
     * Getter und Setter
     */
    /**
     * @return Kommando
     */
    public String getCommand() {
        return command;
    }

    /**
     * @return Erzeuger
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @return Erzeugungsdatum
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @return Ausf�hrungsdatum
     */
    public Date getExecutionDate() {
        return executionDate;
    }

    /**
     * @return Id
     */
    public String getId() {
        return id;
    }

    /**
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * @return Status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @return ben�tigte Zeit
     */
    public int getUsedTime() {
        return usedTime;
    }

    /**
     * @param string
     */
    public void setCommand(String string) {
        if (checkChange(command, string))
            command = string;
    }

    /**
     * @param string
     */
    public void setCreatedBy(String string) {
        if (checkChange(createdBy, string))
            createdBy = string;
    }

    /**
     * @param date
     */
    public void setExecutionDate(Date date) {
        if (checkChange(executionDate, date))
            executionDate = date;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        if (checkChange(name, string))
            name = string;
    }

    /**
     * @param i
     */
    public void setStatus(int i) {
        if (checkChange(status, i))
            status = i;
    }

    /*
     * Konstruktoren und Hilfsmethoden
     */
    /**
     * 
     */
    protected DupCheck() {
    }

    /*
     * Konstanten
     */
    /**
     * Diese Konstante ist die Pseudo-DupCheck-ID eines neuen Duplikatchecks.
     */
    public static final String DUP_CHECK_NEW = null;

    /*
     * Hilfsmethoden
     */
    /**
     * Diese Methode stellt fest, ob die beiden �bergebenen Objekte sich unterscheiden
     * und passt gegebenenfalls den Inhalt von {@link #wasChanged wasChanged} an.
     * 
     * @param oldValue
     * @param newValue
     * @return <code>true</code>, falls �nderungen auffallen
     */
    protected boolean checkChange(Object oldValue, Object newValue) {
        boolean changed = false;
        changed = (oldValue == null) ? (newValue != null) : !oldValue.equals(newValue);
        wasChanged |= changed;
        return changed;
    }

    /**
     * Diese Methode liefert die Duplikate zu einer Ergebnis-Id.
     * 
     * @param resultId
     * @return Duplikate
     * @throws ContactDBException
     */
    abstract protected Addresses getDuplicateAddresses(int resultId) throws ContactDBException;
    
    /**
     * Diese Methode f�hrt Duplikate eines Duplikatchecks zusammen.
     * 
     * @param resultId Id des zusammengef�hrten Duplikatcheck-Resultats
     * @param targetId Id, unter der die Duplikate zusammengef�hrt werden sollen.
     *  Bei 0 wird automatisch eine ausgew�hlt. 
     * @param addressData zusammengef�hrte Adressdaten 
     * @param addressesToMerge Adressen, die in der Zusammenf�hrung aufgehen.
     * @param deleteMerged gibt an, ob gemergte Adressen in den L�schverteiler
     *  verschoben werden sollen.
     * @throws ContactDBException
     */
    abstract protected void merge(int resultId, int targetId, Address addressData, Addresses addressesToMerge, boolean deleteMerged) throws ContactDBException;

    /**
     * Diese Methode setzt den Status eines DupCheckResults.
     * 
     * @param resultId Id des DupCheckResults
     * @param status neuer Status des DupCheckResults
     */
    abstract protected void setResultStatus(int resultId, int status) throws ContactDBException;

    /**
     * Diese Methode stellt fest, ob die beiden �bergebenen Integer sich unterscheiden
     * und passt gegebenenfalls den Inhalt von {@link #wasChanged wasChanged} an.
     * 
     * @param oldValue
     * @param newValue
     * @return <code>true</code>, wenn eine �nderung festgestellt wird.
     */
    protected boolean checkChange(int oldValue, int newValue) {
        boolean changed = (oldValue != newValue);
        wasChanged |= changed;
        return changed;
    }

    /*
     * private Membervariablen f�r den Duplikatcheck.
     */
    protected String id = null;
    protected int status = 0;
    protected String name = null;
    protected String command = null;
    protected Date executionDate = null;
    protected Date creationDate = new Date();;
    protected String createdBy = null;
    protected int usedTime = 0;

    protected boolean wasChanged = false;
}
