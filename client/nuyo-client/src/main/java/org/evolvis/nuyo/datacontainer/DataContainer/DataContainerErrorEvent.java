/*
 * Created on 29.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

/**
 * @author niko
 *
 */
public class DataContainerErrorEvent
{
  public static final Object SHOWERRORS = "SHOWERRORS";
  public static final Object HIDEERRORS = "HIDEERRORS";
  
  private Object m_oType;
  
  public DataContainerErrorEvent(Object type)
  {
    m_oType = type;
  }  
  
  public Object getType()
  {
    return m_oType;
  }
}
