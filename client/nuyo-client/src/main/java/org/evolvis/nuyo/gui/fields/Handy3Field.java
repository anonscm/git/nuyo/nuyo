/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class Handy3Field extends GenericTextField
{
  public Handy3Field()
  {
    super("HANDY3", AddressKeys.HANDY3, CONTEXT_ADRESS, "GUI_Fields_Handy3_ToolTip", "GUI_Fields_Handy3", 0);
  }
}
