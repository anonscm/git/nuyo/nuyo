/*
 * Created on 30.03.2004
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.HashMap;
import java.util.Map;

import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;

import de.tarent.commons.plugin.Plugin;




/**
 * @author niko
 *
 */
public class FieldPool
{
  private static final TarentLogger logger = new TarentLogger(FieldPool.class);
  private GUIListener guiListener;
  private ControlListener controlListener;
  private Map fieldsMap;
  private WidgetPool widgetPool;
  
  public FieldPool(GUIListener oGUIListener, ControlListener oControlListener)
  {
    guiListener = oGUIListener;
    controlListener = oControlListener;
    widgetPool = new WidgetPool();
    fieldsMap = new HashMap();
       
    Plugin calendarPlugin = PluginRegistry.getInstance().getPlugin(CalendarPlugin.ID);
    if(calendarPlugin != null) {
        if(calendarPlugin.isTypeSupported(TarentCalendar.class)){
            widgetPool.addController(CalendarVisibleElement.MANAGER, calendarPlugin.getImplementationFor(TarentCalendar.class));
        } else logger.warning("Couldn't init FieldPool!", "Tarent Calender interface is not supported by current calender plugin");
    } else logger.warning("Couldn't init FieldPool!", "Calender Plugin is not registered."); 
    
    // TODO: causes problems on Blackdown 1.4 ...
    readFields();    
  }

  
  public Map getFieldMap()
  {
    return fieldsMap;
  }
  
  
//  private String[] getClasses(String packagename)
//  {
//    URL pluginlocation = this.getClass().getResource(packagename);
//    if (pluginlocation.getProtocol().equalsIgnoreCase("jar"))
//    {
//      JarResourcesFetcher jarresources = new JarResourcesFetcher(m_oGUIListener.getPluginLocator());
//      if (packagename.startsWith("/")) packagename = packagename.substring(1);
//      List list = jarresources.getResourceList(packagename);
//      String[] filenames = new String[list.size()];
//      for(int i=0; i<(list.size()); i++)
//      {
//        filenames[i] = (String)(list.get(i));
//      }
//      return filenames;
//    }
//    else try
//    {
//      File file = new File(new URI(pluginlocation.toExternalForm()));
//      if (file.isDirectory())
//      {        
//        File[] files = file.listFiles();
//        String[] filenames = new String[files.length];
//        for(int i=0; i<(files.length); i++)
//        {
//          filenames[i] = files[i].getName();
//        }
//        return filenames;
//      }
//    }
//    catch (URISyntaxException use) {
//        use.printStackTrace();
//    }
//    return null;
//  }
  
  
  
/*  
  private String[] getClasses(String packagename)
  {
    URL pluginlocation = this.getClass().getResource(packagename);
    if (pluginlocation.getProtocol().equalsIgnoreCase("jar"))
    {
      JarResourcesFetcher jarresources = new JarResourcesFetcher();
      if (packagename.startsWith("/")) packagename = packagename.substring(1);
      List list = jarresources.getResourceList(packagename);
      String[] filenames = new String[list.size()];
      for(int i=0; i<(list.size()); i++)
      {
        filenames[i] = (String)(list.get(i));
      }
      return filenames;
    }
    else
    {
      File file = new File(pluginlocation.getFile());
      if (file.isDirectory())
      {        
        File[] files = file.listFiles();
        String[] filenames = new String[files.length];
        for(int i=0; i<(files.length); i++)
        {
          filenames[i] = files[i].getName();
        }
        return filenames;
      }
    }
    return null;
  }
*/  
  
  
  private void readFields()
  {
    String[] names = guiListener.getPluginLocator().getClasses("/org/evolvis/nuyo/gui/fields");
        
    //String[] names = getClasses("/de/tarent/contact/gui/fields");
    if (names == null) {
        guiListener.getLogger().severe("unable to locate field classes.");
        return;
    }
    for(int i=0; i<(names.length); i++)
    {
      String name = names[i];
      if (name.endsWith(".class"))
      {
        if (name.indexOf('$') == -1)
        {
          String fieldname = name.substring(0, name.length()-6);
          String classname = "org.evolvis.nuyo.gui.fields." + fieldname;
          try
          {
            Class newclass = Class.forName(classname);
            if (AddressField.class.isAssignableFrom(newclass))
            {  
              addFieldClass(newclass);
            }
            else
            {
              guiListener.getLogger().severe("unable to instanciate class \"" + classname + "\" for field \"" + fieldname + "\" because class not implementing correct interface.");                        
            }
          } 
          catch (ClassNotFoundException e)
          {
            guiListener.getLogger().severe("unable to locate class \"" + classname + "\" for field \"" + fieldname + "\"");            
            e.printStackTrace();
          }
        }
      }
    }
  }
  

  public WidgetPool getWidgetPool()
  {
    return widgetPool;
  }
  
  public ContactAddressField getField(String key)
  {    
    return (ContactAddressField)(fieldsMap.get(key));
  }
  
  public boolean addField(String key, ContactAddressField field)
  {
    if (! fieldsMap.containsKey(key))
    {  
      field.setGUIListener(guiListener);
      field.setControlListener(controlListener);
      field.setWidgetPool(widgetPool);
      
      return (fieldsMap.put(key, field) == null);
    }
    else
    {
      guiListener.getLogger().severe("unable to add Field \"" + key + "\" because duplicate key");
      return false;
    }
  }
    
  private boolean addFieldClass(Class c)
  {
    try
    {
      ContactAddressField af = (ContactAddressField) c.newInstance();
      String key = af.getKey();      
      if (addField(key, af))
      {
        guiListener.getLogger().config("imported Field \"" + key + "\" from class \"" + c.getName() + "\"");
        return true;
      }
      else
      {
        guiListener.getLogger().severe("failed to import Field \"" + key + "\" from class \"" + c.getName() + "\"");        
        return false;
      }
    } 
    catch (InstantiationException e)
    {
      e.printStackTrace();
      guiListener.getLogger().severe("instantiation of class \"" + c.getName() + "\" failed because of an InstantiationException!");            
      return false;
    } 
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
      guiListener.getLogger().severe("instantiation of class \"" + c.getName() + "\" failed because of an IllegalAccessException!");            
      return false;
    }
  }  
}
