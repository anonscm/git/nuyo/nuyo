/*
 * Created on 09.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @author niko
 *
 */
public class TextViewer
{
  private JEditorPane m_oEditorPane;
  private JFrame m_oFrame = null;
  private String m_sText;

  public TextViewer(String text, String title)
  {    
    try
    {    
      m_oEditorPane = new JEditorPane();
      m_oEditorPane.setEditable(false);
      m_sText = text;
            
      if (m_sText != null) 
      {
        m_oEditorPane.setText(m_sText);
      } 

      //    Put the editor pane in a scroll pane.
      JScrollPane editorScrollPane = new JScrollPane(m_oEditorPane);
      editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      editorScrollPane.setPreferredSize(new Dimension(250, 145));
      editorScrollPane.setMinimumSize(new Dimension(10, 10));    
  
      JPanel panel = new JPanel(new BorderLayout());
      panel.add(editorScrollPane, BorderLayout.CENTER);    
          
      m_oFrame = new JFrame(title);
      
      m_oFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      m_oFrame.addWindowListener(new ViewerWindowListener());
      m_oFrame.getContentPane().add(panel);
      m_oFrame.pack();      
      m_oFrame.setVisible(true);
    }
    catch(Exception e)
    {
      System.err.println("rendering of document failed.");      
      shutdown();
    }      
  }

  private void shutdown()
  {
    if (m_oFrame != null) m_oFrame.setVisible(false);
  }
  

  private class ViewerWindowListener extends WindowAdapter
  {
    public void windowClosing(WindowEvent e)
    {
      shutdown();
    }    
  }
  
}
