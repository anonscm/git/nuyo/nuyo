package org.evolvis.nuyo.selector;

import java.util.PropertyResourceBundle;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.xana.action.ActionContainer;
import org.evolvis.xana.swing.MenuBar;


public class SelectorMenuBar extends MenuBar implements ActionContainer {
	
	public SelectorMenuBar(String uniqueName){
		super(uniqueName, PropertyResourceBundle.getBundle( StartUpConstants.MENU_BUNDLE_NAME, StartUpConstants.LOCALE));
		//mnemonics.clear();
		
	}

}
