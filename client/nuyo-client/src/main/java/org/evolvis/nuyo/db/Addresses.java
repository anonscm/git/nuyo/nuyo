/* $Id: Addresses.java,v 1.14 2007/08/30 16:10:29 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.commons.datahandling.entity.EntityList;

/**
 * Diese Klasse stellt die IDs von Adressen einer Adresssammlung zur Verf�gung.
 * 
 * @author mikel
 */
public interface Addresses extends EntityList {
    
    /**
     * @return den maximalen Index, also Anzahl der Adressnummern - 1; wenn dieser
     * Wert kleiner als -1 ist, so ist die Anzahl der Adressnummern noch unbekannt. 
     */
    public int getIndexMax();
    
    /**
     * @param index der Index der zuholenden Adressnummer zwischen 0 und dem maximalen
     * Index.
     * @return die bezeichnete Adressnummer oder 0, wenn sie noch nicht abgeholt ist.
     * @throws IndexOutOfBoundsException wird geworfen, wenn ein ung�ltiger Index
     * angegeben wurde.
     */
    public int getAddressNumber(int index);
    
    /**
     * Reloads the address set with the same AddressListParameter.
     * If the address set if not persistent, this action may do nothing. 
     */
    public void reload();
    
    /**
     * @param index der Index der zuholenden Adressnummer zwischen 0 und dem maximalen
     * Index.
     * @return die bezeichnete Adresse oder null, wenn sie noch nicht abgeholt ist.
     * @throws IndexOutOfBoundsException wird geworfen, wenn ein ung�ltiger Index
     * angegeben wurde.
     * @see #getAddressNumber(int)
     */
    public Address get(int index);
    
    public void addAddress(Address address);
        
    
    /**
     * Sets a filter on the addresses
     * @param listParameter Filter list parameters
     */
    public void setAddressListParameter(AddressListParameter listParameter);

    /**
     * Returns the filter on the addresses
     */
    public AddressListParameter getAddressListParameter();

    /**
     * Returns a list of all pks as Integer Objects.
     */
	public PrimaryKeyList getPkList();
	
    /**
     * Returns a String list of all pks sepperated by " ".
     */
	public String getPkListAsString();
    
}
