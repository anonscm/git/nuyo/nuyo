/* tarent-octopus, Webservice Data Integrator and Applicationserver
* Copyright (C) 2002 tarent GmbH
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-octopus'
* (which makes passes at compilers) written
* by Thomas Fuchs.
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.db.octopus.old;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * 
 * @deprecated Abgel�st von neuer Beanbasierter Implementierung
 */
public class OctopusSSL {
    
    private URL url;
	private HttpURLConnection urlCon = null;
	private static Logger logger = Logger.getLogger(OctopusSSL.class.getName());
	
    private X509TrustManager xtm = new X509TrustManager() {
         public void checkClientTrusted(X509Certificate[] chain, 
	                                String authType) {}
         public void checkServerTrusted(X509Certificate[] chain, 
	                                String authType) {}
         public X509Certificate[] getAcceptedIssuers() {
	    return null;
	 }
      }; 

    private HostnameVerifier hnv = new HostnameVerifier() {
         public boolean verify(String hostname, SSLSession session) {
	    	return true;
         }
      }; 

    
    public OctopusSSL(String urlstr) {
     try
      {
		this.url = new URL(urlstr);
      }catch(MalformedURLException mue) {
      	logger.log(Level.WARNING,"URL-Fehler: "+mue);
      }
     
     
     // Define the connection for the Server we want
     // to retrieve the certificates for
     String site = url.getHost();
     //int port = 443;
     int port = url.getPort();

     // Configure our KeyStore/TrustManager/Certificate file
     String keyStore = "key.dat";
     String keyStorePasswd = "";
     String keyStoreAlias = "keystorealias";


     // Creates a TrustManager that will allow us to
     // connect to the site so we can download the
     // Server's certificate
     createTrustALLManager();

     // Get the Server's certificate chain
     javax.security.cert.X509Certificate[] xc = getServerCert(site, port);

     // Add the server's certificate chain to our
     // certificate file
     for (int i=0; i < xc.length; i++) {
          addToKeyStore(keyStore, (keyStorePasswd).toCharArray(),keyStoreAlias, xc[i]);
     }
     
     // Verbindung �ffnen
     try
      {
	 	urlCon = (HttpURLConnection) url.openConnection();
	  }catch(IOException e){
	  	logger.log(Level.WARNING,"OctopusSSL: "+e);
	   }
    
    }
	
	
	
	 /** This will create a TrustManager that will trust
     *  ALL certificates and install it as the default
     *  SSLSocketFactory TrustManager.
     *
     *  <p>Use this function to replace the default
     *  TrustManager when you are connecting to an SSL
     *  site that the certificate is not trusted.
     */
    public void createTrustALLManager() {
         try {
              SSLContext sc = SSLContext.getInstance("TLS");
              sc.init(null,trustAllCerts, new java.security.SecureRandom());
              HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
         	  HttpsURLConnection.setDefaultHostnameVerifier( hnv );
         	  
         	  
         } catch (Exception e) {
         }
    }

	
	public HttpURLConnection getConnection() {
		 try {
			   return urlCon;
  
		 } catch (Exception e) {
		 	logger.log(Level.WARNING,"OctopusSSL.getConnection(): "+e);
		 }
		 return null;
	}
	

    /** The trust ALL TrustManager. Used by createALLTrustManager()
     *  to replace the default SSLSocketFactory TrustManager.
     */
    private TrustManager[] trustAllCerts = new TrustManager[] {
         new X509TrustManager() {
              public void checkClientTrusted(X509Certificate[] chain,
			  	                                String authType) {}
			  public void checkServerTrusted(X509Certificate[] chain,
			  	                                String authType) {}
			  public X509Certificate[] getAcceptedIssuers() {
	    		return null;
		 	  }
         }
    };

    /** Retrieves the Servers SSL Certificate chain.
     *  @param hostname The SSL host to retrieve the certicate
     *          chain from, i.e. www.verisign.com.
     *  @param port The port number of the host that accepts
     *          SSL connections. This is usually 443 unless the host
     *          has been configured to accept SSL connections on
     *          another port.
     *  @return javax.security.cert.X509Certificate[]
     *          The host server's certificate chain. Null if there
     *          was a problem retrieving the certificate chain.
     */
    public javax.security.cert.X509Certificate[] getServerCert(String hostname, int port) {

         javax.security.cert.X509Certificate[] serverCerts = null;
         try {
              // Create client socket
              SSLSocketFactory factory = HttpsURLConnection.getDefaultSSLSocketFactory();
              SSLSocket socket = (SSLSocket)factory.createSocket(hostname,port);

              // Without doing the handshake first we get
              // "javax.net.ssl.SSLPeerUnverifiedException:
              // peer not authenticated"
              // Connect to the server
              socket.startHandshake();

              // Retrieve the server's certificate chain
              serverCerts = socket.getSession().getPeerCertificateChain();

              // Close the socket
              socket.close();
         } catch (Exception e) {
         	logger.log(Level.WARNING,"getServerCert(): Exception: "
                   + e.toString() + "\n" + e.getMessage());
              e.printStackTrace();
         }
         return serverCerts;
    }


    /** Adds the specified certificate to a KeyStore file.
     *
     *  <p>The system default KeyStore, or TrustManager, file
     *  used is either jssecacerts or cacerts, located in
     *  the <java.home>/lib/security directory.
     *
     *  @param keystoreFile The KeyStore, or TrustManager, file
     *          that the Certificate is to be added to. This is
     *          either a relative path from the execution directory
     *          or an absolute system path.
     *  @param keystorePassword The password to use on the file.
     *  @param alias An alias that the Certificate will be stored
     *          under.
     *  @param cert The Certificate to add.
     *  @return boolean True if the Certificate was successfully
     *           added, False otherwise.
     */
    public boolean addToKeyStore(
              String keystoreFile, char [] keystorePassword,
              String alias, javax.security.cert.Certificate cert) {
         try {
              // Create an empty keystore
              java.security.KeyStore keystore =
                   java.security.KeyStore.getInstance(
                        java.security.KeyStore.getDefaultType());

              // Read in existing keystore data. This is need
              // to initialize the KeyStore.
              FileInputStream in = new FileInputStream(keystoreFile);
              keystore.load(in, null);
              in.close();

              ////////////////////////////////////////
              // This block converts a
              // javax.security.cert.Certiticate to
              // the legacy java.security.cert.Certificate
              // format. Use this with problems where a
              // cast exception (javax.security.cert ->
              // java.security.cert) occurs. The newer
              // versions of the X509 certificates are
              // part of the 'javax' hierarchy and are
              // not compatible with the legacy Class
              // files under the 'java' hierarchy.
              // <code>
              ByteArrayInputStream bais =
                   new ByteArrayInputStream(cert.getEncoded());
              java.security.cert.CertificateFactory cf =
                   java.security.cert.CertificateFactory.getInstance(
                        "X.509");
              java.security.cert.Certificate certP = null;
              while (bais.available() > 0) {
                   certP = cf.generateCertificate(bais);
              }
              // </code>
              // End Certificate conversion block
              ////////////////////////////////////////

              // Add the certificate
              keystore.setCertificateEntry(
                   alias, certP);

              // Save the new keystore contents
              FileOutputStream out =
                   new FileOutputStream(keystoreFile);
              keystore.store(out, keystorePassword);
              out.close();
         } catch (Exception e) {
         	logger.log(Level.WARNING,"addToKeyStore(): Exception: "
                   + e.toString() + "\n" + e.getMessage());
              return false;
         }
         return true;
    }
  }