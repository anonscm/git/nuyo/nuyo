/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JComponent;

import org.evolvis.nuyo.controller.DoubleCheckManager;
import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetComboBox;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressTextField;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class LandField extends ContactAddressTextField
{
  private TarentWidgetComboBox m_oCombobox_land;  
  private TarentWidgetLabel m_oLabel_land;
  private FocusListener m_oFocusListener = null;
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
    if (issensitive)
    {
      if (m_oFocusListener != null) m_oCombobox_land.removeFocusListener(m_oFocusListener);
      m_oFocusListener = new combo_focus_doublecheck(m_oCombobox_land);
      m_oCombobox_land.addFocusListener(m_oFocusListener);
    }
    else
    {
      {
        if (m_oFocusListener != null) m_oCombobox_land.removeFocusListener(m_oFocusListener);
        m_oFocusListener = null;
      }      
    }
  }
  
  public void postFinalRealize()
  {    
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createLandPanel(widgetFlags);
    return panel;
  }

  public String getKey()
  {
    return "LAND";
  }

  public void setEditable(boolean iseditable)
  {
    m_oCombobox_land.setEnabled(iseditable);
	  
    if (iseditable)
        checkLand();
  }

  public void setData(PluginData data)
  {
    Object land = data.get(AddressKeys.LAND);
    if (land != null) m_oCombobox_land.setSelectedIndex(getGUIListener().getIndexOfLand(land.toString()));
    else m_oCombobox_land.setSelectedIndex(0);
  }

  public void getData(PluginData data)
  {
    data.set(AddressKeys.LAND, m_oCombobox_land.getSelectedItem().toString());    
  }

  public boolean isDirty(PluginData data)
  {
    if (!((m_oCombobox_land.getSelectedItem().equals("")) && (data.get(AddressKeys.LAND) == null))) //$NON-NLS-1$
      if (!(m_oCombobox_land.getSelectedItem().equals(data.get(AddressKeys.LAND)))) return(true);
    
    return false;
  }
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  public String getFieldName()
  {
    if (fieldName != null) return fieldName; 
    else return Messages.getString("GUI_MainFrameNewStyle_Standard_Land");
  }
  
  public String getFieldDescription()
  {
    if (fieldDescription != null) return fieldDescription;
    else return Messages.getString("GUI_MainFrameNewStyle_Standard_Land_ToolTip");    
  }
  
  
  
  private EntryLayoutHelper createLandPanel(String widgetFlags)
  {
    m_oCombobox_land = new TarentWidgetComboBox(getGUIListener().getLaenderList().toArray());
    m_oCombobox_land.setEditable(false);
    m_oCombobox_land.setSelectedIndex(0);
    m_oCombobox_land.addActionListener(new combobox_land_changed());
    m_oCombobox_land.setToolTipText(getFieldDescription()); //$NON-NLS-1$
    getWidgetPool().addWidget("COMBO_LAND", m_oCombobox_land);    
    
    m_oLabel_land = new TarentWidgetLabel(getFieldName());
    return(new EntryLayoutHelper(m_oLabel_land, m_oCombobox_land, widgetFlags));
  }



  private void checkLand()
  {
    if (getWidgetPool().isWidgetRegistered("COMBO_BUNDESLAND"))
    {  
      JComponent comp = getWidgetPool().getWidget("COMBO_BUNDESLAND");
      
      boolean landIsGermany = m_oCombobox_land.getSelectedItem().toString().equalsIgnoreCase(Messages.getString("GUI_MainFrameNewStyle_Standard_Deutschland"));
      
      comp.setEnabled(m_oCombobox_land.isEnabled());
      
      if (!landIsGermany){
    	  ((TarentWidgetComboBox)comp).setSelectedIndex(0);
    	  comp.setEnabled(false);
      } 
    }
  } 
  
  public class combobox_land_changed implements ActionListener
  {
    public void actionPerformed(ActionEvent e)      
    {
      checkLand();
    } 
  }

  
  private class combo_focus_doublecheck implements FocusListener
  {
    TarentWidgetComboBox combo;  
    Object value;

    public combo_focus_doublecheck(TarentWidgetComboBox cb)
    {
      combo = cb;            
    }
    
    public void focusGained(FocusEvent fe)
    {
      value = combo.getSelectedItem();    
    }
    
    public void focusLost(FocusEvent fe)
    {
      if (!(combo.getSelectedItem().equals(value)))
      {          
        // trigger DoublettenCheck
    	  DoubleCheckManager.getInstance().checkForDoublesAndHandleResults();           
      }
    }
  }          
  
}
