package org.evolvis.nuyo.gui.admin;

/**
 * A listener will be notified about category roles update.
 * This is the case when any <i>commit-action</i> was performed successfully.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public interface CategoryRolesListener {
    
    public void categoryRolesUpdated();
}
