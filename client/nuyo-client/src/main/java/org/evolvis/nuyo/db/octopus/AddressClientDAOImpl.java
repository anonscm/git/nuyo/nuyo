package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressClientDAO;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.AddressId;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.remote.Method;
import org.evolvis.nuyo.remote.SoapClient;

/**
 * @author Sebastian Mancke, tarent GmbH
 * 
 * This is a management class for address obejcts.
 *  
 */
public class AddressClientDAOImpl implements AddressClientDAO {
    
    OctopusDatabase db;

    public AddressClientDAOImpl(OctopusDatabase db) {
        this.db = db;
    }

    /**
     * Saves the address.
     * The the changed-date of the supplied object will be set.
     */
	public void save(Address address) throws ContactDBException {
        Method method = new Method("saveAddress");
        method.add(AddressWorkerConstants.PARAM_ADDRESS, address);
        Address savedAddress = (Address)method.invoke();
        if (savedAddress == null)
            throw new ContactDBException(ContactDBException.EX_OBJECT_INCOMPLETE, Messages.getString("AddressClientDAOImpl_No_Address_Object"));
        address.setAenderungsdatum(savedAddress.getAenderungsdatum());
    }

    /**
     * Create a new address.
     * The the id, creation- and changed-date of the supplied object will be set.
     * @param Address the address to save
     * @param subcategorieList A list of integer id's with the initial subcategories
     */
	public void create(Address address, List subCategories) throws ContactDBException {
		Method method = new Method("createAddress");
        method.add(AddressWorkerConstants.PARAM_ADDRESS, address);
        method.add(AddressWorkerConstants.PARAM_ADD_SUB_CATEGORIES, subCategories);
        Address savedAddress = (Address)method.invoke();
        if (savedAddress == null)
            throw new ContactDBException(ContactDBException.EX_OBJECT_INCOMPLETE, "No address object returned by server");
        address.setId(savedAddress.getId());
        address.setErfassungsdatum(savedAddress.getErfassungsdatum());
        address.setAenderungsdatum(savedAddress.getAenderungsdatum());
    }

    /**
     * Deletes the address completely (not only from some categories)
     */
	public void delete(Address address) throws ContactDBException {
        Method method = new Method("deleteAddress");
        method.add(AddressWorkerConstants.PARAM_ADDRESS_ID, new Integer(address.getId()));
        method.invoke();
	}
    
	public Addresses getDoubles(Address address, int exclude) throws ContactDBException {
		Map sm = new HashMap();
		sm.put("exclude", new Integer(exclude));
		sm.put("vorname", address.getVorname());
		sm.put("name", address.getNachname());
		sm.put("organisation", address.getInstitution());
		sm.put("strasse", address.getStrasse());
		sm.put("hausNr", address.getHausNr());
		sm.put("ort", address.getOrt());
        sm.put("plz", address.getPlz());
		sm.put("postfach", address.getPostfachNr());
		sm.put("plzpost", address.getPlzPostfach());
		sm.put("telefonP", address.getTelefonPrivat());
		sm.put("telefonD", address.getTelefonDienstlich());
		sm.put("handyP", address.getHandyPrivat());
		sm.put("handyD", address.getHandyDienstlich());
		sm.put("faxP", address.getFaxPrivat());
		sm.put("faxD", address.getFaxDienstlich());
        sm.put("email", address.getEmailAdresseDienstlich());
        sm.put("emailp", address.getEmailAdressePrivat());
        Map duplicates = (Map) SoapClient.getDuplicates(sm, new Integer(exclude));
        
        if (duplicates == null || duplicates.size() < 1)
        		return null;
        // TODO: it would be better to do this in the gui layer
        int maxDoubles = 30;
        if (duplicates.size() > maxDoubles) {
            ApplicationServices.getInstance().getCommonDialogServices().showInfo(Messages.getFormattedString("AM_Doubles_Shrinked", new Integer(duplicates.size()), new Integer(maxDoubles)));
        }

 		ArrayList addressPks = new ArrayList();

        int counter = 0;
 		for (Iterator it = duplicates.values().iterator(); it.hasNext() && counter++ < maxDoubles;)
            addressPks.add(new Integer(it.next().toString()));
        
        AddressesImpl addresses = new AddressesImpl(db);
        addresses.setPkFilter(addressPks);
        addresses.initialLoad();
        addresses.waitFor(0);
        return addresses;
	}
    
    /**
     * Returns the contact history (aka "Journal") for an address
     */
	public List getHistory(Address address)
        throws ContactDBException {
        
	    // 	Falls keine Adresse ausgew�hlt ist,
	    //	nicht anfragen und einfach eine leere liste 
	    //	zur�ckgeben, da ansonsten ein Securityfehler
	    //	auf dem Server Auftritt
	    if (address == null || address.getAdrNr() <= 0)
	    	return Collections.EMPTY_LIST; 
        
	    ISelection f = Selection.getInstance()
	    	.add(new AddressId(new Integer(address.getAdrNr())));	
        return (List)ContactImpl.getContacts(f);
	}


    /**
     * Returns the user associated with an address, 
     * or null, if non is associated.
     */
 	public User getAssociatedUser(Address address) throws ContactDBException {
        if (address.getAssociatedUserID() != null)
            return new UserImpl(address.getAssociatedUserID());
        return null;
    }

    /**
     * TODO: Addresszugriff: set the associated user
     * Associate a user with an address.
     */
 	public void setAssociatedUser(Address address, User user) throws ContactDBException {
        throw new RuntimeException("AddressClientDAOImpl#setAssociatedUser() not implemented yet");
    }


// TODO: Addresszugriff: should we also implement this here?
//        
// 	/**
// 	 * Der Login des Benutzers, der diesem Addressobjekt zugeordnet ist. Wenn
// 	 * keine Zuordnung besteht: <code>null/code>
// 	 */
// 	public String getAssociatedUserLogin() throws ContactDBException {
//         User user = new UserImpl(getAssociatedUserID());
//         if (user == null)
//             return null;
//         return user.getLoginName();
// 	}

}
