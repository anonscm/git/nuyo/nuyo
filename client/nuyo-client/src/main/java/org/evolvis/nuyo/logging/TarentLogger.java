/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 Copyright (C) 2002 tarent GmbH

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 tarent GmbH., hereby disclaims all copyright
 interest in the program 'tarent-contact'
 (which makes passes at compilers) written
 by Nikolai R�ther.
 signature of Elmar Geese, 1 June 2002
 Elmar Geese, CEO tarent GmbH*/
package org.evolvis.nuyo.logging;

import java.awt.Toolkit;
import java.awt.Window;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;


/**
 * @author niko
 */
public class TarentLogger {

    private static Window window = null;
	private Logger		logger;
	private List		logEntryList;
	private List		logListenerList;
	private int			maxEntriesCount	= -1;
	private LogWindow	logWindow;
	private Level		popUpLevel;
	private GUIListener	guiListener;
	private User		user;
	private Map			counterMap;

	public TarentLogger(Logger aLogger) {
		
		logger = aLogger;
		
		init();
	}

	public TarentLogger(Class aClass) {
		
		logger = Logger.getLogger(aClass.getName());
		
		init();
	}

	private void init() {
		
		logEntryList = new ArrayList();
		
		logListenerList = new ArrayList();
		
		counterMap	= new HashMap();
	}
	
	public void setGUIListener(GUIListener gl) {
		guiListener = gl;
	}


	// -------------------------------------------

	public Logger getLogger() {
		return logger;
	}



	public void setCurrentUser(User aUser) {
		user = aUser;
	}


	public User getCurrentUser() {
		return user;
	}


	public LogWindow getLogWindow() {

		if (logWindow == null) {
			
			logWindow = createLogWindow();
		}

		return logWindow;
	}


	public void setMaxEntriesCount(int value) {
		maxEntriesCount = value;
		checkMaxEntries();
	}


	public void setLevelToPopUp(Level level) {
		popUpLevel = level;
	}


	public void addLogListener(TarentLogListener loglistener) {
		logListenerList.add(loglistener);
	}


	public void removeLogListener(TarentLogListener loglistener) {
		logListenerList.remove(loglistener);
	}


	public void log(Level level, String msg) {
		logger.log(level, msg);
		addLogEntry(new LogEntry(level, msg));
	}


	public void log(Level level, String msg, Object param1) {
		logger.log(level, msg, param1);
		addLogEntry(new LogEntry(level, msg, param1));
	}


	public void log(Level level, String msg, Object params[]) {
		logger.log(level, msg, params);
		addLogEntry(new LogEntry(level, msg, params));
	}


	public void log(Level level, String msg, Throwable thrown) {
		logger.log(level, msg, thrown);
		addLogEntry(new LogEntry(level, msg, thrown));
	}


	// -------------------------------------------

	public void setLevel(Level level) {
		logger.setLevel(level);
	}


	// -------------------------------------------

	public void fine(String msg) {
		log(Level.FINE, msg);
	}


	public void finer(String msg) {
		log(Level.FINER, msg);
	}


	public void finest(String msg) {
		log(Level.FINEST, msg);
	}


	public void config(String msg) {
		log(Level.CONFIG, msg);
	}
	
	
	public void info(String msg) {
		log(Level.INFO, msg);
	}
	
	/** Logs a serious failure and shows a message in Error Dialog.*/
	public void severe(String msg) {
		severe(msg, (String) null);
	}

	/** Logs a serious failure and shows a message in Error Dialog.*/
	public void severe(String msg, String causeDetails) {

	    if(msg == null) msg = Messages.getString("UNKOWN_ERROR_MESSAGE");
        
        logMsgAndNotifyUser(Level.SEVERE, msg, causeDetails);
	}
	
    /** Logs a serious failure and shows a message in Error Dialog.*/
	public void severe(String msg, Throwable cause) {

        if(msg == null) msg = Messages.getString("UNKOWN_ERROR_MESSAGE") + " " + cause.getClass().getName();
        
        String extendedText = getCauseRootMessage(cause);
        
        cause.printStackTrace();
        
        logMsgAndNotifyUser(Level.SEVERE, msg, extendedText);
	}
	
	/** Logs a potential problem and shows a message in Error Dialog.*/
	public void warning(String msg) {		
		warning(msg, (String) null);
	}

    /** Logs silent a potential problem (doesn't show a warning dialog).*/
    public void warningSilent( String msg) {
        log(Level.WARNING, msg);
    }

    /** Logs silent a potential problem (doesn't show a warning dialog).*/
    public void warningSilent( String msg, Throwable e) {
        log(Level.WARNING, msg, e);
    }
    
	/** Logs a potential problem and shows a message in Error Dialog.*/
	public void warning(String msg, String causeDetails) {
        
        if(msg == null) msg = Messages.getString("UNKOWN_ERROR_MESSAGE");
        
        logMsgAndNotifyUser(Level.WARNING, msg, causeDetails);
	}

	/** Logs a potential problem and shows a message in Error Dialog.*/
	public void warning(String msg, Throwable cause) {
		
		if(msg == null) msg = Messages.getString("UNKOWN_ERROR_MESSAGE") + " " + cause.getClass().getName();
		
		String extendedText = getCauseRootMessage(cause);
        
        logMsgAndNotifyUser(Level.WARNING, msg, extendedText);
	}
	

    private void logMsgAndNotifyUser(Level level, String msg, String causeDetails) {
        
        log(level, msg + (causeDetails == null ? "" : ": " + causeDetails));
        
        Toolkit.getDefaultToolkit().beep();
        
        if(ApplicationServices.getInstance().getCommonDialogServices() != null){
            
            ApplicationServices.getInstance().getCommonDialogServices()
                .publishError(Messages.getString("GUI_WARNING_CAPTION"), msg, causeDetails);
            
        } else {
            
            showError(Messages.getString("GUI_WARNING_CAPTION"), msg, causeDetails);
        }
    }
    
    private void showError(String caption, String msg, String causeDetails){       
    	if(causeDetails != null)
    		JOptionPane.showMessageDialog(window, msg+"\r\n\n"+causeDetails, caption, JOptionPane.ERROR_MESSAGE);
    	else
    		JOptionPane.showMessageDialog(window, msg, caption, JOptionPane.ERROR_MESSAGE);
    }

	// Aller tiefste Exception als Cause Message verwenden.
	private String getCauseRootMessage(Throwable e) {

		if (e != null) {
    		
    		Throwable cause = e;
    		
    		while (cause.getCause() != null) {
    			
    			cause = cause.getCause();
    		}
    		
    		return cause.getMessage();
    	}
		
		return Messages.getString("SEE_LOG_MESSAGE");
	}
    

	public int getNumberOfEntriesByLevel(Level level) {
		
		Integer id = new Integer(level.intValue());
		
		Integer i = ((Integer) (counterMap.get(id)));
		
		if (i != null)
		
			return i.intValue();
		
		else
			
			return (0);
	}


	private int getCounter(LogEntry entry) {
		
		Integer id = new Integer(entry.getLevel().intValue());
		
		if (!(counterMap.containsKey(id))) {
		
			counterMap.put(id, new Integer(0));
			
			return 0;
			
		} else {
			
			return ((Integer) (counterMap.get(id))).intValue();
		}
	}


	private void setCounter(LogEntry entry, int i) {
		
		Integer id = new Integer(entry.getLevel().intValue());
		
		counterMap.put(id, new Integer(i));
	}


	private void addToCounter(LogEntry entry, int howmuch) {
		
		setCounter(entry, getCounter(entry) + howmuch);
	}


	private void substractFromCounter(LogEntry entry, int howmuch) {

		setCounter(entry, getCounter(entry) - howmuch);
	}


	private void addLogEntry(LogEntry entry) {
		
		logEntryList.add(entry);
		
		addToCounter(entry, 1);

		Iterator it = logListenerList.iterator();
		
		TarentLogListener listener;
		
		while (it.hasNext()) {
		
			listener = (TarentLogListener) it.next();
			
			listener.addedLogEntry(entry);
		}
		
		checkMaxEntries();

		if (popUpLevel != null) {
		
			if (entry.getLevel().intValue() >= popUpLevel.intValue()) {
			
				if (getLogWindow() != null)
					
					getLogWindow().showWindow(true);
			}
		}
	}


	private void checkMaxEntries() {
		
		if (maxEntriesCount == -1)
			return;

		if (getNumberOfLogEntries() > maxEntriesCount) {
			
			int numentriestokill = getNumberOfLogEntries() - maxEntriesCount;
			
			LogEntry entry;
			
			for (int i = 0; i < numentriestokill; i++) {
				
				entry = (LogEntry) (logEntryList.remove(0));
				
				substractFromCounter(entry, 1);
			}

			Iterator it = logListenerList.iterator();
			
			TarentLogListener listener;
			
			while (it.hasNext()) {
				
				listener = (TarentLogListener) it.next();
				
				listener.removedFirstLogEntries(numentriestokill);
			}
		}
	}


	// -------------------------------------------

	private LogWindow createLogWindow() {
		
		URL logImageBaseURL = this.getClass().getResource("/de/tarent/contact/resources/"); //$NON-NLS-1$    
		
		if (guiListener != null)
		
			return new SimpleLogWindow(guiListener, this, logImageBaseURL);
		
		else
			
			return null;
	}


	// -------------------------------------------

	public List getLogEntries() {
		return logEntryList;
	}


	public int getNumberOfLogEntries() {
		return logEntryList.size();
	}


	public LogEntry getLogEntry(int index) {
		
		return (LogEntry) logEntryList.get(index);
	}


	public void clearLogEntries() {
		
		int size = logEntryList.size();
		
		logEntryList.clear();

		Iterator it = logListenerList.iterator();
		
		TarentLogListener listener;
		
		while (it.hasNext()) {
		
			listener = (TarentLogListener) (it.next());
			
			listener.removedFirstLogEntries(size);
		}
	}


	// -------------------------------------------

	public boolean writeSingle(int index, LogFormatter formatter, LogWriter writer) {
		
		LogEntry entry = getLogEntry(index);
		
		return writer.writeLog(formatter.formatLog(entry));
	}


	public boolean writeAll(LogFormatter formatter, LogListFormatter listformatter, LogWriter writer) {
		
		List stringList = new ArrayList();
		
		for (int i = 0; i < getNumberOfLogEntries(); i++) {
		
			stringList.add(formatter.formatLog(getLogEntry(i)));
		}

		return writer.writeLog(listformatter.formatList(stringList));
	}

    public static void setParentWindow(Window currentWindow) {
        window = currentWindow;
    }

}
