package org.evolvis.nuyo.gui.categorytree;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.evolvis.xana.swing.utils.SwingIconFactory;

/**
 * Instances of this class denote how a category or subcategory is selected.
 * 
 * @author Robert Schuster
 *
 */
class Selection
{
  /**
   * Denotes that a [sub]category is marked as a positive in a selection.
   * That may mean that it is explicitly included in an operation or that
   * it will be part of an add operation. 
   */
  final static Selection POSITIVE = new Selection("positive", SwingIconFactory.CATEGORY_TREE_POSITIVE_SELECTION);

  /**
   * Denotes that a [sub]category is marked as a negative in a selection.
   * That may mean that it is explicitly excluded from an operation or that
   * it will be part of a remove operation. 
   */
  final static Selection NEGATIVE = new Selection("negative", SwingIconFactory.CATEGORY_TREE_NEGATIVE_SELECTION);

  /**
   * Denotes that a [sub]category has no indication for the current operation.
   * It means that it will not be regarded for the operation.  
   */
  final static Selection NEUTRAL = new Selection("neutral", SwingIconFactory.CATEGORY_TREE_NEUTRAL_SELECTION);

  /**
   * Denotes that a category's children (= subcategories) do not share a common
   * state.
   * 
   * <p>Only categories can have this selection state, subcategories do not!</p> 
   */
  final static Selection MIXED = new Selection("mixed", SwingIconFactory.CATEGORY_TREE_MIXED_SELECTION);
  
  String label;
  
  Icon icon;
  
  private Selection(String label, String iconKey)
  {
    this.label = label;
    icon = (ImageIcon)SwingIconFactory.getInstance().getIcon(iconKey);
  }
  
  public String toString()
  {
    return label;
  }
 
  Icon getIcon()
  {
    return icon;
  }
  
}