package org.evolvis.nuyo.db;

public class Reminder {
	
	String description;
	int linkedAppointment;
	long offSetInSeconds;
	
	public Reminder(String desc, int app, long offset){
		if (desc != null) description = desc;
		else description = "";
		
		linkedAppointment = app;
		offSetInSeconds = offset;		
	}
	
	public int getLinkedAppointment() {
		return linkedAppointment;
	}

	public void setLinkedAppointment(int linkedAppointment) {
		this.linkedAppointment = linkedAppointment;
	}

	public long getOffSet() {
		return offSetInSeconds;
	}

	public void setOffSet(long offSetInSeconds) {
		this.offSetInSeconds = offSetInSeconds;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
