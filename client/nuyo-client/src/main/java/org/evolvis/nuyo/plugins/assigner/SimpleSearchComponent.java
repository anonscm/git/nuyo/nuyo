package org.evolvis.nuyo.plugins.assigner;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.selector.Selector;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.commons.datahandling.ListFilterPropertyName;

public class SimpleSearchComponent implements ActionListener, MouseListener, KeyListener{
    
    private static Logger logger = Logger.getLogger(SimpleSearchComponent.class.getName());
    private static final String SEARCH_FIELD_TEXT="Ihr Suchbegriff";
    private static final Color COLOR_INIT_TEXT = Color.GRAY;
    
    private Color textColor;
    private boolean initState=true;
    private JPanel mainPanel;
    
    private SimpleCategorySelector categorySelector;
    private JComboBox comboSearchFields;
    private Map fieldkeys;
    
    private JButton searchButton;
    private JTextField fieldSearchText;
    
    private Database database;
    private Addresses addresses;
    
    private List resultListeners;
    private ImageIcon iconFilter;
    private Map criteria;
    private List filters;
    
    private AddressListParameter param;
    
    private static  String[] FIELD_NAMES = {"<Label>", "Herr/Frau", "Nachname", "Vorname", "Titel", "akadamischer Titel", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
        "Namenszusatz", "Institution", "Stra�e", "Postleitzahl", "Ort", "Land", "Landeskennzeichen", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
        "Bundesland", "Telefon dienstlich", "Telefon privat", "Fax dienstlich", "Fax privat", "E-Mail"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
    private static String[] FIELD_KEYS = {Address.PROPERTY_SHORT_ADDRESS_LABEL.getKey(), Address.PROPERTY_HERRN_FRAU.getKey(), Address.PROPERTY_NACHNAME.getKey(),
                                          Address.PROPERTY_VORNAME.getKey(), Address.PROPERTY_TITEL.getKey(), Address.PROPERTY_AKAD_TITEL.getKey(),
                                          Address.PROPERTY_NAMENSZUSATZ.getKey(), Address.PROPERTY_INSTITUTION.getKey(), Address.PROPERTY_STRASSE.getKey(),
                                          Address.PROPERTY_PLZ.getKey(), Address.PROPERTY_ORT.getKey(), Address.PROPERTY_LAND.getKey(), Address.PROPERTY_LKZ.getKey(),
                                          Address.PROPERTY_BUNDESLAND.getKey(), Address.PROPERTY_TELEFON_DIENSTLICH.getKey(), Address.PROPERTY_TELEFON_PRIVAT.getKey(),
                                          Address.PROPERTY_FAX_DIENSTLICH.getKey(), Address.PROPERTY_FAX_PRIVAT.getKey(), Address.PROPERTY_E_MAIL_DIENSTLICH.getKey()};

    
    public SimpleSearchComponent(Database db) {
        
        database = db;
        filters = new ArrayList();
        criteria = new HashMap();
        resultListeners = new ArrayList();
        fieldkeys =new HashMap();
        categorySelector = new SimpleCategorySelector();
        categorySelector.showVirtualCategories(true);
        categorySelector.showAllSubcategoriesEntry(true);
        comboSearchFields = new JComboBox();
        fieldSearchText = new JTextField();
        fieldSearchText.addMouseListener(this);
        fieldSearchText.addKeyListener(this);
        textColor=fieldSearchText.getForeground();
        toggleInitState(true);
        searchButton = new JButton(Messages.getString("SimpleSearchComponent.button_search")); //$NON-NLS-1$
        searchButton.addActionListener(this);
        param = new AddressListParameter();
        try {
			addresses = database.getChosenAddresses();
		} catch (ContactDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

       init();
    }
    
    public void init(){
        //initialisiere GUI
        categorySelector.loadCategoryLists();
        fillFieldList();
    }
    
    private void fillFieldList() {
        for (int i = 0; i < FIELD_NAMES.length; i++) {
           comboSearchFields.addItem(FIELD_NAMES[i]);
        }
    }
    
//    private void updateFiltersAndCriteria(List categories, List subcategories, String fieldKey, String searchText){
//        
//        //---- update filter -------
//        
//        CategoriesFilter categoriesFilter = new CategoriesFilter();
//        SubCategoriesFilter subCategoriesFilter = new SubCategoriesFilter();
//        try {
//            Iterator iterator = categories.iterator();
//            while (iterator.hasNext()){
//                categoriesFilter.add((Category)iterator.next());
//            }
//            iterator = subcategories.iterator();
//            while (iterator.hasNext()){
//                SubCategory s = (SubCategory)iterator.next();
//                subCategoriesFilter.add(s);
//            }
//        } catch (ContactDBException e1) {
//            logger.warning(Messages.getString("SimpleSearchComponent.error_while_adding_a_category")); //$NON-NLS-1$
//            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("SimpleSearchComponent.error_while_adding_a_category")); //$NON-NLS-1$
//        }
//        filters.clear();
//        filters.add(categoriesFilter);
//        filters.add(subCategoriesFilter);
//        logger.finer("Filter wurde aktualisiert"); //$NON-NLS-1$
//        
//        //--------- update criteria ---------
//        criteria.clear();
//        criteria.put(fieldKey, searchText);
//        criteria.put("matchCodes", Boolean.TRUE); //$NON-NLS-1$
//        logger.finer("Kriterien wurden aktualisiert"); //$NON-NLS-1$
//    }
    
    private void updateFiltersAndCriteria(String fieldKey, String searchText, List category, List subCategory){
    	param.clearFilter();
    	List temp = new ArrayList();
    	Iterator iter = subCategory.iterator();
    	while(iter.hasNext()){
    		Object next = iter.next();
    		SubCategory subCat = (SubCategory)next;
    		temp.add(new Integer(subCat.getId()));
    	}
    	param.setDisjunctionSubcategories(temp);
    	param.setFilterList(Arrays.asList(new Object[]{new ListFilterPropertyName(fieldKey), "%"+searchText+"%", ListFilterOperator.LIKE}));
    	System.out.println(param.isCategoryFilterEnabled());
    	System.out.println(param.isSearchFilterEnabled());
    	
    }
    
    /*
     * calls for a new addresses-object with matching filters an criteria
     */
    private void updateAddresses(){    	
    	addresses.setAddressListParameter(param);
    }
    
    private JPanel buildPanel(){
        FormLayout layout = new FormLayout(
                "pref:GROW, 3dlu, right:pref, 3dlu, pref:GROW, 3dlu, pref", //columns  //$NON-NLS-1$
        "pref, 3dlu, pref");      // rows //$NON-NLS-1$
        //create and configure a builder
        PanelBuilder builder = new PanelBuilder(layout);
        builder.setDefaultDialogBorder();
        
        //Obtain a reusable constraints object to place components in the grid.
        CellConstraints cc = new CellConstraints();
        builder.add(categorySelector.getComponent(), cc.xywh(1,1,1,3));
        builder.addLabel(Messages.getString("SimpleSearchComponent.addressfield"), cc.xy(3,1)); //$NON-NLS-1$
        builder.addLabel(Messages.getString("SimpleSearchComponent.searchtext"), cc.xy(3,3)); //$NON-NLS-1$
        builder.add(comboSearchFields, cc.xy(5,1));
        builder.add(fieldSearchText, cc.xy(5,3));
        builder.add(searchButton, cc.xy(7,1));
        return builder.getPanel();
    }
    
    private void toggleInitState(boolean init){
        if (initState){
            if (init){
                fieldSearchText.setForeground(COLOR_INIT_TEXT);
                fieldSearchText.setText(this.SEARCH_FIELD_TEXT);
            } else {
                fieldSearchText.setForeground(textColor);
                fieldSearchText.setText("");
                initState=false;
            }
        }
    }
    
    private JPanel buildVerticallyAlignedPanel(){
        FormLayout layout = new FormLayout(
                "pref, 3dlu, pref", //columns  
        "pref, 3dlu, pref,3dlu, pref,3dlu, pref");      // rows 
        
        //create and configure a builder
        PanelBuilder builder = new PanelBuilder(layout);
        builder.setDefaultDialogBorder();
        //Obtain a reusable constraints object to place components in the grid.
        CellConstraints cc = new CellConstraints();
        builder.add(fieldSearchText, cc.xy(1,1));
        builder.add(comboSearchFields, cc.xy(1,3));
        builder.add(categorySelector.getComboCategories(), cc.xy(1,5));
        builder.add(categorySelector.getComboSubcategories(), cc.xy(1,7));
        builder.add(searchButton, cc.xy(3,1));
       return builder.getPanel();
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==searchButton){
            //search button was clicked
            if (initState) toggleInitState(false);
            searchButtonClicked();
        }
    }
    
    public void searchButtonClicked(){
        logger.finer("Suche wird gestartet"); //$NON-NLS-1$
        ApplicationServices.getInstance().getCommonDialogServices().setWaiting(true);
        List categories = categorySelector.getSelectedCategories();
        List subcategories = categorySelector.getSelectedSubcategories();
        String fieldkey = FIELD_KEYS[comboSearchFields.getSelectedIndex()];
        String searchtext = /*"*" + */fieldSearchText.getText()/*.trim() + "*"*/; //$NON-NLS-1$ //$NON-NLS-2$
        updateFiltersAndCriteria(fieldkey, searchtext, categories, subcategories);
        updateAddresses();        
        fireSearchCompleted();
        ApplicationServices.getInstance().getCommonDialogServices().setWaiting(false);
    }
    
    public JPanel getComponent(){
        if (mainPanel==null) mainPanel=buildVerticallyAlignedPanel();
        return mainPanel;
    }
    
    public Addresses getAddresses() {
        return addresses;
    }
 
    public void setAddresses(Addresses addresses) {
        this.addresses = addresses;
    }
    
    public void addResultListener(Object o){
        resultListeners.add(o);
    }
    public boolean removeResultListener(Object o){
        return resultListeners.remove(o);
    }
    public void fireSearchCompleted(){
        Iterator iterator = resultListeners.iterator();
        while (iterator.hasNext()){
            ((Selector)iterator.next()).searchCompleted();
        }
    }
    
    //------------- Getters and Setters -----------------
    
    public JTextField getFieldSearchText() {
        return fieldSearchText;
    }
    
    public void setFieldSearchText(JTextField fieldSearchText) {
        this.fieldSearchText = fieldSearchText;
    }
    
    public SimpleCategorySelector getCategorySelector() {
        return categorySelector;
    }
    
    public void setCategorySelector(SimpleCategorySelector categorySelector) {
        this.categorySelector = categorySelector;
    }
    
    public JComboBox getComboSearchFields() {
        return comboSearchFields;
    }
    
    public void setComboSearchFields(JComboBox comboSearchFields) {
        this.comboSearchFields = comboSearchFields;
    }
    
    public JButton getSearchButton() {
        return searchButton;
    }
    
    public void setSearchButton(JButton searchButton) {
        this.searchButton = searchButton;
    }
    
    public void mouseClicked(MouseEvent e) {
        toggleInitState(false);		
    }
    
    public void mousePressed(MouseEvent e) {
        //do nothing here
    }
    
    public void mouseReleased(MouseEvent e) {
        //do nothing here		
    }
    
    public void mouseEntered(MouseEvent e) {
        //do nothing here
    }
    
    public void mouseExited(MouseEvent e) {
        //do nothing here
    }
    
    public void keyTyped(KeyEvent e) {
        if (initState) toggleInitState(false);         
    }
    
    public void keyPressed(KeyEvent e) {
        // do nothing here
    }
    
    public void keyReleased(KeyEvent e) {
        // do nothing here
    }
}
