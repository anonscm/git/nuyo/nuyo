/**
 * 
 */
package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.UIManager.LookAndFeelInfo;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.config.ConfigManager;

/**
 * An action to switch the used Look And Feel at runtime
 * 
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class SwitchLookAndFeelAction extends AbstractGUIAction {

	public final static TarentLogger logger = new TarentLogger(SwitchLookAndFeelAction.class);

	public void actionPerformed(ActionEvent arg0) {

		LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
		Map lafNameMap = new HashMap();

		for(int i=0; i < lafs.length; i++)
		{
			lafNameMap.put(lafs[i].getName(), lafs[i].getClassName());
		}

		String selected = (String)JOptionPane.showInputDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame(), Messages.getString("GUI_SWITCH_LOOK_AND_FEEL_MESSAGE"), Messages.getString("GUI_SWITCH_LOOK_AND_FEEL_TITLE"), JOptionPane.QUESTION_MESSAGE, null, lafNameMap.keySet().toArray(), UIManager.getLookAndFeel().getName());

		if(selected == null) return;

		ConfigManager.getPreferences().put(StartUpConstants.PREF_LOOK_AND_FEEL, (String)lafNameMap.get(selected));
		
		try {
			UIManager.setLookAndFeel((String)lafNameMap.get(selected));

			// need to update the whole gui
			SwingUtilities.updateComponentTreeUI(ApplicationServices.getInstance().getCommonDialogServices().getFrame());
			ApplicationServices.getInstance().getCommonDialogServices().getFrame().validate();
		} catch (ClassNotFoundException e) {
			logger.warning(Messages.getString("GUI_SWITCH_LOOK_AND_FEEL_ERROR"), e);
		} catch (InstantiationException e) {
			logger.warning(Messages.getString("GUI_SWITCH_LOOK_AND_FEEL_ERROR"), e);
		} catch (IllegalAccessException e) {
			logger.warning(Messages.getString("GUI_SWITCH_LOOK_AND_FEEL_ERROR"), e);
		} catch (UnsupportedLookAndFeelException e) {
			logger.warning(Messages.getString("GUI_SWITCH_LOOK_AND_FEEL_ERROR"), e);
		}
	}
}
