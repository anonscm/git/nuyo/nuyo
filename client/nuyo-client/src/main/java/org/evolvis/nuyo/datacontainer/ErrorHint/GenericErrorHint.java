/*
 * Created on 14.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorHint;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GenericErrorHint extends ErrorHintAdapter
{
  public GenericErrorHint(String name, String description, Exception e, GUIElement guielement, DataContainer datacontainer)
  {
    super(name, description, e, guielement, datacontainer);
  }

}
