package org.evolvis.nuyo.db.persistence;

import org.evolvis.nuyo.db.ContactDBException;

/**
 * @author kleinw
 *
 *	Hiermit kann "gelauscht" werden, ob sich 1-n Beziehungen ge�ndert haben.
 *
 */
public interface EntityRelationListener {
    
    public void relationAdded(EntityRelationEvent event) throws ContactDBException;
    public void relationRemoved(EntityRelationEvent event) throws ContactDBException;
    
}
