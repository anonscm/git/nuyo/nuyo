/*
 * Created on 30.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class UserPluginData implements PluginData
{  
  public final static Object KEY_USER = "KEY_USER";
  
  private User m_oUser;
  
  public UserPluginData(User user)
  {
    m_oUser = user;
  }

  public Object get(Object key)
  {
    if (KEY_USER.equals(key)) return m_oUser;
    return null;
  }

  public boolean set(Object key, Object value)
  {
    return false;
  }
  
  public Class getDatatype(Object key)
  {
    Object value = get(key);
    if (value != null) return value.getClass();
    else return null;
  }    
}
