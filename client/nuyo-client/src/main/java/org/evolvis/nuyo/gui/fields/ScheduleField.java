/*
 * Created on 14.04.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetPanel;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentRequest;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Schedule;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.octopus.AppointmentImpl;
import org.evolvis.nuyo.db.octopus.UserImpl;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.PersistenceManager;
import org.evolvis.nuyo.gui.calendar.AppointmentDefault;
import org.evolvis.nuyo.gui.calendar.MonthlyCalendarPanel;
import org.evolvis.nuyo.gui.calendar.SEPRegistry;
import org.evolvis.nuyo.gui.calendar.ScheduleData;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.calendar.ScheduleEntry;
import org.evolvis.nuyo.gui.calendar.ScheduleEntryListener;
import org.evolvis.nuyo.gui.calendar.ScheduleEntryPanel;
import org.evolvis.nuyo.gui.calendar.SchedulePanel;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 */
public class ScheduleField extends ContactAddressField implements CalendarVisibleElement
{
    private ScheduleField m_oThisScheduleField;
    
    private boolean m_bShowSavingAppointmentOnApplication = true;
    
    private TarentWidgetPanel m_Panel;
    
    private SchedulePanel m_oSchedulePanelDay;
    private SchedulePanel m_oSchedulePanelWeek;
    private MonthlyCalendarPanel m_oPanelMonth;
    
    private ScheduleData m_oScheduleDataDay;
    private ScheduleData m_oScheduleDataWeek;
    private ScheduleData m_oScheduleDataMonth;
    private EntryLayoutHelper m_oScheduleFieldPanel = null;
    
    private ScheduleDate m_oCurrentDate = new ScheduleDate();
    private ScheduleDate m_oEndDate = null;
    
    private ScheduleDate m_oLastDate = new ScheduleDate();
    private JPanel m_oCardPanel;
    private String m_sActiveCardID = null;
    
    private ScheduleNavigationField m_oCalendarNavigationField = null;
    private ScheduleDetailField m_oCalendarDetailField = null;
    
    //private List m_oActiveScheduleEntryPanels = null;
    private int m_ActiveAppointmentID = -1;
    private Date m_oDisplayedStart = null;
    private Date m_oDisplayedEnd = null;    
    private SchedulePanel m_oDisplayedSchedulePanel = null;
    
    
    public final static String CARD_DAY = "DAY";
    public final static String CARD_WEEK = "WEEK";
    public final static String CARD_MONTH = "MONTH";
    
    public String getFieldName()
    {
        return "Kalender";    
    }
    
    public String getFieldDescription()
    {
        return "Eine Kalenderansicht";
    }
    
    
    public boolean isWriteOnly()
    {
        return true;  
    }
    
    public int getContext()
    {
        return CONTEXT_USER;
    }  
    
    
    public EntryLayoutHelper getPanel(String widgetFlags)
    {
        m_oThisScheduleField = this;
        if(m_oScheduleFieldPanel == null) m_oScheduleFieldPanel = createPanel(widgetFlags);
        
        getAppointmentDisplayManager().addAppointmentDisplayListener(this);
        
        return m_oScheduleFieldPanel;
    }
    
    public void postFinalRealize()
    {    
        //setDateToToday();
        
        if (getDisplayedSchedulePanel() != null)
        {
            getDisplayedSchedulePanel().scrollToHour(8);
        }
    }
    
    public void setData(PluginData data)
    {
    }
    
    
    public void setDateToToday()
    {
        ScheduleDate date = (new ScheduleDate()).getFirstSecondOfDay();        
        
        if (ScheduleField.CARD_DAY.equals(getCardVisible()))
        {
        }
        else if (ScheduleField.CARD_WEEK.equals(getCardVisible()))
        {
            date = date.getMondayOfWeek();
        }
        if (ScheduleField.CARD_MONTH.equals(getCardVisible()))
        {
            date = date.getFirstDayOfMonth();
        }
        
        ScheduleNavigationField field = (ScheduleNavigationField)(getWidgetPool().getController(CalendarVisibleElement.NAVIGATOR));
        if (field != null)
        {  
            field.setCurrentDate(date);
        }
    }
    
     
    private void fillMonthPanel(MonthlyCalendarPanel oPanelMonth, Date start, Date end)
    {
        //Schedule schedule = getGUIListener().getSchedule(getGUIListener().getCurrentUser());
        if (true/*schedule != null*/)
        {  
            try
            {
                oPanelMonth.removeAllAppointments();
                Collection appointments = getAppointments(start, end);
                //Collection appointments = getCalendar().getAppointments(start, end, null);    
                //Collection appointments = schedule.getAppointments(start, end, null);
                
                if (appointments != null)
                {
                    Iterator appiter = appointments.iterator();
                    while (appiter.hasNext())
                    {
                        Appointment appointment = (Appointment)(appiter.next()); 
                        
                        oPanelMonth.addAppointment(appointment);
                    }
                }
            } 
            catch (ContactDBException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    
    
    private SchedulePanel getDisplayedSchedulePanel()
    {
        if (CARD_DAY.equals(getCardVisible()))
        {
            return m_oSchedulePanelDay;
        }
        else if (CARD_WEEK.equals(getCardVisible()))
        {
            return m_oSchedulePanelWeek;
        }
        
        return null;
    }
    
    public void fillSchedulePanel(boolean forcereload)
    {
        SchedulePanel oSchedulePanel = null;
        Date start = new ScheduleDate(m_oCurrentDate).getFirstSecondOfDay().getDate();
        Date end = m_oCurrentDate.getDate();
        
        if (CARD_DAY.equals(getCardVisible())){
            oSchedulePanel = m_oSchedulePanelDay;
            end = m_oCurrentDate.getDateWithAddedDays(1).getDate();
        }else if (CARD_WEEK.equals(getCardVisible())){
            oSchedulePanel = m_oSchedulePanelWeek;
            end = m_oCurrentDate.getDateWithAddedDays(7).getDate();
            m_oSchedulePanelWeek.setFirstDay(new ScheduleDate(start).getMondayOfWeek());
        }else if (CARD_MONTH.equals(getCardVisible())){
            oSchedulePanel = null;
            start = new ScheduleDate(start).getFirstDayOfMonth().getDate();
            end = new ScheduleDate(start).getLastSecondOfMonth().getDate();         
        }
        // nur neu aufbauen wenn nicht neues intervall
        if (oSchedulePanel != null) 
            if ((!forcereload) && (start.equals(m_oDisplayedStart) && end.equals(m_oDisplayedEnd) && oSchedulePanel.equals(m_oDisplayedSchedulePanel))) 
                return;
        
        if (oSchedulePanel == null)
        {      
            fillMonthPanel(m_oPanelMonth, start, end);
        }
        else
        {       
            // Wipe old panel
            if ( m_oDisplayedSchedulePanel != null) m_oDisplayedSchedulePanel.removeAllScheduleEntries();
            // Wipe new panel
            oSchedulePanel.removeAllScheduleEntries();
            if (getGUIListener().getCalendarCollection() == null) return;
            try
            {
                Collection appointments = getAppointments(start, end);
                
                if (appointments != null)
                {  
                    //System.out.println("ScheduleField::fillSchedulePanel: found Appointments to add (" + appointments.size() + ")");
                    Iterator appiter = appointments.iterator();
                    while (appiter.hasNext())
                    {
                        // Re-Add all Appointments in Daterange
                        Appointment appointment = (Appointment)(appiter.next());
                        
                        if (appointment.isPrivat()){
                            //System.out.println("Private: " +  appointment.toString());
                            if (getGUIListener().getCalendarCollection().getCalendar(appointment.getOwner().getCalendar(false).getId()) != null)
                                // Only add private Appointments when the owners calender is in CurrentCalender Selection
                                createScheduleEntrysByAppointment(oSchedulePanel, appointment);
                        }else{
                            // All other non-private stuff must be added
                            createScheduleEntrysByAppointment(oSchedulePanel, appointment);
                        }
                        
                        if (appointment.getId() == m_ActiveAppointmentID)
                            setAppointmentActive(appointment.getId(),true);
                    }
                }
            } 
            catch (ContactDBException e)
            {
                e.printStackTrace();
            }
        } 
        m_oDisplayedStart = start;
        m_oDisplayedEnd = end;    
        m_oDisplayedSchedulePanel = oSchedulePanel;
        if (! CARD_MONTH.equals(getCardVisible())) m_oDisplayedSchedulePanel.resolveDayConflicts();
    }
    
    /**
     * This method creates the SEPs for the given appointment and registers them in the
     * SEPRegistry. 
     * 
     * @param schedulepanel
     * @param appointment
     * @return List of created ScheduleEntryPanels
     */
    private List createScheduleEntrysByAppointment(SchedulePanel schedulepanel, Appointment appointment)
    {
        Vector SEPsforAppointment = new Vector();
        ScheduleEntry newentry = null;
        Calendar colorGivingCalendar = null;
        String colorString = null;
        boolean disableDragListener = false;
        boolean isReleasedAndEditable = false;
        
        
        try
        {
            ScheduleDate start = new ScheduleDate(appointment.getStart());
            ScheduleDate end = new ScheduleDate(appointment.getEnd());
            appointment.setTemporary(false);
            
            // Determine the source Calendar for the Appointment
            
            if (appointment.getOwner().getId() == getGUIListener().getUser(null, true).getId()){
                
                //Current user is owner of appointment
                colorGivingCalendar = getGUIListener().getUser(null, true).getCalendar(true);
                
            }else if ( getGUIListener().getCalendarCollection().getCollection().contains(appointment.getOwner().getCalendar(false))){
                
                //Other owners Calendar
                colorGivingCalendar = appointment.getOwner().getCalendar(false);
            
            }else{ 
                
                // Invited in a calendar, find it!
                Iterator calIter = appointment.getCalendars().iterator();
                while (calIter.hasNext()){
                    colorGivingCalendar = (Calendar)calIter.next();
                    if (getGUIListener().getCalendarCollection().getCalendar(colorGivingCalendar.getId()) != null) 
                        break;
                }
                
            }
            
            // Get Custom Color 
            Color newColor = Color.WHITE;

            // 25.10.05 Von Sebastion umgebogen, damit die Kalenderfarbe nicht jedes mal neu geholt wird
            // colorString = getGUIListener().getUserParameter(null, "CalendarColor_ID_" + colorGivingCalendar.getId());            
            colorString = getGUIListener().getCalendarColor(colorGivingCalendar.getId());
            if (colorString != null)
                newColor = Color.decode(colorString);
            
            // Disable DragListener for Appointments originating from a released Calender and not in WriteRange ...
            CalendarSecretaryRelation calsec = appointment.getOwner().getCalendar(false).getCalendarSecretaryforUser(getGUIListener().getUser(null, true).getId());
            if (calsec != null ){
                disableDragListener = !( calsec.getWriteDateRange().containsDate(appointment.getStart()) && 
                                        calsec.getWriteDateRange().containsDate(appointment.getEnd()));
                isReleasedAndEditable = !disableDragListener;
                
            }
            
            
            
            if (end.before(start)){
                //Oooops ....
                appointment.setEnd(start.getDateWithAddedHours(1).getDate());
                end = new ScheduleDate(appointment.getEnd());
                getGUIListener().getLogger().log(Level.WARNING, "ScheduleField::createScheduleEntrysByAppointment: Multi Day Appointment invalid! - fixed");
            }
                        
            if (!(start.isSameDay(end)))
            { 
                // Multiday Appointment
                int numdays = 0;
                ScheduleDate displayStart = schedulepanel.getFirstDay().getDateWithAddedDays(-1);
                ScheduleDate displayEnd = schedulepanel.getFirstDay().getDateWithAddedDays(schedulepanel.getNumDays());
                
                Date startdate = start.getDate();
                Date enddate = end.getDate();
                
                Date ddisstart = displayStart.getDate();
                Date ddisend = displayEnd.getDate();
                
                
                if (start.before(displayStart) && end.before(displayEnd)){
                    // Start before diplay-start but ends within displayed range
                    while( (! displayStart.getFirstSecondOfDay().getDateWithAddedDays(numdays).isSameDay(end) ) && ( numdays < 50) ){
                        //Full Days
                        ScheduleDate tmpstart = displayStart.getFirstSecondOfDay().getDateWithAddedDays(numdays);
                        ScheduleDate tmpend = tmpstart.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-1);
                        newentry = new ScheduleEntry(schedulepanel, tmpstart,tmpend,  appointment.isJob(), isReleasedAndEditable, null, newColor);
                        // getGUIListener().getLogger().log(Level.INFO, "ScheduleField::createScheduleEntrysByAppointment: Middle Day:"+ newentry.getStartDate().getDateTimeString() + " to " +newentry.getEndDate().getDateTimeString() );
                        newentry.setFuzzyEnd(true);
                        setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                        numdays++;
                    }
                    // Last day
                    newentry = new ScheduleEntry(schedulepanel, end.getFirstSecondOfDay(), end ,  appointment.isJob(), isReleasedAndEditable, null, newColor);
                    setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                    //-----------------------------------------------------------------------------------
                    
                }else if(start.after(displayStart) && end.before(displayEnd)){
                    // Start and End in DisplayRange
                    while (! displayStart.getFirstSecondOfDay().getDateWithAddedDays(numdays).isSameDay(end)){
                        if (displayStart.getFirstSecondOfDay().getDateWithAddedDays(numdays).isSameDay(start)){
                            // First day
                            newentry = new ScheduleEntry(schedulepanel, start, start.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-1),appointment.isJob(), isReleasedAndEditable,null,newColor);
                            newentry.setFuzzyEnd(true);
                            setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                        }else if (displayStart.getFirstSecondOfDay().getDateWithAddedDays(numdays).after(start)){
                            // Full Day
                            ScheduleDate tmpstart = displayStart.getFirstSecondOfDay().getDateWithAddedDays(numdays);
                            ScheduleDate tmpend = tmpstart.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-1);
                            newentry = new ScheduleEntry(schedulepanel, tmpstart,tmpend,  appointment.isJob(), isReleasedAndEditable, null, newColor);
                            // getGUIListener().getLogger().log(Level.INFO, "ScheduleField::createScheduleEntrysByAppointment: Middle Day:"+ newentry.getStartDate().getDateTimeString() + " to " +newentry.getEndDate().getDateTimeString() );
                            newentry.setFuzzyEnd(true);
                            setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);    
                        }
                        numdays++;
                    }
                    // Last day
                    newentry = new ScheduleEntry(schedulepanel, end.getFirstSecondOfDay(), end ,  appointment.isJob(), isReleasedAndEditable, null, newColor);
                    setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                    
                    
                    //-----------------------------------------------------------------------------------                   
                    
                }else if(start.after(displayStart) && end.after(displayEnd)){
                    // Start in DisplayRange, End after displayed range
                    for (int i = 0; i <  schedulepanel.getNumDays() +1; i++){
                        if (displayStart.getFirstSecondOfDay().getDateWithAddedDays(i).isSameDay(start)){
                            // First day
                            newentry = new ScheduleEntry(schedulepanel, start, start.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-1),appointment.isJob(), isReleasedAndEditable,null,newColor);
                            newentry.setFuzzyEnd(true);
                            setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);                        
                        }else if (displayStart.getFirstSecondOfDay().getDateWithAddedDays(i).after(start)){
                            // Full Days
                            ScheduleDate tmpstart = displayStart.getFirstSecondOfDay().getDateWithAddedDays(i);
                            ScheduleDate tmpend = tmpstart.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-1);
                            
                            newentry = new ScheduleEntry(schedulepanel, tmpstart,tmpend,  appointment.isJob(), isReleasedAndEditable, null, newColor);
                            // getGUIListener().getLogger().log(Level.INFO, "ScheduleField::createScheduleEntrysByAppointment: Middle Day:"+ newentry.getStartDate().getDateTimeString() + " to " +newentry.getEndDate().getDateTimeString() );
                            newentry.setFuzzyEnd(true);
                            setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                        }
                    }
                    
                }else{
                    // Overlaps displayed range
                    for (int i = 0; i <  schedulepanel.getNumDays() +1; i++){
                        // Full Days
                        ScheduleDate tmpstart = displayStart.getFirstSecondOfDay().getDateWithAddedDays(i);
                        ScheduleDate tmpend = tmpstart.getFirstSecondOfDay().getDateWithAddedDays(1).getDateWithAddedSeconds(-1);
                        
                        newentry = new ScheduleEntry(schedulepanel, tmpstart,tmpend,  appointment.isJob(), isReleasedAndEditable,null, newColor);
                        // getGUIListener().getLogger().log(Level.INFO, "ScheduleField::createScheduleEntrysByAppointment: Middle Day:"+ newentry.getStartDate().getDateTimeString() + " to " +newentry.getEndDate().getDateTimeString() );
                        newentry.setFuzzyEnd(true);
                        setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                    }
                }
            }else{
                // One Day  
                newentry = new ScheduleEntry(schedulepanel, start, end, appointment.isJob(), isReleasedAndEditable, null, newColor);
                setEntryDefaults(newentry,appointment,schedulepanel,SEPsforAppointment);
                
                
                /*
                newentry.setFonts(m_oFontMenu, m_oFontText, m_oFontDate, m_oFontType, m_oFontDescription);
                getAppointmentDisplayManager().addAppointmentDisplayListener(newentry);
                
                newentry.setAppointment(appointment);
                try {
                    SEPRegistry.getInstance().addSEP(newentry,schedulepanel.getID(),SEPRegistry.SEPPOSITION_LAST);
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                SEPsforAppointment.add(newentry);
                */          
            }
        } 
        catch (ContactDBException e)
        {
            e.printStackTrace();
        }
        
        
        // Now loop when all Entrys are added!
        Iterator myiter = SEPsforAppointment.iterator();
        while (myiter.hasNext()){
            ScheduleEntryPanel sep = (ScheduleEntryPanel) myiter.next();
            sep.revalidateDisplay();
            schedulepanel.addScheduleEntrySilent(sep);
            sep.setDragListenerToSelectOnly(disableDragListener);
        }
        return SEPsforAppointment;
    }
    
    private void setEntryDefaults(ScheduleEntry newentry, Appointment appointment,SchedulePanel schedulepanel, Vector SEPsforAppointment ){
        //Common stuff ...
        getAppointmentDisplayManager().addAppointmentDisplayListener(newentry);
        newentry.setAppointment(appointment);
        
        try {
            SEPRegistry.getInstance().addSEP(newentry,schedulepanel.getID(),SEPRegistry.SEPPOSITION_LAST);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        SEPsforAppointment.add(newentry);
    }
    
    
    public void getData(PluginData data)
    {
    }
    
    public boolean isDirty(PluginData data)
    {
        return false;
    }
    
    public void setEditable(boolean iseditable)
    {
    }
    
    public String getKey()
    {
        return "SCHEDULE";
    }
    
    public void setDoubleCheckSensitive(boolean issensitive)
    {
    }
    
    
    public void setCardVisible(String id)
    {
        CardLayout cl = (CardLayout)(m_oCardPanel.getLayout());
        cl.show(m_oCardPanel, id);    
        m_sActiveCardID = id;
    }
    
    public String getCardVisible()
    {
        return m_sActiveCardID;
    }
    
    
    public void setDateInterval(ScheduleDate date, ScheduleDate enddate)
    {
        //Monitor Date Selection Changes when switching from day/week/month and viceversa
        if (date.getDateWithAddedDays(1).isSameDay(enddate)){
            //Day
            m_oLastDate = date;
        }else if (date.getDateWithAddedDays(7).isSameDay(enddate)){
            //Week 
            if (m_oLastDate.before(date) || m_oLastDate.after(enddate)){
                //Update LastDate to new Selection
                m_oLastDate = date;
            }
        }else if (date.getLastSecondOfMonth().isSameDay(enddate)){
            //Month changed 
            if (m_oLastDate.before(date) || m_oLastDate.after(enddate)){
                //Update LastDate to new Selection
                m_oLastDate = date;
            }
            
        }
        
        m_oCurrentDate = date;
        m_oEndDate = enddate;
        
        m_oSchedulePanelDay.setFirstDay(m_oCurrentDate);
        m_oSchedulePanelWeek.setFirstDay(m_oCurrentDate);
        m_oPanelMonth.setFirstDay(m_oCurrentDate);
        
        if (m_oCalendarNavigationField == null) 
        {
            Object controler = getWidgetPool().getController(CalendarVisibleElement.NAVIGATOR);
            if (controler instanceof ScheduleNavigationField)
            {  
                m_oCalendarNavigationField = (ScheduleNavigationField)controler;
            }
        }
        
        if (m_oCalendarNavigationField != null) 
        {
            m_oCalendarNavigationField.fillDateCombos(m_oCurrentDate);
        }
        
        fillSchedulePanel(false);
    }
    
    public ScheduleDate getFirstDay()
    {
        return m_oCurrentDate;
    }
    
    public ScheduleDate getLastDate()
    {
        return m_oLastDate;
    }
    
    
    private EntryLayoutHelper createPanel(String widgetFlags)
    {
        m_oScheduleDataDay = getControlListener().getScheduleData();
        m_oScheduleDataWeek = new ScheduleData(m_oScheduleDataDay);
        m_oScheduleDataMonth = new ScheduleData(m_oScheduleDataDay);
        
        m_oCurrentDate = (new ScheduleDate()).getMondayOfWeek();
        SchedulePanelItemListener listener = new SchedulePanelItemListener();
        m_oCardPanel = new JPanel(new CardLayout());
        
        // Tagesansicht...
        m_oScheduleDataDay.dayWidth = m_oScheduleDataWeek.dayWidth * 7;
        m_oSchedulePanelDay = new SchedulePanel(1,m_oScheduleDataDay, SchedulePanel.TYPE_DAY, 1);
        m_oSchedulePanelDay.setAppointmentDisplayManager(getAppointmentDisplayManager());      
        m_oSchedulePanelDay.setWidgetPool(getWidgetPool());
        m_oSchedulePanelDay.setGUIListener(getGUIListener());
        m_oSchedulePanelDay.addScheduleEntryListener(listener);    
        m_oCardPanel.add(m_oSchedulePanelDay, CARD_DAY);
        
        // Wochenansicht...
        m_oSchedulePanelWeek = new SchedulePanel(7,m_oScheduleDataWeek, SchedulePanel.TYPE_WEEK, 7);
        m_oSchedulePanelWeek.setAppointmentDisplayManager(getAppointmentDisplayManager());      
        m_oSchedulePanelWeek.setWidgetPool(getWidgetPool());      
        m_oSchedulePanelWeek.setGUIListener(getGUIListener());
        m_oSchedulePanelWeek.addScheduleEntryListener(listener);    
        m_oCardPanel.add(m_oSchedulePanelWeek, CARD_WEEK);
        
        // Monatsansicht...
        m_oPanelMonth = new MonthlyCalendarPanel(getGUIListener(), getWidgetPool(), m_oScheduleDataMonth);
        m_oPanelMonth.setAppointmentDisplayManager(getAppointmentDisplayManager());      
        m_oPanelMonth.setShedulePointColor(new Color(0xff,0x94,0x94));
        m_oCardPanel.add(m_oPanelMonth, CARD_MONTH);
        
        
        m_Panel = new TarentWidgetPanel(new BorderLayout());
        
        m_Panel.add(m_oCardPanel, BorderLayout.CENTER);
        
        getWidgetPool().addController("CONTROLER_SCHEDULEPANEL", this);
        
        setCardVisible(CARD_WEEK);    
        setGridHeight(m_oScheduleDataDay.snapGridHeight);  
        
        fillSchedulePanel(true);
        
        return new EntryLayoutHelper(new TarentWidgetInterface[] { m_Panel }, widgetFlags);
    }
    
    
    
    // wenn der Eintrag verschoben oder in der L�nge ver�ndert
    // wurde soll automatisch abgespeichert werden...
    private void autosaveScheduleEntry(ScheduleEntryPanel entry)
    {

        // Von Sebastian eingefgt,
        // damit niemals versucht wird einen Termin zu speichern, der nicht schreibbar ist.
        // entry.getAppointment().isAppointmentEditable() 
        if (!entry.isAppointmentEditable()) 
            {
                getGUIListener().getLogger().log(Level.INFO, "Cancel autosave, because ist was called on an not editable Appointment!");
                return;
            }

        if (entry.getAppointment() != null)
        {  
                
            try
            {
                if (m_bShowSavingAppointmentOnApplication) getGUIListener().setWaiting(true);
                
                if (SEPRegistry.getInstance().getSEPCount(entry.getAppointment().getId()) == 1){ 
                    entry.getAppointment().setStart(entry.getStartDate().getDate());
                    entry.getAppointment().setEnd(entry.getEndDate().getDate());
                }
                // richtiges datum setzen
                
                //TODO: soll das temp flag hier gel�scht werden?
                //entry.getAppointment().setTemporary(false);   
                
                //Simon: fehlendes Dirty gesetzt
                ((IEntity)entry.getAppointment()).setDirty(true);
                
                PersistenceManager.commit(entry.getAppointment());
            } 
            catch (ContactDBException e) 
            {
                e.printStackTrace();
            }
            finally
            {
                if (m_bShowSavingAppointmentOnApplication) getGUIListener().setWaiting(false);
            }
        }
    }
    
    
    public void changedAppointment(Appointment appointment)
    {    
        if (appointment == null) return;    
        updateAppointment(appointment);
        
        //FIXME: lkajsdlkajsdsajkld
        fillSchedulePanel(true);
    }
    
    public void addedAppointment(Appointment appointment)
    {    
        //setAppointmentActive(appointment.getId(),true);
        fillSchedulePanel(true);
    }
    
    public void removedAppointment(Appointment appointment)
    {    
        SchedulePanel panel = getDisplayedSchedulePanel();
        if (panel != null)
        {  
            List entrys = findEntrysOfAppointment(panel, appointment.getId());
            if ( ! entrys.isEmpty())
            {
                
                fillSchedulePanel(true);
            }
        }
    }
    
    public Appointment getAppointment(int appointmentid)
    {
        SchedulePanel panel = getDisplayedSchedulePanel();
        if (panel != null)
        {  
            List entrys = findEntrysOfAppointment(panel, appointmentid);
            if ( ! entrys.isEmpty())
            {
                return (Appointment) entrys.get(0);      
            }
        }    
        return null;
    }
    
    public void changedCalendarCollection()
    {
        setAppointmentActive(m_ActiveAppointmentID,false);   
        fillSchedulePanel(true);    
    }
    
    
    
    public void setViewType(String viewtype)
    {
        setCardVisible(viewtype);
    }
    
    public void setVisibleInterval(ScheduleDate start, ScheduleDate end)
    {
        setDateInterval(start, end);    
    }
    
    /**
     *  This Method selects an Appointment to be the currently active one.
     *  Also updates the graphic appearance of the corresponding SEPS. 
     */
    public void setAppointmentActive(int appointmentid, boolean active)
    {   
        SchedulePanel panel = getDisplayedSchedulePanel();
        if (panel != null)
        {  
            if (active){
                // Disable last active if present
                if (m_ActiveAppointmentID != -1){
                    List cSEPS = findEntrysOfAppointment(panel, m_ActiveAppointmentID);
                    if (cSEPS != null) {
                        for (int i=0; i < cSEPS.size(); i++){
                            ScheduleEntryPanel aSEP =  (ScheduleEntryPanel)cSEPS.get(i);
                            aSEP.setActive(false);
                            //System.out.println("Schedulefiled::setAppointmentActive [DISABLE LAST Active ONE] ("  + active + ") Number" + i);
                        }
                    }
                }
                m_ActiveAppointmentID = appointmentid;
                List cSEPS = findEntrysOfAppointment(panel, m_ActiveAppointmentID);
                if (cSEPS != null) {
                    for (int i=0; i < cSEPS.size(); i++){
                        ScheduleEntryPanel aSEP =  (ScheduleEntryPanel)cSEPS.get(i);
                        aSEP.setActive(true);
                        //System.out.println("Schedulefiled::setAppointmentActive [ENABLE NEW ONE] ("  + active + ") Number" + i);
                    }
                }
            }else{
                // Just disable, unset m_ActiveAppointmentID
                List cSEPS = findEntrysOfAppointment(panel, m_ActiveAppointmentID);
                if (cSEPS != null) {
                    for (int i=0; i < cSEPS.size(); i++){
                        ScheduleEntryPanel aSEP =  (ScheduleEntryPanel)cSEPS.get(i);
                        aSEP.setActive(false);
                        //  System.out.println("Schedulefiled::setAppointmentActive [DISABLE ONLY] ("  + active + ") Number" + i);
                    }
                }
                m_ActiveAppointmentID = -1;
            }
        }else{
            Logger.getLogger("test").log(Level.WARNING,"SET APPOINTMENT ACTIVE DID NOTHING!");
        }
    }
    
    
    public void updateAppointment(Appointment appointment)
    { 
        
        if (m_oSchedulePanelDay != null)
        {  
            List entries = findEntrysOfAppointment(m_oSchedulePanelDay, appointment.getId());
            if (! entries.isEmpty())
            {
                Iterator iterator = entries.iterator();
                while (iterator.hasNext())
                {  
                    ScheduleEntryPanel sep = (ScheduleEntryPanel) iterator.next();
                    sep.setAppointment(appointment);
                }
            }
        }    
        
        if (m_oSchedulePanelWeek != null)
        {  
            List entries = findEntrysOfAppointment(m_oSchedulePanelWeek, appointment.getId());
            if (! entries.isEmpty())
            {
                Iterator iterator = entries.iterator();
                while (iterator.hasNext())
                {  
                    ScheduleEntryPanel sep = (ScheduleEntryPanel) iterator.next();
                    sep.setAppointment(appointment);
                }
            }
        }  
    }
    
    
    
    public void selectedAppointment(Appointment appointment)
    {    
        setAppointmentActive(appointment.getId(),true);
    }
    
    /**
     * 
     * @param schedulepanel
     * @param appointmentid
     * @return List containing the visible SEPs on the schedulepanel for the given appointment 
     */
    private List findEntrysOfAppointment(SchedulePanel schedulepanel, int appointmentid)
    {
        if (appointmentid == -1) return null;
        
        Vector result = new Vector();
        List entries = SEPRegistry.getInstance().getSEPSforSchedulePaneliD(schedulepanel.getID());
        if (entries != null){
            Iterator iterator = entries.iterator();
            while (iterator.hasNext())
            {  
                ScheduleEntryPanel entry = (ScheduleEntryPanel)(iterator.next());
                if (entry != null) 
                    if (entry.getAppointment() != null) 
                        if (entry.getAppointment().getId() == appointmentid) 
                            result.add(entry);
            }
        }
        // getGUIListener().getLogger().log(Level.INFO, "ScheduleField::findEntrysOfAppointment: found "+result.size()+" SEPS for App with ID " + appointmentid);    
        return result;
    }
    
    /**
     * Erzeugt einen Eintrag im privaten Kalender des aktuell angemeldeten Benutzers
     * 
     * @param isTask gibt an ob es sich um einen normalen Termin handeln soll oder eine Aufgabe (Task/ToDo) 
     * @param dayinpanel der Index des Tags in den eingefgt werden soll (abh�ngig vom gerade angezeigten Ausschnitt)
     * @param secondinpanel die Sekunde an der eingefgt werden soll (abh�ngig vom gerade angezeigten Ausschnitt)
     * 
     */
    public void createAppointmentOrTodo(boolean isTask, int dayinpanel, int secondinpanel)
    {
        if (getDisplayedSchedulePanel() != null)
        {  
            getDisplayedSchedulePanel().createNewAppointmentOrToDo(isTask, dayinpanel, secondinpanel,null,null);
        }
    }
    
    
    /**
     */
    public void snapToGrid(boolean snap)
    {
        if (m_oSchedulePanelDay != null) m_oSchedulePanelDay.setSnapToGrid(snap);
        if (m_oSchedulePanelWeek != null) m_oSchedulePanelWeek.setSnapToGrid(snap);
    }
    
    
    public void setGridHeight(int seconds)
    {
        if (m_oSchedulePanelDay != null) m_oSchedulePanelDay.setGridHeight(seconds);
        if (m_oSchedulePanelWeek != null) m_oSchedulePanelWeek.setGridHeight(seconds);    
    }
    
    
    /**
     * gibt den aktuell angezeigten Date-Range zur�ck
     * @return ScheduleDate-Array der im Index 0 das Startdate enth�lt und in Index 1 das Enddate.
     */
    public ScheduleDate[] getDisplayedDateRange()
    {
        ScheduleDate[] dates = new ScheduleDate[2];
        dates[0] = new ScheduleDate(m_oCurrentDate);
        dates[1] = new ScheduleDate(m_oEndDate);
        return dates;
    }
    
    /**
     * gibt den aktuell angezeigten Date-Range zur�ck
     * @return ScheduleDate-Array der im Index 0 das Startdate enth�lt und in Index 1 das Enddate.
     */
    public int getDisplayedDays()
    {
        if (getDisplayedSchedulePanel() != null)
        {  
            return getDisplayedSchedulePanel().getNumDays();
        }
        return 0;
    }
    
    
    
    /**
     * L�sche einen Eintrag im Kalender
     * @param appointmentid das Appointment das gel�scht werden soll
     */
    public void deleteAppointment(int appointmentid)
    {
        SchedulePanel panel = getDisplayedSchedulePanel();
        if (panel != null)
        {  
            List entries = findEntrysOfAppointment(panel, appointmentid);
            if (! entries.isEmpty())
            {
                panel.deleteScheduleEntryPanel((ScheduleEntryPanel) entries.get(0));
            }else{
                // SEP not in view
                try{
                    
                    Appointment  app = getAppointmentDisplayManager().askForAppointment(new Integer(m_ActiveAppointmentID));
                    PersistenceManager.delete(app);
                    AppointmentImpl.flushCache();
                    getAppointmentDisplayManager().fireRemovedAppointment(app, m_oThisScheduleField);
                }catch (ContactDBException e){
                    e.printStackTrace();
                }
                
            }
            
            
        }
    }
    
    /**
     * Editiere einen Eintrag im Kalender (wird im Detail angezeigt)
     * @param appointmentid das Appointment das editiert werden soll
     */
//  public void editAppointment(int appointmentid)
//  {
//  SchedulePanel panel = getDisplayedSchedulePanel();
//  if (panel != null)
//  {  
//  List entries = findEntrysOfAppointment(panel, appointmentid);
//  if (! entries.isEmpty())
//  {
//  Iterator iterator = entries.iterator();
//  while (iterator.hasNext())
//  {  
//  // FIXME: NUR EINMAL AUFRUFEN??
//  panel.editScheduleEntryPanel((ScheduleEntryPanel) iterator.next());
//  }
//  }
//  }
//  }
    
    // dieser Listener setzt Events eines GUI-Items in die eines Appointments um... 
    private class SchedulePanelItemListener implements ScheduleEntryListener
    {
        // diese Events sind uninteressant da hier erst das Ergebnis z�hlt...
        public void startMoving(ScheduleEntryPanel entry, int x, int y) {}    
        public void startResizing(ScheduleEntryPanel entry, int size) {}    
        public void deleteSelected(ScheduleEntryPanel entry) {} // der Termin wurde ber die GUI aufgefordert sich zu l�schen...        
        
        // der Termin wird gerade ber die GUI verschoben
        public void isMoving(ScheduleEntryPanel entry, boolean ismoving, int x, int y) 
        {
            entry.refreshDisplay();      
        }
        
        // der Termin wird gerade ber die GUI vergr��ert/verkleinert
        public void isResizing(ScheduleEntryPanel entry, boolean isresizing, int size)
        {
            entry.refreshDisplay();      
        }
        
        // der Termin wurde ber die GUI verschoben
        public void stopMoving(ScheduleEntryPanel entry, int x, int y) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_SCHEDULEDETAIL");
            if (controler instanceof ScheduleDetailField)
            {  
                m_oCalendarDetailField = (ScheduleDetailField)controler;
                m_oCalendarDetailField.saveAppointment(true);
            }
            autosaveScheduleEntry(entry);
            
            getAppointmentDisplayManager().fireChangedAppointment(entry.getAppointment(), m_oThisScheduleField);      
        }    
        
        // der Termin wurde ber die GUI in der L�nge ver�ndert
        public void stopResizing(ScheduleEntryPanel entry, int size) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_SCHEDULEDETAIL");
            if (controler instanceof ScheduleDetailField)
            {  
                m_oCalendarDetailField = (ScheduleDetailField)controler;
                m_oCalendarDetailField.saveAppointment(true);
            }
            autosaveScheduleEntry(entry);
            getAppointmentDisplayManager().fireChangedAppointment(entry.getAppointment(), m_oThisScheduleField);
        }
        
        
        // der Termin wurde gel�scht
        public void removedEntry(ScheduleEntryPanel entry)
        {
            getAppointmentDisplayManager().fireRemovedAppointment(entry.getAppointment(), m_oThisScheduleField);
        }          
        
        
        // der Termin wurde doppelt angeklickt (inzwischen nur noch ein Klick)
        public void doubleClicked(ScheduleEntryPanel entry) 
        {      
            setAppointmentActive(entry.getAppointment().getId(),true);      
            getAppointmentDisplayManager().fireSelectedAppointment(entry.getAppointment(), m_oThisScheduleField);                  
        }
        
        
        // der Termin wurde ber die GUI aufgefordert "sich zu editieren"...
        public void editSelected(ScheduleEntryPanel entry) 
        {      
            setAppointmentActive(entry.getAppointment().getId(),true);      
            getAppointmentDisplayManager().fireSelectedAppointment(entry.getAppointment(), m_oThisScheduleField);                  
        }
        
        /** 
         * 
         * Ein neues ScheduleEntryPanel wurde in der GUI hinzugefgt, ausgel�st entweder
         * durch das anlegen eines neuen Appointments, oder aber ein mehrt�giger Termin
         * wurde ver�ndert.
         *  
         **/       
        public void addedEntry(ScheduleEntryPanel entry,ScheduleDate startDate, ScheduleDate endDate, Integer calendarID)
        { 
            Schedule schedule = getGUIListener().getSchedule(null);
            if (schedule != null)
            {  
                getGUIListener().setWaiting(true);
                try
                {        
                    // Hier wird ein neues Appointment erzeugt!!!
                    Calendar calendar = getGUIListener().getCalendarCollection().getCalendar(calendarID.intValue());
                    Collection calendarscollection = null;
                    Appointment appointment = null;
                    
                    if (calendar.getType() != Calendar.TYPE_USER){
                        try {
                            throw new Exception("GruppenKalender wurden f�r das LTB Release deaktiviert!!! FIXME!");
                            /*  
                             // Gruppen- oder ResourcenKalender
                              // Termin in Gruppen- und Benutzer Kalender eintragen
                               calendarscollection = new ArrayList();
                               calendarscollection.add(calendar);
                               
                               if (entry.getUserId() != null){
                               // Create as Proxy
                                calendarscollection.add(UserImpl.getUserFromID(entry.getUserId()).getCalendar(true));
                                appointment = schedule.createAppointment(calendarscollection, entry.isJob(), entry.getStartDate().getDate(), entry.getEndDate().getDate(), false,UserImpl.getUserFromID(entry.getUserId()));
                                }else{
                                // Create as Original User
                                 calendarscollection.add(getGUIListener().getUser(null).getCalendar(true));
                                 appointment = schedule.createAppointment(calendarscollection, entry.isJob(), entry.getStartDate().getDate(), entry.getEndDate().getDate(), false,null);
                                 }*/
                        } catch (Exception e1) {
                            // Auto-generated catch block
                            e1.printStackTrace();
                        }
                        
                    }else{ 
                        // Privater Kalender
                        calendarscollection = Collections.singleton(calendar);
                        appointment = schedule.createAppointment(calendarscollection, entry.isJob(), startDate.getDate(), endDate.getDate(), false,UserImpl.getUserFromID(entry.getUserId()));
                    }
                    
                    entry.setAppointment(appointment);
                    AppointmentDefault.setToDefaults(appointment, true);            
                    AppointmentDefault.initialize(appointment, calendar);    
                    
                    User user = UserImpl.getUserFromID(entry.getUserId());
                    
                    if (user != null ) 
                    {           
                        Collection calendars = appointment.getCalendars();
                        if (calendars != null)
                        {  
                            Iterator it = calendars.iterator();
                            while(it.hasNext())
                            {
                                Calendar itcalendar = (Calendar)it.next();
                                User calendaruser = itcalendar.getUser();
                                AppointmentRequest request = appointment.getRelation(itcalendar);
                                if (request != null)
                                {
                                    if ((calendaruser != null) &&  (calendaruser.getId() == user.getId()))                    
                                    {
                                        request.setRequestLevel(AppointmentRequest.LEVEL_REQUIRED);
                                        request.setRequestStatus(AppointmentRequest.STATUS_CONFIRMED);
                                        request.setRequestType(AppointmentRequest.TYPE_INVITATION);
                                    }
                                    else
                                    {
                                        request.setRequestLevel(AppointmentRequest.LEVEL_REQUIRED);
                                        request.setRequestStatus(AppointmentRequest.STATUS_NEEDS_ACTION);
                                        request.setRequestType(AppointmentRequest.TYPE_REQUEST);
                                    }
                                    PersistenceManager.commit(request);
                                }
                            }                 
                        }
                    }   
                    PersistenceManager.commit(appointment);
                } 
                catch (ContactDBException e)
                {
                    if (e.getExceptionId() == ContactDBException.EX_APPOINTMENT_CONFLICTS)
                    {
                        ApplicationServices.getInstance().getCommonDialogServices().showInfo("Termin fhrt zu einem Konflikt");
                        // Achtung: dieser Termin fhrt zu einem Konflikt...
                    }
                    else
                    {  
                        getGUIListener().getLogger().log(Level.SEVERE, "ScheduleField::addedEntry(): Fehler beim Anlegen eines Termins.", e);
                    }
                }
                finally
                {
                    getGUIListener().setWaiting(false);
                }
            }
            else
            {
                getGUIListener().getLogger().severe("ScheduleField::addedEntry(): Das Schedule-Objekt konnte nicht geladen werden!");        
            }
            getAppointmentDisplayManager().fireAddedAppointment(entry.getAppointment(), null);
            setAppointmentActive(entry.getAppointment().getId(),true);
            getAppointmentDisplayManager().fireSelectedAppointment(entry.getAppointment(), m_oThisScheduleField);
        }
        
        // ein Termin hat ber die GUI seine "inneren Werte" ver�ndert...
        public void changedValues(ScheduleEntryPanel entry)
        {
            entry.revalidateDisplay();
        }
        
    } // end listener
    
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridActive(boolean)
     */
    public void setGridActive(boolean usegrid)
    {
        snapToGrid(usegrid);    
    }

    /** Handles export event.*/
    public void clickedExportButton() {
    }
    
    /** Handles new appointment event.*/
    public void clickedNewAppointmentButton() {
    }

    /** Handles new task event.*/
    public void clickedNewTaskButton() {
    }
    
} // end class
