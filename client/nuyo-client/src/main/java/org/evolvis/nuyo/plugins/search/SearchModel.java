package org.evolvis.nuyo.plugins.search;

import java.util.prefs.Preferences;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.gui.categorytree.SelectionState;
import org.evolvis.nuyo.gui.search.SearchFilterState;


/**
 * TODO: Document me plenty!
 * 
 * @author rschus
 *
 */
class SearchModel
{
  private AddressListParameter addressListParameters;
  
  private SearchFilterState searchFilterState;
  private Preferences searchFilterPanelPrefs;
  
  private SelectionState selectionState;
  private Preferences categoryTreePrefs;
  
  SearchModel(AddressListParameter addressListParameters)
  {
	 this(addressListParameters, ApplicationServices.getInstance().getActionManager().getPreferences("searchDialog"));
  }
  
  SearchModel(AddressListParameter addressListParameters, Preferences prefs)
  {
    this.addressListParameters = addressListParameters;
    
    categoryTreePrefs = prefs.node("categoryTree");
    
    selectionState = SelectionState.restoreFromPreferences(categoryTreePrefs);

    searchFilterPanelPrefs = prefs.node("searchFilterPanel");
    searchFilterState = SearchFilterState.restoreFromPreferences(searchFilterPanelPrefs);
  }
  
  void setFilterState(SearchFilterState sfState)
  {
    searchFilterState = sfState;
  }
  
  SearchFilterState getFilterState()
  {
    return searchFilterState;
  }
  
  void setSelectionState(SelectionState selState)
  {
    selectionState = selState; 
  }
  
  SelectionState getSelectionState()
  {
    return selectionState;
  }
  
  void updateAddressListParameters()
  {
    selectionState.storeInPreferences(categoryTreePrefs);
    searchFilterState.storeInPreferences(searchFilterPanelPrefs);
    
    selectionState.updateAdressListParameters(addressListParameters);
    searchFilterState.updateAdressListParameters(addressListParameters);
  }

  public String getAddressSourceLabel() {
	  if(addressListParameters != null)
		  return addressListParameters.getAddressSourceLabel();
	  else
		  return "";
  }  
}
