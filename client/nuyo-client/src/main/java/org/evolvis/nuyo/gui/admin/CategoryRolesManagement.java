package org.evolvis.nuyo.gui.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controls.ExtendedDataItemModel;
import org.evolvis.nuyo.controls.ExtendedDataLabeledComponent;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.Role;
import org.evolvis.nuyo.db.persistence.PersistenceManager;
import org.evolvis.nuyo.gui.GUIHelper;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;


/**
 * A class to manage category roles.
 * It provides a panel with a table and create/delete/commit button.
 * <p>
 * A registered <code>CategoryRolesListener</code> will be notified about the changes within a table
 * if and only if any <i>commit-action</i> was performed successfully. 
 * Invalid changes won't be commited. In this case the affected roles should be corrected first.  
 * <p>
 * 
 * @see org.evolvis.nuyo.gui.admin.CategoryRolesListener
 * @see org.evolvis.nuyo.gui.AdminPanel
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class CategoryRolesManagement {
    //###### 
    // TODO: integrate also global roles here and rename this class to RolesManagement 
    //######
    
    private static final TarentLogger logger = new TarentLogger(CategoryRolesManagement.class);
    private GUIListener m_oGUIListener;
    
    private JTable categoryRolesTable;
    private CategoryRolesTableModel categoryRolesTableModel;
    private ExtendedDataItemModel panelWrapper;
    
    private static CategoryRolesManagement instance;
    private static final Object singletonMonitor = new Object();
    private final static String[] categoryRoleRightsTableColumnNames;
    
    private CategoryRolesListener listener;
    
    static{
        categoryRoleRightsTableColumnNames = new String[]{
                   Messages.getString("GUI_Admin_Folder_Rolle_Name"),    
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht1"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht2"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht3"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht4"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht5"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht6"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht7"),
                   Messages.getString("GUI_Admin_Folder_Rolle_Recht8")
        };
    }
    
    private CategoryRolesManagement(){
    }
    
    public static CategoryRolesManagement getInstance() {
        if(instance != null) return instance;
        synchronized (singletonMonitor) {
            if (instance == null){
                instance = new CategoryRolesManagement();
            }
        }
        return instance;
    }

    public void setListener(CategoryRolesListener aListener){
        listener = aListener;
    }
    
    public ExtendedDataItemModel getPanel(CategoryRolesListener listener){
        setListener(listener);
        if(panelWrapper == null){
            panelWrapper = createCategoryRolePanel();
        }
        return panelWrapper;
    }
    
    public ExtendedDataItemModel createCategoryRolePanel() 
    {
        m_oGUIListener = ApplicationServices.getInstance().getActionManager();
        categoryRolesTableModel = new CategoryRolesTableModel(getCategoryRoles(),categoryRoleRightsTableColumnNames);
        categoryRolesTable = new CategoryRolesTable(categoryRolesTableModel);
        categoryRolesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrollPane = new JScrollPane(categoryRolesTable);

        FormLayout layout = new FormLayout("pref,5dlu,pref,fill:pref:grow,5dlu,pref",//columns
                                           "max(90dlu;min):grow,5dlu,pref");//rows
        layout.setColumnGroups(new int[][]{ {1,3,6} });
        CellConstraints cc = new CellConstraints();
        PanelBuilder builder = new PanelBuilder(layout);
        builder.setDefaultDialogBorder();
        builder.add(scrollPane, cc.xyw(1,1,4,CellConstraints.DEFAULT,CellConstraints.FILL));

        
        JButton commitCategoryRoleButton = new JButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"));
        commitCategoryRoleButton.addActionListener(new CommitButtonActionListener());
        builder.add(commitCategoryRoleButton, cc.xy(6,1,CellConstraints.DEFAULT, CellConstraints.TOP));

        JButton createCategoryRoleButton = new JButton(Messages.getString("GUI_Admin_Folder_Rolle_neu"));
        createCategoryRoleButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                categoryRolesTableModel.addRow(new CategoryRoleLineItem());
                int rowIndex = categoryRolesTableModel.getRowCount() - 1;
                logger.finest("created role index: " + rowIndex);
                //FIXME: selection not really achieved - Swing BUG?
                categoryRolesTable.setRowSelectionInterval(rowIndex,rowIndex);
                categoryRolesTable.setColumnSelectionInterval(0,0);
            }
        });
        builder.add(createCategoryRoleButton, cc.xy(1,3));
        
        JButton button_delNewFolderRole = new JButton(Messages.getString("GUI_Admin_Folder_Rolle_del"));
        button_delNewFolderRole.addActionListener(new DeleteButtonActionListener());
        builder.add(button_delNewFolderRole, cc.xy(3,3));
        
        panelWrapper = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Folder_Rollen"));
        panelWrapper.addLabeledComponent(new ExtendedDataLabeledComponent("", "", builder.getPanel()));
        panelWrapper.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_ROLE_FR));
        return panelWrapper;    
    }
    
    private void fireUpdateEvent() {
        if(listener != null) listener.categoryRolesUpdated();
        else logger.info("[!] no listener for category update");
    }
    
    private List getCategoryRoles(){
        if(m_oGUIListener == null) return Collections.EMPTY_LIST;
        List roles = m_oGUIListener.getFolderRoles();
        Collections.sort(roles, new RoleComparator());
        return roles;
    }

    /** Deletes category role permanently.*/
    public class DeleteButtonActionListener implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            	SwingUtilities.invokeLater( new Runnable() {
                public void run() {
                    int selectedRow = categoryRolesTable.getSelectedRow();
                    if (categoryRolesTableModel.getRowCount() > 0 && selectedRow !=-1){
                            Role roleToDelete = categoryRolesTableModel.getRoleAt( selectedRow );
                            if ( roleToDelete.isDeletable() ) {
                                if(!roleToDelete.isTransient())
									try {
										PersistenceManager.delete( roleToDelete );
									} catch (ContactDBException e) {
										ApplicationServices.getInstance().getCommonDialogServices()
							            .publishError(Messages.getString("GUI_Admin_Role_Delete_Refused_1"));
							        	return;
									}
								categoryRolesTableModel.removeRow( selectedRow );
	                                
                                if(categoryRolesTableModel.getRowCount() > 0) {
                                    if (selectedRow == categoryRolesTableModel.getRowCount()) --selectedRow;
                                    	categoryRolesTable.setRowSelectionInterval(selectedRow,selectedRow);
                                }
                                fireUpdateEvent();
                            }
                            else {
                                ApplicationServices.getInstance().getCommonDialogServices()
                                    .showInfo( Messages.getString( "GUI_Admin_Role_Undeletable" ) );
                                if(categoryRolesTableModel.getRowCount() > 0) categoryRolesTable.setRowSelectionInterval(selectedRow,selectedRow);
                            }
                    }
                }
            });
        }
    }

    /** Commits all changes within category roles table to a database.*/
    public class CommitButtonActionListener implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            SwingUtilities.invokeLater( new Runnable() {
                public void run() {
                	
                	// we have to stop editing before in order to apply changes to the table-model
                	
                	// for some reasen we have to clear selection to make the following work..
                	categoryRolesTable.getSelectionModel().clearSelection();
                	
                	if(categoryRolesTable.getCellEditor() != null)
                		categoryRolesTable.getCellEditor().stopCellEditing();
                	
                    int rolesCount = categoryRolesTableModel.getRowCount();
                    if ( rolesCount > 0 ) {
                        boolean atLeastOneRoleCommited = false;
                        boolean exceptionOccured = false;
                        int lastSavedRoleIndex = 0;
                        logger.info( "saving contact category roles..." );
                        for ( int i = 0; i < rolesCount; i++ ) {
                            Role role = categoryRolesTableModel.getRoleAt( i );
                            //(1) VALIDATE changes
                            if ( role.isDirty() ) {
                                try {
                                    if(role.isTransient() && (null == role.getRoleName())) throw new RolesManagementException(Messages.getFormattedString("GUI_Admin_Save_Role_ERROR_EmptyName", i+1));
                                    logger.fine(role.getRoleName());
                                    //(2) COMMIT changes
                                    PersistenceManager.commit( role );
                                    atLeastOneRoleCommited = true;
                                    lastSavedRoleIndex = i;
                                }
                                catch ( ContactDBException e1 ) {
                                    logger.info("[!] commit failed");
                                    categoryRolesTable.setRowSelectionInterval(i,i);
                                    ApplicationServices.getInstance().getCommonDialogServices().publishError( Messages.getFormattedString( "GUI_Admin_Save_Role_ERROR_DB", role.getRoleName()), e1 );
                                    exceptionOccured = true;
                                    continue;
                                }
                                catch ( RolesManagementException e1 ) {
                                    logger.info("[!] " + e1.getMessage());
                                    categoryRolesTable.setRowSelectionInterval(i,i);
                                    ApplicationServices.getInstance().getCommonDialogServices().publishError( e1.getMessage());
                                    exceptionOccured = true;
                                    continue;
                                }
                            } else if ( role.isTransient() ) {
                                //empty role object (just created)
                                categoryRolesTable.setRowSelectionInterval(i,i);
                                ApplicationServices.getInstance().getCommonDialogServices().showInfo( Messages.getFormattedString("GUI_Admin_Save_Role_ERROR_EmptyName", i+1));
                                exceptionOccured = true;
                                continue;
                            }//current role hasn't changed
                        }
                        if(atLeastOneRoleCommited){
                            //(3) UPDATE gui
                            if(categoryRolesTableModel.getRowCount() > 0) categoryRolesTable.setRowSelectionInterval(lastSavedRoleIndex,lastSavedRoleIndex);
                            if(!exceptionOccured) {
                                logger.info("updating & sorting category roles table...");
                                categoryRolesTableModel.setData(getCategoryRoles());
                            }
                            fireUpdateEvent();
                        } else if(!exceptionOccured) {
                            logger.info("[!] no changes to commit");
                            ApplicationServices.getInstance().getCommonDialogServices().showInfo( Messages.getString( "GUI_Admin_Save_Role_No_Changes" ) );
                        }
                    } 
                }
            } );
        }
    }
    
    /**
     * A table model to handle each <code>Role</code> instance as a row (line item) value object.<br>
     * All changes from GUI will be first validated and then accepted if only all integrity condition are fulfilled.
     * Otherwise the changes will be refused.
     *  
     * @see RolesTableLineItemVO
     */
    private class CategoryRolesTableModel extends AbstractTableModel 
    {
        private List<RolesTableLineItemVO> lineItemVOs;
        private List<String> columnNames;
        private Set<String> uniqueRoleNames;
        
        public CategoryRolesTableModel( List<ObjectRole> categoryRoles, String[] newColumnNames ) {            
            columnNames = Arrays.asList(newColumnNames);
            setData(categoryRoles);
        }

        private void setData( List<ObjectRole> categoryRoles ) {
            int count = categoryRoles != null ? categoryRoles.size() : 0;
            lineItemVOs = new ArrayList<RolesTableLineItemVO>(count);
            uniqueRoleNames = new HashSet<String>(count + 5);
            for(ObjectRole nextRole: categoryRoles){
                lineItemVOs.add(new CategoryRoleLineItem(nextRole));
                uniqueRoleNames.add(nextRole.getRoleName());
            }
            SwingUtilities.invokeLater(new Runnable(){
                public void run() {
                    fireTableDataChanged();
                }
            });
        }
        
        public String getColumnName(int column){
            return columnNames.get(column);
        }
        
        public void removeRow( int index ) {
            checkRowIndex(index);
            uniqueRoleNames.remove(getRoleAt(index).getRoleName());
            lineItemVOs.remove(index);
            fireTableRowsDeleted(index, index);
        }

        public void addRow( RolesTableLineItemVO item ) {
            assert item != null : "item != null" + item;
            lineItemVOs.add(item);
            final int updateIndex = getRowCount() - 1;
            SwingUtilities.invokeLater(new Runnable(){
                public void run() {
                    fireTableRowsInserted(updateIndex,updateIndex);
                }
            });
        }

        public int getRowCount(){
            return lineItemVOs.size();
        }
        
        public int getColumnCount(){
            return columnNames.size();
        }
        
        public Object getValueAt(int row, int column){
            return lineItemVOs.get(row).getValueAtColumn(column);
        }

        public void setValueAt(Object newValue, int rowIndex, int columnIndex){
            logger.finest("set value at ("+rowIndex+","+columnIndex+"): " + newValue);
            Object oldValue = getValueAt(rowIndex, columnIndex); 
            if(oldValue != null && oldValue.equals(newValue)) return;
            try{
                if(columnIndex == 0) checkNameValue((String) newValue);
                lineItemVOs.get(rowIndex).setValueAtColumn(newValue, columnIndex);
            } catch (RolesManagementException e) {
                logger.warning(e.getMessage());
                categoryRolesTable.setRowSelectionInterval(rowIndex, rowIndex);
            } finally{
                // update/reset table values 
                fireTableCellUpdated(rowIndex, columnIndex);
            }
        }

        private void checkNameValue( String name ) throws RolesManagementException {
            assert name!= null : "null as name";
            if("".equals(name)) throw new RolesManagementException(Messages.getString("GUI_Admin_Edit_Rolle_ERROR_EmptyName"));
            if(uniqueRoleNames.contains(name)) throw new RolesManagementException(Messages.getFormattedString("GUI_Admin_Edit_Role_ERROR_NameAlreadyExist", name));
            else uniqueRoleNames.add(name);
        }

        public Role getRoleAt( int row ) {
            checkRowIndex( row );
            return lineItemVOs.get(row).getRole();
        }

        public boolean isCellEditable( int row, int column ) {
            return true;
        }
        
        public Class getColumnClass( int columnIndex ) {
            return columnIndex == 0 ? String.class : Boolean.class;   
        }
        
        private void checkRowIndex( int index ) throws IllegalArgumentException {
            if(index >= getRowCount()) throw new IllegalArgumentException("row index should be less then " + getRowCount() + ": " + index);
        }
    }
}
