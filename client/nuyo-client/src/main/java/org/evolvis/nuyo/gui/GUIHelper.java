/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
/*
 * Created on 23.05.2003
 *
 */
package org.evolvis.nuyo.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.util.HashMap;

import javax.swing.ImageIcon;

import org.evolvis.nuyo.controller.ApplicationServices;


/**
 * @author niko
 *
 */
public class GUIHelper
{
  public final static String ICON_PERSON = "Person";
  public final static String ICON_RESSOURCE = "Ressource";
  public final static String ICON_SYSUSER = "SysUser";
  public final static String ICON_EXTUSER = "ExtUser";
  
  public final static String ICON_SEX_MALE = "sex_male";
  public final static String ICON_SEX_FEMALE = "sex_female";
  public final static String ICON_SEX_UNKNOWN = "sex_unknown";
  public final static String ICON_SEX_FIRMA = "sex_firma";

  public final static String ICON_ADMIN_TAB_CATEGORY ="ICON_ADMIN_TAB_CATEGORY";
  public final static String ICON_ADMIN_TAB_USER ="ICON_ADMIN_TAB_USER";
  public final static String ICON_ADMIN_TAB_EXTRAS ="ICON_ADMIN_TAB_EXTRAS";

  public final static String  ICON_ADMIN_TAB_CATEGORY_CREATE = "ICON_ADMIN_TAB_CATEGORY_CREATE";
  public final static String  ICON_ADMIN_TAB_SUBCATEGORY_CREATE = "ICON_ADMIN_TAB_SUBCATEGORY_CREATE";
  public final static String  ICON_ADMIN_TAB_SUBCATEGORY_SHOW = "ICON_ADMIN_TAB_SUBCATEGORY_SHOW";
  public final static String  ICON_ADMIN_TAB_SUBCATEGORY_DELETE = "ICON_ADMIN_TAB_SUBCATEGORY_DELETE";
  public final static String  ICON_ADMIN_TAB_SUBCATEGORY_EDIT = "ICON_ADMIN_TAB_SUBCATEGORY_EDIT";
  public final static String  ICON_ADMIN_TAB_CATEGORY_SHOW = "ICON_ADMIN_TAB_CATEGORY_SHOW";
  public final static String  ICON_ADMIN_TAB_CATEGORY_HIDE = "ICON_ADMIN_TAB_CATEGORY_HIDE";
  public final static String  ICON_ADMIN_TAB_STDCATEGORY_SET = "ICON_ADMIN_TAB_STDCATEGORY_SET";
  
  public final static String  ICON_ADMIN_TAB_USER_CREATE = "ICON_ADMIN_TAB_USER_CREATE";
  public final static String  ICON_ADMIN_TAB_USER_DELETE = "ICON_ADMIN_TAB_USER_DELETE";
  public final static String  ICON_ADMIN_TAB_USER_ASSOCIATE = "ICON_ADMIN_TAB_USER_ASSOCIATE";
  public final static String  ICON_ADMIN_TAB_USER_CHANGE = "ICON_ADMIN_TAB_USER_CHANGE";
  
  public static final String  ICON_ADMIN_TAB_GROUP = "ICON_ADMIN_TAB_GROUP";
  public final static String  ICON_ADMIN_TAB_GROUP_CREATE = "ICON_ADMIN_TAB_GROUP_CREATE";
  public final static String  ICON_ADMIN_TAB_GROUP_EDIT = "ICON_ADMIN_TAB_GROUP_EDIT";
  public final static String  ICON_ADMIN_TAB_GROUP_DELETE = "ICON_ADMIN_TAB_GROUP_DELETE";
  
  public static final String  ICON_ADMIN_TAB_ROLE= "ICON_ADMIN_TAB_ROLE";
  public final static String  ICON_ADMIN_TAB_ROLE_GR = "ICON_ADMIN_TAB_ROLE_GR";
  public final static String  ICON_ADMIN_TAB_ROLE_FR = "ICON_ADMIN_TAB_ROLE_FR";
  
  public final static String  ICON_ADMIN_TAB_CALENDAR_SHARE = "ICON_ADMIN_TAB_CALENDAR_SHARE";
  
  public final static String  ICON_ADMIN_TAB_FINDER_URL = "ICON_ADMIN_TAB_FINDER_URL";
  public final static String  ICON_ADMIN_TAB_KONFIGURATOR = "ICON_ADMIN_TAB_KONFIGURATOR";
  public final static String  ICON_ADMIN_TAB_CATEGORY_DELETE = "ICON_ADMIN_TAB_CATEGORY_DELETE";
  public final static String  ICON_ADMIN_TAB_CATEGORY_EDIT = "ICON_ADMIN_TAB_CATEGORY_EDIT";
  
  public final static String FONT_MENUTITLE = "MenuTitleFont";
  public final static String FONT_MENUITEM = "MenuItemFont";
  public final static String FONT_FORMULAR = "FormularFont";
  public final static String FONT_TABULATOR = "TabulatorFont";
  public final static String FONT_INFO = "InfoFont";
  public final static String FONT_TABLE = "TableFont";
  public final static String FONT_TABLEHEADER = "TableHeaderFont";
  public final static String FONT_TABLEDRAGBAR = "TableDragBarFont";
  public final static String FONT_LOGTEXT = "LogTextFont";
  
  public final static String CURSOR_NORMAL = "NormalCursor";
  public final static String CURSOR_HAND = "HandCursor";


  // ---------------------------------------------------------------------------------------------------------------------  
  
  private static HashMap iconsMap = new HashMap();
  private static HashMap colorsMap = new HashMap();
  private static HashMap fontsMap = new HashMap();
  private static HashMap cursorsMap = new HashMap();

  
  public static ImageIcon getIcon(String name)
  {
    return((ImageIcon)(iconsMap.get(name)));
  }

  public static Font getFont(String name)
  {
    return((Font)(fontsMap.get(name)));
  }

  public static Cursor getCursor(String name)
  {
    return((Cursor)(cursorsMap.get(name)));
  }

  public static void registerIcon(String name, String filename)
  {
    iconsMap.put(name, ApplicationServices.getInstance().getIconFactory().getIcon(filename));
  }

  public static void registerIcon(String name, ImageIcon icon)
  {
    iconsMap.put(name, icon);
  }

  public static void registerColor(String name, Color color)
  {
    colorsMap.put(name, color);
  }

  public static void registerFont(String name, Font font)
  {
    fontsMap.put(name, font);
  }

  public static void registerCursor(String name, Cursor cursor)
  {
    cursorsMap.put(name, cursor);
  }
}
