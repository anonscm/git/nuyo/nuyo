package org.evolvis.nuyo.plugins.assigner;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;



//import de.tarent.groupware.security.AddressSecurityException;
/**
 * This class contains the functionality for the category assignment plugin.
 * It provides a JPanel with most UI Elements. Dialog or wizard lements can be added in a wrapping class.
 * @author Steffi Tinder, tarent GmbH
 *
 */
class AssignerComponent {

	private static Logger logger = Logger.getLogger(AssignerComponent.class.getName());

	private Addresses addresses;
	private SimpleCategorySelector categoryComponent;
	private JPanel mainPanel;
	private Database database;

	public AssignerComponent() {

		database=ApplicationServices.getInstance().getCurrentDatabase();
		categoryComponent = new SimpleCategorySelector();
		categoryComponent.showAllSubcategoriesEntry(true);
		categoryComponent.loadCategoryLists();
		mainPanel = buildPanel();
	}

	public void assign(){
		assignToSubcategories(categoryComponent.getSelectedSubcategoryIds());
	}

	public void removeAssignments(){
		removeAssignmentsToSubcategories(categoryComponent.getSelectedSubcategoryIds());
	}

	/**
	 * this method assigns addresses to the given list of subcategories.
	 * @param subcategories list of subcategory-ids to which the addresses shall be assigned
	 */
	public void assignToSubcategories(List subcategoryIDs){

		List addressPkList = addresses.getPkList();
		List dummy = new ArrayList();

		try {
			database.changeAddressAssignments(addressPkList,subcategoryIDs,dummy);
		} catch (ContactDBException e) {
			if (e.getOctopusException()!=null){
				//  119 is value of de.tarent.groupware.security.AddressSecurityException.ERROR_NO_AUTHORISATION_TO_CHANGE_ASSIGNMENT
				if (e.getOctopusException().getErrorCode().endsWith(String.valueOf(119))){
					ApplicationServices.getInstance().getCommonDialogServices().publishError("Die Operation wurde nicht ausgef�hrt, da sie f�r mindestens eine der Adressen nicht gen�gend Berechtigungen haben",e);
					logger.info("Die Operation wurde nicht ausgef�hrt, da sie f�r mindestens eine der Adressen nicht gen�gend Berechtigungen haben."); //$NON-NLS-1$
				} else {
					logger.info("Bei der Zuweisung zu einer Unterkategorie ist ein Fehler aufgetreten"); //$NON-NLS-1$
					ApplicationServices.getInstance().getCommonDialogServices().publishError("Bei der Zuweisung zu einer Unterkategorie ist ein Fehler aufgetreten",e);
				}
			} 
		} 
	}

	/**
	 * removes the assignment to subcategories  
	 * @param subcategoryIDs a List of IDs of subcategories from which the addresses shall be removed
	 */
	private void removeAssignmentsToSubcategories(List subcategoryIDs){
		List addressPkList = addresses.getPkList();
		List dummy = new ArrayList();

		try {
			database.changeAddressAssignments(addressPkList,dummy,subcategoryIDs);
		} catch (ContactDBException e) {
			if (e.getOctopusException()!=null){
				//  119 is value of de.tarent.groupware.security.AddressSecurityException.ERROR_NO_AUTHORISATION_TO_CHANGE_ASSIGNMENT
				if (e.getOctopusException().getErrorCode().endsWith(String.valueOf(119))){
					ApplicationServices.getInstance().getCommonDialogServices().publishError("Die Operation wurde nicht ausgef�hrt, da sie f�r mindestens eine der Adressen nicht gen�gend Berechtigungen haben",e);
					logger.info("Die Operation wurde nicht ausgef�hrt, da sie f�r mindestens eine der Adressen nicht gen�gend Berechtigungen haben."); //$NON-NLS-1$
				} else {
					logger.info("Bei der Entfernung einer Zuweisung zu einer Unterkategorie ist ein Fehler aufgetreten"); //$NON-NLS-1$
					ApplicationServices.getInstance().getCommonDialogServices().publishError("Bei der Entfernung der Zuweisung zu einer Unterkategorie ist ein Fehler aufgetreten",e);
				}
			} 
		}
	}

	private JPanel buildPanel(){

		FormLayout layout = new FormLayout(
				"pref:GROW, 3dlu, pref", //columns  
		"pref");      // rows 
		//create and configure a builder
		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		builder.add(categoryComponent.getComponent(), cc.xy(1,1));
		return builder.getPanel();
	}

	public JPanel getComponent(){
		return mainPanel;
	}

	public void setAddresses(Addresses addr){
		addresses=addr;
	}
	public Addresses getAddresses(){
		return addresses;
	}
}
