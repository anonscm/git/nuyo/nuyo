/*
 * $Id: Contact.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 14.05.2004
 */
package org.evolvis.nuyo.db;

import java.util.Date;

/**
 * Diese Schnittstelle beschriebt ein historisiertes Kontakt-Ereignis zwischen internen
 * und externen Personen. Diese Ereignisse sind read-only-Objekte. 
 * 
 * @author mikel
 */
public interface Contact extends HistoryElement {
    /** Adresse der externen Kontaktperson */
    Address getAddress() throws ContactDBException;
    /** Intern beteiligte Kontaktperson */
    User getUser() throws ContactDBException;
    /** Kontaktkategorie, vgl. Konstanten <code>CATEGORY_*</code> */
    int getCategory() throws ContactDBException;
    /** Kontaktkanal, vgl. Konstanten <code>CHANNEL_*</code> */
    int getChannel() throws ContactDBException;
    /** Linktyp, vgl. Konstanten <code>LINKTYPE_*</code> */
    int getLinkType() throws ContactDBException;
    /** dem Kontaktereignis zugeordnetes Objekt */
    int getLink() throws ContactDBException;
    /** Flag: Kontaktereignis war hereinkommend (von au�en initiiert) */
    boolean isInbound() throws ContactDBException;
    /** Start des Kontaktereignisses (UTC) */
    Date getStart() throws ContactDBException;
    /** Dauer des Kontaktereignisses in Sekunden */
    int getDuration() throws ContactDBException;
    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    
    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Betreff, Notiz, strukturierte Infos (E-Mail...) */
    public final static String KEY_SUBJECT = "subject";
    public final static String KEY_NOTE = "note";
    public final static String KEY_STRUCT_INFO = "structInfo";
    
    /** Kontaktkategorien: Gespr�chsnotiz, Terminanfrage, Postversand */
    public final static int CATEGORY_MEMO = 0;
    public final static int CATEGORY_REQUEST = 1;
    public final static int CATEGORY_MAILING = 2;
    
    /** Kontaktkan�le: E-Mail, Telefon, Post, Fax, pers�nlich */
    public final static int CHANNEL_NA = 0;
    public final static int CHANNEL_EMAIL = 1;
    public final static int CHANNEL_PHONE = 2;
    public final static int CHANNEL_MAIL = 3;
    public final static int CHANNEL_FAX = 4;
    public final static int CHANNEL_DIRECT = 5;
    
    /** Linktypen: E-Mail, Termin, Aufgabe, Dokument */
    public final static int LINKTYPE_NA = 0;
    public final static int LINKTYPE_EMAIL = 1;
    public final static int LINKTYPE_APPOINTMENT = 2;
    public final static int LINKTYPE_JOB = 3;
    public final static int LINKTYPE_DOCUMENT = 4;
}
