package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SortUpAction extends AbstractGUIAction {

    private static final long serialVersionUID = 9054825938999370902L;
    private static final TarentLogger log = new TarentLogger(SortUpAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SortUpAction not implemented");
    }

}