/**
 * 
 */
package org.evolvis.nuyo.plugins.calendar;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.CalenderManager;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.plugin.Plugin;
import de.tarent.commons.ui.EscapeDialog;

/**
 * Offers a calender funcionality.
 * You can get an interface, JPanel or JDialog to use a calender.
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class CalendarPlugin implements Plugin {
    
    public static final String ID = "calender";

    private static final TarentLogger logger = new TarentLogger(CalendarPlugin.class);
    private NewAppointmentPanel appointmentPanel;
    private JDialog dialog;
    private JPanel panel;
    private CalenderManager calendarEventsManager;
    private CalendarsSelectionModel selectionModel;


    public void CalenderPlugin(){
        logger.info("Calender Plugin eanabled.");
    }

    public String getID() {
        return ID;
    }

    public void init() {
    }

    public Object getImplementationFor( Class aClass ) {
        if (TarentCalendar.class == aClass) return getAppointmentPanel(); 
        if (CalendarsSelectionModel.class == aClass) return getCalendarsSelectionModel(); 
        if (JPanel.class == aClass) return getPanel();
        if (JDialog.class == aClass) return getDialog();
        return null;
    }

    private JDialog getDialog() {
        if(dialog == null) {
            dialog = new EscapeDialog( ApplicationServices.getInstance().getActionManager().getFrame(), 
                                  Messages.getString( "GUI_Calender_Dialog_Title" ) );
            dialog.getContentPane().add( getPanel() );
            dialog.setSize( 800, 500 );
            dialog.setLocationRelativeTo( null );
        }
        return dialog;
    }

    private JPanel getPanel() {
        if(panel == null){
            panel = new JPanel();            
            panel.setLayout(new BorderLayout());
            panel.add(getAppointmentPanel(), BorderLayout.CENTER); //$NON-NLS-1$
            panel.add(Box.createVerticalStrut(20), BorderLayout.SOUTH); //$NON-NLS-1$
        }
        return panel;
    }

    private NewAppointmentPanel getAppointmentPanel() {
        if(appointmentPanel == null) {
            appointmentPanel = new NewAppointmentPanel(getCalenderEventsManager(), getCalendarsSelectionModel());
        }
        return appointmentPanel;
    }

    private CalendarsSelectionModel getCalendarsSelectionModel() {
        if(selectionModel == null) {
            selectionModel = new CalendarsSelectionModelImpl(getCalenderEventsManager());
        }
        return selectionModel;
    }

    private CalenderManager getCalenderEventsManager() {
        if (calendarEventsManager == null){
            calendarEventsManager = 
                new CalenderManager(ApplicationServices.getInstance().getMainFrame());
        }
        return calendarEventsManager;
    }

    public List getSupportedTypes() {
        List supportedTypes = new ArrayList();
        supportedTypes.add(TarentCalendar.class);
        supportedTypes.add(CalendarsSelectionModel.class);
        supportedTypes.add(JPanel.class);
        supportedTypes.add(JDialog.class);
        return supportedTypes;
    }

    public boolean isTypeSupported( Class aClass ) {
        return (aClass == TarentCalendar.class) 
        || (aClass == CalendarsSelectionModel.class) 
        || (aClass == JDialog.class) 
        || (aClass == JPanel.class);
    }

	public String getDisplayName() {
		// TODO Auto-generated method stub
		return null;
	}
}
