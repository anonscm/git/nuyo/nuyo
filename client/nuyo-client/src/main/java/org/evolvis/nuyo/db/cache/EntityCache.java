package org.evolvis.nuyo.db.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.db.filter.AbstractRangedDateSelection;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.SelectionElement;
import org.evolvis.nuyo.db.persistence.IEntity;



/**
 * @author kleinw
 *
 *	Motivation : 
 *	Es geht hier nicht um das massenweise Cachen gr��ere 
 *	Daten, sondern darum, h�ufig wiederkehrende Aufrufe
 *	mit kleinen Ergebnismengen zu verwalten, um unn�tige
 *	Soapaufrufe zu verhindern und zu gew�hrleisten,
 *	dass eine identische Selection auch identische Objekte
 *	erh�lt.
 *
 *	Diese Klasse verwaltet Anfragen, die an den AbstractEntityFetcher
 *	gestellt wurden. Durch die Tatsache, dass jede Entit�t 
 *	(Calendar, User, Address, Appointment, etc.) �ber einen eigenen 
 *	statischen AEF verf�gt, ist auch dieses Objekt genau einmal pro
 *	Entit�t vorhanden.
 *
 *	Neben der Wiedererkennung von Selections wird hier auch die Modifikation
 *	und das L�schen von Eintr�gen, sowie die Gr��enverwaltung und
 *	Veraltungskriterien gehandelt.
 *	
 *	Momentan findet die Zuordnung von Selection und Werten
 *	(Collections von Entities) mit 2 Listen statt, die bei der 
 *	HashMap die Wiedererkennung der Selections streikt.
 *
 */
public class EntityCache {
    
    /**	In dieser Liste werden Selections gehalten */
    private List _selections = new ArrayList();
    /** Hier werden passend zu Selections ID-Listen gehalten */
    private List _values = new ArrayList();
    
    
    /**	Diese Map die Entities aller Selektion */
    private Map _entities = new HashMap();
    /**	Alle eines Types, wenn als Selection <null> �bergeben wurde */
    private Map _allEntities = null;
    
    
    /**	Eine Liste von Entities per Selection beziehen.@param selection - Selektion */
    public Collection get(ISelection selection) {
        //	eingeschr�nkte Auswahl
        if(selection != null) {
            if(_selections.contains(selection)) {
                Collection tmp = (Collection)_values.get(_selections.indexOf(selection));
                Collection entityCollection = new ArrayList();
                for(Iterator it = tmp.iterator();it.hasNext();) {
                    Object o = (_entities.get((Integer)it.next()));
					if(o != null)
					    entityCollection.add(o);
                }
                return entityCollection;
            } else
                return null;
        }
        //	alle zur�ckgeben
        else
            return (_allEntities == null)?null:_allEntities.values();
    }
    
    
    public void store(Collection entities) {
        for(Iterator it = entities.iterator();it.hasNext();) {
            IEntity entity = (IEntity)it.next();
            _entities.put(new Integer(entity.getId()), entity);
        }
    }
    
    
    /**	Collection von Entities speichern.@param selection - Selektion,@param entities - dazugeh�rige Entities */
    public void store(ISelection selection, Collection entities) {
        if(selection == null) {
            //	Hier kommen alle Instanzen eines Typs zur�ck */
            for(Iterator it = entities.iterator();it.hasNext();) {
                IEntity entity = (IEntity)it.next();
                if(_allEntities == null)
                    _allEntities = new HashMap();
                _allEntities.put(new Integer(entity.getId()), entity);
            }
        } else {
            if(!_selections.contains(selection)) {
                if(_entities == null)
                    _entities = new HashMap();
                Collection idList = new ArrayList();
                for(Iterator it = entities.iterator();it.hasNext();) {
                    IEntity entity = (IEntity)it.next();
                    idList.add(new Integer(entity.getId()));
                    if(!_entities.containsKey(new Integer(entity.getId())))
                        _entities.put(new Integer(entity.getId()), entity);
                }
                _selections.add(selection);
            	_values.add(idList);
            }
        }
    }
    
    
    /**	Hier kann ein Objekt per ID gel�scht werden.@param id - ID des Objekts */ 
    public void delete(Integer id) {
        if(_entities != null)
            _entities.remove(id);
        //Fix f�r l�schen aus allEntities
        if(_allEntities != null)
        	_allEntities.remove(id);
        Collection selectionsToRemove = new ArrayList();
        for(Iterator it = _selections.iterator();it.hasNext();) {
            ISelection selection = (ISelection)it.next();
            if(selection.containsIdSelection()) { 
                selectionsToRemove.add(selection);
            }
        }
        for(Iterator it = selectionsToRemove.iterator();it.hasNext();) {
            Object tmp = it.next();
            _values.remove(_selections.indexOf(tmp));
            _selections.remove(tmp);
        }
    }
    /** Hier wird der gesamte Cache geleert */
    public void flush(){
    	if(_entities != null) _entities.clear();
    	if(_allEntities != null)_allEntities.clear();
    	if(_values != null) _values.clear();
    	if(_selections != null) _selections.clear();
    }
    
    public void add(IEntity entity) {
        if(!_entities.containsKey(new Integer(entity.getId()))) {
            _entities.put(new Integer(entity.getId()), entity);
        }
    }
    
    
    /**	Die gespeicherten Selektion holen */
    public List getSelections() {return _selections;}
    
    
    /** Werte holen */
    public List getValues() {return _values;}
    
    
    /**	Hier werden alle Selections geholt, die eine RangedSelection beinhalten */
    public Collection getRangedSelections() {
        Collection rangedSelections = new ArrayList();
        for(Iterator it = _selections.iterator();it.hasNext();) {
            ISelection selection = (ISelection)it.next();
            for(Iterator it2 = selection.getFilterElements().iterator();it2.hasNext();) {
                SelectionElement element = (SelectionElement)it2.next();
                if(element instanceof AbstractRangedDateSelection) 
                    rangedSelections.add(selection);
            }
        }
        return rangedSelections;	
    }
    
    
    /**	Hilfsklasse */
    static private class CacheUtils {

        /**	Erzeugt aus einer Liste von Entities eine von IDs.@param entities - Die Entities */
        static private Collection getIdList(Collection entities) {
            Collection idList = new ArrayList();
            for(Iterator it = entities.iterator();it.hasNext();) {
                idList.add(new Integer(((IEntity)it.next()).getId()));
            }
            return idList;
        }
        
   }
    
}
