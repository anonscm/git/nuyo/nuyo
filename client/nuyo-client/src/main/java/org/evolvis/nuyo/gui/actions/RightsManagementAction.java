/* $Id: RightsManagementAction.java,v 1.7 2007/08/30 16:10:20 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nils Neumaier
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 

package org.evolvis.nuyo.gui.actions;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.AdminPanel;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.ui.EscapeDialog;

/**
 * Exiting the Application
 * 
 * Icon: admin.gif
 * Messages: 
 *      GUI_MainFrameNewStyle_Register_Administration,
 *      GUI_MainFrameNewStyle_Register_Administration_ToolTip
 */
public class RightsManagementAction extends AbstractGUIAction {
	
    private static final long serialVersionUID = 4554154903904372423L;
    private JDialog adminSection;

    public void actionPerformed( ActionEvent e ) {
    	SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                adminSection.setVisible(true);
            }
        });
    }
    
    public void init() {
        GUIListener actionmanager = ApplicationServices.getInstance().getActionManager();
        adminSection = new EscapeDialog(actionmanager.getFrame(), Messages.getString("GUI_RightsManagement_Title"));
        
        // Call getContentPane() for 1.4-compatibility.
        adminSection.getContentPane().add(new AdminPanel(actionmanager, actionmanager.getMainControlListener(), false));
        adminSection.setSize(800, 500);
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        int xDistance =  screenDimension.width > 800 ? screenDimension.width - 800 : screenDimension.width;  
        adminSection.setLocation(new Point(xDistance/2, 150));
    }
}
