/* $Id: GroupObjectRoleImpl.java,v 1.9 2007/05/02 14:14:51 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.GroupObjectRoleConnector;
import org.evolvis.nuyo.db.cache.Cachable;
import org.evolvis.nuyo.db.cache.EntityCache;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;


/**
 *
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class GroupObjectRoleImpl extends GroupObjectRoleBean implements
		Cachable {

	/**Methode um Rechte zu holen*/
	public static final String METHOD_GETOBJECTRIGHTS = "getCategoryRights";
	/**Methode um die Rechte anzulegen oder zu ver�ndern*/
	public static final String METHOD_CREATE_OR_MODIFY_OBJECTRIGHT = "createOrModifyCategoryRight";
	/**Methode um ein Recht zu l�schen*/
	public static final String METHOD_DELETE_OBJECTRIGHT = "deleteCategoryRight";
	
	//	Initialisierung des Objekts zum Holen von Daten 
    static {
        _fetcher = new AbstractEntityFetcher(METHOD_GETOBJECTRIGHTS, false) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    GroupObjectRoleConnector groc = new GroupObjectRoleImpl();
                    ((IEntity)groc).populate(values);
                    return (IEntity)groc;
                }
            };
    }
	
	/**	Statische Methode zum Beziehen aller GroupObjectRoles */
	static public Collection getGroupObjectRoles(ISelection selection) throws ContactDBException {return _fetcher.getEntities(selection, false);};
    

	public void updateInCache() throws ContactDBException {
        EntityCache cache = _fetcher.getCache();
        cache.store(null, Collections.singletonList(this));
	}

	public void deleteFromCache() throws ContactDBException {
		_fetcher.getCache().delete(_id);
	}

	public void populate(ResponseData data) throws ContactDBException {
		_responseData = data;
		List rights = (List) data.getORes().getData("arights");
		List roles = (List) data.getORes().getData("roles");
		List groups = (List) data.getORes().getData("groups");
		List objects = (List) data.getORes().getData("objects");
		List content = (List) data.getORes().getData(data.getUsedContent());
		//Als erstes feststellen, an welcher stelle der Liste rights wir uns befinden...
		int i=content.indexOf(data.getContent());
		Map rightMap = (Map) rights.get(i);
		Map roleMap = (Map) roles.get(i);
		Map groupMap = (Map) groups.get(i);
		Map objectMap = (Map) objects.get(i);
		_id = (Integer) rightMap.get("Pk");
		object = new Category();
		((Category)object).setId(new Integer(""+objectMap.get("PkCategory")).intValue());
		((Category)object).setName(""+objectMap.get("Categoryname"));
		((Category)object).setDescription(""+objectMap.get("Description"));
		((Category)object).setCategoryType((Integer)(objectMap.get("Categorytype")));
            //((IEntity)object).populate(new ResponseData(new Object[]{objectMap.get("PkCategory"), objectMap.get("Categoryname"), objectMap.get("Description"), objectMap.get("Categorytype")}));
		group = new UserGroupImpl();
		((IEntity)group).populate(new ResponseData(new Object[]{groupMap.get("Pk"), groupMap.get("Groupname"), groupMap.get("Description"), null, null, groupMap.get("FkGlobalrole"), groupMap.get("Isprivate"), groupMap.get("Grouptype")}));
		role = new ObjectRoleImpl();
		((IEntity)role).populate(new ResponseData(roleMap));
	}

	public void validate() throws ContactDBException {
		//Tue erstmal noch nichtes
	}

	public void commit() throws ContactDBException {
		prepareCommit(METHOD_CREATE_OR_MODIFY_OBJECTRIGHT);
		Map attributes = new HashMap();
		attributes.put("categoryID", ((Category)object).getIdAsInteger());
		attributes.put("groupID", new Integer(group.getId()));
		attributes.put("roleID", new Integer(role.getId()));
		_method.add("attributes", attributes);
		_id = (Integer) _method.invoke();
	}

	public void delete() throws ContactDBException {
		Method method = new Method(METHOD_DELETE_OBJECTRIGHT);
		method.add("id", _id);
		method.invoke();
	}

	public void rollback() throws ContactDBException {
		populate(_responseData);
	}

}
