/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class HandyPrivatField extends GenericTextField
{
  public HandyPrivatField()
  {
    super("HANDYPRIVAT", AddressKeys.HANDYPRIVAT, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Handy_privat_ToolTip", "GUI_MainFrameNewStyle_Standard_Handy_privat", 30);
  }
}
