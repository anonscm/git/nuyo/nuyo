/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;

import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.db.AppointmentRequest;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.octopus.AppointmentImpl;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;


/**
 * @author niko
 *
 */
public abstract class ContactAddressField implements AddressField {
    public final static int CONTEXT_UNKNOWN = 0;
    public final static int CONTEXT_ADRESS = 1;
    public final static int CONTEXT_MAILBATCH = 2;
    public final static int CONTEXT_USER = 3;
    public final static int CONTEXT_MAIL = 3;
    public final static int CONTEXT_NONE = 4;

    private ControlListener controlListener;
    protected EntryLayoutHelper panel;
    private GUIListener guiListener;
    private WidgetPool widgetPool;

    protected String fieldDescription;
    protected String fieldName;
    
    protected ContactAddressField()
    {
    }

    public void setFieldName( String name ) {
        fieldName = name;
        if ( panel != null ) {
            if ( name != null )
                panel.getTitleComponent().setData( fieldName );
        }
    }

    public void setFieldDescription( String description ) {
        fieldDescription = description;
        if ( panel != null ) {
            if ( description != null )
                panel.getEntryComponent().getComponent().setToolTipText( getFieldDescription() );
        }
    }

    public GUIListener getGUIListener() {
        return guiListener;
    }

    public ControlListener getControlListener() {
        return controlListener;
    }
    
    public WidgetPool getWidgetPool() {
        return widgetPool;
    }

    public TarentCalendar getAppointmentDisplayManager() {
        return (TarentCalendar) ( getWidgetPool().getController( CalendarVisibleElement.MANAGER ) );
    }

    public void setGUIListener( GUIListener gl ) {
        guiListener = gl;
    }

    public void setControlListener( ControlListener gl ) {
        controlListener = gl;
    }

    public void setWidgetPool( WidgetPool widgetpool ) {
        widgetPool = widgetpool;
    }

    public boolean isWriteOnly() {
        return false;
    }

    public int getContext() {
        return CONTEXT_UNKNOWN;
    }

    private Object m_oDisplayedAddress = null;

    public Object getDisplayedObject() {
        return m_oDisplayedAddress;
    }

    public void setDisplayedObject( Object displayedobject ) {
        m_oDisplayedAddress = displayedobject;
    }

    public int getTimerTicks() {
        return -1;
    }

    public void onTimerEvent() {
    }

    public void onShow() {
    }

    public Collection getAppointments( Date start, Date end ) throws ContactDBException {
        Collection allAppointments = getGUIListener().getCalendarCollection().getAppointments( start, end, null );
        Collection appointments = new ArrayList();
        appointments.addAll( allAppointments );

        AppointmentImpl tmpAppointment;
        AppointmentRequest tmpRelation;
        Calendar cal = getGUIListener().getUser( null ).getCalendar( true );

        for ( Iterator iter = allAppointments.iterator(); iter.hasNext(); ) {

            tmpAppointment = (AppointmentImpl) iter.next();
            tmpRelation = tmpAppointment.getRelation( cal );

            // if the status of this Appointment is set to DECLINED from the current User it will not be displayed
            if ( tmpRelation != null && tmpRelation.getRequestStatus() == AppointmentRequest.STATUS_DECLINED ) {
                appointments.remove( tmpAppointment );
                getGUIListener().getLogger()
                    .log(
                          Level.INFO,
                          "ContactAdressField::getAppointments: Appointment " + tmpAppointment.getId()
                              + " identified as status DECLINED -> Appointment removed from Panel" );
            }
        }
        return appointments;
    }
    
    
}
