/* $Id: User.java,v 1.5 2007/02/01 17:21:35 asteban Exp $
 * 
 * Created on 21.04.2004
 *
 */
package org.evolvis.nuyo.db;

import java.util.Collection;
import java.util.List;

/**
 * Diese Schnittstelle stellt einen Benutzer des Systems dar.
 * 
 * @author mikel
 */
public interface User {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();


    /**
     * Liefert die Globalen Rechte des Users
     */
    public GlobalRole getGlobalRights() throws ContactDBException;
    /**
     * Liefert die Rechte des Users auf eine Kategory
     */
    public ObjectRole getCategoryRights(String categoryObjectID) throws ContactDBException;
    
    /**
     * �berpr�ft alle Kategorien ob auf irgendeiner Kategorie Rechte f�r diesen User bestehen
     * Welches kategoriebezogene Recht �berpr�ft werden soll, wird als zweiter Parameter �bergeben 
     */
    public boolean hasAnyCategoryRights(List categorys, String type) throws ContactDBException;
    
    /** Login-Name */
    public String getLoginName() throws ContactDBException;
    
    /** Adminstatus */
    public boolean isAdminUser() throws ContactDBException;
    
    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    /**	Parameter f�r den User */
    public String getParameter(String key) throws ContactDBException;
    public void setParameter(String key, String value) throws ContactDBException;
    
    /** assoziiertes Adressobjekt; <code>null</code>, wenn es keines gibt */
    public Address getAssociatedAddress() throws ContactDBException;
    

    /** Benutzergruppen, denen der Benutzer angeh�rt */
    public Collection getGroups() throws ContactDBException;
    public void add(UserGroup group) throws ContactDBException;
    public void remove(UserGroup group) throws ContactDBException;

    /** Kalendarien, auf die der Benutzer Zugriff hat */
    public Collection getCalendars() throws ContactDBException;
    public void add(Calendar calendar) throws ContactDBException;
    public void remove(Calendar calendar) throws ContactDBException;
    public boolean canRead(Calendar calendar) throws ContactDBException;
    public boolean canWrite(Calendar calendar) throws ContactDBException;
    
    /**
     * Diese Methode holt das Kalendarium des Benutzers. Falls es noch keines gibt,
     * kann dieses automatisch erzeugt werden.
     * 
     * @param create zeigt an, ob gegebenenfalls ein Kalendarium f�r den Benutzer erzeugt
     *  werden soll.
     * @return Kalendarium des Benutzers; falls keines existiert und das Erzeugungsflag
     *  nicht gesetzt ist, wird <code>null</code> zur�ckgegeben.
     */
    public Calendar getCalendar(boolean create) throws ContactDBException;

    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Login-Name, Vorname, Nachname */
    public final static String KEY_LOGIN_NAME = "login";
    public final static String KEY_FIRST_NAME = "firstname";
    public final static String KEY_LAST_NAME = "lastname";
    public final static String KEY_PASSWORD = "password";
    public final static String KEY_USER_TYPE = "userType";
    public final static String KEY_EMAIL = "email";


    public final static int USER_TYPE_OTHER = 0;
    public final static int USER_TYPE_ADMIN = 1;
}
