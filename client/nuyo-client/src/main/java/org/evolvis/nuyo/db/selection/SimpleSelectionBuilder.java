/* $Id: SimpleSelectionBuilder.java,v 1.5 2007/02/16 11:07:34 nils Exp $
 * Created on 20.10.2003
 */
package org.evolvis.nuyo.db.selection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.Selection;


/**
 * Dieser Selektionserzeuger kann einfache Selektionen erzeugen, die dem
 * bisherigen Umfang der ContactClient-Suche entsprechen.
 * 
 * @author mikel
 */
public class SimpleSelectionBuilder {
    /**
     * Der Konstruktor legt das zugeh�rige Datenbankobjekt ab.
     * 
     * @param db Datenbankobjekt.
     */
    public SimpleSelectionBuilder(Database db) {
        super();
        this.db = db;
    }

    /**
     * Diese Methode liefert die Selektion, die durch die aktuellen Angaben
     * festgelegt wird.
     *  
     * @return aktuelle Selektion.
     */
    public Selection getSelection() {
        return null;
    }

    /*
     * Getter und Setter
     */
    /**
     * Diese Methode liefert die aktuelle Kategorie.
     * 
     * @return Schl�ssel der aktuellen Kategorie.
     */
    public String getKategorie() {
        return kategorie;
    }

    /**
     * Diese Methode setzt die aktuelle Kategorie.
     * 
     * @param kategorie Schnl�ssel der neuen aktuellen Kategorie.
     * @throws ContactDBException
     */
    public void setKategorie(String kategorie) throws ContactDBException {
        this.kategorie = null;
        unterkategorien.clear();
        unterkategorieSelection.clear();
        if (db.getCategory(kategorie) != null)
            this.kategorie = kategorie;
        refreshUnterkategorien();
        unterkategorieSelection.addAll(unterkategorien.keySet());
    }

    /**
     * Diese Methode aktualisiert die Unterkategorien der aktuellen
     * Kategorie aus der Datenbank.
     * 
     * @throws ContactDBException
     */
    public void refreshUnterkategorien() throws ContactDBException {
        unterkategorien.clear();
        if (kategorie != null) {
            unterkategorien.putAll(db.getVerteiler(kategorie));
        }
    }

    /**
     * Diese Methode liefert, ob eine bestimmte Unterkategorie ausgew�hlt ist.
     *  
     * @param unterkategorie Schl�ssel der betroffenen Unterkategorie.
     * @return Selektionsstatus der betroffenen Unterkategorie.
     */
    public boolean isUnterkategorieSelected(String unterkategorie) {
        return unterkategorieSelection.contains(unterkategorie);
    }

    /**
     * Diese Methode legt fest, ob eine bestimmte Unterkategorie ausgew�hlt ist.
     * 
     * @param unterkategorie Schl�ssel der betroffenen Unterkategorie; falls
     *  hier <code>null</code> angegeben wurde, werden alle Unterkategorien
     *  (de)selektiert.
     * @param selected neuer Selektionsstatus der betroffenen Unterkategorie.
     */
    public void setUnterkategorieSelected(String unterkategorie, boolean selected) {
        if (unterkategorie == null) {
            if (selected)
                unterkategorieSelection.addAll(unterkategorien.keySet());
            else
                unterkategorieSelection.clear();
        } else if (unterkategorien.containsKey(unterkategorie)) {
            if (selected)
                unterkategorieSelection.add(unterkategorie);
            else
                unterkategorieSelection.remove(unterkategorie);
        }
    }

    /**
     * Diese Methode liefert die Schl�ssel der ausgew�hlten Unterkategorien.
     * 
     * @return Menge der Schl�ssel der ausgew�hlten Unterkategorien.
     */
    public Set getSelectedUnterkategorien() {
        return new HashSet(unterkategorieSelection);
    }

    /**
     * Diese Methode liefert, ob allgemeine Adressen hinzugef�gt werden
     * sollen oder nicht.
     * 
     * @return Status der Einf�gung allgemeiner Adressen.
     */
    public boolean isAllgemeine() {
        return allgemeine;
    }

    /**
     * Diese Methode legt fest, ob allgemeine Adressen hinzugef�gt werden
     * sollen oder nicht.
     * 
     * @param allgemeine neuer Status der Einf�gung allgemeiner Adressen.
     */
    public void setAllgemeine(boolean allgemeine) {
        this.allgemeine = allgemeine;
    }

    /*
     * gesch�tzte Felder
     */
    private Database db = null;
    private String kategorie = null;
    private Map unterkategorien = new HashMap();
    private Set unterkategorieSelection = new HashSet();
    private boolean allgemeine = false;
}
