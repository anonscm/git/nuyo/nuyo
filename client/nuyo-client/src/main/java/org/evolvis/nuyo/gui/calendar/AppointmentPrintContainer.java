/*
 * Created on 29.07.2005
 *
 */
package org.evolvis.nuyo.gui.calendar;

/**
 * @author simon
 *
 * 
 */
public class AppointmentPrintContainer {
	
	private String _monat;
	private int _kw;
	private String _titel;
	private String _ort;
	private String _start;
	private String _end;
	private String _Teilnehmer;
	private String _Tag_vonbis;
	private String _Datum_vonbis;
	
	
	
	
	
	

	public String get_end() {
		return _end;
	}
	public void set_end(String _end) {
		this._end = _end;
	}
	public int get_kw() {
		return _kw;
	}
	public void set_kw(int _kw) {
		this._kw = _kw;
	}
	public String get_monat() {
		return _monat;
	}
	public void set_monat(String _monat) {
		this._monat = _monat;
	}
	public String get_ort() {
		return _ort;
	}
	public void set_ort(String _ort) {
		this._ort = _ort;
	}
	public String get_start() {
		return _start;
	}
	public void set_start(String _start) {
		this._start = _start;
	}
	public String get_Teilnehmer() {
		return _Teilnehmer;
	}
	public void set_Teilnehmer(String teilnehmer) {
		_Teilnehmer = teilnehmer;
	}
	public String get_titel() {
		return _titel;
	}
	public void set_titel(String _titel) {
		this._titel = _titel;
	}
	public String get_Datum_vonbis() {
		return _Datum_vonbis;
	}
	public void set_Datum_vonbis(String datum_vonbis) {
		_Datum_vonbis = datum_vonbis;
	}
	public String get_Tag_vonbis() {
		return _Tag_vonbis;
	}
	public void set_Tag_vonbis(String tag_vonbis) {
		_Tag_vonbis = tag_vonbis;
	}
}
