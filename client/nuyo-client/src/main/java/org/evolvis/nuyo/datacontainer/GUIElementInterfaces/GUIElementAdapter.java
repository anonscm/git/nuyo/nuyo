/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElementInterfaces;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import org.evolvis.nuyo.controls.TarentWidgetChangeListener;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIValueChangedEvent;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class GUIElementAdapter implements GUIElement, TarentGUIEventListener
{
  private DataContainer m_oDataContainer;
  protected List m_oParameterList = new ArrayList();
  private Map m_oDataMap = null;

  public GUIElementAdapter()
  {    
    m_oDataContainer = null;        
  }
    
  public abstract void setFocus();

  public abstract void initElement();
  public abstract void disposeElement();
  public abstract boolean addParameter(String key, String value);
  public abstract boolean canDisplay(Class data);
  public abstract Class displays();
  public abstract TarentWidgetInterface getComponent();
  public abstract void setData(Object data);
  public abstract Object getData();

  
  public void setDataMap(Map map)
  {
    m_oDataMap = map;
  }

  public Map getDataMap()
  {
    return m_oDataMap;
  }
  
  public Dimension getNeededSize()
  {
    if (getComponent() != null)
    {
      return getComponent().getComponent().getPreferredSize();
    }
    return null;
  }


  public List getEventsConsumable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_REQUESTFOCUS);
    list.add(TarentGUIEvent.GUIEVENT_VALUECHANGED);
    return list;
  }

  public List getEventsFireable()
  {
    ArrayList list = new ArrayList();
    list.add(TarentGUIEvent.GUIEVENT_FOCUSGAINED);
    list.add(TarentGUIEvent.GUIEVENT_FOCUSLOST);
    list.add(TarentGUIEvent.GUIEVENT_VALUECHANGED);
    return list;
  }

  public void event(TarentGUIEvent tge)
  {
    if (TarentGUIEvent.GUIEVENT_REQUESTFOCUS.equals(tge.getName()))
    {
      setFocus();
    }    
    else if (tge instanceof TarentGUIValueChangedEvent)
    {
      Object newvalue = ((TarentGUIValueChangedEvent)tge).getValue();
      setData(newvalue);
    }    
  }
  
  public void init()
  {
    getDataContainer().addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_REQUESTFOCUS, this);    
    getDataContainer().addTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALUECHANGED, this);    
    initElement();
  }

  public void dispose()
  {
    getDataContainer().removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_REQUESTFOCUS, this);    
    getDataContainer().removeTarentGUIEventListener(TarentGUIEvent.GUIEVENT_VALUECHANGED, this);    
    disposeElement();
  }
  
  
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().addTarentGUIEventListener(event, handler);
  }
    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().removeTarentGUIEventListener(event, handler);
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    getDataContainer().fireTarentGUIEvent(e);
  }

  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;

    boolean namewasset = false;
    if (m_oDataContainer.getFieldDescriptor() != null)
    {
      if (m_oDataContainer.getFieldDescriptor().getName() != null)
      {
        if (m_oDataContainer.getFieldDescriptor().getName().length() > 0)
        {
          getComponent().getComponent().setName("tcGUIField_" + m_oDataContainer.getFieldDescriptor().getName());
          namewasset = true;
        }
      }
    }

    if (!namewasset) getComponent().getComponent().setName("tcGUIField_" + m_oDataContainer.getKey());
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }
    
  public Point getOffsetForError(ErrorHint errorhint)
  {
    return(new Point(0,0));
  }

  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    getComponent().addWidgetChangeListener(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    getComponent().removeWidgetChangeListener(listener);    
  }
  
  
  public ImageIcon loadIcon(String filename)
  {
    String path = (String)(getDataContainer().getDataMap().get("GFXPATH"));
    if (path == null)
    {
      path = System.getProperty("user.home") + File.separator + "DocTopus" + File.separator + "gfx" + File.separator;  
    }
    String file = path + filename;
    
    ImageIcon icon = null;
    if (new File(file).exists())
    {
      icon = new ImageIcon(file);          
    }
    else
    {
      URL imagebase = this.getClass().getResource("/de/tarent/datacontainer/resources/" + filename);
      if (imagebase == null) {
          System.out.println("WARNING: Can not find image resource in CLASSPATH :/de/tarent/datacontainer/resources/" + filename);
          return null;
      }
      icon = new ImageIcon(imagebase);
    }
    
    return icon;
  }
  
  
  
}
