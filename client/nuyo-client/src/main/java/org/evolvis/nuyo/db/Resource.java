/* $Id: Resource.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 22.04.2004
 */
package org.evolvis.nuyo.db;

/**
 * Diese Schnittstelle stellt eine Ressource im Kontext der Groupware dar.
 * 
 * @author mikel
 */
public interface Resource {
    //
    // Attribute
    //
    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    /** automatische Freigabe */
    public boolean isAutoPublishing() throws ContactDBException;
    public void setAutoPublishing(boolean newAutoPublishing) throws ContactDBException;
    
    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Name, Beschreibung, Ersteller, letzter Bearbeiter */
    public final static String KEY_NAME = "name";
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    
}
