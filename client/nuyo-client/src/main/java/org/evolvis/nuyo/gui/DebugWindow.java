/*
 * Created on 02.07.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ApplicationServices;


/**
 * @author niko
 *
 */
public class DebugWindow extends JFrame
{
  private GUIListener m_oGUIListener;
  
  public DebugWindow(GUIListener oGUIListener)
  {
    super("DEBUG");
    m_oGUIListener = oGUIListener;
    
    JPanel panel = new JPanel(new GridLayout(0,1));    
    for(int i=0; i<3; i++)
    {  
      JButton button = new JButton("MessageBox #" + i + 1);
      button.addActionListener(new button_clicked(i + 1));      
      panel.add(button);
    }
    
    this.getContentPane().add(panel);
    this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    this.pack();
    this.setVisible(true);
  }
  
  private class button_clicked implements ActionListener
  {
    private int m_iTyp;
    
    public button_clicked(int type)
    {
      m_iTyp = type;    
    }
    
    private final static String bullet = "\u2022";
    
    public void actionPerformed(ActionEvent e)
    {
      String caption = "caption";
      String question = "question";
      String[] answers = null;
      switch(m_iTyp)
      {
        case(1): 
          caption = "Eingabe-Pr�fung";
          question = "Der neue Datensatz ist unvollst�ndig und kann so nicht angelegt werden.\n\nBitte f�llen Sie folgende Eingabefelder und w�hlen Sie anschlie�en die Funktion \"anlegen\" erneut:\n\n" + bullet + " PLZ\n" + bullet + " Ort\n\nHinweis: Wenn Sie den Datensatz nicht anlegen wollen, k�nnen Sie die Funktion \"abbrechen\" w�hlen.";
          answers = new String[1];
          answers[0] = "Ok";
        break;      
      }
      ApplicationServices.getInstance().getCommonDialogServices().askUser(caption, question, answers, 0);
    }
  }
  
  
}
