/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Hendrik Helwich. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.db;

import org.evolvis.nuyo.db.octopus.OctopusDatabase;

/**
 * Repr�sentation eines Suchfeldes als Knoten in einem booleschen Ausdruck.
 * 
 * @author hendrik
 */
public class SearchField implements SearchAtom {

	private String name; //name des Suchfeldes
	private String searchString = null; //Suchstring
	private boolean useMatchcode = false;
	//private short operation = -1;
	private byte operation;
	private boolean negated;
	
	public static final byte EQUAL = 0;
	public static final byte CONTAINS = 1;
	public static final byte LIKE = 2;
	public static final byte IN = 3;
	
	private static final byte NR_OF_OPERATIONS = 4;
	
	//Array-Index mu� mit der Nummerierung der Operatoren �bereinstimmen.
	private static final String[][] sql = {{"eq", " =",   " <>"},
										   {"ct", " LIKE"," NOT LIKE"},
										   {"lk", " LIKE"," NOT LIKE"},
										   {"in", " IN",  " NOT IN"}};
	private boolean isList = false;

	public SearchField(String name) {
		setName(name);
		setOperation(EQUAL);
	}

	/**
	 * 
	 * @see org.evolvis.nuyo.db.SearchAtom#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	public SearchAtom getLeft() {
		throw new RuntimeException("ein Suchfeld kann keine Kinder enthalten");
	}

	public SearchAtom getRight() {
		throw new RuntimeException("ein Suchfeld kann keine Kinder enthalten");
	}

	public void addChild(SearchAtom atom) {
		throw new RuntimeException("ein Suchfeld kann keine Kinder enthalten");
	}

	public String getOperation() {
		if (negated)
			return sql[operation][2];
		else
			return sql[operation][1];
	}
	
	public void setName(String name) {
		this.name = name.toUpperCase();
		if (this.name.startsWith("MATCHCODE")) {
			useMatchcode = true;
			this.name = this.name.substring(9);
		}
	}

	public String getName() {
		if (useMatchcode)
			return "MATCHCODE" + name;
		return name;
	}

	public String getSearchString() {
		String str = searchString;
		if (operation == CONTAINS) {
			if (str.charAt(0) != '%')
				str = '%' + str;
			if (str.charAt(str.length() - 1) != '%')
				str += '%';
		}
		if (useMatchcode)
			return OctopusDatabase.getMatchcode(name, searchString);
		else
			return str;
	}

	public void setOperation(byte opcode, boolean negated) {
		this.negated = negated;
		if (opcode<0 || opcode>=NR_OF_OPERATIONS)
			throw new IllegalArgumentException("unbekannte Operation");
		operation = opcode;
	}

	public void setOperation(byte opcode) {
		setOperation(opcode, false);
	}

	public void setOperation(String opcode) {
		if (opcode.charAt(0) == 'n') {
			negated = true;
			opcode = opcode.substring(1);
		} else
			negated = false;
		for (byte b=0; b<NR_OF_OPERATIONS; b++)
			if (opcode.equals(sql[b][0])) {
				operation = b;
				return;
			}
		throw new IllegalArgumentException("unbekannte Operation: " + opcode);
	}

	public void setSearchString(String searchString) {
		if (searchString.indexOf('*') != -1) {
			this.searchString = searchString.replace('*', '%');
			if (operation == EQUAL)
				operation = LIKE;
		} else
			this.searchString = searchString;
		if (searchString.charAt(0)=='(')
			isList = true;
	}    

	public String toString() {
		StringBuffer sql = new StringBuffer();
		sql.append(getName());
		sql.append(getOperation());
		if (isList) { //Liste darf nicht in Anf�hrungsstrichen stehen
			sql.append(" ");
			sql.append(getSearchString());
		} else {
			sql.append(" '");
			sql.append(getSearchString());
			sql.append("'");
		}
		return sql.toString();
	}
}
