/**
 * 
 */
package org.evolvis.nuyo.gui.categorytree;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.DocumentListener;
import javax.swing.tree.TreePath;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.ui.DescriptiveTextField;
import de.tarent.commons.ui.swing.ComboBoxMouseWheelNavigator;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class FilterPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6685789700875514647L;

	protected DescriptiveTextField filterTextField;
	protected JComboBox selectionFilter;

	protected Visibility[] visibilities;

	protected JTree tree;

	protected CategoryTreeDataModel model;
	

	public FilterPanel(DocumentListener docListener, Visibility[] visibilities, JTree tree, CategoryTreeDataModel model) {

		this.visibilities = visibilities;
		this.tree = tree;
		this.model = model;

		FormLayout layout = new FormLayout("pref, 2dlu, fill:pref:grow, 2dlu, pref", "pref");

		setLayout(layout);

		CellConstraints cc = new CellConstraints();

		JButton resetButton = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/de/tarent/commons/gfx/edit-clear.png"))));
		resetButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				FilterPanel.this.filterTextField.setText("");
				FilterPanel.this.selectionFilter.setSelectedIndex(0);
			}
		});

		filterTextField = new DescriptiveTextField();
		filterTextField.setDescription("Enter your filter terms here");

		filterTextField.getDocument().addDocumentListener(docListener);

		String[] visibilityNames = new String[visibilities.length];


		selectionFilter = new JComboBox(visibilities);
		selectionFilter.setRenderer(new DefaultListCellRenderer() {

			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				return super.getListCellRendererComponent(list, ((Visibility)value).getLabel(), index, isSelected, cellHasFocus);
			}

		});
		selectionFilter.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				Iterator ite = FilterPanel.this.model.getExpandedNodes();

				if (e.getItem() != null)
					FilterPanel.this.model.setVisibility((Visibility)e.getItem());

				prepareModel(FilterPanel.this.tree, FilterPanel.this.model);

				while (ite.hasNext())
					FilterPanel.this.tree.expandPath((TreePath) ite.next());
			}

		});
		selectionFilter.addMouseWheelListener(new ComboBoxMouseWheelNavigator(selectionFilter));

		this.add(resetButton, cc.xy(1, 1));
		this.add(filterTextField, cc.xy(3, 1, "d, f"));
		this.add(selectionFilter, cc.xy(5, 1));
	}

	private static void prepareModel(final JTree t, CategoryTreeDataModel dm)
	{
		CategoryTreeDataModel.Consumer consumer = new CategoryTreeDataModel.Consumer() {
			public void repaint() {
				t.repaint();
			}
		};

		t.setModel(dm.createSnapshot(consumer));
	}
}
