/*
 * Created on 13.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.parser;

/**
 * @author niko
 *
 */
public class ResourceFetcherException extends Exception
{
  public ResourceFetcherException()
  {
    super();
  }

  public ResourceFetcherException(String message)
  {
    super(message);
  }

  public ResourceFetcherException(Throwable cause)
  {
    super(cause);
  }

  public ResourceFetcherException(String message, Throwable cause)
  {
    super(message, cause);
  }
}
