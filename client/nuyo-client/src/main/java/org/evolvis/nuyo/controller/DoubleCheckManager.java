package org.evolvis.nuyo.controller;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.dialogs.DoubletSearchDialog;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.utils.TaskManager;

public class DoubleCheckManager {
	
	private static DoubleCheckManager instance;
	
	private static final TarentLogger log = new TarentLogger(DoubleCheckManager.class);
    private GUIListener guiListener;
    private TaskManager.Task validator;
	
	private DoubleCheckManager(){
		
		guiListener = ApplicationServices.getInstance().getActionManager();
        validator = new TaskManager.Task(){
            public void run(TaskManager.Context ctx) {
            	checkForDoublesAndHandleResults(false);
            }
            
            public void cancel()
            {
            }
        };
	}
	
	public static DoubleCheckManager getInstance(){
		if (instance != null)
			return instance;
		return new DoubleCheckManager();
	}
	
	/**
     * Veranlasst das Suchen nach Doubletten in einem eigenen Thread und bietet gegebenenfalls eine Liste
     * zur �bernahme an.
     * Auf Wunsch des Benutzers wird die aktuell angew�hlte/editierte Adresse verworfen und ein Zuweisungsdialog
     * f�r die als Dublikat erkannte Adresse ge�ffnet.
     * Das Nichtvorhandensein von Dublikaten wird nicht extra angezeigt.
	 */
	 	
	public void checkForDoublesAndHandleResults(){
		TaskManager.getInstance().registerExclusive(validator, Messages.getString("SaveContactAction_CheckingDuplicates"), false);
	}
	
	private void checkForDoublesAndHandleResults(boolean informNoDoubles){
		DoubletSearchDialog dialog = guiListener.guiRequestCheckForDouble(informNoDoubles, false); 
		String status = dialog == null ? DoubletSearchDialog.STATUS_SAVE : dialog.getStatus();
		if (status == null)
			status = DoubletSearchDialog.STATUS_ABORT;
    	
		if (status.equals(DoubletSearchDialog.STATUS_ABORT))
			return;
		
		else if (status.equals(DoubletSearchDialog.STATUS_JUMP_TO_DOUBLE)){
			Address chosenDouble = dialog.getSelectedAddress();
			
			if (chosenDouble != null){
				List pkList = new ArrayList();
				pkList.add(new Integer(chosenDouble.getId()));
				
				guiListener.userRequestNotEditable(GUIListener.DO_DISCARD);
				AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
				alp.setPkFilter(pkList);
		        alp.setAddressSourceLabel("");
		        ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
		   
			}
		}
		
		else if (status.equals(DoubletSearchDialog.STATUS_SAVE)){
			
		}
	}
	
}
