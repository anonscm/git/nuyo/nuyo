/*
 * Created on 23.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.Events;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.plugin.PluginData;

/**
 * @author niko
 *
 */
public class TarentGUISourceChangedEvent extends TarentGUIEvent
{
  private PluginData m_oPluginData = null;
  
  public TarentGUISourceChangedEvent(DataContainer sender, PluginData data)
  {
    super(TarentGUIEvent.GUIEVENT_SOURCECHANGED, sender, null);
    m_oPluginData = data;
  }
  
  public PluginData getSource()
  {
    return m_oPluginData;
  }
}
