/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;

/**
 * @author niko
 */
public class ExtendedDataCollectionForTabs implements ExtendedDataCollection
{
  private JTabbedPane m_oTabbedPane;
  
  public JPanel createContainer(ArrayList list)
  {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    
    m_oTabbedPane = new JTabbedPane();
    m_oTabbedPane.setFont(new Font(null, Font.PLAIN, 12));
    m_oTabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    m_oTabbedPane.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(m_oTabbedPane));


    for(int i=0; i<(list.size()); i++)
    {    
      ExtendedDataItemModel itemmodel = (ExtendedDataItemModel)(list.get(i));
      ImageIcon icon = itemmodel.getIcon();
      ExtendedDataItemForTabs item = new ExtendedDataItemForTabs();
      JPanel itempanel = item.createPanel(itemmodel);
      m_oTabbedPane.addTab(itemmodel.getTitle(),  icon, itempanel,   itemmodel.getToolTip()); 
    }

    panel.add(m_oTabbedPane, BorderLayout.CENTER);
    return(panel);
  }

  public void setFont(Font font)
  {
    m_oTabbedPane.setFont(font);
  }
  
  
  
  
  public void setBackgroundColor(Color color)
  {  
  }
  
}
