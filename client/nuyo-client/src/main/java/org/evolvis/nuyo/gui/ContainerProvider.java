/*
 * Created on 22.03.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.util.Map;

/**
 * @author niko
 *
 */
public interface ContainerProvider
{
  public String getContainerProviderName();
  public Map getContainers();
  public void postFinalRealize();
}
