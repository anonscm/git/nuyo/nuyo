/*
 * Created on 16.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * @author niko
 *
 */
public class FileLogWriter implements LogWriter
{
  String m_sFileName = null;
  
  public FileLogWriter(String filename)
  {
    m_sFileName = filename;
  }
  
  public boolean writeLog(String text)
  {
    if (m_sFileName == null) return false;
    Writer writer;
    try
    {
      writer = new FileWriter(m_sFileName);
      writer.write(text);
      writer.close();
    }
    catch (IOException e)
    {
      e.printStackTrace();
      return false;
    }
    
    return true;
  }

}
