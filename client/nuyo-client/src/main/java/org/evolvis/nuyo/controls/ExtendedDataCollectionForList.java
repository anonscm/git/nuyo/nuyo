/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @author niko
 *
 */
public class ExtendedDataCollectionForList implements ExtendedDataCollection
{
  private JScrollPane m_oScrollPane;
  private JPanel m_oPanel;      
  private JPanel m_oScrollingPanel;      
      
  public JPanel createContainer(ArrayList list)
  {
    m_oPanel = new JPanel();
    m_oPanel.setLayout(new BorderLayout());
        
    m_oScrollingPanel = new JPanel();
    BoxLayout boxlayout = new BoxLayout(m_oScrollingPanel, BoxLayout.Y_AXIS);
    m_oScrollingPanel.setLayout(boxlayout);    
    m_oScrollPane = new JScrollPane(m_oScrollingPanel);    
    
    for(int i=0; i<(list.size()); i++)
    {    
      ExtendedDataItemModel itemmodel = (ExtendedDataItemModel)(list.get(i));
      ImageIcon icon = itemmodel.getIcon();
      ExtendedDataItemForList item = new ExtendedDataItemForList();

      JPanel itempanel = item.createPanel(itemmodel);
      item.setTitleIcon(icon);
      m_oScrollingPanel.add(itempanel);
    }
    
    m_oPanel.add(m_oScrollPane, BorderLayout.CENTER);
    return(m_oPanel);
  }
  
  public void setBackgroundColor(Color color)
  {  
    m_oScrollPane.setBackground(color);
    m_oPanel.setBackground(color);
    m_oScrollingPanel.setBackground(color);
  }
  
  public void setFont(Font font)
  {
    for(int i=0; i<(m_oScrollingPanel.getComponentCount()); i++)
    {
      m_oScrollingPanel.getComponent(i).setFont(font);
    }
  }
  
  
}
