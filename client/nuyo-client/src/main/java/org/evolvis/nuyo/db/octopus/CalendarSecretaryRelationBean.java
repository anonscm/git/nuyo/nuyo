/*
 * Created on 09.04.2005
 *
 */
package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.util.DateRange;

/**
 * @author simon
 *
 **/

public abstract class CalendarSecretaryRelationBean extends AbstractEntity implements
        CalendarSecretaryRelation {
    
    //
	//	Instanzmerkmale
	//
    
    protected Integer _SecretaryUserID;
    protected Integer _SecretaryUserOwnerID;
    protected Integer _scheduleID;
    protected DateRange _accessDateRange;
    protected DateRange _readDateRange;
    protected DateRange _writeDateRange;
    
    /**	Hiermit werden eine oder mehrere Instanzen dieses Typs bezogen */
    static protected AbstractEntityFetcher _fetcher;

    public Integer getSecretaryUserID() throws ContactDBException {
        return _SecretaryUserID;
    }

    public Integer getSecretaryUserOwnerID() throws ContactDBException {
        return _SecretaryUserOwnerID;
    }

    public Integer getCalendarID() throws ContactDBException {
        return _scheduleID;
    }

    public DateRange getAccessDateRange() {
        return _accessDateRange;
    }

    public void setAccessDateRange(DateRange AccessDateRange) {
        _accessDateRange = AccessDateRange;
    }

    public DateRange getReadDateRange() {
        return _readDateRange;
    }

    public void setReadDateRange(DateRange ReadDateRange) {
        _readDateRange = ReadDateRange;
    }

    public DateRange getWriteDateRange() {
        return _writeDateRange;
    }

    public void setWriteDateRange(DateRange WriteDateRange) {
        _writeDateRange = WriteDateRange;
    }
    
}
