/*
 * $Id: Schedule.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 19.05.2003
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright interest in the program 'tarent-contact' (which makes passes at
 * compilers) written by Michael Klink.
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.Collection;
import java.util.Date;

/**
 * Diese Schnittstelle h�lt einen Terminplan. Hieraus werden Termine erzeugt, eingesehen, ge�ndert und zerst�rt.
 * 
 * @author mikel
 */
public interface Schedule {
    /**
     * Diese Methode erzeugt einen Termin. <br>
     * Die verwendeten Date-Objekte stellen UTC-Zeiten dar.
     * 
     * @param calendars
     *            die Kalendarien, in die der Termin initial eingetragen wird und bei denen vor Erzeugung gegebenenfalls
     *            ein Konflikt das Erzeugen des Termins verwirft.
     * @param isJob
     *            Flag, ob der Termin eine Aufgabe ist.
     * @param start
     *            Startdatum und -Zeit (inklusive) (UTC)
     * @param end
     *            Enddatum und -zeit (exklusive) (UTC)
     * @param mayConflict
     *            wenn dieser Parameter <code>false</code> ist, wird im Falle eines Konflikts mit bestehenden anderen
     *            Terminen in den �bergebenen Kalendern eine Ausnahme geworfen. Sonst wird der neue Termin erzeugt.
     * @param User
     * 			  User f�r den das Appointment angelegt wird (kann ungleich dem Ersteller sein)
     * @throws ContactDBException
     *             bei Datenzugriffsproblemen. Als Sonderfall dient die Ausnahme-ID
     *             {@link ContactDBException#EX_APPOINTMENT_CONFLICTS}bei Terminkonflikten.
     */
    public Appointment createAppointment(Collection calendars, boolean isJob, Date start, Date end,
            boolean mayConflict, User User) throws ContactDBException;

//     /**
//      * Diese Methode liefert den Termin oder das ToDo zu einer ID.
//      * 
//      * @param id Termin-ID
//      * @return Termin mit der angegebenen ID; <code>null</code>, falls es unter der
//      *  angegebenen ID keinen Termin gibt oder der angemeldete Benutzer keinen Zugriff
//      *  auf den Termin hat (der Termin also sich in keinen vom Benutzer lesbaren Kalender
//      *  befindet und der Benutzer nicht Besitzer des Termins ist)
//      * @throws ContactDBException
//      */
//     public Appointment getAppointment(int id) throws ContactDBException;
    

    /**
     * Diese Methode liefert Terminerinnerungen in einem Zeitinterval.
     * 
     * @param start
     *            UTC-Starttermin (inklusive)
     * @param end
     *            UTC-Endtermin (exklusive)
     * @param filters
     *            Sammlung von Filtern
     * @return Liste der Termine im Zeitintervall
     */
    public Collection getReminders(Date start, Date end, Collection filters) throws ContactDBException;
}