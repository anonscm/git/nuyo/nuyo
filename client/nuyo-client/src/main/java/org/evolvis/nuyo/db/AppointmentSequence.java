/* $Id: AppointmentSequence.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

import java.util.Collection;
import java.util.Date;

/**
 * Diese Schnittstelle beschreibt eine Sammlung wiederkehrender Termine.
 * 
 * @author mikel
 */
public interface AppointmentSequence {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();
   
    //
    // Attribute
    //
    /** Besitzer der Terminreihe */
    public User getOwner() throws ContactDBException;

    /** Start der Terminreihe (UTC) */
    public Date getStart() throws ContactDBException;
    public void setStart(Date newStart) throws ContactDBException;
    
    /** Ende der Terminreihe (UTC) */
    public Date getEnd() throws ContactDBException;
    public void setEnd(Date newEnd) throws ContactDBException;

    /** Anzahl der Wiederholungen */
    public int getOccurences() throws ContactDBException;
    public void setOccurences(int newOccurences) throws ContactDBException;

    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    /** die einzelnen Termine selbst */
    public Collection getAppointments() throws ContactDBException;
    public void remove(Appointment appointment) throws ContactDBException;
    
    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Name */
    public final static String KEY_NAME = "name";
}
