/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Hanno Wendt.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.plugins.mail.orders;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Context;

/**
 * This is the panel with mail orders.
 * It shows the list of orders and its' details.
 * A mail order can be here created and removed.
 * 
 * @author hanno
 */
public class MailBatchesPanel extends JPanel {
    private JScrollPane tableScrollPane;
    private JPanel buttonsPanel;
    private JButton newOrderButton;
    private JButton removeOrderButton;

    private JTable table;
    private MyTableModel tableModel;
    private ListComparator listComparator;
    private DefaultTableColumnModel columnsModel;
    private ListSelectionModel mySelectionModel;

    private Vector data;
    private Vector columnsNames;
    private List setList;

    private GUIListener guiListener;
    private DispatchRequestPanel currentAuftrag;
    private DispatchRequestListener mailOrderListener;

    /** 
     * Constructor for application window.
     */
    public MailBatchesPanel(GUIListener controlListener, DispatchRequestListener val) {
        super();
        guiListener = controlListener;
        mailOrderListener = val;
        tableModel = new MyTableModel();
        table = new JTable(tableModel);
        listComparator = new ListComparator(3, false);

        init(this);
    }

    private void init(JPanel contentPane) {
        mySelectionModel = table.getSelectionModel();
        mySelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new table_row_selected());

        buttonsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        newOrderButton = new JButton(Messages.getString("GUI_MailBatchesPanel_New")); //$NON-NLS-1$		
        newOrderButton.setToolTipText(Messages.getString("GUI_MailBatchesPanel_New_ToolTip"));
        newOrderButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createNewMailOrder();
            }
        });
        
        // Registers a binding which listens on address list size changes
        // and disables the newOrderButton when the value is zero.
        // 
        // This effectively prevents the user from creating mail orders
        // with empty content.
        ApplicationServices.getInstance().getBindingManager().addBinding(
        		new AbstractReadOnlyBinding(ApplicationModel.ADDRESS_LIST_KEY + ".size")
        		{
        		  public void setViewData(Object arg)
        		    {
    			      if (arg == null)
    			        return;
    			      newOrderButton.setEnabled(((Integer) arg).intValue() > 0);
    		        }
                });

        removeOrderButton = new JButton(Messages.getString("GUI_MailBatchesPanel_Delete")); //$NON-NLS-1$		
        removeOrderButton.setToolTipText(Messages.getString("GUI_MailBatchesPanel_Delete_ToolTip"));
        removeOrderButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                guiListener.setWaiting(true);
                if (deleteSelectedMailOrders())
                    mailOrderListener.clearEntries();
                guiListener.setWaiting(false);
            }
        });

        buttonsPanel.add(removeOrderButton);
        buttonsPanel.add(newOrderButton);
        
        tableScrollPane = new JScrollPane(table);
        
        contentPane.setLayout(new BorderLayout(5, 5));
        contentPane.add(buttonsPanel, BorderLayout.NORTH);
        contentPane.add(tableScrollPane, BorderLayout.CENTER);
        getAndShowIt();
    }

    public void refreshList() {
        getAndShowIt();
    }

    public class table_row_selected implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            //Ignore extra messages.
            if (e.getValueIsAdjusting())
                return;

            ListSelectionModel lsm = (ListSelectionModel) e.getSource();
            if (lsm.isSelectionEmpty()) {
                //no rows are selected
            } else {
                //selectedRow is selected
                viewAndChangeAuftrag();
            }
        }
    }

    /** Loads and sorts table data (mail orders).*/
    private void getAndShowIt() {
        guiListener.setWaiting(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setAlignmentX(LEFT_ALIGNMENT);
        initColumnsNames();
        loadTableData();
        initColumnsWidth();
        mailOrderListener.setNumEntries(tableModel.getRowCount());
        guiListener.setWaiting(false);
    }

    private void initColumnsWidth() {
        columnsModel = (DefaultTableColumnModel) table.getColumnModel();
        TableColumn col = columnsModel.getColumn(2);
        columnsModel.removeColumn(col);
        setColumnWidth(0, 50);
        setColumnWidth(1, 225);
        setColumnWidth(2, 100);
        setColumnWidth(3, 100);
        setColumnWidth(4, 100);
        setColumnWidth(5, 100);
        setColumnWidth(6, 100);
        setColumnWidth(7, 100);
    }

    private void loadTableData() {
        data = new Vector();
        setList = guiListener.getVersandAuftraege();
        Collections.sort(setList, new MailBatchComparator(false));
        if (setList != null) // used first in server-version
            {
            Iterator it = setList.iterator();
            MailBatch nextMailOrder;
            Vector currentData;
            while (it.hasNext()) {
                nextMailOrder = (MailBatch) it.next();
                currentData = new Vector();
                currentData.add(new Boolean(false));
                currentData.add(nextMailOrder.getName());
                currentData.add(nextMailOrder.getInitiatorName());
                currentData.add(nextMailOrder.getDateTime());
                currentData.add(Integer.toString(nextMailOrder.getTotal()));
                currentData.add(Integer.toString(nextMailOrder.getMergeMail()));
                currentData.add(Integer.toString(nextMailOrder.getMergeFax()));
                currentData.add(Integer.toString(nextMailOrder.getMergePost()));
                currentData.add(Integer.toString(nextMailOrder.getNotAssigned()));
                data.add(currentData);
            }
        }
        Collections.sort(data, listComparator);
        tableModel.setDataVector(data, columnsNames);
    }

    private void initColumnsNames() {
        columnsNames = new Vector();
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_Delete")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_MailOrder")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_User")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_Date")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_Total")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_EMail")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_Fax")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_Mail")); //$NON-NLS-1$
        columnsNames.add(Messages.getString("GUI_MailBatchesPanel_TableHeader_Remaining")); //$NON-NLS-1$
    }

    
    /** Removes selected mail orders from history and DB.*/
    private boolean deleteSelectedMailOrders() {
        Vector tempDataVector = new Vector(tableModel.getDataVector());
        Vector innerTemp = new Vector();

        int activerow = mySelectionModel.getMinSelectionIndex();
        boolean activerowdeleted = false;

        for (int i = tempDataVector.size() - 1; i > -1; i--) {
            innerTemp = (Vector) tempDataVector.elementAt(i);
            if (innerTemp.firstElement().equals(new Boolean(true))) {
                if (i == activerow)
                    activerowdeleted = true;
                tempDataVector.removeElementAt(i);
                MailBatch batch = (MailBatch) setList.remove(i);
                try {
                    batch.delete();
                } catch (ContactDBException e) {
                }
            }
        }
        //Collections.sort(tempDataVector, listComparator);
        tableModel.setDataVector(tempDataVector, columnsNames);
        columnsModel = (DefaultTableColumnModel) table.getColumnModel();
        TableColumn col = columnsModel.getColumn(2);
        columnsModel.removeColumn(col);
        setColumnWidth(0, 100);
        setColumnWidth(1, 225);
        setColumnWidth(2, 125);
        tableScrollPane.repaint();

        mailOrderListener.setNumEntries(tableModel.getRowCount());
        return (activerowdeleted);
    }

    /**
     * This method transfers a new mail order to DB (with data from mail-orders-Tab) 
     * and then adds it into the table.
     */
    public void createNewMailOrder() {
    	TaskManager.getInstance().registerBlocking(new TaskManager.SwingTask()
    	{
    		Vector newData;

			public void runImpl(Context ctx)
			  throws TaskManager.SwingTask.Exception
			{
                try {
                    // get to DB transfered data
                    MailBatch batch = guiListener.createNewMailOrder();
                    
                    /* TODO: Fix this when tarent-commons has made the
                     * constructors public.
                     if (batch == null)
                     throw new TaskManager.SwingTask.Exception("");
                    */
                    
                    setList.add(0, batch);
                    Collections.sort(setList, new MailBatchComparator(false));
		            
                    newData = new Vector();
                    newData.add(batch.getName());
                    newData.add(batch.getInitiatorName());
                    newData.add(batch.getDateTime());
                    newData.add(Integer.toString(batch.getTotal()));
                    newData.add(Integer.toString(batch.getMergeMail()));
                    newData.add(Integer.toString(batch.getMergeFax()));
                    newData.add(Integer.toString(batch.getMergePost()));
                    newData.add(Integer.toString(batch.getNotAssigned()));
                } catch (Throwable e) {
                    throw new TaskManager.SwingTask.Exception(e.getMessage(), e);
                }
		        
			}

			protected void failed(Context arg0) {
				// Nothing to be done.
			}

			protected void succeeded(Context arg0) {
	            addNewMailOrder(newData);
	            mySelectionModel.setSelectionInterval(0, 0);
	            viewAndChangeAuftrag();
	            mailOrderListener.setNumEntries(tableModel.getRowCount());
			}
    		
    	}, Messages.getString("GUI_MailBatchesPanel_CreatingMailOrder"), false);
    	
    }

    /** Makes the selected mail order editable. */
    private void viewAndChangeAuftrag() {
        int row = mySelectionModel.getMinSelectionIndex();
        if (row != -1) {
            MailBatch batch = (MailBatch) setList.get(row);
            currentAuftrag = new DispatchRequestPanel(guiListener, this, batch);

            mailOrderListener.insertPanel(currentAuftrag);
        } else {
            informUser();
        }
    }

    /** Adds a new entry (mail order) into the table.*/
    private void addNewMailOrder(Vector newOrderData) {
        newOrderData.add(0, new Boolean(false));
        tableModel.insertRow(0, newOrderData);
    }

    private void setColumnWidth(int pColumn, int pWidth) {
        TableColumnModel colModel = table.getColumnModel();
        colModel.getColumn(pColumn).setPreferredWidth(pWidth);
    }

    private void informUser() {
    	ApplicationServices.getInstance().getCommonDialogServices().showInfo(
            Messages.getString("GUI_MailBatchesPanel_Note_Title"),
            Messages.getString("GUI_MailBatchesPanel_Note_Message"));
    }

    /** A model for mail orders table.*/
    private class MyTableModel extends DefaultTableModel {
        /** Allows first column to edit.*/
        public boolean isCellEditable(int row, int column) {
            if (column == 0)
                return true;
            else
                return false;
        }

        /** Returns the entry type of a given column.
         *  The first column is Boolean and the others Strings.
         */
        public Class getColumnClass(int c) {
            if (c == 0)
                return Boolean.class;
            else
                return String.class;
        }

        public void setValueAt(Object value, int row, int col) {
            Vector v = (Vector) dataVector.elementAt(row);
            v.setElementAt(value, col);
            fireTableCellUpdated(row, col);
        }

        public void selectThemAll(boolean selected) {
            for (int i = 0; i < dataVector.size(); i++) {
                Vector v = (Vector) dataVector.elementAt(i);
                v.setElementAt(new Boolean(selected), 0);
                fireTableCellUpdated(i, 0);
            }
        }
    }

    /**
     * List comporator for table entries.
     * The list comparison will be executed on string elements at a given list position.
     */
    private class ListComparator implements Comparator {
        
        private int position;
        private boolean ascending;

        /**
         * @param Position of string elements in a list to be compared.
         */
        public ListComparator(int position, boolean ascending) {
            this.position = position;
            this.ascending = ascending;
        }
        
        public int compare(Object o1, Object o2) {
        	o1 = ((List)o1).get(position);
        	o2 = ((List)o2).get(position);
        	String s1 = o1 == null ? "" : o1.toString();
        	String s2 = o2 == null ? "" : o2.toString();
        	if (ascending)
        		return s1.compareTo(s2);
            else
            	return s2.compareTo(s1);
        }
    }
    
    
    /**
     * Compares two mail orders by date.
     */
    private class MailBatchComparator implements Comparator {
        private boolean ascending;

        public MailBatchComparator(boolean ascending) {
            this.ascending = ascending;
        }

        public int compare(Object o1, Object o2) {
            String s1 = o1 == null ? "" : ((MailBatch)o1).getDateTime();
            String s2 = o2 == null ? "" : ((MailBatch)o2).getDateTime();
            if (ascending)
                return s1.compareTo(s2);
            else
                return s2.compareTo(s1);
        }
    }
}
