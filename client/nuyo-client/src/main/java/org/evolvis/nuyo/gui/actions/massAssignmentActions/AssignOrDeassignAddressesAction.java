package org.evolvis.nuyo.gui.actions.massAssignmentActions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.octopus.SubCategoryImpl;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.actions.AbstractContactDependentAction;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.utils.TaskManager;

abstract class AssignOrDeassignAddressesAction extends AbstractContactDependentAction {

	private static final TarentLogger log = new TarentLogger(
			AssignOrDeassignAddressesAction.class);

	public static final String KEY_ADDRESSPKS = "addresspks";

	public static final String KEY_OPERATION_TYPE = "operation_type";

	public static final String OPERATION_ADD = "add";

	public static final String OPERATION_REMOVE = "remove";

	public static final String OPERATION_MOVE = "move";


	JDialog categoryTreeDialog;

	public final void actionPerformed(ActionEvent e) {

        TaskManager.getInstance().register(new TaskManager.Task() {

                public void run(TaskManager.Context cntx) {

                    // build the tree structure in background
                    CategoryTree tree = actionPerformedImpl();
                    
                    // Checks if action is doable.
                    if (tree == null)
                        return;
                    
                    // show the tree in a swing thread
                    createAndShowCategoryTree(tree);
                }
                
                public void cancel() {}

            }, Messages.getString("GUI_AssignOrDeassignAddressAction_FindAddressSet"), false);
	}

	protected abstract CategoryTree actionPerformedImpl();

    protected void createAndShowCategoryTree(final CategoryTree tree) {
		SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    categoryTreeDialog = new JDialog(ApplicationServices.getInstance()
                                                     .getMainFrame().getFrame(), true);
                    categoryTreeDialog.setTitle(tree.getTitle());
                    categoryTreeDialog.getContentPane().add(tree);
                    categoryTreeDialog.pack();
                    categoryTreeDialog.setLocationRelativeTo(null);
                    categoryTreeDialog.setVisible(true);
                }
            });
    }

	protected void showCategoryTree() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				categoryTreeDialog.setVisible(true);
			}
		});

	}

	protected void hideCategoryTree() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				categoryTreeDialog.setVisible(false);
			}
		});
	}

	protected void closeCategoryTree() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				categoryTreeDialog.setVisible(false);
				categoryTreeDialog.dispose();
			}
		});
	}

	/**
	 * Helper method which converts a list of subcategory instances into one
	 * that contains their primary keys (or ids).
	 * 
	 * @param subCategoryList
	 * @return
	 */
	protected final List asPkList(List subCategoryList) {
		List subCategoryPks = new ArrayList();
		for (Iterator iter = subCategoryList.iterator(); iter.hasNext();) {
			subCategoryPks
					.add(((SubCategoryImpl) iter.next()).getIdAsInteger());
		}

		return subCategoryPks;
	}

	/**
	 * @author Nils Neumaier this handler generates a Task for the Taskmanager
	 *         for parallel execution of the "assignAddresses operation",
	 *         handles error cases and neccessary user interaction
	 */

	protected class AssignAddressesHandler implements
			CategoryTree.AssignHandler {

		private List addressPks;

		private boolean addOrRemove; // flag that says if this is an

		// add-operation (true) or a
		// remove-operation (false)

		/**
		 * @param addressPks
		 *            List with pks of all addresses on which the operation will
		 *            be executed
		 * @param addOrRemove
		 *            flag that says if this is an add-operation (true) or a
		 *            remove-operation (false)
		 */
		public AssignAddressesHandler(List addressPks, boolean addOrRemove) {
			this.addOrRemove = addOrRemove;
			this.addressPks = addressPks;
		}

		public void cancel() {
			closeCategoryTree();
		}

		/**
		 * @param subCategories
		 *            List of subcategories to add or remove
		 * @param listToIgnore
		 *            this is just a dummy list to fullfill the conditions of
		 *            CategoryTree.AssignHandler
		 */
		public void submit(List subCategories, List _) {
			List subCategoryPks = asPkList(subCategories);

			AssignAddressesTask assignAddressesTask = new AssignAddressesTask(
					addressPks, addOrRemove ? subCategoryPks : null,
					addOrRemove ? null : subCategoryPks, categoryTreeDialog);

			TaskManager
					.getInstance()
					.register(
							assignAddressesTask,
							Messages
									.getString((addOrRemove ? "GUI_AssignOrDeassignAddressAction_Add"
											: "GUI_AssignOrDeassignAddressAction_Remove")),
							false);
			hideCategoryTree();
		}
	}

	protected class MoveAddressesHandler implements CategoryTree.AssignHandler {

		private List addressPks;

		/**
		 * @param addressPks
		 *            List with pks of all addresses on which the operation will
		 *            be executed
		 * @param addOrRemove
		 *            flag that says if this is an add-operation (true) or a
		 *            remove-operation (false)
		 */
		
		public MoveAddressesHandler(List addressPks) {
			this.addressPks = addressPks;
		}

		public void cancel() {
			closeCategoryTree();
		}

		public void submit(List addSubCats, List removeSubCats) {
			
			if (addSubCats == null || addSubCats.size() == 0 || removeSubCats == null || removeSubCats.size() == 0){
				JOptionPane.showMessageDialog(null, Messages.getString("GUI_AssignOrDeassignAddressAction_MoveAddresses_WARNING"));
				return;
			}
				
			AssignAddressesTask assignAddressesTask = new AssignAddressesTask(
					addressPks, asPkList(addSubCats), asPkList(removeSubCats), categoryTreeDialog);
			TaskManager
					.getInstance()
					.register(
							assignAddressesTask,
							Messages	.getString("GUI_AssignOrDeassignAddressAction_Move"),
							false);
			hideCategoryTree();
		}
	}
}