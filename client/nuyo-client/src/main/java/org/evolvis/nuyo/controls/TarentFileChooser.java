/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai Ruether. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controls;

import java.awt.Component;
import java.io.File;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

/*
 * Created on 08.08.2003
 * 
 */

/**
 * @author niko
 * 
 */
public class TarentFileChooser extends JFileChooser {
	private JDialog m_oDialog;

	private int m_iReturnValue = 0;

	private String m_sStartFolder;

	private Component m_oParentComponent;

	public TarentFileChooser() {
		this("");
	}

	public TarentFileChooser(String startfolder) {
		m_sStartFolder = startfolder;
	}

	private void init() {
		m_oDialog = createDialog(m_oParentComponent);
		setCurrentDirectory(new File(m_sStartFolder));

		m_oDialog.setModal(true);
	}

	public int showOpenDialog(JFrame parent) {
		m_iReturnValue = 0;
		m_oParentComponent = parent;
		init();
		m_oDialog.setVisible(true);
		return (m_iReturnValue);
	}

	public int showSaveDialog(JDialog parent) {
		m_iReturnValue = 0;
		m_oParentComponent = parent;
		init();
		this.setApproveButtonText("Speichern");
		m_oDialog.setVisible(true);
		return (m_iReturnValue);
	}

	/**
	 * Overridden to automatically close dialog upon approval.
	 */
	public void approveSelection() {
		m_iReturnValue = JFileChooser.APPROVE_OPTION;
		m_oDialog.setVisible(false);
	}

	/**
	 * Overridden to automatically close dialog upon cancellation.
	 */
	public void cancelSelection() {
		m_iReturnValue = JFileChooser.CANCEL_OPTION;
		m_oDialog.setVisible(false);
	}

}
