/* $Id: ContactException.java,v 1.3 2006/10/01 06:05:29 aleksej Exp $
 *
 * Created on 13.05.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

/**
 * Diese Klasse stellt Contact-Client-interne Ausnahmen dar.
 *  
 * @author niko
 */
public class ContactException extends Exception
{
  public static final int CALENDAR_SELECTION_MODEL_NOT_INITIALIZED = 0;

/*
   * Getter und Setter
   */
  public int getExceptionId() {
    return exceptionId;
  }
  
  /*
   * Konstruktoren
   */
  /**
   * Dieser Konstruktor �bernimmt nur eine Exception-Id. 
   */
  public ContactException(int exId)
  {
    super();
    exceptionId = exId;
  }

  /**
   * Dieser Konstruktor �bernimmt eine Exception-Id und einen Fehlertext. 
   */
  public ContactException(int exId, String arg0)
  {
    super(arg0);
    exceptionId = exId;
  }

  /**
   * Dieser Konstruktor �bernimmt eine Exception-Id, einen Fehlertext und
   * eine initiale Ausnahme. 
   */
  public ContactException(int exId, String arg0, Throwable arg1)
  {
    super(arg0, arg1);
    exceptionId = exId;
  }

  /**
   * Dieser Konstruktor �bernimmt eine Exception-Id und
   * eine initiale Ausnahme. 
   */
  public ContactException(int exId, Throwable arg0)
  {
    super(arg0);
    exceptionId = exId;
  }

  /*
   * gesch�tzte Member-Variablen
   */
  int exceptionId = 0;
}
