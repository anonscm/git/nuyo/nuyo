/* $Id: AppointmentReminder.java,v 1.4 2006/10/27 13:21:53 aleksej Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

import java.util.Date;

/**
 * Diese Schnittstelle stellt Terminerinnerungen dar.
 * 
 * @author mikel
 */
public interface AppointmentReminder {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();

    //
    // Attribute
    //
    /** Der Termin, an den erinnert werden soll */
    public Appointment getAppointment() throws ContactDBException;
    
    /** Erinnerungskanal, vgl Konstanten <code>CHANNEL_*</code> */
    public int getChannel() throws ContactDBException;
    public void setChannel(int newChannel) throws ContactDBException;

    /** TimeOffset */
    public int getTimeOffset() throws ContactDBException;
    public void setTimeOffset(int timeOffset) throws ContactDBException;
    
    /** TimeUnit */
    public int getTimeUnit() throws ContactDBException;
    public void setTimeUnit(int timeUnit) throws ContactDBException;

    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    /** Erinnerungszeit */
    public Date getRuntime() throws ContactDBException;
    public void setRuntime(Date newRuntime) throws ContactDBException;

    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Erinnerungsadresse (E-Mail,...), Notiz */
    public final static String KEY_TARGET = "target";
    public final static String KEY_NOTE = "note";
    public final static String KEY_EMAIL = "email";

    /** Kan�le: Keiner, akustisch */
    public final static int CHANNEL_NONE = 0;
   // public final static int CHANNEL_AUDIBLE = 1; Comment: removed for LTB Release
    public final static int CHANNEL_OPTICAL = 2;
   // public final static int CHANNEL_MAIL = 3;
   // public final static int CHANNEL_SMS = 4;
    
}
