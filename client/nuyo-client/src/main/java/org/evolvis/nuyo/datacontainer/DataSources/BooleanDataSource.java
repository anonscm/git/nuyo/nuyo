/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.DataSources;

import java.util.Iterator;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSource;
import org.evolvis.nuyo.datacontainer.DataSourceInterfaces.DataSourceAdapter;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author niko
 *
 */
public class BooleanDataSource extends DataSourceAdapter implements TarentGUIEventListener
{
  public String getListenerName()
  {
    return "BooleanDataSource";
  }  

  public DataSource cloneDataSource()
  {
    BooleanDataSource datasource = new BooleanDataSource();
    
    datasource.m_oDataContainer = m_oDataContainer;
    datasource.m_sDBKey = m_sDBKey ;

    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      datasource.m_oParameterList.add(((ObjectParameter)(it.next())).cloneObjectParameter());
    }
    
    return datasource;
  }
  
  public boolean canHandle(Class data)
  {
    return data.isInstance(Boolean.class);
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.DATASOURCE, "BooleanDataSource", new DataContainerVersion(0));
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    return false;
  }

  public boolean readData(PluginData plugindata)
  {
    Object newdata = plugindata.get(getDBKey()); 
    getDataContainer().setGUIData(newdata);    
    return (newdata != null);
  }

  public boolean writeData(PluginData plugindata)
  {
    if (getDataContainer().isValid())
    {
      Object data = getDataContainer().getDataStorage().getData();
      return plugindata.set(getDBKey(), data);
    }
    else
    {
      System.err.println("ERROR unable to store data \"" + getDBKey() + "\" to " + plugindata + ": data not valid!");    
    }
    return false;
  }
  
}
