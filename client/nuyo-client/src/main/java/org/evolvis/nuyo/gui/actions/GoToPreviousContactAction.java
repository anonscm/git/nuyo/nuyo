package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;

import de.tarent.commons.datahandling.binding.AbstractReadOnlyBinding;
import de.tarent.commons.datahandling.binding.Binding;

public class GoToPreviousContactAction extends AbstractBindingRestrictedAction {

    private static final long serialVersionUID = 6018305727110049965L;
    
	protected Binding[] initBindings()
	{
		// A binding that listens on changes to the currently selected address
		// in order to check if this action makes sense (it does not,
		// if the address-index is '0')
		return new Binding[] { new AbstractReadOnlyBinding(
				ApplicationModel.SELECTED_ADDRESS_INDEX_KEY) {

			public void setViewData(Object arg) {
				checkState();
			}

		}, new AbstractReadOnlyBinding(
				ApplicationModel.ADDRESS_LIST_KEY) {

			public void setViewData(Object arg) {
				checkState();
			}
		}};
	}
	
	protected void checkState() {
		
		// check if null or dummy-object
		// note: dummy object means 'nothing selected'
		// if nothing selected application model returns a dummy object and the selected address index is thereby '0'.  
		boolean isDummyObject = ApplicationServices.getInstance().getApplicationModel().getAddresses().getSize() == 0;

		if (isDummyObject || ApplicationServices.getInstance().getApplicationModel().getSelectedAddressIndex() > 0)
			setBindingEnabled(true);
		else
			setBindingEnabled(false);
		updateActionEnabledState();
	
	}

    public void actionPerformed(ActionEvent e) {
        ApplicationServices.getInstance().getActionManager().userRequestPrevAddress();
    }
}