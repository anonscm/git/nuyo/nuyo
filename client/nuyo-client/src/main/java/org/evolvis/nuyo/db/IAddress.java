package org.evolvis.nuyo.db;

import java.util.Collection;

/**
 * @author kleinw
 *
 *	Repr�sentiert eine Adresse.
 *
 */
public interface IAddress {

    /**	Eine der Adresse zugeordneten User beziehen */
    public User getAssociatedUser();
    /**	Einer Adresse einen User zuordnen.@param user - der zu zuordnende User */
    public void associateUser(User user);
    
    
    /**	Alle Unterkategorien der Adresse beziehen */
    public Collection getSubCategories();
    /**	Adresse einer Unterkategorie zuweisen.@param category - die zuzuweisende Kategorie */
    public void add(SubCategory category);
    
    
    /** Alle Kategorien, in denen die Adresse entalten ist */
    public Collection getCategories();
    /**	Adresse einer weiteren Kategorie zuorden.@param category - die Kategorie */
    public void addCategory(Category category);
    
    
    /**	Kurzbezeichner f�r diese Adresse beziehen */
    public String getShortAddressLabel();
    
    
    /** Alle Ereignisse holen, an diese Adresse beteiligt war */
    public Collection getHistory();
    
}
