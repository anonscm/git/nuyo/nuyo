/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.orders;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;

import de.tarent.commons.plugin.Plugin;
import de.tarent.commons.ui.EscapeDialog;

/**
 * Offers a mail ordering funcionality.
 * You can get an interface, JPanel or JDialog to manage the mail orders.
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class MailOrdersPlugin implements Plugin {
    
    public static final String ID = "mail-orders";

    private static final TarentLogger logger = new TarentLogger(MailOrdersPlugin.class);
    private JDialog dialog;
    private JPanel panel;
    private DispatchPanel dispatchPanel;

    public void CalenderPlugin(){
        logger.info( getClass().getName() + " enabled.");
    }

    public String getID() {
        return ID;
    }

    public void init() {
    }

    public Object getImplementationFor( Class aClass ) {
        if (MailOrdersManager.class == aClass) return getMailOrdersManager(); 
        if (JPanel.class == aClass) return getPanel();
        if (JDialog.class == aClass) return getDialog();
        return null;
    }

    private DispatchPanel getMailOrdersManager() {
        if(dispatchPanel == null){
            dispatchPanel = new DispatchPanel(ApplicationServices.getInstance().getActionManager(), ApplicationServices.getInstance().getMainFrame());
        }
        return dispatchPanel;
    }

    private JDialog getDialog() {
        //TODO: still not completed dialog
        //BUG to be fixed: 
        // can't create new order ater one has been created or canceled
        // because search filter will be activated or something like this
        //BUG-workaround: disable filter, that is show all contacts
        //WARNING: workaround is not acceptable (customized search filter should remain)
        
        //to begin see: MailBatchAllDialog

        if(dialog == null) {
            dialog = new EscapeDialog( ApplicationServices.getInstance().getActionManager().getFrame(), 
                                  Messages.getString( "GUI_Mail_Order_Dialog_Title" ) );
            dialog.getContentPane().add( getPanel() );
            dialog.pack();
            dialog.setModal(true);
            dialog.setSize( 800, 500 );
            dialog.setLocationRelativeTo( null );
        }
        return dialog;
    }

    private JPanel getPanel() {
        if(panel == null){
            panel = new JPanel();  
            panel.setLayout(new BorderLayout());
            panel.setBorder(new EmptyBorder(2, 2, 2, 2));
            dispatchPanel = getMailOrdersManager();
            panel.add(dispatchPanel, BorderLayout.CENTER);
            panel.add(Box.createVerticalStrut(20), BorderLayout.SOUTH);
        }
        return panel;
    }

    public List getSupportedTypes() {
        List supportedTypes = new ArrayList();
        supportedTypes.add(MailOrdersManager.class);
        supportedTypes.add(JPanel.class);
        supportedTypes.add(JDialog.class);
        return supportedTypes;
    }

    public boolean isTypeSupported( Class aClass ) {
        return (aClass == MailOrdersManager.class) 
        || (aClass == JDialog.class) 
        || (aClass == JPanel.class);
    }

	public String getDisplayName() {
		return "Mail Orders";
	}
}