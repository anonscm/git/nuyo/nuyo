/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class FaxDienstlichField extends GenericTextField
{
  public FaxDienstlichField()
  {
    super("FAXDIENST", AddressKeys.FAXDIENST, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Fax_dienstlich_ToolTip", "GUI_MainFrameNewStyle_Standard_Fax_dienstlich", 30);
  }
}
