package org.evolvis.nuyo.selector.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.selector.EntityView;
import org.evolvis.xana.action.AbstractGUIAction;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.ui.EscapeDialog;

public class TestHtmlAction extends AbstractGUIAction implements AddressListConsumer {

	private AddressListProvider provider;
	private static Logger logger = Logger.getLogger(DetailViewAction.class.getName());
	private String templateURL;
	private EntityList entities;
	private JButton loadButton;
	EntityView view;
	JTextField urlField = new JTextField();

	public void registerDataProvider(AddressListProvider provider) {
		this.provider=provider;		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==loadButton){
			try {
				templateURL = new File(urlField.getText()).toURL().toString();
				view.load(templateURL);
			} catch (MalformedURLException e1) {
				logger.severe("kann url nicht laden: "+urlField.getText());
			}
		} else {
			String templateAttribute = (String)this.getValue("templateName");
			templateURL = (this.getClass().getResource(templateAttribute)).getFile();
			JDialog dialog;
			if (provider!=null){
				entities = provider.getAddresses();
					view = new EntityView(templateURL,entities);
					dialog = buildDialog(view.getComponent());
				dialog.setSize(800,600);
				dialog.setVisible(true);
			} else logger.info("Kein Daten-Provider vorhanden");
		}
	}

	private JPanel buildErrorPanel(String message){
		JPanel panel = new JPanel();
		JLabel label = new JLabel(message);
		panel.add(label);
		return panel;
	}

	private JPanel buildReloadPanel(){
		loadButton = new JButton("Load");
		loadButton.addActionListener(this);
		urlField.setText(templateURL);
		FormLayout layout = new FormLayout(
				"pref:GROW,3dlu,pref", //columns 
		"fill:pref");// rows
		PanelBuilder builder = new PanelBuilder(layout);
		CellConstraints cc = new CellConstraints();

		builder.setDefaultDialogBorder();	
		builder.add(urlField, cc.xy(1,1));
		builder.add(loadButton, cc.xy(3,1));
		return builder.getPanel();
	}
	//TODO: setAllwaysOnTop as soon as java 5 is allowed
	public JDialog buildDialog(JPanel panel){
		//reload-panel:
		JPanel reloadPanel=buildReloadPanel();

		FormLayout layout = new FormLayout(
				"pref:GROW", //columns 
		"pref,3dlu,fill:pref:GROW");// rows
		PanelBuilder builder = new PanelBuilder(layout);
		CellConstraints cc = new CellConstraints();
		builder.setDefaultDialogBorder();	

		builder.add(reloadPanel, cc.xy(1,1));
		builder.add(panel, cc.xy(1,3));

		JDialog dialog = new EscapeDialog();
		dialog.getContentPane().add(builder.getPanel());
		dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		dialog.setTitle("Detailansicht");
		dialog.pack();
		dialog.setModal(true);
		return dialog;
	}

}
