/* $Id: Role.java,v 1.5 2007/04/26 19:47:32 aleksej Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;


/**
 *
 * Interface f�r eine Rolle
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public interface Role {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();
    
    //
    // Attribute
    //
    /** Name der Rolle */
    public String getRoleName();
	public void setRoleName(String name);
    
    /** Rechteattribute, vgl. Konstanten <code>KEY_*</code> */
	public Boolean hasRight(String key);
	public void setRight(String key, Boolean newRight);
    /**
     * Says whether a persistent role object can be deleted. 
     * @return <code>true</code> if persistent deletable or not yet persistent <code>false</code> otherwise.
     */    
    public boolean isDeletable();
    /** Says whether a role exists only locally, that means not persistent.*/
    public boolean isTransient();
    /** Says whether a local role object has other attribute values then persistent one.*/
    public boolean isDirty();

    //
    // Methoden
    //

    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Login-Name, Vorname, Nachname */
    public final static String KEY_ROLENAME = "rolename";
    
}
