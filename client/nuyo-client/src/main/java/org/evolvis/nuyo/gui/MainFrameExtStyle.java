/* $Id: MainFrameExtStyle.java,v 1.131 2007/08/30 16:10:24 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;

import javax.help.HelpBroker;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationModel;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ApplicationStarter;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.controller.event.ContextVetoListener;
import org.evolvis.nuyo.controller.event.DataChangeListener;
import org.evolvis.nuyo.controller.manager.AmRequestFacade;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Mail;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.veto.Veto;
import org.evolvis.nuyo.gui.action.journal.JournalEditor;
import org.evolvis.nuyo.gui.assignmentview.CategoryAssignmentViewer;
import org.evolvis.nuyo.gui.calendar.ScheduleData;
import org.evolvis.nuyo.gui.categorytree.CategoryTree;
import org.evolvis.nuyo.gui.fieldhelper.AddressField;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.plaf.TarentUIDefaultsModifier;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.calendar.TarentCalendar;
import org.evolvis.nuyo.plugins.mail.DispatchEMailPanel;
import org.evolvis.nuyo.plugins.mail.orders.MailOrdersManager;
import org.evolvis.nuyo.plugins.mail.orders.MailOrdersPlugin;
import org.evolvis.nuyo.security.SecurityManager;
import org.evolvis.nuyo.util.general.DataAccess;
import org.evolvis.nuyo.util.parser.GUIParseError;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.action.ContextChangeListener;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Appearance.Key;
import org.evolvis.xana.swing.MenuBar;
import org.evolvis.xana.swing.SideMenuBar;
import org.evolvis.xana.swing.ToolBar;
import org.evolvis.xana.utils.IconFactory;

import de.tarent.commons.datahandling.binding.BeanBinding;
import de.tarent.commons.plugin.Plugin;
import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;
import de.tarent.commons.utils.TaskManager;
import de.tarent.commons.utils.TaskManager.Blocker;

/**
 * This class represents the main application window.<br>
 * It manages the GUI elements only.<br> 
 * The application logic is stored in {@link org.evolvis.nuyo.controller.ActionManager ActionManager}.<br>
 * 
 * @author niko
 */
public class MainFrameExtStyle extends BaseFrame implements ControlListener, ContextVetoListener, DataChangeListener, ContextChangeListener, Blocker {

    private static final TarentLogger logger = new TarentLogger( MainFrameExtStyle.class );
    private static final String TABSET_DEFAULT = "TABSET_DEFAULT";
    
    private TabListener m_oTabListener = new TabListener();
    private GuiListenerDataAccess guiListenerDataAccess;
    private ToolBar toolBar;
    
    private MainFrameDragBar mainFrameDragBar;
    private AddressTableView contactsTable;
    
    private JournalEditor journalEditor;

    private int millisecondsPerTick = 1000;
    
    private boolean isToCheckWindowSizeOnStartup;

    // non user-definable values
    private int taskBarHeight = 50;

    // -------------- class-instances used by this class --------------

    // Der Actionmanager
    private GUIListener guiListener;

    // GUI-Optionen
    private boolean isToShowTerminPanel;

//    private boolean isSearch;
    // interne variablen
    private int currentIndex;
    private int NumOfEntries;
    private int sendGroupMenuIndexOffset;
    private Address displayedAddress;

    private int searchMode = GUIListener.ADDRESSSET_KEINFILTER;

    // ------------ GUI-Elemente von Bedeutung ;-) --------------

    // das Hauptfenster
    // the MainFrame is inherited of BaseFrame and can be accessed through getFrame()
    private JLayeredPane layeredPane;
    private JPanel overlayPanel;
    private OverlaySizeListener overlaySizeListener;

    private int windowStartupWidth = -1;
    private int oldNumOfEntries;
    private Component lastComponent;

    // die Panels f�r die Dateneingabe
    private JPanel inputPanel;
    private JPanel centerPanel;
    private JPanel mainFramePanel;
    private JSplitPane verticalSplitPane;
    private Map tabsMap = new HashMap();

    // die Tabulatoren
    private JTabbedPane tabbedPane;
    private JComponent contactTab;
    private JPanel mailOrderTab;
    private JComponent extendedTab;
    private JComponent categoryAssignmentViewTab;
    private JPanel appointmentsTab;

    // -----------------------------------------------------

    // in externe Klassen ausgelagerte Eingabeelemente
    private AddressPanel contactPanel;
    private AddressPanel extendedTopPanel;
    private TarentCalendar tarentCalendar;

    private MailOrdersManager mailOrdersManager;

    // -----------------------------------------------------

    // -- begin: used by menu -- - // SBM_REWORK remove everything
    private ImageIcon auswvertTitleIcon;
    // -- end: used by menu --

    private ImageIcon icon_tab_adresse;
    private ImageIcon icon_tab_extended;
    private ImageIcon icon_tab_protected;
    private ImageIcon icon_tab_versand;
    private ImageIcon icon_tab_termine;

    private ImageIcon icon_window;
    private ImageIcon icon_verteiler;
    private ImageIcon icon_verteilernone;

    // ========================================================================
    // noch in dieser Klasse verwaltete Eingabeelemente

    // Status Bar
    private StatusListener statusListener;

    // info panel oben
    private JPanel topPanel;

    private JPanel glassPanel;
    private ScheduleData scheduleData;
    private boolean wasTableOpenedAutomaticaly;
    private boolean shouldTableBeOpen;
    private PanelInserter panelInserter;
    private Map tabMap = new HashMap();

    private Map tableStatusMap = new HashMap();

    private boolean isToShowDragBarInitial;

    private Mail mail;
    private User user;

    /**
     * Method MainFrame. Der Konstruktor der MainFrame-Klasse.
     */
    public MainFrameExtStyle(GUIListener theGUIListener) {
        super();
        
    	// Adds and modifies some UI defaults for certain components used in the client.
        // This is done this late because doing it earlier would expand application startup time.
    	TarentUIDefaultsModifier.modifyUIDefaults();

        //register the MainFrameExtStyle at the ApplicationServices as CommonDialogServices:
        ApplicationServices.getInstance().setCommonDialogServices(this);
        //register to listen for context changes
        ActionRegistry.getInstance().addContextChangeListener(this);

        guiListener = theGUIListener;
        guiListenerDataAccess = new GuiListenerDataAccess(guiListener);

        // leider gibt es Unterschiede im Verhalten des Maximierens von Fenstern
        // zwischen Windows und Linux... daher diese Fallunterscheidung!
        isToCheckWindowSizeOnStartup = true;
        
        Appearance ape = ConfigManager.getAppearance();

        // aus Config-Datei holen ob AdminClient
        isToShowTerminPanel = ape.getAsBoolean(Key.SHOW_SCHEDULEPANEL, true);
        isToShowDragBarInitial = ape.getAsBoolean(Key.SHOW_DRAGBARINITIAL);

        List previewDBColumnsList = new ArrayList();
        List previewAdressColumnsList = new ArrayList();
        String sPreviewColumns = ape.get(Key.PREVIEW_COLUMNS);

        if (sPreviewColumns != null) {

            List list = guiListener.getAvailablePreviewRows();

            if (list != null) {

                String[] columns = sPreviewColumns.split(";");

                for (int i = 0; i < (columns.length); i++) {
                    String column = columns[i];
                    String[] keys = column.split("=");
                    if (list.contains(keys[0])) {
                        previewDBColumnsList.add(keys[0]);
                        previewAdressColumnsList.add(keys[1]);
                    } else {
                        guiListener.getLogger().config("Der Eintrag \"" + columns[i] + "\" ist kein g�ltiger Schlssel f�r eine Preview-Spalte.");
                    }
                }

                String[] saUsedDbColumns = new String[previewDBColumnsList.size()];
                String[] saUsedAdressColumns = new String[previewAdressColumnsList.size()];
                for (int i = 0; i < (previewDBColumnsList.size()); i++) {
                    saUsedDbColumns[i] = (String) (previewDBColumnsList.get(i));
                    saUsedAdressColumns[i] = (String) (previewAdressColumnsList.get(i));
                }
                guiListener.setPreviewColumnDbIDs(saUsedDbColumns);
                guiListener.setPreviewColumnAdressIDs(saUsedAdressColumns);
            }
        } else {
            guiListener.getLogger().config("Es wurde kein Konfigurationseintrag (\"PreviewColumns\") f�r die Preview-Tabelle gefunden. Es werden Vorgabe-Werte benutzt.");
        }
        
        // register this frame to block user input when a task requests this
        TaskManager.getInstance().setBlocker(this);
   }


    public DataAccess getDataAccess() {
        return guiListenerDataAccess;
    }

    public ScheduleData getScheduleData() {
        return (scheduleData);
    }


    public void selectAddress(int index) {
        contactsTable.selectAddress(index);

    }


    public int getNumEntries() {
        return NumOfEntries;
    }



    /**
     * Method init. Initialisiert das Hauptfenster und alle anderen Fenster/Dialoge. Alle erzeugten Fenster werden auf "invisible" gesetzt.
     */
    public void init() throws ContactGuiException {
        
        initIconsColorsFonts();

        GUIHelper.registerCursor(GUIHelper.CURSOR_NORMAL, null);
        GUIHelper.registerCursor(GUIHelper.CURSOR_HAND, new Cursor(Cursor.HAND_CURSOR));

        //contactsTable = new MainFrameAdvancedAdressTable(guiListener, this, tableHeaderFont, tableFont);
        contactsTable = new AddressTable();
        ApplicationServices.getInstance().getBindingManager()
            .addBinding(new BeanBinding(contactsTable, AddressTable.SELECTED_INDEX_KEY, ApplicationModel.SELECTED_ADDRESS_INDEX_KEY));

        ((AddressTable)contactsTable).addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent e)
			{
				// Synchronize the AddressTable-selection with other GUI-components (e.g. the address-panel)
				ApplicationServices.getInstance().getActionManager().userRequestSelectAddress(e.getFirstIndex());

                // synchronize the view, because a change of the address may be canceled my the action manager,
                // if the address was in edit mode and the users has choosen 'cancel'
                contactsTable.selectAddress(ApplicationServices.getInstance().getApplicationModel().getSelectedAddressIndex().intValue());
			}
        });
        
        try {
            
            createMainFrame();
            
        } catch (MalformedURLException mue) {
            guiListener.getLogger().log(Level.SEVERE, "Die Initialisierung des Fensters ist fehlgeschlagen. Vermutlich konnten die Icons nicht geladen werden.", mue);
            throw new ContactGuiException(ContactGuiException.EX_INIT_FAILED, "Die Initialisierung des Fensters ist fehlgeschlagen. Vermutlich konnten die Icons nicht geladen werden.");
        }

        // die hochziehbare Tabelle erzeugen...
        registerTableComponent("Adressen", contactsTable.getDragComponent());
        setTableView("Adressen");

        // ColumnPositions laden...
        String columnposis = ConfigManager.getAppearance().get(Key.TABLE_COLUMN_POSITIONS);
        if (columnposis != null && columnposis.length() > 0) {
                contactsTable.setColumnPositions(columnposis);
        }

        guiListener.addContextVetoListener(this);
        guiListener.addDataChangeListener(this);

        // ------------------- Kontakt zu Mail-Server aufnehmen -------------------
        user = guiListener.getUser(guiListener.getCurrentUser(), true);
        
        // ----------------------------------------------------------------------------------
        // TODO: Make something with Thread.join()
        panelInserter.startTimerTask((long) millisecondsPerTick);

        // Initialization of the menu.
        ActionRegistry registry = ActionRegistry.getInstance();
        
        MenuBar menuBar = new MenuBar(StartUpConstants.MAINFRAME_MENU_BAR_ID, PropertyResourceBundle.getBundle( StartUpConstants.MENU_BUNDLE_NAME, StartUpConstants.LOCALE));
        getFrame().setJMenuBar(menuBar);
        
        SideMenuBar smb = new SideMenuBar(StartUpConstants.MAINFRAME_SIDEMENU_BAR_ID);
        
        registry.addContainer(menuBar);
        registry.addContainer(smb);
        
        // Makes side menu bar scrollable.
        JScrollPane scrollPane = new JScrollPane(smb);
        
        // Disables an odd looking border around the menus.
        scrollPane.setBorder(null);
        
        // Allows making the visible space of the scrollpane smaller than
        // what is usually allowed (it normally reserves space for its
        // scrollbar buttons).
        scrollPane.setMinimumSize(new Dimension(0, 0));
        
        verticalSplitPane.setLeftComponent(scrollPane);
    }


    private void initIconsColorsFonts() {
        TypeToImageMapper.registerIcon(0, "Anruf", "anruf.gif");
        TypeToImageMapper.registerIcon(1, "Fraktionstermin", "fraktionstermin.gif");
        TypeToImageMapper.registerIcon(2, "Plenum", "plenum.gif");
        TypeToImageMapper.registerIcon(3, "Sitzung", "sitzung.gif");
        TypeToImageMapper.registerIcon(4, "Urlaub", "urlaub.gif");

        // GUIHelper mit Daten fttern...
        GUIHelper.registerIcon(GUIHelper.ICON_PERSON, "person.gif");
        //GUIHelper.registerIcon(GUIHelper.ICON_RESSOURCE, "ressource.gif");// not found
        GUIHelper.registerIcon(GUIHelper.ICON_SYSUSER, "sysuser.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_EXTUSER, "extuser.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_SEX_MALE, "male.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_SEX_FEMALE, "female.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_SEX_UNKNOWN, "unknown.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_SEX_FIRMA, "firma.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY, "admin_category.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_USER, "admin_user.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_EXTRAS, "admin_extras.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_CREATE, "admin_category.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_CREATE, "admin_subcategory_create.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_SHOW, "admin_subcategory_show.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_DELETE, "admin_subcategory_delete.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_EDIT, "admin_subcategory_show.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_SHOW, "admin_user_addcategory.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_DELETE, "admin_subcategory_delete.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_HIDE, "admin_user_deletecategory.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_STDCATEGORY_SET, "admin_user_setstdcategory.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_USER_CREATE, "admin_user_create.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_USER_DELETE, "admin_user_delete.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_USER_ASSOCIATE, "admin_user_setstdcategory.gif"); // TODO
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_USER_CHANGE, "admin_user_change.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_GROUP, "admin_group.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_GROUP_CREATE, "admin_group_create.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_GROUP_EDIT, "admin_group_edit.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_GROUP_DELETE, "admin_group_delete.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_ROLE, "admin_role.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_ROLE_GR, "admin_role_gr.gif");
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_ROLE_FR, "admin_role_fr.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CALENDAR_SHARE, "admin_calendar_share.gif");

        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_KONFIGURATOR, "konfigurator.gif");
        // TODO: richtiges Icon setzen
        GUIHelper.registerIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_EDIT, "admin_subcategory_show.gif");
    }

    /**
     * Method openGUI. �ffnet das Hauptfenster der Applikation.
     */
    public void openGUI() {

        guiListener.addContextVetoListener(this);
        guiListener.addDataChangeListener(this);
        getFrame().addComponentListener(new WindowShownListener());
        contactPanel.addComponentListener(new ComponentSizeListener());
        TarentLogger.setParentWindow(getFrame());
        //set drag bar position
        getDragBarPane().realized();
        getFrame().setVisible(true);
        // set divider-position in contact-panel
        contactPanel.setDividerPosition();
    }


    /** This method will be invoked after all visible components have been initialized. */ 
    public void realizedWindow() {
        if(!isToShowDragBarInitial) showTable(false);
        
        // allen Panels mitteilen das sie nun realisiert sind...
        panelInserter.postFinalRealize();
    }

    // die beiden folgenden Listener werden benutzt um im Adresspanel die Splitpane mittig zu setzen
    public class WindowShownListener extends ComponentAdapter {

        public void componentShown(ComponentEvent e) {
            getFrame().removeComponentListener(this);
            windowStartupWidth = contactPanel.getWidth();
        }
    }

    public class ComponentSizeListener extends ComponentAdapter {

        public void componentResized(ComponentEvent e) {
            if (windowStartupWidth != -1) {
                if (contactPanel.getWidth() != windowStartupWidth) {
                    contactPanel.removeComponentListener(this);
                    realizedWindow();
                }
            }
        }
    }


    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Method closeGUI. Schliesst das Hauptfenster der Applikation
     */
    public void closeGUI() {
        getFrame().setVisible(false);
        guiListener.removeDataChangeListener(this);
        guiListener.removeContextVetoListener(this);
    }

    /** Returns the selected status of the check box 'include common contacts' (ger. 'allgemeine Adressen einbeziehen').*/
    public boolean getAllgemeineAdressen() {
        logger.warningSilent("[!] status of check box 'include common contacts' unknown.");
        return false;
    }


    public void setAllgemeineAdressen(boolean isselected) {
    }


    /**
     * Method setWaiting. Setzt je nach Parameter den Mauspfeil auf "Sanduhr" bzw. "normal" sollte mit dem Parameter "true" aufgerufen werden bevor eine langwierige Operation
     * ausgefhrt wird. Nach Beendigung der Operation muss die Methode erneut aufgerufen werden, jedoch nun mit dem Parameter "false"
     * 
     * @param iswaiting
     * @deprecated Avoid for new code. Use TaskManager instead (which will invoke setBlocked)
     */

    public void setWaiting(boolean iswaiting) {
    
    	getFrame().setEnabled(!iswaiting);
    	
    	if(iswaiting)
    		getFrame().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    	else
    		getFrame().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }


    /**
     * Diese Methode teilt mit, dass sich die zugrundeliegende Sammlung von Adressen ge�ndert hat.
     * 
     * @param addresses die neue Adresssammlung.
     */
    public void addressesChanged(Addresses addresses) {
        NumOfEntries = addresses.getIndexMax() + 1;
        contactsTable.discardTableBuffer();
        contactsTable.setAddressList(addresses);

//         tarentSearch.setNamesOfSelectedVerteiler();
    }



    private void updateTab(Object container, boolean updateall, boolean overwrite) {
        panelInserter.updateTab(container, updateall, overwrite, displayedAddress, mail, user);
    }


    public Address getDisplayedAddress() {
        return displayedAddress;
    }


    public void setDisplayedAddress(Address address) {
        displayedAddress = address;
    }


    public void setAddress(Address address, boolean overwrite) throws ContactDBException {
        // boolean overwrite = false;
        if (displayedAddress == null)
            overwrite = true;

        displayedAddress = address;

        boolean isEditAllowed = isEditMode() || isNewMode(); 
        Component comp = tabbedPane.getSelectedComponent();
        if (comp != null) {
            if (comp.equals(contactTab) || isEditAllowed)
                updateTab(contactPanel, false, overwrite);
            if (comp.equals(extendedTab) || isEditAllowed)
                updateTab(extendedTopPanel, false, overwrite);
            if (comp.equals(tarentCalendar))
                updateTab(tarentCalendar, false, overwrite);
        }
        
        // Feststellen, ob Adresse bearbeitet werden darf, oder nicht...

        // Aktualisierung der Aktivierung von Tabs und Meneintr�gen.
        updateVisibleState(isEditAllowed);

        // zus�tzlich hier das Adressbezogene Bearbeiten
    }


    /**
     * Method getAddress. Diese Methode liest den Inhalt der GUI in ein Address-Objekt.
     * 
     * @param address
     * @throws ContactDBException
     */
    public void getAddress(Address address) throws ContactDBException {
        panelInserter.getAddress(address, displayedAddress);
    }


    /**
     * Method setEditable. Setzt die GUI-Elemente je nach bergebenem Parameter auf "editierbar" bzw. "nur lesen"
     * 
     * @param veto enth�lt ggfs Einschr�nkungen der vollen Editierbarkeit.
     * @see org.evolvis.nuyo.db.veto.Restriction
     */
    public void handleContextVeto(Veto veto) {
        // TODO: Veto verarbeiten
        panelInserter.setEditable(veto, isEditMode(), displayedAddress);
    }


    private void setButtonFilterOffEnabled(boolean isenabled) {
//         tarentSearch.setButtonFilterOffEnabled(isenabled);
        if (mailOrdersManager != null)
            mailOrdersManager.setButtonFilterOffEnabled(isenabled);
    }

    private void updateVisibleState(final boolean isEditAllowed) {
        //TODO: palij: fire handleContextVeto where required 
        panelInserter.setEditable(null,//no veto: it will be handled later in handleContextVeto(Veto)  
                                  isEditAllowed, displayedAddress);
        
        if (isEditAllowed) {// is editable
            
            // disable seach filter button
            setButtonFilterOffEnabled(false);

            //select contact TAB if not selected
            int selectedtab = tabbedPane.getSelectedIndex();
            if ((selectedtab != getTab(TAB_CONTACT).getIndex(tabbedPane)) 
                && (selectedtab != (getTab(TAB_EXTENDED).getIndex(tabbedPane)))
                && (selectedtab != (getTab(TAB_CATEGORY_ASSIGNMENT_VIEW).getIndex(tabbedPane)))) {
                tabbedPane.setSelectedIndex((getTab(TAB_CONTACT).getIndex(tabbedPane)));
            }

            setTabsVisibility(false);

            if (isEditMode()) {
                // im EDIT-Mode
                //check rights to edit
                if (guiListener.userRequestUserHasRightOnAddress(SecurityManager.FOLDERRIGHT_EDIT)) {
                    // User darf editieren
                    setMenuEditableAware(true);
                } else {
                    // User darf nicht editieren
                    setMenuEditableAware(false);
                    
                    // Ist User auf einem Tab, wo er nicht sein darf?
                    int index = tabbedPane.getSelectedIndex();
                    if (index == getTab(TAB_CONTACT).getIndex(tabbedPane) 
                        || index == getTab(TAB_EXTENDED).getIndex(tabbedPane)) {
                        tabbedPane.setSelectedIndex((getTab(TAB_CONTACT).getIndex(tabbedPane)));
                    }
                }
            } else {
                // im NEW-Mode
            }
        } else {// is NOT editable
            
            setButtonFilterOffEnabled(isSearchMode());
            
            Integer categoryid = null;
    		
            int indexOfCategory = guiListener.getSelectedCategory();
    			if (indexOfCategory >= 0){
    				categoryid = ((Category)guiListener.getCategoriesObjectList().get(indexOfCategory)).getIdAsInteger();
    				logger.fine( "Kategorie " + categoryid );
    			}
    		
    	    
            // Bearbeiten erstmal aktivieren.
            // Evtl. wird es sp�ter adressabh�ngig wieder deaktiviert.
            setTabsVisibility(true);
            // Tabs wieder enablen, die wegen Rechten disabled waren
            setMenuEditableAware(true);
        }
    }


    private boolean isEditMode() {
        return ActionRegistry.getInstance().isContextEnabled(StartUpConstants.EDIT_MODE);
    }

    private boolean isSearchMode() {
        return ActionRegistry.getInstance().isContextEnabled(StartUpConstants.SEARCH_MODE);
    }
    
    private boolean isNewMode() {
        return ActionRegistry.getInstance().isContextEnabled(StartUpConstants.NEW_MODE);
    }
    

    private void setMenuEditableAware(boolean value) {
        try {
            tabbedPane.setEnabledAt((getTab(TAB_CONTACT).getIndex(tabbedPane)), value);
            tabbedPane.setEnabledAt((getTab(TAB_EXTENDED).getIndex(tabbedPane)), value);
        }
        catch ( IndexOutOfBoundsException e ) {
            logger.warning("Failed setting some tabs "+ (value ? "enabled" : "disable"),"Invalid tab index: " + e.getMessage());
        }
    }


    private void setTabsVisibility(boolean value) {
        if ((getTab(TAB_MAIL_ORDERS) != null))
            if ((getTab(TAB_MAIL_ORDERS).getIndex(tabbedPane) != -1))
                tabbedPane.setEnabledAt((getTab(TAB_MAIL_ORDERS).getIndex(tabbedPane)), value);
    }

    /**
     * Selects a given tab if it's enabled.
     * 
     * @param key gibt das zu aktivierende Tab an (TAB_CONTACT, TAB_EMAIL, TAB_EMAIL_RECEPTION, TAB_PROTECTED, TAB_EXTENDED, TAB_ADMIN,
     *            TAB_MAIL_ORDERS, TAB_JOURNAL, TAB_ORGANIZER)
     * 
     * @exception NullPointerException if key is invalid not registered            
     */
    public void activateTab(String key) {
        if(!tabbedPane.isEnabledAt((getTab(key).getIndex(tabbedPane)))) return;
        tabbedPane.setSelectedIndex((getTab(key).getIndex(tabbedPane)));
    }

    
    
    /** 
     * Returns true if a given tab is enabled.
     * 
     * @param key of a given tab
     *  
     * @exception NullPointerException if key is invalid not registered
     */
    public boolean isTabEnabled(String key){
        return tabbedPane.isEnabledAt((getTab(key).getIndex(tabbedPane)));
    }

    // ---------------------------------------------------------------------------------------------------------

    /**
     * Method isDirty. �berprft ob sich der Inhalt der GUI-Elemente gegenber dem bergebenen Address-Objekt ge�ndert hat
     * 
     * @param address (Das Address-Objekt zur Gegenprfung)
     * @return boolean (ist true wenn sich etwas ge�ndert hat)
     * @throws ContactDBException
     */
    public boolean isDirty(Address address) throws ContactDBException {
        return panelInserter.isDirty(address);
    }


    // ---------------------------------------------------------------------------------------------------------
    /**
     * Method setNavigationCounter. Erm�glicht das Setzen des aktuellen Datensatzes und der Anzahl angezeigter Datens�tze (Navigationsleiste)
     * 
     * @param currentindex (Der Index des aktuell angezeigten Datensatzes)
     * @param numentries (Die Anzahl aller zur Navigation bereitstehenden Datens�tze)
     */
    public void setNavigationCounter(int currentindex, int numentries) {        
        ApplicationModel model = ApplicationServices.getInstance().getApplicationModel();
        //        model.setSelectedAddressCount(new Integer(numentries));
        //model.setSelectedAddressIndex(new Integer(currentindex));
    }
    
    
    /**
     * Method getNavigationCounterCurrentIndex. Erm�glicht die Abfrage des aktuell angezeigten Datensatzes
     * 
     * @return int (Der Index des aktuell angezeigten Datensatzes)
     */
    public int getNavigationCounterCurrentIndex() {
        return (currentIndex);
    }


    /**
     * Method getNavigationCounterNumEntries. Erm�glicht die Abfrage der Anzahl von Datens�tzen
     * 
     * @return int
     */
    public int getNavigationCounterNumEntries() {
        return (NumOfEntries);
    }


    public void categoryChanged(Category newCategory) {
        try {
            // Rechte auf Kategorie holen
            ObjectRole orole = user.getCategoryRights(newCategory.getIdAsString());
            if (!orole.hasRightAdd() || newCategory.getVirtual().booleanValue()) {
                // "neu"-Button grau
            }
        } catch (ContactDBException e) {
            // Default
            //disableVirtualCategoryMode();
        }
    }


    public void showVeto(String caption, Veto veto) {
        publishError(veto.getMessage(), veto.getDetail());
        // TODO: showVeto
    }


    public PanelInserter getPanelInserter() {
        return panelInserter;
    }

    // ----------------------------------------------------------------
    public class SizeRestrictedDocument extends PlainDocument {

        private int m_iMaxChars;


        public SizeRestrictedDocument(int maxchars) {
            m_iMaxChars = maxchars;
        }


        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if (str == null)
                return;
            int insertsize = str.length();
            int docsize = this.getLength();
            if ((insertsize + docsize) < m_iMaxChars) {
                super.insertString(offs, str, a);
            } else {
                int maxinsert = m_iMaxChars - docsize;
                if (maxinsert > 0) {
                    super.insertString(offs, str.substring(0, maxinsert), a);
                }
            }
        }
    }


    public void setCaption(String title) {
        getFrame().setTitle(title);
    }




    private void readIcons() throws MalformedURLException {
        logger.info("reading icons...");
        IconFactory factory = ApplicationServices.getInstance().getIconFactory();

        auswvertTitleIcon = (ImageIcon)factory.getIcon("title_auswvert.gif"); //$NON-NLS-1$

        icon_tab_adresse = (ImageIcon)factory.getIcon("tab_adresse.gif"); //$NON-NLS-1$
        icon_tab_extended = (ImageIcon)factory.getIcon("tab_extended.gif"); //$NON-NLS-1$
        icon_tab_protected = (ImageIcon)factory.getIcon("tab_protected.gif"); //$NON-NLS-1$
        icon_tab_versand = (ImageIcon)factory.getIcon("serial-dispatch.png"); //$NON-NLS-1$
        icon_tab_termine = (ImageIcon)factory.getIcon("office-calendar.png");

        icon_window = (ImageIcon)factory.getIcon("nuyo.png"); //$NON-NLS-1$
        icon_verteiler = (ImageIcon)factory.getIcon("tab_category_assignment.gif"); //$NON-NLS-1$
        icon_verteilernone = (ImageIcon)factory.getIcon("verteiler_none.gif"); //$NON-NLS-1$
        
    }

    // ---------------------------------------------------------------------------------------------------------


    public void showContactEntryDialog(String subject, String note, Date datum, int duration, boolean incoming, int category, int channel, int link, int linkType, User user, Address address,
                                       boolean readonly) {
        if (datum == null)
            datum = new Date();
        if (subject == null)
            subject = new String();
        if (note == null)
            note = new String();
        if (user == null)
            user = guiListener.getUser(guiListener.getCurrentUser());
        if (address == null)
            address = guiListener.getAddress();

        // Creates the journal editor on first use
        if (journalEditor == null)
        	journalEditor = new JournalEditor(getFrame());
        
        // Initializes it with the given properties
        journalEditor.setup(subject, note, datum, duration, incoming, category, channel);
        
        // and displays it. This blocks other operations and is self-contained.
        // That means OK-cancel-handling is done inside it already.) 
        journalEditor.show();
    }

    // ---------------------------------------------------------------

    private Object getTabKey(JComponent panel) {
        if (panel == null) {
            logger.warningSilent("[!] can't find tab key for JPanel: NULL");
            return null;
        }
        if (panel.equals(contactTab))
            return TAB_CONTACT;
        if (panel.equals(extendedTab))
            return TAB_EXTENDED;
        if (panel.equals(appointmentsTab))
            return TAB_ORGANIZER;
        if (panel.equals(mailOrderTab))
            return TAB_MAIL_ORDERS;
        if (panel.equals(categoryAssignmentViewTab))
            return TAB_CATEGORY_ASSIGNMENT_VIEW;
            
        return null;
    }

    private interface TabAction {

        public void activatedTab();
    }

    private class TabStatus {

        private String tableView;
        private boolean isTableVisible;
        private boolean isTableToOpenAutomatically;
        private ContainerProvider containerProvider;
        private TabAction tabAction;
        private String helpKey;


        public TabStatus(String tableview, boolean visible, boolean autoopen, ContainerProvider provider, TabAction tabaction, String helpkey) {
            tableView = tableview;
            isTableVisible = visible;
            isTableToOpenAutomatically = autoopen;
            containerProvider = provider;
            tabAction = tabaction;
            helpKey = helpkey;
        }


        public void action() {
            setTableView(tableView);

            panelInserter.setVisibleContainerProvider(containerProvider);

            if (containerProvider != null) {
                updateTab(containerProvider, true, false);
            }

            // perform special action...
            if (tabAction != null)
                tabAction.activatedTab();

            // set HelpKey...
            if (helpKey != null) {
                initHelpKey();
            }

            if (wasTableOpenedAutomaticaly && (!getDragBarPane().getTableWasMoved())) {
                guiListener.userRequestShowTable(false);
                wasTableOpenedAutomaticaly = false;
            }

            if (isTableToOpenAutomatically) {
                if (!getDragBarPane().isTableVisible()) {
                    guiListener.userRequestShowTable(true);
                    wasTableOpenedAutomaticaly = true;
                    getDragBarPane().setTableWasMoved(false);
                }
            } else {// to open not automatically
                if (isTableVisible) {
                    if (!(getDragBarPane().isTableVisible()) && (shouldTableBeOpen)) {
                        // Tabelle wird angezeigt
                        guiListener.userRequestShowTable(true);

                        shouldTableBeOpen = false;
                    }
                } else {
                    // Tabelle wird versteckt
                    if (getDragBarPane().isTableVisible()) {

                        shouldTableBeOpen = true;
                    }
                    guiListener.userRequestShowTable(false);

                }
            }
        }


        private void initHelpKey() {
            HelpBroker myHelpBroker = ApplicationServices.getInstance().getHelpBroker();
            if(myHelpBroker != null) {
                myHelpBroker.enableHelpKey(getFrame().getRootPane(), helpKey, myHelpBroker.getHelpSet());
            } else {
                logger.warning("[!] [MainFrameExtStyle]: couldn't set tab help key: help broker doesn't exist");
            }
        }
    }

    private void initTabStatusList() {
        tableStatusMap.put(TAB_CONTACT, new TabStatus("Adressen", true, false, contactPanel, null, "bertarentcontact.htm"));
        tableStatusMap.put(TAB_EXTENDED, new TabStatus("Adressen", true, false, extendedTopPanel, null, "datendereinzelnenadresse.htm"));
        tableStatusMap.put(TAB_MAIL_ORDERS, new TabStatus("VersandAuftraege", true, true, null, null, "officeintegration.htm"));
        tableStatusMap.put(TAB_ORGANIZER, new TabStatus("Adressen", false, false, tarentCalendar, null, null));
        // TODO: TableStatusMap provides a mean to handle component value updates and some address table
        // stuff. The component update mechanism is done through AdvancedPanelInserter which is clumsy
        // and prone to errors. When the category assignment view ("Kategoriezuordnung") tab was updated
        // the new binding API was used instead and therefore the AdvancedPanelInserter mechanism is not needed for it.
    }

    public class TabListener implements ChangeListener {

        public void stateChanged(ChangeEvent e) {
            Object tabkey = getTabKey((JComponent) (tabbedPane.getSelectedComponent()));
            if (tabkey != null) {
                TabStatus entry = (TabStatus) (tableStatusMap.get(tabkey));
                if (entry != null) {
                    entry.action();
                } else {
                	// Ignores missing entry for category assignment view tab.
                	if (!tabkey.equals(TAB_CATEGORY_ASSIGNMENT_VIEW))
                      guiListener.getLogger().severe("trying to refresh view for unregistered tab \"" + tabkey + "\"");
                }
            } else logger.warning("[TabListener]: Can't get selected tab: NULL");
        }
    }

    /* This method creates all tabs and fills its' inner panels with date. */ 
    private void initMainFrameContent() {
        logger.info("initializing frame content...");

        if (ConfigManager.getAppearance().getAsBoolean(org.evolvis.xana.config.Appearance.Key.SHOW_SCHEDULEPANEL, true))
        		initScheduleData();

        getFrame().setTitle(Messages.getString("GUI_MainFrameNewStyle_Titel_tarent-contact"));
        getFrame().getContentPane().setLayout(new BorderLayout());

        //root-pane
        createContentPane();
        //south
        createStatusBar();
        //north
        createTopPanel();
        //center
        createLayeredCenterPane();
        initializeTabRegistry();
        activateRegisteredTabs();
        initTabStatusList();
        addTabsAndTableToLayeredPane();
        // load data...
        fillGUIwithContent();
        setFieldsDoubleCheckSensitive();        
    }


    private void fillGUIwithContent() {
        panelInserter = new AdvancedPanelInserter(guiListener, this);
        panelInserter.addContainers(contactPanel);
        panelInserter.addContainers(extendedTopPanel);
        if (isToShowTerminPanel){
            panelInserter.addContainers(tarentCalendar);
        } else logger.info("[!] calender panel won't be shown");

        List errors = panelInserter.layoutElements();
        
        if (errors.size() > 0) {
            String errorlist = "";
            for (int i = 0; i < (errors.size()); i++) {
                GUIParseError err = (GUIParseError) (errors.get(i));
                errorlist += (err.toString() + "\n");
            }
            showExtendedMessage("Beim Einlesen der GUI-Beschreibung aus der Konfigurationsdatei sind Fehler aufgetreten.", errorlist);
        }

        // allen Fields mit CONTEXT_USER den aktuellen User vermitteln...
        panelInserter.setUser(guiListener.getUser(guiListener.getCurrentUser(), true));
        
    }
    
    /**
     * this method takes a list of keys for GUI components from a config file
     * and sets all corresponding GUI fields sensitive to automatic check for dublicates
     * so that the dubcheck will be trigged as soon as the field losts focus and its 
     * content has been changed   
     */
    
    private void setFieldsDoubleCheckSensitive(){
    	
    	if (panelInserter == null)
    		return;
    	StringTokenizer strTok = new StringTokenizer(ConfigManager.getAppearance().get(Key.DUBCHECKSENTIVEFIELDS));
    	while (strTok.hasMoreTokens()){
    		AddressField field = panelInserter.getAdressField(strTok.nextToken());
    		if (field != null){
    			field.setDoubleCheckSensitive(true);
    		}
    	}
    }


    private void addTabsAndTableToLayeredPane() {
        centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());
        centerPanel.add(tabbedPane, BorderLayout.CENTER); //$NON-NLS-1$

        Dimension panelsize = new Dimension(500, 400);
        centerPanel.setMinimumSize(panelsize);
        centerPanel.setMaximumSize(panelsize);
        centerPanel.setPreferredSize(panelsize);
        centerPanel.setLocation(0, 0);
        centerPanel.setSize(panelsize);
        addComponentToResize(centerPanel);
        layeredPane.add(centerPanel, JLayeredPane.DEFAULT_LAYER);

        getDragBarPane().create();
        layeredPane.add(getDragBarPane().getSplitPane(), JLayeredPane.DRAG_LAYER);
    }


    private void createLayeredCenterPane() {
        layeredPane = new JLayeredPane();
        overlayPanel = new JPanel();
        overlayPanel.setLayout( new BorderLayout() );
        overlayPanel.add( layeredPane, BorderLayout.CENTER );
        verticalSplitPane.setRightComponent(overlayPanel);

        overlaySizeListener = new OverlaySizeListener( overlayPanel, getDragBarPane() );
        overlayPanel.addComponentListener(overlaySizeListener);
    }


    private MainFrameDragBar getDragBarPane() {
        if(mainFrameDragBar == null) {
            mainFrameDragBar = new MainFrameDragBar(guiListener, this);
        }
        return mainFrameDragBar;
    }


    private void createContentPane() {
        mainFramePanel = new JPanel();
        mainFramePanel.setLayout(new BorderLayout());
        
        verticalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        verticalSplitPane.setOneTouchExpandable(true);
        mainFramePanel.add(verticalSplitPane, BorderLayout.CENTER);
        //note: only LAST_DIVIDER_LOCATION_PROPERTY will be changed when resizing a split pane
        verticalSplitPane.addPropertyChangeListener(JSplitPane.LAST_DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener(){
            //listen for (manual and programatic) size changes of a side menu 
            public void propertyChange(PropertyChangeEvent evt ) {
                logger.finest("[side-menu] resized -> synchronizing toggle check boxes...");
                int xLocation = ((JSplitPane) evt.getSource()).getDividerLocation();
                boolean isVisible = xLocation != verticalSplitPane.getMinimumDividerLocation();
                ApplicationServices.getInstance().getApplicationModel().setSideMenuVisible(Boolean.valueOf(isVisible));
            }});
        
        getFrame().getContentPane().add(mainFramePanel);
    }


    private void createStatusBar() {
        StatusBar statusBar = new StatusBar();
        statusListener = statusBar;
        mainFramePanel.add(statusBar, BorderLayout.SOUTH);
        statusBar.initBindings();
        statusListener.setUserName(guiListener.getCurrentUser());
    }

    private void createTopPanel() {
        toolBar = new ToolBar(StartUpConstants.MAINFRAME_TOOL_BAR_ID);
        ActionRegistry.getInstance().addContainer(toolBar);
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        topPanel.add(toolBar, BorderLayout.CENTER);
        mainFramePanel.add(topPanel, BorderLayout.NORTH);
    }


    private void activateRegisteredTabs() {
        tabbedPane = new JTabbedPane();
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        tabbedPane.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(tabbedPane));
        
        String tabdef = ConfigManager.getAppearance().get(Key.TABDEFINITION);

        if (tabdef != null) {
            logger.info("TAB-Panel has persistent configuration, loading...");
            createUserTabSet("User", tabdef);
            activateTabSet("User", tabbedPane);
        } else {
            logger.info("loading TAB-Panel default configuration...");
            createDefaultTabSet();
            activateTabSet(TABSET_DEFAULT, tabbedPane);
        }
    }
    
    private JComponent createCategoryAssignmentViewTab() {
        categoryAssignmentViewTab = new CategoryAssignmentViewer();
        return categoryAssignmentViewTab;
    }

    private JComponent createContactTab() {
        
        contactPanel = AddressPanel.createStandardAddressPanel();
        
        contactTab = new JScrollPane(contactPanel);
        
        return contactTab;  
    }


    private JPanel createCalenderTab() {
        Plugin calenderPlugin = PluginRegistry.getInstance().getPlugin(CalendarPlugin.ID);
        if(calenderPlugin != null) {
            if(calenderPlugin.isTypeSupported(TarentCalendar.class)){
                tarentCalendar = (TarentCalendar) calenderPlugin.getImplementationFor(TarentCalendar.class);
            } else logger.warning("Couldn't load an implemetation of calender interface", "Tarent Calender interface is not supported.");
            if(calenderPlugin.isTypeSupported(JPanel.class)){
                appointmentsTab = (JPanel) calenderPlugin.getImplementationFor(JPanel.class);
            } else logger.warning("Couldn't load an implemetation of calender panel", "Calender Panel is not supported.");
        } else logger.warning("Couldn't integrate a calender panel", "CalendarPlugin is not registered");
        return appointmentsTab;
    }

    private JComponent createExtendedTab() {
        extendedTopPanel = AddressPanel.createExtendedAddressPanel();
        extendedTab = new JScrollPane(extendedTopPanel);
        return extendedTab;
    }


    private JPanel createMailOrderTab() {
        Plugin mailOderPlugin = PluginRegistry.getInstance().getPlugin(MailOrdersPlugin.ID);
        if(mailOderPlugin != null) {
            if(mailOderPlugin.isTypeSupported(MailOrdersManager.class)){
                mailOrdersManager = (MailOrdersManager) mailOderPlugin.getImplementationFor(MailOrdersManager.class);
            } else logger.warning("Couldn't load an implemetation of MailOrdersManager", "MailOrdersManager interface is not supported.");
            if(mailOderPlugin.isTypeSupported(JPanel.class)){
                mailOrderTab = (JPanel) mailOderPlugin.getImplementationFor(JPanel.class);
            } else logger.warning("Couldn't load an implemetation of mail order panel", "Mail Order Panel is not supported.");
        } else logger.warning("Couldn't integrate a mail order panel", "MailOrderPlugin is not registered");
        return mailOrderTab;
    }


    private void initScheduleData() {
      Appearance ape = ConfigManager.getAppearance();
        scheduleData = new ScheduleData();
        // -- DAYWIDTH --
        scheduleData.dayWidth = ape.getAsInt(Key.SCHEDULE_DAYWIDTH, 80);
        // -- GRIDHEIGHT --
        scheduleData.snapGridHeight = ape.getAsInt(Key.SCHEDULE_GRIDHEIGHT, 15 * 60);
        // -- USER GRID --
        scheduleData.useGrid = ape.getAsBoolean(Key.SCHEDULE_USEGRID);
    }


    public void setToolBarVisible(boolean value) {
        toolBar.setVisible(value);
    }

    public void setSideMenuVisible(boolean visibleValue) {
        verticalSplitPane.setDividerLocation(!visibleValue ? verticalSplitPane.getMinimumDividerLocation() 
                                                   : verticalSplitPane.getLastDividerLocation());
    }

    private Tab getTab(Object key) {
        return ((Tab) (tabMap.get(key)));
    }


    private boolean existsTab(Object key) {
        return tabMap.containsKey(key);
    }


    private void registerTab(Object key, Tab tab) {
        tabMap.put(key, tab);
    }

    private class Tab {
        private String title;
        private Icon icon;
        private Component component;
        private String toolTip;


        public Tab(String aTitle, Icon anIcon, Component aComp, String aTooltip) {
            title = aTitle;
            icon = anIcon;
            component = aComp;
            toolTip = aTooltip;
            if(component == null) logger.warning(title + "-TAB has no content to show: it will be disabled");
        }


        public void addToPanel(JTabbedPane tabpane) {
            tabpane.addTab(title, icon, component, toolTip);
        }


        private int getIndex(JTabbedPane tabpane) {
            return tabpane.indexOfComponent(component);
        }
    }


    // Erzeuge Tabulatoren und schreibe in Map um sie konfigurieren zu können
    private void initializeTabRegistry() {
        registerTab(TAB_CONTACT, new Tab(Messages.getString("GUI_MainFrameNewStyle_Register_Standard"), icon_tab_adresse, createContactTab(), Messages.getString("GUI_MainFrameNewStyle_Register_Standard_ToolTip")));
        registerTab(TAB_EXTENDED, new Tab(Messages.getString("GUI_MainFrameNewStyle_Register_Erweitert"), icon_tab_extended, createExtendedTab(), Messages.getString("GUI_MainFrameNewStyle_Register_Erweitert_ToolTip")));
        registerTab(TAB_CATEGORY_ASSIGNMENT_VIEW, new Tab(Messages.getString("GUI_MainFrameNewStyle_Register_Zuordnung"), icon_verteiler, createCategoryAssignmentViewTab(), Messages.getString("GUI_MainFrameNewStyle_Register_Zuordnung_ToolTip")));
        registerTab(TAB_MAIL_ORDERS, new Tab(Messages.getString("GUI_MainFrameNewStyle_Register_Versand"), icon_tab_versand, createMailOrderTab(), Messages.getString("GUI_MainFrameNewStyle_Register_Versand_ToolTip")));
        if (ConfigManager.getAppearance().getAsBoolean(org.evolvis.xana.config.Appearance.Key.SHOW_SCHEDULEPANEL, true))
        		registerTab(TAB_ORGANIZER, new Tab(Messages.getString("GUI_MainFrameNewStyle_Register_Termine"), icon_tab_termine, createCalenderTab(), Messages.getString("GUI_MainFrameNewStyle_Register_Termine_ToolTip")));
    }

    private void createDefaultTabSet() {
        TabSet tabset = new TabSet();
        tabset.addTab(TAB_CONTACT);
        tabset.addTab(TAB_EXTENDED);
        tabset.addTab(TAB_CATEGORY_ASSIGNMENT_VIEW);
        tabset.addTab(TAB_MAIL_ORDERS);
        if (isToShowTerminPanel) {
            tabset.addTab(TAB_ORGANIZER);
        } else logger.warningSilent("[!] appointments to show: false");
        registerTabSet(TABSET_DEFAULT, tabset);
    }


    private void createUserTabSet(String name, String tabdefinition) {
        if (tabdefinition != null) {
            String[] tabs = tabdefinition.split(",");

            TabSet tabset = new TabSet();

            for (int i = 0; i < (tabs.length); i++) {
                String tabname = tabs[i].trim().toUpperCase();
                if (!(tabname.startsWith("TAB_")))
                    tabname = "TAB_" + tabname;

                if (!existsTab(tabname)) {
                    logger.warning("Invalid tab name: " + tabname);
                } else
                  tabset.addTab(tabname);
            }
            registerTabSet(name, tabset);
        }
    }


    private void registerTabSet(Object key, TabSet tabset) {
        tabsMap.put(key, tabset);
    }


    private TabSet getTabSet(Object key) {
        return (((TabSet) tabsMap.get(key)));
    }


    private void activateTabSet(Object key, JTabbedPane tabpane) {
        TabSet tabset = getTabSet(key);
        if (tabset != null) {
            tabpane.removeChangeListener(m_oTabListener);
            tabpane.removeAll();
            tabset.addToPanel(tabpane);
            tabpane.addChangeListener(m_oTabListener);
        }
    }

    private class TabSet {

        private List m_oTabKeys;


        public TabSet() {
            m_oTabKeys = new ArrayList();
        }


        public void addTab(String key) {
            m_oTabKeys.add(key);
        }


        public void addToPanel(JTabbedPane tabpane) {
            for (int i = 0; i < (m_oTabKeys.size()); i++) {
                Tab tab = getTab(m_oTabKeys.get(i));
                tab.addToPanel(tabpane);
            }
        }
    }


    public JLayeredPane getLayeredPane() {
        return (layeredPane);
    }


    /** 
     * Creates the main frame. 
     * The context menu state (extended|collapsed) will be set by ToggleMenuAction.
     */
    private void createMainFrame() throws MalformedURLException {

        logger.info("creating main frame:");
        readIcons();

        // create Layout...
        initMainFrameContent();

        // die letzten Schritte
        getFrame().addWindowListener(new WindowAdapter() {

            /** @see AmRequestFacade#userRequestExitApplication() */
            /* @see saveUserPreferences() */
            public void windowClosing(WindowEvent e) {
                guiListener.userRequestExitApplication();
            }
        });
        getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        // jetzt nur noch anzeigen...
        getFrame().pack();
        getFrame().setVisible(false);

        Dimension size = new Dimension(overlayPanel.getWidth(), contactPanel.getHeight());
        contactPanel.setMinimumSize(size);
        contactPanel.setMaximumSize(size);
        contactPanel.setPreferredSize(size);

        getFrame().setIconImage(icon_window.getImage());

        // auf viel zu klein stellen...
        if (isToCheckWindowSizeOnStartup)
            getFrame().setSize(new Dimension(10, 10));

        // Bildschirmgr��e ermitteln...
        Dimension actualwinsize = getFrame().getSize();

        // versuchen das OS zum maximieren des Fensters zu bewegen...
        getFrame().setExtendedState(JFrame.MAXIMIZED_HORIZ);
        getFrame().setExtendedState(JFrame.MAXIMIZED_VERT);
        getFrame().setExtendedState(JFrame.MAXIMIZED_BOTH);

        if (isToCheckWindowSizeOnStartup) {
            // testen ob Fenster nun gro� genug ist...
            Dimension winsize = getFrame().getSize();
            if (winsize.equals(actualwinsize)) {
                // Fenstergr��e manuell setzen...
                Dimension maxsize = getMaxScreenSize();
                Dimension framesize = new Dimension(maxsize.width, maxsize.height - taskBarHeight);
                getFrame().setSize(framesize);
            }
        }
        
        // versuchen das OS zum maximieren des Fensters zu bewegen...
        getFrame().setExtendedState(JFrame.MAXIMIZED_HORIZ);
        getFrame().setExtendedState(JFrame.MAXIMIZED_VERT);
        getFrame().setExtendedState(JFrame.MAXIMIZED_BOTH);

        // die GlassPane wird immer angezeigt wenn der Warte-Cursor angezeigt wird!
        glassPanel = new JPanel();
        getFrame().setGlassPane(glassPanel);
        glassPanel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }


    private Dimension getMaxScreenSize() {
        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(getFrame().getGraphicsConfiguration());
        return new Dimension(screensize.width - insets.left - insets.right, screensize.height - insets.top - insets.bottom);
    }

    
    /* (non-Javadoc)
     * @see de.tarent.contact.controller.ControlListener#getSearchMode()
     */
    public int getSearchMode() {
        return (searchMode);
    }


    public void setFilterActiveIcon(boolean isactive, String info, int type) {
        setButtonFilterOffEnabled(isactive);
        
        String filtertext = "";
        searchMode = type;
        switch (type) {
        case (GUIListener.ADDRESSSET_KEINFILTER):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_KEINFILTER");
            break;

        case (GUIListener.ADDRESSSET_TEXTSUCHE):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_TEXTSUCHE");
            break;

        case (GUIListener.ADDRESSSET_VERSANDAUFTRAG_ALL):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_VERSANDAUFTRAG_ALL");
            break;

        case (GUIListener.ADDRESSSET_VERSANDAUFTRAG_NICHTZUGEORDNET):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_VERSANDAUFTRAG_NICHTZUGEORDNET");
            break;

        case (GUIListener.ADDRESSSET_VERSANDAUFTRAG_EMAIL):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_VERSANDAUFTRAG_EMAIL");
            break;

        case (GUIListener.ADDRESSSET_VERSANDAUFTRAG_FAX):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_VERSANDAUFTRAG_FAX");
            break;

        case (GUIListener.ADDRESSSET_VERSANDAUFTRAG_POST):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_VERSANDAUFTRAG_POST");
            break;

        case (GUIListener.ADDRESSSET_FEHLERHAFTE_EMAILS):
            filtertext = Messages.getString("GUI_MainFrameExtStyle_Filter_ADDRESSSET_FEHLERHAFTE_EMAILS");
            break;
        }

        setTableFilterText("Adressen", filtertext + "   ");
    }


    // ----------------------------------------------------------------

    // Wrapper-Methode f�r Hannos MailBatchAllDialog
    // Derzeit wird der optimierte Postversand direkt aufgerufen.
    public void openSerienVersandAlle() {
        tabbedPane.setSelectedComponent(mailOrderTab);
    }


    // ----------------------------------------------------------------

    /**
     * Diese Methode teilt mit, dass sich eine Adresse ge�ndert hat.
     * 
     * @param address das aktualisierte Address-Objekt.
     * @param index der Index der aktualisierten Adresse in der aktuellen Adressensammlung; wenn -1, so ist die Adresse nicht in der Addresssammlung.
     */
    public void addressChanged(Address address, int index) {
        contactsTable.addressChanged(address, index);
    }


    public void savingAddressChanges(Address address, int index) {
    }


    public void discardingAddressChanges(Address address, int index) {
        // embeddedverteilerassign.getAndShowIt();
    }

    // ------------------------------------------------------------------------------------
    public boolean cancelAddress() {
        if (isNewMode() || isEditMode()) {
            return guiListener.userRequestNotEditable(GUIListener.DO_DISCARD);
        }
        return true;
    }


    public boolean saveAddress() {
        if (isNewMode() || isEditMode()) {
            return guiListener.userRequestNotEditable(GUIListener.DO_SAVE);
        }
        return true;
    }

    /**
     * Diese Methode fordert zum Ausfhren eines Kanals eines Versandauftrages auf. Dies kann mittels des Rckgabewertes abgelehnt werden.
     * 
     * @param batch der auszufhrende Versandauftrag
     * @param channel der auszufhrende Versandkanal
     * @return <code>true</code> gdw dieser Kanal bearbeitet werden kann.
     */
    /*
    public boolean executeMailBatch(MailBatch batch, MailBatch.CHANNEL channel) {
    	if(PluginRegistry.getInstance().isPluginAvailable("mail"))
    	{
    		Plugin mailPlugin = PluginRegistry.getInstance().getPlugin("mail");
    		
    		if(mailPlugin.isTypeSupported(FrameAddressListPerformer.class))
    		{
    			FrameAddressListPerformer performer = (FrameAddressListPerformer)mailPlugin.getImplementationFor(FrameAddressListPerformer.class);
    			try
				{
					performer.setAddresses(batch.getChannelAddresses(channel));
				} catch (ContactDBException e)
				{
					// TODO I18n
					logger.warning("Failed sending Mail-Batch", e);
				}
    			performer.getFrame().setLocationRelativeTo(ApplicationServices.getInstance().getActionManager().getFrame());
    			performer.getFrame().setVisible(true);
    		}
    	}
    	return true;
    }
    */


    /**
     * Diese Methode fordert zum Ausfhren eines Kanals einer Adresse auf. Dies kann mittels des Rckgabewertes abgelehnt werden.
     * 
     * @param address die auszufhrende Adresse
     * @param channel der auszufhrende Versandkanal
     * @param action die konkrete Aktion, eine der Konstanten an {@link ApplicationStarter}
     * @return <code>true</code> gdw dieser Kanal bearbeitet werden kann.
     */
    public boolean executeAddress(Address address, MailBatch.CHANNEL channel, String action) {
        return false;
    }


    /**
     * Diese Methode liefert zu einem Schlssel einen konfigurierten Mitteilungsstring.
     * 
     * @param key Schlssel
     * @return zugeh�riger Mitteilungsstring; falls nicht gefunden, wird <code>'!' + key + '!'</code> geliefert.
     * @see org.evolvis.nuyo.controller.ControlListener#getMessageString(java.lang.String)
     */
    public String getMessageString(String key) {
        return Messages.getString(key);
    }


    public boolean showAddress(Address address) {
        return guiListener.userRequestSelectAddress(address, GUIListener.DO_ASK, "Einzel-Adresse", GUIListener.ADDRESSSET_SINGLE_ADDRESS);
    }


    public void refreshHistory() {
        Address address = guiListener.getAddress();
        if (address != null) {
            panelInserter.setAddressFieldData(AddressKeys.HISTORY, address);
        }
    }



    public JPanel getOverlayPanel() {
        return overlayPanel;
    }


    int getCurrentIndex() {
        return currentIndex;
    }


    public void registerTableComponent(String name, JComponent component) {
        getDragBarPane().registerTableComponent(name, component);
    }


    public void setTableView(String name) {
        getDragBarPane().setTableView(name);
    }


    public void setTableText(String name, String text) {
        getDragBarPane().setTableText(name, text);
    }


    public void setTableStaticText(String name, String text) {
        getDragBarPane().setTableStaticText(name, text);
    }


    public void setTableFilterText(String name, String text) {
        getDragBarPane().setTableFilterText(name, text);
    }


    public void showTable(boolean show) {
        getDragBarPane().showTable(show);
    }


    public void setPreviewBusy(boolean isbusy) {
        getDragBarPane().setPreviewBusy(isbusy);
    }


    public void setPreviewPercent(int percent) {
        getDragBarPane().setPreviewPercent(percent);
    }


    public void addComponentToResize( JComponent comp ) {
        overlaySizeListener.addComponentToResize(comp);
    }


    public void removeComponentToResize( JComponent comp ) {
        overlaySizeListener.removeComponentToResize(comp);
    }


    public void selectCalendar( int calendarID ) {
        if(tarentCalendar != null) tarentCalendar.selectCalendar(new Integer(calendarID));
        else logger.warningSilent("[!] couldn't select calender: TarentCalendar interface not registered");
    }


    public void changeCalendar() {
        if(tarentCalendar != null) tarentCalendar.changeCalendar();
        else logger.warningSilent("[!] couldn't change calender: TarentCalendar interface not registered");
    }

    /** 
     * Handles an activated context.<p>
     * 
     * @see org.evolvis.nuyo.gui.action.ContextChangeListener#contextChanged(java.lang.String, boolean)
     * @see org.evolvis.nuyo.gui.action.ActionRegistry#addContextChangeListener(ContextChangeListener)
     * @see org.evolvis.nuyo.gui.action.ActionRegistry#removeContextChangeListener(ContextChangeListener)
     */
    public void contextChanged( String context, boolean isEnabled ) {
        logger.fine( "[context-handling]: " + context );
        if ( StartUpConstants.NEW_MODE.equals( context ) ) {
            updateVisibleState(isEnabled);
        }
        else if ( StartUpConstants.EDIT_MODE.equals( context ) ) {
            updateVisibleState(isEnabled);
        }
        else if ( StartUpConstants.SEARCH_MODE.equals( context ) ) {
            if(isEnabled){
                setFilterActiveIcon(true, getMessageString("AM_Search_Filter"), GUIListener.ADDRESSSET_TEXTSUCHE);
            }
        }
        else if ( StartUpConstants.VIEW_MODE.equals( context ) ) {
            if(isEnabled){
                setFilterActiveIcon(false, null, GUIListener.ADDRESSSET_KEINFILTER);
            }
        }
        else if (DispatchEMailPanel.EMAIL_FIRST_MAIL_MODE.equals(context) || DispatchEMailPanel.EMAIL_LAST_MAIL_MODE.equals(context) || DispatchEMailPanel.EMAIL_PREVIEW_MODE.equals(context))
        {
        	// ignore modes only relevant for the E-Mail-Dialog
        }
        else {
            logger.warningSilent("[!] unknown context: " + context);
        }
    }
    
    /**
     * Lets user choose initial subcategories for a new address. 
     * 
     * <p>Returns <code>null</code> if operation is to be aborted.</p>
     * 
     * <p>This method is called when the saving operation of a new address
     * is started.</p>
     *  
     * @return
     */
    public List requestSubCategoriesForNewAddress(Address newAddress)
    {
		// Fiddles out the categories on which we can assign the address to
		// and the ones where the address is currently assigned to.
		List visibleCats = null;

		try {
			visibleCats = ApplicationServices.getInstance().getCurrentDatabase().getCategoriesWithCategoryRightsOR(
					false, false, true, false, false, false, true, false, false);
		} catch (ContactDBException e1) {
			return null;
		}
		
		NewAddressAssignHandler assignHandler = new NewAddressAssignHandler();

		// Prepares a category tree that can display the action and makes it visible.
		CategoryTree tree = CategoryTree.createSingleAssignmentCategoryTree(
				visibleCats, Collections.EMPTY_LIST, assignHandler);

		JDialog categoryTreeDialog = new JDialog(ApplicationServices.getInstance()
				.getMainFrame().getFrame(), true);
		// Allows the assignhandler to close the dialog.
		assignHandler.dialog = categoryTreeDialog;
		
		categoryTreeDialog.setTitle(tree.getTitle());
		categoryTreeDialog.add(tree);
		categoryTreeDialog.pack();
		categoryTreeDialog.setLocationRelativeTo(null);
		
		// The next call blocks until the user chose the [sub]category
		// assignment or canceled the operation.
		categoryTreeDialog.setVisible(true);
		
      return assignHandler.subCategories;
    }

    /** Simple AssignHandler implementation which stores the resulting
     * subcategory list in its object variable to 
     * @author rob
     *
     */
    private static class NewAddressAssignHandler implements CategoryTree.AssignHandler
    {
    	List subCategories = null;
    	JDialog dialog;

		public void cancel() {
			// Nothing else to do.
			dialog.setVisible(false);
		}

		public void submit(List subCategoryList, List _) {
			subCategories = subCategoryList;
			
			dialog.setVisible(false);
		}
    	
    }

	public void setBlocked(boolean blocked)
	{
		getFrame().setEnabled(!blocked);
		
		// TODO make deactivation visible to user
    	
    	if(blocked)
    		getFrame().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    	else
    		getFrame().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
} // end class MainFrame
