package org.evolvis.nuyo.gui.actions;
import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class ExportAppointmentsAndTasksAction extends AbstractGUIAction {
    
    private static final long serialVersionUID = -3817338920839311232L;
    private static final TarentLogger log = new TarentLogger(ExportAppointmentsAndTasksAction.class);
    private GUIListener guiListener;
    private Runnable exportExecutor;

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null){
            SwingUtilities.invokeLater(exportExecutor);
        } else log.warning("ExportAppointmentsAndTasksAction is not initialized");
    }

    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        exportExecutor = new Runnable(){
            public void run() {
                // get Plugin
                // get schedule panel, get navigation panel, export action
            }
        };
    }
}
