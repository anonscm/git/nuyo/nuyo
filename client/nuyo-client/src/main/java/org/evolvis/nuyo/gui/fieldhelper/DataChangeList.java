/*
 * Created on 20.06.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author niko
 *
 */
public class DataChangeList
{
  private List m_oDataChanges;
  
  public DataChangeList()
  {
    m_oDataChanges = new ArrayList();
  }
  
  public void addDataChange(DataChange change)
  {
    m_oDataChanges.add(change);
  }

  public boolean containsChanges()
  {
    return (m_oDataChanges.size() > 0);
  }
  
  public int getNumberOfChanges()
  {
    return (m_oDataChanges.size());
  }
  
  public String getDataChangesString(String prefix, String postfix)
  {
    String text = "";
    Iterator it = m_oDataChanges.iterator();
    while(it.hasNext())
    {
      DataChange change = (DataChange)(it.next());
      String field = change.getChangeDescription() + " [" + change.getBeforeValue().toString() + " => " + change.getAfterValue().toString() + "]";
      text += prefix + field + postfix;
    }    
    if (text.endsWith(postfix)) text = text.substring(0, text.length() - postfix.length());
    return text;
  }
  
  
}
