/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.FieldDescriptors;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface FieldDescriptor
{
  public FieldDescriptor cloneFieldDescriptor();
  public void init();
  public void dispose();
  
  public void setName(String name);
  public String getName();

  public void setDescription(String name);
  public String getDescription();

  public void setShortDescription(String name);
  public String getShortDescription();
    
  public void setKey(char key);
  public char getKey();
  
  public String getCurrentStatusText();  
  
  public void setDataContainer(DataContainer dc);
  public DataContainer getDataContainer();
  
  // EVENTS...
  public List getEventsConsumable();
  public List getEventsFireable();
  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler);    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler);
  public void fireTarentGUIEvent(TarentGUIEvent e);          
}
