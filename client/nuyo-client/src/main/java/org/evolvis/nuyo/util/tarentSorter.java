/*
 * Created on 12.08.2004
 *
 */
package org.evolvis.nuyo.util;

import java.text.Collator;
import java.util.Locale;


/**
 * @author niko
 *
 */
public class tarentSorter
{
  public static int compareAlpha(String s1, String s2)
  {   
    return Collator.getInstance(Locale.GERMAN).compare(s1, s2);
//    return replaceUmlauts(s1).compareTo(replaceUmlauts(s2));
  }
  
  private static String replaceUmlauts(String s)
  {
    s = s.replaceAll("�", "ae");
    s = s.replaceAll("�", "oe");
    s = s.replaceAll("�", "ue");
    s = s.replaceAll("�", "Ae");
    s = s.replaceAll("�", "Oe");
    s = s.replaceAll("�", "Ue");
    s = s.replaceAll("�", "ss");
    return s;
  }
  
  
}
