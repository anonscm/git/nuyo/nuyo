package org.evolvis.nuyo.plugins.calendar;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JColorChooser;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.logging.TarentLogger;



/**
 * Shows color chooser.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ColorChooserListener extends MouseAdapter {

    private static final String dialogMessage = Messages.getString("GUI_MainFrameNewStyle_Actionbox_Calendars_ColorChooserTitle");
    private static final ControlListener controlListener = ApplicationServices.getInstance().getMainFrame();
    private static final GUIListener guiListener = ApplicationServices.getInstance().getActionManager();
    private static final TarentLogger logger = new TarentLogger(ColorChooserListener.class);
    
    private final CalendarEntryPanel calendarEntryPanel;
    private final Integer calendarID;
    private Color selectedColor;
    
    /** Constructor */
    public ColorChooserListener(final Integer aCalendarID, final CalendarEntryPanel panel){
        calendarID = aCalendarID;
        calendarEntryPanel = panel;
    }

    /** Shows Color Choorser Dialog and saves selected color in DB.*/ 
    public void mouseClicked(MouseEvent e) {
        selectedColor = JColorChooser.showDialog(controlListener.getFrame(), dialogMessage, Color.BLUE);
        if (selectedColor != null) {
            if(calendarEntryPanel != null){
                
                calendarEntryPanel.setCalendarColor(selectedColor);
                
                new Thread("color-update-thread"){//is thread safe: no exceptions will be thrown
                    public void run() {
                        fireCalendarViewUpdate();
                    }
                }.start();
                
            } else logger.warningSilent("");
        } 
    }

    private void fireCalendarViewUpdate() {
        //update view
        controlListener.changeCalendar();//is thread safe!
        //push color to DB
        try {
            // 25.10.05 Von Sebastion umgebogen, damit die Kalenderfarbe nicht jedes mal neu geholt wird
            // m_oGUIListener.setUserParameter(null,"CalendarColor_ID_"  calendarEntryPanel.getCalendar().getId() , new Integer(selectedColor.getRGB()).toString());
            guiListener.setCalendarColor(calendarID.intValue(), new Integer(selectedColor.getRGB()).toString());
        }
        catch ( Exception e ) {
            logger.severe("Couldn't save color persistent", e);
        }
    }
}