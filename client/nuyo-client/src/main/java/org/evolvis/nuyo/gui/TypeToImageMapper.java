/* $Id: TypeToImageMapper.java,v 1.4 2006/07/17 19:25:21 aleksej Exp $
 * 
 * Created on 21.05.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH 
 */
package org.evolvis.nuyo.gui;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import org.evolvis.nuyo.controller.ApplicationServices;


/**
 * @author niko
 *
 */
public class TypeToImageMapper
{  
  private static ArrayList m_oIcons = new ArrayList();
  private static ArrayList m_oNames = new ArrayList();
  
  public static ImageIcon getIcon(int type)
  {
    return((ImageIcon)(m_oIcons.get(type)));
  }

  public static String getName(int type)
  {
    return(asString(m_oNames.get(type)));
  }

  public static void registerIcon(int type, String name, String filename)
  {
    m_oIcons.add(type, ApplicationServices.getInstance().getIconFactory().getIcon(filename));
    m_oNames.add(type, name);
  }

  private final static String asString(Object o) {
      return o == null ? null : o.toString();
  }
}
