
/* $Id: ActionManager.java,v 1.43 2007/08/30 17:43:16 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller;

import javax.swing.JFrame;

import org.evolvis.nuyo.controller.VersionCheck.VersionsIncompatibleException;
import org.evolvis.nuyo.controller.VersionCheck.VersionsNotFullyCompatibleException;
import org.evolvis.nuyo.controller.manager.AmRequestFacade;
import org.evolvis.nuyo.datacontainer.parser.PluginLocator;
import org.evolvis.nuyo.db.CalendarCollection;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.UserDataProviderScreen;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.nuyo.plugins.calendar.CalendarPlugin;
import org.evolvis.nuyo.plugins.calendar.CalendarsSelectionModel;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Appearance.Key;

import de.tarent.commons.config.Config;
import de.tarent.commons.plugin.Plugin;
import de.tarent.commons.ui.swing.LoginDialog;
import de.tarent.commons.utils.VersionTool;


/**
 *  Diese Klasse dient der Steuerung der Anwendung und dem Dispatching der Events.
 * Aufgrund des enthaltenen Funktionsumfangs wurde der Code auf mehrere Klassen
 * verteilt.<br>
 *  AmReferences enth�lt Basisreferenzen auf Datenquelle, Darstellung, etc;<br>
 *  AmCommonDialogs enth�lt Aufrufe von Fehler-, Info- und Frage-Dialogen
 * und einfache weitere Darstellungsebenenreferenzen.<br>
 *  AmDatabaseFacade enth�lt gepufferte und durchgeleitete Datenquellenabfragen.<br>
 *  AmSelection enth�lt Methoden, die die Adressauswahl und die aktuelle Adresse
 * bestimmen.<br>
 *  AmActionFacade enth�lt Methoden, die komplexe Aktionen, insbesondere mit
 * externen Auswirkungen ausf�hren.<br>
 *  AmRequestFacade enth�lt Methoden, die HighLevel-Requests der Darstellungsebene
 * abbilden.<br>
 *  ActionManager, also diese Klasse, initialisiert nun die Basisreferenzen
 * aus Konfiguration und Anmeldedialog heraus. 
 * 
 * @author Sebastian, mikel
 */
public class ActionManager extends AmRequestFacade implements GUIListener {

	/**
	 * Initialisiert die Anwendung, macht sie aber noch nicht sichtbar.
	 */
	public ActionManager(Database db, LoginDialog loginProvider) {
		ApplicationServices.getInstance().setActionManager(this);        

		setDb(db);

		// Since the action manager retrieves, changes and stores a lot of
		// data on which other parts of the application depend (mainly
		// the UI) the registration has to be done before those depended
		// parts become active.
		initApplicationModel(ApplicationServices.getInstance().getApplicationModel());

		Appearance ape = ConfigManager.getAppearance();
		String appCaption = getApplicationCaption();

		// Freak Logger:
		getLogger().setGUIListener(this);

		setPluginLocator(new PluginLocator(getLogger().getLogger()));

		isToAskBeforeExitApplication = "askuser".equalsIgnoreCase(ape.get(Key.EXIT_MODE)); //diese Sachen m�ssen bleiben

		try {

			//initial loading of categories
			//getDb().getInitDatas();

			String dbVersion = getDb().getVersion(Config.APPLICATION_VERSION); 
			// getVersion() macht automatisch auch ein getInitDatas()
			loginProvider.setStatusText("Datenquellenversion: " + dbVersion +"\n");

			VersionCheck VC = new VersionCheck(getApplicationCaption(), dbVersion);


			if (getDb() instanceof OctopusDatabase)
				((OctopusDatabase)getDb()).setUserDataProvider(new UserDataProviderScreen((JFrame)getControlListener(), this));

			loginProvider.setStatusText("hole Stammdaten...");

			//fetch lists depends on getInitDatas
			fetchLists();
			if (getCategoriesList().size() == 0) {
				publishError("Ihr Zugang ist nicht oder nur fehlerhaft konfiguriert.\nBitte wenden Sie sich an Ihren Administrator.");
				//TODO: so ok?? logger.severe("Benutzer " + getApplicationUser() + " hat keine zugeordneten Kategorien.");
				logger.severe("Benutzer " + getDb().getUser() + " hat keine zugeordneten Kategorien.");
				System.exit(1);
			}
			loginProvider.setStatusText(" " + Messages.getString("ActionManager_Start_Message_Done") + "\n");
			loginProvider.setStatus(20);

			loginProvider.setStatusText(Messages.getString("ActionManager_Start_Message_Creating_GUI"));
			setControlListener(new MainFrameExtStyle(this));
			ApplicationServices.getInstance().setMainFrame(getControlListener());
			loginProvider.setStatus(30);

			loginProvider.setStatusText(Messages.getString("ActionManager_Start_Message_Initializing_GUI"));
			getControlListener().init();
			loginProvider.setStatus(50);

			loginProvider.setStatusText(Messages.getString("ActionManager_Start_Message_Initializing_GUI_Components"));
			getEventsManager().fireSetCaption(appCaption);
			loginProvider.setStatus(70);

			loginProvider.setStatusText(Messages.getString("ActionManager_Start_Message_Retrieving_Addresses"));
			loadInitialAddresses();
			loginProvider.setStatus(90);
		} catch (VersionsIncompatibleException e) {
			loginProvider.setStatusText(Messages.getString("ActionManager_Start_Message_Failed"));
			//TODO: publishError geht hier noch nicht! publishError("Fehler beim Erstellen der Datenbankverbindung.\n", e.getMessage());
			// omg, i must die
			System.exit(0);

		} catch (VersionsNotFullyCompatibleException e) {
			loginProvider.setStatusText("!!! Versionen nicht 100% kompatibel !!!:\nDetails:\n" + e.getMessage()+"\n" );
		} catch (ContactDBException e){
			logger.warning("ContactDBException, ExceptionId: "+ ((ContactDBException)e).getExceptionId());
		}catch (Exception e) {
			logger.severe(Messages.getFormattedString("ActionManager_Start_Message_Initialization_Failed", e.getClass().getName()), e);
			System.exit(1);
		}

		logger.fine("[ActionManager] Habe alles initialisiert");
		loginProvider.setStatus(100);

		//TODO: so weit nach vorne verschieben wie irgendwie m�glich!
		logger.setCurrentUser(getUser(getCurrentUser()));

		// den loginProvider schliessen...
		loginProvider.setDialogVisible(false);
	}

	public ControlListener getMainControlListener(){
		return getControlListener();
	}

	/**
	 * Macht die Anwendung sichtbar
	 */
	public void start() {
		getControlListener().openGUI();
		ActionRegistry.getInstance().enableContext(VIEW_MODE);
		setWaiting(false);

		logger.info("[ActionManager] Habe alles gestartet");
	}

	/**
	 * Macht die Anwendung unsichtbar
	 */
	public void stop() {
		getControlListener().closeGUI();
		logger.fine("[ActionManager] Habe alles gestoppt");
	}    

	private CalendarsSelectionModel calendarsSelectionModel;

	public CalendarCollection getCalendarCollection() {
		try {
			return getCalendarsSelectionModel().getCalendarCollection();
		}
		catch ( ContactException e ) {
			logger.warningSilent("[!] no CalendarsSelectionModel",e);
			return null;
		}
	}

	public int getCurrentCalendarID() {
		try {
			return getCalendarsSelectionModel().getCurrentCalendarID().intValue();
		}
		catch ( ContactException e ) {
			logger.warningSilent("[!] no calendar selection model",e);
			return -1;
		}
	}

	private CalendarsSelectionModel getCalendarsSelectionModel() throws ContactException{
		if(calendarsSelectionModel == null) {
			Plugin calenderPlugin = PluginRegistry.getInstance().getPlugin(CalendarPlugin.ID);
			if(calenderPlugin != null) {
				if(calenderPlugin.isTypeSupported(CalendarsSelectionModel.class)){
					calendarsSelectionModel = (CalendarsSelectionModel) calenderPlugin.getImplementationFor(CalendarsSelectionModel.class);
				} else logger.warning("Couldn't load an implemetation of calender selection model", "CalendarsSelectionModel is not supported by calendar plugin.");
			} else logger.warning("Couldn't load an implemetation of calender selection model", "CalendarPlugin is not registered");
			// check if loaded
			if(calendarsSelectionModel == null) {
				logger.warningSilent("[!] [ActionManager]: can't get calendars: calendarsSelectionModel not initialized");
				throw new ContactException(ContactException.CALENDAR_SELECTION_MODEL_NOT_INITIALIZED);
			}
		}
		return calendarsSelectionModel;
	}

	public static String getApplicationCaption() {

		String applicationVersion = VersionTool.getInfoFromClass(Starter.class).getVersion("n/a");
		String applicationCaption = Messages.getString("MainFrame_Title");
		String applicationBuild = VersionTool.getInfoFromClass(Starter.class).getBuildID("development build");
		
		// if we are running a development-build show as many information as possible
		if(applicationVersion == null || applicationVersion.equals("n/a") || applicationVersion.toLowerCase().indexOf("snapshot") != -1)
			return applicationCaption + " " + applicationVersion + " (" + applicationBuild + ")";

		// if we are running on a final build, do not show build ID
		return applicationCaption + " " + applicationVersion;
	}
}
