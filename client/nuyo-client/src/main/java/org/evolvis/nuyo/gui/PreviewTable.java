/*
 * Created on 11.08.2004
 *
 */
package org.evolvis.nuyo.gui;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

/**
 * @author niko
 *
 */
public class PreviewTable extends JTable
{
  private List m_oColumnToolTips = new ArrayList();
  
  
  public PreviewTable(AbstractTableModel model)
  {
    super(model);
  }
  
  public void setColumnToolTip(int column, String tooltip)
  {
    m_oColumnToolTips.add(column, tooltip);
  }
  
  protected JTableHeader createDefaultTableHeader() 
  {
    return new JTableHeader(columnModel) 
    {
        public String getToolTipText(MouseEvent e) 
        {
            String tip = null;
            java.awt.Point p = e.getPoint();
            int index = columnModel.getColumnIndexAtX(p.x);
            int realIndex = columnModel.getColumn(index).getModelIndex();
            try
            {
              String tooltip = (String)(m_oColumnToolTips.get(realIndex));
              return tooltip;
            }
            catch(IndexOutOfBoundsException iobe)
            {
              return ""; 
            }
        }
    };  
  }  
}
