/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Date;
import java.util.Iterator;

import org.evolvis.nuyo.controls.TarentWidgetDateSpinner;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;



/**
 * TODO: remove if not used in AddressFieldFactory.getGUIElement()!
 * @deprecated not used (only by factory)
 * 
 * @author niko
 */
public class DateField extends GUIElementAdapter implements FocusListener
{
  private TarentWidgetDateSpinner m_oDateSpinner;
  
  public DateField()
  {
    super();
     
    m_oDateSpinner = new TarentWidgetDateSpinner();
  }
  
  public String getListenerName()
  {
    return "DateField";
  }  
  
  public GUIElement cloneGUIElement()
  {
    DateField checkfield = new DateField();
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      checkfield.addParameter(param.getKey(), param.getValue());
    }
    return checkfield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "DateField", new DataContainerVersion(0));
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    return false;
  }

  public boolean canDisplay(Class data)
  {
    return data.isInstance(Date.class);
  }

  public Class displays()
  {
    return Date.class;
  }
  

  public TarentWidgetInterface getComponent()
  {
    return m_oDateSpinner;
  }

  public void setData(Object data)
  {
    if (data instanceof Date)
    {
      m_oDateSpinner.setData((Date)data);      
    }
  }

  public Object getData()
  {
    return m_oDateSpinner.getData();
  }


  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    //System.out.println("focuslost at " + e.getComponent().getLocation());
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }
  
  
  public void initElement()
  {
    m_oDateSpinner.addFocusListener(this);
  }

  public void disposeElement()
  {
    m_oDateSpinner.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oDateSpinner.requestFocus();
  }
  
  
  public Point getOffsetForError(ErrorHint errorhint)
  {
    return(new Point(0,0));
  }

}
