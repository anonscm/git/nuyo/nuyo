package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SortPostCodeAction extends AbstractGUIAction {

    private static final long serialVersionUID = 7386979989686803448L;
    private static final TarentLogger log = new TarentLogger(SortPostCodeAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SortPostCodeAction not implemented");
    }

}