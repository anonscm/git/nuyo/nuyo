package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.xana.action.AbstractGUIAction;


/**
 * Shows tarent-contact help window.  
 * 
 * @author Aleksej Palij
 */
public class HelpBookAction extends AbstractGUIAction {

	private static final long	serialVersionUID	= -6447188186939584869L;

	public void actionPerformed(ActionEvent e) {
	    SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                ApplicationServices.getInstance().getHelpBroker().setDisplayed(true);
            }
        });
	}
	
}
