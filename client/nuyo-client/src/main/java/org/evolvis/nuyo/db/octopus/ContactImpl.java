package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;



/**
 * @author kleinw
 *
 *	Historisiertes Kontaktereignis zwischen externen und internen Usern.
 *
 */
class ContactImpl extends ContactBean {

    //
    //	Soapanbindung
    //
    /**	Methode zum Holen von Kontakten */
    private static final String METHOD_GETCONTACTS = "getContacts";
    
    
    //	Objekt zum Beziehen Contactobjekten */
    static {
        _fetcher = new AbstractEntityFetcher(METHOD_GETCONTACTS, true) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    Contact contact = new ContactImpl();
                    ((IEntity)contact).populate(values);
                    return (IEntity)contact;
                }
            };
    }
    
    
    /** Leerer Konstruktor */
    protected ContactImpl() {}

    /**	Spezieller Konstruktor */
    ContactImpl(Address address, User user, Integer category, Integer channel, Integer duration, Boolean inBound, 
            			String subject, String note, String structInfo, Date startDate, boolean create) throws ContactDBException {
        
        _address = address;
        _user = user;
        _category = category;
        _channel = channel;
        _duration = duration;
        _inbound = inBound;
        
        _attributes.put(Contact.KEY_SUBJECT, subject);
        _attributes.put(Contact.KEY_NOTE, note);
        _attributes.put(Contact.KEY_STRUCT_INFO, structInfo);
        
        _startDate = startDate;
        _historyDate = startDate;
        
        if (create)
            commit();
        
    }
    
    
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}

    
    /** Speichern des Objekts */
    public void commit() throws ContactDBException {
		Method method = new Method("createContact");
		method.add("attributes", _attributes);
        method.add("addressid", new Integer(_address.getAdrNr()));
        method.add("category", _category);
        method.add("channel", _channel);
        method.add("duration", _duration);
        //TODO: hier _linkType �bermitteln...
		method.add("linktype", new Integer(0));
		method.add("inbound", _inbound);
		method.add("startdate", new Long(_startDate.getTime()));
		method.invoke();
    }

    public Map getAsMap() throws ContactDBException {
		Map map = new HashMap();
		map.put("attributes", _attributes);
        map.put("addressid", new Integer(_address.getAdrNr()));
        map.put("category", _category);
        map.put("channel", _channel);
        map.put("duration", _duration);
        //TODO: hier _linkType �bermitteln...
		map.put("linktype", new Integer(0));
		map.put("inbound", _inbound);
		map.put("startdate", new Long(_startDate.getTime()));
        return map;
    }
    
    public void delete() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED, "Diese Funktion ist noch nicht implementiert.");
    }

    
    /**	der zuletzt geladene Zustand wird wieder angenommen */
    public void rollback() throws ContactDBException {populate(_responseData);}


	/**	Bef�llen des Objekts durch DB-Inhalte */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        setAttribute(Contact.KEY_SUBJECT, data.getString(1));
        setAttribute(Contact.KEY_NOTE, data.getString(2));
        setAttribute(Contact.KEY_STRUCT_INFO, data.getString(3));
        _category = data.getInteger(4);
        _channel = data.getInteger(5);
        _linkType = data.getInteger(6);
        _startDate = data.getDate(7);
        _historyDate = data.getDate(7);
        _inbound = (data.getInteger(8).intValue() == 1)?new Boolean(true):new Boolean(false);
        _duration = data.getInteger(9);
        _user = new UserImpl(data.getInteger(10));
    }

    
    /** Gem�� eines Filters k�nnen hier Contactobjekte abgeholt werden */
    static public Collection getContacts(ISelection filter) throws ContactDBException {
    	return _fetcher.getEntities(filter, false);
    }
    
	
	/**	Stringrepr�sentation des Objekts */
	public String toString(){
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("--TcContact: " + getAttribute(KEY_SUBJECT) + "--\n");
			sb.append("ID: " + getId() + "\n");
			sb.append("Channel: " + ContactUtils.generateChannelString(getChannel()) + "\n");
			sb.append("Category: " + ContactUtils.generateCategoryString(getCategory())+ "\n");
			sb.append("-- Ende TcContact --");
		} catch (ContactDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
    
}
