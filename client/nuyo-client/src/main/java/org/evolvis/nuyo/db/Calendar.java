/* $Id: Calendar.java,v 1.4 2006/10/27 13:21:53 aleksej Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

import java.util.Collection;
import java.util.Date;

/**
 * Diese Schnittstellen stellt ein Kalendarium dar.
 * 
 * @author mikel
 */
public interface Calendar {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();
    
    //
    // Attribute
    //
    /** Bei Benutzerkalendern: zugeh�riger Benutzer */
    public User getUser() throws ContactDBException;
    
    /** Bei Gruppenkalendern: zugeh�rige Gruppe */
    public UserGroup getUserGroup() throws ContactDBException;

    /** Bei PrivatKalendern : Freigaben an andere Benutzer*/
    public Collection getCalendarSecretaries() throws ContactDBException;
    
    public CalendarSecretaryRelation getCalendarSecretaryforUser(int userid) throws ContactDBException;
    
    /** Bei Ressourcenkalendern: zugeh�rige Ressource */
    public Resource getResource() throws ContactDBException;

    /** Typ des Kalendariums, vgl Konstanten <code>TYPE_*</code> */
    public int getType() throws ContactDBException;

    /** Textattribute, vgl. Konstanten <code>KEY_*</code> */
    public String getAttribute(String key) throws ContactDBException;
    public void setAttribute(String key, String newValue) throws ContactDBException;

    /** Benutzer mit Zugriff auf den Kalender */
    public Collection getUsers() throws ContactDBException;
    public void add(User newUser) throws ContactDBException;
    public void remove(User newUser) throws ContactDBException;
    public boolean canRead(User newUser) throws ContactDBException;
    public boolean canWrite(User newUser) throws ContactDBException;
    
    /** Freigaben auf Kalender*/
    public void add(CalendarSecretaryRelation newCalSec) throws ContactDBException;
    public void remove(CalendarSecretaryRelation calSec) throws ContactDBException;
    
    /** Gruppen mit Zugriff auf den Kalender */
    public Collection getUserGroups() throws ContactDBException;
    public void add(UserGroup group) throws ContactDBException;
    public void remove(UserGroup group) throws ContactDBException;
    public boolean canRead(UserGroup group) throws ContactDBException;
    public boolean canWrite(UserGroup group) throws ContactDBException;

    /**
     * Diese Methode liefert Termine im Kalendarium in einem Zeitinterval.
     * 
     * @param start
     *            UTC-Starttermin (inklusive)
     * @param end
     *            UTC-Endtermin (exklusive)
     * @param filters
     *            Sammlung von Filtern
     * @return Liste der Termine im Zeitintervall
     */
    public Collection getAppointments(Date start, Date end, Collection filters) throws ContactDBException;

    /**
     * Diese Methode gibt ein Appointment zur�ck
     * @param id
     * @return
     * @throws ContactDBException
     */
    public Appointment getAppointment(Integer id) throws ContactDBException;

    /**
     * Diese Methode liefert Terminerinnerungen im Kalendarium in einem Zeitinterval.
     * 
     * @param start
     *            UTC-Starttermin (inklusive)
     * @param end
     *            UTC-Endtermin (exklusive)
     * @param filters
     *            Sammlung von Filtern
     * @return Liste der Termine im Zeitintervall
     */
    public Collection getReminders(Date start, Date end, Collection filters) throws ContactDBException;
    
    //
    // Konstanten
    //
    /** Textattribut-Schl�ssel: Name, Ersteller, letzter Bearbeiter */
    public final static String KEY_NAME = "name";
    public final static String KEY_CREATOR = "createdby";
    public final static String KEY_CHANGER = "changedby";
    
    /** Kalendariumstypen: Benutzer, Gruppe, Ressource, anderer */
    public final static int TYPE_USER = 1;
    public final static int TYPE_USER_GROUP = 2;
    public final static int TYPE_RESOURCE = 3;
    public final static int TYPE_OTHER = 0;
}
