/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextViewField;


/**
 * @author niko
 *
 */
public class AdressnummerField extends GenericTextViewField
{
  public AdressnummerField()
  {
    super("ADRESSNR", AddressKeys.ADRESSNR, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Adress_Nr_ToolTip", "GUI_MainFrameNewStyle_Standard_Adress_Nr", 0);
  }  
}
