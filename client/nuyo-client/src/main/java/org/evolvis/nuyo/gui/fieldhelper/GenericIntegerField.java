/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;

import java.awt.Color;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetIntegerField;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.plugin.PluginData;


/**
 * @author niko
 *
 */
public class GenericIntegerField extends ContactAddressIntegerField
{
  private TarentWidgetLabel m_oLabel;
  private TarentWidgetIntegerField   m_oTextfield; 
  
  private String m_sKey;
  private Object m_AdressKey;
  private String m_sTooltipKey;
  private String m_sLabelKey;
  private int m_iMaxLength;
  private int m_iContext;
  
  
  public GenericIntegerField(String panelkey, Object adresskey, int context, String tooltipkey, String labelkey, int maxlength)
  {
    m_sKey = panelkey;
    m_AdressKey = adresskey;
    m_sTooltipKey = tooltipkey;
    m_sLabelKey = labelkey;
    m_iMaxLength = maxlength;
    m_iContext = context;    
  }

  
  public String getFieldName()
  {
    if (fieldName != null) return fieldName; 
    else return Messages.getString(m_sLabelKey);
  }
  
  public String getFieldDescription()
  {
    if (fieldDescription != null) return fieldDescription;
    else return Messages.getString(m_sTooltipKey);    
  }
  
  
  public int getContext()
  {
    return m_iContext;
  }
  
  public void postFinalRealize()
  {    
  }
  
  public TarentWidgetLabel getLabel()
  {
    return m_oLabel;
  }
  
  public TarentWidgetIntegerField getIntegerField()
  {
    return m_oTextfield;
  }
  
  
  public void setDoubleCheckSensitive(boolean issensitive)
  {
    setDoubleCheckSensitive(m_oTextfield, issensitive);
  }
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if (panel == null) panel = createPanel(m_sTooltipKey, m_sLabelKey, m_iMaxLength, widgetFlags);
    
    return panel;
  }

  public String getKey()
  {
    return m_sKey;
  }

  public void setEditable(boolean iseditable)
  {
    if (iseditable)
    {  
      m_oTextfield.setEditable(true);
    }
    else
    {  
      m_oTextfield.setEditable(false);
      m_oTextfield.setBackground(Color.WHITE);
    }
  }

  public void setData(PluginData data)
  {
    m_oTextfield.setIntegerValue(((Integer)(data.get(m_AdressKey))).intValue());
  }

  public void getData(PluginData data)
  {
    data.set(m_AdressKey, new Integer(m_oTextfield.getIntegerValue()));
  }

  public boolean isDirty(PluginData data)
  {
    if (m_oTextfield.getText().length() == 0)
    {
      if ( ((Integer)(data.get(m_AdressKey))).intValue() != 0) return(true);
    }
    else
    {
      try
      {
        if (!(((Integer)(data.get(m_AdressKey))).intValue() == (Integer.parseInt(m_oTextfield.getText())))) return(true);
      }
      catch(NumberFormatException nfe)
      {
        return(true); 
      }
    }
      
    return false;
  }
  
  
  // ---------------------------------------- Field specific Methods ---------------------------------------

  private EntryLayoutHelper createPanel(String tooltipkey, String labelkey, int maxlength, String widgetFlags)
  {    
    m_oTextfield = createTextField(getFieldDescription(), maxlength);
    m_oLabel = new TarentWidgetLabel(getFieldName());     
    return(new EntryLayoutHelper(m_oLabel, m_oTextfield, widgetFlags));
  }

}
