/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import javax.swing.JFrame;

import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.plugin.FrameMailBatchPerformer;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class DispatchEMailPerformer implements FrameMailBatchPerformer
{	
	public DispatchEMailPerformer()
	{
		
	}
	
	public JFrame getFrame()
	{
		return DispatchEMailDialog.getInstance();
	}

	public void execute()
	{
		
	}

	public MailBatch getMailBatch()
	{
		return null;
	}

	public void setMailBatch(MailBatch pMailBatch, MailBatch.CHANNEL pChannel)
	{
		DispatchEMailDialog.getInstance().setMailBatch(pMailBatch, pChannel);
	}
}
