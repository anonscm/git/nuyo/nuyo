package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;
import java.util.Date;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Contact;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.action.AddressListProviderAdapter;
import org.evolvis.nuyo.plugins.office.OfficeExportAction;



public class ContactViaPostPrivateAction extends AbstractConsignmentDependentAction {

	private static final long	serialVersionUID	= 7934474462886388081L;

	public void actionPerformed(ActionEvent e) {
		NonPersistentAddresses addresses = new NonPersistentAddresses();
		addresses.addAddress(ApplicationServices.getInstance().getActionManager().getAddress());
    	OfficeExportAction action = new OfficeExportAction();
    	action.registerDataProvider(new AddressListProviderAdapter(addresses));
    	action.actionPerformed(e);
    	
    	// create journal entry
        ApplicationServices.getInstance().getActionManager().userRequestCreateContactEntryDialog(Messages.getString("GUI_Dispatch_Mail_Subject"), "", new Date(), 0, false, Contact.CATEGORY_MAILING, Contact.CHANNEL_MAIL, 0, 0, null, null);
	}

    protected void init() {
    }
}
