/*
 * Created on 16.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorDisplay;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.OverlayLayout;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ErrorBubble extends JPanel
{  
  // parameters for font-size
  private final static int m_iStdTextFontSize = 10;                   // used for text inserted via "setBubbleText()"
  private final static int m_iErrorHeaderTextFontSize = 12;    // used for Header inserted via "setBubbleError()"
  private final static int m_iErrorBodyTextFontSize = 10;       // used for Body inserted via "setBubbleError()"
  
  private final static Color m_oStdTextColor = Color.BLACK; 
  private final static Color m_oErrorHeaderTextColor = Color.RED; 
  private final static Color m_oErrorBodyTextColor = Color.BLACK; 

  
  // private, internal variables 
  private JTextPane m_oTextPane;
  private JLabel m_oLabel; 
  private ImageIcon m_oImage;

  private JScrollPane m_oScroll;
  private Dimension m_oPanelSize;
  private Dimension m_oTextSize;
  
  public ErrorBubble()
  {
    m_oPanelSize = new Dimension(244, 92);
    m_oTextSize = new Dimension(130, 68);

    this.setLayout(new OverlayLayout(this));
    this.setMinimumSize(m_oPanelSize);
    this.setMaximumSize(m_oPanelSize);
    this.setPreferredSize(m_oPanelSize);
    this.setSize(m_oPanelSize);
    
    String home = System.getProperty("user.home") + File.separator;  
    m_oImage = new ImageIcon(home + "bubble.gif");
    m_oLabel = new JLabel(m_oImage);

    m_oTextPane = new JTextPane();
    m_oTextPane.setEditable(false);

    m_oScroll = new JScrollPane(m_oTextPane);
    m_oScroll.setBorder(null);
    m_oScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    m_oScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    
    this.add(m_oScroll);
    this.add(m_oLabel);
        
    m_oScroll.setOpaque(false);        
    m_oLabel.setOpaque(false);        
    this.setOpaque(false);        
        
    m_oLabel.addComponentListener(new BubbleComponentListener());
    
    BubbleMouseListener bml = new BubbleMouseListener();
    this.addMouseListener(bml);
    this.addMouseMotionListener(bml);    
  }

  public Dimension getBubbleSize()
  {
    return(m_oPanelSize);
  }

  private class BubbleComponentListener implements ComponentListener
  {
    public void componentResized(ComponentEvent e) { setPos(); }
    public void componentMoved(ComponentEvent e) { setPos(); }
    public void componentShown(ComponentEvent e) { setPos(); }
    public void componentHidden(ComponentEvent e) { }
  }

  private void setPos()
  {
    m_oLabel.setLocation(0, 0);
    m_oLabel.setMinimumSize(m_oPanelSize);
    m_oLabel.setMaximumSize(m_oPanelSize);
    m_oLabel.setPreferredSize(m_oPanelSize);
    m_oLabel.setSize(m_oPanelSize);

    m_oScroll.setLocation(m_oLabel.getLocation().x + 77, m_oLabel.getLocation().y + 12);
    m_oScroll.setMinimumSize(m_oTextSize);
    m_oScroll.setMaximumSize(m_oTextSize);
    m_oScroll.setPreferredSize(m_oTextSize);
    m_oScroll.setSize(m_oTextSize);

    m_oScroll.revalidate();
  }
  
  
  public void setBubbleText(String text)
  {
    Document doc = m_oTextPane.getDocument();

    SimpleAttributeSet bodyattr = new SimpleAttributeSet();                 
    StyleConstants.setForeground((MutableAttributeSet)bodyattr, m_oStdTextColor);
    StyleConstants.setBold((MutableAttributeSet)bodyattr, false);
    StyleConstants.setFontSize((MutableAttributeSet)bodyattr, m_iStdTextFontSize);

    try
    {
      doc.insertString(doc.getLength(), text, bodyattr);
    } 
    catch (BadLocationException ble) {}
  }


  public void setBubbleError(ErrorHint errorhint)
  {
    Document doc = m_oTextPane.getDocument();
    
    SimpleAttributeSet headerattr = new SimpleAttributeSet();                 
    StyleConstants.setForeground((MutableAttributeSet)headerattr, m_oErrorHeaderTextColor);
    StyleConstants.setBold((MutableAttributeSet)headerattr, true);
    StyleConstants.setFontSize((MutableAttributeSet)headerattr, m_iErrorHeaderTextFontSize);

    SimpleAttributeSet bodyattr = new SimpleAttributeSet();                 
    StyleConstants.setForeground((MutableAttributeSet)bodyattr, m_oErrorBodyTextColor);
    StyleConstants.setBold((MutableAttributeSet)bodyattr, false);
    StyleConstants.setFontSize((MutableAttributeSet)bodyattr, m_iErrorBodyTextFontSize);

    try
    {
      doc.insertString(doc.getLength(), errorhint.getName() + "\n", headerattr);
      doc.insertString(doc.getLength(), errorhint.getDescription(), bodyattr);
    } 
    catch (BadLocationException ble) {}
  }
  
  
  private class BubbleMouseListener extends MouseInputAdapter
  {
    private Point m_oPos = null;
    private Point m_oOffset = null;

    public void mousePressed(MouseEvent e)
    { 
      m_oOffset = e.getPoint();
    }
    
    public void mouseReleased(MouseEvent e)
    { 
      m_oPos = null;
      m_oOffset = null;
    }
    
    public void mouseDragged(MouseEvent e)
    { 
      if (m_oOffset != null)
      {
        m_oPos = getLocation();
        m_oPos.x += e.getX() - (m_oOffset.x);      
        m_oPos.y += e.getY() - (m_oOffset.y);      
        setLocation(m_oPos);    
      }
    }
    
    public void mouseEntered(MouseEvent e) 
    {
    }
    
    public void mouseExited(MouseEvent e) 
    {     
    }
  }
  
  
}
