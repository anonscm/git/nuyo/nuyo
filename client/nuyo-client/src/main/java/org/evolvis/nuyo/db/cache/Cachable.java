package org.evolvis.nuyo.db.cache;

import org.evolvis.nuyo.db.ContactDBException;



/**
 * @author kleinw
 *
 *	Jede Entit�t kann zus�tzlich dieses Interface implementieren.
 *	Dann werden die Methoden "commit", "delete", etc. sichtbar gemacht.
 *
 */
public interface Cachable {
     
    public void updateInCache() throws ContactDBException;
    public void deleteFromCache() throws ContactDBException;
    
}
