/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.GUIElements;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;

import org.evolvis.nuyo.controls.TarentWidgetIntegerField;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerObjectDescriptor;
import org.evolvis.nuyo.datacontainer.DataContainer.DataContainerVersion;
import org.evolvis.nuyo.datacontainer.DataContainer.ObjectParameter;
import org.evolvis.nuyo.datacontainer.DataContainer.ParameterDescription;
import org.evolvis.nuyo.datacontainer.ErrorHint.ErrorHint;
import org.evolvis.nuyo.datacontainer.ErrorHint.PositionErrorHint;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElementAdapter;



/**
 * TODO: remove if not used in AddressFieldFactory.getGUIElement()!
 * @deprecated not used (only by factory)
 * 
 * @author niko
 */
public class IntegerField extends GUIElementAdapter implements TarentGUIEventListener, FocusListener
{
  private TarentWidgetIntegerField m_oTextField;
  public String getListenerName()
  {
    return "IntegerField";
  }  

  public IntegerField()
  {
    super();
    
    m_oTextField = new TarentWidgetIntegerField();
  }

  public GUIElement cloneGUIElement()
  {
    IntegerField textfield = new IntegerField();
    
    Iterator it = m_oParameterList.iterator();
    while(it.hasNext())
    {
      ObjectParameter param = ((ObjectParameter)(it.next()));
      textfield.addParameter(param.getKey(), param.getValue());
    }
    return textfield;
  }
  
  public DataContainerObjectDescriptor getDataContainerObjectDescriptor()
  {
    DataContainerObjectDescriptor d = new DataContainerObjectDescriptor(DataContainerObjectDescriptor.GUIFIELD, "IntegerField", new DataContainerVersion(0));

    ParameterDescription maxlenpd = new ParameterDescription("maxlen", "maximale Anzahl erlaubter Zeichen", "die maximale Anzahl in diesem Feld erlaubter Zeichen.");
    maxlenpd.addValueDescription("Integer", "ein beliebiger Integer Wert");
    d.addParameterDescription(maxlenpd);
    
    return d;
  }
  

  public boolean addParameter(String key, String value)
  {    
    if ("maxlen".equalsIgnoreCase(key)) 
    {
      try
      {
        int len = Integer.parseInt(value);
        m_oTextField.setLengthRestriction(len);
        m_oParameterList.add(new ObjectParameter(key, value));
        return true;
      }
      catch(NumberFormatException nfe) {}
    }
    return false;
  }

  public boolean canDisplay(Class data)
  {
    return data.isInstance(Integer.class);
  }

  public Class displays()
  {
    return Integer.class;
  }
  

  public TarentWidgetInterface getComponent()
  {
    return m_oTextField;
  }

  public void setData(Object data)
  {
    m_oTextField.setData(((Integer)data));
  }

  public Object getData()
  {
    return(m_oTextField.getData());
  }

  public void focusGained(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSGAINED, getDataContainer(), null));      
  }

  public void focusLost(FocusEvent e)
  {
    fireTarentGUIEvent(new TarentGUIEvent(TarentGUIEvent.GUIEVENT_FOCUSLOST, getDataContainer(), null));      
  }

  public void initElement()
  {
    m_oTextField.addFocusListener(this);
  }

  public void disposeElement()
  {
    m_oTextField.removeFocusListener(this);
  }

  public void setFocus()
  {
    m_oTextField.requestFocus();
  }
 
  public Point getOffsetForError(ErrorHint errorhint)
  {
    int sizex = 0;     
    if (errorhint instanceof PositionErrorHint)
    {
      String text = m_oTextField.getText();
      Graphics2D g2d = (Graphics2D)(m_oTextField.getGraphics());
      int pos = ((PositionErrorHint)errorhint).getPosition();
      for(int i=0; i<pos; i++)
      {        
        Rectangle2D rect = m_oTextField.getFont().getStringBounds(String.valueOf(text.charAt(i)), g2d.getFontRenderContext());
        sizex += rect.getWidth();
      }      
      return(new Point(sizex, 0));
    }    
    return(new Point(0,0));
  }
  
}
