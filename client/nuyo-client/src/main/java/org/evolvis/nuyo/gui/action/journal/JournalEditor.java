

package org.evolvis.nuyo.gui.action.journal;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.evolvis.nuyo.gui.Messages;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.toedter.calendar.JDateChooser;

import de.tarent.commons.ui.EscapeDialog;

/**
 * The {@link JournalEditor} is a dialog for entering journal entry
 * details which can then be uploaded to the database. The latter is
 * done automatically when the dialog's "ok" button is clicked.
 * 
 * <p>Instances of this class shall be reused. When the {@link #show}
 * method returns the dialog is closed and the method's return value
 * denotes which action the user has chosen.</p>
 * 
 * <p>Please note that database interaction is done in a separate
 * thread. You should not expect journal data to be up to date
 * immediately after the dialog got closed.</p>
 * 
 * @author Robert Schuster
 *
 */
public final class JournalEditor
{

  JDialog dialog;

  JTextField address;

  JTextField title;

  JDateChooser date;

  JComboBox category;

  JComboBox channel;

  JTextField user;

  JComboBox direction;

  JTextField duration;

  JTextField subject;

  JTextArea note;

  JButton submit;

  JButton cancel;

  JournalEditorModel model;
  
  int result;

  public JournalEditor(Frame owner)
  {
    dialog = new EscapeDialog(owner,
                              Messages.getString("GUI_JournalEditor_Title"),
                              true);

    init();
  }

  public JournalEditor(Dialog owner)
  {
    dialog = new EscapeDialog(owner,
                              Messages.getString("GUI_JournalEditor_Title"),
                              true);

    init();
  }
  
  /**
   * Initializes the dialog's components from the given properties.
   * 
   * <p>This method is to be called every time the dialog is made
   * visible again.</p>
   * 
   * @param subject
   * @param note
   * @param date
   * @param duration
   * @param inbound
   * @param category
   * @param channel
   */
  public void setup(String subject, String note, Date date, int duration, boolean inbound, int category, int channel)
  {
      address.setText(model.addressLabel);
      user.setText(model.userLoginName);
	  this.subject.setText(subject);
	  this.note.setText(note);
	  this.date.setDate(date);
	  this.duration.setText(String.valueOf(duration));
	  this.direction.setSelectedItem(new Boolean(inbound));
	  this.category.setSelectedItem(new Integer(category));
	  this.channel.setSelectedItem(new Integer(channel));
  }
  
  /**
   * Sets up the dialog with predefined values for a new journal
   * entry.
   * 
   * <p>This method is to be called every time the dialog is made
   * visible again.</p>
   *
   */
  public void setupNewEntry()
  {
	address.setText(model.addressLabel);
	user.setText(model.userLoginName);
	subject.setText("");
	note.setText("");
	duration.setText("15"); // arbitrary default: 15 minutes
	category.setSelectedIndex(0);
	channel.setSelectedIndex(0);
	direction.setSelectedIndex(0);
  }

  private void init()
  {
    model = new JournalEditorModel();
    
    addContents(dialog.getContentPane());

    dialog.pack();

    dialog.setLocationRelativeTo(null);

  }

  private void addContents(Container comp)
  {
    FormLayout l = new FormLayout(
                                  "3dlu, pref, 3dlu, pref:grow, 40dlu, pref, 3dlu, pref:grow, 3dlu",
                                  "3dlu, pref, 6dlu, pref, 3dlu, pref, 3dlu, pref, 6dlu, pref, 3dlu, fill:pref:grow, 12dlu, pref, 3dlu");

    l.setColumnGroups(new int[][] { { 4, 8 } });
    comp.setLayout(l);

    CellConstraints cc = new CellConstraints();

    address = new JTextField();
    address.setEditable(false);
    comp.add(address, cc.xyw(2, 2, 7));

    comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Date")), cc.xy(2, 4));
    date = new JDateChooser();
    comp.add(date, cc.xy(4, 4));

    comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Category")),
             cc.xy(2, 6));
    category = new JComboBox(model.categoryListModel);
    category.setRenderer(model.categoryListRenderer);
    comp.add(category, cc.xy(4, 6));

    comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Channel")),
             cc.xy(2, 8));
    channel = new JComboBox(model.channelListModel);
    channel.setRenderer(model.channelListRenderer);
    comp.add(channel, cc.xy(4, 8));

    comp.add(new JLabel(Messages.getString("GUI_JournalViewer_User")), cc.xy(6,
                                                                             4));
    user = new JTextField(10);
    user.setEditable(false);
    comp.add(user, cc.xy(8, 4));

    comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Direction")),
             cc.xy(6, 6));
    direction = new JComboBox(model.directionListModel);
    direction.setRenderer(model.directionListRenderer);
    comp.add(direction, cc.xy(8, 6));

    comp.add(new JLabel(Messages.getString("GUI_JournalViewer_Duration")),
             cc.xy(6, 8));
    duration = new JTextField();
    comp.add(duration, cc.xy(8, 8));

    comp.add(new JLabel(Messages.getString("GUI_JournalEditor_Subject")),
             cc.xy(2, 10));
    subject = new JTextField();
    comp.add(subject, cc.xyw(4, 10, 5));

    note = new JTextArea(10, 5);
    note.setLineWrap(true);
    note.setWrapStyleWord(true);
    comp.add(new JScrollPane(note), cc.xyw(2, 12, 7));

    submit = new JButton(Messages.getString("GUI_JournalEditor_Submit"));
    
    // submit-button should react on "return"
    dialog.getRootPane().setDefaultButton(submit);
    
    submit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {

        model.date = date.getDate();

        model.category = ((Integer) category.getSelectedItem()).intValue();
        model.channel = ((Integer) channel.getSelectedItem()).intValue();
        model.inbound = ((Boolean) direction.getSelectedItem()).booleanValue();
        model.subject = subject.getText();
        model.note = note.getText();
        
        try
          {
            model.duration = new Integer(duration.getText()).intValue();
          }
        catch (NumberFormatException e)
          {
            // TODO: Add some error handling here
          }

        model.createEntry();
        
        result = JOptionPane.OK_OPTION;

        dialog.setVisible(false);
      }
    });

    cancel = new JButton(Messages.getString("GUI_JournalEditor_Cancel"));
    cancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        result = JOptionPane.CANCEL_OPTION;

        dialog.setVisible(false);
      }
    });

    PanelBuilder pb = new PanelBuilder(new FormLayout("pref, 3dlu:grow, pref",
                                                      "pref"));
    pb.add(submit, cc.xy(1, 1));
    pb.add(cancel, cc.xy(3, 1));

    comp.add(pb.getPanel(), cc.xyw(2, 14, 7));

  }

  /** Displays the {@link JournalEditor} and blocks execution
   * until it is closed (via its own UI components).
   * 
   * <p>The return value is either {@link JOptionPane.OK_OPTION}
   * or {@link JOptionPane.CANCEL_OPTION} denoting that either a
   * new entry has been created or the journal entry creation was
   * canceled</p>
   * 
   * @return
   */
  public int show()
  {
    dialog.setVisible(true);
    
    return result;
  }

}
