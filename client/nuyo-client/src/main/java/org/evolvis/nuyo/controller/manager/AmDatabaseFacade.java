/* $Id: AmDatabaseFacade.java,v 1.24 2007/08/30 16:10:28 fkoester Exp $
 * 
 * Created on 08.03.2004
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke, Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.controller.manager;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ContactException;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.NonPersistentAddresses;
import org.evolvis.nuyo.db.Schedule;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressPluginData;
import org.evolvis.nuyo.util.general.ContactLookUpNotFoundException;
import org.evolvis.nuyo.util.general.DataAccess;

import de.tarent.commons.config.Config;



/**
 * Diese Klasse liefert eine Datenbank-Facade, auf die der
 * {@link de.tarent.contact.controller.ActionManager ActionManager} aufsetzt.
 * 
 * @author mikel
 */
/**
 * @author niko
 *
 */
public class AmDatabaseFacade extends AmCommonDialogs {
	
    //
    // zur Datenbank durchgereichte Methoden
    //
    /**
     * Diese Methode erzeugt ein tempor�res Address-Sammelobjekt.
     * 
     * @return ein nicht persistentes leeres Addresses-Objekt.
     */
    public NonPersistentAddresses createNonPersistentAddresses() {
        return new NonPersistentAddresses(getDb());
    }

    /**
     * Diese Methode liefert eine Addresses-Instanz auf Basis der Einschr�nkungen,
     * die �ber den aktuellen Benutzer und als Parameter gegeben sind.<br>
     * Derzeit werden nicht die generischen Filter, sondern nur die Suchfilter beachtet.
     * 
     * @param filters eine Sammlung von Filtern (z.B. Kategorien, Unterkategorien, Versandauftr�ge,...)
     * @param criteria eine Map von Suchfeldern und -Strings; die Schl�ssel sind die Feldkonstanten aus
     *  {@link Address}, also STD_FIELD_* und CAT_FIELD_*, und zus�tzlich "" f�r die Suche in allen Feldern;
     *  die Such-Strings sind die zu suchenden Feldinhalte, wobei '*' als Jokerzeichen erlaubt ist.
     * @return Sammlung der Adressen, die den Filtern und Suchkriterien gen�gen.
     */
    public Addresses getAddresses(Collection filters, Map criteria) {
        if (getDb() != null) try {
            return getDb().getAddresses(filters, criteria);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Addresses_Get_DbError"), new Object[]{filters, criteria}, sex);
        }
        return null;
    }

    /**
     * Liefert eine angegebene Adresse
     * 
     * @param addrNr ID der Adresse.
     */
    public Address getAddress(int addrNr) {
        if (getDb() != null) try {
            return getDb().getAddress(addrNr);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Address_Get_DbError"), new Object[]{new Integer(addrNr)}, sex);
        }
        return null;
    }
    
    /**
     * Diese Methode liefert die Kollektion aller Kategorien als Abbildung von
     * Kategorieschl�sseln auf Kategorienamen.
     * 
     * @return eine SortedMap aller Kategorien.
     */
    public Map getAllCategories() {
        if (getDb() != null) try {
            return getDb().getAllCategories();
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Category_Get_DbError"), sex);
        }
        return new TreeMap();
    }
    
    /**
     * Diese Methode l�dt die Liste der Kategorien neu vom Server
     * @throws ContactDBException
     */
    
    public void reloadCategories() throws ContactDBException{
    	if (getDb() != null) try {
            getDb().reloadCategories();
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Category_Refresh"), sex);
        }
    }


    /**
     * Diese Methode liefert die Kollektion aller Kategorien als Liste
     * @return eine Liste aller Kategorien (db.Category), in der Sortierung, mit der sie angezeigt werden sollen.
     */
    public List getCategoriesObjectList() {
        if (getDb() != null) try {
            return getDb().getCategoriesList();
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Category_Get_DbError"), sex);
        }
        return new ArrayList();
    }
    
    
    
    /**
     * Diese Methode liefert die Kollektion aller mit der akteuellen Adressauswahl verkn�pften Kategorien als Liste
     * @param edit 
     * @param add
     * @param remove
     * @param addSubCat
     * @param removeSubCat
     * @param structure
     * @param grant
     * @param includeVirtual inlude or exlude virtual categories
	 * @return list of category objects of all categories that are connected to the selected addresses and have proper rights
     */
    
    public List getAssociatedCategoriesForAddressSelection(boolean read, boolean edit, boolean add, boolean remove, boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeVirtual) {
    		if (getDb() != null) try {
            return getDb().getAssociatedCategoriesForAddressSelection(ApplicationServices.getInstance().getActionManager().getAddresses().getPkList(), read, edit, add, remove, addSubCat, removeSubCat, structure, grant, includeVirtual);
        } catch (ContactDBException ex) {
            publishError(getMessageString("AM_Category_Get_DbError"), ex);
        }
        return new ArrayList();
    }
    
    /**
     * 
     * @return  category objects of all categories that are connected to the selected address
     */
    
    public List getAssociatedCategoriesForSelectedAddress(){
    	 	if (getDb() != null) try {
             return getDb().getAssociatedCategoriesForSelectedAddress(new Integer(ApplicationServices.getInstance().getActionManager().getAddress().getId()));
         } catch (ContactDBException ex) {
             publishError(getMessageString("AM_Category_Get_DbError"), ex);
         }
         return new ArrayList();
    }
    
    public List getAssociatedCategoriesForAddressSelection(boolean edit, boolean add, boolean remove, boolean addSubCat, boolean removeSubCat, boolean structure, boolean grant, boolean includeVirtual) throws ContactDBException{
    		return getDb().getAssociatedCategoriesForAddressSelection(ApplicationServices.getInstance().getActionManager().getAddresses().getPkList(), true, edit, add, remove, addSubCat, removeSubCat, structure, grant, includeVirtual);    	
    }
    
    
    public List getAssociatedCategoriesForAddressSelection(){
    		return getAssociatedCategoriesForAddressSelection(false, false, false, false, false, false, false, false, true);    	    
    }
    
   
    
public List getUserCategoriesWithCategoryRight(int right, boolean includeVirtual){
    	
    
    	List kategorieList = new ArrayList();
        Map tmpList = getAllCategories();
        
        Map.Entry entry;
        Category category;
        
        try {
			for (Iterator it = tmpList.entrySet().iterator(); it.hasNext();){
				entry = (Map.Entry)it.next();
				category = (Category) entry.getValue();
				 
				if (getDb().userHasRightOnFolder(getCurrentUser(), right, category.getId())){
					
					if (includeVirtual || (!includeVirtual && !category.getVirtual().booleanValue()))
					
						kategorieList.add(category);
						
					//System.out.println("Kategorie " + katname + " wird angezeigt in Combobox");
				}
				else{
					//System.out.println("Kategorie " + katname + " wird nicht angezeigt in Combobox");
				}
			}
		} catch (ContactDBException e) {
			publishError("Fehler beim auslesen der Kategorie-IDs.", e);
			e.printStackTrace();
		}
		
		Collections.sort(kategorieList, new CategoryComparator());
		
        return kategorieList; 
        
    }

	


	static class CategoryComparator implements Comparator {
		
		public int compare(Object o1, Object o2){
		    String s1 = "";
			String s2 = "";
			s1 = ((Category) o1).getName().toLowerCase();
			s2 = ((Category) o2).getName().toLowerCase();
		
		    return Collator.getInstance().compare(s1,s2);
	  	}
	}

    
    /**
     * Holt alle Kategorien, auf die der User das Recht AUTH_GRANT hat
     * @see #getCategoriesObjectList()
     * @return Liste mit Kategorien
     */
    public List getGrantableCategoriesObjectList(){
        if (getDb() != null) try {
            return getDb().getGrantableCategoriesList();
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Category_Get_DbError"), sex);
        }
        return new ArrayList();    	
    }

    /**
     * Diese Methode liefert die Kollektion aller Benutzer als Abbildung
     * von Benutzer-ID auf Benutzernamen.
     * 
     * @return eine SortedMap aller Benutzer.
     */
    public SortedMap getBenutzer() {
        if (getDb() != null) try {
            return getDb().getUserNames();
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_User_Get_DbError"), sex);
        }
        return new TreeMap();
    }

    /**
     * Diese Methode liefert zu einer PLZ das zugeh�rige Bundesland.
     * 
     * @param plz die Postleitzahl, die zu testen ist.
     * @return der Name des Bundeslandes, zu dem die PLZ geh�rt,
     *  oder <code>null</code>, falls es nicht feststellbar ist.
     * @see org.evolvis.nuyo.db.Database#getBundeslandForPLZ(int)
     */
    public String getBundeslandForPLZ(String plz) {
        if (getDb() != null) try {
            return getDb().getBundeslandForPLZ(plz);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_State_GetByZip_DbError"), new Object[]{new Integer(plz)}, sex);
        }
        return null;
    }

    /**
     * Diese Methode liefert zu einer PLZ die zugeh�rige Stadt.
     *  
     * @param plz die Postleitzahl, die zu testen ist.
     * @return der Name der Stadt, zu dem die PLZ geh�rt,
     *  oder <code>null</code>, falls es nicht feststellbar ist.
     */
    public String getCityForPLZ(String plz) {
        if (getDb() != null) try {
            return getDb().getCityForPLZ(plz);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_City_GetByZip_DbError"), new Object[]{new Integer(plz)}, sex);
        }
        return null;
    }

    /**
     * Diese Methode liefert die Kommunikationsdatentypen, die von der DB-Umsetzung
     * abh�ngen k�nnen, als Abbildung von Identifikatoren auf Feldbeschreibungen
     * vom Typ {@link org.evolvis.nuyo.db.Database.CommunicationDescription CommunicationDescription}.
     * 
     * @return Abbildung mit den Kommunikationsdatenbeschreibungen.
     */
    public SortedMap getCommunicationFields() {
        if (getDb() != null) try {
            return getDb().getCommunicationFields();
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Communication_GetFields_DbError"), sex);
        }
        return new TreeMap();
    }

    /**
     * Diese Methode liefert das Login des aktuellen Benutzers, f�r den
     * die Datenquellenanmeldung durchgef�hrt wurde.
     * 
     * @return Login des aktuellen Benutzers
     */
    public String getCurrentUser() {
        if (getDb() != null)
            return getDb().getUser();
        return null;
    }

    /**
     * Diese Methode liefert den Wert eines Features der Datenquelle.
     * 
     * @param feature Feature-Identifikator
     * @return Wert des Features in der aktuellen Datenquelle
     * @see org.evolvis.nuyo.gui.GUIListener#getDatabaseFeature(java.lang.String)
     */
    public Object getDatabaseFeature(String feature) {
        if (getDb() != null)
            return getDb().getFeature(feature);
        return null;
    }

    /**
     * Diese Methode liefert die Datenquellenversion.
     * 
     * @return Versionsangabe der Datenquelle oder "unknown"
     */
    public String getDatabaseVersion() {
        if (getDb() != null) try {
            return getDb().getVersion(Config.APPLICATION_VERSION);
        } catch (ContactDBException e) {
            logger.log(Level.WARNING, getMessageString("AM_DbVersion_Get_DbError"), e);
        }
        return "unknown";
    }

    /** Liefert eine Darstellbare Ausgabe der Server URL */
    public String getDisplaySource() {
        try {
            return getDb().getDisplaySource();
        } catch (ContactDBException e) {
            return "unknown";
        }
    }
    

    /**
     * Diese Methode liefert den Terminplan eines Benutzers.
     * 
     * @param userId Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Terminplan des Benutzers oder <code>null</code>, falls
     *  diese Funktionalit�t nicht vorliegt.
     */
    public Schedule getSchedule(String userId) {
        if (userId == null ){
            userId = getCurrentUser();
        }
        if (getDb() != null) try {
            return getDb().getSchedule(userId);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Schedule_GetByUser_DbError"), new Object[]{userId}, sex);
        }
        return null;
    }

    /**
     * Diese Methode liefert die ID der Standardkategorie des angegebenen Benutzers.
     * 
     * @param user Benutzer, dessen StandardKategorie gefragt ist;
     *  falls <code>null</code>, wird der aktuell eingelogte genommen.
     * @return Schl�ssel der StandardKategorie des Benutzers.
     */
    public String getStandardCategory(String user) {
        if (getDb() != null) try {
            return getDb().getStandardCategory(user);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_UserAttribute_Get_DbError"), new Object[]{"Standard Category", user}, sex);
        }
        return "";
    }

    /**
     * Diese Methode liefert eine Map der Benutzer-Logins auf zugeh�rige
     * {@link User}-Instanzen.
     * 
     * @return Map Benutzerlogins auf User-Instanzen.
     */
    public Collection getUserGroups() {
        if (getDb() != null) try {
            return getDb().getUserGroups();
        } catch (ContactDBException se) {
            publishError(getMessageString("AM_UserGroups_Get_DbError"));
        }
        return Collections.EMPTY_SET;
    }

    /**
     * Diese Methode liefert eine Map der Benutzer-Logins auf zugeh�rige
     * {@link User}-Instanzen.
     * 
     * @return Map Benutzerlogins auf User-Instanzen.
     */
    public Map getUsers() {
        if (getDb() != null) try {
            return getDb().getUsers();
        } catch (ContactDBException se) {
            publishError(getMessageString("AM_UserObjects_Get_DbError"));
        }
        return Collections.EMPTY_MAP;
    }

    /**
     * Diese Methode liefert zu dem angegebenen Benutzer (Default: der
     * eingelogte Benutzer) eine User-Instanz.
     * 
     * @param userLogin Login-Name des Benutzers; <code>null</code> referenziert den
     *  aktuell eingelogten Benutzer.
     * @return User-Instanz zum angegebenen Benutzer oder <code>null</code>, wenn
     *  es keinen Benutzer zum angegebenen Login gibt.
     */
    public User getUser(String userLogin) {
        if (getDb() != null) try {
        	return getDb().getUser(userLogin == null ? getDb().getUser() : userLogin);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_UserObject_Get_DbError"), new Object[]{userLogin}, sex);
        }
        return null;
    }
    
    public User getUser(String userLogin, boolean enableChaching) {
        if (getDb() != null) try {
        	return getDb().getUser(userLogin == null ? getDb().getUser() : userLogin , enableChaching);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_UserObject_Get_DbError"), new Object[]{userLogin}, sex);
        }
        return null;
    }

    /**
     * Diese Methode liefert den Namen eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Name des Benutzers oder <code>null</code>, falls
     *  dieser Benutzer nicht existiert.
     */
    public String getUserName(String userid) {
        if (getDb() != null) try {
            return getDb().getUserName(userid == null ? getDb().getUser() : userid);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_UserAttribute_Get_DbError"), new Object[]{"Name", userid}, sex);
        }     
        return null;
    }

    /**
     * Speichert die Farbe eines Kaleders auf dem Server
     *
     * @param kalenderID Id des Kalenders
     * @param colorCode String mit dem Farbcode
     */
    public void setCalendarColor(int kalenderID, String colorCode) {
        String key = "CalendarColor_ID_"+kalenderID;
        calendarColors.put(key, colorCode);
        setUserParameter(null, key, colorCode);
    }


    /**
     * Liefert die Farbe eines Kaleders auf dem Server
     *
     * @param kalenderID Id des Kalenders
     * @return Farbe des Kalenders als RGB-Code
     */
    public String getCalendarColor(int kalenderID) {
        String key = "CalendarColor_ID_"+kalenderID;
        if (calendarColors.get(key) == null) {
            calendarColors.put(key, getUserParameter(null, key));
        }
        return (String)calendarColors.get(key);
    }
     

    /**
     * Diese Methode liefert einen Parameter eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @param key Schl�ssel des Parameters.
     * @return Parameter des Benutzers oder <code>null</code>, falls
     *  dieser Benutzer nicht existiert.
     */
    public String getUserParameter(String userid, String key) {
        if (getDb() != null) try {
            return getDb().getUserParameter(userid == null ? getDb().getUser() : userid, key);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Parameter_GetByUserKey_DbError"), sex);
        }
        return null;
    }

    /**
     * Diese Methode setzt einen Parameter eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @param key Schl�ssel des Parameters.
     * @param value neuer Wert des Parameters oder <code>null</code>, falls
     *  der Parameter gel�scht werden soll.
     */
    public void setUserParameter(String userid, String key, String value) {
        if (getDb() != null) try {
            getDb().setUserParameter(userid == null ? getDb().getUser() : userid, key, value);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Parameter_SetByUserKey_DbError"), sex);
        }
    }
    
    /**
     * Diese Methode setzt den EinladungsStatus.
     * 
     * @param eventId
     *            Id des events
     * @param statusId
     *            Einladungsstatus
     * @return Erfolgsflag
     */
    public void setEventStatus(int userId, int eventId, int statusId) {
        if (getDb() != null) try {
            getDb().setEventStatus(userId, eventId,statusId);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Parameter_SetMYEventStatus_DbError"), sex);
        }
    }
    
    

    /**
     * Diese Methode liefert das isAdmin-Flag eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Admin-Flag des Benutzers
     */
    public boolean isAdmin(String userid) {
        if (getDb() != null) try {
            return getDb().isUserAdmin(userid == null ? getDb().getUser() : userid);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_UserAttribute_Get_DbError"), new Object[]{"isAdmin", userid}, sex);
        }
        return false;
    }
    
    /**
     * Diese Methode liefert das Special-Flag eines Benutzers.
     * 
     * @param userid Id des Benutzers; wenn sie <code>null</code> ist,
     *  so ist der aktuell angemeldete Benutzer gemeint. 
     * @return Special-Flag des Benutzers
     */
    public boolean isSpecial(String userid) {
        if (getDb() != null) try {
            return getDb().isUserSpecial(userid == null ? getDb().getUser() : userid);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_UserAttribute_Get_DbError"), new Object[]{"isSpecial", userid}, sex);
        }
        return false;
    }


    /**
     * Liefert die Unterkategorien, die im Verteilerchooser angezeigt werden
     * sollen. Die Keys sind die Unterkategorieschl�ssel und die Values sind
     * die lesbaren Namen
     * 
     * @param kategorie wenn <code>null</code>, so ist die aktuelle Kategorie
     *  gemeint.
     * @return Map mit String Keys, und String Values; Bei einem DB Fehler wird
     *  null zur�ck gegeben.
     */
    public SortedMap getVerteiler(String kategorie) {
        if (getDb() != null) try {
            return getDb().getVerteiler(kategorie);
        } catch (ContactDBException se) {
            publishError(getMessageString("AM_SubCategory_Get_DbError"), new Object[]{kategorie});
        }
        return new TreeMap();
    }


    /**
     * Diese Methode liefert die Kollektion der Kategorien, die der
     * angegebene Benutzer sieht, als Abbildung von Kategorieschl�sseln
     * auf Kategorieobjekte.
     *
     * Von Sebastian:
     * Ich habe das Interface auf Map ge�ndert! Die Sortierung der Verteiler in dieser Map ist egal!!
     * f�r eine Sortierte Liste gibt es die Funktion getKategorienList()
     *
     * 
     * @param user Benutzer, dessen Gruppen abgefragt werden; wenn der
     *  Eintrag <code>null</code> ist, wird der aktuell eingelogte benutzt.
     * @return SortedMap der Kategorien des Benutzers.
     */
    public Map getCategories(String user) {
        if (getDb() != null) try {
            return getDb().getCategories(user);
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Category_GetByUser"), new Object[]{user}, sex);
        }
        return new TreeMap();
    }



    //
    // Getter und Setter der gepufferten Daten
    //
    /**
     * Diese Methode liefert eine nicht �nderbare Liste der Anredetexte.
     * 
     * @return Liste der Anredetexte
     */
    public List getAnredetexteList() {
        if (null == anreden)
            logger.warning(getMessageString("AM_Salutations_Invalid"));
        return Collections.unmodifiableList(anreden);
    }

    /**
     * Diese Methode liefert eine nicht �nderbare Liste der Bundesl�nder.
     * 
     * @return Liste der Bundesl�nder
     */
    public List getBundeslaenderList() {
        if (null == bundeslaender)
            logger.warning(getMessageString("AM_States_Invalid"));
        return Collections.unmodifiableList(bundeslaender);
    }

    /**
     * Diese Methode liefert eine nicht �nderbare Liste der L�nder.
     * 
     * @return Liste der L�nder
     */
    public List getLaenderList() {
        if (null == laender)
            logger.warning(getMessageString("AM_Countries_Invalid"));
        return Collections.unmodifiableList(laender);
    }

    /**
     * Diese Methode liefert eine nicht �nderbare Liste der Landeskennzeichen.
     * 
     * @return Liste der Landeskennzeichen
     */
    public List getLKZList() {
        if (null == lkz)
            logger.warning(getMessageString("AM_LKZ_Invalid"));
        return Collections.unmodifiableList(lkz);
    }

    /**
     * Diese Methode liefert eine nicht �nderbare Liste der
     * Kategorienschl�ssel.
     * 
     * @return Liste der Kategorienschl�ssel
     */
    public List getCategoriesList() {
        if (null == kategorienKeys)
            logger.warning(getMessageString("AM_Categories_Invalid"));
        return Collections.unmodifiableList(kategorienKeys);
    }

    //
    // weitere gepufferte Zugriffsmethoden
    //
    /**
     * Diese Methode liefert den Index der �bergebenen Anrede.
     * 
     * @param anrede Anredetext
     * @return Index der �bergebenen Anrede.
     */
    public int getIndexOfAnrede(String anrede) {
        int index = anreden.indexOf(anrede);
        return (index > -1) ? index : 0;
    }

    /**
     * Liefert den Index eines Bundeslandes in der Liste der Bundesl�nder. Es
     * besteht die implizite Annahme, dass der erste Eintrag (Index 0) Default
     * ist.
     * 
     * @param bundesland das Bundesland, dessen Index gesucht wird.
     * @return Index des �bergebenen Bundeslands; wenn das
     *  Bundesland nicht gefunden wird, wird 0 zur�ckgegeben.
     * @see #getBundeslaenderList()
     */
    public int getIndexOfBundesland(String bundesland) {
        int index = bundeslaender.indexOf(bundesland);
        return (index > -1) ? index : 0;
    }

    /**
     * Liefert den Index eines Landes in der Liste der L�nder. Es besteht die
     * implizite Annahme, dass der erste Eintrag (Index 0) Default ist.
     * 
     * @param land das Land, dessen Index gesucht wird
     * @return Index des �bergebenen Landes
     * @see #getLaenderList()
     */
    public int getIndexOfLand(String land) {
        int index = laender.indexOf(land);
        return (index > -1) ? index : 0;
    }

    /**
     * Diese Methode liefert die ID der aktuellen Kategorie.
     *  
     *  @deprecated Es gibt keine "ausgewaehlte Kategorie" mehr
     * @return Name Schl�ssel der aktuellen Kategorie 
     */
    public String getNameOfSelectedCategory() {
        return getDb().getCategory();
    }

    /**
     * Liefert das Objekt zu dem Key einer Kategorie
     *
     */
    public Category getCategory(String key) {
        try {
            return getDb().getCategory(key);        
        } catch (ContactDBException sex) {
            publishError(getMessageString("AM_Category_Get_DbError"), sex);
            return null;
        }
    }

    /**
     * Diese Methode liefert den Index der aktuellen Kategorie in der gepufferten
     * Kategorienliste.
     *  
     * @return index Index der aktuellen Kategorie 
     */
    public int getSelectedCategory() {
        Object key = getCategories(null).get(getDb().getCategory());
        return getCategoriesObjectList().indexOf(key);
    }

    
    private String[] convertListToStringArray(List list)
    {
      String[] texte = new String[list.size()];
      for(int i=0; i<(list.size()); i++)
      {
        texte[i] = (String)(list.get(i));
      }
      return texte;      
    }
    
    
    /**
     * Diese Methode liest eine Tabelle 
     * mit LookUp-Daten aus der Datenbank
     * (z.B. f�r Combo-Boxen)
     */
    //TODO: IMPLEMENT
    public String[] getLookupTableEntries(String key)
    {
      if (key.equalsIgnoreCase("tc.lut.salutations"))
      {
        return convertListToStringArray(getAnredetexteList());
      }
      else if (key.equalsIgnoreCase("tc.lut.regions"))
      {
        return convertListToStringArray(getBundeslaenderList());
      }
      else if (key.equalsIgnoreCase("tc.lut.countrys"))
      {
        return convertListToStringArray(getLaenderList());
      }
      else if (key.equalsIgnoreCase("tc.lut.lkz"))
      {
        return convertListToStringArray(getLKZList());
      }
      
      return null;
    }

    
    public DataAccess getDataAccess()
    {
      return getControlListener().getDataAccess();
    }
    
    
    
    private class LookupTableNotFoundException extends ContactException
    {
      public LookupTableNotFoundException()
      {
        super(0);
      }      
    }
    
    /**
     * Diese Methode liefert einen Wert aus  
     * einer LookUp-Tabelle
     */    
    public Object getLookupTableValue(String lookuptablekey, Object key) throws ContactLookUpNotFoundException
    {
      if (lookuptablekey.equalsIgnoreCase("tc.lut.plz2region"))
      {
        if (key instanceof String)
        {
          try 
          {
            return getBundeslandForPLZ((String)key);
          }
          catch (NumberFormatException nfe) {}
        }
        return null;
      }
      if (lookuptablekey.equalsIgnoreCase("tc.lut.plz2city"))
      {
        if (key instanceof String)
        {
          try 
          {
            return getCityForPLZ((String)key);
          }
          catch (NumberFormatException nfe) {}
        } 
        return null;
      }
      else if (lookuptablekey.equalsIgnoreCase("tc.index.region"))
      {
        if (key instanceof String)
        {
          return new Integer(getIndexOfBundesland((String)key));
        } 
        return null;
      }
      else if (lookuptablekey.equalsIgnoreCase("tc.index.salutation"))
      {
        if (key instanceof String)
        {
          return new Integer(getIndexOfAnrede((String)key));
        } 
        return null;
      }
      else if (lookuptablekey.equalsIgnoreCase("tc.index.country"))
      {
        if (key instanceof String)
        {
          return new Integer(getIndexOfLand((String)key));
        } 
        return null;
      }
      
      throw new ContactLookUpNotFoundException(ContactLookUpNotFoundException.EX_LOOKUP_NOT_FOUND);
      //return null;
    }


    //TODO: Simon: implement
    private String[] m_saPreviewColumnDbIDs = new String[]{"VORNAME", "NACHNAME", "STRASSE", "PLZ", "city", "EMAIL"}; 
    private String[] m_saPreviewColumnAdressIDs = new String[]{"tc.std.VORNAME", "tc.std.NACHNAME", "tc.std.STRASSE", "tc.std.PLZ", "tc.std.ORT", "tc.std.EMAIL"}; 

    public String[] getPreviewColumnDbIDs()
    {      
      return m_saPreviewColumnDbIDs;
    }
    
    public void setPreviewColumnDbIDs(String[] ids)
    {      
      m_saPreviewColumnDbIDs = ids; 
    }
    
    public String[] getPreviewColumnAdressIDs()
    {      
      return m_saPreviewColumnAdressIDs;
    }
    
    public void setPreviewColumnAdressIDs(String[] ids)
    {      
      m_saPreviewColumnAdressIDs = ids; 
    }
    
    public String getPreviewColumnName(String id)
    {      
      String name = AddressPluginData.getName(id);
      if (name == null) return "_" + id + "_";
      return name;
    }
    
    public List getAvailablePreviewRows(){
        if (getDb() != null) try {
            return getDb().getAvailablePreviewRows();
        } catch (ContactDBException sex) {
            publishError("Fehler beim Abholen der m�glichen Preview Spalten");
        }
        return null;
    }
	
	public List getGlobalRoles(){
		
        if(getDb() != null){
			try{
				return getDb().getGlobalRoles();
			}catch(ContactDBException ce){
				publishError("Fehler beim Abholen der Globalen Rollen", ce);
			}
		}
		return null;
	}
    
	public List getFolderRoles(){
		if(getDb() != null){
			try{
				return getDb().getObjectRoles();
			}catch(ContactDBException ce){
				publishError("Fehler beim Abholen der Globalen Rollen", ce);
			}
		}
		return null;
	}
    //
    // Refresh-Methoden f�r gepufferte Daten
    //
    /**
     * Diese Methode liest statische und semi-statische Inhalte aus der
     * Datenbank, um sie hier zu puffern. Dies betrifft die Liste der Anreden,
     * L�nder, Landeskennzeichen, Bundesl�nder und alle Listen, die in
     * {@link #updateLists()} bezogen werden.
     */
    protected void fetchLists() throws ContactDBException {
        updateLists();

        SortedMap anredenMap = getDb().getAnreden();
        anredenMap.put("0", "");
        anreden = new ArrayList(anredenMap.values());
        replaceNullWithEmtyString(anreden);
        Collections.sort(anreden);

        SortedMap laenderMap = getDb().getLaender();
        laender = new ArrayList(laenderMap.values());
        replaceNullWithEmtyString(laender);
        Collections.sort(laender);

        SortedMap lkzMap = getDb().getLKZen();
        lkz = new ArrayList(lkzMap.values());
        replaceNullWithEmtyString(lkz);

        SortedMap bundeslaenderMap = getDb().getBundeslaender();
        bundeslaender = new ArrayList(bundeslaenderMap.values());
        replaceNullWithEmtyString(bundeslaender);
        Collections.sort(bundeslaender);
    }

    /**
     * Diese Methode liest semi-statische Inhalte aus der Datenbank, um sie
     * hier zu puffern. Dies betrifft die Liste der Kategorienschl�ssel.
     */
    protected void updateLists() throws ContactDBException {
        List kategorien = getDb().getCategoriesList();
        kategorienKeys = new ArrayList(kategorien.size());
        for (Iterator iter = kategorien.iterator(); iter.hasNext();) {
            kategorienKeys.add(((Category)iter.next()).getIdAsString());
        }
    }

    /** Message-Strings */
    protected String getMessageString(String key) {
	    if (getControlListener() != null)
	        return getControlListener().getMessageString(key);
	    else
	        return Messages.getString(key);
	}

	//
    // private Hilfsmethoden
    //
    /**
     * Diese Methode ersetzt in einer Liste <code>null</code> -Eintr�ge durch
     * Leerstrings.
     * 
     * @param list zu bearbeitende Liste
     */
    private static void replaceNullWithEmtyString(List list) {
        for (int i = 0; i < list.size(); i++)
            if (list.get(i) == null) list.set(i, "");
    }

    //
    // private Membervariablen
    //
    /** Liste der bekannten Anreden */
    private List anreden = null;

    /** Liste der Bundesl�nder */
    private List bundeslaender = null;

    /** Liste der L�nder */
    private List laender = null;

    /** Liste der Landeskennzeichen */
    private List lkz = null;

    /** Liste der Kategorienschl�ssel */
    private List kategorienKeys = null;
    
    /** Farben, mit denen die Kalender angezeigt werden sollen. */
    private Map calendarColors = new HashMap();
}
