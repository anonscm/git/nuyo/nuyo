/*
 * Created on 14.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.ErrorHint;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.GUIElementInterfaces.GUIElement;


/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ErrorHintAdapter implements ErrorHint
{
  private String m_sName;
  private String m_sDescription;
  private Exception m_oException;  
  private DataContainer m_oDataContainer;
  private GUIElement m_oGUIElement;
  
  public ErrorHintAdapter(String name, String description, Exception e, GUIElement oGUIElement, DataContainer datacontainer)
  {
    m_sName = name;
    m_sDescription = description;
    m_oException = e;  
    m_oGUIElement = oGUIElement;
    m_oDataContainer = datacontainer;
  }
    
  public GUIElement gertGUIElement()
  {
    return m_oGUIElement;
  }
  
  public String getName()
  {
    return m_sName;
  }

  public String getDescription()
  {
    return m_sDescription;
  }

  public Exception getException()
  {
    return m_oException;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }

  public void setName(String name)
  {
    m_sName = name;
  }

  public void setDescription(String description)
  {
    m_sDescription = description;
  }

  public void setException(Exception e)
  {
    m_oException = e;
  }

  public void setDataContainer(DataContainer datacontainer)
  {
    m_oDataContainer = datacontainer;
  }



}
