/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Nikolai Ruether. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/ 
package org.evolvis.nuyo.plugins.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.evolvis.nuyo.logging.TarentLogger;


/**
 * A collapsing menu item that consists of another menu items.<br>
 * 
 * @see org.evolvis.nuyo.gui.MainFrameMemus#createMenuPanelNavigation
 * <p>
 * @author niko
 * @deprecated Calendar plugin uses it. Remove it when rewritten.
 */
class CollapsingMenuItemContainer extends JPanel implements CollapsingMenuItem
{
  private static final TarentLogger logger = new TarentLogger(CollapsingMenuItemContainer.class);
  private static final long serialVersionUID = 1L;
  private JButton    imageButton;
  private JComponent m_oComponent;
  private Color      m_oNormalColor;
  private Color      m_oHighlightColor;
  private Color      m_oForegroundColor;
  private Object     m_oUserData;
  private int        m_iUserData;
  private String     m_sUserString;
  private String     m_sUserDescription;
  private boolean    m_bIsEnabled;
  private boolean    m_bIsSelected;
  private boolean    m_bIsVisible;  
  private boolean    m_bDrawBorderWhenSelected;
  private Color      m_oSelectionColor;    
  private Color      m_oSelectionHighlightColor;    
  private Border     m_oBorder;
  private Border     m_oNormalBorder;
  private Border     m_oSelectedBorder;  
  private HighlightMouseListener m_oMouselistener;  
  private ArrayList  m_oComponentsToHighlight;
  private ArrayList  m_oComponentsToBorderHighlight;
  private ArrayList  m_oComponentsToDisable;
  private Border     m_oComponentNormalBorder;
  private Border     m_oComponentHighlightBorder;
  private Font       m_oFont;
  private Cursor m_oHandCursor;
  private ImageIcon m_oIcon;
  private String m_sText;
  private ActionListener actionListener;//when item clicked 
  private String m_sDescription;
  private HashMap m_oComponentDescriptions;
  private AbstractButton m_oAbstractButtonWithAccelerator;

  public CollapsingMenuItemContainer(final ImageIcon icon,final JComponent component,final ActionListener anActionListener)
  {
    super();    
    this.setLayout(new BorderLayout());
    m_oComponentsToHighlight = new ArrayList();
    m_oComponentsToBorderHighlight = new ArrayList();
    m_oComponentsToDisable = new ArrayList();
    m_oIcon = icon;
    actionListener = anActionListener;
    m_sDescription = null;
    m_oComponentDescriptions = new HashMap();
	m_oAbstractButtonWithAccelerator = null;

    m_oComponentNormalBorder = new LineBorder(Color.BLACK, 1);
    m_oComponentHighlightBorder = new BevelBorder(BevelBorder.RAISED);

    m_oHandCursor = new Cursor(Cursor.HAND_CURSOR);

    m_sUserString = "";

    m_oBorder = new EmptyBorder(2,2,2,2);
    m_oNormalBorder = new EmptyBorder(1,1,1,1);
    m_oSelectedBorder = new LineBorder(Color.BLACK, 1);
    this.setBorder(BorderFactory.createCompoundBorder(m_oNormalBorder, m_oBorder));

    m_bIsEnabled = true;
    m_bIsSelected = false;
    m_bDrawBorderWhenSelected = false;
 
    m_oComponent = component;
 
    // if no icon set... just create empty button...
    if (icon == null) 
    {
      imageButton = new JButton();
    }
    else
    {      
      imageButton = new JButton(icon);
    }    
    imageButton.setBorderPainted(false);
    imageButton.setFocusPainted(false);        
    imageButton.setMargin(new Insets(0,0,0,0));
    if (actionListener != null) imageButton.addActionListener(actionListener);
    imageButton.setCursor(m_oHandCursor);


    // if no text set... just create empty button...
    if (component == null) 
    {
      m_oComponent  = new JLabel();
    }

    if (m_oComponent instanceof JLabel)
    {
      m_oComponent.setOpaque(true);
    }

            
    // add buttons to container...            
    this.add(imageButton, "West");
    this.add(m_oComponent, "Center");

    // set colors...
    m_oNormalColor = imageButton.getBackground();
    calculateHighlightColor();
    calculateForegroundColor();
    m_oComponent.setForeground(m_oForegroundColor);

    m_oMouselistener = new HighlightMouseListener();

    // add MouseListener for RollOver-Effect...    
    imageButton.addMouseListener(m_oMouselistener); 
    
    if (m_oComponent instanceof JComboBox)
    {
      JComboBox combo = ((JComboBox)m_oComponent);
      for (int i=0; i < (combo.getComponentCount()); i++)
      {
        Component comp = combo.getComponent(i);
        comp.addMouseListener(m_oMouselistener);        
      }
    }
    else
    { 
      m_oComponent.addMouseListener(m_oMouselistener);
    }
  }
  
  public MouseListener getHighlightMouseListener()
  {
    return(m_oMouselistener);
  }
    
  
  public ImageIcon getIcon()
  {
    return(m_oIcon);
  }


  public ActionListener getActionListener()
  {
    return actionListener;
  }

  public void setActionListener(ActionListener newActionListener)
  {
    if (actionListener == null){
        actionListener = newActionListener;
        imageButton.addActionListener(actionListener);
    } else {
        logger.warningSilent("[!] couldn't set new action listener: already exists");
    }
  }

  public void addComponentDescription(Object component, String description)
  {
    m_oComponentDescriptions.put(component, description);
  }

  public String getComponentDescription(Object component)
  {
    return((String)((m_oComponentDescriptions.get(component))));
  }

    
    
  public void addComponentToHighlight(JComponent comp)
  {  
    m_oComponentsToHighlight.add(comp);
  }

  public void addComponentToDisable(JComponent comp)
  {  
    m_oComponentsToDisable.add(comp);
  }

  public void setComponentNormalBorder(Border border)
  {
    m_oComponentNormalBorder = border;
  }

  public void setComponentHighlightBorder(Border border)
  {
    m_oComponentHighlightBorder = border;
  }

  public void addComponentToBorderHighlight(JComponent comp)
  {  
    m_oComponentsToBorderHighlight.add(comp);
  }


  
  public void setUserString(String text)
  {  
    m_sUserString = text;
  }
  
  public String getUserString()
  {  
    return(m_sUserString);
  }
  
  
  public void setUserDescription(String text)
  {  
    m_sUserDescription = text;
  }
  
  public String getUserDescription()
  {  
    return(m_sUserDescription);
  }
  
  
  
  
  // z.B: JButton.TOP
  public void setIconAlignmentY(int alignment)
  {  
    imageButton.setVerticalAlignment(alignment);    
  }
  
  public void setIconAlignmentX(int alignment)
  {  
    imageButton.setHorizontalAlignment(alignment);
  }  
  
  private void setItemColorMouseOver()
  {
    if (m_bIsEnabled)
    {
      for(int i=0; i<(m_oComponentsToBorderHighlight.size()); i++)
      {
        ((JComponent)m_oComponentsToBorderHighlight.get(i)).setBorder(m_oComponentHighlightBorder);
      }
      
      if (m_bIsSelected)
      {
        m_oComponent.setBackground(m_oSelectionHighlightColor);
        imageButton.setBackground(m_oSelectionHighlightColor);
        if (m_bDrawBorderWhenSelected) this.setBorder(BorderFactory.createCompoundBorder(m_oSelectedBorder, m_oBorder));
        else                           this.setBorder(BorderFactory.createCompoundBorder(m_oNormalBorder, m_oBorder));
        
        for(int i=0; i<(m_oComponentsToHighlight.size()); i++)
        {
          ((JComponent)m_oComponentsToHighlight.get(i)).setBackground(m_oSelectionHighlightColor);
        }
      }
      else
      {
        m_oComponent.setBackground(m_oHighlightColor);
        imageButton.setBackground(m_oHighlightColor);
        this.setBorder(BorderFactory.createCompoundBorder(m_oNormalBorder, m_oBorder));

        for(int i=0; i<(m_oComponentsToHighlight.size()); i++)
        {
          ((JComponent)m_oComponentsToHighlight.get(i)).setBackground(m_oHighlightColor);
        }
      }
    }
  }


  private void setItemColorNoMouseOver()
  {    
    for(int i=0; i<(m_oComponentsToBorderHighlight.size()); i++)
    {
      ((JComponent)m_oComponentsToBorderHighlight.get(i)).setBorder(m_oComponentNormalBorder);
    }     

    if (m_bIsSelected)
    {
      m_oComponent.setBackground(m_oSelectionColor);
      imageButton.setBackground(m_oSelectionColor);
      if (m_bDrawBorderWhenSelected) this.setBorder(BorderFactory.createCompoundBorder(m_oSelectedBorder, m_oBorder));
      else                           this.setBorder(BorderFactory.createCompoundBorder(m_oNormalBorder, m_oBorder));

      for(int i=0; i<(m_oComponentsToHighlight.size()); i++)
      {
        ((JComponent)m_oComponentsToHighlight.get(i)).setBackground(m_oSelectionColor);
      }      
    }
    else
    {
      m_oComponent.setBackground(m_oNormalColor);
      imageButton.setBackground(m_oNormalColor);
      this.setBorder(BorderFactory.createCompoundBorder(m_oNormalBorder, m_oBorder));

      for(int i=0; i<(m_oComponentsToHighlight.size()); i++)
      {
        ((JComponent)m_oComponentsToHighlight.get(i)).setBackground(m_oNormalColor);
      }      
    }
  }

    
  
  
  
  
  
  
  public class HighlightMouseListener implements MouseListener
  {
    public void mouseClicked(MouseEvent me)
    {
    } 
    
    public void mouseEntered(MouseEvent me)
    {
      setItemColorMouseOver();
    } 
    
    public void mouseExited(MouseEvent me)
    {
      setItemColorNoMouseOver();
    } 
    
    public void mousePressed(MouseEvent me)
    {
    } 
    
    public void mouseReleased(MouseEvent me)
    {
    } 
  }
  

  public JButton getImageButton()
  {
    return(imageButton);
  }


  public JComponent getComponent()
  {
    return(m_oComponent);
  }

  public JComponent getItemComponent()
  {
    return(m_oComponent);
  }

  public void setComponent(JComponent component)
  {
    m_oComponent = component;
    m_oComponent.setBackground(m_oNormalColor);
    this.add(m_oComponent, "Center");    
  }

  public void setIcon(ImageIcon icon)
  {
    imageButton.setIcon(icon);
    m_oIcon = icon;    
  }

  public void setHighlightColor(Color color)
  {
    m_oHighlightColor = color;
  }

  public void setForegroundColor(Color color)
  {
    m_oForegroundColor = color;
    imageButton.setForeground(m_oForegroundColor);
    m_oComponent.setForeground(m_oForegroundColor);
  }

  public void setBackgroundColor(Color color)
  {
    m_oNormalColor = color;
    imageButton.setBackground(m_oNormalColor);
    m_oComponent.setBackground(m_oNormalColor);
    this.setBackground(m_oNormalColor);
  }

  public void calculateHighlightColor()
  {    
    m_oHighlightColor = m_oNormalColor.brighter();    
  }

  public void calculateForegroundColor()
  {    
    m_oForegroundColor = m_oNormalColor.darker();   
  }



  public void setSelectionColor(Color color)
  {
    m_oSelectionColor = color;
    setItemColorNoMouseOver();    
  }

  public void setSelectionHighlightColor(Color color)
  {
    m_oSelectionHighlightColor = color;    
    setItemColorNoMouseOver();
  }

  public void drawBorderWhenSelected(boolean drawborder)
  {
    m_bDrawBorderWhenSelected = drawborder;
  }


  public void setSelectedItemBorder(int width)
  {    
    m_oNormalBorder = new EmptyBorder(width, width, width, width);
    m_oSelectedBorder = new LineBorder(Color.BLACK, width);
  }


  public void setItemBorder(int width)
  {    
    m_oBorder = new EmptyBorder(width, width, width, width); 
    this.setBorder(m_oBorder);
  }


  public void setItemBorder(int top, int left, int bottom, int right)
  {
    this.setBorder(new EmptyBorder(top, left, bottom, right));
  }

  public void setItemToolTipText(String text)
  {
    imageButton.setToolTipText(text);
    m_oComponent.setToolTipText(text);
  }


  public void setItemEnabled(boolean isenabled)
  {
    m_bIsEnabled = isenabled;
    imageButton.setEnabled(isenabled);
    m_oComponent.setEnabled(isenabled);
        
    for(int i=0; i<(m_oComponentsToDisable.size()); i++)
    {
      ((JComponent)m_oComponentsToDisable.get(i)).setEnabled(isenabled);
    }          
  }

  public boolean getItemEnabled()
  {
    return(m_bIsEnabled);
  }


  public void setItemSelected(boolean isselected)
  {
    m_bIsSelected = isselected;
    setItemColorNoMouseOver();
  }

  public boolean isSelected()
  {
    return(m_bIsSelected);
  }
  
  
  public void setItemIndent(int indent)
  {
    imageButton.setMargin(new Insets(0,indent,0,0));
  }

  public void setItemVisible(boolean isvisible)
  {
    m_bIsVisible = isvisible;
    this.setVisible(m_bIsVisible);
  }
  
  public boolean getItemVisible()
  {
    return(m_bIsVisible);
  }




  public void setUserDataObject(Object userdata)
  {
    m_oUserData = userdata;
  }

  public void setUserDataInt(int userdata)
  {
    m_iUserData = userdata;
  }
  
  public Object getUserDataObject()
  {
    return(m_oUserData);
  }

  public int getUserDataInt()
  {
    return(m_iUserData);
  }

  public void setItemFont(Font font)
  {
    m_oFont = font;
  }

  public Font getItemFont()
  {
    return(m_oFont);
  }
  
  public String getText()
  {
    return(m_sText);
  }
  
  public void setText(String text)
  {
    m_sText = text;
  }

  public void setDescription(String text)
  {
    m_sDescription = text;
  }
  
  public String getDescription()
  {
    return(m_sDescription);
  }
  
  
  
  private AbstractButton findAbstractButton(Component comp)
  {
    if (comp instanceof AbstractButton) return((AbstractButton)comp);
    else if (comp instanceof Container)
  	{
      for (int i=0; i < (((Container)comp).getComponentCount()); i++)
      {
        Component c = findAbstractButton(((Container)comp).getComponent(i));
        if (c != null) return((AbstractButton)comp);
      }
    }    
    return(null);
  }
  
  private AbstractButton findAbstractButton()
  {
  	if (m_oAbstractButtonWithAccelerator == null) m_oAbstractButtonWithAccelerator = findAbstractButton(m_oComponent);   	
  	return(m_oAbstractButtonWithAccelerator);
  }
  
  public void setAcceleratorKey(char key)
  {    	  
  	if (Character.toUpperCase(key) >= 'A' && Character.toUpperCase(key) <= 'Z')
  	{
      AbstractButton button = findAbstractButton();
      if (button != null) button.setMnemonic((int)Character.toUpperCase(key));
  	}
  }

  public void setAcceleratorKey(String key)
  {
    if (key != null) 
    {
      if (key.length() > 0) setAcceleratorKey(key.charAt(0));
    } 
  }
  
  public void setAcceleratorIndex(int charnum)
  {
	AbstractButton button = findAbstractButton();
  	if (button != null) button.setDisplayedMnemonicIndex(charnum);
  }  
  
}
