package org.evolvis.nuyo.db.filter;

import java.util.Collection;
import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;




/**
 * @author kleinw
 *
 *	Interface f�r Filterimplementierungen
 *
 */
public interface ISelection {
    
    /**	Hinzuf�gen eines Elements */
    public ISelection add(SelectionElement element);
 
    
    /**	Eine Maprepr�sentation des Filters ist f�r Soap geeignet */
    public Map encode() throws ContactDBException;
    
    
    /**	Liste aller Filter holen */
    public Collection getFilterElements();
    
    
    public Integer isSingleIdSelection();
    public boolean containsIdSelection();
    
    
}
