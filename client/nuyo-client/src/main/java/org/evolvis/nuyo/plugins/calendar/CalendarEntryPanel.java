package org.evolvis.nuyo.plugins.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

class CalendarEntryPanel extends JPanel {

    private static final long serialVersionUID = 6829947372426720705L;

    private JButton nameButton;
    private JButton descButton;
    private JCheckBox checkBox;
    private JPanel colorPanel;


    public CalendarEntryPanel(final String name, final String desc, final ImageIcon icon, final String tooltiptext, final Color calendarColor) {
        JPanel panel = this;
        panel.setLayout(new BorderLayout());
        panel.setBackground(Color.WHITE);

        JPanel toppanel = new JPanel();
        toppanel.setLayout(new BorderLayout());
        toppanel.setBackground(Color.WHITE);

        JPanel botpanel = new JPanel();
        botpanel.setLayout(new BorderLayout());
        botpanel.setBackground(Color.WHITE);

        colorPanel = new JPanel();
        colorPanel.setLayout(new BorderLayout());
        colorPanel.setBackground(calendarColor);

        colorPanel.add(Box.createHorizontalStrut(20));
        colorPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        nameButton = new JButton(name);
        nameButton.setBorderPainted(false);
        nameButton.setBackground(Color.WHITE);
        nameButton.setFocusPainted(false);
        nameButton.setHorizontalAlignment(SwingConstants.LEFT);
        nameButton.setMargin(new Insets(0, 0, 0, 0));
        nameButton.setToolTipText(tooltiptext);

        descButton = new JButton(desc);
        descButton.setBorderPainted(false);
        descButton.setBackground(Color.WHITE);
        descButton.setFocusPainted(false);
        descButton.setHorizontalAlignment(SwingConstants.LEFT);
        descButton.setMargin(new Insets(0, 0, 0, 0));
        descButton.setToolTipText(tooltiptext);

        checkBox = new JCheckBox();
        checkBox.setBackground(Color.WHITE);
        checkBox.setFocusPainted(false);
        checkBox.setToolTipText(tooltiptext);

        toppanel.add(checkBox, BorderLayout.WEST);
        toppanel.add(nameButton, BorderLayout.CENTER);
        toppanel.add(colorPanel, BorderLayout.EAST);

        botpanel.add(descButton, BorderLayout.CENTER);

        panel.add(toppanel, BorderLayout.NORTH);
    }

    public void addActionListener( ActionListener al_selected ) {
        descButton.addActionListener(al_selected);
        nameButton.addActionListener(al_selected);
        checkBox.addActionListener(al_selected);
    }


    public JCheckBox getCheckBox() {
        return (checkBox);
    }

    public void setHighlightMouseListener(MouseListener ml) {
        nameButton.addMouseListener(ml);
        descButton.addMouseListener(ml);
        checkBox.addMouseListener(ml);
    }


    public void setHighlightComponents(CollapsingMenuItemContainer item) {
        item.addComponentToHighlight(nameButton);
        item.addComponentToHighlight(descButton);
        item.addComponentToHighlight(checkBox);

        Border normalborder = BorderFactory.createCompoundBorder(new EmptyBorder(3, 3, 3, 3), new LineBorder(Color.LIGHT_GRAY, 1));
        Border highlightborder = BorderFactory.createCompoundBorder(new EmptyBorder(2, 2, 2, 2), new BevelBorder(BevelBorder.RAISED));
        item.setComponentNormalBorder(normalborder);
        item.setComponentHighlightBorder(highlightborder);

        item.addComponentToDisable(nameButton);
        item.addComponentToDisable(descButton);
        item.addComponentToDisable(checkBox);
    }


    public void setCalendarColor(Color color) {
        if (color != null) {
            colorPanel.setBackground(color);
        }
    }

    public void addColorChooserListener(final MouseListener listener ) {
        colorPanel.addMouseListener(listener);
    }
}
