/* $Id: TarentWidgetComponentListPanel.java,v 1.2 2007/01/11 18:56:41 robert Exp $
 * 
 * Created on 11.04.2003
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Nikolai R�ther.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */ 
package org.evolvis.nuyo.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 * @author d-fens
 *
 */
public class TarentWidgetComponentListPanel extends JPanel implements TarentWidgetInterface 
{  
  private JScrollPane            m_oScrollPane;
  private List                        m_oList;
  private JPanel                    m_oListPanel;

  public TarentWidgetComponentListPanel() 
  {
    m_oListPanel = new JPanel(new GridLayout(0,1));    
    m_oScrollPane = new JScrollPane(m_oListPanel);
    m_oScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    m_oScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    m_oList = new ArrayList();
    
    this.setLayout(new BorderLayout());    
    this.add(m_oScrollPane, BorderLayout.CENTER); 
  }
  
  /* (non-Javadoc)
   * @see de.tarent.controls.tarentWidgetInterface#setData(java.lang.Object)
   */
  public void setData(Object data) 
  {
    removeAllData();
    addData(data);
  }

  public void addData(Object data) 
  {
    if (data instanceof Component[])
    {
      for(int i=0; i<((Component[])data).length; i++)
      {
        Component comp = ((Component[]) data)[i];
        m_oListPanel.add(comp);
        m_oList.add(comp);
        setComponentSelected(comp, false);
        setMouseListenerRecursively(comp, new SelectionListener(comp));
      }      
    }
    else if (data instanceof Component)
    {
      Component comp = ((Component)data); 
      m_oListPanel.add(comp);
      m_oList.add(comp);
      setComponentSelected(comp, false);
      setMouseListenerRecursively(comp, new SelectionListener(comp));
    }
  }


  private void setMouseListenerRecursively(Component comp, MouseListener listener)
  {
    if (comp instanceof Container)
    {
       for (int i=0; i<((Container)comp).getComponentCount(); i++)
       {
         setMouseListenerRecursively(((Container)comp).getComponent(i), listener);
       }
    }

    comp.addMouseListener(listener);
  }

  public void removeData(Object data) 
  {
    if (data instanceof Component)
    {
      m_oListPanel.remove((Component)data);
      m_oList.remove(data);
    }
  }

  public void removeAllData() 
  {
    m_oListPanel.removeAll();
    m_oList.clear();
  }

  public int getNumberOfEntries()
  {
    return m_oList.size();    
  }

  public Object getData() 
  {
   return(null);
  }

  public JComponent getComponent() 
  {
    return (this);
  }

  public void setLengthRestriction(int maxlen) 
  {
  }

  public void setWidgetEditable(boolean iseditable) 
  {
  }

  public boolean isEqual(Object data) 
  {
    return false;
  }

  private Component m_oSelectedComponent = null;

  public void setSelectedComponent(Component oComponent)
  {
     if (m_oSelectedComponent != null) setComponentSelected(m_oSelectedComponent, false);
     m_oSelectedComponent = oComponent;
     setComponentSelected(m_oSelectedComponent, true);
  }  

  
  private void setBackgroundRecursive(Component comp, boolean enable)
  {
    if (comp instanceof Container)
    {
      for (int i=0; i<((Container)comp).getComponentCount(); i++)
      {
        setBackgroundRecursive(((Container)comp).getComponent(i), enable);
      }
    }

    if (comp instanceof JComponent) ((JComponent)comp).setOpaque(true);
    comp.setBackground(enable ? Color.BLACK : Color.WHITE);  
    comp.setForeground(enable ? Color.WHITE : Color.BLACK);  
  }
  
  
  private void setComponentSelected(Component oComponent, boolean selected)
  {
    setBackgroundRecursive(oComponent, selected);
  }


  private class SelectionListener implements MouseListener
  {
    private Component m_oComponent;

    public SelectionListener(Component oComponent)
    {
      m_oComponent = oComponent;    
    }    

    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mousePressed(MouseEvent e) 
    {
      setSelectedComponent(m_oComponent);
    }
    public void mouseReleased(MouseEvent e) {}
  }

  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  
}

  