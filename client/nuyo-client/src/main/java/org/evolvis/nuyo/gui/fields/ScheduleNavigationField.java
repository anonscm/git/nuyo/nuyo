/*
 * Created on 14.04.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetPanel;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.octopus.CalendarImpl;
import org.evolvis.nuyo.gui.calendar.AppointmentPrintContainer;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.calendar.TODOPrintContainer;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.nuyo.plugins.calendar.CalendarException;
import org.evolvis.nuyo.util.DateRange;
import org.evolvis.spreadsheet.export.SpreadSheet;
import org.evolvis.spreadsheet.export.SpreadSheetFactory;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

/**
 * @author niko
 *
 */
public class ScheduleNavigationField extends ContactAddressField implements CalendarVisibleElement
{  
    public final static String VIEW_MONTH = "VIEW_MONTH";
    public final static String VIEW_WEEK = "VIEW_WEEK";
    public final static String VIEW_DAY = "VIEW_DAY";
    
    private static final TarentLogger logger = new TarentLogger(ScheduleNavigationField.class);
    private ScheduleListField scheduleList;
    private EntryLayoutHelper navPanel;
    private ScheduleField scheduleField;
    private ToDoListField toDoList; 
    
    private Map monthStringToDateMap;
    private Map yearStringToDateMap;
    private JComboBox monthComboBox;
    private JComboBox yearComboBox;
    
    private JToggleButton viewMonthButton;
    private JToggleButton viewWeekButton;
    private JToggleButton viewDayButton;
    private ButtonGroup buttonGroup;
    
    private JButton newAppointmentButton;
    private JButton newToDoButton;
    
    private JButton todayButton;
    private JButton backButton;
    private JButton nextButton;
    
    private JCheckBox snapToGridCheckBox;
    
    private JComboBox exportComboBox;
    private JButton   exportButton;
    
    private MonthChangedListener combo_month_changed_listener = new MonthChangedListener();
    private YearChangedListener combo_year_changed_listener = new YearChangedListener();
    
    public String getFieldName()
    {
        return "Kalendernavigation";    
    }
    
    public String getFieldDescription()
    {
        return "Ein Feld zur Navigation in Kalendern";
    }
    
    public EntryLayoutHelper getPanel(String widgetFlags)
    {
        monthStringToDateMap = new HashMap();
        yearStringToDateMap = new HashMap();
        
        getWidgetPool().addController(CalendarVisibleElement.NAVIGATOR, this);
        
        if(navPanel == null) navPanel = createPanel(widgetFlags);
        
        getAppointmentDisplayManager().addAppointmentDisplayListener(this);    
        
        return navPanel;
    }
    
    
    public int getContext()
    {
        return CONTEXT_USER;
    }  
    
    
    
    public void postFinalRealize()
    {
        //fillDateLabel(getScheduleField().getLastDate());
        setView(VIEW_WEEK, true, false);  
        getAppointmentDisplayManager().fireSetGridActive(getControlListener().getScheduleData().useGrid, null);
        //getScheduleField().snapToGrid(getControlListener().getScheduleData().m_bInitialUseGrid);
    }
    
    public void setData(PluginData data)
    {    
    }
    
    public void getData(PluginData data)
    {
    }
    
    public boolean isDirty(PluginData data)
    {
        return false;
    }
    
    public void setEditable(boolean iseditable)
    {
    }
    
    
    /**
     * Returns the reference key to use in configuration file.
     * I.e. 'Octopus-localhost.xml' use it as follows:
     * <pre>
     * <!-- ~~~~~~~~~~~~~~ TerminPanel ~~~~~~~~~~~~~~~~~~~~ -->
     * <Entry name="CALENDARNAVIGATION" flags="" container="SHED4"/>
     * </pre> 
     */
    public String getKey()
    {
        return "CALENDARNAVIGATION";
    }
   
    public void setDoubleCheckSensitive(boolean issensitive)
    {
    }
    
    public void setCurrentDate(ScheduleDate date)
    {
        setCurrentDate(date, true, false);
    }
        
    public void setCurrentDateByMonth(ScheduleDate date){
        setCurrentDate(date, true, true);
    }
    
    public void fillDateCombos(ScheduleDate date)
    {
        monthComboBox.removeActionListener(combo_month_changed_listener);    
        yearComboBox.removeActionListener(combo_year_changed_listener);    
        
        fillYearCombo(date);
        fillMonthCombo(date);
        
        monthComboBox.addActionListener(combo_month_changed_listener);    
        yearComboBox.addActionListener(combo_year_changed_listener);    
        
        fillDateLabel(date);
    }
    
    
    public void fillDateLabel(ScheduleDate date)
    {
        String text = "?";
        if (date != null)
        {  
            if (ScheduleField.CARD_DAY.equals(getScheduleField().getCardVisible()))
            {
                text = date.getDateString();
            }
            else if (ScheduleField.CARD_WEEK.equals(getScheduleField().getCardVisible()))
            {      
                text = date.getDateString() + " bis " + date.getDateWithAddedDays(6).getDateString();
            }
            else if (ScheduleField.CARD_MONTH.equals(getScheduleField().getCardVisible()))
            {      
                text = date.getMonthString() + " " + date.getYearString();
            }
        }  
        // m_oLabelCurrentDate.setText(text);  
    }
    
    
    private void fillMonthCombo(ScheduleDate currentdate)
    {
        monthComboBox.removeAllItems();
        monthStringToDateMap.clear();
        
        ScheduleDate date = currentdate.getFirstSecondOfDay();
        date.addMonths(-6);
        
        String datestring;
        for(int i=0; i<12; i++)
        {
            datestring = date.getMonthString();
            monthComboBox.addItem(datestring);
            monthStringToDateMap.put(datestring, new ScheduleDate(date));
            date.addMonths(1);
        }    
        
        monthComboBox.setSelectedItem(currentdate.getMonthString());
    }
    
    private void fillYearCombo(ScheduleDate currentdate)
    {
        yearComboBox.removeAllItems();
        yearStringToDateMap.clear();
        
        ScheduleDate date = currentdate.getFirstSecondOfDay();
        date.addYears(-4);
        String datestring;
        for(int i=0; i<8; i++)
        {
            datestring = date.getYearString();
            yearComboBox.addItem(datestring);
            yearStringToDateMap.put(datestring, new ScheduleDate(date));
            date.addYears(1);
        }    
        yearComboBox.setSelectedItem(currentdate.getYearString());
    }
    
    
    private class BackClickListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (getScheduleField() != null) 
            {
                ScheduleDate date = getScheduleField().getFirstDay();
                
                if (ScheduleField.CARD_DAY.equals(getScheduleField().getCardVisible()))
                {
                    date.addDays(-1);  
                }
                else if (ScheduleField.CARD_WEEK.equals(getScheduleField().getCardVisible()))
                {
                    date.addDays(-7);  
                }
                if (ScheduleField.CARD_MONTH.equals(getScheduleField().getCardVisible()))
                {
                    date.addMonths(-1);  
                }
                
                setCurrentDate(date);
            }
        }
    }
    
    
    public void setDateToToday()
    {
        if (getScheduleField() != null) 
        {
            getScheduleField().setDateToToday();
        }    
    }
    
    
    private class button_today_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            setDateToToday();
        }
    }
    
    private class button_next_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (getScheduleField() != null) 
            {
                ScheduleDate date = getScheduleField().getFirstDay();
                
                if (ScheduleField.CARD_DAY.equals(getScheduleField().getCardVisible()))
                {
                    date.addDays(1);  
                }
                else if (ScheduleField.CARD_WEEK.equals(getScheduleField().getCardVisible()))
                {
                    date.addDays(7);  
                }
                if (ScheduleField.CARD_MONTH.equals(getScheduleField().getCardVisible()))
                {
                    date.addMonths(1);  
                }
                
                setCurrentDate(date);
            }
        }
    }
    
    /**
     * Updates the buttons' states: day, month, year, new task, new appointment.
     */
    public void setView(Object key)
    {
        setView(key, true, true);
    }
    
    
    private void setView(Object key, boolean updatebuttons, boolean fireEvents)
    {
        
        newAppointmentButton.setEnabled(! VIEW_MONTH.equals(key));
        newToDoButton.setEnabled(! VIEW_MONTH.equals(key));
        
        if (VIEW_DAY.equals(key))
        {
            if (getScheduleField() != null) 
            {                
                getScheduleField().setCardVisible(ScheduleField.CARD_DAY);
                fillDateLabel(getScheduleField().getLastDate());
                setCurrentDate(getScheduleField().getLastDate(), fireEvents, false);
            }      
            if (updatebuttons) buttonGroup.setSelected(viewDayButton.getModel(), true);
            backButton.setToolTipText("zum vorherigen Tag wechseln...");
            nextButton.setToolTipText("zum n�chsten Tag wechseln...");        
        }
        else if (VIEW_WEEK.equals(key))
        {
            if (getScheduleField() != null) 
            {
                ScheduleDate date = getScheduleField().getFirstDay();        
                date = date.getMondayOfWeek();
                getScheduleField().setCardVisible(ScheduleField.CARD_WEEK);
                
                setCurrentDate(getScheduleField().getLastDate(), fireEvents, false);
                fillDateLabel(getScheduleField().getLastDate());
                
            }      
            if (updatebuttons) buttonGroup.setSelected(viewWeekButton.getModel(), true);        
            backButton.setToolTipText("zur vorherigen Woche wechseln...");
            nextButton.setToolTipText("zur n�chsten Woche wechseln...");        
        }
        else if (VIEW_MONTH.equals(key))
        {
            if (getScheduleField() != null) 
            {
                getScheduleField().setCardVisible(ScheduleField.CARD_MONTH);
                fillDateLabel(getScheduleField().getLastDate());
                setCurrentDate(getScheduleField().getLastDate(), fireEvents, false);
            }      
            if (updatebuttons) buttonGroup.setSelected(viewMonthButton.getModel(), true);        
            backButton.setToolTipText("zum vorherigen Monat wechseln...");
            nextButton.setToolTipText("zum n�chsten Monat wechseln...");        
        }
    }
    
    
    private class DayClickListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            setView(VIEW_DAY, false, true);
        }
    }
    
    private class button_week_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            setView(VIEW_WEEK, false, true);
        }
    }
    
    private class button_month_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            setView(VIEW_MONTH, false, true);
        }
    }
    
    private class button_snap_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            boolean snap = snapToGridCheckBox.isSelected();
            //getScheduleField().snapToGrid(snap);
            getAppointmentDisplayManager().fireSetGridActive(snap, null);
        }
    }
    
    private class NewAppointmentOrTaskButtonClicked implements ActionListener
    {
        private boolean isTask;
        
        public NewAppointmentOrTaskButtonClicked(boolean isTaskValue){
            isTask = isTaskValue;    
        }
        public void actionPerformed(ActionEvent e)
        {
            createNewAppointmentOrTask(isTask);
        }
    }
    
    private class MonthChangedListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (getScheduleField() != null) 
            {
                Object item = monthComboBox.getSelectedItem();
                ScheduleDate selecteddate = (ScheduleDate)(monthStringToDateMap.get(item));
                //SIMON if (selecteddate != null) setCurrentDate(selecteddate);
                if (selecteddate != null) setCurrentDateByMonth(selecteddate.getFirstDayOfMonth());
            }
        }    
    }
    
    private class YearChangedListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (getScheduleField() != null) 
            {
                ScheduleDate selecteddate = (ScheduleDate)(yearStringToDateMap.get(yearComboBox.getSelectedItem()));
                if (selecteddate != null) setCurrentDate(selecteddate);
            }
        }    
    }
    
    // Methods of interface CalendarVisibleElement
    public void changedAppointment(Appointment appointment){}
    public void addedAppointment(Appointment appointment) {}
    public void removedAppointment(Appointment appointment) {}
    public void selectedAppointment(Appointment appointment) {}
    public void changedCalendarCollection() {
        
        boolean enableButton = false;
        
        enableButton = getGUIListener().getCalendarCollection().getCollection().contains(getGUIListener().getCalendarCollection().getUserCalendar());
        
        Collection calendars = getGUIListener().getCalendarCollection().getCollection();
        
        try {
            for (Iterator iter = calendars.iterator(); iter.hasNext();){
                
                CalendarImpl next = (CalendarImpl) iter.next();
                
                CalendarSecretaryRelation relation = next.getCalendarSecretaryforUser(getGUIListener().getUser(null).getId());
                
                if (relation != null){
                    DateRange range = relation.getWriteDateRange();
                    Date now = new Date();
                    
                    if (range.getStartDate().before(now) && (range.getEndDate() == null || range.getEndDate().after(now))){
                                enableButton = true;
                                break;
                    }
                }
            }
        } catch (ContactDBException e) {
            e.printStackTrace();
            getGUIListener().getLogger().log(Level.WARNING, "Fehler bei Ermittlung der Benutzerberechtigung im Kalender");
        }
        
        newAppointmentButton.setEnabled(enableButton);
        newToDoButton.setEnabled(enableButton);
    }
    public Appointment getAppointment(int appointmentid) { return null; }
    
    public void setViewType(String viewtype)
    {
        if (ScheduleField.CARD_DAY.equals(viewtype)) setView(VIEW_DAY);
        else if (ScheduleField.CARD_WEEK.equals(viewtype)) setView(VIEW_WEEK);
        else if (ScheduleField.CARD_MONTH.equals(viewtype)) setView(VIEW_MONTH);
    }
    
    public void setVisibleInterval(ScheduleDate start, ScheduleDate end)
    {
        setCurrentDate(start, false, false);
    }
    
    public void setGridActive(boolean usegrid)
    {
        if (snapToGridCheckBox.isSelected() != usegrid)
        {
            snapToGridCheckBox.doClick();
        }
    }
    
    public void setGridHeight(int seconds)
    {
    }

    private ScheduleField getScheduleField()
    {
        if (scheduleField == null) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_SCHEDULEPANEL");
            if (controler instanceof ScheduleField)
            {  
                scheduleField = (ScheduleField)controler;
            }
        }
        return scheduleField;
    }

    private ScheduleListField getScheduleList()
    {
        if (scheduleList == null) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_SCHEDULELIST");
            if (controler instanceof ScheduleListField)
            {  
                scheduleList = (ScheduleListField)controler;
            }
        }
        return scheduleList;
    }

    private ToDoListField getToDoList()
    {
        if (toDoList == null) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_TODOLIST");
            if (controler instanceof ToDoListField)
            {  
                toDoList = (ToDoListField)controler;
            }
        }
        return toDoList;
    }

    private StringBuffer getDateSB(){
        // Termine only
        Iterator APPintiterator = getScheduleList().getCurrentEvents().iterator();
        StringBuffer SB = new StringBuffer("\"Termine\",,,,,,\n\n");
        SB.append("\"Monat\",\"KW\",\"Titel\",\"Ort\",\"Tag\",\"Datum\",\"Beginn\",\"Ende\",\"Teilnehmer\"\n");
        String lastMonth = "";
        int lastKW = 0;             
        while(APPintiterator.hasNext())
        {
            AppointmentPrintContainer APC = (AppointmentPrintContainer) APPintiterator.next();
            if (! APC.get_monat().equals(lastMonth)){
                // new Month
                SB.append("\"" + APC.get_monat() + "\",,,,,,,,\n");
                lastMonth = APC.get_monat();
            }
            if ( APC.get_kw() != lastKW){
                // new KW
                SB.append(",\"" + APC.get_kw() + "\",,,,,,,\n");
                lastKW = APC.get_kw();
            }
            SB.append(",,\"" + APC.get_titel() + "\",\"" + APC.get_ort() + "\",\"" + APC.get_Tag_vonbis() + "\",\""+ APC.get_Datum_vonbis() + "\",\""+ APC.get_start() + "\",\"" + APC.get_end() + "\",\"" + APC.get_Teilnehmer() + "\",\n");
        }
        return SB;
    }

    private StringBuffer getTODOSB(){
        // Termine only
        Iterator APPintiterator = getToDoList().getCurrentTodos().iterator();
        StringBuffer SB = new StringBuffer("\"Aufgaben\",,,,,,\n\n");
        SB.append("\"Monat\",\"KW\",\"Titel\",\"Ort\",\"F�lligkeit\",\"Erledigt (%)\",\"Priorit�t\"\n");
        String lastMonth = "";
        int lastKW = 0;             
        while(APPintiterator.hasNext())
        {
            TODOPrintContainer TPC = (TODOPrintContainer) APPintiterator.next();
            if (! TPC.get_monat().equals(lastMonth)){
                // new Month
                SB.append("\"" + TPC.get_monat() + "\",,,,,,\n");
                lastMonth = TPC.get_monat();
            }
            if ( TPC.get_kw() != lastKW){
                // new KW
                SB.append(",\"" + TPC.get_kw() + "\",,,,,\n");
                lastKW = TPC.get_kw();
            }
            SB.append(",,\"" + TPC.get_titel() + "\",\"" + TPC.get_ort() + "\",\"" + TPC.get_faelligkeit() + "\",\"" + TPC.get_status() + "\",\"" + TPC.get_prio() + "\",\n");
        }
        return SB;
    }

    private void getTodoSXC(SpreadSheet spreadsheet){
        // Termine only
        Iterator APPintiterator = getToDoList().getCurrentTodos().iterator();
        
        spreadsheet.openRow();
        spreadsheet.addCell("Aufgaben");
        spreadsheet.closeRow();
        
        spreadsheet.openRow();
        spreadsheet.closeRow();
        
        spreadsheet.openRow();
        spreadsheet.addCell("Monat");
        spreadsheet.addCell("KW");
        spreadsheet.addCell("Titel");
        spreadsheet.addCell("Ort");
        spreadsheet.addCell("F�lligkeit");
        spreadsheet.addCell("Erledigt (%)");
        spreadsheet.addCell("Priorit�t");
        spreadsheet.closeRow();
        
        String lastMonth = "";
        int lastKW = 0;             
        while(APPintiterator.hasNext())
        {
            TODOPrintContainer TPC = (TODOPrintContainer) APPintiterator.next();
            if (! TPC.get_monat().equals(lastMonth)){
                // new Month
                spreadsheet.openRow();
                spreadsheet.addCell( TPC.get_monat() );
                spreadsheet.closeRow();
    
                lastMonth = TPC.get_monat();
            }
            if ( TPC.get_kw() != lastKW){
                // new KW
    
                spreadsheet.openRow();
                spreadsheet.addCell(null);
                spreadsheet.addCell( new Integer(TPC.get_kw()));
                spreadsheet.closeRow();
    
                lastKW = TPC.get_kw();
            }
    
            spreadsheet.openRow();
            spreadsheet.addCell(null);
            spreadsheet.addCell(null);
            spreadsheet.addCell(TPC.get_titel());
            spreadsheet.addCell(TPC.get_ort() );
            spreadsheet.addCell(TPC.get_faelligkeit() );
            spreadsheet.addCell(new Integer(TPC.get_status()));
            spreadsheet.addCell(new Integer(TPC.get_prio()));
            spreadsheet.closeRow();
    
        }
    }

    private void getDateSXC(SpreadSheet spreadsheet){
        // Termine only
        Iterator APPintiterator = getScheduleList().getCurrentEvents().iterator();
        
        spreadsheet.openRow();
        spreadsheet.addCell("Termine");
        spreadsheet.closeRow();
        
        spreadsheet.openRow();
        spreadsheet.closeRow();
        
        spreadsheet.openRow();
        spreadsheet.addCell("Monat");
        spreadsheet.addCell("KW");
        spreadsheet.addCell("Titel");
        spreadsheet.addCell("Ort");
        spreadsheet.addCell("Tag");
        spreadsheet.addCell("Datum");
        spreadsheet.addCell("Beginn");
        spreadsheet.addCell("Ende");
        spreadsheet.addCell("Teilnehmer");
        spreadsheet.closeRow();
        
        String lastMonth = "";
        int lastKW = 0;             
        while(APPintiterator.hasNext())
        {
            AppointmentPrintContainer APC = (AppointmentPrintContainer) APPintiterator.next();
            if (! APC.get_monat().equals(lastMonth)){
                
                // new Month
                spreadsheet.openRow();
                spreadsheet.addCell(APC.get_monat());
                spreadsheet.closeRow();
                
                lastMonth = APC.get_monat();
            }
            if ( APC.get_kw() != lastKW){
    
                // new KW
                spreadsheet.openRow();
                spreadsheet.addCell(null);
                spreadsheet.addCell(new Integer(APC.get_kw()));
                spreadsheet.closeRow();
                
                lastKW = APC.get_kw();
            }
            
            spreadsheet.openRow();
            spreadsheet.addCell(null);
            spreadsheet.addCell(null);
            spreadsheet.addCell(APC.get_titel());
            spreadsheet.addCell(APC.get_ort() );
            spreadsheet.addCell(APC.get_Tag_vonbis() );
            spreadsheet.addCell(APC.get_Datum_vonbis());
            spreadsheet.addCell(APC.get_start());
            spreadsheet.addCell(APC.get_end());
            spreadsheet.addCell(APC.get_Teilnehmer());
            spreadsheet.closeRow();
        }
    }

    private EntryLayoutHelper createPanel(String widgetFlags)
    {
        TarentWidgetPanel panel = new TarentWidgetPanel(); 
        panel.setLayout(new BorderLayout());
        
        IconFactory iconFactory = SwingIconFactory.getInstance();
        backButton = new JButton((ImageIcon)iconFactory.getIcon("calendar_back.gif"));
        backButton.setFocusPainted(false);
        backButton.setToolTipText("zur�ck...");
        
        todayButton = new JButton("heute");
        todayButton.setFocusPainted(false);
        todayButton.setToolTipText("den heutigen Tag anzeigen...");
        
        nextButton = new JButton((ImageIcon)iconFactory.getIcon("calendar_next.gif"));
        nextButton.setFocusPainted(false);
        nextButton.setToolTipText("weiter...");
        
        
        viewDayButton = new JToggleButton((ImageIcon)iconFactory.getIcon("calendar_day.gif"));
        viewDayButton.setToolTipText("zur Tagesansicht wechseln...");
        viewDayButton.setFocusPainted(false);
        
        viewWeekButton = new JToggleButton((ImageIcon)iconFactory.getIcon("calendar_week.gif"));
        viewWeekButton.setToolTipText("zur Wochenansicht wechseln...");
        viewWeekButton.setFocusPainted(false);
        
        viewMonthButton = new JToggleButton((ImageIcon)iconFactory.getIcon("calendar_month.gif"));
        viewMonthButton.setToolTipText("zur Monatsansicht wechseln...");
        viewMonthButton.setFocusPainted(false);
        
        buttonGroup = new ButtonGroup();
        buttonGroup.add(viewDayButton);
        buttonGroup.add(viewWeekButton);
        buttonGroup.add(viewMonthButton);
        
        //m_oButtonGroup.setSelected()
        
        //m_oButtonViewWeek.setSelected(true);
        
        newAppointmentButton = new JButton((ImageIcon)iconFactory.getIcon("calendar_new.gif"));
        newAppointmentButton.setToolTipText("einen neuen Termin erzeugen...");
        newAppointmentButton.setFocusPainted(false);
        
        newToDoButton = new JButton((ImageIcon)iconFactory.getIcon("task_new.gif"));
        newToDoButton.setToolTipText("eine neue Aufgabe erzeugen...");
        newToDoButton.setFocusPainted(false);
        
        monthComboBox = new JComboBox();
        yearComboBox = new JComboBox();    
        
        // m_oLabelCurrentDate = new JLabel("...");
        // m_oLabelCurrentDate.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
        
        snapToGridCheckBox = new JCheckBox("Magnetraster");
        snapToGridCheckBox.setToolTipText("magnetisches Raster beim Erstellen von Terminen/Aufgaben ein-/ausschalten...");
        snapToGridCheckBox.setFocusPainted(false);
        snapToGridCheckBox.setSelected(getControlListener().getScheduleData().useGrid);
        
        exportButton = new JButton("Export");
        exportButton.setToolTipText("Termine/Aufgaben exportieren ...");
        exportButton.setFocusPainted(false);
        
        exportComboBox = new JComboBox();
        exportComboBox.addItem("Termine");
        exportComboBox.addItem("Aufgaben");
        exportComboBox.addItem("Alles");
        exportComboBox.setSelectedIndex(0);
        
        JPanel buttonpanel = new JPanel(new FlowLayout());
        buttonpanel.add(backButton);
        buttonpanel.add(todayButton);
        buttonpanel.add(nextButton);    
        buttonpanel.add(Box.createHorizontalStrut(20));
        buttonpanel.add(viewDayButton);
        buttonpanel.add(viewWeekButton);
        buttonpanel.add(viewMonthButton);
        buttonpanel.add(Box.createHorizontalStrut(20));
        buttonpanel.add(newAppointmentButton);
        buttonpanel.add(newToDoButton);
        buttonpanel.add(Box.createHorizontalStrut(20));
        buttonpanel.add(monthComboBox);
        buttonpanel.add(yearComboBox);
        //buttonpanel.add(Box.createHorizontalStrut(10));
        // buttonpanel.add(m_oLabelCurrentDate);    
        buttonpanel.add(Box.createHorizontalStrut(10));
        buttonpanel.add(snapToGridCheckBox);
        buttonpanel.add(exportButton);
        buttonpanel.add(exportComboBox);
        
        
        backButton.addActionListener(new BackClickListener());
        todayButton.addActionListener(new button_today_clicked());
        nextButton.addActionListener(new button_next_clicked());
        viewDayButton.addActionListener(new DayClickListener());
        viewWeekButton.addActionListener(new button_week_clicked());
        viewMonthButton.addActionListener(new button_month_clicked());
        newAppointmentButton.addActionListener(new NewAppointmentOrTaskButtonClicked(false));
        newToDoButton.addActionListener(new NewAppointmentOrTaskButtonClicked(true));
        snapToGridCheckBox.addActionListener(new button_snap_clicked());
        exportButton.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e ) {
                clickedExportButton();
            }
        });
        
        panel.add(buttonpanel, BorderLayout.WEST);
        
        return(new EntryLayoutHelper(new TarentWidgetInterface[] { panel }, widgetFlags));
    }

    private void setCurrentDate(ScheduleDate date, boolean fire, boolean isMonthMode )
    {
        ScheduleDate oEndDate = date;
        if (getScheduleField() != null)
        {  
            if (ScheduleField.CARD_DAY.equals(getScheduleField().getCardVisible())) oEndDate = date.getDateWithAddedDays(1);  
            else if (ScheduleField.CARD_MONTH.equals(getScheduleField().getCardVisible())){
                //Erster Tag des Monats
                date = date.getFirstDayOfMonth();
                oEndDate = date.getDateWithAddedMonths(1);  
            }
            else if (ScheduleField.CARD_WEEK.equals(getScheduleField().getCardVisible())){
                //SIMON wochenansicht bei monatsauswahl fixed
                if (isMonthMode){
                    date = date.getMondayOfMonth();
                }else{
                    date = date.getMondayOfWeek();
                }
                
                
                oEndDate = date.getDateWithAddedDays(7); 
            }
            
            if (fire) getAppointmentDisplayManager().fireSetVisibleInterval(date, oEndDate, null);      
        }
        
        fillDateCombos(date);
    }

    /** Handles export button clicked event.*/
    public void clickedExportButton() {
        exportToFile();
    }

    /* Asks user for a file to save to and then pushes the spread sheet filled with data into this file.*/
    private void exportToFile() throws HeadlessException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter( new FileFilter() {
            public boolean accept( File file ) {
                return ( file.getName().toLowerCase().endsWith( ".sxc" ) && !file.isDirectory() );
            }

            public String getDescription() {
                return "sxc-Datei(*.sxc)";
            }
        } );

        if ( fileChooser.showSaveDialog( exportButton ) == JFileChooser.APPROVE_OPTION ) {
            FileOutputStream fos = null;
            File file = null;
            try {
                //select file
                if ( !fileChooser.getSelectedFile().getName().endsWith( ".sxc" ) ) {
                    file = new File( fileChooser.getSelectedFile().getPath() + ".sxc" );
                }
                else {
                    file = fileChooser.getSelectedFile();
                }
                //get output stream
                fos = new FileOutputStream( file );
                //save data
                getFilledSpreadSheet().save( fos );
            }
            catch ( FileNotFoundException e1 ) {
                logger.warning("Export failed", "File not found: " + e1.getMessage());
            }
            catch ( IOException e1 ) {
                logger.warning("Export failed", "Input/Output Exception: " + e1.getMessage());
            }
            catch ( CalendarException e ) {
                logger.warning("Export failed", e.getMessage());
            } finally {
                try{
                    //close output stream
                    if(fos != null) fos.close();
                } catch(Exception e){}
            }
        }
        else {//cancelled by user
        }
    }

    /* Returns a spread sheet filled with apprepriate data.*/
    private SpreadSheet getFilledSpreadSheet() throws CalendarException {
        SpreadSheet spreadSheet = SpreadSheetFactory.getSpreadSheet( SpreadSheetFactory.TYPE_SXC_DOCUMENT );
        try {
            spreadSheet.init();
            spreadSheet.openTable( "chr", 9 );
        }
        catch ( IOException e3 ) {
            throw new CalendarException("Export failed: Input Output Exception: " + e3.getMessage(), e3);
        }

        // Start Export here
        if ( exportComboBox.getSelectedIndex() == 0 ) {
            // Appointments only
            getDateSXC( spreadSheet );
        }
        else if ( exportComboBox.getSelectedIndex() == 1 ) {
            // Tasks only
            getTodoSXC( spreadSheet );
        }
        else {
            // All Data
            getDateSXC( spreadSheet );
            getTodoSXC( spreadSheet );
        }
        spreadSheet.closeTable();
        return spreadSheet;
    }
    
    /** Handles new appointment event.*/
    public void clickedNewAppointmentButton() {
        createNewAppointmentOrTask(false);
    }

    /** Handles new task event.*/
    public void clickedNewTaskButton() {
        createNewAppointmentOrTask(true);
    }

    /* Creates a new task for 'true' and a new appointment for 'false'.*/
    private void createNewAppointmentOrTask(boolean isTask) {
        if (getScheduleField() != null) 
        {
            int days = getScheduleField().getDisplayedDays();
            int startsecond = 8 * 60 * 60;// 8 hours
            if (days == 1){
                getScheduleField().createAppointmentOrTodo(isTask, 0, startsecond);
            } else if (days > 1){
                getScheduleField().createAppointmentOrTodo(isTask, days / 2, startsecond);
            }
        } else {
            logger.warning("Couldn't create new " + (isTask ?  "task" : "appointment"), "Schedule Panel: NULL");
        }
    }
    // END of Methods of interface CalendarVisibleElement  
}
