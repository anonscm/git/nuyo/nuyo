package org.evolvis.nuyo.db.octopus;

import java.util.Date;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentRequest;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;



/**
 * @author kleinw
 *	
 *	Implementierung AppointmentRequest.	
 *
 */
abstract public class RequestBean extends AbstractEntity implements AppointmentRequest {

    //
    //	Instanzmerkmale
    //
    /** Typ der Request */
    protected Integer _type = new Integer(2);
    /** Status der Request */
    protected Integer _status = new Integer(1);
    /** Level der Request */
    protected Integer _level = new Integer(1);
    /** Zeitpunkt der Erstellung */
    protected Date _creation;
    
    
    /**	Ursprungsdaten f�r Rollback halten */
    protected ResponseData _data;
    
    
    /**	Zur Request dazugeh�riges Appointment */
    protected Appointment _appointment;
    
    
    /** dazugeh�riges Appointment */
    public Appointment getAppointment() throws ContactDBException {return _appointment;}

    
    /**	Level der Request holen */
    public int getRequestLevel() throws ContactDBException {return(_level == null)?0:_level.intValue();}
    /**	Level setzen.@param newRequestLevel - zu setzender Level */
    public void setRequestLevel(int newRequestLevel) throws ContactDBException {_level = new Integer(newRequestLevel);}

    
    /**	Status holen */
    public int getRequestStatus() throws ContactDBException {return(_status == null)?0:_status.intValue();}
    /**	Status setzen.@param newRequestStatus - zu setzender Status */
    public void setRequestStatus(int newRequestStatus) throws ContactDBException {_status = new Integer(newRequestStatus);}

    
    /**	Typ holen */
    public int getRequestType() throws ContactDBException {return(_type == null)?0:_type.intValue();}
    /**	Typ setzen.@param typ - Typ setzen */
    public void setRequestType(int newRequestType) throws ContactDBException {_type = new Integer(newRequestType);}

    
    /** Kreationsdatum holen */
	public Date getCreation() throws ContactDBException {return _creation;}
 
    
}
