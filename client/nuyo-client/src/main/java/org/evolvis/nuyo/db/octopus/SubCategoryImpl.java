package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.remote.Method;

public class SubCategoryImpl extends SubCategory {

    public SubCategoryImpl() {       
    }


    public void create() throws ContactDBException {
        (new Method("createVerteilerSOAP"))
            .add("gruppe", ""+getParentCategory())
            .add("name1", getSubCategoryName())
            .add("name2", getDescription())
            .invoke();
       
    }
    
    public void commit() throws ContactDBException {
        Method method = new Method("modifySubCategory");
        method.add("categoryPk", getParentCategory());
        method.add("subCategoryPk", getIdAsInteger());
        if (getSubCategoryName() != null)
            method.add("subCategoryName", getSubCategoryName());
        if (getDescription() != null) 
            method.add("subCategoryDescription", getDescription());
        //System.out.println("Subcategory.commit(): " + method.toString());
        String tmp = (String)method.invoke();
        if (tmp != null && tmp.length() > 0) 
            throw new ContactDBException(ContactDBException.EX_OCTOPUS, tmp);
    }


    public void delete()
    	throws ContactDBException {
        Method method = new Method("deleteVerteilerSOAP");
        method.add("key", getIdAsInteger());
        method.add("verteilergruppe", ""+getParentCategory());
        method.invoke();
    }

}
