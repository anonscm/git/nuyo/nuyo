package org.evolvis.nuyo.db.filter;

import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author kleinw
 *
 *	Filterfeld f�r Stringwerte
 *
 */
abstract public class AbstractStringSelection implements SelectionElement {

    /**	Hier wird der Wert gehalten */
    protected String _string;
    
    
    /**	Konstruktor */
    public AbstractStringSelection(String string) {_string = string;}
    
    
    /**	Eintrag in die Map */
    public void includeInMethod(Map map) throws ContactDBException {map.put(getFilterKey(), _string);}
   
    
    /**	Der Feldname wird von der abgeleiteten Klasse gesetzt */
    abstract public String getFilterKey() throws ContactDBException;
    
    
    public String getString () {return _string;} 
    
    
    public boolean equals(Object arg0) {
        if(arg0 instanceof AbstractStringSelection) {
            AbstractStringSelection selection = (AbstractStringSelection)arg0;
            return (selection.getString().equals(_string))?true:false;
        }
        return false;
    }
    
    
    /**	Stringrepr�sentation */
    public String toString () {return new StringBuffer().append("str = ").append(_string).toString();}
    
    
    /**	Filter f�r UserLogin */
    static public class UserLogin extends AbstractStringSelection {
        public UserLogin(String userLogin) {super(userLogin);}
        public String getFilterKey() throws ContactDBException {return "userlogin";}
    }
    
}
