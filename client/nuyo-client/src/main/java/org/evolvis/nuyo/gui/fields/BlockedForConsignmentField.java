/*
 * Created on 14.04.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.util.logging.Logger;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetCheckBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.plugin.PluginData;



/**
 * @author Nils Neumaier, tarent GmbH
 *
 */
public class BlockedForConsignmentField extends ContactAddressField
{  
  private static Logger logger = Logger.getLogger(BlockedForConsignmentField.class.getName());
  
 // private JLabel m_oInfoLabel;
  
  
  private TarentWidgetCheckBox checkbox;
  
  private EntryLayoutHelper m_oPanel = null;
  private Boolean ischecked = new Boolean(false);
  
  public String getFieldName()
  {
    return "BlockedForConsignment";    
  }
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public String getFieldDescription()
  {
    return "Ein Feld zur Anzeige des Sperrrvermerks";
  }
  
  
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if(m_oPanel == null) m_oPanel = createPanel(widgetFlags);
    return m_oPanel;
  }
  
  
  public void postFinalRealize()
  {    
  }
  
  public void setData(PluginData data)
  {
  	ischecked = (Boolean) data.get(AddressKeys.BLOCKEDFORCONSIGNMENT);
  	  	
  	if (ischecked != null)
  		checkbox.setSelected(ischecked.booleanValue());
  	else
  		checkbox.setSelected(false);
  }
    
  public void getData(PluginData data)
  {
  	ischecked = new Boolean(checkbox.isSelected());
  	
  	if (ischecked != null){
  		data.set(AddressKeys.BLOCKEDFORCONSIGNMENT, (Boolean) ischecked);
  	}
  }

  
  public boolean isDirty(PluginData data)
  {
	  if (data.get(AddressKeys.BLOCKEDFORCONSIGNMENT) != null 
			  && checkbox.isSelected() != ((Boolean) data.get(AddressKeys.BLOCKEDFORCONSIGNMENT)).booleanValue()) 
	    	  		
		  return(true);
	      
	  return false;
  }

  public void setEditable(boolean iseditable)
  {
  	checkbox.setEnabled(iseditable);
  }

  public String getKey()
  {
    return "BLOCKEDFORCONSIGNMENT";
  }

  public void setDoubleCheckSensitive(boolean issensitive)
  {
  }  
  
  private EntryLayoutHelper createPanel(String widgetFlags)
  {
    checkbox = new TarentWidgetCheckBox();
    checkbox.setEnabled(false);
    
    return(new EntryLayoutHelper(new TarentWidgetInterface[] {
      new TarentWidgetLabel(Messages.getString("GUI_BlockedForConsignment")), checkbox }, widgetFlags));
  }  
}
