/*
 * ArrayListTransferHandler.java is used by the 1.4
 * DragListDemo.java example.
 */

package org.evolvis.nuyo.gui.dnd;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

public class TarentDnDTransferHandler extends TransferHandler 
{
  private static Logger logger = Logger.getLogger(TarentDnDTransferHandler.class.getName());

  TarentDnDListener m_oDnDListener;

  private List m_oLocalDnDDataFlavorsDrop;
  private List m_oSerialDnDDataFlavorsDrop;

  private List m_oLocalDnDDataFlavorsDrag;
  private List m_oSerialDnDDataFlavorsDrag;

  public TarentDnDTransferHandler(TarentDnDListener tdndl) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::tarentDnDMultipleTransferHandler()");    
    m_oDnDListener = tdndl;

    m_oLocalDnDDataFlavorsDrop = new ArrayList();
    m_oSerialDnDDataFlavorsDrop = new ArrayList();

    m_oLocalDnDDataFlavorsDrag = new ArrayList();
    m_oSerialDnDDataFlavorsDrag = new ArrayList();
  }

  public void addDataFlavor(Class transferclass, boolean usefordrag, boolean usefordrop)
  {
    // System.out.println("tarentDnDMultipleTransferHandler::addDataFlavor()");    
    String transfername = transferclass.getName();    

    DataFlavor oLocalDnDDataFlavor = null;
    DataFlavor oSerialDnDDataFlavor = null;
    String localDnDDataType;    

    localDnDDataType = DataFlavor.javaJVMLocalObjectMimeType + ";class=" + transfername;
    try 
    {
      oLocalDnDDataFlavor = new DataFlavor(localDnDDataType);
    } 
    catch (ClassNotFoundException e) 
    {
        logger.info("tarentDnDTransferHandler: unable to create data flavor");
        //e.printStackTrace();        
    }
    oSerialDnDDataFlavor = new DataFlavor(transferclass, transfername);

    if (usefordrag)
    {    
      m_oLocalDnDDataFlavorsDrag.add(oLocalDnDDataFlavor);
      m_oSerialDnDDataFlavorsDrag.add(oSerialDnDDataFlavor);
    }

    if (usefordrop)
    {    
      m_oLocalDnDDataFlavorsDrop.add(oLocalDnDDataFlavor);
      m_oSerialDnDDataFlavorsDrop.add(oSerialDnDDataFlavor);
    }
  }


  public boolean importData(JComponent comp, Transferable trans) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::importData()");        
    Object data = null;
    if (!canImport(comp, trans.getTransferDataFlavors())) 
    {
      return false;
    }
    try 
    {
      DataFlavor localflavor;
      DataFlavor serialflavor;
      if ((localflavor = getLocalDnDDataFlavor(trans.getTransferDataFlavors())) != null) 
      {
        data = trans.getTransferData(localflavor);
      } 
      else if ((serialflavor = getSerialDnDDataFlavor(trans.getTransferDataFlavors())) != null) 
      {
        data = trans.getTransferData(serialflavor);
      } 
      else 
      {
        return false;
      }
    } 
    catch (UnsupportedFlavorException ufe) 
    {
        logger.info("tarentDnDTransferHandler: importData: unsupported data flavor");
      return false;
    } 
    catch (IOException ioe) 
    {
      logger.info("tarentDnDTransferHandler: importData: I/O exception");
      return false;
    }

    m_oDnDListener.objectDropped(comp, data);
    return true;
  }

  protected void exportDone(JComponent c, Transferable data, int action) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::exportDone()");    
    m_oDnDListener.exportDone(c); 
  }

  private DataFlavor getLocalDnDDataFlavor(DataFlavor[] flavors) 
  {
    if (m_oLocalDnDDataFlavorsDrop == null) 
    {
      return null;
    }

    if (m_oLocalDnDDataFlavorsDrop.size() == 0) 
    {
      return null;
    }

    for (int i = 0; i < flavors.length; i++) 
    {
      for(int a=0; a<(m_oLocalDnDDataFlavorsDrop.size()); a++)
      {
        if (flavors[i].equals(m_oLocalDnDDataFlavorsDrop.get(a))) 
        {
          return (DataFlavor)(m_oLocalDnDDataFlavorsDrop.get(a));
        }
      }
    }
    return null;
  }

  private DataFlavor getSerialDnDDataFlavor(DataFlavor[] flavors) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::getSerialDnDDataFlavor()");    
    if (m_oSerialDnDDataFlavorsDrop == null) 
    {
      return null;
    }

    if (m_oSerialDnDDataFlavorsDrop.size() == 0) 
    {
      return null;
    }

    for (int i = 0; i < flavors.length; i++) 
    {
      for (int a=0; a<(m_oSerialDnDDataFlavorsDrop.size()); a++)
      if (flavors[i].equals(m_oSerialDnDDataFlavorsDrop.get(a))) 
      {
        return (DataFlavor)(m_oSerialDnDDataFlavorsDrop.get(a));
      }
    }
    return null;
  }

  public boolean canImport(JComponent comp, DataFlavor[] flavors) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::canImport()");    
    if (getLocalDnDDataFlavor(flavors) != null)  
    { 
      return true; 
    }
    
    if (getSerialDnDDataFlavor(flavors) != null) 
    { 
      return true; 
    }
    
    return false;
  }

  protected Transferable createTransferable(JComponent comp) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::createTransferable()");    
    Object data = m_oDnDListener.objectDragged(comp);
    return new tarentDnDDataTransferable(data);
  }

  public int getSourceActions(JComponent comp) 
  {
    // System.out.println("tarentDnDMultipleTransferHandler::getSourceActions()");    
    return(COPY_OR_MOVE);
  }

  public class tarentDnDDataTransferable implements Transferable 
  {
    private Object m_oData;

    public tarentDnDDataTransferable(Object data) 
    {
      // System.out.println("tarentDnDDataTransferable::tarentDnDDataTransferable()");    
      m_oData = data;
    }

    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException 
    {
      // System.out.println("tarentDnDDataTransferable::getTransferData()");    
      if (!isDataFlavorSupported(flavor)) 
      {
        throw new UnsupportedFlavorException(flavor);
      }
      return m_oData;
    }

    public DataFlavor[] getTransferDataFlavors() 
    {
      // System.out.println("tarentDnDDataTransferable::getTransferDataFlavors()");    
      DataFlavor[] flavorsarray = new DataFlavor[(m_oLocalDnDDataFlavorsDrag.size() + m_oSerialDnDDataFlavorsDrag.size())];
      int i=0;
      for(int a=0; a<(m_oLocalDnDDataFlavorsDrag.size()); a++)
      {
        flavorsarray[i++] = (DataFlavor)(m_oLocalDnDDataFlavorsDrag.get(a));
      }

      for(int a=0; a<(m_oSerialDnDDataFlavorsDrag.size()); a++)
      {
        flavorsarray[i++] = (DataFlavor)(m_oSerialDnDDataFlavorsDrag.get(a));
      }

      return (DataFlavor[])(flavorsarray);
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) 
    {
      // System.out.println("tarentDnDDataTransferable::isDataFlavorSupported()");    
      if (m_oLocalDnDDataFlavorsDrag.contains(flavor)) 
      {
        return true;
      }
      
      if (m_oSerialDnDDataFlavorsDrag.contains(flavor)) 
      {
        return true;
      }
      
      return false;
    }
    
  }
}
