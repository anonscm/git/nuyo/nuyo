/*
 * Created on 24.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author niko
 *
 */
public class ParameterDescription
{
  private String   m_sKey;
  private String   m_sName;
  private String   m_sDescription;
  private List     m_oValueDescriptions;
  
  public ParameterDescription(String key, String name, String description, List valuedescriptions)
  {
    m_sKey = key;
    m_sName = name;
    m_sDescription = description; 
    m_oValueDescriptions = valuedescriptions;
  }
  
  public ParameterDescription(String key, String name, String description)
  {
    m_sKey = key;
    m_sName = name;
    m_sDescription = description; 
    m_oValueDescriptions = null;
  }
  
  public ParameterDescription(String key, String name)
  {
    m_sKey = key;
    m_sName = name;
    m_sDescription = "Parameter \"" + name + "\""; 
    m_oValueDescriptions = null;
  }
  
  public ParameterDescription(String key, String name, String description, String valuedescriptionkey, String valuedescription)
  {
    m_sKey = key;
    m_sName = name;
    m_sDescription = description;     
    addValueDescription(valuedescriptionkey, valuedescription);        
  }

  public void addValueDescription(String valuekey, String valuedescription)
  {
    if (m_oValueDescriptions == null) m_oValueDescriptions = new ArrayList();
    String[] valdes = new String[2];
    valdes[0] = valuekey;
    valdes[1] = valuedescription;
    m_oValueDescriptions.add(valdes);
  }

  public List getValueDescriptions()
  {
    return m_oValueDescriptions;
  }

  public String getDescription()
  {
    return m_sDescription;
  }

  public String getKey()
  {
    return m_sKey;
  }

  public String getName()
  {
    return m_sName;
  }
}
