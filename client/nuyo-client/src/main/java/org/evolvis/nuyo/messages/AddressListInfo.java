/* $Id: AddressListInfo.java,v 1.2 2006/06/06 14:12:07 nils Exp $
 * 
 * Created on 09.10.2003
 */
package org.evolvis.nuyo.messages;

import java.util.EventObject;

/**
 * Dies ist die Basisklasse f�r Informationsereignisse, die die Adressliste
 * betreffen.
 *  
 * @author mikel
 */
public class AddressListInfo extends EventObject implements InfoEvent {
    /**
     * @param source Ereignisquelle
     */
    public AddressListInfo(Object source) {
        super(source);
    }

}
