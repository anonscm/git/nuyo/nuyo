package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.filter.Selection;
import org.evolvis.nuyo.db.filter.AbstractBooleanSelection.IsCreate;
import org.evolvis.nuyo.db.filter.AbstractBooleanSelection.IsPersonal;
import org.evolvis.nuyo.db.filter.AbstractIdSelection.UserId;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractPersistentRelation;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.remote.Method;



/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.User
 *
 */
abstract public class UserBean extends AbstractEntity implements User {

    private static Logger logger = Logger.getLogger(UserBean.class.getName());

    //
    //	Instanzmerkmale
    //
//    Alle Kategorien eines beliebigen Users braucht eigentlich niemand zu sehen.
//     /**	1-n Beziehung User => Kategorien */
//     protected AbstractPersistentRelation _categories;
    /**	1-n Beziehung User => UserGroups */
    protected AbstractPersistentRelation _userGroups;
    /**	1-n Beziehung User => Calendars */
    protected AbstractPersistentRelation _calendars;
    /**	Objekt zum Holen von Usern */
    static protected AbstractEntityFetcher _fetcher;
    
    protected List _proxyList = null;

    /** Daten des pers�nlichen Kalenders, wird pro Userobjekt einmalig von Server geholt. */
    Calendar _personalCal = null;


    /**
     * Berechtigungen eines Users auf Kategorien.
     * Keys sind die Integer Kategory IDs, Values sind UserObjectRoleImpl Objekte
     */
    Map categoryRights = null;
        
    /**	Status des Benutzers 
     * @throws ContactDBException*/
    public boolean isAdminUser(){
        try {
            return USER_TYPE_ADMIN == Integer.parseInt(getAttribute(User.KEY_USER_TYPE));
        } catch (NumberFormatException nfe) {
            logger.log(Level.WARNING, "Kann Usertype von User nicht parsen", nfe);
            return false;
        }
    }
    
    /**	Loginname des Benutzers */
    public String getLoginName() throws ContactDBException {return (String)_attributes.get(KEY_LOGIN_NAME);}
        
    
    /**	Bezieht alle dem Benutzer zugeordneten Gruppen */
    public Collection getGroups() throws ContactDBException {return _userGroups.getActualRelations();}
    /**	F�gt eine Benutzergruppe hinzu. @param group - Gruppe, dem der Benutzer hinzugef�gt werden soll */
    public void add(UserGroup group) throws ContactDBException {_userGroups.add((IEntity)group);}
    /**	L�scht eine UserGroup vom Benutzer. @param group - zu l�schende Gruppe */
    public void remove(UserGroup group) throws ContactDBException {_userGroups.remove((IEntity)group);}
    
    
    
    /**	Alle dem Benutzer zugeordneten Kalender. */
    public Collection getCalendars() throws ContactDBException {return _calendars.getActualRelations();}
    /**	F�gt einen Kalendar hinzu. @param - Kalender, der hinzugef�gt werden soll */
    public void add(Calendar calendar) throws ContactDBException {_calendars.add((IEntity)calendar);}
    /**	L�scht einen Kalender. @param - zu l�schender Kalender */
    public void remove(Calendar calendar) throws ContactDBException {_calendars.remove((IEntity)calendar);}
    /**	Darf der Benutzer in diesem Kalender lesen */
    public boolean canRead(Calendar calendar) throws ContactDBException {return getReleaseType(calendar);}
    /**	Darf der Benutzer in diesem Kalender schreiben */
    public boolean canWrite(Calendar calendar) throws ContactDBException {return getReleaseType(calendar);}

    
    /**	Pers�nlicher Kalender des Benutzers. @param create - Ist noch kein Kalender zugeordnet, wird er implizit erzeugt. */
    public Calendar getCalendar(boolean create) throws ContactDBException {
    	
        if (_personalCal == null) {
            ISelection selection = Selection.getInstance()
                .add(new UserId(new Integer(getId())))
                .add(new IsPersonal(new Boolean(true)))
                .add(new IsCreate(new Boolean(true)));
            
            List tmp = (List)CalendarImpl.getCalendars(selection);
            if(tmp != null && tmp.size() == 1){
                _personalCal = (Calendar)tmp.get(0);
            }
            else
                throw new ContactDBException(ContactDBException.EX_INSUFFICIENT_SERVER_DATA, "Beim Holen des pers�nlichen Kalenders trat ein Fehler auf.");
        }
        return _personalCal;
    }
    
    
    /**	Hier kann ein Parameter des Users bezogen werden. @param key - Schl�ssel des Parameters */
    public String getParameter(String key) throws ContactDBException {
        Method method = new Method("getUserParameter");
        method.add("userid", _id);
        if(key != null)
            method.add("parameter", key);
        List tmp = (List)method.invoke();
        if(tmp != null && tmp.size() > 0) {
            return (String)tmp.get(0);
        } else
            return null;
    }
    /**	Hier kann ein Parameter gespeichert werden. @param key - Schl�ssel, @param value - Wert */
    public void setParameter(String key, String value) throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED, "Diese Funktion ist noch nicht implementiert.");
    }

        
    /**	Soapmethode f�r Lese- oder Schreibmodus bzgl. eines Kalendars */
    private boolean getReleaseType(Calendar calendar) throws ContactDBException {
        Method method = new Method("getScheduleReleaseType");
        method.add("userid", new Integer(getId()));
        method.add("scheduleid", new Integer(calendar.getId()));
        Integer tmp = (Integer)method.invoke();
        if(tmp == null)
            return false;
        else if(tmp.intValue() == 1)
            return true;
        else
            return false;
    }
    
}
