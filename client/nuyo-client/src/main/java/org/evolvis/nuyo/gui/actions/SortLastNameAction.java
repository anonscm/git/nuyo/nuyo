package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

public class SortLastNameAction extends AbstractGUIAction {

    private static final long serialVersionUID = -3939395627935356208L;
    private static final TarentLogger log = new TarentLogger(SortLastNameAction.class);

    public void actionPerformed(ActionEvent e) {
        log.warning("SortLastNameAction not implemented");
    }

}