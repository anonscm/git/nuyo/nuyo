/*
 * Created on 14.04.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetButton;
import org.evolvis.nuyo.controls.TarentWidgetCheckBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetLabel;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressDateField;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Environment.Key;


/**
 * @author nils
 *
 */
public class FollowupField extends ContactAddressDateField
{  
  private static Logger logger = Logger.getLogger(FollowupField.class.getName());
  
 // private JLabel m_oInfoLabel;
  
  
  private TarentWidgetButton m_oButton_Change;
  private TarentWidgetCheckBox checkbox;
  
  private EntryLayoutHelper m_oPanel = null;
  private Boolean ischecked = new Boolean(false);
  private int daysToAdd = 5;
  
  private boolean complained;
  
  public String getFieldName()
  {
    return "Followup";    
  }
  
  public int getContext()
  {
    return CONTEXT_ADRESS;
  }
  
  public String getFieldDescription()
  {
    return "Ein Feld zur Anzeige der Wiedervorlage";
  }
  
  
  
  public EntryLayoutHelper getPanel(String widgetFlags)
  {
    if(m_oPanel == null) m_oPanel = createPanel(widgetFlags);
    return m_oPanel;
  }
  
  
  public void postFinalRealize()
  {    
  }
  
  public void setData(PluginData data)
  {
	complained = false;
  	ischecked = (Boolean) data.get(AddressKeys.FOLLOWUP);
  	Date date = (Date) data.get(AddressKeys.FOLLOWUPDATE);
  	  	
  	if (ischecked != null && date != null){
  		checkbox.setSelected(ischecked.booleanValue());
  		dateField.setEditable(ischecked.booleanValue());
  	}
  	else{checkbox.setSelected(false);}
  	
  	if (!checkbox.isSelected()){			
		date = null;	  		
	}
  	
  	setTextFieldFromDate(date);
  }

  public void getData(PluginData data)
  {
  	ischecked = new Boolean(checkbox.isSelected());
  	
  	if (ischecked != null){
  		data.set(AddressKeys.FOLLOWUP, (Boolean) ischecked);
  	}
  	
  	Date dateToSave = getDateFromTextField();
  	
  	if (dateToSave == null && ischecked ){
		
		if (!complained){
			JOptionPane.showMessageDialog(null, Messages.getString("GUI_FollowUpField_DateFormatException") + Messages.getString("GUI_FollowUpField_FormatPattern_Msg"));
			complained = true;
                       
        	}
		 
        // Use the last known date which was ok to be written into
        // the database.
        
        // TODO in my opinion the address-saving-process should be interrupted! -- Fabian
        dateToSave = getLastDate();
  	}
        
  	data.set(AddressKeys.FOLLOWUPDATE, dateToSave);
  }
  
  public boolean isDirty(PluginData data)
  {
	  if (data.get(AddressKeys.FOLLOWUP) != null)
	      if (checkbox.isSelected() != ((Boolean) data.get(AddressKeys.FOLLOWUP)).booleanValue()) 
	    	  	return(true);
	  Date dateToSave = null;
		
	  try {
		  dateToSave = shortYearDateFormat.parse(dateField.getText());
	  } catch (ParseException e) {
		  // do nothing
	  }
	
	  if (dateToSave == null)
		  if (((Date)(data.get(AddressKeys.FOLLOWUPDATE))) != null) 
		  	  return(true);
	  
	  else
	  {
		  if (((Date)(data.get(AddressKeys.FOLLOWUPDATE))) != null)
			  if (!(((Date)(data.get(AddressKeys.FOLLOWUPDATE))).equals(dateToSave))) 
				  return(true);
	  }   
	  return false;
  }

  public void setEditable(boolean iseditable)
  {
  	checkbox.setEnabled(iseditable);
    
    if (checkbox.isSelected()){
    	dateField.setEditable(iseditable);
        checkbox.setEnabled(iseditable);
        m_oButton_Change.setEnabled(iseditable);
        //if (((String)datespinner.getData()).equals("")){
        //	Date tmpDate = new Date();
        //	datespinner.setData(tmpDate.getDate() + "." + tmpDate.getMonth() + "." + tmpDate.getYear());
        //}
    }
    else {
    	dateField.setData("");
    	//datespinner.setEnabled(false);
        m_oButton_Change.setEnabled(false);
        
    }
  }

  public String getKey()
  {
    return "FOLLOWUP";
  }

  public void setDoubleCheckSensitive(boolean issensitive)
  {
  }
  
  private EntryLayoutHelper createPanel(String widgetFlags)
  {
    daysToAdd = ConfigManager.getEnvironment().getAsInt(Key.FOLLOW_UP_DAYS_TO_ADD);
    setShortDateFormat(Messages.getString("GUI_FollowUpField_ShortYearFormatPattern"));
	setDateformat(Messages.getString("GUI_FollowUpField_FormatPattern"));
	
    
    m_oButton_Change = new TarentWidgetButton(Messages.getFormattedString("GUI_FollowUpField_Add", new Integer(daysToAdd)));
    m_oButton_Change.setToolTipText(Messages.getFormattedString("GUI_FollowUpField_Add_ToolTip", new Integer(daysToAdd)));
    m_oButton_Change.addActionListener(new ChangeButtonClicked());
    m_oButton_Change.setEnabled(false);
    
    checkbox = new TarentWidgetCheckBox();
    checkbox.addActionListener(new CheckboxDisabled());
    checkbox.setEnabled(false);
    
    return(new EntryLayoutHelper(new TarentWidgetInterface[] {
      new TarentWidgetLabel(Messages.getString("GUI_FollowUpField_Label")),
      checkbox,
      dateField,
      m_oButton_Change }, widgetFlags));
  }

  private class ChangeButtonClicked implements ActionListener
  {
    public void actionPerformed(ActionEvent e){
    
    	if (getDateFromTextField() != null){
    		Date tmpdate = getDateFromTextField();
    		ScheduleDate schedDate = new ScheduleDate(tmpdate);
    		tmpdate = schedDate.getDateWithAddedDays(daysToAdd).getDate();
    		setTextFieldFromDate(tmpdate);
    	}
    }
  }
  
  
  
  private class CheckboxDisabled implements ActionListener
  {
    public void actionPerformed(ActionEvent e){
    	
    	boolean checkBoxIsChecked = checkbox.isSelected();
    	
    	dateField.setEditable(checkBoxIsChecked);
    	m_oButton_Change.setEnabled(checkBoxIsChecked);
    	
    	if (!checkBoxIsChecked){
    		dateField.setText("");
    	}
    	else{
    		if (dateField.getText().equals("")){
    		
    			//**** Schlage Datum vor damit das TextFeld nicht leer ist ***
    			
	    		Date tmpdate = new Date();
	        	ScheduleDate schedDate = new ScheduleDate(tmpdate);
	        	tmpdate = schedDate.getDateWithAddedDays(daysToAdd).getDate();
	        	setTextFieldFromDate(tmpdate);    			
			}
		}    		
    }
  }
}
