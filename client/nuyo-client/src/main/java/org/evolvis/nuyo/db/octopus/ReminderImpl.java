package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentReminder;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;



/**
 * @author kleinw
 *
 *	Implementierung von EventReminder.
 *
 */
public class ReminderImpl extends ReminderBean {

    /**	Erzeugen des Objekts per DB-Inhalt */
    protected ReminderImpl() {};

    
    /**	Anlegen eines neuen Objekts */
    protected ReminderImpl(Appointment appointment, Calendar calendar) {
        _calendar = calendar;
        _appointment = appointment;
    }

    
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}
   
    
    /**	Der Reminder wird zusammen mit dem Appointment gespeichert */
    public void commit() throws ContactDBException {setDirty(true);}
    
    
    /**	Der Reminder wird nur am Appointmentobjekt gel�scht */
    public void delete() throws ContactDBException {setDeleted(true);}

    
    /**	Wiederherstellung des Reminders */
    public void rollback() throws ContactDBException {
        populate(_responseData);
        
    }
 
    
    /**	F�llen eines neuen Objekts anhand von DB-Werten */
    public void populate(ResponseData data) throws ContactDBException {
        _id = data.getInteger(0);
        _channel = data.getInteger(1);
        setAttribute(AppointmentReminder.KEY_NOTE, data.getString(2));
        _timeUnit = data.getInteger(3);
        _timeOffset = data.getInteger(4);
    }
    
    
    /**	Stringrepr�sentation des Reminders */
    public String toString() {
        return new StringBuffer()
        	.append("Reminder : ")
        	.append(" ")
        	.append(_attributes)
        	.append(" ")
        	.append(_channel)
        	.append(" ")
        	.append(_timeUnit)
        	.append(" ")
        	.append(_timeOffset)
        	.toString();
    }
    
}
