/* $Id: GroupObjectRoleConnector.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

/**
 *
 * Verbindung von Gruppe, Objekt mit eine Rolle
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public interface GroupObjectRoleConnector {
	public Object getObject();
	public void setObject(Object o);
	
	public ObjectRole getRole();
	public void setRole(ObjectRole r);
	
	public UserGroup getGroup();
	public void setGroup(UserGroup ug);
}
