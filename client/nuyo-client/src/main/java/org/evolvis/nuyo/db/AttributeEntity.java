package org.evolvis.nuyo.db;

import java.util.Iterator;




public interface AttributeEntity {

    public AttributeEntity createInstance(Database db) throws ContactDBException ;
    public Object getAttribute(String key) throws ContactDBException;
    public void setId(String id) throws ContactDBException;
    public void setAttribute(String key, Object value) throws ContactDBException;

    public Iterator getEntityProperties();
}