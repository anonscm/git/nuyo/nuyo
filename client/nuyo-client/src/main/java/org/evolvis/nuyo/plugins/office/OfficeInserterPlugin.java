/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.plugin.DialogAddressListPerformer;

import de.tarent.commons.plugin.Plugin;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeInserterPlugin implements Plugin
{
	private OfficeInserter officeInserter;
	public static final String ID = "office_inserter";
	
	public OfficeInserterPlugin()
	{
		
	}

	public String getID()
	{
		return ID;
	}

	public Object getImplementationFor(Class pType)
	{
		if(pType.equals(DialogAddressListPerformer.class))
		{
			if(officeInserter == null) init();
			return officeInserter;
		}
		return null;
	}

	public List getSupportedTypes()
	{
		List types = new ArrayList();
		types.add(DialogAddressListPerformer.class);
		return types;
	}

	public void init()
	{
		officeInserter = new OfficeInserter();
	}

	public boolean isTypeSupported(Class pType)
	{
		return pType.equals(DialogAddressListPerformer.class);
	}

	public String getDisplayName() {
		return "Office Insert";
	}
}
