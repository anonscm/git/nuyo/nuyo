package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Resource;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;

/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.Resource
 *
 */
abstract public class ResourceBean  extends AbstractEntity implements Resource{

    //
    //	Instanzmerkmale
    //
    /**	Autopublishing */
    protected Boolean _isAutoPublishing = new Boolean(false);
    /** Beschaffung von DB-Daten */
    static protected AbstractEntityFetcher _fetcher;
    
    
    /** Modus lesen */
    public boolean isAutoPublishing() throws ContactDBException {return (_isAutoPublishing == null)?false:_isAutoPublishing.booleanValue();}
    /**	Modus schreiben.@param newAutoPublishing - zu setzender Modus */
    public void setAutoPublishing(boolean newAutoPublishing) {_isAutoPublishing = new Boolean(newAutoPublishing);}
    
}
