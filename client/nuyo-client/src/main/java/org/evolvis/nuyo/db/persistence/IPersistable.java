package org.evolvis.nuyo.db.persistence;

import org.evolvis.nuyo.db.ContactDBException;

/**
 * @author kleinw
 *
 *	Das Persistable Objekt enth�lt unten definierte
 *	Methoden, um in irgendeiner Weise speicherbar
 *	zu sein.
 *
 *	Das Interface ist bewusst einfach gehalten,
 *	da hier neben der klassischen RDBS auch ein
 *	Dateisystem gemeint sein k�nnte.
 *
 */
public interface IPersistable {

    
    /**	Ist die Entit�t vollst�ndig? */
    public void validate() throws ContactDBException;
    
    
    /**	Speichern oder �ndern des Objekts */
    public void commit() throws ContactDBException;
    
    
    /**	Objekt in der Datenquelle l�schen */
    public void delete() throws ContactDBException;
    
    
    /**	Objekt nach (lokalen) �nderungen wiederherstellen */
    public void rollback() throws ContactDBException;
    
}
