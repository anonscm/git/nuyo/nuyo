package org.evolvis.nuyo.util.general;

/*
 * Created on 13.10.2004
 *
 */



/**
 * @author niko
 *
 */
public interface DataAccess
{    
  public Object getLookupTableValue(String key, Object data) throws ContactLookUpNotFoundException;
  public String[] getLookupTableEntries(String key) throws ContactLookUpNotFoundException;  
}
