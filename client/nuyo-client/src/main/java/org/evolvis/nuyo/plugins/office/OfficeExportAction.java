/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.plugin.DialogAddressListPerformer;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.xana.action.AbstractThreadedGUIAction;

import de.tarent.commons.plugin.Plugin;

/**
 * 
 * This action is part of the office-plugin and exports an address-list for processing by office-integration
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */

public class OfficeExportAction extends AbstractThreadedGUIAction implements AddressListConsumer
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6700579978665067951L;

	private TarentLogger logger = new TarentLogger(OfficeExportAction.class);

	private AddressListProvider provider;

	public void eventOccured(ActionEvent e)
	{
		ApplicationServices.getInstance().getActionManager().setWaiting(true);
		
		DialogAddressListPerformer performer = retrieveDialogAddressListPerformer();

		if(performer != null)
		{
			performer.setAddresses(provider.getAddresses());
			performer.execute();
		}
		else
			logger.warning(Messages.getString(this, "Load_Plugin_Failure"));

		ApplicationServices.getInstance().getActionManager().setWaiting(false);
	}

	public void registerDataProvider(AddressListProvider pProvider)
	{
		provider = pProvider;
	}

	public DialogAddressListPerformer retrieveDialogAddressListPerformer()
	{
		Plugin officePlugin = PluginRegistry.getInstance().getPlugin(OfficeExporterPlugin.ID);
		DialogAddressListPerformer addListPerf = null;
		if (officePlugin == null)
		{
			logger.warning("Plugin " + OfficeExporterPlugin.ID + " kann nicht geladen werden");
			return null;
		}

		if (officePlugin.isTypeSupported(DialogAddressListPerformer.class))
		{
			addListPerf = (DialogAddressListPerformer)officePlugin.getImplementationFor(DialogAddressListPerformer.class);
		} 
		else {
			logger.info("Angeforderter Plugin-Typ "+ DialogAddressListPerformer.class.getName() + " wird nicht unterst�tzt.");
		}

		return addListPerf;
	}
}