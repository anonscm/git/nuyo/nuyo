/**
 * 
 */
package org.evolvis.nuyo.plugins.mail.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.plugins.mail.DispatchEMailDialog;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class SaveCurrentTemplateAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6674639365695102565L;

	public void actionPerformed(ActionEvent e)
	{
		DispatchEMailDialog.getInstance().saveCurrentTemplate();
	}
}
