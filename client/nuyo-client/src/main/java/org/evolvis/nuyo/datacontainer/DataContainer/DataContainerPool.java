/*
 * Created on 08.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

import java.util.List;

/**
 * @author niko
 *
 */
public interface DataContainerPool
{
  public void addDataContainer(DataContainer datacontainer);
  public void removeDataContainer(DataContainer datacontainer);
  public DataContainer getDataContainer(String key);
  public boolean containsDataContainer(String key);
  public List getUsedDataContainers();
}
