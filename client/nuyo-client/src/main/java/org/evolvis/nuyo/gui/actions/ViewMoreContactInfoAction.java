package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Activates a view with additional information of the current contact.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ViewMoreContactInfoAction extends AbstractGUIAction {

    private static final long serialVersionUID = 2949882384466045005L;
    private static final TarentLogger logger = new TarentLogger(ViewMoreContactInfoAction.class);
    private ControlListener mainFrame;

    public void actionPerformed(ActionEvent e) {
        if(mainFrame != null) {
            mainFrame.activateTab(MainFrameExtStyle.TAB_EXTENDED);
        } else logger.warning(getClass().getName() + "is not initialized.");
    }
    
    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();
        setEnabled(mainFrame.isTabEnabled(MainFrameExtStyle.TAB_EXTENDED));
    }
}
