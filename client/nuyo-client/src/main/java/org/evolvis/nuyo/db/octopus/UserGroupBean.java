package org.evolvis.nuyo.db.octopus;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.AbstractPersistentRelation;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;



/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.UserGroup.
 *
 */
abstract public class UserGroupBean extends AbstractEntity implements UserGroup {

    private static Logger logger = Logger.getLogger(UserGroupBean.class.getName());

    /** Zur Gruppe dazugeh�riger Kalender */
    protected Calendar _calendar;
	/**	1-n Beziehung UserGroup => User */
	protected AbstractPersistentRelation _users;
	/**	Objekt zum Laden bestimmter Benutzergruppen */
	static protected AbstractEntityFetcher _fetcher;
		

    public boolean isPrivate() {
        return (new Boolean(getAttribute(UserGroup.KEY_IS_PRIVATE))).booleanValue();
    }

    public boolean isAdminGroup() {
        try {
            return GROUP_TYPE_ADMIN == Integer.parseInt(getAttribute(UserGroup.KEY_GROUP_TYPE));
        } catch (NumberFormatException nfe) {
            logger.log(Level.WARNING, "Kann Grouptype von UserGroup nicht parsen. Ausdruck: " + getAttribute(UserGroup.KEY_GROUP_TYPE) , nfe);
            return false;
        }
    }

	/**	Alle Benutzer der Gruppe */	
	public Collection getUsers() throws ContactDBException {
	    return _users.getActualRelations();
	}
	/** Benutzer hinzuf�gen */
	public void add(User newUser) throws ContactDBException {_users.add((IEntity)newUser);}
	/**	L�scht einen User. @param user - zu l�schender User */
	public void remove(User user) throws ContactDBException {_users.remove((IEntity)user);}
	/**	F�gt einen Manager hinzu. @param newManager - neuer Manager */

	/**	Zur Gruppe geh�render Kalender. @param create - erzeugt implizit einen Kalender, wenn noch nicht vorhanden */
	public Calendar getCalendar(boolean create) throws ContactDBException {
		if (_calendar == null) {
			Method method = new Method("getSchedules");
			method.add("groupid", new Integer(getId()));
			method.addParam("create", new Boolean(create));
			List tmp = (List) method.invoke();
			if (tmp != null && tmp.size() > 0) {
			    if(tmp.get(0) instanceof Object[]) {
			        Object[] entry = (Object[])tmp.get(0);
			        _calendar = new CalendarImpl();
			        ((IEntity)_calendar).populate(new ResponseData(entry));
			    } 
                else if(tmp.get(0) instanceof List) {
			        List entry = (List)tmp.get(0);
			        _calendar = new CalendarImpl();
			        ((IEntity)_calendar).populate(new ResponseData(entry));
			    }
			}
		}
		return _calendar;
	}

    /**	Stringrepr�sentation des Objekts */
	public String toString() {
		return getAttributeAsString(UserGroup.KEY_NAME);
	}


    public int compareTo(Object o) {
        if (o == null)
            return 1;
        return toString().compareTo(o.toString());
    }

}
