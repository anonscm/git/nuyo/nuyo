/* $Id: AppointmentCalendarRelation.java,v 1.4 2006/10/27 13:21:53 aleksej Exp $
 * 
 * Created on 21.04.2004
 */
package org.evolvis.nuyo.db;

import java.util.Collection;

/**
 * Diese Schnittstelle beschreibt die Attribute einer Termin-Kalender-Relation.
 * 
 * @author mikel
 */
public interface AppointmentCalendarRelation extends AppointmentRequest {

    /**	Liefert eine eindeutige Kennung zu diesem Objekt */
    public int getId();
   
    //
    // Attribute
    //
    /** das zugeh�rige Kalendarium */
    public Calendar getCalendar() throws ContactDBException;

    /** die Anzahl der Terminbeteiligten */
    public int getParticipantCount() throws ContactDBException;

    /** der Anzeigemodus, vgl. Konstanten <code>DISPLAY_*</code> */
    public int getDisplayMode() throws ContactDBException;
    public void setDisplayMode(int newDisplayMode) throws ContactDBException;
    
    // TODO: eventuell andere Reminderverwaltung
    /** r/o-Sammlung von Terminerinnerungen */
    public Collection getReminders() throws ContactDBException;
    /** f�gt dem Termin im Kalender eine neue Erinnerung hinzu */
    public AppointmentReminder createReminder() throws ContactDBException;
    /** entfernt eine Terminrinnerung im Kalender */
    public void remove(AppointmentReminder reminder) throws ContactDBException;

    //
    // Konstanten
    //
    /** Anzeigemodi: frei/gebucht/abwesend/mit Vorbehalt */
    public final static int DISPLAY_FREE = 1;
    public final static int DISPLAY_BOOKED = 2;
    public final static int DISPLAY_AWAY = 3;
    public final static int DISPLAY_CONDITIONALLY = 4;
}
