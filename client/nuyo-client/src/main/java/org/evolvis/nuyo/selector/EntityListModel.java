package org.evolvis.nuyo.selector;

import java.util.logging.Logger;

import de.tarent.commons.datahandling.binding.CompoundModel;
import de.tarent.commons.datahandling.binding.DataChangedEvent;
import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityList;

/**
 * The EntityListModel extends the CompoundModel to add problem specific functionality. 
 * In this case, the Model needs to keep a current Entity and an EntityList that can be accessed via the Model interface (set/getAttribute with a hierarchically structured key). 
 * The current Entity actually is one of the Entities stored in EntityList and can be used like a cursor. 
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class EntityListModel extends CompoundModel {

	public static final String CURRENT_ENTITY="current";
	public static final String ENTITY_LIST = "entitylist";
	private static Logger logger = Logger.getLogger(EntityListModel.class.getName());

	/**
	 * sets the current Entity to the next Entity in EntityList if there is at least one Entity with an index higher than that of the current.
	 *
	 */
	public void nextEntity(){
		EntityList list=getEntityList();
		int index = list.indexOf(getCurrentEntity());
		if (index+1<list.getSize()){
			setCurrentEntity((Entity)list.getEntityAt(index+1));
		}
	}
	/**
	 * sets the current Entity to the previous Entity in EntityList if the current Entity has an index greater than zero
	 *
	 */
	public void previousEntity(){
		EntityList list=getEntityList();
		int index = list.indexOf(getCurrentEntity());
		if (index>0){
			setCurrentEntity((Entity)list.getEntityAt(index-1));
		}
	}

	public Entity getCurrentEntity(){
		return (Entity)getObject(CURRENT_ENTITY);
	}

	public void setCurrentEntity(Entity entity){
		registerObject(CURRENT_ENTITY, entity);
		fireDataChanged(new DataChangedEvent(this, CURRENT_ENTITY));
	}
	public void setEntityList(EntityList list){
		registerEntityList(ENTITY_LIST, list);
		fireDataChanged(new DataChangedEvent(this, ENTITY_LIST));
	}
	public EntityList getEntityList(){
		return (EntityList)getEntityList(ENTITY_LIST);
	}
}


