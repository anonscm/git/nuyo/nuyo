

package org.evolvis.nuyo.logging;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.util.JConsole;

/**
 * Small service wrapper for BeanShell based scripting support.
 * <p>
 * This class initializes a local beanshell console and makes it automatically
 * visible when the system property "bss.enable" is set.
 * </p>
 * <p>
 * Instances of this class should be retrieved through the
 * {@link org.evolvis.nuyo.controller.ApplicationServices} class.
 * </p>
 * <p>
 * Quick introduction to BeanShell:
 * <ul>
 * <li>type Java commands as you are used to</li>
 * <li>leave type information away (e.g. do <code>f = new JFrame();</code></li>
 * <li>there is a built-in <code>print</code> command.</li>
 * <li>the interpreter itself can be reached through <code>this</code></li>
 * <li>the interpreter has the array fields <code>methods</code> and
 * <code>variables</code></li>
 * <li><code>print</code> is very good at printing arrays (!)</li>
 * <li>scripted methods can be created (e.g.
 * <code>hw(someone) { print("Hello " + someone); };</code>);
 * <li>interfaces can be implemented (e.g.
 * <code> al = new ActionListener() { actionPerformed(ae) { ... } };</code>
 * <li>do interface definition only like shown above (anonymous interfaces are
 * not possible)</li>
 * <li><code>server(4444)</code> opens the lovely remote console on port 4444</li>
 * <li><code>import</code> command seems to be buggy, use "*" instead of
 * fully-classified names</li>
 * <li>have fun</li>
 * </ul>
 * @author Robert Schuster
 */
public class BeanShellService
{
  Interpreter interpreter;

  JFrame frame;

  /**
   * Sets the console up and makes it visible if the system property
   * "bss.enable" is set.
   */
  public BeanShellService()
  {
    JConsole c = new JConsole();

    interpreter = new Interpreter(c);
    new Thread(interpreter).start();

    frame = new JFrame("BeanShell debugging service");
    frame.getContentPane().add(c);
    frame.setSize(320, 240);

    frame.setLocationRelativeTo(null);


    // Makes the BeanShell window visible but does
    // this on the Swing thread to prevent a deadlock
    //
    // Note: This deadlock did not occur always only sometimes.
    if (System.getProperty("bss.enable", null) != null)
    {
      SwingUtilities.invokeLater(new Runnable()
      {
        public void run()
        {
            init();
        }
      });
      
      frame.setVisible(true);
    }

  }

  /**
   * Predefines the interpreter environment with some variables that may be
   * useful for debugging.
   * <p>
   * Adjust this method when the need for more variables or initialization
   * arises.
   * </p>
   */
  private void init()
  {
    eval("setAccessibility(true)");
    set("as", ApplicationServices.getInstance());
    set("bss", this);
  }

  /**
   * Evaluates the given beanshell command in the interpreter.
   * <p>
   * An <code>IllegalStateException</code> is thrown in case the command
   * caused trouble.
   * </p>
   * @param command
   */
  public void eval(String command)
  {
    try
      {
        interpreter.eval(command);
      }
    catch (EvalError e)
      {
        throw new IllegalStateException(e);
      }
  }

  /**
   * Makes the given object available in the interpreter environment under the
   * provided name.
   * <p>
   * An <code>IllegalStateException</code> is thrown in case the command
   * caused trouble.
   * </p>
   * @param command
   */
  public void set(String name, Object o)
  {
    try
      {
        interpreter.set(name, o);
      }
    catch (EvalError e)
      {
        throw new IllegalStateException(e);
      }

  }

  /**
   * Retrieves the value of a scripted variable.
   * <p>
   * An <code>IllegalStateException</code> is thrown in case the command
   * caused trouble.
   * </p>
   * @param name
   * @return
   */
  public Object get(String name)
  {
    try
      {
        return interpreter.get(name);
      }
    catch (EvalError e)
      {
        throw new IllegalStateException(e);
      }

  }

  /**
   * Adjusts the visibility of the window containing the beanshell console.
   * @param b
   */
  public void setVisible(boolean b)
  {
    frame.setVisible(b);
  }

}
