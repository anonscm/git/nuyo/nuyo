package org.evolvis.nuyo.gui.search;

import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 * Is an interface used by a {@link org.evolvis.nuyo.gui.search.FilterModel} instance
 * in order to visualize its changes.<p>
 * 
 * @see org.evolvis.nuyo.gui.search.FilterModel
 * @see org.evolvis.nuyo.gui.search.SearchFilterPanel 
 *  
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
interface FilterModelListener {
    
    /**
     * Forces to notify a model about the parameter changes.<p> 
     * Will be invoked by the model in order to synchronize visible parameter values.<p>
     */
    public void pickParamsPerformed();

    /**
     * Clears the panel and loads all current conditions from the model.<p>
     */
    public void fullUpdatePerformed();
    
    /**
     * Appends a new row, that represents a given condition.<p>
     * 
     * @param newCondition to append
     */
    public void appended(SearchCondition newCondition);
    
    /**
     * Inserts a new row for a given condition.<p>
     * 
     * @param atPosition - order position of a given condition 
     * @param newCondition to insert 
     */
    public void inserted( int atPosition, SearchCondition condition );

    /**
     * Removes a row, which is assigned to a given condition.<p>
     * 
     * @param atPosition - order position of a given condition 
     * @param condition was deleted 
     */
    public void removed( int atPosition, SearchCondition conditino );

    /**
     * Sets '-' button visible/unvisible if a given condition is set removable/unremovable.
     * It will be invoked by a model if required, i.e. to keep the last condition remaining 
     * or make it removable if more conditions present.<p>
     */
    public void setRemovablePerformed( SearchCondition condition );

    /** 
     * Sets the attribute of a given condition selected in a {@link JComboBox}, 
     * which is assigned to this condition. It will be invoked by a model, 
     * after a newly attribute change/selection has been validated.<p>
     */
    public void attributeChanged( SearchCondition condition );

    /**
     * Sets a given set (represented by an array) of operators to a {@link JComboBox}, 
     * which is assigned to a given condition. It will be invoked by a model 
     * if the newly selected property (or attribute) requires another operators set.<p>
     */
    public void operatorsChanged( LabeledFilterOperator[] operators, SearchCondition condition );

    /** 
     * Sets the operator of a given condition selected in a {@link JComboBox}, which is assigned to this condition.<p>
     * It will be invoked by a model after the new selection has been handled.<p>
     */
    public void operatorChanged( SearchCondition condition );
    
    /**
     * Sets the parameter text of a given condition in {@link JTextField}, which is assigned to this condition.<p>
     * It can be invoked by a model in two cases:<p>
     * a) if changes occured to model programmatically through the public API or<p>
     * b) if the user presses '+' Button:<p>
     *  the changes will be delegated the model,<p>
     *  the model anylizes this changes and then<p> 
     *  delegates validated changes back to the same {@link JTextField}.<p>
     * <p> 
     * That is the model and GUI data synchronisation occurs efficiently (on demand only).
     */
    public void paramChanged( SearchCondition condition );

}
