/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

/**
 * @author niko
 *
 */
public class HolidayDescription extends DayDescription
{
  public HolidayDescription()
  {
    super(DayDescription.HOLYDAY);
  }
}
