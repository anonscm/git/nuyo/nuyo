package org.evolvis.nuyo.db.octopus;

import java.util.Collection;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Resource;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;



/**
 * @author kleinw
 *
 *	Implementierung von de.tarent.contact.db.Resource.
 *
 */
public class ResourceImpl extends ResourceBean implements Resource {
    
    //
    //	Soapanbindung
    //
    /** Methode zum Beziehen von Resourcen */
    private static final String METHOD_GET_RESOURCES = "getResources";
    private static final String PARAM_AUTOPUBLISHING = "autopublishing";
    private static final String METHOD_CREATE_RESOURCE = "createResource";
    

    //	Statische Initialisierung des Datenholers */
    static {
        _fetcher = new AbstractEntityFetcher(METHOD_GET_RESOURCES, false) {
                public IEntity populate(ResponseData values) throws ContactDBException {
                    Resource resource = new ResourceImpl();
                    ((IEntity)resource).populate(values);
                    return (IEntity)resource;
                }
            };
    }
    
    
    /**	leerer Konstruktor */
    protected ResourceImpl() {}
    
    
    /**	Ist das Objekt vollst�ndig? */
    public void validate() throws ContactDBException {}
    
    
    /**	Speicherung des Objekts */
    public void commit() throws ContactDBException {
        prepareCommit(METHOD_CREATE_RESOURCE);
        _method.add(PARAM_AUTOPUBLISHING, _isAutoPublishing);
        _id = (Integer)_method.invoke();
    }

    
    /**	L�schen des Objekts */
    public void delete() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED, "Diese Funktion ist noch nicht implementiert.");
    }

    
    /**	Wiederherstellung des Objekts */
    public void rollback() throws ContactDBException {populate(_responseData);}
    
    
    /**	F�llen des Objekts anhand von DB-Daten */
    public void populate(ResponseData data) throws ContactDBException {
        _responseData = data;
        _id = data.getInteger(0);
        setAttribute(Resource.KEY_NAME, data.getString(1));
        setAttribute(Resource.KEY_DESCRIPTION, data.getString(2));
        _isAutoPublishing = data.getBoolean(3);
        _created = data.getDate(4);
        _changed = data.getDate(5);
    }

    
    /**	Hier kann gem�� Filter eine Liste von Resource-Instanzen geholt werden */
    static public Collection getResources(ISelection filter) throws ContactDBException {return _fetcher.getEntities(filter);}
    
}
