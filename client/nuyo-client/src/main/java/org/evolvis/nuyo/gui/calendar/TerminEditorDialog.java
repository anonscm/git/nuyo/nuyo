package org.evolvis.nuyo.gui.calendar;
/*
 * Created on 21.07.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 
package de.tarent.contact.gui.calendar;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;



*//**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 *//*
public class TerminEditorDialog extends JDialog implements DialogControl
{
  private TerminEditor m_oTerminEditor;
  private JDialog m_oDialog;
  
  public TerminEditorDialog(ScheduleData oScheduleData, ScheduleEntry entry)
  {
    this.setTitle("TerminEditor");
    m_oDialog = this;
    
    m_oTerminEditor = new TerminEditor(oScheduleData, entry);    
    m_oTerminEditor.setDialogControl(this);
    this.getContentPane().add(m_oTerminEditor);

    this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    this.addWindowListener(new WindowAdapter() 
    {
      public void windowClosing(WindowEvent e) 
      {
        m_oTerminEditor.doCancel();        
      }
    });

    this.pack();
    this.setSize(600, 600);
    this.setLocationRelativeTo(entry);
    this.setVisible(true);
    m_oTerminEditor.initGUI();
  }

  public void closeDialog()
  {
    this.setVisible(false);
  }
}
*/