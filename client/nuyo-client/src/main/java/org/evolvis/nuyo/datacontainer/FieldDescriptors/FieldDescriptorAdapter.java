/*
 * Created on 08.10.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package org.evolvis.nuyo.datacontainer.FieldDescriptors;

import java.util.List;

import org.evolvis.nuyo.datacontainer.DataContainer.DataContainer;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEvent;
import org.evolvis.nuyo.datacontainer.Events.TarentGUIEventListener;



/**
 * @author niko
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class FieldDescriptorAdapter implements FieldDescriptor
{
  protected DataContainer m_oDataContainer;

  protected String m_sName;
  protected String m_sDescription;
  protected String m_sShortDescription;
  protected char m_cKey;

  public FieldDescriptorAdapter()
  {
    m_oDataContainer = null;        
  }

  public abstract void init();
  public abstract void dispose();
  public abstract String getCurrentStatusText();
  public abstract List getEventsConsumable();
  public abstract List getEventsFireable();

  public void setName(String name)
  {
    m_sName = name;
  }

  public String getName()
  {
    return m_sName;
  }

  public void setDescription(String description)
  {
    m_sDescription = description;
  }

  public String getDescription()
  {
    return m_sDescription;
  }

  public void setShortDescription(String text)
  {
    m_sShortDescription = text;
  }

  public String getShortDescription()
  {
    return m_sShortDescription;
  }

  public void setKey(char key)
  {
    m_cKey = key;
  }

  public char getKey()
  {
    return m_cKey;
  }

  public void addTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().addTarentGUIEventListener(event, handler);
  }
    
  public void removeTarentGUIEventListener(String event, TarentGUIEventListener handler)
  {
    getDataContainer().removeTarentGUIEventListener(event, handler);
  }

  public void fireTarentGUIEvent(TarentGUIEvent e)
  {
    getDataContainer().fireTarentGUIEvent(e);
  }

  public void setDataContainer(DataContainer dc)
  {
    m_oDataContainer = dc;
  }

  public DataContainer getDataContainer()
  {
    return m_oDataContainer;
  }

}
