/*
 * Created on 15.09.2004
 *
 */
package org.evolvis.nuyo.logging;

/**
 * @author niko
 * 
 */
public interface TarentLogListener {

	public void removedFirstLogEntries(int numentriesremoved);


	public void addedLogEntry(LogEntry entry);
}
