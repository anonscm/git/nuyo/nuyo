package org.evolvis.nuyo.selector;

import java.awt.Cursor;
import java.util.logging.Logger;

import javax.swing.JFrame;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.gui.BaseFrame;
import org.evolvis.nuyo.plugin.PluginRegistry;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.swing.MenuBar;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.FormLayout;


/**
 * This class wraps the class Selector and and adds additional frame and standalone-funcitonality.
 * That is for example a MenuBar and user interaction (like user-question dialogs or error publishing).
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class SelectorFrame extends BaseFrame{
	
	private static final Logger logger = Logger.getLogger(SelectorFrame.class.getName());
	public static final String SELECTOR_MENU_BAR_ID = "selector.menu";
	
	protected static String ACTIONS_CONFIGURATION_FILE_NAME = "selector.men";
	protected static String PLUGINS_CONFIGURATION_FILE_NAME = "selector.plu";
	
	private Selector selector;
		
	public SelectorFrame(){
		
		super();
		
		logger.finest("registriere BaseFrame Services");
		ApplicationServices.getInstance().setCommonDialogServices(this);
//		from now on the BaseFrame services (like error publishing) are available
		
		logger.finer("instantiiere den Selector");
		selector = new Selector();
		
		// MenuBar and Plugin configuration
		MenuBar menuBar = new SelectorMenuBar(SELECTOR_MENU_BAR_ID);
		ActionRegistry.getInstance().addContainer(menuBar);

                Appearance ape = ConfigManager.getAppearance();
		PluginRegistry.getInstance().init(ape.getPluginDefinitions());
                ActionRegistry.getInstance().init(ape.getActionDefinitions());
		
		getFrame().setTitle("tarent-selector");
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().setJMenuBar(menuBar);
		
		FormLayout layout = new FormLayout(
				"pref:GROW", //columns 
		"fill:pref:GROW");// rows
		
		//create and configure a builder
		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();		
		builder.add(selector.getComponent());
		
		getFrame().getContentPane().add(builder.getPanel());
		getFrame().getRootPane().setDefaultButton(selector.getDefaultButton());
		getFrame().pack();
		
	}
	
	public void setWaiting(boolean setWaiting){
		if (setWaiting){
			Cursor cursor = new Cursor(Cursor.WAIT_CURSOR);
			this.getFrame().getContentPane().setCursor(cursor);
		} else {
			Cursor cursor = Cursor.getDefaultCursor();
			this.getFrame().getContentPane().setCursor(cursor);
		}
	}
	
}
