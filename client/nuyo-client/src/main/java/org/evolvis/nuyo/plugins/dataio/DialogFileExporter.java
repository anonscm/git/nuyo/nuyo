/* $Id: DialogFileExporter.java,v 1.7 2006/07/28 12:13:04 aleksej Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Jens Neumaier. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.plugins.dataio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.plugin.AddressExporter;
import org.evolvis.nuyo.plugin.DialogAddressExporter;
import org.evolvis.nuyo.plugin.PluginRegistry;

import de.tarent.commons.plugin.Plugin;

/**
 * This DialogFileExporter is part of the <code>ExportFilePlugin</code>.
 * All plugins supplying an implementation complying with
 * the <code>AddressExporter<code> interface will be detected
 * and controlled by this component. After collecting desired export information
 * from all capable plugins a FileChooser will be opened. An additional configuration
 * dialog for the selected export type will be shown if supplied by the exporter.
 * 
 * @author Jens Neumaier, tarent GmbH
 * @see org.evolvis.nuyo.plugins.dataio.ExportFilePlugin
 * @see org.evolvis.nuyo.plugin.AddressExporter
 * 
 */
public class DialogFileExporter implements DialogAddressExporter {
    
    private static Logger logger = Logger.getLogger(DialogFileExporter.class.getName());
    
    private Addresses addresses;
    
    /** Last chosen export File to remember current path */
    private File lastChosenFile = null;
    /** Last chosen export FileFilter */
    private FileFilter lastChosenFileFilter = null;    
    
    public DialogFileExporter() {
        // Exporter plugins cannot be detected in the constructor
        // because the may not be constructed yet
    }

    public void setAddresses(Addresses addresses) {
        this.addresses = addresses;        
    }

    public Addresses getAddresses() {
        return addresses;
    }

    public void execute() {
        /*
         * Preparing export
         * - check if addresses have been selected
         * - get exporter plugins
         * - prepare filechooser
         * 
         */
        
        /** Mapping from FileFilter descriptions to responsible AddressExporter-Plugin */
        Map filterDescriptionToExporter = new HashMap(8);
        /** List of plugins supporting adress export */
        List exporterPlugins = PluginRegistry.getInstance().getSupportedPlugins(AddressExporter.class);
        
        // Checking if addresses have been selected
        if (addresses.getSize() == 0) {
            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString(this, "Error_no_addresses_selected"));
            return;
        }
        
        // Preparing filechooser
        JFileChooser fileChooser = new JFileChooser();
        // Do not use "All Files"-Filter
        fileChooser.setAcceptAllFileFilterUsed(false);
        
        Iterator iter = exporterPlugins.iterator();
        while (iter.hasNext()) {
            Plugin plugin = (Plugin) iter.next();
            AddressExporter exporter = (AddressExporter) plugin.getImplementationFor(AddressExporter.class);
            // Getting FileFilter for each supported file export type
            FileFilter[] fileTypes = exporter.getSupportedFileTypes();
            
            for (int i = 0; i < fileTypes.length; i++) {
                FileFilter filter = fileTypes[i];
                // Adding available FileFilters to the filechooser
                fileChooser.addChoosableFileFilter(filter);
                // Mapping from FileFilter descriptions to responsible AddressExporter-Plugin
                filterDescriptionToExporter.put(filter.getDescription(), exporter);                
            }
        }            
        
        // Remembering actual path and last chosen FileFilter from last use
        if (lastChosenFile != null) {
            fileChooser.setCurrentDirectory(lastChosenFile);
            fileChooser.setFileFilter(lastChosenFileFilter);
        }               
        
        /*
         * Selecting file
         * - show filechoser dialog
         * - check if selected file is correct
         * 
         */
        
        /** current main frame to place save dialog */ 
        JFrame mainFrame = ApplicationServices.getInstance().getCommonDialogServices().getFrame();        
        /** current file chosen in JFileChooser */
        File chosenFile = null;
        /** currect FileFilter */
        DefaultFileFilter chosenFileFilter = null;
        
        boolean isFileSelectionCorrect = false;
        // Asking for file selection while selected file is writable and
        // overwritting is confirmed if file exists.
        // User may abort by pressing the cancel button.
        while (!isFileSelectionCorrect) {
            int fileChooserReturn = fileChooser.showSaveDialog(mainFrame);
            
            // Cancel this loop if user aborts file selection dialog or an ERROR_OPTION has been returned
            if (fileChooserReturn != JFileChooser.APPROVE_OPTION)
                break;
            
            // Get the chosen FileFilter as DefaultFileFilter
            chosenFileFilter = (DefaultFileFilter) fileChooser.getFileFilter();
            
            // Checking if the user specified an explicit extension by selecting
            // a file or giving a full name including extension.
            // This is relevant if a file type is supported with more than one
            // possible extension. If no extension is given or the full name
            // is not accepted by the FileFilter the deault extension will be appended.
            chosenFile = fileChooser.getSelectedFile();
            StringBuffer absoluteFilePath = new StringBuffer(chosenFile.getAbsolutePath());
            if (!chosenFileFilter.accept(chosenFile)) {
                absoluteFilePath.append('.').append(chosenFileFilter.getDefaultExtension());
                chosenFile = new File(absoluteFilePath.toString());
            }
            
            // Checking if the file already exists
            if (chosenFile.exists()) {
                // Asking the user to confirm file overwrite
                String overwriteCaption = Messages.getString(this, "Ask_overwrite_existing_file_caption");
                String overwriteQuestion = Messages.getFormattedString(this, "Ask_overwrite_existing_file", chosenFile.getName());
                String[] answers = { Messages.getString(this, "Yes"), Messages.getString(this, "No") };
                
                int questionReturn = ApplicationServices.getInstance().getCommonDialogServices().askUser(overwriteCaption, overwriteQuestion, answers, 0);
                
                // If the file should not be overwritten the file choosing process will start again
                if (questionReturn == 1) {
                    continue;
                }
            }
            
            // Checking if existing file is writable
            if (chosenFile.exists() && !chosenFile.canWrite())
                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getFormattedString(this, "Error_file_not_writable", chosenFile.getName()));
            else
                isFileSelectionCorrect = true;
        }
        
        // Keeping actual path and FileFilter for the next export dialog
        if (chosenFile != null)
            lastChosenFile = chosenFile;
        if (chosenFileFilter != null)
            lastChosenFileFilter = chosenFileFilter;
        
        /*
         * Exporting to File
         * - get responsible exporter plugin
         * - showing optional export configuration dialog
         * - open export stream on selected file
         * - invoke export on responsible exporter plugin
         *  
         */
        
        if (isFileSelectionCorrect) {
            /** Responsible AddressExporter-Plugin */
            AddressExporter responsibleExporter = (AddressExporter) filterDescriptionToExporter.get(chosenFileFilter.getDescription());
            
            // Setting addresses
            responsibleExporter.setAddresses(addresses);
            
            // Showing (optional) configuration dialog
            JDialog configurationDialog = responsibleExporter.getConfigurationDialog(chosenFileFilter);
            if (configurationDialog != null) {
                configurationDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                configurationDialog.pack();
                configurationDialog.setLocationRelativeTo(mainFrame);
                configurationDialog.setModal(true);
            }
            
            OutputStream out = null;
            // Opening an OutputStream on the selected file
            try {
                out = new FileOutputStream(chosenFile);
            } catch (FileNotFoundException e) {
                logger.log(Level.SEVERE, Messages.getFormattedString(this, "Error_file_not_writable", chosenFile.getName()), e);
                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getFormattedString(this, "Error_file_not_writable", chosenFile.getName()));
            }
            
            // Trying to export chosen FileFilter to OutputStream
            try {
                // Exporting to OutputStream passing original FileFilter description for export type identification
                responsibleExporter.export(out, chosenFileFilter.getRawDescription());
            } catch (IOException e) {
                logger.log(Level.SEVERE, Messages.getString(this, "Error_writing_FileOutputStream"), e); //$NON-NLS-1$
                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString(this, "Error_writing_FileOutputStream")); //$NON-NLS-1$
                // Trying to delete created/overwritten file 
                if (chosenFile.delete())
                    logger.log(Level.INFO, Messages.getFormattedString(this, "Log_deleting_file_after_failure", chosenFile.getAbsolutePath()));
                else
                    logger.log(Level.INFO, Messages.getFormattedString(this, "Log_error_deleting_file_after_failure", chosenFile.getAbsolutePath()));
            } catch (ExporterException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                // Publishing error for case: Empty result from server
                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString(this, "Error_calling_octopus_task"));
                // Trying to delete created/overwritten file 
                if (chosenFile.delete())
                    logger.log(Level.INFO, Messages.getFormattedString(this, "Log_deleting_file_after_failure", chosenFile.getAbsolutePath()));
                else
                    logger.log(Level.INFO, Messages.getFormattedString(this, "Log_error_deleting_file_after_failure", chosenFile.getAbsolutePath()));
            }
        }
        
    }

}
