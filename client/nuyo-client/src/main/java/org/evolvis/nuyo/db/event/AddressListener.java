/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Michael Klink. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.db.event;

import java.sql.SQLException;

import org.evolvis.nuyo.db.Address;


/**
 * Diese Schnittstelle muss von Klassen implementiert werden, die von den
 * Address-Objekten �ber Ereignisse benachrichtigt werden wollen.
 * 
 * @author mikel
 */
public interface AddressListener {
    public void creating(Address address) throws SQLException;
    public void created(Address address);
    public void loading(Address address, int mask) throws SQLException;
    public void loaded(Address address, int mask);
    public void changing(Address address, String field, Object oldValue, Object newValue) throws SQLException;
    public void changed(Address address, String field, Object oldValue, Object newValue);
    public void committing(Address address) throws SQLException;
    public void committed(Address address);
    public void rollingBack(Address address) throws SQLException;
    public void rolledBack(Address address);
    public void deleting(Address address) throws SQLException;
    public void deleted(Address address);
}
