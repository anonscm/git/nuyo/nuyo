package org.evolvis.nuyo.db.octopus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.Categorys;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.groupware.category.CategoryList;
import org.evolvis.nuyo.remote.Method;

import de.tarent.commons.datahandling.FilterNode;
import de.tarent.commons.datahandling.ListFilter;
import de.tarent.commons.datahandling.ListFilterOperator;
import de.tarent.octopus.client.OctopusResult;

public class CategorysImpl extends Categorys {

    private static Logger logger = Logger.getLogger(CategorysImpl.class.getName());

    public final static String CATEGORY_METHOD = "getCategories";

    public final static String KEY_COLUMN_NAMES = "columnNames";
    public final static String KEY_ENTITY_TABLE = "entityTable";
    

    public CategorysImpl(Database db) {
        super(db);
    }

    public void loadCategorys() throws ContactDBException {
    	resetCategoryList();
    	List filterList = new ArrayList();
		
		// all categories have to be readable and grantable
		//new FilterNode(Category.PROPERTY_RIGHTREAD.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToList(filterList);
		//new FilterNode(Category.PROPERTY_RIGHTGRANT.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToListAND(filterList);
		
        Method method = new Method(CATEGORY_METHOD);
        method.add(AddressWorkerConstants.PARAM_CATEGORIES_FILTER+"."+ListFilter.PARAM_FILTER_LIST, filterList);
		method.invoke();
		
        OctopusResult result = method.getORes();
        CategoryList loadedcategories = (CategoryList) result.getData("categories");
               
        if (loadedcategories == null)
            throw new ContactDBException(ContactDBException.EX_MISSING_RESULT_DATA, 
                                         "Es konnten keine Kategorien geladen werden.");
        
        for (Iterator iter = loadedcategories.iterator(); iter.hasNext();){
        		add((Category) iter.next());
        }
           
    }
    
    public List getAllGrantableCategorys() throws ContactDBException {
    
    		List filterList = new ArrayList();
		
		// all categories have to be readable and grantable
		new FilterNode(Category.PROPERTY_RIGHTGRANT.getKey(), ListFilterOperator.EQ, new Integer(1), false).appendToList(filterList);
		
        Method method = new Method(CATEGORY_METHOD);
        method.add(AddressWorkerConstants.PARAM_CATEGORIES_FILTER+"."+ListFilter.PARAM_FILTER_LIST, filterList);
		method.invoke();
		
        OctopusResult result = method.getORes();
        return (CategoryList) result.getData("categories");
    }

	public Category getVirtualRootCategory() throws ContactDBException {
		for (Iterator iter = categorys.iterator(); iter.hasNext();){
			Category current = (Category) iter.next();
			if (current.getType() == Category.TYPE_ALL_ADDRESSES)
				return current;
		}
		
		return null;
	}   
	
	public Category getTrashCategory() throws ContactDBException{
		for (Iterator iter = categorys.iterator(); iter.hasNext();){
			Category current = (Category) iter.next();
			if (current.getType() == Category.TYPE_TRASH)
				return current;
		}
		return null;
	}
}
