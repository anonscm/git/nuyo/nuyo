package org.evolvis.nuyo.gui.action;

import org.evolvis.nuyo.db.Addresses;
import org.evolvis.xana.action.DataProvider;

/**
 * This interface should be implemented by classes that act as a 
 * Addressdata Provider for Action- or Plugin-classes.
 * @author Steffi Tinder, tarent GmbH
 *
 */
public interface AddressListProvider extends DataProvider {
	
		public Addresses getAddresses();

}
