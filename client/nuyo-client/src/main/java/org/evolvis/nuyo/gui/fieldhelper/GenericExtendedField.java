/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fieldhelper;



/**
 * @author niko
 *
 */
public class GenericExtendedField extends GenericTextField
{
  public GenericExtendedField(String guikey, String dbkey, String tooltipkey, String labelkey, int size)
  {
    super(guikey, dbkey, CONTEXT_ADRESS, tooltipkey, labelkey, size);
  }
}
