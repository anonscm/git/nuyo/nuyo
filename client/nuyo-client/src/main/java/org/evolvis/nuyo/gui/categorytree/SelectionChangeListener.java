/**
 * 
 */
package org.evolvis.nuyo.gui.categorytree;

interface SelectionChangeListener
{
  void selectionChanged(Selection sel);
}