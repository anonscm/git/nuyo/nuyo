/* $Id: HistoryElement.java,v 1.3 2006/08/28 14:27:49 aleksej Exp $
 * 
 * Created on 31.03.2004
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.Date;

/**
 * Diese Schnittstelle wird von allen Objekten implementiert, die in einer
 * Kontakthistorie auftreten k�nnen.
 * 
 * @author mikel
 */
public interface HistoryElement {
    //
    // Attribute
    //
    /**
     * Diese Methode liefert den UTC-Zeitpunkt, an dem dieses HistoryElement
     * einzutragen ist.
     * 
     * @return UTC-History-Zeitpunkt.
     */
    public Date getHistoryDate();
    
    //
    // Konstanten f�r HistoryElemente
    //
    /** Typ: Termin */
    public final static String TYPE_APPOINTMENT = "appointment";
    /** Typ: Versandauftrag */
    public final static String TYPE_MAILBATCH = "mailbatch";
    /** Typ: Kontaktereignisse */
    public final static String TYPE_CONTACT = "contact";
}
