/**
 * 
 */
package org.evolvis.nuyo.plugins.office;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.plugin.DialogAddressListPerformer;

import de.tarent.commons.plugin.Plugin;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeExporterPlugin implements Plugin
{
	private OfficeExporter officeExp = null;
	public static final String ID = "office_exporter";
	
	/**
	 * 
	 */
	public OfficeExporterPlugin()
	{
		
	}

	public String getID()
	{
		return ID;
	}

	public void init()
	{
		officeExp = new OfficeExporter();
	}

	public Object getImplementationFor(Class type)
	{
		if(type.equals(DialogAddressListPerformer.class))
		{
			if(officeExp == null) init();
			return officeExp;
		}
		return null;
	}

	public List getSupportedTypes()
	{
		// Java 1.5 List<Class> types = new ArrayList<Class>();
		List types = new ArrayList();
		types.add(DialogAddressListPerformer.class);
		return types;
	}

	public boolean isTypeSupported(Class type)
	{
		return type.equals(DialogAddressListPerformer.class);
	}

	public String getDisplayName() {
		return "Office Export";
	}

}