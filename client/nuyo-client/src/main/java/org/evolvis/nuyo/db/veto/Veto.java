/* $Id: Veto.java,v 1.2 2006/06/06 14:12:12 nils Exp $
 * 
 * Created on 29.10.2003
 */
package org.evolvis.nuyo.db.veto;

import java.util.Collections;
import java.util.List;

/**
 * Diese Klasse stellt ein partielles oder vollst�ndiges Veto einer
 * {@link org.evolvis.nuyo.db.veto.VetoableAction} dar.
 * 
 * @author mikel
 */
public class Veto {
    /*
     * �ffentliche Konstruktoren 
     */
    /**
     * Dieser Konstruktor erzeugt ein kategorisches Veto. 
     * 
     * @param message Kurztext
     * @param detail Langtext
     * @param forbidden Flag, ob es ein Verbot oder ein Warnung ist.
     */
    public Veto(String message, String detail, boolean forbidden) {
        super();
        this.forbidden = forbidden;
        this.restricted = forbidden;
        this.message = message;
        this.detail = detail;
        this.restrictions = Collections.EMPTY_LIST;
    }

    /**
     * Dieser Konstruktor erzeugt ein beschr�nktes Veto.
     * 
     * @param message Kurztext
     * @param detail Langtext
     * @param restrictions Liste mit Einschr�nkungen
     */
    public Veto(String message, String detail, List restrictions) {
        super();
        this.forbidden = false;
        this.restricted = true;
        this.message = message;
        this.detail = detail;
        this.restrictions = restrictions;
    }

    /*
     * Getter und Setter
     */
    /**
     * @return Langtext
     */
    public String getDetail() {
        return detail;
    }

    /**
     * @return Verboten-Flag
     */
    public boolean isForbidden() {
        return forbidden;
    }

    /**
     * @return Kurztext
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return Eingeschr�nkt-Flag
     */
    public boolean isRestricted() {
        return restricted;
    }

    /**
     * @return Liste mit Einschr�nkungen
     */
    public List getRestrictions() {
        return Collections.unmodifiableList(restrictions);
    }

    /*
     * gesch�tzte Variablen
     */
    private boolean restricted;
    private boolean forbidden;
    String message;
    String detail;
    List restrictions;
}
