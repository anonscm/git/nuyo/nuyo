/*
 * Created on 16.09.2004
 *
 */
package org.evolvis.nuyo.logging;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

/**
 * @author niko
 *
 */
public class XMLLogFormatter implements LogFormatter
{
  public String formatLog(LogEntry entry)
  {
    String text = "";
    try
    {
    
      text += "<logentry>";
    
      text += "<date>";    
      text += URLEncoder.encode(entry.getDate().toString(), "UTF-8");
      text += "</date>";
  
      text += "<level>";
      text += URLEncoder.encode(entry.getLevelText(), "UTF-8");
      text += "</level>";
  
      text += "<message>";      
      text += URLEncoder.encode(entry.getMessage(), "UTF-8");
      text += "</message>";
  
      List params = entry.getParameters();
      if (params != null)
      {
        text += "<parameters>";
        Iterator it = params.iterator();
        while(it.hasNext())
        {
          Object param = it.next();      
          text += "<value>";          
          text += URLEncoder.encode(param.toString(), "UTF-8");
          text += "</value>";
        }
        text += "</parameters>";
      }
      
      String throwabletext = entry.getThrowableText();
      if (throwabletext != null)
      {
        text += "<exception>";        
        text += URLEncoder.encode(throwabletext, "UTF-8");
        text += "</exception>";
      }
      
      text += "</logentry>";
    
    }
    catch (UnsupportedEncodingException e)
    {
      e.printStackTrace();
    }
    return text;
  }

}
