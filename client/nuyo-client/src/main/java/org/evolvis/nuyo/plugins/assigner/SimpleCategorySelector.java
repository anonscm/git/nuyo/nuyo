package org.evolvis.nuyo.plugins.assigner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.Database;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.gui.Messages;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;



class SimpleCategorySelector implements ActionListener{
    
    private static final String LABEL_ALL_SUBCATEGORIES = "<Alle Unterkategorien>";
    private static Logger logger = Logger.getLogger(SimpleSearchComponent.class.getName());
    
    private JComboBox comboCategories;
    private JComboBox comboSubcategories;
    
    private JPanel mainPanel;
    private Database database;
    private boolean showVirtualCategories = false;
    private boolean showAllSubcategoriesEntry = false;
    
    
    public SimpleCategorySelector() {
        
        database=ApplicationServices.getInstance().getCurrentDatabase();
        
        int width = 200;
        int height = 20; 
        
        comboCategories = new JComboBox();
        comboSubcategories = new JComboBox();
        
        //comboCategories.setPreferredSize(new Dimension(width,height));
        comboCategories.addActionListener(this);
        
        //comboSubcategories.setPreferredSize(new Dimension(width,height));
        comboSubcategories.addActionListener(this);
    }
    
    public void loadCategoryLists(){
        try {
            fillCategoryList();
            String categoryID = (String)((Category)comboCategories.getSelectedItem()).getIdAsString();
            fillSubCategoryList(categoryID);
        } catch (ContactDBException e) {
            logger.warning("Fehler beim laden der Kategorielisten");
            ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Laden der Kategorielisten",e);
            
        }
    }
    /*
     * fills the category-combobox with the names of the categories the user can see
     */
    private void fillCategoryList() throws ContactDBException{
        final Iterator iterator = database.getCategoriesList().iterator();
        while (iterator.hasNext()) {
            final Category category = (Category) (iterator.next());
            if (!category.getVirtual().booleanValue() || showVirtualCategories){
                comboCategories.addItem(category);
            }
        }
    }
    /*
     * fills the subcategory-combobox with the subcategories 
     * of the category indicated by categoryID
     */
    private void fillSubCategoryList(String categoryId) throws ContactDBException {
        if(categoryId == null) throw new NullPointerException("categoryId");
        if("".equals(categoryId)) throw new IllegalArgumentException("empty categoryId");
        comboSubcategories.removeAllItems();
        if (showAllSubcategoriesEntry){
            //show entry <Alle Unterkategorien>
            comboSubcategories.addItem(LABEL_ALL_SUBCATEGORIES);
        }
        Map subcategoryMap = database.getVerteiler(categoryId);
        Iterator it = subcategoryMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry entry = (Map.Entry) (it.next());
            SubCategory sub = (SubCategory)entry.getValue();
            comboSubcategories.addItem(sub);
        }
    }
    
    public List getSelectedSubcategories(){
        List list = new ArrayList();
        if (!(comboSubcategories.getSelectedIndex()==-1)){
            if (LABEL_ALL_SUBCATEGORIES.equals(comboSubcategories.getSelectedItem())) {   
                for (int i=0;i<comboSubcategories.getItemCount();i++){
                    if (! LABEL_ALL_SUBCATEGORIES.equals(comboSubcategories.getSelectedItem())) {    
                        list.add((SubCategory)comboSubcategories.getItemAt(i));
                    }
                }
            } else {
                list.add((SubCategory)comboSubcategories.getSelectedItem());
            }
        }
        return list;
    }
    
    public List getSelectedSubcategoryIds(){
        final List ids = new ArrayList();
        if (!(comboSubcategories.getSelectedIndex()==-1)){
            try {   
                if (LABEL_ALL_SUBCATEGORIES.equals(comboSubcategories.getSelectedItem())) {   
                    for (int i=0;i<comboSubcategories.getItemCount();i++){
                        if (comboSubcategories.getItemAt(i) instanceof SubCategory) {
                            ids.add(((SubCategory)comboSubcategories.getItemAt(i)).getIdAsString());
                        }
                    }
                } else {
                    ids.add(((SubCategory)comboSubcategories.getSelectedItem()).getIdAsString());
                } 
            } catch (Exception e){
                logger.warning("Fehler beim Zufriff auf die Daten einer Unterkategorie");
                ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Zufriff auf die Daten einer Unterkategorie",e);
                
            }
        }
        return ids;
    }
    
    public List getSelectedCategories(){
        List categoryList = new ArrayList();
        Category category = (Category)comboCategories.getSelectedItem();
        categoryList.add(category);
        return categoryList;
    }
    
    private JPanel buildPanel(){
        
        final FormLayout layout = new FormLayout(
                "right:pref, 3dlu, pref:GROW", //columns 
        "pref, 3dlu, pref");      // rows
        
        //create and configure a builder
        final PanelBuilder builder = new PanelBuilder(layout);
        final CellConstraints cc = new CellConstraints();
        
        builder.addLabel("Kategorie:", cc.xy(1,1));
        builder.addLabel("Unterkategorie:", cc.xy(1,3));
        
        builder.add(comboCategories, cc.xy(3,1));
        builder.add(comboSubcategories, cc.xy(3,3));
        
        return builder.getPanel();
    }
    
    public void handleSelectedCategory(){
        final Category cat = (Category)comboCategories.getSelectedItem();
        try {
            fillSubCategoryList(cat.getIdAsString());
        } catch (ContactDBException e1) {
            logger.warning(Messages.getString("SimpleSearchComponent.error_while_loading_subcategorylists")); //$NON-NLS-1$
            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("SimpleSearchComponent.error_while_loading_categorylists"),e1); //$NON-NLS-1$
        }
        
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==comboCategories){
            handleSelectedCategory();
        } else if (e.getSource()==comboSubcategories){
            //do nothing here
            logger.fine("selection of subcategories ignored");
        }
    }
    
    public void showVirtualCategories(boolean show){
        showVirtualCategories=show;
    }
    
    public void showAllSubcategoriesEntry(boolean show){
        showAllSubcategoriesEntry = show;
    }
    
    public JPanel getComponent(){
        if (mainPanel==null) mainPanel=buildPanel();
        return mainPanel;
    }
    
    public JComboBox getComboCategories() {
        return comboCategories;
    }
    
    public void setComboCategories(JComboBox comboCategories) {
        this.comboCategories = comboCategories;
    }
    
    public JComboBox getComboSubcategories() {
        return comboSubcategories;
    }
    
    public void setComboSubcategories(JComboBox comboSubcategories) {
        this.comboSubcategories = comboSubcategories;
    }
    
    
    
}
