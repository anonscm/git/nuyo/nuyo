package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressListParameter;
import org.evolvis.nuyo.gui.GUIListener;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.dialogs.DoubletSearchDialog;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

import de.tarent.commons.utils.TaskManager;

public class SaveContactAction extends AbstractGUIAction {

    private static final long   serialVersionUID    = 8122860390409416450L;
    private static final TarentLogger log = new TarentLogger(SaveContactAction.class);
    private GUIListener guiListener;
    private TaskManager.Task saveExecutor; 

    public void actionPerformed(ActionEvent e) {
        if(guiListener != null) {
            TaskManager.getInstance().registerExclusive(saveExecutor, (String)getValue( NAME ), false);
        } else log.warning("Save action not initialized");
    }
    
    public void init(){
        guiListener = ApplicationServices.getInstance().getActionManager();
        
        saveExecutor = new TaskManager.Task(){
                public void run(TaskManager.Context cntx) {
                	// Transfers user-given data of address object and causes them to be
                	// checked and error messages displayed.
                	cntx.setActivityDescription(Messages.getString("SaveContactAction_TakingData"));
                	if (!guiListener.takeAddressDataFromGui())
                		return;
                	
                	DoubletSearchDialog dialog = guiListener.guiRequestCheckForDouble(false, true); 
                	String status = dialog == null ? DoubletSearchDialog.STATUS_SAVE : dialog.getStatus();
                	
                	// Runs the duplicate check.
                	cntx.setActivityDescription(Messages.getString("SaveContactAction_CheckingDuplicates"));
                	if (status.equals(DoubletSearchDialog.STATUS_ABORT)){
                		return;
                	}
                	else if (status.equals(DoubletSearchDialog.STATUS_JUMP_TO_DOUBLE)){
                		Address chosenDouble = dialog.getSelectedAddress();
            			
            			if (chosenDouble != null){
            				List pkList = new ArrayList();
            				pkList.add(new Integer (chosenDouble.getId()));
            				
            				guiListener.userRequestNotEditable(GUIListener.DO_DISCARD);
            				AddressListParameter alp = ApplicationServices.getInstance().getApplicationModel().getAddressListParameter();
            				alp.setPkFilter(pkList);
            		        alp.setAddressSourceLabel("");
            		        ApplicationServices.getInstance().getApplicationModel().fireAddressListParameterChanged();
            		   
            			}
                	}
                	else if (status.equals(DoubletSearchDialog.STATUS_SAVE)){
                		// Stores the values.
                		cntx.setActivityDescription(Messages.getString("SaveContactAction_SavingAddress"));
                		guiListener.userRequestNotEditable(GUIListener.DO_SAVE);
                	}
                }
                
                public void cancel() {}
            };
    }
}
