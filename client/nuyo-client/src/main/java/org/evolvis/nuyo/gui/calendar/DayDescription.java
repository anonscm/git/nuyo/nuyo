/*
 * Created on 26.02.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author niko
 *
 */
public class DayDescription
{
  private static Logger logger = Logger.getLogger(DayDescription.class.getName());

  private Object m_oType;
  private String m_sDayName;
  private String m_sShortDayName;
  private String m_sName;
  private String m_sDescription;
  
  public final static Object WORKDAY = "WORKDAY";
  public final static Object HOLYDAY = "HOLYDAY";
  public final static Object WEEKEND = "WEEKEND";
  
  public DayDescription(Object type)
  {
    m_oType = type;
    m_sDayName = "";
    m_sName = "";
    m_sDescription = "";
  }
  
  public Object getType()
  {
    return m_oType;
  }

  public void setType(Object type)
  {
    m_oType = type;
  }

  public String getDescription()
  {
    return m_sDescription;
  }

  public void setDescription(String description)
  {
    m_sDescription = description;
  }

  public String getDayName()
  {
    return m_sDayName;
  }

  public void setDayName(String name)
  {
    m_sDayName = name;
  }
  
  public String getName()
  {
    return m_sName;
  }

  public void setName(String name)
  {
    m_sName = name;
  }

  public String getShortDayName()
  {
    return m_sShortDayName;
  }

  public void setShortDayName(String name)
  {
    m_sShortDayName = name;
  }
  
  
  public void dump(String text)
  {
    logger.log(Level.WARNING, text + ": " + m_oType.toString() + "; " + getDayName() + "; " + getDescription());   
  }

}
