/**
 * 
 */
package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.evolvis.liboffice.OfficeAutomat;
import org.evolvis.liboffice.ui.swing.conf.FirstRunWizardSwing;
import org.evolvis.liboffice.ui.swing.conf.OfficeConfigurationDialog;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.plugins.office.OfficeConfigurationManager;
import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.config.ConfigManager;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class OfficeConfigurationAction extends AbstractGUIAction
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5090831420098463600L;

	public void actionPerformed(ActionEvent arg0)
	{
		OfficeConfigurationManager.getInstance().setup();
		
		// If user is allowed to change settings, show configuration-dialog
		if(ConfigManager.getEnvironment().isUserOverridable())
		{
			new OfficeConfigurationDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame(), new FirstRunWizardSwing(ApplicationServices.getInstance().getCommonDialogServices().getFrame())).setVisible(true);
		}
		// otherwise, just display message and show current configuration
		else
		{
			String currentConfigNote = null;
			if(OfficeAutomat.getOfficeConfigurator().getPreferredOffice() == null) currentConfigNote = "No office is currently configured";
			else currentConfigNote = OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getName()+" "+OfficeAutomat.getOfficeConfigurator().getPreferredOffice().getType().getDisplayVersion()+" is currently setup as your default";
			JOptionPane.showMessageDialog(ApplicationServices.getInstance().getCommonDialogServices().getFrame(), "<html>You are not allowed to change configuration-settings. Ask your administrator.<br>"+currentConfigNote+"</html>");
		}
	}
}