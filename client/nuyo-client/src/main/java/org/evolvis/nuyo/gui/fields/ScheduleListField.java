/*
 * Created on 14.04.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.evolvis.nuyo.controls.EntryLayoutHelper;
import org.evolvis.nuyo.controls.TarentWidgetComponentTable;
import org.evolvis.nuyo.controls.TarentWidgetIconComboBox;
import org.evolvis.nuyo.controls.TarentWidgetInterface;
import org.evolvis.nuyo.controls.TarentWidgetPanel;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.octopus.AddressImpl;
import org.evolvis.nuyo.gui.GUIHelper;
import org.evolvis.nuyo.gui.calendar.AppointmentComparator;
import org.evolvis.nuyo.gui.calendar.AppointmentPrintContainer;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.gui.calendar.ScheduleType;
import org.evolvis.nuyo.gui.fieldhelper.CalendarVisibleElement;
import org.evolvis.nuyo.gui.fieldhelper.ContactAddressField;
import org.evolvis.nuyo.plugin.PluginData;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Appearance.Key;
import org.evolvis.xana.swing.utils.SwingIconFactory;
import org.evolvis.xana.utils.IconFactory;

/**
 * @author niko
 *
 */
public class ScheduleListField extends ContactAddressField implements CalendarVisibleElement
{
    private ScheduleListField m_oThisScheduleListField;
    
    int m_iMaxAddressesInTooltip = 10;
    boolean m_bShowAddressesInTooltip = true;
    
    public final static Object SCHEDULEFILTER_TIME_ALL = "SCHEDULEFILTER_TIME_ALL";
    public final static Object SCHEDULEFILTER_TIME_ALL_FUTURE = "SCHEDULEFILTER_TIME_ALL_FUTURE";
    
    public final static Object SCHEDULEFILTER_TIME_TODAY = "SCHEDULEFILTER_TIME_TODAY";
    public final static Object SCHEDULEFILTER_TIME_THIS_WEEK = "SCHEDULEFILTER_TIME_THIS_WEEK";
    public final static Object SCHEDULEFILTER_TIME_THIS_MONTH = "SCHEDULEFILTER_TIME_THIS_MONTH";
    
    public final static Object SCHEDULEFILTER_TIME_WEEK = "SCHEDULEFILTER_TIME_WEEK";
    public final static Object SCHEDULEFILTER_TIME_TWO_WEEKS = "SCHEDULEFILTER_TIME_TWO_WEEKS";
    public final static Object SCHEDULEFILTER_TIME_THREE_WEEKS = "SCHEDULEFILTER_TIME_THREE_WEEKS";
    
    public final static Object SCHEDULEFILTER_TIME_SCHEDULE = "SCHEDULEFILTER_TIME_SCHEDULE";
    /*alle
    alle zuk�nftigen
    ----------------
    heute
    diese Woche (alle Termine der Woche, incl. der vergangenen Termin/Aufgaben)
    diesen Monat (alle Termine des Monats, incl. der vergangenen Termin/Aufgaben)
    ----------------
    7 Tage
    14 Tage 
    21 Tage
    ----------------
    Kalenderansicht
    */
    
    public final static Object SCHEDULEFILTER_PRIORITY_ALL = "SCHEDULEFILTER_PRIORITY_ALL";
    public final static Object SCHEDULEFILTER_PRIORITY_LOW = "SCHEDULEFILTER_PRIORITY_LOW";
    public final static Object SCHEDULEFILTER_PRIORITY_MID = "SCHEDULEFILTER_PRIORITY_MID";
    public final static Object SCHEDULEFILTER_PRIORITY_HIGH = "SCHEDULEFILTER_PRIORITY_HIGH";
    public final static Object SCHEDULEFILTER_PRIORITY_PRIVATE = "SCHEDULEFILTER_PRIORITY_PRIVATE";
    
    protected final static DateFormat myDateTimeFormatter = new SimpleDateFormat("dd.MM.yy - HH:mm");
    protected final static DateFormat myDateFormatter = new SimpleDateFormat("dd.MM.yy");
    protected final static DateFormat myHourFormatter = new SimpleDateFormat("HH:mm");
    protected final static DateFormat myWeekDayFormatter = new SimpleDateFormat("EEE");
    protected final static DateFormat myMonthTimeFormatter = new SimpleDateFormat("MMMMMMMMMMMM");
    protected final static DateFormat myKWTimeFormatter = new SimpleDateFormat("w");
    protected final static DateFormat myLongDateTimeFormatter = new SimpleDateFormat("EEE, dd.MMMM.yyyy - HH:mm");
    
    
    //private ScheduleData m_oScheduleData;
    
    private ScheduleDate m_oFirstDay = null;
    private ScheduleDate m_oEndDay = null;
    
    private EntryLayoutHelper m_oSchedulePanel = null;
    
    private TarentWidgetIconComboBox m_oIconComboBox_Filter1;
    private TarentWidgetIconComboBox m_oIconComboBox_Filter2;
    private TarentWidgetComponentTable m_oCompTable_Schedules;
    
    private ImageIcon m_oIcon_Unknown;
    
    private ImageIcon m_oIcon_Jump; 
    private ImageIcon m_oIcon_JumpDay; 
    private ImageIcon m_oIcon_JumpWeek; 
    private ImageIcon m_oIcon_JumpMonth; 
    private ImageIcon m_oIcon_Edit; 
    
    private List currentEventsSorted;
    
    public String getFieldName()
    {
        return "Terminliste";    
    }
    
    public String getFieldDescription()
    {
        return "Ein Feld zur Auflistung von Terminen";
    }
    
    
    public boolean isWriteOnly()
    {
        return true;  
    }
    
    public int getContext()
    {
        return CONTEXT_USER;
    }    
    
    public EntryLayoutHelper getPanel(String widgetFlags)
    {
        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIcon_Unknown = (ImageIcon)iconFactory.getIcon("history_appointment.gif"); 
        
        m_oIcon_Jump = (ImageIcon)iconFactory.getIcon("jump.gif"); 
        m_oIcon_JumpDay = (ImageIcon)iconFactory.getIcon("jump_day.gif"); 
        m_oIcon_JumpWeek = (ImageIcon)iconFactory.getIcon("jump_week.gif"); 
        m_oIcon_JumpMonth = (ImageIcon)iconFactory.getIcon("jump_month.gif"); 
        m_oIcon_Edit = (ImageIcon)iconFactory.getIcon("schedule_edit.gif"); 
        
        getAppointmentDisplayManager().addAppointmentDisplayListener(this);
        
        if(m_oSchedulePanel == null) m_oSchedulePanel = createPanel(widgetFlags);
        return m_oSchedulePanel;
    }
    
    
    public void postFinalRealize()
    {    
        fillScheduleList();
    }
    
    public void setData(PluginData data)
    {
    }
    
    public void getData(PluginData data)
    {
    }
    
    public boolean isDirty(PluginData data)
    {
        return false;
    }
    
    public void setEditable(boolean iseditable)
    {
    }
    
    public String getKey()
    {
        return "SCHEDULELIST";
    }
  
    public void setDoubleCheckSensitive(boolean issensitive)
    {
    }
    
    
    private TarentWidgetComponentTable.ObjectTableModel m_oTableModel;
    
    private EntryLayoutHelper createPanel(String widgetFlags)
    {
        TarentWidgetPanel panel = new TarentWidgetPanel(); 
        panel.setLayout(new BorderLayout());
        
        // tabelle erzeugen...
        m_oTableModel = new TarentWidgetComponentTable.ObjectTableModel(3);    
        
        m_oTableModel.setColumnName(0, "Typ");    
        m_oTableModel.setColumnEditable(0, false);
        
        m_oTableModel.setColumnName(1, "Zeit");    
        m_oTableModel.setColumnEditable(1, false);
        
        m_oTableModel.setColumnName(2, "Titel");    
        m_oTableModel.setColumnEditable(2, false);
        
        m_oCompTable_Schedules = new TarentWidgetComponentTable(m_oTableModel);
        m_oCompTable_Schedules.getJTable().getSelectionModel().addListSelectionListener(new ScheduleListSelectionListener());
        m_oCompTable_Schedules.getJTable().setRowHeight(20);
        m_oCompTable_Schedules.getJTable().setCellSelectionEnabled(false);
        m_oCompTable_Schedules.getJTable().setRowSelectionAllowed(true);    
        m_oCompTable_Schedules.getJTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        m_oCompTable_Schedules.setFixedColumnSize(0, 40);
        m_oCompTable_Schedules.setColumnSize(1, 300);
        
        m_oCompTable_Schedules.setColumnComponent(0, new ImageIcon(), new ImageIcon());
        m_oCompTable_Schedules.setColumnComponent(1, new JTextField(), new JTextField());
        m_oCompTable_Schedules.setColumnComponent(2, new JTextField(), new JTextField());
        
        MouseListener popupListener = new PopupListener();
        m_oCompTable_Schedules.getJTable().addMouseListener(popupListener);
        
        JPanel combopanel = new JPanel(new GridLayout(1,2));
        m_oIconComboBox_Filter1 = new TarentWidgetIconComboBox();
        m_oIconComboBox_Filter2 = new TarentWidgetIconComboBox();
        
        /*alle
        alle zuk�nftigen
        ----------------
        heute
        diese Woche (alle Termine der Woche, incl. der vergangenen Termin/Aufgaben)
        diesen Monat (alle Termine des Monats, incl. der vergangenen Termin/Aufgaben)
        ----------------
        7 Tage
        14 Tage 
        21 Tage
        ----------------
        Kalenderansicht
        */

        IconFactory iconFactory = SwingIconFactory.getInstance();
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_alltime.gif"), "alle", SCHEDULEFILTER_TIME_ALL));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_alltime.gif"), "alle zuk�nftigen", SCHEDULEFILTER_TIME_ALL_FUTURE));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("calendar_next.gif"), "---", null));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_today.gif"), "heute", SCHEDULEFILTER_TIME_TODAY));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "diese Woche", SCHEDULEFILTER_TIME_THIS_WEEK));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_month.gif"), "diesen Monat", SCHEDULEFILTER_TIME_THIS_MONTH));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("calendar_next.gif"), "---", null));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "7 Tage", SCHEDULEFILTER_TIME_WEEK));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "14 Tage", SCHEDULEFILTER_TIME_TWO_WEEKS));
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_week.gif"), "21 Tage", SCHEDULEFILTER_TIME_THREE_WEEKS));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("calendar_next.gif"), "---", null));
        
        m_oIconComboBox_Filter1.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_view.gif"), "Kalenderansicht", SCHEDULEFILTER_TIME_SCHEDULE));
        
        // ------------------------------------------------------------------------------------------------------------------
        
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_all.gif"), "alle", SCHEDULEFILTER_PRIORITY_ALL));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_lopri.gif"), "niedrige Priorit�t", SCHEDULEFILTER_PRIORITY_LOW));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_midpri.gif"), "mittlere Priorit�t", SCHEDULEFILTER_PRIORITY_MID));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_hipri.gif"), "hohe Priorit�t", SCHEDULEFILTER_PRIORITY_HIGH));
        m_oIconComboBox_Filter2.addIconComboBoxEntry(new TarentWidgetIconComboBox.IconComboBoxEntry((ImageIcon)iconFactory.getIcon("filter_private.gif"), "privat", SCHEDULEFILTER_PRIORITY_PRIVATE));
        
        FilterComboChanged combolistener = new FilterComboChanged();
        m_oIconComboBox_Filter1.addActionListener(combolistener);    
        m_oIconComboBox_Filter2.addActionListener(combolistener);    
        
        combopanel.add(m_oIconComboBox_Filter1);
        combopanel.add(m_oIconComboBox_Filter2);
        
        JLabel label = new JLabel("Termine: ");
        label.setFont(GUIHelper.getFont(GUIHelper.FONT_FORMULAR));
        panel.add(label, BorderLayout.NORTH);        
        
        JPanel firstlinepanel = new JPanel(new BorderLayout());
        firstlinepanel.add(combopanel, BorderLayout.CENTER);
        firstlinepanel.add(label, BorderLayout.WEST);
        
        JPanel mainpanel = new JPanel(new BorderLayout());
        mainpanel.add(firstlinepanel, BorderLayout.NORTH);        
        mainpanel.add(m_oCompTable_Schedules, BorderLayout.CENTER);        
        
        panel.add(mainpanel, BorderLayout.CENTER);        
        panel.setMinimumSize(new Dimension(5,5));
        
        getWidgetPool().addController("CONTROLER_SCHEDULELIST", this);
        
        m_bUseFilterComboChanged = false;
        m_oIconComboBox_Filter1.setSelectedItemByKey(SCHEDULEFILTER_TIME_ALL_FUTURE);
        m_oIconComboBox_Filter2.setSelectedItemByKey(SCHEDULEFILTER_PRIORITY_ALL);
        m_bUseFilterComboChanged = true;
        
        return new EntryLayoutHelper(new TarentWidgetInterface[] { panel }, widgetFlags);
    }
    
    private boolean m_bUseFilterComboChanged = true;
    private class FilterComboChanged implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (m_bUseFilterComboChanged)
            {  
                fillScheduleList();
            }
        }
    }
    
    
    public boolean isInDisplayedInterval(Date date)
    {
        ScheduleDate[] interval = getDisplayedDateInterval();
        ScheduleDate testdate = new ScheduleDate(date);    
        return (interval[0].before(testdate) && interval[1].after(testdate)) || interval[0].equals(testdate) || interval[1].equals(testdate);
    }
    
    public void setDateInterval(ScheduleDate date, ScheduleDate enddate)
    {
        //System.out.println("ScheduleListField::setFirstDay(" + date.getDateTimeString() + ", " + enddate.getDateTimeString() + ")");    
        if ( (!(date.equals(m_oFirstDay))) || (!(enddate.equals(m_oEndDay))) )
        {  
            m_oFirstDay = date;
            m_oEndDay = enddate;

            // Update der Liste ist nur dann n�tig, 
            // wenn diese auf "Kalenderansicht" steht, da sie sonst vom Kalenderzeitraum unabh�ngig ist.
            Object timefilterkey = getTimefilterkey();            
            if (SCHEDULEFILTER_TIME_SCHEDULE.equals(timefilterkey))
                fillScheduleList();
        }
    }
    
    public List getCurrentEvents(){
        return currentEventsSorted;
    }
    
    
    private void insertScheduleEntry(ImageIcon icon, String name, String date, Appointment appointment, String[] tooltips)
    {
        Object[] entry = createEntry(icon, name, date);        
        
        if (entry != null) 
        {            
            m_oCompTable_Schedules.addData(entry, tooltips);      
            m_oCompTable_Schedules.setRowKey(m_oCompTable_Schedules.getNumberOfEntries() - 1, appointment);
        }
    }
    
    private Object getTimefilterkey() {
        return ((TarentWidgetIconComboBox.IconComboBoxEntry)m_oIconComboBox_Filter1.getSelectedItem()).getKey();
    }
    
    private ScheduleDate[] getDisplayedDateInterval()
    {
        ScheduleDate[] interval = new ScheduleDate[2]; 
        
        Object timefilterkey = getTimefilterkey();
        ScheduleDate startdate = m_oFirstDay;
        ScheduleDate enddate = m_oEndDay;
        
        if (SCHEDULEFILTER_TIME_ALL.equals(timefilterkey))
        {      
            startdate = new ScheduleDate(0,0,0);
            enddate = new ScheduleDate(0,0,9999);
        }
        else if (SCHEDULEFILTER_TIME_ALL_FUTURE.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = new ScheduleDate(0,0,9999);
        }
        else if (SCHEDULEFILTER_TIME_TODAY.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(1);
        }
        else if (SCHEDULEFILTER_TIME_THIS_WEEK.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getMondayOfWeek().getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(7);
        }
        else if (SCHEDULEFILTER_TIME_THIS_MONTH.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstDayOfMonth().getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedMonths(1);
        }
        else if (SCHEDULEFILTER_TIME_WEEK.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(7);
        }
        else if (SCHEDULEFILTER_TIME_TWO_WEEKS.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(14);
        }
        else if (SCHEDULEFILTER_TIME_THREE_WEEKS.equals(timefilterkey))
        {      
            startdate = (new ScheduleDate()).getFirstSecondOfDay();
            enddate = startdate.getDateWithAddedDays(21);
        }
        else if (SCHEDULEFILTER_TIME_SCHEDULE.equals(timefilterkey))
        {      
            if ((m_oFirstDay != null) && (m_oEndDay != null))
            {        
                startdate = m_oFirstDay;
                enddate = m_oEndDay;
            }
            else
            {
                getGUIListener().getLogger().severe("ScheduleListField: es wurden noch nie Start und Enddatum des TerminPanels gesetzt!");
                startdate = (new ScheduleDate()).getFirstSecondOfDay();
                enddate = startdate.getDateWithAddedDays(1);        
            }
        }    
        
        //getGUIListener().getLogger().severe("Datum start: " + startdate.getDateString() + " ende: " + enddate.getDateString());
        interval[0] = startdate;
        interval[1] = enddate;
        return interval;
    }
    
    
    
    private int getSelectedAppointmentID()
    {
        int id = -1;
        int row = m_oCompTable_Schedules.getJTable().getSelectedRow();
        if (row != -1)
        {
            Appointment appointment = (Appointment)(m_oCompTable_Schedules.getRowKey(row));
            if (appointment != null)
            {  
                id = appointment.getId();
            }
        }
        return id;
    }
    
    
    private boolean selectAppointmentByID(int id)
    {
        int row = getRowWithAppointmentID(id);
        if (row != -1)
        {
            m_oCompTable_Schedules.getJTable().getSelectionModel().setSelectionInterval(row, row);
            return true;
        }
        else
        {
            m_oCompTable_Schedules.getJTable().getSelectionModel().clearSelection();
            return false;
        }
    }
    
    
    
    
    private Object m_oUsedTimeFilter = null;
    private Object m_oUsedPriorityFilter = null;
    private ScheduleDate m_oUsedStartDate = null;  
    private ScheduleDate m_oUsedEndDate = null;  
    
    public void fillScheduleList()
    {    
        Object timefilterkey = ((TarentWidgetIconComboBox.IconComboBoxEntry)m_oIconComboBox_Filter1.getSelectedItem()).getKey();
        Object priorityfilterkey = ((TarentWidgetIconComboBox.IconComboBoxEntry)m_oIconComboBox_Filter2.getSelectedItem()).getKey();
        
        // wenn schon geladen dann nicht mehr laden      
        ScheduleDate[] interval = getDisplayedDateInterval();
        ScheduleDate startdate = interval[0];
        ScheduleDate enddate = interval[1];
        
        boolean bMustBePrivat = false;
        int iPriority = -1;
        int selectedid = getSelectedAppointmentID();    
        
        if (SCHEDULEFILTER_PRIORITY_ALL.equals(priorityfilterkey))
        {      
            iPriority = -1;
        }
        else if (SCHEDULEFILTER_PRIORITY_LOW.equals(priorityfilterkey))
        {      
            iPriority = 1;
        }
        else if (SCHEDULEFILTER_PRIORITY_MID.equals(priorityfilterkey))
        {      
            iPriority = 2;
        }
        else if (SCHEDULEFILTER_PRIORITY_HIGH.equals(priorityfilterkey))
        {      
            iPriority = 3;
        }
        else if (SCHEDULEFILTER_PRIORITY_PRIVATE.equals(priorityfilterkey))
        {      
            bMustBePrivat = true;
        }
        
        m_oCompTable_Schedules.removeAllData();
        if (getGUIListener().getCalendarCollection() == null) return;  
        
        getGUIListener().setWaiting(true);      
        try
        {
            Collection appointments = getAppointments(startdate.getDate(), enddate.getDate());
            List sortlist = new ArrayList(appointments);
            Comparator comparator = new AppointmentComparator();
            Collections.sort(sortlist, comparator);
            
            // Save for Export
            currentEventsSorted = new ArrayList();
            
            Iterator appointmentiterator = sortlist.iterator();
            while(appointmentiterator.hasNext())
            {
                Appointment appointment = (Appointment)(appointmentiterator.next());
                int category = appointment.getAppointmentCategory();
                String subject = appointment.getAttribute(Appointment.KEY_SUBJECT);
                String description = appointment.getAttribute(Appointment.KEY_DESCRIPTION);
                Date start = appointment.getStart();
                Date end = appointment.getEnd();
                int priority = appointment.getPriority();
                boolean privat = appointment.isPrivat(); 
                
                boolean mustdisplay = true;
                
                if (privat)
                    mustdisplay = (getGUIListener().getCalendarCollection().getCalendar(appointment.getOwner().getCalendar(false).getId()) != null);
                        
                
                if (bMustBePrivat && !privat) mustdisplay = false;
                else
                {
                    if (iPriority != -1)
                    {
                        if (iPriority != priority) mustdisplay = false;
                    }
                }
                
                if (appointment.isJob()) mustdisplay = false;
                
                if (mustdisplay)
                { 
                    
                    // Prepare new Container
                    AppointmentPrintContainer APContainer = new AppointmentPrintContainer();
                    String Ort = appointment.getAttribute(Appointment.KEY_LOCATION).trim();
                    if (Ort.equals("")) Ort = " - ";
                    APContainer.set_ort(Ort);
                    APContainer.set_titel(appointment.getAttribute(Appointment.KEY_SUBJECT));
                    if (new ScheduleDate(appointment.getStart()).isSameDay(new ScheduleDate(appointment.getEnd()))){
                        APContainer.set_Tag_vonbis(myWeekDayFormatter.format(start));
                        APContainer.set_Datum_vonbis(myDateFormatter.format(start));
                    }else{
                        APContainer.set_Tag_vonbis(myWeekDayFormatter.format(start) + " - " + myWeekDayFormatter.format(end));
                        APContainer.set_Datum_vonbis(myDateFormatter.format(start) + " - " + myDateFormatter.format(end)) ;
                    }
                    
                    APContainer.set_start(myHourFormatter.format(start));
                    APContainer.set_end(myHourFormatter.format(end));
                    APContainer.set_monat(myMonthTimeFormatter.format(start));
                    APContainer.set_kw(Integer.parseInt(myKWTimeFormatter.format(start)));
                                    
                    
                    ScheduleType type = getControlListener().getScheduleData().getScheduleTypes().getScheduleType(category);
                    ImageIcon icon = m_oIcon_Unknown;
                    String typetext = "unbekannt";
                    if (type != null)
                    {  
                        icon = type.getIcon();
                        typetext = type.getName() + " (" + type.getDescription() + ")";
                    }
                    
                    String startdatestr = (myDateTimeFormatter.format(start)); 
                    String longdatestr = (myLongDateTimeFormatter.format(start));
                    String longenddatestr = (myLongDateTimeFormatter.format(end));
                    
                    // Add all users to Popup/ Export
                    
                    String users = "";
                    String exportUsers = "";
                    Collection addresses = getAllAddressesOfAppointment(appointment);
                    if ((addresses != null) && (m_bShowAddressesInTooltip))
                    {
                        if (addresses.size() > 0)
                        {   
                            // Fill Export Container
                            Iterator addressiterator = addresses.iterator();
                            while(addressiterator.hasNext())
                            {
                                Address address = (Address)(addressiterator.next());
                                users += ("* " + address.getShortAddressLabel() + "\n");
                                exportUsers += ("* " +  address.getVorname() + ", " + address.getNachname() + " \n");
                            }
                            if (users.endsWith("\n")) users = users.substring(0, users.length()-1);
                            APContainer.set_Teilnehmer( exportUsers);
                            // End Export
                            
                            
                            users = "";
                            if (addresses.size() <= m_iMaxAddressesInTooltip)
                            {                   
                                addressiterator = addresses.iterator();
                                while(addressiterator.hasNext())
                                {
                                    Address address = (Address)(addressiterator.next());
                                    users += ("<b>*</b> " + address.getShortAddressLabel() + "\n");
                                }
                                if (users.endsWith("\n")) users = users.substring(0, users.length()-1);
                                users = "<b>Teilnehmer:</b><br>" + users.replaceAll("\n" , "<br>") + "<br>";
                            }
                            else
                            {
                                users = "<b>Teilnehmer:</b><br>insgesamt " + addresses.size() + " Adressen zugeordnet.<br>";                      
                            }
                        }else{
                            APContainer.set_Teilnehmer("");
                        }
                    }
                    
                    String priotext = "Priorit�t: " + priority + "<br>";
                    String privattext = privat ? "ist privat" : "";                 
                    
                    String encodeddescription;
                    if (description.trim().length() > 0)
                    {  
                        encodeddescription = description.trim().replaceAll("\n" , "<br>") + "<br>";
                    }
                    else
                    {
                        encodeddescription = "";
                    }
                    
                    String tooltiptext ="";
                    if (privat && ( appointment.getOwner().equals(getGUIListener().getUser(null)) == false )){
                        subject = ConfigManager.getAppearance().get(Key.PRIVATE_DATE);
                        APContainer.set_titel(subject);
                        APContainer.set_Teilnehmer(" - ");
                        APContainer.set_ort(" - ");
                        tooltiptext =  "<html><i>"+ subject +"</i><br>" + longdatestr + " bis " + longenddatestr + "</html>";
                    }else{
                        tooltiptext =  "<html><i>" + typetext + "</i><br>" + longdatestr + " bis " + longenddatestr + "<br><br><b>" + subject + "</b><br><br>" + encodeddescription + "<br>" + users + "<br>" + priotext + privattext + "</html>";
                    }
                    
                    
                    
                    String[] tooltips = new String[3];
                    tooltips[0] = tooltiptext;
                    tooltips[1] = tooltiptext;
                    tooltips[2] = tooltiptext;
                    
                    // Update Export
                    currentEventsSorted.add(APContainer);
                    
                    insertScheduleEntry(icon, subject, startdatestr, appointment, tooltips);
                }
            }
        } 
        catch (ContactDBException e)
        {
            e.printStackTrace();
        }
        finally
        {
            getGUIListener().setWaiting(false);
        }
        
        if (m_oCompTable_Schedules.getNumberOfEntries() > 0) m_oCompTable_Schedules.initColumnSizes(0);
        useSelectionListener = false;
        selectAppointmentByID(selectedid);        
        useSelectionListener = true;
    }  
    
    private List getAllAddressesOfAppointment(Appointment appointment)
    {
        String errormessage =   "Folgende Adressen von Teilnehmern dieses Termin werden nicht angezeigt\n" +
                                "(Sie besitzen wahrscheinlich keine entsprechende Leseberechtigung):\n\n";
        String previewString;
        
        ArrayList addresses = new ArrayList();
        try
        {
            // interne Teilnehmer (Systembenutzer) hinzuf�gen...
            Collection calendars = appointment.getCalendars();
            Iterator it = calendars.iterator();
            while(it.hasNext())
            {
                Calendar calendar = (Calendar)(it.next());
                User user = calendar.getUser();
                if (user != null)
                {
                    // ist benutzerkalender (interner teilnehmer)
                    Address address = user.getAssociatedAddress();
                    if (address != null)
                    {  
                        addresses.add(address);
                    }
                    else
                    { 
                        AddressImpl fakeAddress = new AddressImpl();
                        fakeAddress.setId(-1);
                        addresses.add(fakeAddress);
                        addresses.add(fakeAddress);
                        errormessage += "Adresse von User " + user.getLoginName() + "\n";
                        
                        //getGUIListener().publishError("Die Adresse zu dem Benutzer " + user.getLoginName() + " kann nicht angezeigt werden, da keine Leseberechtigung auf diese Adresse besteht");
                        // Log Ausgabe nur auf Stufe INFO. Sie im normalen Betrieb nicht auftauchen.
                        getGUIListener().getLogger().log(Level.INFO, "getAssociatedAddress() lieferte keine Adresse zur�ck -> Wahrscheinlich unzureichende Rechte");
                    }
                }
            }
            
            //if (error) getGUIListener().publishError(errormessage);
            
            // externe Teilnehmer hinzuf�gen...
            addresses.addAll(appointment.getAddresses());
            
        } 
        catch (ContactDBException e)
        {
            getGUIListener().getLogger().log(Level.SEVERE, "Fehler beim Sammeln aller Benutzer eines Appointments", e);      
        }
        return addresses;
        
    }
    
    private Object[] createEntry(ImageIcon icon, String name, String date)
    {
        Object[] objects = new Object[3]; 
        
        objects[0] = icon;
        objects[1] = date;
        objects[2] = name;
        
        return(objects);
    }
    
    
    private int getRowWithAppointmentID(int id)
    {
        for(int i=0; i<(m_oTableModel.getRowCount()); i++)
        {
            Appointment appointment = (Appointment)(m_oTableModel.getRowKey(i));
            if (appointment.getId() == id) return i;
        }
        return -1;
    }
    
    
    private void updateAppointmentOfRow(int row, Appointment appointment)
    {
        m_oTableModel.setRowKey(row, appointment);
    }
    
    public void updateAppointment(Appointment appointment)
    {
        if (appointment != null)
        {        
            int row = getRowWithAppointmentID(appointment.getId());
            if (row != -1)
            {
                updateAppointmentOfRow(row, appointment);
            }
        }
    }
    
    public void changedAppointment(Appointment appointment)
    {
        updateAppointment(appointment);
        
        //System.out.println("changedAppointment(" + appointment + ")");    
        if (appointment != null)
        {        
            int row = getRowWithAppointmentID(appointment.getId());
            // if (row != -1)
            // {
            fillScheduleList();         // ver�ndertes Appointment wird momentan angezeigt... neu laden!
            // }
        }
    }
    
    public void addedAppointment(Appointment appointment)
    {
        if (appointment == null) return;
        fillScheduleList();
    }
    
    public void removedAppointment(Appointment appointment)
    {
        int row = getRowWithAppointmentID(appointment.getId());
        if (row != -1)
        {
            fillScheduleList();
        }        
    }
    
    
    private boolean useSelectionListener = true;
    public void selectedAppointment(Appointment appointment)
    {    
        //System.out.println("selectedAppointment(" + appointment + ")");    
        int row = getRowWithAppointmentID(appointment.getId());
        if (row != -1)
        {
            useSelectionListener = false;
            m_oCompTable_Schedules.getJTable().getSelectionModel().setSelectionInterval(row, row);
            useSelectionListener = true;
        }
        else
        {
            m_oCompTable_Schedules.getJTable().clearSelection();      
        }
    }
    
    
    
    public Appointment getAppointment(int appointmentid)
    {
        int row = getRowWithAppointmentID(appointmentid);
        if (row != -1)
        {
            Appointment appointment = (Appointment)(m_oTableModel.getRowKey(row));
            return appointment;      
        }
        return null;
    }
    
    
    public void setViewType(String viewtype)
    {
        // not interested in this...
    }
    
    public void setVisibleInterval(ScheduleDate start, ScheduleDate end)
    {
        setDateInterval(start, end);  
    }
    
    
    
    private class ScheduleListSelectionListener implements ListSelectionListener
    {
        public void valueChanged(ListSelectionEvent e)
        {
            if (useSelectionListener)
            {  
                int row = m_oCompTable_Schedules.getJTable().getSelectedRow();
                if (row != -1)
                {
                    Appointment appointment = (Appointment)(m_oCompTable_Schedules.getRowKey(row));
                    if (appointment != null)
                    {  
                        getAppointmentDisplayManager().fireSelectedAppointment(appointment, m_oThisScheduleListField);
                    }
                }
            }
        }
    }
    
    
    private class ActionButtonClicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String name = ((JButton)(e.getSource())).getName();
            String[] coords = name.split(",");
            int row = -1;
            int column = -1;
            try
            {
                row = Integer.parseInt(coords[0]);
                column = Integer.parseInt(coords[1]);
            }
            catch(NumberFormatException nfe) {}
            
            Object rowkey = m_oCompTable_Schedules.getRowKey(row);
            //TODO:
            
        }
    }
    
    
    public void changedCalendarCollection()
    {
        fillScheduleList();    
    }
    
    
    // PopUpMenu stuff...
    
    private JPopupMenu createPopUpMenu(Appointment appointment)
    {
        JPopupMenu oPopupMenu = new JPopupMenu();
        
        
        JMenuItem menuItemEDIT = new JMenuItem("<html>Termin bearbeiten</html>");          
        menuItemEDIT.setIcon(m_oIcon_Edit);
        menuItemEDIT.addActionListener(new MenuItem_EDIT_clicked(appointment));
        oPopupMenu.add(menuItemEDIT);
        
        JMenu oMenuJump = new JMenu("<html>springe zu Termin</html>");
        oMenuJump.setIcon(m_oIcon_Jump);
        
        JMenuItem menuItemJUMPDAY = new JMenuItem("<html>in der Tages�bersicht</html>");          
        menuItemJUMPDAY.setIcon(m_oIcon_JumpDay);
        menuItemJUMPDAY.addActionListener(new MenuItem_JUMP_clicked(appointment, ScheduleField.CARD_DAY));
        oMenuJump.add(menuItemJUMPDAY);
        
        JMenuItem menuItemJUMPWEEK = new JMenuItem("<html>in der Wochen�bersicht</html>");          
        menuItemJUMPWEEK.setIcon(m_oIcon_JumpWeek);
        menuItemJUMPWEEK.addActionListener(new MenuItem_JUMP_clicked(appointment, ScheduleField.CARD_WEEK));
        oMenuJump.add(menuItemJUMPWEEK);
        
        JMenuItem menuItemJUMPMONTH = new JMenuItem("<html>in der Monats�bersicht</html>");          
        menuItemJUMPMONTH.setIcon(m_oIcon_JumpMonth);
        menuItemJUMPMONTH.addActionListener(new MenuItem_JUMP_clicked(appointment, ScheduleField.CARD_MONTH));
        oMenuJump.add(menuItemJUMPMONTH);
        
        oPopupMenu.add(oMenuJump);
        
        return oPopupMenu;
    }
    
    
    private class MenuItem_JUMP_clicked implements ActionListener
    {    
        private Appointment m_oAppointment;
        private String m_oViewType;
        
        public MenuItem_JUMP_clicked(Appointment appointment, String viewtype)
        {
            m_oAppointment = appointment;
            m_oViewType = viewtype;
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            if (getScheduleField() != null) 
            {
                jumpToDate(m_oAppointment, m_oViewType);
            }
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    private class MenuItem_EDIT_clicked implements ActionListener
    {    
        private Appointment m_oAppointment;
        
        public MenuItem_EDIT_clicked(Appointment appointment)
        {
            m_oAppointment = appointment;
        }
        
        public void actionPerformed(ActionEvent e)
        {      
            if (m_oAppointment != null)
            {  
                getAppointmentDisplayManager().fireSelectedAppointment(m_oAppointment, m_oThisScheduleListField);
            }
        }    
    }
    
    // .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .     
    
    
    class PopupListener extends MouseAdapter 
    {
        public void mousePressed(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        public void mouseReleased(MouseEvent e) 
        {
            maybeShowPopup(e);
        }
        
        private void maybeShowPopup(MouseEvent e) 
        {
            if (e.isPopupTrigger()) 
            {
                Point p = e.getPoint();
                int row = m_oCompTable_Schedules.getJTable().rowAtPoint(p);
                int visiblecol = m_oCompTable_Schedules.getJTable().columnAtPoint(p);
                int col = m_oCompTable_Schedules.getJTable().convertColumnIndexToModel(visiblecol);        
                
                if (row != -1)
                {
                    Appointment appointment = (Appointment)(m_oCompTable_Schedules.getRowKey(row));
                    if (appointment != null)
                    {  
                        JPopupMenu menu = createPopUpMenu(appointment);         
                        menu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }else if(e.getClickCount() == 2){
                
                Point p = e.getPoint();
                int row = m_oCompTable_Schedules.getJTable().rowAtPoint(p);
                int visiblecol = m_oCompTable_Schedules.getJTable().columnAtPoint(p);
                int col = m_oCompTable_Schedules.getJTable().convertColumnIndexToModel(visiblecol);        
                
                if (row != -1)
                {
                    Appointment appointment = (Appointment)(m_oCompTable_Schedules.getRowKey(row));
                    if (appointment != null)
                    {  
                        jumpToDate(appointment,getScheduleField().getCardVisible());
                    }
                }       
                
            }
        }
    }
    
    
    
    // ---------------------------------------------------------------------------------------------
    
    private ScheduleField m_oScheduleField = null;
    
    private ScheduleField getScheduleField()
    {
        if (m_oScheduleField == null) 
        {
            Object controler = getWidgetPool().getController("CONTROLER_SCHEDULEPANEL");
            if (controler instanceof ScheduleField)
            {  
                m_oScheduleField = (ScheduleField)controler;
            }
        }
        return m_oScheduleField;
    }
    
    
    public void jumpToDate(Appointment appointment, String viewtype)
    {
        try
        {
            ScheduleDate date = new ScheduleDate(appointment.getStart());    
            ScheduleDate oEndDate = date;
            if (getScheduleField() != null)
            {  
                if (ScheduleField.CARD_DAY.equals(viewtype)) oEndDate = date.getDateWithAddedDays(1);  
                else if (ScheduleField.CARD_MONTH.equals(viewtype)) oEndDate = date.getDateWithAddedMonths(1);  
                else if (ScheduleField.CARD_WEEK.equals(viewtype)) 
                {
                    date = date.getMondayOfWeek();
                    oEndDate = date.getDateWithAddedDays(7); 
                }
                
                getAppointmentDisplayManager().fireSetViewType(viewtype, this);
                getAppointmentDisplayManager().fireSetVisibleInterval(date, oEndDate, this);
                getAppointmentDisplayManager().fireSelectedAppointment(appointment, this);
            }    
        }
        catch (ContactDBException e1)
        {
            e1.printStackTrace();
        }
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridActive(boolean)
     */
    public void setGridActive(boolean usegrid)
    {
        // TODO Auto-generated method stub
        
    }
    
    /* (non-Javadoc)
     * @see de.tarent.contact.gui.fieldhelper.CalendarVisibleElement#setGridHeight(int)
     */
    public void setGridHeight(int seconds)
    {
        // TODO Auto-generated method stub
        
    }

    /** Handles export event.*/
    public void clickedExportButton() {
    }
    
    /** Handles new appointment event.*/
    public void clickedNewAppointmentButton() {
    }

    /** Handles new task event.*/
    public void clickedNewTaskButton() {
    }
    
}
