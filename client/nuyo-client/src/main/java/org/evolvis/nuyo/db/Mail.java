/* $Id: Mail.java,v 1.2 2006/06/06 14:12:08 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Michael Klink and Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.util.List;

/**
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.2 $
 */
public interface Mail {
    /**
     * Diese Methode initialisiert Server-seitig den Mail-Zugriff f�r den eingeloggen Benutzer. 
     * 
     * @throws ContactDBException
     */
	public void init() throws ContactDBException;

	/**
	 * Diese Methode liefert eine Liste der Mail-Stores, die dem Benutzer zugeordnet sind.  
	 * 
	 * @return Liste der Mail-Stores des Benuters.
	 * @throws ContactDBException
	 */
	public List getStores() throws ContactDBException;
	
	/**
	 * Diese Methode liefert einen Mail-Store des Benutzers.
	 * 
	 * @param storeid die ID des Stores; der Wert 0 liefert den Default-Store des Benutzers.
	 * @return der angeforderte Mail-Store
	 * @throws ContactDBException insbesondere, wenn der angeforderte Store nicht verf�gbar ist
	 * 	oder der Benutzer nicht auf ihn zugreifen darf.
	 */
	public MailStore getStore(int storeid) throws ContactDBException;

	/**
	 * Diese Methode erzeugt einen MailStore
	 * 
	 * @return ein neuer Mail-Store.
	 * @throws ContactDBException
	 */
	public MailStore createStore() throws ContactDBException;
}
