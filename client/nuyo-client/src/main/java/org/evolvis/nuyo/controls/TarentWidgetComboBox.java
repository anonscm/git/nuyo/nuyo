/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JComponent;

/**
 * @author niko
 */
public class TarentWidgetComboBox extends JComboBox implements TarentWidgetInterface
{
  public TarentWidgetComboBox()
  {
    super();
    this.addActionListener(new ComboChangeListener());
  }
  
  public TarentWidgetComboBox(Object entries)
  {
    this();
    this.setData(entries);
    this.addActionListener(new ComboChangeListener());
  }
  
  public TarentWidgetComboBox(String[] entries)
  {
    this();
    for(int i=0; i<(((String[])entries).length); i++)
    {
      this.addItem((((String[])entries)[i]));
    }
    this.addActionListener(new ComboChangeListener());
  }
  
  public TarentWidgetComboBox(Vector entries)
  {
  	
  	this();
  	for(Iterator it = entries.iterator(); it.hasNext();){
  		this.addItem(it.next());
  	}
    this.addActionListener(new ComboChangeListener());
  }
  
  
  private class ComboChangeListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      fireValueChanged(getSelectedItem());
    }
  }

	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    if (data instanceof Object[])
    {
      this.removeAllItems();
      for(int i=0; i<(((Object[])data).length); i++)
      {
        this.addItem((((Object[])data)[i]));
      }
    }
	}

	/**
	 * @see TarentWidgetInterface#getData()
	 */
  public Object getData() 
  {
    return this.getSelectedItem();
  }

  public JComponent getComponent()
  {
    return(this);
  }

  public JComboBox getComboBox()
  {
    return(this);
  }
  public void setLengthRestriction(int maxlen)
  {
  }

  public void setWidgetEditable(boolean iseditable)
  {
    this.setEnabled(iseditable);
  }

  public boolean isEqual(Object data)
  {
    if (data.equals(this.getSelectedItem())) return(true);
    else                                                         return(false);
  }
  
  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  
  private void fireValueChanged(Object newvalue)
  {
    Iterator it = m_oWidgetChangeListeners.iterator();
    while(it.hasNext())
    {
      TarentWidgetChangeListener listener = (TarentWidgetChangeListener)(it.next());
      listener.changedValue(newvalue);
    }
  }
  
  
}
