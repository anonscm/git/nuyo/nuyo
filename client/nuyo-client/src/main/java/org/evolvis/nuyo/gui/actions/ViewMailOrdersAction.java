package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.gui.MainFrameExtStyle;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.xana.action.AbstractGUIAction;

/**
 * Activates a view of mail orders.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class ViewMailOrdersAction extends AbstractGUIAction {

    private static final TarentLogger logger = new TarentLogger(ViewMailOrdersAction.class);
    private static final long serialVersionUID = 8753118872290291767L;
    private ControlListener mainFrame;

    public void actionPerformed(ActionEvent e) {
        if(mainFrame != null) {
            mainFrame.activateTab(MainFrameExtStyle.TAB_MAIL_ORDERS);
        } else logger.warning(getClass().getName() + "is not initialized.");
    }
    
    public void init(){
        mainFrame = ApplicationServices.getInstance().getMainFrame();
        setEnabled(mainFrame.isTabEnabled(MainFrameExtStyle.TAB_MAIL_ORDERS));
    }
}
