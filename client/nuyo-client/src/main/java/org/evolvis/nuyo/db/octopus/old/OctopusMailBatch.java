/* $Id: OctopusMailBatch.java,v 1.10 2007/08/30 16:10:33 fkoester Exp $
 * 
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Thomas Fuchs.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus.old;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.evolvis.nuyo.db.Addresses;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.MailBatch;
import org.evolvis.nuyo.db.octopus.AddressesImpl;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.SoapClient;


/**
 *
 * Diese Klasse stellt einen Versandauftrag in einem Soap-Kontext dar.
 * 
 * @deprecated Abgel�st von neuer Beanbasierter Implementierung
 * 
 * @author thomas
 */
public class OctopusMailBatch extends MailBatch {

    OctopusDatabase db;
    OctopusConnector oc = null;
    String modul = null;
    String vertgrp = null;
    
    static final public String FUNCTION_GETCHANNELCOUNT = "getChannelCount";
    static final public String FUNCTION_SETCHANNELCOUNT = "setChannelCount";


    /**
     * Dieser Konstruktor erh�lt alle Daten zum Versandauftrag als Vector
     * �bergeben.
     */

    public OctopusMailBatch(OctopusDatabase db,
                            OctopusConnector oc,
                            String modul,
                            List values,
                            String username,
                            String vertgrp,
                            boolean isnew) throws ContactDBException
    {
        this.db = db;
        this.oc = oc;
        this.modul = modul;
        this.vertgrp = vertgrp;
        this.id = Integer.parseInt(asString(values.get(0)));
        this.name = asString(values.get(1));
        this.userId = asString(values.get(2));
        this.userName = username;
        String dateString = asString(values.get(3));
        SimpleDateFormat longformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        SimpleDateFormat shortformat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        
		try {
			Date date = longformat.parse(dateString);
			this.dateTime = shortformat.format(date);
		} catch (ParseException e) {
			 // if conversion fails we use the original string
			 this.dateTime = dateString;
		}
       
        
        if (isnew) 
        setName("Versandauftrag (" + dateTime + ")");
        //this.recSrc = recSrc;
        //this.filtered = filtered;
        //this.filter = filter;
        this.mergeFax = Integer.parseInt(asString(values.get(4)));
        this.mergeMail = Integer.parseInt(asString(values.get(5)));
        this.mergePost = Integer.parseInt(asString(values.get(6)));
        this.notAssigned = Integer.parseInt(asString(values.get(7)));
        this.sentFax = Integer.parseInt(asString(values.get(8)));
        this.sentMail = Integer.parseInt(asString(values.get(9)));
        this.sentPost = Integer.parseInt(asString(values.get(10)));
        this.total = Integer.parseInt(asString(values.get(11)));
    }



    /**
     * L�scht diesen Versandauftrag.
     * 
     * @throws ContactDBException
     *             bei Problemen in der Datenbank
     */
    public void delete() throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("function", "delete");
        map.put("id", new Integer(id));
        oc.call(modul, "mailBatchSOAP", map);
        id = BATCH_NEW_ID;
    }

    /**
     * Diese Methode z�hlt die Elemente (Adressen) in T_MergeOrder, die in dem
     * angegebenen Kanal liegen.
     * 
     * @return die Adressanzahl im angegebenen Kanal.
     */
    protected int getChannelCount(CHANNEL channel) throws ContactDBException
    {
            Map map = new TreeMap();
            map.put("function", "getChannelCount");
            map.put("channel", "");
            map.put("id", new Integer(getId()));
            if (channel != null && !CHANNEL.TOTAL.equals(channel)) 
            map.put("channel", channel.getChannelName());
            //        	Map m = new TreeMap((Map) oc.call(modul, "mailBatchSOAP", map));

            Map tmp = SoapClient.executeDispatchOrderFunction("getChannelCount",
                new Integer(getId()), channel);

            //System.out.println("Channel: "+channel+" Count:
            // "+((Integer)m.get("id")).intValue());
            return ((Integer) tmp.get("id")).intValue();
    }

    /**
     * Diese Methode setzt Z�hler MergeMail, MergePost, MergeFax, Total,
     * NotAssigned an der Tabelle T_MergeOrder.
     */
    protected void setChannelCount(CHANNEL channel) throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("id", new Integer(getId()));
        map.put("function", "setChannelCount");
        if (CHANNEL.ALL.equals(channel)) {
            total = getChannelCount(CHANNEL.TOTAL);
            mergeMail = getChannelCount(CHANNEL.MAIL);
            mergeFax = getChannelCount(CHANNEL.FAX);
            mergePost = getChannelCount(CHANNEL.POST);
            notAssigned = getChannelCount(CHANNEL.NOT_ASSIGNED);
            map.put("channel", "c_all");
            map.put("total", new Integer(total));
            map.put("mail", new Integer(mergeMail));
            map.put("fax", new Integer(mergeFax));
            map.put("post", new Integer(mergePost));
            map.put("assigned", new Integer(notAssigned));
            oc.call(modul, "mailBatchSOAP", map);
        }
        else if (CHANNEL.MAIL.equals(channel)) {
            mergeMail = getChannelCount(CHANNEL.MAIL);
            notAssigned = getChannelCount(CHANNEL.NOT_ASSIGNED);
            map.put("channel", "c_mail");
            map.put("mail", new Integer(mergeMail));
            map.put("assigned", new Integer(notAssigned));
            oc.call(modul, "mailBatchSOAP", map);
        }
        else if (CHANNEL.FAX.equals(channel)) {
            mergeFax = getChannelCount(CHANNEL.FAX);
            notAssigned = getChannelCount(CHANNEL.NOT_ASSIGNED);
            map.put("channel", "c_fax");
            map.put("fax", new Integer(mergeFax));
            map.put("assigned", new Integer(notAssigned));
            oc.call(modul, "mailBatchSOAP", map);
        }
        else if (CHANNEL.POST.equals(channel)) {
            mergePost = getChannelCount(CHANNEL.POST);
            notAssigned = getChannelCount(CHANNEL.NOT_ASSIGNED);
            map.put("channel", "c_post");
            map.put("post", new Integer(mergePost));
            map.put("assigned", new Integer(notAssigned));
            oc.call(modul, "mailBatchSOAP", map);
        }
    }

    /**
     * Ordnet Adressen dem �bergebenen Kanal zu.
     * 
     * @throws ContactDBException
     *             bei Problemen in der Datenbank
     */
    public void setChannel(CHANNEL channel) throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("id", new Integer(getId()));
        map.put("function", "setChannel");
        if (CHANNEL.MAIL.equals(channel))
            {
                if (getSentMail() != NOT_ASSIGNED) 
                    return;
                map.put("channel", "mail");
                oc.call(modul, "mailBatchSOAP", map);
                setSentMail(ASSIGNED_NOT_SENT);
            }
        else if (CHANNEL.FAX.equals(channel))
            {
                if (getSentFax() != NOT_ASSIGNED) 
                    return;
                map.put("channel", "fax");
                oc.call(modul, "mailBatchSOAP", map);
                setSentFax(ASSIGNED_NOT_SENT);
            }
        else if (CHANNEL.POST.equals(channel))
            {
                if (getSentPost() != NOT_ASSIGNED) 
                    return;
                map.put("channel", "post");
                oc.call(modul, "mailBatchSOAP", map);
                setSentPost(ASSIGNED_NOT_SENT);
            }
        else
            return;
        setChannelCount(channel);
    }

    /**
     * Gibt Adressen dem �bergebenen Kanal frei.
     * 
     * @throws ContactDBException
     *             bei Problemen in der Datenbank
     */
    public void resetChannel(CHANNEL channel) throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("id", new Integer(getId()));
        map.put("function", "resetChannel");
        if (CHANNEL.MAIL.equals(channel))
            {
                if (sentMail != SENT)
                    {
                        map.put("channel", "mail");
                        oc.call(modul, "mailBatchSOAP", map);
                        setChannelCount(CHANNEL.ALL);
                        setSentMail(NOT_ASSIGNED);
                    }
            }
        else if (CHANNEL.FAX.equals(channel))
            {
                if (sentFax != SENT)
                    {
                        map.put("channel", "fax");
                        oc.call(modul, "mailBatchSOAP", map);
                        setChannelCount(CHANNEL.ALL);
                        setSentFax(NOT_ASSIGNED);
                    }
            }
        else if (CHANNEL.POST.equals(channel))
            {
                if (sentPost != SENT)
                    {
                        map.put("channel", "post");
                        oc.call(modul, "mailBatchSOAP", map);
                        setChannelCount(CHANNEL.ALL);
                        setSentPost(NOT_ASSIGNED);
                    }
            }
    }

    /**
     * F�hrt den �bergebenen Kanal aus.
     * 
     * @throws ContactDBException
     *             bei Problemen in der Datenbank
     */
    public void doChannel(CHANNEL channel) throws ContactDBException
    {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED,
            "OctopusMailBatch.doChannel() nicht implementiert");
    }

    /**
     * Diese Methode liefert die Adressen zum angegeben Kanal.
     * 
     * @throws ContactDBException
     *             bei Problemen in der Datenbank
     */
    public Addresses getChannelAddresses(CHANNEL channel)
        throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("id", new Integer(getId()));
        map.put("function", "getChannelAddresses");
        map.put("channel", channel.getChannelName());
        SortedMap m = new TreeMap((Map) oc.call(modul, "mailBatchSOAP", map));
        
        List pkList = new ArrayList();
        for (Iterator it = m.values().iterator();it.hasNext();) {
            pkList.add(new Integer(it.next().toString()));
        }
        
        AddressesImpl addressList = new AddressesImpl(db);                
        addressList.setPkFilter(pkList);
        addressList.initialLoad();
        addressList.waitFor(0);

        return addressList;           
    }

    /**
     * Diese Methode setzt ein Feld des Datensatzes zum aktuellen Sendeauftrag
     * auf einen neuen Wert.
     * 
     * @param name
     *            Name des zu setzenden Datenbankfeldes.
     * @param value
     *            int-Wert, den das Datenbankfeld annehmen soll.
     */
    public void setValue(String name, int value) throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("function", "setIntValue");
        map.put("id", new Integer(id));
        map.put("name", name);
        map.put("value", new Integer(value));
        oc.call(modul, "mailBatchSOAP", map);
    }

    /**
     * Diese Methode setzt ein Feld des Datensatzes zum aktuellen Sendeauftrag
     * auf einen neuen Wert.
     * 
     * @param name
     *            Name des zu setzenden Datenbankfeldes.
     * @param value
     *            String-Wert, den das Datenbankfeld annehmen soll.
     */
    public void setValue(String name, String value) throws ContactDBException
    {
        Map map = new TreeMap();
        map.put("function", "setStringValue");
        map.put("id", new Integer(id));
        map.put("name", name);
        map.put("value", value);
        oc.call(modul, "mailBatchSOAP", map);
    }

    /**
     * Setzt den Bezeichner des Auftrags.
     * 
     * @param name
     *            Name des Auftrags.
     */
    public void setName(String name) throws ContactDBException
    {
        setValue("ACTIONTITLE", name);
        super.setName(name);
    }

    /**
     * Setzt den Status der als Fax zu versendenden Exemplare.
     * 
     * @param sentFax
     *            {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht
     *            zugeordnet; {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT},
     *            wenn zugeordnet, aber nicht ausgef�hrt; {@link #SENT SENT},
     *            wenn ausgef�hrt.
     */
    public void setSentFax(int sentFax) throws ContactDBException
    {
        setValue("ADDRESSCOUNT06", sentFax);
        super.setSentFax(sentFax);
    }

    /**
     * Setzt den Status der als Email zu versendenden Exemplare.
     * 
     * @param sentMail
     *            {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht
     *            zugeordnet; {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT},
     *            wenn zugeordnet, aber nicht ausgef�hrt; {@link #SENT SENT},
     *            wenn ausgef�hrt.
     */
    public void setSentMail(int sentMail) throws ContactDBException
    {
        setValue("ADDRESSCOUNT05", sentMail);
        super.setSentMail(sentMail);
    }

    /**
     * Setzt den Status der als Brief zu versendenden Exemplare.
     * 
     * @param sentPost {@link #NOT_ASSIGNED NOT_ASSIGNED}, wenn noch nicht zugeordnet;
     *         {@link #ASSIGNED_NOT_SENT ASSIGNED_NOT_SENT}, wenn zugeordnet,
     *         aber nicht ausgef�hrt; {@link #SENT SENT}, wenn ausgef�hrt.
     */
    public void setSentPost(int sentPost) throws ContactDBException
    {
        setValue("ADDRESSCOUNT07", sentPost);
        super.setSentPost(sentPost);
    }

    private final static String asString(Object o) {
        return o == null ? null : o.toString();
    }

    public void commit() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }

    public void rollback() throws ContactDBException {
        throw new ContactDBException(ContactDBException.EX_NOT_IMPLEMENTED);
    }
    /* (non-Javadoc)
     * @see de.tarent.contact.db.PersistentEntity#isCommit()
     */



    /* (non-Javadoc)
     * @see de.tarent.contact.db.PersistentEntity#populate(java.util.List)
     */
    public void populate(ResponseData values) {
        // TODO Auto-generated method stub
        
    }



    /* (non-Javadoc)
     * @see de.tarent.contact.db.PersistentEntity#serialize()
     */
    public Object serialize() {
        // TODO Auto-generated method stub
        return null;
    }



    /* (non-Javadoc)
     * @see de.tarent.contact.db.PersistentEntity#getState()
     */
    public int getState() {
        // TODO Auto-generated method stub
        return 0;
    }
}
