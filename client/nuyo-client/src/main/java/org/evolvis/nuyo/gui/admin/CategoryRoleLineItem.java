package org.evolvis.nuyo.gui.admin;

import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.Role;
import org.evolvis.nuyo.db.octopus.ObjectRoleImpl;

/**
 * A wrapper object for a category role in order to represent it in a table.
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
class CategoryRoleLineItem implements RolesTableLineItemVO {
    private ObjectRole role;
    private boolean isPersistent;
    
    /** Creates a not persistent category role and a table line for it.*/
    CategoryRoleLineItem(){
        role = new ObjectRoleImpl();
    }
    
    /** Creates a table line for a given persistent category role.*/
    CategoryRoleLineItem(ObjectRole r){
        role = r;
        isPersistent = true;
    }

    public Object getValueAtColumn( int index ) {
        switch ( index ) {
            case 0: return role.getRoleName();
            case 1: return role.hasRight(ObjectRole.KEY_READ);
            case 2: return role.hasRight(ObjectRole.KEY_EDIT);
            case 3: return role.hasRight(ObjectRole.KEY_ADD);
            case 4: return role.hasRight(ObjectRole.KEY_REMOVE);
            case 5: return role.hasRight(ObjectRole.KEY_STRUCTURE);
            case 6: return role.hasRight(ObjectRole.KEY_ADDSUB);
            case 7: return role.hasRight(ObjectRole.KEY_REMOVESUB);
            case 8: return role.hasRight(ObjectRole.KEY_GRANT);
            default: throw new IllegalArgumentException("column index should be between 0 and 8: " + index);
        }
    }

    public void setValueAtColumn( Object o, int index ) {
        switch ( index ) {
            case 0: role.setRoleName((String) o); break;
            case 1: role.setRight(ObjectRole.KEY_READ, (Boolean) o); break;
            case 2: role.setRight(ObjectRole.KEY_EDIT, (Boolean) o); break;
            case 3: role.setRight(ObjectRole.KEY_ADD, (Boolean) o); break;
            case 4: role.setRight(ObjectRole.KEY_REMOVE, (Boolean) o); break;
            case 5: role.setRight(ObjectRole.KEY_STRUCTURE, (Boolean) o); break;
            case 6: role.setRight(ObjectRole.KEY_ADDSUB, (Boolean) o); break;
            case 7: role.setRight(ObjectRole.KEY_REMOVESUB, (Boolean) o); break;
            case 8: role.setRight(ObjectRole.KEY_GRANT, (Boolean) o); break;
            default: throw new IllegalArgumentException("column index should be between 0 and 8: " + index);
        }
    }

    public boolean isPersistent() {
        return isPersistent;
    }

    public void setPersistent( boolean value ) {
        isPersistent = value;
    }
    
    public Role getRole(){
        return role;
    }
}
