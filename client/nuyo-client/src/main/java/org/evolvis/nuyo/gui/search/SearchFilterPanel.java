package org.evolvis.nuyo.gui.search;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.evolvis.nuyo.groupware.AddressProperty;
import org.evolvis.nuyo.gui.Messages;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import de.tarent.commons.ui.swing.ComboBoxMouseWheelNavigator;

/**
 * A panel used for a filter definition through the user interaction.
 * It consists of 1 to n conditions represented by the rows.
 * All user interactions will be delegated to a {@link FilterModel} instance.
 * A model anylizes, validates and then delegates through 
 * the {@link FilterModelListener} interface the changes back to the panel.
 * In this manner the filter model can react and handle not only the user changes, 
 * but also the changes, that occures programmatically through the public filter methods.<p>
 * 
 * @see org.evolvis.nuyo.gui.search.FilterModel
 * @see org.evolvis.nuyo.gui.search.FilterModelListener
 * 
 * @author Aleksej Palij (a.palij@tarent.de), tarent GmbH Bonn
 */
public class SearchFilterPanel extends JPanel implements FilterModelListener {
    
    private static final Logger LOGGER = Logger.getLogger(SearchFilterPanel.class.getName());
    
    /* a row specification for the creation of new rows */
    private static final RowSpec ROW_SPEC = new RowSpec("fill:pref");
    
    /* condition key will be used to retrieve the conditions assigned to row components as client property*/
    private static final String CONDITION_KEY = "condition";
    private FormLayout layout;
    private CellConstraints cc;
    
    /* a map for quick remove and data retrieving actions:
     * key:=SearchCondition
     * value:=JComponents[]
     */
    private Map rows;
    
    /* constants to retrieve components from any row content (represented by JComponents[])*/
    private static final int ATTRIBUTE_BOX = 0;
    private static final int OPERATOR_BOX = 1;
    private static final int INPUT_FIELD = 2;
    private static final int PLUS_BUTTON = 3;
    private static final int MINUS_BUTTON = 4;
    
    /* a model to delegate the user interactions */
    private FilterModel model;
    
    /**
     * Creates an instance of the <code>SearchFilterPanel</code> properly
     * configured with the address properties destined for the search dialog.
     */
    public static SearchFilterPanel createSearchDialogPanel()
    {
      return new SearchFilterPanel(FilterModel.createSearchPropertyFilterModel());
    }

    /** 
     * Creates an instance of panel and loads all existing conditions from the given model.<p>
     * 
     * @param aModel to delegate the user interactions.
     */
    SearchFilterPanel( FilterModel aModel ) {
        model = aModel;
        model.setFilterModelListener(this);
       // setBorder(Borders.DIALOG_BORDER);
        setBorder(BorderFactory.createEmptyBorder());
        initLayout();
        rows = new HashMap();
        //load all conditions
        fullUpdatePerformed();
    }

    private void initLayout() {
        //define columns only (the rows will be added later)
        layout = new FormLayout("p, 3dlu, p, 3dlu, pref:grow, 3dlu, 25dlu, 3dlu, 25dlu");
        //init column constraints
        cc = new CellConstraints();
        //combo boxes should have the same width
        layout.setColumnGroups(new int[][]{{1, 3}});
        //
        setLayout(layout);
    }
    
    /**
     * Clears the panel and loads all current conditions from the model.<p>
     */
    public void fullUpdatePerformed() {
        clear();
        if(layout.getRowCount() > 0) initLayout();//reset layout
        Iterator conditions = model.getConditions().iterator();
        while(conditions.hasNext()){
            appended((SearchCondition) conditions.next());
        }
        revalidate();
        repaint();
    }


    /**
     * Removes a row, which is assigned to a given condition.<p>
     * 
     * @param atPosition - order position of a given condition 
     * @param condition was deleted 
     */
    public void removed( int atPosition, SearchCondition condition ) {
        int rowToRemove = getRowFor(atPosition);
        LOGGER.fine("[removing] row: " + rowToRemove);
        //there are two constraints to invoke removeRow method (see API):
        //1. the row must not contain components and 
        //2. must not be part of a row group
        //--- #1 deleting row components:
        JComponent[] rowContent = (JComponent[]) rows.get(condition);
        for(int i = rowContent.length - 1; i >= 0 ; i--) {
            layout.removeLayoutComponent(rowContent[i]);
            remove(rowContent[i]);
        }
        //--- #2 the rows are not grouped
        //--- deleting the current row
        layout.removeRow(rowToRemove);
        //--- deleting the row gap
        layout.removeRow(rowToRemove);
        revalidate();
        repaint();
    }

    private void clear() {
        removeAll();
        rows.clear();
    }

    /**
     * Inserts a new row for a given condition.<p>
     * 
     * @param sequenceIndex - order position of a given condition 
     * @param newCondition to insert 
     */
    public void inserted( int sequenceIndex, SearchCondition newCondition ) {
        LOGGER.fine("[insert] row: " + getRowFor(sequenceIndex));
        layout.insertRow( getRowFor(sequenceIndex), ROW_SPEC);//"fill:pref"
        addRowContent( getRowFor( sequenceIndex ), createRowContent( newCondition ));        
        layout.insertRow( getRowFor(sequenceIndex) + 1, FormFactory.RELATED_GAP_ROWSPEC);
        revalidate();
    }

    /* <code>panel_row := sequence_position * 2 + 1</code> (because of row gaps).*/ 
    private int getRowFor( int sequenceIndex ) {
        return sequenceIndex * 2 + 1;
    }

    /**
     * Appends a new row, that represents a given condition.<p>
     * 
     * @param newCondition to append
     */
    public void appended( SearchCondition newCondition ) {
        LOGGER.fine("[appending] row: " + (layout.getRowCount() + 1));
        layout.appendRow(ROW_SPEC);//"fill:pref"
        addRowContent( layout.getRowCount(), createRowContent( newCondition ));
        layout.appendRow(FormFactory.RELATED_GAP_ROWSPEC);
        revalidate();
    }

    private JComponent[] createRowContent( SearchCondition newCondition ) {
        JComponent[] newRow = new JComponent[5];
        newRow[ATTRIBUTE_BOX] = createAttributeBox(newCondition);
        newRow[OPERATOR_BOX] = createOperatorBox(newCondition);
        newRow[INPUT_FIELD] = createInputField(newCondition);
        newRow[PLUS_BUTTON] = createPlusButton(newCondition);
        newRow[MINUS_BUTTON] = createMinusButton(newCondition);
        //
        newRow[MINUS_BUTTON].setVisible(newCondition.isRemovable());
        //
        rows.put(newCondition, newRow);
        return newRow;
    }

    private void addRowContent( int rowToAdd, JComponent[] newRow ) {
        LOGGER.fine("[adding-row-content]: " + rowToAdd);
        add(newRow[ATTRIBUTE_BOX], cc.xy(1,rowToAdd));
        add(newRow[OPERATOR_BOX], cc.xy(3,rowToAdd));
        add(newRow[INPUT_FIELD], cc.xy(5,rowToAdd));
        add(newRow[PLUS_BUTTON], cc.xy(7,rowToAdd));
        add(newRow[MINUS_BUTTON], cc.xy(9,rowToAdd));
    }
    
    private JComponent createOperatorBox( SearchCondition newCondition ) {
        JComboBox box = new JComboBox(model.getOperatorsForType(newCondition.getAttribute().getDataType()));
        box.setSelectedItem(newCondition.getOperator());
        box.putClientProperty(CONDITION_KEY, newCondition);
        box.addItemListener(new ItemListener(){
            public void itemStateChanged( ItemEvent e ) {
                JComboBox box = (JComboBox) e.getSource();
                if(e.getStateChange() == ItemEvent.SELECTED){
                    model.updateOperator((LabeledFilterOperator) e.getItem(), (SearchCondition) box.getClientProperty(CONDITION_KEY));
                } else if (e.getStateChange() == ItemEvent.DESELECTED){
                }
            }
        });
        // add the MouseWheelNavigator
        box.addMouseWheelListener(new ComboBoxMouseWheelNavigator(box));
        return box;
    }


    private JComboBox createAttributeBox( SearchCondition newCondition ) {
        JComboBox box = new JComboBox(model.getAttributes());
        // Sets a different renderer which is suitable for the objects in the model
        // (= AddressProperty) and displays there label instead of their key.
        box.setRenderer(model.getAttributeRenderer());
        
        box.setSelectedItem(newCondition.getAttribute());
        box.putClientProperty(CONDITION_KEY, newCondition);
        box.addItemListener(new ItemListener(){
            public void itemStateChanged( ItemEvent e ) {
                JComboBox box = (JComboBox) e.getSource();
                if(e.getStateChange() == ItemEvent.SELECTED){
                    model.updateAttribute((AddressProperty) e.getItem(), (SearchCondition) box.getClientProperty(CONDITION_KEY));
                } else if (e.getStateChange() == ItemEvent.DESELECTED){
                }
            }
        });
        // add the MouseWheelNavigator
        box.addMouseWheelListener(new ComboBoxMouseWheelNavigator(box));
        return box;
    }


    private JTextField createInputField(SearchCondition newCondition) {
        JTextField field = new JTextField();
        field.setText(newCondition.getParamText());
        field.putClientProperty(CONDITION_KEY, newCondition);
        field.addFocusListener(new FocusListener(){
            public void focusGained( FocusEvent e ) {
                ((JTextField) e.getSource()).selectAll();
            }
            public void focusLost( FocusEvent e ) {
            }
        });
        return field;
    }


    private JButton createMinusButton( SearchCondition newCondition ) {
        JButton b = new JButton("-");
        b.setToolTipText(Messages.getString("GUI_SearchFilterPanel_Minus_ToolTip"));
        b.putClientProperty(CONDITION_KEY, newCondition);
        b.addActionListener(new ActionListener(){
            public void actionPerformed( ActionEvent e ) {
                JButton b = (JButton) e.getSource();
                model.removeCondition((SearchCondition) b.getClientProperty(CONDITION_KEY));
            }
        });
        return b;
    }

    private JButton createPlusButton(SearchCondition newCondition) {
        JButton b = new JButton("+");
        b.setToolTipText(Messages.getString("GUI_SearchFilterPanel_Plus_ToolTip"));
        b.putClientProperty(CONDITION_KEY, newCondition);
        b.addActionListener(new ActionListener(){
            public void actionPerformed( ActionEvent e ) {
                JButton b = (JButton) e.getSource();
                SearchCondition conditionToAddAfter = (SearchCondition) b.getClientProperty(CONDITION_KEY); 
                JTextField field = (JTextField) ((JComponent[]) rows.get(conditionToAddAfter))[INPUT_FIELD];
                model.changeParam(field.getText(), (SearchCondition) field.getClientProperty(CONDITION_KEY));
                model.addCondition(conditionToAddAfter);
            }
        });
        return b;
    }

    
    /**
     * Sets '-' button visible/unvisible if a given condition is set removable/unremovable.
     * It will be invoked by a model if required, i.e. to keep the last condition remaining 
     * or make it removable if more conditions present.<p>
     */
    public void setRemovablePerformed( SearchCondition condition ) {
        ((JComponent[]) rows.get(condition))[MINUS_BUTTON].setVisible(condition.isRemovable());
        revalidate();
    }


    /** 
     * Sets the attribute of a given condition selected in a {@link JComboBox}, 
     * which is assigned to this condition. It will be invoked by a model, 
     * after a newly attribute change/selection has been validated.<p>
     */
    public void attributeChanged( SearchCondition condition ) {
        JComponent[] rowContent = (JComponent[]) rows.get(condition);
        JComboBox attributes = (JComboBox) rowContent[ATTRIBUTE_BOX];
        AddressProperty newAttribute = condition.getAttribute();
        if(attributes.getSelectedItem().equals(newAttribute)) return;
        attributes.setSelectedItem(newAttribute);
    }


    /**
     * Sets a given set (represented by an array) of operators to a {@link JComboBox}, 
     * which is assigned to a given condition. It will be invoked by a model 
     * if the newly selected property (or attribute) requires another operators set.<p>
     */
    public void operatorsChanged( LabeledFilterOperator[] operators, SearchCondition condition ) {
        JComponent[] rowContent = (JComponent[]) rows.get(condition);
        JComboBox operatorsBox = (JComboBox) rowContent[OPERATOR_BOX];
        operatorsBox.setModel(new DefaultComboBoxModel(operators));
    }

    /** 
     * Sets the operator of a given condition selected in a {@link JComboBox}, which is assigned to this condition.<p>
     * It will be invoked by a model after the new selection has been handled.<p>
     */
    public void operatorChanged( SearchCondition condition ) {
        JComponent[] rowContent = (JComponent[]) rows.get(condition);
        JComboBox operatorsBox = (JComboBox) rowContent[OPERATOR_BOX];
        operatorsBox.setSelectedItem(condition.getOperator());
    }

    /**
     * Sets the parameter text of a given condition in {@link JTextField}, which is assigned to this condition.<p>
     * It can be invoked by a model in two cases:<p>
     * a) if changes occured to model programmatically through the public API or<p>
     * b) if the user presses '+' Button:<p>
     *  the changes will be delegated the model,<p>
     *  the model anylizes this changes and then<p> 
     *  delegates validated changes back to the same {@link JTextField}.<p>
     * <p> 
     * That is the model and GUI data synchronisation occurs efficiently (on demand only).
     */
    public void paramChanged( SearchCondition condition ) {
        JComponent[] rowContent = (JComponent[]) rows.get(condition);
        JTextField field = (JTextField) rowContent[INPUT_FIELD];
        field.setText(condition.getParamText());
    }


    /**
     * Notifies a model about the parameter changes.<p> 
     * Will be invoked by the model in order to synchronize the parameter values
     * with text field text.<p>
     */
    public void pickParamsPerformed() {
        Iterator conditions = rows.keySet().iterator();
        while(conditions.hasNext()){
            SearchCondition nextCondition = (SearchCondition) conditions.next();
            JComponent[] rowContent = (JComponent[]) rows.get(nextCondition);
            String newText = ((JTextField) rowContent[INPUT_FIELD]).getText();
            if(newText.equals(nextCondition.getParamText())) continue;
            model.updateParamOnly(newText, nextCondition);
        }
    }

    /**
     * Sets the param field of all search conditions to the empty string
     * and conveys this update to the model.
     * 
     * <p>This method allows outside instance to quickly remove user-given
     * data.</p>
     */
    public void clearParams() {
        Iterator conditions = rows.keySet().iterator();
        while(conditions.hasNext()){
            SearchCondition nextCondition = (SearchCondition) conditions.next();
            JComponent[] rowContent = (JComponent[]) rows.get(nextCondition);
            ((JTextField) rowContent[INPUT_FIELD]).setText("");
            
            if("".equals(nextCondition.getParamText())) continue;
              model.updateParamOnly("", nextCondition);
        }
    }
    
    /**
     * Returns a snapshot of the search filter's state.
     * 
     * @return
     */
    public SearchFilterState getState()
    {
      model.removeDuplicates();
      
      return new SearchFilterState(model.getConditions());
    }
    
    /**
     * Sets the given {@link SearchFilterState} instance
     * and lets the <code>SearchFilterPanel</code> and
     * its model reinitialize with that.
     * 
     * <p>In other words this can be used to restore the
     * panel's state.</p>
     * 
     * @param s
     */
    public void setState(SearchFilterState s)
    {
      model.removeAllConditions();
      
      Iterator ite = s.conditions.iterator();
      
      while (ite.hasNext())
        model.addCondition((SearchCondition) ite.next());
    }
    
}
