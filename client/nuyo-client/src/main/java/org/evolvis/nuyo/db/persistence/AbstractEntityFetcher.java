package org.evolvis.nuyo.db.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.cache.EntityCache;
import org.evolvis.nuyo.db.filter.ISelection;
import org.evolvis.nuyo.remote.Method;

import de.tarent.octopus.client.OctopusResult;


/**
 * @author kleinw
 *
 *	Hier wird der Vorgang des Holens von Daten vom Server gekapselt.
 *	Alle Objekttypen k�nnen diese Klasse verwenden, wie die Objekte
 *	anhand der Listeninhalte gef�llt werden, muss allerdings der
 *	konkrete Typ entscheiden (populate).
 *
 *
 *	Hier kann f�r jeden Instanztyp (Appointment, Calendar, User, etc.),
 *	ob das Ergebnis als konkatenierter String geholt werden soll, wodurch
 *	sich die Soapresponse erheblich verkleinert.
 *	In der betreffenden Instanz wird dann das Objekt in der populate(ResponseData)
 *	gef�llt, unabh�ngig davon, ob das Ergebnis als String zur�ck kam oder nicht.
 *
 *
 *	Zus�tzlich kann bei Bedarf die zur Selection passende Collection
 *	von Instanzen in einen Cache geschrieben werden.
 *
 */
abstract public class AbstractEntityFetcher {

    /**	Soapmethode f�r den Soapzugriff */
    private String _method;
    /**	Umwandlung von Collections in Strings mit Trennzeichen */
    private boolean _singleString;
    
    
    /**	Abh�ngig nach Filtertyp werden hier gewisse Instanzen gehalten */
    private EntityCache _cache = new EntityCache();
    
    
    /**	Name der Soapmethode wird ben�tigt.@param methodName - Name der Methode@param singleString - Das Ergebnis wird komprimiert zur�ckgegeben */
    public AbstractEntityFetcher(String methodName, boolean singleString) {_method = methodName;_singleString = singleString;}
    
    
    /**	Hier wird gem�� Filter eine Liste von Objekten geholt und die entsprechenden Instanztypen gef�llt. @param filter - der Filter */
    public Collection getEntities(ISelection selection) throws ContactDBException {
    	return getEntities( selection, true);    	
    }
    
    public Collection getEntities(ISelection selection, boolean enableCaching) throws ContactDBException {
    	Collection lookUp = null;
    	if (enableCaching){
    		lookUp = _cache.get(selection);
    	}
        if(lookUp != null)
            return lookUp;
        List entities = new ArrayList();
        Method m = new Method(_method);
        List tmp = null;
        if(selection != null)
            m.addFilter(selection);
        if(_singleString) {
            tmp = new ArrayList();
            m.add("singleStringResult", new Boolean(true));
            String singleString = (String)m.invoke();
            for(StringTokenizer st1 = new StringTokenizer(singleString, "^");st1.hasMoreTokens();) {
                if(st1.hasMoreTokens()) {
	                String item = st1.nextToken();
	                List listItem = new ArrayList();
	                for(StringTokenizer st2 = new StringTokenizer(item, "#");st2.hasMoreTokens();) {
	                    listItem.add(st2.nextToken());
	                }
	                tmp.add(listItem);
                }
            }
        } else{
            tmp = (List)m.invoke();
        }
        if(tmp != null && tmp.size() > 0) {
            if(tmp.get(0) instanceof Object[]) {
                throw new ContactDBException(ContactDBException.EX_INSUFFICIENT_SERVER_DATA, 
                "Die vom Server gelieferten Daten sind nicht wie erwartet. Es sollten keine Object[] zur�ckgegeben werden.");
//                for(Iterator it=tmp.iterator();it.hasNext();) {
//                    Object[] entry = (Object[])it.next();
//                    IEntity entity = populate(new ResponseData(_method, Arrays.asList(entry), m.getORes()));
//                    entity.setTransient(false);
//                    entity.setDirty(false);
//                    entity.setInvalid(false);
//                    entities.add(entity);
//                }
            } else if(tmp.get(0) instanceof List) {
                for(Iterator it=tmp.iterator();it.hasNext();) {
                    IEntity entity = populate(new ResponseData(_method, (List)it.next(), m.getORes()));
                    entity.setTransient(false);
                    entity.setDirty(false);
                    entity.setInvalid(false);
                    entities.add(entity);
                }
            } else if(tmp.get(0) instanceof Map){
				for(Iterator it = tmp.iterator(); it.hasNext();){
					IEntity entity = populate(new ResponseData(_method, (Map)it.next(), m.getORes(), m.getUsedContent()));
					entity.setTransient(false);
                    entity.setDirty(false);
                    entity.setInvalid(false);
                    entities.add(entity);
				}
            }
            else{
				throw new ContactDBException(ContactDBException.EX_INSUFFICIENT_SERVER_DATA, 
                "Die vom Server gelieferten Daten sind nicht wie erwartet. Es wurde ein Object[] erwartet.");
				
            }
        }
        _cache.store(selection, entities);
        return entities;
    }	


    public EntityCache getCache() {return _cache;}
    
    
    /**	Wie ein einzelner Eintrag der Liste in eine Instanz zu wandeln ist, muss die entsprechende Instanz selbst entscheiden. @param values - Datenliste vom Server */
    abstract public IEntity populate(ResponseData response) throws ContactDBException;
    
    
    /**	Elemente dieser Liste k�nnen direkt gecastet bezogen werden. Dabei kann der abgefragte Wert auch als String vorliegen. */
    final static public class ResponseData {
        
        private String _method;
		private String _usedContent;
		private OctopusResult _oRes;
		private Map content = new HashMap();
		private int counter = 0;
        
        /**	Leerer Konstruktor */
        public ResponseData() {}
        
        /**	Hier kann eine normale Liste als Ausgangspunkt genommen werden */
        public ResponseData(Collection c) {
			Iterator it = c.iterator();
			for(int i=0;i<c.size()&&it.hasNext();i++){
				Object o = it.next();
				content.put(new Integer(i), o);
				counter++;
			}
		}
		
		public ResponseData(Map map){
			content.putAll(map);
			counter += map.size();
		}
        
        /**	F�r bessere Fehlerdiagnose gibt es f�r den AbstractFetcher noch die M�glichkeit, den Namen der Methode zu �bergeben */
        public ResponseData(String method, Collection c, OctopusResult oRes) {this(c);_method = method;_oRes = oRes;}
        
        
		public ResponseData(String method, Map map, OctopusResult oRes, String used){
			this(map);
			_method = method;
			_oRes = oRes;
			_usedContent = used;
		}
		
		/**	Hier kann direkt ein Objectarray �bergeben werden */
        public ResponseData(Object[] o) {
            for(int i=1;i<=o.length;i++) {
                content.put(new Integer(i-1),o[i-1]);
				counter++;
            }
        }
		
		public Object get(String key){
			return content.get(key);
		}
        
        /**	Holt Objekt gecastet aus der Liste */
        public Object get(int index, Class type) throws ContactDBException {
        	if(index - 1 > content.size()) throw new ContactDBException(ContactDBException.EX_INSUFFICIENT_SERVER_DATA, "Die vom Server gelieferten Daten sind nicht erwartungsgem��.");
             try {
                Object tmp = content.get(new Integer(index));
                if(tmp instanceof String) {
                    if(type == Integer.class) {
                        return new Integer((String)tmp);
                    } else if(type == Long.class) {
                        return new Long((String)tmp);
                    } else if(type == Date.class) {
                        Object tmpDate = content.get(new Integer(index));
                        if(tmpDate != null && tmpDate instanceof Long) 
                            return new Date(((Long)tmpDate).longValue());
                        else
                            return tmp;
                    } else
                        return tmp;
                } else
                    return tmp;
            } catch (NumberFormatException e) {
                throw new ContactDBException(ContactDBException.EX_INSUFFICIENT_SERVER_DATA, "Fehler bei der Auswertung Server-Methode '" + _method + "' : Datenfeld [" + index + "] konnte nicht in eine Zahl gewandelt werden.");
            }
        }
        
        /**	Holt Objekt als Integer */
        public Integer getInteger(int index) throws ContactDBException {
            return (Integer)get(index, Integer.class);
        } 

        public Integer getInteger(String key) throws ContactDBException {
            Object o = get(key);
            if (o == null)
                return null;
            else if (o instanceof Integer)
                return (Integer)o;
            else
                return new Integer(""+o);            
                
        } 
        
        public String getString(String key) throws ContactDBException {
            Object o = get(key);
            if (o == null)
                return null;
            else if (o instanceof String)
                return (String)o;
            else
                return new String(""+o);            
                
        } 

        /**	Holt Objekt als Long */
        public Long getLong(int index) throws ContactDBException{
            return (Long)get(index, Long.class);
        }

        /**	Holt Objekt als String */
        public String getString(int index) throws ContactDBException{
            Object o = get(index, String.class);
            if (o == null)
                return null;
            else if (o instanceof String)
                return (String)o;
            else
                return new String(""+o);            
        } 
       
        /**	Holt Objekt als Date */
        public Date getDate(int index) throws ContactDBException{
            Object tmp = get(index, Date.class);
            if(tmp != null) {
                if(tmp instanceof Long) {
                    Date date = new Date();
                    date.setTime(((Long)tmp).longValue());
                    return date;
                } else if (tmp instanceof String) {
                    try {
	                    Long l = new Long((String)tmp);
	                    Date d = new Date(l.longValue());
	                    return d;
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    //	TODO Date noch einf�gen
                } else
                    return null;
            } else 
                return null;
       }
        
        /** Holt Objekt als Boolean */
        public Boolean getBoolean(int index) throws ContactDBException{
            return toBoolean(get(index));
        }		

        /** Holt Objekt als Boolean */
        public Boolean getBoolean(String key) throws ContactDBException{
            return toBoolean(get(key));
        }

        protected Boolean toBoolean(Object o) {
            if (o instanceof Integer) {
                return new Boolean( (((Integer)o).intValue() == 1)  );
            } 
            else if (o instanceof Boolean) {
                return (Boolean)o;
            } 
            else if (o instanceof String && "1".equals(o)) {
                return Boolean.TRUE;
            } 
            return new Boolean(""+o);            
        }

        

		public Object get(int index){
			return content.get(new Integer(index));
		}
		
		public void add(Object o){
			content.put(new Integer(++counter), o);
		}
		
		public OctopusResult getORes(){
			return _oRes;
		}
		
		public Map getContent(){
			return content;
		}
		
		public String getUsedContent(){
			return _usedContent;
		}

       //	TODO weitere Datentypen
        
    }
    
}