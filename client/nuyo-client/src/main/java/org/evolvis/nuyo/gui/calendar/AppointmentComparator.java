/*
 * Created on 15.06.2004
 *
 */
package org.evolvis.nuyo.gui.calendar;
import java.util.Comparator;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author niko
 *
 */
public class AppointmentComparator implements Comparator
{
  public final static Object FIELD_TYPE = "FIELD_TYPE";
  public final static Object FIELD_DATE = "FIELD_TYPE";
  public final static Object FIELD_NAME = "FIELD_NAME";

  
  
  
  private Object m_oFieldKey = FIELD_DATE;
  private int m_iEqual = 0; 
  private int m_iLess = -1; 
  private int m_iMore = 1; 
  
  public void setCompareField(Object field)
  {      
    m_oFieldKey = field;
  }
  
  public void setReverse(boolean reverse)
  {      
    if (reverse)
    {
      m_iLess = 1; 
      m_iMore = -1;         
    }
    else
    {
      m_iLess = -1; 
      m_iMore = 1;         
    }
  }
  
  public int compare(Object o1, Object o2)
  {
    Appointment appointment1 = (Appointment)o1; 
    Appointment appointment2 = (Appointment)o2; 
    
    try
    {
      if (FIELD_DATE.equals(m_oFieldKey))
      {          
        if (appointment1.getStart().equals(appointment2.getStart())) return m_iEqual;
        else if (appointment1.getStart().before(appointment2.getStart())) return m_iLess;
        else return m_iMore;
      }
      else if (FIELD_NAME.equals(m_oFieldKey))
      {          
        String subject1 = appointment1.getAttribute(Appointment.KEY_SUBJECT);
        String subject2 = appointment2.getAttribute(Appointment.KEY_SUBJECT);          
        int cmp = (subject1.compareToIgnoreCase(subject2));
        if (cmp == 0) return m_iEqual;          
        else if (cmp < 0) return m_iLess;
        else return m_iMore;
      }
      else if (FIELD_TYPE.equals(m_oFieldKey))
      {          
        int category1 = appointment1.getAppointmentCategory();
        int category2 = appointment2.getAppointmentCategory();
        
        if (category1 == category2) return m_iEqual;          
        else if (category1 < category2) return m_iLess;
        else return m_iMore;
      }
      
      
    } catch (ContactDBException e) {}
    return 0;
  }
}

