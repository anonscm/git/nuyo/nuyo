/* $Id: DefaultFileFilter.java,v 1.4 2006/06/06 14:12:11 nils Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Jens Neumaier. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.plugins.dataio;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.filechooser.FileFilter;

/**
 * This is a simple FileFilter-Implemenation.
 * 
 * A DefaultFileFilter can have a default file extension (for saving files).
 * 
 * @author Jens Neumaier, tarent GmbH
 * @see javax.swing.filechooser.FileFilter
 *
 */
public class DefaultFileFilter extends FileFilter {

    private Hashtable filters = null;
    private String defaultFilter = null;
    private String description = null;
    private String fullDescription = null;
    
    public DefaultFileFilter() {
        this.filters = new Hashtable();
    }
    
    public DefaultFileFilter(String extension) {
        this(extension, null);
    }
    
    public DefaultFileFilter(String extension, String description) {
        this();
        if (extension!=null) addExtension(extension);
        if (description!=null) setDescription(description);
    }
    
    /**
     * @see DefaultFileFilter#DefaultFileFilter(String[], String) 
     * @param filters Supported file extensions
     */
    public DefaultFileFilter(String[] filters) {
        this(filters, null);
    }
    
    /**
     * If more than one extension is supported the first extension
     * will be used when saving a file if the user gives no explicit
     * extension by choosing or specifying a file with extension.
     *  
     * @see DefaultFileFilter#getDefaultExtension()
     * @see DefaultFileFilter#setDefaultExtension(String)
     * @param filters supported file extensions
     * @param description description for this FileFilter
     */
    public DefaultFileFilter(String[] filters, String description) {
        this();
        for (int i = 0; i < filters.length; i++) {
            addExtension(filters[i]);
        }
        if(description!=null) setDescription(description);
    }
    
    public boolean accept(File f) {
        if(f != null) {
            if (f.isDirectory())
                return true;
            
            String extension = getExtension(f);

            if (extension != null && filters.get(getExtension(f)) != null)
                return true;
        }
        return false;
    }

    public String getExtension(File f) {
        if(f != null) {
            String filename = f.getName();
            int i = filename.lastIndexOf('.');
            if(i>0 && i<filename.length()-1)
                return filename.substring(i+1).toLowerCase();            
        }
        return null;
    }
    
    /**
     * Returns the default extension for saving files.
     * If no extension was defined as default using
     * setDefaultExtension(String) the first extension
     * specified in this FileFilter will be used.
     * 
     * @see DefaultFileFilter#DefaultFileFilter(String[], String)
     * @see DefaultFileFilter#setDefaultExtension(String)
     * @return default extension
     */
    public String getDefaultExtension() {
        return defaultFilter;
    }
    
    /**
     * Sets the default extension for saving files.
     * 
     * @param extension extension to set
     * @see #getDefaultExtension()
     */
    public void setDefaultExtension(String extension) {
        defaultFilter = extension;
    }
    
    public void addExtension(String extension) {
        if (filters.isEmpty())
            defaultFilter = extension;
        
        filters.put(extension.toLowerCase(), this);
        fullDescription = null;
    }
    
    /**
     * Returns the human readable description of this filter. For
     * example: "JPEG and GIF Image Files (*.jpg, *.gif)"
     */
    public String getDescription() {
        if(fullDescription == null) {
            fullDescription = description==null ? "(" : description + " (";
            // build the description from the extension list
            Enumeration extensions = filters.keys();
            if(extensions != null) {
                fullDescription += "." + (String) extensions.nextElement();
                while (extensions.hasMoreElements()) {
                    fullDescription += ", ." + (String) extensions.nextElement();
                }
            }
            fullDescription += ")";            
        }
        return fullDescription;
    }
    
    /**
     * Sets the description of this FileFilter.
     * 
     * @param description descrition to set
     */
    public void setDescription(String description) {
        this.description = description;
        fullDescription = null;
    }
    
    /**
     * Returns the original description of this FileFilter.
     * Original means no extra information for a FileChooser
     * is appended to the description.
     * 
     * @return original description
     * @see #getDescription()
     */
    public String getRawDescription() {
       return description;
    }
}
