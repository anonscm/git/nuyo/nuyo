/*
 * Created on 22.09.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

/**
 * @author niko
 *
 */
public class ObjectParameter
{
  private String m_sKey;
  private String m_sValue;
  
  public ObjectParameter(String key, String value)
  {
    m_sKey = key;
    m_sValue = value;
  }
  
  public String getKey()
  {
    return m_sKey;
  }

  public String getValue()
  {
    return m_sValue;
  }
  
  public ObjectParameter cloneObjectParameter()
  {
    return(new ObjectParameter(m_sKey, m_sValue));
  }
}
