/* $Id: AdminPanel.java,v 1.54 2007/08/30 16:10:24 fkoester Exp $
 tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 Copyright (C) 2002 tarent GmbH
 
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 
 tarent GmbH., hereby disclaims all copyright
 interest in the program 'tarent-contact'
 (which makes passes at compilers) written
 by Michael Klink.
 signature of Elmar Geese, 1 June 2002
 Elmar Geese, CEO tarent GmbH*/ 

package org.evolvis.nuyo.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.evolvis.nuyo.config.StartUpConstants;
import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.controller.ControlListener;
import org.evolvis.nuyo.controller.event.MasterDataListener;
import org.evolvis.nuyo.controls.ExtendedDataContainer;
import org.evolvis.nuyo.controls.ExtendedDataItemModel;
import org.evolvis.nuyo.controls.ExtendedDataLabeledComponent;
import org.evolvis.nuyo.controls.TarentWidgetDateSpinner;
import org.evolvis.nuyo.db.Address;
import org.evolvis.nuyo.db.AddressClientDAO;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.CalendarSecretaryRelation;
import org.evolvis.nuyo.db.Category;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.GlobalRole;
import org.evolvis.nuyo.db.GroupObjectRoleConnector;
import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.SubCategory;
import org.evolvis.nuyo.db.User;
import org.evolvis.nuyo.db.UserGroup;
import org.evolvis.nuyo.db.octopus.CalendarSecretaryRelationImpl;
import org.evolvis.nuyo.db.octopus.GlobalRoleBean;
import org.evolvis.nuyo.db.octopus.GlobalRoleImpl;
import org.evolvis.nuyo.db.octopus.GroupObjectRoleImpl;
import org.evolvis.nuyo.db.octopus.ObjectRoleImpl;
import org.evolvis.nuyo.db.octopus.UserGroupBean;
import org.evolvis.nuyo.db.persistence.IEntity;
import org.evolvis.nuyo.db.persistence.PersistenceManager;
import org.evolvis.nuyo.db.veto.VetoableAction;
import org.evolvis.nuyo.gui.admin.CategoryRolesListener;
import org.evolvis.nuyo.gui.admin.CategoryRolesManagement;
import org.evolvis.nuyo.gui.admin.RoleComparator;
import org.evolvis.nuyo.gui.calendar.ScheduleDate;
import org.evolvis.nuyo.logging.TarentLogger;
import org.evolvis.nuyo.util.DateRange;
import org.evolvis.xana.config.Appearance;
import org.evolvis.xana.config.ConfigManager;
import org.evolvis.xana.config.Appearance.Key;

import de.tarent.commons.ui.swing.TabbedPaneMouseWheelNavigator;

/**
 * A panel for administration.
 * 
 * @author niko, mikel
 */
public class AdminPanel extends JPanel implements MasterDataListener, CategoryRolesListener
{
  private static final TarentLogger logger = new TarentLogger(AdminPanel.class);
  
  // the max length of fields.
  // note: '-1' means no length restriction.
  private final int m_iMaxLength_CreateCategory_Key = 50;
  private final int m_iMaxLength_CreateCategory_Name = 100;
  private final int m_iMaxLength_CreateCategory_Description = 250;

  private final int m_iMaxLength_CreateVerteiler_Key = 50;
  private final int m_iMaxLength_CreateVerteiler_Name1 = 50;
  private final int m_iMaxLength_CreateVerteiler_Name2 = 50;
  private final int m_iMaxLength_CreateVerteiler_Name3 = 50;

  private final int m_iMaxLength_EditVerteiler_Key = 50;
  private final int m_iMaxLength_EditVerteiler_Name1 = 50;
  private final int m_iMaxLength_EditVerteiler_Name2 = 50;
  private final int m_iMaxLength_EditVerteiler_Name3 = 50;

  private final int m_iMaxLength_EditCategory_Key = 50;
  private final int m_iMaxLength_EditCategory_Name = 100;
  private final int m_iMaxLength_EditCategory_Description = 250;

  private final int m_iMaxLength_CreateUser_UserID = 50;
  private final int m_iMaxLength_CreateUser_UserName = 50;
  private final int m_iMaxLength_CreateUser_UserFamilyName = 50;
  private final int m_iMaxLength_CreateUser_Email = 50;
  private final int m_iMaxLength_CreateUser_Password = 50;
  
  private final int m_iMaxLength_CreateGroup_GroupID = 50;
  private final int m_iMaxLength_CreateGroup_Description = 50;

  private final int m_iMaxLength_EditGroup_GroupID = 50;
  private final int m_iMaxLength_EditGroup_Description = 50;

  private final int m_iMaxLength_RM_filterGruppe = 50;
  private final int m_iMaxLength_RM_filterRolle = 50;
  private final int m_iMaxLength_RM_filterObject = 50;

  private final int m_iMaxLength_grantTo = 50;

  private final int m_iMaxLength_ChangeUser_UserName = 50;
  private final int m_iMaxLength_ChangeUser_UserFamilyName = 50;
  private final int m_iMaxLength_ChangeUser_Email = 50;
  private final int m_iMaxLength_ChangeUser_Password = 50;

  // -----------------------------------------------------------------------------------------
  
    private GUIListener m_oGUIListener;
    private ControlListener m_oControlListener;
    
    private JTextArea m_oLogMessagesTextArea;  
    private JSplitPane m_oSplit;
    
    
    private boolean m_bShowVerteilerKey = false;
    private boolean m_bShowLogPanel = false;
    private String m_sErrorText = "";
    private int m_iLineLength = 50;
    private boolean m_bErrorTextExists = false;
    private boolean m_bCategoryAdded = false;
    private boolean m_bCategoryDeassigned = false;
    private boolean m_bStandardCategoryAssigned = false;
    private boolean m_bSubCategoryAdded = false;
    private boolean m_bSubCategoryRemoved = false;
    private boolean m_bUserAdded = false;
    private boolean m_bUserRemoved = false;
    private boolean m_bUserChanged = false;
    private boolean m_bCategoryRemoved = false;
    private boolean m_bSubCategoryChanged = false;
    private boolean m_bCategoryChanged = false;
    
    private ArrayList m_oShowPanel;
    
    public final static Object CREATECATEGORY = "CREATECATEGORY";
    public final static Object DELETECATEGORY = "DELETECATEGORY";
    public final static Object CREATESUBCATEGORY = "CREATESUBCATEGORY";
    public final static Object DELETESUBCATEGORY = "DELETESUBCATEGORY";
    public final static Object SHOWSUBCATEGORY = "SHOWSUBCATEGORY";
    public final static Object CREATEUSER = "CREATEUSER";
    public final static Object DELETEUSER = "DELETEUSER";
    public final static Object EDITUSER = "EDITUSER";
    public final static Object ASSOCIATEUSER = "ASSOCIATEUSER";
    
    public final static Object CREATEGROUP = "CREATEGROUP";
    public final static Object DELETEGROUP = "DELETEGROUP";
    public final static Object EDITGROUP = "EDITGROUP";
    
    public final static Object GLOBALROLE = "GLOBALROLE";
    public final static Object FOLDERROLE = "FOLDERROLE";
    
    public final static Object RIGHTSMANAGEMENT = "RIGHTSMANAGEMENT";
    
    public final static Object SHOWCATEGORY = "SHOWCATEGORY";
    public final static Object HIDECATEGORY = "HIDECATEGORY";
    public final static Object SETSTANDARDCATEGORY = "SETSTANDARDCATEGORY";
    public final static Object FINDERSERVICE = "FINDERSERVICE";    
    public final static Object EDITSUBCATEGORY = "EDITSUBCATEGORY";  
    public final static Object EDITCATEGORY = "EDITCATEGORY";
    
    public final static Object CALENDARPROXY = "CALENDARPROXY";

    private Preferences m_oPreferences;
    
    private List adminGroups;
    
    public AdminPanel(GUIListener am, ControlListener cl, boolean showcategorykey)
    {
        super();
        
        m_bShowVerteilerKey = showcategorykey;
        m_oGUIListener = am;
        m_oControlListener = cl;
        
        adminGroups = new ArrayList();
        m_oPreferences = m_oGUIListener.getPreferences(StartUpConstants.PREF_ADMIN_PANEL_NODE_NAME);
        
        
        m_oShowPanel = new ArrayList();

        try {
            
        	// globale Rechte pruefen
        	checkVisibility(Key.ADMIN_CREATECATEGORY, CREATECATEGORY);
        	checkVisibility(Key.ADMIN_EDITCATEGORY, EDITCATEGORY);
        	checkVisibility(Key.ADMIN_DELETECATEGORY, DELETECATEGORY);
        	checkVisibility(Key.ADMIN_CREATEUSER, CREATEUSER);
        	checkVisibility(Key.ADMIN_DELETEUSER, DELETEUSER);
        	checkVisibility(Key.ADMIN_EDITUSER, EDITUSER);
        	checkVisibility(Key.ADMIN_CALENDARPROXY, CALENDARPROXY);
        	checkVisibility(Key.ADMIN_CREATEGROUP, CREATEGROUP);
        	checkVisibility(Key.ADMIN_EDITGROUP, EDITGROUP);
        	checkVisibility(Key.ADMIN_DELETEGROUP, DELETEGROUP);
            
        	
        	// kategoriebezogene Rechte pruefen
        	checkVisibility(Key.ADMIN_CREATESUBCATEGORY, CREATESUBCATEGORY);
        	checkVisibility(Key.ADMIN_DELETESUBCATEGORY, DELETESUBCATEGORY);
        	checkVisibility(Key.ADMIN_EDITSUBCATEGORY, EDITSUBCATEGORY);        	
        	checkVisibility(Key.ADMIN_RIGHTSMANAGEMENT, RIGHTSMANAGEMENT);
        	checkVisibility(Key.ADMIN_GLOBALROLE, GLOBALROLE);
        	checkVisibility(Key.ADMIN_FOLDERROLE, FOLDERROLE);
        	
        	
		}
        catch (ContactDBException e) {e.printStackTrace();}
        
        checkConfig(Key.ADMIN_ASSOCIATEUSER, ASSOCIATEUSER);
        
        this.setLayout(new BorderLayout());
        
        ExtendedDataContainer containerCategory = createExtendedDataContainer("category");
        ExtendedDataContainer containerUser = createExtendedDataContainer("user");
        ExtendedDataContainer containerGroups = createExtendedDataContainer("groups");
        ExtendedDataContainer containerRightsManagement = createExtendedDataContainer("rights");
        // not integrated in BMU-release
        ExtendedDataContainer containerCalendar = null;
        if (showPanel(CALENDARPROXY)){
        	containerCalendar = createExtendedDataContainer("calendar");
        }
        ExtendedDataContainer containerExtras = createExtendedDataContainer("extras");

        //    Funktionsgruppe Kategorie:
        
        if (!m_oShowPanel.isEmpty()){
        
	        if (showPanel(CREATECATEGORY)) containerCategory.addExtendedDataItemModel(createCreateCategoryPanel());
	        if (showPanel(EDITCATEGORY)) containerCategory.addExtendedDataItemModel(createEditCategoryPanel());
	        if (showPanel(DELETECATEGORY)) containerCategory.addExtendedDataItemModel(createDeleteKategoriePanel());
	        if (showPanel(SHOWSUBCATEGORY)) containerCategory.addExtendedDataItemModel(createShowVerteilerPanel());
	        if (showPanel(CREATESUBCATEGORY)) containerCategory.addExtendedDataItemModel(createCreateVerteilerPanel());
	        if (showPanel(EDITSUBCATEGORY)) containerCategory.addExtendedDataItemModel(createEditVerteilerPanel());
	        if (showPanel(DELETESUBCATEGORY)) containerCategory.addExtendedDataItemModel(createDeleteVerteilerPanel());
	        
	        //    Funktionsgruppe Benutzer:    
	        if (showPanel(CREATEUSER)) containerUser.addExtendedDataItemModel(createCreateUserPanel());
	        if (showPanel(EDITUSER)) containerUser.addExtendedDataItemModel(createChangeUserPanel());
	        if (showPanel(DELETEUSER)) containerUser.addExtendedDataItemModel(createDeleteUserPanel());
	        
	        if (showPanel(ASSOCIATEUSER)) containerUser.addExtendedDataItemModel(createAssociateUserPanel());
	        if (showPanel(SHOWCATEGORY)) containerUser.addExtendedDataItemModel(createShowCategoryPanel());
	        
	        if (showPanel(HIDECATEGORY)) containerUser.addExtendedDataItemModel(createHideCategoryPanel());
	        if (showPanel(SETSTANDARDCATEGORY)) containerUser.addExtendedDataItemModel(createSetStandardkategoriePanel());
	
	        // not integrated in BMU-release
	        // Calendar
	        if (showPanel(CALENDARPROXY))
				try {
					containerCalendar.addExtendedDataItemModel(createCalendarProxyPanel());
				} catch (ContactDBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        
	        //  Funktionsgruppe Gruppe:
	        if (showPanel(CREATEGROUP)) containerGroups.addExtendedDataItemModel(createCreateGroupPanel());
	        if (showPanel(EDITGROUP)) containerGroups.addExtendedDataItemModel(createEditGroupPanel());
	        if (showPanel(DELETEGROUP)) containerGroups.addExtendedDataItemModel(createDeleteGroupPanel());
	        
	        
	        //  Funktionsgruppe Berechtigungen:
	        if (showPanel(RIGHTSMANAGEMENT))containerRightsManagement.addExtendedDataItemModel(createRightsManagementPanel());
	        if (showPanel(GLOBALROLE)) containerGroups.addExtendedDataItemModel(createGlobalRolePanel());
	        if (showPanel(FOLDERROLE)) containerRightsManagement.addExtendedDataItemModel(CategoryRolesManagement.getInstance().getPanel(this));
        }
        else {
            m_oGUIListener.getLogger().log(Level.INFO, "Kein Adminpanel anzeigen...");
        }
        
        
        containerCategory.realize();
        containerUser.realize();
        containerGroups.realize();
        containerRightsManagement.realize();
        containerExtras.realize();
        // not integrated in BMU-release
        if (showPanel(CALENDARPROXY))
			containerCalendar.realize();
        
        JTabbedPane subtab = new JTabbedPane();    
        subtab.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
       subtab.addMouseWheelListener(new TabbedPaneMouseWheelNavigator(subtab));
        
        if (containerCategory.getNumberOfExtendedDataItemModels() > 0) subtab.addTab(Messages.getString("GUI_Admin_Tab_Kategorien"), GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY), containerCategory,   Messages.getString("GUI_Admin_Tab_Kategorien_ToolTip"));
        if (containerUser.getNumberOfExtendedDataItemModels() > 0) subtab.addTab(Messages.getString("GUI_Admin_Tab_User"), GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_USER), containerUser,   Messages.getString("GUI_Admin_Tab_User_ToolTip"));
        if (containerGroups.getNumberOfExtendedDataItemModels() > 0) subtab.addTab(Messages.getString("GUI_Admin_Tab_Group"), GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_GROUP), containerGroups,   Messages.getString("GUI_Admin_Tab_Group_ToolTip"));
        if (containerRightsManagement.getNumberOfExtendedDataItemModels() > 0) subtab.addTab(Messages.getString("GUI_Admin_Tab_Role"), GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_ROLE), containerRightsManagement,   Messages.getString("GUI_Admin_Tab_Role_ToolTip"));
        if (containerExtras.getNumberOfExtendedDataItemModels() > 0) subtab.addTab(Messages.getString("GUI_Admin_Tab_Extras"), GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_EXTRAS), containerExtras,   Messages.getString("GUI_Admin_Tab_Extras_ToolTip"));
        // not integrated in BMU-release
        if (showPanel(CALENDARPROXY))
			if (containerCalendar.getNumberOfExtendedDataItemModels() > 0) subtab.addTab(Messages.getString("GUI_Admin_Tab_Calendar"), GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_EXTRAS), containerCalendar,   Messages.getString("GUI_Admin_Tab_Calendar_ToolTip"));
        
        JPanel datapanel = new JPanel();
        datapanel.setLayout(new BorderLayout());
        datapanel.add(subtab, BorderLayout.CENTER);
        
        
        JPanel logpanel = new JPanel();
        logpanel.setLayout(new BorderLayout());
        
        m_oLogMessagesTextArea = new JTextArea();
        m_oLogMessagesTextArea.setEditable(false);
        JScrollPane logscroll = new JScrollPane(m_oLogMessagesTextArea);
        logscroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        logscroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        logpanel.add(logscroll, BorderLayout.CENTER);
        
        JLabel loglabel = new JLabel("Aktionshistorie");
        loglabel.setBorder(new CompoundBorder(new LineBorder(Color.BLACK, 1), new EmptyBorder(3,3,3,3)));
        logpanel.add(loglabel, BorderLayout.NORTH);
        
        
        JPanel mainpanel = new JPanel(new BorderLayout());
        
        if (m_bShowLogPanel)
        {    
            m_oSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
            m_oSplit.setLeftComponent(datapanel);
            m_oSplit.setRightComponent(logpanel);
            m_oSplit.setOneTouchExpandable(true);
            m_oSplit.setContinuousLayout(true);
            
            mainpanel.add(m_oSplit, BorderLayout.CENTER);
        }
        else
        {
            mainpanel.add(datapanel, BorderLayout.CENTER);
        }
        
        
        this.add(mainpanel, BorderLayout.CENTER);
        
        am.addMasterDataListener(this);    
    }
    
    public void categoryRolesUpdated(){
        logger.fine("updating rights tripple comboboxes...");
        updateRightsManagementBoxes();
    }

    public ExtendedDataContainer createExtendedDataContainer(String prefKey) 
    {
        ExtendedDataContainer container = new ExtendedDataContainer(m_oPreferences, prefKey);
        return container;
    }

    
	/**
	 * Tests whether a veto for a given action exists.
	 * @param action
	 * @return true, if veto exists
	 */
    private boolean checkForVeto(VetoableAction action){
		return (m_oGUIListener.userRequestCheckForVeto(action)!=null);
    }
    
    public boolean showPanel(Object key)
    {
        return m_oShowPanel.contains(key);
    }
    
    public void insertPanelToShow(Object key)
    {
        m_oShowPanel.add(key);
    }
    
    private void checkVisibility(Key key, Object objkey) throws ContactDBException {
                
    	User currentUser = m_oGUIListener.getUser(null, true);
    	
        GlobalRole currentRole = currentUser.getGlobalRights();
        //currentUser.getCategoryRights();

		boolean show = false;
		
		
		//****** GLOBALE RECHTE ******
		//Kategorien
		
		if (key.equals(Key.ADMIN_CREATECATEGORY)) {
			show = currentRole.hasRightEditFolder();
		}
		else if (key.equals(Key.ADMIN_EDITCATEGORY)){
			show = ((GlobalRoleBean)currentRole).hasRightEditFolder();
		}
		else if (key.equals(Key.ADMIN_DELETECATEGORY)){
			show = ((GlobalRoleBean)currentRole).hasRightRemoveFolder();
		}
		
		//Benutzer

		else if (key.equals(Key.ADMIN_CREATEUSER)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();
		}
		else if (key.equals(Key.ADMIN_DELETEUSER)){
			show = ((GlobalRoleBean)currentRole).hasRightRemoveUser();
		}		
		else if (key.equals(Key.ADMIN_EDITUSER)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();
		}

		//Calendar
		
		else if (key.equals(Key.ADMIN_CALENDARPROXY)){
            // Jeder User hat einen Kalender, den er jemand anderem freigeben kann
			show = ConfigManager.getAppearance().getAsBoolean(Key.SHOW_SCHEDULEPANEL, true);
		}
		
		// Gruppen
		
		else if (key.equals(Key.ADMIN_EDITGROUP)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();
		}
		else if (key.equals(Key.ADMIN_CREATEGROUP)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();
		}
		else if (key.equals(Key.ADMIN_DELETEGROUP)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();
		}
		
		// ***** KATEGORIEBEZOGENE RECHTE *****
		
		// Unterkategorien
		
		else if (key.equals(Key.ADMIN_CREATESUBCATEGORY)){
			show = (currentUser.hasAnyCategoryRights(getKategorien(true, true, false), ObjectRole.KEY_ADDSUB));
		}
		else if (key.equals(Key.ADMIN_DELETESUBCATEGORY)){
			show = (currentUser.hasAnyCategoryRights(getKategorien(true, true, false), ObjectRole.KEY_REMOVESUB));
		}
		else if (key.equals(Key.ADMIN_EDITSUBCATEGORY)){
			show = (currentUser.hasAnyCategoryRights(getKategorien(true, true, false), ObjectRole.KEY_ADDSUB));
		}
		
		// Rechtevergabe
		
		else if (key.equals(Key.ADMIN_RIGHTSMANAGEMENT)){
			
			List categories = getKategorien(true, true, false);			
		
			show = (categories != null) && !categories.isEmpty() //don't show if no categories 
				&& currentUser.hasAnyCategoryRights(categories, ObjectRole.KEY_GRANT);
			
			//TODO show ADMIN_RIGHTSMANAGEMENT when first category created
		}
		else if (key.equals(Key.ADMIN_GLOBALROLE)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();			
		}
		else if (key.equals(Key.ADMIN_FOLDERROLE)){
			show = ((GlobalRoleBean)currentRole).hasRightEditUser();
		}
		
		
		
		
		/*
        
        Config.ADMIN_SHOWSUBCATEGORY
        Config.ADMIN_ASSOCIATEUSER
             
        Config.ADMIN_GLOBALROLE
         Config.ADMIN_STARTCONFIGURATOR     
        
        Config.ADMIN_CALENDARPROXY
        */
    	
		
		if (show) insertPanelToShow(objkey); // panel der liste der zu zeigenden objekte hinzuf?gen
    }
    
    
    private void checkConfig(Key configKey, Object showKey)
    {
      Appearance ape = ConfigManager.getAppearance();
      
      if (ape.getAsBoolean(configKey))
        insertPanelToShow(showKey);
    }
    
    // --------------------------------------------------------
    
    public void setDividerLocation(double location)
    {
        if (m_bShowLogPanel) m_oSplit.setDividerLocation(location);
    }
    
    private String m_sLine = "";
    
    public void addLogText(String text)
    {
        m_oLogMessagesTextArea.append(text);
        m_sLine += text;
    }
    
    public void addLogTextEndMarker()
    {
        int linelen = m_sLine.length();
        if (linelen > m_iLineLength) m_iLineLength = linelen; 
        
        String line = "";
        for(int i=0; i<m_iLineLength; i++) line +="-";
        m_oLogMessagesTextArea.append(line + "\n");
        m_sLine = "";
    }
    
    public void setErrorText(String text, boolean isfatal)
    {
        m_sErrorText = text.replace('\n', ' ');
        m_bErrorTextExists = true;
    }
    
   
    private String getErrorText()
    {
        return(m_sErrorText);
    }
    
    
    private void showResult(boolean isgood)
    {
        if (isgood) addLogText("fertig.\n"); 
        else 
        {
            if (m_bErrorTextExists)
            {
                addLogText("fehlgeschlagen, da: \"" + getErrorText() + "\".\n");
            }
            else addLogText("fehlgeschlagen.\n");
        } 
    }
    
    
    
    // --------------------------------------------------------
    
    private JTextField m_oTextField_CreateCategory_Key;
    private JTextField m_oTextField_CreateCategory_Name;
    private JTextField m_oTextField_CreateCategory_Description;
    
    private ExtendedDataItemModel createCreateCategoryPanel()
    {
        JLabel keyLabel = createEntryLabel(Messages.getString("GUI_Admin_Kategorie_Key") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateCategory_Key = createEntryTextField("", m_iMaxLength_CreateCategory_Key); //$NON-NLS-1$
        JPanel keyPanel = createEntryPanel(keyLabel, m_oTextField_CreateCategory_Key);
        
        JLabel nameLabel = createEntryLabel(Messages.getString("GUI_Admin_Kategorie_Name") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateCategory_Name = createEntryTextField("", m_iMaxLength_CreateCategory_Name); //$NON-NLS-1$
        JPanel namePanel = createEntryPanel(nameLabel, m_oTextField_CreateCategory_Name);
        
        JLabel descriptionLabel = createEntryLabel(Messages.getString("GUI_Admin_Kategorie_Description") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateCategory_Description = createEntryTextField("", m_iMaxLength_CreateCategory_Description); //$NON-NLS-1$
        JPanel descriptionPanel = createEntryPanel(descriptionLabel, m_oTextField_CreateCategory_Description);
        
        JPanel createKategorieEntryPanel = createEntriesPanel();
        if (m_bShowVerteilerKey) createKategorieEntryPanel.add(keyPanel);
        createKategorieEntryPanel.add(namePanel);
        createKategorieEntryPanel.add(descriptionPanel);
        
        JButton button_createKategorie = createButton(Messages.getString("GUI_Admin_Kategorie_Ausfuehren"), new button_createCategory_clicked()); //$NON-NLS-1$
        
        JPanel createKategoriePanel = new JPanel(new BorderLayout());
        createKategoriePanel.add(createKategorieEntryPanel, BorderLayout.CENTER);
        createKategoriePanel.add(createButtonPanel(button_createKategorie), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Kategorie_anlegen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", createKategoriePanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_CREATE));
        return item;    
    }
    


    public class button_createCategory_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String key  = m_oTextField_CreateCategory_Key.getText().trim();
            String name = m_oTextField_CreateCategory_Name.getText().trim();
            String description = m_oTextField_CreateCategory_Description.getText().trim();

            // Testen, ob der Kategoriename bereits existiert
            for (Iterator iter = m_oGUIListener.getAllCategories().values().iterator(); iter.hasNext();) {
                if (name.equals(((Category)iter.next()).getName())) {
                    m_oControlListener.showMessage("Der angegebene Kategoriename existiert bereits!");
                    return;
                }
            }
       
            if (m_bShowVerteilerKey)
            {
                if (key.length() == 0)
                {
                    m_oControlListener.showMessage("Sie m?ssen einen Schl?ssel angeben!");
                    m_oTextField_CreateCategory_Key.requestFocus(true);
                    return;   
                }
            }
            
            if (name.length() > 0)
            {
                m_bCategoryAdded = false;
                m_oGUIListener.setWaiting(true);
                if (key.length() == 0) 
                {
                    key = null;
                    addLogText("erzeuge Kategorie mit automatisch generiertem Schlssel und Namen \"" + name + "\"... ");
                }
                else
                { 
                    addLogText("erzeuge Kategorie mit Schlssel \"" + key +"\"" + " und Namen \"" + name + "\"... ");
                }
                m_oGUIListener.userRequestCreateCategory(key, name, description);
                updateUserKategorienBoxes();
                
                updateStandardKategorieBox();
                updateShowKategorieBox();
                updateHideKategorieBox();
				updateRightsManagementBoxes();
                
                showResult(m_bCategoryAdded); 
                addLogTextEndMarker();
                m_oGUIListener.setWaiting(false);
            }
            else
            {
                m_oControlListener.showMessage("Sie m?ssen einen Namen angeben!");
                m_oTextField_CreateCategory_Name.requestFocus(true);
            }

            m_oTextField_CreateCategory_Key.setText("");
            m_oTextField_CreateCategory_Name.setText("");
            m_oTextField_CreateCategory_Description.setText("");
        }
    }
    
    
    // --------------------------------------------------------
    
    private JComboBox m_oComboBox_CreateVerteiler_Kategorie;
    private JTextField m_oTextField_CreateVerteiler_Key;
    private JTextField m_oTextField_CreateVerteiler_Name1;
    private JTextField m_oTextField_CreateVerteiler_Name2;
    private JTextField m_oTextField_CreateVerteiler_Name3;
    
    private ExtendedDataItemModel createCreateVerteilerPanel()
    {
    	
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_Unterkategorie_Kategorie") + " ",null); //$NON-NLS-1$
        List kategorieList = getUserCategoriesWithCategoryRight(6, false, true);
        
        m_oComboBox_CreateVerteiler_Kategorie = createEntryComboBox(kategorieList.toArray());
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_CreateVerteiler_Kategorie);
        
        JLabel keyLabel = createEntryLabel(Messages.getString("GUI_Admin_Unterkategorie_Key") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateVerteiler_Key = createEntryTextField("", m_iMaxLength_CreateVerteiler_Key); //$NON-NLS-1$
        JPanel keyPanel = createEntryPanel(keyLabel, m_oTextField_CreateVerteiler_Key);
        
        JLabel name1Label = createEntryLabel(Messages.getString("GUI_Admin_Unterkategorie_Name_1") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateVerteiler_Name1 = createEntryTextField("", m_iMaxLength_CreateVerteiler_Name1); //$NON-NLS-1$
        JPanel name1Panel = createEntryPanel(name1Label, m_oTextField_CreateVerteiler_Name1);
        
        JLabel name2Label = createEntryLabel(Messages.getString("GUI_Admin_Unterkategorie_Name_2") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateVerteiler_Name2 = createEntryTextField("", m_iMaxLength_CreateVerteiler_Name2); //$NON-NLS-1$
        JPanel name2Panel = createEntryPanel(name2Label, m_oTextField_CreateVerteiler_Name2);
        
        JLabel name3Label = createEntryLabel(Messages.getString("GUI_Admin_Unterkategorie_Name_3") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateVerteiler_Name3 = createEntryTextField("", m_iMaxLength_CreateVerteiler_Name3); //$NON-NLS-1$
        JPanel name3Panel = createEntryPanel(name3Label, m_oTextField_CreateVerteiler_Name3);
        
        JPanel createVerteilerEntryPanel = createEntriesPanel();
        createVerteilerEntryPanel.add(kategoriePanel);
        if (m_bShowVerteilerKey) createVerteilerEntryPanel.add(keyPanel);
        createVerteilerEntryPanel.add(name1Panel);
        createVerteilerEntryPanel.add(name2Panel);
        if(m_oGUIListener.getDatabaseFeature("verteiler.kommentar")!=null)createVerteilerEntryPanel.add(name3Panel);
        
        JButton button_createVerteiler = createButton(Messages.getString("GUI_Admin_Unterkategorie_Ausfuehren"), new button_createverteiler_clicked()); //$NON-NLS-1$
        
        updateKategorienBoxes(false);
        
        JPanel createVerteilerPanel = new JPanel(new BorderLayout()); // createInnerPanel(Messages.getString("GUI_Admin_Unterkategorie_erzeugen")); 
        createVerteilerPanel.add(createVerteilerEntryPanel, BorderLayout.CENTER);
        createVerteilerPanel.add(createButtonPanel(button_createVerteiler), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Unterkategorie_anlegen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", createVerteilerPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_CREATE));
        return item;    
    }
    
    
    public class button_createverteiler_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {      
            Category gruppe = (Category) m_oComboBox_CreateVerteiler_Kategorie.getSelectedItem();
            String key    = m_oTextField_CreateVerteiler_Key.getText().trim();
            String name1  = m_oTextField_CreateVerteiler_Name1.getText().trim();
            String name2  = m_oTextField_CreateVerteiler_Name2.getText().trim();
            String name3  = m_oTextField_CreateVerteiler_Name3.getText().trim();

            // Name angegeben
            if ( name1.length() <= 0) {
                m_oControlListener.showMessage("Sie m?ssen mindestens einen Namen angeben!");
                m_oTextField_CreateVerteiler_Key.requestFocus(true);
                return;
            }      

            // Name existiert bereits 
            Iterator verteilerMapping = m_oGUIListener.getVerteiler(gruppe.getIdAsString()).entrySet().iterator();
            while (verteilerMapping.hasNext()) {
                Map.Entry entry = (Map.Entry) verteilerMapping.next();
                if (name1.equals( ((SubCategory) entry.getValue()).getSubCategoryName() )) {
                    m_oControlListener.showMessage("Dieser Kategoriename ist bereits vorhanden!");
                    m_oTextField_CreateVerteiler_Key.requestFocus(true);
                    return;
                }
            }
       
            m_bSubCategoryAdded = false;
            m_oGUIListener.setWaiting(true);            
            
            if (key.length() == 0){
                key = name1;      // key = null;
                addLogText("erzeuge Unterkategorie mit automatisch generiertem Schlssel und dem Namen \"" + name1 + "\", \"" + name2 + "\", \"" + name3 +  " in der Kategorie \"" + gruppe + "\"... ");
            }
            else
                {
                    addLogText("erzeuge Unterkategorie mit Schlssel \"" + key +"\"" + " und dem Namen \"" + name1 + "\", \"" + name2 + "\", \"" + name3 +  " in der Kategorie \"" + gruppe + "\"... ");
                }
            m_oGUIListener.userRequestCreateVerteiler(gruppe.getIdAsInteger(), name1, name2);
            showResult(m_bSubCategoryAdded); 
        
            addLogTextEndMarker();
            updateDeleteVerteilerBox();
            updateUserKategorienBoxes();        
            m_oGUIListener.setWaiting(false);
        
            m_oTextField_CreateVerteiler_Key.setText("");
            m_oTextField_CreateVerteiler_Name1.setText("");
            m_oTextField_CreateVerteiler_Name2.setText("");
            m_oTextField_CreateVerteiler_Name3.setText("");
        }
    }
    
    
    
    // --------------------------------------------------------  
    protected void updateDeleteVerteilerBox() 
    {
        Category kategorie = (Category) m_oComboBox_DeleteVerteiler_Kategorie.getSelectedItem();
        if(kategorie==null)return;
        List verteiler;
		verteiler = getVerteiler(kategorie.getIdAsString());
		updateBox(m_oComboBox_DeleteVerteiler_Verteiler, verteiler);    
		
    }
    
    protected List getVerteiler(String kategorie) 
    {
        List verteiler = new ArrayList();
        Iterator verteilerMapping = m_oGUIListener.getVerteiler(kategorie).entrySet().iterator();
        while (verteilerMapping.hasNext()) 
        {
            Map.Entry entry = (Map.Entry) verteilerMapping.next();
            SubCategory subcat = (SubCategory) entry.getValue();
            verteiler.add(subcat);
        }
        Collections.sort(verteiler, new ToStringComparator());
        return verteiler;
    }
       
    private JComboBox m_oComboBox_DeleteVerteiler_Kategorie;
    private JComboBox m_oComboBox_DeleteVerteiler_Verteiler;
    
    private ExtendedDataItemModel createDeleteVerteilerPanel()
    {
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_DeleteVerteiler_Kategorie") + " ",null); //$NON-NLS-1$
        List kategorieList = getUserCategoriesWithCategoryRight(7, false, false);
       
        m_oComboBox_DeleteVerteiler_Kategorie = createEntryComboBox(kategorieList.toArray());
        m_oComboBox_DeleteVerteiler_Kategorie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateDeleteVerteilerBox();
            }
        });
        
        String kategorie = getKeyFromComboBoxEntry(m_oComboBox_DeleteVerteiler_Kategorie);
        List verteiler = getVerteiler(kategorie);    
        JLabel verteilerLabel = createEntryLabel(Messages.getString("GUI_Admin_DeleteVerteiler_Verteiler") + " ",null); //$NON-NLS-1$
        m_oComboBox_DeleteVerteiler_Verteiler = createEntryComboBox(verteiler.toArray());
        
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_DeleteVerteiler_Kategorie);
        JPanel verteilerPanel = createEntryPanel(verteilerLabel, m_oComboBox_DeleteVerteiler_Verteiler);
        
        JPanel deleteVerteilerEntryPanel = createEntriesPanel();
        deleteVerteilerEntryPanel.add(kategoriePanel);
        deleteVerteilerEntryPanel.add(verteilerPanel);
        
        JButton button_deleteVerteiler = createButton(Messages.getString("GUI_Admin_DeleteVerteiler_Ausfuehren"), new button_deleteVerteiler_clicked()); //$NON-NLS-1$    
        
        JPanel delVerteilerPanel = new JPanel(new BorderLayout());
        delVerteilerPanel.add(deleteVerteilerEntryPanel, BorderLayout.CENTER);
        delVerteilerPanel.add(createButtonPanel(button_deleteVerteiler), BorderLayout.EAST);
        
        updateDeleteVerteilerBox();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_DeleteVerteiler"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", delVerteilerPanel));
        
        //TODO: wieder enablen wenn die Funktion zur verfgung steht!
        //     button_deleteVerteiler.setEnabled(false);
        //     m_oComboBox_DeleteVerteiler_Kategorie.setEnabled(false);
        //     m_oComboBox_DeleteVerteiler_Verteiler.setEnabled(false);     
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_DELETE));
        return item;    
    }
    
    public class button_deleteVerteiler_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String bezeichnung  = ""+m_oComboBox_DeleteVerteiler_Verteiler.getSelectedItem();
            String[] answers = {"Ja", "Nein"};
            int answer = ApplicationServices.getInstance().getCommonDialogServices().askUser(Messages.getString("GUI_Admin_ReallyDeleteSubCategoryTitle"), Messages.getFormattedString("GUI_Admin_ReallyDeleteSubCategory", bezeichnung), answers, 1);
            if (answer == 0)
            {         
                m_oGUIListener.setWaiting(true);      
                String kategorie;
			
                kategorie = ((Category)m_oComboBox_DeleteVerteiler_Kategorie.getSelectedItem()).getIdAsString();
				String verteiler    = ((SubCategory)m_oComboBox_DeleteVerteiler_Verteiler.getSelectedItem()).getIdAsString();       
				addLogText("l?sche die Unterkategorie \"" + verteiler + "\" aus der Kategorie \"" + kategorie + "\"... ");         
				m_bSubCategoryRemoved = false;
				m_oGUIListener.userRequestDeleteVerteiler(kategorie, verteiler);
				showResult(m_bSubCategoryRemoved);
			
                addLogTextEndMarker();
                updateDeleteVerteilerBox();
                updateUserKategorienBoxes();
                updateShowVerteilerList();
                updateRightsManagementPanel();
                m_oGUIListener.setWaiting(false);
            }
        }
    }
    
    
    
    
    
    // --------------------------------------------------------
    // --------------------------------------------------------
    // --------------------------------------------------------
    
    
    
    private JComboBox m_oComboBox_DeleteKategorie_Kategorie;
    
    private ExtendedDataItemModel createDeleteKategoriePanel()
    {
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_DeleteKategorie_Kategorie") + " ",null); //$NON-NLS-1$
        m_oComboBox_DeleteKategorie_Kategorie = createEntryComboBox(getKategorien(false, true, true).toArray());
        m_oComboBox_DeleteKategorie_Kategorie.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //updateKategorienBoxes();
            }
        });
        
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_DeleteKategorie_Kategorie);
        
        JPanel deleteKategorieEntryPanel = createEntriesPanel();
        deleteKategorieEntryPanel.add(kategoriePanel);
        
        JButton button_deleteKategorie = createButton(Messages.getString("GUI_Admin_DeleteKategorie_Ausfuehren"), new button_deleteKategorie_clicked()); //$NON-NLS-1$
        
        JPanel delKategoriePanel = new JPanel(new BorderLayout());
        delKategoriePanel.add(deleteKategorieEntryPanel, BorderLayout.CENTER);
        delKategoriePanel.add(createButtonPanel(button_deleteKategorie), BorderLayout.EAST);
        
        updateKategorienBoxes(false);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_DeleteKategorie"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", delKategoriePanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_DELETE));
        return item;    
    }
    
    public class button_deleteKategorie_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
        	String kategorie  = getKeyFromComboBoxEntry(m_oComboBox_DeleteKategorie_Kategorie);
            String bezeichnung  = ""+m_oComboBox_DeleteKategorie_Kategorie.getSelectedItem();

            String[] answers = {"Ja", "Nein"};
            int answer = ApplicationServices.getInstance().getCommonDialogServices().askUser(Messages.getString("GUI_Admin_ReallyDeleteCategoryTitle"), Messages.getFormattedString("GUI_Admin_ReallyDeleteCategory", bezeichnung), answers, 1);
            if (answer == 0)
            {         
                m_oGUIListener.setWaiting(true);             
                addLogText("l?sche die Kategorie \"" + kategorie + "\"... ");         
                m_bCategoryRemoved = false;
                m_oGUIListener.userRequestDeleteCategory(kategorie);
                
                showResult(m_bCategoryRemoved);
                addLogTextEndMarker();
                updateUserKategorienBoxes();
                updateRightsManagementBoxes();
                updateRightsManagementPanel();
                updateUserKategorienBoxes();
                updateDeleteVerteilerBox();
                m_oGUIListener.setWaiting(false);
            }
        }
    }
    
    
    
    
    private JComboBox m_oComboBox_ShowVerteiler_Kategorie;
    private tarentList m_oList_ShowVerteiler_Verteiler;
    
    private ExtendedDataItemModel createShowVerteilerPanel()
    {
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_ShowVerteiler_Kategorie") + " ",null); //$NON-NLS-1$
        m_oComboBox_ShowVerteiler_Kategorie = createEntryComboBox(getKategorien().toArray());
        m_oComboBox_ShowVerteiler_Kategorie.addActionListener(new ActionListener() 
                {
            public void actionPerformed(ActionEvent e) 
            {
                updateShowVerteilerList();
            }
                });
        
        String kategorie = getKeyFromComboBoxEntry(m_oComboBox_ShowVerteiler_Kategorie);
        List verteiler = getVerteiler(kategorie);    
        JLabel verteilerLabel = createEntryLabel(Messages.getString("GUI_Admin_ShowVerteiler_Verteiler") + " ",null); //$NON-NLS-1$
        m_oList_ShowVerteiler_Verteiler = createEntryList(verteiler.toArray());
        JScrollPane scroll = new JScrollPane(m_oList_ShowVerteiler_Verteiler);
        
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_ShowVerteiler_Kategorie);
        JPanel verteilerPanel = createEntryPanel(verteilerLabel, scroll);
        
        JPanel showVerteilerEntryPanel = createEntriesPanel();
        showVerteilerEntryPanel.add(kategoriePanel);
        showVerteilerEntryPanel.add(verteilerPanel);
        
        JPanel showVerteilerPanel = new JPanel(new BorderLayout());
        showVerteilerPanel.add(showVerteilerEntryPanel, BorderLayout.CENTER);
        
        updateShowVerteilerList();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_ShowVerteiler"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", showVerteilerPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_SHOW));
        return item;    
    }
    
    protected void updateShowVerteilerList() 
    {
        String kategorie = getKeyFromComboBoxEntry(m_oComboBox_ShowVerteiler_Kategorie);
        if (kategorie != null){
        	List verteiler = getVerteiler(kategorie);
            updateList(m_oList_ShowVerteiler_Verteiler, verteiler);
        }
    }
    
    
    
    
    // --------------------------------------------------------
    
    
    
    
    //TODO
    private JComboBox m_oComboBox_EditVerteiler_Kategorie;
    private JComboBox m_oComboBox_EditVerteiler_SubKategorie;
    private JTextField m_oTextField_EditVerteiler_Key;
    private JTextField m_oTextField_EditVerteiler_Name1;
    private JTextField m_oTextField_EditVerteiler_Name2;
    private JTextField m_oTextField_EditVerteiler_Name3;
    
    private ExtendedDataItemModel createEditVerteilerPanel()
    {
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_EditVerteiler_Kategorie") + " ",null); //$NON-NLS-1$
        List kategorieList = getUserCategoriesWithCategoryRight(6, true, false);
        
        
        
        m_oComboBox_EditVerteiler_Kategorie = createEntryComboBox(kategorieList.toArray());
        m_oComboBox_EditVerteiler_Kategorie.addActionListener(new ActionListener() 
                {
            public void actionPerformed(ActionEvent e) 
            {
                updateEditVerteilerList();
            }
                });
        
        JLabel subkategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_EditVerteiler_SubKategorie") + " ",null); //$NON-NLS-1$
        Object[] empty = {""};
        m_oComboBox_EditVerteiler_SubKategorie = createEntryComboBox(empty);
        m_oComboBox_EditVerteiler_SubKategorie.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                updateNamePanels();
            }
        });
        
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_EditVerteiler_Kategorie);
        JPanel verteilerPanel = createEntryPanel(subkategorieLabel, m_oComboBox_EditVerteiler_SubKategorie);    
        
        // ----------------------------------
        
        JLabel keyLabel = createEntryLabel(Messages.getString("GUI_Admin_EditVerteiler_Key") + " ",null); //$NON-NLS-1$
        m_oTextField_EditVerteiler_Key = createEntryTextField("", m_iMaxLength_EditVerteiler_Key); //$NON-NLS-1$
        JPanel keyPanel = createEntryPanel(keyLabel, m_oTextField_EditVerteiler_Key);
        
        JLabel name1Label = createEntryLabel(Messages.getString("GUI_Admin_EditVerteiler_Name_1") + " ",null); //$NON-NLS-1$
        m_oTextField_EditVerteiler_Name1 = createEntryTextField("", m_iMaxLength_EditVerteiler_Name1); //$NON-NLS-1$
        JPanel name1Panel = createEntryPanel(name1Label, m_oTextField_EditVerteiler_Name1);
        
        JLabel name2Label = createEntryLabel(Messages.getString("GUI_Admin_EditVerteiler_Name_2") + " ",null); //$NON-NLS-1$
        m_oTextField_EditVerteiler_Name2 = createEntryTextField("", m_iMaxLength_EditVerteiler_Name2); //$NON-NLS-1$
        JPanel name2Panel = createEntryPanel(name2Label, m_oTextField_EditVerteiler_Name2);
        
        
        JLabel name3Label = createEntryLabel(Messages.getString("GUI_Admin_EditVerteiler_Name_3") + " ",null); //$NON-NLS-1$
        m_oTextField_EditVerteiler_Name3 = createEntryTextField("", m_iMaxLength_EditVerteiler_Name3); //$NON-NLS-1$
        JPanel name3Panel = createEntryPanel(name3Label, m_oTextField_EditVerteiler_Name3);
        
        
        JPanel editVerteilerEntryPanel = createEntriesPanel();
        editVerteilerEntryPanel.add(kategoriePanel);    
        editVerteilerEntryPanel.add(verteilerPanel);
        //editVerteilerEntryPanel.add(kategoriePanel);
        if (m_bShowVerteilerKey) editVerteilerEntryPanel.add(keyPanel);
        editVerteilerEntryPanel.add(name1Panel);
        editVerteilerEntryPanel.add(name2Panel);
        if(m_oGUIListener.getDatabaseFeature("verteiler.kommentar")!=null) editVerteilerEntryPanel.add(name3Panel);
        
        //-----------------------
        JPanel editVerteilerPanel = new JPanel(new BorderLayout());
        editVerteilerPanel.add(editVerteilerEntryPanel, BorderLayout.CENTER);
        
        
        
        JButton button_editVerteiler = createButton(Messages.getString("GUI_Admin_EditVerteiler_Aendern"), new button_editverteiler_clicked()); //$NON-NLS-1$
        
        editVerteilerPanel.add(createButtonPanel(button_editVerteiler), BorderLayout.EAST);
        
        
        //----------------------------------
        
        updateEditVerteilerList();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_EditVerteiler"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", editVerteilerPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_SUBCATEGORY_EDIT));
        return item;    
    }
    
    
    /**
     * 
     */
    protected void updateNamePanels() {
        SubCategory test = (SubCategory) m_oComboBox_EditVerteiler_SubKategorie.getSelectedItem();
        if(test!=null){
    	        String name1 = test.getSubCategoryName();
            String name2 = test.getDescription();
            String name3 = "";
            m_oTextField_EditVerteiler_Name1.setText(name1);
            m_oTextField_EditVerteiler_Name2.setText(name2);
           // m_oTextField_EditVerteiler_Name3.setText(name3);
        }
        
    }
    
    private class button_editverteiler_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String gruppe = ((Category)m_oComboBox_EditVerteiler_Kategorie.getSelectedItem()).getIdAsString();
            String subcategory = ((SubCategory)m_oComboBox_EditVerteiler_SubKategorie.getSelectedItem()).getIdAsString();
            String key    = m_oTextField_EditVerteiler_Key.getText().trim();
            String name1  = m_oTextField_EditVerteiler_Name1.getText().trim();
            String beschreibung = m_oTextField_EditVerteiler_Name2.getText().trim();
            String beschreibung2 = "";
            //String beschreibung2 = m_oTextField_EditVerteiler_Name3.getText().trim();
            
            
            
            boolean dublicate = checkForDublicatedVerteilerName(gruppe, name1, subcategory);
            
            if (!dublicate){
            
	            if ((name1.trim()+beschreibung.trim()).length() > 0)
	            {
	                m_bSubCategoryChanged = false;
	                m_oGUIListener.setWaiting(true);
	                
	                if (key.length() == 0)
	                {
	                    key = null;      
	                    addLogText("?ndere Unterkategorie mit automatisch generiertem Schl?ssel und dem Namen \"" + name1 + "\", \"" + beschreibung  +  " in der Kategorie \"" + gruppe + "\"... ");
	                }
	                else
	                {
	                    addLogText("?ndere Unterkategorie mit Schl?ssel \"" + key +"\"" + " und dem Namen \"" + name1 + "\", \"" + beschreibung +  " in der Kategorie \"" + gruppe + "\"... ");
	                }
	                
	                
	                m_oGUIListener.userRequestEditVerteiler(gruppe, subcategory, name1, beschreibung, beschreibung2);
	                
	                showResult(m_bSubCategoryChanged); 
	                addLogTextEndMarker();
	                updateDeleteVerteilerBox();
	                //updateShowVerteilerList();
	                updateEditVerteilerList();
	                //updateKategorienBoxes();
	                updateUserKategorienBoxes();
	                m_oGUIListener.setWaiting(false);
	            }
	            else
	            {
	                m_oControlListener.showMessage("Sie m?ssen mindestens einen Namen angeben!");
	                m_oTextField_EditVerteiler_Key.requestFocus(true);
	            }
	        }
            
            else{
            	m_oControlListener.showMessage("Der angegebene Name ist bereits f?r eine Unterkategorie in dieser Kategorie vergeben!");
                m_oTextField_EditVerteiler_Key.requestFocus(true);
            }
        	}
    }
    
    private boolean checkForDublicatedVerteilerName(String kategorie, String verteilerName, String verteilerKey){
    	
    	//System.out.println("Checke Verteilernamen auf Dublikate...");
    	//System.out.println("Kategorie: " + kategorie + ", VerteilerID: " +  verteilerKey + ", VerteilerName: " + verteilerName);
    	
    	 if (kategorie != null)
         {
             Map subcategories = m_oGUIListener.getVerteiler(kategorie);
             Iterator  subcategoriesiterator = subcategories.keySet().iterator();   
             
             while(subcategoriesiterator.hasNext())
             { 
		         Object keyobj = (Object)(subcategoriesiterator.next());
		         SubCategory subcategory = (SubCategory)(subcategories.get(keyobj));
		         //System.out.println("Prfe " + subcategory.getId() + " " + subcategory.getName());
		         if (verteilerName.equals(subcategory.getSubCategoryName()) && !(verteilerKey.equals(subcategory.getIdAsString()) ) ){
		         	//System.out.println("IDENTISCH!!!!");
		         	return true;
		         }
             }
         }
    
    	 return false;
    }
    
    /**
     * 
     */
    protected void updateEditVerteilerList() 
    {
        Category kategorie = (Category) m_oComboBox_EditVerteiler_Kategorie.getSelectedItem();
        m_oComboBox_EditVerteiler_SubKategorie.removeAllItems();
        
        if (kategorie != null)
        {
            List subcategorylist = new ArrayList();
            
            Map subcategories;
			subcategories = m_oGUIListener.getVerteiler(kategorie.getIdAsString());
            
			for (Iterator subcategoriesiterator = subcategories.keySet().iterator(); subcategoriesiterator.hasNext();) {
             
                Object keyobj = (Object)(subcategoriesiterator.next());
                SubCategory subcategory = (SubCategory)(subcategories.get(keyobj));
                subcategorylist.add(subcategory);
            }
            
            Collections.sort(subcategorylist, new ToStringComparator());
            updateBox(m_oComboBox_EditVerteiler_SubKategorie, subcategorylist);
/*	            Iterator subcategorylistiterator = subcategorylist.iterator();   
	            while(subcategorylistiterator.hasNext())
	            {         
	                String name = (String)(subcategorylistiterator.next());
	                m_oComboBox_EditVerteiler_SubKategorie.addItem(name);
	            }
*/			
        }    
    }
 
    private JComboBox m_oComboBox_EditCategory_Kategorie;
    private JTextField m_oTextField_EditCategory_Key;
    private JTextField m_oTextField_EditCategory_Name;
    private JTextField m_oTextField_EditCategory_Description;
    
    private ExtendedDataItemModel createEditCategoryPanel()
    {
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_EditCategory_Kategorie") + " ",null); //$NON-NLS-1$
        m_oComboBox_EditCategory_Kategorie = createEntryComboBox(getKategorien(false, true, true).toArray());
        m_oComboBox_EditCategory_Kategorie.addActionListener(new ActionListener() 
                {
            public void actionPerformed(ActionEvent e) 
            {
                updateEditCategoryPanel();
            }
                });
        
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_EditCategory_Kategorie);
        
        // ----------------------------------
        
        JLabel keyLabel = createEntryLabel(Messages.getString("GUI_Admin_EditCategory_Key") + " ",null); //$NON-NLS-1$
        m_oTextField_EditCategory_Key = createEntryTextField("", m_iMaxLength_EditCategory_Key); //$NON-NLS-1$
        JPanel keyPanel = createEntryPanel(keyLabel, m_oTextField_EditCategory_Key);
        
        JLabel nameLabel = createEntryLabel(Messages.getString("GUI_Admin_EditCategory_Name") + " ",null); //$NON-NLS-1$
        m_oTextField_EditCategory_Name = createEntryTextField("", m_iMaxLength_EditCategory_Name); //$NON-NLS-1$
        JPanel namePanel = createEntryPanel(nameLabel, m_oTextField_EditCategory_Name);
        
        JLabel descriptionLabel = createEntryLabel(Messages.getString("GUI_Admin_EditCategory_Description") + " ",null); //$NON-NLS-1$
        m_oTextField_EditCategory_Description = createEntryTextField("", m_iMaxLength_EditCategory_Description); //$NON-NLS-1$
        JPanel descriptionPanel = createEntryPanel(descriptionLabel, m_oTextField_EditCategory_Description);
        
        JPanel editCategoryEntryPanel = createEntriesPanel();
        editCategoryEntryPanel.add(kategoriePanel);    
        if (m_bShowVerteilerKey) editCategoryEntryPanel.add(keyPanel);
        editCategoryEntryPanel.add(namePanel);
        editCategoryEntryPanel.add(descriptionPanel);
        
        //-----------------------
        JPanel editCategoryPanel = new JPanel(new BorderLayout());
        editCategoryPanel.add(editCategoryEntryPanel, BorderLayout.CENTER);
        
        JButton button_editCategory = createButton(Messages.getString("GUI_Admin_EditCategory_Aendern"), new button_editcategory_clicked()); //$NON-NLS-1$
        
        editCategoryPanel.add(createButtonPanel(button_editCategory), BorderLayout.EAST);
        
        //----------------------------------
        
        updateEditCategoryPanel();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_EditCategory"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", editCategoryPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_EDIT));
        return item;    
    }
    
    
    private class button_editcategory_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String gruppe = getKeyFromComboBoxEntry(m_oComboBox_EditCategory_Kategorie);
            String key    = m_oTextField_EditCategory_Key.getText().trim();
            String name  = m_oTextField_EditCategory_Name.getText().trim();
            String description  = m_oTextField_EditCategory_Description.getText().trim();
            if ((name.length() > 0) && (gruppe != null))
            {
                m_bCategoryChanged = false;
                m_oGUIListener.setWaiting(true);
                if (key.length() == 0)
                {
                    key = null;      
                    addLogText("\u00E4ndere Kategorie mit automatisch generiertem Schl\u00FCssel und dem Namen \"" + name + "\"... ");
                }
                else
                {
                    addLogText("\u00E4ndere Kategorie mit Schl\u00FCssel \"" + key +"\"" + " und dem Namen \"" + name + "\"... ");
                }
                
                m_oGUIListener.userRequestEditCategory(gruppe, name, description);
                
                showResult(m_bCategoryChanged); 
                addLogTextEndMarker();
                updateDeleteVerteilerBox();
                updateShowVerteilerList();
                updateUserKategorienBoxes();
				updateRightsManagementBoxes();
				updateRightsManagementPanel();
                m_oGUIListener.setWaiting(false);
            }
            else
            {
                m_oControlListener.showMessage("Sie m\u00FCssen mindestens einen Namen angeben!");
                m_oTextField_EditCategory_Key.requestFocus(true);
            }      
        }
    }
    
    
    
    protected void updateEditCategoryPanel() 
    {
        String kategorie = getKeyFromComboBoxEntry(m_oComboBox_EditCategory_Kategorie);    
        
        if (kategorie != null)
        {      
            Set kategorien = m_oGUIListener.getAllCategories().entrySet();
            
            Category categorytochange = null;
            if (kategorien != null)
            {  
                Iterator kategorieniterator = kategorien.iterator();
                while(kategorieniterator.hasNext())
                {
                    Map.Entry entry = (Map.Entry) kategorieniterator.next();
                    Category category = (Category)(entry.getValue());
                    if (kategorie.equals(category.getIdAsString()))
                        {
                            categorytochange = category;
                        }
                }
            }
            
            if (categorytochange != null)
            {
                m_oTextField_EditCategory_Key.setText(categorytochange.getIdAsString());
                m_oTextField_EditCategory_Name.setText(categorytochange.getName());
                m_oTextField_EditCategory_Description.setText(categorytochange.getDescription());        
            }
            else
            {
                // ist nicht mehr in liste...
                m_oGUIListener.getLogger().log(Level.WARNING, "CLEAR FIELDS!!!");
                m_oTextField_EditCategory_Key.setText("");
                m_oTextField_EditCategory_Name.setText("");
                m_oTextField_EditCategory_Description.setText("");        
            }      
        }    
    }

    private JComboBox m_oComboBox_ShowKategorie_Kategorie;
    private JComboBox m_oComboBox_ShowKategorie_UserID;
    
    private ExtendedDataItemModel createShowCategoryPanel()
    {
        JLabel userLabel = createEntryLabel(Messages.getString("GUI_Admin_Zuordnung_UserID") + " ",null); //$NON-NLS-1$
        m_oComboBox_ShowKategorie_UserID = createEntryComboBox(getBenutzer().toArray());
        m_oComboBox_ShowKategorie_UserID.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {          
                updateShowKategorieBox();
            }
        });
        JPanel userPanel = createEntryPanel(userLabel, m_oComboBox_ShowKategorie_UserID);
        
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_Zuordnung_Kategorie") + " ",null); //$NON-NLS-1$
        String user = getValueFromComboBoxEntry(m_oComboBox_ShowKategorie_UserID);
        List kategorien = getKategorien();
        kategorien.removeAll(getUserKategorien(user));
        m_oComboBox_ShowKategorie_Kategorie = createEntryComboBox(kategorien.toArray());
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_ShowKategorie_Kategorie);
        
        JPanel showKategorieEntryPanel = createEntriesPanel();
        showKategorieEntryPanel.add(userPanel);
        showKategorieEntryPanel.add(kategoriePanel);
        
        //         JButton button_showKategorie = createButton(Messages.getString("GUI_Admin_Zuordnung_Ausfuehren"), new button_showKategorie_clicked()); //$NON-NLS-1$
        //         button_showKategorie.setCursor(m_oHandCursor);
        
        JPanel setStandardkategoriePanel = new JPanel(new BorderLayout());
        setStandardkategoriePanel.add(showKategorieEntryPanel, BorderLayout.CENTER);
        //setStandardkategoriePanel.add(createButtonPanel(button_showKategorie), BorderLayout.EAST);
        
        updateShowKategorieBox();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Zuordnung_Kategorie_zuordnen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", setStandardkategoriePanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_SHOW));
        return item;    
    }
    
    
    
    // --------------------------------------------------------
    
    private JComboBox m_oComboBox_HideKategorie_Kategorie;
    private JComboBox m_oComboBox_HideKategorie_UserID;
    
    private ExtendedDataItemModel createHideCategoryPanel()
    {
        JLabel userLabel = createEntryLabel(Messages.getString("GUI_Admin_Loeschen_UserID") + " ",null); //$NON-NLS-1$
        m_oComboBox_HideKategorie_UserID = createEntryComboBox(getBenutzer().toArray());
        m_oComboBox_HideKategorie_UserID.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateHideKategorieBox();
            }
        });
        
        String user = getValueFromComboBoxEntry(m_oComboBox_HideKategorie_UserID);
        List kategorien = getUserKategorien(user);    
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_Loeschen_Kategorie") + " ",null); //$NON-NLS-1$
        m_oComboBox_HideKategorie_Kategorie = createEntryComboBox(kategorien.toArray());
        
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_HideKategorie_Kategorie);
        JPanel userPanel = createEntryPanel(userLabel, m_oComboBox_HideKategorie_UserID);
        
        JPanel deleteKategorieEntryPanel = createEntriesPanel();
        deleteKategorieEntryPanel.add(userPanel);
        deleteKategorieEntryPanel.add(kategoriePanel);
        
        JButton button_deleteKategorie = createButton(Messages.getString("GUI_Admin_Loeschen_Ausfuehren"), new button_hideKategorie_clicked()); //$NON-NLS-1$
        
        JPanel delKategoriePanel = new JPanel(new BorderLayout());
        delKategoriePanel.add(deleteKategorieEntryPanel, BorderLayout.CENTER);
        delKategoriePanel.add(createButtonPanel(button_deleteKategorie), BorderLayout.EAST);
        
        updateHideKategorieBox();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Loeschen_Kategorie_zuordnen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", delKategoriePanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CATEGORY_HIDE));
        return item;    
    }
    
    public class button_hideKategorie_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {          
            String gruppe  = getKeyFromComboBoxEntry(m_oComboBox_HideKategorie_Kategorie);
            String user    = getValueFromComboBoxEntry(m_oComboBox_HideKategorie_UserID);
            m_bCategoryDeassigned = false;
            m_oGUIListener.setWaiting(true);      
            addLogText("entferne Benutzer \"" + user + "\" aus der Kategorie \"" + gruppe + "\"... ");
            //m_oGUIListener.userRequestHideCategory(gruppe, user);
            updateStandardKategorieBox();
            updateShowKategorieBox();
            updateHideKategorieBox();
            showResult(m_bCategoryDeassigned); 
            addLogTextEndMarker();
            m_oGUIListener.setWaiting(false);
        }
    }
    
    
    // --------------------------------------------------------
    
    private JComboBox m_oComboBox_SetStandardkategorie_Kategorie;
    private JComboBox m_oComboBox_SetStandardkategorie_UserID;
    
    private ExtendedDataItemModel createSetStandardkategoriePanel()
    {
        JLabel userLabel = createEntryLabel(Messages.getString("GUI_Admin_Standardkategorie_UserID") + " ",null); //$NON-NLS-1$
        m_oComboBox_SetStandardkategorie_UserID = createEntryComboBox(getBenutzer().toArray());
        m_oComboBox_SetStandardkategorie_UserID.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                m_oGUIListener.setWaiting(true);
                updateStandardKategorieBox();
                m_oGUIListener.setWaiting(false);
            }
        });
        JPanel userPanel = createEntryPanel(userLabel, m_oComboBox_SetStandardkategorie_UserID);
        
        JLabel kategorieLabel = createEntryLabel(Messages.getString("GUI_Admin_Standardkategorie_Kategorie") + " ",null); //$NON-NLS-1$
        m_oComboBox_SetStandardkategorie_Kategorie = createEntryComboBox(getKategorien().toArray());
        JPanel kategoriePanel = createEntryPanel(kategorieLabel, m_oComboBox_SetStandardkategorie_Kategorie);
        
        JPanel setStandardkategorieEntryPanel = createEntriesPanel();
        setStandardkategorieEntryPanel.add(userPanel);
        setStandardkategorieEntryPanel.add(kategoriePanel);
        
        JButton button_setStandardkategorie = createButton(Messages.getString("GUI_Admin_Standardkategorie_Ausfuehren"), new button_setStandardkategorie_clicked()); //$NON-NLS-1$
        
        JPanel setStandardkategoriePanel = new JPanel(new BorderLayout());
        setStandardkategoriePanel.add(setStandardkategorieEntryPanel, BorderLayout.CENTER);
        setStandardkategoriePanel.add(createButtonPanel(button_setStandardkategorie), BorderLayout.EAST);
        
        updateStandardKategorieBox();
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Standardkategorie_setzen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", setStandardkategoriePanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_STDCATEGORY_SET));
        return item;    
    }
    
    public class button_setStandardkategorie_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String gruppe  = getKeyFromComboBoxEntry(m_oComboBox_SetStandardkategorie_Kategorie);
            String user    = getValueFromComboBoxEntry(m_oComboBox_SetStandardkategorie_UserID);
            m_bStandardCategoryAssigned = false;
            m_oGUIListener.setWaiting(true);
            addLogText("setze Standardkategorie des Benutzers \"" + user + "\" auf die Kategorie \"" + gruppe + "\"... ");
            showResult(m_bStandardCategoryAssigned); 
            addLogTextEndMarker();
            m_oGUIListener.setWaiting(false);
        }
    }
    
    private JTextField m_oTextField_CreateUser_UserID;
    private JTextField m_oTextField_CreateUser_UserName;
    private JTextField m_oTextField_CreateUser_UserFamilyName;
    private JTextField m_oTextField_CreateUser_Password;
    private JTextField m_oTextField_CreateUser_Password2;
    private JTextField m_oTextField_CreateUser_Email;
    //private JCheckBox m_oCheckBox_CreateUser_Special;
    
    
    private JTextField m_oTextField_CreateGroup_GroupID;
    private JTextField m_oTextField_CreateGroup_Description;
    private JComboBox m_oComboBox_CreateGroup_UserID;
    private JTextField m_oTextField_EditGroup_GroupID;
    private JTextField m_oTextField_EditGroup_Description;
    private JComboBox m_oComboBox_EditGroup_GroupID;
    private JComboBox m_oComboBox_EditGroup_RoleID;
    private boolean isInUpdate_m_oComboBox_EditGroup_RoleID = false;
    private JComboBox m_oComboBox_EditGroup_UserID;
    private tarentList GroupAdminList;
    private tarentList GroupUserList;
    private JComboBox m_oComboBox_DeleteGroup_GroupID;
    private JCheckBox m_oCheckBox_CreateGroup_private;
    private tarentList allGroupUsersList;
    private tarentList allowedGroupUsersList;
    
    private UserGroup _currentUserGroup;
    private tarentList allProxyUsersList;
    private tarentList allowedProxyUsersList;
	private JComboBox	m_oComboBox_CreateGroup_RoleID;
    
    private ExtendedDataItemModel createCreateUserPanel() 
    {
        JLabel userIdLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_UserID") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateUser_UserID = createEntryTextField("", m_iMaxLength_CreateUser_UserID); //$NON-NLS-1$
        JPanel userIdPanel = createEntryPanel(userIdLabel, m_oTextField_CreateUser_UserID);
        
        JLabel userNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_UserName") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateUser_UserName = createEntryTextField("", m_iMaxLength_CreateUser_UserName); //$NON-NLS-1$
        JPanel userNamePanel = createEntryPanel(userNameLabel, m_oTextField_CreateUser_UserName);
        
        JLabel userFamilyNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_UserFamilyName") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateUser_UserFamilyName = createEntryTextField("", m_iMaxLength_CreateUser_UserFamilyName); //$NON-NLS-1$
        JPanel userFamilyNamePanel = createEntryPanel(userFamilyNameLabel, m_oTextField_CreateUser_UserFamilyName);
        
        JLabel passwordLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Password") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateUser_Password = new JPasswordField();// createEntryTextField(""); //$NON-NLS-1$
        addLengthRestriction(m_oTextField_CreateUser_Password, m_iMaxLength_CreateUser_Password);
        JPanel passwordPanel = createEntryPanel(passwordLabel, m_oTextField_CreateUser_Password);
        
        JLabel passwordLabel2 = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Password2") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateUser_Password2 = new JPasswordField();// createEntryTextField(""); //$NON-NLS-1$
        addLengthRestriction(m_oTextField_CreateUser_Password2, m_iMaxLength_CreateUser_Password);
        JPanel passwordPanel2 = createEntryPanel(passwordLabel2, m_oTextField_CreateUser_Password2);
        
        JLabel emailLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_UserEmail") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateUser_Email = createEntryTextField("", m_iMaxLength_CreateUser_Email); //$NON-NLS-1$
        JPanel emailPanel = createEntryPanel(emailLabel, m_oTextField_CreateUser_Email);
        
        //JLabel specialLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Versandgruppe") + " ",null); //$NON-NLS-1$
        //m_oCheckBox_CreateUser_Special = createEntryCheckBox(false, Messages.getString("GUI_Admin_Benutzer_erweiterte_Nutzung")); //$NON-NLS-1$
       // JPanel specialPanel = createEntryPanel(specialLabel, m_oCheckBox_CreateUser_Special);
        
        JPanel createUserEntryPanel = createEntriesPanel();
        createUserEntryPanel.add(userIdPanel);
        createUserEntryPanel.add(userNamePanel);
        createUserEntryPanel.add(userFamilyNamePanel);
        createUserEntryPanel.add(passwordPanel);
        createUserEntryPanel.add(passwordPanel2);
        createUserEntryPanel.add(emailPanel);
        /*
        if (m_bShowSpecialFlag) createUserEntryPanel.add(specialPanel);
        createUserEntryPanel.add(adminPanel);
        */
        
        JButton button_createUser = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"), new button_createUser_clicked()); //$NON-NLS-1$
        
        JPanel createUserPanel = new JPanel(new BorderLayout());
        createUserPanel.add(createUserEntryPanel, BorderLayout.CENTER);
        createUserPanel.add(createButtonPanel(button_createUser), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Benutzer_anlegen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", createUserPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_USER_CREATE));
        return item;    
    }
    
    private ExtendedDataItemModel createCreateGroupPanel() 
    {
        JLabel GroupNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_GroupName") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateGroup_GroupID = createEntryTextField("", m_iMaxLength_CreateGroup_GroupID); //$NON-NLS-1$
        JPanel groupIdPanel = createEntryPanel(GroupNameLabel, m_oTextField_CreateGroup_GroupID);
        
        JLabel GroupDescriptionLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_GroupDescription") + " ",null); //$NON-NLS-1$
        m_oTextField_CreateGroup_Description = createEntryTextField("", m_iMaxLength_CreateGroup_Description); //$NON-NLS-1$
        JPanel groupDescriptionPanel = createEntryPanel(GroupDescriptionLabel, m_oTextField_CreateGroup_Description);
        
        JLabel RoleDescriptionLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_RoleDescription") + " ",null); //$NON-NLS-1$
        m_oComboBox_CreateGroup_RoleID = createEntryComboBox(getGlobalRoles().toArray());
        JPanel tmpPanel1wrap = createEntryPanel(RoleDescriptionLabel, m_oComboBox_CreateGroup_RoleID);

        
        JPanel createGroupEntryPanel = createEntriesPanel();
        createGroupEntryPanel.add(groupIdPanel);
        createGroupEntryPanel.add(groupDescriptionPanel);
        createGroupEntryPanel.add(tmpPanel1wrap);
        
        JButton button_createGroup = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"), new button_createGroup_clicked()); //$NON-NLS-1$
        
        JPanel createGroupPanel = new JPanel(new BorderLayout());
        createGroupPanel.add(createGroupEntryPanel, BorderLayout.CENTER);
        createGroupPanel.add(createButtonPanel(button_createGroup), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Gruppe_anlegen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", createGroupPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_GROUP_CREATE));
        return item;    
    }
    
    private ExtendedDataItemModel createEditGroupPanel() 
    {
        JLabel GroupNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_GroupName") + " ",null); //$NON-NLS-1$
        m_oComboBox_EditGroup_GroupID = createEntryComboBox(getBenutzerGruppen().toArray());
        m_oComboBox_EditGroup_GroupID.addActionListener(new ActionListener(){
           
            public void actionPerformed(ActionEvent e) {
                
                updateEditGroupPanel();
            }});
        
        
        JPanel groupIdPanel = createEntryPanel(GroupNameLabel, m_oComboBox_EditGroup_GroupID);
        
        
        m_oTextField_EditGroup_GroupID = createEntryTextField("", m_iMaxLength_EditGroup_GroupID); //$NON-NLS-1$
        JPanel groupIdTextPanel = createEntryPanel(GroupNameLabel, m_oTextField_EditGroup_GroupID);
        
        JLabel GroupDescriptionLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_GroupDescription") + " ",null); //$NON-NLS-1$
        m_oTextField_EditGroup_Description = createEntryTextField("", m_iMaxLength_EditGroup_Description); //$NON-NLS-1$
        JPanel tmpPanelwrap = createEntryPanel(GroupDescriptionLabel, m_oTextField_EditGroup_Description);
        
        JLabel RoleDescriptionLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_RoleDescription") + " ",null); //$NON-NLS-1$
        m_oComboBox_EditGroup_RoleID = createEntryComboBox(new RoleComboboxModel(getGlobalRoles()));
        m_oComboBox_EditGroup_RoleID.addItemListener(new ItemListener()
        {
           public void itemStateChanged(ItemEvent e)
           {
               if(e.getStateChange() == ItemEvent.SELECTED)
               {
                   boolean newSelectedRoleAdminPrivilegs =  ((RoleComboboxModel)m_oComboBox_EditGroup_RoleID.getModel()).hasSelectedRoleAdminPrivilegs();
                   String selGroup     = (String) m_oComboBox_EditGroup_GroupID.getSelectedItem();
                   
                   if(!isInUpdate_m_oComboBox_EditGroup_RoleID && adminGroups.contains(selGroup) && !newSelectedRoleAdminPrivilegs)
                       {
                           isInUpdate_m_oComboBox_EditGroup_RoleID = true;
                           ((RoleComboboxModel)m_oComboBox_EditGroup_RoleID.getModel()).selectAdminRole();
                           isInUpdate_m_oComboBox_EditGroup_RoleID = false;
                           JOptionPane.showMessageDialog(null, Messages.getString("GUI_Admin_Message_Admin_Recht_entziehen"));
                           return;                   
                       }               
               }
           } 
        });
        JPanel tmpPanel1wrap = createEntryPanel(RoleDescriptionLabel, m_oComboBox_EditGroup_RoleID);
        
        JPanel groupDescriptionPanel = createEntriesPanel();
        groupDescriptionPanel.add(tmpPanelwrap);
        groupDescriptionPanel.add(tmpPanel1wrap);
        groupDescriptionPanel.add(Box.createVerticalStrut(20));
        
        
        //  Linke Listbox
        JLabel allUsersLabel = createEntryViewField(Messages.getString("GUI_Admin_Gruppe_AVAILUSERS"));
       
        allGroupUsersList = createEntryList(null, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        JPanel tmpPanel1 = createEntriesPanel();
        tmpPanel1.add(allUsersLabel);
        tmpPanel1.add(new JScrollPane(allGroupUsersList));
        
        //Buttons Mitte      
        JButton button_addUser = createButton(Messages.getString("GUI_Admin_Gruppe_USERADD"),new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (allGroupUsersList.getSelectedIndices().length > 0 && _currentUserGroup != null){
                    try {
                    	int[] indices = allGroupUsersList.getSelectedIndices();
                    	
                    	Object[] selectedElements = new Object[indices.length];
                        //Gruppe updaten aber nicht comitten
                    	for (int i = 0; i < indices.length; i++){
                    		selectedElements[i] = allGroupUsersList.getModel().getElementAt(indices[i]);
                    		User tmpuser = m_oGUIListener.getUser((String) selectedElements[i]);
                    		_currentUserGroup.add(tmpuser);
                    		allowedGroupUsersList.insertElement(allGroupUsersList.getDefaultModel().getElementAt(indices[i]));                    		
                    	}
                    	
                    	for (int i = 0; i < selectedElements.length; i++){
                    		allGroupUsersList.getDefaultModel().removeElement(selectedElements[i]);
                    	}
                    } catch (ContactDBException e1) {
                        e1.printStackTrace();
                    }
                    
                    //Sort List after adding new entries
                    Object[] _tmpsort = allowedGroupUsersList.getDefaultModel().toArray(); 
                    Arrays.sort(_tmpsort, new ToStringComparator());
                    allowedGroupUsersList.getDefaultModel().removeAllElements();
                    allowedGroupUsersList.insertElements(_tmpsort);
                    
                }
            }});
        button_addUser.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton button_deleteUser = createButton(Messages.getString("GUI_Admin_Gruppe_USERDEL"), new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (allowedGroupUsersList.getSelectedIndices().length > 0 && _currentUserGroup!=null){
                    try {
                    	int[] indices = allowedGroupUsersList.getSelectedIndices();
                        Object[] selectedElements = new Object[indices.length];
//                    	Gruppe updaten aber nicht comitten
                    	for (int i = 0; i < indices.length; i++){                   	
                                            	
                    		selectedElements[i] = allowedGroupUsersList.getModel().getElementAt(indices [i]);
                    		User tmpuser = m_oGUIListener.getUser((String) selectedElements[i]);
                    		// Bei der Admingruppe kontrollieren, dass nicht der Admin heraus genommen wird
                            if (_currentUserGroup.isAdminGroup() && tmpuser.isAdminUser()) {
                                m_oControlListener.showMessage("Der Benutzer admin kann nicht aus der admin-Gruppe entfernt werden!");
                                return;
                            }
                            _currentUserGroup.remove(tmpuser);
                            allGroupUsersList.insertElement(allowedGroupUsersList.getDefaultModel().getElementAt(indices[i]));
                        }
                    	
                    	for (int i = 0;  i < selectedElements.length; i++){
                    	    allowedGroupUsersList.getDefaultModel().removeElement(selectedElements[i]);                    	
                	    }
                        
                    } catch (ContactDBException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    //Gui
                    Object[] _tmpsort = allGroupUsersList.getDefaultModel().toArray();
                    Arrays.sort(_tmpsort, new ToStringComparator());
                    allGroupUsersList.getDefaultModel().removeAllElements();
                    allGroupUsersList.insertElements(_tmpsort);
                }
            }});
        button_deleteUser.setAlignmentX(Component.CENTER_ALIGNMENT);
        JPanel tmpPanel2 = createEntriesPanel();
        tmpPanel2.add(Box.createVerticalStrut(20));
        tmpPanel2.add(button_addUser);
        tmpPanel2.add(Box.createVerticalStrut(10));
        tmpPanel2.add(button_deleteUser);
        tmpPanel2.add(Box.createVerticalStrut(20));
        JPanel emptyPanel1 = createEntriesPanel();
        emptyPanel1.add(Box.createHorizontalStrut(20));
        JPanel emptyPanel2 = createEntriesPanel();
        emptyPanel2.add(Box.createHorizontalStrut(20));
        
        //Rechte Listbox  
        JLabel allowedUsersLabel = createEntryViewField(Messages.getString("GUI_Admin_Gruppe_AUTHORIZEDUSERS"));
        allowedGroupUsersList = createEntryList(null, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JPanel tmpPanel3 = createEntriesPanel();
        tmpPanel3.add(allowedUsersLabel);
        tmpPanel3.add(new JScrollPane(allowedGroupUsersList));
        JPanel tmpPanel4 = createGenericPanel_XAxis(new JComponent[]{tmpPanel1,emptyPanel1,tmpPanel2,emptyPanel2,tmpPanel3},true);
        
        
        
        JPanel createGroupEntryPanel = createEntriesPanel();
        createGroupEntryPanel.add(groupIdPanel);
        createGroupEntryPanel.add(groupIdTextPanel);
        createGroupEntryPanel.add(groupDescriptionPanel);
        createGroupEntryPanel.add(tmpPanel4);
        
        JButton button_createGroup = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"), new button_editGroup_clicked()); //$NON-NLS-1$
        
        JPanel createGroupPanel = new JPanel(new BorderLayout());
        createGroupPanel.add(createGroupEntryPanel, BorderLayout.CENTER);
        createGroupPanel.add(createButtonPanel(button_createGroup), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Gruppe_editieren"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", createGroupPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_GROUP_CREATE));
		
		updateEditGroupPanel();
        return item;    
    }
    
    
    
    
    private ExtendedDataItemModel createDeleteGroupPanel() 
    {
        JLabel GroupNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Gruppe_GroupName") + " ",null); //$NON-NLS-1$
        m_oComboBox_DeleteGroup_GroupID = createEntryComboBox(new UpdateableComboboxModel( getBenutzerGruppen(false, false).toArray()));
        JPanel groupCBPanel = createEntryPanel(GroupNameLabel, m_oComboBox_DeleteGroup_GroupID);
        
        JButton button_deleteGroup = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"), new button_deleteGroup_clicked()); //$NON-NLS-1$
        
        JPanel deleteGroupPanel = new JPanel(new BorderLayout());
        deleteGroupPanel.add(groupCBPanel, BorderLayout.CENTER);
        deleteGroupPanel.add(createButtonPanel(button_deleteGroup), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Group_Delete"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", deleteGroupPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_GROUP_DELETE));
        return item;    
    }
    
   
    private GRTableModel m_tmodelGlobalRoles;
    private TableModel categoryRolesTableModel;
    private RMTableModel m_tmodelRM;
    private GR_RoleTable m_tableGlobalRoles;
//    private FR_RoleTable categoryRolesTable;
    private RMTable m_tableRM;
    private JButton button_delNewGlobalRole;

    private Vector m_vectorGlobalRolesData;
	private Vector m_vectorGlobalRoleRowKeys;
    private Vector categoryRoles;
    private Vector m_vectorRMData;
    private Vector m_vectorRMfilteredData;
        
    private Vector m_vectorGlobalRolesHeader;
    private Vector categoryRoleRightsTableColumnNames;
    private Vector m_vectorRMHeader;
    
    private Vector _oneGRRow;
    private Vector _oneFRRow;
    private JComboBox m_oComboBox_RMgroup;
    private JComboBox m_oComboBox_RMrole;
    private JComboBox m_oComboBox_RMobject;
    
    private JTextField m_otextfield_RM_filterGruppe;
    private JTextField m_otextfield_RM_filterRolle;
    private JTextField m_otextfield_RM_filterObject;
    
    private RM_filterchanged _RM_selectionListener;
    private JLabel m_olabel_defRights;
    
    private ExtendedDataItemModel createGlobalRolePanel() 
    {
        
        m_vectorGlobalRolesHeader = new Vector();
        m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Name")); //$NON-NLS-1$    
        m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht1")); //$NON-NLS-1$
        m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht2")); //$NON-NLS-1$
        m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht3")); //$NON-NLS-1$
        m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht4")); //$NON-NLS-1$
        if (showPanel(CALENDARPROXY)){
        	m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht5")); //$NON-NLS-1$
        	m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht6")); //$NON-NLS-1$
        }
        m_vectorGlobalRolesHeader.add(Messages.getString("GUI_Admin_Globale_Rolle_Recht7")); //$NON-NLS-1$
        
        	
        m_vectorGlobalRolesData = new Vector();
        m_tmodelGlobalRoles = new GRTableModel();
        m_tmodelGlobalRoles.setDataVector(m_vectorGlobalRolesData,m_vectorGlobalRolesHeader);

        m_tableGlobalRoles = new GR_RoleTable(m_tmodelGlobalRoles);
        m_tableGlobalRoles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_tableGlobalRoles.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    int index = m_tableGlobalRoles.getSelectedRow();
                    if(index > -1) {
						Object role_o = m_tmodelGlobalRoles.getValueAt(index, showPanel(CALENDARPROXY) ? 8 : 6);
						if(role_o instanceof GlobalRole
                           && ((GlobalRole)role_o).isAdminRole()) {
                            
                            button_delNewGlobalRole.setEnabled( false );
                            return;                            
                        }
                    }
                    button_delNewGlobalRole.setEnabled( true );
                }
            });
        	

        JScrollPane _ScrollPane = new JScrollPane(m_tableGlobalRoles);
        	_ScrollPane.setPreferredSize(new Dimension(10,150));
        
        JButton button_createGlobalRole = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"),new button_saveGRole_clicked()); //$NON-NLS-1$

        JButton button_addNewGlobalRole = createButton(Messages.getString("GUI_Admin_Globale_Rolle_neu"), new button_newGRole_clicked()); //$NON-NLS-1$
        button_delNewGlobalRole = createButton(Messages.getString("GUI_Admin_Globale_Rolle_del"), new button_delGRole_clicked()); //$NON-NLS-1$
        JPanel buttonPanel = createButtonPanel_XAxis(new JButton[]{button_addNewGlobalRole,button_delNewGlobalRole});

        JPanel createGlobalRolePanel = new JPanel(new BorderLayout());
        createGlobalRolePanel.add(_ScrollPane,BorderLayout.CENTER);
        createGlobalRolePanel.add(buttonPanel,BorderLayout.SOUTH);
        createGlobalRolePanel.add(createButtonPanel(button_createGlobalRole), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Globale_Rollen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", createGlobalRolePanel));
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_ROLE_GR));
		updateGlobalRoleBox();
        return item;    
    }


    private JButton button_deleteRight;
    private ExtendedDataItemModel createRightsManagementPanel() 
    {
    	// ComboBoxes
        try {
			m_oComboBox_RMgroup = createEntryComboBox(getBenutzerGruppenObjects(true).toArray());
			m_oComboBox_RMrole = createEntryComboBox(ObjectRoleImpl.getObjectRoles(null).toArray());
	        m_oComboBox_RMobject = createEntryComboBox(getGrantableCategoryObjects().toArray());
		} catch (ContactDBException e) {
			logger.warning("Fehler beim Beziehen der Detaildaten!");
			ApplicationServices.getInstance().getCommonDialogServices().publishError("Fehler beim Beziehen der Detaildaten!", e);
		}
        JPanel _comboPanel = createGenericPanel_XAxis(new JComponent[]{m_oComboBox_RMgroup,(JComponent) Box.createHorizontalGlue(),m_oComboBox_RMrole,(JComponent) Box.createHorizontalGlue(),m_oComboBox_RMobject},false);

        JButton button_createRight = createButton(Messages.getString("GUI_Admin_Recht_anlegen"),new button_add_Right_clicked()); //$NON-NLS-1$
		button_deleteRight = createButton(Messages.getString("GUI_Admin_Right_Delete"), new button_delete_Right_clicked()); //$NON-NLS-1$
        JPanel _addpanel = createButtonPanel_XAxis(new JButton[]{button_createRight, button_deleteRight});
        
        m_olabel_defRights = createEntryViewField(Messages.getString("GUI_Admin_Recht_definierte_Rechte"));
        JPanel _labelpanel = createGenericPanel_XAxis(new JComponent[]{m_olabel_defRights},true);
        
        //Tabelle        
        m_vectorRMHeader = new Vector();
        m_vectorRMHeader.add(Messages.getString("GUI_Admin_RM_Gruppe")); //$NON-NLS-1$    
        m_vectorRMHeader.add(Messages.getString("GUI_Admin_RM_Rolle")); //$NON-NLS-1$
        m_vectorRMHeader.add(Messages.getString("GUI_Admin_RM_Objekt")); //$NON-NLS-1$

        m_vectorRMData         = new Vector();
        m_vectorRMfilteredData = new Vector();
        m_tmodelRM             = new RMTableModel();
        m_tmodelRM.setDataVector(m_vectorRMData,m_vectorRMHeader);
        

        m_tableRM = new RMTable(m_tmodelRM);
        m_tableRM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_tableRM.getSelectionModel().addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                int index = m_tableRM.getSelectedRow();
                if(index > -1)
                {
                    GroupObjectRoleConnector connector =  m_tmodelRM.getRow(index);
                    
                    if (connector.getGroup().isAdminGroup())
                        {
                            button_deleteRight.setEnabled( false );
                            return;
                        }
                    button_deleteRight.setEnabled( true );
                }
            }
        });
        
        JScrollPane _ScrollPane = new JScrollPane(m_tableRM);
        	_ScrollPane.setPreferredSize(new Dimension(10,150));
        
        //Documentlistener instantiieren
        _RM_selectionListener = new RM_filterchanged();
        m_otextfield_RM_filterGruppe = createEntryTextField("", m_iMaxLength_RM_filterGruppe);
        	m_otextfield_RM_filterGruppe.getDocument().addDocumentListener(_RM_selectionListener);
        m_otextfield_RM_filterRolle = createEntryTextField("", m_iMaxLength_RM_filterRolle);
        	m_otextfield_RM_filterRolle.getDocument().addDocumentListener(_RM_selectionListener);
        m_otextfield_RM_filterObject = createEntryTextField("", m_iMaxLength_RM_filterObject);
        	m_otextfield_RM_filterObject.getDocument().addDocumentListener(_RM_selectionListener);
        
        JPanel _filterPanel = createGenericPanel_XAxis(new JComponent[]{m_otextfield_RM_filterGruppe,m_otextfield_RM_filterRolle,m_otextfield_RM_filterObject},false);

        JLabel _filterLabel = createEntryViewField(Messages.getString("GUI_Admin_Recht_Filter"));
        JPanel _filterLabelPanel = createGenericPanel_XAxis(new JComponent[]{_filterLabel},true);

        JLabel _addLabel = createEntryViewField(Messages.getString("GUI_Admin_Recht_definieren"));
        JPanel _addLabelPanel = createGenericPanel_XAxis(new JComponent[]{_addLabel},true);

        JPanel _rmmainpanel = createEntriesPanel();
        	_rmmainpanel.add(_labelpanel);
        	_rmmainpanel.add(_ScrollPane);
        	_rmmainpanel.add(_filterLabelPanel);
        	_rmmainpanel.add(_filterPanel);
        	_rmmainpanel.add(_addLabelPanel);
        	_rmmainpanel.add(_comboPanel);
        	_rmmainpanel.add(_addpanel);
        	
        	
        
        JButton button_updateRights = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"), new button_saveRM_clicked()); //$NON-NLS-1$
        
        JPanel RMPanel = new JPanel(new BorderLayout());
        RMPanel.add(_rmmainpanel,BorderLayout.CENTER);
        RMPanel.add(createButtonPanel(button_updateRights), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Rights"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", RMPanel));
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_ROLE_FR));
		
		updateRightsManagementPanel();
        return item;    
    }

    private final class GR_RoleTable extends JTable {
        final String[] _tooltips = {
        		Messages.getString("GUI_Admin_Globale_Rolle_Name_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht1_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht2_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht3_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht4_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht5_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht6_ToolTip"),
        		Messages.getString("GUI_Admin_Globale_Rolle_Recht7_ToolTip")};

        private GR_RoleTable(TableModel dm) {
            super(dm);
        }
        public String getToolTipText(MouseEvent e) {
            
            java.awt.Point p = e.getPoint();
            int colIndex = columnAtPoint(p);
            int realColumnIndex = convertColumnIndexToModel(colIndex);
            
            return _tooltips[realColumnIndex];
        }
        
        protected JTableHeader createDefaultTableHeader() {
            return new JTableHeader(columnModel) {
                
                public TableCellRenderer getDefaultRenderer() {
                    return new MyGRHeaderRenderer();
                }
                
                public String getToolTipText(MouseEvent e) {
                    java.awt.Point p = e.getPoint();
                    int colIndex = columnAtPoint(p);
                    int realColumnIndex = convertColumnIndexToModel(colIndex);
                    
                    return _tooltips[realColumnIndex];
                }
            };
        }
    }

    private final class RMTable extends JTable {
        final String[] _tooltips = {
                "<html>Gruppe<b>, f?r die die Berechtigung gilt</b></html>",
                "<html>Rolle<b> mit den Berechtigungen, die die Gruppe auf die Kategorie hat</b></html>",
                "<html>Kategorie<b>, auf der die Berechtigung gilt</b></html>"};

        private RMTable(TableModel dm) {
            super(dm);
        }
        public String getToolTipText(MouseEvent e) {
            
            java.awt.Point p = e.getPoint();
            int rowIndex = rowAtPoint(p);
            int colIndex = columnAtPoint(p);
            int realColumnIndex = convertColumnIndexToModel(colIndex);
            
            return _tooltips[realColumnIndex];
        }
        
        protected JTableHeader createDefaultTableHeader() {
            return new JTableHeader(columnModel) {
                
                //public TableCellRenderer getDefaultRenderer() {
                //    return new MyFRHeaderRenderer();
                //}
                
                public String getToolTipText(MouseEvent e) {
                    java.awt.Point p = e.getPoint();
                    int rowIndex = rowAtPoint(p);
                    int colIndex = columnAtPoint(p);
                    int realColumnIndex = convertColumnIndexToModel(colIndex);
                    
                    return _tooltips[realColumnIndex];
                }
            };
        }
    }
    
    
     
    
    private class GRTableModel extends DefaultTableModel 
    {
		private Vector m_oRowKeys = new Vector();
      
        private Comparator comp;
		public GRTableModel()
        {
            comp = new Comparator()
            {
              public int compare(Object arg0, Object arg1)
              {
                 if(arg0 instanceof Vector  && arg1 instanceof Vector)
                 {
                    return ((String)((Vector)arg0).firstElement()).compareTo( ((String)((Vector)arg1).firstElement()));
                 }
                return 0;
              }  
            };
            sortData();
        }
        
        
        public void setDataVector(Vector dataVector, Vector columnIdentifiers)
        {
            super.setDataVector(dataVector, columnIdentifiers);
            sortData();
        }
        
        /* (non-Javadoc)
         * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
         */
        public void setValueAt(Object aValue, int row, int column)
        {
            super.setValueAt(aValue, row, column);
            sortData();
        }
        
        public void sortData()
        {
            //Collections.sort(dataVector, comp );
            //fireTableDataChanged();
        }
        
        /**Sagt welche Spalten editierbar sein drfen.*/
        
        public boolean isCellEditable(int row, int column) 
        {
            Object role_o = getValueAt(row,8);
            if(role_o instanceof GlobalRole){
                GlobalRole role = (GlobalRole) role_o;
                return !role.isAdminRole();
            }
            return true;
        }
        
        /**Sagt von welchem Datentyp die Spalten sind.*/
        public Class getColumnClass(int c) 
        {
            if (c == 0) return String.class;
            else        return Boolean.class;
        }

        public int getRowCount() {
        	try {
        		if (m_vectorGlobalRolesData == null)
        			return 0;
        		return m_vectorGlobalRolesData.size();
        	} catch (NullPointerException e) {
                return 0;
        	}
        }

        public int getColumnCount() {
            return m_vectorGlobalRolesHeader.size();
        }

		public void addRow(Object[] rowData) {
			if(rowData.length>getColumnCount()){
				m_oRowKeys.add(rowData[getColumnCount()]);
			}
			super.addRow(rowData);
            sortData();
		}
		
		/* (non-Javadoc)
		 * @see javax.swing.table.DefaultTableModel#removeRow(int)
		 */
		public void removeRow(int row) {
			if(row<getColumnCount()){
				m_oRowKeys.remove(row);
			}
			super.removeRow(row);
			sortData();
		}


		public void addRow(Vector rowData) {
			if(rowData.size()>getColumnCount()){
				m_oRowKeys.add(rowData.remove(getColumnCount()));
			}
			super.addRow(rowData);
            sortData();
		}

		public Object getValueAt(int row, int column) {
			if(column>=getColumnCount()){
				return m_oRowKeys.get(row);
			}
			return super.getValueAt(row, column);
		}
    }
	
	
    private class RMTableModel extends DefaultTableModel 
    {
		private List rows = new ArrayList();
        /**Sagt welche Spalten editierbar sein drfen.*/
        
        public boolean isCellEditable(int row, int column) 
        {
            return false;
        }
		
		public void addRow(GroupObjectRoleConnector gorc){
			rows.add(gorc);
		}
		
		public GroupObjectRoleConnector getRow(int i){
			if(rows.size()<i){return null;}
			return (GroupObjectRoleConnector) rows.get(i);
		}
		
		public Object getValueAt(int row, int column) {
			if(column==0) return ((GroupObjectRoleConnector)rows.get(row)).getGroup();
			if(column==1) return ((GroupObjectRoleConnector)rows.get(row)).getRole();
			if(column==2) return ((GroupObjectRoleConnector)rows.get(row)).getObject();
			return null;
		}

		public void setDataVector(Vector arg0, Vector arg1) {
			rows = arg0;
			columnIdentifiers = arg1;
			fireTableDataChanged();
		}

		public int getRowCount() {
			return rows.size();
		}
		
		public int getColumnCount() {
			return 3;
		}
		
		
		
       /* public int getRowCount() {
            return m_vectorRMData.size();
        }

        public int getColumnCount() {
            return m_vectorRMHeader.size();
        }*/
    }
    
    private class MyGRHeaderRenderer extends DefaultTableCellRenderer{
        public MyGRHeaderRenderer() {
            super();
            
        }
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JComponent comp = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            comp.setPreferredSize(new Dimension(comp.getPreferredSize().width,comp.getPreferredSize().height*2));
           
            JTableHeader header = table.getTableHeader();
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
	        setHorizontalAlignment(JLabel.CENTER);
            return comp;
        }
        
        
    }
//    private class MyFRHeaderRenderer extends DefaultTableCellRenderer{
//        public MyFRHeaderRenderer() {
//            super();
//            
//        }
//        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//            JComponent comp = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//            comp.setPreferredSize(new Dimension(comp.getPreferredSize().width,comp.getPreferredSize().height*2));
//           
//            JTableHeader header = table.getTableHeader();
//            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
//	        setHorizontalAlignment(JLabel.CENTER);
//            return comp;
//        }
//        
//        
//    }
    private class MyRMHeaderRenderer extends DefaultTableCellRenderer{
        public MyRMHeaderRenderer() {
            super();
            
        }
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JComponent comp = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            comp.setPreferredSize(new Dimension(comp.getPreferredSize().width,comp.getPreferredSize().height*2));
           
            JTableHeader header = table.getTableHeader();
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
	        setHorizontalAlignment(JLabel.CENTER);
            return comp;
        }
        
        
    }
    
    public class button_createUser_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String userId   = m_oTextField_CreateUser_UserID.getText();
            String userName = m_oTextField_CreateUser_UserName.getText();
            String userFamilyName = m_oTextField_CreateUser_UserFamilyName.getText();
            String password = m_oTextField_CreateUser_Password.getText().trim();
            String password2 =  m_oTextField_CreateUser_Password2.getText().trim();
            String email =  m_oTextField_CreateUser_Email.getText().trim();
            //boolean special = m_oCheckBox_CreateUser_Special.isSelected();
            //boolean admin = m_oCheckBox_CreateUser_Admin.isSelected();
            
            boolean dublicatedname = checkForDublicatedUserName(userId);
           
            /**
             * Email Dublikatensuche ausgeschaltet, damit der Admin mehrere User 
             * schnell hinterneinander anlegen kann und dabei seine emailadresse 
             * verwenden kann
             */
            // boolean dublicatedemail = checkForDublicatedEmail(email);
                        
            if (email.length() == 0){
            	m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_NoEMail"));
            	m_oTextField_CreateUser_Email.requestFocus(true);
            }
            
            //else if (dublicatedemail){
            //	m_oControlListener.showMessage("Es existiert bereits ein Benutzer mit dieser Emailadresse!");
            //	m_oTextField_CreateUser_Email.requestFocus(true);      
            //}
            
            else if (dublicatedname){
            	m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_Duplicate"));
                m_oTextField_CreateUser_UserID.requestFocus(true);            	
            }
            
            else if (password.length() == 0) 
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_NoPassword"));
                m_oTextField_CreateUser_Password.requestFocus(true);
            } 
            else if (userId.trim().length() == 0) 
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_NoUserID"));
                m_oTextField_CreateUser_UserID.requestFocus(true);
            } 
            else if (userName.trim().length() == 0) 
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_NoForeName"));
                m_oTextField_CreateUser_UserName.requestFocus(true);
            } 
            else if (userFamilyName.trim().length() == 0) 
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_NoSurName"));
                m_oTextField_CreateUser_UserFamilyName.requestFocus(true);
            } 
            else if (!password.equals(password2)) 
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateUser_PasswordsNotEqual"));
                m_oTextField_CreateUser_Password.requestFocus(true);
            } 
            
            else 
            {
                m_oGUIListener.setWaiting(true);
                m_bUserAdded = false;
                //String exttext = "";
                //if (special) exttext += "mit der Berechtigung zum erweiterten Versand";
                //if (special && admin) exttext += " und ";
                //if (admin) exttext += "mit Administratorberechtigung";
                
                addLogText(Messages.getFormattedString("GUI_Admin_Adding_User", (Object)userFamilyName, (Object)userName, (Object)userId));                
                m_oGUIListener.userRequestCreateUser(userId, password, userFamilyName, userName, email);
                updateBenutzerBoxes();
                flushCreateBenutzerPanel();
                updateEditGroupPanel();
                try {
    				updateSchedulePanel();
    			} catch (ContactDBException e1) {
    				// TODO Auto-generated catch block
    				e1.printStackTrace();
    			}
                initCalendardLists();
                showResult(m_bUserAdded); 
                updateKategorienBoxes(true);
                addLogTextEndMarker();
                m_oGUIListener.setWaiting(false);
            }
            
        }
    }
    
    public class button_createGroup_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String groupName  = m_oTextField_CreateGroup_GroupID.getText();
            if (groupName.length() == 0) 
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_CreateGroup_No_GroupName"));
                m_oTextField_CreateGroup_GroupID.requestFocus(true);
            }
            else
            {
                String message = Messages.getString("GUI_Admin_CreateGroup_GroupName_Already_Exists");
                Iterator it = m_oGUIListener.getUserGroups().iterator();
                while (it.hasNext())
                {
                    UserGroupBean group = (UserGroupBean)it.next();
                    if (group.getAttribute(UserGroup.KEY_NAME).equals( groupName) )
                    {
                    	ApplicationServices.getInstance().getCommonDialogServices().publishError(message,(Exception)null);
                       return;
                    }
                }
                
                
                m_oGUIListener.setWaiting(true);
                String group_description = m_oTextField_CreateGroup_Description.getText();
                String group_shortname = null;
                int globalrolefk = ((GlobalRole)m_oComboBox_CreateGroup_RoleID.getSelectedItem()).getId();
                
                
                UserGroup  mtempUserGroup = null;
                try
                {
                    mtempUserGroup = m_oGUIListener.userRequestCreateUserGroup(groupName,group_description,group_shortname, globalrolefk);
                }
                catch(Exception ex)
                {
                	ApplicationServices.getInstance().getCommonDialogServices().publishError(message,(Exception)null);
                    return;
                }
                
                
                
                if (mtempUserGroup != null){
                    try {
                        PersistenceManager.commit(mtempUserGroup);
                        showResult(true);
                        updateGroupBoxes();
                    } catch (ContactDBException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                        showResult(false);
                    }
                }else{
                    showResult(false);
                } 
				updateRightsManagementBoxes();
                m_oGUIListener.setWaiting(false);
            }
        }
    }
   
    public class button_newGRole_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String RoleName = null;
            if (m_tmodelGlobalRoles.getRowCount() > 0 ) 
            	RoleName = (String) m_tmodelGlobalRoles.getValueAt(m_tmodelGlobalRoles.getRowCount() - 1,0);
            
            /**
             * Nils Neumaier 2.4.2007: 
             * commented this part out because an errormessage seems to be needless
             * at this time because we get an similar message if this error occurs during 
             * save operation. 
             */
            //if (RoleName != null && RoleName.length() == 0) 
            // {
            //TODO: Fehlermeldung
            //  m_oControlListener.showMessage(Messages.getString("GUI_Admin_NewRoll_NoRoll"));
            //}else{
                // Dummy Data
            	_oneGRRow = new Vector();
            	_oneGRRow.add(new String(""));
            	_oneGRRow.add(Boolean.FALSE);
            	_oneGRRow.add(Boolean.FALSE);
            	_oneGRRow.add(Boolean.FALSE);
            	_oneGRRow.add(Boolean.FALSE);
            	if (showPanel(CALENDARPROXY)){
            		_oneGRRow.add(Boolean.FALSE);
            		_oneGRRow.add(Boolean.FALSE);
            	}
            	_oneGRRow.add(Boolean.FALSE);
				_oneGRRow.add(null);
            	// Dummy Data
                m_tmodelGlobalRoles.addRow(_oneGRRow);
           // }
            
        }
    }
    
    public class button_delGRole_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            if (m_tmodelGlobalRoles.getRowCount() > 0 && m_tableGlobalRoles.getSelectedRow() >= 0){
				Object role = m_tmodelGlobalRoles.getValueAt(m_tableGlobalRoles.getSelectedRow(), showPanel(CALENDARPROXY)? 8 : 6);
				if(role!=null&&role instanceof GlobalRole){
					
					
					if ( ((GlobalRoleImpl)role).isDeletable()){
						
						boolean delete = true;	
						try{
							PersistenceManager.delete((IEntity)role);
						}catch(ContactDBException cde){
							delete = false;
                            Throwable cause = cde.getCause();
                            while(cause.getCause() != null) cause = cause.getCause();
                            String errorMessage = cause.getMessage();
                            if(errorMessage.startsWith(Exception.class.getName())) {
                                //note: in case of a network error (AxisFault) we have some error from server in form of a string.  
                                //      An Exception object will be thrown only if there are any dependences to a given role.
                                //      All dependences are listed (begining with "-") inside a message of an exception object. 
                                //      This is a workaround in order not to loose the umlaut-characters 
                                //      when transporting a message through the network (see bug 2696).
                                errorMessage = Messages.getString("GUI_Admin_Role_Delete_Refused_1") +"\n\n"
                                                + Messages.getString("GUI_Admin_Role_Delete_Refused_2") + "\n\n"
                                                + errorMessage.substring(errorMessage.indexOf("-"), errorMessage.length());
                                logger.info("[!] there are some role dependences...");
                                ApplicationServices.getInstance().getCommonDialogServices().publishError(errorMessage);
                            } else {
                                logger.info("[!] cause: " + cause.getClass().getName());
                                cde.printStackTrace();
                                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_Delete_Global_Role_Error"), cde);                            
                            }
						}
						
						if (delete)m_tmodelGlobalRoles.removeRow(m_tableGlobalRoles.getSelectedRow());
					}
					else {
						ApplicationServices.getInstance().getCommonDialogServices().showInfo(Messages.getString("GUI_Admin_Global_Role_Not_Deletable"));
					}
				}
                
            }
            
        }
    }
    
    public class button_saveGRole_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
        	// we have to stop editing before in order to apply changes to the table-model
        	
        	// for some reasen we have to clear selection to make the following work..
        	m_tableGlobalRoles.getSelectionModel().clearSelection();
        	
        	if(m_tableGlobalRoles.getCellEditor() != null)
        		m_tableGlobalRoles.getCellEditor().stopCellEditing();
        	
            Map _map = new HashMap();
            if (m_tmodelGlobalRoles.getRowCount() > 0 ){
                for (int i = 0; i < m_tmodelGlobalRoles.getRowCount();i++){
                	if (((String)m_tmodelGlobalRoles.getValueAt(i,0)).length() < 1){
                		m_oControlListener.showMessage(Messages.getString("GUI_Admin_Role_Names_Must_Not_Be_Empty"));
                		return;
                	}                    
                	
                    if (_map.containsKey((String) m_tmodelGlobalRoles.getValueAt(i,0))){
                        m_oControlListener.showMessage(Messages.getString("GUI_Admin_Role_Names_Must_Be_Different"));
                        m_tableGlobalRoles.setRowSelectionInterval(i,i);
                        return;
                    }else{
                        _map.put((String) m_tmodelGlobalRoles.getValueAt(i,0),null);
						Object role_o = m_tmodelGlobalRoles.getValueAt(i,showPanel(CALENDARPROXY)? 8 : 6);
						GlobalRole role;
						if(role_o != null && role_o instanceof GlobalRole){
							role = (GlobalRole) role_o;
						}else{
							role = new GlobalRoleImpl();
                            ((GlobalRoleImpl)role).setAttribute(GlobalRole.KEY_ROLE_TYPE, ""+GlobalRole.ROLE_TYPE_OTHER);
							m_tmodelGlobalRoles.m_oRowKeys.add(i, role);
						}
						role.setRoleName((String) m_tmodelGlobalRoles.getValueAt(i, 0));
						role.setRight(GlobalRole.KEY_EDITUSER, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 1));
						role.setRight(GlobalRole.KEY_REMOVEUSER, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 2));
						role.setRight(GlobalRole.KEY_EDITFOLDER, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 3));
						role.setRight(GlobalRole.KEY_REMOVEFOLDER, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 4));
						if (showPanel(CALENDARPROXY)){
							role.setRight(GlobalRole.KEY_EDITSCHEDULE, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 5));
							role.setRight(GlobalRole.KEY_REMOVESCHEDULE, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 6));
							role.setRight(GlobalRole.KEY_DELETE, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 7));
						}
						else {
							role.setRight(GlobalRole.KEY_EDITSCHEDULE, Boolean.FALSE);
							role.setRight(GlobalRole.KEY_REMOVESCHEDULE, Boolean.FALSE);
							role.setRight(GlobalRole.KEY_DELETE, (Boolean) m_tmodelGlobalRoles.getValueAt(i, 5));
						}
						
						try {
							PersistenceManager.commit(role);
						} catch (ContactDBException e1) {
							ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_Save_Global_Role_Error"), e1);
						}
                    }
                }
            }
            updateGlobalRoleBoxes();
        }
    }
    
    public class button_saveRM_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e){
			Iterator it = m_vectorRMData.iterator();
			while(it.hasNext()){
				GroupObjectRoleConnector gorc = (GroupObjectRoleConnector) it.next();
				try {
					if(((IEntity)gorc).isTransient()){
						PersistenceManager.commit(gorc);
					}
				} catch (ContactDBException e1) {
					ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_Save_Rights_Error"), e1);
				}
			}
        }
    }

    public class RM_filterchanged implements DocumentListener{

        public void insertUpdate(DocumentEvent e) {
            doUpdate();
        }
        public void removeUpdate(DocumentEvent e) {
            doUpdate();
        }
        public void changedUpdate(DocumentEvent e) {
            doUpdate();            
        }
        
        public void doUpdate(){
            // Eventuell optimieren
            m_vectorRMfilteredData.clear();
            
            String _groupfilter = m_otextfield_RM_filterGruppe.getText().toLowerCase();
            String _rolefilter = m_otextfield_RM_filterRolle.getText().toLowerCase();
            String _objectfilter = m_otextfield_RM_filterObject.getText().toLowerCase();
            if (_groupfilter.equals("") && _rolefilter.equals("") && _objectfilter.equals("")){
                m_tmodelRM.setDataVector(m_vectorRMData,m_vectorRMHeader);
                m_olabel_defRights.setText(Messages.getString("GUI_Admin_Recht_definierte_Rechte"));
            }else{            
	            for (int i = 0; i < m_vectorRMData.size(); i++){
	                GroupObjectRoleConnector _oneRowRM = (GroupObjectRoleConnector) m_vectorRMData.get(i);
	                if (_oneRowRM.getGroup().toString().toLowerCase().indexOf(_groupfilter) != -1 && 
	                    _oneRowRM.getRole().toString().toLowerCase().indexOf(_rolefilter) != -1 && 
	                    _oneRowRM.getObject().toString().toLowerCase().indexOf(_objectfilter) != -1){
	                    m_vectorRMfilteredData.add(_oneRowRM);
	                }
	            }
	            m_tmodelRM.setDataVector(m_vectorRMfilteredData,m_vectorRMHeader);
	            m_olabel_defRights.setText(Messages.getString("GUI_Admin_Recht_definierte_Rechte_gefiltert"));
            }
            m_tmodelRM.fireTableDataChanged();        
        }
    }
    
    
    public class button_add_Right_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
        	if (m_oComboBox_RMobject.getSelectedItem() == null || m_oComboBox_RMobject.getSelectedItem().toString().equals(""))
        		return;
        	
            GroupObjectRoleConnector gorc = new GroupObjectRoleImpl();
            gorc.setGroup((UserGroup) m_oComboBox_RMgroup.getSelectedItem());
			gorc.setRole((ObjectRole)m_oComboBox_RMrole.getSelectedItem());
			gorc.setObject(m_oComboBox_RMobject.getSelectedItem());
            if (!m_vectorRMData.contains(gorc)){
                m_vectorRMData.add(gorc);
            }
            m_olabel_defRights.setText(Messages.getString("GUI_Admin_Recht_definierte_Rechte"));

            _RM_selectionListener.doUpdate();
        }
    }
	
	public class button_delete_Right_clicked implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			if(m_tmodelRM.getRowCount()>0&&m_tableRM.getSelectedRow()!=-1){
				GroupObjectRoleImpl gorc = (GroupObjectRoleImpl) m_tmodelRM.getRow(m_tableRM.getSelectedRow());
				try {
                    if (!((IEntity)gorc).isTransient())
                        PersistenceManager.delete((IEntity)gorc);
					m_vectorRMData.remove(gorc);
					m_tmodelRM.fireTableRowsDeleted(m_tableRM.getSelectedRow(), m_tableRM.getSelectedRow());
				} catch (ContactDBException e) {
					ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_SaveRight_Error"),e);
				}
			}
            _RM_selectionListener.doUpdate();
		}
		
	}
        
    public class button_editGroup_clicked implements ActionListener
    { 
        
        public void actionPerformed(ActionEvent e)
        {
        	String oldGroupName = null;
        	try {
        		oldGroupName = (String)_currentUserGroup.getAttribute(UserGroup.KEY_NAME);
			} catch (ContactDBException e2) {
				ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_EditGroup_Error"), e2);
			}
			
        	String groupname = m_oTextField_EditGroup_GroupID.getText();
        	
        	if (groupname == null || groupname.equals("")){
        		ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_EditGroup_No_GroupName"));
        		return;
        	}
        	
        	if (!groupname.equals(oldGroupName)){
        		List allGroups = getBenutzerGruppen();
        		for (Iterator iter = allGroups.iterator(); iter.hasNext();){
        			String tempGroupName = (String)iter.next();
        			if (groupname.equalsIgnoreCase(tempGroupName)){
        				ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_EditGroup_Dublicate_GroupName"));
        				return;
        			}
        		}
        	}
        	
    	    m_oGUIListener.setWaiting(true);
            try {
				_currentUserGroup.setAttribute(UserGroup.KEY_NAME, groupname);
				_currentUserGroup.setAttribute(UserGroup.KEY_DESCRIPTION, m_oTextField_EditGroup_Description.getText());
				_currentUserGroup.setAttribute(UserGroup.KEY_SHORTNAME, null);
				_currentUserGroup.setAttribute(UserGroup.KEY_GLOBALROLE, String.valueOf( ((RoleComboboxModel) m_oComboBox_EditGroup_RoleID.getModel()).getRoleAt(m_oComboBox_EditGroup_RoleID.getSelectedIndex()) .getId()));
									
				PersistenceManager.commit(_currentUserGroup);
				
			} catch (ContactDBException e1) {
				ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("GUI_Admin_EditGroup_Error"), e1);
			}
			
			updateRightsManagementBoxes();
			updateRightsManagementPanel();
            updateGroupBoxes();
			((UpdateableComboboxModel)m_oComboBox_DeleteGroup_GroupID.getModel()).refresh( getBenutzerGruppen(false, false).toArray() );
            m_oComboBox_EditGroup_GroupID.setSelectedItem(groupname);
            
            m_oGUIListener.setWaiting(false);
        }
    }
    
    public class button_deleteGroup_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            m_oGUIListener.setWaiting(true);
            final Iterator myIter = m_oGUIListener.getUserGroups().iterator();
            new Thread("delete-group-dialog"){
                public void run() {
                    while (myIter.hasNext()){
                        UserGroup tmpUG = (UserGroup) myIter.next();
                        try {
                            if (tmpUG.getAttribute(UserGroup.KEY_NAME).equals(m_oComboBox_DeleteGroup_GroupID.getSelectedItem().toString())){
                                String question = Messages.getFormattedString("GUI_Admin_Message_Delete_Group", new Object[]{tmpUG.getAttribute(UserGroup.KEY_NAME)}); 
                                if(0 != ApplicationServices.getInstance().getCommonDialogServices()
                                    .askUser(question, Messages.getString("Buttons_Yes_NO").split("[:]", 2), 0)) return;
                                PersistenceManager.delete(tmpUG);
                                
                                updateGroupBoxes();
                                updateKategorienBoxes(true);                        
                                break;
                            }
                        }
                        catch ( ContactDBException e1 ) {
                            e1.printStackTrace();
                        }
                    }
                }
            }.start();
			updateRightsManagementBoxes();
			updateRightsManagementPanel();
            m_oGUIListener.setWaiting(false);
        }
    }
    
    public class button_AddAsAdmin_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            m_oGUIListener.setWaiting(true);
            
            String user = m_oComboBox_EditGroup_UserID.getSelectedItem().toString();
            GroupAdminList.m_oListModel.addElement(user);
            List tmpList = Arrays.asList(GroupAdminList.m_oListModel.toArray());
            Collections.sort(tmpList, new ValueComparator());
            GroupAdminList.m_oListModel.clear();
            Iterator myIter = tmpList.iterator();
            while (myIter.hasNext()){
                GroupAdminList.m_oListModel.addElement(myIter.next());
            }
            m_oComboBox_EditGroup_UserID.removeItem(m_oComboBox_EditGroup_UserID.getSelectedItem());
            
            m_oGUIListener.setWaiting(false);
            
        }
    }
    
    public class button_AddAsUser_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            m_oGUIListener.setWaiting(true);
            
            String user = m_oComboBox_EditGroup_UserID.getSelectedItem().toString();
            GroupUserList.m_oListModel.addElement(user);
            List tmpList = Arrays.asList(GroupUserList.m_oListModel.toArray());
            Collections.sort(tmpList, new ValueComparator());
            GroupUserList.m_oListModel.clear();
            Iterator myIter = tmpList.iterator();
            while (myIter.hasNext()){
                GroupUserList.m_oListModel.addElement(myIter.next());
            }
            m_oComboBox_EditGroup_UserID.removeItem(m_oComboBox_EditGroup_UserID.getSelectedItem());
            
            m_oGUIListener.setWaiting(false);
            
        }
    }
    
    private JComboBox m_oComboBox_DeleteUser_UserID;
    private JComboBox m_oComboBox_EditSchedulePanel_Users;
    private JCheckBox m_oCheckBox_DeleteUser_DeleteAddress;
    private JTextField 	tfd_grantTo;
    private TarentWidgetDateSpinner twds_endDate;
    private TarentWidgetDateSpinner twds_startDate;
    private TarentWidgetDateSpinner twds_appointmentReadStartDate;
    private TarentWidgetDateSpinner twds_appointmentReadEndDate;
    private TarentWidgetDateSpinner twds_appointmentWriteStartDate;
    private TarentWidgetDateSpinner twds_appointmentWriteEndDate;
    private JRadioButton twrb_end1_unlim;
    private JRadioButton twrb_end1_date;
    private JRadioButton twrb_end2_unlim;
    private JRadioButton twrb_end2_date;
    private JRadioButton twrb_end3_unlim;
    private JRadioButton twrb_end3_date;
    private Collection CalSecCollection;
    private Calendar ReleaseCalender;
    
    private ExtendedDataItemModel createDeleteUserPanel() {
        JLabel userLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_User") + " ",null); //$NON-NLS-1$
        m_oComboBox_DeleteUser_UserID = createEntryComboBox(getBenutzer().toArray());
        JPanel userPanel = createEntryPanel(userLabel, m_oComboBox_DeleteUser_UserID);

        m_oCheckBox_DeleteUser_DeleteAddress = createEntryCheckBox(false, Messages.getString("GUI_Admin_Benutzer_Delete_Address"));
        JPanel checkboxPanel = createEntryPanel(createEntryLabel("",null), m_oCheckBox_DeleteUser_DeleteAddress);
        
        JPanel deleteUserEntryPanel = createEntriesPanel();
        deleteUserEntryPanel.add(userPanel);
        deleteUserEntryPanel.add(checkboxPanel);
        
        JButton button_deleteUser = createButton(Messages.getString("GUI_Admin_Benutzer_Ausfuehren"), new button_deleteUser_clicked()); //$NON-NLS-1$
        
        JPanel deleteUserPanel = new JPanel(new BorderLayout());
        deleteUserPanel.add(deleteUserEntryPanel, BorderLayout.CENTER);
        deleteUserPanel.add(createButtonPanel(button_deleteUser), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Benutzer_loeschen"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", deleteUserPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_USER_DELETE));
        return item;    
    }
    
    public class button_deleteUser_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String user = getKeyFromComboBoxEntry(m_oComboBox_DeleteUser_UserID).trim();
            String username = getValueFromComboBoxEntry(m_oComboBox_DeleteUser_UserID).trim();            

            if (user.length() > 0)
            {

                try { 
                    User userObj = m_oGUIListener.getUser(m_oGUIListener.getUserName(user));
                    if (userObj.isAdminUser()) {
                        m_oControlListener.showMessage(Messages.getString("GUI_Admin_User_Admin_Undeletable"));
                        return;
                    }

                    String[] answers = {"Ja", "Nein"};

                    int answer = ApplicationServices.getInstance().getCommonDialogServices().askUser(Messages.getString("GUI_Admin_ReallyDeleteUserTitle"), Messages.getFormattedString("GUI_Admin_ReallyDeleteUser", user), answers, 1);
                    if (answer == 0)
                    {         
                        
                        m_bUserRemoved = false;
                        m_oGUIListener.setWaiting(true);
                        addLogText("entferne Benutzer \"" + user + "\"... ");
                        
                        m_oGUIListener.userRequestDeleteUser(user, m_oCheckBox_DeleteUser_DeleteAddress.isSelected());
                        updateBenutzerBoxes();
                        updateEditGroupPanel();
                        initCalendardLists();
                        showResult(m_bUserRemoved);
                        updateKategorienBoxes(true);
                        try {
            				updateSchedulePanel();
            			} catch (ContactDBException e1) {
            				logger.warning("Tried to update SchedulePanel. No Panel found.");
            			}
                        addLogTextEndMarker();
                        m_oGUIListener.setWaiting(false);
                    }
                } catch (ContactDBException ex) {
                    ex.printStackTrace();
                }
            }
            else
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_DeleteUser_No_User_Selected"));
                m_oComboBox_DeleteUser_UserID.requestFocus(true);
            }
        }
    }
    
    
    // ----------------------------------------------------------------------------------
    private void updateSchedulePanel() throws ContactDBException{
    	
    	User currentUser = m_oGUIListener.getUser(null, true);
        
        if (currentUser.getGlobalRights().hasRightEditSchedule() || currentUser.getGlobalRights().isAdminRole() ){        	
        	m_oComboBox_EditSchedulePanel_Users = createEntryComboBox(getBenutzer().toArray());
        }
		      
        else{
        	ArrayList benutzer = new ArrayList();
        	benutzer.add(combineKeyValue(String.valueOf(currentUser.getId()) , m_oGUIListener.getCurrentUser()));
            
        	m_oComboBox_EditSchedulePanel_Users = createEntryComboBox(benutzer.toArray());            
        }
        	
       
        m_oComboBox_EditSchedulePanel_Users.addActionListener(new CalendarUserSelect());
        
    }
    
    
    private ExtendedDataItemModel createCalendarProxyPanel() throws ContactDBException{
        
        JPanel tmpPanel = null;
        Dimension MyDim = new Dimension(130,24);
        
        User currentUser = m_oGUIListener.getUser(null, true);
        
        ArrayList benutzer = new ArrayList();
        benutzer.add(combineKeyValue(String.valueOf(currentUser.getId()) , m_oGUIListener.getCurrentUser()));
         
        if (currentUser.getGlobalRights().hasRightEditSchedule()){ //|| currentUser.getGlobalRights().isAdminRole() ){        	
        	m_oComboBox_EditSchedulePanel_Users = createEntryComboBox(getBenutzer().toArray());
        }
		      
        else 
        	m_oComboBox_EditSchedulePanel_Users = createEntryComboBox(benutzer.toArray());
       
        m_oComboBox_EditSchedulePanel_Users.addActionListener(new CalendarUserSelect());
        tmpPanel = createGenericPanel_XAxis(new JComponent[]{createEntryViewField(Messages.getString("GUI_Admin_Calendar_Proxy_ADMIN1") +  " "),
                                                             m_oComboBox_EditSchedulePanel_Users
                                            },true);
        
        JPanel tmpPanelwrap = createEntriesPanel();
        tmpPanelwrap.add(tmpPanel);
        tmpPanelwrap.add(Box.createVerticalStrut(20));
        
        
        //Linke Listbox
        JLabel allUsersLabel = createEntryViewField(Messages.getString("GUI_Admin_Calendar_Proxy_UNAUTHORIZEDUSERS"));
        //JList allUsersList = createEntryList(getBenutzer().toArray());
        allProxyUsersList = createEntryList(null);
        JPanel tmpPanel1 = createEntriesPanel();
        tmpPanel1.add(allUsersLabel);
        tmpPanel1.add(new JScrollPane(allProxyUsersList));
        
        //Buttons Mitte      
        JButton button_addUser = createButton(Messages.getString("GUI_Admin_Calendar_Proxy_USERADD"),null); //$NON-NLS-1$
        button_addUser.setAlignmentX(Component.CENTER_ALIGNMENT);
        button_addUser.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (allProxyUsersList.getSelectedIndex() != -1){
                    
                    String element = (String) allProxyUsersList.getDefaultModel().getElementAt(allProxyUsersList.getSelectedIndex());
                    Integer ReleaseuserID = new Integer(getKeyFromKeyValue(element));
                    
                    allowedProxyUsersList.insertElement(element);
                    allProxyUsersList.getDefaultModel().remove(allProxyUsersList.getSelectedIndex());
                    Object[] _tmpsort = allowedProxyUsersList.getDefaultModel().toArray(); 
                    Arrays.sort(_tmpsort, new ValueComparator());
                    allowedProxyUsersList.getDefaultModel().removeAllElements();
                    allowedProxyUsersList.insertElements(_tmpsort);
                    
                    final Integer calendarID;
                    try {
                        calendarID = new Integer(ReleaseCalender.getId());
                        CalendarSecretaryRelation newCalSecRel = new CalendarSecretaryRelationImpl(calendarID,ReleaseuserID);
                        ScheduleDate sd = new ScheduleDate(new Date());
                        newCalSecRel.setAccessDateRange(new DateRange(sd.getFirstSecondOfDay().getDate(),null));
                        newCalSecRel.setReadDateRange(new DateRange(sd.getFirstSecondOfDay().getDate(),null));
                        newCalSecRel.setWriteDateRange(new DateRange(sd.getFirstSecondOfDay().getDate(),null));
                        
                        PersistenceManager.commit(newCalSecRel);
                        ReleaseCalender.add(newCalSecRel);
                        PersistenceManager.commit(ReleaseCalender);
                        CalSecCollection.add(newCalSecRel);
                        
                        //GUI
                        twds_startDate.setData(sd.getDate());
                        twds_appointmentReadStartDate.setData(sd.getDate());
                        twds_appointmentWriteStartDate.setData(sd.getDate());
                    } catch (ContactDBException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    
                    
                }
            }});
        
        JButton button_deleteUser = createButton(Messages.getString("GUI_Admin_Calendar_Proxy_USERDEL"), null); //$NON-NLS-1$
        button_deleteUser.setAlignmentX(Component.CENTER_ALIGNMENT);
        button_deleteUser.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e) {
                if (allowedProxyUsersList.getSelectedIndex() != -1)
                {
                    String element = (String) allowedProxyUsersList.getDefaultModel().getElementAt(allowedProxyUsersList.getSelectedIndex());
                    String userName = getValueFromKeyValue(element);
                    Integer ReleaseuserID = new Integer(getKeyFromKeyValue(element));
                    CalendarSecretaryRelationImpl calsecToRemove = null;
                    
                    for( Iterator iter = CalSecCollection.iterator(); iter.hasNext();){
                        CalendarSecretaryRelationImpl calsecimpl = (CalendarSecretaryRelationImpl) iter.next();
                        try {
                            if (calsecimpl.getSecretaryUserID().equals(ReleaseuserID)){
                                //remove this one
                                ReleaseCalender.remove(calsecimpl);
                                calsecimpl.delete();
                                calsecToRemove = calsecimpl;
                            }
                        } catch (ContactDBException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                    
                    CalSecCollection.remove(calsecToRemove);
                    allProxyUsersList.insertElement(element);
                    allowedProxyUsersList.getDefaultModel().remove(allowedProxyUsersList.getSelectedIndex());
                    Object[] _tmpsort = allProxyUsersList.getDefaultModel().toArray(); 
                    Arrays.sort(_tmpsort, new ValueComparator());
                    allProxyUsersList.getDefaultModel().removeAllElements();
                    allProxyUsersList.insertElements(_tmpsort);
                    resetReleaseGui();
                }
            }});
        
        JPanel tmpPanel2 = createEntriesPanel();
        tmpPanel2.add(Box.createVerticalStrut(20));
        tmpPanel2.add(button_addUser);
        tmpPanel2.add(Box.createVerticalStrut(10));
        tmpPanel2.add(button_deleteUser);
        tmpPanel2.add(Box.createVerticalStrut(20));
        JPanel emptyPanel1 = createEntriesPanel();
        emptyPanel1.add(Box.createHorizontalStrut(20));
        JPanel emptyPanel2 = createEntriesPanel();
        emptyPanel2.add(Box.createHorizontalStrut(20));
        
        //Rechte Listbox  
        JLabel allowedUsersLabel = createEntryViewField(Messages.getString("GUI_Admin_Calendar_Proxy_AUTHORIZEDUSERS"));
        allowedProxyUsersList= createEntryList(null);
        allowedProxyUsersList.addListSelectionListener(new ListSelectionListener(){

            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting() == false) {

                    if (allowedProxyUsersList.getSelectedIndex() != -1) {
                        // Selection, enable and fill GUI
                        Integer userID = new Integer(getKeyFromKeyValue(allowedProxyUsersList.getSelectedValue().toString()));
                        tfd_grantTo.setEnabled(true);
                        tfd_grantTo.setText(getValueFromKeyValue(allowedProxyUsersList.getSelectedValue().toString()));

                        if (CalSecCollection!= null){
                            for (Iterator iter = CalSecCollection.iterator(); iter.hasNext();){
                                CalendarSecretaryRelationImpl calsec = (CalendarSecretaryRelationImpl) iter.next();
                                try {
                                    if (calsec.getSecretaryUserID().equals(userID)){
                                        // Fill
                                        twrb_end1_date.setEnabled(true);
                                        twrb_end1_unlim.setEnabled(true);
                                        twrb_end2_date.setEnabled(true);
                                        twrb_end2_unlim.setEnabled(true);
                                        twrb_end3_date.setEnabled(true);
                                        twrb_end3_unlim.setEnabled(true);
                                        
                                        //1
                                        twds_startDate.setEnabled(true);
                                        twds_startDate.setData(calsec.getAccessDateRange().getStartDate());
                                        if (calsec.getAccessDateRange().hasEndDate()){
                                          twds_endDate.setData(calsec.getAccessDateRange().getEndDate());
                                            twds_endDate.setEnabled(true);
                                            twrb_end1_date.setSelected(true);
                                            twrb_end1_date.setEnabled(true);
                                        }else{
                                            twds_endDate.setEnabled(false);
                                            twrb_end1_unlim.setSelected(true);
                                            twrb_end1_unlim.setEnabled(true);
                                        }
                                        
                                        //2
                                        twds_appointmentReadStartDate.setEnabled(true);
                                        twds_appointmentReadStartDate.setData(calsec.getReadDateRange().getStartDate());
                                        
                                        if (calsec.getReadDateRange().hasEndDate()){
                                          twds_appointmentReadEndDate.setData(calsec.getReadDateRange().getEndDate());
                                            twds_appointmentReadEndDate.setEnabled(true);
                                            twrb_end2_date.setSelected(true);
                                            twrb_end2_date.setEnabled(true);
                                        }else{
                                            twds_appointmentReadEndDate.setEnabled(false);
                                            twrb_end2_unlim.setSelected(true);
                                            twrb_end2_unlim.setEnabled(true);
                                        }
                                        
                                        //3
                                        twds_appointmentWriteStartDate.setEnabled(true);
                                        twds_appointmentWriteStartDate.setData(calsec.getWriteDateRange().getStartDate());
                                        
                                        if (calsec.getWriteDateRange().hasEndDate()){
                                          twds_appointmentWriteEndDate.setData(calsec.getWriteDateRange().getEndDate());
                                            twds_appointmentWriteEndDate.setEnabled(true);
                                            twrb_end3_date.setEnabled(true);
                                            twrb_end3_date.setSelected(true);
                                        }else{
                                            twds_appointmentWriteEndDate.setEnabled(false);
                                            twrb_end3_unlim.setEnabled(true);
                                            twrb_end3_unlim.setSelected(true);
                                        }


                                    }
                                } catch (ContactDBException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                }
                
            }});
        JPanel tmpPanel3 = createEntriesPanel();
        tmpPanel3.add(allowedUsersLabel);
        tmpPanel3.add(new JScrollPane(allowedProxyUsersList));
        JPanel tmpPanel4 = createGenericPanel_XAxis(new JComponent[]{tmpPanel1,emptyPanel1,tmpPanel2,emptyPanel2,tmpPanel3},true);
        
        // Detailansicht
        JLabel lbl_grantTo = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_GRANT_TO") + " ",MyDim );
        tfd_grantTo = createEntryTextField("", m_iMaxLength_grantTo);
        tfd_grantTo.setEditable(false);
        JPanel entry1 = createEntryPanel(lbl_grantTo,tfd_grantTo);
        
//        JLabel lbl_grantRW = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_GRANT_RW") + " ",MyDim );
//        JCheckBox chk_granRW = createEntryCheckBox(false,Messages.getString("GUI_Admin_Kalender_Proxy_GRANT_RWDETAIL"));
//        JPanel entry2 = createEntryPanel(lbl_grantRW,chk_granRW);
        
        // Vertretung: ANFANG
        JLabel lbl_grantStartDate = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_GRANT_STARTDATE") + " ",MyDim );
        twds_startDate = new TarentWidgetDateSpinner("dd.MM.yyyy");
        twds_startDate.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                Date start = (Date)(twds_startDate.getValue());
                Date end = (Date)(twds_endDate.getValue());
                if (start.after(end))
                {
                    twds_endDate.setValue(start);
                }       
            }});
        
        JPanel entry3 = createComponentEntryPanelXAXIS(lbl_grantStartDate,twds_startDate);
        
        // Vertretung: ENDE
        JLabel lbl_grantEndDate = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_GRANT_ENDDATE") + " ",MyDim );
        twrb_end1_unlim = new JRadioButton("unbegrenzt");
        twrb_end1_unlim.setSelected(true);
        twrb_end1_unlim.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                twds_endDate.setEnabled(! twrb_end1_unlim.isSelected());                
            }});
        twrb_end1_date = new JRadioButton("endet am");
        ButtonGroup g1 = new ButtonGroup();
        g1.add( twrb_end1_unlim );
        g1.add( twrb_end1_date );
        twds_endDate = new TarentWidgetDateSpinner("dd.MM.yyyy");
        twds_endDate.setEnabled(false);
        twds_endDate.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                Date start = (Date)(twds_startDate.getValue());
                Date end = (Date)(twds_endDate.getValue());
                if (start.after(end))
                {
                    twds_startDate.setValue(end);
                }       
            }});
        
        JPanel entry4 = createGenericPanel_XAxis(new JComponent[]{twrb_end1_unlim,twrb_end1_date,twds_endDate},true);
        
        // Termine lesen: ANFANG
        JLabel lbl_appointmentreadStartDate = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_APPOINTMENT_READSTARTDATE") + " ",MyDim );
        twds_appointmentReadStartDate = new TarentWidgetDateSpinner("dd.MM.yyyy");
        twds_appointmentReadStartDate.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                Date start = (Date)(twds_appointmentReadStartDate.getValue());
                Date end = (Date)(twds_appointmentReadEndDate.getValue());
                if (start.after(end))
                {
                    twds_appointmentReadEndDate.setValue(start);
                }       
            }});      	
        JPanel entry5 = createComponentEntryPanelXAXIS(lbl_appointmentreadStartDate,twds_appointmentReadStartDate);
        
        // Termine lesen: ENDE
        JLabel lbl_appointmentReadEndDate = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_APPOINTMENT_READENDDATE") + " ",MyDim );
        twrb_end2_unlim = new JRadioButton("unbegrenzt");
        twrb_end2_unlim.setSelected(true);
        twrb_end2_unlim.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                twds_appointmentReadEndDate.setEnabled(! twrb_end2_unlim.isSelected());
                
            }});
        twrb_end2_date = new JRadioButton("endet am");
        ButtonGroup g = new ButtonGroup();
        g.add( twrb_end2_unlim );
        g.add( twrb_end2_date );
        
        twds_appointmentReadEndDate = new TarentWidgetDateSpinner("dd.MM.yyyy");
        twds_appointmentReadEndDate.setEnabled(false);
        twds_appointmentReadEndDate.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                Date start = (Date)(twds_appointmentReadStartDate.getValue());
                Date end = (Date)(twds_appointmentReadEndDate.getValue());
                if (start.after(end))
                {
                    twds_appointmentReadStartDate.setValue(end);
                }       
            }});
        JPanel entry6 = createGenericPanel_XAxis(new JComponent[]{twrb_end2_unlim,twrb_end2_date,twds_appointmentReadEndDate},true);
        
        
        // Termine schreiben: ANFANG
        JLabel lbl_appointmentWriteStartDate = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_APPOINTMENT_WRITESTARTDATE") + " ",MyDim );
        twds_appointmentWriteStartDate= new TarentWidgetDateSpinner("dd.MM.yyyy");
        twds_appointmentWriteStartDate.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                Date start = (Date)(twds_appointmentWriteStartDate.getValue());
                Date end = (Date)(twds_appointmentWriteEndDate.getValue());
                if (start.after(end))
                {
                    twds_appointmentWriteEndDate.setValue(start);
                }       
            }});      	
        JPanel entry7 = createComponentEntryPanelXAXIS(lbl_appointmentWriteStartDate,twds_appointmentWriteStartDate);
        
        // Termine schreiben: ENDE
        JLabel lbl_appointmentEndDate = createEntryLabel(Messages.getString("GUI_Admin_Kalender_Proxy_APPOINTMENT_WRITEENDDATE") + " ",MyDim );
        twrb_end3_unlim = new JRadioButton("unbegrenzt");
        twrb_end3_unlim.setSelected(true);
        twrb_end3_unlim.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                twds_appointmentWriteEndDate.setEnabled(! twrb_end3_unlim.isSelected());
                
            }});
        twrb_end3_date = new JRadioButton("endet am");
        ButtonGroup g2 = new ButtonGroup();
        g2.add( twrb_end3_unlim );
        g2.add( twrb_end3_date );
        
        twds_appointmentWriteEndDate= new TarentWidgetDateSpinner("dd.MM.yyyy");
        twds_appointmentWriteEndDate.setEnabled(false);
        twds_appointmentWriteEndDate.addChangeListener(new ChangeListener(){
            
            public void stateChanged(ChangeEvent e) {
                Date start = (Date)(twds_appointmentWriteStartDate.getValue());
                Date end = (Date)(twds_appointmentWriteEndDate.getValue());
                if (start.after(end))
                {
                    twds_appointmentWriteStartDate.setValue(end);
                }       
            }});
        JPanel entry8 = createGenericPanel_XAxis(new JComponent[]{twrb_end3_unlim,twrb_end3_date,twds_appointmentWriteEndDate},true);
        // Button
        JButton btn_anwenden = createButton(Messages.getString("GUI_Admin_Kalender_Proxy_Button_Anwenden"),new ActionListener(){
            
            public void actionPerformed(ActionEvent e) {
                
                if (allowedProxyUsersList.getSelectedIndex() != -1)
                {
	                //Save current CalSec ...
	                Integer userID = new Integer(getKeyFromKeyValue(allowedProxyUsersList.getSelectedValue().toString()));
	                if (CalSecCollection!= null){
	                    for (Iterator iter = CalSecCollection.iterator(); iter.hasNext();){
	                        CalendarSecretaryRelationImpl calsec = (CalendarSecretaryRelationImpl) iter.next();
	                            try {
	                                if (calsec.getSecretaryUserID().equals(userID)){
	                                    //Update this ...
	                                    if (twrb_end1_date.isSelected()){
	                                        calsec.setAccessDateRange(new DateRange((Date)twds_startDate.getValue(),(Date)twds_endDate.getValue()));
	                                    }else{
	                                        calsec.setAccessDateRange(new DateRange((Date)twds_startDate.getValue(),null));
	                                    }
	                                    if (twrb_end2_date.isSelected()){
	                                        calsec.setReadDateRange(new DateRange((Date)twds_appointmentReadStartDate.getValue(),(Date)twds_appointmentReadEndDate.getValue()));
	                                    }else{
	                                        calsec.setReadDateRange(new DateRange((Date)twds_appointmentReadStartDate.getValue(),null));
	                                    }
	                                    if (twrb_end3_date.isSelected()){
	                                        calsec.setWriteDateRange(new DateRange((Date)twds_appointmentWriteStartDate.getValue(),(Date)twds_appointmentWriteEndDate.getValue()));
	                                    }else{
	                                        calsec.setWriteDateRange(new DateRange((Date)twds_appointmentWriteStartDate.getValue(),null));
	                                    }
	                                    
	                                    //Save...
	                                    calsec.setDirty(true);                                    
	                                    PersistenceManager.commit(calsec);
	                                }
	                            } catch (ContactDBException e1) {
	                                // TODO Auto-generated catch block
	                                e1.printStackTrace();
	                            }
	                    }
	                }
                }   
            }});
        
        JPanel ButtonPanel = createButtonPanel_XAxis(new JButton[]{btn_anwenden});
      
        // Rein damit
        JPanel GrantPanel = createGenericPanel_XAxis(new JComponent[]{entry3,entry4},true);
        JPanel READDurationPanel = createGenericPanel_XAxis(new JComponent[]{entry5,entry6},true);
        JPanel WRITEDurationPanel = createGenericPanel_XAxis(new JComponent[]{entry7,entry8},true);
        
         
        // Zusammenbauen
        JPanel detailpanel = createEntriesPanel();
        detailpanel.add(Box.createVerticalStrut(20));
        detailpanel.add(entry1);
        //detailpanel.add(entry2);
        detailpanel.add(GrantPanel);
        detailpanel.add(READDurationPanel);
        detailpanel.add(WRITEDurationPanel);
        detailpanel.add(Box.createVerticalStrut(20));
        detailpanel.add(ButtonPanel);
        
                
        JPanel calendarProxyPanel = new JPanel(new BorderLayout());
        calendarProxyPanel.add(tmpPanelwrap, BorderLayout.NORTH);
        calendarProxyPanel.add(tmpPanel4, BorderLayout.CENTER);
        calendarProxyPanel.add(detailpanel, BorderLayout.SOUTH);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Kalender_Proxy"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", calendarProxyPanel));
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_CALENDAR_SHARE));
        initCalendardLists();
        return item;    
    }
    
    
    private void initCalendardLists()
    {
    // Abort if Calender panel is not shown	
    	if (!showPanel(CALENDARPROXY))    		
    		return;
    		
        final String username   = getValueFromComboBoxEntry(m_oComboBox_EditSchedulePanel_Users).trim();
        final User user = m_oGUIListener.getUser(username);
        final Integer currentUserID = new Integer(user.getId());
        resetReleaseGui();
        
        allowedProxyUsersList.removeAllElements();
        allProxyUsersList.removeAllElements();
        if (username.length() > 0)
        {            
            if (user != null) 
            {
                try {
                    ReleaseCalender = user.getCalendar(true);
                    CalSecCollection = ReleaseCalender.getCalendarSecretaries();
                    final Vector idVector = new Vector();
                    
                    if (CalSecCollection!= null){
                        for (Iterator iter = CalSecCollection.iterator(); iter.hasNext();){
                            CalendarSecretaryRelationImpl calsec = (CalendarSecretaryRelationImpl) iter.next();
                            idVector.add(calsec.getSecretaryUserID());
                        }
                    }
                    // GUI fllen alle Benutzer durchgehen und entweder links oder rechts einfgen
                    List alist = getBenutzer();
                    for (Iterator iter = alist.iterator(); iter.hasNext();){
                        String nextEntry = (String) iter.next();
                        Integer userID = new Integer(getKeyFromKeyValue(nextEntry));
                        
                        if (idVector.contains(userID))
                        {
                            allowedProxyUsersList.insertElement(nextEntry);
                        }else if (userID.equals(currentUserID)){
                            allProxyUsersList.insertElement(nextEntry);
                        }
                    }
                    
                    //TODO: GUI fertig
                } catch (ContactDBException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }else{
            m_oGUIListener.getLogger().log(Level.WARNING, "CalendarUserSelect::actionPerformed: UserName == null");
        }
    }
    
    //----------------------------------------------------------------------------------
    private class CalendarUserSelect implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        {
            initCalendardLists();
        }
    }
    //----------------------------------------------------------------------------------
    
    private void resetReleaseGui(){
        //      Reset GUI 
        Date now = new Date();
		tfd_grantTo.setEnabled(false);
		tfd_grantTo.setText("");
		twds_startDate.setEnabled(false);
                twds_startDate.setData(now);
		twds_endDate.setEnabled(false);
                twds_endDate.setData(now);
		twds_appointmentReadStartDate.setEnabled(false);
                twds_appointmentReadStartDate.setData(now);
		twds_appointmentReadEndDate.setEnabled(false);
                twds_appointmentReadEndDate.setData(now);
		twds_appointmentWriteStartDate.setEnabled(false);
                twds_appointmentWriteStartDate.setData(now);
		twds_appointmentWriteEndDate.setEnabled(false);
                twds_appointmentWriteEndDate.setData(now);
		twrb_end1_date.setEnabled(false);
		twrb_end1_unlim.setEnabled(false);
		twrb_end1_unlim.setSelected(true);
		twrb_end2_date.setEnabled(false);
		twrb_end2_unlim.setEnabled(false);
		twrb_end2_unlim.setSelected(true);
		twrb_end3_date.setEnabled(false);
		twrb_end3_unlim.setEnabled(false);
		twrb_end3_unlim.setSelected(true);
    }
    private JComboBox m_oComboBox_AssociateUser_UserID;
    
    private ExtendedDataItemModel createAssociateUserPanel() 
    {
        JLabel userLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_User") + " ",null); //$NON-NLS-1$
        m_oComboBox_AssociateUser_UserID = createEntryComboBox(getBenutzer().toArray());
        JPanel userPanel = createEntryPanel(userLabel, m_oComboBox_AssociateUser_UserID);
        
        JPanel associateUserEntryPanel = createEntriesPanel();
        associateUserEntryPanel.add(userPanel);
        
        JButton button_associateUser = createButton(Messages.getString("GUI_Admin_Benutzer_Assoziieren"), new button_associateUser_clicked()); //$NON-NLS-1$
        //JButton button_deassociateUser = createButton(Messages.getString("GUI_Admin_Benutzer_Deassoziieren"), new button_deassociateUser_clicked()); //$NON-NLS-1$
        //button_deassociateUser.setCursor(m_oHandCursor)
        
        JPanel associateUserPanel = new JPanel(new BorderLayout());
        associateUserPanel.add(associateUserEntryPanel, BorderLayout.CENTER);
        //associateUserPanel.add(createButtonPanel(new JButton[] {button_associateUser, button_deassociateUser}), BorderLayout.EAST);
        associateUserPanel.add(createButtonPanel(new JButton[] {button_associateUser}), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Benutzer_Tab_assoziieren"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", associateUserPanel));
        
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_USER_ASSOCIATE));
        return item;    
    }
        
    public class button_deassociateUser_clicked implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            String username = getKeyFromComboBoxEntry(m_oComboBox_AssociateUser_UserID).trim();
            
            if (username.length() > 0)
            {
                User user = m_oGUIListener.getUser(username);
                if (user != null) 
                {
                    deassociateAddress(user);
                }
            }
            else
            {
                m_oControlListener.showMessage("Sie m?ssen einen Benutzer ausw?hlen!");
                m_oComboBox_AssociateUser_UserID.requestFocus(true);
            }      
        }
    }
    
    private boolean deassociateAddress(User user)
    {
        try
        {
            Address address = user.getAssociatedAddress();
            if (address != null)
            {
                AddressClientDAO aDAO = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO();
                aDAO.setAssociatedUser(address, null);
                aDAO.save(address);
            }
        } 
        catch (ContactDBException e)
        {
            return false;
        }
        return true;
    }
    
    private boolean associateAddress(User user, Address address)
    {
        try
        {
            AddressClientDAO aDAO = ApplicationServices.getInstance().getCurrentDatabase().getAddressDAO();
            aDAO.setAssociatedUser(address, null);
            aDAO.save(address);
        } 
        catch (ContactDBException e)
        {
            return false;
        }
        return true;
    }
    
    public class button_associateUser_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String username = getKeyFromComboBoxEntry(m_oComboBox_AssociateUser_UserID).trim();
            
            if (username.length() > 0)
            {
                m_oGUIListener.setWaiting(true);
                Address address = m_oGUIListener.getAddress();
                User user = m_oGUIListener.getUser(username);
                if ((address != null) && (user != null))
                {  
                    String addresslabel = "";
                    try
                    {
                        if (user.getAssociatedAddress() != null)
                        {
                            deassociateAddress(user);
                        }
                        
                        addresslabel = address.getShortAddressLabel();
                        addLogText(Messages.getFormattedString("GUI_Admin_AssociateUser_Associating_User", (Object)user, (Object)addresslabel));
                        
                        associateAddress(user, address);	          
                    } 
                    catch (ContactDBException e1)
                    {
                        e1.printStackTrace();
                    }
                }        
                
                updateBenutzerBoxes();
                m_oGUIListener.setWaiting(false);
            }
            else
            {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_AssociateUser_No_User_Selected"));
                m_oComboBox_AssociateUser_UserID.requestFocus(true);
            }
        }
    }
    
    
    //---------------------------------------------------------------------------
    
    
    
    private JComboBox m_oComboBox_ChangeUser_UserID;
    private JTextField m_oTextField_ChangeUser_UserName;
    private JTextField m_oTextField_ChangeUser_UserFamilyName;
    private JTextField m_oTextField_ChangeUser_Password;
    private JTextField m_oTextField_ChangeUser_Password2;
    private JTextField m_oTextField_ChangeUser_Email;
    
    private ExtendedDataItemModel createChangeUserPanel() 
    {
        JLabel userIdLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Change_UserID") + " ",null); //$NON-NLS-1$
        m_oComboBox_ChangeUser_UserID = createEntryComboBox(getBenutzer().toArray()); //$NON-NLS-1$    
        m_oComboBox_ChangeUser_UserID.addActionListener(new ActionListener() 
                { 	
            public void actionPerformed(ActionEvent e) 
            {
                m_oGUIListener.setWaiting(true);
                updateChangeUserBox();
                m_oGUIListener.setWaiting(false);
            }
                });    
        JPanel userIdPanel = createEntryPanel(userIdLabel, m_oComboBox_ChangeUser_UserID);
        
        JLabel userNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Change_UserName") + " ",null); //$NON-NLS-1$
        m_oTextField_ChangeUser_UserName = createEntryTextField("", m_iMaxLength_ChangeUser_UserName); //$NON-NLS-1$
        JPanel userNamePanel = createEntryPanel(userNameLabel, m_oTextField_ChangeUser_UserName);
        
        JLabel userFamilyNameLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Change_UserFamilyName") + " ",null); //$NON-NLS-1$
        m_oTextField_ChangeUser_UserFamilyName = createEntryTextField("", m_iMaxLength_ChangeUser_UserFamilyName); //$NON-NLS-1$
        JPanel userFamilyNamePanel = createEntryPanel(userFamilyNameLabel, m_oTextField_ChangeUser_UserFamilyName);
        
        JLabel passwordLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Change_Password") + " ",null); //$NON-NLS-1$
        m_oTextField_ChangeUser_Password = new JPasswordField();// createEntryTextField(""); //$NON-NLS-1$
        addLengthRestriction(m_oTextField_ChangeUser_Password, m_iMaxLength_ChangeUser_Password);
        JPanel passwordPanel = createEntryPanel(passwordLabel, m_oTextField_ChangeUser_Password);
        
        JLabel passwordLabel2 = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Change_Password2") + " ",null); //$NON-NLS-1$
        m_oTextField_ChangeUser_Password2 = new JPasswordField();// createEntryTextField(""); //$NON-NLS-1$
        addLengthRestriction(m_oTextField_ChangeUser_Password2, m_iMaxLength_ChangeUser_Password);
        JPanel passwordPanel2 = createEntryPanel(passwordLabel2, m_oTextField_ChangeUser_Password2);
        
        JLabel emailLabel = createEntryLabel(Messages.getString("GUI_Admin_Benutzer_Change_Email") + " ",null); //$NON-NLS-1$
        m_oTextField_ChangeUser_Email = createEntryTextField("", m_iMaxLength_ChangeUser_Email); //$NON-NLS-1$
        JPanel emailPanel = createEntryPanel(emailLabel, m_oTextField_ChangeUser_Email);
        
        
        JPanel changeUserEntryPanel = createEntriesPanel();
        changeUserEntryPanel.add(userIdPanel);
        changeUserEntryPanel.add(userNamePanel);
        changeUserEntryPanel.add(userFamilyNamePanel);
        changeUserEntryPanel.add(passwordPanel);
        changeUserEntryPanel.add(passwordPanel2);
        changeUserEntryPanel.add(emailPanel);
        
        JButton button_changeUser = createButton(Messages.getString("GUI_Admin_Benutzer_Change_Ausfuehren"), new button_changeUser_clicked()); //$NON-NLS-1$
        
        JPanel changeUserPanel = new JPanel(new BorderLayout());
        changeUserPanel.add(changeUserEntryPanel, BorderLayout.CENTER);
        changeUserPanel.add(createButtonPanel(button_changeUser), BorderLayout.EAST);
        
        ExtendedDataItemModel item = new ExtendedDataItemModel(Messages.getString("GUI_Admin_Benutzer_Change"));
        item.addLabeledComponent(new ExtendedDataLabeledComponent("", "", changeUserPanel));
        
        updateChangeUserBox();
        item.setIcon(GUIHelper.getIcon(GUIHelper.ICON_ADMIN_TAB_USER_CHANGE));
        return item;    
    }
    
    public class button_changeUser_clicked implements ActionListener
    { 
        public void actionPerformed(ActionEvent e)
        {
            String userId = getKeyFromComboBoxEntry(m_oComboBox_ChangeUser_UserID).trim();
            String userName = m_oTextField_ChangeUser_UserName.getText().trim();
            String userFamilyName = m_oTextField_ChangeUser_UserFamilyName.getText().trim();
            String password = m_oTextField_ChangeUser_Password.getText().trim();
            String password2 = m_oTextField_ChangeUser_Password2.getText().trim();
            String email =  m_oTextField_ChangeUser_Email.getText().trim();
            
            if (!password.equals(password2)) {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_ChangeUser_Passwords_Not_Equal"));
                m_oTextField_ChangeUser_Password.requestFocus(true);
                return;
            }

            if (userFamilyName.length() == 0) {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_ChangeUser_No_SurName"));
                m_oTextField_ChangeUser_UserFamilyName.requestFocus(true);
                return;
            }

            if (userName.length() == 0) {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_ChangeUser_No_ForeName"));
                m_oTextField_ChangeUser_UserName.requestFocus(true);
                return;
            }

            if (userId.length() == 0) {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_ChangeUser_No_UserID"));
                m_oComboBox_ChangeUser_UserID.requestFocus(true);
                return;
            }

            if (email.length() == 0) {
                m_oControlListener.showMessage(Messages.getString("GUI_Admin_ChangeUser_No_Email"));
                m_oTextField_ChangeUser_Email.requestFocus(true);
                return;
            }
            
            m_oGUIListener.setWaiting(true);
            m_bUserChanged = false;
            addLogText(Messages.getFormattedString("GUI_Admin_ChangeUser_Changing_User", (Object)userId, (Object)userName));                
            m_oGUIListener.userRequestEditUser(userId, password, userFamilyName, userName, email);
            updateBenutzerBoxes();
            try {
				updateSchedulePanel();
			} catch (ContactDBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            initCalendardLists();
            showResult(m_bUserChanged); 
            updateKategorienBoxes(false);
            addLogTextEndMarker();
            m_oGUIListener.setWaiting(false);
        }
    }
    

    protected void updateChangeUserBox() 
    {
        String userid = getValueFromComboBoxEntry(m_oComboBox_ChangeUser_UserID);
        User user = m_oGUIListener.getUser(userid, false); // false schaltet chahing aus!!!
       
        try
        {
            m_oTextField_ChangeUser_UserName.setText(asString(user.getAttribute(User.KEY_FIRST_NAME)));
            m_oTextField_ChangeUser_UserFamilyName.setText(asString(user.getAttribute(User.KEY_LAST_NAME)));
            m_oTextField_ChangeUser_Password.setText(user.getAttribute(User.KEY_PASSWORD));
            m_oTextField_ChangeUser_Password2.setText(user.getAttribute(User.KEY_PASSWORD));
            m_oTextField_ChangeUser_Email.setText(m_oGUIListener.getUserParameter(user.getAttribute(User.KEY_LOGIN_NAME ), "email"));           
        } catch (ContactDBException e) {
            e.printStackTrace();
        }
    }
    
    protected void updateGroupBoxes()
    {
        m_oComboBox_EditGroup_GroupID.setModel(new DefaultComboBoxModel(getBenutzerGruppen().toArray()));
        m_oComboBox_DeleteGroup_GroupID.setModel(new UpdateableComboboxModel(getBenutzerGruppen(false, false).toArray()));

        if (m_oComboBox_DeleteGroup_GroupID.getModel().getSize() != 0){
            m_oComboBox_DeleteGroup_GroupID.setSelectedIndex(0);
            //m_oComboBox_EditGroup_GroupID.setSelectedIndex(0); 		
        }
        updateEditGroupPanel();
    }

    protected void updateGlobalRoleBoxes()
    {
        if (m_oComboBox_CreateGroup_RoleID != null) 
            updateBox(m_oComboBox_CreateGroup_RoleID, getGlobalRoles());
        if (m_oComboBox_EditGroup_RoleID != null) {
            isInUpdate_m_oComboBox_EditGroup_RoleID = true;
            updateBox(m_oComboBox_EditGroup_RoleID, getGlobalRoles());
            isInUpdate_m_oComboBox_EditGroup_RoleID = false;
        }
    }
	
	protected void updateGlobalRoleBox()
	{
		List roleslist = getGlobalRoles();
        m_vectorGlobalRolesData.clear();
        m_tmodelGlobalRoles.setDataVector(m_vectorGlobalRolesData, m_vectorGlobalRolesHeader);
        Iterator it = roleslist.iterator();
		while(it.hasNext()){
			GlobalRole role = (GlobalRole)it.next();
			_oneGRRow = new Vector();
        	_oneGRRow.add(role.getRoleName());
        	_oneGRRow.add(new Boolean(role.hasRightEditUser()));
        	_oneGRRow.add(new Boolean(role.hasRightRemoveUser()));
        	_oneGRRow.add(new Boolean(role.hasRightEditFolder()));
        	_oneGRRow.add(new Boolean(role.hasRightRemoveFolder()));
        	if (showPanel(CALENDARPROXY)){
        		_oneGRRow.add(new Boolean(role.hasRightEditSchedule()));
        		_oneGRRow.add(new Boolean(role.hasRightRemoveSchedule()));
        	}
        	_oneGRRow.add(new Boolean(role.hasRightDelete()));
			_oneGRRow.add(role);
            m_tmodelGlobalRoles.addRow(_oneGRRow);
		}

	}
	
	protected void updateRightsManagementPanel(){
		
		try {
			m_vectorRMData.clear();
			m_tmodelRM.setDataVector(m_vectorRMData, m_vectorRMHeader);
			Collection rows = GroupObjectRoleImpl.getGroupObjectRoles(null);
			Iterator it = rows.iterator();
			while(it.hasNext()){
				Object row = it.next();
				m_vectorRMData.add((GroupObjectRoleConnector)row);
			}
			m_tmodelRM.setDataVector(m_vectorRMData, m_vectorRMHeader);
		} catch (ContactDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void updateRightsManagementBoxes(){
		try {
			if (m_oComboBox_RMrole != null) m_oComboBox_RMrole.setModel(new DefaultComboBoxModel(ObjectRoleImpl.getObjectRoles(null).toArray()));
			if (m_oComboBox_RMgroup != null) m_oComboBox_RMgroup.setModel(new DefaultComboBoxModel(getBenutzerGruppenObjects(true).toArray()));
			if (m_oComboBox_RMobject != null) m_oComboBox_RMobject.setModel(new DefaultComboBoxModel(getGrantableCategoryObjects().toArray()));
		} catch (ContactDBException e) {
            logger.warningSilent("[!] failed updating tripple rights combo boxes",e);
		}
	}

    
    // ---------------------------------------------------------------------------------------------------------------
    
    protected void flushCreateBenutzerPanel(){
    	
    	m_oTextField_CreateUser_UserID.setText("");
	    m_oTextField_CreateUser_UserName.setText("");
	    m_oTextField_CreateUser_UserFamilyName.setText("");
	    m_oTextField_CreateUser_Password.setText("");
		m_oTextField_CreateUser_Password2.setText("");
		m_oTextField_CreateUser_Email.setText("");
		//m_oCheckBox_CreateUser_Special.setText("");
		
    }
    
    /*
     * Methoden zum Auffrischen von Combobox-Inhalten
     */
    
    protected void updateBenutzerBoxes() 
    {
        List benutzer = getBenutzer();
        
       
        //updateBox(m_oComboBox_SetStandardkategorie_UserID, benutzer);
        //updateBox(m_oComboBox_, benutzer);
        //updateBox(m_oComboBox_ChangeUser_UserID, benutzer);
        updateBox(m_oComboBox_DeleteUser_UserID, benutzer);
        //updateBox(m_oComboBox_AssociateUser_UserID, benutzer);
        updateBoxFuzzy(m_oComboBox_ChangeUser_UserID, benutzer); 
        updateChangeUserBox();    
    }
    
    
    protected void updateUserKategorienBoxes() 
    {
        //updateStandardKategorieBox();
        //updateShowKategorieBox();
        //updateHideKategorieBox();
        //updateShowVerteilerList();
        updateKategorienBoxes();
    }
    
    protected void updateKategorienBoxes(){
    	updateKategorienBoxes(false);
    }
    
    /**
     * L?dt alle Comboboxen und Listen im Kategorien-Tab neu 
     * @param reloadFromServer : Gibt an, ob vor dem Refresh die Kategorieliste neu vom Server geladen werden
     * soll. Das kann n?tig sein, da man ansonsten unerwnschte Cashing-Effekt haben kann 
     * (Letzte ?nderungen werden dann nicht angezeigt) 
     */
    protected void updateKategorienBoxes(boolean reloadFromServer){
    	
    	if (reloadFromServer){
	    	try {
				m_oGUIListener.reloadCategories();
			} catch (ContactDBException e) {
				m_oGUIListener.getLogger().log(Level.WARNING, Messages.getString("GUI_Admin_Refresh_Categories_Error"));
				//e.printStackTrace();
			}
    	}
    	
        List kategorien = getKategorien();
        //List kategorien_private = getKategorien(true, false, false);
        List kategorien_delete = getKategorien(false, false, false);
        List kategorien_virt_trash = getKategorien(false, true, true);
        List kategorienAddSub = getUserCategoriesWithCategoryRight(6, false, true);
        List kategorienEditSub = getUserCategoriesWithCategoryRight(6, true, false);
        List kategorienRemSub = getUserCategoriesWithCategoryRight(7, false, false);
        
        if (m_oComboBox_CreateVerteiler_Kategorie != null) updateBox(m_oComboBox_CreateVerteiler_Kategorie, kategorienAddSub);
        if (m_oComboBox_DeleteVerteiler_Kategorie != null) updateBox(m_oComboBox_DeleteVerteiler_Kategorie, kategorienRemSub);
        if (m_oComboBox_ShowVerteiler_Kategorie != null) updateBox(m_oComboBox_ShowVerteiler_Kategorie, kategorien);
        if (m_oComboBox_DeleteKategorie_Kategorie != null) updateBox(m_oComboBox_DeleteKategorie_Kategorie, kategorien_delete);

        if (m_oComboBox_EditCategory_Kategorie != null) updateBox(m_oComboBox_EditCategory_Kategorie, kategorien_virt_trash);
        if (m_oComboBox_EditVerteiler_Kategorie != null) updateBox(m_oComboBox_EditVerteiler_Kategorie, kategorienEditSub);
        if ((m_oComboBox_EditVerteiler_Kategorie != null) && (m_oComboBox_EditVerteiler_SubKategorie != null))
        {
            updateEditVerteilerList();
        }        
        if (m_oComboBox_EditCategory_Kategorie != null) updateEditCategoryPanel(); 
    }
    
    protected void updateShowKategorieBox() 
    {
    	//System.out.println("m_oComboBox_ShowKategorie_UserID: "+m_oComboBox_ShowKategorie_UserID);
        String user = getValueFromComboBoxEntry(m_oComboBox_ShowKategorie_UserID);
        List kategorien = getKategorien();
        kategorien.removeAll(getUserKategorien(user));
        updateBox(m_oComboBox_ShowKategorie_Kategorie, kategorien);
    }
      
    
    
    //  protected void updateEditKategorieBox() 
    //  {
    //    String user = getKeyFromComboBoxEntry(m_oComboBox_EditCategory_Kategorie);
    //    List kategorien = getUserKategorien(user);
    //    if (! updateBox(m_oComboBox_EditCategory_Kategorie, kategorien))
    //    {
    //      // delete TextFields...
    //      m_oTextField_EditCategory_Key.setText("");
    //      m_oTextField_EditCategory_Name.setText("");
    //      m_oTextField_EditCategory_Description.setText("");
    //    }
    //  }
    
    protected void updateHideKategorieBox() 
    {
        String user = getValueFromComboBoxEntry(m_oComboBox_HideKategorie_UserID);
        List kategorien = getUserKategorien(user);
        updateBox(m_oComboBox_HideKategorie_Kategorie, kategorien);
    }
    
    protected void updateStandardKategorieBox() 
    {
        // Es gibt keine Standard Kategorien mehr.
        //         String user = getValueFromComboBoxEntry(m_oComboBox_SetStandardkategorie_UserID);               
        //         String standard = m_oGUIListener.getStandardCategory(user);
        //         Map kategorienMap = m_oGUIListener.getCategories(user);
        //         updateBox(m_oComboBox_SetStandardkategorie_Kategorie, getUserKategorien(user));
        //         if (standard != null && kategorienMap.containsKey(standard))
        //             m_oComboBox_SetStandardkategorie_Kategorie.setSelectedItem(combineKeyValue(standard, asString(kategorienMap.get(standard))));
    }
    
    protected boolean updateBox(JComboBox combobox, List items)         
    {
        if (combobox == null)
            return false;
        Object current = combobox.getSelectedItem();
        combobox.removeAllItems();
        Iterator it = items.iterator();
        while (it.hasNext()) combobox.addItem(it.next());
        
        if (current != null) 
        {
            Object comboitem = combobox.getSelectedItem();
            combobox.setSelectedItem(current);
            if (comboitem == null) return false;
            else return comboitem.equals(combobox.getSelectedItem());
        }
        else
        {
            return false;      
        }
    }
    
    protected void updateBoxFuzzy(JComboBox combobox, List items) 
    {
        String current = getKeyFromComboBoxEntry(combobox).trim();
        
        combobox.removeAllItems();
        Iterator it = items.iterator();
        while (it.hasNext()) combobox.addItem(it.next());
        
        if (current != null) 
        {
            for(int i=0; i<(combobox.getItemCount()); i++)
            {
                String itemtext = asString(combobox.getItemAt(i));
                if (itemtext.startsWith(current)) combobox.setSelectedIndex(i);
            }
        }
    }
      
    protected void updateList(tarentList list, List items) 
    {
        int current = list.getSelectedIndex();
        list.removeAllElements();
        list.insertElements(items);
        list.setSelectedIndex(current);
    }
    
    /*
     * Die folgenden Methoden arbeiten Daten aus dem oder f?r den GUIListeners auf.
     */

    protected List getGrantableCategoryObjects() 
    {
        User currentUser = m_oGUIListener.getUser(null);
        List list = new LinkedList();
        //FIXME (BUG 1448): Das darf nicht ber die Standart-Kategorie-holen Mechanismen laufen, da die nur Kategorien holen, bei denen AUTH_READ gesetzt ist
        // (per JOIn auf v_user_folder), wir brauchen hier die die AUTH_GRANT gesetzt haben, unabh?ngig von AUTH_READ
        List fullList = m_oGUIListener.getGrantableCategoriesObjectList();
        for (Iterator iter = fullList.iterator(); iter.hasNext();) {
            Category cat = (Category)iter.next();           
            String catid = null;
            try {
                catid = cat.getIdAsString();
                if ((! cat.isPrivate().booleanValue())
                    && currentUser.getCategoryRights(catid).hasRightGrant())
                    list.add(cat);            
            } catch (ContactDBException cde) {
                // ignore
            }
        }
        
        Collections.sort(list, new CategoryComparator());
        return list;
    }

    
    protected List getKategorien(boolean includePrivate, boolean includeVirtual, boolean includeTrash) 
    {
        // debug
        /*Iterator it = m_oGUIListener.getAllCategories().entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry entry = (Map.Entry) it.next();

            try
            {
                System.out.println("getKategorien "+ ((Category)entry.getValue()).getName());
            }
            catch (ContactDBException e)
            {
                e.printStackTrace();
            }
        }*/
    	List kategorieList = prepareCategoriesForComboBox(m_oGUIListener.getAllCategories().entrySet().iterator(), includePrivate, includeVirtual, includeTrash);
                return kategorieList;
    }   

    protected List getKategorien() 
    {
    	List kategorieList = prepareCategoriesForComboBox(m_oGUIListener.getAllCategories().entrySet().iterator(), false, false, false);
    	return kategorieList;
    }    

    
    
    /**
     * Liefert eine Liste von Kategorien, auf die der User ein bestimmtes Recht besitzt 
     * @param right das abgefragt Kategorierecht
     * @param includeVirtual , Flag, ob virtuelle Kategorien mit bercksichtigt werden
     * @param showCategoriesWithNoSubs, Flag, ob Kategorien ohne U-Kats mitgeliefert werden
     * @return
     */
    
    protected List getUserCategoriesWithCategoryRight(int right, boolean includeVirtual, boolean showCategoriesWithNoSubs){
    	
    	
    	List kategorieList = new ArrayList();
        Map tmpList = m_oGUIListener.getAllCategories();
        
        Map.Entry entry;
        Category category;
        
        	for (Iterator it = tmpList.entrySet().iterator(); it.hasNext();){
			entry = (Map.Entry)it.next();
			category = (Category) entry.getValue();
			 
			if (m_oGUIListener.userRequestUserHasRightOnFolder(category.getId(), right)){
				
				if ((includeVirtual || (!includeVirtual && !category.getVirtual().booleanValue())) 
					&& (showCategoriesWithNoSubs || (!showCategoriesWithNoSubs && category.getSubCategories().size() > 0)))
				
					kategorieList.add(category);
			}
			else{
				//System.out.println("Kategorie " + katname + " wird nicht angezeigt in Combobox");
			}
		}
	
		Collections.sort(kategorieList, new CategoryComparator());
		
        return kategorieList; 
        
    }
    
    protected List getUserKategorien(String user) 
    {
        return prepareCategoriesForComboBox(m_oGUIListener.getCategories(user).entrySet().iterator(), false, false, false);
    }
    
    protected List prepareCategoriesForComboBox(Iterator kategorieMapping, boolean includePrivate, boolean includeVirtual, boolean includeTrash) 
    {
        List kategorien = new ArrayList();
        while (kategorieMapping.hasNext()) 
        {
            Map.Entry entry = (Map.Entry) kategorieMapping.next();
            String cbLine = null;
            if (entry.getValue() instanceof Category){ 
                    Category cat = (Category) entry.getValue();
                    if ( (!includeVirtual && cat.getVirtual().booleanValue())
                         || (!includePrivate && cat.isPrivate().booleanValue())
                         || (!includeTrash && cat.isTrash()))
                        continue;
                    cbLine = cat.getName();
            }
            else 
                cbLine = entry.getValue().toString();
            
            kategorien.add(combineKeyValue(entry.getKey().toString(), cbLine));
        }
        Collections.sort(kategorien, new ValueComparator());
        return kategorien;
    }
    
    private boolean checkForDublicatedUserName(String u_name){
    	
    	
        Map users = m_oGUIListener.getBenutzer();
        Iterator benutzerMapping = users.entrySet().iterator();
        while (benutzerMapping.hasNext()) 
        {
            Map.Entry entry = (Map.Entry) benutzerMapping.next();
            if (u_name.equals(entry.getValue().toString())){
            	return true;
            }
        }
    	
    	return false;    	   	
    }
    
    protected List getBenutzer() 
    {
        List benutzer = new ArrayList();
        Map users = m_oGUIListener.getBenutzer();
        Iterator benutzerMapping = users.entrySet().iterator();
        while (benutzerMapping.hasNext()) 
        {
            Map.Entry entry = (Map.Entry) benutzerMapping.next();
            benutzer.add(combineKeyValue(entry.getKey().toString(), entry.getValue().toString()));
        }
        Collections.sort(benutzer, new ValueComparator());
        return benutzer;
    }
	
	protected List getGlobalRoles(){
		
		List roles = m_oGUIListener.getGlobalRoles();
		Collections.sort(roles, new RoleComparator());
		return roles;
	}
	
    
//	protected List getFolderRoles(){
//		
//		List roles = m_oGUIListener.getFolderRoles();
//		Collections.sort(roles, new RoleComparator());
//		return roles;
//	}

	protected List getBenutzerGruppen() {
        return getBenutzerGruppen(false, true);
    }
    
	protected List getBenutzerGruppen(boolean includePrivate, boolean includeAdmin) 
    {
        adminGroups.clear();
        List gruppen = new ArrayList();
        Iterator gruppenMapping = m_oGUIListener.getUserGroups().iterator();
        while (gruppenMapping.hasNext()) 
        {
            UserGroup entry = (UserGroup) gruppenMapping.next();
            if (!includePrivate && entry.isPrivate())
                continue;            
            try {
                if (entry.isAdminGroup())
                {
                    adminGroups.add(entry.getAttribute(UserGroup.KEY_NAME));
                    if (!includeAdmin) continue;
                }
                    
            } catch (ContactDBException e) {
                e.printStackTrace();
            }
            try {
                gruppen.add(entry.getAttribute(UserGroup.KEY_NAME));
            } catch (ContactDBException e) {
                e.printStackTrace();
            }
        }
        Collator myCollator = Collator.getInstance();
        Collections.sort(gruppen,myCollator);
        return gruppen;
    }

	protected List getBenutzerGruppenObjects(boolean includePrivate) 
    {
        List gruppen = new ArrayList();
        for (Iterator gruppenMapping = m_oGUIListener.getUserGroups().iterator(); gruppenMapping.hasNext();) 
        {
            UserGroup entry = (UserGroup) gruppenMapping.next();
            if (!includePrivate && entry.isPrivate())
                continue;            
            gruppen.add(entry);
        }
        Collections.sort(gruppen, new UserGroupComparator());
        
        return gruppen;
    }
    
    protected final static String KEY_VALUE_DIV = ": "; //$NON-NLS-1$
    
    static class ValueComparator implements Comparator 
    {
        public int compare(Object o1, Object o2) 
        {
            String s1 = getValueFromKeyValue(asString(o1));
            String s2 = getValueFromKeyValue(asString(o2));
            if (s1 == null) return (s2 == null) ? 0 : -1;
            if (s2 == null) return 1;
            int i = Collator.getInstance().compare(s1.trim(),s2.trim());
            if (i == 0) i = Collator.getInstance().compare(s1.trim(),s2.trim());
            if (i == 0) i = Collator.getInstance().compare(s1,s2);
            return i;
        }
    }
    
    static class ToStringComparator implements Comparator
    {
        public int compare(Object o1, Object o2) 
        {
            String s1 = o1.toString();
            String s2 = o2.toString();
            if (s1 == null) return (s2 == null) ? 0 : -1;
            if (s2 == null) return 1;
            int i = Collator.getInstance().compare(s1.trim(),s2.trim());
            if (i == 0) i = Collator.getInstance().compare(s1.trim(),s2.trim());
            if (i == 0) i = Collator.getInstance().compare(s1,s2);
            return i;
        }
    }
    
//    static class RoleComparator implements Comparator {
//    	
//    	public int compare(Object o1, Object o2){
//    		String s1 = ((Role) o1).getRoleName().toLowerCase();
//    		String s2 = ((Role) o2).getRoleName().toLowerCase();
//		
//    		return Collator.getInstance().compare(s1,s2);
//    	}
//    }
    
    static class UserGroupComparator implements Comparator {
    	
    	public int compare(Object o1, Object o2){
    		String s1 = "";
			String s2 = "";
			try {
				s1 = ((UserGroup) o1).getAttribute(UserGroup.KEY_NAME).toLowerCase();
				s2 = ((UserGroup) o2).getAttribute(UserGroup.KEY_NAME).toLowerCase();
			} catch (ContactDBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    return Collator.getInstance().compare(s1,s2);
    	}
    }
    
    static class CategoryComparator implements Comparator {
    	
    	public int compare(Object o1, Object o2){
		    
			String s1 = ((Category) o1).getName() != null? ((Category) o1).getName().toLowerCase() : "";
			String s2 = ((Category) o2).getName() != null? ((Category) o2).getName().toLowerCase() : "";
			
		    return Collator.getInstance().compare(s1,s2);
	  	}
    }
    
    
    
    protected static String combineKeyValue(String key, String value) 
    {
        return key + KEY_VALUE_DIV + value;
    }
    
    protected static String getKeyFromKeyValue(String keyValue) 
    {
        if (keyValue == null) return null;
        int index = keyValue.indexOf(KEY_VALUE_DIV);
        return index > 0 ? keyValue.substring(0, index) : null;
    }
    
    protected static String getValueFromKeyValue(String keyValue) 
    {
        if (keyValue == null) return null;
        int index = keyValue.indexOf(KEY_VALUE_DIV);
        return index > 0 ? keyValue.substring(index + KEY_VALUE_DIV.length()) : keyValue;
    }
    
    protected static String getKeyFromComboBoxEntry(JComboBox combobox) 
    {
        if (combobox == null)return null;
        
        Object currentobj = combobox.getSelectedItem();
        return (currentobj != null) ? getKeyFromKeyValue(currentobj.toString()) : null;
        
    }
    
    protected static String getValueFromComboBoxEntry(JComboBox combobox) 
    {
        if (combobox == null)
            return null;
        Object currentobj = combobox.getSelectedItem();
        return (currentobj != null) ? getValueFromKeyValue(currentobj.toString()) : null;
    }
    
    /*
     * Die folgenden Methoden erzeugen Komponenten, aus denen das 
     * Admin-Panel zusammengesetzt wird.
     */
    
    static final private Dimension LABEL_DIMENSION = new Dimension(100, 20);
    
    protected JLabel createEntryLabel(String title,Dimension OptionalDimension) 
    {
        JLabel label = new JLabel(title, SwingConstants.RIGHT);
        if (OptionalDimension == null){
            label.setMinimumSize(LABEL_DIMENSION);
            label.setMaximumSize(LABEL_DIMENSION);
            label.setPreferredSize(LABEL_DIMENSION);    
        }else{
            label.setMinimumSize(OptionalDimension);
            label.setMaximumSize(OptionalDimension);
            label.setPreferredSize(OptionalDimension);    
        }
        return label;
    }
    
    protected JLabel createEntryViewField(String title) 
    {
        JLabel label = new JLabel(title, SwingConstants.LEFT);
        return label;
    }
    
    protected JTextField createEntryTextField(String initialEntry, int maxlength) 
    {
        JTextField textfield = new JTextField(initialEntry);
        addLengthRestriction(textfield, maxlength);
        return textfield;
    }
    
    protected JCheckBox createEntryCheckBox(boolean initialEntry, String text) {
        JCheckBox checkbox = new JCheckBox(text, initialEntry);
        return checkbox;
    }
    
    
    protected JComboBox createEntryComboBox(Object[] boxEntries) {
        JComboBox combobox = new JComboBox(boxEntries);
        return combobox;
    }
    
    protected JComboBox createEntryComboBox(ComboBoxModel model)
    {
        JComboBox combobox = new JComboBox( model );
        return combobox;
    }
    
    private class UpdateableComboboxModel implements ComboBoxModel
    {
        private Object[] data;
        private Object selectedItem = "";
        private List listener;
        public UpdateableComboboxModel(Object[] data)
        {
            this.data = data;
            this.listener = new ArrayList();
        }

        private void update()
        {
            Iterator it = listener.iterator();
            while (it.hasNext())
            {
                ListDataListener l = (ListDataListener)it.next();
                l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()-1));
                l.contentsChanged(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, getSize()-1));
                l.contentsChanged(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0, getSize()-1));
            }
        }
        
        public int getSize()
        {
            return data.length;
        }

        public Object getElementAt(int index)
        {
            return data[ index ];
        }

        public void addListDataListener(ListDataListener l)
        {
            listener.add( l );
        }

        public void removeListDataListener(ListDataListener l)
        {
            listener.add( l );
        }
        
        public void refresh(Object[] data)
        {
            this.data = data;
            update();
        }

        public void setSelectedItem(Object anItem)
        {
            selectedItem = anItem;
        }

        public Object getSelectedItem()
        {
            return selectedItem;
        }
        
        
    }
    
    
    private class RoleComboboxModel implements MutableComboBoxModel
    {
        private List data;
        private Object selectedItem = null;
        private List listener;
        public RoleComboboxModel(List data)
        {
            this.data = data;
            this.listener = new ArrayList();
        }

        private void update()
        {
            Iterator it = listener.iterator();
            while (it.hasNext())
            {
                ListDataListener l = (ListDataListener)it.next();
                l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()-1));
                l.contentsChanged(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, getSize()-1));
                l.contentsChanged(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0, getSize()-1));
            }
        }
        
        public int getSize()
        {
            return data.size();
        }

        public Object getElementAt(int index)
        {
            return data.get(index);
        }

        public void addListDataListener(ListDataListener l)
        {
            listener.add( l );
        }

        public void removeListDataListener(ListDataListener l)
        {
            listener.add( l );
        }
        
        public void refresh(List data)
        {
            this.data = data;
            update();
        }

        public void setSelectedItem(Object anItem)
        {
            selectedItem = anItem;
        }

        public Object getSelectedItem()
        {
            return selectedItem;
        }
        
        
        public boolean hasSelectedRoleAdminPrivilegs()
        {
            Iterator it = data.iterator();
            while (it.hasNext())
            {
                GlobalRole role = (GlobalRole)it.next();
                String roleName = role.getRoleName();
               
                if(roleName != null && roleName.equals(selectedItem))
                {                    
                    return role.isAdminRole();
                }
            }
            return false;
        }

        public void selectAdminRole()
        {
            Iterator it = data.iterator();
            while (it.hasNext())
            {
                GlobalRole role = (GlobalRole)it.next();
               
                if(role.isAdminRole()) {
                    setSelectedItem(role);
                    update();
                    return;
                }                            
            }
        }
        
        public GlobalRole getRoleAt(int index)
        {
            return (GlobalRole) data.get( index );
        }

        public void addElement(Object obj)
        {
            data.add( obj );
            update();
        }

        public void removeElement(Object obj)
        {
            data.remove( obj );
            update();
        }

        public void insertElementAt(Object obj, int index)
        {
            data.set(index, obj);
            update();
        }

        public void removeElementAt(int index)
        {
            data.remove( index );
            update();
        }
        
    }

    
    
    private class tarentList extends JList
    {
        private DefaultListModel m_oListModel;
        
        public tarentList()
        {
            super();
            m_oListModel = new DefaultListModel();
            this.setModel(m_oListModel);
        }
        
        public tarentList(Object[] entries)
        {
            this();
            insertElements(entries);
        }
        
        public tarentList(List entries)
        {
            this();
            insertElements(entries);
        }
        
        public void insertElements(Object[] entries)
        {
            if (entries != null){
                for(int i = 0; i<(entries.length); i++)
                {
                    m_oListModel.addElement(entries[i]);
                }
            }
        }
        
        public void insertElements(List entries)
        {
            Iterator it = entries.iterator();
            while (it.hasNext())
            {
                m_oListModel.addElement(it.next());
            }
        }
        
        public void insertElement(Object entry)
        {
            m_oListModel.addElement(entry);
        }
        
        public void removeAllElements()
        {
            m_oListModel.removeAllElements();
        }
        
        public DefaultListModel getDefaultModel()
        {
            return(m_oListModel);
        }
    }
    
    protected tarentList createEntryList(Object[] boxEntries){
    	return createEntryList(boxEntries, ListSelectionModel.SINGLE_SELECTION);
    }
    
    protected tarentList createEntryList(Object[] boxEntries, int selectionMode) 
    {
        tarentList list = new tarentList(boxEntries);
        //list.setVisibleRowCount(-1);
        list.setVisibleRowCount(5);
        list.setSelectionMode(selectionMode);
        return list;
    }
    
    protected JPanel createEntryPanel(JLabel label, JComponent textField) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(label, BorderLayout.WEST);
        textField.addFocusListener(new SelectAllOnFocusEventListener());
        panel.add(textField, BorderLayout.CENTER);
        return panel;
    }
    
    protected JPanel createComponentEntryPanelXAXIS(JLabel label,  JComponent aComponent) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(label);
        panel.add(aComponent);
        return panel;
    }
    
    
    protected JPanel createComponentEntryPanel(JComponent label, JComponent aComponent) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(label, BorderLayout.WEST);
        panel.add(aComponent, BorderLayout.CENTER);
        return panel;
    }
    
    
    
    private class SelectAllOnFocusEventListener implements FocusListener
    {
        public void focusGained(FocusEvent e)
        {
            Object source = e.getSource();
            if (source instanceof JTextField)
            {
                ((JTextField)source).selectAll();
            }
            else if (source instanceof JTextArea)
            {
                ((JTextArea)source).selectAll();
            }
        }
        
        public void focusLost(FocusEvent e)
        {
        }
    }
    
    
    protected JPanel createEntriesPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        return panel;
    }
    
    protected JPanel createButtonPanel(JButton button) {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.add(button);
        buttonPanel.add(Box.createVerticalGlue());
        return buttonPanel;
    }
    
    protected JPanel createButtonPanel(JButton[] buttons) 
    {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        for(int i=0; i<(buttons.length); i++) buttonPanel.add(buttons[i]);
        buttonPanel.add(Box.createVerticalGlue());
        return buttonPanel;
    }
    protected JPanel createButtonPanel_XAxis(JButton[] buttons) 
    {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        for(int i=0; i<(buttons.length); i++) {
            buttonPanel.add(buttons[i]);
            buttonPanel.add(Box.createHorizontalStrut(20));
        }
        buttonPanel.add(Box.createHorizontalGlue());
        return buttonPanel;
    }
    
    protected JPanel createGenericPanel_XAxis(JComponent[] components,boolean addEndingGlue) 
    {
        JPanel GenericPanel = new JPanel();
        GenericPanel.setLayout(new BoxLayout(GenericPanel, BoxLayout.X_AXIS));
        for(int i=0; i<(components.length); i++) GenericPanel.add(components[i]);
        if (addEndingGlue ) GenericPanel.add(Box.createHorizontalGlue());
        return GenericPanel;
    }
    protected JButton createButton(String text, ActionListener listener) {
        JButton button = new JButton(text);
        button.addActionListener(listener);
        return button;
    }
    
    protected JPanel createInnerPanel(String title) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(new EmptyBorder(5,5,5,5));
        if (title != null && title.length() > 0) {
            JLabel lblTitel = new JLabel(title, SwingConstants.LEFT);
            lblTitel.setBorder(new EmptyBorder(1,1,3,1));
            panel.add(lblTitel, BorderLayout.NORTH);
        }
        return panel;
    }
    
    protected JPanel createOuterPanel(JPanel innerPanel) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(2,2,2,2), new LineBorder(Color.BLACK, 1)));
        panel.add(innerPanel, BorderLayout.NORTH);
        return panel;
    }
    
    
    
    
    public void categoryAdded(String categoryID, String categoryName, String categoryDescription)
    {
        m_bCategoryAdded = true;
    }
    
    public void categoryEdited(String categoryID, String categoryName, String categoryDescription) 
    {
        m_bCategoryChanged = true;
    }
    
    public void categoryRemoved(String categoryID)
    {
        m_bCategoryRemoved = true;
    }
    
    public void categoryAssigned(String userID, String categoryID)
    {
    }
    
    public void categoryDeassigned(String userID, String categoryID)
    {
        m_bCategoryDeassigned = true;
    }
    
    public void standardCategoryAssigned(String userID, String categoryID)
    {
        m_bStandardCategoryAssigned = true;
    }
    
    public void subCategoryAdded(String categoryID, String id, String name1, String name2, String name3)
    {
        m_bSubCategoryAdded = true;
    }
    
    public void subCategoryEdited(String categoryID, String id, String name1, String name2, String name3) 
    {
        m_bSubCategoryChanged = true;
    }
    
    public void subCategoryRemoved(String categoryID, String id)
    {
        m_bSubCategoryRemoved = true;
    }
    
    public void userGroupAdded(UserGroup newGroup) {
        // TODO Auto-generated method stub
    }
    
    public void userAdded(String userID, String userFamilyName, String userName, String email)
    {
        m_bUserAdded = true;
    }
    
    public void userRemoved(String userID)
    {
        m_bUserRemoved = true;
    }
    
    public void userChanged(String userID, String userFamilyName, String userName, String email)
    {
        m_bUserChanged = true;
    }
    
    
    private final static String asString(Object o) {
        return o == null ? null : o.toString();
    }

    /* (non-Javadoc)
     * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
     */
    public void insertUpdate(DocumentEvent e) {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
     */
    public void removeUpdate(DocumentEvent e) {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
     */
    public void changedUpdate(DocumentEvent e) {
        // TODO Auto-generated method stub
        
    }



	/*
	 * TODO: Was macht diese Methode?
	 */
	protected void updateEditGroupPanel() {
		Iterator myUserGroupsIterator = m_oGUIListener.getUserGroups().iterator();
		while(myUserGroupsIterator.hasNext()){
		    UserGroup tmpUserGroup = (UserGroup) myUserGroupsIterator.next();
		    try {
		        if (tmpUserGroup.getAttribute(UserGroup.KEY_NAME).equals((String)m_oComboBox_EditGroup_GroupID.getItemAt(m_oComboBox_EditGroup_GroupID.getSelectedIndex()))){
		            _currentUserGroup = tmpUserGroup;
		            allGroupUsersList.getDefaultModel().clear();
		            allowedGroupUsersList.getDefaultModel().clear();
		            m_oTextField_EditGroup_GroupID.setText(tmpUserGroup.getAttribute(UserGroup.KEY_NAME));
		            m_oTextField_EditGroup_Description.setText(tmpUserGroup.getAttribute(UserGroup.KEY_DESCRIPTION));
		            Iterator UsersIter = tmpUserGroup.getUsers().iterator();
		            while (UsersIter.hasNext()){
		                User tmpUser = (User) UsersIter.next();
		                allowedGroupUsersList.getDefaultModel().addElement(tmpUser.getAttribute(User.KEY_LOGIN_NAME));						
		            }
		            //Restliche Systemuser in andere Listbox fllen
		            Iterator myAllUsersIter = m_oGUIListener.getBenutzer().entrySet().iterator();
		            while (myAllUsersIter.hasNext()){
		                Map.Entry tmpMap = (Map.Entry) myAllUsersIter.next();
		                if (!allowedGroupUsersList.getDefaultModel().contains(tmpMap.getValue().toString())) allGroupUsersList.getDefaultModel().addElement(tmpMap.getValue().toString());
		            }
		            Object[] _tmpsort = allGroupUsersList.getDefaultModel().toArray(); 
		            Arrays.sort(_tmpsort, new ToStringComparator());
		            allGroupUsersList.getDefaultModel().removeAllElements();
		            allGroupUsersList.insertElements(_tmpsort);
		            //Rolle korrekt anzeigen
		            for(int i=0; i<m_oComboBox_EditGroup_RoleID.getItemCount(); i++){
		            	GlobalRole tmp = ((RoleComboboxModel) m_oComboBox_EditGroup_RoleID.getModel()).getRoleAt( i );
		            	if(String.valueOf(tmp.getId()).equals(_currentUserGroup.getAttribute(UserGroup.KEY_GLOBALROLE))){
                            isInUpdate_m_oComboBox_EditGroup_RoleID = true;
		            		m_oComboBox_EditGroup_RoleID.setSelectedIndex(i);
                            isInUpdate_m_oComboBox_EditGroup_RoleID = false;
		            	}
		            }
		        }
		    } catch (ContactDBException e1) {
		        // TODO Auto-generated catch block
		        e1.printStackTrace();
		    }
		}
	}
     
  
  private void addLengthRestriction(JTextField textfield, int maxlen)
  {
    if (maxlen != -1) textfield.setDocument(new SizeRestrictedDocument(maxlen));    
  }
  
  public class SizeRestrictedDocument extends PlainDocument
  {
    private int m_iMaxChars;
    
    public SizeRestrictedDocument(int maxchars)
    {
      m_iMaxChars = maxchars;
    }
    
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException 
    {
      if (str == null) return;        
      int insertsize = str.length();
      int docsize = this.getLength();
      if ((insertsize+docsize) < m_iMaxChars) 
      {
        super.insertString(offs, str, a);
      }
      else
      {
        int maxinsert = m_iMaxChars - docsize;
        if (maxinsert > 0)
        {            
          super.insertString(offs, str.substring(0, maxinsert), a);
        }
      }        
    }
  }
    
}
