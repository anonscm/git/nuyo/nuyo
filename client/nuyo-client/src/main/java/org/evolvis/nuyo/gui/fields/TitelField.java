/*
 * Created on 29.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class TitelField extends GenericTextField
{
  public TitelField()
  {
    super("TITEL", AddressKeys.TITEL, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Titel_ToolTip", "GUI_MainFrameNewStyle_Standard_Titel", 50);
  }
}
