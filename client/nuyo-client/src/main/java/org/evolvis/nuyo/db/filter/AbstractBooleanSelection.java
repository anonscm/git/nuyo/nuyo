package org.evolvis.nuyo.db.filter;

import java.util.Map;

import org.evolvis.nuyo.db.ContactDBException;


/**
 * @author kleinw
 *
 *	Filterfeld f�r allgemeine Felder.
 *
 */
abstract public class AbstractBooleanSelection implements SelectionElement {
   
    /**	zu haltender Wert */
    protected Boolean _value;
    
    /** Konstruktor */
    public AbstractBooleanSelection(Boolean value) {_value = value;}  
    
    /** Hier wird die Map gef�llt */
    public void includeInMethod(Map map) throws ContactDBException {
        map.put(getFilterKey(), _value);
    }
    
    public Boolean getBoolean() {return _value;}
    
    
    public boolean equals(Object arg0) {
        if(arg0 instanceof AbstractBooleanSelection) {
            AbstractBooleanSelection selection = (AbstractBooleanSelection)arg0;
            return (selection.getBoolean().booleanValue() == _value.booleanValue())?true:false;
        } else
            return false;
    }
  
    
    public String toString() {return new StringBuffer().append(_value).toString();}
    
    
    /**	Hier wird der Mapkey f�r den Filter festgelegt */
    abstract public String getFilterKey() throws ContactDBException;

    /** IsManager Filter  */
    static public class IsManager extends AbstractBooleanSelection {
        public IsManager(Boolean value) {super(value);}
        public String getFilterKey() {return "ismanager";}
    }

    /** IsCreate Filter  */
    static public class IsCreate extends AbstractBooleanSelection {
        public IsCreate(Boolean value) {super(value);}
        public String getFilterKey() {return "create";}
    }

    /** IsPersonal Filter  */
    static public class IsPersonal extends AbstractBooleanSelection {
        public IsPersonal (Boolean value) {super(value);}
        public String getFilterKey() {return "personal";}
    }
    
}
