package org.evolvis.nuyo.db.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher.ResponseData;
import org.evolvis.nuyo.remote.Method;




/**
 * @author kleinw
 *
 *	Diese Basisklasse stellt gemeinsame Funktionalit�ten
 *	zur Verf�gung, die alle Objekte, die auf DB-Objekte
 *	abgebildet werden.	
 *
 *	Die Methoden aus IPersistable m�ssen von der abgeleiteten
 *	Klasse implementiert werden.
 *
 */
abstract public class AbstractEntity implements IEntity {

    /**	DB-PrimaryKey f�r das Objekt */
    protected Integer _id;
    /**	von der DB generiertes Erstellungsdatum */
    protected Date _created;
    /**	von der DB generiertes �nderungsdatum */
    protected Date _changed;
    /**	SoapMethod f�r die Kommunikation mit Octopus */
    protected Method _method;
    
    
    /** Objekt ist nur lokal vorhanden */
    protected boolean _isTransient = true;
    /** Objekt ist auch in der DB vorhanden und ge�ndert */
    protected boolean _isDirty = false;
    /** Objekt wurde in der DB gel�scht */
    protected boolean _isDelete = false;
    /** Objekt befindet sich in einem ung�ltigen Zustand */
    protected boolean _isInvalid = false;
    
    
    /**	Hier wird der Stand vom letzten Laden gehalten */
    protected ResponseData _responseData;
    
    
    protected Collection _relations = null;
    
    
    /**	Beh�lter f�r Attribute */
    protected MonitoredMap _attributes = new MonitoredMap(this);
    
    
    /**	DB-PrimaryKey f�r das Objekt. */
    public int getId() {return (_id == null)?0:_id.intValue();}

    
    /**	Attribut per Schl�ssel beziehen.@param key - Schl�ssel, �ber den der Wert geholt werden soll */
    public String getAttribute(String key) {return _attributes.get(key)!=null?_attributes.get(key).toString():null;}
	public String getAttributeAsString(String key) {return _attributes.get(key)!=null?_attributes.get(key).toString():null;}
	public Object getAttributeAsObject(String key){return _attributes.get(key);}
    /**	Attribut schreiben.@param key - Schl�ssel, unter dem gespeichert werden soll.@param value - der zu speichernde Wert */
	public void setAttribute(String key, String value) {_attributes.put(key, value);}
    public void setAttribute(String key, Object value) {_attributes.put(key, value);}
    
    
    /**	Erstellungsdatum lesen */
    public Date getCreated() {return _created;}
    /**	�nderung der Eigenschaft.@param created - Zeitpunkt der �nderung */
    public void setCreated(Date created) {_created = created;}

    
    /**	Objekt nur lokal vorhanden */
    public boolean isTransient() {return _isTransient;}
    /**	Eigenschaft setzen.@param isTransient - ist transient */
    public void setTransient(boolean isTransient) {_isTransient = isTransient;}

    
    /**	Objekt ist in der DB vorhanden und wurde lokal ge�ndert */
    public boolean isDirty() {return _isDirty;}
    /**	Eigenschaft setzen.@param isDirty - ist lokal ge�ndert */
    public void setDirty(boolean isDirty) {_isDirty = isDirty;}
    
    
    /**	Objekt wurde in der DB gel�scht */
    public boolean isDeleted() {return _isDelete;}
    /**	Eigenschaft setzen.@param isDeleted - ist gel�scht */
    public void setDeleted(boolean isDeleted) {_isDelete = isDeleted;}


    /**	Objekt ist ung�ltig */
    public boolean isInvalid() {return _isInvalid;}
    /**	Eigenschaft setzen.@param isInvalid - ist ung�ltig */
    public void setInvalid(boolean isInvalid) {_isInvalid = isInvalid;}


    /**	�nderungsdatum lesen */
    public Date getChanged() {return _changed;}
    /**	�nderungsdatum setzen.@param changed - �nderungsdatum */
    public void setChanged(Date changed) {_changed = changed;}
    
    
    /** Soapanfrage vorbereiten */
    public void prepareCommit(String method) 
    	throws ContactDBException {
        _method = new Method(method);
        _method.add("id", _id);
        if(isDirty())
            _method.add("attributes", _attributes);
    }
    
    
    protected void addRelation(AbstractPersistentRelation relation) {
        if(_relations == null)
            _relations = new ArrayList();
        _relations.add(relation);
    }
    
   
    /**	Alle 1-n Beziehungen holen */
    public Collection getRelations() {return _relations;}
    
    
    /**	Beispielsweise f�r Methoden der Collection Klasse. */
    public boolean equals(Object obj) {
        if(!(obj instanceof IEntity))
            return false;
        else {
            if (_id == null)
                return false;
            else return (_id.intValue() == ((IEntity)obj).getId());
        }
    }
    
    
    /**	Diese Map wird auf changed gesetzt, sobald ein Value tats�chlich ge�ndert wurde. */
    static public class MonitoredMap extends HashMap {

        /**	�nderungsflag */
        private IEntity _entity;
        
        
        /**	Konstruktion.@param parentEntity - Entity, die diese Map beinhaltet */
        protected MonitoredMap(IEntity parentEntity) {_entity = parentEntity;}
        
        
        /**	Map 'merkt' sich, ob sie ge�ndert wurde */
        public Object put(Object key, Object value) {
            if(containsKey(key)) {
                if(get(key) == null || !(get(key).equals(value))) {
                    _entity.setDirty(true);
                    return super.put(key, value);
                } else
                    return null;
            } else {
                _entity.setDirty(true);
                return super.put(key, value);
            }
        }
    }
    
}
