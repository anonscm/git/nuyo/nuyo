/**
 * 
 */
package org.evolvis.nuyo.db.octopus;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AddressWorkerConstants {
	public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_ADDRESS_ID = "addressid";
    public static final String PARAM_ADDRESSES_FILTER = "addressesFilter";
    public static final String PARAM_CATEGORIES_FILTER = "categoriesFilter";
    public static final String PARAM_DISJUNCTION_SUBCATEGORIES = "disjunctionSubcategories";
    public static final String PARAM_CONJUNCTION_SUBCATEGORIES = "conjunctionSubcategories";
    public static final String PARAM_NEGATED_SUBCATEGORIES = "negatedSubcategories";
    public static final String PARAM_ADDRESS_PKS = "addressPks";
    public static final String PARAM_ADD_SUB_CATEGORIES = "addSubCategories";
}
