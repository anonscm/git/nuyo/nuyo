/*
 * Created on 15.10.2004
 *
 */
package org.evolvis.nuyo.datacontainer.DataContainer;

/**
 * @author niko
 *
 * Diese Klasse stellt detailierte Versionsinformationen
 * �ber ein DataContainer-Objekt zur Verf�gung.
 * 
 */
public class DataContainerVersion
{
  private int m_iVersion;
  private int m_iSubVersion;
  private String m_sComment;
  
  public DataContainerVersion(int version)
  {
    m_iVersion = version;
    m_iSubVersion = 0;
    m_sComment = null;
  }

  public DataContainerVersion(int version, int subversion)
  {
    m_iVersion = version;
    m_iSubVersion = subversion;
    m_sComment = null;
  }
  
  public DataContainerVersion(int version, int subversion, String comment)
  {
    m_iVersion = version;
    m_iSubVersion = subversion;
    m_sComment = comment;
  }  
  
  /**
   * @return Returns the SubVersion.
   */
  public int getSubVersion()
  {
    return m_iSubVersion;
  }
  
  /**
   * @return Returns the Version.
   */
  public int getVersion()
  {
    return m_iVersion;
  }
  
  /**
   * @return Returns the Comment.
   */
  public String getComment()
  {
    return m_sComment;
  }
}
