/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class AkadTitelField extends GenericTextField
{
  public AkadTitelField()
  {
    super("AKADTITEL", AddressKeys.AKADTITEL, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_Standard_Akad_Titel_ToolTip", "GUI_MainFrameNewStyle_Standard_Akad_Titel", 50);
  }
}
