package org.evolvis.nuyo.gui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.help.CSH;
import javax.swing.SwingUtilities;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.xana.action.AbstractGUIAction;

public class HelpDirectAction extends AbstractGUIAction {

    private static final long serialVersionUID = -5166800447324115375L;
    private ActionListener helpListener;

    public void actionPerformed(final ActionEvent e) {
        SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                helpListener.actionPerformed(e);
            }
        });
    }
    
    public void init() {
        helpListener = new CSH.DisplayHelpAfterTracking(ApplicationServices.getInstance().getHelpBroker());
    }
    
}