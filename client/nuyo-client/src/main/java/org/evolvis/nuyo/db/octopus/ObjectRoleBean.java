/* $Id: ObjectRoleBean.java,v 1.2 2006/06/06 14:12:06 nils Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2005 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Philipp Kirchner
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db.octopus;

import org.evolvis.nuyo.db.ObjectRole;
import org.evolvis.nuyo.db.persistence.AbstractEntity;
import org.evolvis.nuyo.db.persistence.AbstractEntityFetcher;

/**
 *
 * Implementierung von ObjectRole
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public abstract class ObjectRoleBean extends AbstractEntity implements
		ObjectRole {
    /**	Objekt zum Holen von Rollen */
    static protected AbstractEntityFetcher _fetcher;
    
    public boolean isDeletable(){
    	
    	Object tmp = getAttributeAsObject(KEY_DELETABLE);
		if(tmp!=null){
			if(tmp instanceof Boolean){
				return ((Boolean) getAttributeAsObject(KEY_DELETABLE)).booleanValue();
			}
		}
		return true;
    }
    
	public boolean hasRightAdd() {
		return hasRight(KEY_ADD).booleanValue();
	}

	public boolean hasRightAddSub() {
		return hasRight(KEY_ADDSUB).booleanValue();
	}

	public boolean hasRightEdit() {
		return hasRight(KEY_EDIT).booleanValue();
	}

	public boolean hasRightGrant() {
		return hasRight(KEY_GRANT).booleanValue();
	}

	public boolean hasRightRead() {
		return hasRight(KEY_READ).booleanValue();
	}

	public boolean hasRightRemove() {
		return hasRight(KEY_REMOVE).booleanValue();
	}

	public boolean hasRightStructure() {
		return hasRight(KEY_STRUCTURE).booleanValue();
	}

	public boolean hasRightRemoveSub() {
		return hasRight(KEY_REMOVESUB).booleanValue();
	}

	public String getRoleName(){
		return getAttributeAsString(KEY_ROLENAME);
	}
	
	public void setRoleName(String name){
		setAttribute(KEY_ROLENAME, name);
	}

	public Boolean hasRight(String key){
		return getAttributeAsObject(key)!=null&&getAttributeAsObject(key) instanceof Boolean?(Boolean)getAttributeAsObject(key):Boolean.FALSE;
	}

	public void setRight(String key, Boolean newRight){
		setAttribute(key, newRight);
		
	}

}
