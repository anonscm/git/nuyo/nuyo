package org.evolvis.nuyo.db.octopus;

import java.util.Date;

import org.evolvis.nuyo.db.Appointment;
import org.evolvis.nuyo.db.AppointmentReminder;
import org.evolvis.nuyo.db.Calendar;
import org.evolvis.nuyo.db.ContactDBException;
import org.evolvis.nuyo.db.persistence.AbstractEntity;


/**
 * @author kleinw
 *
 *	Implementierung de.tarent.contact.db.AppointmentReminder
 *
 */
abstract public class ReminderBean extends AbstractEntity implements AppointmentReminder {

    //
    //	Instanzmerkmale
    //
    /**	Id der dazuge�rigen Kalenderraltion */
    protected Integer _relationId;
    /**	Kanal des Reminders */
    protected Integer _channel;
    /** Timeunit */
    protected Integer _timeUnit;
    /**	Timeoffset */
    protected Integer _timeOffset;
    /**	Runtime */
    protected Date _runTime;
    /**	flag, ob Reminder von der Kalenderrelation gel�scht wurde */
    protected boolean _isRemoved;

    /** Das dazugeh�rige Appointment */
    protected Appointment _appointment;
    /** Der dazuge�rige Kalender */
    protected Calendar _calendar;
    
    /**	Das dazugeh�rige Appointment */
    public Appointment getAppointment() throws ContactDBException {return _appointment;}

    
    /**	Kanal beziehen */
    public int getChannel() throws ContactDBException {return (_channel==null)?0:_channel.intValue();}
    /**	Kanal setzen. @param newChannel - der zu setzende Kanal */
    public void setChannel(int newChannel) throws ContactDBException {_channel = new Integer(newChannel);}

    
    /**	Runtime des Reminders */
    public Date getRuntime() throws ContactDBException {return _runTime;}
    /**	Runtime setzen.@param newRuntime - zu setzende Runtime */
    public void setRuntime(Date newRuntime) throws ContactDBException {_runTime = newRuntime;}

    
    /**	Timeoffset holen */
    public int getTimeOffset() throws ContactDBException {return (_timeOffset == null)?0:_timeOffset.intValue();}
    /**	Timeoffset setzen */
    public void setTimeOffset(int timeOffset) throws ContactDBException {_timeOffset = new Integer(timeOffset);}
    
    
    /**	Timeunit holen */
    public int getTimeUnit() throws ContactDBException {return (_timeUnit == null)?0:_timeUnit.intValue();}
    /**	Timeunit setzen.@param timeUnit - zu setzender Unit */
    public void setTimeUnit(int timeUnit) throws ContactDBException {_timeUnit = new Integer(timeUnit);}
    
}
