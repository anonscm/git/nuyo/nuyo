package org.evolvis.nuyo.plugin;

import org.evolvis.nuyo.db.Addresses;

/**
 * This interface marks a special kind of plugin, i.e. a plugin that performs on addresslists.
 * The implementing class can be extracted from a plugin by calling the getImplementationFor() method.  
 *
 */
public interface AddressListPerformer {
	
	public void setAddresses(Addresses addresses);
	public Addresses getAddresses();
	public void execute();

}
