package org.evolvis.nuyo.gui.categorytree;

import org.evolvis.nuyo.gui.Messages;

/**
 * Instances of the <code>Visibility<code> class are used
 * to define which elements of a category tree are visible
 * depending on their selection state.
 * 
 * <p>Additionally they provide localized a label and tooltip
 * text for the UI component resembling a certain visibility
 * state.</p>
 *  
 * @author Robert Schuster
 *
 */
class Visibility
{

  static final Visibility ALL = new Visibility("all", "GUI_CategoryTree_Visibility_All");
  
  static final Visibility SELECTED = new Visibility("selected", "GUI_CategoryTree_Visibility_Selected");

  static final Visibility UNSELECTED = new Visibility("unselected", "GUI_CategoryTree_Visibility_Unselected");
  
  private String name;
  
  private String labelKey;
  
  private Visibility(String name, String labelKey)
  {
    this.name = name;
    this.labelKey = labelKey;
  }
  
  public String toString()
  {
    return name;
  }
  
  String getLabel()
  {
    return Messages.getString(labelKey);
  }
  
  String getLabelTooltip()
  {
    return Messages.getString(labelKey + "_ToolTip");
  }
  
  char getLabelMnemonic()
  {
	return Messages.getString(labelKey + "_Mnemonic").charAt(0);
  }
  
  static Visibility get(String visname)
  {
    if (ALL.name.equals(visname))
      return ALL;
    else if (SELECTED.name.equals(visname))
      return SELECTED;
    else if (UNSELECTED.name.equals(visname))
      return UNSELECTED;
    else
      return null;
  }
}
