/* $Id: ExporterException.java,v 1.4 2006/07/28 12:13:04 aleksej Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Jens Neumaier. 
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.plugins.dataio;

/**
 * This Exception will be thrown by classes implementing
 * the <code>AddressExporter</code> interface.
 * 
 * @author Jens Neumaier, tarent GmbH
 * @see org.evolvis.nuyo.plugin.AddressExporter
 *
 */
public class ExporterException extends Exception {

    private static final long serialVersionUID = 5363861742743882466L;

    public static final String NO_RESULT_STRING = "Result string from octopus call is empty or not present.";
    
    public ExporterException() {
        super();
    }

    public ExporterException(String message) {
        super(message);
    }

    public ExporterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExporterException(Throwable cause) {
        super(cause);
    }

}
