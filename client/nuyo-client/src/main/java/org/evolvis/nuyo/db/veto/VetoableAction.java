/* $Id: VetoableAction.java,v 1.2 2006/06/06 14:12:12 nils Exp $
 * 
 * Created on 29.10.2003
 */
package org.evolvis.nuyo.db.veto;

import org.evolvis.nuyo.db.Address;


/**
 * Diese Klasse stellt eine Aktion dar, die durch ein
 * {@link org.evolvis.nuyo.db.veto.Veto} untersagt werden kann.
 *  
 * @author mikel
 */
public class VetoableAction {
    /*
     * �ffentliche Factory-Methoden
     */
    public static VetoableAction classifyAddress(Address address, String userId) {
        return new AddressAction(ACTION_CLASSIFY, userId, address);
    }
    public static VetoableAction createAddress(Address address, String userId) {
        return new AddressAction(ACTION_CREATE, userId, address);
    }
    public static VetoableAction deleteAddress(Address address, String userId) {
        return new AddressAction(ACTION_DELETE, userId, address);
    }
    public static VetoableAction editAddress(Address address, String userId) {
        return new AddressAction(ACTION_EDIT, userId, address);
    }

    public static VetoableAction createAddress(Address address, String userId, String category) {
        return new AddressAction(ACTION_CREATE, userId, address, category);
    }
    public static VetoableAction deleteAddress(Address address, String userId, String category) {
        return new AddressAction(ACTION_DELETE, userId, address, category);
    }
    public static VetoableAction editAddress(Address address, String userId, String category) {
        return new AddressAction(ACTION_EDIT, userId, address, category);
    }

	/*
     * �ffentliche Konstanten 
     */
    public final static Object ACTION_CLASSIFY = "classify";    
    public final static Object ACTION_CREATE = "create";
    public final static Object ACTION_DELETE = "delete";
    public final static Object ACTION_EDIT = "edit";    

    /*
     * Getter und Setter
     */
    /**
     * @return Aktionstoken
     */
    public Object getAction() {
        return action;
    }

    /**
     * @return Benutzer-Id
     */
    public String getUserId() {
        return userId;
    }

    /*
     * gesch�tzter Konstruktor 
     */
    protected VetoableAction(Object action, String userId) {
        super();
        this.action = action;
        this.userId = userId;
    }

    /*
     * private Daten
     */
    private String userId;
    private Object action;
}
