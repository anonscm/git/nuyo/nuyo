package org.evolvis.nuyo.gui.calendar;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.Date;

import javax.swing.JPanel;

/*
 * Created on 17.07.2003
 */

/**
 * @author niko
 *
 */
public class BigDayNamesPanel extends JPanel 
{
  private ScheduleData m_oScheduleData;
  private int m_iNumDays;
  private ScheduleDate m_oFirstDay;

  private boolean m_bShowShort = true;
  private boolean m_bShowDayName = false;
  private boolean m_bShowDate = false;
  private boolean m_bShowName = false;
  private boolean m_bShowDescription = false;        
  
  private int m_iLineHeight;
  
  private Font m_oFontStd;
  private Font m_oFontToday;
  
  
  public BigDayNamesPanel(ScheduleData sd, ScheduleDate firstday, int numdays)
  {
    m_oScheduleData = sd;
    m_oFirstDay = firstday;
    m_iNumDays = numdays;
    
    // IST DAS GUT SO?
    try
    {
      String teststring = "ABCDEFGHIJKLNMOPQRSTUVWXYZ01234567689";
      Rectangle2D rect = this.getGraphics().getFontMetrics().getStringBounds(teststring, this.getGraphics());
      m_iLineHeight = (int)(rect.getHeight());
    }
    catch(Exception e)
    {
      m_iLineHeight = 15;
    }
  }

  public void setFont(Font font)
  {
    m_oFontStd = font;
    m_oFontToday = font.deriveFont(Font.BOLD);
    super.setFont(font);
  }
  
  public void setFirstDay(ScheduleDate date)
  {
    m_oFirstDay = new ScheduleDate(new Date(date.getDate().getTime()));
    this.repaint();
  }
  
  public void setNumDays(int numdays)
  {
    m_iNumDays = numdays;
    this.repaint();
  }
  
  
  public ScheduleDate getFirstDay()
  {
    return new ScheduleDate(new Date(m_oFirstDay.getDate().getTime()));
  }
  
  public boolean isOpaque()
  {
    return(true); 
  }
  
  protected void paintComponent(Graphics g) 
  {        
    Rectangle clip = g.getClipBounds();
    Insets insets = getInsets();
    Dimension  size = getSize();

    g.setColor(m_oScheduleData.backgroundPanelColor);
    g.fillRect(0, 0, size.width, size.height);    
    
    // die Tage f�llen
    g.setColor(m_oScheduleData.freeDayColor);
    g.fillRect(0, 0, (m_oScheduleData.dayWidth * m_iNumDays), m_oScheduleData.firstHourY);
    
    // das Tages-Grid zeichnen
    for(int d = 0; d<(m_iNumDays); d++)
    {
      ScheduleDate date = m_oFirstDay.getDateByOffset(d);
      DayDescription dd = DateDescriptionFactory.instance().getDescriptionForDay(date);
      
      if (d < m_iNumDays)
      {  
        if (dd instanceof WorkDayDescription)
        {        
          g.setColor(m_oScheduleData.workDayColor);
        }
        else if (dd instanceof HolidayDescription)
        {        
          g.setColor(m_oScheduleData.holidayColor);
        }
        else if (dd instanceof WeekendDayDescription)
        {        
          g.setColor(m_oScheduleData.freeDayColor);
        }
        int x = (d * m_oScheduleData.dayWidth);
        int y = 0;
        int w = m_oScheduleData.dayWidth;
        int h = m_oScheduleData.firstHourY;
        g.fillRect(x, y, w, h);        
      }
      
      g.setColor(m_oScheduleData.gridColor);
      g.drawLine((d * m_oScheduleData.dayWidth), 0, (d * m_oScheduleData.dayWidth), m_oScheduleData.firstHourY);
      if (d < m_iNumDays) //TODO
      { 
        g.setColor(m_oScheduleData.dayFontColor);         
        int posy = (m_oScheduleData.hourHeight / 2) + 2;
        
        if (date.isSameDay(new ScheduleDate()))
        {
	      	//g.setColor(Color.green);
          g.setFont(m_oFontToday);
	      }
        else
        {
          //g.setColor(m_oScheduleData.m_oColorDayText);
          g.setFont(m_oFontStd);
          //g.setColor(Color.BLACK);
        }
    
        
        if (m_bShowShort)
        {
          String linetext = dd.getShortDayName() + " " + date.getShortDateString();
          Rectangle2D rect_0 = g.getFontMetrics().getStringBounds(linetext, g);
          int height_0 = (int)(rect_0.getHeight());
          int posx_0 = (int)(((m_oScheduleData.dayWidth) - rect_0.getWidth()) / 2.0);
          g.drawString(linetext, ((d * m_oScheduleData.dayWidth)) + posx_0, posy);        
        }
        else
        {
          if (m_bShowDayName)
          {
            Rectangle2D rect_1 = g.getFontMetrics().getStringBounds(dd.getDayName(), g);
            int height_1 = (int)(rect_1.getHeight());
            int posx_1 = (int)(((m_oScheduleData.dayWidth) - rect_1.getWidth()) / 2.0);
            g.drawString(dd.getDayName(), ((d * m_oScheduleData.dayWidth)) + posx_1, posy);
            posy += height_1;
          }
          
          if (m_bShowDate)
          {          
            Rectangle2D rect_2 = g.getFontMetrics().getStringBounds(date.getDateString(), g);
            int height_2 = (int)(rect_2.getHeight());
            int posx_2 = (int)(((m_oScheduleData.dayWidth) - rect_2.getWidth()) / 2.0);

            g.drawString(date.getDateString(), ((d * m_oScheduleData.dayWidth)) + posx_2, posy);
            posy += height_2;
          }
          
          if (m_bShowName)
          {
            Rectangle2D rect_3 = g.getFontMetrics().getStringBounds(dd.getName(), g);
            int height_3 = (int)(rect_3.getHeight());
            int posx_3 = (int)(((m_oScheduleData.dayWidth) - rect_3.getWidth()) / 2.0);

            g.drawString(dd.getName(), ((d * m_oScheduleData.dayWidth)) + posx_3, posy);
            posy += height_3;     
          }
          
          if (m_bShowDescription)
          {
            Rectangle2D rect_4 = g.getFontMetrics().getStringBounds(dd.getDescription(), g);
            int height_4 = (int)(rect_4.getHeight());
            int posx_4 = (int)(((m_oScheduleData.dayWidth) - rect_4.getWidth()) / 2.0);

            g.drawString(dd.getDescription(), ((d * m_oScheduleData.dayWidth)) + posx_4, posy);
            posy += height_4;
          }        
        }               
      }       
    }
  }
  
  public int getDayNumOfXPos(int x)
  {
    if (x> (m_oScheduleData.dayWidth * m_iNumDays)) return -1;
    return x / (m_oScheduleData.dayWidth);
  }
  
  /**
   * @return Returns the m_bShowDate.
   */
  public boolean isShowDate() 
  {
    return m_bShowDate;
  }
  /**
   * @param showDate The m_bShowDate to set.
   */
  public void setShowDate(boolean showDate) 
  {
    m_bShowDate = showDate;
  }
  /**
   * @return Returns the m_bShowDayName.
   */
  public boolean isShowDayName() 
  {
    return m_bShowDayName;
  }
  /**
   * @param showDayName The m_bShowDayName to set.
   */
  public void setShowDayName(boolean showDayName) 
  {
    m_bShowDayName = showDayName;
  }
  /**
   * @return Returns the m_bShowDescription.
   */
  public boolean isShowDescription() 
  {
    return m_bShowDescription;
  }
  /**
   * @param showDescription The m_bShowDescription to set.
   */
  public void setShowDescription(boolean showDescription) 
  {
    m_bShowDescription = showDescription;
  }
  /**
   * @return Returns the m_bShowName.
   */
  public boolean isShowName() 
  {
    return m_bShowName;
  }
  /**
   * @param showName The m_bShowName to set.
   */
  public void setShowName(boolean showName) 
  {
    m_bShowName = showName;
  }
  /**
   * @return Returns the m_bShowShort.
   */
  public boolean isShowShort() 
  {
    return m_bShowShort;
  }
  /**
   * @param showShort The m_bShowShort to set.
   */
  public void setShowShort(boolean showShort) 
  {
    m_bShowShort = showShort;
  }
  
  public int getUsedHeight()
  {
    if (isShowShort()) return m_iLineHeight;     
    int height = 0;
    if (isShowName()) height += m_iLineHeight;
    if (isShowDate()) height += m_iLineHeight;
    if (isShowDescription()) height += m_iLineHeight;
    if (isShowDayName()) height += m_iLineHeight;   
    
    // Minimum garantieren damit der Benutzer eine Chanche 
    // hat die Texte wieder einzuschalten falls er mal alle 
    // deaktiviert hat...
    if (height < m_iLineHeight) height = m_iLineHeight;
    return height;
  }
  
  
}

