package org.evolvis.nuyo.dupview;

import java.util.Iterator;
import java.util.Set;

import de.tarent.commons.datahandling.binding.DataChangedEvent;
import de.tarent.commons.datahandling.binding.MapModel;
import de.tarent.commons.datahandling.entity.EntityException;

/**
 * This Class is used to enable or deactivate columns in JHTMLFormViews.
 * 
 * @author Thomas Schmitz, tarent GmbH
 *
 */
public class ColumnManager extends MapModel {
	
	SelectionManager manager;
	
	/**
	 * Creates a new instance and deactivates all columns in the
	 * given SelectionManager.
	 * 
	 * @param manager a SelectionManager that contains keys for
	 * 				  the columns.
	 */
	public ColumnManager(SelectionManager manager) {
		this.manager = manager;
		for(int i = 0; i <= manager.getSize(); i++) {
			try {
				setAttribute(Integer.toString(i), new Boolean(false));
			} catch (EntityException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setAttribute(String key, Object value) throws EntityException {
		getStorageMap().put(key, value);
		if (value.equals(Boolean.FALSE)) {
			Set set = manager.selections.keySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()){
				Object next = iter.next();
				String[] keyParts = ((String)next).split("\\.");
				String row = keyParts[0];
				String column = keyParts[1];
				if(column.equals(key)){
					manager.setAttribute(row+".0", new Boolean(true));
				}
			}
		}
		fireDataChanged(new DataChangedEvent(this, key));
	}
}
