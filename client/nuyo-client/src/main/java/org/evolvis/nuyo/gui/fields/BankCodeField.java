/*
 * Created on 29.03.2004
 *
 */
package org.evolvis.nuyo.gui.fields;

import org.evolvis.nuyo.gui.fieldhelper.AddressKeys;
import org.evolvis.nuyo.gui.fieldhelper.GenericTextField;


/**
 * @author niko
 *
 */
public class BankCodeField extends GenericTextField
{
  public BankCodeField()
  {
    super("BANKCODE", AddressKeys.BANKCODE, CONTEXT_ADRESS, "GUI_MainFrameNewStyle_ErweitertTop_BankCode_ToolTip", "GUI_MainFrameNewStyle_ErweitertTop_BankCode", 60);
  }
}
