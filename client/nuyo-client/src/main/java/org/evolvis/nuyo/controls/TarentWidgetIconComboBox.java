/*tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
Copyright (C) 2002 tarent GmbH

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

tarent GmbH., hereby disclaims all copyright
interest in the program 'tarent-contact'
(which makes passes at compilers) written
by Nikolai R�ther.
signature of Elmar Geese, 1 June 2002
Elmar Geese, CEO tarent GmbH*/ 
package org.evolvis.nuyo.controls;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

/**
 * @author niko
 */
public class TarentWidgetIconComboBox extends JComboBox implements TarentWidgetInterface
{
  static private Color m_oSelectedBG = Color.black;
  static private Color m_oSelectedFG = Color.white;
  static private Color m_oUnselectedBG = Color.white;
  static private Color m_oUnselectedFG = Color.black;
  
  public TarentWidgetIconComboBox()
  {
    super();
    this.setRenderer(new IconCellRenderer());
  }
    
  
  public TarentWidgetIconComboBox(IconComboBoxEntry[] entries)
  {
    this();
    this.setRenderer(new IconCellRenderer());

    for(int i=0; i<(((IconComboBoxEntry[])entries).length); i++)
    {
      this.addItem((((IconComboBoxEntry[])entries)[i]));
    }
  }
  
  public void addIconComboBoxEntry(IconComboBoxEntry entry)
  {
    this.addItem(entry);
  }

  public void setSelectedColors(Color foreground, Color background)
  {
    m_oSelectedBG = background;
    m_oSelectedFG = foreground;
  }

  public void setUnselectedColors(Color foreground, Color background)
  {
    m_oUnselectedBG = background;
    m_oUnselectedFG = foreground;
  }
  

	/**
	 * @see TarentWidgetInterface#setData(Object)
	 */
	public void setData(Object data) 
  {
    if (data == null)
    {
      this.removeAllItems();
    }
    else if (data instanceof IconComboBoxEntry[])
    {
      for(int i=0; i<(((IconComboBoxEntry[])data).length); i++)
      {
        this.addItem((((IconComboBoxEntry[])data)[i]));
      }
    }
    else if (data instanceof Object[])
    {
      for(int i=0; i<(((Object[])data).length); i++)
      {
        this.addItem((((Object[])data)[i]));
      }
    }
	}

	/**
	 * @see TarentWidgetInterface#getData()
	 */
	public Object getData() 
  {
    String text = "";
    for (int i=0; i<(this.getItemCount()); i++)    
    {
      text += this.getItemAt(i) + ";";
    }
    return(text);
	}


	public IconComboBoxEntry getItemByKey(Object key) 
	{
	  for (int i=0; i<(this.getItemCount()); i++)    
	  {
	    IconComboBoxEntry entry = (IconComboBoxEntry)(this.getItemAt(i));
	    if (entry.getKey().equals(key)) return entry;
	  }
	  return(null);
	}

	
	public IconComboBoxEntry getItemByIndex(int index) 
	{
	  IconComboBoxEntry entry = (IconComboBoxEntry)(this.getItemAt(index));
	  return(entry);
	}
	
	public int getNumberOfItems() 
	{
	  return this.getItemCount();
	}
	
	
  public boolean setSelectedItem(String text)
  {
    for (int i = 0; i < (this.getItemCount()); i++)
    {
      IconComboBoxEntry entry = (IconComboBoxEntry)(this.getItemAt(i));
      if (entry.getText().equals(text))
      {
        this.setSelectedIndex(i);
        return true;
      }
    }
    return false;
  }
  
  public boolean setSelectedItem(ImageIcon icon)
  {
    for (int i = 0; i < (this.getItemCount()); i++)
    {
      IconComboBoxEntry entry = (IconComboBoxEntry)(this.getItemAt(i));
      if (entry.getIcon().equals(icon))
      {
        this.setSelectedIndex(i);
        return true;
      }
    }
    return false;
  }

  public boolean setSelectedItemByKey(Object key)
  {
    IconComboBoxEntry entry = getItemByKey(key);
    if (entry != null)
    {
      this.setSelectedItem(entry);
      return true;
    }
    return false;
  }

  

  public JComponent getComponent()
  {
    return(this);
  }

  public JComboBox getComboBox()
  {
    return(this);
  }
  public void setLengthRestriction(int maxlen)
  {
  }

  public void setWidgetEditable(boolean iseditable)
  {
    this.setEnabled(iseditable);
  }

  public boolean isEqual(Object data)
  {
    if (data.equals(((IconComboBoxEntry)(this.getSelectedItem())).getText())) return(true);
    else                                                                                                                   return(false);
  }

  
  static public class IconComboBoxEntry
  {
    private String m_sText;
    private ImageIcon m_oIcon;
    private Object m_oKey;
    private JPanel m_oPanel;
    private JLabel m_oIconLabel;
    private JLabel m_oTextLabel;
    private Font m_oFont;
    
    public IconComboBoxEntry(ImageIcon icon, String text)
    {
      m_sText = text;
      m_oIcon = icon;      
      m_oKey = null;
      m_oFont = new Font(null, Font.PLAIN, 12);
      m_oPanel = createPanel();
    }
    
    public IconComboBoxEntry(ImageIcon icon, String text, Object key)
    {
      m_sText = text;
      m_oIcon = icon;      
      m_oKey = key;
      m_oFont = new Font(null, Font.PLAIN, 12);
      m_oPanel = createPanel();
    }
    
    public void setFont(Font font)
    {
      m_oFont = font;
      m_oTextLabel.setFont(m_oFont);      
    }

    public Object getKey()
    {
      return m_oKey;
    }

    public void setKey(Object key)
    {
      m_oKey = key;
    }
        
    public String getText()
    {
      return(m_sText);
    }
    
    public ImageIcon getIcon()
    {
      return(m_oIcon);
    }    

    public void setText(String text)
    {
      m_sText = text;
      m_oPanel = createPanel();
    }
    
    public void setIcon(ImageIcon icon)
    {
      m_oIcon = icon;
      m_oPanel = createPanel();
    }    

    public JPanel getPanel()
    {
      return(m_oPanel);
    }

    public void setSelected(boolean isSelected)
    {
      Color bgcol = isSelected ? m_oSelectedBG : m_oUnselectedBG;
      Color fgcol = isSelected ? m_oSelectedFG : m_oUnselectedFG;

      m_oPanel.setBackground(bgcol);
      m_oPanel.setForeground(fgcol);

      m_oIconLabel.setBackground(bgcol);
      m_oIconLabel.setForeground(fgcol);

      m_oTextLabel.setBackground(bgcol);
      m_oTextLabel.setForeground(fgcol);
    }

    private JPanel createPanel()
    {
      JPanel panel = new JPanel();
      panel.setLayout(new BorderLayout());
      panel.setOpaque(true);

      m_oIconLabel = new JLabel(getIcon());
      m_oIconLabel.setBorder(new EmptyBorder(1,1,1,5));
      m_oTextLabel = new JLabel(getText());
      m_oTextLabel.setFont(m_oFont);
                 
      panel.add(m_oIconLabel, BorderLayout.WEST);
      panel.add(m_oTextLabel, BorderLayout.CENTER);
           
      Dimension size;
      if (getIcon() != null)
      {  
        size = new Dimension(100, getIcon().getIconHeight() + 2);
      }
      else
      {  
        size = new Dimension(100, 32);
      }
      panel.setMinimumSize(size);
      panel.setMaximumSize(size);
      panel.setPreferredSize(size);
      
      //panel.repaint();
      
      return(panel);
    }

  }
  
  
  private class IconCellRenderer implements ListCellRenderer 
  {
       public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
       {
		     if (value == null) return new JLabel();
         if (value instanceof IconComboBoxEntry)
         {
         	JPanel panel = ((IconComboBoxEntry)value).getPanel();
           ((IconComboBoxEntry)value).setSelected(isSelected);
           
           return panel;
         }
         else return(new JLabel("!please contact the developer of this application!"));
       }
   }


  private List m_oWidgetChangeListeners = new ArrayList();
  public void addWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.add(listener);
  }
  
  public void removeWidgetChangeListener(TarentWidgetChangeListener listener)
  {
    m_oWidgetChangeListeners.remove(listener);    
  }
  

}
