/**
 * 
 */
package org.evolvis.nuyo.plugins.mail;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class MutableListModel extends AbstractListModel
{
	List data;

	public MutableListModel()
	{
		this(new ArrayList());
	}

	public MutableListModel(List pData)
	{
		data = pData;
	}

	public void setData(List pData)
	{
		data = pData;
		fireContentsChanged(this, 0, getSize());
	}

	public List getData()
	{
		return data;
	}

	public void add(Object pObject)
	{
		data.add(pObject);
		// TODO proof this
		fireContentsChanged(this, getSize()-2, getSize()-1);
	}

	public void remove(Object pObject)
	{
		data.remove(pObject);
	}
	
	public void removeElementAt(int pIndex)
	{
		if(pIndex >= 0 && pIndex < getData().size())
		{
			data.remove(pIndex);
			fireIntervalRemoved(this, pIndex, pIndex);
		}
	}

	public Object getElementAt(int index)
	{
		return data.get(index);
	}

	public int getSize()
	{
		return data.size();
	}
}
