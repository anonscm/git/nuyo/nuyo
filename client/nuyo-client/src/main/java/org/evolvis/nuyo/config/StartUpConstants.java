package org.evolvis.nuyo.config;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public interface StartUpConstants {

    // Resources
    public String CALENDAR_RESOURCES_PATH = "/org/evolvis/nuyo/gui/calendar/resources/";
    public String HELP_SET_PATH = "/org/evolvis/nuyo/resources/help/tarent-contact.hs";
    public String EMAIL_RESOURCES_PATH = "/org/evolvis/nuyo/gui/email/resources/";
    public String CONTROLS_RESOURCES_PATH = "/org/evolvis/nuyo/controls/resources/";
    public String CONTACT_RESOURCES_PATH = "/org/evolvis/nuyo/resources/";
    public String DEFAULT_PLUGIN_FOLDER_PATH = "/org/evolvis/nuyo/datacontainer/";
    public String CONFIG_RESOURCES_PATH = "/org/evolvis/nuyo/resources/";
    
    // Configuration files
    public String APPEARANCE_CONFIG = "appearance.xml";
    
    public String ENVIRONMENT_CONFIG = "environment.xml";
    
    public String CONFIGURATION_FILE_NAME = "config.xml";
    public String ACTIONS_CONFIGURATION_FILE_NAME = "actions.men";
    public String CONFIG_DIR_NAME1 = "config";
    public String CONFIG_DIR_NAME2 = "client" + File.separator + "configs";
    public String PERSONAL_CONFIG_FILE_NAME = System.getProperty( "user.home" ) + File.separator + ".tarent"
        + File.separator + "nuyo" + File.separator + "config.xml";
    
    // Locale Bundles
    public String MESSAGES_BUNDLE_NAME = "org.evolvis.nuyo.resources.messages";
    public String MENU_BUNDLE_NAME = "org.evolvis.nuyo.gui.menu.resources.mnemonics";
    
    // Configuration constants:
    public int ACTIONS_INIT_DELAY_SECONDS = 3;
    // IDs:
    public String MAINFRAME_MENU_BAR_ID = "mainFrame.menu";
    public String MAINFRAME_TOOL_BAR_ID = "mainFrame.toolBar";
    public String MAINFRAME_SIDEMENU_BAR_ID = "mainFrame.sideMenu";
    public String MAILDIALOG_MENU_BAR_ID = "mailDialog.menu";
    public String MAILDIALOG_TOOL_BAR_ID = "mailDialog.toolBar";

    // User Settings
    //-- nodes:
    /** Preferences node name of personal <i>nuyo</i> settings.*/
    public String PREF_BASE_NODE_NAME = "/org/evolvis/nuyo";
    /** Preferences node name of the application user's login name. */ 
    public String PREF_USER_NODE_NAME = "user";
    /** Preferences node name for the last connection */
    public String PREF_LAST_CONNECTION_NODE_NAME = "connection.last";
    /** Preferences node name of panels' settings.*/
    public String PREF_PANELS_NODE_NAME = "panels";
    /** Preferences node name of <i>admin panel</i> settings.*/
    public String PREF_ADMIN_PANEL_NODE_NAME = "adminPanel";
    /** Preferences node name of <i>context menu</i> settings. In the past the context menu was called collapsing menu.*/
    public String PREF_MENUS_NODE_NAME = "menus";
    /** Preferences node name of <i>table</i> settings.*/
    public String PREF_TABLE_NODE_NAME = "table";
    /** Preferences node name of <i>calender selection</i> settings.*/
    public String PREF_CALENDAR_SELECTION = "calendar/selection";
    /** Preferences node name of <i>VCard</i> settings.*/
    public String PREF_VCARD_SUB_NODE = "vcard";
    //-- keys:
    /** Preferences key of <i>VCard</i> directory.*/
    public String PREF_VCARD_DIR_KEY = "current.directory";
    /** Preferences key of <i>export</i> directory.*/
    public String PREF_EXPORT_DIR_KEY = "last.export.directory";
    /** Preferences key of table <i>columns widths</i>.*/
    public String PREF_TABLE_COLUMNS_WIDTHS_KEY = "widths";
    /** Preferences key of table <i>columns positions</i>.*/
    public String PREF_TABLE_COLUMNS_POSITIONS_KEY = "positions";
    /** Preferences key of <i>drag bar position</i>.*/
    public String PREF_DRAG_BAR_POSITION_KEY = "drag.bar.position";
    /** Preferences key of <i>side menu visibility</i>.*/
    public String PREF_SIDE_MENU_IS_VISIBLE_KEY = "sideMenu.isVisible";
    /** Preferences key of <i>table visibility</i>.*/
    public String PREF_TABLE_IS_VISIBLE_KEY = "table.isVisible";
    /** Preferences key of <i>tool bar visibility</i>.*/
    public String PREF_TOOL_BAR_IS_VISIBLE_KEY = "toolBar.isVisible";
    /** Preferences key of <i>address panel divider</i>.*/
    public String PREF_ADRESS_PANEL_DIVIDER_KEY = "AdressPanel_Divider";
    /** Preferences key of <i>e-mail panel horizontal divider</i>.*/
    public String PREF_EMAIL_PANEL_HORIZONTAL_DIVIDER_KEY = "EMailPanel_HorizontalDivider";
    /** Preferences key of <i>e-mail panel vertical divider</i>.*/
    public String PREF_EMAIL_PANEL_VERTICAL_DIVIDER_KEY = "EMailPanel_VerticalDivider";
    /** Preferences key of <i>extended top panel divider</i>.*/
    public String PREF_EXTENDED_TOP_PANEL_DIVIDER_KEY = "ErweitertTopPanel_Divider";
    /** Preferences key of last used <i>user's emails</i> to report any error or bug.*/
    public String PREF_BUG_REPORT_LAST_USED_EMAILS_KEY = "user.emails.lastUsedForBugReport";
    /** Preferences key for configured Look And Feel */
    public String PREF_LOOK_AND_FEEL = "lookAndFeel";
    
    /** Stores whether the user opted not to complain about the application complaining
     * about running with an unsupported Java version and implementation.
     */
    public String PREF_COMPLAIN_UNSUPPORTED_JAVA_VERSION = "complainUnsupportedJavaVersion";
    
    /**Context modi*/
    public String EDIT_MODE = "edit";
    public String NEW_MODE = "new";
    public String SEARCH_MODE = "search";
    public String VIEW_MODE = "view";

    // Look And Feel settings...
    public Color TITLE_COLOR = Color.WHITE;
    // Default fonts
    public Font PLAIN_12_FONT = new Font(null, Font.PLAIN, 12);
    public Font PLAIN_10_FONT = new Font(null, Font.PLAIN, 10);
    public Font BOLD_10_FONT = new Font(null, Font.BOLD, 10);
    
    
    // other settings
    public DateFormat DATE_FORMAT = SimpleDateFormat.getDateInstance();
    public DateFormat TIME_FORMAT = SimpleDateFormat.getTimeInstance();
    public DateFormat TIME_DURATION_FORMAT = new SimpleDateFormat("HH:mm", Locale.UK);
    
    // RSCHUS_TODO: Make locale setable in configuration
    public Locale LOCALE = Locale.getDefault();
    
    public FileFilter configFileFilter = new FileFilter() {
        public boolean accept( File pathname ) {
            return ( pathname.isFile() && pathname.canRead() && pathname.toString().endsWith( ".xml" ) );
        }
    };
}
