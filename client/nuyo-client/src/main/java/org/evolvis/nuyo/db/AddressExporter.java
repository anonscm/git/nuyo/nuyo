/* $Id: AddressExporter.java,v 1.11 2007/08/30 16:10:29 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Logger;

import org.evolvis.nuyo.controller.ApplicationServices;
import org.evolvis.nuyo.db.octopus.AddressesImpl;
import org.evolvis.nuyo.db.octopus.OctopusDatabase;
import org.evolvis.nuyo.gui.Messages;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.spreadsheet.export.SpreadSheet;
import org.evolvis.spreadsheet.export.SpreadSheetFactory;

import de.tarent.commons.datahandling.PrimaryKeyList;
import de.tarent.commons.utils.Pojo;
import de.tarent.commons.utils.TaskManager;

/**
 * Enth�lt Routinen zum Export von Adressen in unterschiedliche Formate.
 * 
 * @author Sebastian
 */
public class AddressExporter implements TaskManager.Task {    

    public static final String FORMAT_ODS_DOCUMENT = "ODS";
    public static final String FORMAT_SXC_DOCUMENT = "SXC";
    public static final String FORMAT_CSV_DOCUMENT = "CSV";

    static final String[][] extensions = new String[][] {{"csv"}, {"sxc"}, {"ods"}};
    static final String[] descriptions = new String[] { "CSV Tabelle [.csv]", "OpenOffice 1.1.x [.sxc]", "OpenOffice 2.0 [.ods]"};
    static final String[] fileFormats = new String[] { AddressExporter.FORMAT_CSV_DOCUMENT, AddressExporter.FORMAT_SXC_DOCUMENT, AddressExporter.FORMAT_ODS_DOCUMENT};
            
	private static Logger logger = Logger.getLogger(AddressExporter.class.getName());

    AddressListProvider addressesProvider;
    File file;
    String documentFormat;
    boolean canceled = false;
    int numberOfAddressesToExport;

    public AddressExporter(AddressListProvider addressesProvider, File file, String documentFormat, int numberOfAddressesToExport) {
        this.addressesProvider = addressesProvider;
        this.file = file;
        this.documentFormat = documentFormat;
        this.numberOfAddressesToExport = numberOfAddressesToExport;
    }

    /**
     * Gibt eine Liste der unterst�tzen Erweiterungen zur�ck; 
     */
    public static String[][] getExtensions() {
        return extensions;
    }

    /**
     * Gibt eine Liste der Formatbeschreibungen zur�ck; 
     */
    public static String[] getDescriptions() {
        return descriptions;
    }

    /**
     * Gibt eine Liste der Formatkonstanten zur�ck; 
     */
    public static String[] getFileFormats() {
        return fileFormats;
    }


    /**
     * Exportiert die Adressliste in eine Datei mit dem angegebenen Format.
     *
     * @param addresses Die Liste der Adressen
     * @param file Export Datei
     * @param documentFormat Konstante f�r den Dokumenttyp
     */
    public void run(TaskManager.Context ctx) {

        try {

            canceled = false;

            Addresses addressesFromGUI = addressesProvider.getAddresses();
            
            /*
             *  copy addresses from GUI to a new addresslist to avoid that the list changes during the export
             */
            
            Addresses addresses = new AddressesImpl((OctopusDatabase) ApplicationServices.getInstance().getCurrentDatabase());
            
            AddressListParameter listparams = new AddressListParameter();
            PrimaryKeyList keys = addressesFromGUI.getPkList();
            listparams.setPkFilter(keys);
            addresses.setAddressListParameter(listparams);
            addresses.get(0); // needs to be called to initialize 
            
            if (addresses == null) {
                ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("AM_Export_Error"));
                return;
            }
            
            String sheetType = null;
            if (FORMAT_CSV_DOCUMENT.equals(documentFormat))
                sheetType = SpreadSheetFactory.TYPE_CSV_DOCUMENT;
            else if (FORMAT_ODS_DOCUMENT.equals(documentFormat))
                sheetType = SpreadSheetFactory.TYPE_ODS_DOCUMENT;
            else if (FORMAT_SXC_DOCUMENT.equals(documentFormat))
                sheetType = SpreadSheetFactory.TYPE_SXC_DOCUMENT;
            else 
                throw new RuntimeException(Messages.getString("AddressExporter_Unknown_Export_Format"));


            String[] spreadSheedHeader = Messages.getString("AddressExporter_Table_Header").split(";");
            String[] spreadSheedFields = Messages.getString("AddressExporter_Table_Fields").split(";");
            if (spreadSheedHeader.length != spreadSheedFields.length)
                logger.warning(Messages.getString("AddressExporter_Wrong_Column_Conts"));
            int cols = Math.min(spreadSheedHeader.length, spreadSheedFields.length);
            SpreadSheet sheet = SpreadSheetFactory.getSpreadSheet(sheetType);
            sheet.init();
            sheet.openTable(Messages.getString("AddressExporter_Table_Name"), cols);
            sheet.openRow();
            for (int i = 0; i < cols; i++)        
                sheet.addCell(spreadSheedHeader[i]);
            sheet.closeRow();

            int oldFetchSize = 0;
            if (addresses instanceof AddressesImpl) {
                oldFetchSize = ((AddressesImpl)addresses).getFetchSize();
                ((AddressesImpl)addresses).setFetchSize(200);
            }

            List correctIds = ((AddressesImpl) addresses).getPkList();
            int numberOfAddressesInList = correctIds.size();
            
            if (numberOfAddressesInList != numberOfAddressesToExport){
            	// the given list of addresses has not the same number of addresses as the user wanted to export.
                // this is normally the case when some of the addresses have been kept back by the server because the
                // user has no right to read the address(es). Sometimes this is caused by a change in usergroups or rightsmanagement.
                
            	if (Math.abs(numberOfAddressesInList - numberOfAddressesToExport) == 1)
            		ApplicationServices.getInstance().getCommonDialogServices().showInfo(Messages.getString("GUI_VersandAuftrag_Meldung_Falsche_Anzahl_Addressen_Single") + Messages.getString("GUI_VersandAuftrag_Meldung_Falsche_Anzahl_Addressen"));
            	           	
            	else
            		ApplicationServices.getInstance().getCommonDialogServices().showInfo(Messages.getFormattedString("GUI_VersandAuftrag_Meldung_Falsche_Anzahl_Addressen_Multi", Math.abs(numberOfAddressesInList - numberOfAddressesToExport)) + Messages.getString("GUI_VersandAuftrag_Meldung_Falsche_Anzahl_Addressen"));
            }   
            
            ctx.setGoal(addresses.getIndexMax());
            
            for (int i = 0; i <= addresses.getIndexMax() && !canceled; i++) {
                Address address = addresses.get(i);
                
                Integer currentId = new Integer(address.getId());
                correctIds.remove(currentId);
                sheet.openRow();
            
                for (int j = 0; j < cols; j++) {                
                	Object item = Pojo.get(address, spreadSheedFields[j]);
                    sheet.addCell(item == null ? "" : item.toString());
                }
                //if (i % 10 == 0) {
                    ctx.setCurrent(i);
                    ctx.setActivityDescription(Messages.getFormattedString("Export_Status", new Integer(i), new Integer(addresses.getIndexMax())));
               // }
                sheet.closeRow();
            }
            
            if (correctIds.size() > 0)
            	logger.warning("Folgende Adressen wurden scheinbar nicht exportiert:\n" + correctIds.toString());
          
            if (addresses instanceof AddressesImpl) {
                ((AddressesImpl)addresses).setFetchSize(oldFetchSize);
            }

            sheet.closeTable();

            OutputStream os = new FileOutputStream(file);
            sheet.save(os);
            os.flush();
            os.close();

            // should we remove the file here,
            // it the export was canceled?
            
        } catch (IOException e) {
            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("AM_Export_IoError"), e);
        } catch (Exception e) {
            ApplicationServices.getInstance().getCommonDialogServices().publishError(Messages.getString("AM_Export_Error"), e);
        }
    }

    public void cancel() {
        canceled = true;
    }
}
