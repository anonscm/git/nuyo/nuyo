/* tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
* Copyright (C) 2002 tarent GmbH

* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

* tarent GmbH., hereby disclaims all copyright
* interest in the program 'tarent-contact'
* (which makes passes at compilers) written
* by Hendrik Helwich. 
* signature of Elmar Geese, 1 June 2002
* Elmar Geese, CEO tarent GmbH
*/
package org.evolvis.nuyo.db;

/** 
 * Dieses Interface stellt Knoten in einem booleschen Ausdruck dar.
 * Es ist entweder ein Operator wie NOT, AND oder OR, oder ein 
 * Suchfeld.
 * 
 * @author hendrik
 */
public interface SearchAtom {
	
	public boolean isOperator();
	
	public SearchAtom getLeft();
	
	public SearchAtom getRight();
	
	public void addChild(SearchAtom atom);
}