package org.evolvis.nuyo.selector.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.evolvis.nuyo.gui.action.AddressListConsumer;
import org.evolvis.nuyo.gui.action.AddressListProvider;
import org.evolvis.nuyo.selector.EntityView;
import org.evolvis.xana.action.AbstractGUIAction;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.FormLayout;

import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.ui.EscapeDialog;

public class DetailViewAction extends AbstractGUIAction implements AddressListConsumer {

	private AddressListProvider provider;
	private static Logger logger = Logger.getLogger(DetailViewAction.class.getName());
	private String templateURL;
	private EntityList entities;


	public void registerDataProvider(AddressListProvider provider) {
		this.provider=provider;		
	}

	public void actionPerformed(ActionEvent e) {
		String templateAttribute = (String)this.getValue("templateName");
		int height = Integer.valueOf((String)this.getValue("height")).intValue();
		int width = Integer.valueOf((String)this.getValue("width")).intValue();
		
		
		templateURL = (this.getClass().getResource(templateAttribute)).getFile();
		JDialog dialog;
		if (provider!=null){
			entities = provider.getAddresses();
			if (entities.getSize()!=0){
				EntityView view = new EntityView(templateURL,entities);
				dialog = buildDialog(view.getComponent());
			} else {
				//no data selected
				dialog = buildDialog(buildErrorPanel("Keine Daten zum Anzeigen vorhanden"));
			}
			dialog.setSize(width,height);
			dialog.setVisible(true);
		} else logger.info("Kein Daten-Provider vorhanden");

	}
	
	private JPanel buildErrorPanel(String message){
		JPanel panel = new JPanel();
		JLabel label = new JLabel(message);
		panel.add(label);
		return panel;
	}

	//TODO: setAllwaysOnTop as soon as java 5 is allowed
	public JDialog buildDialog(JPanel panel){
		FormLayout layout = new FormLayout(
				"pref:GROW", //columns 
		"fill:pref:GROW");// rows
		PanelBuilder builder = new PanelBuilder(layout);
		builder.setDefaultDialogBorder();	
		builder.add(panel);

		JDialog dialog = new EscapeDialog();
		dialog.getContentPane().add(builder.getPanel());
		dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		dialog.setTitle("Detailansicht");
		dialog.pack();
		dialog.setModal(true);
		return dialog;
	}

}
