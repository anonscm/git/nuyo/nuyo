/* $Id: AddressCommand.java,v 1.5 2007/08/30 16:10:33 fkoester Exp $
 * 
 * Created on 09.10.2003
 */
package org.evolvis.nuyo.messages;

import java.util.EventObject;

import org.evolvis.nuyo.db.Address;


/**
 * Dies ist die Basisklasse f�r Kommandoklassen, die die aktuelle Adresse
 * steuern.
 *  
 * @author mikel
 */
public class AddressCommand extends EventObject implements CommandEvent {
    /*
     * Konstruktoren
     */
    /**
     * @param source Ereignisquelle
     * @param address betroffene Addresse
     */
    public AddressCommand(Object source, Address address) {
        super(source);
        this.address = address;
    }

    /*
     * Getter und Setter
     */
    /**
     * @return neue Adresse
     */
    public Address getAddress() {
        return address;
    }

    /*
     * Klasse Object
     */
    public String toString() {
        return super.toString() + "[address: " + address.getAdrNr() + ']';
    }

    /*
     * gesch�tzte Variablen 
     */
    Address address;
}
