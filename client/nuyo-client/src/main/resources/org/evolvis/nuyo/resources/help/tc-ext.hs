<?xml version='1.0' encoding='iso-8859-1'  ?>
<!DOCTYPE helpset  
PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 1.0//EN"         "http://java.sun.com/products/javahelp/helpset_1_0.dtd">
<helpset version="1.0">
<!-- title -->
<title>tc-ext</title>
<!-- maps -->
<maps>
<homeID>top</homeID>
<mapref location="tc-ext-map.jhm"/>
</maps>
<!-- views -->
<view>
<name>TOC</name>
<label>TOC</label>
<type>javax.help.TOCView</type>
<data>tc-ext-toc.xml</data>
</view>
<!-- views -->
<view>
<name>Index</name>
<label>Index</label>
<type>javax.help.IndexView</type>
<data>tc-ext-index.xml</data>
</view>
</helpset>
