/* $Id: MailConstants.java,v 1.2 2006/03/16 13:49:29 jens Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.nuyo.mail;


/**
 * Konstanten
 */
public interface MailConstants {
	/**
	 * Content
	 */
	public final static String CONTENT_STORELIST = "mailStoreList";
	public final static String CONTENT_STOREDETAIL = "mailStoreDetail";
	public final static String CONTENT_MAILLIST = "mailList";
	public final static String CONTENT_MAILLIST_INFO = "mailListInfo";
	public final static String CONTENT_MAILDETAIL = "mailDetail";
	public final static String CONTENT_MAILFOLDER = "mailFolder";
	public final static String CONTENT_FOLDERLIST = "folderList";
	public final static String CONTENT_FOLDERLIST_RECURSIV = "folderListRecursive";
	public final static String CONTENT_FOLDERLIST_ALL = "folderListAll";
	public final static String CONTENT_FOLDERDETAIL = "folderDetail";
	public final static String CONTENT_STOREID = "mailStoreID";
	public final static String CONTENT_FOLDERID = "mailFolderID";
	public final static String CONTENT_MESSAGEID = "mailID";
	public final static String CONTENT_STORESTATUS = "mailStoreStatus";
	public final static String CONTENT_ERROR = "mailError";
	public final static String CONTENT_EXCEPTION = "mailException";
	public final static String CONTENT_ATTACHMENTSTREAM = "mailstream";
	
	/**
	 * Parameter Navigation
	 */
	public final static String PARAM_STOREID = "store";
	public final static String PARAM_FOLDERID = "folder";
	public final static String PARAM_MESSAGEID = "message";
	public final static String PARAM_MESSAGEDATE = "date";
	public final static String PARAM_ATTACHMENTID = "attachment";
	public final static String PARAM_INDEXFROM = "from";
	public final static String PARAM_INDEXTO = "to";
	public final static String PARAM_INDEXSIZE = "size";
	public final static String PARAM_SORTKEY = "sort";
	public final static String PARAM_SORTREVERSE = "sortreverse";
	public final static String PARAM_ACTION = "action";
	public final static String PARAM_FILTER = "filter";
	public final static String PARAM_FLAG = "flag";
	public final static String PARAM_FLAGVALUE = "flagValue";
	public final static String PARAM_MESSAGEIDS = "messageids";
	public final static String PARAM_BOOLEAN = "bool";
	
	/**
	 * Parameter Konfiguration
	 */
	public final static String PARAM_DISPLAYNAME = "name";
	public final static String PARAM_SERVERNAME = "server";
	public final static String PARAM_SERVERPORT = "port";
	public final static String PARAM_SERVERPROTOCOL = "protocol";
	public final static String PARAM_USERNAME = "username";
	public final static String PARAM_PASSWORD = "password";
	public final static String PARAM_INBOXFOLDER = "inboxFolder";
	public final static String PARAM_SENTFOLDER = "sentFolder";
	public final static String PARAM_TRASHFOLDER = "trashFolder";
	
	/**
	 * Parameter-Values
	 */
	public final static String VALUE_REPLY_RE = "re";
	public final static String VALUE_REPLY_REALL = "reAll";
	public final static String VALUE_REPLY_FWD = "forward";
	public final static String VALUE_CONF_INSERT = "insertStore";
	public final static String VALUE_CONF_UPDATE = "updateStore";
	public final static String VALUE_CONF_REMOVE = "removeStore";
	public final static String VALUE_CONF_SETFOLDER = "setStoreFolder";
	
	/**
	 * MailStore
	 */
	public final static String PROTOCOL_IMAP = "imap";
	public final static String PROTOCOL_POP3 = "pop3";

	/**
	 * Server-Parameter
	 */
	public final static String SERVER_ID = "id";
	public final static String SERVER_PROTOCOL = "protocol";
	public final static String SERVER_NAME = "server";
	public final static String SERVER_PORT = "port";
	public final static String SERVER_USERNAME = "username";
	public final static String SERVER_PASSWORD = "password";
	public final static String SERVER_DISPLAYNAME = "displayName";
	public final static String SERVER_FOLDERINBOX = "inbox";
	public final static String SERVER_FOLDERSENT = "sent";
	public final static String SERVER_FOLDERTRASH = "trash";

	/**
	 * Folder-Parameter
	 */
	public final static String FOLDER_OBJECT = "folder";
	public final static String FOLDER_NAME = "name";
	public final static String FOLDER_FULLNAME = "fullname";
	public final static String FOLDER_COUNT_ALL = "count";
	public final static String FOLDER_COUNT_UNREAD = "unread";
	public final static String FOLDER_COUNT_NEW = "new";
	public final static String FOLDER_COUNT_DELETED = "deleted";
//	public final static String FOLDER_MAIL_LIST = "maillist";
//	public final static String FOLDER_SORT_KEY = "sortkey";
//	public final static String FOLDER_SORT_REVERSE = "sortreverse";
//	public final static String FOLDER_STATUS = "status";

	/**
	 * Session-Namen
	 */
	public final static String SESSION_STORES = "mailStores";

	/**
	 * Statuswerte
	 */
	public final static Integer STORESTATUS_NONE = new Integer(0);
	public final static Integer STORESTATUS_INIT = new Integer(100);
	public final static Integer STORESTATUS_CONNECT = new Integer(200);
	public final static Integer STORESTATUS_READY = new Integer(300);
	public final static Integer FOLDERSTATUS_NONE = new Integer(0);
	public final static Integer FOLDERSTATUS_INIT = new Integer(100);
	public final static Integer FOLDERSTATUS_CONNECT = new Integer(200);
	public final static Integer FOLDERSTATUS_READY = new Integer(300);
	
	/**
	 * Statuswerte
	 */
	public final static Integer JOBSTATUS_BUILDING = new Integer(0);
	public final static Integer JOBSTATUS_NEW = new Integer(1);
	public final static Integer JOBSTATUS_WORKING = new Integer(2);
	public final static Integer JOBSTATUS_DONE = new Integer(3);
	public final static Integer JOBSTATUS_ERROR = new Integer(4);
	
	/**
	 * Fehlermeldungen
	 */
	public final static String ERROR_INVALID_PROTOCOL = "invalid protocol";
	public final static String ERROR_INVALID_PORT = "invalid port";
	public final static String ERROR_INVALID_SERVER = "invalid server";
	
	/**
	 * Mail-Address - Keys in Map
	 */
	public static final String MAILADDRESS_STRING = "string";
	public static final String MAILADDRESS_NAME = "name";
	public static final String MAILADDRESS_MAIL = "mail";
	
	/**
	 * Mail-Daten - Keys in Map
	 */
	public static final String MESSAGE_ID = "id";
	public static final String MESSAGE_SIZE = "size";
	public static final String MESSAGE_FOLDER = "folder";
	public static final String MESSAGE_SUBJECT = "subject";
	public static final String MESSAGE_CONTENT = "content";
	public static final String MESSAGE_HEADER = "header";
	public static final String MESSAGE_TO = "to";
	public static final String MESSAGE_CC = "cc";
	public static final String MESSAGE_BCC = "bcc";
	public static final String MESSAGE_FROM = "from";
	public static final String MESSAGE_REPLYTO = "replyto";
	public static final String MESSAGE_SENTDATE = "sentdate";
	public static final String MESSAGE_FLAGS = "flags";
	
	/**
	 * Flags
	 */
	public static final String FLAGS_ANSWERED = "ANSWERED";
	public static final String FLAGS_DELETED = "DELETED";
	public static final String FLAGS_DRAFT = "DRAFT";
	public static final String FLAGS_FLAGGED = "FLAGGED";
	public static final String FLAGS_RECENT = "RECENT";
	public static final String FLAGS_SEEN = "SEEN";
	public static final String FLAGS_USER = "USER";

	/**
	 * Keys f�r Werte in Rekursiver Map,
	 * siehe MailHelper
	 */
	public static final String RECURSIVEMAP_ID = "id";
	public static final String RECURSIVEMAP_NAME = "name";
	public static final String RECURSIVEMAP_CHILDREN = "children";
	
	/**
	 * Keys f�r Filter
	 */
	public static final String FILTER_SIMPLEADDRESSES = "simpleAddresses";
	public static final String FILTER_USEINPUTSTREAMS = "useInputStreams";
}
