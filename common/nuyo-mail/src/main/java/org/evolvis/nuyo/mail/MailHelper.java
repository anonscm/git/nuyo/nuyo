/* $Id: MailHelper.java,v 1.9 2007/06/15 15:58:28 fkoester Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contact Management
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Christoph Jerolimov.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.nuyo.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.evolvis.nuyo.groupware.AddressProperty;

import de.tarent.commons.utils.Pojo;

/**
 * Hilfsklasse fr die eMail-Verwaltung.
 * 
 * @author Christoph Jerolimov <c.jerolimov@tarent.de>
 * @version $Revision: 1.9 $
 */
public abstract class MailHelper {
	
	private final static Logger logger = Logger.getLogger(MailHelper.class.getName());
	
	// static filter
	public final static Vector filter_all = new Vector();
	public final static Vector filter_cache = new Vector();
	public final static Vector filter_content = new Vector();
	static {
		// dieser wird insbesondere vom WebClient
		// in der Detail-Ansicht verwendet
		filter_all.add(MailConstants.MESSAGE_ID);
		filter_all.add(MailConstants.MESSAGE_SIZE);
		filter_all.add(MailConstants.MESSAGE_FOLDER);
		filter_all.add(MailConstants.MESSAGE_SUBJECT);
		filter_all.add(MailConstants.MESSAGE_CONTENT);
		filter_all.add(MailConstants.MESSAGE_HEADER);
		filter_all.add(MailConstants.MESSAGE_TO);
		filter_all.add(MailConstants.MESSAGE_CC);
		filter_all.add(MailConstants.MESSAGE_BCC);
		filter_all.add(MailConstants.MESSAGE_FROM);
		filter_all.add(MailConstants.MESSAGE_REPLYTO);
		filter_all.add(MailConstants.MESSAGE_SENTDATE);
		filter_all.add(MailConstants.MESSAGE_FLAGS);
		filter_all.add(MailConstants.FILTER_USEINPUTSTREAMS);
		
		filter_cache.add(MailConstants.MESSAGE_ID);
		filter_cache.add(MailConstants.MESSAGE_SUBJECT);
		filter_cache.add(MailConstants.MESSAGE_SENTDATE);
		filter_cache.add(MailConstants.MESSAGE_FROM);
		filter_cache.add(MailConstants.MESSAGE_FLAGS);
		
		filter_content.add(MailConstants.MESSAGE_CONTENT);
		filter_content.add(MailConstants.FILTER_USEINPUTSTREAMS);
	}
	
	
	/**
	 * Konvertiert eine Message in eine einfache Map.
	 * 
	 * Allgemeine Infos:
	 *   Integer  id
	 *   Integer  size
	 *   String   folder
	 * 
	 * Mail-Infos:
	 *   Map      header
	 *   String   subject
	 *   Object   content      (siehe getContent(...))
	 *   Map      contenttype  (siehe getContentType(...))
	 * 
	 * Mail-Adressen:
	 *   List     to           (siehe getAddressList(...))
	 *   List     cc           (siehe getAddressList(...))
	 *   List     bcc          (siehe getAddressList(...))
	 *   List     from         (siehe getAddressList(...))
	 *   List     replyto      (siehe getAddressList(...))
	 * 
	 * Mail-Status:
	 *   Date     sentDate
	 *   Map      flags        (siehe getMailFlags(...))
	 * 
	 * @param message
	 * 
	 * @param filter
	 *   Filter welche Daten angezeigt werden sollen, Key = MESSAGE_*
	 * @param simpleAddresses
	 *   Filter ob Addressen als String oder Map zurck gegeben werden.
	 *   siehe dazu getAddressList()
	 * @param attachment
	 *   Filter ob Attachment (InputStreams) in den Content gestellt werden
	 *   sollen oder nicht (null).
	 * 
	 * @return message als Map
	 */
	static final public Map getMailInfo(
			Message message,
			List filter,
			boolean simpleAddresses,
			boolean attachment
			) throws MailException {
		
		if (filter == null)
			throw new NullPointerException(); 
		
		Map result = new HashMap();
		
		// Allgemeine Infos: id, size, folder
		try {
			if (filter.contains(MailConstants.MESSAGE_ID))
				result.put(MailConstants.MESSAGE_ID, new Integer(message.getMessageNumber()));
			if (filter.contains(MailConstants.MESSAGE_SIZE))
				result.put(MailConstants.MESSAGE_SIZE, new Integer(message.getSize()));
			if (filter.contains(MailConstants.MESSAGE_FOLDER))
				result.put(MailConstants.MESSAGE_FOLDER, message.getFolder().getFullName());
		} catch (MessagingException e) {
			throw new MailException(e);
		}
		
		// Mail-Infos: subject, content, contenttype
		try {
			if (filter.contains(MailConstants.MESSAGE_SUBJECT))
				result.put(MailConstants.MESSAGE_SUBJECT, message.getSubject());
			if (filter.contains(MailConstants.MESSAGE_CONTENT))
				result.put(MailConstants.MESSAGE_CONTENT, getContent(
						new Vector(),
						message.getContent(),
						message.getContentType(),
						attachment));
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (MailException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Mail-Infos: header
		if (filter.contains(MailConstants.MESSAGE_HEADER)) {
			try {
				Map headerMap = new HashMap();
				Enumeration headers = message.getAllHeaders();
				while (headers.hasMoreElements()) {
					Header header = (Header)headers.nextElement();
					headerMap.put(header.getName(), header.getValue());
				}
				result.put(MailConstants.MESSAGE_HEADER, headerMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Mail-Adressen: to, cc, bcc, from, replyto
		try {
			if (filter.contains(MailConstants.MESSAGE_TO))
				result.put(MailConstants.MESSAGE_TO, getAddressList(message.getRecipients(Message.RecipientType.TO), simpleAddresses)); // An
			if (filter.contains(MailConstants.MESSAGE_CC))
				result.put(MailConstants.MESSAGE_CC, getAddressList(message.getRecipients(Message.RecipientType.CC), simpleAddresses)); // Cc
			if (filter.contains(MailConstants.MESSAGE_BCC))
				result.put(MailConstants.MESSAGE_BCC, getAddressList(message.getRecipients(Message.RecipientType.BCC), simpleAddresses)); // Bcc
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			if (filter.contains(MailConstants.MESSAGE_FROM))
				result.put(MailConstants.MESSAGE_FROM, getAddressList(message.getFrom(), simpleAddresses)); // Von
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			if (filter.contains(MailConstants.MESSAGE_REPLYTO))
				result.put(MailConstants.MESSAGE_REPLYTO, getAddressList(message.getReplyTo(), simpleAddresses)); // An wen zurck
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Mail-Status: sentDate
		try {
			if (filter.contains(MailConstants.MESSAGE_SENTDATE))
				if (message.getSentDate() != null)
					result.put(MailConstants.MESSAGE_SENTDATE, new Long(message.getSentDate().getTime()));
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Mail-Status: flags
		try {
			if (filter.contains(MailConstants.MESSAGE_FLAGS))
				result.put(MailConstants.MESSAGE_FLAGS, new Integer((message.getFlags().hashCode())));
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	
	/**
	 * Generiert aus den Informationen subject, sentdate und size
	 * einer eMail einen eindeutigen Hash-Code.
	 * 
	 * @throws MessagingException
	 */
	static final public int getMailHashCode(Message message) throws MessagingException {
		return (message.getSubject() != null ? message.getSubject().hashCode() : 0xAAAAAAAA) ^ message.getSize();
	}
	
	/**
	 * Vergleicht eine eMail-Nachricht mit dem bergebenen hashcode
	 * und sentdate, siehe getMailHashCode.
	 * 
	 * @see #getMailHashCode(Message)
	 * @throws MessagingException
	 */
	static final public boolean compareMail(Message message, int hashcode, long sentdate) throws MessagingException {
		return (((message.getSubject() != null ? message.getSubject().hashCode() : 0xAAAAAAAA) ^ message.getSize()) == hashcode)
				&& (message.getSentDate() != null ? message.getSentDate().getTime() == sentdate : false);
	}
	
	/**
	 * Schreibt den Content einer eMail-Nachricht in eine Liste.
	 * 
	 * Strings werden immer und InputStreams nur wenn attachment wahr
	 * ist (sonst null) in den Content geschrieben.
	 * Arbeitet bei Part und Multipart rekursiv.
	 * 
	 * Jeder Eintrag in der Liste entspricht einem Nachrichten-Teil
	 * der eMail und wird durch eine Map reprsentiert.
	 * Die Map enthlt zustzlich auch den ContentType.
	 * 
	 * @see #getContentType(String)
	 * 
	 * @param list In die der Content geschrieben wird.
	 * @param content Object das verschiedene 
	 * @param type String mit eMail-Content-Type
	 * @param attachment
	 * @return Liste mit Content-Elementen.
	 * @throws MailException
	 */
	static public List getContent(
			List list,
			Object content,
			String type,
			boolean attachment)
	throws MailException {
		
		if (content instanceof String) {
			Map map = getContentType(type);
			map.put("content", content);
			list.add(map);
		} else if (content instanceof Part) {
			try {
				Part part = (Part)content;
				getContent(list, part.getContent(), part.getContentType(), attachment);
			} catch (IOException e) {
				throw new MailException(e);
			} catch (MessagingException e) {
				throw new MailException(e);
			}
		} else if (content instanceof Multipart) {
			try {
				Multipart multipart = (Multipart)content;
				for (int i = 0; i < multipart.getCount(); i++) {
					getContent(list, multipart.getBodyPart(i), multipart.getContentType(), attachment);
				}
				return list;
			} catch (MessagingException e) {
				throw new MailException(e);
			}
		} else if (content instanceof InputStream) {
			Map map = getContentType(type);
			map.put("content", attachment ? content : null);
			list.add(map);
		} else {
			throw new MailException("unknown mail-content with " + content.getClass());
		}
		return list;
	}

	/**
	 * Gibt eine Map mit dem ContentType zurck.
	 * 
	 * @param contentType
	 *   text/plain; charset=ISO-8859-1; format=flowed
	 * 
	 * @return contentType als Map
	 *   "type" => "text/plain" (in lower case)
	 *   "charset" => "ISO-8859-1"
	 *   "format" => flowed
	 */
	static final private Map getContentType(String contentType) {
		Map result = new HashMap();
		
		if (contentType == null)
			return result;

		String[] infos = contentType.split(";");
		if (infos.length >= 1) {
			result.put("type", infos[0].trim().toLowerCase());
		}
		for (int i = 1; i < infos.length; i++) {
			int c = infos[i].indexOf(":") > infos[i].indexOf("=") ?
					infos[i].indexOf(":") : infos[i].indexOf("=");
			if (c != -1) {
				String key = infos[i].substring(0, c).trim().toLowerCase();
				String value = infos[i].substring(c + 1).trim();
				if (value.charAt(0) == '\"')
					value = value.substring(1, value.length());
				if (value.charAt(value.length() - 1) == '\"')
					value = value.substring(0, value.length() - 1);
				result.put(key, value);
			}
		}
		return result;
	}

//	/**
//	 * Gibt die Flags einer eMail als Map zurck.
//	 * 
//	 * @param messageFlags
//	 * @return messageFlags als Map
//	 */
//	static final public Map getMailFlags(Flags messageFlags) {
//		Map result = new HashMap();
//		Flags.Flag[] systemFlags = messageFlags.getSystemFlags();
//		for (int i = 0; i < systemFlags.length; i++) {
//			if (systemFlags[i] == Flags.Flag.ANSWERED) {
//				result.put(MailConstants.FLAGS_ANSWERED, Boolean.TRUE);
//			} else if (systemFlags[i] == Flags.Flag.DELETED) {
//				result.put(MailConstants.FLAGS_DELETED, Boolean.TRUE);
//			} else if (systemFlags[i] == Flags.Flag.DRAFT) {
//				result.put(MailConstants.FLAGS_DRAFT, Boolean.TRUE);
//			} else if (systemFlags[i] == Flags.Flag.FLAGGED) {
//				result.put(MailConstants.FLAGS_FLAGGED, Boolean.TRUE);
//			} else if (systemFlags[i] == Flags.Flag.RECENT) {
//				result.put(MailConstants.FLAGS_RECENT, Boolean.TRUE);
//			} else if (systemFlags[i] == Flags.Flag.SEEN) {
//				result.put(MailConstants.FLAGS_SEEN, Boolean.TRUE);
//			} else if (systemFlags[i] == Flags.Flag.USER) {
//				result.put(MailConstants.FLAGS_USER, Boolean.TRUE);
//			}
//		}
//		return result;
//	}

	/**
	 * Gibt das Address-Array als einfache Liste zurck.
	 * Jede eMail entspricht einem Eintrag der Liste und wird
	 * durch eine Map resprsentiert.
	 * 
	 * @see #MAILADDRESS_ISO_END
	 * @see #MAILADDRESS_ISO_QUOT
	 * @see #MAILADDRESS_ISO_START
	 * 
	 * @param addresses
	 * @param simpleAddresses
	 * @return addresses als List
	 */
	static final public List getAddressList(Address[] addresses, boolean simpleAddresses) {
		if (addresses == null) return null;
		Vector result = new Vector();
		for (int i = 0; i < addresses.length; i++) {
			if (simpleAddresses) {
				result.add(addresses[i].toString());
			} else {
				Map address = new HashMap();
				String string;
				try {
					string = new InternetAddress(addresses[i].toString()).toUnicodeString();
				} catch (AddressException e) {
					string = addresses[i].toString();
				}
				address.put(MailConstants.MAILADDRESS_STRING, string);
				int lt = string.indexOf(MAILADDRESS_ISO_START);
				int gt = string.indexOf(MAILADDRESS_ISO_END);
				if (lt > 1 && gt > 1 && lt < gt) {
					String name = string.substring(0, lt - 1);
					String mail = string.substring(lt + 1, gt);
					if (name.startsWith(MAILADDRESS_ISO_QUOT))
						name = name.substring(1);
					if (name.endsWith(MAILADDRESS_ISO_QUOT))
						name = name.substring(0, name.length() - 1);
					address.put(MailConstants.MAILADDRESS_NAME, name.trim());
					address.put(MailConstants.MAILADDRESS_MAIL, mail.trim());
				} else {
					address.put(MailConstants.MAILADDRESS_MAIL, string.trim());
				}
				result.add(address);
			}
		}
		return result;
	}
	
	/**
	 * Gibt aus dem Address-Array den ersten Namen zurck  
	 * 
	 * @param address
	 */
	static final public String getAddressName(Address address) {
		if (address != null)  {
			String result;
			try {
				result = new InternetAddress(address.toString()).toUnicodeString();
			} catch (AddressException e) {
				result = address.toString();
			}
			int lt = result.indexOf(MAILADDRESS_ISO_START);
			int gt = result.indexOf(MAILADDRESS_ISO_END);
			if (lt > 1 && gt > 1 && lt < gt) {
				result = result.substring(0, lt - 1);
				if (result.startsWith(MAILADDRESS_ISO_QUOT))
					result = result.substring(1);
				if (result.endsWith(MAILADDRESS_ISO_QUOT))
					result = result.substring(0, result.length() - 1);
			}
			return result.trim();
		}
		return null;
	}
	
	/**
	 * Gibt aus dem Address-Array den ersten Namen zurck  
	 * 
	 * @param address
	 */
	static final public String getAddressName(Address[] address) {
		if (address.length > 0)  {
			String result;
			try {
				result = new InternetAddress(address[0].toString()).toUnicodeString();
			} catch (AddressException e) {
				result = address[0].toString();
			}
			int lt = result.indexOf(MAILADDRESS_ISO_START);
			int gt = result.indexOf(MAILADDRESS_ISO_END);
			if (lt > 1 && gt > 1 && lt < gt) {
				result = result.substring(0, lt - 1);
				if (result.startsWith(MAILADDRESS_ISO_QUOT))
					result = result.substring(1);
				if (result.endsWith(MAILADDRESS_ISO_QUOT))
					result = result.substring(0, result.length() - 1);
			}
			return result.trim();
		}
		return null;
	}
	
	/**
	 * Gibt aus dem Address-Array die erste eMail-Adresse zurck.
	 * 
	 * @param address
	 */
	static final public String getAddressMail(Address address) {
		if (address != null)  {
			String result;
			try {
				result = new InternetAddress(address.toString()).toUnicodeString();
			} catch (AddressException e) {
				result = address.toString();
			}
			int lt = result.indexOf(MAILADDRESS_ISO_START);
			int gt = result.indexOf(MAILADDRESS_ISO_END);
			if (lt > 1 && gt > 1 && lt < gt) {
				result = result.substring(lt + 1, gt);
			}
			return result.trim();
		}
		return null;
	}
	
	/**
	 * Gibt aus dem Address-Array die erste eMail-Adresse zurck.
	 * 
	 * @param address
	 */
	static final public String getAddressMail(Address[] address) {
		if (address.length > 0)  {
			String result;
			try {
				result = new InternetAddress(address[0].toString()).toUnicodeString();
			} catch (AddressException e) {
				result = address[0].toString();
			}
			int lt = result.indexOf(MAILADDRESS_ISO_START);
			int gt = result.indexOf(MAILADDRESS_ISO_END);
			if (lt > 1 && gt > 1 && lt < gt) {
				result = result.substring(lt + 1, gt);
			}
			return result.trim();
		}
		return null;
	}
	
	/**
	 * Diese Methode erstellt eine rekursive Map.
	 * Der Seperator muss als Regulrer Ausdruck angegeben werden.
	 * 
	 * INBOX
	 *   current => 1
	 *   children =>
	 *     Sent
	 *       current => 2
	 *       children => null
	 *     Trash
	 *       current => 3
	 *       children => null
	 *     Bugs
	 *       current => 4
	 *       children => null
	 * 
	 * @param in
	 * @param regex
	 * @return rekursive map
	 */
	static final public Map createRecursiveMap(Map in, String regex) {
		Map result = new TreeMap();
		Iterator it = in.keySet().iterator();
		while (it.hasNext()) {
			Integer id = (Integer)it.next();
			String name = (String)((Map)in.get(id)).get(MailConstants.FOLDER_FULLNAME);
			String entry[] = name.split(regex, -1);
			Map current = result;
			int l = entry.length - 1;
			for (int i = 0; i < entry.length; i++) {
				if (!current.containsKey(entry[i])) {
					Map sub = new TreeMap();
					sub.put(MailConstants.RECURSIVEMAP_CHILDREN, new TreeMap());
					if (l == i) {
						sub.put(MailConstants.RECURSIVEMAP_ID, id);
						sub.put(MailConstants.RECURSIVEMAP_NAME, name);
					}
					current.put(entry[i], sub);
				} else {
					if (l == i) {
						((Map)current.get(entry[i])).put(MailConstants.RECURSIVEMAP_ID, id);
						((Map)current.get(entry[i])).put(MailConstants.RECURSIVEMAP_NAME, name);
					}
				}
				current = (Map)((Map)current.get(entry[i])).get(MailConstants.RECURSIVEMAP_CHILDREN);
			}
		}
		return result;
	}
	
	/**
	 * Quotet einen eMail-Text.
	 * 
	 * @param text
	 */
	static final public String quotation(String firstline, String text) {
		String line[] = TEXT_LINEPATTERN.split(text.trim());
		StringBuffer buffer = new StringBuffer(text.length() + 1000);
		buffer.append(TEXT_QUOTE).append(line[0]);
		
		if (line.length != 0) {
			boolean append = line[0].endsWith(" ");
			for (int i = 1; i < line.length; i++) {
				if (!append) buffer.append(TEXT_NLQUOTE);
				buffer.append(line[i]);
				append = line[i].endsWith(" ");
			}
		}
		
		int n = 0;
		int p = TEXT_LINEMAX;
		while ((p = buffer.indexOf(" ", p)) != -1) {
			n = buffer.indexOf("\r", n) - 1;
			if (p < n) {
				buffer.insert(p + 1, TEXT_NLQUOTE);
			} else {
				p = n += 2;
			}
			p += TEXT_LINEMAX;
		}
		if (firstline.length() > 0) {
			buffer.insert(0, firstline);
			buffer.insert(firstline.length(), "\r\n");
		}
		return buffer.toString();
	}
	
	private static final String MAILADDRESS_ISO_START = "<";
	private static final String MAILADDRESS_ISO_END = ">";
	private static final String MAILADDRESS_ISO_QUOT = "\"";

	private static final Pattern TEXT_LINEPATTERN = Pattern.compile("\r\n|\r|\n");
	private static final int TEXT_LINEMAX = 72;
	private static final String TEXT_QUOTE = "> ";
	private static final String TEXT_NLQUOTE = "\r\n> ";
	
	private static Pattern[] TEMPLATE_PATTERNS = null;
	
	/**
	 * Replaces address property keys in the given text template with actual
	 * values from an {@link de.tarent.groupware.Address} object.
	 * 
	 * @param template
	 * @param address
	 * @return
	 */
	public static String doTemplateReplacement(String template, Object dataObject)
	{
		// TODO: Usually used for mail body and subject. However then alot of work is done twice
		// and could be done more performant.
		List<AddressProperty> props = AddressProperty.getReplaceProperties();
		
		// 1. Erzeuge REGEX Patterns fürs Suchen und Ersetzen von Adressfeldern (falls es nich schon existiert)
		if (TEMPLATE_PATTERNS == null)
		{
			TEMPLATE_PATTERNS = new Pattern[props.size()];
			
			for (int i = 0; i< TEMPLATE_PATTERNS.length; i++)
			{
				TEMPLATE_PATTERNS[i] = Pattern.compile(props.get(i).getReplacementExpression());
			}
		}
		
		String cs = template;
		StringBuffer work = new StringBuffer(template.length());
		for (int i = 0; i< TEMPLATE_PATTERNS.length; i++)
		{
			Matcher m = TEMPLATE_PATTERNS[i].matcher(cs);
			while (m.find())
			{
                Object value = Pojo.get(dataObject, props.get(i).getKey());
				if(value != null){
					// first replace all '\' to prevent
					//	java.lang.StringIndexOutOfBoundsException: String index out of range: 28
					//	at java.lang.String.charAt(String.java:558)
					//	at java.util.regex.Matcher.appendReplacement(Matcher.java:696)
					
					String myString = Matcher.quoteReplacement(value.toString());
					try {
						m.appendReplacement(work, myString);
					} catch (StringIndexOutOfBoundsException e) {
						logger.warning("replacement for tag \""+props.get(i)+"\" failed for entry " + myString + ".");
					}
				}
				else
				{
					// we have no replacement for the tag, so just replace it with an empty string
					logger.warning("No replacement for tag \""+props.get(i)+"\" found. replacing with an empty string");
					m.appendReplacement(work, "");
				}
			}
			m.appendTail(work);
			
			cs = work.toString();
			work.setLength(0);
		}
		
		return cs;
	}
}
