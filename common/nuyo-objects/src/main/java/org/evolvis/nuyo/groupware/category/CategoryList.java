package org.evolvis.nuyo.groupware.category;

import java.util.ArrayList;
import java.util.Collection;


/**
 * List wrapper for Categories. This class is used to get a typemapping for axis working.
 */
public class CategoryList extends ArrayList {
    
    public CategoryList() {
    }
    
    public CategoryList(Collection c) {
        super(c);
    }
}