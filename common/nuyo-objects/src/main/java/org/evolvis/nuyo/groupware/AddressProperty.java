package org.evolvis.nuyo.groupware;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.tarent.commons.datahandling.entity.EntityProperty;

/**
 * <code>AdressProperty</code> provides type-safe keys for the address data
 * fields and allows indexed access to the property names as well as i18n.
 * 
 * <p>
 * Additionally <code>AddressProperty</code> can be in zero to many lists of
 * properties. This allows one to receive only a set of properties that are
 * interesting for a certain purpose (e.g. all properties which can be searched
 * for).
 * </p>
 * 
 * @author Robert Schuster, tarent GmbH Bonn
 */
public class AddressProperty extends EntityProperty implements Comparable<AddressProperty> {

        /** Collator used for sorting {@link AddressProperty} instances according
         * to the native language.
         * 
         * <p>Note that {@link Collator#getInstance()} returns a different instance
         * on each invocation so it is neccessary to keep one instance for all sort
         * requests in order to not waste objects.</p>
         */
        static Collator collator = Collator.getInstance();
  
	static
	{
		// HACK: This hack provokes the Address class to be initialized
		// whenever the AddressProperty class is touched. This behavior
		// is needed because Address' initialization causes static lists
		// in AdressProperty to be filled. Without Address being initialized
		// those lists would be empty. 
                try
                {
		Class.forName(Address.class.getName());
                }
                catch (ClassNotFoundException e)
                {
                  // Unlikely.
                }
	}

	/**
	 * Bit field denoting that a property is in no special property list.
	 * 
	 */
	public static final int NONE = 0;

	/**
	 * Bit field denoting that a property is internal to the application and not
	 * to be exposed to the user (e.g. database ids and such).
	 */
	public static final int INTERNAL = 1;

	/**
	 * Bit field denoting that a property can be searched for.
	 */
	public static final int SEARCH = 2;

	/**
	 * Bit field denoting that a property can be used for replacing values in a
	 * mail template.
	 */
	public static final int REPLACE = 4;

	private static final Map<String, AddressProperty> ALL_PROPS = new HashMap<String, AddressProperty>();

        private static final List<AddressProperty> COMMON_PROPS = new ArrayList<AddressProperty>();

	private static final List<AddressProperty> INTERNAL_PROPS = new ArrayList<AddressProperty>();

	private static final List<AddressProperty> SEARCH_PROPS = new ArrayList<AddressProperty>();

	private static final List<AddressProperty> REPLACE_PROPS = new ArrayList<AddressProperty>();

	private static ResourceBundle currentResourceBundle = PropertyResourceBundle
			.getBundle("org.evolvis.nuyo.groupware.addressproperty");

	/**
	 * Java class denoting the type of the address property.
	 */
	private Class dataType;

	/**
	 * Creates an <code>AddressProperty</code> which belongs only to the given
	 * lists.
	 * 
	 * @param name
	 * @param fields
	 *            bitfield denoting the list affiliation
	 * @return
	 */
	static AddressProperty make(String name, int fields) {
		return new AddressProperty(name, fields);
	}

	/**
	 * Creates an <code>AddressProperty</code> which
	 * <ul>
	 * <li>can be searched for</li>
	 * <li>can be used to be replaced in a mail template</li>
	 * </ul>
	 * </p>
	 * 
	 * @param name
	 * @return
	 */
	static AddressProperty make(String name) {
		return new AddressProperty(name, SEARCH | REPLACE);
	}

	private Class retrieveDataType() {
		if (key.startsWith("common"))
		{
			// Strips "common" and the two trailing digits from the key.
			String type = key.substring(6, key.length() - 2).intern();
			
			if (type == "text")
				return String.class;
			else if (type == "date")
				return Date.class;
			else if (type == "bool")
				return Boolean.class;
		    else if (type == "int")
		    	return Integer.class;
		    else if (type == "money")
		    	return Float.class;
		    else
		    	throw new IllegalStateException("Unknown datatype for common address property: " + key);
		}
		
		try {
			PropertyDescriptor descs[] = Introspector
					.getBeanInfo(Address.class).getPropertyDescriptors();

			for (int i = 0; i < descs.length; i++) {
				if (descs[i].getName().equals(key))
					return descs[i].getPropertyType();
			}

			throw new IllegalStateException(
					"Address property is not a bean property of Address class");
		} catch (IntrospectionException ie) {
			throw new IllegalStateException(
					"Unable to introspect Address class");
		}

	}

        /**
         * Creates an {@link AddressProperty} instance and puts it into
         * one or several internal lists for later retrieval for specific
         * uses.
         * 
         * <p>Note that the 'common' AddressProperty (those which start with <code>common</code>
         * in their name) are currently treated specially: Regardless of their field
         * value they are put into their own list and in no other. This is done to
         * prevent that they show up in the client's search dialog or are part of
         * mail template replacement process. There reason for this is that it is 
         * undecided how the common fields should be handled properly.
         * (2007-01-30 Robert Schuster)</p>
         * 
         * @param name
         * @param fields
         */
	private AddressProperty(String name, int fields) {
		super(name);
		
		dataType = retrieveDataType();

		ALL_PROPS.put(name, this);

                if (name.startsWith("common"))
                  {
                    COMMON_PROPS.add(this);
                    return;
                  }

		if ((fields & INTERNAL) > 0)
			INTERNAL_PROPS.add(this);

		if ((fields & SEARCH) > 0)
			SEARCH_PROPS.add(this);

		if ((fields & REPLACE) > 0)
			REPLACE_PROPS.add(this);
	}

    /** Retrieves a property with the given identifier.
     * 
     * @param name
     * @return
     */
    public static AddressProperty lookup(String name)
    {
        return ALL_PROPS.get(name);
    }

    /**
     * Returns the count off all properties
     */
    public static int getCount() {
        return ALL_PROPS.size();
    }
    
	/**
	 * Returns a list of all registered properties.
	 * 
	 * @return
	 */
	public static List<AddressProperty> getAllProperties() {
		return new ArrayList<AddressProperty>(ALL_PROPS.values());
	}

	/**
	 * Returns a list of all registered internal properties.
	 * 
	 * @return
	 */
	public static List<AddressProperty> getInternalProperties() {
		return new ArrayList<AddressProperty>(INTERNAL_PROPS);
	}

	/**
	 * Returns a list of all registered properties which can be searched for.
	 * 
	 * @return
	 */
	public static List<AddressProperty> getSearchProperties() {
		return new ArrayList<AddressProperty>(SEARCH_PROPS);
	}

        /** Returns a list of all registered properties which can be searched
         * for sorted by their label.
         * 
         * <p>The returned list is sorted suitable for displaying its elements
         * in a UI component.</p>
         * 
         * @return
         */
        public static List<AddressProperty> getSearchPropertiesSorted() {
          List<AddressProperty> l = getSearchProperties();
          
          Collections.sort(l);
          
          return l;
        }
        

	/**
	 * Returns a list of all registered properties which can be replaced with
	 * actual values using a mailtext- or office-template.
	 * 
	 * @return
	 */
	public static List<AddressProperty> getReplaceProperties() {
		return new ArrayList<AddressProperty>(REPLACE_PROPS);
	}

        /** Returns a list of all registered properties which can be replaced with
         * actual values using a mailtext- or office-template.
         * 
         * <p>The returned list is sorted suitable for displaying its elements
         * in a UI component.</p>
         * 
         * @return
         */
        public static List<AddressProperty> getReplacePropertiesSorted() {
          List<AddressProperty> l = getReplaceProperties();
          Collections.sort(l);
          
          return l;
        }

	/**
	 * Returns a human-readable localized label of the property.
	 * 
	 * <p>
	 * In case a property cannot be found the method returns the property's key
	 * quoted in exclamation marks ("!").
	 * </p>
	 * 
	 * <p>
	 * In case a property's label is empty the method returns the property's key
	 * quoted in question marks ("?").
	 * </p>
	 * 
	 */
	public String getLabel() {
		try {
			String res = currentResourceBundle.getString("ADDR_PROP_" + key);

			if (res.trim().length() == 0)
				return "?" + key + "?";

			return res;

		} catch (MissingResourceException e) {
			String bundleSuffix = "";
			if (!"".equals(currentResourceBundle.getLocale().getLanguage())) {
				bundleSuffix += "_"
						+ currentResourceBundle.getLocale().getLanguage();
			}

			if (!"".equals(currentResourceBundle.getLocale().getCountry())) {
				bundleSuffix += "_"
						+ currentResourceBundle.getLocale().getCountry();
			}
			return "!" + key + "!";
		}
	}

	/**
	 * Returns a string which is to be inserted in a mail text template to
	 * denote that this expression is to replaced for a serial letter.
	 * 
	 * @return
	 */
	public String getReplacementExpression() {
		return "<!" + key + "!>";
	}

	/**
	 * Returns the property'y datatype.
	 * 
	 * @return
	 */
	public Class getDataType() {
		return dataType;
	}

	public int compareTo(AddressProperty o)
	{
                // By using the collator sorting is done native language-wise
                // (and not Unicode codepoint value-wise).
		return collator.compare(getLabel(), o.getLabel());
	}

}
