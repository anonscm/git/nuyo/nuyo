package org.evolvis.nuyo.groupware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.evolvis.nuyo.groupware.Category;
import org.evolvis.nuyo.groupware.CategoryProperty;
import org.evolvis.nuyo.groupware.SubCategory;

import de.tarent.commons.datahandling.entity.EntityException;

/**
 * 
 * @author Nils Neumaier
 *
 */

public class CategoryImpl implements Category {
	
	
	protected HashMap data = new HashMap();
	
	
	public void addSubCategory(SubCategoryImpl subCategory) {
		if (getStandardData(PROPERTY_SUBCATEGORYS) == null)
			setStandardData(PROPERTY_SUBCATEGORYS, new ArrayList());
		((List)getStandardData(PROPERTY_SUBCATEGORYS)).add(subCategory);
	}

	public void fill(Category sourceCategory) {
	}

	public Object getAttribute(String id) {
		return getStandardData(id);
	}

	public String getCategoryName() {
		return saveToString(getStandardData(PROPERTY_CATEGORYNAME));  
	}

	public Integer getCategoryType() {
		return saveToInt(getStandardData(PROPERTY_CATEGORYTYPE));
	}

	public String getDescription() {
		return saveToString(getStandardData(PROPERTY_DESCRIPTION));
	}

	public int getId() {
		return new Integer(saveToInt(getStandardData(PROPERTY_ID)));
	}

	public Integer getPrivateUser() {
		return new Integer(saveToInt(getStandardData(PROPERTY_USERPRIVATE)));
	}
	
	public Boolean getRightToAddAddress() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTADD);
	}

	public Boolean getRightToAddSubCategory() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTADDSUBCAT);
	}

	public Boolean getRightToEditAddress() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTEDIT);
	}

	public Boolean getRightToReadAddress() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTREAD);
	}

	public Boolean getRightToRemoveAddress() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTREMOVE);
	}

	public Boolean getRightToRemoveSubCategory() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTREMOVESUBCAT);
	}
	
	public Boolean getRightToStructure() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTSTRUCTURE);
	}
	
	public Boolean getRightToGrant() {
		return (Boolean)getStandardData(Category.PROPERTY_RIGHTGRANT);
	}

	public Object getStandardData(String id) {
		return data.get(id);
	}

	public Object getStandardData(CategoryProperty prop) {
		return getStandardData(prop.getKey());
	}

	public SubCategory getSubCategory(int subCategoryPk) {
		List subcategories = (List)(getStandardData(PROPERTY_SUBCATEGORYS));
		for (Iterator iter = subcategories.iterator(); iter.hasNext();){
			SubCategory subcat = (SubCategory)iter.next();
			if (subcat.getId() == subCategoryPk)
				return subcat;
		}
		return null;
	}
	
	public List getSubCategories(){
		return (List)getStandardData(PROPERTY_SUBCATEGORYS);
	}
	
	public void setSubCategories(List subs) {
		setStandardData(PROPERTY_SUBCATEGORYS, subs);
	}

	public Boolean getPrivate() {
		return getStandardData(PROPERTY_ISPRIVATE) != null? ((Boolean)getStandardData(PROPERTY_ISPRIVATE)): new Boolean(false);
	}

	public Boolean getVirtual() {
		return getStandardData(PROPERTY_ISVIRTUAL) != null? ((Boolean)getStandardData(PROPERTY_ISVIRTUAL)): new Boolean(false);
	}

	public void removeSubCategory(int subCategoryPk) {
		List subcategories = (List)getStandardData(PROPERTY_SUBCATEGORYS);
		for (Iterator iter = subcategories.iterator(); iter.hasNext();){
			SubCategory subcat = (SubCategory)iter.next();
			if (subcat.getId() == subCategoryPk){
				subcategories.remove(subcat);
				break;
			}	
		}
	}

	public void removeAllSubCategories() {
		((List)getStandardData(PROPERTY_SUBCATEGORYS)).clear();
	}

	public void setAttribute(String id, Object newValue) throws EntityException {
		setStandardData(id, newValue);
	}

	public void setCategoryName(String categoryname) {
		setStandardData(PROPERTY_CATEGORYNAME, categoryname);
	}

	public void setCategoryType(Integer type) {
		setStandardData(PROPERTY_CATEGORYTYPE, type);
	}

	public void setDescription(String description) {
		setStandardData(PROPERTY_DESCRIPTION, description);
	}

	public void setId(int newid) {
		setStandardData(PROPERTY_ID, new Integer(newid));
	}

	public void setPrivate(Boolean isprivate) {
		setStandardData(PROPERTY_ISPRIVATE, isprivate);
	}

	public void setPrivateUser(Integer userId) {
		setStandardData(PROPERTY_USERPRIVATE, userId);
	}

	public void setStandardData(String id, Object newValue) {
		data.put(id, newValue);
	}

	public void setStandardData(CategoryProperty prop, Object newValue) {
		setStandardData(prop.getKey(), newValue);
	}

//	public void setSubCategory(SubCategory subcategories) {
//		setStandardData(PROPERTY_SUBCATEGORYS, subcategories);
//	}

	public void setVirtual(Boolean isvirtual) {
		setStandardData(PROPERTY_ISVIRTUAL,isvirtual);
	}
	
	
	/**
	 * Diese Methode wendet auf den Parameter {@link Object#toString()} an,
	 * sofern er nicht <code>null</code> ist.
	 * 
	 * @param o das in einen String zu ueberfuehrende Objekt.
	 * @return die String-Darstellung des Objekts. 
	 */    
	public static String saveToString(Object o) {
		return (o == null) ? null : o.toString();
	}	

    /**
     * Returns the int value of the object or 0 as default
     */
	public static int saveToInt(Object o) {
		return (o instanceof Integer) ? ((Integer)o).intValue() : 0;
	}	

	public boolean equals(Object category){
		return (category instanceof Category && equals((Category)category));
	}
	
	public boolean equals(Category category){
		return (this.getId() == category.getId());
	}

	public void setRightToAddAddress(Boolean right) {
		setStandardData(PROPERTY_RIGHTADD, right);
	}

	public void setRightToAddSubCategory(Boolean right) {
		setStandardData(PROPERTY_RIGHTADDSUBCAT, right);
	}

	public void setRightToEditAddress(Boolean right) {
		setStandardData(PROPERTY_RIGHTEDIT, right);
	}

	public void setRightToGrant(Boolean right) {
		setStandardData(PROPERTY_RIGHTGRANT, right);	
	}

	public void setRightToReadAddress(Boolean right) {
		setStandardData(PROPERTY_RIGHTREAD, right);	
	}

	public void setRightToRemoveAddress(Boolean right) {
		setStandardData(PROPERTY_RIGHTREMOVE, right);		
	}

	public void setRightToRemoveSubCategory(Boolean right) {
		setStandardData(PROPERTY_RIGHTREMOVESUBCAT, right);		
	}

	public void setRightToStructure(Boolean right) {
		setStandardData(PROPERTY_RIGHTSTRUCTURE, right);		
	}
	
}
