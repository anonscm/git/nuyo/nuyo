/* $Id: Category.java,v 1.8 2007/07/17 13:16:20 nils Exp $
 * 
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * 
 * Copyright (C) 2002 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke. 
 * 
 * signature of Elmar Geese, 1 June 2002
 * 
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.nuyo.groupware;

import java.util.List;

import org.evolvis.nuyo.groupware.impl.SubCategoryImpl;

import de.tarent.commons.datahandling.entity.Entity;
import de.tarent.commons.datahandling.entity.EntityException;

/**
 * This is the basic interface for a Category.
 *
 * @author Nils Neumaier, tarent GmbH
 */
public interface Category extends Entity {

	public final static int TYPE_NORMAL = 1;
    public final static int TYPE_PUBLIC = 10;
    public final static int TYPE_TRASH = 11;
    public final static int TYPE_PRIVATE = 12;
    public final static int TYPE_OTHER = 13;
    public final static int TYPE_ALL_ADDRESSES = 14;
    public final static int TYPE_REVIEW = 15;
	
    public final static String propertyNamePrefix = "CAT_PROP_";
	
    /**
     * Fills this Category with all fields of the supplied source Category. The id-field will not be modified.
     */
    public void fill(Category sourceCategory);
	

    // generic methods for setting and retrieving properties

	public Object getStandardData(String id);

    public Object getStandardData(CategoryProperty prop);
	
	/**
	 * Diese Methode liefert Standarddaten zur bergebenen Identifikation.
	 * 
	 * @param id Identifikator, vergleich hier zu auch
	 *  {@link Database#getExtendedFields() Database.getExtendedFields()}.
	 * @param newValue die neuen Standarddaten.
	 */
	public void setStandardData(String id, Object newValue);
	
	public void setStandardData(CategoryProperty prop, Object newValue);
        

	/**
	 * this method is necessary for the implemented interface Entity
	 */
	public Object getAttribute(String id);
	

	/**
	 * this method is necessary for the implemented interface Entity
	 * @param id
	 * @param newValue
	 * @throws ContactDBException
	 */
	public void setAttribute(String id, Object newValue) throws EntityException;



    // getXXX, setXXX methods
	
	public final static CategoryProperty PROPERTY_ID = CategoryProperty.make("id");
	/**
	 * Diese Methode liefert die ID der Adresse. Diese entspricht der AdrNr
	 */
    public int getId();	

    /**
     * Sets the id of the object. The Id is the same as the adrNr
     */
    public void setId(int newid);
    
    
	
	public final static CategoryProperty PROPERTY_CATEGORYNAME = CategoryProperty.make("categoryName");
	/**
	 * Returns the name of the category.
	 * @return String
	 */
	public String getCategoryName();
	
	/**
	 * Sets the name of the category.
	 * @param vorname The name to set
	 */
	public void setCategoryName(String categoryname);




	public final static CategoryProperty PROPERTY_DESCRIPTION = CategoryProperty.make("description");
	/**
	 * @return the description
	 */
	public String getDescription();
	
	/**
	 * @param description the description of the category
	 */
	public void setDescription(String description);	
	
	
	
	public final static CategoryProperty PROPERTY_CATEGORYTYPE = CategoryProperty.make("categoryType");
	/**
	 * @return the type of the category
	 */
	public Integer getCategoryType();
	
	/**
	 * @param description the description of the category
	 */
	public void setCategoryType(Integer type);	
    
	
	
	public final static CategoryProperty PROPERTY_ISVIRTUAL = CategoryProperty.make("virtual");
	/**
	 * @return flag if this category is virtual
	 */
	public Boolean getVirtual();
	
	/**
	 * @param isvirtual
	 */
	public void setVirtual(Boolean isvirtual);
	
	
	
	public final static CategoryProperty PROPERTY_ISPRIVATE = CategoryProperty.make("private");
	/**
	 * @return flag if this category is private
	 */
	public Boolean getPrivate();
	
	/**
	 * @param isprivate
	 */
	public void setPrivate(Boolean isprivate);
	
	
	
	public final static CategoryProperty PROPERTY_USERPRIVATE = CategoryProperty.make("privateUser");
	/**
	 * @return assigned user if this is a private category
	 */
	public Integer getPrivateUser();
	
	/**
	 * @param userId
	 */
	public void setPrivateUser(Integer userId);
	
	
	
	public final static CategoryProperty PROPERTY_SUBCATEGORYS = CategoryProperty.make("subCategory");
	
	/**
	 * @return one subcategory with the given primary key
	 */
    public SubCategory getSubCategory(int subCategoryPk);
    
    /**
     * @return List of all subcategories of this category
     */
    public List getSubCategories();
    
    /**
     * @param subs List of Subcategory object
     */
    public void setSubCategories(List subs);
    
    /**	
     * assigns a subcategory to this category
     * @param subCategory 
     */ 
    public void addSubCategory(SubCategoryImpl subCategory);
    
    /**	deassigns a subcategory from this category
     * @param subCategory
     */
    public void removeSubCategory(int subCategoryPk);
    
    /**
     * deassigns all subcategories from this category
     */
    public void removeAllSubCategories();
    
    
    /***************************
     * Accessright Properties
     ***************************/
    
    
    public final static CategoryProperty PROPERTY_RIGHTREAD = CategoryProperty.make("rightToReadAddress");
    
    /**
     * @return right to read addresses in this category
     */
    public Boolean getRightToReadAddress();
    
    public void setRightToReadAddress(Boolean right);
    
    
    public final static CategoryProperty PROPERTY_RIGHTEDIT = CategoryProperty.make("rightToEditAddress");
    
    /**
     * @return right to edit addresses in this category
     */
    public Boolean getRightToEditAddress();
    
    public void setRightToEditAddress(Boolean right);
    
    /**
     * @return right to add addresses to this category
     */
    public final static CategoryProperty PROPERTY_RIGHTADD = CategoryProperty.make("rightToAddAddress");
	
    public Boolean getRightToAddAddress();
    
    public void setRightToAddAddress(Boolean right);
    
    
    public final static CategoryProperty PROPERTY_RIGHTREMOVE = CategoryProperty.make("rightToRemoveAddress");
	
    /**
     * @return right to remove addresses from this category
     */
    public Boolean getRightToRemoveAddress();
    
    public void setRightToRemoveAddress(Boolean right);
    
    
    public final static CategoryProperty PROPERTY_RIGHTADDSUBCAT = CategoryProperty.make("rightToAddSubCategory");
    
    /**
     * @return right to add subcategories to this category
     */
    public Boolean getRightToAddSubCategory();
    
    public void setRightToAddSubCategory(Boolean right);
    
    
    public final static CategoryProperty PROPERTY_RIGHTREMOVESUBCAT = CategoryProperty.make("rightToRemoveSubCategory");
    
    /**
     * @return right to remove subcategories from this category
     */
    public Boolean getRightToRemoveSubCategory();    
    
    public void setRightToRemoveSubCategory(Boolean right);
    
    
    public final static CategoryProperty PROPERTY_RIGHTSTRUCTURE = CategoryProperty.make("rightToStructure");
    /**
     * @return right to structure addresses in this category and its subcategories
     */
    public Boolean getRightToStructure();    
    
    public void setRightToStructure(Boolean right);
    
    
    public final static CategoryProperty PROPERTY_RIGHTGRANT = CategoryProperty.make("rightToGrant");
    
    /**
     * @return right to assign roles/rights on this category
     */
    public Boolean getRightToGrant();    
    
    public void setRightToGrant(Boolean right);
    
}  
